/*    */ package org.apache.commons.codec.binary;
/*    */ 
/*    */ import java.io.OutputStream;
/*    */ 
/*    */ public class Base64OutputStream extends BaseNCodecOutputStream
/*    */ {
/*    */   public Base64OutputStream(OutputStream out)
/*    */   {
/* 53 */     this(out, true);
/*    */   }
/*    */ 
/*    */   public Base64OutputStream(OutputStream out, boolean doEncode)
/*    */   {
/* 66 */     super(out, new NewBase64(false), doEncode);
/*    */   }
/*    */ 
/*    */   public Base64OutputStream(OutputStream out, boolean doEncode, int lineLength, byte[] lineSeparator)
/*    */   {
/* 86 */     super(out, new NewBase64(lineLength, lineSeparator), doEncode);
/*    */   }
/*    */ }

/* Location:           C:\Users\wangxin\Desktop\commons-codec-1.6.jar
 * Qualified Name:     org.apache.commons.codec.binary.Base64OutputStream
 * JD-Core Version:    0.5.3
 */