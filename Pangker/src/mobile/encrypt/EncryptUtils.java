package mobile.encrypt;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mobile.http.Parameter;

/**
 * @author fitch
 * @createtime 2012-6-13 下午3:02:05
 * @version 1.0
 * @desc 客户生加密处理类
 */
public class EncryptUtils {
	// 参数分隔符
	private final static String Separator = "$";
	// 密钥
	private static String strKey = "6DC7BC0758C8E310015E922C0DA87F9DA76B8040A8AB67F8";
	// 密钥向量
	private static byte[] defaultIV = { 1, 2, 3, 4, 5, 6, 7, 8 };
	private static final String appid = "10000";
	private static final String CodingType = "UTF-8";
	


	/**
	 * paras 包括所有请求参数，Authenticator除外。
	 * 
	 * @param paras
	 * @return
	 * @throws Exception
	 */
	public static String getAuthenticator(List<Parameter> paras) throws Exception {
		try {
			if (paras == null || paras.size() == 0) {
				return "";
			}
			Parameter[] ps = paras.toArray(new Parameter[paras.size()]);
			Arrays.sort(ps);
			StringBuffer br = new StringBuffer();
			for (int i = 0; i < ps.length; i++) {
				if (i != 0) {
					br.append(Separator);
				}
				br.append(ps[i].mValue);
			}
			String digest = Des3.GenerateDigest(br.toString());
			br.append(Separator).append(digest);
			return Des3.Encrypt(br.toString(), strKey, defaultIV);
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}

	}

	public static String getAuthenticator(String s) throws Exception {
		try {
			if (s == null || s.length() == 0) {
				return "";
			}
			return Des3.Encrypt(s, strKey, defaultIV);
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}
	}

	public static String encrypt(String s) throws Exception {
		// TODO Auto-generated constructor stub
		if (s == null || s.length() == 0) {
			return null;
		}
		return Des3.Encrypt(s, strKey, defaultIV);
	}

	public static String decrypt(String encrypt) throws Exception {
		// TODO Auto-generated method stub
		return Des3.Decrypt(encrypt, strKey, defaultIV);
	}

	/**
	 * 处理请求参数
	 * 
	 * @param appId
	 *            应用ID
	 * @param userId
	 *            用户ID
	 * @param imsi
	 *            IM号
	 * @param sysTime
	 *            请求时间
	 * @param url
	 *            请求action
	 * @return
	 * @throws Exception
	 */
	public static String getAuthString(String appId, String userId, String imsi, String sysTime, String url)
			throws Exception {
		StringBuffer token = new StringBuffer();
		token.append(appId == null ? "" : appId);
		token.append(Separator);
		token.append(userId == null ? "" : userId);
		token.append(Separator);
		token.append(imsi == null ? "" : imsi);
		token.append(Separator);
		token.append(sysTime == null ? "" : sysTime);
		token.append(Separator + Des3.GenerateDigest(token.toString()));
		StringBuffer back = new StringBuffer();
		back.append(appId);
		back.append(Separator);
		back.append(Des3.Encrypt(token.toString(), strKey, defaultIV));
		return url + "?t=" + URLEncoder.encode(back.toString(), CodingType);
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras
				.add(new Parameter(
						"c",
						"我是中国人我是中国人我是中我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人国人我是中国人我是中国人我是中国人我是中国人我是中国人我是中国人"));
		paras.add(new Parameter("b", "b"));
		paras.add(new Parameter("bbbb", "bbb$$bb"));
		paras.add(new Parameter("a", "a"));
		paras.add(new Parameter("aa", "aa"));

		String str = getAuthenticator(paras);
		System.out.println("---" + str);
		System.out.println(Des3.Decrypt(str, strKey, defaultIV));
	}

}
