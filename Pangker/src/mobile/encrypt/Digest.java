package mobile.encrypt;

import java.security.MessageDigest;

public class Digest {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	/**
	 * MD5算法
	 */
	public static String algorithm_MD5 = "MD5";

	/**
	 * SHA-1算法
	 */

	public static String algorithm_SHA = "SHA-1";

	private String algorithm;

	private MessageDigest messageDigest;

	public Digest(String algorithm) throws Exception {
		this.algorithm = algorithm;
		messageDigest = MessageDigest.getInstance(this.algorithm);
	}

	/**
	 * 取得basic64的MD5的值
	 * 
	 * @param text
	 *            String
	 * @return String
	 */
	public byte[] crypt(byte[] content) throws Exception {
		messageDigest.reset();
		messageDigest.update(content);
		return messageDigest.digest();
	}

	public byte[] crypt(String key, byte[] content) throws Exception {
		messageDigest.reset();
		if (key == null || key.equals("")) {
		} else {
			messageDigest.update(key.getBytes());
			messageDigest.update(content);
		}
		return messageDigest.digest();
	}
}
