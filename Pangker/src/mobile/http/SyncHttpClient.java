package mobile.http;

import java.util.Iterator;
import java.util.List;

import mobile.encrypt.EncryptUtils;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.PartBase;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;

import android.util.Log;

/**
 * 
 * @author: feijinbo eidt by >>>>wangxin
 * @createTime: 2012-1-12 下午2:24:16
 * @version: 1.0
 * @desc :HttpClient 公共方法
 */
public class SyncHttpClient {
	// 超时等待
	private static final int CONNECTION_TIMEOUT = 5000;

	// 字符集
	private static final String CONTENT_CHARSET = "UTF-8";

	/**
	 * Using POST method.
	 * 
	 * @param url
	 *            The remote URL.
	 * @param queryString
	 *            The query string containing parameters
	 * @return Response string.
	 * @throws Exception
	 */
	public String httpPost(String url, String queryString) throws Exception {
		String responseData = null;
		HttpClient httpClient = new HttpClient();
		PostMethod httpPost = new PostMethod(url);
		// 设置超时
		httpPost.getParams().setParameter("http.socket.timeout", CONNECTION_TIMEOUT);
		// 设置字符集
		httpPost.addRequestHeader("Content-Type", "text/html; charset=utf-8");
		if (queryString != null && !queryString.equals("")) {
			httpPost.setRequestEntity(new ByteArrayRequestEntity(queryString
					.getBytes()));
		}
		try {
			int statusCode = httpClient.executeMethod(httpPost);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("HttpPost Method failed: "
						+ httpPost.getStatusLine());
			}
			responseData = httpPost.getResponseBodyAsString();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			httpPost.releaseConnection();
			httpClient = null;
		}

		return responseData;
	}

	/**
	 * post by pairs
	 * 
	 * @param url
	 * @param pairs
	 * @return
	 * @throws Exception
	 */
	public String httpPost(String url, List<Parameter> parameters, String systime)
			throws Exception {
		String responseData = null;
		HttpClient httpClient = new HttpClient();
		PostMethod httpPost = new PostMethod(url);
		// 设置超时
		httpPost.getParams().setParameter("http.socket.timeout", CONNECTION_TIMEOUT);
		// 设置字符集
		httpPost.getParams().setParameter(
				HttpMethodParams.HTTP_CONTENT_CHARSET, CONTENT_CHARSET);
		// add by wangxin start
		parameters.add(new Parameter("systime", systime));

		String authenticator = EncryptUtils.getAuthenticator(parameters);
		parameters.add(new Parameter("authenticator", authenticator));
		// add by wangxin end
		// 添加参数
		Log.i("http", "url==" + url);
		for (Iterator<Parameter> iter = parameters.iterator(); iter.hasNext();) {
			Parameter para = iter.next();
			httpPost.addParameter(para.mName == null ? "" : para.mName,
					para.mValue == null ? "" : para.mValue);
		}
		try {
			int statusCode = httpClient.executeMethod(httpPost);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("HttpPost Method failed: "
						+ httpPost.getStatusLine());
			}
			// Read the response body.
			responseData = httpPost.getResponseBodyAsString();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			httpPost.releaseConnection();
			httpClient = null;
		}

		return responseData;
	}

	/**
	 * Using POST method with multiParts.
	 * 
	 * @param url
	 *            the request url.
	 * @param params
	 * @param fileName
	 *            must provide
	 * @param file
	 * @return
	 * @throws Exception
	 */

	public String httpPostWithFile(String url, List<Parameter> params,
			String fileParamName, PartBase filePart, String systime)
			throws Exception {
		String responseData = null;
		HttpClient httpClient = new HttpClient();
		PostMethod httpPost = new PostMethod(url);
		try {
			// add by wangxin start
			params.add(new Parameter("systime", systime));

			String authenticator = EncryptUtils.getAuthenticator(params);
			params.add(new Parameter("authenticator", authenticator));
			// add by wangxin end
			int length = params.size() + (filePart != null ? 1 : 0);
			Part[] parts = new Part[length];
			int i = 0;
			for (Parameter entry : params) {
				parts[i++] = new StringPart(entry.mName, entry.mValue,
						CONTENT_CHARSET);
			}
			if (filePart != null)
				parts[i++] = filePart;
			// 设置重试次数
			httpPost.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(3, false));
			httpPost.setRequestEntity(new MultipartRequestEntity(parts,
					httpPost.getParams()));
			int statusCode = httpClient.executeMethod(httpPost);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("HttpPost Method failed: "
						+ httpPost.getStatusLine());
			}
			responseData = httpPost.getResponseBodyAsString();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			httpPost.releaseConnection();
			httpClient = null;
		}
		return responseData;
	}

	/**
	 * Using POST method with byte file
	 * 
	 * @param url
	 * @param queryString
	 * @param byData
	 * @return
	 * @throws Exception
	 */

	public String httpPostByteFile(String url, String queryString, byte[] byData)
			throws Exception {
		String responseData = null;
		url += '?' + queryString;
		HttpClient httpClient = new HttpClient();
		PostMethod httpPost = new PostMethod(url);
		try {
			httpPost.setRequestEntity(new ByteArrayRequestEntity(byData));
			int statusCode = httpClient.executeMethod(httpPost);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("HttpPost Method failed: "
						+ httpPost.getStatusLine());
			}
			// Read the response body.
			responseData = httpPost.getResponseBodyAsString();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			httpPost.releaseConnection();
			httpClient = null;
		}
		return responseData;
	}

	/**
	 * 上传资源文件，资源带封面
	 * 
	 * @param url
	 * @param params
	 * @param fileParamName
	 * @param file
	 * @param cover
	 * @return
	 * @throws Exception
	 */
	public String httpPostWithFile(String url, List<Parameter> params,
			PartBase filePart, byte[] cover, String systime) throws Exception {
		String responseData = null;
		HttpClient httpClient = new HttpClient();
		PostMethod httpPost = new PostMethod(url);
		try {
			// add by wangxin start
			params.add(new Parameter("systime", systime));

			String authenticator = EncryptUtils.getAuthenticator(params);
			params.add(new Parameter("authenticator", authenticator));
			// add by wangxin end

			int length = params.size() + (filePart != null ? 1 : 0)
					+ (cover != null && cover.length > 0 ? 1 : 0);
			Part[] parts = new Part[length];
			int i = 0;
			for (Parameter entry : params) {
				parts[i++] = new StringPart(entry.mName, entry.mValue,
						CONTENT_CHARSET);
			}
			if (filePart != null)
				parts[i++] = filePart;
			// 添加封面
			if (cover != null && cover.length > 0) {
				parts[i++] = new ByteArrayPart(cover, "pk_cover", null);
			}
			httpPost.setRequestEntity(new MultipartRequestEntity(parts,
					httpPost.getParams()));
			int statusCode = httpClient.executeMethod(httpPost);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("HttpPost Method failed: "
						+ httpPost.getStatusLine());
			}
			responseData = httpPost.getResponseBodyAsString();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			httpPost.releaseConnection();
			httpClient = null;
		}
		return responseData;
	}

}
