package mobile.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.httpclient.methods.multipart.FilePartSource;
import org.apache.commons.httpclient.methods.multipart.PartBase;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.apache.commons.httpclient.util.EncodingUtil;

import com.wachoo.pangker.downupload.ProgressListener;
import com.wachoo.pangker.entity.UploadJob;

/**
 * @author: wangxin
 * @createTime: 2012-2-7 下午1:47:38
 * @version: 1.0
 * @desc :文件上传功能类，解决FileItem中文乱码问题.
 */
public class MyFilePart extends PartBase {
	// >>>>>>>上传文件长度
	private int fileLen;
	// >>>>>>>>每次通知间隔时间
	public static int noticeSend = 2000;

	public static final String DEFAULT_CONTENT_TYPE = "application/octet-stream";
	public static final String DEFAULT_CHARSET = "ISO-8859-1";
	public static final String DEFAULT_TRANSFER_ENCODING = "binary";

	protected static final String FILE_NAME = "; filename=";
	private static final byte[] FILE_NAME_BYTES = EncodingUtil.getAsciiBytes("; filename=");
	private PartSource source;
	// add by >>>> lb
	private boolean ifCancel = false;

	// add by >>>> wangxin
	private ProgressListener listener;

	public ProgressListener getListener() {
		return listener;
	}

	public void setListener(ProgressListener listener) {
		this.listener = listener;
	}

	public MyFilePart(String name, PartSource partSource, String contentType, String charset,
			ProgressListener listener) {
		super(name, contentType == null ? "application/octet-stream" : contentType,
				charset == null ? "ISO-8859-1" : charset, "binary");

		if (partSource == null) {
			throw new IllegalArgumentException("Source may not be null");
		}
		this.source = partSource;
		this.listener = listener;
	}

	public MyFilePart(String name, PartSource partSource, ProgressListener listener) {
		this(name, partSource, null, null, listener);

	}

	public MyFilePart(String name, File file, ProgressListener listener) throws FileNotFoundException {
		this(name, new FilePartSource(file), null, null, listener);
		this.fileLen = (int) file.length();
	}

	public MyFilePart(String name, File file, String contentType, String charset, ProgressListener listener)
			throws FileNotFoundException {
		this(name, new FilePartSource(file), contentType, charset, listener);
		this.fileLen = (int) file.length();
	}

	public MyFilePart(String name, String fileName, File file, ProgressListener listener)
			throws FileNotFoundException {
		this(name, new FilePartSource(fileName, file), null, null, listener);
		this.fileLen = (int) file.length();
	}

	public MyFilePart(File file, String charset, ProgressListener listener) throws FileNotFoundException {
		this(file.getAbsolutePath(), new FilePartSource(file.getName(), file), new MimetypesFileTypeMap()
				.getContentType(file), charset, listener);
		this.fileLen = (int) file.length();
	}

	public MyFilePart(String name, String fileName, File file, String contentType, String charset,
			ProgressListener listener) throws FileNotFoundException {
		this(name, new FilePartSource(fileName, file), contentType, charset, listener);
		this.fileLen = (int) file.length();
	}

	protected void sendDispositionHeader(OutputStream out) throws IOException {
		// LOG.trace("enter sendDispositionHeader(OutputStream out)");
		super.sendDispositionHeader(out);
		String filename = this.source.getFileName();
		if (filename != null) {
			out.write(FILE_NAME_BYTES);
			out.write(QUOTE_BYTES);
			out.write(EncodingUtil.getBytes(filename, getCharSet()));
			out.write(QUOTE_BYTES);
		}
	}

	int done = 0;

	public int getDone() {
		return done;
	}

	/**
	 * @return the fileLen
	 */
	public int getFileLen() {
		return fileLen;
	}

	/**
	 * @param fileLen
	 *            the fileLen to set
	 */
	public void setFileLen(int fileLen) {
		this.fileLen = fileLen;
	}

	protected void sendData(OutputStream out) {
		InputStream instream = null;
		try {
			fileLen = (int) lengthOfData();
			// LOG.trace("enter sendData(OutputStream out)");
			if (lengthOfData() == 0L) {
				// LOG.debug("No data to send.");
				return;
			}
			byte[] tmp = new byte[1024];

			instream = this.source.createInputStream();
			int len;
			long timeSend = System.currentTimeMillis();
			while ((len = instream.read(tmp)) >= 0) {

				// >>>>>>>如果取消上传
				if (ifCancel) {
					listener.onTransfState(UploadJob.UPLOAD_CANCEL);
					return;
				}
				out.write(tmp, 0, len);
				done += len;
				if (done == fileLen || System.currentTimeMillis() - timeSend >= noticeSend) {
					timeSend = System.currentTimeMillis();
					//android.util.Log.i("writefilelen", String.valueOf(done));
					if (listener != null) {
						listener.transferred(fileLen, done);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			android.util.Log.i("writefilelen", "Exception" + e.getMessage());

			if (listener != null) {
				listener.onTransfState(UploadJob.UPLOAD_FAILED);
			}
		} finally {
			try {
				instream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			android.util.Log.i("writefilelen", "close");
		}
	}

	protected PartSource getSource() {
		// LOG.trace("enter getSource()");
		return this.source;
	}

	protected long lengthOfData() throws IOException {
		// LOG.trace("enter lengthOfData()");
		return this.source.getLength();
	}

	// >>>>>>>取消上传任务
	public void cancel() {
		// TODO Auto-generated method stub
		ifCancel = true;
	}

}
