package com.wachoo.pangker.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mobile.encrypt.Des3;
import mobile.json.JSONUtil;

import org.apache.http.util.EncodingUtils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.entity.LocalAppInfo;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.response.VersionResult;

/**
 * 
 * @author wangxin
 * 
 */
public class Util {

	private static String TAG = com.wachoo.pangker.util.Util.getClassName();// log
	// tag
	public static final int IO_BUFFER_SIZE = 8 * 1024;
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private static final Uri PREFERRED_APN_URI = Uri.parse("content://telephony/carriers/preferapn");

	public static String[] mSmileyTexts;
	public static HashMap<String, Integer> smileyToRes;
	public static Pattern pattern;

	public static final String TAB_ALL = "全部";
	public static final String TAB_TOURIST = "游客";
	public static final String TAB_PODCAST = "播客";
	public static final String TAB_fanyou = "饭友";
	public static final String TAB_LVYOU = "驴友";
	public static final String TAB_QIUYOU = "球友";
	public static final String TAB_QIYOU = "棋友";
	public static final String TAB_NETFRIEND = "网友";
	public static final String TAB_OTHER = "其他";

	static {
		mSmileyTexts = new String[] { "[超汗]", "[超囧]", "[大可]", "[大哭]", "[大笑]", "[大眼]", "[得意]", "[恶魔]", "[飞吻]", "[愤怒]",
				"[鬼脸]", "[汗死]", "[憨笑]", "[极寒]", "[吓到]", "[坏笑]", "[黄心]", "[开心]", "[可爱]", "[恐惧]", "[口罩]", "[轱辘]", "[困了]",
				"[蓝心]", "[流汗]", "[媚眼]", "[眯眼]", "[亲亲]", "[色色]", "[生气]", "[叹气]", "[吐舌]", "[小囧]", "[小哭]", "[斜视]", "[郁闷]"

		};
	}

	/**
	 * Tab的搜索的关键字
	 */
	public static final String[] GALLERY_KEY = { TAB_ALL, TAB_TOURIST, TAB_PODCAST, TAB_fanyou, TAB_LVYOU, TAB_QIUYOU,
			TAB_QIYOU, TAB_NETFRIEND, TAB_OTHER };

	/**
	 * 查询当前手机的APN设置。
	 * 
	 * @param context
	 * @return
	 */
	public static int getApnDefault(Context context) {
		Cursor cursor = context.getContentResolver().query(PREFERRED_APN_URI, null, null, null, null);
		if (cursor == null) {
			return 0;
		}
		if (cursor.moveToNext()) {
			String proxy = cursor.getString(cursor.getColumnIndex("proxy"));
			String port = cursor.getString(cursor.getColumnIndex("port"));
			// System.out.println("proxy--"+proxy+"+port+"+port);
			try {
				if (proxy.trim().equals("") || port.trim().equals("") || port == null || proxy == null) {
					return 1; // cnwap
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error(TAG, e);
				return 1;
			}
		}
		return 0;
	}

	public static String getPhoneImeiNum(Context context) {
		TelephonyManager phoneMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String imei = phoneMgr.getDeviceId();// 设备
		return imei;
	}

	/**
	 * 获取手机的IMSI码
	 * 
	 * @param context
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:22:27
	 */
	public static String getImsiNum(Context context) {
		TelephonyManager phoneMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String imsi = phoneMgr.getSubscriberId();
		return imsi;
	}

	/**
	 * 自动获取手机号码
	 * 
	 * @param context
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:22:18
	 */
	public static String getPhoneNum(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String temp = tm.getLine1Number();
		if (temp != null && !"".equals(temp)) {
			String phoneNum = temp;
			return phoneNum;
		}
		return null;
	}

	/**
	 * by heyao 判断程序是否运行在模拟器中
	 * 
	 * @param context
	 * @return true表示在模拟器中，false表示在真手机中
	 */
	public static boolean isInEmulator(Context context) {
		TelephonyManager phoneMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String imsi = phoneMgr.getSubscriberId();
		if (imsi != null && imsi.equalsIgnoreCase("310260000000000"))
			return true;
		else
			return false;
	}

	/**
	 * webview 图片显示
	 * 
	 * @author zsy
	 */
	public static String getHtml(String imgurl) {
		return new String(
				"<html><body style=\"margin: 0px;\"><div style=\"text-align:center;height=100%\"><img align=\"left\" src=\""
						+ imgurl
						+ "\" onerror=\"javascript:this.src='file:///android_asset/img/img83_no.jpg'\"   height=\"100%\"></div></body></html>");
	}

	public static String getHtmlWithA(String imgurl, String ImgThumbUrl) {
		return new String(
				"<html><body style=\"margin: 0px;\"><div style=\"text-align:center;height=100%\"><a href='#' onclick=\"window.miblogscript.scriptGoBigImg('"
						+ imgurl
						+ "')\" ><img align=\"left\" src=\""
						+ ImgThumbUrl
						+ "\" height=\"100%\"></div></body></html>");
	}

	// 提示信息
	public static void showMsg(String msg, Context context) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 判断是否是手机号码
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public static boolean isMobile(String phoneNumber) {
		return phoneNumber != null && phoneNumber.matches("1[0-9]{10}");
	}

	/**
	 * 
	 * 判断是否是电信手机号码
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public static boolean isCtMobile(String phoneNumber) {
		return phoneNumber != null
				&& (phoneNumber.startsWith("133") || phoneNumber.startsWith("189") || phoneNumber.startsWith("180")
						|| phoneNumber.startsWith("153") || phoneNumber.startsWith("1349"));
	}

	/**
	 * 获取指定字符串前面的字符串
	 * 
	 * @param s
	 * @param separator
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:23:19
	 */
	public static String getLeftString(String s, String separator) {
		try {

			int index = s.indexOf(separator);

			if (index > -1)
				return s.substring(0, index);
			else
				return s;
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}
	}

	/**
	 * 获取指定字符串后面的字符串
	 * 
	 * @param s
	 * @param separator
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:23:43
	 */
	public static String getRightString(String s, String separator) {
		int index = s.indexOf(separator);
		if (index > -1)
			return s.substring(index + 1, s.length());
		else
			return "";
	}

	/**
	 * 获取指定字符串后面的字符串
	 * 
	 * @param s
	 * @param separator
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:23:43
	 */
	public static String getLastRightString(String s, String separator) {
		int index = s.indexOf(separator);
		if (index > -1)
			return s.substring(index + 1, s.length());
		else
			return "";
	}

	public static String getVersionContent(String content) {
		String[] str = content.split(";");
		String contentString = "";
		for (int i = 0; i < str.length; i++) {
			contentString = contentString + str[i] + "\n";
		}
		return contentString;
	}

	/**
	 * 判断字符串是否是数字
	 * 
	 * @author zsy
	 * @return boolean 返回 true是数字
	 */
	public static boolean isNumeric(String str) {
		if (str == null || str.length() == 0) {
			return false;
		}
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}

	/**
	 * 时间转换器
	 * 
	 * @author zsy
	 */

	public static Date strToDate(String str) {
		if (str == null || str.length() == 0) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date date = df.parse(str);
			return date;
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * 时间转换器
	 * 
	 * @author zsy
	 */

	public static Date strToDate2(String str) {
		if (str == null || str.length() == 0) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
		try {
			Date date = df.parse(str);
			return date;
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * 产生随机id
	 * 
	 * @author zsy
	 */

	public static long newSynId() {
		long time = System.currentTimeMillis();
		int ran = (int) (Math.random() * 9000 + 1000);
		return time * 10000 + ran;// Integer.parseInt(tempid);
	}

	public static String getTimeBy(long timestamp) {
		try {
			String time;
			long newTime = System.currentTimeMillis();
			long differenceTime = newTime - (timestamp);
			int hour = (int) (differenceTime / 1000 / 60 / 60);
			if (hour >= 24) {
				SimpleDateFormat sdf = new SimpleDateFormat(PangkerConstant.DATE_FORMAT);
				time = sdf.format(new Date(timestamp));
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");
				time = sdf.format(new Date(timestamp));
			}
			return time;
		} catch (Exception e) {
			logger.error(TAG, e);
			return "";
		}
	}

	/**
	 * 获取特定格式的时间串
	 * 
	 * @param timestamp
	 *            格式如System.currentTimeMillis();
	 * @param format
	 *            格式如 yyyy-MM-dd hh:mm:ss
	 * @return 返回 yyyy-MM-dd hh:mm:ss格式的字串
	 */
	public static String getTimeOfFormat(long timestamp, String format) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(new Date(timestamp));
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return null;
	}

	// 个数限制
	public static String stringTobur(String strValue) {
		if (strValue.length() > 14) {
			return strValue.substring(0, 11) + "...";
		}
		return strValue;
	}

	// 根据文本替换成图片
	public static CharSequence replace(CharSequence text, Context context) {
		SpannableStringBuilder builder = new SpannableStringBuilder(text);
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			int resId = smileyToRes.get(matcher.group());
			Drawable drawable = context.getResources().getDrawable(resId);
			drawable.setBounds(0, 10, 30, 40);
			ImageSpan span = new ImageSpan(drawable, ImageSpan.ALIGN_BASELINE);
			builder.setSpan(span, matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		return builder;
	}

	/**
	 * 格式化时间 yyyy-MM-dd HH:mm:ss
	 * 
	 * @param time
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:25:24
	 */
	public static String FormatTime(long time) {
		SimpleDateFormat sfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattime = "";
		formattime = sfd.format(time);
		return formattime;
	}

	/**
	 * 将String时间类型转换为Long时间类型
	 * 
	 * @param sourceTime
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:25:45
	 */
	public static long string2long(String sourceTime) {
		long longTime = 0L;
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date d = null;
		try {
			d = f.parse(sourceTime);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		longTime = d.getTime();
		return longTime;
	}

	/**
	 * json string to class (object)
	 * 
	 * @param jsonString
	 * @param clazz
	 * @return
	 */
	public static Object JSONStringToO(String jsonString, Class<Object> clazz) {
		// TODO Auto-generated method stub
		return JSONUtil.fromJson(jsonString, clazz);
	}

	/**
	 * 根据distance排序UserItem
	 */
	public final static Comparator<UserItem> distance_sort = new Comparator<UserItem>() {
		@Override
		public int compare(UserItem item1, UserItem item2) {
			if (item1.getDistance() != null && item2.getDistance() != null) {
				return item1.getDistance().toLowerCase().compareTo(item2.getDistance().toLowerCase());
			} else {
				return 0;
			}
		}

	};

	public static char[] md5Char = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' }; // 用于文件MD5校验码生成
	public static final String NAME = "name", NUMBER = "number", SORT_KEY = "sort_key";

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public static void launch(Context From, Class<Activity> To, int StartFlag) {
		Intent intent = new Intent(From, To);
		intent.setFlags(StartFlag);
		From.startActivity(intent);
	}

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public static void launch(Intent intent, Context From) {
		From.startActivity(intent);
	}

	/**
	 * 直接启动activity
	 * 
	 * @param From
	 * @param To
	 */
	public static void launch(Context From, Class<Activity> To) {
		Intent intent = new Intent(From, To);
		From.startActivity(intent);
	}

	/**
	 * 根据时间取得时间点
	 * 
	 * @param date
	 *            格式如：1338427757079
	 * @return
	 * 
	 *         Add by zhengjy
	 */
	public static String getDateDiff(Date date) {
		Calendar today = Calendar.getInstance();
		int nowMonth = today.get(Calendar.MONTH);
		int nowDay = today.get(Calendar.DAY_OF_MONTH);
		int oldDay = date.getDate();
		String result = "";
		try {
			if (nowMonth == date.getMonth()) {
				if (nowDay == oldDay) {
					result = "今天  " + getDateByFormat(date, "HH:mm");
				} else if (nowDay - oldDay == 1) {
					result = "昨天  " + getDateByFormat(date, "HH:mm");
				} else if (nowDay - oldDay == 2) {
					result = "前天  " + getDateByFormat(date, "HH:mm");
				} else
					result = getDateByFormat(date, "MM月dd日  HH:mm");
			} else
				result = getDateByFormat(date, "MM月dd日  HH:mm");

		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return result;
	}

	public static String getDateByFormat(Date date, String format) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(date);

		} catch (Exception e) {
			logger.error(TAG, e);
			return "";
		}
	}

	/**
	 * check String is null or “”
	 * 
	 * @param x
	 * @return
	 */
	public static boolean isEmpty(String x) {
		if (x == null) {
			return true;
		} else if (x.trim().equals("")) {
			return true;
		}
		return false;
	}

	/**
	 * 取得当前时间 格式yyyy-MM-dd HH:MM:ss
	 * 
	 * @return
	 */
	public static String getSysNowTime() {
		Date now = new Date();
		DateFormat format = new SimpleDateFormat(PangkerConstant.DATE_FORMAT);
		String formatTime = format.format(now);
		return formatTime;
	}

	public static String getNowPullTime() {
		Date now = new Date();
		DateFormat format = new SimpleDateFormat(PangkerConstant.DATE_PULL_FORMAT);
		String formatTime = format.format(now);
		return formatTime;
	}

	public static long timeInterval(String startTime, String endTime) {
		DateFormat f = new SimpleDateFormat(PangkerConstant.DATE_FORMAT);
		long start = 0;
		long end = 0;
		try {
			start = f.parse(startTime).getTime();

			end = f.parse(endTime).getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);

		}
		return end - start;
	}

	/**
	 * 取得当前时间 格式yyyyMMddHHMMss
	 * 
	 * @return
	 */
	public static String getNowTime() {
		DateFormat format = new SimpleDateFormat("yyyyMMddHHMMss");
		return format.format(new Date());
	}

	/**
	 * 取得文本值
	 * 
	 * @param context
	 * @param id
	 * @return
	 */
	public static String getRawText(Context context, int id) {
		InputStream is = context.getResources().openRawResource(id);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String result = BufferedReaderToS(br);
			is.close();
			br.close();
			return result;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * BufferedReader to string
	 * 
	 * @param br
	 * @return
	 * @throws IOException
	 */
	public static String BufferedReaderToS(BufferedReader br) throws IOException {
		String str = br.readLine();
		StringBuilder result = new StringBuilder();
		while (str != null && str.length() > 0) {
			result.append(str);
			result.append("\n");
			str = br.readLine();
		}
		return result.toString();
	}

	/**
	 * 判断SD卡是否存在
	 * 
	 * @return
	 */
	public static boolean isExistSdkard() {
		if (Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			return true;
		}
		return false;
	}

	/**
	 * @param x
	 * @param length
	 * @return
	 */
	public static String getStringByLength(String x, int length) {
		if (isEmpty(x)) {
			return null;
		}
		for (int i = 0; i <= length && i < x.length(); i++) {

			try {
				if (getHalfStringLength(String.valueOf(x.charAt(i))) >= 2) {
					length--;
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				return "";
			}
		}
		if (x.length() > length) {
			return x.substring(0, length) + "...";
		}
		return x;
	}

	/***
	 * 
	 * @param o
	 * @return
	 */
	public static String objectToString(Object o) {
		String value = "";
		try {
			value = o.toString();
		} catch (Exception e) {

		}
		return value;
	}

	/**
	 * 从SharedPreferences获取用户在登录页保存的信息
	 * 
	 * @param tag
	 * @param context
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List LoadLoginData(Context context) {
		SharedPreferences sp = context.getSharedPreferences("LoginActivity", Context.MODE_PRIVATE);
		List list = new ArrayList();
		list.add(sp.getBoolean("autoLogin", false));
		list.add(sp.getString("userName", ""));
		list.add(sp.getString("userPassword", ""));
		list.add(sp.getBoolean("savePass", false));
		return list;
	}

	/**
	 * 设置用户登录信息到SharedPreferences中
	 * 
	 * @param map
	 * @param tag
	 * @param context
	 */
	@SuppressWarnings("unchecked")
	public static void setUserData(List list, Context context) {
		Editor editor = context.getSharedPreferences("LoginActivity", Context.MODE_PRIVATE).edit();
		editor.putBoolean("autoLogin", ((Boolean) list.get(0)));
		editor.putString("userName", list.get(1).toString());
		editor.putString("userPassword", list.get(2).toString());
		editor.putBoolean("savePass", ((Boolean) list.get(3)));
		editor.commit();
	}

	/**
	 * 将数据流变成byte[]
	 * 
	 * @param inStream
	 * @return
	 * @throws Exception
	 */
	public static byte[] readStream(InputStream inStream) throws Exception {
		byte[] buffer = new byte[1024];
		int len = -1;
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		while ((len = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		byte[] data = outStream.toByteArray();
		outStream.close();
		inStream.close();
		return data;

	}

	/**
	 * 通讯录
	 * 
	 * @param str
	 * @return String
	 */
	public static String getAlpha(String str) {
		if (str == null || str.trim().length() == 0) {
			return "#";
		}
		char c = str.trim().substring(0, 1).charAt(0);
		// 正则表达式，判断首字母是否是英文字母
		Pattern pattern = Pattern.compile("^[A-Za-z]+$");
		if (pattern.matcher(c + "").matches()) {
			return (c + "").toUpperCase();
		} else {
			return "#";
		}
	}

	/**
	 * 通讯录
	 * 
	 * @param cursor
	 * @return List<ContentValues>
	 */
	public static List<ContentValues> getList(Cursor cursor) {
		List<ContentValues> list = new ArrayList<ContentValues>();
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				ContentValues cv = new ContentValues();
				cursor.moveToPosition(i);
				String name = cursor.getString(1);
				String number = cursor.getString(2);
				String sortKey = cursor.getString(3);
				if (number.startsWith("+86")) {
					cv.put(NAME, name);
					cv.put(NUMBER, number.substring(3)); // 去掉+86
					cv.put(SORT_KEY, sortKey);
				} else {
					cv.put(NAME, name);
					cv.put(NUMBER, number);
					cv.put(SORT_KEY, sortKey);
				}
				list.add(cv);
			}
		}
		return list;
	}

	/**
	 * userinfo ---> userItem
	 * 
	 * @param userinfo
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:27:05
	 */
	public static UserItem toUserItem(UserInfo userinfo) {
		UserItem item = new UserItem();
		item.setPangker(true);
		item.setAvailable(true);
		item.setUserId(userinfo.getUserId() != null ? userinfo.getUserId() : "");
		item.setNickName(userinfo.getNickName() != null ? userinfo.getNickName() : "");
		item.setUserName(userinfo.getUserName() != null ? userinfo.getUserName() : "");

		item.setSign(userinfo.getSign() != null ? userinfo.getSign() : "");
		return item;
	}

	public static List<UserItem> toUserItem(List<UserInfo> userList) {
		List<UserItem> contactList = new ArrayList<UserItem>();
		if (userList != null && userList.size() > 0) {
			for (int i = 0; i < userList.size(); i++) {
				UserInfo user = userList.get(i);
				if (user != null) {
					UserItem item = toUserItem(user);
					contactList.add(item);
				}
			}
		}
		return contactList;
	}

	public static String getMd5(String fileName) throws Exception {
		File file = new File(fileName);
		if (!file.isFile()) {
			return null;
		}
		MessageDigest digest = null;
		FileInputStream in = null;
		byte buffer[] = new byte[2024];
		int len;
		try {
			digest = MessageDigest.getInstance("MD5");
			in = new FileInputStream(file);
			while ((len = in.read(buffer, 0, 1024)) != -1) {
				digest.update(buffer, 0, len);
			}
			in.close();

		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
			return null;
		}
		BigInteger bigInt = new BigInteger(1, digest.digest());
		return bigInt.toString(16);

	}

	public static String toMd5String(byte[] b) {
		StringBuilder sb = new StringBuilder(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			sb.append(md5Char[(b[i] & 0xf0) >>> 4]);
			sb.append(md5Char[b[i] & 0x0f]);
		}
		return sb.toString();
	}

	/**
	 * 
	 * @param distance
	 * @return
	 * 
	 * @author wangxin 2012-2-2 上午10:38:28
	 */
	public static String getRang(String distance) {

		int dis = Integer.parseInt(distance);
		String returnDis = "";
		if (dis <= 100)
			returnDis = "100米之内";
		else if (dis <= 300)
			returnDis = "300米之内";
		else if (dis <= 500)
			returnDis = "500米之内";
		else {
			returnDis = "500米之外";
		}
		return returnDis;
	}

	private static String getStrByMinit(long minDiff) {
		if (minDiff <= 0) {
			return "刚刚";
		}
		if (minDiff < 60 && minDiff > 0) {
			return minDiff + "分钟之内";
		}
		long hour = minDiff / 60;
		if (hour < 24 && hour > 0) {
			return hour + "小时之内";
		}
		long day = hour / 24;
		if (day < 30 && day > 0) {
			return day + "天前";
		}
		long month = day / 30;
		if (month < 12 && month > 0) {
			return month + "月前";
		}
		long year = month / 12;
		return year + "年前";
	}

	/**
	 * 距离当前的时间间隔
	 * 
	 * @param time
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:27:34
	 */
	public static String TimeIntervalBtwNow(String time) {
		if (isEmpty(time)) {
			return "未知";
		}
		long minDiff = dateDiff(time, getSysNowTime(), PangkerConstant.DATE_FORMAT);
		return getStrByMinit(minDiff);
	}

	/**
	 * 根据时间取得时间点
	 * 
	 * @param date
	 * @return
	 */
	public static String TimeIntervalBtwNow(long lDate) {
		Date date = new Date(lDate);
		Calendar cal = Calendar.getInstance();
		long diff = 0;
		Date dnow = cal.getTime();
		diff = dnow.getTime() - date.getTime();

		return getStrByMinit(diff / 1000 / 60);
	}

	/**
	 * 根据时间取得时间点
	 * 
	 * @param date
	 * @return
	 */
	public static String TimeIntervalBtwNow(Date date) {
		Calendar cal = Calendar.getInstance();
		long diff = 0;
		Date dnow = cal.getTime();
		diff = dnow.getTime() - date.getTime();

		return getStrByMinit(diff / 1000 / 60);
	}

	/**
	 * 
	 * @param startTime
	 * @param endTime
	 * @param format
	 * @param str
	 * @return md by lb 返回结果同一成分钟
	 * @author wangxin 2012-2-10 下午04:31:59
	 */
	public static Long dateDiff(String startTime, String endTime, String format, String str) {
		// 按照传入的格式生成一个simpledateformate对象
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long nh = 1000 * 60 * 60;// 一小时的毫秒数
		long nm = 1000 * 60;// 一分钟的毫秒数
		long diff;
		long day = 0;
		long hour = 0;
		long min = 0;
		// 获得两个时间的毫秒时间差异
		try {
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
			day = diff / nd;// 计算差多少天
			hour = diff / nh;// 计算差多少小时
			min = diff / nm;// 计算差多少分钟

		} catch (ParseException e) {

		}
		if (isEmpty(str)) {
			return min;
		} else if (str.equalsIgnoreCase("h")) {
			return hour;
		} else if (str.equalsIgnoreCase("d")) {
			return day;
		} else {
			return min;
		}
	}

	/**
	 * * md by lb 返回结果同一成分钟
	 * 
	 * @author lb 2012年5月30日11:26:38
	 * @param startTime
	 * @param endTime
	 * @param format
	 * @param str
	 * @return
	 */
	public static Long dateDiff(String startTime, String endTime, String format) {
		// 按照传入的格式生成一个simpledateformate对象
		return dateDiff(startTime, endTime, format, null);
	}

	public static long dateDiffNow(String time, String format) throws ParseException {
		DateFormat df = new SimpleDateFormat(format);
		Date timebef = df.parse(time);
		return System.currentTimeMillis() - timebef.getTime();
	}

	public static long dateDiffNow(String time) throws ParseException {
		return dateDiffNow(time, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 判断context是否运行
	 * 
	 * @param context
	 * @return
	 * 
	 * @author wangxin 2012-2-21 上午09:28:09
	 */
	public static boolean isRunning(Context context) {
		ActivityManager activityManager = (ActivityManager) context.getApplicationContext().getSystemService(
				Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> list = activityManager.getRunningAppProcesses();
		for (RunningAppProcessInfo run : list) {
			System.out.println(run.processName);
		}
		return false;
	}

	/**
	 * 
	 * 判断旁客程序是否在前台运行（激活状态）
	 * 
	 * @param context
	 * @return
	 * 
	 * @author wangxin 2012-2-8 上午11:32:38
	 */
	public static boolean isActivated(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName task_info = manager.getRunningTasks(2).get(0).topActivity;
		System.out.println(task_info.getClassName());
		if (task_info.getClassName().equals(context.getClass().getName())) {
			return false;
		} else if (task_info.getPackageName().equals(PangkerConstant.PANGKER_PACKAGE_NAME)) {
			return true;
		}
		return false;
	}

	/**
	 * addUserIDDomain
	 * 
	 * @param userid
	 * @return
	 * 
	 * @author wangxin 2012-2-17 下午05:49:35
	 */
	public static String addUserIDDomain(String userid) {
		userid = getLeftString(userid, "@") + PangkerConstant.PANGKER_DOMAIN;
		return userid;
	}

	/**
	 * addUserIDDomain
	 * 
	 * @param userid
	 * @return
	 * 
	 * @author wangxin 2012-2-17 下午05:49:35
	 */
	public static String addCompleteUserID(String userid) {
		userid = getLeftString(userid, "@") + PangkerConstant.PANGKER_DOMAIN + "/" + PangkerConstant.PANGKER_RESOURCE;
		return userid;
	}

	/**
	 * 
	 * trimUserIDDomain
	 * 
	 * @param userid
	 * @return
	 * 
	 * @author wangxin 2012-2-20 下午08:04:38
	 */
	public static String trimUserIDDomain(String userid) {
		userid = getLeftString(userid, "@");
		return userid;
	}

	/**
	 * 将日期格式化为指定格式的字符串
	 * 
	 * @return
	 */
	public static String FormatDate(Date dt, String format) {

		SimpleDateFormat df = new SimpleDateFormat(format);
		df.setTimeZone(TimeZone.getDefault());
		return df.format(dt);
	}

	public static boolean checkApkExist(Context context, String packageName) {
		if (packageName == null || "".equals(packageName))
			return false;
		try {
			context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
			return true;
		} catch (NameNotFoundException e) {
			return false;
		}
	}

	public static String getApkFileInfo(String apkPath) {
		File apkFile = new File(apkPath);
		if (!apkFile.exists() || !apkPath.toLowerCase().endsWith(".apk")) {
			return null;
		}
		String PATH_PackageParser = "android.content.pm.PackageParser";
		Class<?> pkgParserCls;
		try {
			pkgParserCls = Class.forName(PATH_PackageParser);
			Class<?>[] typeArgs = { String.class };
			Constructor<?> pkgParserCt = pkgParserCls.getConstructor(typeArgs);
			Object[] valueArgs = { apkPath };
			Object pkgParser = pkgParserCt.newInstance(valueArgs);

			DisplayMetrics metrics = new DisplayMetrics();
			metrics.setToDefaults();// 这个是与显示有关的, 这边使用默认
			typeArgs = new Class<?>[] { File.class, String.class, DisplayMetrics.class, int.class };
			Method pkgParser_parsePackageMtd = pkgParserCls.getDeclaredMethod("parsePackage", typeArgs);
			valueArgs = new Object[] { new File(apkPath), apkPath, metrics, 0 };
			Object pkgParserPkg = pkgParser_parsePackageMtd.invoke(pkgParser, valueArgs);

			// 从返回的对象得到名为"applicationInfo"的字段对象
			if (pkgParserPkg == null) {
				return null;
			}

			Field appInfoFld = pkgParserPkg.getClass().getDeclaredField("applicationInfo");

			// 从对象"pkgParserPkg"得到字段"appInfoFld"的值
			if (appInfoFld.get(pkgParserPkg) == null) {
				return null;
			}
			ApplicationInfo info = (ApplicationInfo) appInfoFld.get(pkgParserPkg);
			return info.packageName;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
		return null;
	}

	/**
	 * 获取未安装的apk信息
	 * 
	 * @param ctx
	 * @param apkPath
	 * @return
	 */
	public static LocalAppInfo getApkFileInfo(Context ctx, String apkPath) {
		if (apkPath == null) {
			return null;
		}
		File apkFile = new File(apkPath);
		if (!apkFile.exists() || !apkPath.toLowerCase().endsWith(".apk")) {
			return null;
		}
		LocalAppInfo appInfoData;
		String PATH_PackageParser = "android.content.pm.PackageParser";
		String PATH_AssetManager = "android.content.res.AssetManager";
		try {
			// 反射得到pkgParserCls对象并实例化,有参数
			Class<?> pkgParserCls = Class.forName(PATH_PackageParser);
			Class<?>[] typeArgs = { String.class };
			Constructor<?> pkgParserCt = pkgParserCls.getConstructor(typeArgs);
			Object[] valueArgs = { apkPath };
			Object pkgParser = pkgParserCt.newInstance(valueArgs);

			// 从pkgParserCls类得到parsePackage方法
			DisplayMetrics metrics = new DisplayMetrics();
			metrics.setToDefaults();// 这个是与显示有关的, 这边使用默认
			typeArgs = new Class<?>[] { File.class, String.class, DisplayMetrics.class, int.class };
			Method pkgParser_parsePackageMtd = pkgParserCls.getDeclaredMethod("parsePackage", typeArgs);

			valueArgs = new Object[] { new File(apkPath), apkPath, metrics, 0 };

			// 执行pkgParser_parsePackageMtd方法并返回
			Object pkgParserPkg = pkgParser_parsePackageMtd.invoke(pkgParser, valueArgs);

			// 从返回的对象得到名为"applicationInfo"的字段对象
			if (pkgParserPkg == null) {
				return null;
			}
			Field appInfoFld = pkgParserPkg.getClass().getDeclaredField("applicationInfo");

			// 从对象"pkgParserPkg"得到字段"appInfoFld"的值
			if (appInfoFld.get(pkgParserPkg) == null) {
				return null;
			}
			ApplicationInfo info = (ApplicationInfo) appInfoFld.get(pkgParserPkg);

			// 反射得到assetMagCls对象并实例化,无参
			Class<?> assetMagCls = Class.forName(PATH_AssetManager);
			Object assetMag = assetMagCls.newInstance();
			// 从assetMagCls类得到addAssetPath方法
			typeArgs = new Class[1];
			typeArgs[0] = String.class;
			Method assetMag_addAssetPathMtd = assetMagCls.getDeclaredMethod("addAssetPath", typeArgs);
			valueArgs = new Object[1];
			valueArgs[0] = apkPath;
			// 执行assetMag_addAssetPathMtd方法
			assetMag_addAssetPathMtd.invoke(assetMag, valueArgs);

			// 得到Resources对象并实例化,有参数
			Resources res = ctx.getResources();
			typeArgs = new Class[3];
			typeArgs[0] = assetMag.getClass();
			typeArgs[1] = res.getDisplayMetrics().getClass();
			typeArgs[2] = res.getConfiguration().getClass();
			Constructor<Resources> resCt = Resources.class.getConstructor(typeArgs);
			valueArgs = new Object[3];
			valueArgs[0] = assetMag;
			valueArgs[1] = res.getDisplayMetrics();
			valueArgs[2] = res.getConfiguration();
			res = (Resources) resCt.newInstance(valueArgs);

			// 读取apk文件的信息
			appInfoData = new LocalAppInfo();
			if (info != null) {
				if (info.icon != 0) {// 图片存在，则读取相关信息
					Drawable icon = res.getDrawable(info.icon);// 图标
					appInfoData.setAppIcon(icon);
					appInfoData.setResourceId(info.uid);
				}
				if (info.labelRes != 0) {
					String name = (String) res.getText(info.labelRes);// 名字
					appInfoData.setAppName(name);
				} else {
					String apkName = apkFile.getName();
					appInfoData.setAppName(apkName.substring(0, apkName.lastIndexOf(".")));
				}
				String pkgName = info.packageName;// 包名
				appInfoData.setAppPackage(pkgName);
			} else {
				return null;
			}
			PackageManager pm = ctx.getPackageManager();
			PackageInfo packageInfo = pm.getPackageArchiveInfo(apkPath, PackageManager.GET_ACTIVITIES);
			if (packageInfo != null) {
				appInfoData.setAppVersion(packageInfo.versionName);// 版本号
				appInfoData.setAppVersioncode(packageInfo.versionCode + "");// 版本码
			}
			appInfoData.setSize(apkFile.length());
			appInfoData.setAppPath(apkPath);
			return appInfoData;
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return null;
	}

	/**
	 * 获取文件大小
	 * 
	 * @param f
	 * @return
	 * 
	 * @author wangxin
	 * @throws IOException
	 * @date 2012-3-5 下午05:54:16
	 */
	public static int getFileSize(File f) throws IOException {
		int s = 0;
		if (f.isFile()) {
			FileInputStream stream = new FileInputStream(f);
			s = stream.available();
		}
		return s;
	}

	public static String getConstellation(Integer key) {
		try {
			if (key == null)
				return "";
			return PangkerConstant.CONSTELLATIONS[key];
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}

	}

	public static String getZodiac(Integer key) {
		try {
			if (key == null)
				return "";
			return PangkerConstant.ZODIACS[key];
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}

	}

	public static String readFileData(Context context, String fileName) {
		String res = "";
		try {
			FileInputStream fin = context.openFileInput(fileName);
			int length = fin.available();
			byte[] buffer = new byte[length];
			fin.read(buffer);
			res = EncodingUtils.getString(buffer, "UTF-8");
			fin.close();
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return res;
	}

	public static void delFileData(Context context, String fileName) {
		try {
			context.deleteFile(fileName);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(TAG, e);
		}
	}

	public static void writeFileData(Context context, String fileName, String writestring) {
		try {
			FileOutputStream fout = context.openFileOutput(fileName, Context.MODE_PRIVATE);
			byte[] bytes = writestring.getBytes();
			fout.write(bytes);
			fout.close();
		} catch (Exception e) {
			logger.error(TAG, e);
		}
	}

	// >>>wangxin 删除当前用户信息
	public static void delHomeUser(Context context) {
		delFileData(context, PangkerConstant.CERTIFICATE_FILE_NAME);
	}

	// >>>wangxin 更新当前用户信息
	public static void updateHomeUser(Context context, UserInfo info) {
		delFileData(context, PangkerConstant.CERTIFICATE_FILE_NAME);
		writeHomeUser(context, info);
	}

	public static UserInfo readHomeUser(Context context) {
		UserInfo info = null;
		try {
			String encryptJson = readFileData(context, PangkerConstant.CERTIFICATE_FILE_NAME);
			Log.i("PangkerApplication", encryptJson);
			String json = Des3.Decrypt(encryptJson, Configuration.getEncryptKey(), Configuration.getDefaultIV());
			info = JSONUtil.fromJson(json, UserInfo.class);
		} catch (Exception e) {
			// TODO: handle exception
			info = null;
		}
		return info;
	}

	public static boolean writeHomeUser(Context context, UserInfo info) {
		try {
			String json = JSONUtil.toJson(info, false);
			String encryptJson = Des3.Encrypt(json, Configuration.getEncryptKey(), Configuration.getDefaultIV());

			writeFileData(context, PangkerConstant.CERTIFICATE_FILE_NAME, encryptJson);

		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

		return true;
	}

	// 国标码和区位码转换常量

	static final int GB_SP_DIFF = 160;

	// 存放国标一级汉字不同读音的起始区位码

	static final int[] secPosValueList = {

	1601, 1637, 1833, 2078, 2274, 2302, 2433, 2594, 2787,

	3106, 3212, 3472, 3635, 3722, 3730, 3858, 4027, 4086,

	4390, 4558, 4684, 4925, 5249, 5600 };

	// 存放国标一级汉字不同读音的起始区位码对应读音

	static final char[] firstLetter = {

	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j',

	'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',

	't', 'w', 'x', 'y', 'z' };

	// 获取一个字符串的拼音码

	public static String getFirstLetter(String oriStr) {
		String str = oriStr.toLowerCase();
		StringBuffer buffer = new StringBuffer();
		char ch;
		char[] temp;

		for (int i = 0; i < str.length(); i++) { // 依次处理str中每个字符
			ch = str.charAt(i);
			temp = new char[] { ch };
			byte[] uniCode = new String(temp).getBytes();
			if (uniCode[0] < 128 && uniCode[0] > 0) { // 非汉字
				buffer.append(temp);
			} else {
				buffer.append(convert(uniCode));
			}
		}
		return buffer.toString();
	}

	/**
	 * 获取一个汉字的拼音首字母。 GB码两个字节分别减去160，转换成10进制码组合就可以得到区位码
	 * 例如汉字“你”的GB码是0xC4/0xE3，分别减去0xA0（160）就是0x24/0x43
	 * 0x24转成10进制就是36，0x43是67，那么它的区位码就是3667，在对照表中读音为‘n’
	 */

	public static char convert(byte[] bytes) {

		char result = '-';
		int secPosValue = 0;
		int i;
		for (i = 0; i < bytes.length; i++) {
			bytes[i] -= GB_SP_DIFF;
		}

		secPosValue = bytes[0] * 100 + bytes[1];
		for (i = 0; i < 23; i++) {
			if (secPosValue >= secPosValueList[i] && secPosValue < secPosValueList[i + 1]) {
				result = firstLetter[i];
				break;
			}
		}
		return result;
	}

	public static String getFileformat(String filename) {
		String fileformat = filename.substring(filename.lastIndexOf(".") + 1);
		return fileformat;
	}

	public static boolean isExitFileSD(String path) {
		if (isEmpty(path)) {
			return false;
		}
		File file = new File(path);
		return file.exists();
	}

	public static boolean deleteFileSD(String path) {
		File file = new File(path);
		if (file.exists()) {
			return file.delete();
		}
		return false;
	}

	// sdcard是否可读写
	public static boolean IsCanUseSdCard() {
		try {
			return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return false;
	}

	/**
	 * 播放器进度条时间处理方法
	 * 
	 * @param time
	 * @return
	 */
	public static String toTime(int time) {

		time /= 1000;
		int minute = time / 60;
		int second = time % 60;
		minute %= 60;
		return String.format("%02d:%02d", minute, second);
	}

	/**
	 * 歌曲专辑图片显示,如果有歌曲图片，才会返回，否则为null，要注意判断
	 * 
	 * @param trackId
	 * @return 返回类型是String 类型的图片地址，也就是uri
	 */
	public static String getAlbumArt(Context context, int trackId) {// trackId是音乐的id
		String mUriTrack = "content://media/external/audio/media/#";
		String[] projection = new String[] { "album_id" };
		String selection = "_id = ?";
		String[] selectionArgs = new String[] { Integer.toString(trackId) };
		Cursor cur = context.getContentResolver().query(Uri.parse(mUriTrack), projection, selection, selectionArgs,
				null);
		int album_id = 0;
		if (cur == null) {
			return null;
		}
		if (cur.getCount() > 0 && cur.getColumnCount() > 0) {
			cur.moveToNext();
			album_id = cur.getInt(0);
		}
		cur.close();
		cur = null;

		if (album_id < 0) {
			return null;
		}
		String mUriAlbums = "content://media/external/audio/albums";
		projection = new String[] { "album_art" };
		cur = context.getContentResolver().query(Uri.parse(mUriAlbums + "/" + Integer.toString(album_id)), projection,
				null, null, null);

		String album_art = null;
		if (cur.getCount() > 0 && cur.getColumnCount() > 0) {
			cur.moveToNext();
			album_art = cur.getString(0);
		}
		cur.close();
		cur = null;

		return album_art;
	}

	/**
	 * 根据应用包名判断应用是否已在本机安装过
	 * 
	 * @param context
	 * @param packageName
	 * @return
	 * @Add by zhengjy
	 */
	public static boolean checkAppIsExist(Context context, String packageName) {
		PackageInfo packageInfo;
		try {
			packageInfo = context.getPackageManager().getPackageInfo(packageName,
					PackageManager.GET_UNINSTALLED_PACKAGES);
		} catch (NameNotFoundException e) {
			logger.error(TAG, e);
			return false;
		}
		return packageInfo != null;
	}

	public static String getShowTimes(String downtime) {
		SimpleDateFormat sdtime = new SimpleDateFormat(PangkerConstant.DATE_FORMAT);
		Date d = null;
		try {
			d = sdtime.parse(downtime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
		}
		SimpleDateFormat sd = new SimpleDateFormat("yyyy.MM.dd");
		if (d == null) {
			return "";
		}
		return sd.format(d);
	}

	// >>>> 判断sd卡是否存在
	public static boolean SDCardIsExit() {
		String status = Environment.getExternalStorageState();

		if (status.equals(Environment.MEDIA_MOUNTED)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 根据生日获取年龄
	 * 
	 * @param birthDay
	 * @return
	 * @throws Exception
	 */
	public static int getAge(Date birthDay) throws Exception {
		Calendar cal = Calendar.getInstance();
		if (cal.getTime().before(birthDay)) {
			throw new IllegalArgumentException("出生时间大于当前时间!");
		}
		int yearNow = cal.get(Calendar.YEAR);
		int monthNow = cal.get(Calendar.MONTH) + 1;// 注意此处，如果不加1的话计算结果是错误的
		int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
		cal.setTime(birthDay);
		int yearBirth = cal.get(Calendar.YEAR);
		int monthBirth = cal.get(Calendar.MONTH);
		int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
		int age = yearNow - yearBirth;
		if (monthNow <= monthBirth) {
			if (monthNow == monthBirth) {
				// monthNow==monthBirth
				if (dayOfMonthNow < dayOfMonthBirth) {
					age--;
				}
			} else {
				// monthNow>monthBirth
				age--;
			}
		}
		return age;
	}

	public static String getZodiac(int year) {
		String str = "鼠猪狗鸡猴羊马蛇龙兔虎牛";
		Integer now = Calendar.YEAR;
		Integer num = (now - year - 1) % 12;
		return str.substring(num, num + 1);
	}

	public static final int[] constellationEdgeDay = { 20, 19, 21, 21, 21, 22, 23, 23, 23, 23, 22, 22 };

	/**
	 * 根据日期获取生肖
	 * 
	 * @return
	 */
	public static int date2Zodica(Calendar time) {
		int index = time.get(Calendar.YEAR) % 12;// 此处得到的顺序是 { "猴", "鸡", "狗",
		// "猪", "鼠", "牛", "虎", "兔",
		// "龙", "蛇", "马", "羊" };
		if (index >= 4) {
			index = index - 4;
		} else
			index = index - 4 + 12;
		return index;
	}

	/**
	 * 根据日期获取星座
	 * 
	 * @param time
	 * @return
	 */
	public static int date2Constellation(Calendar time) {
		int month = time.get(Calendar.MONTH);
		int day = time.get(Calendar.DAY_OF_MONTH);
		if (day < constellationEdgeDay[month]) {
			month = month - 1;
		}
		if (month < 0) {
			month = 11;// default to return 魔羯
		}
		return month;
	}

	/**
	 * 震动
	 * 
	 * @param activity
	 * @param milliseconds
	 *            -->震动的时长，单位是毫秒
	 */
	public static void Vibrate(Context context, long milliseconds) {
		Vibrator vib = (Vibrator) context.getSystemService(Service.VIBRATOR_SERVICE);
		vib.vibrate(milliseconds);
	}

	/**
	 * 震动
	 * 
	 * @param activity
	 * @param pattern
	 *            -->自定义震动模式 。数组中数字的含义依次是[静止时长，震动时长，静止时长，震动时长。。。]时长的单位是毫秒
	 * @param isRepeat
	 *            -->是否反复震动，如果是true，反复震动，如果是false，只震动一次
	 */
	public static void Vibrate(Context context, long[] pattern, boolean isRepeat) {
		Vibrator vib = (Vibrator) context.getSystemService(Service.VIBRATOR_SERVICE);
		vib.vibrate(pattern, isRepeat ? 1 : -1);
	}

	/**
	 * 获取自身类名
	 * 
	 * @author wubo
	 * @createtime 2012-7-9
	 */
	public static String getClassName() {
		return new Throwable().getStackTrace()[1].getClassName();
	}

	/**
	 * TODO 去掉字符串中间的空格和 “-”字符(此处通讯录号码去空格)
	 * 
	 * @param str
	 *            原始字符串
	 * @return 结果字符串
	 */
	public static String delSpace(String str) {
		if (str == null) {
			return null;
		}
		char[] strOld = str.toCharArray();
		StringBuffer strNew = new StringBuffer();
		for (char a : strOld) {
			if (a != ' ' && a != '-') {
				strNew.append(a);
			}
		}
		return strNew.toString().trim();
	}

	/**
	 * date转str
	 * 
	 * @author wubo
	 * @createtime 2012-8-21
	 * @param date
	 * @return
	 */
	public static String date2Str(Date date) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
		try {
			return df.format(date);
		} catch (Exception ex) {
			return "";
		}
	}

	public static boolean isFolderExists(String strFolder) {
		File file = new File(strFolder);

		if (!file.exists()) {
			if (file.mkdirs()) {
				return true;
			} else
				return false;
		}
		return true;
	}

	/**
	 * 生成随即密码
	 * 
	 * @param pwd_len
	 *            生成的密码的总长度
	 * @return 密码的字符串
	 */
	public static String genRandomNum(int pwd_len) {
		// 35是因为数组是从0开始的，26个字母+10个数字
		final int maxNum = 36;
		int i; // 生成的随机数
		int count = 0; // 生成的密码的长度
		char[] str = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
				't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

		StringBuffer pwd = new StringBuffer("");
		Random r = new Random();
		while (count < pwd_len) {
			// 生成随机数，取绝对值，防止生成负数，

			i = Math.abs(r.nextInt(maxNum)); // 生成的数最大为36-1

			if (i >= 0 && i < str.length) {
				pwd.append(str[i]);
				count++;
			}
		}

		return pwd.toString();
	}

	/**
	 * Workaround for bug pre-Froyo, see here for more info:
	 * http://android-developers.blogspot.com/2011/09/androids-http-clients.html
	 */
	public static void disableConnectionReuseIfNecessary() {
		// HTTP connection reuse which was buggy pre-froyo
		if (hasHttpConnectionBug()) {
			System.setProperty("http.keepAlive", "false");
		}
	}

	/**
	 * Get the external app cache directory.
	 * 
	 * @param context
	 *            The context to use
	 * @return The external cache dir
	 */

	public static File getExternalCacheDir(Context context) {
		if (hasExternalCacheDir() && context.getExternalCacheDir() != null) {
			return context.getExternalCacheDir();
		}

		// Before Froyo we need to construct the external cache dir ourselves
		final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
		return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
	}

	/**
	 * Check how much usable space is available at a given path.
	 * 
	 * @param path
	 *            The path to check
	 * @return The space available in bytes
	 */

	public static long getUsableSpace(File path) {

		final StatFs stats = new StatFs(path.getPath());
		return (long) stats.getBlockSize() * (long) stats.getAvailableBlocks();
	}

	/**
	 * Get the memory class of this device (approx. per-app memory limit)
	 * 
	 * @param context
	 * @return
	 */
	public static int getMemoryClass(Context context) {
		return ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
	}

	/**
	 * Check if OS version has a http URLConnection bug. See here for more
	 * information:
	 * http://android-developers.blogspot.com/2011/09/androids-http-clients.html
	 * 
	 * @return
	 */
	public static boolean hasHttpConnectionBug() {
		return Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO;
	}

	/**
	 * Check if OS version has built-in external cache dir method.
	 * 
	 * @return
	 */
	public static boolean hasExternalCacheDir() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	/**
	 * 
	 * password check
	 * 
	 * @author wubo
	 * @createtime 2012-9-13
	 * @param str
	 * @return
	 */
	public static boolean isPassword(String str) {
		if (str == null || str.length() < 2 || str.length() > 10) {
			return false;
		}
		Pattern pattern = Pattern.compile("[0-9A-Za-z_]");
		Matcher matcher = pattern.matcher(str);
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean ifShowLock(int restype) {
		for (int i = 0; i < PangkerConstant.RES_VISIBLE_LOCK.length; i++) {
			if (restype == PangkerConstant.RES_VISIBLE_LOCK[i]) {
				return true;
			}
		}
		return false;
	}

	public static void instalApplication(Context context, String path) {
		if (Util.isExitFileSD(path)) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setDataAndType(Uri.parse("file://" + path), "application/vnd.android.package-archive");
			context.startActivity(intent);
		} else {
			Toast.makeText(context, "请检查SD卡文件是否存在！", Toast.LENGTH_SHORT).show();
		}
	}

	public static String getToday() {

		Date now = new Date();
		DateFormat d1 = DateFormat.getDateInstance();
		return d1.format(now);

	}

	/**
	 * 复制文件（夹）到一个目标文件夹
	 * 
	 * @param resFile
	 *            源文件（夹）
	 * @param objFolderFile
	 *            目标文件夹
	 * @throws IOException
	 *             异常时抛出
	 */
	public static void copy(File resFile, String folderFile, String userId) throws IOException {
		String saveFileName = userId + "." + CompressFormat.PNG.toString();
		if (!resFile.exists())
			return;
		File objFolderFile = new File(folderFile);
		if (!objFolderFile.exists())
			objFolderFile.mkdirs();
		if (resFile.isFile()) {
			String saveFile = objFolderFile.getPath() + File.separator + saveFileName;
			Util.deleteFileSD(saveFile);
			File objFile = new File(saveFile);
			// 复制文件到目标地
			InputStream ins = new FileInputStream(resFile);
			FileOutputStream outs = new FileOutputStream(objFile);
			byte[] buffer = new byte[1024 * 512];
			int length;
			while ((length = ins.read(buffer)) != -1) {
				outs.write(buffer, 0, length);
			}
			ins.close();
			outs.flush();
			outs.close();
		} else {
			String objFolder = objFolderFile.getPath() + File.separator + resFile.getName();
			File _objFolderFile = new File(objFolder);
			_objFolderFile.mkdirs();
			for (File sf : resFile.listFiles()) {
				copy(sf, objFolder, saveFileName);
			}
		}
	}

	public static String formatDuration(Integer seconds) {
		if (seconds == null) {
			return null;
		}

		int minutes = seconds / 60;
		int secs = seconds % 60;

		StringBuilder builder = new StringBuilder(6);
		builder.append(minutes).append(":");
		if (secs < 10) {
			builder.append("0");
		}
		builder.append(secs);
		return builder.toString();
	}

	static public Integer String2Integer(String str) {
		if (null == str) {
			return 0;
		}
		try {
			return Integer.valueOf(str);
		} catch (Exception e) {
			return 0;
		}
	}

	static public Long String2Long(String str) {
		if (null == str) {
			return 0l;
		}
		try {
			return Long.valueOf(str);
		} catch (Exception e) {
			return 0l;
		}
	}

	static public Double String2Double(String str) {
		if (null == str) {
			return null;
		}
		try {
			return Double.valueOf(str);
		} catch (Exception e) {
			return null;
		}
	}

	public static String getHtmlString(String str) {
		String html = "<a style=\"color:white;\" href='null'>" + str + "</a>";
		return html;
	}

	public static String getHtmlLimitString(String str, int limit) {
		String html = "<a style=\"color:white;\" href='null'>" + getStringByLength(str, limit) + "</a>";
		return html;
	}

	public static String getLastPath(String currentPath) {
		// TODO Auto-generated method stub
		return currentPath.substring(0, currentPath.lastIndexOf("/"));
	}

	public static String getUpdateAppend(String[] keys, String[] values) {
		// TODO Auto-generated method stub
		StringBuffer appString = new StringBuffer();
		for (int i = 0; i < keys.length; i++) {
			if (i == keys.length - 1) {
				appString.append(keys[i]).append("='").append(values[i]).append("'");
			} else {
				appString.append(keys[i]).append("='").append(values[i]).append("',");
			}
		}
		return appString.toString();
	}

	/**
	 * 字符串定长截断函数。
	 * 
	 * @param text
	 *            需要截断的字符串
	 * @param textMaxChar
	 *            需要留下的长度
	 * @author niko7@21cn.com
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static int cutStringHalfLength(String text, int textMaxChar) throws UnsupportedEncodingException {
		int size, index;
		if (textMaxChar < 0)
			return 0;
		for (size = 0, index = 0; index < text.length() && size < textMaxChar; index++) {
			size += getHalfStringLength(text.substring(index, index + 1));
		}

		return index;
	}

	/**
	 * 半角长度获取
	 * 
	 * @param ss
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static int getHalfStringLength(String ss) throws UnsupportedEncodingException {
		byte[] b = null;
		b = ss.getBytes("GBK");
		return b.length;
	}

	public static boolean isExistLocatanct(List<LocalContacts> contacts, String contactId) {
		final Iterator<LocalContacts> iter = new ArrayList<LocalContacts>(contacts).iterator();
		while (iter.hasNext()) {
			LocalContacts contact = (LocalContacts) iter.next();
			if (contact != null && contact.getContactId().equals(contactId)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取文件大小
	 * 
	 * @param filePath
	 * @return
	 */
	public static int fileSize(String filePath) {
		File f = new File(filePath);
		try {
			FileInputStream fis = new FileInputStream(f);
			try {

				return fis.available();
			} catch (IOException e1) {
				return 0;
			}
		} catch (FileNotFoundException e2) {
			return 0;
		}
	}

	/**
	 * 获取文件大小
	 * 
	 * @param filePath
	 * @return
	 */
	public static int fileSize(File file) {

		try {
			FileInputStream fis = new FileInputStream(file);
			try {

				return fis.available();
			} catch (IOException e1) {
				return 0;
			}
		} catch (FileNotFoundException e2) {
			return 0;
		}
	}

	/*
	 * 获取当前程序的版本名
	 */
	public static VersionResult getVersion(Context mContext) throws Exception {
		// 获取packagemanager的实例
		PackageManager packageManager = mContext.getPackageManager();
		// getPackageName()是你当前类的包名，0代表是获取版本信息
		PackageInfo packInfo = packageManager.getPackageInfo(mContext.getPackageName(), 0);
		VersionResult version = new VersionResult();
		version.setVersioncode(String.valueOf(packInfo.versionCode));
		version.setIllustrate(packInfo.versionName);
		return version;
	}

	/**
	 * 判断是否后台运行
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isBackground(Context context) {

		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
		for (RunningAppProcessInfo appProcess : appProcesses) {
			if (appProcess.processName.equals(context.getPackageName())) {
				if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {

					return true;
				} else {

					return false;
				}
			}
		}
		return false;
	}


	public static Date formatDate(String time) {
		SimpleDateFormat sdf = new SimpleDateFormat(PangkerConstant.DATE_FORMAT);
		try {
			return sdf.parse(time);
		} catch (ParseException e) {
			return null;
		}
	}

}
