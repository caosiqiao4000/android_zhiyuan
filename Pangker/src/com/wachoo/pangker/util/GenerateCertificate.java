package com.wachoo.pangker.util;

import java.util.List;

import mobile.http.Parameter;
import android.content.Context;

import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.SupportRequestParams;

/**
 * 
 * 生成本地证书（登录使用）
 * 
 * @author wangxin
 *
 */
public class GenerateCertificate {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	private SharedPreferencesUtil SPUtil;
	/**
	 * 请求获取登录证书
	 */
	public void getLoginCertificate(Context context,String IMSI,IUICallBackInterface iCallBackInterface) {
		// Looper.prepare();
		try {
			SPUtil = new SharedPreferencesUtil(context);
			List<Parameter> requestParams = SupportRequestParams.getUserNameParams(IMSI);// 
			System.out.println("imsi--" + IMSI);
			System.out.println("requestParams-" + requestParams);
			ServerSupportManager supportManager = new ServerSupportManager(context, iCallBackInterface);
			supportManager.supportRequest("", requestParams);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
	}
}
