package com.wachoo.pangker.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import android.content.Context;

/**
 * @author wubo
 * @createtime 2012-3-27
 */
public class MeetRoomUtil {
	private static String TAG = "MeetRoomUtil";// log
	// tag
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private static String P2PServiceIP = "192.168.3.212";
//	private static String P2PServiceIP = "119.147.1.212";
	private static int P2PService_TUP_PORT = 3230;
	private static int P2PService_SESSION_PORT = 3120;
	// GroupSP监听端口：1230，客户端连接SP进行发布/获取资源
	private static long spUid;// 用户信息
	
	public static final String P2P_SERVER_IP = "P2P_SERVER_IP";
	public static final String P2P_SP_UID = "P2P_SP_UID";
	public static final String P2P_GROUP_TUP_PORT = "P2P_GROUP_TUP_PORT";
	public static final String P2P_GROUP_SESSION_PORT = "P2P_GROUP_SESSION_PORT";

	public static final int TYPE_SOCK_TYPE = 0xEAB4;// TCP:0xEAB1,UDT:0xEAB2,CTP:0xEAB3

	public static final int handler_SP2CRoom_Login_Resp = 100;
	public static final int handler_SP2CRoom_Status_Resp = 101;
	public static final int handler_SP2CRoom_Status_Notify = 102;
	public static final int handler_SP2CRoom_Speech_Notify = 103;
	public static final int handler_SP2NextHop_Text_Stream = 104;
	public static final int handler_SP2CRoom_Speech_Resp = 105;
	public static final int handler_SP2CRoom_SpeechList_Rsep = 106;
	public static final int handler_SP2CRoom_SpeechListStatus_Notify = 107;
	public static final int handler_SP2CRoom_StreamStop = 108;
	public static final int handler_SP2CRoom_MicUp = 109;//
	public static final int handler_SP2CRoom_MicCatch = 110;
	public static final int handler_SP2NextHop_Loc_Stream = 111;
	public static final int handler_SP2Login_Fail = 112;
	public static final int handler_SetTime_Flag = 113;
	public static final int handler_SP2Client_NotRead_Resp = 114;
	public static final int handler_SP2NextHop_PIC_Stream = 115;
	public static final int handler_SP2Status_Notify = 116;
	// >>>>>>>>>>>群组断开groupSP的通知消息
	public static final int handler_SP2CRoom_Disconnect = 119; //连接失败准备退出
	public static final int handler_SP2CRoom_ReLogin = 120;
	public static final int handler_SP2CRoom_Logout = 121;//完全退出

	// >>>>>>>占麦
	public static final int handler_SpeechMicUP_Notify = 117;
	// >>>>>>下麦
	public static final int handler_SpeechMicDOWN_Notify = 118;
	
	
	public static final int handler_SP2CRoom_Map_Enter = 131;//进入群组地图
	public static final int handler_SP2CRoom_Map_Leave = 132;//离开群组地图
	public static final int handler_SP2CRoom_Map_Refresh = 133;//刷新群组地图，他人进入/离开

	public static final String MEETROOM_CHAT_TXT_MESSAGE_CONTENT = "MEETROOM_CHAT_TXT_MESSAGE_CONTENT";
	public static final String MEETROOM_CHAT_LOCATION_CONTENT = "MEETROOM_CHAT_LOCATION_CONTENT";

	/**
	 * 获取本机IP
	 * 
	 * @author wubo
	 * @createtime 2012-3-27
	 * @return
	 */
	public static String getIp() {
		String ipaddr = "";
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en
					.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr
						.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) {

						ipaddr = inetAddress.getHostAddress().toString();
					}
				}
				return ipaddr;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
		}
		return "";
	}

	public static long getSpUid(Context context) {
		// return 215;
		if(spUid == 0){
			SharedPreferencesUtil sp = new SharedPreferencesUtil(context);
			spUid = Long.valueOf(sp.getString(MeetRoomUtil.P2P_SP_UID, "4294967301"));
		}
		return spUid;
	}

	public static void setSpUid(long spUid) {
		MeetRoomUtil.spUid = spUid;
	}

	public static String getP2PServiceIp(Context context) {
//		if(Util.isEmpty(p2PServiceIp)){
//			SharedPreferencesUtil sp = new SharedPreferencesUtil(context);
//			p2PServiceIp = sp.getString(P2P_SERVER_IP, p2PServiceIp);
//		}
		return P2PServiceIP;
	}

	public static int getGroupSpTUPPort(Context context) {
		if(P2PService_TUP_PORT == 0){
			SharedPreferencesUtil sp = new SharedPreferencesUtil(context);
			P2PService_TUP_PORT = sp.getInt(P2P_GROUP_TUP_PORT, P2PService_TUP_PORT);
		}
		return P2PService_TUP_PORT;
	}
	
	public static int getGroupSpSessionPort(Context context) {
		if(P2PService_SESSION_PORT == 0){
			SharedPreferencesUtil sp = new SharedPreferencesUtil(context);
			P2PService_SESSION_PORT = sp.getInt(P2P_GROUP_SESSION_PORT, P2PService_SESSION_PORT);
		}
		return P2PService_SESSION_PORT;
	}

	/**
	 * 发言状态
	 * @author wubo
	 * @createtime Apr 25, 2012
	 */
	public static enum SpeechAllow {
		// >>>>>>初始化
		INITIALIZATION,
		// >>>>>> 抢麦（等待中）
		IN_MIC_SEQUENCE,
		// >>>>>>通知可以发言
		CAN_SPEAK,
		// >>>>>发言中
		SPEECHING
	}

	// CAN_UP_MIC:可以抢麦，状态为未使用；CAN_DOWN_MIC：可以下麦
	public static enum MicStatus {
		CAN_UP_MIC, CAN_DOWN_MIC
	}


	/**
	 * byte to int
	 * 
	 * @author wubo
	 * @time 2012-11-16
	 * @param b
	 * @return
	 * 
	 */
	public static int byteToInt(byte[] bytes) {
		int addr = bytes[0] & 0xFF;

		addr |= ((bytes[1] << 8) & 0xFF00);

		addr |= ((bytes[2] << 16) & 0xFF0000);

		addr |= ((bytes[3] << 24) & 0xFF000000);

		return addr;

	}
	
	public static long byteToLong(byte[] b) {
		long l = 0;

		l = b[0];

		l |= ((long) b[1] << 8);

		l |= ((long) b[2] << 16);

		l |= ((long) b[3] << 24);

		l |= ((long) b[4] << 32);

		l |= ((long) b[5] << 40);

		l |= ((long) b[6] << 48);

		l |= ((long) b[7] << 56);

		return l;

	}

	/**
	 * int to byte
	 * @author wubo
	 * @time 2012-11-19
	 * @param i
	 * @return
	 * 
	 */
	public static byte[] intToByte(int i) {
		byte[] bt = new byte[4];
		bt[0] = (byte) (0xff & i);
		bt[1] = (byte) ((0xff00 & i) >> 8);
		bt[2] = (byte) ((0xff0000 & i) >> 16);
		bt[3] = (byte) ((0xff000000 & i) >> 24);
		return bt;
	}

}
