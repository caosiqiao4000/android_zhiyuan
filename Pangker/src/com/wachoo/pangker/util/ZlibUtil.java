package com.wachoo.pangker.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import android.util.Log;

/**
 *@author wubo
 *@createtime Apr 18, 2012
 */
public class ZlibUtil {  
	private static String TAG = "ZlibUtil";// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
    /** 
     * 压缩 
     *  
     * @param data 
     *            待压缩数据 
     * @return byte[] 压缩后的数据 
     */  
    public static byte[] compress(byte[] data) {  
        byte[] output = new byte[0];  
  
        Deflater compresser = new Deflater();  
  
        compresser.reset();  
        compresser.setInput(data);  
        compresser.finish();  
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);  
        try {  
            byte[] buf = new byte[1024];  
            while (!compresser.finished()) {  
                int i = compresser.deflate(buf);  
                bos.write(buf, 0, i);  
            }  
            output = bos.toByteArray();  
        } catch (Exception e) {  
            output = data;  
            logger.error(TAG, e);  
        } finally {  
            try {  
                bos.close();  
            } catch (IOException e) {  
                logger.error(TAG, e);  
            }  
        }  
        compresser.end();  
        return output;  
    }  
  
    /** 
     * 压缩 
     *  
     * @param data 
     *            待压缩数据 
     *  
     * @param os 
     *            输出流 
     */  
    public static void compress(byte[] data, OutputStream os) {  
        DeflaterOutputStream dos = new DeflaterOutputStream(os);  
  
        try {  
            dos.write(data, 0, data.length);  
  
            dos.finish();  
  
            dos.flush();  
        } catch (IOException e) {  
            logger.error(TAG, e);  
        }  
    }  
  
    /** 
     * 解压缩 
     *  
     * @param data 
     *            待压缩的数据 
     * @return byte[] 解压缩后的数据 
     */  
    public static byte[] decompress(byte[] data) {  
        byte[] output = new byte[0];  
  
        Inflater decompresser = new Inflater();  
        decompresser.reset();  
        decompresser.setInput(data);  
  
        ByteArrayOutputStream o = new ByteArrayOutputStream(data.length);  
        
        try {  
        	byte[] buf = new byte[1024];  
            while (!decompresser.finished()) {  
                int i = decompresser.inflate(buf);  
                o.write(buf, 0, i);  
            }  
            output = o.toByteArray(); 
            return output; 
        } catch (Exception e) {  
        	Log.e(TAG, "decompress==>" + e.getMessage());
            logger.error(TAG, e);  
            return null;
        } finally {  
        	decompresser.end();  
            try {  
                o.close();  
            } catch (IOException e) {  
                logger.error(TAG, e);  
            }  
        }  
    }  
  
    /** 
     * 解压缩 
     *  
     * @param is 
     *            输入流 
     * @return byte[] 解压缩后的数据 
     */  
    public static byte[] decompress(InputStream is) {  
        InflaterInputStream iis = new InflaterInputStream(is);  
        ByteArrayOutputStream o = new ByteArrayOutputStream(1024);  
        try {  
            int i = 1024;  
            byte[] buf = new byte[i];  
  
            while ((i = iis.read(buf, 0, i)) > 0) {  
                o.write(buf, 0, i);  
            }  
  
        } catch (IOException e) {  
            logger.error(TAG, e);  
        }  
        return o.toByteArray();  
    }  
}  
