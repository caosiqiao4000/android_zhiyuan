package com.wachoo.pangker.util;

import mobile.encrypt.EncryptUtils;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.SmsManager;

import com.wachoo.pangker.PangkerConstant;
/**
 * 
 * ConnectivityHelper 网络工具
 * 
 * @author wangxin
 *
 */
public class ConnectivityHelper {
	
	/**
	 * 判断网络是否可用
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isConnectivityAvailable(Context context) {
		// 判断网络是否可用
		NetworkInfo info = null;
		ConnectivityManager cManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		info = cManager.getActiveNetworkInfo();
		if (info == null || !info.isConnected()) {
			return false;
		}
		if (info != null && info.isAvailable()) {
			return true;
		}
		if (info.isRoaming()) {
			return true;
		}
		return false;
	}

	/**
	 * 判断wifi网络是否可用
	 * 
	 * @param context
	 * @return
	 */
	public static boolean WifiIsAvailable(Context context) {
		// 判断网络是否可用
		NetworkInfo info = null;
		ConnectivityManager cManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		info = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (info != null && info.isAvailable()) {
			return true;
		}
		return false;
	}
	
	/**
	 * 判断moblie网络是否可用
	 * 
	 * @param context
	 * @return
	 */
	public static boolean MobileIsAvailable(Context context) {
		// 判断网络是否可用
		NetworkInfo info = null;
		ConnectivityManager cManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		info = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (info != null && info.isAvailable()) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * 获得手机网络类别
	 */

	public static int getPhoneNetworkType(String IMSI) {

		// IMSI号前面3位460是国家，紧接着后面2位00 02是中国移动，01是中国联通，03是中国电信。

		System.out.println(IMSI);

		if (IMSI.startsWith("46000") || IMSI.startsWith("46002")) {

			return PangkerConstant.CHINA_MOBILE_TYPE;

		} else if (IMSI.startsWith("46001")) {

			return PangkerConstant.CHINA_UNICOM_TYPE;

		} else if (IMSI.startsWith("46003")) {

			return PangkerConstant.CHINA_TELECOM_TYPE;

		}
		return PangkerConstant.CHINA_UNICOM_TYPE;
	}

	/**
	 * #0#888#460002281198348#asdfc#
	 * 
	 * TYPE=0 UID=888 IMSI=460002281198348 Authenticator= asdfc
	 * 
	 * Authenticator 计算规则 ： Des3.Encrypt(TYPE+$+ UID+$+IMSI)
	 */
	public static void sendBindSMSMessage(String UID, String IMSI) {

		String Authenticator;
		try {
			Authenticator = EncryptUtils.getAuthenticator("0$" + UID + "$" + IMSI);

			String content = "#0#" + UID + "#" + IMSI + "#" + Authenticator + "#"; // 发送信息的内容

			SmsManager manager = SmsManager.getDefault();
			switch (getPhoneNetworkType(IMSI)) {
			case PangkerConstant.CHINA_MOBILE_TYPE:
				manager.sendTextMessage(PangkerConstant.CHINA_MOBILE_PORT, null, content, null, null);
				break;
			case PangkerConstant.CHINA_UNICOM_TYPE:
				manager.sendTextMessage(PangkerConstant.CHINA_UNICOM_PORT, null, content, null, null);
				break;
			case PangkerConstant.CHINA_TELECOM_TYPE:
				manager.sendTextMessage(PangkerConstant.CHINA_TELECOM_PORT, null, content, null, null);
				break;
			default:
				break;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static final int NONET = -1;
	public static final int CMNET = 3;
	public static final int CMWAP = 2;
	public static final int WIFI = 1;

	/**
	 * 获取当前的网络状态 -1：没有网络 1：WIFI网络2：wap网络3：net网络
	 * 
	 * @param context
	 * @return
	 */
	public static int getAPNType(Context context) {
		int netType = NONET;
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

		if (networkInfo == null) {
			return netType;
		}
		int nType = networkInfo.getType();
		System.out.println("networkInfo.getExtraInfo() is " + networkInfo.getExtraInfo());
		if (nType == ConnectivityManager.TYPE_MOBILE) {
			if (networkInfo.getExtraInfo().toLowerCase().equals("cmnet")) {
				netType = CMNET;
			} else {
				netType = CMWAP;
			}
		} else if (nType == ConnectivityManager.TYPE_WIFI) {
			netType = WIFI;
		}
		return netType;
	}


}
