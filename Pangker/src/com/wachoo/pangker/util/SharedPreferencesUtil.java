package com.wachoo.pangker.util;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 这是一个保存共享的数据工具类
 * 
 * @author Administrator
 * 
 */
public class SharedPreferencesUtil {

	private final String PREFERENCE_NAME = "jxdximclient";
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private Context mContext;
	private SharedPreferences mSharedPreferences;

	public SharedPreferencesUtil(Context context) {
		mContext = context;
		mSharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
	}

	// 保存字符串
	public void saveString(String key, String value) {
		mSharedPreferences.edit().putString(key, value).commit();
	}

	// 获取字符串
	public String getString(String key, String... defValue) {
		if (defValue.length > 0)
			return mSharedPreferences.getString(key, defValue[0]);
		else
			return mSharedPreferences.getString(key, "");

	}

	// 保存布尔值
	public void saveInt(String key, Integer value) {
		mSharedPreferences.edit().putInt(key, value).commit();
	}

	// 获取布尔值
	public int getInt(String key, Integer defValue) {
		return mSharedPreferences.getInt(key, defValue);
	}

	// 保存布尔值
	public void saveBoolean(String key, Boolean value) {
		mSharedPreferences.edit().putBoolean(key, value).commit();
	}

	// 获取布尔值
	public Boolean getBoolean(String key, Boolean... defValue) {
		if (defValue.length > 0)
			return mSharedPreferences.getBoolean(key, defValue[0]);
		else
			return mSharedPreferences.getBoolean(key, false);

	}

	// 保存布尔值
	public void saveFloat(String key, Float value) {
		mSharedPreferences.edit().putFloat(key, value).commit();
	}

	// 获取布尔值
	public Float getFloat(String key, Float... defValue) {
		if (defValue.length > 0)
			return mSharedPreferences.getFloat(key, defValue[0]);
		else
			return mSharedPreferences.getFloat(key, 0.0f);

	}

	// 返回用户的信息。
	public JSONObject getJSON(String count) {
		JSONObject object;
		try {
			object = new JSONObject(mSharedPreferences.getString(count, ""));
			return object;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
			return null;
		}
	}

	public void saveJSONObject(String count, String json) {
		mSharedPreferences.edit().putString(count, json).commit();
	}

	public void removeJSONObject(String count) {
		try {
			mSharedPreferences.edit().remove(count);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static final String SHAREDPREFERENCES_NAME = "my_pref";
	// 引导界面KEY
	private static final String KEY_GUIDE_ACTIVITY = "guide_activity";

	/**
	 * 判断activity是否引导过
	 * 
	 * @param context
	 * @return 是否已经引导过 true引导过了 false未引导
	 */
	public static boolean activityIsGuided(Context context, String className) {
		if (context == null || className == null || "".equalsIgnoreCase(className))
			return false;
		String[] classNames = context
				.getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_WORLD_READABLE)
				.getString(KEY_GUIDE_ACTIVITY, "").split("\\|");// 取得所有类名 如
																// com.my.MainActivity
		for (String string : classNames) {
			if (className.equalsIgnoreCase(string)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 设置该activity被引导过了。 将类名已 |a|b|c这种形式保存为value，因为偏好中只能保存键值对
	 * 
	 * @param context
	 * @param className
	 */
	public static void setIsGuided(Context context, String className) {
		if (context == null || className == null || "".equalsIgnoreCase(className))
			return;
		String classNames = context.getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_WORLD_READABLE)
				.getString(KEY_GUIDE_ACTIVITY, "");
		StringBuilder sb = new StringBuilder(classNames).append("|").append(className);// 添加值
		context.getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_WORLD_READABLE)// 保存修改后的值
				.edit().putString(KEY_GUIDE_ACTIVITY, sb.toString()).commit();
	}
}
