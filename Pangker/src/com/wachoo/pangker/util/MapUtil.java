package com.wachoo.pangker.util;

import java.text.DecimalFormat;
import java.util.List;

import mobile.json.JSONUtil;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.Point;
import android.location.LocationManager;
import android.util.Log;

import com.amap.mapapi.core.GeoPoint;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.map.app.LocateManager;
import com.wachoo.pangker.map.poi.Poi_concise;

public class MapUtil {

	public final static double EarthRadiu = 6378.137f;

	/**
	 * 根据两点间经纬度坐标（double值），计算两点间距离，单位为米
	 * 
	 * @param lng1
	 * @param lat1
	 * @param lng2
	 * @param lat2
	 * @return
	 */
	public static int DistanceOfTwoPoints(double lat1, double lng1, double lat2, double lng2) {

		double radLat1 = rad(lat1);
		double radLat2 = rad(lat2);
		double a = radLat1 - radLat2;
		double b = rad(lng1) - rad(lng2);

		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1)
				* Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
		s = s * EarthRadiu;
		return Integer.parseInt(Math.round(s * 1000) + "");
	}

	public static double DistanceOfTwoPoints(Location loca1, Location loca2) {
		return DistanceOfTwoPoints(loca1.getLatitude(), loca1.getLongitude(), loca2.getLatitude(),
				loca2.getLongitude());
	}

	private static double rad(double d) {
		return d * Math.PI / 180;
	}

	public static double setMapLength(Point point1, Point point2) {
		double d = Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2));
		return d;
	}

	public static GeoPoint toPoint(Location location) {
		if (location == null) {
			return null;
		}
		int lat = (int) (location.getLatitude() * 1e6);
		int lng = (int) (location.getLongitude() * 1e6);
		return new GeoPoint(lat, lng);
	}

	public static Location toLocation(GeoPoint point) {
		if (point == null) {
			return null;
		}
		Location location = new Location();
		double latitude = 1.0 * point.getLatitudeE6() / 1E6;
		location.setLatitude(latitude);
		double longitude = 1.0 * point.getLongitudeE6() / 1E6;
		location.setLongitude(longitude);
		return location;
	}

	public static double getRange(Poi_concise concise) {
		double lat1 = Double.parseDouble(concise.getLat());
		double lng1 = Double.parseDouble(concise.getLon());

		// double lat = PangkerCache.getCurrentLocation().getLatitude();
		// double lng = PangkerCache.getCurrentLocation().getLongitude();

		return DistanceOfTwoPoints(lat1, lng1, lat1, lng1);
	}

	/**
	 * 
	 * @param context
	 * @return
	 */
	public static boolean checkGoogleMap(Context context) {
		boolean isInstallGMap = false;
		List<PackageInfo> packs = context.getPackageManager().getInstalledPackages(0);
		for (int i = 0; i < packs.size(); i++) {
			PackageInfo p = packs.get(i);
			if (p.versionName == null) { // system packages
				continue;
			}
			if ("com.google.android.apps.maps".equals(p.packageName)) {
				isInstallGMap = true;
				break;
			}
		}
		return isInstallGMap;
	}

	public static boolean isOpenGPS(Context context) {
		LocationManager alm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		return alm.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
	}

	public static GeoPoint toGeoPoint(String msg) {
		try {

			String[] point = msg.split(",");
			int lat = (int) (Double.parseDouble(point[0]) * 1e6);
			int lng = (int) (Double.parseDouble(point[1]) * 1e6);
			return new GeoPoint(lat, lng);
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	public static com.wachoo.pangker.entity.Location getLastLocation(Context context) {
		SharedPreferencesUtil sPreferencesUtil = new SharedPreferencesUtil(context);
		String strLocation = sPreferencesUtil.getString(LocateManager.LOCATE_JSON, "");
		com.wachoo.pangker.entity.Location loc = JSONUtil.fromJson(strLocation,
				com.wachoo.pangker.entity.Location.class);
		if (loc == null) {
			loc = new com.wachoo.pangker.entity.Location(LocateManager.LOCATE_LON, LocateManager.LOCATE_LAT);
		}
		return loc;
	}

	public static com.wachoo.pangker.entity.Location getLastDirftLocation(Context context) {
		SharedPreferencesUtil sPreferencesUtil = new SharedPreferencesUtil(context);
		String strLocation = sPreferencesUtil.getString(LocateManager.DIRFT_JSON, "");
		com.wachoo.pangker.entity.Location loc = JSONUtil.fromJson(strLocation,
				com.wachoo.pangker.entity.Location.class);
		if (loc == null) {
			loc = new com.wachoo.pangker.entity.Location(LocateManager.LOCATE_LON, LocateManager.LOCATE_LAT);
		}
		return loc;
	}

	public static String toString(GeoPoint geoPoint) {
		Location location = toLocation(geoPoint);
		return new StringBuffer().append(location.getLatitude()).append(",").append(location.getLongitude())
				.toString();
	}

	public static String getDistance(double distance) {
		String str_distance = null;
		if (distance > 1000) {
			distance = distance / 1000;
			DecimalFormat df = new DecimalFormat("#");
			str_distance = df.format(distance) + "公里";
		} else {
			str_distance = String.valueOf(distance) + "米";
		}
		return str_distance;
	}

	/**
	 * 行人距离 String TODO 如果小于50米，则显示‘50米内’
	 * 
	 * @param distance
	 * @return
	 */
	public static String getUserDistance(double distance) {
		Log.i("getUserDistance", "distance" + distance);
		String str_distance = null;
		if (distance < 50) {
			str_distance = "50米内";
		}
		// 几百米内
		else if (distance > 50 && distance < 900) {
			for (int i = 1; i < 10; i++) {
				if (distance > (i - 1) * 100 && distance < i * 100) {
					str_distance = i * 100 + "米内";
					break;
				}
			}
		}
		// 20公里内
		else if (distance < 20000) {
			for (int i = 1; i < 21; i++) {
				if (distance > (i - 1) * 1000 && distance < i * 1000) {
					str_distance = i + "公里内";
					break;
				}
			}
		} else
			str_distance = "20公里外";

		Log.i("getUserDistance", "str_distance" + str_distance);
		return str_distance;
	}

	/**
	 * 亲友距离 String TODO 亲友圈’中的距离，显示具体数字（如1035KM），小于10KM时显示到米（如9500米）
	 * 
	 * @param distance
	 * @return
	 */
	public static String getFriendDistance(int distance) {
		String str_distance = null;
		if (distance < 100) {
			str_distance = "100米以内";
		} else if (distance >= 100 && distance < 1000) {
			str_distance = distance + "米";
		}
		// 20公里内
		else if (distance >= 1000) {
			str_distance = (distance / 1000) + "公里";
		}

		return str_distance;
	}
}
