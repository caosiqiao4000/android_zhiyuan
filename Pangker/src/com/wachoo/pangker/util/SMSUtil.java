package com.wachoo.pangker.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.wachoo.pangker.activity.R;

public class SMSUtil {
	/**
	 * TODO系统发送短信界面
	 * 
	 * @param mobileNum
	 *            接受人（支持多人，分号“；”隔离）
	 * @param smsStr
	 *            短信内容
	 */
	public static void sendInviteSMS(String mobileNum, String pangkerNo,
			Context context) {
		// >>>>>>>邀请注册的文字内容
		String invite = context.getString(R.string.invite_register_text);
		// >>>>>>>旁客的主页
		String pangkerHome = context.getString(R.string.pangker_home_url);

		// >>>>>>>>邀请注册短信内容
		String sendInviteMessage = null;
		if (Util.isEmpty(pangkerNo))
			sendInviteMessage = invite + pangkerHome;
		else {
			pangkerHome = pangkerHome + pangkerNo + ".html";
			sendInviteMessage = invite + pangkerHome;
		}

		// >>>>>>>>>>发送短信
		Uri smsToUri = Uri.parse("smsto:" + mobileNum);
		Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
		mIntent.putExtra("sms_body", sendInviteMessage);
		context.startActivity(mIntent);
	}

	public static boolean isMobileNO(String mobiles) {
		Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}
}
