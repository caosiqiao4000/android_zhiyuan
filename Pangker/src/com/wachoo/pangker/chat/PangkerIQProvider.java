package com.wachoo.pangker.chat;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

import com.wachoo.pangker.chat.OpenfireManager.BusinessType;

public class PangkerIQProvider implements IQProvider {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	@Override
	public IQ parseIQ(XmlPullParser paramXmlPullParser) throws Exception {
		// TODO Auto-generated method stub
		// paramXmlPullParser.get
		PangkerIQ iq = new PangkerIQ();
		BusinessType businessType = BusinessType.error;
		try {
			int i = 0;

			while (i == 0) {
				int j = paramXmlPullParser.next();
				paramXmlPullParser.getAttributeValue("", "reason");
				if (j == 2) {
					if (paramXmlPullParser.getName().equals("pangker")) {
						String business = paramXmlPullParser.getAttributeValue("", "businesstype");
						businessType = Enum.valueOf(BusinessType.class, business);
						iq.setReason(paramXmlPullParser.getAttributeValue("", "reason"));
						iq.setResponse(paramXmlPullParser.getAttributeValue("", "response"));
						iq.setBusinessID(paramXmlPullParser.getAttributeValue("", "businessid"));
						i = 1;
					}
				}
			}
			// paramXmlPullParser.next();
			// paramXmlPullParser.getAttributeValue("", "businesstype");
			// paramXmlPullParser.getAttributeValue(
			// paramXmlPullParser.getNamespace(), "businesstype");
			// reason = paramXmlPullParser.getAttributeValue(1);

		} catch (Exception e) {
			logger.error(TAG, e);
		}

		iq.setBusinessType(businessType);
		return iq;
	}

}
