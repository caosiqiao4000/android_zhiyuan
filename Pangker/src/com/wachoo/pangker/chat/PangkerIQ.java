package com.wachoo.pangker.chat;

import org.jivesoftware.smack.packet.IQ;

import com.wachoo.pangker.chat.OpenfireManager.BusinessType;

public class PangkerIQ extends IQ {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	private BusinessType businessType;
	private String reason;
	private String response;
	private String businessID;

	public String getBusinessID() {
		return businessID;
	}

	public void setBusinessID(String businessID) {
		this.businessID = businessID;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getReason() {
		return reason;
	}

	public PangkerIQ(String to) {
		setTo(to);
	}

	public PangkerIQ() {
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BusinessType getBusinessType() {
		return businessType;
	}

	public void setBusinessType(BusinessType businessType) {
		this.businessType = businessType;
	}

	public String getElementName() {
		return "pangker";
	}

	public String getNamespace() {
		return "business:iq:type";
	}

	@Override
	public String getChildElementXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<").append(getElementName()).append(" xmlns=\"")
				.append(getNamespace()).append("\" businesstype=\"")
				.append(getBusinessType()).append("\" reason=\"")
				.append(getReason()).append("\" response=\"")
				.append(getResponse()).append("\" businessid=\"")
				.append(getBusinessID()).append("\">").append(getBusinessType())
				.append("</").append(getElementName()).append(">");
		return sb.toString();
	}


}
