package com.wachoo.pangker.chat;

import com.wachoo.pangker.entity.ChatMessage;

public interface IUserMsgListener {

	public boolean onReceiveUserMsg(ChatMessage msg);

	public void onMessageReceived(int messageID);
}
