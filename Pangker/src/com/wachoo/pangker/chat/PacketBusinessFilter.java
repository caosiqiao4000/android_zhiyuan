package com.wachoo.pangker.chat;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Packet;

import com.pangker.xmpp.SystemNotice.SystemNoticeType;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;

public class PacketBusinessFilter implements PacketFilter {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	@Override
	public boolean accept(Packet paramPacket) {
		if (paramPacket instanceof Message) {
			Message message = (Message) paramPacket;
			Object property = message.getProperty(PangkerConstant.BUSINESS_MESSAGE);
			if (message.getType() == Type.chat && property != null
					&& (property instanceof BusinessType || property instanceof SystemNoticeType)) {
				return true;
			}
		}
		return false;
	}

}
