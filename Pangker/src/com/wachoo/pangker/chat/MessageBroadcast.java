package com.wachoo.pangker.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.group.ChatRoomInfoActivity;
import com.wachoo.pangker.activity.msg.NoticeInfoActivity;
import com.wachoo.pangker.activity.res.PictureInfoActivity;
import com.wachoo.pangker.activity.res.ResInfoActivity;
import com.wachoo.pangker.activity.res.ResSpeaktInfoActivity;
import com.wachoo.pangker.api.TabBroadcastManager;
import com.wachoo.pangker.db.IMsgInfoDao;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.impl.MsgInfoDaoImpl;
import com.wachoo.pangker.db.impl.UserMsgDaoImpl;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.entity.MessageTip;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.NoticeInfo;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.notice.MessageNotifier;
import com.wachoo.pangker.server.response.Commentinfo;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

public class MessageBroadcast {

	private Context context;
	private IMsgInfoDao iMsgInfoDao;
	private IUserMsgDao userMsgDao;

	private MessageChangedManager messageChangedManager;
	private MessageNotification msgNotification;
	private String mUserID;// 当前登陆用户的UID
	private PangkerApplication mApplication;// >>>>
	private SharedPreferencesUtil mUtil;
	private Gson gson = new Gson();
	private MessageNotifier messageNotifier;

	public MessageBroadcast(Context context) {
		super();
		this.context = context;

		iMsgInfoDao = new MsgInfoDaoImpl(context);
		userMsgDao = new UserMsgDaoImpl(context);
		messageChangedManager = PangkerManager.getMessageChangedManager();
		msgNotification = new MessageNotification(context);
		mApplication = (PangkerApplication) context.getApplicationContext();
		mUserID = mApplication.getMyUserID();
		mUtil = new SharedPreferencesUtil(context);
		messageNotifier = new MessageNotifier(mApplication);
	}

	/**
	 * 保存消息，并提示（在会话界面提示未读消息+1 ，如果是自己发的消息不会增加提示）
	 * 
	 * @param msg
	 */
	public void sendNoticeMsg(ChatMessage msg) {
		int count = 1;
		String content = msg.getContent();
		if (ChatMessage.MSG_FROM_ME.equals(msg.getDirection())) {
			count = 0;
			if (msg.getMsgType() == PangkerConstant.RES_VOICE) {
				content = "您给Ta发送了一段语音.";
			}
		} else {
			if (msg.getMsgType() == PangkerConstant.RES_VOICE) {
				content = "Ta给您发送了一段语音.";
			}
		}
		broadcastMsgInfo(msg.getUserId(), msg.getUserName(), MsgInfo.MSG_PERSONAL_CHAT, msg.getMsgType(), content,
				count);
	}

	/**
	 * 只保存消息，不增加未读提示（已经读过消息）
	 * 
	 * @param msg
	 */
	public void sendNoNoticeMsg(ChatMessage msg) {
		broadcastMsgInfo(msg.getUserId(), msg.getUserName(), MsgInfo.MSG_PERSONAL_CHAT, msg.getMsgType(),
				msg.getContent(), 0);
	}

	public void sendNotice(NoticeInfo noticeInfo) {
		String theme = "系统消息";
		if (noticeInfo.getMsgType() == MsgInfo.MSG_APPLY) {
			theme = "申请消息";
		}
		if (noticeInfo.getMsgType() == MsgInfo.MSG_INVITION) {
			theme = "邀请消息";
		}
		TabBroadcastManager broadcastManager = PangkerManager.getTabBroadcastManager();
		Message msg = new Message();
		msg.obj = noticeInfo;
		int count = 0;
		if (!broadcastManager.sendResultMessage(NoticeInfoActivity.class.getName(), msg)) {
			count = 1;
		}
		broadcastMsgInfo(noticeInfo.getOpeUserid(), theme, noticeInfo.getMsgType(), noticeInfo.getNotice(), count);
	}

	public void sendRecommendInfo(RecommendInfo recommendInfo, int count) {
		broadcastMsgInfo(recommendInfo.getFromId(), "推荐消息", MsgInfo.MSG_RECOMMEND,
				"Ta向您推荐：" + recommendInfo.getResName(), count);
	}

	private boolean showNotification(MsgInfo msgInfo) {
		// >>>>>>>>判断是否通知
		for (int messagetype : MsgInfo.NOT_NOTIFICATION_PROMPTS_MESSAGE) {
			if (messagetype == msgInfo.getType()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param fromId
	 *            来自谁的消息
	 * @param theme
	 *            消息主题
	 * @param nType
	 *            消息类型
	 * @param restype
	 *            资源类型
	 * @param nContent
	 *            信息内容
	 * @param count
	 *            消息数量
	 */
	private void broadcastMsgInfo(String fromId, String theme, int nType, int restype, String nContent, int count) {
		MsgInfo msgInfo = new MsgInfo();
		msgInfo.setFromId(fromId);
		msgInfo.setType(nType);
		msgInfo.setContent(nContent);
		msgInfo.setTime(Util.getSysNowTime());
		msgInfo.setTheme(theme);
		msgInfo.setUncount(count);
		msgInfo.setUserId(mUserID);
		msgInfo.setResType(restype);

		long msgId = iMsgInfoDao.saveMsgInfo(msgInfo);
		if (msgId > 0) {
			msgInfo.setMsgId(msgId);
			Log.d("MsgInfo", nContent);
			// >>>>>>>判断是否是自己发送的消息，如果是自己的不需要通知
			if (!msgInfo.getFromId().equals(mUserID) && count > 0) {
				// >>>>>>>>声音通知
				if (isSoundNotice(msgInfo)) {
					messageNotifier.messageSoundNotice();
				}
				// >>>>>>>>震动通知
				if (isVibrateNotice(msgInfo)) {
					messageNotifier.messageVibratorNotice();
				}
			} else {
				// >>>>>>如果是碰一下业务
				if (msgInfo.getResType() == PangkerConstant.RES_TOUCH) {
					messageNotifier.messageVibratorNotice();
				}
			}
			// 发送监听,通知主界面,会话界面，主页粉丝or关注人数量变化消息
			messageChangedManager.chatMessageChanged(new MessageChangedEvent(context, msgInfo));

			try {
				if (count != 0 && showNotification(msgInfo)) { // 发别人的消息和
					int unReadMessageCount = iMsgInfoDao.getUnreadCount(mUserID);
					String notiContent = null;
					if (nType == MsgInfo.MSG_PERSONAL_CHAT) {
						notiContent = theme + "发来一条消息.";
					} else {
						notiContent = theme + ": " + nContent;
					}
					msgNotification.showNotification(null, notiContent, msgInfo, unReadMessageCount);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void broadcastMsgInfo(String fromId, String theme, int nType, String nContent, int count) {
		broadcastMsgInfo(fromId, theme, nType, 0, nContent, count);
	}

	/**
	 * 私聊会话消息功能实现
	 * 
	 * @param msg
	 * @param intent
	 */
	public void sendChatUserMsg(ChatMessage msg, Intent intent) {
		msg.setTime(Util.getSysNowTime()); // 手机当前时间
		// >>>>>>>>私信会话记录保存
		int id = (int) userMsgDao.saveUserMsg(msg);
		if (id > 0) {
			msg.setId(id);// 保存聊天记录到终端db
			if (PangkerManager.getUserMsgManager().chatMessageChanged(msg)) {
				// >> 只保存消息，不增加未读提示（已经读过消息）
				sendNoNoticeMsg(msg);
			} else {
				sendNoticeMsg(msg);
			}
			if (intent != null) {
				if (!PangkerManager.getActivityStackManager().isChatActivityShow(msg.getUserId())) {
					context.startActivity(intent);
				}
			}
		}
	}

	/**
	 * isSoundNotice
	 * 
	 * @param messageInfo
	 * @return
	 */
	private boolean isSoundNotice(MsgInfo messageInfo) {
		// >>>>>>判断是否播声音提示
		if (!mUtil.getBoolean(PangkerConstant.SP_SOUND_SET + mUserID, true)) {
			return false;
		}

		// >>>>>>>>判断是否通知
		for (int messagetype : MsgInfo.NOT_VOICE_PROMPTS_MESSAGE) {
			if (messagetype == messageInfo.getType()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * isVibrateNotice
	 * 
	 * @param messageInfo
	 * @return
	 */
	private boolean isVibrateNotice(MsgInfo messageInfo) {
		// >>>>如果是评一下业务
		if (messageInfo.getResType() == PangkerConstant.RES_TOUCH) {
			return true;
		}
		if (!mUtil.getBoolean(PangkerConstant.SP_SHOCK_SET + mUserID, false)) {
			return false;
		}

		return true;
	}

	public void canceNotify() {
		msgNotification.cancelNotify();
	}

	// >>>>>>>>>群组通知实现代码
	public void sendMeetroom(MeetRoom meetRoom, int uncount) {
		String theme = meetRoom.getGroupname();// 群组名称
		String content = null;
		if (meetRoom.getMsgType() == MeetRoom.s_type_loc) {
			Location location = gson.fromJson(meetRoom.getContent(), Location.class);
			content = meetRoom.getNickname() + ":" + location.getAddress();
		} else {
			content = meetRoom.getNickname() + ":" + meetRoom.getContent();
		}
		broadcastMsgInfo(meetRoom.getGid(), theme, MsgInfo.MSG_GROUP_CHAT, content, uncount);
	}

	// >>>>>>>>>群组通知实现代码
	public void sendMeetroom(UserGroup uGroup) {
		broadcastMsgInfo(uGroup.getGroupId(), uGroup.getGroupName(), MsgInfo.MSG_GROUP_CHAT,
				"您进了群组：" + uGroup.getGroupName(), 0);
	}

	// >>>>>>>>>语音呼叫实现代码
	public ChatMessage sendCallMsg(String fromId, String uName) {
		String nContent = "您语音呼叫了对方。";
		ChatMessage msg = new ChatMessage();
		msg.setTime(Util.getSysNowTime()); // 手机当前时间
		msg.setUserId(fromId); // 目标用户UserID
		msg.setContent(nContent);// 内容
		msg.setUserName(uName);
		msg.setMyUserId(mUserID);// 当前用户userid
		msg.setDirection(ChatMessage.MSG_FROM_ME);// 方向
		msg.setMsgType(PangkerConstant.RES_CALL);
		msg.setMessageRevc(1);
		long messageID = userMsgDao.saveUserMsg(msg);
		msg.setId((int) messageID);
		if (messageID > 0) {
			sendNoNoticeMsg(msg);
			return msg;
		}
		return null;
	}

	public void sendMessageTip(MessageTip messageTip) {
		// TODO Auto-generated method stub

		// >>>>>>>>>>直接显示
		switch (messageTip.getResType()) {
		// >>>>>>群组
		case PangkerConstant.RES_BROADCAST:// >>>>群组资源
			ChatRoomInfoActivity chatRoomInfoActivity = PangkerManager.getActivityStackManager()
					.getChatRoomInfoActivityByGroupID(Util.String2Long(messageTip.getResId()));
			if (chatRoomInfoActivity != null)
				chatRoomInfoActivity.uiComment.addComment(Commentinfo.getCommentInfo(messageTip));
			break;
		case PangkerConstant.RES_PICTURE:// >>>>图片资源
			PictureInfoActivity pictureInfoActivity = PangkerManager.getActivityStackManager()
					.getPicInfoActivityByResID(Util.String2Long(messageTip.getResId()));
			if (pictureInfoActivity != null)
				pictureInfoActivity.uiComment.addComment(Commentinfo.getCommentInfo(messageTip));
			break;
		case PangkerConstant.RES_SPEAK:// >>>>说说资源
			ResSpeaktInfoActivity speaktInfoActivity = PangkerManager.getActivityStackManager()
					.getResSpeaktInfoActivityByResID(Util.String2Long(messageTip.getResId()));
			if (speaktInfoActivity != null)
				speaktInfoActivity.uiComment.addComment(Commentinfo.getCommentInfo(messageTip));
			break;

		case PangkerConstant.RES_MUSIC:// >>>>音乐资源
		case PangkerConstant.RES_APPLICATION:// >>>>应用资源
		case PangkerConstant.RES_DOCUMENT:// >>>>文档资源
			ResInfoActivity resInfoActivity = PangkerManager.getActivityStackManager().getResInfoActivityByResID(
					Util.String2Long(messageTip.getResId()));
			if (resInfoActivity != null)
				resInfoActivity.uiComment.addComment(Commentinfo.getCommentInfo(messageTip));
			break;

		default:
			break;
		}

		broadcastMsgInfo(messageTip.getFromId(), "提示信息", MsgInfo.MSG_TIP, messageTip.getResType(),
				messageTip.getMessagetip(), 1);
	}
}
