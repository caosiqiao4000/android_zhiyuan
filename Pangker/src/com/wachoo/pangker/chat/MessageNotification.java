package com.wachoo.pangker.chat;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.RemoteViews;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.activity.msg.NoticeInfoActivity;
import com.wachoo.pangker.activity.res.ResRecommendActivity;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.ChatMessage;

/**
 * 通知栏消息显示工具类
 * 
 * @author caosq
 * 
 */
public class MessageNotification {
	private Context context;
	private NotificationManager manager;
	private Notification notification;
	private RemoteViews remoteView;
	private PendingIntent contentIntent;
	private Intent toIntent;

	/** 取消私信的通知栏 */
	public static final int NOTIFICATION_FLAG = 0xAAA;
	private Handler myHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			try {
				if (msg.what == 1) {
					NotifyInfo notifyInfo = (NotifyInfo) msg.obj;

					// >>>>>>>>>>通知栏显示内容
					remoteView.setTextViewText(R.id.msg_title, notifyInfo.getTickerTitle());
					remoteView.setTextViewText(R.id.msg_text, notifyInfo.getTickerText());

					// >>>>>>>通知
					notification.icon = notifyInfo.getIcon(); // >>>>>>>>通知图标
					notification.when = System.currentTimeMillis(); // >>>>>>>通知时间
					notification.tickerText = notifyInfo.getTickerText();// >>>>>>>通知内容
					int unReadMessageCount = notifyInfo.getUnReadMessageCount();// >>>>>>>>>>>通知总条数
					if (isMsgChatActivity(notifyInfo.getFromId())) {
						toIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					} else {
						toIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					}
					contentIntent = PendingIntent.getActivity(context, 0, toIntent, PendingIntent.FLAG_UPDATE_CURRENT);
					notification.contentIntent = contentIntent;

					// >>>>>>>>>>从db中获取未读信息条数
					notification.setLatestEventInfo(context, notifyInfo.getTickerTitle(), "你有" + unReadMessageCount
							+ "未读消息", contentIntent);
					manager.notify(NOTIFICATION_FLAG, notification);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
	};

	private boolean isMsgChatActivity(String userId) {
		if (toIntent != null && toIntent.getComponent().getClassName().contains("MsgChatActivity")) {
			if (PangkerManager.getActivityStackManager().isChatActivityShow(userId)) {
				return false;
			}
			return true;
		}
		return false;
	}

	public void cancelNotify() {
		// TODO Auto-generated method stub
		manager.cancel(MessageNotification.NOTIFICATION_FLAG);
	}

	public MessageNotification(Context context) {
		this.context = context;
		manager = (NotificationManager) this.context.getSystemService(Context.NOTIFICATION_SERVICE);
		notification = new Notification(R.drawable.icon, "", System.currentTimeMillis());

		notification.flags = Notification.FLAG_AUTO_CANCEL;

		// 自定义通知栏列项的布局
		remoteView = new RemoteViews(context.getPackageName(), R.layout.custom_notification);
		notification.contentView = remoteView;
	}

	/**
	 * Show a notification while this service is running.
	 * 
	 * @param icon
	 *            显示图标 资源ID
	 * @param text
	 *            通知的内容
	 * @param msgInfo
	 *            判断跳到哪个页面 暂时加入通知栏任务栏里
	 */
	public void showNotification(Integer icon, CharSequence tickerText, MsgInfo msgInfo, int unCount) {
		if (icon == null) {
			icon = R.drawable.title_message_prompt;
		}
		toIntent = getToIntent(msgInfo);

		// Set the icon, scrolling text and timestamp
		Message msgMessage = myHandler.obtainMessage();
		// >>>>>>>>通知对象
		NotifyInfo notifyInfo = new NotifyInfo("私信通知", tickerText.toString(), icon, unCount, msgInfo.getFromId());

		msgMessage.what = 1;
		msgMessage.obj = notifyInfo;
		myHandler.sendMessage(msgMessage);
	}

	/**
	 * 通知对象
	 * 
	 * @author wangxin
	 * 
	 */
	class NotifyInfo {

		private String fromId;
		private String tickerTitle;
		private String tickerText;
		private Integer icon;
		private int unReadMessageCount;

		public NotifyInfo(String tickerTitle, String tickerText, Integer icon, int unReadMessageCount, String fromId) {
			super();
			this.tickerTitle = tickerTitle;
			this.tickerText = tickerText;
			this.icon = icon;
			this.unReadMessageCount = unReadMessageCount;
			this.fromId = fromId;

		}

		public String getTickerTitle() {
			return tickerTitle;
		}

		public void setTickerTitle(String tickerTitle) {
			this.tickerTitle = tickerTitle;
		}

		public String getTickerText() {
			return tickerText;
		}

		public void setTickerText(String tickerText) {
			this.tickerText = tickerText;
		}

		public Integer getIcon() {
			return icon;
		}

		public void setIcon(Integer icon) {
			this.icon = icon;
		}

		public int getUnReadMessageCount() {
			return unReadMessageCount;
		}

		public void setUnReadMessageCount(int unReadMessageCount) {
			this.unReadMessageCount = unReadMessageCount;
		}

		public String getFromId() {
			return fromId;
		}

		public void setFromId(String fromId) {
			this.fromId = fromId;
		}
	}

	private Intent getToIntent(MsgInfo msgInfo) {
		try {
			Intent intent = null;
			if (msgInfo.getType() == MsgInfo.MSG_RECOMMEND) {
				intent = new Intent(context, ResRecommendActivity.class);
				intent.putExtra("FROM_ID", msgInfo.getFromId());
			}
			if (msgInfo.getType() == MsgInfo.MSG_NOTICE || msgInfo.getType() == MsgInfo.MSG_APPLY
					|| msgInfo.getType() == MsgInfo.MSG_INVITION) {
				intent = new Intent(context, NoticeInfoActivity.class);
				intent.putExtra("NOTICE_TYPE", msgInfo.getType());
				intent.putExtra("FROM_ID", msgInfo.getFromId());
			}
			if (msgInfo.getType() == MsgInfo.MSG_PERSONAL_CHAT) {
				intent = new Intent(context, MsgChatActivity.class);
				intent.putExtra(ChatMessage.USERID, msgInfo.getFromId());
				intent.putExtra(ChatMessage.USERNAME, msgInfo.getTheme());
			}
			if (msgInfo.getType() == MsgInfo.MSG_GROUP_CHAT) { // 进入群组
				// 不处理 直接进入 ChatRoomMainActivity 类
				intent = new Intent(context, ChatRoomMainActivity.class);
			}
			return intent;
		} catch (Exception e) {
			e.printStackTrace();
			return new Intent(context, MsgChatActivity.class);
		}
	}

}
