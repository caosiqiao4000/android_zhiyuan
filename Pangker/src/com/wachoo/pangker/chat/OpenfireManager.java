package com.wachoo.pangker.chat;

import java.io.File;
import java.util.Iterator;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.OfflineMessageManager;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.bytestreams.ibb.provider.CloseIQProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.DataPacketProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.OpenIQProvider;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.filetransfer.FileTransfer;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.search.UserSearch;

import android.os.Build;
import android.util.Log;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.pangker.xmpp.SystemNotice.SystemNoticeType;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.entity.MessageTip;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.AddressMembers;
import com.wachoo.pangker.util.Util;

/**
 * xmpp IM通讯
 * 
 * @author wangxin
 * 
 */
public class OpenfireManager {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	private static final String tag = "OpenfireManager";
	private ConnectionConfiguration conConf;
	private XMPPConnection connection;

	public void setConnection(XMPPConnection connection) {
		this.connection = connection;
	}

	public XMPPConnection getConnection() {
		return connection;
	}

	public static final String APP_NAME = "pangker";

	/**
	 * 连接到xmpp服务器
	 * 
	 * @return
	 */
	public OpenfireManager() {

	}

	/**
	 * initOpenFireManager
	 * 
	 * @return
	 * 
	 * @author wangxin
	 * @throws XMPPException
	 * @date 2012-3-7 上午11:14:48
	 */
	private void initOpenFireManager() throws XMPPException {
		// TODO Auto-generated method stub
		configure(ProviderManager.getInstance());
		XMPPConnection.DEBUG_ENABLED = true;
		conConf = null;
		conConf = new ConnectionConfiguration(Configuration.getChatServer(), Configuration.getChatPort(),
				Configuration.getChatDomain());
		//
		// conConf = new ConnectionConfiguration("192.168.3.88",
		// Configuration.getChatPort(),
		// Configuration.getChatDomain());
		conConf.setSASLAuthenticationEnabled(true);

		if (Build.VERSION.SDK_INT >= 14) {
			conConf.setTruststoreType("AndroidCAStore");
			conConf.setTruststorePassword(null);
			conConf.setTruststorePath(null);
		} else {
			conConf.setTruststoreType("BKS");
			String path = System.getProperty("javax.net.ssl.trustStore");
			if (path == null)
				path = System.getProperty("java.home") + File.separator + "etc" + File.separator + "security"
						+ File.separator + "cacerts.bks";
			conConf.setTruststorePath(path);
		}

		conConf.setReconnectionAllowed(true);
		conConf.setSecurityMode(SecurityMode.disabled);

		connection = new XMPPConnection(conConf);

	}

	public boolean connectOpenfire() {
		// 根据之前的配置，连接服务器
		try {
			initOpenFireManager();
			connection.connect();
			initFeatures();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 
	 * @author wangxin 2012-3-13 上午11:17:08
	 */
	private void initFeatures() {
		if (isConnected()) {
			initFeatures(connection);
		}
	}

	/**
	 * 登出 login out *
	 * 
	 * @author wangxin 2012-2-21 下午05:50:30
	 */
	public void loginOut() {
		try {
			if (connection != null) {
				connection.disconnect();
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.error(TAG, e);
		} finally {
			connection = null;
		}
	}

	private String XMPPRegisterError;

	public String getXMPPRegisterError() {
		return XMPPRegisterError;
	}

	public void close() {

		conConf = null;
		connection = null;

	}

	/**
	 * 登录到xmpp服务器
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws XMPPException
	 */
	public boolean Login(String userid, String password) throws XMPPException {
		if (!isConnected()) {
			return false;
		}
		if (userid != null && userid.length() > 0 && password != null && password.length() > 0) {
			// 登录
			connection.login(userid, password, PangkerConstant.PANGKER_RESOURCE);

		} else {
			return false;
		}
		return true;
	}

	/**
	 * 判断xmpp服务器是否连通 Returns true if currently connected to the XMPP server
	 * 
	 * @return
	 */
	public boolean isConnected() {
		if (connection != null && connection.isConnected()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param username
	 * @throws XMPPException
	 */
	public void removeEntry(String userid) {
		final String fullJid = Util.addUserIDDomain(userid);
		if (!isLoggedIn()) {
			return;
		}

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					RosterEntry entryFull = connection.getRoster().getEntry(fullJid);
					RosterEntry entry = connection.getRoster().getEntry(Util.trimUserIDDomain(fullJid));
					if (entry != null)
						connection.getRoster().removeEntry(entry);
					else if (entryFull != null)
						connection.getRoster().removeEntry(entryFull);
				} catch (Exception e) {
					// TODO: handle exception
					logger.error(TAG, e);
				}
			}
		}).start();

	}

	/**
	 * 拒绝对方的好友请求
	 * 
	 * @param userID
	 */
	public void refuseFriendRequest(String userID) {
		final String fullJid = Util.addUserIDDomain(userID);
		if (!isLoggedIn()) {
			return;
		}
		Presence subscription = new Presence(Presence.Type.unsubscribe);
		subscription.setTo(fullJid);
		connection.sendPacket(subscription);
	}

	public boolean createEntry(String user, final String name, final String[] groups) {
		if (!isLoggedIn()) {
			return false;
		}
		final String fullJid = Util.addUserIDDomain(user);
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				try {

					connection.getRoster().createEntry(fullJid, name, groups);
				} catch (Exception e) {
					// TODO: handle exception
					logger.error(TAG, e);

				}
			}
		}).start();

		return true;
	}

	public void setFriendRemark(String user, String RemarkName) {
		if (!isLoggedIn()) {
			return;
		}
		try {
			connection.getRoster().getEntry(user + "@pangker").setName(RemarkName);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(TAG, e);
		}
	}

	/**
	 * 添加好友请求
	 * 
	 * @param username
	 */
	public void sendPacketAdd(String userid) {
		if (!isLoggedIn()) {
			return;
		}
		Presence pre = new Presence(Presence.Type.subscribe);
		pre.setTo(userid);
		// pre.setFrom(connection.getUser());
		connection.sendPacket(pre);
	}

	/**
	 * 添加好友请求
	 * 
	 * @param username
	 */
	public void sendPacketAdded(String userid) {
		if (!isLoggedIn()) {
			return;
		}
		Presence pre = new Presence(Presence.Type.subscribed);
		pre.setTo(userid);
		connection.sendPacket(pre);
	}

	// public void sendPacketDeled(String userid) {
	// if (!isLoggedIn()) {
	// return;
	// }
	// Presence pre = new Presence(Presence.Type.unsubscribed);
	// pre.setTo(userid);
	// // pre.setFrom(connection.getUser());
	// connection.sendPacket(pre);
	// }

	/**
	 * Returns true if currently authenticated by successfully calling the login
	 * method.
	 * 
	 * @return
	 */
	public boolean isLoggedIn() {
		if (isConnected() && connection.isAuthenticated()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return
	 */
	public String getConncetionID() {
		if (isConnected()) {
			return connection.getConnectionID();
		} else {
			return "";
		}
	}

	public void sendIQ(String peerUserID, String msg) {
		PangkerIQ iq = new PangkerIQ();
		iq.setBusinessID("1");
		iq.setTo("peerUser");
		if (isLoggedIn()) {
			connection.sendPacket(iq); // 发送
		}
	}

	// 发消息例程
	// peerUserID：对方用户的ID
	// msg：待发消息的内容
	public void sendMsg(String peerUserID, String msg, String icon, String fromUserNickname) {
		if (!isLoggedIn()) {
			return;
		}
		final Message oMsg = new Message(peerUserID, Message.Type.chat);

		oMsg.setBody(msg);
		oMsg.setPacketID(APP_NAME); // 应用ID标识，必须填上
		oMsg.setProperty("from_icon", icon);
		oMsg.setProperty("from_nickname", fromUserNickname);
		if (isLoggedIn()) {
			connection.sendPacket(oMsg); // 发送
		}
	}

	/**
	 * 
	 * @param message
	 * @author wangxin 2012-3-12 下午12:04:34
	 */
	public void sendChatMessage(Message message) {
		if (isLoggedIn()) {
			message.setPacketID(APP_NAME); // 应用ID标识，必须填上
			connection.sendPacket(message);
		}
	}

	/**
	 * 
	 * @param message
	 * @author wangxin 2012-3-12 下午12:04:34
	 */
	public void sendChatMessage(ChatMessage chatMessage) {
		final Message message = new Message(Util.addUserIDDomain(chatMessage.getUserId()));
		message.setType(Type.chat);
		message.setFrom(chatMessage.getMyUserId());
		message.setProperty("messagetype", chatMessage.getMsgType());
		if (chatMessage.getFilepath() != null) {
			message.setProperty("filepath", chatMessage.getFilepath());
		}
		// >>>>>>>>根据此判断是否为有效Message
		if (chatMessage.getId() > 0) {
			message.addExtension(new MessageRecivedPacketExtension(chatMessage.getId()));
			Log.i("MessageReceived", "send-messageID=" + chatMessage.getId());
		}
		message.setBody(chatMessage.getContent());
		sendChatMessage(message);
	}

	// 取用户的离线消息例程
	public Iterator<Message> getUserOfflineMsg() {
		if (!isLoggedIn()) {
			return null;
		}
		// 实例化离线消息管理器
		OfflineMessageManager offMsgManager = new OfflineMessageManager(connection);
		try {
			// 取属于该用户的所有离线消息
			return offMsgManager.getMessages();
		} catch (XMPPException e) {
			logger.error(TAG, e);
		}
		return null;
	}

	/**
	 * Adds a listener to this roster. The listener will be fired anytime one or
	 * more changes to the roster are pushed from the server. 启动监听
	 */
	public void startRosterListener(RosterListener rosterListener) {
		if (isLoggedIn()) {
			connection.getRoster().addRosterListener(rosterListener);
		}
	}

	/**
	 * Removes a listener from this roster. The listener will be fired anytime
	 * one or more changes to the roster are pushed from the server. 停止监听
	 */
	public void stopRosterListener(RosterListener rosterListener) {
		if (isLoggedIn()) {
			connection.getRoster().removeRosterListener(rosterListener);
		}
	}

	/**
	 * 
	 * @param roster
	 * @param userName
	 * @return
	 * 
	 * @author wangxin 2012-2-20 上午10:00:24
	 */
	public boolean removefriend(Roster roster, String userName) {
		try {
			userName = Util.trimUserIDDomain(userName);
			RosterEntry entry = roster.getEntry(userName);
			userName = Util.addUserIDDomain(userName);
			RosterEntry entryFull = roster.getEntry(userName);
			if (entry != null) {
				roster.removeEntry(entry);
			} else if (entryFull != null) {
				roster.removeEntry(entryFull);
			} else {
				return false;
			}

		} catch (XMPPException e) {
			// TODO Auto-generated catch block
			// logger.error(TAG, e);
			logger.error(TAG, e);
			return false;
		}
		return true;
	}

	// >>>>>>> wangxin 联系人变化通知
	public void sendContactsChangedPacket(String uid, String ruid, BusinessType changetype) {
		if (isLoggedIn()) {
			Message message = new Message(Util.addUserIDDomain(ruid));
			message.setFrom(Util.addUserIDDomain(uid));
			message.setType(Type.chat);
			message.setBody("sendContactsChangedPacket");
			message.setPacketID(APP_NAME);
			message.setProperty(PangkerConstant.BUSINESS_MESSAGE, changetype);
			connection.sendPacket(message);
		}

	}

	// >>>>>>>>> wangxin 本地注册后通知通讯录好友
	public void sendReigsterPacket(String uid, String ruid, String phoneNum, String msg) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(ruid));
		message.setPacketID(APP_NAME);
		message.setType(Type.chat);
		if (Util.isEmpty(msg))
			msg = " ";
		message.setBody(msg);// 可不填
		message.setProperty("RUserid", uid);// 注册者Uid
		message.setProperty("BUSINESS_MESSAGE", SystemNoticeType.contacts_register);
		message.setProperty("CONTACTS_CHANGED", phoneNum);// 注册者号码
		connection.sendPacket(message);
	}

	// >>>>>>>>>> wangxin 群组邀请请求 start
	public void sendGroupInvite(String uid, String inviteUid, String msg) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(inviteUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setType(Type.chat);
		message.setBody(msg);
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.groupInvite);
		connection.sendPacket(message);
	}

	public void sendGroupResponse(String uid, String inviteUid, String result, String msg) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(inviteUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setType(Type.chat);
		message.setBody(msg);
		message.setProperty("result", result);
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.groupResponse);
		connection.sendPacket(message);
	}

	// >>>>>>>>>> wangxin 网络通讯录 start
	// 邀请某用户加入自己的网络通讯录
	public void sendNetAddressBookInviteReq(String uid, String inviteUid, AddressBooks addressBooks, String msg) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(inviteUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setType(Type.chat);
		message.setBody(msg);
		message.setProperty(AddressBooks.BOOK_KEY, addressBooks);
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.inviteNetAddressbookReq);
		connection.sendPacket(message);
	}

	// 对方给的答复
	public void sendNetAddressBookInviteResp(String uid, String inviteUid, AddressMembers member, String result,
			String msg) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(inviteUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setType(Type.chat);
		message.setBody(msg);
		message.setProperty("result", result);
		message.setProperty(AddressMembers.ADDRESSMEMBER_KEY, member);
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.inviteNetAddressbookResp);
		connection.sendPacket(message);
	}

	// 申请加入网络通讯录
	public void sendNetAddressBookApplyReq(String uid, String inviteUid, AddressBooks addressBooks, String msg) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(inviteUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setType(Type.chat);
		message.setBody(msg);
		message.setProperty(AddressBooks.BOOK_KEY, addressBooks);
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.applyNetAddressBookReq);
		connection.sendPacket(message);
	}

	/**
	 * 申请加入网络通讯录,管理员答复
	 * 
	 * @param uid
	 *            :发送人Id
	 * @param inviteUid
	 *            邀请人的Id
	 * @param result
	 *            1：同意；0：拒绝
	 * @param addressBooks
	 * @param msg
	 *            返回消息
	 */
	public void sendNetAddressBookApplyResp(String uid, String inviteUid, String result, AddressBooks addressBooks,
			String msg) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(inviteUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setType(Type.chat);
		message.setBody(msg);
		message.setProperty("result", result);
		message.setProperty(AddressBooks.BOOK_KEY, addressBooks);
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.applyNetAddressBookResp);
		connection.sendPacket(message);
	}

	// 被管理员删除
	public void sendBeDelNetAddressBook(String uid, String inviteUid, String bookId, String result) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(inviteUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setBody("beDel");
		message.setType(Type.chat);
		message.setSubject(bookId);
		message.setProperty("result", result);
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.beDelNetAddressBook);
		connection.sendPacket(message);
	}

	// 网络通讯录通知
	public void sendNetAddressBook(String uid, String inviteUid, String result, String msg, AddressBooks addressBooks,
			BusinessType businessType) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(inviteUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setType(Type.chat);
		message.setBody(msg);
		message.setProperty("result", result);
		message.setProperty(AddressBooks.BOOK_KEY, addressBooks);
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, businessType);
		connection.sendPacket(message);
	}

	/**
	 * 碰一下对方通过openfire通知
	 * 
	 * @param uid
	 * @param touchUid
	 * @param messageID
	 */
	public void sendTouchMessage(String uid, String touchUid, long messageID) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(touchUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setType(Type.chat);
		message.setBody("touch");
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.touch);
		message.addExtension(new MessageRecivedPacketExtension(messageID));
		connection.sendPacket(message);

	}

	/**
	 * 来自客户端对客户端通知
	 * 
	 * @param uid
	 * @param inviteUid
	 * @param type
	 *            类别
	 * @param resId资源id
	 * @param resName资源名称
	 * @param msg理由
	 */
	public void sendResRecommend(String uid, String inviteUid, int type, String resId, String resName, String singer,
			String msg, String shareUid) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(inviteUid));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(uid));
		message.setType(Type.chat);
		if (Util.isEmpty(msg))
			msg = "  ";
		message.setBody(msg);
		message.setSubject(resName);
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.resRecommend);
		message.setProperty("type", type);
		message.setProperty("result", resId);
		message.setProperty("shareUid", shareUid);
		if (singer != null)
			message.setProperty("singer", singer);

		connection.sendPacket(message);
	}

	/**
	 * 来自客户端对客户端通知
	 * 
	 * @param uid
	 * @param inviteUid
	 * @param type
	 *            类别
	 * @param resId资源id
	 * @param resName资源名称
	 * @param msg理由
	 */
	public void sendMessageTip(MessageTip messageTip) {
		if (!isLoggedIn())
			return;
		Message message = new Message(Util.addUserIDDomain(messageTip.getUserId()));
		message.setPacketID(APP_NAME);
		message.setFrom(Util.addUserIDDomain(messageTip.getFromId()));
		message.setType(Type.chat);
		if (Util.isEmpty(messageTip.getContent()))
			messageTip.setContent("  ");
		message.setBody(messageTip.getContent());
		message.setSubject(messageTip.getResName());
		message.setProperty(PangkerConstant.BUSINESS_MESSAGE, BusinessType.messagetip);

		/**
		 * RES_DOCUMENT = 11; // 资源——文档 RES_MUSIC = 13; // 资源——音乐 RES_PICTURE =
		 * 12;// 资源——图片 RES_APPLICATION = 14;// 资源——应用 RES_BROADCAST = 15;//
		 * 资源——直播
		 * 
		 * RES_BROADCAST = 99;//留言的回复
		 */
		message.setProperty(OPENFIRE_NOTICE_FIELD_RESTYPE, messageTip.getResType());
		message.setProperty(OPENFIRE_NOTICE_FIELD_RESID, messageTip.getResId());
		message.setProperty(OPENFIRE_TIP_TYPE, messageTip.getTipType());
		message.setProperty(OPENFIRE_TIP_COMMENTID, messageTip.getCommentID());
		Log.d(tag, "to=>userId:::" + Util.addUserIDDomain(messageTip.getUserId()));
		connection.sendPacket(message);
	}

	// /**
	// * 来自后台系统通知定义
	// *
	// * @param uid
	// * 被邀请者注册Id
	// * @param inviteUid
	// * 返回对象
	// * @param type
	// * 类别
	// * @param resId资源id
	// * @param resName资源名称
	// * @param msg理由
	// */
	// public void sendResRecommendFromSystem(String uid, String inviteUid, int
	// type, String resId,
	// String resName, String msg) {
	// if (!isLoggedIn())
	// return;
	// Message message = new Message(Util.addUserIDDomain(inviteUid));
	// message.setPacketID(APP_NAME);
	// message.setType(Type.chat);
	// message.setBody(msg);
	// message.setProperty("RUserid", uid);
	// message.setProperty("resId", resId);
	// message.setSubject(resName);
	// message.setProperty("type", type);
	// message.setProperty(PangkerConstant.BUSINESS_MESSAGE,
	// SystemNoticeType.resRecommend);
	// connection.sendPacket(message);
	// }
	//
	// /**
	// * TODO 亲友邀请通知（系统通知）
	// *
	// * @param uid
	// * 被邀请者注册Id
	// * @param inviteUid
	// * 返回对象
	// * @param mobile
	// * 被邀请者手机号
	// * @param msg
	// * 返回消息
	// */
	// public void sendRelativeRecommend(String uid, String inviteUid, String
	// mobile, String msg) {
	// if (!isLoggedIn())
	// return;
	// Message message = new Message(Util.addUserIDDomain(inviteUid));
	// message.setPacketID(APP_NAME);
	// message.setProperty("RUserid", uid);
	// message.setType(Type.chat);
	// message.setBody(msg);// 可不填
	// message.setSubject(mobile);
	// message.setProperty(PangkerConstant.BUSINESS_MESSAGE,
	// SystemNoticeType.traceRelativeRecommend);
	// connection.sendPacket(message);
	// }

	public void sendFile(String toUid, File file) throws XMPPException, InterruptedException {

		FileTransferManager manager = new FileTransferManager(connection);
		OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(Util.addUserIDDomain(toUid));
		long timeOut = 1000000;
		long sleepMin = 3000;
		long spTime = 0;
		int rs = 0;

		transfer.sendFile(file, "pls re file!");
		rs = transfer.getStatus().compareTo(FileTransfer.Status.complete);
		while (rs != 0) {
			rs = transfer.getStatus().compareTo(FileTransfer.Status.complete);
			spTime = spTime + sleepMin;
			if (spTime > timeOut) {
				return;
			}
			Log.d("transfer", "pro--->" + transfer.getProgress());
			Thread.sleep(sleepMin);
		}
	}

	// >>>>>>>>>资源id
	public static final String OPENFIRE_NOTICE_FIELD_RESID = "RESID";
	// >>>>>>>>>资源类型
	public static final String OPENFIRE_NOTICE_FIELD_RESTYPE = "RESTYPE";
	// >>>>>>>>>信息提示类型
	public static final String OPENFIRE_TIP_TYPE = "TIPTYPE";
	// >>>>>>>>>评论id
	public static final String OPENFIRE_TIP_COMMENTID = "COMMENTID";

	// >>>>>>>>>> wangxin 网络通讯录 end

	// 各种类型的请求
	public static enum BusinessType {
		addfriend, // 添加好友
		delfriend, // 删除
		addattent, delattent, addfans, delfans, contacts_register, // 通讯录好友注册通知
		// 群组邀请
		groupInvite, groupResponse,
		// 友踪
		inviteDriftUserRequest, inviteDriftUserResponse,
		// >>>>>>>单位通讯录
		inviteNetAddressbookReq, inviteNetAddressbookResp, applyNetAddressBookReq, applyNetAddressBookResp, beDelNetAddressBook, applyExitNetAddressBook, applyExitNetAddressBookAgree, touch, // 碰一下
		error, resRecommend,
		// 资源推荐/亲友推荐
		// >>>>>>>>消息通知
		messagetip
	}

	/**
	 * 上线
	 * 
	 * @param connection
	 */
	public void updateStateToAvailable(XMPPConnection connection) {
		Presence presence = new Presence(Presence.Type.available);
		connection.sendPacket(presence);
	}

	/**
	 * 下线
	 * 
	 * @param connection
	 */
	public void updateStateToUnAvailable(XMPPConnection connection) {
		Presence presence = new Presence(Presence.Type.unavailable);
		connection.sendPacket(presence);
	}

	/**
	 * 对某个用户设置上线
	 * 
	 * @param connection
	 * @param userName
	 */
	public void updateStateToUnAvailableToSomeone(XMPPConnection connection, String userName) {
		Presence presence = new Presence(Presence.Type.unavailable);
		presence.setTo(userName);
		connection.sendPacket(presence);
	}

	/**
	 * 对某个用户设置下线
	 * 
	 * @param connection
	 * @param userName
	 */
	public void updateStateToAvailableToSomeone(XMPPConnection connection, String userName) {
		Presence presence = new Presence(Presence.Type.available);
		presence.setTo(userName);
		connection.sendPacket(presence);

	}

	/**
	 * 修改心情
	 * 
	 * @param connection
	 * @param status
	 */
	public void changeStateMessage(XMPPConnection connection, String status) {
		Presence presence = new Presence(Presence.Type.available);
		presence.setStatus(status);
		connection.sendPacket(presence);

	}

	private static void initFeatures(XMPPConnection xmppConnection) {
		ServiceDiscoveryManager.setIdentityName("Android_IM");
		ServiceDiscoveryManager.setIdentityType("phone");
		ServiceDiscoveryManager sdm = ServiceDiscoveryManager.getInstanceFor(xmppConnection);
		if (sdm == null) {
			sdm = new ServiceDiscoveryManager(xmppConnection);
		}
		sdm.addFeature("http://jabber.org/protocol/disco#info");
		sdm.addFeature("http://jabber.org/protocol/caps");
		sdm.addFeature("urn:xmpp:avatar:metadata");
		sdm.addFeature("urn:xmpp:avatar:metadata+notify");
		sdm.addFeature("urn:xmpp:avatar:data");
		sdm.addFeature("http://jabber.org/protocol/nick");
		sdm.addFeature("http://jabber.org/protocol/nick+notify");
		sdm.addFeature("http://jabber.org/protocol/xhtml-im");
		sdm.addFeature("http://jabber.org/protocol/muc");
		sdm.addFeature("http://jabber.org/protocol/commands");
		sdm.addFeature("http://jabber.org/protocol/si/profile/file-transfer");
		sdm.addFeature("http://jabber.org/protocol/si");
		sdm.addFeature("http://jabber.org/protocol/bytestreams");
		sdm.addFeature("http://jabber.org/protocol/ibb");
		sdm.addFeature("http://jabber.org/protocol/feature-neg");
		sdm.addFeature("jabber:iq:privacy");
	}

	// 解决aSmack IQ扩展的问题，必须
	private void configure(ProviderManager pm) {

		pm.addIQProvider("rootName", "ns", new PangkerIQProvider());

		// Private Data Storage
		pm.addIQProvider("query", "jabber:iq:private", new PrivateDataManager.PrivateDataIQProvider());
		// Time
		try {
			pm.addIQProvider("query", "jabber:iq:time", Class.forName("org.jivesoftware.smackx.packet.Time"));
		} catch (ClassNotFoundException e) {
			logger.error(TAG, e);
		}
		// Roster Exchange
		pm.addExtensionProvider("x", "jabber:x:roster", new RosterExchangeProvider());
		// Message Events
		pm.addExtensionProvider("x", "jabber:x:event", new MessageEventProvider());
		// Chat State
		pm.addExtensionProvider("active", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
		pm.addExtensionProvider("composing", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
		pm.addExtensionProvider("paused", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
		pm.addExtensionProvider("inactive", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
		pm.addExtensionProvider("gone", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
		// XHTML
		pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im", new XHTMLExtensionProvider());
		// Group Chat Invitations
		pm.addExtensionProvider("x", "jabber:x:conference", new GroupChatInvitation.Provider());
		// Service Discovery # Items
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());
		// Service Discovery # Info
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());
		// Data Forms
		pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());
		// MUC User
		pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user", new MUCUserProvider());
		// MUC Admin
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin", new MUCAdminProvider());
		// MUC Owner
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner", new MUCOwnerProvider());
		// Delayed Delivery
		pm.addExtensionProvider("x", "jabber:x:delay", new DelayInformationProvider());
		// Version
		try {
			pm.addIQProvider("query", "jabber:iq:version", Class.forName("org.jivesoftware.smackx.packet.Version"));
		} catch (ClassNotFoundException e) {
			logger.error(TAG, e);
		}
		// VCard
		pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());
		// Offline Message Requests
		pm.addIQProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());
		// Offline Message Indicator
		pm.addExtensionProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());
		// Last Activity
		pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());
		// User Search
		pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());
		// SharedGroupsInfo
		pm.addIQProvider("sharedgroup", "http://www.jivesoftware.org/protocol/sharedgroup",
				new SharedGroupsInfo.Provider());
		// JEP-33: Extended Stanza Addressing
		pm.addExtensionProvider("addresses", "http://jabber.org/protocol/address", new MultipleAddressesProvider());
		// FileTransfer
		// pm.addIQProvider("si","http://jabber.org/protocol/si", new
		// StreamInitiationProvider());
		// pm.addIQProvider("query","http://jabber.org/protocol/bytestreams",
		// new BytestreamsProvider());
		// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
		// IBBProviders.Open());
		// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
		// IBBProviders.Close());
		// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb", new
		// IBBProviders.Data());

		pm.addIQProvider("si", "http://jabber.org/protocol/si", new StreamInitiationProvider());
		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams", new BytestreamsProvider());

		// Privacy
		pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());
		// Private Data Storage
		pm.addIQProvider("query", "jabber:iq:private", new PrivateDataManager.PrivateDataIQProvider());

		// Time
		try {
			pm.addIQProvider("query", "jabber:iq:time", Class.forName("org.jivesoftware.smackx.packet.Time"));
		} catch (ClassNotFoundException e) {
			logger.error(TAG, e);
			Log.w("TestClient", "Can't load class for org.jivesoftware.smackx.packet.Time");
		}

		// Roster Exchange
		pm.addExtensionProvider("x", "jabber:x:roster", new RosterExchangeProvider());

		// Message Events
		pm.addExtensionProvider("x", "jabber:x:event", new MessageEventProvider());

		// Chat State
		pm.addExtensionProvider("active", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
		pm.addExtensionProvider("composing", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
		pm.addExtensionProvider("paused", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
		pm.addExtensionProvider("inactive", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
		pm.addExtensionProvider("gone", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());

		// XHTML
		pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im", new XHTMLExtensionProvider());

		// Group Chat Invitations
		pm.addExtensionProvider("x", "jabber:x:conference", new GroupChatInvitation.Provider());

		// Service Discovery # Items
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());

		// Service Discovery # Info
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());

		// Data Forms
		pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());

		// MUC User
		pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user", new MUCUserProvider());

		// MUC Admin
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin", new MUCAdminProvider());

		// MUC Owner
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner", new MUCOwnerProvider());

		// Delayed Delivery
		pm.addExtensionProvider("x", "jabber:x:delay", new DelayInformationProvider());

		// Version
		try {
			pm.addIQProvider("query", "jabber:iq:version", Class.forName("org.jivesoftware.smackx.packet.Version"));
		} catch (ClassNotFoundException e) {
			logger.error(TAG, e);
		}

		// VCard
		pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

		// Offline Message Requests
		pm.addIQProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());

		// Offline Message Indicator
		pm.addExtensionProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());

		// Last Activity
		pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());

		// User Search
		pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());

		// SharedGroupsInfo
		pm.addIQProvider("sharedgroup", "http://www.jivesoftware.org/protocol/sharedgroup",
				new SharedGroupsInfo.Provider());

		// JEP-33: Extended Stanza Addressing
		pm.addExtensionProvider("addresses", "http://jabber.org/protocol/address", new MultipleAddressesProvider());

		// FileTransfer
		pm.addIQProvider("si", "http://jabber.org/protocol/si", new StreamInitiationProvider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams", new BytestreamsProvider());
		pm.addIQProvider("open", "http://jabber.org/protocol/ibb", new OpenIQProvider());
		pm.addIQProvider("close", "http://jabber.org/protocol/ibb", new CloseIQProvider());
		pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb", new DataPacketProvider());

		// Privacy
		pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());
		// pm.addIQProvider("command", "http://jabber.org/protocol/commands",
		// new AdHocCommandDataProvider());
		// pm.addExtensionProvider("malformed-action",
		// "http://jabber.org/protocol/commands", new
		// AdHocCommandDataProvider.MalformedActionError());
		// pm.addExtensionProvider("bad-locale",
		// "http://jabber.org/protocol/commands", new
		// AdHocCommandDataProvider.BadLocaleError());
		// pm.addExtensionProvider("bad-payload",
		// "http://jabber.org/protocol/commands", new
		// AdHocCommandDataProvider.BadPayloadError());
		// pm.addExtensionProvider("bad-sessionid",
		// "http://jabber.org/protocol/commands", new
		// AdHocCommandDataProvider.BadSessionIDError());
		// pm.addExtensionProvider("session-expired",
		// "http://jabber.org/protocol/commands", new
		// AdHocCommandDataProvider.SessionExpiredError());}
		// pm.addIQProvider("vCard", "vcard-temp", new
		// org.jivesoftware.smackx.provider.VCardProvider());

		/*
		 * pm.addIQProvider("command", "http://jabber.org/protocol/commands",
		 * new AdHocCommandDataProvider());
		 * pm.addExtensionProvider("malformed-action",
		 * "http://jabber.org/protocol/commands", new
		 * AdHocCommandDataProvider.MalformedActionError());
		 * pm.addExtensionProvider("bad-locale",
		 * "http://jabber.org/protocol/commands", new
		 * AdHocCommandDataProvider.BadLocaleError());
		 * pm.addExtensionProvider("bad-payload",
		 * "http://jabber.org/protocol/commands", new
		 * AdHocCommandDataProvider.BadPayloadError());
		 * pm.addExtensionProvider("bad-sessionid",
		 * "http://jabber.org/protocol/commands", new
		 * AdHocCommandDataProvider.BadSessionIDError());
		 * pm.addExtensionProvider("session-expired",
		 * "http://jabber.org/protocol/commands", new
		 * AdHocCommandDataProvider.SessionExpiredError());
		 */
	}
	// public static List<UserBean> searchUsers(XMPPConnection connection,String
	// serverDomain,String userName) throws XMPPException
	// {
	// List<UserBean> results = new ArrayList<UserBean>();
	// System.out.println("查询开始..............."+connection.getHost()+connection.getServiceName());
	//
	// UserSearchManager usm = new UserSearchManager(connection);
	//
	//
	// Form searchForm = usm.getSearchForm(serverDomain);
	// Form answerForm = searchForm.createAnswerForm();
	// answerForm.setAnswer("Username", true);
	// answerForm.setAnswer("search", userName);
	// ReportedData data = usm.getSearchResults(answerForm, serverDomain);
	//
	// Iterator<Row> it = data.getRows();
	// Row row = null;
	// UserBean user = null;
	// while(it.hasNext())
	// {
	// user = new UserBean();
	// row = it.next();
	// user.setUserName(row.getValues("Username").next().toString());
	// user.setName(row.getValues("Name").next().toString());
	// user.setEmail(row.getValues("Email").next().toString());
	// System.out.println(row.getValues("Username").next());
	// System.out.println(row.getValues("Name").next());
	// System.out.println(row.getValues("Email").next());
	// results.add(user);
	// //若存在，则有返回,UserName一定非空，其他两个若是有设，一定非空
	// }
	//
	// return results;
	// }
}
