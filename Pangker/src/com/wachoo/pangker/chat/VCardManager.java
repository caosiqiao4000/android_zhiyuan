package com.wachoo.pangker.chat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;

import org.jivesoftware.smackx.packet.VCard;

import android.content.Context;
import android.util.Log;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.impl.PKUserDaoImpl;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UserinfoQueryResult;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

/**
 * VCardManager
 * 
 * VCardManager handles all VCard loading/caching within pangker. 管理用户vcard信息
 * 
 * @author wangxin
 * 
 */
public class VCardManager {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private PangkerApplication application;

	// 当前用户Vcard信息
	private VCard personalVCard;
	// 是否取得当前用户Vcard标示
	private boolean vcardLoaded;
	// 用户联系人的vcard信息
	private Map<String, VCard> vcards = new HashMap<String, VCard>();
	//
	private LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<String>();

	private Context context;
	private IPKUserDao ipkUserDao;
	private String myUserID;

	/**
	 * VCardManager
	 * 
	 * @param context
	 */
	public VCardManager(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		application = (PangkerApplication) context.getApplicationContext();
		myUserID = application.getMyUserID();
		ipkUserDao = new PKUserDaoImpl(context);
	}

	/**
	 * 
	 * 根据用户id获取用户信息
	 * 
	 * @param userid
	 * @return
	 */
	public UserInfo getUserInfo(String userid) {
		Log.i(TAG, "getUserInfo---start;userid--" + userid);
		if (ipkUserDao == null)
			ipkUserDao = new PKUserDaoImpl(context);
		UserInfo user = ipkUserDao.getUserByUserID(userid);
		if (user == null) {
			Log.i(TAG, "from---server vcard");
			user = getUserFromServer(userid, myUserID);
		} else {
			Log.i(TAG, "from---database");
		}
		return user;
	}

	/**
	 * 
	 * @param userid
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-28 下午03:19:50
	 */
	public UserItem getUserItem(String userid) {
		UserItem item = null;
		item = ipkUserDao.getUserItemByUserID(userid);
		if (item != null) {
			return item;
		}
		item = getUserItemFromServer(userid, myUserID);
		return item;
	}

	/**
	 * 
	 * @param userid
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-28 下午02:40:20
	 */
	public UserItem getUserItemFromServer(String userid, String myUserID) {
		UserItem userItem = new UserItem(getUserFromServer(userid, myUserID));
		return userItem;
	}

	/**
	 * 
	 * @param userid
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-28 下午02:33:59
	 */
	public UserInfo getUserFromServer(String userid, String myUserID) {
		UserInfo info = null;
		try {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("opuid", myUserID));
			paras.add(new Parameter("uid", Util.trimUserIDDomain(userid)));
			paras.add(new Parameter("optype", "1"));
			SyncHttpClient http = new SyncHttpClient();
			String supportResponse = http.httpPost(
					Configuration.getUserinfoQuery(), paras,
					application.getServerTime());
			UserinfoQueryResult result = JSONUtil.fromJson(
					supportResponse.toString(), UserinfoQueryResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				info = result.getUserInfo();
				if (info.getIconType() == 1)
					info.setIconBytes(ImageUtil.getImageBytesFromURL(info
							.getIconUrl()));
				ipkUserDao.saveUser(info);
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.error(TAG, e);
		}
		return info;
	}

	// /***
	// *
	// * get user information from user vcard by userid
	// *
	// * @param userid
	// * @return
	// */
	// public UserInfo getUserFormVCard(String userid) {
	//
	// UserInfo user = new UserInfo();
	// final VCard card = getVCard(userid);
	// user.setUserId(userid);
	// user.setPhone(card.getField("phone"));
	// user.setUserName(card.getFirstName());
	// user.setNickName(card.getNickName());
	// try {
	// if (card.getField("icontype") != null) {
	// user.setIconType(Integer.valueOf(card.getField("icontype")));
	// }
	// if (card.getField("identity") != null) {
	// user.setIconType(Integer.valueOf(card.getField("identity")));
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// logger.error(TAG, e);
	// user.setIconType(0);
	// user.setIdentity(0);
	// }
	//
	// user.setSign(card.getField("sign"));
	// return user;
	// }

	private UserInfo VCardToUser(VCard card) {
		UserInfo user = new UserInfo();
		user.setNickName(card.getNickName());
		user.setUserName(card.getFirstName());
		user.setSign(card.getField("sign"));
		user.setEmail(card.getEmailHome());
		user.setIconBytes(card.getAvatar());
		return user;
	}

	// /**
	// * 根据id取得vcard
	// *
	// * @param jid
	// * @return
	// */
	// private VCard getVCard(String userid) {
	// return getVCard(userid, true);
	// }

	// private VCard getVCardFromMemory(String jid) {
	// // Check in memory first.
	// if (vcards.containsKey(jid)) {
	// return vcards.get(jid);
	// }
	//
	// // if not in memory
	// VCard vcard = loadFromFileSystem(jid);
	// if (vcard == null) {
	// addToQueue(jid);
	//
	// // Create temp vcard.
	// vcard = new VCard();
	// vcard.setJabberId(jid);
	// }
	//
	// return vcard;
	// }

	/**
	 * Adds a jid to lookup vCard.
	 * 
	 * @param jid
	 *            the jid to lookup.
	 */
	private void addToQueue(String jid) {
		if (!queue.contains(jid)) {
			queue.add(jid);
		}
	}

	// /**
	// * 重新加载指定用户的vcard信息
	// *
	// * @param jid
	// * the userid of the user.
	// * @return the new VCard.
	// */
	// public VCard reloadVCard(String userid) {
	// userid = StringUtils.parseBareAddress(userid);
	// VCard vcard = new VCard();
	// try {
	// vcard.load(PangkerManager.getOpenfireManager().getConnection(), userid);
	// vcard.setJabberId(userid);
	// vcards.put(userid, vcard);
	// } catch (XMPPException e) {
	// logger.error(TAG, e);
	// vcard.setError(new XMPPError(409));
	// vcards.put(userid, vcard);
	// }
	// // Persist XML
	// // persistVCard(jid, vcard);
	// return vcards.get(userid);
	// }

	// /**
	// *
	// * @param vCard
	// */
	// private void saveToFileSystem(VCard vCard) {
	// // MyDataBaseAdapter m_MyDataBaseAdapter = new
	// // MyDataBaseAdapter(MainService.getMainContext());
	//
	// }

	// /**
	// * 根据id取得vcard信息
	// *
	// * @param jid
	// * @param useCache
	// * @return
	// */
	// private VCard getVCard(String userid, boolean useCache) {
	// userid = StringUtils.parseBareAddress(userid);
	// if (!vcards.containsKey(userid) || !useCache) {
	// VCard vcard = new VCard();
	// try {
	// // // 从本地存储取得
	// // VCard localVCard = loadFromFileSystem(userid);
	// // if (localVCard != null) {
	// // localVCard.setJabberId(userid);
	// // vcards.put(userid, localVCard);
	// // return localVCard;
	// // }
	// // 从服务器取得。
	// vcard.load(PangkerManager.getOpenfireManager().getConnection(), userid);
	// vcard.setJabberId(userid);
	// vcards.put(userid, vcard);
	//
	// } catch (XMPPException e) {
	// logger.error(TAG, e);
	// vcard.setJabberId(userid);
	// // Log.warning("Unable to load vcard for " + jid, e);
	// vcard.setError(new XMPPError(409));
	// vcards.put(userid, vcard);
	// }
	// }
	// return vcards.get(userid);
	// }

	// /**
	// * 从本地获取用户的vcard信息
	// *
	// * @param jid
	// * @return
	// */
	// private VCard loadFromFileSystem(String userid) {
	//
	// return null;
	// }

	// /***
	// * 根据用户的vcard获取用户头像信息
	// *
	// * @param vcard
	// * @return
	// */
	// public Bitmap getUserImage(VCard vcard) {
	// Bitmap ic = null;
	// try {
	// if (vcard == null || vcard.getAvatar() == null) {
	// return null;
	// }
	// ic = ImageUtil.Bytes2Bimap(vcard.getAvatar());
	// System.out.println("图片大小:" + ic.getHeight() + " " + ic.getWidth());
	//
	// } catch (Exception e) {
	// logger.error(TAG, e);
	// }
	// return ic;
	// }

	// /**
	// * 取得当前用户VCard信息
	// *
	// * @return
	// */
	// private VCard getMyVCard() {
	// if (!vcardLoaded) {
	// try {
	// personalVCard.load(PangkerManager.getOpenfireManager().getConnection());
	//
	// // 保存用户头像到本地
	// byte[] bytes = personalVCard.getAvatar();
	// if (bytes != null) {
	// // 保存头像代码
	// }
	// } catch (Exception e) {
	// logger.error(TAG, e);
	// }
	// vcardLoaded = true;
	// }
	// return personalVCard;
	// }

}
