//package com.wachoo.pangker.chat;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Iterator;
//import java.util.List;
//
//import org.jivesoftware.smack.XMPPConnection;
//import org.jivesoftware.smack.XMPPException;
//import org.jivesoftware.smackx.Form;
//import org.jivesoftware.smackx.FormField;
//import org.jivesoftware.smackx.ServiceDiscoveryManager;
//import org.jivesoftware.smackx.muc.HostedRoom;
//import org.jivesoftware.smackx.muc.MultiUserChat;
//import org.jivesoftware.smackx.muc.RoomInfo;
//import org.jivesoftware.smackx.packet.DiscoverInfo;
//import org.jivesoftware.smackx.packet.DiscoverItems;
//
//import com.wachoo.pangker.PangkerConstant;
//import com.wachoo.pangker.PangkerManager;
//import com.wachoo.pangker.entity.TraceGroup;
//import com.wachoo.pangker.util.Util;
//
///**
// * 
// * 友踪组实现类
// * 
// * @author wangxin
// * 
// */
//public class TraceGroupManager {
//
//	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag
//
//	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
//	final OpenfireManager openfireManager = PangkerManager.getOpenfireManager();
//	private XMPPConnection connection = openfireManager.getConnection();
//	private static final String rooms_uffix = "@conference.pangker";
//	public static final String user_uffix = "@pangker";
//
//	/**
//	 * Creates a new multi user chat with the specified connection and room
//	 * name. 创建一个组
//	 * 
//	 * @param name
//	 * @throws XMPPException
//	 */
//	public MultiUserChat CreateTraceGroup(String name) throws XMPPException {
//		final OpenfireManager openfireManager = PangkerManager.getOpenfireManager();
//		return new MultiUserChat(openfireManager.getConnection(), name);
//	}
//
//	/**
//	 * 
//	 * 创建一个聊天组（非持久聊天室） 不设置密码
//	 * 
//	 * @param roomName
//	 * @param users
//	 * @param inviteReason
//	 * @return
//	 */
//	public MultiUserChat createConferenceRoom(String roomID, String roomName, List<String> users,
//			String inviteReason,String createrUid) {
//
//		MultiUserChat muc = null;
//		try {
//			// ----创建手动配置聊天室----
//			muc = new MultiUserChat(connection, roomID + rooms_uffix);
//
//			// 销毁聊天室
//			// muc.destroy("Test", null);
//			muc.create(connection.getUser());
//			// 获取聊天室的配置表单
//			Form form = muc.getConfigurationForm();
//			// 根据原始表单创建一个要提交的新表单
//			Form submitForm = form.createAnswerForm();
//			// 向提交的表单添加默认答复
//			for (Iterator<FormField> fields = form.getFields(); fields.hasNext();) {
//				FormField field = (FormField) fields.next();
//				if (!FormField.TYPE_HIDDEN.equals(field.getType()) && field.getVariable() != null) {
//					submitForm.setDefaultAnswer(field.getVariable());
//				}
//			}
//			// 重新设置聊天室名称
//			if (roomName != null)
//				submitForm.setAnswer("muc#roomconfig_roomname", roomName);
//			// 设置聊天室的新拥有者
//			List<String> owners = new ArrayList<String>();
//			owners.add(createrUid + "@pangker");
//			submitForm.setAnswer("muc#roomconfig_roomowners", owners);
//			// 设置密码
//			// submitForm.setAnswer("muc#roomconfig_passwordprotectedroom",true);
//			// submitForm.setAnswer("muc#roomconfig_roomsecret", "reserved");
//			// 设置描述
//			submitForm.setAnswer("muc#roomconfig_roomdesc", roomName);
//			// 设置聊天室是持久聊天室，即将要被保存下来
//			// submitForm.setAnswer("muc#roomconfig_persistentroom", true);
//			// 发送已完成的表单到服务器配置聊天室
//			muc.sendConfigurationForm(submitForm);
//			muc.changeSubject(roomName);
//			// muc.invite("2@pangker", "篮球跟踪组,一起参加啊呵呵~~");
//			for (String jid : users) {
//				muc.invite(jid + user_uffix, inviteReason);
//			}
//		} catch (XMPPException e) {
//			// TODO Auto-generated catch block
//			logger.error(TAG, e);
//		}
//		return muc;
//	}
//
//	/**
//	 * Checks if a <code>ChatRoom</code> exists.
//	 * 
//	 * @param jid
//	 *            the jid of the user.
//	 * @return true if the ChatRoom exists.
//	 */
//	public boolean chatRoomExists(String jid) {
//
//		return true;
//	}
//
//	/**
//	 * destroy chatroom
//	 * 
//	 * @param reason
//	 * @param alternateJID
//	 * @return
//	 */
//	public boolean destroy(MultiUserChat muc, String reason, String alternateJID) {
//		try {
//			muc.destroy(reason, alternateJID);
//		} catch (Exception e) {
//			// TODO: handle exception
//			logger.error(TAG, e);
//			return false;
//		}
//		return true;
//	}
//
//	/**
//	 * 
//	 * @param roomid
//	 * @return
//	 * 
//	 * @author wangxin
//	 * @date 2012-2-27 下午04:46:29
//	 */
//	public TraceGroup getTraceGroupByID(String roomid, String creater) {
//		TraceGroup group = new TraceGroup();
//		try {
//			HostedRoom hostedRoom = getHostedRoomByID(roomid);
//			group.setUid(Util.trimUserIDDomain(creater));// 友踪组创建者
//			group.setGroupId(Util.getLeftString(roomid, "@"));
//			group.setGroupType(PangkerConstant.GROUP_TRACES);
//			
//			RoomInfo roomInfo = MultiUserChat.getRoomInfo(connection, roomid);
//			group.setGroupName(roomInfo.getSubject());// 友踪组名称
//			group.setGroupSid(roomInfo.getSubject());// 资源sid
//			group.setLastVisitTime(Util.getSysNowTime());// 最后一次加入友踪组的时间
//			hostedRoom.getName();
//
//		} catch (Exception e) {
//			// TODO: handle exception
//			logger.error(TAG, e);
//		}
//		return group;
//	}
//
//	/**
//	 * 
//	 * @param jid
//	 * @return
//	 * 
//	 * @author wangxin
//	 * @date 2012-2-27 下午05:38:48
//	 */
//	private String getRoomName(String jid) {
//		return Util.getRightString(jid, "/");
//	}
//
//	/**
//	 * 
//	 * @param roomid
//	 * @return
//	 * 
//	 * @author wangxin
//	 * @date 2012-2-27 下午05:24:49
//	 */
//	public HostedRoom getHostedRoomByID(String roomid) {
//		HostedRoom hostedRoom = null;
//		Collection<HostedRoom> rooms;
//		try {
//			rooms = MultiUserChat.getHostedRooms(connection, roomid);
//			Iterator<HostedRoom> iterator = rooms.iterator();
//			while (iterator.hasNext()) {
//				hostedRoom = (HostedRoom) iterator.next();
//			}
//		} catch (XMPPException e) {
//			// TODO Auto-generated catch block
//			logger.error(TAG, e);
//		}
//		return hostedRoom;
//	}
//
//	public void printRooms() {
//		List<String> col;
//		try {
//			col = getConferenceServices(connection.getServiceName(), connection);
//
//			for (Object aCol : col) {
//				String service = (String) aCol;
//				// 查询服务器上的聊天室
//				Collection<HostedRoom> rooms = MultiUserChat.getHostedRooms(connection, service);
//				for (HostedRoom room : rooms) {
//					// 查看Room消息
//					RoomInfo roomInfo = MultiUserChat.getRoomInfo(connection, room.getJid());
//					if (roomInfo != null) {
//					}
//				}
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			logger.error(TAG, e);
//		}
//	}
//
//	public List<String> getConferenceServices(String server, XMPPConnection connection) throws Exception {
//		List<String> answer = new ArrayList<String>();
//		ServiceDiscoveryManager discoManager = ServiceDiscoveryManager.getInstanceFor(connection);
//		DiscoverItems items = discoManager.discoverItems(server);
//		for (Iterator<DiscoverItems.Item> it = items.getItems(); it.hasNext();) {
//			DiscoverItems.Item item = (DiscoverItems.Item) it.next();
//			if (item.getEntityID().startsWith("conference") || item.getEntityID().startsWith("private")) {
//				answer.add(item.getEntityID());
//			} else {
//				try {
//					DiscoverInfo info = discoManager.discoverInfo(item.getEntityID());
//					if (info.containsFeature("http://jabber.org/protocol/muc")) {
//						answer.add(item.getEntityID());
//					}
//				} catch (XMPPException e) {
//					logger.error(TAG, e);
//				}
//			}
//		}
//		return answer;
//	}
//
//}
