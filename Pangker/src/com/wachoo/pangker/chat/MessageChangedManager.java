package com.wachoo.pangker.chat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 通知有消息的提示 (发送监听,通知主界面,会话界面，主页粉丝or关注人数量变化消息)
 * 
 * @author wangxin
 * 
 */
public class MessageChangedManager {
	protected List<IMessageListener> listenerList = new ArrayList<IMessageListener>();

	public MessageChangedManager() {
		// TODO Auto-generated constructor stub
		if(listenerList == null){
			listenerList = new ArrayList<IMessageListener>();
		}
	}
	/**
	 * 添加一个Listener监听器
	 */
	public synchronized void addListenter(IMessageListener listener) {
		listenerList.add(listener);
	}
	
	public void exitManager() {
		// TODO Auto-generated method stub
		listenerList.clear();
	}

	/**
	 * 移除一个已注册的Listener监听器. 如果监听器列表中已有相同的监听器listener1、listener2,
	 * 并且listener1==listener2, 那么只移除最近注册的一个监听器。
	 */
	public synchronized void removeListenter(IMessageListener listener) {
		listenerList.remove(listener);
	}

	public void chatMessageChanged(MessageChangedEvent event) {
		IMessageListener listener = null;
		Iterator<IMessageListener> it = listenerList.iterator();// 这步要注意同步问题
		while (it.hasNext()) {
			listener = (IMessageListener) it.next();
			if (listener != null)
				listener.messageChanged(event.getInfo());
		}
	}
}
