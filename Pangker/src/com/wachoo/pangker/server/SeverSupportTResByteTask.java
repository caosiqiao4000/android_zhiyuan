package com.wachoo.pangker.server;

import java.lang.ref.WeakReference;
import java.util.List;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import android.content.Context;
import android.os.AsyncTask;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.util.ConnectivityHelper;

public class SeverSupportTResByteTask extends
		AsyncTask<Object, Integer, Object> {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private String url;
	private List<Parameter> paras;
	private byte[] bytes;
	private int caseKey;
	private MyFilePart uploader;

	protected SyncHttpClient http = new SyncHttpClient();

	private WeakReference<Context> mContextReference;
	private IUICallBackInterface callBackInterfaceReference;
	private PangkerApplication application;

	/***
	 * 
	 * SeverSupportTask public String httpPostWithFile(String url,
	 * List<Parameter> params,String fileParamName, File file, byte[] cover)
	 * throws Exception
	 * 
	 * @param context
	 * @param pdShow
	 * @param paras
	 * @param url
	 * @param callBackInterface
	 */
	public SeverSupportTResByteTask(Context context,
			IUICallBackInterface callBackInterface, String url,
			List<Parameter> paras, MyFilePart uploader, byte[] cover,
			int caseKey) {
		this.application = (PangkerApplication) context.getApplicationContext();
		this.mContextReference = new WeakReference<Context>(context);
		this.callBackInterfaceReference = callBackInterface;
		this.url = url;
		this.paras = paras;
		this.uploader = uploader;
		this.bytes = cover;
		this.caseKey = caseKey;

	}

	@Override
	protected Object doInBackground(Object... params) {
		// >>>>>>>>>add by wangxin 弱引用防止回收！
		if (isCancelled() || mContextReference.get() == null
				|| callBackInterfaceReference == null) {
			return HttpReqCode.canceled;
		}
		if (!ConnectivityHelper
				.isConnectivityAvailable(mContextReference.get())) {
			return HttpReqCode.no_network;
		}
		try {

			String resString = http.httpPostWithFile(url, paras, uploader,
					bytes, application.getServerTime());
			if (resString == null) {
				return HttpReqCode.error;
			}
			return resString;
		} catch (Exception e) {
			logger.error(TAG, e);
			return HttpReqCode.error;
		}
	}

	@Override
	protected void onPostExecute(Object result) {
		if (isCancelled() || mContextReference.get() == null
				|| callBackInterfaceReference == null) {
			return;
		}
		if (callBackInterfaceReference != null && !isCancelled()) {
			callBackInterfaceReference.uiCallBack(result, this.caseKey);
		}
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(progress);
	}

}
