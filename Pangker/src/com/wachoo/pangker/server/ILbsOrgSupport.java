package com.wachoo.pangker.server;

import com.wachoo.pangker.api.PangkerException;
import com.wachoo.pangker.map.poi.Geo_Search_Request;
import com.wachoo.pangker.map.poi.Geo_Search_Response;
import com.wachoo.pangker.map.poi.Poi_Search_Request;
import com.wachoo.pangker.map.poi.Poi_Search_Response;

/**
 * ILbsOrgSupport 
 * 使用驴博士 进行Map相关查询
 * @author wangxin
 *
 */
public interface ILbsOrgSupport {

	/**
	 * 查询geo信息
	 * @param request
	 * @return
	 * @throws PangkerException
	 */
	public Geo_Search_Response GeoSearch(Geo_Search_Request request) throws PangkerException;
	
	/**
	 * 查询poi信息
	 * @param request
	 * @return
	 * @throws PangkerException
	 */
	public Poi_Search_Response PoiSearch(Poi_Search_Request request) throws PangkerException;
}
