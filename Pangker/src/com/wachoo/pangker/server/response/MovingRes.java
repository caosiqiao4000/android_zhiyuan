package com.wachoo.pangker.server.response;

/**
 * 
 * 资源随着人的移动而移动
 * 
 * @author wangxin
 * 
 */
public class MovingRes implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	public static final String SID_FIELD = "sid";// 资源ID
	public static final String UID_FIELD = "uid";// 所属用户的用户id
	public static final String CREATETIME = "createTime";
	public static final String CTRLAT_FIELD = "longitude";// 中心经度
	public static final String CTRLON_FIELD = "latitude";// 中心纬度

	private Long sid;// 资源id
	private Long uid;// 资源所属用户id
	private Integer type;// 资源类型资源类型 10：问答，11：阅读，12：图片，13：音乐，14：应用，15：直播，16：视频.
	private Long score;// 资源评分
	private Long hot;// 人气值
	private Long shareTimes;// 分享次数
	private Long downloadTimes;// 下载次数
	private Long visitedTimes;// 访问次数
	private Long favorTimes;// 收藏次数
	private Long commentTimes;// 评论次数
	private String issuetime;// 创建时间
	private double lon;// 中心经度
	private double lat;// 中心纬度
	private Long distance;// 距离
	private int opetype; // 操作类型
	private Integer ifShare; // 是否分享
	private Integer ifCollect;// 是否允许收藏
	private Integer ifOpen; // 是否公开
	private String fileFormat;
	private Double fileSize;
	private String resName;
	private boolean isShare = false;// 是否是分享的（转播资源）
	private String shareUid;// 转播人id （分享用户的UserID）

	/**
	 * @return the commentTimes
	 */
	public Long getCommentTimes() {
		if (commentTimes == null) {
			return 0l;
		}
		return commentTimes;
	}

	/**
	 * @param commentTimes
	 *            the commentTimes to set
	 */
	public void setCommentTimes(Long commentTimes) {
		this.commentTimes = commentTimes;
	}

	/**
	 * @return the hot
	 */
	public Long getHot() {
		return hot;
	}

	/**
	 * @param hot
	 *            the hot to set
	 */
	public void setHot(Long hot) {
		this.hot = hot;
	}

	/**
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @return the lon
	 */
	public Double getLon() {
		return lon;
	}

	/**
	 * @param lon
	 *            the lon to set
	 */
	public void setLon(double lon) {
		this.lon = lon;
	}

	/**
	 * @return the lat
	 */
	public Double getLat() {
		return lat;
	}

	/**
	 * @param lat
	 *            the lat to set
	 */
	public void setLat(double lat) {
		this.lat = lat;
	}

	/**
	 * @return the opetype
	 */
	public Integer getOpetype() {
		return opetype;
	}

	/**
	 * @param opetype
	 *            the opetype to set
	 */
	public void setOpetype(int opetype) {
		this.opetype = opetype;
	}

	/**
	 * @return the distance
	 */
	public Long getDistance() {
		return distance;
	}

	/**
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(Long distance) {
		this.distance = distance;
	}

	/**
	 * @return the sid
	 */
	public Long getSid() {
		return sid;
	}

	/**
	 * @param sid
	 *            the sid to set
	 */
	public void setSid(Long sid) {
		this.sid = sid;
	}

	/**
	 * @return the uid
	 */
	public Long getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(Long uid) {
		this.uid = uid;
	}

	/**
	 * @return the score
	 */
	public Long getScore() {
		if (score == null) {
			return 0l;
		}
		return score;
	}

	/**
	 * @param score
	 *            the score to set
	 */
	public void setScore(Long score) {
		this.score = score;
	}

	/**
	 * @return the shareTimes
	 */
	public Long getShareTimes() {
		return shareTimes;
	}

	/**
	 * @param shareTimes
	 *            the shareTimes to set
	 */
	public void setShareTimes(Long shareTimes) {
		this.shareTimes = shareTimes;
	}

	/**
	 * @return the downloadTimes
	 */
	public Long getDownloadTimes() {
		if (downloadTimes == null) {
			return 0l;
		}
		return downloadTimes;
	}

	/**
	 * @param downloadTimes
	 *            the downloadTimes to set
	 */
	public void setDownloadTimes(Long downloadTimes) {
		this.downloadTimes = downloadTimes;
	}

	/**
	 * @return the visitedTimes
	 */
	public Long getVisitedTimes() {
		return visitedTimes;
	}

	/**
	 * @param visitedTimes
	 *            the visitedTimes to set
	 */
	public void setVisitedTimes(Long visitedTimes) {
		this.visitedTimes = visitedTimes;
	}

	/**
	 * @return the favorTimes
	 */
	public Long getFavorTimes() {
		if (favorTimes == null) {
			return 0l;
		}
		return favorTimes;
	}

	/**
	 * @param favorTimes
	 *            the favorTimes to set
	 */
	public void setFavorTimes(Long favorTimes) {
		this.favorTimes = favorTimes;
	}

	/**
	 * @return the issuetime
	 */
	public String getIssuetime() {
		return issuetime;
	}

	/**
	 * @param issuetime
	 *            the issuetime to set
	 */
	public void setIssuetime(String issuetime) {
		this.issuetime = issuetime;
	}

	public Integer getIfShare() {
		return ifShare;
	}

	public void setIfShare(Integer ifShare) {
		this.ifShare = ifShare;
	}

	public Integer getIfCollect() {
		return ifCollect;
	}

	public void setIfCollect(Integer ifCollect) {
		this.ifCollect = ifCollect;
	}

	public Integer getIfOpen() {
		return ifOpen;
	}

	public void setIfOpen(Integer ifOpen) {
		this.ifOpen = ifOpen;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public Double getFileSize() {
		return fileSize;
	}

	public void setFileSize(Double fileSize) {
		this.fileSize = fileSize;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getLatlng() {
		return lat + "," + lon;
	}

	public boolean getIsShare() {
		return isShare;
	}

	public void setIsShare(boolean isShare) {
		this.isShare = isShare;
	}

	public String getShareUid() {
		return shareUid;
	}

	public void setShareUid(String shareUid) {
		this.shareUid = shareUid;
	}

}
