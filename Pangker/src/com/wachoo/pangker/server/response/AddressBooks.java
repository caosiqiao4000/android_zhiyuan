package com.wachoo.pangker.server.response;
/**
 * 
 * @author zhengjy
 * 
 *  单位通讯录对象
 *
 */
public class AddressBooks implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int FLAG_MYSELF = 0;//本人创建
	public static final int FLAG_OTHER = 1;//本人加入。
	public static final String BOOK_KEY = "AddressBooks";
	public static final String BOOK_ID_KEY = "bookId";
	
	private Long id;  //通讯录ID标识
	private String name; //通讯录名称
	private Long uid;   //创建人
	private String username;
	private String mobile;  //创建人手机号码
	private String createtime;
	private Integer num;  //人员总数
	private Long version;
	private Integer flag;  ///0:本人创建，1：本人加入。
	
	public AddressBooks(){}

	public AddressBooks(Long id, String name, Long uid, String username,
			String mobile, String createtime, Integer num, Long version,
			Integer flag) {
		super();
		this.id = id;
		this.name = name;
		this.uid = uid;
		this.username = username;
		this.mobile = mobile;
		this.createtime = createtime;
		this.num = num;
		this.version = version;
		this.flag = flag;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	

}
