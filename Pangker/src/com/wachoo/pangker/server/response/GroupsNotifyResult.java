package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.UserItem;
/**
 * 
 * ContactNotifyResult
 * 
 * @author wangxin
 *
 */
@SuppressWarnings("serial")
public class GroupsNotifyResult extends BaseResult {
	
	private int errorCode;
	private String errorMessage;
	
	private List<ContactGroup> cgroups = new ArrayList<ContactGroup>();

	private List<UserGroup> groups = new ArrayList<UserGroup>();

	private List<UserItem> relatives = new ArrayList<UserItem>();
	
	
	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the cgroups
	 */
	public List<ContactGroup> getCgroups() {
		return cgroups;
	}

	/**
	 * @param cgroups the cgroups to set
	 */
	public void setCgroups(List<ContactGroup> cgroups) {
		this.cgroups = cgroups;
	}

	/**
	 * @return the groups
	 */
	public List<UserGroup> getGroups() {
		return groups;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(List<UserGroup> groups) {
		this.groups = groups;
	}
	
	public List<UserItem> getRelatives() {
		return relatives;
	}

	public void setRelatives(List<UserItem> relatives) {
		this.relatives = relatives;
	}
	
}
