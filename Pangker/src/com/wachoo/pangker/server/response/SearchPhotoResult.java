package com.wachoo.pangker.server.response;


/**
 * @author: zhengjy
 * @createTime: 2011-12-27
 * @desc :查询图片返回结果对象
 */
public class SearchPhotoResult implements java.io.Serializable {
	
	private int opeType;
	private int resId;
	private String title;   //	
	private int distance;
	private long longitude;
	private long latitude;
	private String previewUrl;
	
	public SearchPhotoResult(){}
	
	public SearchPhotoResult(int opeType, int resId, String title,
			int distance, long longitude, long latitude, String previewUrl) {
		super();
		this.opeType = opeType;
		this.resId = resId;
		this.title = title;
		this.distance = distance;
		this.longitude = longitude;
		this.latitude = latitude;
		this.previewUrl = previewUrl;
	}
	public int getOpeType() {
		return opeType;
	}
	public void setOpeType(int opeType) {
		this.opeType = opeType;
	}
	public int getResId() {
		return resId;
	}
	public void setResId(int resId) {
		this.resId = resId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public long getLongitude() {
		return longitude;
	}
	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}
	public long getLatitude() {
		return latitude;
	}
	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}
	public String getPreviewUrl() {
		return previewUrl;
	}
	public void setPreviewUrl(String previewUrl) {
		this.previewUrl = previewUrl;
	}
	
	

}
