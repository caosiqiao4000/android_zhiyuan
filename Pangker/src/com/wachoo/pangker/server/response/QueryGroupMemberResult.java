package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.entity.UserInfo;

/**
 * 
 * @author wubo
 * @createtime 2012-3-16
 */
public class QueryGroupMemberResult extends BaseResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<UserInfo> users = new ArrayList<UserInfo>();
	private long total;
	private long totalcount;

	public List<UserInfo> getUsers() {
		return users;
	}

	public void setUsers(List<UserInfo> users) {
		this.users = users;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public long getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(long totalcount) {
		this.totalcount = totalcount;
	}
}
