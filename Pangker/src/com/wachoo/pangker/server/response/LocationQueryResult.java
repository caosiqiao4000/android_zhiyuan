package com.wachoo.pangker.server.response;

import java.io.Serializable;

/**
 * 
 * @TODO漂移位置信息查询返回
 * 
 * 接口DriftLocationQuery.json
 * 
 * @author zhengjy
 *
 *  2012-7-11
 *
 */
public class LocationQueryResult extends BaseResult implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int STATUS_NORMAL = 10;//10正常
	public static final int STATUS_DRIFT = 11;//11漂移
	public static final int STATUS_PASS = 12;//12穿越

	private String lon;
	private String lat;
	private Integer status;
	private String targetUid;
	private String targetUserName;
	
	public LocationQueryResult(){}
	
	public LocationQueryResult(String lon, String lat, Integer status) {
		super();
		this.lon = lon;
		this.lat = lat;
		this.status = status;
	}

	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTargetUid() {
		return targetUid;
	}

	public void setTargetUid(String targetUid) {
		this.targetUid = targetUid;
	}

	public String getTargetUserName() {
		return targetUserName;
	}

	public void setTargetUserName(String targetUserName) {
		this.targetUserName = targetUserName;
	}
	
}
