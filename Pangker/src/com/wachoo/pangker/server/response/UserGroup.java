package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.util.Log;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.group.P2PUserInfo;
import com.wachoo.pangker.util.MeetRoomUtil.SpeechAllow;

/**
 * 群组信息
 * 
 * @author wubo
 * @createtime 2012-8-20
 */
public class UserGroup implements java.io.Serializable, Comparable<UserGroup> {

	private static final long serialVersionUID = 1L;

	// 群组访问权限，提供如下选项：
	// 1）所有人（默认）
	// 2）所有好友
	// 3）所有亲友
	// 4）指定好友组（选择某1个好友组）
	// 5）通过密码访问（点击可设置目录密码）
	public static String[] PERMISSION_ITEM_NAME = new String[] { "所有人可访问", "好友可访问", "亲友可访问", "指定好友组",
			"通过密码访问" };
	public static int[] PERMISSION_ITEM_ID = new int[] { // >>>>>>>>>>权限对应ID
	PangkerConstant.Role_PUBLIC,// >>>>>>>>所有人（默认）
			PangkerConstant.Role_FRIEND, // >>>>>>>>>所有好友
			PangkerConstant.Role_RELATIVES,// >>>>>>>>所有亲友
			PangkerConstant.Role_CONTACT_GROUP,// >>>>>>指定好友组
			PangkerConstant.Role_PASSWORD }; // >>>>>>>通过密码访问

	public static final String GroupType_Meet = "10";
	public static final String GroupType_Loc = "12";
	public static final String GroupType_Trace = "13";

	public static final String GROUPID_KEY = "groupId";
	public static final String GROUP_NAME_KEY = "groupName";

	private String clientId;// 客户端唯一标识
	private String groupId;// 组唯一标识
	private String uid;// 群组创建者
	private String groupSid;// 组SID标识
	private String groupName;// 组名称
	private String groupType;// 组类别：10:会议室，12：带位置属性的组，13：友踪组
	private String loType;// 0固定群组,1移动群组,2,没有位置属性
	private String createTime;// 群组创建时间
	private String lastVisitTime;// 群组访问时间
	private Integer groupFlag;// 拥有者:0:我的群组,1:收藏过的群组,2:参与过的群组,3:我转播的群组
	private String shareId; // 转播的的shareId 主键
	private Integer ifShare; // 是否分享 0 不分享 1：分享
	private Integer ifCover;// 是否上传过图片
	private String groupLabel;// 群标签
	private Integer category;
	private Integer totalMembers;// 群组成员数
	private Integer visitUsers;// 总来访人次
	private Integer onlineNum;// 在线人数
	private Integer voiceChannelType = 1;// 群组通道模式
	private Integer maxSpeechTime = 300;// 群组发言时长
	private String Label;// 群组标签，（转播和收藏等）

	private List<P2PUserInfo> groupMics = new ArrayList<P2PUserInfo>();// 群组麦序列表
	private List<MeetRoom> messages = new ArrayList<MeetRoom>(); // >>>>群组聊天内容
	private List<P2PUserInfo> conlist = new ArrayList<P2PUserInfo>();// 群组成员列表
	private List<P2PUserInfo> mapList = new ArrayList<P2PUserInfo>();// 群组地图成员列表

	private boolean isAdminSpeaking = false;
	// >>>>>>>>>> 是否在当前群组中 True ：是 ；No ：否
	private boolean isJoin = false;
	// >>>>>>>>>> 当前群组中是否接收语音 True ：是 ；No ：否
	private boolean isAcceptVoice = true;

	public boolean isJoin() {
		return isJoin;
	}

	public void setJoin(boolean isJoin) {
		this.isJoin = isJoin;
	}

	public boolean isAcceptVoice() {
		return isAcceptVoice;
	}

	public void setAcceptVoice(boolean isAcceptVoice) {
		this.isAcceptVoice = isAcceptVoice;
	}

	// >>>>>>>>>>>>>>>>>add by wangxin start
	public List<P2PUserInfo> getConlist() {
		return conlist;
	}

	public void setConlist(List<P2PUserInfo> conlist) {
		this.conlist = conlist;
		for (P2PUserInfo info : conlist) {
			Log.i("P2PUserInfo", "addMember:" + info.toString());
		}
	}

	public List<P2PUserInfo> getMaplist() {
		return mapList;
	}

	/**
	 * 添加成员
	 * 
	 * @param info
	 * @return
	 */
	public boolean addMember(P2PUserInfo info) {
		Log.i("P2PUserInfo", "addMember:" + info.toString());
		if (!isHaveMember(info.getUID())) {
			this.conlist.add(info);
			return true;
		}
		return false;
	}

	public boolean updateMember(P2PUserInfo info) {
		Log.i("P2PUserInfo", "updateMember:" + info.toString());
		int index = removeMember(info.getUID());
		if (index > 0) {
			this.conlist.add(index, info);
		} else {
			this.conlist.add(info);
		}
		return false;
	}

	// 更新群组地图模式
	public void addMapMember(P2PUserInfo info) {
		Log.i("P2PUserInfo", "addMapMember:" + info.toString());
		int index = removeMapMember(info.getUID());
		if (index >= 0) {
			this.mapList.add(index, info);
		} else {
			this.mapList.add(info);
		}
	}

	// 更新群组地图模式
	public int removeMapMember(long UID) {
		// TODO Auto-generated method stub
		for (int i = 0; i < this.mapList.size(); i++) {
			if (this.mapList.get(i).getUID() == UID) {
				this.mapList.remove(i);
				return i;
			}
		}
		return -1;
	}

	public boolean isInMapMember(long UID) {
		for (int i = 0; i < this.mapList.size(); i++) {
			if (this.mapList.get(i).getUID() == UID) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断成员是否存在
	 * 
	 * @param UID
	 * @return
	 */
	public boolean isHaveMember(long UID) {
		for (int i = 0; i < this.conlist.size(); i++) {
			if (this.conlist.get(i).getUID() == UID) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 删除成员 true 删除成功 false 删除失败
	 * 
	 * @param UID
	 * @return
	 */
	public int removeMember(long UID) {
		for (int i = 0; i < this.conlist.size(); i++) {
			if (this.conlist.get(i).getUID() == UID) {
				this.conlist.remove(i);
				return i;
			}
		}
		return -1;
	}

	// >>>>>>>>是否可以发言(默认为初始化)
	private SpeechAllow speechStatus = SpeechAllow.INITIALIZATION;

	public SpeechAllow getSpeechStatus() {
		return speechStatus;
	}

	public void setSpeechStatus(SpeechAllow speechStatus) {
		this.speechStatus = speechStatus;
	}

	public List<P2PUserInfo> getGroupMics() {
		return groupMics;
	}

	public void setGroupMics(List<P2PUserInfo> groupMics) {
		this.groupMics = groupMics;
	}

	// >>>>>>>指定位置加入成员
	public void addGroupMics(P2PUserInfo info, int location) {
		this.groupMics.add(location, info);
	}

	// >>>>>>>指定位置加入成员
	public void addGroupMics(P2PUserInfo info) {
		this.groupMics.add(info);
	}

	/**
	 * 从麦序列表中删除用户
	 * 
	 * @param UID
	 */
	public boolean removeGroupMics(long UID) {
		if (getGroupMics().size() > 0) {
			for (int k = getGroupMics().size() - 1; k >= 0; k--) {
				if (getGroupMics().get(k).getUID() == UID) {
					getGroupMics().remove(k);
					return true;
				}
			}
		}
		return false;
	}

	// >>>>>>>>>判断是否已经在麦序列表
	public boolean isAtGroupMics(long UID) {
		for (int j = 0; j < getGroupMics().size(); j++) {
			if (getGroupMics().get(j).getUID() == UID) {
				return true;
			}
		}
		return false;
	}

	// >>>>>>>>>离开群组
	public void leaveGroup() {
		speechStatus = SpeechAllow.INITIALIZATION; // 不允许发言
		setJoin(false); // 设置不在群组中
		conlist.clear(); // 清空成员
		groupMics.clear();// 清空麦序列表
	}

	// >>>>>>>>>>>>>>>>>add by wangxin end

	public UserGroup() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<MeetRoom> getMessages() {
		return messages;
	}

	public void setMessages(List<MeetRoom> messages) {
		this.messages = messages;
		Collections.sort(messages);
	}

	public void addMessage(MeetRoom message) {
		this.messages.add(message);
		Collections.sort(messages);
	}

	public void addMessages(List<MeetRoom> messages) {
		this.messages.addAll(messages);
		Collections.sort(messages);
	}

	public void addMessages(List<MeetRoom> messages, boolean isNoread) {
		if (isNoread) {
			this.messages.addAll(0, messages);
		}
		Collections.sort(messages);
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getGroupSid() {
		return groupSid;
	}

	public void setGroupSid(String groupSid) {
		this.groupSid = groupSid;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastVisitTime() {
		return lastVisitTime;
	}

	public void setLastVisitTime(String lastVisitTime) {
		this.lastVisitTime = lastVisitTime;
	}

	public Integer getGroupFlag() {
		return groupFlag;
	}

	public void setGroupFlag(Integer groupFlag) {
		this.groupFlag = groupFlag;
	}

	public String getGroupLabel() {
		return groupLabel;
	}

	public void setGroupLabel(String groupLabel) {
		this.groupLabel = groupLabel;
	}

	public Integer getConverFlag() {
		return ifCover;
	}

	public void setConverFlag(Integer converFlag) {
		this.ifCover = converFlag;
	}

	public String getLotype() {
		return loType;
	}

	public void setLotype(String lotype) {
		this.loType = lotype;
	}

	@Override
	public int compareTo(UserGroup another) {
		// TODO Auto-generated method stub
		return this.groupFlag.compareTo(another.groupFlag);
	}

	public String getLoType() {
		return loType;
	}

	public void setLoType(String loType) {
		this.loType = loType;
	}

	public Integer getIfCover() {
		return ifCover;
	}

	public void setIfCover(Integer ifCover) {
		this.ifCover = ifCover;
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public Integer getTotalMembers() {
		return totalMembers;
	}

	public void setTotalMembers(Integer totalMembers) {
		this.totalMembers = totalMembers;
	}

	public Integer getVisitUsers() {
		return visitUsers;
	}

	public void setVisitUsers(Integer visitUsers) {
		this.visitUsers = visitUsers;
	}

	public Integer getOnlineNum() {
		return onlineNum;
	}

	public void setOnlineNum(Integer onlineNum) {
		this.onlineNum = onlineNum;
	}

	public String getShareId() {
		return shareId;
	}

	public void setShareId(String shareId) {
		this.shareId = shareId;
	}

	public Integer getIfShare() {
		return ifShare;
	}

	public void setIfShare(Integer ifShare) {
		this.ifShare = ifShare;
	}

	public boolean isAdmin(String userID) {
		return userID.equals(uid);
	}

	public boolean isSingleVoiceChannel() {
		return voiceChannelType == 1;
	}

	public Integer getVoiceChannelType() {
		return voiceChannelType;
	}

	public void setVoiceChannelType(Integer voiceChannelType) {
		this.voiceChannelType = voiceChannelType;
	}

	public Integer getMaxSpeechTime() {
		return maxSpeechTime;
	}

	public void setMaxSpeechTime(Integer maxSpeechTime) {
		this.maxSpeechTime = maxSpeechTime;
	}

	public boolean isAdminSpeaking() {
		return isAdminSpeaking;
	}

	public void setAdminSpeaking(boolean isAdminSpeaking) {
		this.isAdminSpeaking = isAdminSpeaking;
	}

	public String getLabel() {
		return Label;
	}

	public void setLabel(String label) {
		Label = label;
	}

	public void clearData() {
		groupMics.clear();
		conlist.clear();
		mapList.clear();
	}

}
