package com.wachoo.pangker.server.response;

public class Client2UMCResult extends BaseResult {
	private P2pServerconfig sp;//

	public Client2UMCResult() {
		super();
	}

	public P2pServerconfig getSp() {
		return sp;
	}

	public void setSp(P2pServerconfig sp) {
		this.sp = sp;
	}
}