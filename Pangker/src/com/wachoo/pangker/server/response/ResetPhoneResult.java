package com.wachoo.pangker.server.response;

import com.wachoo.pangker.entity.UserInfo;


/**
 * 换卡换号申请接口返回
 * 
 * 接口ResetPhoneApply.json
 * 
 * @author zhengjy
 *
 * @version 2012-8-30
 */

public class ResetPhoneResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserInfo userInfo;
	
	
	public ResetPhoneResult(){}
	
	public ResetPhoneResult(int errorCode,String errorMessage, UserInfo userInfo) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.userInfo = userInfo;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
}
