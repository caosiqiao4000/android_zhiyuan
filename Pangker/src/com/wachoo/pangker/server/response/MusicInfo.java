package com.wachoo.pangker.server.response;

/**
 * 
 * 音乐资源基本信息
 * 
 * @author wangxin
 * 
 */
public class MusicInfo extends MovingRes {
	/*
	 * 1、歌曲名， 2、歌手， 3、歌曲时间， 4、专辑（专辑图片，专辑名称，专辑ID[用来获取图片])， 5、歌曲大小 6.歌曲路径
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String MUSICNAME_FIELD = "musicname";// 音乐名称（歌名）
	public static final String MUSICTYPE_FIELD = "musictype";// 音乐类型
	public static final String MUSICTIME_FIELD = "musictime";// 音乐播放时间
	public static final String MUSICRESOURCE_FIDED = "musicresource"; // 音乐资源地址

	private String musicName; // 歌名
	// private String singer; // 歌手
	// private String album; // 专辑
	private int musicType; // 音乐类型
	// private String desc; // 音乐介绍
	private int musicId;
	private String singer;
	private long duration;
	private String musicAlubm;
	private int musicSize;
	private String musicPath;//
	private boolean useLocalCache = false;// >>>>>>>>判断是否使用本地缓存的音乐文件(针对网络音乐使用)，true：使用

	// false：不使用

	public boolean isUseLocalCache() {
		return useLocalCache;
	}

	public void setUseLocalCache(boolean useLocalCache) {
		this.useLocalCache = useLocalCache;
	}

	/**
	 * 判断是否是网络音乐，如果musicPath 文件存在True；然后在根据Sid判断
	 */
	public boolean isNetMusic() {
		if (getSid() != null)
			return true;
		return false;
	}

	public String getMusicName() {
		return musicName;
	}

	public void setMusicName(String musicName) {
		this.musicName = musicName;
	}

	public int getMusicType() {
		return musicType;
	}

	public void setMusicType(int musicType) {
		this.musicType = musicType;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getMusicAlubm() {
		return musicAlubm;
	}

	public void setMusicAlubm(String musicAlubm) {
		this.musicAlubm = musicAlubm;
	}

	public int getMusicSize() {
		return musicSize;
	}

	public void setMusicSize(int musicSize) {
		this.musicSize = musicSize;
	}

	public String getMusicPath() {
		return musicPath;
	}

	public void setMusicPath(String musicPath) {
		this.musicPath = musicPath;
	}

	public int getMusicId() {
		return musicId;
	}

	public void setMusicId(int musicId) {
		this.musicId = musicId;
	}

	@Override
	public String toString() {
		return "MusicInfo [musicName=" + musicName + ", musicType=" + musicType + ", musicId=" + musicId
				+ ", singer=" + singer + ", musicTime=" + duration + ", musicAlubm=" + musicAlubm
				+ ", musicSize=" + musicSize + ", musicPath=" + musicPath + "]";
	}

}
