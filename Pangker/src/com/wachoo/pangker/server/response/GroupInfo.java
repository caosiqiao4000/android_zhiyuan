package com.wachoo.pangker.server.response;
/**
 * @author zhengjy
 *
 * @version 2012-1-11 
 */

public class GroupInfo implements java.io.Serializable{
	
	public String groupId;   //组唯一标识
	public String uid;      //群组创建者
	public String groupSid;   //组SID标识
	public String groupName;   //组名称
	public String groupType;  ///组类别：10:会议室，11：直播室，12：群组讨论组，13：友踪组
	public String createTime;  //群组创建时间
	
	
	public GroupInfo(){}
	
	public GroupInfo(String groupId, String uid, String groupSid,
			String groupName, String groupType,String createTime) {
		super();
		this.groupId = groupId;
		this.uid = uid;
		this.groupSid = groupSid;
		this.groupName = groupName;
		this.groupType = groupType;
		this.createTime = createTime;
	}
	
	
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getGroupSid() {
		return groupSid;
	}
	public void setGroupSid(String groupSid) {
		this.groupSid = groupSid;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
}
