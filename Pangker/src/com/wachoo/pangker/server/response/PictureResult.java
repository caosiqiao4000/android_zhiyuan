package com.wachoo.pangker.server.response;

import java.util.List;

/**
 * @author zhengjy
 *
 * @version 2012-1-6
 */

public class PictureResult implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int total;
	private boolean exact;
	private List<PicInfo> data;
	
	
	public PictureResult(){}
	
	public PictureResult(int total, boolean exact, List<PicInfo> data) {
		super();
		this.total = total;
		this.exact = exact;
		this.data = data;
	}
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public boolean isExact() {
		return exact;
	}
	public void setExact(boolean exact) {
		this.exact = exact;
	}
	public List<PicInfo> getData() {
		return data;
	}
	public void setData(List<PicInfo> data) {
		this.data = data;
	}
	
}
