package com.wachoo.pangker.server.response;


/**
 * 
 * @author longxianwen
 * 
 */
public class DocInfo extends MovingRes {

	private static final long serialVersionUID = 1L;
	public static final String FILE_PATH = "file_path";
	public static final String FILE_NAME = "file_name";
	//详细属性
	private String docname; // 文档名称
	private Integer doctype; // 文档类型
	
	public String getDocname() {
		return docname;
	}
	public void setDocname(String docname) {
		this.docname = docname;
	}
	public Integer getDoctype() {
		return doctype;
	}
	public void setDoctype(Integer doctype) {
		this.doctype = doctype;
	}
}
