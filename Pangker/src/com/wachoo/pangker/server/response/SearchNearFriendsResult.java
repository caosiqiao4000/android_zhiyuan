package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.entity.UserItem;

/**
 * @author zhengjy
 * @createTime
 * @desc 查询附近好友
 */
public class SearchNearFriendsResult extends BaseResult {

	private static final long serialVersionUID = 1L;

	private List<UserItem> users = new ArrayList<UserItem>();

	public SearchNearFriendsResult() {

	}

	public SearchNearFriendsResult(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public List<UserItem> getUsers() {
		return users;
	}

	public void setUsers(List<UserItem> users) {
		this.users = users;
	}

}
