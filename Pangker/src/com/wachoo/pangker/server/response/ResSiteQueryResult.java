package com.wachoo.pangker.server.response;

import java.util.List;

import com.wachoo.pangker.entity.DirInfo;

@SuppressWarnings("serial")
public class ResSiteQueryResult extends BaseResult {

	private String dirid;
	
	private int resType;
	
	private List<DirInfo> dirinfos;
	
	private List<ResInfo> resinfos;

	private int count;

	private int totalcount;
	
	public ResSiteQueryResult() {
		// TODO Auto-generated constructor stub
	}

	public List<DirInfo> getDirinfos() {
		return dirinfos;
	}

	public void setDirinfos(List<DirInfo> dirinfos) {
		this.dirinfos = dirinfos;
	}

	public List<ResInfo> getResinfos() {
		return resinfos;
	}

	public void setResinfos(List<ResInfo> resinfos) {
		this.resinfos = resinfos;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

	public String getDirid() {
		return dirid;
	}

	public void setDirid(String dirid) {
		this.dirid = dirid;
	}

	public int getResType() {
		return resType;
	}

	public void setResType(int resType) {
		this.resType = resType;
	}
	
}
