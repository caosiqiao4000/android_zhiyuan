package com.wachoo.pangker.server.response;
/**
 * @author zhengjy
 *
 * @version 2012-3-27 
 */

public class InviteResult implements java.io.Serializable  {
	public static final int SUCCESS = 1;
	public static final int fail = 0;
	public static final int repeat = 2;
	private int code;
	private String mobile;
	private String message;
	
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
