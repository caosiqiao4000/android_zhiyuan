package com.wachoo.pangker.server.response;

import java.util.List;

import com.wachoo.pangker.entity.RecommendInfo;


/**
 * 
 * 
 */

public class QueryBoxResResult extends BaseResult{

	/**
	 * 
	 */
	private List<RecommendInfo> boxRes;
	private List<RecommendInfo> outBoxRes;
	
	public QueryBoxResResult(){}
	
	public QueryBoxResResult(int errorCode,String errorMessage,List<RecommendInfo> boxRes,List<RecommendInfo> outBoxRes ) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.boxRes = boxRes;
		this.outBoxRes = outBoxRes;
	}

	public List<RecommendInfo> getBoxRes() {
		return boxRes;
	}

	public void setBoxRes(List<RecommendInfo> boxRes) {
		this.boxRes = boxRes;
	}

	public List<RecommendInfo> getOutBoxRes() {
		return outBoxRes;
	}

	public void setOutBoxRes(List<RecommendInfo> outBoxRes) {
		this.outBoxRes = outBoxRes;
	}


	
}
