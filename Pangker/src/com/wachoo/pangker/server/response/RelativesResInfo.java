package com.wachoo.pangker.server.response;

import java.io.Serializable;

import android.text.InputFilter;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.entity.Location;

/**
 * 亲友资源返回结果
 * 
 * @author: zhanghg
 * @createTime 2013-5-10上午9:42:13
 * @version: 1.0
 * @desc:
 * @lastModify:
 */
public class RelativesResInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int showLength = 50;

	private Long id;// 主键id
	private Long uid;// 用户id
	private Long ruid;// 亲友id
	private String userName;// 亲友昵称
	private Long resId;// 资源id
	private String resName;// 资源名称
	private String resDesc;// 资源描述
	private Integer resType;// 资源类型
	private String createTime;// 创建时间
	private int resSource;

	private Location location;
	// >>>>>>>>>>>>显示更多
	private String btnShowText = PangkerConstant.SHOW_MORE_TEXT;
	private InputFilter[] filters = null;

	public Location getLocation() {
		return location;
	}

	public String getBtnShowText() {
		return btnShowText;
	}

	public void setBtnShowText(String btnShowText) {
		this.btnShowText = btnShowText;
	}

	public InputFilter[] getFilters() {
		if (filters == null) {
			filters = new InputFilter[1];
			filters[0] = new InputFilter.LengthFilter(showLength);
		}
		return filters;
	}

	public void setFilters(InputFilter[] filters) {
		this.filters = filters;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getResSource() {
		return resSource;
	}

	public void setResSource(int resSource) {
		this.resSource = resSource;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Long getRuid() {
		return ruid;
	}

	public void setRuid(Long ruid) {
		this.ruid = ruid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getResId() {
		return resId;
	}

	public void setResId(Long resId) {
		this.resId = resId;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResDesc() {
		return resDesc;
	}

	public void setResDesc(String resDesc) {
		this.resDesc = resDesc;
	}

	public Integer getResType() {
		return resType;
	}

	public void setResType(Integer resType) {
		this.resType = resType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
