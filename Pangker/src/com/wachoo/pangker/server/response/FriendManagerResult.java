package com.wachoo.pangker.server.response;


/**
 * @author yunyf
 * @createTime 
 * @desc 好友管理返回结果
 */
public class FriendManagerResult extends BaseResult{

	private static final long serialVersionUID = 1L;

	public int errorCode = 0;

	public String errorMessage = "未处理!";
	

	public FriendManagerResult() {

	}

	public FriendManagerResult(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	

	
}
