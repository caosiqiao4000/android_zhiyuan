package com.wachoo.pangker.server.response;

import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.util.Util;

/**
 * @author zhengjy
 * 
 * @version 2012-1-11
 */

public class ResInfo implements java.io.Serializable {
	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;

	private int _id;
	private String resId; // 目录Id
	private String parentResId;//
	private int fileCount;// 父目录Id
	private String dirCover;// 目录封面，仅仅是对应用资源和音乐
	private String latlng;// 经纬度
	private String type;// 资源类别
	private String packageinfo;// 应用包名

	private String path;// 可以是文件名，如果是0就是默认的标签,可以是网络路径
	private int isfile;// 文件类型，是否是文件夹。0表示文件夹，1：文件。
	private int index;// 资源类别：图片，文档等
	private int deal;// 处理形式，收藏，还是分享.
	private String appraisement;// 资源评价

	private String lon;// 经纬度
	private String lat;// 经纬度
	public String issuetime;

	public String sid;
	public String uid;
	public String resName;
	public Integer resType;
	public String createTime;
	public Long score;
	public Long hot;
	public Long shareTimes;
	public Long downloadTimes;
	public Long visitedTimes;
	public Long broadcastTimes;
	public Long favorTimes;
	public Integer ifCollect;
	public Double collectFee;
	public Double collectDeduct;
	public String fileFormat;
	public String resLabel;
	public Integer ifShare; // 是否分享
	public Integer ifRebroadcast;// 0:广播 1:转播

	public Integer ifOpen;
	public Double downloadFee;
	public Long fileSize;
	public String memo;
	public Long commentTimes;
	private Long shareTime;
	public Integer apptype;
	public String appdesc;
	public String singer;
	public Integer musicType;
	public String musicdesc;
	public Integer picType;
	public Integer docType;
	public String docDesc;
	private String picDesc;
	private String docAuthor;

	public Integer isGarner;// 0:未另存为 ,1:已另存为
	public Integer isMyres;// 0:非本人资源 ,1:本人资源
	private String sdFilePath;// 可以是文件名，如果是0就是默认的标签,可以是网络路径
	public Integer isRebroadcast;// 0:未转播 1:已转播
	public Integer isCollect;// 0:未收藏 1:已收藏
	public Integer isScore;// 0:未评分 1:已评分
	public Integer appraiseTimes;// 评分次数
	public String shareUserName;// 广播者用户名
	public String reshareUserName;// 转播者用户名(转播的资源，必须返回)
	public Integer duration;// 音乐的播放长度;

	private Integer resSource; // 0：原创 1：收藏
	private Long resOwnerUid;// 收藏资源时，资源拥有者
	private String resOwnerNickname;// 资源拥有者昵称
	private String upPoi;

	public ResInfo() {
	}

	public ResInfo(int id, String resId, String parentResId, int fileCount, String dirCover, String latlng,
			String type, String path, int isfile, int index, int deal, String appraisement, String lon, String lat,
			String issuetime, String sid, String uid, String resName, Integer resType, String createTime, Long score,
			Long hot, Long shareTimes, Long downloadTimes, Long visitedTimes, Long broadcastTimes, Long favorTimes,
			Integer ifCollect, Double collectFee, Double collectDeduct, String fileFormat, String resLabel,
			Integer ifShare, Integer ifOpen, Double downloadFee, Long fileSize, String memo, Long commentTimes,
			Integer apptype, String appdesc, String singer, Integer musicType, String musicdesc, Integer picType,
			Integer docType, String docDesc, Integer isMyres, Integer isRebroadcast, Integer isCollect,
			Integer isScore, Integer appraiseTimes) {
		super();
		_id = id;
		this.resId = resId;
		this.parentResId = parentResId;
		this.fileCount = fileCount;
		this.dirCover = dirCover;
		this.latlng = latlng;
		this.type = type;
		this.path = path;
		this.isfile = isfile;
		this.index = index;
		this.deal = deal;
		this.appraisement = appraisement;
		this.lon = lon;
		this.lat = lat;
		this.issuetime = issuetime;
		this.sid = sid;
		this.uid = uid;
		this.resName = resName;
		this.resType = resType;
		this.createTime = createTime;
		this.score = score;
		this.hot = hot;
		this.shareTimes = shareTimes;
		this.downloadTimes = downloadTimes;
		this.visitedTimes = visitedTimes;
		this.broadcastTimes = broadcastTimes;
		this.favorTimes = favorTimes;
		this.ifCollect = ifCollect;
		this.collectFee = collectFee;
		this.collectDeduct = collectDeduct;
		this.fileFormat = fileFormat;
		this.resLabel = resLabel;
		this.ifShare = ifShare;
		this.ifOpen = ifOpen;
		this.downloadFee = downloadFee;
		this.fileSize = fileSize;
		this.memo = memo;
		this.commentTimes = commentTimes;
		this.apptype = apptype;
		this.appdesc = appdesc;
		this.singer = singer;
		this.musicType = musicType;
		this.musicdesc = musicdesc;
		this.picType = picType;
		this.docType = docType;
		this.docDesc = docDesc;
		this.isMyres = isMyres;
		this.isRebroadcast = isRebroadcast;
		this.isCollect = isCollect;
		this.isScore = isScore;
		this.appraiseTimes = appraiseTimes;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getUid() {
		return uid;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int id) {
		_id = id;
	}

	public String getResId() {
		return resId;
	}

	public void setResId(String resId) {
		this.resId = resId;
	}

	public int getFileCount() {
		return fileCount;
	}

	public void setFileCount(int fileCount) {
		this.fileCount = fileCount;
	}

	public String getDirCover() {
		return dirCover;
	}

	public void setDirCover(String dirCover) {
		this.dirCover = dirCover;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getIsfile() {
		return isfile;
	}

	public void setIsfile(int isfile) {
		this.isfile = isfile;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getDeal() {
		return deal;
	}

	public void setDeal(int deal) {
		this.deal = deal;
	}

	public String getAppraisement() {
		return appraisement;
	}

	public void setAppraisement(String appraisement) {
		this.appraisement = appraisement;
	}

	public void setLatlng(String latlng) {
		this.latlng = latlng;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public Integer getResType() {
		return resType;
	}

	public void setResType(Integer resType) {
		this.resType = resType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Long getHot() {
		return hot;
	}

	public void setHot(Long hot) {
		this.hot = hot;
	}

	public Long getShareTimes() {
		if (shareTimes == null) {
			return 0l;
		}
		return shareTimes;
	}

	public void setShareTimes(Long shareTimes) {
		this.shareTimes = shareTimes;
	}

	public Long getDownloadTimes() {
		if (downloadTimes == null) {
			return 0l;
		}
		return downloadTimes;
	}

	public void setDownloadTimes(Long downloadTimes) {
		this.downloadTimes = downloadTimes;
	}

	public Long getVisitedTimes() {
		if (visitedTimes == null) {
			return 0l;
		}
		return visitedTimes;
	}

	public void setVisitedTimes(Long visitedTimes) {
		this.visitedTimes = visitedTimes;
	}

	public Long getBroadcastTimes() {
		if (broadcastTimes == null) {
			return 0l;
		}
		return broadcastTimes;
	}

	public void setBroadcastTimes(Long broadcastTimes) {
		this.broadcastTimes = broadcastTimes;
	}

	public Long getFavorTimes() {
		if (favorTimes == null) {
			return 0l;
		}
		return favorTimes;
	}

	public void setFavorTimes(Long favorTimes) {
		this.favorTimes = favorTimes;
	}

	public Integer getIfCollect() {
		if (ifCollect == null) {
			return 0;
		}
		return ifCollect;
	}

	public void setIfCollect(Integer ifCollect) {
		this.ifCollect = ifCollect;
	}

	public Double getCollectFee() {
		if (collectFee == null) {
			return 0.0;
		}
		return collectFee;
	}

	public void setCollectFee(Double collectFee) {
		this.collectFee = collectFee;
	}

	public Double getCollectDeduct() {
		if (collectDeduct == null) {
			return 0.0;
		}
		return collectDeduct;
	}

	public void setCollectDeduct(Double collectDeduct) {
		this.collectDeduct = collectDeduct;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public String getResLabel() {
		return resLabel;
	}

	public void setResLabel(String resLabel) {
		this.resLabel = resLabel;
	}

	public Integer getIfShare() {
		if (ifShare == null) {
			return 0;
		}
		return ifShare;
	}

	public void setIfShare(Integer ifShare) {
		this.ifShare = ifShare;
	}

	public Integer getIfOpen() {
		return ifOpen;
	}

	public void setIfOpen(Integer ifOpen) {
		this.ifOpen = ifOpen;
	}

	public Double getDownloadFee() {
		if (downloadFee == null) {
			return 0.0;
		}
		return downloadFee;
	}

	public void setDownloadFee(Double downloadFee) {
		this.downloadFee = downloadFee;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getMemo() {
		return memo;
	}

	public String getLatlng() {
		if (latlng == null || "".equals(latlng)) {
			return lat + "," + lon;
		}
		return latlng;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getIssuetime() {
		return issuetime;
	}

	public void setIssuetime(String issuetime) {
		this.issuetime = issuetime;
	}

	public Long getCommentTimes() {
		if (commentTimes == null) {
			return 0l;
		}
		return commentTimes;
	}

	public void setCommentTimes(Long commentTimes) {
		this.commentTimes = commentTimes;
	}

	public Integer getApptype() {
		if (apptype == null) {
			return 0;
		}
		return apptype;
	}

	public void setApptype(Integer apptype) {
		this.apptype = apptype;
	}

	public String getAppdesc() {
		return appdesc;
	}

	public void setAppdesc(String appdesc) {
		this.appdesc = appdesc;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public Integer getMusicType() {
		if (musicType == null) {
			return 0;
		}
		return musicType;
	}

	public void setMusicType(Integer musicType) {
		this.musicType = musicType;
	}

	public Integer getPicType() {
		return picType;
	}

	public void setPicType(Integer picType) {
		this.picType = picType;
	}

	public String getMusicdesc() {
		return musicdesc;
	}

	public void setMusicdesc(String musicdesc) {
		this.musicdesc = musicdesc;
	}

	public Integer getsMyres() {
		return isMyres;
	}

	public void setsMyres(Integer isMyres) {
		this.isMyres = isMyres;
	}

	public Integer getIsRebroadcast() {
		if (isRebroadcast == null)
			return 0;
		return isRebroadcast;
	}

	public void setIsRebroadcast(Integer isRebroadcast) {
		this.isRebroadcast = isRebroadcast;
	}

	public Integer getIsCollect() {
		return isCollect;
	}

	public void setIsCollect(Integer isCollect) {
		this.isCollect = isCollect;
	}

	public Integer getIsScore() {
		return isScore;
	}

	public void setIsScore(Integer isScore) {
		this.isScore = isScore;
	}

	public String getParentResId() {
		return parentResId;
	}

	public void setParentResId(String parentResId) {
		this.parentResId = parentResId;
	}

	public Integer getDocType() {
		return docType;
	}

	public void setDocType(Integer docType) {
		this.docType = docType;
	}

	public String getDocDesc() {
		return docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	public Integer getIsMyres() {
		return isMyres;
	}

	public void setIsMyres(Integer isMyres) {
		this.isMyres = isMyres;
	}

	public Integer getAppraiseTimes() {
		return appraiseTimes;
	}

	public void setAppraiseTimes(Integer appraiseTimes) {
		this.appraiseTimes = appraiseTimes;
	}

	public String getShareUserName() {
		return shareUserName;
	}

	public void setShareUserName(String shareUserName) {
		this.shareUserName = shareUserName;
	}

	public String getReshareUserName() {
		return reshareUserName;
	}

	public void setReshareUserName(String reshareUserName) {
		this.reshareUserName = reshareUserName;
	}

	public Integer getIsGarner() {
		if (isGarner == null)
			return 0;
		return isGarner;
	}

	public void setIsGarner(Integer isGarner) {
		this.isGarner = isGarner;
	}

	public Integer getIfRebroadcast() {
		return ifRebroadcast;
	}

	public void setIfRebroadcast(Integer ifRebroadcast) {
		this.ifRebroadcast = ifRebroadcast;
	}

	public Long getShareTime() {
		return shareTime;
	}

	public void setShareTime(Long shareTime) {
		this.shareTime = shareTime;
	}

	public String getPicDesc() {
		return picDesc;
	}

	public void setPicDesc(String picDesc) {
		this.picDesc = picDesc;
	}

	public String getDocAuthor() {
		return docAuthor;
	}

	public void setDocAuthor(String docAuthor) {
		this.docAuthor = docAuthor;
	}

	public String getSdFilePath() {
		return sdFilePath;
	}

	public void setSdFilePath(String sdFilePath) {
		this.sdFilePath = sdFilePath;
	}

	public String getPackageinfo() {
		return packageinfo;
	}

	public void setPackageinfo(String packageinfo) {
		this.packageinfo = packageinfo;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getResSource() {
		return resSource;
	}

	public void setResSource(Integer resSource) {
		this.resSource = resSource;
	}

	public Long getResOwnerUid() {
		return resOwnerUid;
	}

	public void setResOwnerUid(Long resOwnerUid) {
		this.resOwnerUid = resOwnerUid;
	}

	public String getResOwnerNickname() {
		return resOwnerNickname;
	}

	public void setResOwnerNickname(String resOwnerNickname) {
		this.resOwnerNickname = resOwnerNickname;
	}
	
	public String getUpPoi() {
		return upPoi;
	}

	public void setUpPoi(String upPoi) {
		this.upPoi = upPoi;
	}

	public Location getLocation() {
		return new Location(Util.String2Double(getLon()), Util.String2Double(getLat()));
	}
}
