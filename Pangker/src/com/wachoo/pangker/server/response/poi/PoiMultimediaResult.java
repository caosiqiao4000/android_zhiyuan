package com.wachoo.pangker.server.response.poi;

import java.util.List;

import com.wachoo.pangker.server.response.BaseResult;

@SuppressWarnings("serial")
public class PoiMultimediaResult extends BaseResult{

	private int pagecount;

	private List<Multimedia> multimediaList;

	public int getPagecount() {
		return pagecount;
	}

	public void setPagecount(int pagecount) {
		this.pagecount = pagecount;
	}

	public List<Multimedia> getMultimediaList() {
		return multimediaList;
	}

	public void setMultimediaList(List<Multimedia> multimediaList) {
		this.multimediaList = multimediaList;
	}

}
