package com.wachoo.pangker.server.response.poi;
import java.util.List;

import com.wachoo.pangker.server.response.BaseResult;

@SuppressWarnings("serial")
public class PoiCommentResult extends BaseResult{

	private int pagecount;
	
	private List<PoiComment> commentList;
	
	public int getPagecount() {
		return pagecount;
	}

	public void setPagecount(int pagecount) {
		this.pagecount = pagecount;
	}

	public List<PoiComment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<PoiComment> commentList) {
		this.commentList = commentList;
	}

	
}
