package com.wachoo.pangker.server.response.poi;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.server.response.BaseResult;

@SuppressWarnings("serial")
public class MultimediaComment extends BaseResult{
	
	private String userid;// 回复的用户

	private String multimediacommentid;// 多媒体评论Id

	private String username;// 用户的名称

	private String usericon;// 用户的头像

	private String commentcontent;// 评论的内容

	private String commenttime;// 评论的时间

	private List multimediareply = new ArrayList();// <PoiReply> 评论的回复列表

	/**
	 * @return the commentcontent
	 */
	public String getCommentcontent() {
		return commentcontent;
	}

	/**
	 * @param commentcontent
	 *            the commentcontent to set
	 */
	public void setCommentcontent(String commentcontent) {
		this.commentcontent = commentcontent;
	}

	/**
	 * @return the commenttime
	 */
	public String getCommenttime() {
		return commenttime;
	}

	/**
	 * @param commenttime
	 *            the commenttime to set
	 */
	public void setCommenttime(String commenttime) {
		this.commenttime = commenttime;
	}

	/**
	 * @return the multimediacommentid
	 */
	public String getMultimediacommentid() {
		return multimediacommentid;
	}

	/**
	 * @param multimediacommentid
	 *            the multimediacommentid to set
	 */
	public void setMultimediacommentid(String multimediacommentid) {
		this.multimediacommentid = multimediacommentid;
	}

	/**
	 * @return the multimediareply
	 */
	public List getMultimediareply() {
		return multimediareply;
	}

	/**
	 * @param multimediareply
	 *            the multimediareply to set
	 */
	public void setMultimediareply(List multimediareply) {
		this.multimediareply = multimediareply;
	}

	/**
	 * @return the usericon
	 */
	public String getUsericon() {
		return usericon;
	}

	/**
	 * @param usericon
	 *            the usericon to set
	 */
	public void setUsericon(String usericon) {
		this.usericon = usericon;
	}

	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * @param userid
	 *            the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

}

