package com.wachoo.pangker.server.response;

/**
 * 
* @ClassName: SearchLbsMusicResult
* @Description: 调用搜索引警搜索问题返回结果<音乐>
* @author longxianwen 
* @date Mar 8, 2012 5:21:38 PM
*
 */
public class SearchLbsMusicResult implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private MusicResult data;
	private boolean success;

	public SearchLbsMusicResult() {
	}

	public SearchLbsMusicResult(MusicResult data, boolean success) {
		super();
		this.data = data;
		this.success = success;
	}

	public MusicResult getData() {
		return data;
	}

	public void setData(MusicResult data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
