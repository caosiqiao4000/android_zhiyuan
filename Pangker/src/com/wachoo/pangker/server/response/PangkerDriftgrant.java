package com.wachoo.pangker.server.response;
/**
 * @PangkerDriftgrant（漂移权限）
 * 
 * @author zhengjy
 *
 * 2012-7-10
 * 
 */
public class PangkerDriftgrant {
	
	private Long uid;
	private Integer grantType;
	private String grantInfo;
	private String members;
	
	public PangkerDriftgrant(){}
	
	public PangkerDriftgrant(Long uid, Integer grantType, String grantInfo,
			String members) {
		super();
		this.uid = uid;
		this.grantType = grantType;
		this.grantInfo = grantInfo;
		this.members = members;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Integer getGrantType() {
		return grantType;
	}

	public void setGrantType(Integer grantType) {
		this.grantType = grantType;
	}

	public String getGrantInfo() {
		return grantInfo;
	}

	public void setGrantInfo(String grantInfo) {
		this.grantInfo = grantInfo;
	}

	public String getMembers() {
		return members;
	}

	public void setMembers(String members) {
		this.members = members;
	}
	
	
	
}
