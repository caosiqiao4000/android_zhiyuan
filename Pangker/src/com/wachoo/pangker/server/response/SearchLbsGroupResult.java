package com.wachoo.pangker.server.response;


/**
 * @author: zhengjy
 * @createTime: 2012-4-18 
 * 
 * @desc 调用搜索引警搜索人群返回结果
 * 
 * 调用接口 http://IP..../lbsindex/groupsearch
 */
public class SearchLbsGroupResult implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private GroupResult data;
	private boolean success;
	
	public SearchLbsGroupResult(){}
	
	public SearchLbsGroupResult(GroupResult data, boolean success) {
		super();
		this.data = data;
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public GroupResult getData() {
		return data;
	}

	public void setData(GroupResult data) {
		this.data = data;
	}

}
