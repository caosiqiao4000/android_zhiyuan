package com.wachoo.pangker.server.response;

/**
 * 
 * @author wubo
 * @createtime Apr 25, 2012
 */
public class PangkerGroupAddResult extends BaseResult {
	
	private UserGroup groupInfo;

	public UserGroup getGroupInfo() {
		return groupInfo;
	}

	public void setGroupInfo(UserGroup groupInfo) {
		this.groupInfo = groupInfo;
	}

	
	
}
