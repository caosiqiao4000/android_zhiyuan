package com.wachoo.pangker.server.response;

/**
 * @author: feijinbo
 * @createTime: 2011-12-9 下午10:23:07
 * @version: 1.0
 * @desc :联系人分组信息对象定义
 */
public class ContactGroups implements java.io.Serializable {
	private String gid;// 组唯一标识.
	private String name;// 联系人类别名称，如我的好友分组1，好友分组2
	private int count;// 当前分组下记录总数
	private int type;// 组类别标识：用于区分组属于那些联系人类别，如：12：我的好友，14：关注人，15：我的粉丝（粉丝无分组信息）

	private String uid;// 分组所属用户ID

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

}
