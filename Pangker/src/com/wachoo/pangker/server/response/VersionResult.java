package com.wachoo.pangker.server.response;

@SuppressWarnings("serial")
public class VersionResult extends BaseResult{

	private String versioncode;
	
	private String illustrate;

	public String getVersioncode() {
		return versioncode;
	}

	public void setVersioncode(String versioncode) {
		this.versioncode = versioncode;
	}

	public String getIllustrate() {
		return illustrate;
	}

	public void setIllustrate(String illustrate) {
		this.illustrate = illustrate;
	}
	
	
}
