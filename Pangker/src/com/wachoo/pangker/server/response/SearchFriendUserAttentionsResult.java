package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.entity.UserInfo;

/**
 * @author wangshitong
 *2011-12-28下午2:25:52
 */
public class SearchFriendUserAttentionsResult extends BaseResult{
	private static final long serialVersionUID = 1L;
	private int count = -1;
	private int sumCount = -1;
	private List<UserInfo> users = new ArrayList<UserInfo>();
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getSumCount() {
		return sumCount;
	}
	public void setSumCount(int sumCount) {
		this.sumCount = sumCount;
	}
	public List<UserInfo> getUsers() {
		return users;
	}
	public void setUsers(List<UserInfo> users) {
		this.users = users;
	}
}
