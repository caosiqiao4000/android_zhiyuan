package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fitch
 * @createtime 2012-6-25 上午10:56:40
 * @version 1.0
 */
@SuppressWarnings("serial")
public class PhoneBookResult extends BaseResult {

	private List<PUser> pks = new ArrayList<PUser>();// 返回的旁客用户列表.

	public List<PUser> getPks() {
		return pks;
	}

	public void setPks(List<PUser> pks) {
		this.pks = pks;
	}

}
