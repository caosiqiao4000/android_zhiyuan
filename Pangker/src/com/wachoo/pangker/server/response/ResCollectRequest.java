package com.wachoo.pangker.server.response;

/**
 * 
 * PicCollectRequest
 * 
 * @author wangxin
 * 
 */
public class ResCollectRequest {
	private String appid;
	private MovingRes resinfo;
	private String authenticator;

	/**
	 * @return the appid
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * @param appid
	 *            the appid to set
	 */
	public void setAppid(String appid) {
		this.appid = appid;
	}

	/**
	 * @return the resinfo
	 */
	public Object getResinfo() {
		return resinfo;
	}

	/**
	 * @param resinfo
	 *            the resinfo to set
	 */
	public void setResinfo(MovingRes resinfo) {
		this.resinfo = resinfo;
	}

	/**
	 * @return the authenticator
	 */
	public String getAuthenticator() {
		return authenticator;
	}

	/**
	 * @param authenticator
	 *            the authenticator to set
	 */
	public void setAuthenticator(String authenticator) {
		this.authenticator = authenticator;
	}

}
