package com.wachoo.pangker.server.response;

import com.wachoo.pangker.entity.UserInfo;

/**
 * 
 * @author wubo
 * @createtime 2011-12-14 ����06:00:02
 */
public class UserRegisterResult {

	public int errorCode = -1;// 错误编码

	public String errorMessage;// 错误信息

	public UserInfo userInfo;
	
	

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
