package com.wachoo.pangker.server.response;

import com.wachoo.pangker.entity.MessageTip;

public class Commentinfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int COMMENT_TYPE_ONLY = 0; // >>>>>>>>>>仅评论资源
	public static final int COMMENT_TYPE_REPLY = 1;// >>>>>>>>>>评论同时回复
	private String commentId;
	private String userid;
	private String username;
	private String content;
	private String replyuserid;// >>>>>>回复给其他用户的UserID
	private String replyusername;// >>>>>>回复给其他用户的UserName
	private String replycommentid;// >>>>>回复评论id

	private int commenttype = COMMENT_TYPE_ONLY;// >>>>>>>>评论类型，默认只评论

	private String commenttime;

	public static Commentinfo getCommentInfo(MessageTip messageTip) {

		Commentinfo commentinfo = new Commentinfo();
		commentinfo.setCommentId(messageTip.getCommentID());
		commentinfo.setUserid(messageTip.getFromId());
		commentinfo.setUsername(messageTip.getFromName());
		switch (messageTip.getTipType()) {
		case MessageTip.TIP_COMMENT_RES:
		case MessageTip.TIP_COMMENT_BROADCAST_RES:
			commentinfo.setCommenttype(Commentinfo.COMMENT_TYPE_ONLY);
			break;
		case MessageTip.TIP_COMMENT_REPLY:
			commentinfo.setCommenttype(Commentinfo.COMMENT_TYPE_REPLY);
			commentinfo.setReplyuserid(messageTip.getUserId());
			commentinfo.setReplyusername(messageTip.getUserName());
			break;
		}
		commentinfo.setContent(messageTip.getContent());
		return commentinfo;
	}

	public String getCommenttime() {
		return commenttime;
	}

	public void setCommenttime(String commenttime) {
		this.commenttime = commenttime;
	}

	public String getReplycommentid() {
		return replycommentid;
	}

	public void setReplycommentid(String replycommentid) {
		this.replycommentid = replycommentid;
	}

	public String getReplyuserid() {
		return replyuserid;
	}

	public void setReplyuserid(String replyuserid) {
		this.replyuserid = replyuserid;
	}

	public String getReplyusername() {
		return replyusername;
	}

	public void setReplyusername(String replyusername) {
		this.replyusername = replyusername;
	}

	public int getCommenttype() {
		return commenttype;
	}

	public void setCommenttype(int commenttype) {
		this.commenttype = commenttype;
	}

	private boolean isComment = false;

	public Commentinfo() {
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public boolean isComment() {
		return isComment;
	}

	public void setComment(boolean isComment) {
		this.isComment = isComment;
	}

}
