package com.wachoo.pangker.server.response;


/**
 * @author: zhengjy
 * @createTime: 2012-1-6
 * @desc 调用搜索引警搜索问题返回结果
 * 调用接口 http://IP..../picindex/picsearch
 */
public class SearchLbsPictureResult implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private PictureResult data;
	private boolean success;
	
	public SearchLbsPictureResult(){}
	
	public SearchLbsPictureResult(PictureResult data, boolean success) {
		super();
		this.data = data;
		this.success = success;
	}

	public PictureResult getData() {
		return data;
	}

	public void setData(PictureResult data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	

}
