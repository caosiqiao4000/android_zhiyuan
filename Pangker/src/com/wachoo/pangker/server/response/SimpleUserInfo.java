package com.wachoo.pangker.server.response;

/**
 * 邀请的用户对象
 * 
 * @author wangxin
 * 
 */
public class SimpleUserInfo {
	public static final int NOT_PANGKER_USER = 0;// >>>>>>非旁客用户
	public static final int PANGKER_USER = 1;// >>>>>>旁客用户
	private String phone;// 用户手机手机号码
	private String userId;// 用户UID
	private String userName;// 用户姓名
	private String nickName;// 用户昵称
	private String sign;// 签名
	private int isPangker;// 0:非旁客用户（未注册) 1：旁客用户
	private String dealTime;// 时间

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public int getIsPangker() {
		return isPangker;
	}

	public void setIsPangker(int isPangker) {
		this.isPangker = isPangker;
	}

	public String getDealTime() {
		return dealTime;
	}

	public void setDealTime(String dealTime) {
		this.dealTime = dealTime;
	}

}
