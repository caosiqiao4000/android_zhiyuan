package com.wachoo.pangker.server.response;

import java.util.List;

@SuppressWarnings("serial")
public class SearchUnderUsersResult extends BaseResult {

	private int count;
	private int sumCount;
	private int pangkerCount;
	private int notPangkerCount;
	private List<SimpleUserInfo> users;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getSumCount() {
		return sumCount;
	}

	public void setSumCount(int sumCount) {
		this.sumCount = sumCount;
	}

	public List<SimpleUserInfo> getUsers() {
		return users;
	}

	public void setUsers(List<SimpleUserInfo> users) {
		this.users = users;
	}

	public int getPangkerCount() {
		return pangkerCount;
	}

	public void setPangkerCount(int pangkerCount) {
		this.pangkerCount = pangkerCount;
	}

	public int getNotPangkerCount() {
		return notPangkerCount;
	}

	public void setNotPangkerCount(int notPangkerCount) {
		this.notPangkerCount = notPangkerCount;
	}

}
