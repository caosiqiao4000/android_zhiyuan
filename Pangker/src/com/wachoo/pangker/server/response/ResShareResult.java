package com.wachoo.pangker.server.response;

public class ResShareResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String shareid;
	
	public ResShareResult() {
		// TODO Auto-generated constructor stub
	}

	public ResShareResult(String shareid) {
		super();
		this.shareid = shareid;
	}

	public String getShareid() {
		return shareid;
	}

	public void setShareid(String shareid) {
		this.shareid = shareid;
	}
	
	

}
