package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.entity.ContactGroup;

/**
 * @author: feijinbo
 * @createTime: 2011-12-9 下午10:35:46
 * @version: 1.0
 * @desc :查询联系人类别、分组信息接口
 */
public class SearchContactInfoResult extends BaseResult {

	private List<ContactType> types = new ArrayList<ContactType>(); // 联系人类别信息
	private List<ContactGroup> groups = new ArrayList<ContactGroup>(); // 联系人组信息

	public List<ContactType> getTypes() {
		return types;
	}

	public void setTypes(List<ContactType> types) {
		this.types = types;
	}

	public List<ContactGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<ContactGroup> groups) {
		this.groups = groups;
	}

}
