package com.wachoo.pangker.server.response;

import java.util.List;


/**
 * 邀请通讯录用户开通旁客返回
 *  zhengjy
 *  
 *  2012-3-28
 */
public class InviteRegisterResult extends BaseResult {
	
	public List<InviteResult> inviteResult;
	
	public InviteRegisterResult(){}

	public InviteRegisterResult(List<InviteResult> inviteResult,int errorCode, String errorMessage) {
		this.inviteResult=inviteResult;
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}

	public List<InviteResult> getInviteResult() {
		return inviteResult;
	}

	public void setInviteResult(List<InviteResult> inviteResult) {
		this.inviteResult = inviteResult;
	}
	
}
