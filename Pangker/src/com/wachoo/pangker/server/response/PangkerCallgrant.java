package com.wachoo.pangker.server.response;
/**
 * @PangkerCallgrant（呼叫权限）
 * 
 * @author zhengjy
 *
 * 2012-7-10
 * 
 */
public class PangkerCallgrant {
	
	
	private Long uid;
	private Integer grantType;
	private String grantInfo;
	private String members;
	private String password;
	
	
	public PangkerCallgrant(){}


	public PangkerCallgrant(Long uid, Integer grantType, String grantInfo,
			String members, String password) {
		super();
		this.uid = uid;
		this.grantType = grantType;
		this.grantInfo = grantInfo;
		this.members = members;
		this.password = password;
	}


	public Long getUid() {
		return uid;
	}


	public void setUid(Long uid) {
		this.uid = uid;
	}


	public Integer getGrantType() {
		return grantType;
	}


	public void setGrantType(Integer grantType) {
		this.grantType = grantType;
	}


	public String getGrantInfo() {
		return grantInfo;
	}


	public void setGrantInfo(String grantInfo) {
		this.grantInfo = grantInfo;
	}


	public String getMembers() {
		return members;
	}


	public void setMembers(String members) {
		this.members = members;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
