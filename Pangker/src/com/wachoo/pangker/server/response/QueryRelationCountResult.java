package com.wachoo.pangker.server.response;

/**
 * 单位通讯录管理接口返回
 * 
 * 接口AddressBookManager.json
 * 
 * @author zhengjy
 * 
 * @version 2012-6-12
 */

public class QueryRelationCountResult extends BaseResult {

	private Integer driftCount;
	private Integer visitorCount;
	private Integer followCount;
	private Integer fansCount;

	public QueryRelationCountResult() {
	}

	public QueryRelationCountResult(Integer driftCount, Integer visitorCount, Integer followCount,
			Integer fansCount) {
		super();
		this.driftCount = driftCount;
		this.visitorCount = visitorCount;
		this.followCount = followCount;
		this.fansCount = fansCount;
	}

	public Integer getFansCount() {
		return fansCount;
	}

	public void setFansCount(Integer fansCount) {
		this.fansCount = fansCount;
	}

	public Integer getDriftCount() {
		return driftCount;
	}

	public void setDriftCount(Integer driftCount) {
		this.driftCount = driftCount;
	}

	public Integer getVisitorCount() {
		return visitorCount;
	}

	public void setVisitorCount(Integer visitorCount) {
		this.visitorCount = visitorCount;
	}

	public Integer getFollowCount() {
		return followCount;
	}

	public void setFollowCount(Integer followCount) {
		this.followCount = followCount;
	}

}
