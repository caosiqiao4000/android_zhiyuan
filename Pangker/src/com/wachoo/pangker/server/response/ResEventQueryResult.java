package com.wachoo.pangker.server.response;

import java.util.List;

/**
 * 黑名单管理接口返回
 * 
 * @author zhengjy
 *
 * @version 2012-6-4
 */

public class ResEventQueryResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int count;
	private int tcount;
	private int c1;
	private int c2;
	private List<EventResult> result;
	
	
	public ResEventQueryResult(){}


	public ResEventQueryResult(int count, int tcount, int c1, int c2,
			List<EventResult> result) {
		super();
		this.count = count;
		this.tcount = tcount;
		this.c1 = c1;
		this.c2 = c2;
		this.result = result;
	}


	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}


	public int getTcount() {
		return tcount;
	}


	public void setTcount(int tcount) {
		this.tcount = tcount;
	}


	public int getC1() {
		return c1;
	}


	public void setC1(int c1) {
		this.c1 = c1;
	}


	public int getC2() {
		return c2;
	}


	public void setC2(int c2) {
		this.c2 = c2;
	}


	public List<EventResult> getResult() {
		return result;
	}


	public void setResult(List<EventResult> result) {
		this.result = result;
	}

}
