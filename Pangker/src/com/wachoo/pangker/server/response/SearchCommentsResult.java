package com.wachoo.pangker.server.response;

import java.util.List;

public class SearchCommentsResult extends BaseResult{

	private static final long serialVersionUID = 1L;
	private int count;
	private int sumCount;
	private List<Commentinfo> commentinfo;

	public SearchCommentsResult() {
		// TODO Auto-generated constructor stub
	}
	
	public List<Commentinfo> getCommentinfo() {
		return commentinfo;
	}

	public void setCommentinfo(List<Commentinfo> commentinfo) {
		this.commentinfo = commentinfo;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getSumCount() {
		return sumCount;
	}

	public void setSumCount(int sumCount) {
		this.sumCount = sumCount;
	}

}
