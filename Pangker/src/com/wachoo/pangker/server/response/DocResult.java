package com.wachoo.pangker.server.response;

import java.util.List;


/**
* @ClassName: DocResult
* @author longxianwen 
* @date Mar 26, 2012 2:28:57 PM
 */
public class DocResult implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	
	private int total;
	private boolean exact;
	private List<DocInfo> data;
	public DocResult() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DocResult(int total, boolean exact, List<DocInfo> data) {
		super();
		this.total = total;
		this.exact = exact;
		this.data = data;
	}

	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public boolean isExact() {
		return exact;
	}
	public void setExact(boolean exact) {
		this.exact = exact;
	}
	public List<DocInfo> getData() {
		return data;
	}
	public void setData(List<DocInfo> data) {
		this.data = data;
	}
}
