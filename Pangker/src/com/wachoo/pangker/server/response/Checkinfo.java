package com.wachoo.pangker.server.response;

public class Checkinfo implements java.io.Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int USER_TYPE_NOTPANGKER = 0;//0.非旁客用户
	public static final int USER_TYPE_PANGKER_NORMAL = 1;//1.旁客用户，正常态
	public static final int USER_TYPE_PANGKER_ABNORMAL = 2;//2.旁客用户，非正常态
	
	private String mobile;
	private Long uid;
	private Integer flag;
	
	public Checkinfo(){}

	public Checkinfo(String mobile, Long uid, Integer flag) {
		super();
		this.mobile = mobile;
		this.uid = uid;
		this.flag = flag;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	
}
