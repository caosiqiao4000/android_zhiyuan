package com.wachoo.pangker.server.response;

import java.io.Serializable;

/**
 *@author wubo
 *@createtime May 15, 2012
 */
public class LocationQueryCheckResult extends BaseResult implements Serializable {

	private String lon;
	private String lat;
	private int userStatus;//10正常11漂移12穿越
	
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public int getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}
	
	
	
}
