package com.wachoo.pangker.server.response;

import java.util.List;


/**
 * 查找单位通讯录接口返回
 * 
 * 接口AddressBookQueryByAccount.json
 * 
 * @author zhengjy
 *
 * @version 2012-6-12
 */

public class AddressBookResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int VERSION_RESULT = 2; //版本号最新时，errorCode == 2
	private Long version;
	private List<AddressBooks> books;
	private List<AddressMembers> members;
	
	public AddressBookResult(){}
	
	public AddressBookResult(int errorCode,String errorMessage, Long version,List<AddressBooks> books,List<AddressMembers> members) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.version = version;
		this.books = books;
		this.members = members;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public List<AddressBooks> getBooks() {
		return books;
	}

	public void setBooks(List<AddressBooks> books) {
		this.books = books;
	}

	public List<AddressMembers> getMembers() {
		return members;
	}

	public void setMembers(List<AddressMembers> members) {
		this.members = members;
	}

	
}
