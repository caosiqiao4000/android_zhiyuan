package com.wachoo.pangker.server.response;

/**
 * 
 * 照片资源
 * 
 * @author wangxin
 * 
 */
public class PicInfo extends MovingRes {

	public static final String TITLE_FIELD = "title";// 主题
	public static final String RESURL_FIELD = "resurl";// 预览图 、缩略图地址

	private String title; // 标题 （名称）
	private int picType;// 图片类型
	private final String resurl = "";// 预览图 、缩略图地址

	
	
	/**
	 * @return the picType
	 */
	public int getPicType() {
		return picType;
	}

	/**
	 * @param picType the picType to set
	 */
	public void setPicType(int picType) {
		this.picType = picType;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the resurl
	 */
	public String getResurl() {
		return resurl;
	}	
}
