package com.wachoo.pangker.server.response;



public class QueryGroupsByIDResult extends BaseResult {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PangkerGroupInfo groupinfo;
	public PangkerGroupInfo getGroupinfo() {
		return groupinfo;
	}
	public void setGroupinfo(PangkerGroupInfo groupinfo) {
		this.groupinfo = groupinfo;
	}
	
}