package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.entity.ContactGroup;

/** 
 * @author yunyf
 * @createTime
 * @desc 好友管理返回结果
 */
public class SearchFriends extends BaseResult {

	private static final long serialVersionUID = 1L;

	private List<ContactGroup> group = new ArrayList<ContactGroup>();

	public List<ContactGroup> getGroup() {
		return group;
	}

	public void setGroup(List<ContactGroup> group) {
		this.group = group;
	}

}
