package com.wachoo.pangker.server.response;

/**
 *@author wubo
 *@createtime 2012-5-22
 */
public class DriftGrantResult extends BaseResult {
	private static final long serialVersionUID = 1L;
	private Double lon;
	private Double lat;
	private Integer userStatus;
	public Double getLon() {
		return lon;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Integer getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}
	public DriftGrantResult(Double lon, Double lat, Integer userStatus) {
		this.lon = lon;
		this.lat = lat;
		this.userStatus = userStatus;
	}
	public DriftGrantResult() {
	}
	public DriftGrantResult(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);
		// TODO Auto-generated constructor stub
	}
	
	
}
