package com.wachoo.pangker.server.response;

import java.util.List;


/**
 * @author zhengjy 
 * 2011-12-29
 * 查询创建的群组信息返回结果
 */
public class QueryVisitedGroupsResult extends BaseResult {
	
	private List<PangkerGroupInfo> groups;

	public List<PangkerGroupInfo> getGroups() {
		return groups;
	}

	public void setGroups(List<PangkerGroupInfo> groups) {
		this.groups = groups;
	}

	public QueryVisitedGroupsResult(List<PangkerGroupInfo> groups) {
		this.groups = groups;
	}

	public QueryVisitedGroupsResult() {
		// TODO Auto-generated constructor stub
	}

	public QueryVisitedGroupsResult(List<PangkerGroupInfo> groups,int errorCode, String errorMessage) {
		this.groups=groups;
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}
	
}
