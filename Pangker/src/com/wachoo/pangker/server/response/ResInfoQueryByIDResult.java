package com.wachoo.pangker.server.response;

public class ResInfoQueryByIDResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ResInfo resourceinfo;

	public ResInfoQueryByIDResult() {
		// TODO Auto-generated constructor stub
	}

	public ResInfoQueryByIDResult(ResInfo resourceinfo) {
		super();
		this.resourceinfo = resourceinfo;
	}

	public ResInfo getResinfo() {
		return resourceinfo;
	}

	public void setResinfo(ResInfo resourceinfo) {
		this.resourceinfo = resourceinfo;
	}
	
	
}
