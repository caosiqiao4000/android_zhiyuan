package com.wachoo.pangker.server.response;


/**
 * 
 * @author wubo
 * @createtime Apr 20, 2012
 */
public class MeetGroup implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Long groupId;// 组唯一标识
	private String uid;//用户uid
	private Long ownUid;// 群组创建者
	private Long groupSid;// 组SID标识
	private String groupName;// 组名称
	private Integer groupType;// 组类别：10:会议室，11：直播室，12：群组讨论组，13：友踪组
	private Integer groupOwn;//拥有者:0:我的群组,1:收藏过的群组,2:参与过的群组

	public MeetGroup() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Long getOwnUid() {
		return ownUid;
	}

	public void setOwnUid(Long ownUid) {
		this.ownUid = ownUid;
	}

	public Long getGroupSid() {
		return groupSid;
	}

	public void setGroupSid(Long groupSid) {
		this.groupSid = groupSid;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getGroupType() {
		return groupType;
	}

	public void setGroupType(Integer groupType) {
		this.groupType = groupType;
	}

	public Integer getGroupOwn() {
		return groupOwn;
	}

	public void setGroupOwn(Integer groupOwn) {
		this.groupOwn = groupOwn;
	}

	public MeetGroup(Integer id, Long groupId, String uid, Long ownUid,
			Long groupSid, String groupName, Integer groupType, Integer groupOwn) {
		super();
		this.id = id;
		this.groupId = groupId;
		this.uid = uid;
		this.ownUid = ownUid;
		this.groupSid = groupSid;
		this.groupName = groupName;
		this.groupType = groupType;
		this.groupOwn = groupOwn;
	}


	
}
