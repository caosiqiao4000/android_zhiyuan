package com.wachoo.pangker.server.response;

import java.util.List;

import com.wachoo.pangker.entity.UserItem;

@SuppressWarnings("serial")
public class SearchRelativeResult extends BaseResult{

	private int count;
	
	private int sumCount;
	
	private List<UserItem> users;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getSumCount() {
		return sumCount;
	}

	public void setSumCount(int sumCount) {
		this.sumCount = sumCount;
	}

	public List<UserItem> getUsers() {
		return users;
	}

	public void setUsers(List<UserItem> users) {
		this.users = users;
	}
	
}
