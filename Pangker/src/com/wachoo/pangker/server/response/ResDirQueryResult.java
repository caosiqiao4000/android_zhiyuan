package com.wachoo.pangker.server.response;

import java.util.List;

import com.wachoo.pangker.entity.DirInfo;
//>>>del
public class ResDirQueryResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<DirInfo> dirinfos;
	
	private int count;
	
	private int totalcount;

	public ResDirQueryResult() {
		// TODO Auto-generated constructor stub
	}
	
	public ResDirQueryResult(List<DirInfo> dirinfos, int count, int totalcount) {
		super();
		this.dirinfos = dirinfos;
		this.count = count;
		this.totalcount = totalcount;
	}

	public List<DirInfo> getDirinfos() {
		return dirinfos;
	}

	public void setDirinfos(List<DirInfo> dirinfos) {
		this.dirinfos = dirinfos;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
	
}
