package com.wachoo.pangker.server.response;


/**
 * @author: chenjd
 * @createTime: 2012-2-10 上午9:52:18
 * @version: 1.0
 * @desc: 群组进入权限校验返回结果类
 */
public class GroupEnterCheckResult extends BaseResult implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private UserGroup userGroup;
	public GroupEnterCheckResult() {
	}
	public UserGroup getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}
	
}
