package com.wachoo.pangker.server.response;

import java.util.List;

/**
 * @author zhengjy
 *
 * @version 2012-4-18 
 */

public class GroupResult implements java.io.Serializable {
	
	private int total;
	private boolean exact;
	private List<GroupLbsInfo> data;
	
	public GroupResult(){}
	
	public GroupResult(int total, boolean exact, List<GroupLbsInfo> data) {
		super();
		this.total = total;
		this.exact = exact;
		this.data = data;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public boolean isExact() {
		return exact;
	}
	public void setExact(boolean exact) {
		this.exact = exact;
	}
	public List<GroupLbsInfo> getData() {
		return data;
	}
	public void setData(List<GroupLbsInfo> data) {
		this.data = data;
	}
}
