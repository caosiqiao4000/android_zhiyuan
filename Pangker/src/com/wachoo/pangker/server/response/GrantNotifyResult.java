package com.wachoo.pangker.server.response;

/**
 * 
 * @权限信息同步接口
 * 
 * @author zhengjy
 *
 *  2012-7-10
 *
 */
public class GrantNotifyResult extends BaseResult{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PangkerCallgrant callgrant;
	private PangkerDriftgrant driftgrant;
	private PangkerLocationgrant locationgrant;

	
	
	public GrantNotifyResult(){}
	
	public GrantNotifyResult(int errorCode,String errorMessage, PangkerCallgrant callgrant,PangkerDriftgrant driftgrant,PangkerLocationgrant locationgrant) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.callgrant = callgrant;
		this.driftgrant = driftgrant;
		this.locationgrant = locationgrant;
	}

	public PangkerCallgrant getCallgrant() {
		return callgrant;
	}

	public void setCallgrant(PangkerCallgrant callgrant) {
		this.callgrant = callgrant;
	}

	public PangkerDriftgrant getDriftgrant() {
		return driftgrant;
	}

	public void setDriftgrant(PangkerDriftgrant driftgrant) {
		this.driftgrant = driftgrant;
	}

	public PangkerLocationgrant getLocationgrant() {
		return locationgrant;
	}

	public void setLocationgrant(PangkerLocationgrant locationgrant) {
		this.locationgrant = locationgrant;
	}
	
	

	
}
