package com.wachoo.pangker.server.response;

/**
 * LoginValidateResult
 * 
 * @author wangxin
 * 
 */
public class LoginValidateResult {
	// -- 返回代码定义， 按项目的规则进行定义。
	public static final int SUCCESS = 1;// 处理成功

	public static final int FAILED = 0; // 处理失败

	public static final int UNDEAL = -1;// 未处理

	public static final int PHONE_UNBIND = 0;// 没有上传电话号码
	public static final int PHONE_BIND = 1;// 上传过电话号码

	public static final int PARAMETER_ERROR = 101;// 参数异常

	public static final int SYSTEM_ERROR = 500;// 系统异常

	public static final String SYSTEM_ERROR_MESSAGE = "Runtime unknown internal error.";

	private int errorCode = -1;// 错误编码
	private int phoneBind = 0; // 是否绑定电话号码
	private String errorMessage;// 错误信息
	private String account;
	private Integer isBookBind = 0;
	private String sysTime;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getPhoneBind() {
		return phoneBind;
	}

	public void setPhoneBind(int phoneBind) {
		this.phoneBind = phoneBind;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Integer getIsBookBind() {
		return isBookBind;
	}

	public void setIsBookBind(Integer isBookBind) {
		this.isBookBind = isBookBind;
	}

	public String getSysTime() {
		return sysTime;
	}

	public void setSysTime(String sysTime) {
		this.sysTime = sysTime;
	}

}
