package com.wachoo.pangker.server.response;

import com.wachoo.pangker.entity.UserInfo;

/**
 * @author: feijinbo
 * @createTime: 2011-12-26 下午3:14:35
 * @version: 1.0
 * @desc :用户基本信息查询结果类
 */
public class UserinfoQueryResult extends BaseResult {

	// 用户基本信息对象
	private UserInfo userInfo;
	//用户积分对象，用户是否会员等,根据后续业务进行扩充.

	/**
	 * @return the userInfo
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * @param userInfo
	 *            the userInfo to set
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

}
