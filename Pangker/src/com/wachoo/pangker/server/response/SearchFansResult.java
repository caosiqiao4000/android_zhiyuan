package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.entity.UserItem;

public class SearchFansResult extends BaseResult implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int count;
	private int sumCount;
	private List<UserItem> users = new ArrayList<UserItem>();

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getSumCount() {
		return sumCount;
	}

	public void setSumCount(int sumCount) {
		this.sumCount = sumCount;
	}

	public List<UserItem> getUsers() {
		return users;
	}

	public void setUsers(List<UserItem> users) {
		this.users = users;
	}
}
