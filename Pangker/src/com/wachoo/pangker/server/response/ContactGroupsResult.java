package com.wachoo.pangker.server.response;

import java.util.List;


/**
 * @author zhengjy 
 * 2011-12-29
 * 查询创建的群组信息返回结果
 */
public class ContactGroupsResult extends BaseResult {
	
	private List<GroupInfo> groups;

	public List<GroupInfo> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupInfo> groups) {
		this.groups = groups;
	}

	public ContactGroupsResult(List<GroupInfo> groups) {
		this.groups = groups;
	}

	public ContactGroupsResult() {
		// TODO Auto-generated constructor stub
	}

	public ContactGroupsResult(List<GroupInfo> groups,int errorCode, String errorMessage) {
		this.groups=groups;
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}

	
	
	
}
