package com.wachoo.pangker.server;

import java.lang.ref.WeakReference;
import java.util.List;

import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.util.Log;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.ui.dialog.WaitingDialog;
import com.wachoo.pangker.util.ConnectivityHelper;
import com.wachoo.pangker.util.Util;

/**
 * 
 * 异步刷新ui类
 * 
 * @author wangxin
 * 
 */
public class SeverSupportTask extends AsyncTask<Object, Object, Object> {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	// private Context context;

	private WeakReference<Context> mContextReference;
	private WeakReference<IUICallBackInterface> callBackInterfaceReference;
	// request url
	private String url;
	// Parameter
	private List<Parameter> paras;
	// pdShow isShow ?
	private boolean pdShow;
	// ProgressDialog show message
	private String pdMsg;
	// ProgressDialog
	private WaitingDialog popupDialog;

	private int caseKey;
	protected SyncHttpClient http = new SyncHttpClient();
	private PangkerApplication application;

	@Override
	protected Object doInBackground(Object... params) {
		// >>>>>>>>>add by wangxin 弱引用防止回收！
		if (isCancelled() || mContextReference.get() == null
				|| callBackInterfaceReference.get() == null) {
			return HttpReqCode.canceled;
		}
		try {
			if (!ConnectivityHelper.isConnectivityAvailable(mContextReference
					.get())) {
				Thread.sleep(500);
				return HttpReqCode.no_network;
			}

			return http.httpPost(url, paras, application.getServerTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
			return HttpReqCode.error;
		}
	}

	/***
	 * 
	 * SeverSupportTask
	 * 
	 * @param context
	 * @param pdShow
	 * @param paras
	 * @param url
	 * @param callBackInterface
	 */
	public SeverSupportTask(Context context,
			IUICallBackInterface callBackInterface, String url,
			List<Parameter> paras, boolean pdShow, String pdMsg, int caseKey) {
		this.application = (PangkerApplication) context.getApplicationContext();
		this.mContextReference = new WeakReference<Context>(context);
		this.callBackInterfaceReference = new WeakReference<IUICallBackInterface>(
				callBackInterface);
		this.url = url;
		this.paras = paras;
		this.pdShow = pdShow;
		this.pdMsg = pdMsg;
		this.caseKey = caseKey;
	}

	@Override
	protected void onPreExecute() {

		if (isCancelled() || mContextReference.get() == null
				|| callBackInterfaceReference.get() == null)
			return;

		if (pdShow) {
			popupDialog = new WaitingDialog(mContextReference.get());
			if (!Util.isEmpty(pdMsg)) {
				popupDialog.setWaitMessage(pdMsg);
			}
			popupDialog.setCancelable(true);
			popupDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					cancel(true);
				}
			});
			popupDialog.show();
		}
	}

	@Override
	protected void onPostExecute(Object result) {
		// Log.i(TAG, "result==" + result);
		Log.i("WifionPostExecute", "isCancelled" + isCancelled()
				+ ";mContextReference.get()" + mContextReference.get()
				+ ";callBackInterfaceReference.get()="
				+ callBackInterfaceReference.get());
		if (isCancelled() || mContextReference.get() == null
				|| callBackInterfaceReference.get() == null) {

			return;
		}
		if (pdShow) {
			popupDialog.dismiss();
		}
		if (callBackInterfaceReference.get() != null && !isCancelled()) {
			callBackInterfaceReference.get().uiCallBack(result, this.caseKey);
		}

	}

	@Override
	protected void onProgressUpdate(Object... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}
}
