package com.wachoo.pangker;

import android.content.Context;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.api.ContactUserListenerManager;
import com.wachoo.pangker.api.TabBroadcastManager;
import com.wachoo.pangker.chat.MessageChangedManager;
import com.wachoo.pangker.chat.UserMsgManager;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.PKIconFetcher;
import com.wachoo.pangker.image.PKIconLoader;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.music.MusicMediaPlayer;
import com.wachoo.pangker.music.MusicPlayerManager;

/***
 * PangkerIMManager
 * 
 * 旁客管理
 * 
 * @author wangxin
 * 
 */
public final class PangkerManager {

	private static ActivityStackManager activityStackManager;
	private static MusicMediaPlayer mediaPlayer;
	private static MusicPlayerManager musicPlayerManager;
	private static ContactUserListenerManager userListenerManager;
	private static TabBroadcastManager tabBroadcastManager;
	private static MessageChangedManager messageChangedManager;
	private static UserMsgManager userMsgManager;

	/**
	 * MusicPlayerManager
	 * 
	 * @return
	 */
	public static MusicPlayerManager getMusicPlayerManager() {
		if (musicPlayerManager == null) {
			musicPlayerManager = new MusicPlayerManager();
		}
		return musicPlayerManager;
	}

	public static MusicMediaPlayer getMediaPlayer() {
		if (mediaPlayer == null) {
			mediaPlayer = new MusicMediaPlayer();
		}
		return mediaPlayer;
	}

	/**
	 * 
	 * ActivityStackManager
	 * 
	 * @return
	 * 
	 * @author wangxin 2012-2-8 下午03:11:28
	 */
	public static ContactUserListenerManager getContactUserListenerManager() {
		if (userListenerManager == null)
			userListenerManager = new ContactUserListenerManager();
		return userListenerManager;
	}

	public static TabBroadcastManager getTabBroadcastManager() {
		if (tabBroadcastManager == null)
			tabBroadcastManager = TabBroadcastManager.getTabBroadcastManager();
		return tabBroadcastManager;
	}

	/**
	 * 
	 * ActivityStackManager
	 * 
	 * @return
	 * 
	 * @author wangxin 2012-2-8 下午03:11:28
	 */
	public static ActivityStackManager getActivityStackManager() {
		if (activityStackManager == null)
			activityStackManager = new ActivityStackManager();
		return activityStackManager;
	}

	public static MessageChangedManager getMessageChangedManager() {
		if (messageChangedManager == null)
			messageChangedManager = new MessageChangedManager();
		return messageChangedManager;
	}

	public static UserMsgManager getUserMsgManager() {
		if (userMsgManager == null)
			userMsgManager = new UserMsgManager();
		return userMsgManager;
	}

	private static PKIconFetcher userIconResizer;
	private static ImageFetcher groupIconResizer;

	public static PKIconResizer getUserIconResizer(Context context) {
		if (userIconResizer == null) {
			// >>>>>>>>初始化头像使用工具
			userIconResizer = new PKIconFetcher(context, PKIconLoader.iconSideLength);
			ImageCacheParams userIconcacheParams = new ImageCacheParams("usericons");
			userIconcacheParams.imageSideLength = PKIconLoader.iconSideLength;
			// The ImageWorker takes care of loading images into our ImageView
			// children asynchronously
			userIconResizer.setLoadingImage(R.drawable.nav_head);
			ImageCache userIconCache = new ImageCache(context, userIconcacheParams);
			userIconResizer.setmSaveDiskCache(true);
			userIconResizer.setImageCache(userIconCache);

		}
		return userIconResizer;
	}

	public static ImageFetcher getGroupIconResizer(Context context) {
		if (groupIconResizer == null) {
			// >>>>>>>>群组头像初始化
			groupIconResizer = new ImageFetcher(context, PKIconLoader.iconSideLength);
			ImageCacheParams groupIconCacheParams = new ImageCacheParams("groupicons");

			groupIconResizer.setLoadingImage(R.drawable.group_icon);
			ImageCache groupIconCache = new ImageCache(context, groupIconCacheParams);
			groupIconResizer.setmSaveDiskCache(true);
			groupIconResizer.setImageCache(groupIconCache);
		}
		return groupIconResizer;
	}

	public static void exitManager() {
		// TODO Auto-generated method stub
		if (musicPlayerManager != null) {
			musicPlayerManager.exitManager();
		}
		if (userListenerManager != null) {
			userListenerManager.exitManager();
		}
		if (tabBroadcastManager != null) {
			tabBroadcastManager.exitManager();
		}
		if (messageChangedManager != null) {
			messageChangedManager.exitManager();
		}
		if (userMsgManager != null) {
			userMsgManager.exitManager();
		}
		if(userIconResizer != null){
			if(userIconResizer.getImageCache() != null){
				userIconResizer.getImageCache().removeMemoryCaches();
			}
			userIconResizer = null;
		}
		if(groupIconResizer != null){
			if(groupIconResizer.getImageCache() != null){
				groupIconResizer.getImageCache().removeMemoryCaches();
			}
			groupIconResizer = null;
		}
	}
}
