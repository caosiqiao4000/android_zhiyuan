package com.wachoo.pangker.activity.res;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.DocReaderActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.DocAdapter;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.QuickAction;

public class ResLocalDocActivity extends ResLocalActivity {

	private Button btnBack;
	private Button btnEdit;
	private ListView docListView;
	private EmptyView emptyView;
	private DocAdapter docAdapter;

	private List<DirInfo> appList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_mmusic);
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		((TextView) findViewById(R.id.mmtitle)).setText("我的阅读");
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		btnEdit = (Button) findViewById(R.id.iv_more);
		btnEdit.setBackgroundResource(R.drawable.btn_menu_bg);
		btnEdit.setOnClickListener(onClickListener);

		docAdapter = new DocAdapter(this, new ArrayList<DirInfo>());
		docListView = (ListView) findViewById(R.id.res_musicListView);
		docListView.setAdapter(docAdapter);
		docListView.setOnItemClickListener(onItemClickListener);

		emptyView = new EmptyView(this);
		emptyView.addToListView(docListView);
		showEmptyView();

		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.addActionItem(new ActionItem(2, getString(R.string.res_local_auto), ActionItem
				.buildDrawable(this, R.drawable.icon64_file)));
		quickAction.addActionItem(new ActionItem(3, getString(R.string.res_local_directory_lookup),
				ActionItem.buildDrawable(this, R.drawable.icon64_file)), true);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);
		initSelectDoc();
	}

	private void toLocalRes() {
		Intent intent = getIntent();
		intent.putExtra("res_type", PangkerConstant.RES_DOCUMENT);
		intent.setClass(this, SdcardResSearchActivity.class);
		startActivityForResult(intent, RequestCode);
	}

	private void toFileRes() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		intent.setClass(this, FileManagerActivity.class);
		intent.putExtra("file_type", PangkerConstant.RES_DOCUMENT);
		startActivityForResult(intent, RequestCode_FILE);
	}
	
	private void showEmptyView() {
		// TODO Auto-generated method stub
        if(application.ismExternalStorageAvailable()){
        	emptyView.showView(R.drawable.emptylist_icon, "提示：您还没有文档,可以通过右上角的菜单进行操作将文档添加进来!");
    		emptyView.setBtnOne(getString(R.string.res_local_auto), oneClickListener);
    		emptyView.setBtnTwo(getString(R.string.res_local_directory_lookup), twoClickListener);
        } else {
        	emptyView.showView(R.drawable.emptylist_icon, "提示：SD卡不存在!");
        }
	}

	View.OnClickListener oneClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toLocalRes();
		}
	};

	View.OnClickListener twoClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toFileRes();
		}
	};

	private void initSelectDoc() {
		// TODO Auto-generated method stub
		appList = application.getResDirInfos(PangkerConstant.RES_DOCUMENT);
		if (appList == null) {
			appList = new ArrayList<DirInfo>();
		}
		docAdapter.setDirInfoLists(appList);
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 2) {
				toLocalRes();
			}
			if (actionId == 3) {
				toFileRes();
			}
		}
	};

	AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			dirInfo = appList.get(position);
			dealFile(dirInfo);
		}
	};

	private void uploadDoc(DirInfo aoFile, boolean ifclose) {
		if(!application.ismExternalStorageAvailable()){
            showToast("Sd卡文件找不到,无法上传!");
			return;
		}
		Intent intent = new Intent();
		intent.putExtra(DocInfo.FILE_PATH, aoFile.getPath());
		
		if (resLocalStatu == ResLocalModel.change) {
			setResult(RESULT_OK, intent);
		} else {
			intent.setClass(this, UploadDocActivity.class);
			startActivity(intent);
			resLocalStatu = ResLocalModel.seeStatu;
		}
		if (ifclose) {
			this.finish();
		}
	}
	
	private void openDoc(DirInfo aoFile) {
		// TODO Auto-generated method stub
		if(!application.ismExternalStorageAvailable()){
			showToast("SD卡不存在，无法查看!");
			return;
		}
		// 如果是文本格式文件
		if (checkEnds(aoFile.getDirName(), getResources().getStringArray(R.array.textEnds))) {
			Intent intent = new Intent();
			intent.setClass(this, DocReaderActivity.class);
			intent.putExtra("DirInfo", aoFile);
			startActivityForResult(intent, RequestCode_Reader);
		} else if (checkEnds(aoFile.getDirName(), getResources().getStringArray(R.array.docEnds))) {
			Intent intent = new Intent();
			intent.setClass(this, DocReaderActivity.class);
			intent.putExtra("DirInfo", aoFile);
			startActivityForResult(intent, RequestCode_Reader);
		} else {
			showToast("其他格式文件还在开发中...");
		}
	}

	/* 处理上传和打开阅读 */
	private void dealFile(DirInfo aoFile) {
		// TODO Auto-generated method stub
		if (resLocalStatu == ResLocalModel.upLoad || resLocalStatu == ResLocalModel.change) {
			uploadDoc(aoFile, true);
		} else if (resLocalStatu == ResLocalModel.seeStatu) {
			openDoc(aoFile);
		}
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				onBackPressed();
			}
			if (v == btnEdit) {
				quickAction.show(btnEdit);
			}
		}
	};

	/**
	 * 检查文件扩展名
	 * 
	 * @param checkItsEnd
	 * @param fileEndings
	 * @return
	 */
	private boolean checkEnds(String checkItsEnd, String[] fileEndings) {
		for (String aEnd : fileEndings) {
			if (checkItsEnd.toLowerCase().endsWith(aEnd))
				return true;
		}
		return false;
	}

	public class UiEdit implements View.OnClickListener {

		private Button btnAll;
		private Button btnCancel;
		private Button btnDelete;
		private boolean isSelectedAll = false;

		public UiEdit() {
			// TODO Auto-generated constructor stub
			btnAll = (Button) findViewById(R.id.local_all);
			btnAll.setOnClickListener(this);
			btnCancel = (Button) findViewById(R.id.local_cancel);
			btnCancel.setOnClickListener(this);
			btnDelete = (Button) findViewById(R.id.local_conf);
			btnDelete.setOnClickListener(this);
		}

		private void selectAll() {
			isSelectedAll = !isSelectedAll;
			int count = docAdapter.getCount();
			if (isSelectedAll) {
				btnAll.setText("反选");
				btnDelete.setEnabled(true);
				docAdapter.setSelectedAll(isSelectedAll);
				btnDelete.setText("删除(" + count + ")");
			} else {
				btnAll.setText("全选");
				btnDelete.setEnabled(false);
				docAdapter.setSelectedAll(isSelectedAll);
				btnDelete.setText("删除( 0 )");
			}
			if(count == 0){
				btnDelete.setEnabled(false);
			}
		}

		private void deleteDoc() {
			List<DirInfo> lAppInfos = docAdapter.getSeletedMembers();
			for (DirInfo dirInfo : lAppInfos) {
				if (iResDao.deleteSelectResInfo(dirInfo)) {
					appList.remove(dirInfo);
				}
			}
			docAdapter.init();
			btnDelete.setEnabled(false);
			btnDelete.setText("删除(0)");
			docAdapter.notifyDataSetChanged();
			showEmptyView();
		}

		public void showEdit(boolean flag) {
			if (flag) {
				resLocalStatu = ResLocalModel.Edit;
				findViewById(R.id.local_operate).setVisibility(View.VISIBLE);
				btnDelete.setEnabled(false);
				btnDelete.setText("删除(0)");
			} else {
				resLocalStatu = ResLocalModel.seeStatu;
				findViewById(R.id.local_operate).setVisibility(View.GONE);
			}
			docAdapter.setFlagShow(flag);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnAll) {
				selectAll();
			}
			if (v == btnCancel) {
				showEdit(false);

			}
			if (v == btnDelete) {
				deleteDoc();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RequestCode && resultCode == RequestCode) {
			initSelectDoc();
		}
		if (requestCode == RequestCode_Reader) {
			int remark = data.getIntExtra("remark", 0);
			Log.d("remark", "===>" + remark);
			dirInfo.setRemark(remark);
			iResDao.updateResInfo(dirInfo);
			docAdapter.notifyDataSetChanged();
		}
		if (requestCode == RequestCode_FILE && resultCode == RESULT_OK) {
			String docpath = data.getExtras().getString(DocInfo.FILE_PATH);
			File docFile = new File(docpath);
			if (!docFile.exists()) {
				return;
			}
			if (!iResDao.isSelected(userId, docpath, PangkerConstant.RES_DOCUMENT)) {
				DirInfo dirInfo = new DirInfo();
				dirInfo.setPath(docpath);
				dirInfo.setDeal(DirInfo.DEAL_SELECT);
				dirInfo.setUid(userId);
				dirInfo.setIsfile(DirInfo.ISFILE);
				dirInfo.setResType(PangkerConstant.RES_DOCUMENT);
				dirInfo.setDirName(docFile.getName());
				dirInfo.setFileCount((int) docFile.length());

				iResDao.saveSelectResInfo(dirInfo);
				docAdapter.addDirInfo(dirInfo);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
}
