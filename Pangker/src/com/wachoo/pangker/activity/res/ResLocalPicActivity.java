package com.wachoo.pangker.activity.res;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.MyAlbumAdapter;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.QuickAction;

public class ResLocalPicActivity extends ResLocalActivity {

	private GridView picGridView;
	private EmptyView picEmptyView;
	private MyAlbumAdapter imageAdapter;
	private Button btnBack;
	private Button btnEdit;

	private List<DirInfo> picLists;
	// >>>>>>>>>获取本地图片
	private ImageLocalFetcher localFetcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_mpic);

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = false;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>>>加载
		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		localFetcher.setmSaveDiskCache(false);
		localFetcher.setImageCache(mImageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);

		initView();
		initSelectPic();
	}

	private void initView() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		btnEdit = (Button) findViewById(R.id.iv_more);
		btnEdit.setBackgroundResource(R.drawable.btn_menu_bg);
		btnEdit.setOnClickListener(onClickListener);
		TextView title = (TextView) findViewById(R.id.mmtitle);
		title.setText("我的图片");
		btnEdit.setOnClickListener(onClickListener);

		picGridView = (GridView) findViewById(R.id.PicGridView);
		imageAdapter = new MyAlbumAdapter(this, localFetcher);
		picGridView.setAdapter(imageAdapter);
		picGridView.setOnItemClickListener(picOnItemClickListener);

		picEmptyView = new EmptyView(this);
		picEmptyView.addToGridView(picGridView);
		showPicEmptyView();

		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.addActionItem(new ActionItem(2, getString(R.string.res_local_auto), ActionItem.buildDrawable(this,
				R.drawable.icon64_file)));
		quickAction.addActionItem(
				new ActionItem(3, getString(R.string.res_local_directory_lookup), ActionItem.buildDrawable(this,
						R.drawable.icon64_file)), true);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);
	}

	private void showPicEmptyView() {
		// TODO Auto-generated method stub
		if (application.ismExternalStorageAvailable()) {
			picEmptyView.showView(R.drawable.emptylist_icon, "提示：您还没有图片,可以通过右上角的菜单进行操作将图片添加进来!");
			picEmptyView.setBtnOne(getString(R.string.res_local_auto), oneClickListener);
			picEmptyView.setBtnTwo(getString(R.string.res_local_directory_lookup), twoClickListener);
		} else {
			picEmptyView.showView(R.drawable.emptylist_icon, "提示：SD卡不存在!");
		}
	}

	private void initSelectPic() {
		// TODO Auto-generated method stub
		picLists = application.getResDirInfos(PangkerConstant.RES_PICTURE);
		if (picLists == null) {
			picLists = new ArrayList<DirInfo>();
		}
		imageAdapter.setLocalDirInfos(picLists);
	}

	private void toLocalPicRes() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");
		ResLocalPicActivity.this.startActivityForResult(intent, 1);
	}

	private void toPicFileRes() {
		// TODO Auto-generated method stub
		Intent intent2 = getIntent();
		intent2.setClass(this, FileManagerActivity.class);
		intent2.putExtra("file_type", PangkerConstant.RES_PICTURE);
		ResLocalPicActivity.this.startActivityForResult(intent2, RequestCode_FILE);
	}

	View.OnClickListener oneClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toLocalPicRes();
		}
	};

	View.OnClickListener twoClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toPicFileRes();
		}
	};

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 2) {
				toLocalPicRes();
			}
			if (actionId == 3) {
				toPicFileRes();
			}
		}
	};

	private void uploadPic(DirInfo item, boolean isclose) {
		if (!application.ismExternalStorageAvailable()) {
			showToast("Sd卡文件找不到,无法上传!");
			return;
		}
		Intent intent = getIntent();
		intent.putExtra(DocInfo.FILE_PATH, item.getPath());
		if (resLocalStatu == ResLocalModel.change) {
			setResult(RESULT_OK, intent);
		} else {
			intent.setClass(this, UploadPicActivity.class);
			startActivity(intent);
			resLocalStatu = ResLocalModel.seeStatu;
		}
		if (isclose) {
			this.finish();
		}
	}

	private void dealPicFile(DirInfo picInfo, int position) {
		// TODO Auto-generated method stub
		if (resLocalStatu == ResLocalModel.upLoad || resLocalStatu == ResLocalModel.change) {
			uploadPic(picInfo, true);
		}
	}

	AdapterView.OnItemClickListener picOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			DirInfo item = (DirInfo) parent.getItemAtPosition(position);
			dealPicFile(item, position);
		}
	};

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.iv_more: // 退出浏览图片状态,进入删除状态
				quickAction.show(btnEdit);
				break;
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		// if (requestCode == RequestCode && resultCode == RequestCode) {
		// initSelectPic();
		// }
		if (requestCode == 1 && resultCode == RESULT_OK) {
			Uri photoUri = data.getData();
			if (photoUri != null) {
				String filePath = getPhotoPath(photoUri);
				DirInfo dirInfo = new DirInfo();
				if (filePath != null) {
					dirInfo.setPath(filePath);
				}
				if (!iResDao.isSelected(userId, dirInfo.getPath(), PangkerConstant.RES_PICTURE)) {
					dirInfo.setDeal(DirInfo.DEAL_SELECT);
					dirInfo.setUid(userId);
					dirInfo.setIsfile(DirInfo.ISFILE);
					dirInfo.setResType(PangkerConstant.RES_PICTURE);
					iResDao.saveSelectResInfo(dirInfo);
					imageAdapter.addPidInfo(dirInfo);

				}
			}
		}
		if (requestCode == RequestCode_FILE && resultCode == RESULT_OK) {
			String picpath = data.getExtras().getString(DocInfo.FILE_PATH);
			File picFile = new File(picpath);
			if (!picFile.exists()) {
				return;
			}
			if (!iResDao.isSelected(userId, picpath, PangkerConstant.RES_PICTURE)) {
				DirInfo dirInfo = new DirInfo();
				dirInfo.setPath(picpath);
				dirInfo.setDeal(DirInfo.DEAL_SELECT);
				dirInfo.setUid(userId);
				dirInfo.setIsfile(DirInfo.ISFILE);
				dirInfo.setResType(PangkerConstant.RES_PICTURE);
				dirInfo.setDirName(picFile.getName());
				dirInfo.setFileCount((int) picFile.length());

				iResDao.saveSelectResInfo(dirInfo);
				imageAdapter.addPidInfo(dirInfo);
			}
		}

	}

	/**
	 * 获取图片在sd卡中的路径
	 * 
	 * @param uri
	 * @return String
	 */
	public String getPhotoPath(Uri uri) {
		String[] proj = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID };
		Cursor cursor = managedQuery(uri, proj, null, null, null);
		if (cursor == null) {
			return null;
		}
		int colum_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		String path = cursor.getString(colum_index);
		return path;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		mImageCache.removeMemoryCaches();
		super.onDestroy();
	}
}
