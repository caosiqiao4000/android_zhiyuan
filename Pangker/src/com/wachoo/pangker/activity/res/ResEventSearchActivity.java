package com.wachoo.pangker.activity.res;

import mobile.http.Parameter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;

/**
 * 资源动态区间获取
 * 
 * zhengjy
 * 
 * 2012-06-06
 *
 */
public class ResEventSearchActivity extends CommonPopActivity{
	
	protected static final long REQ_TIME = 1000 * 60 * 120; // 默认相隔两个小时
	protected static final int incremental = 20;
	protected View loadmoreView;
	protected LinearLayout lyLoadPressBar;
	protected LinearLayout lyLoadText;
	protected TextView loadText;
	
	private int startLimit = 0;
	private int endLimit = 0;
	private boolean isLoadAll = false;
	protected String endtime;//上次获取资源动态的时间，如果没有默认获取两小时前
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	// >>>>>> getSearchLimit每次加载incremental个
	protected Parameter getSearchLimit() {
		endLimit = startLimit + incremental ;
		return new Parameter("limit", startLimit + "," + endLimit);
	}
	
	// >>>>>>>>>> setStartLimit
	public void setStartLimit(int startLimit) {
		this.startLimit = startLimit;
	}
	
	// >>>>>>>>> setStartLimit
	public void resetStartLimit(){
		startLimit = endLimit + 1;
	}
	//刷新，重新查询
	public void reset(){
		startLimit = 0;
		isLoadAll = false;
	}
	/**
	 * 后台数据是否已经加载完成
	 * @return
	 */
	public boolean isLoadAll(){
		return isLoadAll;
	}

	public void setLoadAll(boolean isLoadAll) {
		this.isLoadAll = isLoadAll;
	}

	public int getStartLimit() {
		return startLimit;
	}

	protected void initPressBar(ListView mListView){
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		loadmoreView = inflater.inflate(R.layout.loadmore_footerview, null);
		mListView.addFooterView(loadmoreView);
		lyLoadPressBar = (LinearLayout)loadmoreView.findViewById(R.id.ly_loadmore_probar);
		lyLoadText = (LinearLayout)loadmoreView.findViewById(R.id.ly_loadmore_text);
		loadText = (TextView)loadmoreView.findViewById(R.id.tv_loadmore);
		
	}
	
	protected void setPresdBarVisiable(boolean isShow){
		if(isShow){
			lyLoadPressBar.setVisibility(View.VISIBLE);
			lyLoadText.setVisibility(View.GONE);
		}
		else {
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.VISIBLE);
		}
		
	}

}
