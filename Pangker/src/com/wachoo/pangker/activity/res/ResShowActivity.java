package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.os.Handler;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.CommentAdapter;
import com.wachoo.pangker.adapter.NoCommentAdapter;
import com.wachoo.pangker.db.IDownloadDao;
import com.wachoo.pangker.db.impl.DownloadDaoImpl;
import com.wachoo.pangker.downupload.DownLoadManager;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.MessageTip;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.listener.TextButtonWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.Commentinfo;
import com.wachoo.pangker.server.response.ResDirQueryResult;
import com.wachoo.pangker.server.response.ResInfo;
import com.wachoo.pangker.server.response.ResInfoQueryByIDResult;
import com.wachoo.pangker.server.response.SearchCommentsResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EditTextKeyborad;
import com.wachoo.pangker.ui.EditTextKeyborad.IKeyboardListener;
import com.wachoo.pangker.ui.FooterView.OnLoadMoreListener;
import com.wachoo.pangker.ui.KeyboardLayout;
import com.wachoo.pangker.ui.ListLinearLayout;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.ListDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * 对两个资源的方法进行同步
 * 
 * @author zxx
 */
public abstract class ResShowActivity extends CommonPopActivity implements IUICallBackInterface,
		QuickAction.OnActionItemClickListener {

	protected int SERACH_TYPE = 0;// 查看类型，1：搜索引擎；2：网盘资源；3：分享资源; 4:编辑模式，不能滑动
	// 搜索引擎：只能查看，无能是否是自己的资源;
	// 网盘资源：分享/取消分享，删除资源；编辑名称、描述
	// 分享资源：取消分享删除资源：如果是自己上传的资源 / 取消转播：如果是转播的资源

	protected KeyboardLayout mKeyboardLayout;

	protected final int pageLength = 10;

	protected final int SUBMIT_COMMENT = 0x34;
	protected final int SEARCH_COMMENT = 0x33;
	protected final int DEL_COMMENT = 0x37;
	protected int KEY_RESINFO = 0x31;
	protected int O_DOWN = 0x33;// 是否下载自己上传的资源

	// protected int NET_NOTICE = 4;// 是否下载自己上传的资源
	protected int NET_SET = 7;// 是否下载自己上传的资源
	protected final int DIR_SEARCH = 0x10;

	protected final int KEY_COLLECT = 0x21;
	protected final int KEY_TRANS = 0x22;
	protected final int KEY_RATINGS = 0x23;

	protected final int KEY_COOMENT = 0x301;
	protected final int KEY_USERINFO = 0x302;
	protected final int KEY_EDITRES = 0x303;
	protected final int KEY_DELETE = 0x304;
	protected final int KEY_SHARE = 0x305;
	protected final int KEY_FORWARD = 0x306;
	protected PangkerApplication application;
	protected ServerSupportManager serverMana;// 后台交互
	protected String userId;
	protected UserInfo userInfo;
	protected ResInfo resInfo;

	protected DownLoadManager downloadManager;// 下载管理
	protected boolean isDownloaded = false;// 示是否上传、下载过，能不能找到sdPath

	// 下载取消以及暂停通知界面
	// protected int flag_download = 0; // 0:表示没有下载过，1：正在下载2：下载完成;3：有自己上传
	protected PKIconResizer mImageResizer;

	protected IDownloadDao downloadDao;// 下载的dao
	protected ResShowEntity resShowEntity;
	protected ListDialog dirDialog;
	private int resType;
	private RatingBar ratingbar;
	private TextView txtScore;
	protected Dialog ratingPopup;
	protected int mScore = 0;// RatingBar评分

	protected ViewFlipper mViewFlipper;
	protected GestureOverlayView mGestureOverlayView;
	protected ViewFlipper pViewFlipper;// 评论用的动画效果
	protected QuickAction quickAction;
	protected boolean isLoading = false;

	// protected int picType = 0;
	protected ActionItem editActionItem = new ActionItem(3, "进行编辑");
	protected ActionItem shareActionItem = new ActionItem(1, "分享资源");

	protected SharedPreferencesUtil spu;
	static final String RESSHOW_ENTITY_KEY = "RESSHOW_ENTITY_KEY";
	protected UICommentSubmit uiSubmit;
	public UIComment uiComment;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		spu.saveString(RESSHOW_ENTITY_KEY, JSONUtil.toJson(resShowEntity, false));
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		resShowEntity = JSONUtil.fromJson(spu.getString(RESSHOW_ENTITY_KEY, ""), ResShowEntity.class);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		application = (PangkerApplication) getApplicationContext();
		spu = new SharedPreferencesUtil(this);

		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
		serverMana = new ServerSupportManager(this, this);

		userId = application.getMyUserID();
		userInfo = application.getMySelf();

		downloadDao = new DownloadDaoImpl(this);
		downloadManager = application.getDownLoadManager();

		if (application.getResLists() == null || application.getResLists().size() <= 0) {
			showToast("Err!");
			// finish();
			return;
		}

		resShowEntity = application.getResLists().get(application.getViewIndex());
		if (resShowEntity == null) {
			showToast("Err!");
			// finish();
			return;
		}
		initType();
		initPop();
	}

	protected String timesText(String times) {
		return "(" + times + ")";
	}

	private void initType() {
		// TODO Auto-generated method stub
		SERACH_TYPE = getIntent().getIntExtra("SERACH_TYPE", 0);
		if (SERACH_TYPE == 2) {
			quickAction = new QuickAction(this, QuickAction.VERTICAL);
			quickAction.setOnActionItemClickListener(this);
		} else if (SERACH_TYPE == 3) {
			quickAction = new QuickAction(this, QuickAction.VERTICAL);
			quickAction.setOnActionItemClickListener(this);
		}
	}

	MessageTipDialog mTipDialog;

	protected void showDownloadDialog() {
		// TODO Auto-generated method stub
		mTipDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
			@Override
			public void buttonResult(boolean isSubmit) {
				// TODO Auto-generated method stub
				if (isSubmit) {
					download(true, false);
				}
			}
		}, this);
		mTipDialog.showDialog("", getStringByID(R.string.downfile_exist));
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		application.getGestureHandler().setRegister(null);
		super.onDestroy();
	}

	// 初始化评分界面
	public void initPop() {
		// TODO Auto-generated method stub
		ratingPopup = new Dialog(this, R.style.MyDialog);
		ratingPopup.setContentView(R.layout.rating_pop);

		Button cancelLayout = (Button) ratingPopup.findViewById(R.id.pop_cancel_layout);
		cancelLayout.setOnClickListener(opopClickListener);
		Button scoreLayout = (Button) ratingPopup.findViewById(R.id.res_score_layout);
		scoreLayout.setOnClickListener(opopClickListener);
		ratingbar = (RatingBar) ratingPopup.findViewById(R.id.ratingBar1);
		txtScore = (TextView) ratingPopup.findViewById(R.id.txtScore);
		ratingbar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
				mScore = (int) (rating * 2); // 1 个 rating代表一个星，一个星代表1分
				txtScore.setText("评分：" + rating);
			}
		});
	}

	protected void toggleComment() {
		if (pViewFlipper.getDisplayedChild() == 1) {
			showComment();
		} else {
			pViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_from_top));
			pViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_to_top));
			pViewFlipper.setDisplayedChild(1);
		}
	}

	protected void showComment() {
		// TODO Auto-generated method stub
		if (pViewFlipper.getDisplayedChild() == 1) {
			pViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_from_bottom));
			pViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_to_bottom));
			pViewFlipper.setDisplayedChild(0);
		}
	}

	private DirInfo getRootIdByType(int type) {
		List<DirInfo> rootDirs = application.getRootDirs();
		if (rootDirs == null) {
			return null;
		}
		for (DirInfo dirInfo : rootDirs) {
			if (dirInfo.getResType() == type) {
				return dirInfo;
			}
		}
		return null;
	}

	protected String getSavePath() {
		switch (resShowEntity.getResType()) {
		case PangkerConstant.RES_APPLICATION:
			return PangkerConstant.DIR_APP;
		case PangkerConstant.RES_MUSIC:
			return PangkerConstant.DIR_MUSIC;
		case PangkerConstant.RES_PICTURE:
			return PangkerConstant.DIR_PICTURE;
		case PangkerConstant.RES_DOCUMENT:
			return PangkerConstant.DIR_DOCUMENT;
		}
		return PangkerConstant.DIR_MUSIC;
	}

	// 根据资源Id获取资源信息
	protected void getResInfo() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		if (resShowEntity.getShareUid() != null) {
			paras.add(new Parameter("suid", resShowEntity.getShareUid()));
		} else {
			Log.e("shareUid", "error>>> No shareUid, please check code!!!!!");
		}
		paras.add(new Parameter("resid", String.valueOf(resShowEntity.getResID())));
		sendServer(paras, Configuration.getResInfoQueryByID(), true, KEY_RESINFO);
		resInfo = null;
		isLoading = true;
	}

	protected void searchComment() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("resid", String.valueOf(resShowEntity.getResID())));
		paras.add(new Parameter("type", "1"));
		paras.add(new Parameter("limit", uiComment.adapter.getCount() + "," + pageLength));
		serverMana.supportRequest(Configuration.getSearchComments(), paras, SEARCH_COMMENT);
	}

	protected void resEditSave(String resName, String resDexc) {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", String.valueOf(resShowEntity.getResID())));
		paras.add(new Parameter("resName", resName));
		paras.add(new Parameter("resDesc", resDexc));
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getResInfoUpdate(), paras, true, mess, KEY_EDITRES);
	}

	// 资源删除
	protected void deleteRes() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		long resID = resShowEntity.getResID();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", String.valueOf(resID)));
		paras.add(new Parameter("dirid", resShowEntity.getDirid()));
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getDeleteRes(), paras, true, mess, KEY_DELETE);
	}

	protected void ushareResInfo(String shareId) {
		String mess = getResourcesMessage(R.string.please_wait);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("optype", "1"));
		if (shareId == null) {
			paras.add(new Parameter("resid", String.valueOf(resShowEntity.getResID())));
			// 取消分享
			if (resInfo.ifShare == 1) {
				serverMana.supportRequest(Configuration.getCancelShare(), paras, true, mess, KEY_SHARE);
			}
			// 分享
			if (resInfo.ifShare == 0) {
				paras.add(new Parameter("type", "1"));
				serverMana.supportRequest(Configuration.getResShare(), paras, true, mess, KEY_SHARE);
			}
		} else {
			paras.add(new Parameter("shareid", shareId));
			serverMana.supportRequest(Configuration.getCancelShare(), paras, true, mess, KEY_FORWARD);
		}

	}

	/**
	 * 资源评论接口
	 * @param content  评论内容 请求数目 接口：AddComment.json
	 */
	protected void submitCooment(Commentinfo commentinfo, String restype) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", commentinfo.getUserid()));

		// >>>>当前用户名称
		paras.add(new Parameter("username", commentinfo.getUsername()));
		paras.add(new Parameter("commenttype", String.valueOf(commentinfo.getCommenttype())));
		// >>>>>>>>>>判断是否是回复case
		if (commentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_REPLY) {
			// >>>>>回复的用户ID
			paras.add(new Parameter("replyuserid", commentinfo.getReplyuserid()));
			paras.add(new Parameter("replyusername", commentinfo.getReplyusername()));
			// >>>>>回复的评论ID
			paras.add(new Parameter("commentid", commentinfo.getReplycommentid()));
		}
		paras.add(new Parameter("resid", String.valueOf(resShowEntity.getResID())));
		paras.add(new Parameter("type", restype));// 操作id
		paras.add(new Parameter("content", commentinfo.getContent()));// 回答内容
		serverMana.supportRequest(Configuration.getAddComment(), paras, true, "正在提交...", SUBMIT_COMMENT);
	}

	protected void delComment(String commentId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("commentid", commentId));// 问题id
		paras.add(new Parameter("type", "1"));// 操作id
		serverMana.supportRequest(Configuration.getDeleteCommenturl(), paras, true, "正在提交...", DEL_COMMENT);
	}

	/**
	 * 搜索第二层目录
	 * 
	 * @param type
	 */
	protected void searchDirInfo(int type) {
		this.resType = type;
		DirInfo dir = getRootIdByType(type);
		if (dir != null) {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appuid", userId));
			paras.add(new Parameter("uid", userId));
			paras.add(new Parameter("dirid", dir.getDirId()));
			paras.add(new Parameter("limit", "0,10"));
			serverMana.supportRequest(Configuration.getResDirQueryByDir(), paras, true, "正在发送...", DIR_SEARCH);
		} else {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appuid", userId));
			paras.add(new Parameter("uid", userId));
			paras.add(new Parameter("type", String.valueOf(type)));
			paras.add(new Parameter("limit", "0,10"));
			serverMana.supportRequest(Configuration.getDirQueryByType(), paras, true, "正在发送...", DIR_SEARCH);
		}
	}

	OnItemClickListener onDItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			DirInfo dirInfo = (DirInfo) parent.getItemAtPosition(position);
			dirDialog.dismiss();
			saveForMe(dirInfo.getDirId());
		}
	};

	OnClickListener opopClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.res_score_layout) {
				ratingServer("2", mScore);
			}
			ratingbar.setRating(0);
			txtScore.setText("评分：" + 0);
			ratingPopup.dismiss();
		}
	};

	protected Parameter getLongitude() {
		if (application.getLocateType() == PangkerConstant.LOCATE_CLIENT) {
			return new Parameter("longitude", String.valueOf(application.getCurrentLocation().getLongitude()));
		}
		return new Parameter("longitude", String.valueOf(application.getDirftLocation().getLongitude()));
	}

	// >>>>>>>>>>> getLatitude
	protected Parameter getLatitude() {
		if (application.getLocateType() == PangkerConstant.LOCATE_CLIENT) {
			return new Parameter("latitude", String.valueOf(application.getCurrentLocation().getLatitude()));
		}
		return new Parameter("latitude", String.valueOf(application.getDirftLocation().getLatitude()));
	}

	/**
	 * 资源收藏为已有，将他人资源收藏为本人资源
	 */
	protected void saveForMe(String dirid) {
		resInfo.setFavorTimes(resInfo.getFavorTimes() + 1);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", String.valueOf(resInfo.getSid())));
		paras.add(new Parameter("type", "0"));
		paras.add(new Parameter("dirid", dirid));
		paras.add(getLongitude());
		paras.add(getLatitude());
		serverMana.supportRequest(Configuration.getGarnerRes(), paras, KEY_COLLECT);
	}

	// 资源转播
	protected void transRes() {
		resInfo.setShareTimes(resInfo.getShareTimes() + 1);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("resid", String.valueOf(resShowEntity.getResID())));
		paras.add(new Parameter("optype", "1"));
		paras.add(new Parameter("type", "2"));
		sendServer(paras, Configuration.getResShare(), true, KEY_TRANS);
	}

	// 发表评分
	protected void ratingServer(String optype, int score) {
		if (resInfo.score == null) {
			resInfo.score = 0l;
		}
		if (resInfo.appraiseTimes == null) {
			resInfo.appraiseTimes = 0;
		}
		resInfo.score += score;
		resInfo.appraiseTimes += 1;
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("score", "" + score));
		paras.add(new Parameter("resid", String.valueOf(resShowEntity.getResID())));
		paras.add(new Parameter("optype", optype));
		sendServer(paras, Configuration.getAppraiseRes(), true, KEY_RATINGS);
	}

	private void sendServer(List<Parameter> paras, String httpurl, boolean flag, int key) {
		serverMana.supportRequest(httpurl, paras, flag, "", key);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		if (!HttpResponseStatus(response)) {
			if (caseKey == KEY_RESINFO) {
				isLoading = false;
			}
			callbackErr(caseKey);
			return;
		}
		if (caseKey == KEY_RESINFO) {// 获取资源信息
			isLoading = false;
			ResInfoQueryByIDResult result = JSONUtil.fromJson(response.toString(), ResInfoQueryByIDResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showResInfo(result.getResinfo());
			} else {
				callbackErr(caseKey);
			}
		}
		if (caseKey == DIR_SEARCH) {
			ResDirQueryResult result = JSONUtil.fromJson(response.toString(), ResDirQueryResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showDir(result.getDirinfos());
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
	}

	private void showDir(List<DirInfo> dirinfos) {
		// TODO Auto-generated method stub
		if (dirinfos == null) {
			dirinfos = new ArrayList<DirInfo>();
		}
		dirDialog = new ListDialog(this, R.style.MyDialog);
		dirDialog.show();
		dirDialog.setOnItemClickListener(onDItemClickListener);
		dirDialog.setTitle("目录选择");
		if (getRootIdByType(resType) != null) {
			dirinfos.add(0, getRootIdByType(resType));
		}
		dirDialog.showDir(dirinfos);
	}

	private void goUserInfo(String userId) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, userId);
		startActivity(intent);
	}

	public class UIUserInfo implements View.OnClickListener {

		public ImageView iv_ownericon;
		public TextView txt_ownername;
		public TextView txt_uploadtime;

		public ImageView iv_broadcastericon;
		public TextView txt_broadcastername;
		public TextView txt_broadcasttime;

		public ImageView iv_share;

		public void hideBroader() {
			findViewById(R.id.ly_broadcast).setVisibility(View.GONE);
		}

		public UIUserInfo() {
			// TODO Auto-generated constructor stub
			iv_ownericon = (ImageView) findViewById(R.id.iv_res_ownericon);
			iv_ownericon.setOnClickListener(this);
			txt_ownername = (TextView) findViewById(R.id.tv_res_ownername);
			txt_uploadtime = (TextView) findViewById(R.id.tv_uploadtime);

			iv_broadcastericon = (ImageView) findViewById(R.id.iv_res_broadcastericon);
			iv_broadcastericon.setOnClickListener(this);
			txt_broadcastername = (TextView) findViewById(R.id.tv_res_boradcastername);
			txt_broadcasttime = (TextView) findViewById(R.id.tv_boradcaster_uploadtime);

			iv_share = (ImageView) findViewById(R.id.iv_share);
		}

		// getReshareUserName
		public void showData() {
			// 资源是否 收藏
			if (resShowEntity.isKeep()) {
				mImageResizer.loadImage(String.valueOf(resInfo.getResOwnerUid()), iv_ownericon);
				txt_ownername.setText("源发布人:" + resInfo.getResOwnerNickname());
			} else {
				mImageResizer.loadImage(resInfo.getUid(), iv_ownericon);
				txt_ownername.setText("源发布人:" + resInfo.getShareUserName());
				// 资源是否转播
				// >>>>>>如果资源发现用户UID为空、null、0不显示,如果发现用户就是资源的上传用户
				if (Util.isEmpty(resShowEntity.getShareUid())// >>>>>>为空 or null
						|| "0".equals(resShowEntity.getShareId())// >>>>>为 0
						|| resShowEntity.getShareUid().equals(resInfo.getUid())) { // >>>>>当前资源上传用户
					findViewById(R.id.ly_broadcast).setVisibility(View.GONE);
				} else {
					findViewById(R.id.ly_broadcast).setVisibility(View.VISIBLE);
					mImageResizer.loadImage(resShowEntity.getShareUid(), iv_broadcastericon);
					txt_broadcastername.setText(resInfo.getReshareUserName());
					if (resInfo.getShareTime() != null) {
						txt_broadcasttime.setText(Util.TimeIntervalBtwNow(resInfo.getShareTime()) + "转播");
					}
				}
			}

			txt_uploadtime.setText("上传:" + Util.getShowTimes(resInfo.getIssuetime()));
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.iv_res_broadcastericon) {
				goUserInfo(resShowEntity.getShareUid());
			}
			if (v.getId() == R.id.iv_res_ownericon) {
				if (resInfo != null) {
					goUserInfo(resInfo.getUid());
				}
			}
		}
	}

	private PopMenuDialog descDialog;
	ActionItem[] msgMenu1 = new ActionItem[] { new ActionItem(1, "复制") };

	private void showDescMenu() {
		// TODO Auto-generated method stub
		if (descDialog == null) {
			descDialog = new PopMenuDialog(this);
			descDialog.setListener(descClickListener);
			descDialog.setTitle("描述信息");
			descDialog.setMenus(msgMenu1);
		}
		descDialog.show();
	}

	protected String getResinfoDesc() {
		// TODO Auto-generated method stub
		switch (resShowEntity.getResType()) {
		case PangkerConstant.RES_DOCUMENT:
			if (!Util.isEmpty(resInfo.docDesc)) {
				return resInfo.docDesc;
			}
			break;
		case PangkerConstant.RES_MUSIC:
			if (!Util.isEmpty(resInfo.musicdesc)) {
				return resInfo.musicdesc;
			}
			break;
		case PangkerConstant.RES_APPLICATION:
			if (!Util.isEmpty(resInfo.appdesc)) {
				return resInfo.appdesc;
			}
			break;
		case PangkerConstant.RES_PICTURE:
			if (!Util.isEmpty(resInfo.getPicDesc())) {
				return resInfo.getPicDesc();
			}
			break;
		}
		return null;
	}

	UITableView.ClickListener descClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			if (actionid == 1) {
				String edsc = getResinfoDesc();
				if (!Util.isEmpty(edsc)) {
					ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
					clip.setText(getResinfoDesc());
					showToast("复制成功");
				}
			}
			descDialog.dismiss();
		}
	};
	// 显示复制描述的功能
	protected View.OnLongClickListener descLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			showDescMenu();
			return true;
		}
	};

	public class UIComment implements View.OnClickListener, UITableView.ClickListener, View.OnLongClickListener {

		private Commentinfo commentinfo;
		private CommentAdapter adapter;
		private NoCommentAdapter adapter2;
		private TextView txtCount;
		private TextView txtLoad;
		private ListLinearLayout commentLayout;
		private LinearLayout comment_layout;
		private PopMenuDialog menuDialog;
		private Handler handler = new Handler();

		public UIComment() {
			// TODO Auto-generated constructor stub
			txtCount = (TextView) findViewById(R.id.txt_count);
			txtLoad = (TextView) findViewById(R.id.txt_load);
			commentLayout = (ListLinearLayout) findViewById(R.id.comment_view);
			commentLayout.setOnLongClickListener(this);
			commentLayout.setClickable(false);
			commentLayout.setFocusable(false);
			commentLayout.setOnClickListener(this);

			comment_layout = (LinearLayout) findViewById(R.id.comment_layout);
			adapter = new CommentAdapter(ResShowActivity.this, new ArrayList<Commentinfo>());
			adapter2 = new NoCommentAdapter(ResShowActivity.this);
		}

		public void showSerach() {
			// TODO Auto-generated method stub
			commentLayout.setShowLoadmore(false);
			comment_layout.setVisibility(View.GONE);
			adapter.clearCommentinfos();
			txtLoad.setVisibility(View.VISIBLE);
			txtLoad.setText("正在加载...");
			uiSubmit.etComment.setText("");
		}

		public void serachCommentFail() {
			// TODO Auto-generated method stub
			txtLoad.setText("加载失败!");
		}

		private OnLoadMoreListener onLoadMoreListener = new OnLoadMoreListener() {

			@Override
			public void onLoadMoreListener() {
				// TODO Auto-generated method stub
				searchComment();
			}
		};

		// >>>>>>>>>增加评论信息
		public void addComment(final Commentinfo commentinfo) {
			handler.post(new Runnable() {

				@Override
				public void run() {
					adapter.addComment(commentinfo);
					comment_layout.setVisibility(View.VISIBLE);
					commentLayout.setAdapter(adapter);
					uiSubmit.etComment.setText("");
					commentCountAdd();
				}
			});
		}

		public void deleteComment() {
			// TODO Auto-generated method stub
			adapter.delComment(commentinfo);
			if (adapter.getCount() > 0) {
				commentLayout.setVisibility(View.VISIBLE);
				txtLoad.setVisibility(View.GONE);
				commentLayout.setAdapter(adapter);
			} else {
				commentLayout.setAdapter(adapter2);
				commentLayout.setShowLoadmore(false);
			}
			commentCountDel();
		}

		private void commentCountAdd() {
			int commentCount = Util.String2Integer(txtCount.getText().toString()) + 1;
			txtCount.setText(String.valueOf(commentCount));
		}

		private void commentCountDel() {
			int commentCount = Util.String2Integer(txtCount.getText().toString()) - 1;
			if (commentCount > 0)
				txtCount.setText(String.valueOf(commentCount));
			else
				txtCount.setText("0");
		}

		// >>>>>>>>>>>>>显示评论信息
		public void showComments(SearchCommentsResult commentResult) {
			comment_layout.setVisibility(View.VISIBLE);
			txtLoad.setVisibility(View.GONE);
			if (commentResult != null && commentResult.errorCode == BaseResult.SUCCESS) {
				if (commentResult.getCommentinfo() != null && commentResult.getCommentinfo().size() > 0) {
					txtCount.setText(String.valueOf(commentResult.getSumCount()));
					if (adapter.getCount() == 0) {
						adapter.setCommentinfos(new ArrayList<Commentinfo>());
					}
					adapter.addCommentinfos(commentResult.getCommentinfo());
					if (commentResult.getSumCount() > adapter.getCount()) {
						commentLayout.setShowLoadmore(true);
						commentLayout.setOnLoadMoreListener(onLoadMoreListener);
					} else {
						commentLayout.setShowLoadmore(false);
					}
					commentLayout.setAdapter(adapter);
				} else {
					txtCount.setText("0");
					commentLayout.setShowLoadmore(false);
					commentLayout.setAdapter(adapter2);
				}
			} else {
				txtCount.setText("0");
				commentLayout.setShowLoadmore(false);
				commentLayout.setAdapter(adapter2);
			}
		}

		ActionItem[] copyMenu = new ActionItem[] { new ActionItem(1, "复制") };

		private void showCopyMenu() {
			menuDialog = new PopMenuDialog(ResShowActivity.this, R.style.MyDialog);
			menuDialog.setListener(this);
			menuDialog.setTitle("评论信息");
			menuDialog.setMenus(copyMenu);
			menuDialog.show();
		}

		ActionItem[] showCopyAndDeleteMenu = new ActionItem[] { new ActionItem(1, "复制"), new ActionItem(2, "删除") };

		private void showCopyAndDeleteMenu() {
			menuDialog = new PopMenuDialog(ResShowActivity.this, R.style.MyDialog);
			menuDialog.setListener(this);
			menuDialog.setTitle("评论信息");
			menuDialog.setMenus(showCopyAndDeleteMenu);
			menuDialog.show();
		}

		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			if (adapter.getCount() == 0) {
				return false;
			}
			commentinfo = (Commentinfo) v.getTag();
			// >>>>>>如果是自己的评论 或者是自己上传的资源
			if (commentinfo.getUserid().equals(userId) || String.valueOf(resInfo.getUid()).equals(userId)) {
				showCopyAndDeleteMenu();
			} else {
				showCopyMenu();
			}
			return false;
		}

		@Override
		public void onClick(int actionId) {
			// >>>>>>>复制评论操作
			if (actionId == 1) {
				ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				Log.i(TAG, "clip=" + clip);
				Log.i(TAG, "commentinfo=" + commentinfo);
				clip.setText(commentinfo.getContent());
			}
			// >>>>>>>>删除评论操作
			if (actionId == 2) {
				delComment(commentinfo.getCommentId());
			}
			menuDialog.dismiss();
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (adapter.getCount() == 0) {
				return;
			}
			commentinfo = (Commentinfo) v.getTag();
			// >>>>>>>如果是自己的不需要做任何动作
			if (!commentinfo.getUserid().equals(userId)) {
				uiSubmit.etComment.requestFocus();
				uiSubmit.popSoftKeyboard();
				uiSubmit.setCommentType(Commentinfo.COMMENT_TYPE_REPLY);
				uiSubmit.setEtCommentHint("回复" + commentinfo.getUsername());
				uiSubmit.setReplyCommentinfo(commentinfo);
			} else
				showToast("这是您自己的留言");
		}

	}

	class UICommentSubmit implements View.OnClickListener {

		public EditTextKeyborad etComment;// 评论内容
		public Button btn_sendComment;// 发送评论
		public Button btn_submit;// 修改
		private int commentType;// >>>>>评论类型
		private int softKeyboardPopLocationY = 0;

		// >>>>>>>>当前评论
		private Commentinfo newCommentinfo;
		// >>>>>>>>回复之前的评论
		private Commentinfo replyCommentinfo;

		public void popSoftKeyboard() {
			((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).showSoftInput(this.etComment, 0);
			int[] location = new int[2];
			pViewFlipper.getLocationOnScreen(location);
			this.softKeyboardPopLocationY = location[1];
		}

		public Commentinfo getNewCommentinfo() {
			return newCommentinfo;
		}

		public void setNewCommentinfo(Commentinfo newCommentinfo) {
			this.newCommentinfo = newCommentinfo;
		}

		public Commentinfo getReplyCommentinfo() {
			return replyCommentinfo;
		}

		public void setReplyCommentinfo(Commentinfo replyCommentinfo) {
			this.replyCommentinfo = replyCommentinfo;
		}

		private IKeyboardListener keyboardListener = new IKeyboardListener() {

			@Override
			public void hide() {
				// TODO Auto-generated method stub
				uiSubmit.setCommentType(Commentinfo.COMMENT_TYPE_ONLY);
				uiSubmit.setEtCommentHint(getString(R.string.comment));
			}
		};

		// public void setCommentTypeOnlyComment() {
		// int[] location = new int[2];
		// pViewFlipper.getLocationOnScreen(location);
		// int softKeyboardLocationY = location[1];
		//
		// // >>>>>>>>如果不是弹出的设置为评论
		// if (this.softKeyboardPopLocationY != softKeyboardLocationY) {
		// uiSubmit.setCommentType(Commentinfo.COMMENT_TYPE_ONLY);
		// uiSubmit.setEtCommentHint(getString(R.string.comment));
		// }
		// }

		public int getCommentType() {
			return commentType;
		}

		public void setCommentType(int commentType) {
			this.commentType = commentType;
		}

		public void setEtCommentHint(String etCommentHint) {
			this.etComment.setHint(etCommentHint);
		}

		public UICommentSubmit() {
			// TODO Auto-generated constructor stub
			pViewFlipper = (ViewFlipper) findViewById(R.id.comment_flipper);
			pViewFlipper.setDisplayedChild(0);
			etComment = (EditTextKeyborad) findViewById(R.id.et_foot_editer);
			btn_sendComment = (Button) findViewById(R.id.btn_foot_pubcomment);
			btn_sendComment.setEnabled(false);
			btn_sendComment.setOnClickListener(this);
			etComment.addTextChangedListener(new TextButtonWatcher(btn_sendComment));
			btn_submit = (Button) findViewById(R.id.btn_submit);
			btn_submit.setOnClickListener(this);
			etComment.setKeyboardListener(keyboardListener);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (resInfo == null) {
				showToast("正在加载,还不能进行该操作,请稍等!");
				return;
			}
			if (v == btn_sendComment) {
				String comment = etComment.getText().toString();
				if (!Util.isEmpty(comment)) {
					this.newCommentinfo = new Commentinfo();
					this.newCommentinfo.setCommenttype(getCommentType());
					this.newCommentinfo.setContent(comment);
					this.newCommentinfo.setUserid(userId);
					this.newCommentinfo.setUsername(application.getMyUserName());
					this.newCommentinfo.setCommenttime(Util.getNowTime());
					// >>>>>是否是回复评论
					if (this.newCommentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_REPLY) {
						this.newCommentinfo.setReplycommentid(getReplyCommentinfo().getCommentId());
						this.newCommentinfo.setReplyuserid(getReplyCommentinfo().getUserid());
						this.newCommentinfo.setReplyusername(getReplyCommentinfo().getUsername());
					}
					submitCooment(this.newCommentinfo, "1");
				} else {
					showToast("发送的内容不能为空!");
				}
			}
			if (v == btn_submit) {
				editResinfo();
			}
		}
	}

	protected void sendMessTip(Commentinfo commentinfo, String userID, int messageTipType) {
		// TODO Auto-generated method stub
		final MessageTip messageTip = new MessageTip();
		messageTip.setCommentID(commentinfo.getCommentId());
		messageTip.setUserId(userID);
		messageTip.setFromId(userId);
		messageTip.setFromName(application.getMyUserName());
		messageTip.setResId(String.valueOf(resShowEntity.getResID()));
		messageTip.setResType(resShowEntity.getResType());
		messageTip.setResName(resShowEntity.getResName());
		messageTip.setTipType(messageTipType);
		messageTip.setContent(commentinfo.getContent());
		messageTip.setCreateTime(Util.getSysNowTime());
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				application.getOpenfireManager().sendMessageTip(messageTip);
			}
		}).start();
	}

	protected void sendMessageTip() {
		Commentinfo commentinfo = uiSubmit.getNewCommentinfo();
		if (commentinfo != null) {

			String commentReplyUserID = null;
			// >>>>二、对其他评论的评论
			if (commentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_REPLY) {
				commentReplyUserID = commentinfo.getReplyuserid();
				sendMessTip(commentinfo, commentReplyUserID, MessageTip.TIP_COMMENT_REPLY);
			}

			// >>>发送给转播人 如果转播人和当前回复的评论人是同一个用户，就不需要通知了
			if (!Util.isEmpty(resShowEntity.getShareUid()) && !resShowEntity.getShareUid().equals(resInfo.getUid())) {
				if (!userId.equals(resShowEntity.getShareUid())
						&& !resShowEntity.getShareUid().equals(commentReplyUserID))
					sendMessTip(commentinfo, resShowEntity.getShareUid(), MessageTip.TIP_COMMENT_BROADCAST_RES);
			}

			// >>>>>>发送给资源上传用户 如果资源上传用户和当前回复的评论人是同一个用户，就不需要通知了
			if (!userId.equals(String.valueOf(resInfo.getUid()))
					&& !String.valueOf(resInfo.getUid()).equals(commentReplyUserID))
				sendMessTip(commentinfo, String.valueOf(resInfo.getUid()), MessageTip.TIP_COMMENT_RES);

		}
	}

	/**
	 * 判断是否是当前资源
	 * 
	 * @param resID
	 * @return
	 */
	public boolean isCurrentResource(long resID) {
		if (resID == resShowEntity.getResID())
			return true;

		return false;
	}

	public abstract void editResinfo();

	public abstract void callbackErr(int caseKey);

	/**
	 * 
	 * @param b
	 *            是否删除原来存在的文件，重新下载
	 * @param isopen
	 *            是否下载完成之后直接打开资源 true 打开 false 不做任何操作
	 */
	public abstract void download(boolean b, boolean isopen);

	public abstract void showResInfo(ResInfo resinfo);

}
