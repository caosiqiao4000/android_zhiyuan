package com.wachoo.pangker.activity.res;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.LocalApkAdapter;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.util.Util;

/**
 * 在SD卡抽出的时候做相应的处理
 * 
 * @author zxx
 */
public class ResLocalAppActivity extends ResLocalActivity {

	private Button btnBack;
	private Button btnEdit;
	private ListView appListView;
	private EmptyView appEmptyView;
	private LocalApkAdapter appAdapter;
	private List<DirInfo> appList;
	private ImageApkIconFetcher imageApkIconFetcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_mmusic);
		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = false;
		mImageCache = new ImageCache(this, imageCacheParams);

		imageApkIconFetcher = new ImageApkIconFetcher(this);
		imageApkIconFetcher.setmSaveDiskCache(false);
		imageApkIconFetcher.setImageCache(mImageCache);
		imageApkIconFetcher.setLoadingImage(R.drawable.icon46_apk);
		initView();
	}

	private void initView() {
		((TextView) findViewById(R.id.mmtitle)).setText("我的应用");
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		btnEdit = (Button) findViewById(R.id.iv_more);
		btnEdit.setBackgroundResource(R.drawable.btn_menu_bg);
		btnEdit.setOnClickListener(onClickListener);

		appAdapter = new LocalApkAdapter(this, new ArrayList<DirInfo>(), imageApkIconFetcher);
		appListView = (ListView) findViewById(R.id.res_musicListView);
		appListView.setOnItemClickListener(onItemClickListener);

		appEmptyView = new EmptyView(this);
		appEmptyView.addToListView(appListView);

		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.addActionItem(new ActionItem(2, getString(R.string.res_local_auto), ActionItem.buildDrawable(this,
				R.drawable.icon64_file)));
		quickAction.addActionItem(
				new ActionItem(3, getString(R.string.res_local_directory_lookup), ActionItem.buildDrawable(this,
						R.drawable.icon64_file)), true);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);
		appListView.setAdapter(appAdapter);

		initdata(application.ismExternalStorageAvailable());
	}

	View.OnClickListener oneClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toLocalRes();
		}
	};

	View.OnClickListener twoClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toFileRes();
		}
	};

	private void showEmptyView() {
		// TODO Auto-generated method stub
		if (application.ismExternalStorageAvailable()) {
			appEmptyView.showView(R.drawable.emptylist_icon, "提示：您还没有应用,可以通过右上角的菜单进行操作将应用添加进来!");
			appEmptyView.setBtnOne(getString(R.string.res_local_auto), oneClickListener);
			appEmptyView.setBtnTwo(getString(R.string.res_local_directory_lookup), twoClickListener);
		} else {
			appEmptyView.showView(R.drawable.emptylist_icon, "提示：SD卡不存在!");
		}
	}

	private void initdata(boolean isEisxtsdk) {
		appList = application.getResDirInfos(PangkerConstant.RES_APPLICATION);
		if (appList == null) {
			appList = new ArrayList<DirInfo>();
		}
		appAdapter.setDirInfoLists(appList);
		showEmptyView();
	}

	private void toLocalRes() {
		Intent intent = getIntent();
		intent.putExtra("res_type", PangkerConstant.RES_APPLICATION);
		intent.setClass(this, SdcardResSearchActivity.class);
		startActivityForResult(intent, RequestCode);
	}

	private void toFileRes() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		intent.setClass(this, FileManagerActivity.class);
		intent.putExtra("file_type", PangkerConstant.RES_APPLICATION);
		startActivityForResult(intent, RequestCode_FILE);
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 2) {
				toLocalRes();
			}
			if (actionId == 3) {
				toFileRes();
			}
		}
	};

	private void dealFile(DirInfo appInfo) {
		// TODO Auto-generated method stub
		if (resLocalStatu == ResLocalModel.upLoad || resLocalStatu == ResLocalModel.change) {
			uploadAppInfo(appInfo, true);
		} else {
			Util.instalApplication(ResLocalAppActivity.this, appInfo.getPath());
		}
	}

	/* checkbox选中一个/单击上传 */
	AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			DirInfo appInfo = (DirInfo) parent.getItemAtPosition(position);
			dealFile(appInfo);
		}
	};

	/* 上传本地程序 */
	private void uploadAppInfo(DirInfo appInfo, boolean ifclose) {
		// TODO Auto-generated method stub
		if (!application.ismExternalStorageAvailable()) {
			showToast("SD卡文件不存在，无法上传!");
			return;
		}
		Intent intent = new Intent();
		intent.putExtra(DocInfo.FILE_PATH, appInfo.getPath());
		if (appInfo.getDirName().contains("apk")) {
			intent.putExtra(DocInfo.FILE_NAME, appInfo.getDirName());
		} else {
			intent.putExtra(DocInfo.FILE_NAME, appInfo.getDirName() + ".apk");
		}
		if (resLocalStatu == ResLocalModel.change) {
			setResult(RESULT_OK, intent);
		} else {
			intent.setClass(this, UploadAppActivity.class);
			startActivity(intent);
			resLocalStatu = ResLocalModel.seeStatu;
		}
		if (ifclose) {
			this.finish();
		}
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == btnBack) {
				if (resLocalStatu == ResLocalModel.upLoad) {
					setResult(0);
				}
				finish();
			}
			if (v == btnEdit) { //
				quickAction.show(btnEdit);
			}
		}
	};

	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == RequestCode && resultCode == RequestCode) {
			initdata(application.ismExternalStorageAvailable());
		}

		if (requestCode == RequestCode_FILE && resultCode == RESULT_OK) {
			String apkpath = intent.getExtras().getString(DocInfo.FILE_PATH);
			File apkFile = new File(apkpath);
			if (!apkFile.exists()) {
				return;
			}
			if (!iResDao.isSelected(userId, apkpath, PangkerConstant.RES_APPLICATION)) {
				DirInfo dirInfo = new DirInfo();
				dirInfo.setPath(apkpath);
				dirInfo.setDeal(DirInfo.DEAL_SELECT);
				dirInfo.setUid(userId);
				dirInfo.setIsfile(DirInfo.ISFILE);
				dirInfo.setResType(PangkerConstant.RES_APPLICATION);
				dirInfo.setDirName(apkFile.getName());
				dirInfo.setFileCount((int) apkFile.length());

				iResDao.saveSelectResInfo(dirInfo);
				appAdapter.addDirInfo(dirInfo);
			}
		}
	}

}
