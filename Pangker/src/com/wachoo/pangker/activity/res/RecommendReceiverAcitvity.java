package com.wachoo.pangker.activity.res;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.util.Util;

public abstract class RecommendReceiverAcitvity extends CommonPopActivity {
	private RelativeLayout mLinearLayout;
	private ImageView mFromUserIcon;
	private TextView mFromUserName;
	private TextView mFromReason;
	private ImageView mDismissIcon;
	private PKIconResizer mImageResizer;

	// >>>>>>>>播放类型 0：默认播放音乐 1：资源推荐过来播放音乐
	private int mResOpenType = DEFAULT_PALY;
	public final static int DEFAULT_PALY = 0;//
	public final static int RECOMMEND_OPEN = 1;//
	public final static String RES_OPEN_TYPE = "RES_OPEN_TYPE";//
	public final static String RECOMMEND_INFO = "recommendInfo";//

	static final String TAG = "RecommendReceiverAcitvity";

	private RecommendInfo mRecommendInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	protected void initRecommendView() {
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());

		// >>>>>获取播放类型
		mResOpenType = getIntent().getIntExtra(RES_OPEN_TYPE, DEFAULT_PALY);
		if (mResOpenType == RECOMMEND_OPEN) {
			mRecommendInfo = (RecommendInfo) getIntent().getSerializableExtra(RECOMMEND_INFO);
			initRecommendView(mRecommendInfo);
		}
	}

	private void initRecommendView(RecommendInfo recommendInfo) {
		// TODO Auto-generated method stub
		// >>>>>设置显示
		if (mLinearLayout == null)
			mLinearLayout = (RelativeLayout) findViewById(R.id.ly_recommend_info);
		mLinearLayout.setVisibility(View.VISIBLE);
		// >>>>>推荐人头像
		if (mFromUserIcon == null)
			mFromUserIcon = (ImageView) findViewById(R.id.iv_user_icon);
		mImageResizer.loadImage(recommendInfo.getFromId(), mFromUserIcon);
		mFromUserIcon.setOnClickListener(onClickListener);
		// >>>>>推荐人用户名
		if (mFromUserName == null)
			mFromUserName = (TextView) findViewById(R.id.tv_username);
		mFromUserName.setText(recommendInfo.getFromName());
		// >>>>>推荐人：推荐理由
		if (mFromReason == null)
			mFromReason = (TextView) findViewById(R.id.tv_res_detail);
		// >>>>>>>>推荐理由
		String reaSon = recommendInfo.getReason();
		if (Util.isEmpty(reaSon)) {
			reaSon = "对方很懒，什么都没有跟您讲。";
		}
		mFromReason.setText("推荐理由:" + reaSon);

		// >>>>>>>>隐藏按钮
		if (mDismissIcon == null) {
			mDismissIcon = (ImageView) findViewById(R.id.iv_dismiss_icon);
		}
		mDismissIcon.setOnClickListener(onClickListener);
	}

	private View.OnClickListener onClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// >>>>>>消失按钮
			if (v == mDismissIcon) {
				if (mLinearLayout != null)
					mLinearLayout.setVisibility(View.GONE);
			}
			// >>>>>>>>>>
			if (v == mFromUserIcon) {
				Intent intent = new Intent(RecommendReceiverAcitvity.this, UserWebSideActivity.class);
				intent.putExtra("userid", mRecommendInfo.getFromId());
				intent.putExtra("username", mRecommendInfo.getFromName());

				startActivity(intent);
			}
		}
	};

	// >>>>刷新UI界面
	private Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			initRecommendView((RecommendInfo) msg.obj);
		};
	};

	protected void refreshUI(RecommendInfo recommendInfo) {
		Log.i(TAG, recommendInfo.toString());
		Message message = mHandler.obtainMessage(1, recommendInfo);
		mHandler.sendMessage(message);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public abstract void refreshRecommendView(RecommendInfo recommendInfo);
}
