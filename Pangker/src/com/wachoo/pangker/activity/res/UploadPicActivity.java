package com.wachoo.pangker.activity.res;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.PicturePreviewActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.about.AgreementActivity;
import com.wachoo.pangker.activity.msg.MsgLoactionActivity;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.listener.CheckBoxChangedListener;
import com.wachoo.pangker.listener.TextLimitWatcher;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class UploadPicActivity extends UploadActivity {

	private String TAG = "PicUploadActivity";// log tag
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	public static String ONLY_CAMERA_KEY = "ONLY_CAMERA_KEY";
	private ImageView imgUserIcon;

	private Button btnBack;
	private TextView tvTitle;
	private TextView tv_pic_location;
	private ImageView imageview;
	private TextView txtTime;
	private LinearLayout typeLayout;
	private Button btnType;
	private TextView txtType;
	private EditText txtPicName; // 音乐名称
	private TextView txtNameCount;
	private Button btnClear;

	private EditText etPhotoDetail;
	// private Spinner spinner;
	private Button btnAlbum;
	private Button btnCamera;
	// private EditText etPicName;
	private CheckBox cbIfShare;

	private String[] picTypes;
	private String[] queTypes;
	// 目录选择
	private LinearLayout selectLayout;
	private LinearLayout locationLayout;
	// 选择的目录
	private PKIconResizer mImageResizer;
	// >>>>>>>>>是否进入时直接打开相机
	private boolean only_camera = false;
	private Bitmap mBitmap;

	private String upPoi;

	// >>> dir_layout_info
	private LinearLayout mDirLinearLayout;
	// >>>location_info
	private LinearLayout mLocationLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.pic_upload);
		init();
		initView();
		initData(savedInstanceState);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(this);
		if (id == 1) {
			builder.setSingleChoiceItems(queTypes, 0, dlistener);
			return builder.create();
		}
		return super.onCreateDialog(id);
	}

	DialogInterface.OnClickListener dlistener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			txtType.setText(queTypes[which]);
			uploadType = picTypes[which];
			dialog.dismiss();
		}
	};

	private void init() {
		// TODO Auto-generated method stub
		queTypes = getResources().getStringArray(R.array.res_pic_type);
		picTypes = getResources().getStringArray(R.array.res_pic_value);
		uploadType = picTypes[0];
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		only_camera = getIntent().getBooleanExtra(UploadPicActivity.ONLY_CAMERA_KEY, false);

		ctrlon = application.getCurrentLocation().getLongitude();
		ctrlat = application.getCurrentLocation().getLatitude();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		hideInputMethod(txtPicName);
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (mBitmap != null && !mBitmap.isRecycled()) {
			Log.d(TAG, "imageview.getDrawingCache().recycle()");
			mBitmap.recycle();
		}
		super.onDestroy();
	}

	private void initView() {
		// TODO Auto-generated method stub
		imgUserIcon = (ImageView) findViewById(R.id.usericon);
		mImageResizer.loadImage(userId, imgUserIcon);

		imageview = (ImageView) findViewById(R.id.res_iv_photo);
		imageview.setOnClickListener(onClickListener);
		btnBack = (Button) findViewById(R.id.btn_back);
		// 设置activity标题
		tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText(R.string.res_upload_photo_title);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		btnUpload = (Button) findViewById(R.id.btn_upload);
		btnUpload.setOnClickListener(onClickListener);

		txtPicName = (EditText) findViewById(R.id.et_file_name);
		txtNameCount = (TextView) findViewById(R.id.txt_name_count);
		txtPicName.addTextChangedListener(new TextLimitWatcher(MaxNameLenght, txtNameCount, txtPicName));
		btnClear = (Button) findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(onClickListener);

		tv_pic_location = (TextView) findViewById(R.id.tv_pic_location);
		if (!Util.isEmpty(upPoi)) {
			tv_pic_location.setText(upPoi);
		}
		txtTime = (TextView) findViewById(R.id.TextViewPicDateTime);

		etPhotoDetail = (EditText) findViewById(R.id.et_photo_detail);

		btnAlbum = (Button) findViewById(R.id.btn_pic_album);
		btnCamera = (Button) findViewById(R.id.btn_pic_camera);
		typeLayout = (LinearLayout) findViewById(R.id.type_layout);
		typeLayout.setOnClickListener(onClickListener);
		txtType = (TextView) findViewById(R.id.tv_type);
		btnType = (Button) findViewById(R.id.btn_type_select);
		btnType.setOnClickListener(onClickListener);

		btnAlbum.setOnClickListener(onClickListener);
		btnCamera.setOnClickListener(onClickListener);
		btnBack.setOnClickListener(onClickListener);

		cbIfShare = (CheckBox) findViewById(R.id.cb_ifshare);
		txtDirName = (TextView) findViewById(R.id.txtDirname);
		// uiUpload = new UIUpload();
		selectLayout = (LinearLayout) findViewById(R.id.dir_layout);
		mDirLinearLayout = (LinearLayout) findViewById(R.id.dir_layout_info);
		selectLayout.setOnClickListener(onClickListener);
		mDirLinearLayout.setOnClickListener(onClickListener);

		mLocationLayout = (LinearLayout) findViewById(R.id.location_info);
		mLocationLayout.setOnClickListener(onClickListener);
		locationLayout = (LinearLayout) findViewById(R.id.location_layout);
		locationLayout.setOnClickListener(onClickListener);

		groupLayout = (LinearLayout) findViewById(R.id.groupLayout);
		txtGroupName = (TextView) findViewById(R.id.txt_groupname);

		cbTerms = (CheckBox) findViewById(R.id.checkbox_item);
		cbTerms.setOnCheckedChangeListener(new CheckBoxChangedListener(btnUpload, promptDialog));
		initUploadButton();
		tvAgreement = (TextView) findViewById(R.id.tv_agreement);
		tvAgreement.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(UploadPicActivity.this, AgreementActivity.class);
				intent.putExtra(AgreementActivity.START_FLAG, AgreementActivity.UPLOAD_RES);
				UploadPicActivity.this.startActivity(intent);
			}
		});
	}

	private void initData(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(groupId)) {
			groupLayout.setVisibility(View.VISIBLE);
			txtGroupName.setText(groupName);
		}
		if (savedInstanceState != null) {
			only_camera = savedInstanceState.getBoolean(UploadPicActivity.ONLY_CAMERA_KEY);
			showToast("请重新选择图源!");
		}
		// 判断是否有选择的图片进行上传
		filePath = getIntent().getStringExtra(DocInfo.FILE_PATH);
		if (filePath != null) {
			showImageWithPath();
		} else {
			// >>>>>打开相机
			if (only_camera) {
				openCamera(false);
			}
			// >>>>>图片选择上传
			else {
				showImageDialog("图片上传", actionItems2, false);
			}
		}

		initDir();
		if (only_camera) {
			btnAlbum.setVisibility(View.INVISIBLE);
		}
	}

	private void showImageWithPath() {
		mBitmap = ImageUtil.decodeFile(filePath, PangkerConstant.PANGKER_ICON_SIDELENGTH,
				PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN * PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		imageview.setImageBitmap(mBitmap);
		txtTime.setVisibility(View.VISIBLE);
		txtTime.setText("时间：" + Util.getSysNowTime());
	}

	private void initDir() {
		// TODO Auto-generated method stub
		sDirInfo = application.getRootDir(PangkerConstant.RES_PICTURE);
		if (sDirInfo != null) {
			selectDirId = sDirInfo.getDirId();
		} else {
			PangkerManager.getTabBroadcastManager().addMsgListener(this);
			initDirService();
		}
		String dirName = sDirInfo != null ? sDirInfo.getDirNames() : "请获取目录";
		txtDirName.setText(dirName);
	}

	@Override
	public void doImageBitmap(String imageLocalPath) {
		// TODO Auto-generated method stub
		if (only_camera && Util.isEmpty(imageLocalPath)) {
			finish();
			return;
		}
		this.filePath = imageLocalPath;
		Intent intent = new Intent(this, PicturePreviewActivity.class);
		intent.putExtra("photo_path", filePath);
		intent.putExtra("photo_type", 1);
		startActivityForResult(intent, PangkerConstant.RES_PICTURE);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == POI_CODE && resultCode == RESULT_OK) {
			Location location = (Location) data.getSerializableExtra("Location");
			ctrlon = location.getLongitude();
			ctrlat = location.getLatitude();
			upPoi = location.getAddress();
			tv_pic_location.setText(upPoi);
		}
		if ((requestCode == PangkerConstant.RES_PICTURE || requestCode == CODE_RESELECT) && resultCode == RESULT_OK) {
			filePath = data.getStringExtra(DocInfo.FILE_PATH);
			if (filePath != null) {
				showImageWithPath();
			}
		}
	};

	private void browsePhoto() {
		// TODO Auto-generated method stub
		if (Util.isEmpty(filePath)) {
			showToast("请先拍照或者选择一张图片!");
			return;
		}

		ArrayList<PictureViewInfo> imagePathes = new ArrayList<PictureViewInfo>();
		PictureViewInfo pictureViewInfo = new PictureViewInfo();
		pictureViewInfo.setPicPath(filePath);
		imagePathes.add(pictureViewInfo);

		Intent it = new Intent(this, PictureBrowseActivity.class);
		it.putExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY, imagePathes);
		it.putExtra("index", 0);
		startActivity(it);
	}

	private void toMsgLocation() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setClass(this, MsgLoactionActivity.class);
		intent.putExtra("msg_type", 1);
		startActivityForResult(intent, POI_CODE);
	}

	private EditTextDialog mDialog;

	private void showPoiDialog() {
		// TODO Auto-generated method stub
		if (mDialog == null) {
			mDialog = new EditTextDialog(this);
			mDialog.setTitle("编辑位置");
		}
		mDialog.showDialog(upPoi, "请输入一个位置", 50, new EditTextDialog.DialogResultCallback() {
			@Override
			public void buttonResult(String result, boolean isSubmit) {
				// TODO Auto-generated method stub
				if (isSubmit && !Util.isEmpty(result)) {
					onLoaderGeo(result);
				}
			}
		});
	}

	/**
	 * 监听事件处理
	 */
	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			if (view == btnBack) {
				UploadPicActivity.this.onBackPressed();
			}
			if (view == btnAlbum) {
				openGallery(false);
			}
			if (view == btnCamera) {
				openCamera(false);
			}
			if (view == locationLayout) {
				// 这里跳转到选择地理位置的界面
				toMsgLocation();
			}
			// >>>>>>>>>资源上传按钮
			if (view == btnUpload) {
				if (!uploadCheck()) {
					return;
				}
				if (filePath != null) {
					uploadCheck(PangkerConstant.RES_PICTURE);
				} else {
					showToast(R.string.res_get_photo_fail);
				}
			}
			if (view == selectLayout || view == mDirLinearLayout) {
				selectDirInfos(PangkerConstant.RES_PICTURE);
			}
			if (view == typeLayout || view == btnType) {
				showDialog(1);
			}
			if (view == imageview) {
				browsePhoto();
			}
			if (view == btnClear) {
				txtPicName.setText("");
			}
			if (view == mLocationLayout) {
				showPoiDialog();
			}
		}
	};

	private boolean checkFormat(String format) {
		for (String f : PangkerConstant.PIC_FORMAT) {
			if (f.equalsIgnoreCase(format)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 上传相片接口
	 * 
	 * @param count
	 *            请求数目, status 接口：Upload
	 */
	protected void uploadFile(String filepath, int job_type) {
		if (Util.isEmpty(selectDirId)) {
			showToast("请选择一个目录!");
			return;
		}
		File file = new File(filepath);//
		filePath = file.getAbsolutePath();//

		String format = filePath.substring(filePath.lastIndexOf('.') + 1);
		// if (!checkFormat(format)) {
		// showToast("抱歉,目前还不支持这种图片的格式!");
		// return;
		// }
		String picName = txtPicName.getText().toString();
		if (Util.isEmpty(picName)) {
			showToast("图片名称不能为空!");
			return;
		}
		// 获取Poi信息

		try {
			md5Value = Util.getMd5(filepath);// 获取文件MD5值
			String photoDetail = etPhotoDetail.getText().toString();

			if (photoDetail == null || photoDetail.trim().equals("")) {
				photoDetail = StringUtils.substringBefore(file.getName(), ".");
			}

			String ifShare = cbIfShare.isChecked() ? "1" : "0";
			uploaderFile(PangkerConstant.RES_PICTURE, photoDetail, ifShare, picName, format, null, 0, job_type, upPoi);
			saveLocalPic();
			onBackPressed();
		} catch (Exception e) {
			logger.error(TAG, e);
			showToast(R.string.to_server_fail);
		}
	}

	private void saveLocalPic() {
		// TODO Auto-generated method stub
		IResDao iResDao = new ResDaoImpl(this);
		DirInfo dirInfo = new DirInfo();
		dirInfo.setPath(filePath);
		dirInfo.setDeal(DirInfo.DEAL_SELECT);
		dirInfo.setUid(userId);
		dirInfo.setIsfile(DirInfo.ISFILE);
		dirInfo.setDirName(txtPicName.getText().toString());
		File file = new File(filePath);// getCompress();
		dirInfo.setFileCount((int) file.length());
		dirInfo.setResType(PangkerConstant.RES_PICTURE);
		dirInfo.setRemark(0);
		if (!iResDao.isSelected(userId, dirInfo.getPath(), dirInfo.getResType())) {
			iResDao.saveSelectResInfo(dirInfo);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		if (only_camera) {
			outState.putBoolean(UploadPicActivity.ONLY_CAMERA_KEY, false);
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				initDir();
			}
		};
		handler.sendMessage(msg);
	}

	@Override
	protected void onLoaderGeo(String address) {
		// TODO Auto-generated method stub
		upPoi = address;
		if (tv_pic_location != null) {
			tv_pic_location.setText(upPoi);
		}
	}

}
