package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.CommentAdapter;
import com.wachoo.pangker.adapter.NoCommentAdapter;
import com.wachoo.pangker.entity.MessageTip;
import com.wachoo.pangker.entity.TextSpeakInfo;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconLoader;
import com.wachoo.pangker.listener.TextButtonWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CommentResult;
import com.wachoo.pangker.server.response.Commentinfo;
import com.wachoo.pangker.server.response.SearchCommentsResult;
import com.wachoo.pangker.server.response.SearchTalkResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EditTextKeyborad;
import com.wachoo.pangker.ui.EditTextKeyborad.IKeyboardListener;
import com.wachoo.pangker.ui.FooterView.OnLoadMoreListener;
import com.wachoo.pangker.ui.KeyboardLayout;
import com.wachoo.pangker.ui.ListLinearLayout;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

/**
 * 
 * 文字资源显示界面
 * 
 * @author wangxin
 * 
 */
public class ResSpeaktInfoActivity extends CommonPopActivity implements IUICallBackInterface {

	public static final String TEXT_SPEAK_INFO_KEY = "TEXT_SPEAK_INFO_KEY";
	private final int DELETE_COMMENT = 0x30;
	private final int SUBMIT_COMMENT = 0x34;
	private final int SEARCH_COMMENT = 0x33;
	private final int SEARCH_SPEAK = 0x35;

	protected KeyboardLayout mKeyboardLayout;
	private Button btnBack;
	private TextView txtTitle;
	private TextView txtSpeakContent;

	protected final int pageLength = 10;

	private PKIconLoader pkIconLoader;
	private TextSpeakInfo mTextSpeakInfo;
	private String speakId;

	private UIUserInfo uiUserInfo;
	private UICommentSubmit uiSubmit;
	public UIComment uiComment;

	private PangkerApplication application;
	private UserInfo userInfo;
	private String userId;
	private ServerSupportManager serverMana;

	protected void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.net_text_info);
		init();
		initView();
		searchData();
	};

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplicationContext();
		userInfo = application.getMySelf();
		userId = userInfo.getUserId();
		serverMana = new ServerSupportManager(this, this);
		// >>>>评论详情
		mTextSpeakInfo = (TextSpeakInfo) getIntent().getSerializableExtra(TEXT_SPEAK_INFO_KEY);
		if (mTextSpeakInfo != null) {
			speakId = String.valueOf(mTextSpeakInfo.getId());
		} else {
			speakId = getIntent().getStringExtra("speakId");
		}

		// >>>>获取用户头像
		pkIconLoader = PangkerManager.getUserIconResizer(application);
	}

	private void initView() {
		btnBack = (Button) findViewById(R.id.btn_back);
		txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("说说详情");
		mKeyboardLayout = (KeyboardLayout) findViewById(R.id.kl_res_info);
		txtSpeakContent = (TextView) findViewById(R.id.tv_content);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		btnBack.setOnClickListener(clickListener);
		txtSpeakContent.setOnLongClickListener(descLongClickListener);

		uiUserInfo = new UIUserInfo();
		uiComment = new UIComment();
		uiSubmit = new UICommentSubmit();
	}

	private void showData(TextSpeakInfo textSpeakInfo) {
		// TODO Auto-generated method stub
		this.mTextSpeakInfo = textSpeakInfo;
		txtSpeakContent.setText(mTextSpeakInfo.getContent());
		uiUserInfo.showInfo();
	}

	private void searchData() {
		// TODO Auto-generated method stub
		if (mTextSpeakInfo == null) {
			searchSpeakInfo(speakId);
		} else {
			showData(mTextSpeakInfo);
		}
		uiComment.showSerach();
		searchComment();
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				finish();
			}
		}
	};

	// 显示复制描述的功能
	protected View.OnLongClickListener descLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			showDescMenu();
			return true;
		}
	};

	private PopMenuDialog descDialog;
	ActionItem[] msgMenu1 = new ActionItem[] { new ActionItem(1, "复制") };

	private void showDescMenu() {
		// TODO Auto-generated method stub
		if (descDialog == null) {
			descDialog = new PopMenuDialog(this);
			descDialog.setListener(descClickListener);
			descDialog.setTitle("说说内容");
			descDialog.setMenus(msgMenu1);
		}
		descDialog.show();
	}

	UITableView.ClickListener descClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			if (actionid == 1) {
				String edsc = mTextSpeakInfo.getContent();
				if (!Util.isEmpty(edsc)) {
					ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
					clip.setText(edsc);
					showToast("复制成功");
				}
			}
			descDialog.dismiss();
		}
	};

	/**
	 * 资源评论接口
	 * 
	 * @param content
	 *            评论内容 请求数目 接口：AddSpeakComment.json
	 */
	protected void submitCooment(Commentinfo commentinfo, String restype) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", commentinfo.getUserid()));

		// >>>>当前用户名称
		paras.add(new Parameter("username", commentinfo.getUsername()));
		paras.add(new Parameter("commenttype", String.valueOf(commentinfo.getCommenttype())));
		// >>>>>>>>>>判断是否是回复case
		if (commentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_REPLY) {
			// >>>>>回复的用户ID
			paras.add(new Parameter("replyuserid", commentinfo.getReplyuserid()));
			paras.add(new Parameter("replyusername", commentinfo.getReplyusername()));
			// >>>>>回复的评论ID
			paras.add(new Parameter("commentid", commentinfo.getReplycommentid()));
		}
		paras.add(new Parameter("resid", String.valueOf(mTextSpeakInfo.getId())));
		paras.add(new Parameter("type", restype));// 操作id
		paras.add(new Parameter("content", commentinfo.getContent()));// 回答内容
		serverMana.supportRequest(Configuration.getAddComment(), paras, true, "正在提交...", SUBMIT_COMMENT);
	}

	/**
	 * 查询说说评论
	 */
	protected void searchComment() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("resid", speakId));
		paras.add(new Parameter("type", "3"));
		paras.add(new Parameter("limit", (uiComment.adapter.getCount() + "," + pageLength)));
		serverMana.supportRequest(Configuration.getSearchComments(), paras, SEARCH_COMMENT);
	}

	/**
	 * 查询说说评论
	 */
	protected void searchSpeakInfo(String speakId) {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", speakId));
		serverMana.supportRequest(Configuration.getSearchTalkById(), paras, true, "请稍后...", SEARCH_SPEAK);
	}

	protected void sendMessageTip() {
		Commentinfo commentinfo = uiSubmit.getNewCommentinfo();
		if (commentinfo != null) {
			String commentReplyUserID = null;
			// >>>>二、对其他评论的评论
			if (commentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_REPLY) {
				commentReplyUserID = commentinfo.getReplyuserid();
				sendMessTip(commentinfo, commentReplyUserID, MessageTip.TIP_COMMENT_REPLY);
			}

			// >>>>>>发送给资源上传用户 如果资源上传用户和当前回复的评论人是同一个用户，就不需要通知了
			if (!userId.equals(String.valueOf(mTextSpeakInfo.getUserId()))
					&& !String.valueOf(mTextSpeakInfo.getUserId()).equals(commentReplyUserID))
				sendMessTip(commentinfo, String.valueOf(mTextSpeakInfo.getUserId()), MessageTip.TIP_COMMENT_RES);
		}
	}

	protected void sendMessTip(Commentinfo commentinfo, String userID, int messageTipType) {
		// TODO Auto-generated method stub
		final MessageTip messageTip = new MessageTip();
		messageTip.setCommentID(commentinfo.getCommentId());
		messageTip.setUserId(userID);
		messageTip.setFromId(userId);
		messageTip.setFromName(application.getMyUserName());
		messageTip.setResId(String.valueOf(mTextSpeakInfo.getId()));
		messageTip.setResType(PangkerConstant.RES_SPEAK);
		messageTip.setResName(mTextSpeakInfo.getUserName());
		messageTip.setTipType(messageTipType);
		messageTip.setContent(commentinfo.getContent());
		messageTip.setCreateTime(Util.getSysNowTime());
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				application.getOpenfireManager().sendMessageTip(messageTip);
			}
		}).start();
	}

	public class UIComment implements View.OnClickListener, UITableView.ClickListener, View.OnLongClickListener {

		private Commentinfo commentinfo;
		private CommentAdapter adapter;
		private NoCommentAdapter adapter2;
		private TextView txtCount;
		private TextView txtLoad;
		private ListLinearLayout commentLayout;
		private LinearLayout loadlLayout;
		private PopMenuDialog menuDialog;
		private Handler handler = new Handler();
		private int sumComments = 0;

		public UIComment() {
			// TODO Auto-generated constructor stub
			txtCount = (TextView) findViewById(R.id.txt_count);
			txtLoad = (TextView) findViewById(R.id.txt_load);
			commentLayout = (ListLinearLayout) findViewById(R.id.comment_view);
			commentLayout.setOnClickListener(this);
			commentLayout.setOnLongClickListener(this);
			commentLayout.setClickable(false);
			commentLayout.setFocusable(false);

			loadlLayout = (LinearLayout) findViewById(R.id.comment_layout);
			adapter = new CommentAdapter(ResSpeaktInfoActivity.this, new ArrayList<Commentinfo>());
			adapter2 = new NoCommentAdapter(ResSpeaktInfoActivity.this);

		}

		public void showSerach() {
			// TODO Auto-generated method stub
			commentLayout.setShowLoadmore(false);
			loadlLayout.setVisibility(View.GONE);
			adapter.setCommentinfos(new ArrayList<Commentinfo>());
			txtLoad.setVisibility(View.VISIBLE);
			txtLoad.setText("正在加载...");
			uiSubmit.etComment.setText("");
		}

		public void serachCommentFail() {
			// TODO Auto-generated method stub
			txtLoad.setText("加载失败!");
		}

		private OnLoadMoreListener onLoadMoreListener = new OnLoadMoreListener() {

			@Override
			public void onLoadMoreListener() {
				// TODO Auto-generated method stub
				searchComment();
			}
		};

		private void showSumComments() {
			// TODO Auto-generated method stub
			txtCount.setText(String.valueOf(sumComments));
		}

		// >>>>>>>>>增加评论信息
		public void addComment(final Commentinfo commentinfo) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					adapter.addComment(commentinfo);
					loadlLayout.setVisibility(View.VISIBLE);
					commentLayout.setAdapter(adapter);
					uiSubmit.etComment.setText("");
					commentCountAdd();
				}
			});
		}

		public void showComments(SearchCommentsResult commentResult) {
			loadlLayout.setVisibility(View.VISIBLE);
			txtLoad.setVisibility(View.GONE);
			if (commentResult != null && commentResult.errorCode == BaseResult.SUCCESS) {
				if (commentResult.getCommentinfo() != null && commentResult.getCommentinfo().size() > 0) {
					sumComments = commentResult.getSumCount();
					showSumComments();
					if (adapter.getCount() == 0) {
						adapter.setCommentinfos(new ArrayList<Commentinfo>());
					}
					adapter.addCommentinfos(commentResult.getCommentinfo());
					if (commentResult.getSumCount() > adapter.getCount()) {
						commentLayout.setShowLoadmore(true);
						commentLayout.setOnLoadMoreListener(onLoadMoreListener);
					} else {
						commentLayout.setShowLoadmore(false);
					}
					commentLayout.setAdapter(adapter);
				} else {
					txtCount.setText("0");
					commentLayout.setAdapter(adapter2);
					commentLayout.setShowLoadmore(false);
				}
			} else {
				txtCount.setText("0");
				commentLayout.setAdapter(adapter2);
				commentLayout.setShowLoadmore(false);
			}
		}

		public void deleteComment() {
			// TODO Auto-generated method stub
			adapter.delComment(commentinfo);
			if (adapter.getCount() > 0) {
				commentLayout.setVisibility(View.VISIBLE);
				txtLoad.setVisibility(View.GONE);
				commentLayout.setAdapter(adapter);
			} else {
				txtCount.setText("0");
				commentLayout.setAdapter(adapter2);
				commentLayout.setShowLoadmore(false);
			}
			commentCountDel();
		}

		private void commentCountAdd() {
			sumComments++;
			showSumComments();
		}

		private void commentCountDel() {
			sumComments--;
			showSumComments();
		}

		ActionItem[] copyMenu = new ActionItem[] { new ActionItem(1, "复制") };

		private void showCopyMenu() {
			menuDialog = new PopMenuDialog(ResSpeaktInfoActivity.this);
			menuDialog.setListener(this);
			menuDialog.setTitle("评论信息");
			menuDialog.setMenus(copyMenu);
			menuDialog.show();
		}

		ActionItem[] showCopyAndDeleteMenu = new ActionItem[] { new ActionItem(1, "复制"), new ActionItem(2, "删除") };

		private void showCopyAndDeleteMenu() {
			menuDialog = new PopMenuDialog(ResSpeaktInfoActivity.this, R.style.MyDialog);
			menuDialog.setListener(this);
			menuDialog.setTitle("评论信息");
			menuDialog.setMenus(showCopyAndDeleteMenu);
			menuDialog.show();
		}

		public void delOneComment() {
			// TODO Auto-generated method stub
			adapter.delComment(commentinfo);
			if (adapter.getCount() > 0) {
				commentLayout.setVisibility(View.VISIBLE);
				txtLoad.setVisibility(View.GONE);
				commentLayout.setAdapter(adapter);
				txtCount.setText(String.valueOf(adapter.getCount()));
			} else {
				txtCount.setText("0");
				commentLayout.setAdapter(adapter2);
				commentLayout.setShowLoadmore(false);
			}
		}

		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			if (adapter.getCount() == 0) {
				return false;
			}
			commentinfo = (Commentinfo) v.getTag();
			// >>>>>>如果是自己的评论 或者是自己上传的资源
			if (commentinfo.getUserid().equals(userId) || mTextSpeakInfo.getUserId().equals(userInfo)) {
				showCopyAndDeleteMenu();
			} else {
				showCopyMenu();
			}
			return false;
		}

		@Override
		public void onClick(int actionId) {
			// TODO Auto-generated method stub

			// >>>>>>>复制评论操作
			if (actionId == 1) {
				ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				Log.i(TAG, "clip=" + clip);
				Log.i(TAG, "commentinfo=" + commentinfo);
				clip.setText(commentinfo.getContent());
			}
			// >>>>>>>>删除评论操作
			if (actionId == 2) {
				delComment(commentinfo.getCommentId());
			}
			menuDialog.dismiss();
		}

		protected void delComment(String commentId) {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("userid", userId));
			paras.add(new Parameter("commentid", commentId));// 问题id
			paras.add(new Parameter("type", "3"));// 操作id
			serverMana.supportRequest(Configuration.getDeleteCommenturl(), paras, true, "正在提交...", DELETE_COMMENT);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (adapter.getCount() == 0) {
				return;
			}
			commentinfo = (Commentinfo) v.getTag();
			// >>>>>>>如果是自己的不需要做任何动作
			if (!commentinfo.getUserid().equals(userId)) {
				uiSubmit.etComment.requestFocus();
				uiSubmit.popSoftKeyboard();
				uiSubmit.setCommentType(Commentinfo.COMMENT_TYPE_REPLY);
				uiSubmit.setEtCommentHint("回复" + commentinfo.getUsername());
				uiSubmit.setReplyCommentinfo(commentinfo);
			} else
				showToast("这是您自己的留言");
		}

	}

	class UICommentSubmit implements View.OnClickListener {

		EditTextKeyborad etComment;// 评论内容
		Button btn_sendComment;// 发送评论
		Button btn_submit;// 修改
		private int commentType = 0;// >>>>>评论类型
		private int softKeyboardPopLocationY = 0;
		private ViewFlipper pViewFlipper;// 评论用的动画效果

		// >>>>>>>>当前评论
		private Commentinfo newCommentinfo;
		// >>>>>>>>回复之前的评论
		private Commentinfo replyCommentinfo;

		public void popSoftKeyboard() {
			((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).showSoftInput(etComment, 0);
			int[] location = new int[2];
			etComment.getLocationOnScreen(location);
			softKeyboardPopLocationY = location[1];
		}

		public Commentinfo getNewCommentinfo() {
			return newCommentinfo;
		}

		public void setNewCommentinfo(Commentinfo newCommentinfo) {
			this.newCommentinfo = newCommentinfo;
		}

		public Commentinfo getReplyCommentinfo() {
			return replyCommentinfo;
		}

		public void setReplyCommentinfo(Commentinfo replyCommentinfo) {
			this.replyCommentinfo = replyCommentinfo;
		}

		public void setCommentTypeOnlyComment() {
			int[] location = new int[2];
			etComment.getLocationOnScreen(location);
			int softKeyboardLocationY = location[1];

			// >>>>>>>>如果不是弹出的设置为评论
			if (softKeyboardPopLocationY != softKeyboardLocationY) {
				uiSubmit.setCommentType(Commentinfo.COMMENT_TYPE_ONLY);
				uiSubmit.setEtCommentHint(getString(R.string.comment));
			}
		}

		public int getCommentType() {
			return commentType;
		}

		public void setCommentType(int commentType) {
			this.commentType = commentType;
		}

		public void setEtCommentHint(String etCommentHint) {
			etComment.setHint(etCommentHint);
		}

		public UICommentSubmit() {
			// TODO Auto-generated constructor stub
			pViewFlipper = (ViewFlipper) findViewById(R.id.comment_flipper);
			pViewFlipper.setDisplayedChild(0);
			etComment = (EditTextKeyborad) findViewById(R.id.et_foot_editer);
			btn_sendComment = (Button) findViewById(R.id.btn_foot_pubcomment);
			btn_sendComment.setEnabled(false);
			btn_sendComment.setOnClickListener(this);
			etComment.addTextChangedListener(new TextButtonWatcher(btn_sendComment));
			btn_submit = (Button) findViewById(R.id.btn_submit);
			btn_submit.setEnabled(false);
			etComment.setKeyboardListener(keyboardListener);
		}

		private IKeyboardListener keyboardListener = new IKeyboardListener() {

			@Override
			public void hide() {
				// TODO Auto-generated method stub
				setCommentType(Commentinfo.COMMENT_TYPE_ONLY);
				setEtCommentHint(getString(R.string.comment));
			}
		};

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (mTextSpeakInfo == null) {
				showToast("正在加载,还不能进行该操作,请稍等!");
				return;
			}
			if (v == btn_sendComment) {
				String comment = etComment.getText().toString();
				if (!Util.isEmpty(comment)) {
					newCommentinfo = new Commentinfo();
					newCommentinfo.setCommenttype(getCommentType());
					newCommentinfo.setContent(comment);
					newCommentinfo.setCommenttime(Util.getNowTime());
					this.newCommentinfo.setUserid(userId);
					this.newCommentinfo.setUsername(application.getMyUserName());
					// >>>>>是否是回复评论
					if (newCommentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_REPLY) {
						newCommentinfo.setReplycommentid(getReplyCommentinfo().getCommentId());
						newCommentinfo.setReplyuserid(getReplyCommentinfo().getUserid());
						newCommentinfo.setReplyusername(getReplyCommentinfo().getUsername());
					}
					submitCooment(newCommentinfo, "3");// 1. 资源 ;此时resid为资源id
														// 2. 群组 ;此时resid为群组id
														// 3. 文字：针对用户的说说功能
				} else {
					showToast("发送的内容不能为空!");
				}
			}
		}
	}

	class UIUserInfo implements View.OnClickListener {

		public ImageView iv_ownericon;
		public TextView txt_ownername;
		public TextView txt_uploadtime;

		public ImageView iv_share;

		public void hideBroader() {
			findViewById(R.id.ly_broadcast).setVisibility(View.GONE);
		}

		public UIUserInfo() {
			// TODO Auto-generated constructor stub
			initComponent();
		}

		private void initComponent() {
			iv_ownericon = (ImageView) findViewById(R.id.iv_res_ownericon);
			iv_ownericon.setOnClickListener(this);
			txt_ownername = (TextView) findViewById(R.id.tv_res_ownername);
			txt_uploadtime = (TextView) findViewById(R.id.tv_uploadtime);
			iv_share = (ImageView) findViewById(R.id.iv_share);
		}

		public void showInfo() {
			pkIconLoader.loadImage(mTextSpeakInfo.getUserId(), iv_ownericon);
			txt_ownername.setText(mTextSpeakInfo.getUserName());
			txt_uploadtime.setText(Util.getShowTimes(mTextSpeakInfo.getSpeakTime()));
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.iv_res_ownericon) {
				if (mTextSpeakInfo != null) {
					goUserInfo(mTextSpeakInfo.getUserId());
				}
			}
		}

		private void goUserInfo(String userId) {
			Intent intent = new Intent(ResSpeaktInfoActivity.this, UserWebSideActivity.class);
			intent.putExtra(UserItem.USERID, userId);
			startActivity(intent);
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			return;
		}

		if (caseKey == SUBMIT_COMMENT) {// 发送评论
			CommentResult submitResult = JSONUtil.fromJson(response.toString(), CommentResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				showToast(submitResult.errorMessage);
				// >>>>通知对方评论成功
				uiSubmit.etComment.setText("");
				uiSubmit.getNewCommentinfo().setCommentId(String.valueOf(submitResult.getCommentId()));
				// >>>>>不需要再次获取
				uiComment.addComment(uiSubmit.getNewCommentinfo());
				sendMessageTip();

			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
		} else if (caseKey == SEARCH_COMMENT) {
			SearchCommentsResult commentResult = JSONUtil.fromJson(response.toString(), SearchCommentsResult.class);
			uiComment.showComments(commentResult);
		} else if (caseKey == SEARCH_SPEAK) {
			SearchTalkResult result = JSONUtil.fromJson(response.toString(), SearchTalkResult.class);
			if (result != null && result.errorCode == BaseResult.SUCCESS) {
				if (result.getSpeakInfoList() != null && result.getSpeakInfoList().size() > 0) {
					showData(result.getSpeakInfoList().get(0));
				}
			} else if (result != null && result.errorCode == 0) {
				showToast(result.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
		} else if (caseKey == DELETE_COMMENT) {
			BaseResult submitResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				// >>>>>>直接删除
				uiComment.deleteComment();
				showToast(submitResult.errorMessage);
			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
		}
	}

	/**
	 * 判断是否是当前资源
	 * 
	 * @param resID
	 * @return
	 */
	public boolean isCurrentResource(long resID) {
		if (resID == mTextSpeakInfo.getId())
			return true;

		return false;
	}
}
