package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.DirsAdapter;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.ResDirManagerResult;
import com.wachoo.pangker.server.response.ResDirQueryResult;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.util.Util;

public class ResDirSelectActivity extends CommonPopActivity implements IUICallBackInterface {

	private int DirType;// 资源类型
	private String dirid;
	private String userid;
	protected ServerSupportManager serverMana;

	private Button btnBack;
	private PullToRefreshListView pullToRefreshListView;
	private ListView dirListView;
	private EmptyView emptyView;
	private DirsAdapter dirsAdapter;
	private DirInfo rootDirInfo;

	private String txtDirName;
	private Button addDir;
	private DirInfo nowDirInfo;// 当前当前操作的资源，包含目录
	private final int DIR_ADD = 10;
	private String mess = "正在向服务器提交...";// 提交提示信息;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blacklist_manager);
		serverMana = new ServerSupportManager(this, this);
		PangkerApplication application = ((PangkerApplication) getApplication());
		userid = application.getMySelf().getUserId();
		DirType = getIntent().getIntExtra("DirType", 0);
		rootDirInfo = application.getRootDir(DirType);
		dirid = rootDirInfo != null ? rootDirInfo.getDirId() : "";
		initView();
		getDirInfo(dirid, userid);
	}

	private void initView() {
		// TODO Auto-generated method stub
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setTag("ResUploadDirSelectActivity");
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);

		dirListView = pullToRefreshListView.getRefreshableView();
		dirListView.setOnItemClickListener(onItemClickListener);
		emptyView = new EmptyView(this);
		emptyView.addToListView(dirListView);
		FooterView footerView = new FooterView(this);
		footerView.addToListView(dirListView);
		footerView.hideFoot();

		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("目录选择");
		// findViewById(R.id.iv_more).setVisibility(View.GONE);
		addDir = (Button) findViewById(R.id.iv_more);
		addDir.setText("新建");
		addDir.setBackgroundResource(R.drawable.btn_default_selector);
		addDir.setOnClickListener(mOnClickListener);
	}

	private final OnClickListener mOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == addDir) {
				// 调用创建资源目录对话框
				showDialog();
			}
		}
	};
	
	EditTextDialog mDialog ;

	private void showDialog() {
		if(mDialog == null){
			mDialog = new EditTextDialog(this);
		}
		mDialog.setTitle("创建资源目录");
		mDialog.showDialog("", "请输入资源目录名称", 20, resultCallback);
	}
	
	EditTextDialog.DialogResultCallback resultCallback = new EditTextDialog.DialogResultCallback() {
		@Override
		public void buttonResult(String result, boolean isSubmit) {
			// TODO Auto-generated method stub
			if(isSubmit){
				txtDirName = result;
				initAddResInfo();
			}
		}
	};

	// 初始化文件添加
	private void initAddResInfo() {
		// TODO Auto-generated method stub
		if (Util.isEmpty(txtDirName)) {
			showToast("请输入文件名称!");
		} else if (isNameExit(txtDirName)) {
			showToast("文件名称已经存在了!");
		} else {
			String pid = rootDirInfo.getDirId();
			nowDirInfo = new DirInfo();
			nowDirInfo.setDirName(txtDirName);
			nowDirInfo.setParentDirId(pid);
			nowDirInfo.setIsfile(0);
			nowDirInfo.setFileCount(0);
			nowDirInfo.setUid(userid);
			nowDirInfo.setResType(DirType);
			sendServer(DIR_ADD, nowDirInfo);
		}
	}

	private void sendServer(int type, DirInfo dirInfo) {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userid));
		paras.add(new Parameter("optype", String.valueOf(type)));
		paras.add(new Parameter("dirname", dirInfo.getDirName()));
		paras.add(new Parameter("restype", "" + dirInfo.getResType()));
		paras.add(new Parameter("dirparentid", dirInfo.getParentDirId()));
		serverMana.supportRequest(Configuration.getResDirManager(), paras, true, mess, type);
	}

	private boolean isNameExit(String string) {
		// TODO Auto-generated method stub
		if (dirsAdapter == null) {
			return false;
		}else {
			for (DirInfo resInfo : dirsAdapter.getDirinfos()) {
				if (resInfo.getIsfile() == DirInfo.ISFILE) {
					continue;
				}
				if (string.equals(resInfo.getDirName())) {
					return true;
				}
			}
		}
		return false;
	}

	OnRefreshListener<ListView> onRefreshListener = new OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			getDirInfo(dirid, userid);
		}
	};

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				backFinish(rootDirInfo);
			}
		}
	};

	OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			DirInfo sDirInfo = (DirInfo) parent.getItemAtPosition(position);
			backFinish(sDirInfo);
		}
	};

	/**
	 * 返回结果
	 */
	private void backFinish(DirInfo dirInfo) {
		if (dirInfo != null) {
			Intent intent = new Intent();
			intent.putExtra("DirInfo", dirInfo);
			setResult(1, intent);
		}
		this.finish();
	}

	private void showDirs(List<DirInfo> dirinfos) {
		// TODO Auto-generated method stub
		if (dirinfos != null && dirinfos.size() > 0) {
			dirinfos.add(0, rootDirInfo);
		} else {
			dirinfos = new ArrayList<DirInfo>();
			dirinfos.add(rootDirInfo);
		}
		dirsAdapter = new DirsAdapter(this, dirinfos);
		dirListView.setAdapter(dirsAdapter);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(response)) {
			if (caseKey == 0) {
				emptyView.showView(R.drawable.emptylist_icon,
						R.string.no_network);
			}
			return;
		}
		if (caseKey == 0) {
			ResDirQueryResult result = JSONUtil.fromJson(response.toString(),ResDirQueryResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showDirs(result.getDirinfos());
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == DIR_ADD) {
			ResDirManagerResult result = JSONUtil.fromJson(response.toString(), ResDirManagerResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showToast(result.getErrorMessage());
				nowDirInfo.setDirId(result.getDirId());
				dirsAdapter.addDirInfo(nowDirInfo);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}

	}

	/**
	 * 查询资源目录
	 */
	private void getDirInfo(String dirid, String userid) {
		pullToRefreshListView.setRefreshing(true);
		List<Parameter> paras = new ArrayList<Parameter>();
		if (Util.isEmpty(dirid)) {
			paras.add(new Parameter("appuid", userid));
			paras.add(new Parameter("uid", userid));
			paras.add(new Parameter("type", String.valueOf(DirType)));
			paras.add(new Parameter("limit", "0,10"));
			serverMana.supportRequest(Configuration.getDirQueryByType(), paras,0);
		} else {
			paras.add(new Parameter("appuid", userid));
			paras.add(new Parameter("uid", userid));
			paras.add(new Parameter("dirid", dirid));// 目录ID(dirid=0，表示查询顶层目录)
			paras.add(new Parameter("limit", "0,10"));
			serverMana.supportRequest(Configuration.getResDirQueryByDir(),paras, 0);
		}

	}
}
