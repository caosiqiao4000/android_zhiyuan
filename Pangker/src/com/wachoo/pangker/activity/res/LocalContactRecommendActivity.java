package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.contact.LocalContactsSelectActivity;
import com.wachoo.pangker.adapter.LocalContactUserAdapter;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CheckUserResult;
import com.wachoo.pangker.server.response.Checkinfo;

/**
 * 
 * @TODO 资源推荐给本地通讯录好友（有可能非旁客用户）
 * 
 * @author zhengjy
 * 
 *         2012-9-6
 * 
 */
public class LocalContactRecommendActivity extends CommonPopActivity implements IUICallBackInterface,
		OnFocusChangeListener {

	private static final int RES_CHECK_USER = 0x10;
	private static final int RECOMMEND_RES = 0x11;

	private final int mRequestCode = 0x10;
	PangkerApplication application;
	private com.wachoo.pangker.ui.MyGridView gvUuseritem;
	private LocalContactUserAdapter userAdapter;
	private EditText etDetail;
	private LinearLayout lyLocalcontact;
	private TextView tvPangkerRecommend;
	private TextView tvLocalcontact;

	private ResShowEntity resShowEntity;
	private String mUserId;

	private int currState = 0;// 0:联系人检验前；1,2：检验中； 3：联系人检验后（知道哪个是旁客用户，非旁客用户）
	private List<String> phones;
	private List<LocalContacts> pangkerLists;
	private List<LocalContacts> notPangkerLists;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_recommend);

		application = (PangkerApplication) getApplication();
		resShowEntity = (ResShowEntity) getIntent().getSerializableExtra(ResShowEntity.RES_ENTITY);
		mUserId = application.getMyUserID();

		initView();

		List<LocalContacts> selectedUser = (List<LocalContacts>) this.getIntent().getSerializableExtra(
				"selectedUser");
		if (selectedUser != null && selectedUser.size() > 0) {
			userAdapter.setUserList(selectedUser);
		}

	}

	private void initView() {
		Button btnRight = (Button) findViewById(R.id.iv_more);
		btnRight.setVisibility(View.GONE);

		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("准备发送");
		lyLocalcontact = (LinearLayout) findViewById(R.id.ly_localcontact);
		tvPangkerRecommend = (TextView) findViewById(R.id.tv_pangker_recommend);
		tvLocalcontact = (TextView) findViewById(R.id.tv_localcontact);

		gvUuseritem = (com.wachoo.pangker.ui.MyGridView) findViewById(R.id.gv_useritem);
		etDetail = (EditText) findViewById(R.id.et_detail);
		etDetail.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				userAdapter.setDeleteState(false);
				return false;
			}
		});
		userAdapter = new LocalContactUserAdapter(this, new ArrayList<LocalContacts>());
		userAdapter.setmAddClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				userAdapter.setDeleteState(false);
				if (currState != 0) {
					showToast("已经校验过通讯录联系人，不能再添加联系人！");
					return;
				} else {
					Intent intent = new Intent(LocalContactRecommendActivity.this,
							LocalContactsSelectActivity.class);
					intent.putExtra("intentType", 1);
					intent.putExtra("fromType", 1);
					LocalContactRecommendActivity.this.startActivityForResult(intent, mRequestCode);

				}

			}
		});

		// 添加成员监听
		userAdapter.setCreater(true);
		gvUuseritem.setAdapter(userAdapter);

		gvUuseritem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				if (position < userAdapter.getCount() - 2) {
					LocalContacts userItem = (LocalContacts) arg0.getAdapter().getItem(position);
					// 移除成员
					if (userAdapter.isDeleteState()) {
						if (currState != 0) {
							removeUser(userItem);
						}
						userAdapter.getUserList().remove(userItem);

						userAdapter.notifyDataSetChanged();
					}
					// 查看成员信息（旁客用户）
					else if (userItem.getRelation() == LocalContacts.RELATION_P_USER
							|| userItem.getRelation() == LocalContacts.RELATION_P_FRIEND) {
						Intent intent = new Intent(LocalContactRecommendActivity.this, UserWebSideActivity.class);
						intent.putExtra(UserItem.USERID, userItem.getUserId());
						intent.putExtra(UserItem.USERNAME, userItem.getUserName());
						intent.putExtra(UserItem.USERSIGN, userItem.getSign());
						LocalContactRecommendActivity.this.startActivity(intent);
					} else {
						showToast("该联系人不是旁客用户!");
					}
				}

			}
		});

		LinearLayout lyGridView = (LinearLayout) findViewById(R.id.ly_gridview);
		lyGridView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				userAdapter.setDeleteState(false);
				return false;
			}
		});
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LocalContactRecommendActivity.this.finish();
			}
		});
		Button btnSubmit = (Button) findViewById(R.id.btn_submit);
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (userAdapter.getUserList() == null || userAdapter.getUserList().size() < 1) {
					showToast("请选择联系人!");
					return;
				} else {
					if (currState == 0) {
						checkPhone(userAdapter.getUserList());
					} else if (currState == 3) {
						resRecommend();

					}
				}
			}
		});
	}

	private void removeUser(LocalContacts userItem) {

		final Iterator iter = new ArrayList(phones).iterator();
		while (iter.hasNext()) {
			String item = iter.next().toString();
			if (item.equals(userItem.getPhoneNum())) {
				this.phones.remove(item);
				break;
			}
		}
		final Iterator pangkerIter = new ArrayList(pangkerLists).iterator();
		while (pangkerIter.hasNext()) {
			LocalContacts item = (LocalContacts) pangkerIter.next();
			if (item.getPhoneNum().equals(userItem.getPhoneNum())) {
				this.pangkerLists.remove(item);
				break;
			}
		}
		final Iterator notPangkerIter = new ArrayList(notPangkerLists).iterator();
		while (notPangkerIter.hasNext()) {
			LocalContacts item = (LocalContacts) notPangkerIter.next();
			if (item.getPhoneNum().equals(userItem.getPhoneNum())) {
				this.notPangkerLists.remove(item);
				break;
			}
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v != gvUuseritem && hasFocus) {
			userAdapter.setDeleteState(false);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == mRequestCode && resultCode == RESULT_OK) {
			List<LocalContacts> selectedUser = (List<LocalContacts>) data
					.getSerializableExtra("selectedUser");
			if (selectedUser != null && selectedUser.size() > 0) {
				userAdapter.addUserList(selectedUser);
			}
		}
	}

	/**
	 * TODO校验联系人是否旁客用户
	 * 
	 * @param contacts
	 *            CheckUser.json
	 */
	private void checkPhone(List<LocalContacts> contacts) {
		StringBuffer mobileSB = new StringBuffer();
		for (int i = 0; i < contacts.size(); i++) {
			if (i == 0) {
				mobileSB.append(contacts.get(i).getPhoneNum());
			} else {
				mobileSB.append(",").append(contacts.get(i).getPhoneNum());
			}
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", mUserId));
		paras.add(new Parameter("mobiles", mobileSB.toString()));
		serverMana.supportRequest(Configuration.getCheckUser(), paras, true, "请稍候...", RES_CHECK_USER);
	}

	/**
	 * TODO 给非旁客用户推荐资源，同时记录的推荐资源信息，记录成功再发送短信邀请
	 * 
	 * @param recommendPhones
	 *            被推荐资源人手机号码,多个号码之间“，”分隔
	 * 
	 *            RecommendRes.json
	 */
	private void RecommendRes(String recommendPhones) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "0"));
		paras.add(new Parameter("uid", mUserId));
		paras.add(new Parameter("resid", String.valueOf(resShowEntity.getResID())));
		paras.add(new Parameter("phone", recommendPhones));// 被推荐资源人手机号码,多个号码之间“，”分隔
		String mMobile = application.getMySelf().getPhone() != null ? application.getMySelf().getPhone() : "";
		paras.add(new Parameter("mobile", mMobile));
		serverMana.supportRequest(Configuration.getRecommendRes(), paras, true, "请稍候...", RECOMMEND_RES);
	}

	/**
	 * 获取号码对应旁客用户LocalContacts信息 UserItem TODO
	 * 
	 * @param phoneNum
	 * @return
	 */
	private LocalContacts getLocalContact(Checkinfo checkInfo) {
		List<LocalContacts> selectMember = userAdapter.getUserList();
		final Iterator iterator = new ArrayList(selectMember).iterator();
		while (iterator.hasNext()) {
			LocalContacts item = (LocalContacts) iterator.next();
			if (item.getPhoneNum().equals(checkInfo.getMobile())) {
				if (checkInfo.getUid() != null) {
					item.setUserId(checkInfo.getUid().toString());

				}
				item.setRelation(checkInfo.getFlag());
				return item;
			}
		}
		return null;
	}

	private void resRecommend() {
		if (notPangkerLists != null && notPangkerLists.size() > 0) {
			StringBuffer sb = new StringBuffer();
			for (LocalContacts localCon : notPangkerLists) {
				if (sb.length() > 0) {
					sb.append(",");
				}
				sb.append(localCon.getPhoneNum());
			}
			if (sb.length() > 0) {
				RecommendRes(sb.toString());
			}
			currState = 2;
		}
		if (pangkerLists != null && pangkerLists.size() > 0) {

			OpenfireManager openfireManager = application.getOpenfireManager();
			String msg = etDetail.getText().toString();
			String resId = String.valueOf(resShowEntity.getResID());
			for (LocalContacts user : pangkerLists) {
				if (user.getRelation() == LocalContacts.RELATION_P_USER
						|| user.getRelation() == LocalContacts.RELATION_P_FRIEND) {
					openfireManager.sendResRecommend(mUserId, user.getUserId(), resShowEntity.getResType(),
							resId, resShowEntity.getResName(), resShowEntity.getSinger(), msg, resShowEntity.getShareUid());
				}
			}
		}
		if (currState == 3) {
			this.finish();
		}
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		if (caseKey == RES_CHECK_USER) {
			CheckUserResult result = JSONUtil.fromJson(supportResponse.toString(), CheckUserResult.class);
			if (result == null) {
				showToast(R.string.to_server_fail);
				return;
			}
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				List<Checkinfo> checkInfos = result.getRs();
				notPangkerLists = new ArrayList<LocalContacts>();
				if (checkInfos != null) {
					phones = new ArrayList<String>();
					pangkerLists = new ArrayList<LocalContacts>();
					notPangkerLists = new ArrayList<LocalContacts>();
					for (Checkinfo checkinfo : checkInfos) {
						if (checkinfo.getFlag() == Checkinfo.USER_TYPE_NOTPANGKER) {
							phones.add(checkinfo.getMobile());
							LocalContacts localContact = getLocalContact(checkinfo);
							if (localContact != null) {
								notPangkerLists.add(localContact);
							}
							currState = 1;
						} else if (checkinfo.getFlag() == Checkinfo.USER_TYPE_PANGKER_NORMAL) {
							LocalContacts localContact = getLocalContact(checkinfo);
							if (localContact != null) {
								pangkerLists.add(localContact);
							}
							currState = 2;
						}
					}
					if (pangkerLists.size() > 0) {
						String usernames = "";
						for (LocalContacts localContacts2 : pangkerLists) {
							usernames = usernames + localContacts2.getUserName() + "，";
						}
						lyLocalcontact.setVisibility(View.VISIBLE);
						tvPangkerRecommend.setText("联系人" + usernames
								+ "是旁客用户，点击发送按钮后则向该用户推荐资源，取消向该用户发送资源推荐，请移除用户!");
					}
					if (notPangkerLists.size() > 0) {
						String usernames = "";
						for (LocalContacts localContacts2 : notPangkerLists) {
							usernames = usernames + localContacts2.getUserName() + "，";
						}
						lyLocalcontact.setVisibility(View.VISIBLE);
						tvLocalcontact.setText("联系人" + usernames
								+ "不是旁客用户，点击发送按钮后则短信邀请TA注册旁客，并推荐资源，取消短信邀请，请移除用户!");
					}
					currState = 3;
				}

			} else
				showToast(R.string.to_server_fail);
		} else if (caseKey == RECOMMEND_RES) {
			BaseResult result = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (result == null) {
				showToast(R.string.to_server_fail);
				return;
			} else if (result.getErrorCode() == BaseResult.SUCCESS) {
				if (caseKey == RECOMMEND_RES) {
				}
			}
			this.finish();
		}

	}

	/**
	 * 发送短信
	 * 
	 * @param contacts
	 *            void
	 */
	private void sendMsg(String phoneNum, String sms) {
		if (phoneNum != null && phoneNum.length() > 0) {
			SmsManager smsMana = SmsManager.getDefault();
			if (sms.length() > 70) {
				ArrayList<String> msgs = smsMana.divideMessage(sms);
				smsMana.sendMultipartTextMessage(phoneNum, null, msgs, null, null);
			} else
				smsMana.sendTextMessage(phoneNum, null, sms, null, null);
		}

	}

}
