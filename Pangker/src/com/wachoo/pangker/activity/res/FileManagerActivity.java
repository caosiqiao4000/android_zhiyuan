package com.wachoo.pangker.activity.res;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.LocalFilesAdapter;
import com.wachoo.pangker.entity.DirectoryFiles;
import com.wachoo.pangker.entity.LocalFileItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.receiver.StorageEventFilter;
import com.wachoo.pangker.receiver.StorageEventReceiver;
import com.wachoo.pangker.receiver.StorageEventReceiver.StorageEventListener;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.util.Util;

public class FileManagerActivity extends CommonPopActivity {

	private PangkerApplication application;

	private int type;// 选择的类型，图片，音乐，应用，文档
	private String SDcardPath = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();// >>>>>根目录
	private String rootPath = SDcardPath;// >>>>>根目录

	private TextView txtDir;
	private ProgressBar progressBar;
	private Button mBackButton;
	private ListView mFilesList;// >>>>>>>列表
	private EmptyView mEmptyView;
	private LocalFilesAdapter mLocalFilesAdapter;

	// >>>>>>>>>获取本地图片
	private ImageLocalFetcher localFetcher;

	private StorageEventReceiver storageEventReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.local_listview);
		application = (PangkerApplication) getApplication();

		type = getIntent().getIntExtra("file_type", PangkerConstant.RES_DOCUMENT);

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = false;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>>>加载
		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		localFetcher.setmSaveDiskCache(false);
		localFetcher.setImageCache(mImageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);

		registerReceiver();
		initView();
		directoryScanner(application.ismExternalStorageAvailable());
	}

	private void registerReceiver() {
		// TODO Auto-generated method stub
		storageEventReceiver = new StorageEventReceiver(mStorageEventListener);
		registerReceiver(storageEventReceiver, new StorageEventFilter());
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver(storageEventReceiver);
		super.onDestroy();
	}

	private void initView() {
		// TODO Auto-generated method stub
		TextView title = (TextView) findViewById(R.id.mmtitle);
		title.setText("SD卡文件");
		findViewById(R.id.iv_more).setVisibility(View.GONE);

		mFilesList = (ListView) findViewById(R.id.musicListView);
		mLocalFilesAdapter = new LocalFilesAdapter(this, new ArrayList<LocalFileItem>(), localFetcher);
		mFilesList.setAdapter(mLocalFilesAdapter);
		mFilesList.setOnItemClickListener(onItemClickListener);

		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(mFilesList);

		// >>>返回按钮
		mBackButton = (Button) findViewById(R.id.btn_back);
		mBackButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				backDirectory();
			}
		});
		txtDir = (TextView) findViewById(R.id.tv_path);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
	}

	StorageEventListener mStorageEventListener = new StorageEventListener() {

		@Override
		public void onStorageStateChanged(boolean isAvailable, boolean writeAble) {
			// TODO Auto-generated method stub
			directoryScanner(isAvailable);
		}
	};

	private String getToastByType() {
		switch (type) {
		case PangkerConstant.RES_APPLICATION:
			return "应用文件";
		case PangkerConstant.RES_DOCUMENT:
			return "文档文件";
		case PangkerConstant.RES_PICTURE:
			return "图片文件";
		case PangkerConstant.RES_MUSIC:
			return "音乐文件";
		}
		return "";
	}

	private void selectFileItem(LocalFileItem fileItem) {
		// TODO Auto-generated method stub
		if (fileItem.isInByType(type)) {
			Intent intent = new Intent();
			intent.putExtra(DocInfo.FILE_PATH, rootPath + "/" + fileItem.getFileName());
			setResult(RESULT_OK, intent);
			finish();
		} else {
			showToast(" 文件类型不符合,请选择" + getToastByType());
		}
	}

	public void onBackPressed() {
		backDirectory();
	};

	AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
			// TODO Auto-generated method stub
			LocalFileItem fileItem = (LocalFileItem) adapterView.getItemAtPosition(i);
			if (fileItem.getFileType() == LocalFileItem.DIRECTORY_TYPE) {
				// >>>当前访问目录
				rootPath = rootPath + "/" + fileItem.getFileName();
				directoryScanner(application.ismExternalStorageAvailable());
			} else {
				// 不能打开
				selectFileItem(fileItem);
			}
		}
	};

	private void directoryScanner(boolean isAvailable) {
		// TODO Auto-generated method stub
		progressBar.setVisibility(View.GONE);
		if (!isAvailable) {
			finish();
			return;
		}
		txtDir.setVisibility(View.VISIBLE);
		txtDir.setText(rootPath);
		File file = new File(rootPath);
		if (file != null && file.exists() && file.isDirectory()) {
			new DirectoryScannerAsyncTask(iLocalFileLoader, this).execute(file);
		}
	}

	// >>>返回上一层目录
	private void backDirectory() {
		// TODO Auto-generated method stub
		if (!rootPath.equals(SDcardPath)) {
			rootPath = Util.getLastPath(rootPath);
			directoryScanner(application.ismExternalStorageAvailable());
		} else {
			this.finish();
		}
	}

	ILocalFileLoader iLocalFileLoader = new ILocalFileLoader() {
		@Override
		public void loadOver(DirectoryFiles directoryFiles) {
			// TODO Auto-generated method stub
			List<LocalFileItem> fileItems = new ArrayList<LocalFileItem>();
			fileItems.addAll(directoryFiles.getDirs());
			fileItems.addAll(directoryFiles.getFiles());
			mFilesList.setSelection(0);
			mLocalFilesAdapter.setmFiles(fileItems);
		}
	};

	class DirectoryScannerAsyncTask extends AsyncTask<File, Integer, DirectoryFiles> {

		private File currentDirectory;
		private ILocalFileLoader iLocalFileLoader;

		public DirectoryScannerAsyncTask(ILocalFileLoader iLocalFileLoader, Context mContext) {
			super();
			this.iLocalFileLoader = iLocalFileLoader;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			progressBar.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}

		@Override
		protected DirectoryFiles doInBackground(File... params) {
			// TODO Auto-generated method stub
			// >>>>目录对象
			DirectoryFiles directoryFiles = new DirectoryFiles();
			// >>>文件对象
			List<LocalFileItem> fileItems = new ArrayList<LocalFileItem>();
			// >>>文件夹对象
			List<LocalFileItem> directoryItems = new ArrayList<LocalFileItem>();
			// >>>>>>>>>>>>获取根目录
			this.currentDirectory = params[0];
			if (currentDirectory != null && currentDirectory.listFiles() != null) {

				for (File currentFile : currentDirectory.listFiles()) {
					// 必须在sd卡存在的情况下加载文件
					if (application.ismExternalStorageAvailable()) {
						// >>>>文件夹
						if (currentFile.isDirectory()) {
							directoryItems.add(new LocalFileItem(currentFile.getAbsolutePath(), currentFile.getName(),
									LocalFileItem.DIRECTORY_TYPE, R.drawable.icon46_file, 0));
						}
						// >>>>文件
						else {
							String fileName = currentFile.getName();
							// >>>APK文件
							if (fileName.endsWith(LocalFileItem.TYPE_APK)) {
								fileItems.add(new LocalFileItem(currentFile.getAbsolutePath(), currentFile.getName(),
										LocalFileItem.FILE_TYPE, R.drawable.icon46_apk, currentFile.length()));
							}
							// >>>MP3文件
							else if (fileName.endsWith(LocalFileItem.TYPE_MP3)) {
								fileItems.add(new LocalFileItem(currentFile.getAbsolutePath(), currentFile.getName(),
										LocalFileItem.FILE_TYPE, R.drawable.icon46_music, currentFile.length()));
							}
							// >>>JPG文件
							else if (fileName.endsWith(LocalFileItem.TYPE_JPG)
									|| fileName.endsWith(LocalFileItem.TYPE_PNG)) {
								fileItems.add(new LocalFileItem(currentFile.getAbsolutePath(), currentFile.getName(),
										LocalFileItem.FILE_TYPE, 0, currentFile.length() / 1024));
							}
							// >>>TXT文件
							else if (fileName.endsWith(LocalFileItem.TYPE_TXT)
									|| fileName.endsWith(LocalFileItem.TYPE_DOC)) {
								fileItems.add(new LocalFileItem(currentFile.getAbsolutePath(), currentFile.getName(),
										LocalFileItem.FILE_TYPE, R.drawable.attachment_doc, currentFile.length()));
							} else {
								fileItems.add(new LocalFileItem(currentFile.getAbsolutePath(), currentFile.getName(),
										LocalFileItem.FILE_TYPE, R.drawable.ems_file, currentFile.length()));
							}
						}
					}
				}
			}
			// >>>进行排序
			Collections.sort(directoryItems);
			Collections.sort(fileItems);
			directoryFiles.setDirs(directoryItems);
			directoryFiles.setFiles(fileItems);
			return directoryFiles;
		}

		@Override
		protected void onPostExecute(DirectoryFiles directoryFiles) {
			// TODO Auto-generated method stub
			super.onPostExecute(directoryFiles);
			progressBar.setVisibility(View.GONE);
			if (this.iLocalFileLoader != null) {
				iLocalFileLoader.loadOver(directoryFiles);
			}
		}
	}

	public interface ILocalFileLoader {
		void loadOver(DirectoryFiles directoryFiles);
	}

}
