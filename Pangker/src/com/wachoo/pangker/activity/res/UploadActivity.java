package com.wachoo.pangker.activity.res;

import java.io.File;
import java.io.FileNotFoundException;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.camera.CameraCompActivity;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.db.UploadDao;
import com.wachoo.pangker.db.impl.UploadDaoImpl;
import com.wachoo.pangker.downupload.UpLoadManager;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.listener.CheckBoxChangedListener.IPromptDialog;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.map.poi.PoiaddressLoader.GeoCallback;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * 如果SD卡突然不在了，界面不没有存在的必要了，同时正在上传的资源也要取消上传!
 * 
 * @author zxx
 * 
 */
public abstract class UploadActivity extends CameraCompActivity implements
		ITabSendMsgListener {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag
	// protected final int MaxDetailLenght = 1000;//上传描述的限制
	protected final int MaxNameLenght = 20;

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	protected final static int CODE_RESELECT = 0x302;// 重新选择

	protected final int UPLOAD_CODE = 0x100;// 上传校验
	protected final int POI_CODE = 0x101;// 位置选择code
	private long LimitFileSize = 20 * 1024 * 1024;
	private UploadDao mUploadDao;
	protected String userId;
	protected String md5Value = null;
	protected UploadJob uploadInfo;
	protected PangkerApplication application;
	UpLoadManager uploadManager;

	protected DirInfo sDirInfo;
	protected String selectDirId = "";// 用户选择的目录id
	protected double ctrlon = 0;
	protected double ctrlat = 0;
	protected String uploadType;

	protected TextView txtDirName;
	protected TextView txtUpload;
	protected LinearLayout upoladLayout;

	protected LinearLayout groupLayout;
	protected TextView txtGroupName;

	protected SharedPreferencesUtil spu;

	protected String groupId;
	protected String groupName;
	protected Button btnUpload;
	protected TextView tvAgreement;
	protected CheckBox cbTerms;
	protected boolean isAgreementChecked = false;;

	protected MessageTipDialog messageTipDialog;
	protected String filePath;
	private PoiaddressLoader poiaddressLoader;
	protected String realPoi;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		application = (PangkerApplication) getApplication();
		spu = new SharedPreferencesUtil(this);
		uploadManager = application.getUpLoadManager();
		userId = application.getMyUserID();
		groupId = getIntent().getStringExtra(UserGroup.GROUPID_KEY);
		groupName = getIntent().getStringExtra(UserGroup.GROUP_NAME_KEY);
		mUploadDao = new UploadDaoImpl(this);
		isAgreementChecked = spu.getBoolean(PangkerConstant.IS_AGREEDMENT_CHECKED_KEY + application.getMyUserID(), false);

		poiaddressLoader = new PoiaddressLoader(getApplicationContext());
		poiaddressLoader.onPoiLoader(application.getCurrentLocation(), this, mGeoCallback);
	}

	protected IPromptDialog promptDialog = new IPromptDialog() {

		@Override
		public void showDialog() {
			// TODO Auto-generated method stub
			if (isAgreementChecked)
				return;
			messageTipDialog = new MessageTipDialog(
					new MessageTipDialog.DialogMsgCallback() {

						@Override
						public void buttonResult(boolean isSubmit) {
							// TODO Auto-generated method stub
							if (isSubmit)
								spu.saveBoolean(
										PangkerConstant.IS_AGREEDMENT_CHECKED_KEY
												+ application.getMyUserID(),
										true);
						}
					}, UploadActivity.this);
			messageTipDialog.showDialog("", "是否记住您的选择?", false);
		}
	};

	protected void initUploadButton() {
		// >>>>>>>>>>判断是否选中
		if (!isAgreementChecked) {
			cbTerms.setChecked(false);
			btnUpload.setTextColor(Color.GRAY);
			btnUpload.setEnabled(false);
		} else {
			cbTerms.setChecked(true);
			btnUpload.setTextColor(Color.BLACK);
			btnUpload.setEnabled(true);
		}
	}

	/**
	 * 上传资源前检查
	 * 
	 * @return
	 */
	protected boolean uploadCheck() {
		if (!application.ismExternalStorageAvailable()) {
			showToast("SD卡文件不存在,无法上传!");
			return false;
		}
		if (!cbTerms.isChecked()) {
			this.showToast(R.string.tip_no_agree_terms);
			return false;
		}
		return true;
	}

	protected void hideInputMethod(View v) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	protected void initDirService() {
		Intent intent = new Intent(this, PangkerService.class);
		intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_RES_INIT);
		startService(intent);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		PangkerManager.getTabBroadcastManager().removeUserListenter(this);
		super.onBackPressed();
	}

	/**
	 * @param resType 资源类型
	 * @param desc  资源描述
	 * @param ifShare 是否分享
	 * @param resName 资源名称
	 * @param format 资源格式
	 * @param author  资源作者
	 * @param musicId 音乐Id
	 * @param duration 音乐播放时长
	 * @throws FileNotFoundException
	 */
	protected void uploaderFile(int resType, String desc, String ifShare, String resName, String format, String author,
			int musicId, int job_type, String upPoi) throws FileNotFoundException {
		File file = new File(filePath);
		if (!file.exists() || file.length() <= 0) {
			showToast("SD卡文件不存在，无法上传!");
			return;
		}
		//先看看上传列表中有没有文件，以及文件的状态
		int status = uploadManager.getUploadJobStatusByPath(filePath);
		//如果没上传过，或者上传失败、取消了，
		if(status == -1 || status == UploadJob.UPLOAD_FAILED || status == UploadJob.UPLOAD_CANCEL){
			uploadInfo = mUploadDao.getUploadInof(userId, filePath);
			int uploadId;
			if (uploadInfo == null) {
				uploadInfo = new UploadJob();
				uploadInfo.setDesc(desc);
				uploadInfo.setFileFormat(format);
				uploadInfo.setDirId(selectDirId);
				uploadInfo.setFilePath(filePath);
				uploadInfo.setMd5Value(md5Value);
				uploadInfo.setUserId(userId);
				uploadInfo.setIfShare(ifShare);
				uploadInfo.setType(resType);
				uploadInfo.setResName(resName);
				uploadInfo.setResType(uploadType);
				uploadInfo.setStatus(UploadJob.UPLOAD_INITOK);
				uploadInfo.setUpTime(Util.getSysNowTime());
				uploadInfo.setFileSize((int) file.length());
				if (!Util.isEmpty(author)) {
					uploadInfo.setAuthor(author);
				}
				if (ctrlat > 0) {
					uploadInfo.setCtrlat(String.valueOf(ctrlat));
				}
				if (ctrlon > 0) {
					uploadInfo.setCtrlon(String.valueOf(ctrlon));
				}
				if (musicId > 0) {
					uploadInfo.setMusicId(musicId);
				}
				if(!Util.isEmpty(realPoi)){
					uploadInfo.setRealPoi(realPoi);
				}
				if(!Util.isEmpty(upPoi)){
					uploadInfo.setUpPoi(upPoi);
				}
				uploadId = (int) mUploadDao.saveUploadInfo(uploadInfo);
			} else {// 如果已经上传过了，但是没有上传成功，
				uploadInfo.setDesc(desc);
				uploadInfo.setMd5Value(md5Value);
				uploadInfo.setIfShare(ifShare);
				uploadInfo.setResName(resName);
				uploadInfo.setDirId(selectDirId);
				uploadInfo.setResType(uploadType);
				uploadInfo.setStatus(UploadJob.UPLOAD_INITOK);
				uploadInfo.setUpTime(Util.getSysNowTime());
				if (ctrlat > 0) {
					uploadInfo.setCtrlat(String.valueOf(ctrlat));
				}
				if (ctrlon > 0) {
					uploadInfo.setCtrlon(String.valueOf(ctrlon));
				}
				if(!Util.isEmpty(realPoi)){
					uploadInfo.setRealPoi(realPoi);
				}
				if(!Util.isEmpty(upPoi)){
					uploadInfo.setUpPoi(upPoi);
				}
				mUploadDao.updateUploadInfo(uploadInfo);

				uploadId = uploadInfo.getId();
			}
			uploadInfo.setGroupId(groupId != null ? groupId : "");
			// id > 0 ：插入db成功 isUpload ： 是否上传文件 true：上传 ；false：不需要上传
			if (uploadId > 0) {
				uploadInfo.setId(uploadId);
				// >>>>>>将下载任务添加到下载管理类
				uploadManager.addUploadJob(uploadInfo);

				showToast("正在上传中，可以进行其他操作!");
				// if (flagCheckFileExist == false) {
				Intent intent = new Intent(this, PangkerService.class);
				intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_UPLOAD);
				// >>>>>>>>>>传递上传jobID到service
				intent.putExtra(UploadJob.UPLOAD_JOB_ID, uploadInfo.getId());
				startService(intent);
				// }
			}
		} else if(status == UploadJob.UPLOAD_UPLOAGINDG){
			showToast("正在上传该文件, 请等待!");
		} else if(status == UploadJob.UPLOAD_SUCCESS){
			showToast("你已经上传过该文件了!");
		} else {
			Intent intent = new Intent(this, UpDownLoadManagerActivity.class);
			startActivity(intent);
		}
	}
	
	protected void uploaderFile(int resType, String desc, String ifShare, String resName, 
			String format, String author, int musicId, int job_type) throws FileNotFoundException {
		uploaderFile(resType, desc, ifShare, resName, format, author, musicId, job_type, null);
	}
	
	private boolean checkFormat(int resType, String format) {
		if (resType == PangkerConstant.RES_APPLICATION) {
			return true;
		}
		if (resType == PangkerConstant.RES_DOCUMENT) {
			for (String checkformat : PangkerConstant.DOC_FORMAT) {
				if (format.toLowerCase().endsWith(checkformat)) {
					return true;
				}
			}
		}
		if (resType == PangkerConstant.RES_PICTURE) {
			for (String checkformat : PangkerConstant.PIC_FORMAT) {
				if (format.toLowerCase().endsWith(checkformat)) {
					return true;
				}
			}
		}
		if (resType == PangkerConstant.RES_MUSIC) {
			for (String checkformat : PangkerConstant.MUSIC_FORMAT) {
				if (format.toLowerCase().endsWith(checkformat)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 文件校验接口 要先判断该文件是否正在上传
	 * 
	 * @param resType
	 *            ，资源
	 * @param filename
	 *            SD卡文件地址 如果是文档doc，就是绝对路径
	 */
	protected void uploadCheck(int resType) {
		if (filePath == null) {
			showToast(R.string.res_get_file_failed);
			return;
		}
		File file = new File(filePath);
		if (!file.exists()) {
			showToast("文件不存在，无法上传!");
			return;
		}
		try {
			md5Value = Util.getMd5(filePath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
		}
		// 判断该文件是否正在上传，如果是，不能上传
		if (mUploadDao.ifUploading(userId, md5Value,
				UploadJob.UPLOAD_UPLOAGINDG) > 0) {
			showToast("该文件正在上传,请等待!");
			return;
		}
		long filelength = file.length();
		if (filelength > LimitFileSize) {
			showToast("文件大小超过限制,无法上传!");
			return;
		}
		String format = Util.getFileformat(filePath);
		if (!checkFormat(resType, format)) {
			showToast("目前还不支持这种格式!");
			return;
		}
		uploadFile(filePath, UploadJob.UPLOAD_START);
	}

	protected void selectDirInfos(int type) {
		Intent intent = new Intent(this, ResDirSelectActivity.class);
		intent.putExtra("DirType", type);
		startActivityForResult(intent, UPLOAD_CODE);
	}
	
	GeoCallback mGeoCallback = new GeoCallback() {
		@Override
		public void onGeoLoader(String address, int code) {
			// TODO Auto-generated method stub
			if(address != null){
				realPoi = address;
				Log.d("realPoi", realPoi);
				onLoaderGeo(address);
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == UPLOAD_CODE && resultCode == 1) {
			DirInfo dirInfo = (DirInfo) data.getSerializableExtra("DirInfo");
			txtDirName.setText(dirInfo.getDirName());
			selectDirId = dirInfo.getDirId();
		}
	}

	@Override
	public void doImageBitmap(String imageLocalPath) {
		// TODO Auto-generated method stub
	}
	
	protected abstract void onLoaderGeo(String address);

	protected abstract void uploadFile(String fileName, int job_type);

}
