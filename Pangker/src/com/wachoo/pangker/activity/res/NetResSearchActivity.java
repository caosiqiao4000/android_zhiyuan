package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.style.SuperscriptSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.MusicListAdapter;
import com.wachoo.pangker.adapter.ResAppAdapter;
import com.wachoo.pangker.adapter.ResNetPicAdapter;
import com.wachoo.pangker.adapter.ResReadListAdapter;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AppInfo;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.server.response.MovingRes;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.PicInfo;
import com.wachoo.pangker.server.response.ResDirQueryResult;
import com.wachoo.pangker.server.response.ResShareResult;
import com.wachoo.pangker.server.response.SearchLbsAppResult;
import com.wachoo.pangker.server.response.SearchLbsDocResult;
import com.wachoo.pangker.server.response.SearchLbsMusicResult;
import com.wachoo.pangker.server.response.SearchLbsPictureResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.QuickAction.OnActionItemClickListener;
import com.wachoo.pangker.ui.QuickAction.OnDismissListener;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

/**
 * 搜索全网周边资源
 * 
 * @author zhengjy
 * 
 */
public class NetResSearchActivity extends ResSearchActivity{

	private String TAG = "ContactFriendInviteActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	private final static int SELECT_REQUESTCODE = 0x10;
	private final static short DIS_HOT_TYPE = 1;
	private final static short DIS_NEW_TYPE = 2;
	private short disType = DIS_HOT_TYPE;

	private static final int RES_DOC_QUERY = 0;
	private static final int RES_PIC_QUERY = 1;
	private static final int RES_MUSIC_QUERY = 2;
	private static final int RES_APP_QUERY = 3;

	private final String[] choiceItems = new String[] { "查询文档资源", "查询音乐资源", "查询图片资源", "查询应用资源" };
 
	private final int[] RES_MENU_INDEXS = { 0x100, 0x101, 0x102,0x103};
	private final static String[] RES_MENU_TITLE = { "全网最新", "全网最热", "今日推荐", "资源搜索"};
	private final static String no_search_data = "很抱歉！没有搜索到您想要的资源！";
	private final static String no_data =  "很抱歉！暂无数据！";

	private TextView tv_title;
	private Button btn_back;
	private EditText etEditName;
	private Button btnMenuShow;
	private LinearLayout btnLayout;

	private String uid;
	private String searchKey;
	private PangkerApplication application;
	private int resType = 0;
	private String resUrl;

	private ResReadListAdapter docAdapter; // 阅读资源数据源
	private MusicListAdapter musicListAdapter; // 音乐资源
	private ResNetPicAdapter picAdapter;// 图片资源
	private ResAppAdapter appAdapter; // 应用资源

	private List<DocInfo> allListFiles;
	private List<MusicInfo> allMusicList;
	private List<PicInfo> photoList;
	private List<AppInfo> appList;

	private PopMenuDialog menuDialog;

	private static final int FAVORITE = 0x102;
	private static final int SAVE_TO_ME = 0x101;

	private ImageView tv_icon_draw;
	private LinearLayout doclayoutTitle;

	private PullToRefreshListView mPullToRefreshListView;
	private ListView mListView;// 全网最热、全网最新容器
	private FooterView mFooterView;
	private EmptyView mEmptyView;

	private QuickAction docpopQuickAction;
	private int pageIndex = 0;
	// >>>>>>获取图片
	private ImageResizer mImageWorker;
	private ImageCache mImageCache;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.net_res_search);

		serverMana = new ServerSupportManager(this, this);

		application = (PangkerApplication) getApplication();
		uid = application.getMyUserID();
		resType = this.getIntent().getIntExtra("resType", 0);

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
		initView();

		initBoundleAdapter();
	}

	View.OnClickListener picOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int location = (Integer) v.getTag();
			intoPic(location);
		}
	};

	View.OnLongClickListener picOnLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			int location = (Integer) v.getTag();
			selectItem = (MovingRes) photoList.get(location);
			showPopMenu();
			return true;
		}
	};

	/**
	 * 为mResListview绑定adpter
	 */
	private void initBoundleAdapter() {
		if (resType == RES_DOC_QUERY_TYPE) {
			resUrl = Configuration.getSearchDoc();
			etEditName.setHint(choiceItems[0]);
			allListFiles = new ArrayList<DocInfo>();
			docAdapter = new ResReadListAdapter(this, allListFiles);
			mListView.setDividerHeight(1);
			mListView.setAdapter(docAdapter);
		} else if (resType == RES_MUS_QUERY_TYPE) {
			etEditName.setHint(choiceItems[1]);
			resUrl = Configuration.getSearchMusic();
			allMusicList = new ArrayList<MusicInfo>();
			musicListAdapter = new MusicListAdapter(this, allMusicList, mImageWorker);
			mListView.setDividerHeight(1);
			mListView.setAdapter(musicListAdapter);
		} else if (resType == RES_PIC_QUERY_TYPE) {
			etEditName.setHint(choiceItems[2]);
			resUrl = Configuration.getSearchPicture();
			photoList = new ArrayList<PicInfo>();
			picAdapter = new ResNetPicAdapter(this, photoList, mImageWorker);
			picAdapter.setPicOnClickListener(picOnClickListener);
			picAdapter.setPicOnLongClickListener(picOnLongClickListener);
			mListView.setAdapter(picAdapter);
			mListView.setDividerHeight(0);
		} else if (resType == RES_APP_QUERY_TYPE) {
			etEditName.setHint(choiceItems[3]);
			resUrl = Configuration.getSearchLbsApp();
			appList = new ArrayList<AppInfo>();
			appAdapter = new ResAppAdapter(this, appList, mImageWorker);
			mListView.setAdapter(appAdapter);
			mListView.setDividerHeight(1);
		}
	}

	public void initView() {
		Button btn_Right = (Button) findViewById(R.id.iv_more);
		btn_Right.setVisibility(View.GONE);
		findViewById(R.id.res_list).setVisibility(View.GONE);
		btnLayout = (LinearLayout)findViewById(R.id.btn_layout);
		tv_title = (TextView) findViewById(R.id.mmtitle);
		tv_title.setText(RES_MENU_TITLE[3]);
		
		btnMenuShow = (Button) findViewById(R.id.btn_reply);
		etEditName = (EditText) findViewById(R.id.et_query_name);
		etEditName.setFilters(new InputFilter[] { new InputFilter.LengthFilter(20) });

		btnMenuShow.setOnClickListener(mOnClickListener);

		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(mOnClickListener);

		tv_icon_draw = (ImageView) findViewById(R.id.title_icon);
		doclayoutTitle = (LinearLayout) findViewById(R.id.ly_title_layout);
		doclayoutTitle.setOnClickListener(mOnClickListener);

		mPullToRefreshListView = (PullToRefreshListView) findViewById(R.id.lv_mPullToRefresh);
		mPullToRefreshListView.setPullToRefreshEnabled(false);
		mPullToRefreshListView.setOnRefreshListener(onRefreshListener);
		mListView = mPullToRefreshListView.getRefreshableView();
		mListView.setOnItemClickListener(mOnItemClickListener);
		mListView.setOnItemLongClickListener(mOnItemLongClickListener);
		mListView.setOnTouchListener(mOnTouchListener);

		mFooterView = new FooterView(this);
		mFooterView.addToListView(mListView);
		mFooterView.setOnLoadMoreListener(onLoadMoreListener);
		mFooterView.hideFoot();

		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(mListView);

		docpopQuickAction = new QuickAction(this, QuickAction.ANIM_GROW_FROM_CENTER);
		docpopQuickAction.setOnActionItemClickListener(onActionItemClickListener);
		docpopQuickAction.setOnDismissListener(onDismissListener);
		for (int i = 0; i < RES_MENU_INDEXS.length; i++) {
			if (i == RES_MENU_INDEXS.length - 1) {
				docpopQuickAction.addActionItem(new ActionItem(RES_MENU_INDEXS[i], RES_MENU_TITLE[i]), true);
			} else {
				docpopQuickAction.addActionItem(new ActionItem(RES_MENU_INDEXS[i], RES_MENU_TITLE[i]));
			}
		}
	}

	private OnTouchListener mOnTouchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			hideSoftInput(v);
			return false;
		}
	};
	
	/**
	 * 搜索类型监听
	 */
	OnActionItemClickListener onActionItemClickListener = new OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			// TODO Auto-generated method stub
			if (actionId == RES_MENU_INDEXS[0]) {// 全网最新
				tv_title.setText(RES_MENU_TITLE[0]);
				tv_icon_draw.setImageResource(R.drawable.icon32_arrow);
				clearList();
				btnLayout.setVisibility(View.GONE);
				mPullToRefreshListView.setPullToRefreshEnabled(true);
				mPullToRefreshListView.setRefreshing(true);
				disType = DIS_NEW_TYPE;
				searchResData();
			} else if (actionId == RES_MENU_INDEXS[1]) {// 全网最热
				tv_title.setText(RES_MENU_TITLE[1]);
				tv_icon_draw.setImageResource(R.drawable.icon32_arrow);
				clearList();
				btnLayout.setVisibility(View.GONE);
				mPullToRefreshListView.setPullToRefreshEnabled(true);
				mPullToRefreshListView.setRefreshing(true);
				disType = DIS_HOT_TYPE;
				searchResData();
			} else if (actionId == RES_MENU_INDEXS[2]) {// 今日推荐
				tv_title.setText(RES_MENU_TITLE[2]);
				tv_icon_draw.setImageResource(R.drawable.icon32_arrow);
				clearList();
				btnLayout.setVisibility(View.GONE);
				mPullToRefreshListView.setPullToRefreshEnabled(true);
				mPullToRefreshListView.setRefreshing(true);
				
			} else if (actionId == RES_MENU_INDEXS[3]) {// 资源搜索
				tv_title.setText(RES_MENU_TITLE[3]);
				tv_icon_draw.setImageResource(R.drawable.icon32_arrow);
				clearList();
				etEditName.setText("");
				btnLayout.setVisibility(View.VISIBLE);
				mPullToRefreshListView.setPullToRefreshEnabled(false);
			}
		}
	};

	OnDismissListener onDismissListener = new OnDismissListener() {
		@Override
		public void onDismiss() {
			// TODO Auto-generated method stub
			tv_icon_draw.setImageResource(R.drawable.icon32_arrow);
		}
	};

	FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			searchResData();
		}
	};

	private View.OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.btn_back:
				hideSoftInput(v);
				NetResSearchActivity.this.finish();
				break;

			case R.id.ly_title_layout:
				hideSoftInput(v);
				docpopQuickAction.show(doclayoutTitle);
				tv_icon_draw.setImageResource(R.drawable.icon32_arrow_p);
				break;

			case R.id.btn_reply:// 搜索
				searchKey = etEditName.getText().toString();
				if (Util.isEmpty(searchKey)) {
					showToast("搜索条件不能为空!");
					return;
				}
				clearList();
				queryLbsRes();
				break;
			}
		}
	};

	/**
	 * clear all lists.
	 */
	private void clearList() {
		pageIndex = 0;
		startLimit = 0;
		if (mEmptyView.isIsshow()) {
			mEmptyView.reset();
		}
		
		if (allListFiles != null) {
			allListFiles.clear();
			docAdapter.notifyDataSetChanged();
		}
		if (allMusicList != null) {
			allMusicList.clear();
			musicListAdapter.notifyDataSetChanged();
		}
		if (photoList != null) {
			photoList.clear();
			picAdapter.notifyDataSetChanged();
		}
		if (appList != null) {
			appList.clear();
			appAdapter.notifyDataSetChanged();
		}
	}

	// pull
	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {

		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			pageIndex = 0;
			startLimit = 0;
			searchResData();
		}
	};

	private void searchResData() {
		switch (resType) {
		case RES_DOC_QUERY_TYPE:
			if (disType == DIS_NEW_TYPE) {
				queryRankRes(RES_DOC_QUERY, 0);
			} else if (disType == DIS_HOT_TYPE) {
				queryRankRes(RES_DOC_QUERY, 1);
			}
			break;
		case RES_MUS_QUERY_TYPE:
			if (disType == DIS_NEW_TYPE) {
				queryRankRes(RES_MUSIC_QUERY, 0);
			} else if (disType == DIS_HOT_TYPE) {
				queryRankRes(RES_MUSIC_QUERY, 1);
			}
			break;
		case RES_PIC_QUERY_TYPE:
			if (disType == DIS_NEW_TYPE) {
				queryRankRes(RES_PIC_QUERY, 0);
			} else if (disType == DIS_HOT_TYPE) {
				queryRankRes(RES_PIC_QUERY, 1);
			}
			break;
		case RES_APP_QUERY_TYPE:
			if (disType == DIS_NEW_TYPE) {
				queryRankRes(RES_APP_QUERY, 0);
			} else if (disType == DIS_HOT_TYPE) {
				queryRankRes(RES_APP_QUERY, 1);
			}
			break;
		}
	}

	/**
	 * 搜索全网资源最新、最热
	 * 
	 * @param search_type
	 *            -->搜索返回的key、资源类型
	 * @param type最新或者最热
	 *            (0/1)
	 */
	private void queryRankRes(int search_type, int type) {
		// judgeContactAdapter();
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", userId));
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("type", String.valueOf(type)));
		paras.add(new Parameter("resType", String.valueOf(search_type)));
		paras.add(getSearchLimit());
		serverMana.supportRequest(Configuration.getResRankQuery(), paras, search_type);
	}

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			if (resType == RES_DOC_QUERY_TYPE) {
				selectItem = (DocInfo) parent.getItemAtPosition(position);
				intoDocInfo(position - 1);
			} else if (resType == RES_MUS_QUERY_TYPE) {
				selectItem = (MusicInfo) parent.getItemAtPosition(position);
				intoMusicInfo((MusicInfo) selectItem, position - 1);
			} else if (resType == RES_PIC_QUERY_TYPE) {

			} else if (resType == RES_APP_QUERY_TYPE) {
				selectItem = (AppInfo) parent.getAdapter().getItem(position);
				intoApp(position -  1);
			}
		}
	};

	private OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			if (resType == RES_DOC_QUERY_TYPE) {
				selectItem = (MovingRes) parent.getItemAtPosition(position);
				showPopMenu();
			} else if (resType == RES_MUS_QUERY_TYPE) {
				selectItem = (MovingRes) parent.getItemAtPosition(position);
				showPopMenu();
			} else if (resType == RES_PIC_QUERY_TYPE) {

			} else if (resType == RES_APP_QUERY_TYPE) {
				selectItem = (MovingRes) parent.getAdapter().getItem(position);
				showPopMenu();
			}
			return true;
		}
	};

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			case FAVORITE:
				broadcastApp();
				break;
			case SAVE_TO_ME:
				switch (resType) {
				case RES_DOC_QUERY_TYPE:
					searchDirInfo(PangkerConstant.RES_DOCUMENT);
					break;
				case RES_PIC_QUERY_TYPE:
					searchDirInfo(PangkerConstant.RES_PICTURE);
					break;
				case RES_MUS_QUERY_TYPE:
					searchDirInfo(PangkerConstant.RES_MUSIC);
					break;
				case RES_APP_QUERY_TYPE:
					searchDirInfo(PangkerConstant.RES_APPLICATION);
					break;
				}
				break;
			}
			menuDialog.dismiss();
		}
	};

	private void showPopMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
			ActionItem[] msgMenu = new ActionItem[] {
					new ActionItem(FAVORITE, getString(R.string.res_question_retransmit)),
					new ActionItem(SAVE_TO_ME, getString(R.string.res_app_collect_btn)) };
			menuDialog.setMenus(msgMenu);
		}
		switch (resType) {
		case RES_DOC_QUERY_TYPE:
			menuDialog.setTitle(((DocInfo) selectItem).getDocname());
			break;
		case RES_PIC_QUERY_TYPE:
			menuDialog.setTitle(((PicInfo) selectItem).getResName());
			break;
		case RES_MUS_QUERY_TYPE:
			menuDialog.setTitle(((MusicInfo) selectItem).getMusicName());
			break;
		case RES_APP_QUERY_TYPE:
			menuDialog.setTitle(((AppInfo) selectItem).getAppname());
			break;
		}
		menuDialog.show();
	}

	private void broadcastApp() {
		// TODO Auto-generated method stub
		Log.i("Out", "Start NetRestSearchActivity's broadcastApp() method.");
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", uid));
		paras.add(new Parameter("resid", String.valueOf(selectItem.getSid())));
		paras.add(new Parameter("optype", String.valueOf(PangkerConstant.OPTYPE_RES)));// 操作类型（0：问答，1：资源，2：群组）资源广播时必填.
		paras.add(new Parameter("type", "2"));// 1: 广播 2:转播
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getResShare(), paras, true, mess, FAVORITE);
		Log.i("Out", "Over NetRestSearchActivity's broadcastApp() method.");
	}

	/**
	 * 查看阅读资源详细
	 * 
	 * @param docInfo
	 */
	private void intoDocInfo(int position) {
		Intent intent = new Intent();

		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		for (DocInfo docInfo : allListFiles) {
			ResShowEntity entity = new ResShowEntity();
			entity.setCommentTimes(docInfo.getCommentTimes());
			entity.setResType(PangkerConstant.RES_DOCUMENT);
			entity.setDownloadTimes(docInfo.getDownloadTimes());
			entity.setFavorTimes(docInfo.getFavorTimes());
			entity.setResID(docInfo.getSid());
			entity.setScore(Integer.parseInt(String.valueOf(docInfo.getScore())));
			entity.setResName(docInfo.getResName());
			entity.setShareUid(docInfo.getShareUid());
			entity.setIfshare(docInfo.getIsShare());
			reList.add(entity);
		}
		application.setResLists(reList);
		application.setViewIndex(position);
		intent.setClass(this, ResInfoActivity.class);
		startActivity(intent);
	}

	/**
	 * 查看音乐资源详细
	 * 
	 * @param musicInfo
	 */
	private void intoMusicInfo(MusicInfo musicInfo, int position) {
		// TODO Auto-generated method stub
		application.setMusiList(allMusicList);

		Intent intent = new Intent();
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		for (MusicInfo info : allMusicList) {
			ResShowEntity entity = new ResShowEntity();
			entity.setCommentTimes(info.getCommentTimes());
			entity.setResType(PangkerConstant.RES_MUSIC);
			entity.setDownloadTimes(info.getDownloadTimes());
			entity.setFavorTimes(info.getFavorTimes());
			entity.setResID(info.getSid());
			entity.setScore(Integer.parseInt(String.valueOf(info.getScore())));
			entity.setResName(info.getResName());
			entity.setShareUid(info.getShareUid());
			entity.setIfshare(info.getIsShare());
			entity.setSinger(info.getSinger());
			reList.add(entity);
		}
		application.setResLists(reList);
		application.setViewIndex(position);
		intent.setClass(this, ResInfoActivity.class);
		startActivity(intent);
	}

	private void intoApp(int position) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		for (AppInfo appInfo : appList) {
			ResShowEntity entity = new ResShowEntity();
			entity.setCommentTimes(appInfo.getCommentTimes());
			entity.setResType(PangkerConstant.RES_APPLICATION);
			entity.setDownloadTimes(appInfo.getDownloadTimes());
			entity.setFavorTimes(appInfo.getFavorTimes());
			entity.setResID(appInfo.getSid());
			entity.setScore(Integer.parseInt(String.valueOf(appInfo.getScore())));
			entity.setResName(appInfo.getResName());
			entity.setShareUid(appInfo.getShareUid());
			entity.setIfshare(appInfo.getIsShare());
			reList.add(entity);
		}
		application.setResLists(reList);
		application.setViewIndex(position);
		intent.setClass(this, ResInfoActivity.class);
		startActivity(intent);
	}

	/**
	 * 查看图片资源详细
	 * 
	 * @param musicInfo
	 */
	private void intoPic(int position) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		for (PicInfo picInfo : photoList) {
			ResShowEntity entity = new ResShowEntity();
			entity.setIfshare(picInfo.getIsShare());
			entity.setCommentTimes(picInfo.getCommentTimes());
			entity.setResType(PangkerConstant.RES_PICTURE);
			entity.setDownloadTimes(picInfo.getDownloadTimes());
			entity.setFavorTimes(picInfo.getFavorTimes());
			entity.setResID(picInfo.getSid());
			entity.setScore(Integer.parseInt(String.valueOf(picInfo.getScore())));
			entity.setResName(picInfo.getResName());
			entity.setShareUid(picInfo.getShareUid());
			reList.add(entity);
		}
		application.setResLists(reList);
		application.setViewIndex(position);
		intent.setClass(this, PictureInfoActivity.class);
		startActivity(intent);
	}

	/**
	 * @Title: queryLbsDoc
	 * @Description: 查询搜索引擎资源文件
	 * @param searchKey
	 *            根据指定关键字查询，为空则查询全部
	 * @return void 返回类型
	 */
	private void queryLbsRes() {
		try {
			ServerSupportManager serverMana = new ServerSupportManager(this, this);
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("userid", uid));
			paras.add(getLongitude());
			paras.add(getLatitude());
			paras.add(new Parameter("searchkey", searchKey));
			paras.add(getSearchLimit());//
			paras.add(getDistanceRang());
			// orderkey =
			// String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
			paras.add(new Parameter("orderkey", String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC)));
			paras.add(new Parameter("searchtype", PangkerConstant.SEARCH_ALL_DATA));
			String mess = getResourcesMessage(R.string.please_wait);
			serverMana.supportRequest(resUrl, paras, true, mess, resType);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (SELECT_REQUESTCODE == requestCode && RESULT_OK == resultCode) {
			LocalContacts contact = (LocalContacts) data.getSerializableExtra(LocalContacts.LOCALCONTACTS_KEY);
			if (contact != null) {
				etEditName.setText(contact.getPhoneNum());
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			mPullToRefreshListView.onRefreshComplete();
			mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
			return;
		}

		switch (caseKey) {
		case RES_DOC_QUERY_TYPE:
		case RES_DOC_QUERY:
			mPullToRefreshListView.onRefreshComplete();
			SearchLbsDocResult docResult = JSONUtil.fromJson(response.toString(), SearchLbsDocResult.class);
			if (docResult != null && docResult.isSuccess()) {
				List<DocInfo> listData = docResult.getData().getData();
				if (listData != null && listData.size() > 0) {
					if (pageIndex == 0) {
						allListFiles = listData;
					} else {
						allListFiles.addAll(listData);
					}
					docAdapter.setmDocs(allListFiles);
					pageIndex++;
					resetStartLimit();
					mFooterView.onLoadComplete(docAdapter.getCount(), docResult.getData().getTotal());
				} else if (listData != null && listData.size() == 0){
					if (pageIndex == 0) {
						mEmptyView.showView(R.drawable.emptylist_icon, no_search_data);
					}else {
						mEmptyView.showView(R.drawable.emptylist_icon, R.string.load_data_fail);
					}
				} else {
					mEmptyView.showView(R.drawable.emptylist_icon, no_data);
				}
			} else {
				mEmptyView.showView(R.drawable.emptylist_icon, R.string.to_server_fail);
			}
			break;
		case RES_MUS_QUERY_TYPE:
		case RES_MUSIC_QUERY:
			mPullToRefreshListView.onRefreshComplete();
			SearchLbsMusicResult musResult = JSONUtil.fromJson(response.toString(), SearchLbsMusicResult.class);
			if (musResult != null && musResult.isSuccess()) {
				List<MusicInfo> newMusicList = musResult.getData().getData();
				if (newMusicList != null && newMusicList.size() > 0) {
					if (pageIndex == 0) {
						allMusicList = newMusicList;
					} else {
						allMusicList.addAll(newMusicList);
					}
					musicListAdapter.setRowContent(allMusicList);
					pageIndex++;
					resetStartLimit();
					mFooterView.onLoadComplete(musicListAdapter.getCount(), musResult.getData().getTotal());
				} else if (newMusicList != null && newMusicList.size() == 0){
					if (pageIndex == 0) {
						mEmptyView.showView(R.drawable.emptylist_icon,no_search_data);
					}else {
						mEmptyView.showView(R.drawable.emptylist_icon, R.string.load_data_fail);
					}
				} else {
					mEmptyView.showView(R.drawable.emptylist_icon, no_data);
				}
			} else {
				mEmptyView.showView(R.drawable.emptylist_icon, R.string.to_server_fail);
			}
			break;

		case RES_PIC_QUERY_TYPE:
		case RES_PIC_QUERY:
			mPullToRefreshListView.onRefreshComplete();
			SearchLbsPictureResult picResult = JSONUtil.fromJson(response.toString(), SearchLbsPictureResult.class);
			if (picResult != null && picResult.isSuccess()) {
				List<PicInfo> picList = picResult.getData().getData();
				if (picList != null && picList.size() > 0) {
					if (pageIndex == 0) {
						photoList = picList;
					} else {
						photoList.addAll(picList);
					}
					picAdapter.setPhotoResult(photoList);
					pageIndex++;
					resetStartLimit();
					mFooterView.onLoadComplete(photoList.size(), picResult.getData().getTotal());
				} else if (picList != null && picList.size() == 0){
					if (pageIndex == 0) {
						mEmptyView.showView(R.drawable.emptylist_icon, no_search_data);
					}else {
						mEmptyView.showView(R.drawable.emptylist_icon, R.string.load_data_fail);
					}
				} else {
					mEmptyView.showView(R.drawable.emptylist_icon, no_data);
				}
			} else {
				mEmptyView.showView(R.drawable.emptylist_icon, R.string.to_server_fail);
			}
			break;
			
		case RES_APP_QUERY_TYPE:
		case RES_APP_QUERY:
			mPullToRefreshListView.onRefreshComplete();
			SearchLbsAppResult appResult = JSONUtil.fromJson(response.toString(), SearchLbsAppResult.class);
			if (appResult != null && appResult.isSuccess()) {
				List<AppInfo> newAppList = appResult.getData().getData();
				if (newAppList != null && newAppList.size() > 0) {
					if (pageIndex == 0) {
						appList = newAppList;
					} else {
						appList.addAll(newAppList);
					}
					appAdapter.setmApps(appList);
					pageIndex++; 
					resetStartLimit();
					mFooterView.onLoadComplete(appAdapter.getCount(), appResult.getData().getTotal());
				} else if (newAppList != null && newAppList.size() == 0){
					if (pageIndex == 0) {
						mEmptyView.showView(R.drawable.emptylist_icon, no_search_data);
					}else {
						mEmptyView.showView(R.drawable.emptylist_icon, R.string.load_data_fail);
					}
				} else {
					mEmptyView.showView(R.drawable.emptylist_icon, no_data);
				}
			} else {
				mEmptyView.showView(R.drawable.emptylist_icon, R.string.to_server_fail);
			}
			break;

		case FAVORITE:
			retransmitResult(response.toString());
			break;
			
		case DIR_SEARCH:
				ResDirQueryResult dirResult = JSONUtil.fromJson(response.toString(), ResDirQueryResult.class);
				if (dirResult != null && BaseResult.SUCCESS == dirResult.errorCode) {
					showDir(dirResult.getDirinfos());
				} else {
					if (dirResult != null) {
						showToast(dirResult.getErrorMessage());
					}
				}
				break;
				
		case DIR_TO_ME:
				BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
				if (result != null && BaseResult.SUCCESS == result.errorCode) {
					showToast("收藏成功!");
					selectItem.setFavorTimes(selectItem.getFavorTimes() + 1);
					switch (resType) {
					case RES_DOC_QUERY_TYPE:
						docAdapter.notifyDataSetChanged();
						break;
					case RES_MUS_QUERY_TYPE:
						musicListAdapter.notifyDataSetChanged();
						break;
					case RES_PIC_QUERY_TYPE:
						break;
					case RES_APP_QUERY_TYPE:
						appAdapter.notifyDataSetChanged();
						break;
					}
				} else {
					if (result != null) {
						showToast(result.getErrorMessage());
					}
				}
				break;
		}
	}

	private void retransmitResult(String response) {
		// TODO Auto-generated method stub
		try {
			ResShareResult retransmitAsk = JSONUtil.fromJson(response.toString(), ResShareResult.class);
			if (retransmitAsk != null && retransmitAsk.errorCode == BaseResult.SUCCESS) {
				showToast(retransmitAsk.errorMessage);
			} else if (retransmitAsk != null && retransmitAsk.errorCode == 0) {
				showToast(retransmitAsk.errorMessage);
			} else {
				showToast(R.string.res_retransmit_failed);
			}
		} catch (Exception e) {
			showToast(R.string.res_retransmit_failed);
			logger.error(TAG, e);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			NetResSearchActivity.this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		NetResSearchActivity.this.finish();
	}

	protected void onDestroy() {
		mImageCache.removeMemoryCaches();
		super.onDestroy();
	};
	
}
