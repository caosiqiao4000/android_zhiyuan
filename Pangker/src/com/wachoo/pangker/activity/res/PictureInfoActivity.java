package com.wachoo.pangker.activity.res;

import java.util.ArrayList;

import mobile.json.JSONUtil;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.os.Message;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.gestures.GestureCommandRegister;
import com.wachoo.pangker.gestures.GestureListener;
import com.wachoo.pangker.gestures.NextCommend;
import com.wachoo.pangker.gestures.PrevCommend;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.listener.ShowhideOnClickListener;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CommentResult;
import com.wachoo.pangker.server.response.ResInfo;
import com.wachoo.pangker.server.response.ResShareResult;
import com.wachoo.pangker.server.response.SearchCommentsResult;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.KeyboardLayout;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.util.ConnectivityHelper;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class PictureInfoActivity extends ResShowActivity implements GestureListener {

	private Button btn_back;// 返回
	private TextView txt_title;
	private ImageView iv_cover;// res iocn
	private TextView tv_ressize;
	private EditText tv_resname;// res name
	private RatingBar rb_res_score; // res score
	private TextView tv_forwardtimes;// 资源分享次数
	private TextView tv_favoritetimes;// favorite times
	private TextView tv_downloadtimes;// download times
	private TextView tv_description;
	private TextView tvResonShow;
	private TextView tvUploadLocation;

	private UIUserInfo uiUserInfo;
	private UIOperate uiOperate;

	private PoiaddressLoader poiLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.net_pic_info);
		poiLoader = new PoiaddressLoader(getApplicationContext());
		init();
		initView();
		initGesture();

		if (application.getResLists() == null || application.getResLists().size() <= 0) {
			showToast("Err!");
			return;
		}
		resShowEntity = application.getResLists().get(application.getViewIndex());
		if (resShowEntity == null) {
			showToast("Err!");
			return;
		}
	}

	private void init() {
		// TODO Auto-generated method stub
		// >>>>>>>>>>图片缓存区
		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);

		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
	}

	private void initView() {
		// TODO Auto-generated method stub
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(onClickListener);
		txt_title = (TextView) findViewById(R.id.mmtitle);

		iv_cover = (ImageView) findViewById(R.id.iv_picshow);
		tv_ressize = (TextView) findViewById(R.id.tv_ressize);
		iv_cover.setOnClickListener(picshowClickListener);
		tv_resname = (EditText) findViewById(R.id.tv_resname);
		rb_res_score = (RatingBar) findViewById(R.id.rb_res_score);
		tv_description = (TextView) findViewById(R.id.tv_description);
		tvResonShow = (TextView) findViewById(R.id.tv_reson_show);
		tv_description.setOnLongClickListener(descLongClickListener);
		tv_favoritetimes = (TextView) findViewById(R.id.tv_favoritetimes);
		tv_downloadtimes = (TextView) findViewById(R.id.tv_downloadtimes);
		tv_forwardtimes = (TextView) findViewById(R.id.tv_forwardtimes);
		tvUploadLocation = (TextView) findViewById(R.id.tv_upload_location);

		uiOperate = new UIOperate();
		uiComment = new UIComment();
		uiUserInfo = new UIUserInfo();
		uiSubmit = new UICommentSubmit();

		mKeyboardLayout = (KeyboardLayout) findViewById(R.id.kl_res_info);
		initData(true);
	}

	private void initData(boolean ifLoad) {
		txt_title.setText("资源详情");
		tv_resname.setText(resShowEntity.getResName());
		tv_description.setText(resShowEntity.getResName());
		tv_forwardtimes.setText(timesText(String.valueOf(resShowEntity.getShareTimes())));
		tv_favoritetimes.setText(timesText(String.valueOf(resShowEntity.getFavorTimes())));
		tv_downloadtimes.setText(timesText(String.valueOf(resShowEntity.getDownloadTimes())));
		uiUserInfo.hideBroader();

		initCover();
		showComment();
		if (ifLoad) {
			getResInfo();
			uiOperate.enableOperateAll(false);
			uiComment.showSerach();
			searchComment();
		}
	}

	private void initCover() {
		// TODO Auto-generated method stub
		int sideLength = ImageUtil.getPicMiddlePreviewResolution(this);
		String IconUrl = ImageUtil.getWebImageURL(resShowEntity.getResID(), sideLength);
		mImageWorker.loadImage(IconUrl, iv_cover, String.valueOf(resShowEntity.getResID()) + "_" + sideLength);
	}

	protected void showComment() {
		// TODO Auto-generated method stub
		if (pViewFlipper.getDisplayedChild() == 1) {
			pViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_from_bottom));
			pViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_to_bottom));
			pViewFlipper.setDisplayedChild(0);
		}
	}

	GestureCommandRegister register = new GestureCommandRegister();

	private void initGesture() {
		// TODO Auto-generated method stub
		mViewFlipper = (ViewFlipper) findViewById(R.id.DownloadViewFlipper);
		mGestureOverlayView = (GestureOverlayView) findViewById(R.id.gestures);

		if (application.getResLists().size() > 1) {
			register.registerCommand("next", new NextCommend(this));
			register.registerCommand("prev", new PrevCommend(this));
			application.getGestureHandler().setRegister(register);
			mGestureOverlayView.addOnGesturePerformedListener(application.getGestureHandler());
		}
	}

	View.OnClickListener picshowClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			showImage();
		}
	};

	private void showImage() {
		// TODO Auto-generated method stub
		if (!application.ismExternalStorageAvailable()) {
			showToast("SD卡不存在,无法浏览图片!");
			return;
		}
		ArrayList<PictureViewInfo> imagePathes = new ArrayList<PictureViewInfo>();
		for (ResShowEntity entity : application.getResLists()) {
			PictureViewInfo viewInfo = new PictureViewInfo();
			viewInfo.setNet(true);
			String imgUrl = Configuration.getImageUrl() + String.valueOf(entity.getResID());
			viewInfo.setPicPath(imgUrl);
			viewInfo.setPicName(entity.getResName());

			imagePathes.add(viewInfo);
		}
		Intent it = new Intent(this, PictureBrowseActivity.class);
		it.putExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY, imagePathes);
		it.putExtra(PictureBrowseActivity.START_FLAG, PictureBrowseActivity.NET_RESINFO);
		it.putExtra("index", application.getViewIndex());
		startActivityForResult(it, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			Log.i("PicinfoActivity", "onRestart");
			if (application.getResLists() == null || application.getResLists().size() <= 0) {
				showToast("Err!");
				// finish();
				return;
			}

			ResShowEntity mNewResShowEntity = application.getResLists().get(application.getViewIndex());
			if (mNewResShowEntity != null && resShowEntity != null
					&& resShowEntity.getResID() != mNewResShowEntity.getResID()) {
				this.resShowEntity = mNewResShowEntity;
				// >>>>>>重新设置
				initData(true);
			}
		}
	}

	public void editResinfo() {
		if (tv_resname.isEnabled()) {
			if (Util.isEmpty(tv_description.getText().toString())) {
				showToast("图片描述不能为空!");
				return;
			}
			resEditSave(tv_description.getText().toString(), tv_description.getText().toString());
		} else {
			tv_resname.setEnabled(true);
			tv_description.setEnabled(true);
		}
	}

	private void checkEdited() {
		// TODO Auto-generated method stub
		checkMenuEdit();
		toggleComment();
	}

	private void checkMenuEdit() {
		// TODO Auto-generated method stub
		if (pViewFlipper.getDisplayedChild() == 1) {
			quickAction.removeActionItem(editActionItem);
			editActionItem.setTitle("进行编辑");
			quickAction.addActionItem(editActionItem);
			tv_resname.setEnabled(false);
			tv_description.setEnabled(false);
			tv_description.setClickable(false);
		} else {
			quickAction.removeActionItem(editActionItem);
			editActionItem.setTitle("取消编辑");
			quickAction.addActionItem(editActionItem);
			tv_resname.setEnabled(true);
			tv_description.setEnabled(true);
			tv_description.setClickable(true);
		}
	}

	private void initEditMenu() {
		// TODO Auto-generated method stub
		editActionItem.setTitle("进行编辑");
		tv_resname.setEnabled(false);
		tv_description.setEnabled(false);
		tv_description.setClickable(false);
		quickAction.addActionItem(editActionItem, true);
	}

	private void checkShare() {
		// TODO Auto-generated method stub
		if (SERACH_TYPE == 4) {
			uiOperate.btn_menu.setVisibility(View.GONE);
			uiUserInfo.iv_share.setVisibility(View.GONE);
			pViewFlipper.setDisplayedChild(1);
			tv_resname.setEnabled(true);
			tv_description.setEnabled(true);
			tv_description.setClickable(true);
		} else if (SERACH_TYPE != 4 && SERACH_TYPE != 0) {
			quickAction.clearActionItem();
			uiUserInfo.iv_share.setVisibility(View.VISIBLE);
			if (resInfo.ifShare == 0) {
				uiUserInfo.iv_share.setImageResource(R.drawable.icon32_unshare);
				shareActionItem.setTitle("分享资源");
				if (SERACH_TYPE == 2) {
					quickAction.addActionItem(shareActionItem);
					quickAction.addActionItem(new ActionItem(2, "删除资源"));
					initEditMenu();
				}
				if (SERACH_TYPE == 3) {
					if (resShowEntity.isIfshare()) {
						quickAction.addActionItem(new ActionItem(4, "取消转播"));
					} else {
						quickAction.addActionItem(shareActionItem);
						quickAction.addActionItem(new ActionItem(2, "删除资源"));
					}
				}
			} else {
				uiUserInfo.iv_share.setImageResource(R.drawable.icon32_share);
				shareActionItem.setTitle("取消分享");
				if (SERACH_TYPE == 2) {
					quickAction.addActionItem(shareActionItem);
					quickAction.addActionItem(new ActionItem(2, "删除资源"));
					initEditMenu();
				}
				if (SERACH_TYPE == 3) {
					if (resShowEntity.isIfshare()) {
						quickAction.addActionItem(new ActionItem(4, "取消转播"));
					} else {
						quickAction.addActionItem(shareActionItem);
						quickAction.addActionItem(new ActionItem(2, "删除资源"));
					}
				}
			}
		}
	}

	@Override
	public void callbackErr(int caseKey) {
		// TODO Auto-generated method stub
		if (caseKey == SEARCH_COMMENT) {
			uiComment.serachCommentFail();
		}
		if (caseKey == KEY_EDITRES) {
			checkEdited();
		}
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.btn_back) {
				finish();
			}
			if (v.getId() == R.id.iv_res_ownericon) {
				Intent intent = new Intent(PictureInfoActivity.this, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, resShowEntity.getShareUid());
				startActivity(intent);
			}
		}
	};

	@Override
	public void download(boolean blag, boolean isopen) {
		// TODO Auto-generated method stub
		if (!Util.isExistSdkard()) {
			showToast("SD卡找不到，不能下载!");
			return;
		}
		resInfo.setDownloadTimes(resInfo.downloadTimes + 1);
		tv_downloadtimes.setText(timesText(String.valueOf(resInfo.downloadTimes)));

		String fileurl = Configuration.getResDownload() + resInfo.getSid();
		String filename = null;
		if (resInfo.getResName().contains(resInfo.getFileFormat())) {
			filename = resInfo.getResName();
		} else {
			filename = resInfo.getResName() + "." + resInfo.getFileFormat();
		}

		String savePath = getSavePath();
		// >>>>>>>>>>初始化下载任务
		DownloadJob downloadJob = new DownloadJob();
		downloadJob.setmResID(Integer.parseInt(resInfo.getSid()));
		double endPos = resInfo.getFileSize();
		downloadJob.setEndPos((int) endPos);
		downloadJob.setDoneSize(0);
		downloadJob.setUrl(fileurl);
		downloadJob.setFileName(filename);
		downloadJob.setSaveFileName(resInfo.getResName() + DownloadJob.SUFFIX_RESID + resInfo.getSid() + "."
				+ resInfo.getFileFormat());
		downloadJob.setSavePath(savePath);
		downloadJob.setType(resInfo.getResType());
		downloadJob.setDwonTime(Util.getSysNowTime());
		downloadJob.setStatus(DownloadJob.DOWNLOAD_INIT);
		// >>>>>>设置下载任务的id
		long JobID = downloadDao.saveDownloadInfo(downloadJob);
		if (JobID > 0) {
			downloadJob.setId((int) JobID);
			showToast(resInfo.getResName() + "已经加入到下载列表中。");
			// >>>>>>>添加下载任务
			downloadManager.addDownloadJob(downloadJob);
			Intent intent = new Intent(this, PangkerService.class);
			intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DOWNLOAD);
			// >>>>>>>>>>传递上传jobID到service
			intent.putExtra(DownloadJob.DOWNLOAD_JOB_ID, downloadJob.getId());
			startService(intent);
		}
	}

	private void showScore() {
		float score = 0f;
		float sum = (float) resInfo.getScore() / 2;
		if (resInfo.appraiseTimes != null && resInfo.appraiseTimes >= 0) {
			score = sum / resInfo.appraiseTimes;
		}
		rb_res_score.setRating(score);

		// 显示资源描述
		String des = resInfo.getPicDesc();
		tv_description.setFilters(ShowhideOnClickListener.filters);
		if (des.length() > PangkerConstant.showLength) {
			tvResonShow.setText("显示更多");
			tvResonShow.setVisibility(View.VISIBLE);
		} else
			tvResonShow.setVisibility(View.GONE);
		tvResonShow.setOnClickListener(new ShowhideOnClickListener(tv_description, des));
		tv_description.setText(des);
	}

	@Override
	public void showResInfo(ResInfo resinfo) {
		if (resinfo == null) {
			showToast("资源不存在!");
			return;
		}
		if (resinfo.isMyres == 0 && resinfo.ifShare == 0) {
			showToast("该用户已经取消分享,暂时不能查看!");
			return;
		}
		this.resInfo = resinfo;
		uiOperate.enableOperateAll(true);
		uiOperate.checkDownload();
		checkShare();

		tv_resname.setText(resinfo.getResName());
		String fileSize = Formatter.formatFileSize(this, resinfo.getFileSize().longValue());
		tv_ressize.setText("大小:" + fileSize);
		showScore();
		tv_favoritetimes.setText(timesText(String.valueOf(resinfo.getFavorTimes())));
		tv_downloadtimes.setText(timesText(String.valueOf(resinfo.getDownloadTimes())));
		tv_forwardtimes.setText(timesText(String.valueOf(resinfo.getShareTimes())));

		if(!Util.isEmpty(resInfo.getUpPoi())){
			tvUploadLocation.setText(resInfo.getUpPoi());
		} else {
			poiLoader.onPoiLoader(resinfo.getLocation(), this, new PoiaddressLoader.GeoCallback() {
				@Override
				public void onGeoLoader(String address, int code) {
					// TODO Auto-generated method stub
					tvUploadLocation.setText(address);
				}
			});
		}

		uiUserInfo.showData();

		if (resInfo.isMyres == 1) {// 本人资源
			uiOperate.btn_favorite.setBackgroundResource(R.drawable.form_vipinf_but_keep_c);
			uiOperate.btn_favorite.setEnabled(false);
			uiOperate.btn_forward.setBackgroundResource(R.drawable.form_vipinf_but_relay_c);
			uiOperate.btn_forward.setEnabled(false);
			uiOperate.enableSocre(false);// 不能评分，
			uiOperate.btn_download.setBackgroundResource(R.drawable.res_btn_downed);
		}
		if (resInfo.isScore != null && resInfo.isScore == 1) {
			uiOperate.enableSocre(false);
		}
		if (resInfo.isGarner != null && resInfo.isGarner == 1) {
			uiOperate.btn_favorite.setBackgroundResource(R.drawable.form_vipinf_but_keep_c);
			uiOperate.btn_favorite.setEnabled(false);
		}
		if (resInfo.isRebroadcast != null && resInfo.isRebroadcast == 1) {
			uiOperate.btn_forward.setBackgroundResource(R.drawable.form_vipinf_but_relay_c);
			uiOperate.btn_forward.setEnabled(false);
		}
	}

	class UIOperate implements View.OnClickListener {

		public Button btn_menu;// 菜单
		/**
		 * 资源的操作分为3种情况
		 * --1：资源进行加载的时候不能进行操作;2：资源是自己的，不能进行相关的操作，以及提示;3：已经进行过的操作，不能进行重复操作
		 */
		public Button btn_praise; // 赞
		public Button btn_tread; // 踩
		public Button btn_score; // 评分

		public Button btn_download;// 下载
		public Button btn_favorite; // 收藏
		public Button btn_forward;// 转播
		public Button btn_recommend;// 推荐

		public UIOperate() {
			// 赞
			btn_praise = (Button) findViewById(R.id.btn_praise);
			btn_praise.setOnClickListener(this);
			// 踩
			btn_tread = (Button) findViewById(R.id.btn_tread);
			btn_tread.setOnClickListener(this);
			// 评分
			btn_score = (Button) findViewById(R.id.btn_score);
			btn_score.setOnClickListener(this);
			// 评论
			btn_download = (Button) findViewById(R.id.btn_download);
			btn_download.setOnClickListener(this);
			// 收藏
			btn_favorite = (Button) findViewById(R.id.btn_favorite);
			btn_favorite.setOnClickListener(this);
			// 转播
			btn_forward = (Button) findViewById(R.id.btn_forward);
			btn_forward.setOnClickListener(this);
			// 推荐
			btn_recommend = (Button) findViewById(R.id.btn_recommend);
			btn_recommend.setOnClickListener(this);

			btn_menu = (Button) findViewById(R.id.iv_more);
			btn_menu.setBackgroundResource(R.drawable.btn_menu_bg);
			btn_menu.setOnClickListener(this);
			if (SERACH_TYPE == 0) {
				btn_menu.setVisibility(View.GONE);
			} else {
				btn_menu.setVisibility(View.VISIBLE);
			}
		}

		public void checkDownload() {
			String fileurl = Configuration.getResDownload() + resShowEntity.getResID();
			DownloadJob downloadInfo = downloadDao.getDownloadInfo(fileurl);
			if (downloadInfo == null) {
				Log.d("UIDownload", "SDK");
				// 到Sd卡判断有没有这个文件
				String filename = resInfo.getResName() + "." + resInfo.getFileFormat();
				if (Util.isExitFileSD(PangkerConstant.DIR_PICTURE + "/" + filename)) {
					isDownloaded = true;
					btn_download.setBackgroundResource(R.drawable.res_btn_downed);
				}
			} else {
				if (downloadInfo.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
					isDownloaded = true;
					btn_download.setBackgroundResource(R.drawable.res_btn_downed);
				}
			}
		}

		public void enableOperateAll(boolean flag) {
			btn_favorite.setEnabled(flag);
			btn_forward.setEnabled(flag);
			btn_download.setEnabled(flag);
			btn_praise.setEnabled(flag);
			btn_tread.setEnabled(flag);
			btn_recommend.setEnabled(flag);
			btn_score.setEnabled(flag);

			if (flag) {
				btn_favorite.setBackgroundResource(R.drawable.res_btn_collect);
				btn_forward.setBackgroundResource(R.drawable.res_btn_broadcast);
				btn_download.setBackgroundResource(R.drawable.res_btn_down);
				btn_praise.setBackgroundResource(R.drawable.btn_praise_bg);
				btn_tread.setBackgroundResource(R.drawable.btn_hate_bg);
				btn_score.setBackgroundResource(R.drawable.res_btn_score);
			}
		}

		public void enableSocre(boolean b) {
			if (!b) {
				btn_praise.setBackgroundResource(R.drawable.form_vipinf_but_good_p);
				btn_tread.setBackgroundResource(R.drawable.form_vipinf_but_stepup_p);
				btn_score.setBackgroundResource(R.drawable.form_vipinf_but_poin_p);
				btn_praise.setEnabled(false);
				btn_tread.setEnabled(false);
				btn_score.setEnabled(false);
			}
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btn_recommend) {
				Intent intent = new Intent(PictureInfoActivity.this, ContactsSelectActivity.class);
				intent.putExtra(ContactsSelectActivity.INTENT_SELECT, ContactsSelectActivity.TYPE_RES_RECOMMEND);
				intent.putExtra(ResShowEntity.RES_ENTITY, resShowEntity);
				startActivity(intent);
				return;
			}
			if (resInfo == null) {
				showToast("正在加载,还不能进行该操作,请稍等!");
				return;
			}
			if (v == btn_menu) {
				quickAction.show(v);
			}

			if (v == btn_forward) {
				transRes();
			}
			if (v == btn_favorite) {
				searchDirInfo(resShowEntity.getResType());
			}
			if (v == btn_praise) {
				ratingServer("0", 10);
			}
			if (v == btn_tread) {
				ratingServer("1", 0);
			}
			if (v == btn_score) {
				ratingPopup.show();
			}
			// 下载事件
			if (v == btn_download) {
				// add by wangxin start
				// 判断是否是手机网络，给用户提示节省流量
				int apnType = ConnectivityHelper.getAPNType(PictureInfoActivity.this);
				if (apnType == -1) {
					showToast("请检查您的网络！");
					return;
				}
				// 判断文件是否正在下载
				String fileurl = Configuration.getResDownload() + resInfo.getSid();
				if (downloadManager.isHttpFileDowning(fileurl)) {
					Intent intent = new Intent(PictureInfoActivity.this, UpDownLoadManagerActivity.class);
					startActivity(intent);
					return;
				}

				// 是否下载过
				if (isDownloaded == true) {
					showDownloadDialog();
					return;
				}
				download(isDownloaded, false);
			}
		}
	}

	public void dealBaseResult(int key) {
		// TODO Auto-generated method stub
		if (key == KEY_COLLECT) {
			showToast("收藏成功!");
			tv_favoritetimes.setText(timesText(String.valueOf(resInfo.getFavorTimes())));
			uiOperate.btn_favorite.setEnabled(false);
			uiOperate.btn_favorite.setBackgroundResource(R.drawable.form_vipinf_but_keep_c);
		}
		if (key == KEY_RATINGS) {
			showScore();
			showToast("评分成功!");
			uiOperate.enableSocre(false);
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		super.uiCallBack(response, caseKey);
		if (!HttpResponseStatus(response))
			return;
		// 评分,收藏
		if (caseKey == KEY_RATINGS || caseKey == KEY_COLLECT) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				dealBaseResult(caseKey);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == KEY_TRANS) {// 转播
			ResShareResult result = JSONUtil.fromJson(response.toString(), ResShareResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showToast("转播成功!");
				tv_forwardtimes.setText(timesText(String.valueOf(resInfo.getShareTimes())));
				uiOperate.btn_forward.setBackgroundResource(R.drawable.form_vipinf_but_relay_c);
				uiOperate.btn_forward.setEnabled(false);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == SUBMIT_COMMENT) {// 发送评论
			CommentResult submitResult = JSONUtil.fromJson(response.toString(), CommentResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				showToast(submitResult.errorMessage);

				uiSubmit.etComment.setText("");
				uiSubmit.getNewCommentinfo().setCommentId(String.valueOf(submitResult.getCommentId()));
				// >>>>>不需要再次获取
				uiComment.addComment(uiSubmit.getNewCommentinfo());
				// >>>>通知对方评论成功
				sendMessageTip();
			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
		}
		if (caseKey == SEARCH_COMMENT) {
			SearchCommentsResult commentResult = JSONUtil.fromJson(response.toString(), SearchCommentsResult.class);
			uiComment.showComments(commentResult);
		}
		if (caseKey == KEY_EDITRES) {
			if (SERACH_TYPE != 4) {
				checkEdited();
			}
			BaseResult resEditResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (resEditResult != null && resEditResult.getErrorCode() == BaseResult.SUCCESS) {
			} else if (resEditResult != null && resEditResult.getErrorCode() == BaseResult.FAILED) {
				showToast(resEditResult.getErrorMessage());
				if (SERACH_TYPE == 4) {
					finish();
				}
			} else
				showToast(R.string.to_server_fail);
		}
		if (caseKey == DEL_COMMENT) {
			BaseResult submitResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				uiComment.deleteComment();
				showToast(submitResult.errorMessage);
			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_delete_failed);
		}
		if (caseKey == KEY_SHARE) {// 分享
			ResShareResult result = JSONUtil.fromJson(response.toString(), ResShareResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				resInfo.ifShare = resInfo.ifShare == 1 ? 0 : 1;
				checkShare();
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == KEY_DELETE || caseKey == KEY_FORWARD) {// 资源删除，显示下一张，没有就关闭
			BaseResult submitResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				showToast(submitResult.errorMessage);
				if (caseKey == KEY_DELETE) {
					Message msg = new Message();
					msg.what = 1;
					PangkerManager.getTabBroadcastManager().sendResultMessage(ResNetManagerActivity.class.getName(),
							msg);
				}
				application.getResLists().remove(application.getViewIndex());
				if (application.getResLists().size() > 0) {
					if (application.getViewIndex() < application.getResLists().size()) {
						resShowEntity = application.getResLists().get(application.getViewIndex());
						initData(true);
					} else if (application.viewIndexMinus() >= 0) {
						resShowEntity = application.getResLists().get(application.getViewIndex());
						initData(true);
					}
				} else {
					finish();
				}
			} else if (submitResult != null) {
				showToast(submitResult.errorMessage);
			}
		}
	}

	@Override
	public void gestureNext() {
		// TODO Auto-generated method stub
		if (isLoading) {
			return;
		}
		if (application.getViewIndex() > 0) {
			application.viewIndexMinus();
			resShowEntity = application.getResLists().get(application.getViewIndex());
			initData(true);
		} else {
			showToast("已经是第一个了!");
		}
	}

	@Override
	public void gesturePrev() {
		// TODO Auto-generated method stub
		if (isLoading) {
			return;
		}
		if (application.getViewIndex() < application.getResLists().size() - 1) {
			application.viewIndexAdd();
			resShowEntity = application.getResLists().get(application.getViewIndex());
			initData(true);
		} else {
			showToast("已经是最后一个!");
		}
	}

	// 进入资源详细信息界面
	private void editRes() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ResInfoEditActivity.class);
		intent.putExtra("resShowEntity", resShowEntity);
		startActivity(intent);
	}

	private MessageTipDialog mMessageTipDialog;

	private void showTipDialog() {
		// TODO Auto-generated method stub
		if (mMessageTipDialog == null) {
			mMessageTipDialog = new MessageTipDialog(mResultCallback, this);
		}
		mMessageTipDialog.showDialog("", "确认要删除该资源？");
	}

	MessageTipDialog.DialogMsgCallback mResultCallback = new MessageTipDialog.DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if (isSubmit) {
				deleteRes();
			}
		}
	};

	@Override
	public void onItemClick(QuickAction source, int pos, int actionId) {
		// TODO Auto-generated method stub
		if (actionId == 1) {
			ushareResInfo(null);
		}
		if (actionId == 2) {
			showTipDialog();
		}
		if (actionId == 3) {
			editRes();
			// checkMenuEdit();
			// toggleComment();
		}
		if (actionId == 4) {
			ushareResInfo(resShowEntity.getShareId());
		}
	}

	protected void onDestroy() {
		mImageCache.removeMemoryCaches();
		super.onDestroy();
	};
}
