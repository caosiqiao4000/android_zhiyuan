package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.contact.FriendGroupActivity;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PasswordDialog;

public class ResSetDirinfoActivity extends CommonPopActivity implements
		IUICallBackInterface {

	public static final int Set_Group_Code = 0x41;
	public static final int Set_Who_Code = 0x42;

	String mess = "正在向服务器提交...";// 提交提示信息
	private String userId;
	private DirInfo dirInfo;
	private ServerSupportManager serverMana;// 后台交互
	private int index;
	private IResDao iResDao;// 仅仅用于修改顶层目录

	private TextView txtTitle;
	private Button btnBack;

	private int[] LinearLayoutId = { R.id.allSelectLayout,// 1）所有好友可访问（默认）------
			R.id.alltraceLayout,// 2）所有亲友可访问
			R.id.groupSelectLayout,// 3）指定好友组可访问（点击选择某一个好友组，只能选择一个）
			R.id.unallSelectLayout, // 4）仅本人可访问-----------
			R.id.passwordLayout // 5）通过密码访问（点击可设置目录密码）
	};
	private int[] CheckBoxId = { R.id.all_CheckBox, // 1）所有好友可访问（默认）------
			R.id.alltrace_CheckBox, // 2）所有亲友可访问
			R.id.group_CheckBox,// 3）指定好友组可访问（点击选择某一个好友组，只能选择一个）
			R.id.unall_CheckBox, // 4）仅本人可访问-----------
			R.id.password_CheckBox // 5）通过密码访问（点击可设置目录密码）
	};

	private LinearLayout[] linearLayouts = new LinearLayout[CheckBoxId.length];
	private CheckBox[] checkBoxs = new CheckBox[CheckBoxId.length];

	private TextView txtGroupName;
	private PasswordDialog passwordDialog;
	private PangkerApplication mApplication;

	private String mSelectedGroupID = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dirinfo_set);
		init();
		initView();
	}

	private void init() {
		// TODO Auto-generated method stub
		dirInfo = (DirInfo) getIntent().getSerializableExtra("DirInfo");
		mApplication = (PangkerApplication) getApplication();
		userId = mApplication.getMyUserID();
		iResDao = new ResDaoImpl(this);
		serverMana = new ServerSupportManager(this, this);
	}

	private void initView() {
		// TODO Auto-generated method stub
		txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText(dirInfo.getDirName());
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		txtGroupName = (TextView) findViewById(R.id.tv_group_name);
		// >>>>>>>>>设置当前选中的分组
		for (int i = 0; i < DirInfo.RES_VISIBLE.length; i++) {
			// >>>>>>>获取设置类别
			linearLayouts[i] = (LinearLayout) findViewById(LinearLayoutId[i]);
			// >>>>>>>设置监听
			linearLayouts[i].setOnClickListener(onClickListener);

			// >>>>>>>获取选中框
			checkBoxs[i] = (CheckBox) findViewById(CheckBoxId[i]);
			// >>>>>>>>>>设置当前设定的权限
			if (dirInfo.getVisitType() != null
					&& dirInfo.getVisitType() == DirInfo.RES_VISIBLE[i]) {
				checkBoxs[i].setChecked(true);
				this.index = i;
				// >>>>>>>如果是设定选择的好友分组
				if (dirInfo.getVisitType() == PangkerConstant.Role_CONTACT_GROUP) {
					int start = dirInfo.getVisitGroups().indexOf("[") + 1;
					int end = dirInfo.getVisitGroups().indexOf("]");
					// >>>>>>>设置当前选中的群组的名称
					mSelectedGroupID = dirInfo.getVisitGroups().substring(
							start, end);
					txtGroupName.setText(mApplication
							.getFriendGroupNameByID(mSelectedGroupID));
				}
			}
		}

		if (dirInfo.getVisitType() == null) {
			checkBoxs[0].setChecked(true);
			this.index = 0;
		}
	}

	// >>>>>>>设置密码对话框
	private void showPasswordDialog() {
		// TODO Auto-generated method stub
		if (passwordDialog == null) {
			passwordDialog = new PasswordDialog(this, R.style.MyDialog);
		}
		passwordDialog.setResultCallback(rMsgCallback);
		passwordDialog.showDialog();
	}

	// >>>>>>>>>密码框
	MessageTipDialog.DialogMsgCallback rMsgCallback = new MessageTipDialog.DialogMsgCallback() {

		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if (passwordDialog.checkPass() && isSubmit) {
				setDirInfo(String.valueOf(DirInfo.RES_VISIBLE[index]));
			}
		}
	};

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				setResult();
				return;
			}
			for (int i = 0; i < DirInfo.RES_VISIBLE.length; i++) {
				if (v == linearLayouts[i]) {

					// >>>>>>>>设置当前选中的项目
					index = i;

					if (v.getId() == R.id.allSelectLayout // >>>>>>>>>所有好友可以访问
							|| v.getId() == R.id.alltraceLayout // 所有亲友可访问
							|| v.getId() == R.id.unallSelectLayout) {// 所有亲友可访问
						if (!checkBoxs[index].isChecked()) {
							setDirInfo(String
									.valueOf(DirInfo.RES_VISIBLE[index]));
						}
					}
					// >>>>>>>>>联系人分组
					else if (v.getId() == R.id.groupSelectLayout) {
						Intent intent = new Intent();
						intent.putExtra("fromType", 3);
						intent.putExtra("dirid", dirInfo.getDirId());
						intent.putExtra(
								FriendGroupActivity.SELECT_CONTACT_GROUP_ID,
								mSelectedGroupID);
						intent.setClass(ResSetDirinfoActivity.this,
								FriendGroupActivity.class);
						startActivityForResult(intent, Set_Group_Code);
					}
					// >>>>>>>>密码访问
					else if (v.getId() == R.id.passwordLayout) {
						showPasswordDialog();
					}

					break;
				}
			}

		}
	};

	public void onBackPressed() {
		setResult();
	};

	private void setResult() {
		// TODO Auto-generated method stub
		Intent data = new Intent();
		data.putExtra("DirInfo", DirInfo.RES_VISIBLE[index]);
		setResult(ResNetManagerActivity.DIR_SET, data);
		finish();
	}

	private void setDirInfo(String type) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("dirid", dirInfo.getDirId()));
		String password = "";
		if (passwordDialog != null) {
			password = passwordDialog.getPassword();
		}
		paras.add(new Parameter("password", password));
		paras.add(new Parameter("type", type));
		serverMana.supportRequest(Configuration.getResInfoQuerySet(), paras,
				true, mess, 0);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			return;
		}

		BaseResult result = JSONUtil.fromJson(response.toString(),
				BaseResult.class);
		if (result != null && BaseResult.SUCCESS == result.errorCode) {
			showResult();
			// showDir(result.getDirinfos());
		} else {
			if (result != null) {
				showToast(result.getErrorMessage());
			}
		}
	}

	private void showResult() {
		// TODO Auto-generated method stub
		for (int i = 0; i < DirInfo.RES_VISIBLE.length; i++) {
			checkBoxs[i].setChecked(false);
		}
		checkBoxs[index].setChecked(true);
		dirInfo.setVisitType(DirInfo.RES_VISIBLE[index]);
		if ("0".equals(dirInfo.getParentDirId())) {
			iResDao.updateResInfo(dirInfo);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Set_Group_Code) {
			if (resultCode == 10) {// 10表示设置成功,11表示未设置
				showResult();

				// >>>>>获取当前设置的分组ID
				mSelectedGroupID = data
						.getStringExtra(FriendGroupActivity.SELECT_CONTACT_GROUP_ID);
				String groupNames = data.getStringExtra("select_group_name");
				txtGroupName.setText(groupNames);
			}
		}
		if (requestCode == Set_Who_Code) {
			if (resultCode == 10) {// 10表示设置成功,11表示未设置
				index = 4;
				showResult();
			}
		}
	}

}
