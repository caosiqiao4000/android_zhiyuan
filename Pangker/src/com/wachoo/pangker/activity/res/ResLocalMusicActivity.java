package com.wachoo.pangker.activity.res;

import java.io.File;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.MusicPlayAdapter;
import com.wachoo.pangker.db.SqliteMusicQuery;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.QuickAction;

public class ResLocalMusicActivity extends ResLocalActivity {

	private SqliteMusicQuery sqliteMusicQuery;
	private MusicInfo musicInfo;
	private MusicPlayAdapter musicAdapter;

	private Button btnBack;
	private Button btnMenu;
	private ListView musicListView;
	private EmptyView musicEmptyView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ress_music);
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("我的音乐");
		btnMenu = (Button) findViewById(R.id.iv_more);
		btnMenu.setBackgroundResource(R.drawable.btn_menu_bg);
		btnMenu.setOnClickListener(onClickListener);

		musicAdapter = new MusicPlayAdapter(this);
		musicListView = (ListView) findViewById(R.id.res_musicListView);
		musicListView.setAdapter(musicAdapter);
		musicListView.setOnItemClickListener(musicOnItemClickListener);

		musicEmptyView = new EmptyView(this);
		musicEmptyView.addToListView(musicListView);
		showMusicEmptyView();

		sqliteMusicQuery = new SqliteMusicQuery(this);
		List<MusicInfo> musicInfos = sqliteMusicQuery.getMusicList();
		musicAdapter.setMusicList(musicInfos);


		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.addActionItem(new ActionItem(3, getString(R.string.res_local_directory_lookup),
				ActionItem.buildDrawable(this, R.drawable.icon64_file)), true);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);
	}

	private void showMusicEmptyView() {
		// TODO Auto-generated method stub
		if (application.ismExternalStorageAvailable()) {
			musicEmptyView.showView(R.drawable.emptylist_icon, "提示：您还没有音乐,可以通过右上角的菜单进行操作将音乐添加进来!");
			musicEmptyView.setBtnTwo(getString(R.string.res_local_directory_lookup), twoClickListener);
		} else {
			musicEmptyView.showView(R.drawable.emptylist_icon, "提示：SD卡不存在!");
		}
	}


	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == btnBack) {
				finish();
			}
			if (v == btnMenu) {
				quickAction.show(btnMenu);
			}
		}
	};

	private void toMusicFileRes() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		intent.setClass(this, FileManagerActivity.class);
		intent.putExtra("file_type", PangkerConstant.RES_MUSIC);
		startActivityForResult(intent, RequestCode_FILE);
	}

	View.OnClickListener twoClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toMusicFileRes();
		}
	};

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 1) {
				if (resLocalStatu != ResLocalModel.Edit) {
					// adapter.init();
					// uiEdit.showEdit(true);
				} else {
					// uiEdit.showEdit(false);
				}
			}
			if (actionId == 3) {
				toMusicFileRes();
			}
		}
	};

	AdapterView.OnItemClickListener musicOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			musicInfo = (MusicInfo) parent.getItemAtPosition(position);
			// 判断是否是上传音乐
			uploadMusic(musicInfo, true);
		}
	};

	// >>>>> wangxin 上传音乐到后台type:0被选择上传 1：选择上传
	private void uploadMusic(MusicInfo musicInfo, boolean isclose) {
		// TODO Auto-generated method stub
		if (!application.ismExternalStorageAvailable()) {
			showToast("Sd卡文件找不到,无法上传!");
			return;
		}
		Intent intent = new Intent();
		intent.putExtra("intent_music", musicInfo);
		intent.putExtra(DocInfo.FILE_PATH, musicInfo.getMusicPath());
		if (resLocalStatu == ResLocalModel.change) {
			setResult(RESULT_OK, intent);
		} else {
			intent.setClass(this, UploadMusicActivity.class);
			startActivity(intent);
		}
		if (isclose) {
			this.finish();
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == RequestCode_FILE && resultCode == RESULT_OK) {
			String filepath = intent.getExtras().getString(DocInfo.FILE_PATH);
			File file = new File(filepath);
			if (!file.exists()) {
				return;
			}
			if (!sqliteMusicQuery.existMusic(filepath)) {
				MusicInfo musicInfo = new MusicInfo();
				musicInfo.setMusicPath(filepath);
				musicInfo.setMusicName(file.getName());
				musicInfo.setSinger("暂无歌手信息");
				musicAdapter.addMusic(musicInfo);
			}
		}
	}

}
