package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.ResSpeakAdapter;
import com.wachoo.pangker.entity.TextSpeakInfo;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.SearchTalkResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;

public class ResSpeakActivity extends CommonPopActivity implements IUICallBackInterface {

	private ServerSupportManager serverMana;
	private PangkerApplication application;
	private UserInfo userInfo;
	private String userId;

	private Button btnBack;
	private TextView tvTitle;
	private Button ivMore;

	private ResSpeakAdapter mSpeakAdapter;
	private PullToRefreshListView refreshListView;
	private ListView mSpeaksListView;
	private FooterView mFooterView;
	private EmptyView mEmptyView;

	private final int SEARCH_TALK = 0X30;// 查询我的说说
	private final int DELET_TALK = 0X31;// 查询我的说说
	private int pageIndex = 0;
	private static final int pageDisLen = 10;// 每一页显示的条数

	private TextSpeakInfo currentSpeakInfo;
	private UserItem mUserItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.net_speak_list);
		mUserItem = (UserItem) getIntent().getSerializableExtra("UserItem");
		initView();
		initData();
	}

	private void initView() {
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(mOnClickListener);
		tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("我的说说");
		ivMore = (Button) findViewById(R.id.iv_more);
		ivMore.setBackgroundResource(R.drawable.btn_speak_selector);// btn_common_add_selector
		ivMore.setOnClickListener(mOnClickListener);
		if (mUserItem != null) {
			ivMore.setVisibility(View.GONE);
			tvTitle.setText(mUserItem.getUserName() + "的说说");
		}

		refreshListView = (PullToRefreshListView) findViewById(R.id.my_speak_list);
		refreshListView.setOnRefreshListener(onRefreshListener);
		mSpeaksListView = refreshListView.getRefreshableView();
		mSpeaksListView.setOnItemClickListener(mOnItemClickListener);
		mSpeaksListView.setOnItemLongClickListener(mOnItemLongClickListener);

		mFooterView = new FooterView(this);
		mFooterView.addToListView(mSpeaksListView);
		mFooterView.setOnLoadMoreListener(onLoadMoreListener);
		mFooterView.hideFoot();

		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(mSpeaksListView);
	}

	private void initData() {
		// TODO Auto-generated method stub
		serverMana = new ServerSupportManager(this, this);
		application = (PangkerApplication) getApplication();
		userInfo = application.getMySelf();
		userId = application.getMyUserID();

		mSpeakAdapter = new ResSpeakAdapter(this, new ArrayList<TextSpeakInfo>());
		mSpeaksListView.setAdapter(mSpeakAdapter);

		checkSpeakList();
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			pageIndex = 0;
			checkSpeakList();
		}
	};

	FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			checkSpeakList();
		}
	};

	/**
	 * 加载我的说说
	 */
	private void checkSpeakList() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", userId));
		paras.add(new Parameter("uid", mUserItem == null ? userId : mUserItem.getUserId()));
		paras.add(new Parameter("limit", pageIndex * pageDisLen + "," + pageDisLen));
		serverMana.supportRequest(Configuration.getSearchTalk(), paras, SEARCH_TALK);
		if (pageIndex == 0) {
			refreshListView.setRefreshing(true);
		}
	}

	/**
	 * 删除说说
	 */
	private void deleteSpeakInfo() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", userId));
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("talkId", String.valueOf(currentSpeakInfo.getId())));
		serverMana.supportRequest(Configuration.getDeleteTalk(), paras, DELET_TALK);
	}

	/**
	 * diaplay my speak info list.
	 */
	private void displaySpeakList(List<TextSpeakInfo> list, int sum) {
		if (list.size() == 0 && mUserItem != null) {
			mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_speak_info1);
			return;
		} else if (list.size() == 0 && mUserItem == null) {
			mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_speak_info2);
			return;
		} else {
			for (TextSpeakInfo temp:list) {
				if (mUserItem != null) {
					temp.setUserName(mUserItem.getUserName());
				}else {
					temp.setUserName(userInfo.getUserName());
				}
			}
			if (pageIndex == 0) {
				mSpeakAdapter.setmSpeakList(list);
			} else {
				mSpeakAdapter.addmSpeakList(list);
			}
			pageIndex++;
			mFooterView.onLoadComplete(mSpeakAdapter.getCount(), sum);
		}
	}

	/**
	 * 列表项单击监听器
	 */
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
			Intent intent = new Intent(ResSpeakActivity.this, ResSpeaktInfoActivity.class);
			intent.putExtra(ResSpeaktInfoActivity.TEXT_SPEAK_INFO_KEY,
					(TextSpeakInfo) parent.getItemAtPosition(position));
			
			startActivity(intent);
		}
	};

	private PopMenuDialog descDialog;
	ActionItem[] msgMenuOwn = new ActionItem[] { new ActionItem(1, "复制"), new ActionItem(2, "删除")};
	ActionItem[] msgMenuOther = new ActionItem[]{new ActionItem(1, "复制")};

	private void showDescMenu() {
		// TODO Auto-generated method stub
		if (descDialog == null) {
			descDialog = new PopMenuDialog(this);
			descDialog.setListener(descClickListener);
			descDialog.setTitle("说说操作");
			if (mUserItem != null) {
				descDialog.setMenus(msgMenuOther);//他人说说
			}else {
				descDialog.setMenus(msgMenuOwn);//我的说说
			}
			
		}
		descDialog.show();
	}

	UITableView.ClickListener descClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			if (actionid == 1) {
				ClipboardManager clip = (ClipboardManager) ResSpeakActivity.this
						.getSystemService(Context.CLIPBOARD_SERVICE);
				clip.setText(currentSpeakInfo.getContent());
			} else if (actionid == 2) {
				deleteSpeakInfo();
			}
			descDialog.dismiss();
		}
	};

	/**
	 * 列表项长按监听器
	 */
	private OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long arg3) {
			currentSpeakInfo = (TextSpeakInfo) parent.getItemAtPosition(position);
			showDescMenu();
			return true;
		}
	};

	private void toSpeak() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		intent.setClass(this, UploadSpeakActivity.class);
		startActivityForResult(intent, 10);
	}

	private View.OnClickListener mOnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				finish();
			}
			if (v == ivMore) {
				toSpeak();
			}
		}
	};

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		refreshListView.onRefreshComplete();
		if (!HttpResponseStatus(response)) {
			return;
		}

		if (caseKey == SEARCH_TALK) {
			SearchTalkResult result = JSONUtil.fromJson(response.toString(), SearchTalkResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				List<TextSpeakInfo> list = result.getSpeakInfoList();
				displaySpeakList(list, result.getSumCount());
			} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
			} else {
				mEmptyView.showView(R.drawable.emptylist_icon, R.string.return_value_999);
				showToast(R.string.return_value_999);
			}
		}

		if (caseKey == DELET_TALK) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				// mSpeakAdapter.deletSpeakItem(currentSpeakInfo);
				pageIndex = 0;
				checkSpeakList();
				showToast(result.getErrorMessage());
			} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
			} else {
				showToast(R.string.return_value_999);
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == UploadSpeakActivity.RESULTCODE) {
			pageIndex = 0;
			checkSpeakList();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

}
