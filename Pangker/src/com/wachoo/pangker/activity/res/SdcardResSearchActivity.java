package com.wachoo.pangker.activity.res;

import java.io.File;
import java.util.List;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.Media;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.LocalAppAdapter;
import com.wachoo.pangker.adapter.LocalAppAdapter.ViewHolderApp;
import com.wachoo.pangker.adapter.LocalDocAdapter;
import com.wachoo.pangker.adapter.LocalDocAdapter.ViewHolderDoc;
import com.wachoo.pangker.adapter.LocalMusicAdapter;
import com.wachoo.pangker.adapter.LocalMusicAdapter.ViewHolderMusic;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.LocalAppInfo;
import com.wachoo.pangker.receiver.StorageEventFilter;
import com.wachoo.pangker.receiver.StorageEventReceiver;
import com.wachoo.pangker.receiver.StorageEventReceiver.StorageEventListener;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.dialog.WaitingDialog;
import com.wachoo.pangker.util.Util;

public class SdcardResSearchActivity extends CommonPopActivity{
	
	private TextView title;
	private Button btnAll;
	private Button btnSure;
	
	private ListView musicListView;
	private EmptyView emptyView;
	private boolean isSelectedAll = false;
	private LinearLayout progressLy;
	private TextView textViewPath;
	
	private LocalDocAdapter docAdapter;
	private LocalAppAdapter appAdapter;
	private LocalMusicAdapter musicAdapter;
	
	protected IResDao iResDao;
	protected String userId;
	PangkerApplication application;
	private int res_type;
	private StorageEventReceiver storageEventReceiver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.local_listview);
		init();
		initview();
		initData();
		loadSdcard();
	}
	
	private void init() {
		// TODO Auto-generated method stub
		res_type = getIntent().getIntExtra("res_type", PangkerConstant.RES_DOCUMENT);
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		iResDao = new ResDaoImpl(this);
		
		storageEventReceiver = new StorageEventReceiver(mStorageEventListener);
		registerReceiver(storageEventReceiver, new StorageEventFilter());
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver(storageEventReceiver);
		super.onDestroy();
	}
	
    private StorageEventListener mStorageEventListener = new StorageEventListener() {
		
		@Override
		public void onStorageStateChanged(boolean isAvailable, boolean writeAble) {
			// TODO Auto-generated method stub
			if(!isAvailable){
				finish();
			}
		}
	};

	private void initview() {
		// TODO Auto-generated method stub
		title = (TextView) findViewById(R.id.mmtitle);
		btnAll = (Button) findViewById(R.id.iv_more);
		btnAll.setBackgroundResource(R.drawable.btn_default_selector);
		btnAll.setText("全选");
		btnAll.setOnClickListener(onClickListener);
		btnAll.setEnabled(false);
		btnSure = (Button) findViewById(R.id.btn_back);
		btnSure.setOnClickListener(onClickListener);
		
		progressLy  = (LinearLayout) findViewById(R.id.progress_ly);
		textViewPath = (TextView) findViewById(R.id.tv_path);
		
		musicListView = (ListView) findViewById(R.id.musicListView);
		musicListView.setOnItemClickListener(onItemClickListener);
		emptyView = new EmptyView(this);
		emptyView.addToListView(musicListView);
	}
	
	private void initData() {
		// TODO Auto-generated method stub
		switch (res_type) {
		case PangkerConstant.RES_DOCUMENT:
			title.setText("本地文档");
			docAdapter = new LocalDocAdapter(this, true);
			musicListView.setAdapter(docAdapter);
			break;
        case PangkerConstant.RES_APPLICATION:
        	title.setText("本地应用");
        	appAdapter = new LocalAppAdapter(this);
        	musicListView.setAdapter(appAdapter);
			break;
        case PangkerConstant.RES_MUSIC:
        	title.setText("本地音乐");
        	musicAdapter = new LocalMusicAdapter(this, true);
    		musicListView.setAdapter(musicAdapter);
        	break;
		}
	}
	
	private void loadSdcard() {
		// TODO Auto-generated method stub
		if (application.ismExternalStorageAvailable()) {
			switch (res_type) {
			case PangkerConstant.RES_DOCUMENT:
				 new LoadDocFromSDCard().execute(); 
				break;
	        case PangkerConstant.RES_APPLICATION:
	        	 new LoadAppFromSDCard().execute();
				break;
	        case PangkerConstant.RES_MUSIC:
	        	new LoadMusicFromSDCard().execute();
				break;
			}
		} else {
			showEmptyView();
		}
	}
	
	private void showEmptyView() {
		// TODO Auto-generated method stub
		progressLy.setVisibility(View.GONE);
		if (!application.ismExternalStorageAvailable()) {
			emptyView.showView(R.drawable.emptylist_icon, "SD卡不存在!");
			return;
		}
		switch (res_type) {
		case PangkerConstant.RES_DOCUMENT:
			emptyView.showView(R.drawable.emptylist_icon, "没有发现本地文档!");  
			break;
        case PangkerConstant.RES_APPLICATION:
        	emptyView.showView(R.drawable.emptylist_icon, "没有发现本地应用!"); 
			break;
        case PangkerConstant.RES_MUSIC:
        	emptyView.showView(R.drawable.emptylist_icon, "没有发现本地音乐!"); 
			break;
		}
		btnAll.setEnabled(true);
	}
	
	private void selectAll() {
		// TODO Auto-generated method stub
		isSelectedAll = !isSelectedAll;
		if(isSelectedAll){
			btnAll.setText("反选");
		} else {
			btnAll.setText("全选");
		}
		switch (res_type) {
		case PangkerConstant.RES_DOCUMENT:
			docAdapter.setSelectedAll(isSelectedAll);
			break;
        case PangkerConstant.RES_APPLICATION:
        	appAdapter.setSelectedAll(isSelectedAll);
			break;
		}
	}
	
	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnAll) {
                selectAll();
			}
			if (v == btnSure) {
				btnSure.setEnabled(false);
				goBack();
			}
		}
	};
	
	AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			switch (res_type) {
			case PangkerConstant.RES_DOCUMENT:
				ViewHolderDoc vHollder = (ViewHolderDoc) view.getTag();
				vHollder.selected.toggle();
				docAdapter.selectByIndex(position, vHollder.selected.isChecked());
				break;
	        case PangkerConstant.RES_APPLICATION:
	        	ViewHolderApp vHollder1 = (ViewHolderApp) view.getTag();
				vHollder1.selected.toggle();
				appAdapter.selectByIndex(position, vHollder1.selected.isChecked());
				break;
	        case PangkerConstant.RES_MUSIC:
	        	ViewHolderMusic vHollder2 = (ViewHolderMusic) view.getTag();
				vHollder2.selected.toggle();
				musicAdapter.selectByIndex(position, vHollder2.selected.isChecked());
				break;
			}
		}
	};
	
	private void goBack() {
		// TODO Auto-generated method stub
		if(!application.ismExternalStorageAvailable()){
			boolean flag = false;
			switch (res_type) {
			case PangkerConstant.RES_DOCUMENT:
				if(docAdapter.getSeletedMembers().size() > 0){
					flag = true;
				}
				break;
	        case PangkerConstant.RES_APPLICATION:
	        	if(appAdapter.getSeletedMembers().size() > 0){
	        		flag = true;
	    		}  
				break;
	        case PangkerConstant.RES_MUSIC:
	        	if(musicAdapter.getSeletedMembers().size() > 0){
					flag = true;
				}
	        	break;
			}
			if(flag){
				showToast("SD卡不存在，无法添加!");
			}
			finish();
			return;
		}
		btnAll.setEnabled(false);
		switch (res_type) {
		case PangkerConstant.RES_DOCUMENT:
			if(docAdapter.getSeletedMembers().size() > 0){
				new LoadDocTask().execute();
			} else {
				finish();
			}
			break;
        case PangkerConstant.RES_APPLICATION:
        	if(appAdapter.getSeletedMembers().size() > 0){
    			new LoadAppTask().execute();
    		} else {
    			finish();
    		}
			break;
        case PangkerConstant.RES_MUSIC:
        	if(musicAdapter.getSeletedMembers().size() > 0){
        		new LoadMuicTask().execute();
    		} else {
    			finish();
    		}
        	break;
		}
	}
	
	class LoadAppTask extends AsyncTask<Void, Void, Void> {

		private WaitingDialog popupDialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			popupDialog = new WaitingDialog(SdcardResSearchActivity.this);
			popupDialog.setWaitMessage("正在加载...");
			popupDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			List<LocalAppInfo> lAppInfos = appAdapter.getSeletedMembers();
			for (LocalAppInfo appInfo : lAppInfos) {
				if (!iResDao.isSelected(userId, appInfo.appPath, PangkerConstant.RES_APPLICATION)) {
					DirInfo dirInfo = new DirInfo();
					dirInfo.setPath(appInfo.appPath);
					dirInfo.setDeal(DirInfo.DEAL_SELECT);
					dirInfo.setUid(userId);
					dirInfo.setIsfile(DirInfo.ISFILE);
					dirInfo.setResType(PangkerConstant.RES_APPLICATION);
					dirInfo.setDirName(appInfo.appName);
					dirInfo.setFileCount((int)appInfo.size);

					iResDao.saveSelectResInfo(dirInfo);
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			popupDialog.dismiss();
			setResult(ResLocalPicActivity.RequestCode);
			finish();
		}
	}
	
	class LoadDocTask extends AsyncTask<Void, Void, Void> {

		private WaitingDialog popupDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			popupDialog = new WaitingDialog(SdcardResSearchActivity.this);
			popupDialog.setWaitMessage("正在加载...");
			popupDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			List<File> files = docAdapter.getSeletedMembers();
			for (File file : files) {
				if (!iResDao.isSelected(userId, file.getPath(), PangkerConstant.RES_DOCUMENT)) {
					DirInfo dirInfo = new DirInfo();
					dirInfo.setPath(file.getPath());
					Log.w("isSelected", file.getPath());
					dirInfo.setDeal(DirInfo.DEAL_SELECT);
					dirInfo.setUid(userId);
					dirInfo.setDirName(file.getName());
					dirInfo.setIsfile(DirInfo.ISFILE);
					dirInfo.setResType(PangkerConstant.RES_DOCUMENT);
					dirInfo.setFileCount((int) file.length());
					dirInfo.setRemark(0);

					iResDao.saveSelectResInfo(dirInfo);
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			popupDialog.dismiss();
			setResult(ResLocalPicActivity.RequestCode);
			onBackPressed();
		}
	}
	
	class LoadMuicTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			List<MusicInfo> musicInfos = musicAdapter.getSeletedMembers();
			for (MusicInfo musicInfo : musicInfos) {
				if (!iResDao.isSelected(userId, musicInfo.getMusicPath(), PangkerConstant.RES_MUSIC)) {
					DirInfo dirInfo = new DirInfo();
					dirInfo.setLocalid(musicInfo.getMusicId());
					dirInfo.setDeal(DirInfo.DEAL_SELECT);
					dirInfo.setUid(userId);
					dirInfo.setIsfile(DirInfo.ISFILE);
					dirInfo.setPath(musicInfo.getMusicPath());
					dirInfo.setAppraisement(musicInfo.getSinger());
					dirInfo.setFileCount(musicInfo.getMusicSize());
					dirInfo.setDirName(musicInfo.getMusicName());
					dirInfo.setResType(PangkerConstant.RES_MUSIC);
					
					iResDao.saveSelectResInfo(dirInfo);
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			setResult(ResLocalPicActivity.RequestCode);
			finish();
		}
	}
	
	class LoadAppFromSDCard extends AsyncTask<Object, LocalAppInfo, Object> {

		private void addAppInfo(LocalAppInfo appInfo) {
			appAdapter.addAppInfo(appInfo);
			textViewPath.setText(appInfo.getAppPath());
		}
		
		@Override
		protected Object doInBackground(Object... params) {
			// TODO Auto-generated method stub
			File file = Environment.getExternalStorageDirectory();
			readFile(file);
			return null;
		}

		private void readFile(File file) {
			// TODO Auto-generated method stub
			if (file.isDirectory()) {
				if(file.listFiles() != null){
					for (File sfile : file.listFiles()) {
						if(application.ismExternalStorageAvailable()){
							readFile(sfile);
						}
					}
				}
			}else {
				if(file.getName().toLowerCase().endsWith(".apk")){
					LocalAppInfo appInfo = Util.getApkFileInfo(SdcardResSearchActivity.this, file.toString());
					if(appInfo != null){
						publishProgress(appInfo);
					}
				}
			}
		}
		@Override
		protected void onProgressUpdate(LocalAppInfo... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			addAppInfo(values[0]);
		}
		
		@Override
		protected void onPostExecute(Object result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			showEmptyView();
		}
	}
	
	class LoadDocFromSDCard extends AsyncTask<Object, File, Object> {

		private void addDcoInfo(File dFile) {
			docAdapter.addDoc(dFile);
			textViewPath.setText(dFile.getPath());
		}
		
		@Override
		protected Object doInBackground(Object... params) {
			// TODO Auto-generated method stub
			File file = Environment.getExternalStorageDirectory();
			readFile(file);
			return null;
		}
		
		private boolean searchFiles(String filename){
			for (String format : PangkerConstant.DOC_FORMAT) {
				if(filename.length() >0 && filename.toLowerCase().endsWith(format)){
					return true;
				}
			}
			return false;
		}

		private void readFile(File file) {
			// TODO Auto-generated method stub
			if (file.isDirectory()) {
				if(file.listFiles() != null){
					for (File sfile : file.listFiles()) {
						if(application.ismExternalStorageAvailable()){
							readFile(sfile);
						}
					}
				}
			}else {
				if(searchFiles(file.getName())){
					publishProgress(file);
				}
			}
		}
		
		@Override
		protected void onProgressUpdate(File... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			addDcoInfo(values[0]);
		}
		
		@Override
		protected void onPostExecute(Object result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			showEmptyView();
		}
	}
	
    class LoadMusicFromSDCard extends AsyncTask<Object, MusicInfo, Object> {
		
		String[] projection = new String[] { Media.DISPLAY_NAME, Media.ALBUM, Media.ARTIST, Media.DURATION,
				Media.SIZE, Media._ID, Media.DATA };
		@Override
		protected Object doInBackground(Object... params) {
			// TODO Auto-generated method stub
			if(!application.ismExternalStorageAvailable()){
				return null;
			}
			Cursor cur = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, 
					null, null, null);
			if (cur != null) {
				cur.moveToFirst();
				for (int i = 0; i < cur.getCount(); i++) {
					if (cur.getString(0).endsWith(".mp3")) {
						MusicInfo mInfo = new MusicInfo();
						mInfo.setMusicName(cur.getString(0));
						mInfo.setMusicAlubm(cur.getString(1));
						mInfo.setSinger(cur.getString(2));
						mInfo.setDuration(cur.getInt(3));
						mInfo.setMusicSize(cur.getInt(4));
						mInfo.setMusicId(cur.getInt(5));
						mInfo.setMusicPath(cur.getString(6));
						publishProgress(mInfo);
					}
					cur.moveToNext();
				}
			}
			return null;
		}
		
		private void addMusic(MusicInfo musicInfo) {
			// TODO Auto-generated method stub
			textViewPath.setText(musicInfo.getMusicPath());
			musicAdapter.addMusicInfo(musicInfo);
		}
		
		@Override
		protected void onProgressUpdate(MusicInfo... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			addMusic(values[0]);
		}
		
		@Override
		protected void onPostExecute(Object result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			showEmptyView();
		}
	}

}
