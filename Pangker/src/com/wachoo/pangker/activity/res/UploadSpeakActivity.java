package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;

/**
 * 写说说并分享
 * 
 * @author JarlinHot
 *
 */
public class UploadSpeakActivity extends CommonPopActivity implements IUICallBackInterface{

	private final int Main_SHARE_KEY = 0x11;
	public final static int RESULTCODE = 0x12;
	private PangkerApplication application;
	private Button btnRight;
	private Button btnBack;
	private TextView tvTitle;
	private EditText etContent;
	private UserInfo userInfo;
	private String mUserId;
	private Button btnSubmmit;

	private Location location;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.text_main_share);
		application = (PangkerApplication) getApplication();
		mUserId = application.getMyUserID();
		userInfo = application.getMySelf();
		location = application.getCurrentLocation();
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		initView();
	}
	
	private void initView() {
		// TODO Auto-generated method stub
		btnRight = (Button) findViewById(R.id.iv_more);
		btnRight.setVisibility(View.GONE);

		tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("写说说");
		
		etContent = (EditText)findViewById(R.id.et_detail);
		
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(mOnClickListener);
		
		btnSubmmit = (Button)findViewById(R.id.btn_share);
		btnSubmmit.setOnClickListener(mOnClickListener);
	}

	private OnClickListener mOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.btn_back:
				hideInputMethod();
				break;

			case R.id.btn_share:
				AddResMainShare();
				break;
			}
		}
	};
	
	/**
	 * 隐藏软键盘
	 */
	private void hideInputMethod(){
		((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
				UploadSpeakActivity.this.getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
		UploadSpeakActivity.this.onBackPressed();
	}
	
	/**
	 * 文字信息分享
	 * 说说分享
	 * 
	 * PublishTalk.json
	 */
	private void AddResMainShare() {
		String content = etContent.getText().toString();
		if(Util.isEmpty(content)){
			showToast("请输入分享文字!");
			return;
		}
		
		if (location == null) {
			showToast("定位失败,不能发表说说");
			return;
		}
		
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", mUserId));//分享者Uid
		paras.add(new Parameter("ifshare", String.valueOf(1)));//默认为分享给 用户添加的好友
		paras.add(new Parameter("content", content));// 分享内容
		paras.add(new Parameter("ctrlon", location.getLongitude()+""));
		paras.add(new Parameter("ctrlat", location.getLatitude()+""));
		paras.add(new Parameter("poiaddress", location.getAddress()==null ? "":location.getAddress()));
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getResMainShare(), paras, true, mess, Main_SHARE_KEY);
	}
	
	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		if (caseKey == Main_SHARE_KEY) {
			BaseResult result = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == result.SUCCESS) {
				showToast("分享成功！");
				setResult(RESULTCODE);
				finish();
			} else if (result != null && result.getErrorCode() == result.FAILED) {
				showToast(result.getErrorMessage());
			} else
				showToast(result.getErrorMessage());
		}
	}

}
