package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.ResPicShareAdapter;
import com.wachoo.pangker.adapter.ResShareAdapter;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.ShareInfo;
import com.wachoo.pangker.server.response.ShareQueryResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;

/**
 * @author Administrator 查看其它用户分享的资源，广播自己的资源，转播别人的资源都在内,
 *         如果uItem为空，表示查询自己分享，转播的资源 遗漏：暂时还没有实现分页
 */
public class ResShareActivity extends CommonPopActivity implements IUICallBackInterface {

	private final int PageLenght = 12;

	private final int QUERY_SHARE = 0x100;// 资源分享查询
	private final int CANCEL_SHARE = 0x101;// 取消分享

	private UserItem uItem;// 查询用户的Id
	private PangkerApplication application;
	// Cache功能
	private Map<Integer, List<ShareInfo>> shareCache = new HashMap<Integer, List<ShareInfo>>();
	private Map<Integer, Integer> sumDatas = new HashMap<Integer, Integer>();

	private RadioGroup radioGroup;
	private int res_type = PangkerConstant.RES_PICTURE;
	// 界面试图
	private TextView txtTitle;
	private PullToRefreshListView pullToRefreshListView;
	private ListView resListView;
	private EmptyView emptyView;
	private FooterView footerView;

	private ResShareAdapter mShareAdapter;
	private ResPicShareAdapter gvPicShareAdapter;
	private Button btnBack;

	private ServerSupportManager serverMana;// 后台交互
	private String userId;
	private ShareInfo shareInfo;

	// 资源操作
	private PopMenuDialog menuDialog;
	ActionItem[] dirMenu1 = new ActionItem[] { new ActionItem(0, "取消转播") };
	ActionItem[] dirMenu2 = new ActionItem[] { new ActionItem(0, "取消分享") };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_net_show);
		init();
		initView();
		dealApp(res_type);
	}

	protected void onDestroy() {
		// TODO Auto-generated method stub
		shareCache.clear();
		sumDatas.clear();
		super.onDestroy();
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		serverMana = new ServerSupportManager(this, this);
		uItem = (UserItem) getIntent().getSerializableExtra(PangkerConstant.INTENT_UESRITEM);

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);

		mShareAdapter = new ResShareAdapter(this, new ArrayList<ShareInfo>(), mImageWorker);
		gvPicShareAdapter = new ResPicShareAdapter(this, new ArrayList<ShareInfo>(), mImageWorker);
		gvPicShareAdapter.setOnClickListener(picOnClickListener);
		if (uItem == null) {
			gvPicShareAdapter.setOnLongClickListener(picOnLongClickListener);
		}
	}

	OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			switch (checkedId) {
			case R.id.res_read:
				res_type = PangkerConstant.RES_DOCUMENT;
				break;
			case R.id.res_pic:
				res_type = PangkerConstant.RES_PICTURE;
				break;
			case R.id.res_music:
				res_type = PangkerConstant.RES_MUSIC;
				break;
			case R.id.res_app:
				res_type = PangkerConstant.RES_APPLICATION;
				break;
			}
			dealApp(res_type);
		}
	};

	private void refresh() {
		// TODO Auto-generated method stub
		shareCache.put(res_type, null);
		sumDatas.put(res_type, null);
		queryResShare(true);
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			refresh();
		}
	};

	private void initView() {
		// TODO Auto-generated method stub
		txtTitle = (TextView) findViewById(R.id.mmtitle);
		if (uItem != null) {
			txtTitle.setText("TA的分享资源");
		} else {
			txtTitle.setText("我的分享资源");
		}
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		findViewById(R.id.iv_more).setVisibility(View.GONE);

		radioGroup = (RadioGroup) findViewById(R.id.res_rg);
		radioGroup.setOnCheckedChangeListener(onCheckedChangeListener);

		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
		pullToRefreshListView.setTag("ResShareActivity");
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		resListView = pullToRefreshListView.getRefreshableView();
		footerView = new FooterView(this);
		footerView.addToListView(resListView);
		footerView.setOnLoadMoreListener(onLoadMoreListener);
		resListView.setOnItemClickListener(mListener);

		emptyView = new EmptyView(this);
		resListView.setEmptyView(emptyView.getView());

		if (uItem == null) {
			resListView.setOnItemLongClickListener(fileItemLongClickListener);
		}

		menuDialog = new PopMenuDialog(this, R.style.MyDialog);
		menuDialog.setListener(menuClickListener);
	}

	// 要记录长度
	private void dealApp(int restype) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		txtTitle.setTag(restype);
		int count = 0;
		if (restype == PangkerConstant.RES_PICTURE) {
			resListView.setDividerHeight(0);
			resListView.setAdapter(gvPicShareAdapter);
			if (shareCache.get(restype) != null && sumDatas.get(restype) != null) {
				gvPicShareAdapter.setShareinfos(shareCache.get(restype));
				count = gvPicShareAdapter.getSize();
				footerView.onLoadComplete(count, sumDatas.get(restype));
				return;
			}
		} else {
			resListView.setAdapter(mShareAdapter);
			resListView.setDividerHeight(1);
			if (shareCache.get(restype) != null && sumDatas.get(restype) != null) {
				mShareAdapter.setResList(shareCache.get(restype));
				count = mShareAdapter.getCount();
				footerView.onLoadComplete(count, sumDatas.get(restype));
				return;
			}
		}
		if (restype == PangkerConstant.RES_PICTURE) {
			gvPicShareAdapter.setShareinfos(new ArrayList<ShareInfo>());
		} else {
			mShareAdapter.setResList(new ArrayList<ShareInfo>());
		}
		queryResShare(true);
	}

	/**
	 * 根据类型查询分享接口 请求数目 接口：QueryShare.json
	 */
	private void queryResShare(boolean ifFirst) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuserid", userId));
		String uid = userId;
		if (uItem != null) {
			uid = uItem.getUserId();
		}
		paras.add(new Parameter("userid", uid));// 用户id
		paras.add(new Parameter("optype", "1"));// 资源类型(大类)：0：问答，1：资源，2：群组资
		paras.add(new Parameter("restype", String.valueOf(res_type)));// 资源类型，optype=2是有效

		int startIndex = 0;
		if (shareCache.get(res_type) != null) {
			startIndex = shareCache.get(res_type).size();
		}
		Log.d("itemId", startIndex + "," + PageLenght);
		paras.add(new Parameter("limit", startIndex + "," + PageLenght));
		serverMana.supportRequest(Configuration.getQueryShare(), paras, QUERY_SHARE);
		if (ifFirst) {
			footerView.hideFoot();
			pullToRefreshListView.setRefreshing(true);
			emptyView.reset();
		}
	}

	/**
	 * 取消分享（广播）的接口
	 */
	private void cancelShare() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("optype", "1"));// 资源类型，optype=2是有效
		if (shareInfo.getFlag() == 0) {
			paras.add(new Parameter("resid", String.valueOf(shareInfo.getResId())));//
		}
		if (shareInfo.getFlag() == 1) {
			paras.add(new Parameter("shareid", String.valueOf(shareInfo.getShareId())));//
		}
		serverMana.supportRequest(Configuration.getCancelShare(), paras, true, "正在提交...", CANCEL_SHARE);
	}

	private void dealResInfo(int position) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		for (ShareInfo shareInfo : shareCache.get(res_type)) {
			ResShowEntity entity = new ResShowEntity();
			entity.setCommentTimes(0);
			entity.setResType(res_type);
			entity.setDownloadTimes(0);
			entity.setFavorTimes(0);
			entity.setResID(shareInfo.getResId());
			entity.setScore(0);
			String shareUid = null;
			// 如果资源是转播的，
			shareUid = uItem != null ? uItem.getUserId() : userId;

			entity.setShareUid(shareUid);
			if (shareInfo.getFlag() == 1) {
				entity.setIfshare(true);
				entity.setShareId(String.valueOf(shareInfo.getShareId()));
			}
			entity.setResName(shareInfo.getName());
			reList.add(entity);
		}

		if (res_type == PangkerConstant.RES_PICTURE) {
			intent.setClass(this, PictureInfoActivity.class);
		} else {
			intent.setClass(this, ResInfoActivity.class);
		}
		application.setResLists(reList);
		application.setViewIndex(position);
		if (uItem == null) {
			intent.putExtra("SERACH_TYPE", 3);
			startActivityForResult(intent, 0);
		} else {
			startActivity(intent);
		}
	}

	private FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			queryResShare(false);
		}
	};

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				finish();
			}
		}
	};
	// 菜单监听
	UITableView.ClickListener menuClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int which) {
			if (which == 0) {
				cancelShare();
			}
			menuDialog.dismiss();
		}
	};

	/**
	 * 图片的点击事情，以及长按事件
	 */
	private View.OnClickListener picOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Integer tag = (Integer) v.getTag(R.id.tag_index);
			dealResInfo(tag);
		}
	};

	private View.OnLongClickListener picOnLongClickListener = new View.OnLongClickListener() {

		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			shareInfo = (ShareInfo) v.getTag(R.id.tag_shareinfo);
			dealShareInfo();
			return true;
		}
	};

	/**
	 * ListView 的onItemLick响应事件
	 */
	OnItemClickListener mListener = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (res_type != PangkerConstant.RES_PICTURE) {
				if (parent.getItemAtPosition(position) instanceof ShareInfo) {
					shareInfo = (ShareInfo) parent.getItemAtPosition(position);
					dealResInfo(position - 1);
				}
			}
		}
	};

	private void dealShareInfo() {
		// TODO Auto-generated method stub
		menuDialog.setTitle("资源操作");
		if (shareInfo.getFlag() == 0) {
			menuDialog.setMenus(dirMenu2);
		}
		if (shareInfo.getFlag() == 1) {
			menuDialog.setMenus(dirMenu1);
		}
		menuDialog.show();
	}

	OnItemLongClickListener fileItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			if (res_type != PangkerConstant.RES_PICTURE) {
				shareInfo = (ShareInfo) parent.getItemAtPosition(position);
				dealShareInfo();
			}
			return true;
		}
	};

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			pullToRefreshListView.onRefreshComplete();
			refreshFailOrNoDataResult();
			footerView.onLoadCompleteErr();
			emptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
			return;
		}

		switch (caseKey) {
		case QUERY_SHARE:
			Log.d("QUERY_SHARE", response.toString());
			emptyView.showView(R.drawable.emptylist_icon, R.string.no_data);
			pullToRefreshListView.onRefreshComplete();
			queryShareResResult(response.toString());
			break;
		case CANCEL_SHARE:
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && result.errorCode == BaseResult.SUCCESS) {
				if (res_type == PangkerConstant.RES_PICTURE) {
					gvPicShareAdapter.delShareInfo(shareInfo);
				} else {
					mShareAdapter.delShareInfo(shareInfo);
				}
				showToast(result.errorMessage);
			} else if (result != null && result.errorCode == 0) {
				showToast(result.errorMessage);
			} else
				showToast(R.string.load_data_exception);
			break;
		}
	}

	private boolean checkIfLoad(List<ShareInfo> shareInfos) {

		if (shareInfos.size() == 0) {
			return false;
		}
		ShareInfo info = shareInfos.get(0);
		if (info.getType() != res_type) {
			return true;
		}
		if (shareCache.get(res_type) == null) {
			return false;
		}
		Iterator<ShareInfo> iterator = shareCache.get(res_type).iterator();
		while (iterator.hasNext()) {
			ShareInfo item = iterator.next();
			if (info.getResId().equals(item.getResId())) {
				return true;
			}
		}
		return false;
	}

	private void refreshFailOrNoDataResult() {
		// TODO Auto-generated method stub
		if (res_type == PangkerConstant.RES_PICTURE) {
			if (shareCache.get(res_type) == null) {
				gvPicShareAdapter.setShareinfos(new ArrayList<ShareInfo>());
			} else {
				gvPicShareAdapter.setShareinfos(shareCache.get(res_type));
			}
		} else {
			if (shareCache.get(res_type) == null) {
				mShareAdapter.setResList(new ArrayList<ShareInfo>());
			} else {
				mShareAdapter.setResList(shareCache.get(res_type));
			}
		}
	}

	private void queryShareResResult(String response) {
		ShareQueryResult result = JSONUtil.fromJson(response, ShareQueryResult.class);
		if (result != null && result.errorCode == BaseResult.SUCCESS) {

			if (result.getResinfos() != null && result.getResinfos().size() > 0) {
				// 先判断是否已经加载过了，如果是，过滤
				if (checkIfLoad(result.getResinfos())) {
					return;
				}
				ShareInfo info = result.getResinfos().get(0);

				if (shareCache.get(info.getType()) == null) {
					shareCache.put(info.getType(), result.getResinfos());
				} else {
					shareCache.get(info.getType()).addAll(result.getResinfos());
				}
				if (sumDatas.get(info.getType()) == null) {
					sumDatas.put(info.getType(), result.getTotalcount());
				}
				if (info.getType() == res_type) {
					if (res_type == PangkerConstant.RES_PICTURE) {
						gvPicShareAdapter.setShareinfos(shareCache.get(res_type));
						footerView.onLoadComplete(gvPicShareAdapter.getSize(), sumDatas.get(res_type));
					} else {
						mShareAdapter.setResList(shareCache.get(res_type));
						footerView.onLoadComplete(mShareAdapter.getCount(), sumDatas.get(res_type));
					}
				}
			} else {
				refreshFailOrNoDataResult();
				footerView.hideFoot();
			}
		} else if (result != null && result.errorCode == 0) {
			refreshFailOrNoDataResult();
			showToast(result.errorMessage);
		} else {
			refreshFailOrNoDataResult();
			showToast(R.string.load_data_exception);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == RESULT_OK) {
			refresh();
		}
	}

}
