package com.wachoo.pangker.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.encrypt.EncryptUtils;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.root.SecurityCenterActivity;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.LoginByIMAccountResult;
import com.wachoo.pangker.util.Util;

public class LoginByFingerprint extends CommonPopActivity implements IUICallBackInterface {

	private EditText ed_account;
	private EditText ed_password;
	private Button btn_login;
	private Button btn_getpassword;
	PangkerApplication application;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.finger_login);
		application = (PangkerApplication) getApplication();
		initView();
	}
	
	private void initView() {
		// TODO Auto-generated method stub
		ed_account = (EditText) findViewById(R.id.et_pass0);
		ed_password = (EditText) findViewById(R.id.et_pass1);
		btn_login = (Button) findViewById(R.id.btn_submit);
		btn_getpassword  = (Button) findViewById(R.id.btn_getpassword);
		btn_login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fingerLogin();
			}
		});
		btn_getpassword.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(LoginByFingerprint.this, SecurityCenterActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	private void fingerLogin() {
		// TODO Auto-generated method stub
		String account = ed_account.getEditableText().toString();
		if(account.length() < 6){
			showToast("请输入6位或以上旁客号码!");
			return;
		}
		String pwd1 = ed_password.getEditableText().toString();
		if(Util.isEmpty(pwd1) || pwd1.length() < 6){
			showToast("请输入正确的旁客密码!");
			return;
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "pangker"));
		paras.add(new Parameter("newIMSI", Util.getImsiNum(this)));
		paras.add(new Parameter("account", account));
		try {
			paras.add(new Parameter("accountPassword", EncryptUtils.encrypt(pwd1)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getLoginByIMAccount(), paras, true ,mess);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;
		
		LoginByIMAccountResult result = JSONUtil.fromJson(supportResponse.toString(), LoginByIMAccountResult.class);
		if(result != null && result.errorCode == BaseResult.SUCCESS){
			//换号切换
			if(result.getChangedIMSI() == 1){
				showToast("账号切换成功!");
			} 
			Util.delFileData(this, PangkerConstant.CERTIFICATE_FILE_NAME);
			Intent intent = new Intent(this, StartActivity.class);
			application.setIsLoginByFingerprint(1);//记录是否通过切换账号登陆，用于旁客账号设置提示，add by zhengjy
			startActivity(intent);
			setResult(RESULT_OK);
			finish();
		} else if(result != null){
			showToast(result.getErrorMessage());
		} else {
			showToast(R.string.to_server_fail);
		}
	}
	
}
