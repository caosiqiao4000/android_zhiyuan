package com.wachoo.pangker.activity.msg;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.res.ResRecommendActivity;
import com.wachoo.pangker.adapter.NoticeinfoAdapter;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.db.INoticeDao;
import com.wachoo.pangker.db.impl.NoticeDaoImpl;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.NoticeInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.ConfrimDialog;
import com.wachoo.pangker.ui.dialog.NetAddressBookNoticeDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

public class NoticeInfoActivity extends CommonPopActivity implements ITabSendMsgListener {

	private final int[] MENU_ID = { 0x101 };
	private final String[] MENU_TITLE = { "删除全部信息" };

	private ListView noticeInfoListView;
	private EmptyView mEmptyView;
	private NoticeinfoAdapter noticeinfoAdapter;
	private Button btnBack;
	private Button iv_more;
	private TextView mmtitle;
	private INoticeDao noticeDao;
	private String userId;
	private NoticeInfo noticeInfo;
	private QuickAction quickAction;
	private int type;
	private String fromId;
	private PopMenuDialog menuDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.local_listview);
		init();
		initView();
	}

	private void init() {
		// TODO Auto-generated method stub
		userId = ((PangkerApplication) getApplication()).getMyUserID();
		noticeDao = new NoticeDaoImpl(this);

		type = getIntent().getIntExtra("NOTICE_TYPE", -1);
		fromId = getIntent().getStringExtra("FROM_ID");

		PangkerManager.getTabBroadcastManager().addMsgListener(this);
	}

	Handler noticeHandler = new Handler() {
		public void handleMessage(Message msg) {
			NoticeInfo oneInfo = (NoticeInfo) msg.obj;
			if (oneInfo != null && userId.equals(oneInfo.getOpeUserid()) && type == oneInfo.getMsgType()) {
				noticeinfoAdapter.addNoticeinfo(oneInfo);
			}
		};
	};

	/**
	 * 在关闭该界面的时候，通知MsginfoActivity
	 */
	public void onBackPressed() {
		if (type == MsgInfo.MSG_NOTICE) {
			prashMsgInfoActivityById();
		}
		PangkerManager.getTabBroadcastManager().removeUserListenter(this);
		super.onBackPressed();
	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub

		super.onDestroy();
	}

	private void initView() {
		// TODO Auto-generated method stub
		mmtitle = (TextView) findViewById(R.id.mmtitle);
		mmtitle.setText("系统消息");
		List<NoticeInfo> noticeInfos = new ArrayList<NoticeInfo>();
		if (type >= 0) {
			noticeInfos = noticeDao.getNoticesByType(userId, type);
		}
		findViewById(R.id.progress_ly).setVisibility(View.GONE);

		noticeInfoListView = (ListView) findViewById(R.id.musicListView);
		noticeInfoListView.setDividerHeight(0);
		noticeinfoAdapter = new NoticeinfoAdapter(this, noticeInfos, initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH));
		noticeinfoAdapter.setOnClickListener(msgOnClickListener);
		noticeinfoAdapter.setOnLongClickListener(msgOnLongClickListener);

		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(noticeInfoListView);
		mEmptyView.showView(R.drawable.emptylist_icon, getString(R.string.message_history_nohistory));
		noticeInfoListView.setAdapter(noticeinfoAdapter);

		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		iv_more = (Button) findViewById(R.id.iv_more);
		iv_more.setBackgroundResource(R.drawable.btn_main_more);
		iv_more.setOnClickListener(onClickListener);

		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);
		for (int i = 0; i < MENU_ID.length; i++) {
			if (i == MENU_ID.length - 1) {
				quickAction.addActionItem(new ActionItem(MENU_ID[i], MENU_TITLE[i]), true);
			} else {
				quickAction.addActionItem(new ActionItem(MENU_ID[i], MENU_TITLE[i]));
			}
		}
	}

	public void lookUserInfo(NoticeInfo item) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, item.getOpeUserid());
		intent.putExtra(UserItem.USERNAME, item.getOpeUsername());
		startActivity(intent);
	}

	View.OnClickListener msgOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			noticeInfo = (NoticeInfo) v.getTag();
			lookNoticeInfo(noticeInfo);
		}
	};

	View.OnLongClickListener msgOnLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			noticeInfo = (NoticeInfo) v.getTag();
			showMenuDialog();
			return true;
		}
	};

	private void showMenuDialog() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
		}
		menuDialog.setMenus(new ActionItem[] { new ActionItem(0, getString(R.string.notice_del)) });
		menuDialog.setTitle(R.string.notice_title);
		menuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			if (actionId == 0) {
				deleteNotice();
			}
			menuDialog.dismiss();
		}
	};

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				onBackPressed();
			}
			if (v.getId() == R.id.iv_more) {
				quickAction.show(iv_more);
			}
		}
	};

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 0x101) { // 删除
				deleteNotices();
			}
		}
	};

	/**
	 * 删除单个信息
	 * 
	 * @param info
	 */
	private void deleteNotice() {
		// TODO Auto-generated method stub
		if (noticeDao.delNoticeInfo(noticeInfo)) {
			noticeinfoAdapter.delNoticeinfo(noticeInfo);
			showToast("删除成功!");
		}
	}

	private void prashMsgInfoActivityById() {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(fromId)) {
			Message msg = new Message();
			msg.what = 2;
			msg.arg1 = type;
			msg.obj = fromId;

			PangkerManager.getTabBroadcastManager().sendResultMessage(MsgInfoActivity.class.getName(), msg);
		}
	}

	/**
	 * 删除全部信息
	 * 
	 * @param info
	 */
	private void deleteNotices() {
		// TODO Auto-generated method stub
		if (noticeDao.delNoticeInfos(userId, type)) {
			noticeinfoAdapter.clearNoticeInfos();
			showToast("删除成功!");
			Message msg = new Message();
			msg.what = 1;

			PangkerManager.getTabBroadcastManager().sendResultMessage(MsgInfoActivity.class.getName(), msg);
		}
	}

	/**
	 * 处理单个消息
	 * 
	 * @param info
	 */
	private void lookNoticeInfo(final NoticeInfo info) {
		// TODO Auto-generated method stub
		// // 处理呼叫信息
		// if (info.getType() == NoticeInfo.NOTICE_CALL) {
		// ConfrimDialog callDialog = new ConfrimDialog(this);
		// callDialog.showCallDialog(info.getOpeUserid(),
		// info.getOpeUsername());
		// }
		if (info.getStatus() == 0) {
			info.setStatus(1);
			noticeDao.updateNoticeInfo(info);
			if (info.getType() == NoticeInfo.NOTICE_FRIEND_ADD) {
				// Log.i(TAG, "添加好友申请广播接收----ok ");
				ConfrimDialog dialog = new ConfrimDialog(this);
				dialog.showAddFriendDialog(Util.addUserIDDomain(info.getOpeUserid()));
				prashMsgInfoActivityById();
			}
			// 申请加入单位通讯录
			if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_RESPONSE) {
				NetAddressBookNoticeDialog netAddressDialog = new NetAddressBookNoticeDialog(this, info);
				netAddressDialog.showDialog();
				prashMsgInfoActivityById();
			}
			// 邀请加入单位通讯录
			if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_INVITE_RESPONSE) {
				NetAddressBookNoticeDialog netAddressDialog = new NetAddressBookNoticeDialog(this, info);
				netAddressDialog.showDialog();
				prashMsgInfoActivityById();
			}

			// 申请退出当前用户所创建的单位通讯录
			if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_APPLY_EXIT) {
				NetAddressBookNoticeDialog netAddressDialog = new NetAddressBookNoticeDialog(this, info);
				netAddressDialog.showDialog();
			}
			if (info.getType() == NoticeInfo.NOTICE_RES_RECOMMEND) {
				Intent intent = new Intent(NoticeInfoActivity.this, ResRecommendActivity.class);
				NoticeInfoActivity.this.startActivity(intent);
			}
			// 群组申请的处理
			if (info.getType() == NoticeInfo.NOTICE_TRACE_REQUEST) {
				if (isEffectiveness(info.getTime(), 1)) {
					ConfrimDialog tracesNoticeDialog = new ConfrimDialog(this);
					tracesNoticeDialog.showIntoGroupDialog(info, new ConfrimDialog.OnDialogClickListener() {
						@Override
						public void onClick(Dialog dialog, boolean positive) {
							// TODO Auto-generated method stub
							if (!positive) {
								info.setStatus(0);
								noticeDao.updateNoticeInfo(info);
								noticeinfoAdapter.notifyDataSetChanged();
							}
						}
					});
					prashMsgInfoActivityById();
				} else {
					showToast("抱歉,邀请已经超过时效!");
				}
			}
			noticeinfoAdapter.notifyDataSetChanged();
		}

	}

	/**
	 * 检测信息的时限
	 * 
	 * @param hour
	 *            ，测试多少小时为失效；单位：小时
	 * @return
	 */
	private boolean isEffectiveness(String time, int hour) {
		// TODO Auto-generated method stub
		SimpleDateFormat sd = new SimpleDateFormat(PangkerConstant.DATE_FORMAT);
		try {
			long lazy = new Date().getTime() - sd.parse(time).getTime();
			if (lazy < hour * 3600 * 1000) {
				return true;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return false;
		}
		return false;
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		noticeHandler.sendMessage(msg);
	}

	public void resultNoticeInfo(NoticeInfo info) {
		// TODO Auto-generated method stub
		info.setStatus(2);
		noticeDao.updateNoticeInfo(info);
		noticeinfoAdapter.notifyDataSetChanged();
	}

}
