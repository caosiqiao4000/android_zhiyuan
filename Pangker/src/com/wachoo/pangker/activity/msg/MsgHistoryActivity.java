package com.wachoo.pangker.activity.msg;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.MsgHistoryAdapter;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.impl.UserMsgDaoImpl;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;

/**
 * 类似QQ消息记录 modify by lb 2012
 * 
 * @author zxx
 * 
 */
public class MsgHistoryActivity extends CommonPopActivity {

	private ListView history;
	private EmptyView emptyView;
	private Button last;
	private Button next;
	private TextView count;
	private MsgHistoryAdapter adapter;
	private UserItem topeer;
	int length = 10;
	int total = 0;
	int totalPage = 0;
	int page = 1;
	IUserMsgDao userMsgDao;

	String uid;
	private ImageLocalFetcher localFetcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.msghistory);
		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = false;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>>>加载
		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		localFetcher.setmSaveDiskCache(false);
		localFetcher.setImageCache(mImageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);

		topeer = (UserItem) getIntent().getSerializableExtra("userinfo");
		initView();
		initData();
		initListenner();
	}

	private void initView() {

		RelativeLayout title_layout = (RelativeLayout) findViewById(R.id.title_layout);
		title_layout.setVisibility(View.VISIBLE);
		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText(R.string.history_show);
		Button btnRight = (Button) findViewById(R.id.iv_more);
		btnRight.setVisibility(View.GONE);
		history = (ListView) findViewById(R.id.history_list);
		emptyView = new EmptyView(this);
		emptyView.addToListView(history);

		last = (Button) findViewById(R.id.last);
		next = (Button) findViewById(R.id.next);
		count = (TextView) findViewById(R.id.count);
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MsgHistoryActivity.this.finish();
			}
		});

		Button btnClear = (Button) findViewById(R.id.clear_history);
		btnClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MessageTipDialog diaolog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-generated method stub
						if (isSubmit && userMsgDao.deleteUserMsgByUid(uid, topeer.getUserId())) {
							showToast("清空成功!");
							showNoMsgData();
						}
					}
				}, MsgHistoryActivity.this);
				diaolog.showDialog("确认清空与该用户的所有聊天记录？", false);
			}
		});

	}

	private void showMsgData(int pageindex) {
		int startLimit = (pageindex - 1) * length;
		List<ChatMessage> list_item = userMsgDao.getUserMsgsByLimit(uid, topeer.getUserId(), startLimit, length);
		count.setText(pageindex + "/" + totalPage);
		adapter.setData(list_item);
	}

	private void showNoMsgData() {
		page = 0;
		totalPage = 0;
		count.setText("0/0");
		adapter.setData(new ArrayList<ChatMessage>());
		emptyView.showView(R.drawable.emptylist_icon, "没有历史记录 ");
	}

	private void initData() {
		adapter = new MsgHistoryAdapter(this, new ArrayList<ChatMessage>(), topeer, localFetcher);
		history.setAdapter(adapter);

		uid = ((PangkerApplication) this.getApplicationContext()).getMySelf().getUserId();
		userMsgDao = new UserMsgDaoImpl(MsgHistoryActivity.this);
		total = userMsgDao.getMsgCount(uid, topeer.getUserId());
		totalPage = (total + length - 1) / length;
		page = totalPage;
		if (page > 0) {
			showMsgData(page);
		} else {
			showNoMsgData();
		}
	}

	private void initListenner() {
		last.setOnClickListener(listener);
		next.setOnClickListener(listener);
	}

	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == next) {// xia一页
				if ((page + 1) <= totalPage) {
					page += 1;
					showMsgData(page);
				} else {
					Toast.makeText(MsgHistoryActivity.this, R.string.toonext, Toast.LENGTH_SHORT).show();
				}
			} else if (v == last) {
				if (page - 1 > 0) {// 上一页
					page--;
					showMsgData(page);
				} else {
					Toast.makeText(MsgHistoryActivity.this, R.string.toolast, Toast.LENGTH_SHORT).show();
				}
			}
		}
	};

	protected void onDestroy() {
		mImageCache.removeMemoryCaches();
		super.onDestroy();
	};
}
