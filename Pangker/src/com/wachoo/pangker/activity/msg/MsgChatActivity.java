package com.wachoo.pangker.activity.msg;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.MsgConstant;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.PicturePreviewActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.camera.CameraCompActivity;
import com.wachoo.pangker.activity.contact.ContactActivity;
import com.wachoo.pangker.activity.res.ResLocalActivity;
import com.wachoo.pangker.activity.res.ResLocalAppActivity;
import com.wachoo.pangker.activity.res.ResLocalDocActivity;
import com.wachoo.pangker.activity.res.ResLocalMusicActivity;
import com.wachoo.pangker.adapter.MsgTalkAdapter;
import com.wachoo.pangker.api.ContactUserListenerManager;
import com.wachoo.pangker.api.IContactUserChangedListener;
import com.wachoo.pangker.audio.VoicePlayer;
import com.wachoo.pangker.audio.VoiceRecorder;
import com.wachoo.pangker.chat.IUserMsgListener;
import com.wachoo.pangker.chat.MessageBroadcast;
import com.wachoo.pangker.chat.MessageRecivedPacketExtension;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.chat.UserMsgManager;
import com.wachoo.pangker.chat.VCardManager;
import com.wachoo.pangker.db.IBlacklistDao;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.IMsgInfoDao;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.ITouchUserDao;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.impl.BlacklistDaoImpl;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.db.impl.MsgInfoDaoImpl;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.db.impl.TouchUserDaoImpl;
import com.wachoo.pangker.db.impl.UserMsgDaoImpl;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.listener.TextButtonWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.BlackManagerResult;
import com.wachoo.pangker.server.response.CallMeSetResult;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmoView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.MeetRoomUtil.SpeechAllow;
import com.wachoo.pangker.util.SmileyParser;
import com.wachoo.pangker.util.Util;

/**
 * IM通话Activity 利用openfire服务器实现私信通话功能
 * 
 * @author wangxin 上下线的更改只有好友实现了 2012年8月22日14:02:09
 */
public class MsgChatActivity extends CameraCompActivity implements IContactUserChangedListener, IUICallBackInterface,
		IUserMsgListener {

	private static final String TAG = "MsgChatActivity";// log tag
	private final int[] MENU_INDEXS = { 0x100, 0x101, 0x102 };
	private final String[] MENU_TITLE = { "个人网站", "聊天记录", "黑名单" };

	private static final int CHANGE = 0x100;
	private static final int FILE_SEND = 0x101;
	private static final int LOCATION_SEND = 0x102;
	private static final int MESS_KEY = 0x103;
	private static final int MESSAGE_RECV = 0x106;// >>>>>消息接收成功
	private final int DIALOG_BLACKLIST = 0x104;
	private final int CALL_CHECK_CASEKEY = 0x105;
	private int currentDialogId = -1;

	private int SEND_LIMIT = 100;

	private TextView userName;// 昵称
	private TextView tvWarnings;
	private EditText et_send_message;// 发送私信内容

	private Button btn_send_message;// 发送私信
	private Button btn_touch; // 碰一下
	private Button btn_expression; // 表情
	private Button btn_actions; // 功能按钮
	private Button btnMenu; // 菜单
	private LinearLayout chat_actions_tools;// 功能按钮

	// >>>wangxin 显示聊天记录
	private View more_chat_message;
	private Button btn_more_message;
	// >>>wangxin 显示聊天记录

	private ITouchUserDao touchUserDao;
	private IUserMsgDao userMsgDao;
	private IResDao resDao;// 将受到的文件保存在自己资源目录下

	private MsgTalkAdapter msgTalkAdapter;
	private UserItem topeer;
	private List<ChatMessage> msgs;
	private ChatMessage msgItem;
	private ListView message_single_listview;

	private EmoView emoView;
	private QuickAction popopQuickAction;

	private long touchtime = 0;
	int count = 0;

	private OpenfireManager openfireManager;
	private SmileyParser parser;

	private PangkerApplication application;
	private String myUserID;
	private ContactUserListenerManager userListenerManager;
	private MessageBroadcast msgInfoBroadcast;
	private UserMsgManager userMsgManager;

	private Button btnChatType;// >>>>>>>>当前聊天类型 文字 || 语音

	private Button btnLocationSend;
	private Button btnImgSend;
	private Button btnFileSend;
	private PopMenuDialog msgDialog;
	private final int DEFAULT_SHOW_MESSAGE_COUNT = 15;

	AudioMeter audioMeter;
	VoiceRecorder audioChatFunction;

	private ImageApkIconFetcher apkIconFetcher;
	private ImageLocalFetcher localFetcher;
	private ImageCache imageCache;

	private boolean mMessageFlag = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// 防止输入法在启动Activity时自动弹出
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.chat_conversations);
		init();
		initView();
		initExpresssion();
		initData();
		// 如果是陌生人(非好友非关注人)，则显示警示语
		initWarning();
		msgs = userMsgDao.getUserMsgsByLimit(myUserID, topeer.getUserId(), DEFAULT_SHOW_MESSAGE_COUNT);
		if (msgs == null || msgs.size() <= 0) {
			msgs = new ArrayList<ChatMessage>();
		}
		msgTalkAdapter.setMsgs(msgs);
		showListButtion();
		// 通知会话消息，私信已读
		noticeMsgInfoActivity();

		audioMeter = new AudioMeter();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		mImageCache.removeMemoryCaches();
		super.onDestroy();
		Log.i(TAG, ">>>MsgChatActivity:onDestroy");
	}

	// 通知会话消息，私信已读
	private void noticeMsgInfoActivity() {
		// TODO Auto-generated method stub
		android.os.Message msg = new android.os.Message();
		msg.what = 2;
		msg.obj = topeer.getUserId();
		msg.arg1 = MsgInfo.MSG_PERSONAL_CHAT;
		PangkerManager.getTabBroadcastManager().sendResultMessage(MsgInfoActivity.class.getName(), msg);

		IMsgInfoDao iMsgInfoDao = new MsgInfoDaoImpl(this);
		iMsgInfoDao.updateMsgInfo(topeer.getUserId(), MsgInfo.MSG_PERSONAL_CHAT);
	}

	/**
	 * 是否需要警告提示
	 */
	private void initWarning() {
		// 如果是陌生人(非好友非关注人)，则显示警示语
		if (application.IsFriend(topeer.getUserId())) {
			tvWarnings.setVisibility(View.GONE);
		} else {
			btnImgSend.setEnabled(false);
			btnImgSend.setBackgroundResource(R.drawable.actionbar_but_photo_p);
			btnFileSend.setEnabled(false);
			btnFileSend.setBackgroundResource(R.drawable.actionbar_but_file_p);
			tvWarnings.setVisibility(View.VISIBLE);
		}
	}

	public UserItem getTopeer() {
		return topeer;
	}

	private void init() {
		// TODO Auto-generated method stub

		application = (PangkerApplication) getApplication();
		myUserID = application.getMyUserID();

		Bundle bundle = getIntent().getExtras();
		String username = bundle.getString(UserItem.USERNAME);
		String usersign = bundle.getString(UserItem.USERSIGN);
		String userid = bundle.getString(UserItem.USERID);
		mMessageFlag = bundle.getBoolean(ChatMessage.MSG_FLAG, false);
		// >>>>>>>>>与自己私信会话禁止
		if (userid.equals(myUserID)) {
			this.finish();
			showToast(R.string.chat_notice_cannot_talk_to_yourself);
		}

		VCardManager cardManager = new VCardManager(this);
		topeer = cardManager.getUserItem(userid);
		if (topeer == null) {
			topeer = new UserItem();
			topeer.setUserId(userid);
			topeer.setUserName(username);
			topeer.setSign(usersign);
		}

		touchUserDao = new TouchUserDaoImpl(this);
		userMsgDao = new UserMsgDaoImpl(this);
		resDao = new ResDaoImpl(this);

		openfireManager = application.getOpenfireManager();
		msgInfoBroadcast = application.getMsgInfoBroadcast();

		userListenerManager = PangkerManager.getContactUserListenerManager();
		userListenerManager.addUserListenter(this);
		userMsgManager = PangkerManager.getUserMsgManager();
		userMsgManager.addListenter(this);

		popopQuickAction = new QuickAction(this, QuickAction.ANIM_GROW_FROM_RIGHT);
		popopQuickAction.setOnActionItemClickListener(onActionItemClickListener);
		for (int i = 0; i < MENU_INDEXS.length; i++) {
			if (i == MENU_INDEXS.length - 1) {
				popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[i], MENU_TITLE[i]), true);
			} else {
				popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[i], MENU_TITLE[i]));
			}
		}

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_ICON_SIDELENGTH;
		imageCacheParams.diskCacheEnabled = false;
		mImageCache = new ImageCache(this, imageCacheParams);

		apkIconFetcher = new ImageApkIconFetcher(this);
		apkIconFetcher.setLoadingImage(R.drawable.icon46_apk);
		apkIconFetcher.setmSaveDiskCache(false);
		apkIconFetcher.setImageCache(imageCache);

		// >>>>>>>>>加载
		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		localFetcher.setmSaveDiskCache(false);
		localFetcher.setImageCache(mImageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if (id == FILE_SEND) {
			builder.setTitle("请选择发送文件").setItems(new String[] { "文档", "音乐", "应用" }, dialogClickListener);
			AlertDialog alert = builder.create();
			return alert;
		}
		return super.onCreateDialog(id);
	}

	private boolean isMsgDownLoading(ChatMessage msg) {
		// TODO Auto-generated method stub
		if (!ChatMessage.MSG_TO_ME.equals(msgItem.getDirection())) {
			return false;
		}
		if (isMsgResByType(msg.getMsgType()) && msgItem.getStatus() != DownloadJob.DOWNLOAD_SUCCESS) {
			return true;
		}
		return false;
	}

	private boolean isMsgResByType(int msgType) {
		// TODO Auto-generated method stub
		int[] msgTypes = { PangkerConstant.RES_VOICE, PangkerConstant.RES_PICTURE, PangkerConstant.RES_DOCUMENT,
				PangkerConstant.RES_MUSIC, PangkerConstant.RES_APPLICATION };
		for (int type : msgTypes) {
			if (type == msgType) {
				return true;
			}
		}
		return false;
	}

	ActionItem[] msgMenu1 = new ActionItem[] { new ActionItem(3, "删除") };
	ActionItem[] msgMenu2 = new ActionItem[] { new ActionItem(1, "复制"), new ActionItem(3, "删除") };
	ActionItem[] msgMenu3 = new ActionItem[] { new ActionItem(3, "删除"), new ActionItem(4, "进行语音呼叫") };

	ActionItem[] msgMenu21 = new ActionItem[] { new ActionItem(1, "复制"), new ActionItem(3, "删除"),
			new ActionItem(11, "重新发送") };

	private void showMsgMenu() {
		// TODO Auto-generated method stub
		if (msgDialog == null) {
			msgDialog = new PopMenuDialog(this, R.style.MyDialog);
			msgDialog.setListener(mClickListener);
		}
		// 如果是接受的语音，不完整，不能处理
		if (isMsgDownLoading(msgItem)) {
			showToast("还在下载中，请稍后处理!");
			return;
		}
		switch (msgItem.getMsgType()) {
		case PangkerConstant.RES_SPEAK:
			msgDialog.setTitle("文字信息");
			if (msgItem.getMessageRevc() == 1) {
				// 发送成功的case
				msgDialog.setMenus(msgMenu2);
			} else if (msgItem.getDirection().equals(ChatMessage.MSG_FROM_ME) && msgItem.getMessageRevc() == 2) {
				// 发送失败的case
				msgDialog.setMenus(msgMenu21);
			}
			break;
		case PangkerConstant.RES_LOCATION:
			msgDialog.setTitle("位置信息");
			msgDialog.setMenus(msgMenu1);
			break;
		case PangkerConstant.RES_VOICE:
			msgDialog.setMenus(msgMenu1);
			msgDialog.setTitle("语音信息");
			break;
		case PangkerConstant.RES_PICTURE:
			msgDialog.setTitle("图片信息");
			msgDialog.setMenus(msgMenu1);
			break;
		case PangkerConstant.RES_APPLICATION:
		case PangkerConstant.RES_DOCUMENT:
		case PangkerConstant.RES_MUSIC:
			msgDialog.setTitle("文件信息");
			msgDialog.setMenus(msgMenu1);
			break;
		case PangkerConstant.RES_TOUCH:
			msgDialog.setMenus(msgMenu1);
			msgDialog.setTitle("私信消息");
			break;
		case PangkerConstant.RES_CALL:
			msgDialog.setMenus(msgMenu3);
			msgDialog.setTitle("呼叫消息");
			break;
		}
		msgDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			if (actionId == 1) {// 复制
				ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clip.setText(msgItem.getContent());
			} else if (actionId == 2) {// 转发

			} else if (actionId == 3) {// 删除
				delUsermsg();
			} else if (actionId == 4) {// 语音呼叫
				callCheck(topeer.getUserId());
			} else if (actionId == 11) {// 重新发送
				msgItem.setMessageRevc(0);
				userMsgDao.updateMessageReceived(msgItem);
				msgTalkAdapter.notifyDataSetChanged();
				// 通过of发送
				sendMessage(msgItem.getContent(), msgItem.getMsgType(), msgItem.getFilepath(), msgItem.getId());
			}
			msgDialog.dismiss();
		}
	};

	private void delUsermsg() {
		// TODO Auto-generated method stub
		if (userMsgDao.deleteUserMsgById(msgItem.getId())) {
			msgTalkAdapter.delMsg(msgItem);
			if (isMsgResByType(msgItem.getMsgType())) {
				File vFile = new File(msgItem.getFilepath());
				if (vFile.exists()) {
					vFile.delete();
				}
			}
		}
	}

	/**
	 * 呼叫校验 void
	 */
	public void callCheck(String friendId) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", friendId));
		paras.add(new Parameter("visituid", myUserID));
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getCallCheck(), paras, true, mess, CALL_CHECK_CASEKEY);
	}

	/**
	 * 黑名单管理
	 */
	public void BlacklistManager(int caseKey) {
		if (!application.getOpenfireManager().isLoggedIn()) {
			showToast(R.string.not_logged_cannot_operating);
			return;
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", myUserID));
		paras.add(new Parameter("ruid", topeer.getUserId()));
		paras.add(new Parameter("type", "0")); // type 操作类型：0：添加，1：查询，2：删除。
		String mess = getResourcesMessage(R.string.requesting);
		serverMana.supportRequest(Configuration.getBlackManager(), paras, true, mess, caseKey);
	}

	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			Intent intent = new Intent();
			intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.change);
			switch (which) {
			case 0:
				intent.setClass(MsgChatActivity.this, ResLocalDocActivity.class);
				startActivityForResult(intent, PangkerConstant.RES_DOCUMENT);
				break;
			case 1:
				intent.setClass(MsgChatActivity.this, ResLocalMusicActivity.class);
				startActivityForResult(intent, PangkerConstant.RES_MUSIC);
				break;
			case 2:
				intent.setClass(MsgChatActivity.this, ResLocalAppActivity.class);
				startActivityForResult(intent, PangkerConstant.RES_APPLICATION);
				break;
			}

		}
	};

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == MENU_INDEXS[0]) {
				showInfo(topeer);
			} else if (actionId == MENU_INDEXS[1]) {
				lookMsgHistory();
			} else if (actionId == MENU_INDEXS[2]) {
				if (topeer.getUserId().equals(myUserID)) {
					showToast("不能添加自己到黑名单!");
					return;
				}
				if (application.IsBlacklistExist(topeer.getUserId())) {
					showToast(R.string.tip_blacklist_exist);
				} else
					showBlackDialog(DIALOG_BLACKLIST);
			}
		}
	};

	private MessageTipDialog mTipDialog;

	private void showBlackDialog(int id) {
		// TODO Auto-generated method stub
		currentDialogId = id;
		if (mTipDialog == null) {
			mTipDialog = new MessageTipDialog(msgCallback, this);
		}
		if (id == DIALOG_BLACKLIST) {
			mTipDialog.showDialog(topeer.getUserName(), getString(R.string.if_addto_blacklist), false);
		}

	}

	MessageTipDialog.DialogMsgCallback msgCallback = new MessageTipDialog.DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if (isSubmit) {
				if (currentDialogId == DIALOG_BLACKLIST) {
					BlacklistManager(DIALOG_BLACKLIST);
				}
			}
		}
	};

	/**
	 * 推出界面时，如果有文件在发送，需要进行提醒!,同时如果在播放语音的时候，要停止
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (emoView.getVisibility() == View.VISIBLE) {
			emoView.setVisibility(View.GONE);
		} else {
			VoicePlayer.getVoicePlayer().stop();
			userMsgManager.removeListenter(this);
			super.onBackPressed();
		}
	}

	/**
	 * init view
	 * 
	 */
	private void initView() {
		et_send_message = (EditText) findViewById(R.id.et_send_message);
		btn_send_message = (Button) findViewById(R.id.btn_send_message);
		// et_send_message.setFilters(new InputFilter[] { new
		// InputFilter.LengthFilter(100) });
		btn_send_message.setEnabled(false);
		// ivStatus = (ImageView) findViewById(R.id.iv_status);
		userName = (TextView) findViewById(R.id.nickname);
		tvWarnings = (TextView) findViewById(R.id.tv_warnings);
		message_single_listview = (ListView) findViewById(R.id.message_single_listview);

		// 显示聊天记录 >>>wangxin
		more_chat_message = LayoutInflater.from(this).inflate(R.layout.more_chat_message, null);
		btn_more_message = (Button) (Button) more_chat_message.findViewById(R.id.btn_more_message);
		btn_more_message.setOnClickListener(listener);
		message_single_listview.addHeaderView(more_chat_message);
		// 显示聊天记录 >>>wangxin

		btn_expression = (Button) findViewById(R.id.btn_expression);
		et_send_message.setOnTouchListener(onTouchListener);
		et_send_message.addTextChangedListener(new TextButtonWatcher(btn_send_message));
		btn_touch = (Button) findViewById(R.id.btn_touch);
		chat_actions_tools = (LinearLayout) findViewById(R.id.chat_actions_tools);
		btn_actions = (Button) findViewById(R.id.btn_actions);
		btn_actions.setOnClickListener(listener);
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(listener);

		btnMenu = (Button) findViewById(R.id.btn_menu);
		btnMenu.setOnClickListener(listener);
		userName.setOnClickListener(listener);

		// >>>>>>>聊天类型
		btnChatType = (Button) findViewById(R.id.btn_chat_type);
		btnChatType.setOnClickListener(listener);

		btnLocationSend = (Button) findViewById(R.id.btn_send_location);
		btnLocationSend.setOnClickListener(listener);
		btnImgSend = (Button) findViewById(R.id.btn_send_phono);
		btnImgSend.setOnClickListener(listener);
		btnFileSend = (Button) findViewById(R.id.btn_send_file);
		btnFileSend.setOnClickListener(listener);

		btn_send_message.setOnClickListener(listener);
		btn_expression.setOnClickListener(listener);
		btn_touch.setOnClickListener(listener);
		message_single_listview.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				hideSoftInput(v);
				return false;
			}
		});
	}

	/**
	 * initData modify by lb 图像要重新获取一遍，如果用户修改了，就要修改保存本地的图像，
	 */
	private void initData() {
		String userNmae = topeer.getAvailableName();
		userName.setText(userNmae);
		msgTalkAdapter = new MsgTalkAdapter(MsgChatActivity.this, new ArrayList<ChatMessage>(), apkIconFetcher,
				localFetcher);
		msgTalkAdapter.setOnLongClickListener(msgOnLongClickListener);
		message_single_listview.setAdapter(msgTalkAdapter);

		if (mMessageFlag) {
			String filename = getIntent().getExtras().getString(ChatMessage.MSG_CONTENT);
			String filepath = getIntent().getExtras().getString(ChatMessage.MSG_FILEPATH);
			int res_type = getIntent().getExtras().getInt(ChatMessage.MSG_TYPE, 0);
			sendFile(filename, filepath, res_type);
		}
	}

	private long insertChatMessage(String chatmessage, int type, String location, int Recv) {
		long messageID = 0;
		ChatMessage msg = new ChatMessage();
		msg.setTime(Util.getSysNowTime()); // 手机当前时间
		msg.setUserId(topeer.getUserId()); // 目标用户UserID
		msg.setContent(chatmessage);// 内容
		msg.setUserName(topeer.getAvailableName());
		if (location != null) {
			msg.setFilepath(location);
		}
		msg.setMessageRevc(Recv);
		msg.setMyUserId(myUserID);// 当前用户userid
		msg.setDirection(ChatMessage.MSG_FROM_ME);// 方向
		msg.setMsgType(type);
		messageID = userMsgDao.saveUserMsg(msg);
		msg.setId((int) messageID);
		if (messageID > 0) {
			msgTalkAdapter.addMsg(msg);
			msgInfoBroadcast.sendNoticeMsg(msg);
			return messageID;
		}
		return messageID;
	}

	private void sendMessage(String chatmessage, int type, String filepath, long sendMessageID) {
		final Message message = new Message(Util.addUserIDDomain(topeer.getUserId()));
		message.setType(Type.chat);
		message.setFrom(myUserID);
		message.setProperty("messagetype", type);
		if (filepath != null) {
			message.setProperty("filepath", filepath);
		}
		// >>>>>>>>根据此判断是否为有效Message
		if (sendMessageID > 0) {
			message.addExtension(new MessageRecivedPacketExtension(sendMessageID));
			Log.i("MessageReceived", "send-messageID=" + sendMessageID);
		}
		message.setBody(chatmessage);
		msgHandler.post(new Runnable() {
			@Override
			public void run() {
				openfireManager.sendChatMessage(message);
			}
		});
	}

	/**
	 * 聊天信息发送
	 * 
	 * @param chatmessage
	 * @param type
	 */
	private void sendChatMessage(String chatmessage, int type, String location) {
		// 是否为空验证
		if (Util.isEmpty(chatmessage)) {
			showToast(R.string.msg_null);
			return;
		}
		// IM是否登录验证
		long messageID = 0l;
		if (openfireManager.isLoggedIn())
			messageID = insertChatMessage(chatmessage, type, location, 0);
		else
			messageID = insertChatMessage(chatmessage, type, location, 2);

		// >>>>>>>>判断是否本地插入
		if (messageID > 0) {
			sendMessage(chatmessage, type, location, messageID);
			et_send_message.setText("");
		}
		msgHandler.post(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				showListButtion();
			}
		});
	}

	private void showListButtion() {
		message_single_listview.setSelection(msgTalkAdapter.getCount());
	}

	View.OnLongClickListener msgOnLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			msgItem = (ChatMessage) v.getTag();
			showMsgMenu();
			return true;
		}
	};

	View.OnTouchListener onTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if (v == et_send_message) {
				emoView.setVisibility(View.GONE);
				chat_actions_tools.setVisibility(View.GONE);
			}
			return false;
		}
	};

	private void lookMsgHistory() {
		Intent intent = new Intent(MsgChatActivity.this, MsgHistoryActivity.class);
		intent.putExtra("userinfo", topeer);
		startActivity(intent);
	}

	/**
	 * 监听时间实现
	 */
	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btn_send_message) {// 发送文字私聊信息按钮
				// 文字聊天信息
				sendChatMessage(et_send_message.getText().toString().trim(), PangkerConstant.RES_SPEAK, null);
			}
			// >>>>>>>表情按钮
			else if (v == btn_expression) {
				if (emoView.getVisibility() == View.VISIBLE) {
					emoView.setVisibility(View.GONE);
				} else {
					emoView.showEmoView();
				}
				hideSoftInput(v);
			}
			// >>>>>>碰一下按钮
			else if (v == btn_touch) {// 碰一下按钮
				// >>>>>>>验证是否登录Openfire服务器
				if (touchUserDao.canTouchUser()) {
					long nowtime = new Date().getTime();
					if (nowtime - touchtime > 1000 * 10) {
						// >>>>>>>本地保存碰一下记录
						long messageID = 0l;
						// >>>>>>>已经登陆，直接发送消息
						if (!openfireManager.isLoggedIn())
							messageID = insertChatMessage(MsgConstant.MSG_TOUCH, PangkerConstant.RES_TOUCH, null, 0);
						// >>>>>>>没有登录，消息照发，提示用户需要重新发送
						else
							messageID = insertChatMessage(MsgConstant.MSG_TOUCH, PangkerConstant.RES_TOUCH, null, 2);

						if (messageID > 0) {
							openfireManager.sendTouchMessage(myUserID, topeer.getUserId(), messageID);
							touchUserDao.itouchUser();
							touchtime = nowtime;
						}
						msgHandler.postDelayed(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								showListButtion();
							}
						}, 100);

					} else {
						showToast("您的touch太过频繁,请间隔10s~");
					}
					chat_actions_tools.setVisibility(View.GONE);
				} else {
					showToast("您每天只能碰十名用户！今天已经不能碰了！");
				}

			}
			// >>>>>>功能按钮显示/隐藏
			else if (v == btn_actions) { // 更多功能操作按钮
				btnChatType.setBackgroundResource(R.drawable.btn_voice_selector);
				audioMeter.showRecord(false);
				hideSoftInput(v);
				// >>>>>>>>设置显示工具栏
				if (chat_actions_tools.getVisibility() == View.VISIBLE) {
					chat_actions_tools.setVisibility(View.GONE);
				} else {
					chat_actions_tools.setVisibility(View.VISIBLE);
					// >>>>>>>>表情隐藏
					emoView.setVisibility(View.GONE);
				}
				return;
			}
			// >>>>>>返回按钮
			else if (v.getId() == R.id.btn_back) { // 返回按钮 ，时间隐藏键盘功能
				userMsgManager.removeListenter(MsgChatActivity.this);
				hideSoftInput(v);
				MsgChatActivity.this.finish();
			}
			// >>>>>>>>>>发送自己当前的位置信息按钮
			else if (v == btnLocationSend) { // 发送位置信息按钮
				// >>>>>>>验证是否登录Openfire服务器
				if (!openfireManager.isLoggedIn()) {
					showToast(R.string.not_logged_cannot_sendmessage);
					return;
				}
				Intent intent = new Intent(MsgChatActivity.this, MsgLoactionActivity.class);
				startActivityForResult(intent, LOCATION_SEND);

			}
			// >>>>>>>>>>发送图片
			else if (v == btnImgSend) { // 发送图片按钮
				// >>>>>>>验证是否登录Openfire服务器
				if (!openfireManager.isLoggedIn()) {
					showToast(R.string.not_logged_cannot_sendmessage);
					return;
				}
				showImageDialog("发送图片", actionItems3, false);
			}
			// >>>>>>>>发送文件按钮
			else if (v == btnFileSend) { // 发送文件按钮

				// >>>>>>>验证是否登录Openfire服务器
				if (!openfireManager.isLoggedIn()) {
					showToast(R.string.not_logged_cannot_sendmessage);
					return;
				}
				if (msgTalkAdapter.getSendingLenght() <= SEND_LIMIT) {
					showDialog(FILE_SEND);
				} else {
					showToast("发送超出限度，请稍后再发送!");
				}
			}
			// >>>>>>>>>右上角功能按钮
			else if (v == btnMenu) {
				popopQuickAction.show(btnMenu);
			}
			// >>>>>>>>>点击用户名操作
			else if (v == userName) {
				showInfo(topeer);
			}
			// >>>>>>>>>查看聊天历史记录按钮
			else if (v == btn_more_message) {
				lookMsgHistory();
			}

			// >>>>>>>切换聊天模式
			else if (v == btnChatType) {
				if (audioMeter.isSendVoice) {
					// >>>>>语音状态切换成位置
					btnChatType.setBackgroundResource(R.drawable.btn_voice_selector);
					audioMeter.showRecord(false);
				} else {
					btnChatType.setBackgroundResource(R.drawable.btn_keyboard_selector);
					audioMeter.showRecord(true);
					hideSoftInput(v);
					// >>>>>>>工具栏隐藏
					chat_actions_tools.setVisibility(View.GONE);
					// >>>>>>>>表情隐藏
					emoView.setVisibility(View.GONE);
				}
			}
			chat_actions_tools.setVisibility(View.GONE);
		}
	};

	/**
	 * 私聊表情选择 void
	 */
	private void initExpresssion() {
		// TODO Auto-generated method stub
		emoView = (EmoView) findViewById(R.id.emoview);
		emoView.setOnEmoClickListener(new EmoView.OnEmoClickListener() {
			@Override
			public void onEmoClickListener(View arg1, int position) {
				// TODO Auto-generated method stub
				String expressStr = getResources().getStringArray(R.array.express_item_texts)[position];
				String oldStr = et_send_message.getText().toString();
				et_send_message.setText(parser.addSmileySpans(oldStr + expressStr));
				et_send_message.setSelection(et_send_message.getText().toString().length());
			}
		});

		parser = new SmileyParser(this);// 表情转化辅助工具
	}

	// 刷新listview
	public Handler msgHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			if (msg.what == CHANGE) {
				msgTalkAdapter.notifyDataSetChanged();
			}
			if (msg.what == PangkerConstant.STATE_CHANGE_REFRESHUI) {
				// initStatus();
			}
			if (msg.what == MESS_KEY) {
				msgTalkAdapter.addMsg((ChatMessage) msg.obj);
				showListButtion();
			}
			// >>>>>>>>通知已送达
			if (msg.what == MESSAGE_RECV) {
				msgTalkAdapter.messageReceived(Integer.parseInt(msg.obj.toString()));
				msgTalkAdapter.notifyDataSetChanged();
			}
		};
	};

	/**
	 * @author wangxin 2012-3-19 下午01:46:51
	 */
	public void showInfo(UserItem user) {
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, user.getUserId());
		intent.putExtra(UserItem.USERNAME, user.getUserName());
		intent.putExtra(UserItem.USERSIGN, user.getSign());
		startActivity(intent);
	}

	private void backupFileToLocal(ChatMessage msg) {
		// TODO Auto-generated method stub
		DirInfo dirInfo = new DirInfo();
		dirInfo.setPath(msg.getFilepath());
		dirInfo.setDeal(DirInfo.DEAL_SELECT);
		Log.d("isSelected", "==>" + msg.getFilepath());
		dirInfo.setUid(myUserID);
		dirInfo.setIsfile(DirInfo.ISFILE);
		dirInfo.setDirName(msg.getFileName());

		dirInfo.setFileCount((int) new File(msg.getFilepath()).length());
		dirInfo.setResType(msg.getMsgType());
		dirInfo.setRemark(0);
		if (!resDao.isSelected(myUserID, dirInfo.getPath(), dirInfo.getResType())) {
			resDao.saveSelectResInfo(dirInfo);
		}
	}

	// 结果接收,通知对方
	public void receiveFileResult(ChatMessage msg) {
		// TODO Auto-generated method stub
		userMsgDao.updateUserMsgStatus(msg);
		int status = msg.getStatus();
		String msgContent = "";
		if (msg.getMsgType() == PangkerConstant.RES_PICTURE) {
			return;
		}
		if (status == DownloadJob.DOWNLOAD_SUCCESS) {
			msgContent = "成功接受\"" + msg.getFileName() + "\"";
			backupFileToLocal(msg);
		} else if (status == DownloadJob.DOWNLOAD_FAILE) {
			msgContent = "接受文件失败:\"" + msg.getFileName() + "\"";
		} else if (status == DownloadJob.DOWNLOAD_REFUSE) {
			msgContent = "拒绝接受\"" + msg.getFileName() + "\"";
		}
		sendMessage(msgContent, PangkerConstant.RES_SPEAK, null, 0);
	}

	// 发送结果
	public void sendFileResult(ChatMessage msg, String fileUrl) {
		Log.d(TAG, "fileName->" + fileUrl);
		if (!Util.isEmpty(fileUrl)) {
			msg.setStatus(UploadJob.UPLOAD_SUCCESS);
			String msgContent;
			if (msg.getMsgType() == PangkerConstant.RES_PICTURE) {
				String filepath = msg.getFilepath();
				msgContent = filepath.substring(filepath.lastIndexOf("/") + 1);
			} else {
				msgContent = msg.getContent();
			}
			sendMessage(msgContent, msg.getMsgType(), fileUrl, msg.getId());
		} else {
			msg.setStatus(UploadJob.UPLOAD_FAILED);
		}
		userMsgDao.updateUserMsgStatus(msg);
		msgTalkAdapter.notifyDataSetChanged();
	}

	// 发送文件
	private void sendFile(String filename, String filepath, int msgType) {
		File file = new File(filepath);
		if (!file.exists()) {
			showToast("文件不存在，请检查后在发送!");
			return;
		}

		ChatMessage msgFile = new ChatMessage();
		filename = filename == null ? file.getName() : filename;
		if (msgType == PangkerConstant.RES_VOICE) {
			msgFile.setContent(filename);
		} else {
			msgFile.setContent(filename);
		}
		msgFile.setDirection(ChatMessage.MSG_FROM_ME);
		msgFile.setMsgType(msgType);
		long len = file.length();
		msgFile.setFileSize(len);
		msgFile.setProgress(0);
		msgFile.setMyUserId(myUserID);
		msgFile.setUserId(topeer.getUserId());
		msgFile.setTime(Util.getSysNowTime()); // 手机当前时间
		msgFile.setUserName(topeer.getUserName());
		msgFile.setFilepath(filepath);
		msgFile.setStatus(UploadJob.UPLOAD_INITOK);
		msgFile.setId((int) userMsgDao.saveUserMsg(msgFile));
		msgTalkAdapter.addMsg(msgFile);
		msgInfoBroadcast.sendNoticeMsg(msgFile);
	}

	class AudioMeter implements View.OnTouchListener, VoiceRecorder.OnAudioStateListener {

		RelativeLayout rcdLayout;
		RelativeLayout chatLayout;
		Button btnCalled;
		TextView btnRecord;

		LinearLayout loadLayout;
		ProgressBar loadBar;

		RelativeLayout recordLayout;
		ImageView volumeView;

		LinearLayout hintLayout;
		TextView hintView;

		private boolean isSendVoice = false;// 是发送语音，还是发送文字

		public AudioMeter() {
			// TODO Auto-generated constructor stub
			rcdLayout = (RelativeLayout) findViewById(R.id.rcd_layout);
			chatLayout = (RelativeLayout) findViewById(R.id.chat_layout);
			btnCalled = (Button) findViewById(R.id.btn_call);
			btnRecord = (TextView) findViewById(R.id.btn_rcd);
			btnRecord.setOnTouchListener(this);
			btnCalled.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					callCheck(topeer.getUserId());
				}
			});
			loadLayout = (LinearLayout) findViewById(R.id.voice_rcd_hint_loading);
			loadBar = (ProgressBar) findViewById(R.id.progressBar1);

			recordLayout = (RelativeLayout) findViewById(R.id.voice_rcd_hint_rcding);
			volumeView = (ImageView) findViewById(R.id.iv_volume);

			hintLayout = (LinearLayout) findViewById(R.id.voice_rcd_hint_tooshort);
			hintView = (TextView) findViewById(R.id.txt_rcd_hint);

			audioChatFunction = new VoiceRecorder(this);
		}

		// >>>>>>设置当前对话模式
		public void showRecord(boolean flag) {
			// TODO Auto-generated method stub
			isSendVoice = flag;
			if (flag) {
				// >>>>>>设置电话呼叫显示、其他隐藏
				btnRecord.setVisibility(View.VISIBLE);
				chatLayout.setVisibility(View.GONE);
				btnCalled.setVisibility(View.VISIBLE);
			} else {
				// >>>>>>设置电话呼叫隐藏
				btnRecord.setVisibility(View.GONE);
				btnCalled.setVisibility(View.GONE);
				chatLayout.setVisibility(View.VISIBLE);
			}
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if (v == btnRecord) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					// btnRecord.setBackgroundResource(R.drawable.commonbar_but_default);

					audioChatFunction.stopRecord();
				}
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					// btnRecord.setBackgroundResource(R.drawable.buttom_green_default);
					if (application.getCurrunGroup() != null
							&& application.getCurrunGroup().getSpeechStatus() == SpeechAllow.SPEECHING) {
						showToast("您当前正在群组中发言，暂时不能使用语音消息。");
					} else {
						audioChatFunction.startRecord();
					}
				}
			}
			return true;
		}

		public void showRecordResult(int status) {
			// TODO Auto-generated method stub
			if (status == 0) {
				rcdLayout.setVisibility(View.VISIBLE);
			} else if (status == 2) {
				rcdLayout.setVisibility(View.GONE);
				msgHandler.post(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						showListButtion();
					}
				});
			} else {
				hintLayout.setVisibility(View.VISIBLE);
				recordLayout.setVisibility(View.GONE);
				msgHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						rcdLayout.setVisibility(View.GONE);
						hintLayout.setVisibility(View.GONE);
						recordLayout.setVisibility(View.VISIBLE);
					}
				}, 400);
			}
		}

		@Override
		public void onAudioStateListener(int status, String file, int record) {
			// TODO Auto-generated method stub
			if (status == -1) {
				showToast("Sd卡不存在，无法使用!");
			}
			// 显示开始录音
			else if (status == 0) {
				showRecordResult(0);
			}
			// 异常时间太短
			else if (status == 1) {
				showRecordResult(1);
			}
			// 录音成功，发送文件
			else if (status == 2) {
				sendFile(String.valueOf(record) + "\"", file, PangkerConstant.RES_VOICE);
				showRecordResult(2);
			}
			// 异常
			else if (status == 3) {

			}
		}

		@Override
		public void onAudioRecordListener(int volume) {
			// TODO Auto-generated method stub
			Log.d("volume", "volume:" + volume);
			switch (volume) {
			case 0:
			case 1:
				volumeView.setImageResource(R.drawable.amp1);
				break;
			case 2:
			case 3:
				volumeView.setImageResource(R.drawable.amp2);
				break;
			case 4:
			case 5:
				volumeView.setImageResource(R.drawable.amp3);
				break;
			case 6:
			case 7:
				volumeView.setImageResource(R.drawable.amp4);
				break;
			case 8:
			case 9:
				volumeView.setImageResource(R.drawable.amp5);
				break;
			case 10:
			case 11:
				volumeView.setImageResource(R.drawable.amp6);
				break;
			default:
				volumeView.setImageResource(R.drawable.amp7);
				break;
			}
		}
	}

	/*
	 * (non-Javadoc)选择用户位置信息返回方法
	 * 
	 * @see com.wachoo.pangker.activity.CamerasActivity#onActivityResult(int,
	 * int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == LOCATION_SEND && resultCode == RESULT_OK) {
			Location location = (Location) intent.getSerializableExtra("Location");
			sendChatMessage("我在" + location.getAddress(), PangkerConstant.RES_LOCATION, location.toResString());
		} else if (resultCode == RESULT_OK
				&& (requestCode == PangkerConstant.RES_DOCUMENT || requestCode == PangkerConstant.RES_PICTURE
						|| requestCode == PangkerConstant.RES_APPLICATION || requestCode == PangkerConstant.RES_MUSIC)) {
			String filepath = intent.getStringExtra(DocInfo.FILE_PATH);
			String filename = intent.getStringExtra(DocInfo.FILE_NAME);
			sendFile(filename, filepath, requestCode);
			msgHandler.post(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					showListButtion();
				}
			});
		}
	}

	@Override
	public boolean isHaveUser(UserItem item) {
		// TODO Auto-generated method stub
		if (item.getUserId().equals(topeer.getUserId())) {
			return true;
		}
		return false;
	}

	@Override
	public void refreshUser(int what) {
		// TODO Auto-generated method stub
		msgHandler.sendEmptyMessage(what);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;
		if (caseKey == CALL_CHECK_CASEKEY) {
			CallMeSetResult result = JSONUtil.fromJson(supportResponse.toString(), CallMeSetResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS && result.getMobile() != null) {
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel://" + result.getMobile()));
				startActivity(intent);
				ChatMessage msg = application.getMsgInfoBroadcast().sendCallMsg(topeer.getUserId(),
						topeer.getAvailableName());
				if (msg != null) {
					msgTalkAdapter.addMsg(msg);
					msgHandler.post(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							showListButtion();
						}
					});
					;
				}
			} else if (result != null && (result.getErrorCode() == BaseResult.FAILED || result.getErrorCode() == 2)) {
				showToast(result.getErrorMessage());
			} else
				showToast(R.string.return_value_999);
		}
		if (caseKey == DIALOG_BLACKLIST) {
			BlackManagerResult blackResult = JSONUtil.fromJson(supportResponse.toString(), BlackManagerResult.class);
			if (null == blackResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (blackResult.getErrorCode() == BaseResult.SUCCESS) {

				showToast(blackResult.getErrorMessage());
				IBlacklistDao iBlacklistDao = new BlacklistDaoImpl(MsgChatActivity.this);
				iBlacklistDao.insertUser(topeer.getUserId());
				OpenfireManager openfireManager = application.getOpenfireManager();

				if (application.IsFriend(topeer.getUserId())) {
					boolean delefriendSuccess = openfireManager.removefriend(openfireManager.getConnection()
							.getRoster(), topeer.getUserId());
					if (delefriendSuccess) {
						IFriendsDao iFriendsDao = new FriendsDaoImpl(MsgChatActivity.this);
						iFriendsDao.delFriend(topeer.getUserId(), PangkerConstant.DEFAULT_GROUPID);
						openfireManager.sendContactsChangedPacket(myUserID, topeer.getUserId(), BusinessType.delfriend);
						userListenerManager.refreshUI(ContactActivity.class.getName(),
								PangkerConstant.STATE_CHANGE_REFRESHUI);
					}
				}
			} else if (blackResult.getErrorCode() == BaseResult.FAILED) {
				if (DIALOG_BLACKLIST == caseKey) {
					showToast(blackResult.getErrorMessage());
				}
			}
		}
	}

	@Override
	public boolean onReceiveUserMsg(ChatMessage msg) {

		// TODO Auto-generated method stub
		Log.i("UserMsgManager", msg.toString());
		if (msg.getUserId().equals(topeer.getUserId())) {
			android.os.Message mess = new android.os.Message();
			mess.what = MESS_KEY;
			mess.obj = msg;
			msgHandler.sendMessage(mess);
			Log.i("UserMsgManager", "YES");
			return true;
		}
		return false;

	}

	@Override
	public void doImageBitmap(String imageLocalPath) {
		// TODO Auto-generated method stub
		if (Util.isExitFileSD(imageLocalPath)) {
			Intent intent = new Intent(this, PicturePreviewActivity.class);
			intent.putExtra("photo_path", imageLocalPath);
			startActivityForResult(intent, PangkerConstant.RES_PICTURE);
		}
	}

	@Override
	public void onMessageReceived(int messageID) {
		// TODO Auto-generated method stub
		this.msgTalkAdapter.messageReceived(messageID);
		android.os.Message message = msgHandler.obtainMessage(MESSAGE_RECV);
		message.obj = messageID;
		msgHandler.sendMessage(message);
	}

}