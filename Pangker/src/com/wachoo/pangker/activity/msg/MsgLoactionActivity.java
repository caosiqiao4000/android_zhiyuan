package com.wachoo.pangker.activity.msg;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapActivity;
import com.amap.mapapi.map.MapController;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.Projection;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.util.MapUtil;

/**
 * 这里并没有定位
 * 
 * @author zxx
 * 
 */
public class MsgLoactionActivity extends MapActivity {

	private PangkerApplication application;
	private Button btnBack;
	private Button btnSend;
	private MapView mapview;
	private MapController mapcontroller;
	private Location poiLocation;
	private int xLoc;
	private int yLoc;
	private Projection projection;
	private LinearLayout popLayout;
	private ImageView imgLocation;
	private TextView txtLocation;
	private ProgressBar pBar;
	private PoiaddressLoader poiLoader;

	@Override
	public void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		this.setMapMode(MAP_MODE_VECTOR);// 设置地图为矢量模式
		super.onCreate(arg0);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.msg_loaction);
		application = (PangkerApplication) getApplication();
		poiLoader = new PoiaddressLoader(application);
		initView();
	}

	private void setSendEnable(boolean enable) {
		// TODO Auto-generated method stub
		btnSend.setEnabled(enable);
		if (enable) {
			btnSend.setTextColor(Color.WHITE);
		} else {
			btnSend.setTextColor(Color.GRAY);
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		btnSend = (Button) findViewById(R.id.iv_more);
		btnSend.setText(R.string.btn_msgsend);
		btnSend.setOnClickListener(onClickListener);
		setSendEnable(false);
		TextView mmtitle = (TextView) findViewById(R.id.mmtitle);
		mmtitle.setText(R.string.mylocation);

		if (getIntent().getIntExtra("msg_type", 0) == 1) {
			btnSend.setText(R.string.confrim);
		}

		mapview = (MapView) findViewById(R.id.atmapsView);
		mapview.setBuiltInZoomControls(true);
		mapcontroller = mapview.getController();
		mapcontroller.setZoom(15);
		mapview.setOnTouchListener(onTouchListener);
		projection = mapview.getProjection();

		popLayout = (LinearLayout) findViewById(R.id.pop_layout);
		txtLocation = (TextView) findViewById(R.id.txtLocation);
		pBar = (ProgressBar) findViewById(R.id.progress_bar);

		imgLocation = (ImageView) findViewById(R.id.img_location);
		ViewTreeObserver vto = imgLocation.getViewTreeObserver();
		vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			public boolean onPreDraw() {
				int[] location = new int[2];
				imgLocation.getLocationInWindow(location);
				xLoc = location[0];
				yLoc = location[1];
				return true;
			}
		});
		if (application.getLocateType() == PangkerConstant.LOCATE_CLIENT) {
			poiLocation = application.getCurrentLocation();
		} else {
			poiLocation = application.getDirftLocation();
		}
		setcenter(poiLocation);
		GeoPoint nowPoint = MapUtil.toPoint(poiLocation);
		if (nowPoint != null) {
			mapcontroller.animateTo(nowPoint);
		} else {
			Toast.makeText(this, "还无法获取您的位置信息!", Toast.LENGTH_SHORT).show();
		}
	}

	private void setcenter(Location location) {
		// TODO Auto-generated method stub
		TranslateAnimation animation = new TranslateAnimation(0, 0, 0, -15);
		animation.setDuration(300);
		animation.setRepeatCount(Animation.INFINITE);
		animation.setRepeatMode(Animation.REVERSE);
		animation.setRepeatCount(1);
		imgLocation.setAnimation(animation);
		animation.start();
		if (location != null) {
			popLayout.setVisibility(View.VISIBLE);
			pBar.setVisibility(View.VISIBLE);
			txtLocation.setVisibility(View.GONE);
			poiLoader.onPoiLoader(location, this, new PoiaddressLoader.GeoCallback() {
				@Override
				public void onGeoLoader(String address, int code) {
					// TODO Auto-generated method stub
					pBar.setVisibility(View.GONE);
					txtLocation.setVisibility(View.VISIBLE);
					txtLocation.setText(address);
					setSendEnable(true);
				}
			});
		} else {
			Toast.makeText(this, "还无法获取您的位置信息!", Toast.LENGTH_SHORT).show();
		}
	}

	private void mapMover() {
		// TODO Auto-generated method stub
		GeoPoint point = projection.fromPixels(xLoc, yLoc);
		poiLocation = MapUtil.toLocation(point);
		setcenter(poiLocation);
	}

	private void startSearch() {
		poiLoader.stopLoader();
		setSendEnable(false);
		popLayout.setVisibility(View.GONE);
	}

	View.OnTouchListener onTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				startSearch();
			}
			if (event.getAction() == MotionEvent.ACTION_UP) {
				mapMover();
			}
			if (event.getAction() == MotionEvent.ACTION_MOVE) {
				startSearch();
			}
			return false;
		}
	};

	private void sendLocation() {
		Intent intent = new Intent();
		poiLocation.setAddress(txtLocation.getText().toString());
		intent.putExtra("Location", poiLocation);
		setResult(RESULT_OK, intent);
		finish();
	}

	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				finish();
			}
			if (v == btnSend) {
				sendLocation();
			}
		}
	};

}
