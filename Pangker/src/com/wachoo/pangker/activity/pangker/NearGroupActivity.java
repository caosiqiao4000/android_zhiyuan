package com.wachoo.pangker.activity.pangker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.group.ChatRoomInfoActivity;
import com.wachoo.pangker.activity.group.GroupSearchActivity;
import com.wachoo.pangker.activity.res.ResSearchActivity;
import com.wachoo.pangker.adapter.NearGroupAdapter;
import com.wachoo.pangker.db.IUserGroupDao;
import com.wachoo.pangker.db.impl.UserGroupDaoIpml;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.SearchLbsGroupResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;

/**
 * 旁客Tab---人群：周边的群聊 zhengjy 2012-4-18
 */
public class NearGroupActivity extends ResSearchActivity implements IUICallBackInterface {

	private String[] MENU_INDEXS;
	private String[] MENU_TITLE;
	private QuickAction popopQuickAction;
	private PopMenuDialog menuDialog;

	private List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
	private PullToRefreshListView pullToRefreshListView;
	private ListView mListView;
	private NearGroupAdapter groupAdapter;
	private Button btnMenu;
	private Button btnSearch;
	private LinearLayout layoutTitle;
	private LinearLayout layoutTitle_nodata;
	private ImageView iconDrow;
	private ImageView iconDrow_nodata;
	private RelativeLayout mTitleTopLayout;
	private RelativeLayout mTitleTopLayout_nodata;
	private TextView tvTitle;
	private TextView tvTitle_nodata;

	private Button btnMenu_nodata;
	private Button btnSearch_nodata;

	private final int SEARCH_GROUP = 0x10;
	private static final int RETRANSMIT = 0x13;
	private static final int COLLECTGROUP = 0x14;

	private String userId;
	private String groupcategory = "0";
	private ImageView iv_empty_icon;
	private TextView tv_empty_prompt;
	
	private GroupLbsInfo groupInfo;
	private UserGroup userGroup;
	private IUserGroupDao userGroupDao;
	private SharedPreferencesUtil sp_Util;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_friends_listview);
		sp_Util = new SharedPreferencesUtil(this);
		
		userGroupDao = new UserGroupDaoIpml(this);

		initView();
		
		initListener();

		MENU_INDEXS = getResources().getStringArray(R.array.group_category_id);
		MENU_TITLE = getResources().getStringArray(R.array.group_category);
		
		popopQuickAction = new QuickAction(this, QuickAction.ANIM_GROW_FROM_CENTER);
		popopQuickAction.setOnActionItemClickListener(onActionItemClickListener);
		List<ActionItem> popActionItems = new ArrayList<ActionItem>();
		for (int i = 0; i < MENU_INDEXS.length; i++) {
			popActionItems.add(new ActionItem(Integer.parseInt(MENU_INDEXS[i]), MENU_TITLE[i]));
		}
		popopQuickAction.setActionItems(popActionItems);
		
		popopQuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				iconDrow.setImageResource(R.drawable.icon32_arrow);
				iconDrow_nodata.setImageResource(R.drawable.icon32_arrow);
			}
		});
		//通过sp_Util记住上次使用的选项
		groupcategory = sp_Util.getString(PangkerConstant.SP_NEARGROUP_KEY, "0");
		for (int i = 0; i < MENU_INDEXS.length; i++) {
			if(groupcategory.equals(MENU_INDEXS[i])){
				tvTitle.setText(MENU_TITLE[i]);
				tvTitle_nodata.setText(MENU_TITLE[i]);
			}
		}
		searchGroupFromLbs("");
	}

	private void initView() {
		userId = ((PangkerApplication) getApplication()).getMyUserID();

		groupAdapter = new NearGroupAdapter(this, groupList);
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_contacts);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		pullToRefreshListView.setTag("pk_near_groups");
		mListView = pullToRefreshListView.getRefreshableView();
		
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View neargroup_bar = inflater.inflate(R.layout.nearuser_bar, null);

		tvTitle = (TextView) neargroup_bar.findViewById(R.id.tv_title);
		tvTitle.setText(R.string.btn_all);
		iconDrow = (ImageView) neargroup_bar.findViewById(R.id.icon_drow);
		iconDrow.setImageResource(R.drawable.icon32_arrow);
		layoutTitle = (LinearLayout) neargroup_bar.findViewById(R.id.layout_title);
		btnMenu = (Button) neargroup_bar.findViewById(R.id.btn_right);
		btnMenu.setVisibility(View.GONE);
		mTitleTopLayout = (RelativeLayout) neargroup_bar.findViewById(R.id.title_top_ly);
		btnSearch = (Button) neargroup_bar.findViewById(R.id.btn_search);
		btnMenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				popopQuickAction.show(btnMenu);
			}
		});
		
		mListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				groupInfo = (GroupLbsInfo)arg0.getItemAtPosition(arg2);
				showPopMenu();
				return true;
			}
		});
		
		mListView.addHeaderView(neargroup_bar);
		
		View neargroup_emptyview = LayoutInflater.from(this).inflate(R.layout.nearuser_empty_view, null, false);

        iv_empty_icon = (ImageView) neargroup_emptyview.findViewById(R.id.iv_empty_icon);
        iv_empty_icon.setImageBitmap(null);
        tv_empty_prompt = (TextView) neargroup_emptyview.findViewById(R.id.textViewEmpty);
        tv_empty_prompt.setText("");

        tvTitle_nodata = (TextView) neargroup_emptyview.findViewById(R.id.tv_title);
        tvTitle_nodata.setText(R.string.btn_all);
        btnMenu_nodata = (Button) neargroup_emptyview.findViewById(R.id.btn_right);
        btnMenu_nodata.setVisibility(View.GONE);
        mTitleTopLayout_nodata = (RelativeLayout) neargroup_emptyview.findViewById(R.id.title_top_ly);
        layoutTitle_nodata = (LinearLayout) neargroup_emptyview.findViewById(R.id.layout_title);
        iconDrow_nodata = (ImageView) neargroup_emptyview.findViewById(R.id.icon_drow);
        iconDrow_nodata.setImageResource(R.drawable.icon32_arrow);
        btnSearch_nodata = (Button) neargroup_emptyview.findViewById(R.id.btn_search);

		btnSearch_nodata.setOnClickListener(btnSearchListener);

		mListView.setEmptyView(neargroup_emptyview);

		initPressBar(mListView);
		loadmoreView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (lyLoadPressBar.getVisibility() == View.GONE && lyLoadText.getVisibility() == View.VISIBLE) {
					if (isLoadAll()) {
						loadText.setText(R.string.nomore_data);
					} else {
						searchGroupFromLbs("");
					}
				}
			}
		});
		mListView.setAdapter(groupAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Intent intent = new Intent();
				intent.putExtra("GroupInfo", (Serializable)groupList);
				intent.putExtra("group_index", (arg2 - 2));
				intent.setClass(NearGroupActivity.this, ChatRoomInfoActivity.class);
				startActivity(intent);
			}
		});
	}
	
	private void initListener(){
		layoutTitle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				popopQuickAction.show(mTitleTopLayout);
				iconDrow.setImageResource(R.drawable.icon32_arrow_p);

			}
		});
		layoutTitle_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				popopQuickAction.show(mTitleTopLayout_nodata);
				iconDrow_nodata.setImageResource(R.drawable.icon32_arrow_p);
			}
		});
		btnSearch.setOnClickListener(btnSearchListener);
		
	}


	OnClickListener btnSearchListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent =new Intent(NearGroupActivity.this,GroupSearchActivity.class);
			startActivity(intent);
		}
	};
	
	private void showPopMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
			ActionItem[] msgMenu = new ActionItem[] {new ActionItem(RETRANSMIT, getString(R.string.res_question_retransmit)), 
					new ActionItem(COLLECTGROUP, getString(R.string.res_app_collect_btn))};
			menuDialog.setMenus(msgMenu);
		}
		menuDialog.setTitle(groupInfo.getgName());
		menuDialog.show();
	}
	
	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			case RETRANSMIT:
				broadcastGroup();
				break;
			case COLLECTGROUP:
				toFavorite();
				break;
			}
			menuDialog.dismiss();
		}
	};

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			popopQuickAction.dismiss();
			searchtype = PangkerConstant.GROUP_TYPE_ALL;
			groupcategory = actionId + "";
			tvTitle.setText(MENU_TITLE[pos]);
			tvTitle_nodata.setText(MENU_TITLE[pos]);
			sp_Util.saveString(PangkerConstant.SP_NEARGROUP_KEY, groupcategory);
			search();
		}
	};
	
	/**
	 * @param s
	 *            void
	 */
	protected void searchGroupList(CharSequence s) {
		List<GroupLbsInfo> searchList = new ArrayList<GroupLbsInfo>();
		if (s == null || s.length() < 1) {
			searchList = groupList;
		}
		if (s != null && s.length() > 0) {
			for (GroupLbsInfo group : groupList) {
				if (group.getgName().contains(s)
						|| (group.getTags() != null && group.getTags().contains(s))) {
					searchList.add(group);
				}
			}
		}
		groupAdapter.setGroupList(searchList);
	}

	/**
	 * 搜索数据
	 */
	@Override
	protected void search() {
		reset();
		loadText.setText(R.string.loadmore);
		searchGroupFromLbs("");
	}

	/**
	 * 搜索周边群聊
	 * 
	 * @param gName群组名
	 * @param loType
	 * 
	 *            return void
	 */
	public void searchGroupFromLbs(String label) {
		if (groupAdapter.getCount() >= incremental) {
			setPresdBarVisiable(true);
		}
		ServerSupportManager serverMana = new ServerSupportManager(getParent(), this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(getLatitude());
		paras.add(getLongitude());
		paras.add(getDistanceRang());
		paras.add(getSearchLbsLimit());
		paras.add(new Parameter("searchkey", label)); // 群组标签，$ 符号分隔
		paras.add(new Parameter("searchtype", searchtype!=null?searchtype:"2")); // 群组类别，0固定群组；1
		// 移动群组。2.全部，3关键字
		paras.add(new Parameter("groupcategory", groupcategory));
		paras.add(new Parameter("orderkey", "4"));
		serverMana.supportRequest(Configuration.getGroupFromLbs(), paras, SEARCH_GROUP);
		if (startLimit == 0) {
			pullToRefreshListView.setRefreshing(true);
		}
	}

//	/**
//	 * 进群记录
//	 */
//	public void groupEnter(String groupId) {
//		ServerSupportManager serverMana = new ServerSupportManager(getParent(), this);
//		List<Parameter> paras = new ArrayList<Parameter>();
//		paras.add(new Parameter("opuid", userId));
//		paras.add(new Parameter("groupid", groupId));
//		// paras.add(new Parameter("optype", "1"));// 0:邀请某人加入群组，1：申请进入群组（必填）
//		paras.add(new Parameter("password", ""));// 群组密码，需要密码校验时使用
//		serverMana.supportRequest(Configuration.getGroupEnter(), paras, GROUP_ENTER);
//	}
	
	private void broadcastGroup() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("resid", String.valueOf(groupInfo.getSid())));
		paras.add(new Parameter("shareid", ""));// 转播时，resid,shareid两个参数中至少有一个
		paras.add(new Parameter("optype", String.valueOf(PangkerConstant.OPTYPE_GROUP)));// 操作的资源类型（0：问答，1：资源，2：群组） type=1时必填.
		paras.add(new Parameter("type", "2"));// 1: 广播 2:转播
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getResShare(), paras, true, mess, RETRANSMIT);	
	}
	
	/**
	 * 收藏群
	 * 
	 * @author wubo
	 * @createtime 2012-3-15
	 */
	public void toFavorite() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", String.valueOf(groupInfo.getSid())));
		paras.add(new Parameter("shareid", ""));
		paras.add(new Parameter("optype", "0"));// 0收藏 1取消收藏
		paras.add(new Parameter("type", "2"));// 0:问答,1:资源,2:群组
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getCollectResManager(), paras,true,mess ,COLLECTGROUP);
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			search();
		}
	};

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		setPresdBarVisiable(false);
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(response)) {
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			if (caseKey == SEARCH_GROUP) {
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				tv_empty_prompt.setText(R.string.no_near_usergroup_prompt);
			}
			return;
		}
		if (caseKey == SEARCH_GROUP) {

			SearchLbsGroupResult groupResult = JSONUtil.fromJson(response.toString(),
					SearchLbsGroupResult.class);
			if (groupResult != null && groupResult.isSuccess() && groupResult.getData() != null) {
				List<GroupLbsInfo> gList = groupResult.getData().getData();

				if (gList != null && gList.size() > 0) {
					if (gList.size() < incremental) {
						lyLoadPressBar.setVisibility(View.GONE);
						lyLoadText.setVisibility(View.GONE);
						setLoadAll(true);
					} else
						setPresdBarVisiable(false);

					// 如果不是从0开始加载
					if (getStartLimit() != 0) {
						List<GroupLbsInfo> addList = new ArrayList<GroupLbsInfo>(gList);
						groupList.addAll(addList);
					}
					// 首次加载
					else {
						if (!isLoadAll()) {
							setPresdBarVisiable(false);
						}
						groupList = gList;
					}
					resetStartLimit();
					groupAdapter.setGroupList(groupList);

				} else if (gList == null || gList.size() == 0) {
					if (getStartLimit() == 0) {
						groupAdapter.setGroupList(new ArrayList<GroupLbsInfo>());
						iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
						tv_empty_prompt.setText(R.string.no_near_usergroup_prompt);
					} else
						showToast(R.string.nomore_data);
					setLoadAll(true);
					lyLoadPressBar.setVisibility(View.GONE);
					lyLoadText.setVisibility(View.GONE);
				} else {
					loadText.setText(R.string.load_data_fail);
					lyLoadPressBar.setVisibility(View.GONE);
					lyLoadText.setVisibility(View.VISIBLE);
				}
			} else {
				if (startLimit == 0) {
					iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
					tv_empty_prompt.setText(R.string.to_server_fail);
				} else {
					showToast(R.string.to_server_fail);
				}

				lyLoadPressBar.setVisibility(View.GONE);
				lyLoadText.setVisibility(View.GONE);
			}

		}  
		
		else if (caseKey == COLLECTGROUP) {
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			BaseResult collectResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if(collectResult == null){
				showToast(R.string.to_server_fail);
				return;
			}
			else if (collectResult.errorCode == BaseResult.SUCCESS ) {
				showToast(collectResult.getErrorMessage());
				userGroup = new UserGroup();
				userGroup.setGroupId(String.valueOf(groupInfo.getSid()));
				userGroup.setGroupSid(String.valueOf(groupInfo.getgSid()));
				userGroup.setGroupName(groupInfo.getgName());
				userGroup.setGroupLabel(groupInfo.getTags());
				userGroup.setLotype(String.valueOf(groupInfo.getLoType()));
				userGroup.setUid(String.valueOf(groupInfo.getUid()));
				userGroup.setGroupType(String.valueOf(groupInfo.getgType()));
				userGroup.setCreateTime(groupInfo.getIssuetime());
				userGroup.setGroupFlag(1);
				userGroupDao.addGroup(userGroup);
			}
			else if(collectResult.errorCode == BaseResult.FAILED){
				showToast(collectResult.getErrorMessage());
			}
			else showToast("收藏失败!");
		}
		
		else if (caseKey == RETRANSMIT) {
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if(result == null){
				showToast(R.string.to_server_fail);
				return;
			}
			else if (result.getErrorCode() == BaseResult.SUCCESS || result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
				
			} else {
				showToast(R.string.return_value_999);
			}
		}
		

	}

}
