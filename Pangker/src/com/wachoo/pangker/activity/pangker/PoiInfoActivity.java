package com.wachoo.pangker.activity.pangker;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ZoomControls;

import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapController;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.Overlay;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonMapActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.map.app.RouteOverlay;
import com.wachoo.pangker.map.poi.Poi_concise;
import com.wachoo.pangker.map.poi.RouteLoader;
import com.wachoo.pangker.util.MapUtil;
import com.wachoo.pangker.util.Util;

public class PoiInfoActivity extends CommonMapActivity {

	public static final int COMMENTRESULT = 0;
	private Poi_concise Poi;
	private MapView mapview;
	private MapController mapcontroller;
	private Bitmap drawIcon;
	private PangkerApplication application;
	private GeoPoint destinationPoint;
	private ZoomControls mZoomControls;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.setMapMode(MAP_MODE_VECTOR);// 设置地图为矢量模式
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.maplocation);
		init();
		initView();
		initData();
	}

	private void init() {
		application = (PangkerApplication) getApplication();
		// .getMySelf().getUserId();
		Poi = (Poi_concise) getIntent().getSerializableExtra(getResourcesMessage(R.string.intent_poi));
	}

	/**
	 * 初始化试图
	 */
	private void initView() {
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("地理位置信息");
		Button btnGo = (Button) findViewById(R.id.iv_more);
		btnGo.setBackgroundResource(R.drawable.btn_default_selector);
		btnGo.setText("GO");
		btnGo.setOnClickListener(onClickListener);
		btnGo.setVisibility(View.GONE);

		findViewById(R.id.ly_userinfo).setVisibility(View.GONE);
		drawIcon = BitmapFactory.decodeResource(getResources(), R.drawable.map_placemark);
		mapview = (MapView) findViewById(R.id.atmapsView);
		mapview.setBuiltInZoomControls(false);
		mapcontroller = mapview.getController();
		mapcontroller.setZoom(15);
		mZoomControls = (ZoomControls) findViewById(R.id.zc_textsize_zoom);
		mZoomControls.setOnZoomInClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mapcontroller.zoomIn();
			}
		});

		mZoomControls.setOnZoomOutClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mapcontroller.zoomOut();
			}
		});

	}

	private void initData() {
		// TODO Auto-generated method stub
		destinationPoint = toGeoPoint(Poi);
		mapcontroller.animateTo(destinationPoint);
		PoiOverItem poiOverlay = new PoiOverItem(this, Poi);
		mapview.getOverlays().add(poiOverlay);
		poiOverlay.addToMap(mapview);
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.btn_back) {
				finish();
			}
			if (v.getId() == R.id.iv_more) {
				for (int i = 1; i < mapview.getOverlays().size(); i++) {
					mapview.getOverlays().remove(i);
				}
				fromAndTo(destinationPoint);
			}
		}
	};

	private Location getCurrentLocation() {
		// TODO Auto-generated method stub
		if (application.getLocateType() == PangkerConstant.LOCATE_CLIENT) {
			return application.getCurrentLocation();
		}
		return application.getDirftLocation();
	}

	private GeoPoint toGeoPoint(Poi_concise poi) {
		double lat = Double.parseDouble(poi.getLat());
		double lng = Double.parseDouble(poi.getLon());
		return new GeoPoint((int) (lat * 1e6), (int) (lng * 1e6));
	}

	private void fromAndTo(GeoPoint endPoint) {
		// TODO Auto-generated method stub
		GeoPoint myPoint = MapUtil.toPoint(getCurrentLocation());
		RouteLoader routeLoader = new RouteLoader(myPoint, endPoint);
		routeLoader.setOnRouteLoaderback(onRouteLoaderback).execute();

		// FromAndTo fromAndTo = new FromAndTo(myPoint, endPoint);
		// try {
		// List<Route> route = Route.calculateRoute(this, fromAndTo,
		// Route.BusDefault);
		// // 构造RouteOverlay 参数为MapActivity cnt, Route rt。这里只取了查到路径的第一条。
		// if (route.size() > 0) {
		// RouteOverlay routeOverlay = new RouteOverlay(this, route.get(0));
		// routeOverlay.registerRouteMessage(this);// 注册人消息处理函数
		// routeOverlay.addToMap(mapview);// 加入地图
		// }
		// } catch (AMapException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

	public RouteLoader.OnRouteLoaderback onRouteLoaderback = new RouteLoader.OnRouteLoaderback() {
		@Override
		public void onRouteLoaderback(List<GeoPoint> routePoints) {
			// TODO Auto-generated method stub
			if (routePoints == null) {
				return;
			}
			RouteOverlay routeOverlay = new RouteOverlay(routePoints);
			List<Overlay> overlays = mapview.getOverlays();
			overlays.add(routeOverlay);
			if (routePoints.size() >= 2) {
				mapcontroller.animateTo(routePoints.get(0));
			}
		}
	};

	class PoiOverItem extends Overlay {

		private View view;
		private TextView textViewLocation;
		private TextView txtName;
		private GeoPoint nowPoint;
		private Paint paint;

		public PoiOverItem(Context context, Poi_concise Poi) {
			// TODO Auto-generated constructor stub
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.pop_location_poi, null);
			textViewLocation = (TextView) view.findViewById(R.id.textViewLocation);
			txtName = (TextView) view.findViewById(R.id.trace_username);
			if(Util.isEmpty(Poi.getName())){
				txtName.setVisibility(View.GONE);
			} else {
				txtName.setText(Util.getStringByLength(Poi.getName(), 40));
			}
			textViewLocation.setText(Poi.getAddress());

			nowPoint = toGeoPoint(Poi);

			paint = new Paint();
			paint.setColor(Color.BLUE);
			paint.setAlpha(150);
			paint.setAntiAlias(true);
			paint.setStyle(Paint.Style.FILL_AND_STROKE);
			paint.setStrokeWidth(4);
		}

		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			Point p = new Point();
			mapView.getProjection().toPixels(nowPoint, p);
			// canvas.drawPoint(p.x, p.y, paint);
			canvas.drawBitmap(drawIcon, p.x - drawIcon.getWidth() / 2, p.y - drawIcon.getHeight() / 2, null);
		}

		public void addToMap(MapView mapview) {
			// TODO Auto-generated method stub
			MapView.LayoutParams lp;
			lp = new MapView.LayoutParams(MapView.LayoutParams.WRAP_CONTENT,
					MapView.LayoutParams.WRAP_CONTENT, nowPoint, 0, -drawIcon.getHeight() / 2 + 10,
					MapView.LayoutParams.BOTTOM_CENTER);
			mapview.addView(view, lp);
		}

	}

}
