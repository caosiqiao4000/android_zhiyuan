package com.wachoo.pangker.activity.pangker;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonActivity;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.group.MyGroupListsActivity;
import com.wachoo.pangker.adapter.RelativesAdapter;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.api.TabBroadcastManager;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.IIntimateDao;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.db.impl.IntimateDaoIpml;
import com.wachoo.pangker.entity.Limit;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.LocationQueryCheckResult;
import com.wachoo.pangker.server.response.RelativesResInfo;
import com.wachoo.pangker.server.response.SearchRelativeResult;
import com.wachoo.pangker.server.response.SearchRelativesResResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;

/**
 * @author Lb 友踪列表，仅仅是用户的列表
 */
public class RelativesDynamicActivity extends CommonActivity implements IUICallBackInterface, ITabSendMsgListener {

	private final String[] MENU_TITLE = { "亲友管理", "邀请群聊" };
	private final int DEL_TO_TRACE = 0x103;
	private final int PHASE_TRACE = 0x104;
	private final int PHASE_RELATION = 0x105;

	private UserItem currUserItem;
	private IFriendsDao iFriendsDao;
	private PangkerApplication application;
	private ServerSupportManager serverMana;
	private String userId;
	private TabBroadcastManager tabBroadcastManager;

	private PullToRefreshListView pullToRefreshListView;
	private ListView traceListView;
	private FooterView footerView;
	private RelativesAdapter relativesAdapter;
	private Button btnMenu;
	private Button btnRight;
	private EditText etSearch;
	private ImageView btnClear;

	private Button btnMenu_nodata;
	private Button btnRight_nodata;
	private EditText etSearch_nodata;
	private ImageView btnClear_nodata;

	private ImageView iv_empty_icon;
	private TextView tv_empty_prompt;

	private QuickAction quickAction;
	private PopMenuDialog menuDialog;

	// >>>>>>>>本地保存亲友数据
	private IIntimateDao intimateDao;

	private int mSearchStart;
	private final int mSearchincremental = 10;

	// >>>>>>获取图片
	protected ImageFetcher mImageWorker;
	protected ImageCache mImageCache;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_friends_listview);
		init();
		initView();
		// >>>>>>获取本地亲友数据
		loadLocalRelatives();
		phaseRelatives(true);
	}

	private void loadLocalRelatives() {
		// TODO Auto-generated method stub
		relativesAdapter.setRelativesResInfos(intimateDao.getIntimateData(getLimit()));
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		iFriendsDao = new FriendsDaoImpl(this);
		tabBroadcastManager = PangkerManager.getTabBroadcastManager();
		tabBroadcastManager.addMsgListener(this);
		serverMana = new ServerSupportManager(getParent(), this);
		intimateDao = new IntimateDaoIpml(this);

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MAX;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);

		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MAX);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
	}

	private void initView() {
		// TODO Auto-generated method stub
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_contacts);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		pullToRefreshListView.setTag("pk_trace_users");
		traceListView = pullToRefreshListView.getRefreshableView();

		footerView = new FooterView(this);
		footerView.addToListView(traceListView);
		footerView.setOnLoadMoreListener(onLoadMoreListener);
		View listBar = LayoutInflater.from(this).inflate(R.layout.contact_search, null);
		btnMenu = (Button) listBar.findViewById(R.id.btn_left);
		btnMenu.setOnClickListener(onClickListener);
		etSearch = (EditText) listBar.findViewById(R.id.et_search);

		etSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (s != null && !s.toString().trim().equals(etSearch_nodata.getText().toString().trim())) {
					etSearch_nodata.setText(s.toString().trim());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				searchUserList(s.toString().trim());
			}
		});
		btnClear = (ImageView) listBar.findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(onClickListener);

		btnRight = (Button) listBar.findViewById(R.id.btn_right);
		btnRight.setOnClickListener(onClickListener);
		btnRight.setBackgroundResource(R.drawable.btn_add_user_selector);
		btnRight.setVisibility(View.VISIBLE);

		// 空白界面
		View emptyView = LayoutInflater.from(this).inflate(R.layout.managerbar_empty_view, null, false);
		iv_empty_icon = (ImageView) emptyView.findViewById(R.id.iv_empty_icon);
		iv_empty_icon.setImageDrawable(null);
		tv_empty_prompt = (TextView) emptyView.findViewById(R.id.textViewEmpty);
		tv_empty_prompt.setText("");

		btnMenu_nodata = (Button) emptyView.findViewById(R.id.btn_left);
		btnMenu_nodata.setOnClickListener(onClickListener);
		etSearch_nodata = (EditText) emptyView.findViewById(R.id.et_search);

		etSearch_nodata.addTextChangedListener(textWatcher);
		btnClear_nodata = (ImageView) emptyView.findViewById(R.id.btn_clear);
		btnClear_nodata.setOnClickListener(onClickListener);

		btnRight_nodata = (Button) emptyView.findViewById(R.id.btn_right);
		btnRight_nodata.setOnClickListener(onClickListener);
		btnRight_nodata.setBackgroundResource(R.drawable.btn_add_user_selector);
		btnRight_nodata.setVisibility(View.VISIBLE);

		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);
		List<ActionItem> menuActionItems = new ArrayList<ActionItem>();
		for (int i = 0; i < MENU_TITLE.length; i++) {
			menuActionItems.add(new ActionItem(i, MENU_TITLE[i]));
		}
		quickAction.setActionItems(menuActionItems);

		traceListView.addHeaderView(listBar);
		// 显示emptyview
		if (application.getTraceList() == null || application.getTraceList().size() == 0) {
			iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
			tv_empty_prompt.setText("正在进行亲友加载...");
		}

		relativesAdapter = new RelativesAdapter(new ArrayList<RelativesResInfo>(), this, mImageWorker);
		relativesAdapter.setLocationClickListener(locateOnClickListener);
		traceListView.setAdapter(relativesAdapter);
		traceListView.setEmptyView(emptyView);

	}

	private FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			phaseRelatives(false);
		}
	};

	TextWatcher textWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub
			if (s != null && !s.toString().trim().equals(etSearch.getText().toString().trim())) {
				etSearch.setText(s.toString().trim());
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}
	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		tabBroadcastManager.removeUserListenter(this);
		super.onDestroy();
	}

	/**
	 * @param s
	 *            void
	 */
	protected void searchUserList(CharSequence s) {
		List<UserItem> searchList = new ArrayList<UserItem>();
		if (s == null || s.length() < 1) {
			searchList = application.getTraceList();
		}
		if (s != null && s.length() > 0) {
			for (UserItem contactUser : application.getTraceList()) {
				if ((contactUser.getUserName() != null && contactUser.getUserName().contains(s))
						|| (contactUser.getrName() != null && contactUser.getrName().contains(s))) {
					searchList.add(contactUser);
				}
			}
		}
		// traceLitsAdapetr.setUserItems(searchList);
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 0) {
				Intent intent = new Intent(RelativesDynamicActivity.this, RelativesManagerActivity.class);
				startActivity(intent);

			}
			if (actionId == 1) {
				Intent intent = new Intent(RelativesDynamicActivity.this, MyGroupListsActivity.class);
				intent.putExtra("fromType", "inviteToTraceRoom");
				startActivity(intent);
			}
		}
	};

	OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			currUserItem = (UserItem) parent.getAdapter().getItem(position);
			shwoTraceMap(currUserItem);
		}
	};

	private View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnMenu) {
				quickAction.show(btnMenu);
			}
			if (v == btnMenu_nodata) {
				quickAction.show(btnMenu_nodata);
			}
			if (v == btnClear || v == btnClear_nodata) {
				etSearch.setText("");
				etSearch_nodata.setText("");
			}
			if (v == btnRight || v == btnRight_nodata) {
				addTraceMembers();
			}
		}
	};

	/**
	 * 列表项长按监听器
	 */
	private OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			// index = position;
			currUserItem = (UserItem) parent.getAdapter().getItem(position);
			showMenu();
			return true;
		}
	};

	private void showMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
			ActionItem[] msgMenu = new ActionItem[] { new ActionItem(1, getString(R.string.del_to_trace)) };
			menuDialog.setMenus(msgMenu);
		}
		menuDialog.setTitle(currUserItem.getUserName());
		menuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			case 1:
				delTraceUserItem();
				break;
			}
			menuDialog.dismiss();
		}
	};

	/**
	 * 向后台发送请求
	 */
	private void delTraceUserItem() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "pangker"));
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("ruid", currUserItem.getUserId()));
		paras.add(new Parameter("type", "1"));
		serverMana.supportRequest(Configuration.getRelativeManager(), paras, true, "正在提交，请稍后...", DEL_TO_TRACE);
	}

	private void addTraceMembers() {
		// TODO Auto-generated method stub
		if (application.getTraceList().size() >= PangkerConstant.TRACEIN_LIMIT) {
			showToast("亲友人数已经超出限制!");
			return;
		}
		Intent intent = new Intent(this, ContactsSelectActivity.class);
		intent.putExtra(ContactsSelectActivity.INTENT_SELECT, ContactsSelectActivity.TYPE_TRACE_RELATIVES);
		startActivity(intent);
	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				// traceLitsAdapetr.setUserItems(application.getTraceList());
				// phaseTraces();
			}
		}
	};

	// 显示地图
	private void shwoTraceMap(UserItem userItem) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, MapLocationActivity.class);
		intent.putExtra("userinfo", userItem);
		startActivity(intent);
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			// phaseTraces();
			resetSearchLimit();
			phaseRelatives(true);
		}
	};

	private void phaseRelatives(boolean isRefreshing) {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(getSearchLimit());
		serverMana.supportRequest(Configuration.getSearchRelativesRes(), paras, PHASE_RELATION);
		if (isRefreshing)
			pullToRefreshListView.setRefreshing(true);
	}

	// >>>>>> getSearchLimit每次加载nUserincremental个
	private Parameter getSearchLimit() {
		return new Parameter("limit", mSearchStart + "," + mSearchincremental);
	}

	private Limit getLimit() {
		return new Limit(this.mSearchStart, this.mSearchincremental);
	}

	private void resetSearchLimit() {
		this.mSearchStart = 0;
	}

	private void addSearchLimit() {
		this.mSearchStart = this.mSearchStart + this.mSearchincremental;
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(response)) {
			if (caseKey == PHASE_TRACE || caseKey == PHASE_RELATION) {
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				tv_empty_prompt.setText(R.string.no_network);
			}
			return;
		}
		if (caseKey == DEL_TO_TRACE) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				if (iFriendsDao.updateRelation(currUserItem.getUserId(), "1")) {
					// traceLitsAdapetr.delUserItems(currUserItem);
				}
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				} else {
					showToast(R.string.res_getphoto_fail);
				}
			}
		} else if (caseKey == PHASE_TRACE) {
			SearchRelativeResult result = JSONUtil.fromJson(response.toString(), SearchRelativeResult.class);
			if (result != null && result.errorCode == BaseResult.SUCCESS) {
				// phaseTraces(result);
			} else {
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				if (result != null) {
					tv_empty_prompt.setText(R.string.no_relatives_prompt);
				} else {
					tv_empty_prompt.setText(R.string.return_value_999);
				}
			}
		} else if (caseKey == PHASE_RELATION) {

			SearchRelativesResResult resResult = JSONUtil.fromJson(response.toString(), SearchRelativesResResult.class);

			if (resResult != null && resResult.errorCode == BaseResult.SUCCESS) {
				showRelations(resResult.getRelativesResInfo());
				// >>>>>>>>>>是否需要显示加载更多功能
				footerView.onLoadComplete(relativesAdapter.getCount(), resResult.getSumCount());
				// >>>>>>>设置查询区间
				addSearchLimit();
			} else {
				footerView.onLoadComplete();
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				if (resResult != null) {
					tv_empty_prompt.setText(R.string.no_relatives_prompt);
				} else {
					tv_empty_prompt.setText(R.string.return_value_999);
				}
			}
		}
	}

	private void showRelations(List<RelativesResInfo> relativesResInfos) {
		// TODO Auto-generated method stub
		if (relativesResInfos != null && relativesResInfos.size() > 0) {
			if (this.mSearchStart == 0)
				relativesAdapter.setRelativesResInfos(relativesResInfos);
			else
				relativesAdapter.addRelativesResInfos(relativesResInfos);
			intimateDao.saveIntimateData(relativesResInfos);
		} else {
			relativesAdapter.setRelativesResInfos(new ArrayList<RelativesResInfo>());
			iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
			tv_empty_prompt.setText(R.string.no_relatives_prompt);
		}
	}

	/**
	 * 判断服务器和本地是否同步
	 * 
	 * @param users
	 * @return
	 */
	private boolean isPhaseTrace(List<UserItem> users) {
		if (application.getTraceList().size() != users.size()) {
			return false;
		}
		for (UserItem user : users) {
			if (!application.IsTracelistExist(user.getUserId())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 根据后台的返回进行同步,如果后台总数和
	 * 
	 * @param result
	 */
	// private void phaseTraces(SearchRelativeResult result) {
	// // TODO Auto-generated method stub
	// if (result.getUsers() == null || result.getUsers().size() <= 0) {
	// iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
	// tv_empty_prompt.setText(R.string.no_relatives_prompt);
	// return;
	// }
	// if (isPhaseTrace(result.getUsers())) {
	// phaseApplicationTraceList(result.getUsers(), false);
	// } else {
	// phaseApplicationTraceList(result.getUsers(), true);
	// }
	// phaseLocation(result.getUsers());
	// }

	/**
	 * 同步位置
	 * 
	 * @param users
	 */
	// private void phaseLocation(List<UserItem> users) {
	// // TODO Auto-generated method stub
	// for (UserItem userlocation : users) {
	// UserItem userItem =
	// traceLitsAdapetr.getUserItemById(userlocation.getUserId());
	// if (userItem == null) {
	// continue;
	// }
	// userItem.setLatitude(userlocation.getLatitude());
	// userItem.setLongitude(userlocation.getLongitude());
	// userItem.setLastUpdateTime(Util.getSysNowTime());
	// iFriendsDao.updateFriendLocation(userItem);
	// }
	// traceLitsAdapetr.notifyDataSetChanged();
	// }

	/**
	 * 对数据库和application的亲友进行同步，application的同步在数据库中已经做了
	 * isClear:如果前后台的数据不一致，便先清理本地亲友数据，否则不更新关系!
	 * 
	 * @param users
	 */
	// private void phaseApplicationTraceList(List<UserItem> users, boolean
	// isClear) {
	// if (isClear) {
	// if (iFriendsDao.clearTraceUsers(userId)) {
	// for (UserItem userInfo : users) {
	// iFriendsDao.updateRelation(userInfo.getUserId(), "2");
	// }
	// }
	// }
	// application.setTraceList(iFriendsDao.getTracelist(userId));
	// traceLitsAdapetr.setUserItems(application.getTraceList());
	// }

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		handler.post(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				resetSearchLimit();
				phaseRelatives(true);
			}
		});
	}

	View.OnClickListener locateOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			locationCheck((RelativesResInfo) v.getTag());
		}
	};

	public void locationCheck(final RelativesResInfo item) {
		// TODO Auto-generated method stub
		ServerSupportManager serverMana = new ServerSupportManager(getParent(), new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				if (!HttpResponseStatus(response)) {
					return;
				}
				LocationQueryCheckResult result = JSONUtil.fromJson(response.toString(), LocationQueryCheckResult.class);
				if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
					if (result.getLat() == null || result.getLon() == null || result.getLat().equals("")
							|| result.getLon().equals("")) {
						showToast("获取" + item.getUserName() + "的位置信息失败,Sorry~");
					} else {
						if (result.getUserStatus() == 12) {
							showToast(item.getUserName() + "正在穿越中,位置不可查看...");
						} else {
							Intent intent = new Intent();
							UserItem userItem = new UserItem();
							userItem.setUserId(String.valueOf(item.getRuid()));
							userItem.setUserName(item.getUserName());

							userItem.setLatitude(item.getLocation().getLatitude());
							userItem.setLongitude(item.getLocation().getLongitude());

							intent.putExtra("userinfo", userItem);
							intent.setClass(RelativesDynamicActivity.this, MapLocationActivity.class);
							startActivity(intent);
						}
					}
				} else if (result != null && (result.getErrorCode() == BaseResult.FAILED || result.getErrorCode() == 2)) {
					showToast(result.getErrorMessage());
				} else
					showToast(R.string.return_value_999);
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", String.valueOf(item.getRuid())));
		paras.add(new Parameter("visituid", userId));
		paras.add(new Parameter("optype", "1"));
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getLocationQueryCheck(), paras, true, mess);
	}

}