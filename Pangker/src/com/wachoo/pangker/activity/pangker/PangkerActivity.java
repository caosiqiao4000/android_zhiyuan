package com.wachoo.pangker.activity.pangker;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.util.SharedPreferencesUtil;

public class PangkerActivity extends TabActivity {

	private TabHost mTabHost;
	private static final String TAB_PEDESTRIAN = "行人";
	private static final String TAB_MERCHANT = "群组";
	private static final String TAB_MAKE_FRIEND = "亲友圈";
	private static final String TAB_NEAR = "附近";

	private RadioGroup mRadioGroup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pangker);
		mTabHost = this.getTabHost();

		// 亲友
		TabSpec ts1 = mTabHost.newTabSpec(TAB_MAKE_FRIEND).setIndicator(TAB_MAKE_FRIEND);
		ts1.setContent(new Intent(PangkerActivity.this, RelativesDynamicActivity.class));
		mTabHost.addTab(ts1);
		// 行人
		TabSpec ts2 = mTabHost.newTabSpec(TAB_PEDESTRIAN).setIndicator(TAB_PEDESTRIAN);
		ts2.setContent(new Intent(PangkerActivity.this, NearUserActivity.class));
		mTabHost.addTab(ts2);
		// 人群
		TabSpec ts3 = mTabHost.newTabSpec(TAB_MERCHANT).setIndicator(TAB_MERCHANT);
		ts3.setContent(new Intent(PangkerActivity.this, NearGroupActivity.class));
		mTabHost.addTab(ts3);

		// 附近
		TabSpec ts4 = mTabHost.newTabSpec(TAB_NEAR).setIndicator(TAB_NEAR);
		ts4.setContent(new Intent(PangkerActivity.this, PoiActivity.class));
		mTabHost.addTab(ts4);

		initView();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// 首次登录旁客设置向导
		addGuideImage(RelativesDynamicActivity.class.getName(), R.drawable.pangke_01);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		Log.i("popPangker", "main--onKeyDown");
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(Intent.ACTION_MAIN);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // android123提示如果是服务里调用，必须加入new
														// task标识
			i.addCategory(Intent.CATEGORY_HOME);
			startActivity(i);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 初始化
	 */
	private void initView() {
		mRadioGroup = (RadioGroup) findViewById(R.id.contact_radio);

		mRadioGroup.setOnCheckedChangeListener(mOnCheckedChangedListener);
	}

	private final OnCheckedChangeListener mOnCheckedChangedListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(final RadioGroup group, final int checkedId) {
			/** 关闭软键盘 ***/
			InputMethodManager imm = (InputMethodManager) PangkerActivity.this
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(mTabHost.getWindowToken(), 0);
			/*****/
			switch (checkedId) {
			case R.id.radio_group:
				mTabHost.setCurrentTabByTag(TAB_MERCHANT);
				break;
			case R.id.radio_pedestrian:
				mTabHost.setCurrentTabByTag(TAB_PEDESTRIAN);
				addGuideImage(NearUserActivity.class.getName(), R.drawable.pangke_02);
				break;
			case R.id.radio_trace:
				mTabHost.setCurrentTabByTag(TAB_MAKE_FRIEND);
				break;
			case R.id.radio_near:
				mTabHost.setCurrentTabByTag(TAB_NEAR);
				addGuideImage(PoiActivity.class.getName(), R.drawable.pangke_03);
				break;

			}
		}
	};

	/**
	 * TODO 旁客用户首次登录设置向导
	 * 
	 * @param className
	 *            进入页面的key
	 * @param resDrawableId
	 *            向导图片
	 */
	public void addGuideImage(final String className, int resDrawableId) {
		View view = getParent().findViewById(R.id.my_content_view);
		// View view =
		// getWindow().getDecorView().findViewById(resounceView);//查找通过setContentView上的根布局
		if (view == null)
			return;
		if (SharedPreferencesUtil.activityIsGuided(this, className)) {
			// 引导过了
			return;
		}
		ViewParent viewParent = view.getParent();
		if (viewParent instanceof FrameLayout) {
			final FrameLayout frameLayout = (FrameLayout) viewParent;
			if (resDrawableId != 0) {// 设置了引导图片
				final ImageView guideImage = new ImageView(this);
				FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.FILL_PARENT);
				guideImage.setLayoutParams(params);
				guideImage.setScaleType(ScaleType.FIT_XY);
				guideImage.setImageResource(resDrawableId);
				guideImage.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						frameLayout.removeView(guideImage);
						SharedPreferencesUtil.setIsGuided(getApplicationContext(), className);// 设为已引导
					}
				});
				frameLayout.addView(guideImage);// 添加引导图片
			}
		}
	}

}
