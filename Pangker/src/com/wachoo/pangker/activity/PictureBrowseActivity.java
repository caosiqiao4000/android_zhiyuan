package com.wachoo.pangker.activity;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.contact.FriendSelectActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.activity.res.RecommendReceiverAcitvity;
import com.wachoo.pangker.activity.res.UploadPicActivity;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.image.ImgsaveTask;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.photoview.PhotoView;
import com.wachoo.pangker.ui.photoview.PhotoViewAttacher;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

/**
 * @author wangxin 图片浏览器
 */
public class PictureBrowseActivity extends CommonPopActivity {

	final int RequestCode_Friend = 0x10;

	private List<PictureViewInfo> mPicLists;
	private int index;
	private PangkerApplication application;

	private ViewPager pager;
	private ImagePagerAdapter adapter;

	private Button btnBack;
	private TextView txtTitle;
	private TextView txtIndex;
	private Button btnMenu;
	private QuickAction quickAction;

	private ImageButton imgZoomIn;
	private ImageButton imgZoomOut;
	private ImageButton btnToLeft;
	private ImageButton btnToRight;

	private ProgressBar mProgressBar;// >>>>>>等待进度条

	public static final String START_FLAG = "START_FLAG";

	public static final int NET_RESINFO = 1;
	private int start_flag = 0;

	// >>>>>>获取图片
	private ImageResizer mImageWorker;
	private RelativeLayout mRelativeLayout;// >>

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pic_browse);
		start_flag = getIntent().getIntExtra(START_FLAG, 0);
		init();
		initView();
		// initRecommendView();
	}

	private void init() {
		// TODO Auto-generated method stub
		mPicLists = (List<PictureViewInfo>) getIntent().getSerializableExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY);
		if (mPicLists == null || mPicLists.size() == 0) {
			showToast("图片不存在!");
			finish();
		}
		index = getIntent().getIntExtra("index", 0);
		application = (PangkerApplication) getApplication();

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_HD;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);

		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_HD);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
	}

	private void initView() {

		mRelativeLayout = (RelativeLayout) findViewById(R.id.ry_top_bar);
		btnBack = (Button) findViewById(R.id.btn_back);
		if (start_flag == NET_RESINFO)
			btnBack.setBackgroundResource(R.drawable.btn_back_noicon_selector);
		else {
			btnBack.setText("");
			btnBack.setBackgroundResource(R.drawable.btn_back_selector);
		}
		btnBack.setOnClickListener(onClickListener);
		txtIndex = (TextView) findViewById(R.id.txt_index);
		txtIndex.setText((index + 1) + "/" + mPicLists.size());
		btnMenu = (Button) findViewById(R.id.btn_menu);
		btnMenu.setOnClickListener(onClickListener);
		txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("图片浏览");

		if (!Util.isEmpty(mPicLists.get(0).getPicName()))
			txtTitle.setText(mPicLists.get(0).getPicName());
		imgZoomIn = (ImageButton) findViewById(R.id.narrow);
		imgZoomIn.setOnClickListener(onClickListener);
		imgZoomOut = (ImageButton) findViewById(R.id.amplification);
		imgZoomOut.setOnClickListener(onClickListener);

		pager = (ViewPager) findViewById(R.id.pager);
		adapter = new ImagePagerAdapter(mPicLists);
		pager.setAdapter(adapter);
		pager.setOnPageChangeListener(onPageChangeListener);
		pager.setCurrentItem(index, true);

		btnToLeft = (ImageButton) findViewById(R.id.rotationLeft);
		btnToLeft.setOnClickListener(onClickListener);

		btnToRight = (ImageButton) findViewById(R.id.rotationRight);
		btnToRight.setOnClickListener(onClickListener);

		// >>>>>>等待进度条
		mProgressBar = (ProgressBar) findViewById(R.id.pb_waitbar);
		mProgressBar.setVisibility(View.GONE);
		// 显示菜单
		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		// 如果是网络的图片
		if (isNetBrowse()) {
			quickAction.addActionItem(new ActionItem(1, "保存到相册"));
			quickAction.addActionItem(new ActionItem(2, "推荐给好友"), true);
		}
		// 如果是本地的图片
		else {
			quickAction.addActionItem(new ActionItem(11, "上传至网盘"));
			quickAction.addActionItem(new ActionItem(12, "转发给好友"), true);
		}
		quickAction.setOnActionItemClickListener(onActionItemClickListener);

	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			mRelativeLayout.setVisibility(View.GONE);
		};
	};

	public void onBackPressed() {
		setResult(1);
		this.finish();
		super.onBackPressed();
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				if (start_flag == NET_RESINFO) {
					application.setViewIndex(index);
				}
				setResult(1);
				finish();

			}
			if (v == btnMenu) {
				quickAction.show(v);
			}
		}

	};

	private boolean isNetBrowse() {
		// TODO Auto-generated method stub
		PictureViewInfo viewInfo = mPicLists.get(0);
		return viewInfo.getPicPath().startsWith(Configuration.getBaseUrl());
	}

	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			// TODO Auto-generated method stub
			switch (actionId) {
			case 1:
				saveGallery();
				break;
			case 2:
				recommendImage();
				break;
			case 11:
				uploadPic();
				break;
			case 12:
				sendToFriend();
				break;
			}
		}
	};

	// 上传到网盘
	private void uploadPic() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		intent.putExtra(DocInfo.FILE_PATH, mPicLists.get(index).getPicPath());
		intent.setClass(this, UploadPicActivity.class);
		startActivity(intent);
	}

	// 保存到本地相册
	private void saveGallery() {
		// TODO Auto-generated method stub
		new ImgsaveTask(this).execute(mPicLists.get(index).getPicPath());
	}

	// 推荐资源
	private void recommendImage() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ContactsSelectActivity.class);
		intent.putExtra(ContactsSelectActivity.INTENT_SELECT, ContactsSelectActivity.TYPE_RES_RECOMMEND);
		intent.putExtra(ResShowEntity.RES_ENTITY, application.getResLists().get(index));
		startActivity(intent);
	}

	// 发送给好友
	private void sendToFriend() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, FriendSelectActivity.class);
		startActivityForResult(intent, RequestCode_Friend);
	}

	protected void onDestroy() {
		super.onDestroy();
	};

	ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {

		@Override
		public void onPageSelected(int pageindex) {
			// TODO Auto-generated method stub
			index = pageindex;
			txtIndex.setText((pageindex + 1) + "/" + mPicLists.size());// >>>>>>>>设置图片名称
			if (mPicLists.get(pageindex).getPicName() != null) {
				txtTitle.setText(mPicLists.get(pageindex).getPicName());
			}

			adapter.notifyDataSetChanged();
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}

	};

	private class ImagePagerAdapter extends PagerAdapter {

		List<PictureViewInfo> mPicLists;

		public ImagePagerAdapter(List<PictureViewInfo> mPicLists) {
			super();
			this.mPicLists = mPicLists;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mPicLists.size();
		}

		private PictureViewInfo getItem(int position) {
			// TODO Auto-generated method stub
			return mPicLists.get(position);
		}

		@Override
		public Object instantiateItem(ViewGroup view, int position) {
			// >>>>>>>>获取图片地址
			PictureViewInfo pictureViewInfo = getItem(position);

			PhotoView photoView = new PhotoView(view.getContext());
			photoView.setId(position);
			photoView.setScaleType(ScaleType.FIT_CENTER);
			photoView.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {

				@Override
				public void onViewTap(View view, float x, float y) {
					// TODO Auto-generated method stub
					Log.i("PictureBrowseActivity", ">>>photoView>>>onViewTap");

					if (mRelativeLayout.getVisibility() == View.GONE)
						mRelativeLayout.setVisibility(View.VISIBLE);
					else
						mRelativeLayout.setVisibility(View.GONE);
				}
			});
			// Now just add PhotoView to ViewPager and return it
			view.addView(photoView, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);

			// >>>>>>>>设置进度条
			mProgressBar.setVisibility(View.VISIBLE);
			mImageWorker.setmProgressBar(mProgressBar);

			// >>>>>>>判断是否是网络图片
			if (pictureViewInfo.isNet()) {
				String key = Util.getRightString(pictureViewInfo.getPicPath(), "=");
				int sideLength = ImageUtil.getPicPreviewResolution(PictureBrowseActivity.this);
				// 设置默认显示图片
				mImageWorker
						.setLoadingImage(mImageWorker.getImageCache().getBitmapFromMemCache(key + "_" + sideLength));
				mImageWorker.loadImage(pictureViewInfo.getPicPath(), photoView, key);

			}
			// >>>>>>>>>本地图片（手机本地）
			else {
				mProgressBar.setVisibility(View.GONE);
				Bitmap bitmap = null;
				if (mImageWorker.getImageCache().getBitmapFromMemCache(pictureViewInfo.getPicPath()) != null) {
					bitmap = mImageWorker.getImageCache().getBitmapFromMemCache(pictureViewInfo.getPicPath());
				} else {
					// >>>>>>>>图片展示
					bitmap = ImageUtil.decodeFile(
							pictureViewInfo.getPicPath(),
							ImageUtil.getWidthPixels(PictureBrowseActivity.this),
							ImageUtil.getWidthPixels(PictureBrowseActivity.this)
									* ImageUtil.getHeightPixels(PictureBrowseActivity.this));
					mImageWorker.getImageCache().addBitmapToCache(pictureViewInfo.getPicPath(), bitmap, false);
				}

				if (bitmap != null)
					photoView.setImageBitmap(bitmap);
				else {
					photoView.setImageResource(R.drawable.photolist_head);
				}
			}
			return photoView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RequestCode_Friend && resultCode == RESULT_OK) {
			UserItem userItem = (UserItem) data.getSerializableExtra("UserItem");
			goToMessageTalk(userItem);
		}
	}

	private void goToMessageTalk(UserItem item) {
		Intent intent = new Intent();
		intent.putExtra(UserItem.USERID, item.getUserId());
		String username = item.getrName() != null ? item.getrName() : item.getUserName();
		intent.putExtra(UserItem.USERNAME, username);
		intent.putExtra(UserItem.USERSIGN, item.getSign());
		intent.putExtra(PangkerConstant.STATE_KEY, item.getState());

		intent.putExtra(ChatMessage.MSG_FLAG, true);
		intent.putExtra(ChatMessage.MSG_FILEPATH, mPicLists.get(index).getPicPath());
		intent.putExtra(ChatMessage.MSG_TYPE, PangkerConstant.RES_PICTURE);
		intent.setClass(this, MsgChatActivity.class);
		launch(intent, this);
	}
}
