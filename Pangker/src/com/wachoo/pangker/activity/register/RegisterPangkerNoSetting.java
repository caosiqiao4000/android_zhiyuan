package com.wachoo.pangker.activity.register;

import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.root.PangkerNoSetting;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.response.BaseResult;

public class RegisterPangkerNoSetting extends PangkerNoSetting implements IUICallBackInterface {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.register_pangkerno_setting);
		mPangkerNo = (EditText) findViewById(R.id.et_account);

		mPassword = (EditText) findViewById(R.id.et_pass1);
		mPasswordConfim = (EditText) findViewById(R.id.et_pass2);
		btn_submit = (Button) findViewById(R.id.btn_submit);
		btn_submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				setFingerAccount(RegisterPangkerNoSetting.this);
			}
		});
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		BaseResult baseResult = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
		if (baseResult != null && baseResult.errorCode == BaseResult.SUCCESS) {
			showToast(baseResult.errorMessage);
			Intent intent = new Intent(RegisterPangkerNoSetting.this, UserUploadHeadIconActivity.class);
			startActivity(intent);
			finish();

		} else if (baseResult != null && baseResult.errorCode == BaseResult.FAILED) {
			showToast(baseResult.getErrorMessage());
		} else {
			showToast(R.string.to_server_fail);
		}
	}

	public void onAttachedToWindow() {
		this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
		super.onAttachedToWindow();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			Log.i(TAG, "KEYCODE_BACK");
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
