package com.wachoo.pangker.activity.register;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.LoginByFingerprint;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.about.AgreementActivity;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.listener.TextCountLimitWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UserRegisterResult;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.util.Util;

/***
 * 注册界面
 * 
 * @author wangxin
 * 
 */
public class RegisterSendMsgActivity extends CommonPopActivity implements IUICallBackInterface {

	private String TAG = "RegisterSendMsgActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();

	private EditText et_username;
	private String IMSI;
	private String userName;
	private String password;// 用户密码
	private UserInfo myself;
	private TextView tvAgreement;

	Builder builder;
	// int count = 2;
	long timeSleep = 1000 * 3;// 10s
	PangkerApplication application;

	//
	private CheckBox cbTerms;

	private Button btnReplaceCard;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.register);
		et_username = (EditText) findViewById(R.id.input_username);
		cbTerms = (CheckBox) findViewById(R.id.checkbox_item);
		Button btn_register = (Button) findViewById(R.id.btn_register);

		Util.delFileData(this, PangkerConstant.CERTIFICATE_FILE_NAME);

		IMSI = Util.getImsiNum(this);
		application = ((PangkerApplication) getApplication());

		// 注册按钮
		btn_register.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (checkInput()) {
					useInterfaceRegister();
				}
			}

		});
		btnReplaceCard = (Button) findViewById(R.id.btn_change_phone);
		btnReplaceCard.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(RegisterSendMsgActivity.this, LoginByFingerprint.class);
				startActivityForResult(intent, 1);
			}
		});
		et_username.addTextChangedListener(new TextCountLimitWatcher(18, et_username));
		tvAgreement = (TextView) findViewById(R.id.tv_agreement);
		tvAgreement.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(RegisterSendMsgActivity.this, AgreementActivity.class);
				intent.putExtra(AgreementActivity.START_FLAG, AgreementActivity.REGISTER);
				startActivity(intent);
			}
		});

	}

	private void useInterfaceRegister() {
		try {

			ServerSupportManager serverMana = new ServerSupportManager(this, this);

			password = Util.genRandomNum(6);
			// TODO Auto-generated method stub
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("username", userName));
			paras.add(new Parameter("imsi", IMSI));
			paras.add(new Parameter("password", password));

			serverMana.supportRequest(Configuration.getRegisterurl(), paras, true, "请稍后...");

		} catch (Exception e) {
			logger.error(TAG, e);
		}
	}

	/**
	 * 检查参数
	 * 
	 * @author zhengjy
	 */
	private boolean checkInput() {
		userName = et_username.getText().toString();
		if (Util.isEmpty(userName)) {
			this.showToast(R.string.tip_no_nickname);
			return false;
		}
		if (!cbTerms.isChecked()) {
			this.showToast(R.string.tip_no_agree_terms);
			return false;
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		showLogoutDialog();
	}

	private void showLogoutDialog() {
		// TODO Auto-generated method stub
		MessageTipDialog logoutDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
			@Override
			public void buttonResult(boolean isSubmit) {
				// TODO Auto-generated method stub
				if (isSubmit) {
					finish();
				}
			}
		}, this);
		logoutDialog.setTitle("退出旁客");
		logoutDialog.showDialog("", "是否确定退出?");
	}

	public void showToast(int id) {
		Toast.makeText(RegisterSendMsgActivity.this, id, Toast.LENGTH_SHORT).show();
	}

	public void showToast(String msg) {
		Toast.makeText(RegisterSendMsgActivity.this, msg, Toast.LENGTH_SHORT).show();
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {

			if (myself == null) {
				showToast("登录失败!");
			} else {
				Intent intent = new Intent(RegisterSendMsgActivity.this, RegisterBindReferrals.class);
				startActivity(intent);
				RegisterSendMsgActivity.this.finish();
			}

		};
	};

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1 && resultCode == RESULT_OK) {
			finish();
		}
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse)){
			Message message = handler.obtainMessage();
			message.what = 1;
			message.sendToTarget();
			return;
		}
			
		UserRegisterResult registerResult = JSONUtil.fromJson(supportResponse.toString(), UserRegisterResult.class);

		if (registerResult.getErrorCode() == BaseResult.SUCCESS && registerResult.getUserInfo() != null
				&& registerResult.getUserInfo().getUserId() != null) {
			myself = registerResult.getUserInfo();
			application.setMySelf(myself);
		} else {
			Log.i(TAG, "注册失败:" + registerResult.getErrorMessage());
		}

		Message message = handler.obtainMessage();
		message.what = 1;
		message.sendToTarget();
	};
}
