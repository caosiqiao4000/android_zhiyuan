package com.wachoo.pangker.activity.register;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.R.drawable;
import com.wachoo.pangker.activity.R.id;
import com.wachoo.pangker.activity.R.layout;
import com.wachoo.pangker.activity.R.string;
import com.wachoo.pangker.activity.camera.CameraCompActivity;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.impl.PKUserDaoImpl;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.image.PKIconLoader;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.HeadUploadResult;
import com.wachoo.pangker.util.ImageUtil;

public class UserUploadHeadIconActivity extends CameraCompActivity implements IUICallBackInterface {

	private String TAG = "UploadAvatarActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();

	private Button btn_next;
	// private Button btn_skip;
	private ImageView iv_head_portrait;

	private String mUserID;
	private final int DIALOG_ID_PORTRAIT = 0x100; // 上传头像id
	private IPKUserDao iPKUserDao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.upload_avater);
		iPKUserDao = new PKUserDaoImpl(this);
		mUserID = ((PangkerApplication) getApplication()).getMyUserID();
		btn_next = (Button) findViewById(R.id.btn_icon_next);
		btn_next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(UserUploadHeadIconActivity.this, UserInfoDetailActivity.class);
				startActivity(intent);
				UserUploadHeadIconActivity.this.finish();

			}
		});

		iv_head_portrait = (ImageView) findViewById(R.id.iv_head_portrait);
		iv_head_portrait.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (v == iv_head_portrait) {
					showImageDialog("头像上传", actionItems2, true);
				}
			}
		});
	}

	private void dealHeadIcon(HeadUploadResult result) {
		// TODO Auto-generated method stub
		if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
			UserInfo userinfo = ((PangkerApplication) getApplication()).getMySelf();
			userinfo.setIconType(1);
			// 更新本地数据库；
			iPKUserDao.updateUser(userinfo);
			showToast(result.getErrorMessage());
			// PKIconCache.refreshUserIcon(mUserID, bitmap);
		} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
			showToast(result.getErrorMessage());
			iv_head_portrait.setImageResource(R.drawable.sign_head);
		} else {
			showToast(R.string.to_server_fail);
			iv_head_portrait.setImageResource(R.drawable.sign_head);
		}
		// setUserIcon(mUserID);

	}

	/**
	 * 上传用户头像
	 * 
	 * @param stream
	 *            void
	 * @throws FileNotFoundException
	 */
	private void uploadHead(String imageLocalPath) {
		try {

			File file = new File(imageLocalPath);

			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", mUserID));

			MyFilePart uploader = new MyFilePart(file.getName(), file, null);
			String mess = getResources().getString(R.string.uploading);
			ServerSupportManager serverMana = new ServerSupportManager(this, this);
			serverMana.supportRequest(Configuration.getUploadHead(), paras, uploader, imageLocalPath, true, mess,
					DIALOG_ID_PORTRAIT);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		if (!HttpResponseStatus(supportResponse))
			return;
		if (caseKey == DIALOG_ID_PORTRAIT) {
			// TODO Auto-generated method stub
			HeadUploadResult result = JSONUtil.fromJson(supportResponse.toString(), HeadUploadResult.class);
			dealHeadIcon(result);
		}
	}

	@Override
	public void doImageBitmap(final String imageLocalPath) {
		// TODO Auto-generated method stub
		Bitmap bitmap = ImageUtil.decodeFile(imageLocalPath, PKIconLoader.iconSideLength, PKIconLoader.iconSideLength
				* PKIconLoader.iconSideLength);

		iv_head_portrait.setImageBitmap(bitmap);
		AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle("是否上传头像?")
				.setPositiveButton("是", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (dialog != null) {
							dialog.dismiss();
						}
						uploadHead(imageLocalPath);

					}
				}).setNegativeButton("否", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (dialog != null) {
							dialog.dismiss();
						}
						if (((PangkerApplication) getApplication()).getMySelf().getIconType() == 0) {
							iv_head_portrait.setImageResource(R.drawable.nav_head);
							return;
						}

					}
				});
		builder.create().show();
	}

}
