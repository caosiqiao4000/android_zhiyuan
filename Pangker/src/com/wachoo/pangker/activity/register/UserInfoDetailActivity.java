package com.wachoo.pangker.activity.register;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.R.array;
import com.wachoo.pangker.activity.R.id;
import com.wachoo.pangker.activity.R.layout;
import com.wachoo.pangker.activity.R.string;
import com.wachoo.pangker.activity.about.PangkerIntroducedActivity;
import com.wachoo.pangker.activity.root.CitySelectionActivity;
import com.wachoo.pangker.entity.Area;
import com.wachoo.pangker.listener.TextCountLimitWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;

public class UserInfoDetailActivity extends CommonActivity implements
		IUICallBackInterface {
	
	private Button btn_submit_next;
	private Button btn_next;

	// 1）真实姓名 2）性别 3）生日 4）生肖 5）星座 6）城市 7）爱好
	private TextView tvBirthday;
	private TextView tvAge;
	private TextView tvZodiac;
	private TextView tvStar;
	private TextView tvCity;
	//private TextView tvHobby;
	// 用户名、性别、年龄、身高、体重、学历
	// 生日、生肖、星座、血型、城市、爱好

	// 点击LinearLayout弹出修改相应属性界面
	private LinearLayout lyAge;
	private LinearLayout lyBirthday;
	private LinearLayout lyStar;
	private LinearLayout lyZodiac;
	private LinearLayout lyCity;
	private RadioGroup radioGroup;

	private final int DIALOG_ID_REALNAME = 0x100;
	private final int DIALOG_ZODIAC = 0x103;
	private final int DIALOG_ID_AGE = 0x104;
	private final int DIALOG_ID_STAR = 0x106;

	private View dialogView;
	private EditText et_dialog;

	private int currentDialogId = -1;
	private int cityRequestCode = 100;

	private String[] SEXCODE_ITEM;// 性别代码
	private String[] ZODIAC_ITEM;// 生肖
	private String[] ZODIACCODE_ITEM;// 生肖代码
	private String[] STAR_ITEM;// 星座
	private String[] STARCODE_ITEM;// 星座代码
	private String[] AGE_ITEM;// 年龄

	private String realname_item = "";// 真实姓名
	private String birthday_item = "";// 生日
	private String sexCode = "0";// 性别代码
	private String starCode = "1"; // 当前星座代码
	private String zodiacCode = "1"; // 当前生肖代码

	private int age_itemid = 0;// 年龄
	private int zodiac_itemid = 0;// 生肖
	private int star_itemid = 0;// 星座
	private int YEAR_ITEM = 1980;
	private int MONTH_ITEM = 0;
	private int DAY_ITEM = 1;
	
	String mUserID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.personal_information);
		mUserID = ((PangkerApplication) getApplication()).getMyUserID();
		initView(); // 初始化view UI界面
		initListener(); // 初始化监听事件
		initData(); // 初始化数据
	}

	// >>>>>>>>>> 初始化view UI界面
	private void initView() {
		// TODO Auto-generated method stub
		btn_next = (Button) findViewById(R.id.skip);
		btn_submit_next = (Button) findViewById(R.id.submit_next);

		tvAge = (TextView) findViewById(R.id.user_tv_age);
		tvBirthday = (TextView) findViewById(R.id.user_tv_birthday);
		tvZodiac = (TextView) findViewById(R.id.user_tv_zodiac);
		tvStar = (TextView) findViewById(R.id.user_tv_star);
		tvCity = (TextView) findViewById(R.id.user_tv_city);

		lyAge = (LinearLayout) findViewById(R.id.user_ly_age);
		lyBirthday = (LinearLayout) findViewById(R.id.user_ly_birthday);
		lyZodiac = (LinearLayout) findViewById(R.id.user_ly_zodiac);
		lyStar = (LinearLayout) findViewById(R.id.user_ly_star);
		lyCity = (LinearLayout) findViewById(R.id.user_ly_city);
		radioGroup = (RadioGroup)findViewById(R.id.radio_group);
		
	}

	/**
	 * 初始化监听器
	 */
	private void initListener() {
		btn_submit_next.setOnClickListener(mClickListener);
		btn_next.setOnClickListener(mClickListener);
		lyAge.setOnClickListener(mClickListener);
		lyBirthday.setOnClickListener(mClickListener);
		lyZodiac.setOnClickListener(mClickListener);
		lyStar.setOnClickListener(mClickListener);
		lyCity.setOnClickListener(mClickListener);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				RadioButton radioButton = (RadioButton)group.findViewById(checkedId);
				if(checkedId == R.id.ck_sex_man && radioButton.isChecked()){
					sexCode = SEXCODE_ITEM[0];
				}
				else if(checkedId == R.id.ck_sex_women && radioButton.isChecked()){
					sexCode = SEXCODE_ITEM[1];
				}
				else if(checkedId == R.id.ck_sex_no ){
					sexCode = SEXCODE_ITEM[0];
				}
			}
		});
	}

	/**
	 * 获取数据数组
	 */
	private void initData() {

		//SEX_ITEM = getResources().getStringArray(R.array.mf_sex_item);
		SEXCODE_ITEM = getResources().getStringArray(R.array.mf_sexcode_item);

		ZODIAC_ITEM = getResources().getStringArray(R.array.zodiac_item);
		ZODIACCODE_ITEM = getResources()
				.getStringArray(R.array.zodiaccode_item);

		STAR_ITEM = getResources()
				.getStringArray(R.array.mf_constellation_item);
		STARCODE_ITEM = getResources().getStringArray(
				R.array.mf_constellationcode_item);

		AGE_ITEM = getResources().getStringArray(R.array.mf_age_item);

	}

	/**
	 * View单击监听器
	 */
	private OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == btn_submit_next) {// 确定
				update();// 修改用户信息
			} else if (v == btn_next) {
				// 跳转到下一个界面
				clearAll();
				Intent intent = new Intent(UserInfoDetailActivity.this, PangkerIntroducedActivity.class);
				startActivity(intent);
				UserInfoDetailActivity.this.finish();
			}/* else if (v == lySex) {
				mShowDialog(DIALOG_ID_SEX);
			}*/ else if (v == lyAge) {
				mShowDialog(DIALOG_ID_AGE);
			} else if (v == lyBirthday) {
				new DatePickerDialog(UserInfoDetailActivity.this,
						listener, YEAR_ITEM, MONTH_ITEM, DAY_ITEM).show();
			} else if (v == lyZodiac) {
				mShowDialog(DIALOG_ZODIAC);
			} else if (v == lyStar) {
				mShowDialog(DIALOG_ID_STAR);
			} else if (v == lyCity) {// 选择城市
				Intent intent = new Intent();
				intent.setClass(UserInfoDetailActivity.this,
						CitySelectionActivity.class);
				startActivityForResult(intent, cityRequestCode);
			} 
		}
	};

	// >>>>>>>>先清除之前的dialog再显示新的dialog
	private void mShowDialog(int id) {
		if (currentDialogId != -1) {
			removeDialog(currentDialogId);
		}
		currentDialogId = id;
		showDialog(id);
	}

	/**
	 * dialog单选以及确定、取消按钮监听
	 */
	private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			if (dialog != null) {
				dialog.cancel();
			}
			setTextView(which);
		}
	};

	/**
	 * 创建新的Dialog
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(
				UserInfoDetailActivity.this);
		switch (id) {
		case DIALOG_ID_REALNAME:
			builder.setTitle("姓名");
			dialogView = LayoutInflater.from(this).inflate(
					R.layout.dialog_editview, null);
			et_dialog = (EditText) dialogView.findViewById(R.id.et_dialog);
			et_dialog.addTextChangedListener(new TextCountLimitWatcher(18,
					et_dialog));
			et_dialog.setHint(R.string.please_input);
			et_dialog.setText(realname_item);
			builder.setView(dialogView);
			builder.setPositiveButton(R.string.submit, mDialogClickListener);
			builder.setNegativeButton(R.string.cancel, null);
			break;
		case DIALOG_ID_AGE:
			builder.setTitle(R.string.mf_set_age_between);
			builder.setSingleChoiceItems(AGE_ITEM, age_itemid,
					mDialogClickListener);
			break;

		case DIALOG_ZODIAC:
			builder.setTitle(R.string.user_set_zodiac);
			builder.setSingleChoiceItems(ZODIAC_ITEM, zodiac_itemid,
					mDialogClickListener);
			break;
		case DIALOG_ID_STAR:
			builder.setTitle(R.string.mf_set_constellation);
			builder.setSingleChoiceItems(STAR_ITEM, star_itemid,
					mDialogClickListener);
			break;

		}
		return builder.create();
	}


	/**
	 * 日期监听
	 */
	private DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, monthOfYear);
			cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			if(Calendar.getInstance().before(cal)){
				showToast(R.string.personall_info_birthday);
				return;
			}
			SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
			birthday_item = simple.format(cal.getTime());
			tvBirthday.setText(birthday_item);
			YEAR_ITEM = year;
			MONTH_ITEM = monthOfYear;
			DAY_ITEM = dayOfMonth;
			
			try {
				int age = Util.getAge(cal.getTime());
				tvAge.setText(String.valueOf(age));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			zodiac_itemid = Util.date2Zodica(cal);
			tvZodiac.setText(ZODIAC_ITEM[zodiac_itemid]);// 生肖
			zodiacCode = ZODIACCODE_ITEM[zodiac_itemid];

			star_itemid = Util.date2Constellation(cal);
			tvStar.setText(STAR_ITEM[star_itemid]);
			starCode = STARCODE_ITEM[star_itemid];
		}
	};

	/**
	 * 更新
	 * 
	 * @author wubo
	 * @createtime 2012-1-4 下午03:43:37
	 */
	public void update() {
		String s_age = tvAge.getText().toString();
		String s_birthday = tvBirthday.getText().toString();

		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", mUserID));
		paras.add(new Parameter("birthday", s_birthday));

		paras.add(new Parameter("sex", sexCode != null ? sexCode : ""));
		if(area != null){
			paras.add(new Parameter("country", area.getCountyID() != null ? area.getCountyID() : ""));
			paras.add(new Parameter("provinceid", area.getProvinceID()));
			paras.add(new Parameter("city", area.getCityID()));
			paras.add(new Parameter("area", area.getAreaID()));
		}
		String s_constellation = tvStar.getText().toString();
		if (!Util.isEmpty(s_constellation)) {
			paras.add(new Parameter("constellation", starCode != null ? starCode : ""));
		}
		String s_zodiac = tvZodiac.getText().toString();
		if (!Util.isEmpty(s_zodiac)) {
			paras.add(new Parameter("zodiac", zodiacCode != null ? zodiacCode : ""));
		}

		paras.add(new Parameter("age", s_age));
        
		serverMana.supportRequest(Configuration.getUserinfoRefresh(), paras, true, "请稍候...");
	}

	/**
	 * Dialog显示后被点击返回值设置
	 * 
	 * @param index
	 */
	private void setTextView(int index) {
		switch (currentDialogId) {
		
		case DIALOG_ID_AGE:
			tvAge.setText(AGE_ITEM[index]);
			break;

		case DIALOG_ZODIAC:
			zodiacCode = ZODIACCODE_ITEM[index];
			tvZodiac.setText(ZODIAC_ITEM[index]);
			break;

		case DIALOG_ID_STAR:
			starCode = STARCODE_ITEM[index];
			tvStar.setText(STAR_ITEM[index]);
			break;
		}
	}

	private com.wachoo.pangker.entity.Area area;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		// 选择城市后返回值
		if (requestCode == cityRequestCode && resultCode == RESULT_OK
				&& data != null) {
			area = (com.wachoo.pangker.entity.Area) data.getExtras()
					.getSerializable(Area.AREA_TAG);
			if (area != null) {
				tvCity.setText(area.getAreaAddress());
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	// 验证非空
	public boolean checkEmpty(String check, final String toastText) {
		if (check == null || check.equals("")) {
			return true;
		} else {
			return false;
		}
	}
	
	private void clearAll() {
		// TODO Auto-generated method stub
		radioGroup.setEnabled(false);
		lyAge.setEnabled(false);
		lyBirthday.setEnabled(false);
		lyZodiac.setEnabled(false);
		lyStar.setEnabled(false);
		lyCity.setEnabled(false);
		
		tvBirthday.setText("");
		tvAge.setText("");
		tvZodiac.setText("");
		tvStar.setText("");
		tvCity.setText("");
	}

	/**
	 * 根据得到的code匹配对应的内容
	 * @author wubo
	 * @createtime 2012-1-4 下午05:08:45
	 * @param code
	 * @param ValuesCode
	 * @param Values
	 * @return
	 */
	public String ConvertCodeToValue(String code, String[] ValueCodes,
			String[] Values, String checkItem) {
		for (int i = 0; i < ValueCodes.length; i++) {
			if (code.equals(ValueCodes[i])) {
				if (checkItem.equals("STAR_ITEMID")) {
					star_itemid = i;
				}
				if (checkItem.equals("AGE_ITEMID")) {
					age_itemid = i;
				}

				if (checkItem.equals("ZODIAC_ITEMID")) {
					zodiac_itemid = i;
				}
				return Values[i];
			}
		}
		return "";
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;
		
		BaseResult result = JSONUtil.fromJson(supportResponse
				.toString(), BaseResult.class);
		if (result != null && result.getErrorCode() != 999) {
			if (result.getErrorCode() == BaseResult.SUCCESS) {
				Intent intent = new Intent(this, PangkerIntroducedActivity.class);
				startActivity(intent);
				UserInfoDetailActivity.this.finish();
			}
		} else {
			showToast(R.string.to_server_fail);
		}
	}
}
