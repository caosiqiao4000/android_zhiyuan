package com.wachoo.pangker.activity.camera;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonMapActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.res.ResLocalActivity;
import com.wachoo.pangker.activity.res.ResLocalPicActivity;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.UITableView.ClickListener;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @author libo
 * 
 */
public abstract class CameraMapActivity extends CommonMapActivity {

	private final String Tag = "CamerasActivity";
	private final String IMAGE_UNSPECIFIED = "image/*";

	private final int REQUESTCODE_CAMERA = 0x10;// 打开相机
	private final int REQUESTCODE_GALLERY = 0x12;// 打开相册
	private final int REQUESTCODE_FAV = 0x13;// 打开本地收藏
	private final int REQUESTCODE_ZOOM = 0x14;// 进行剪裁
	private boolean flag_zoom = false;// 是否进行剪裁

	private String currPhotoName;// 文件名称，一时间来算
	private String tempPhotoPath;// 文件的绝对地址

	// protected Bitmap bitmap;

	// 保存的文件格式,如果是图像的话
	private void initImgpath() {
		File file = new File(PangkerConstant.DIR_UPLOADPIC);
		if (!file.exists()) {
			file.mkdirs();
		}
		currPhotoName = Util.getNowTime() + ".jpg";
		tempPhotoPath = PangkerConstant.DIR_UPLOADPIC + currPhotoName;
	}

	/**
	 * 打开相机，得到图片bitmap之后保存到制定目录下
	 * 
	 * @param ifzoom
	 *            :是否进行剪裁
	 */
	protected void openCamera(boolean ifzoom) {
		// TODO Auto-generated method stub

		this.flag_zoom = ifzoom;
		initImgpath();
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = new File(PangkerConstant.DIR_UPLOADPIC, currPhotoName);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		startActivityForResult(intent, REQUESTCODE_CAMERA);
	}

	/**
	 * 打开相册，得到图片bitmap之后保存到制定目录下
	 * 
	 * @param ifzoom
	 *            :是否进行剪裁
	 */
	protected void openGallery(boolean ifzoom) {
		// TODO Auto-generated method stub
		this.flag_zoom = ifzoom;
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				IMAGE_UNSPECIFIED);
		startActivityForResult(intent, REQUESTCODE_GALLERY);
	}

	private void openFavourtive() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra("ResMiddleManager_Flag",
				ResLocalActivity.ResLocalModel.upLoad);
		intent.setClass(this, ResLocalPicActivity.class);
		startActivityForResult(intent, REQUESTCODE_FAV);
	}

	/**
	 * 对图片进行剪裁
	 * 
	 * @param uri
	 */
	private void zoomPicture(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, IMAGE_UNSPECIFIED);
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 240);
		intent.putExtra("outputY", 240);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, REQUESTCODE_ZOOM);
	}

	/**
	 * 获取图片在sd卡中的路径
	 * 
	 * @param uri
	 * @return String
	 */
	public String getPhotoPath(Uri uri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, proj, null, null, null);
		if (cursor == null) {
			return null;
		}
		try {
			int colum_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(colum_index);
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			// cursor.close();
		}
		return null;
	}

	PopMenuDialog menuDialog;
	protected ActionItem[] actionItems1 = new ActionItem[] {
			new ActionItem(1, "拍照"), new ActionItem(2, "本地相册") };
	protected ActionItem[] actionItems2 = new ActionItem[] {
			new ActionItem(2, "本地相册"), new ActionItem(3, "手机收藏图片") };
	protected ActionItem[] actionItems3 = new ActionItem[] {
			new ActionItem(1, "拍照"), new ActionItem(2, "本地相册"),
			new ActionItem(3, "手机收藏图片") };

	protected void showImageDialog(String title, ActionItem[] actionItems,
			boolean flag_zoom) {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this);
			menuDialog.setListener(mClickListener);
		}
		this.flag_zoom = flag_zoom;
		menuDialog.setTitle(title);
		menuDialog.setMenus(actionItems);
		menuDialog.show();
	}

	protected void showImageDialog(String title, ActionItem[] actionItems) {
		// TODO Auto-generated method stub
		showImageDialog(title, actionItems, false);
	}

	ClickListener mClickListener = new ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			switch (actionid) {
			// 拍照
			case 1:
				openCamera(flag_zoom);
				break;
			// 本地相册
			case 2:
				openGallery(flag_zoom);
				break;
			// 手机收藏图片
			case 3:
				openFavourtive();
				break;
			}
			menuDialog.dismiss();
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		// 对图片进行处理,首先判断返回结果如何
		if (resultCode == Activity.RESULT_OK && data != null) {
			// 拍照的图片机进行处理
			if (requestCode == REQUESTCODE_CAMERA
					|| requestCode == REQUESTCODE_GALLERY) {
				// 首先处理图片,可以进行剪裁
				createPhoto(data, false);
			}
			// 对剪裁的图片进行处理
			else if (requestCode == REQUESTCODE_ZOOM) {
				// 不能进行剪裁了
				createPhoto(data, true);
			}
			if (requestCode == REQUESTCODE_FAV) {
				tempPhotoPath = data.getStringExtra(DocInfo.FILE_PATH);
				doImageBitmap(tempPhotoPath);
			}
		} else {

			// >>>>>打开相机 通过URI方式打开
			if (requestCode == REQUESTCODE_CAMERA) {
				getCameraPhoto();
			}

		}
		Log.d(Tag, "camera error!!!");
	}

	/**
	 * 获取bitmap，同时保存在本地，
	 * 
	 * @param data
	 * @param ifnoZoom
	 *            ，不在进行剪裁
	 * @return
	 */
	private void createPhoto(Intent data, boolean ifnoZoom) {
		if (data.hasExtra("data")) {
			// 保存图片之前先初始化图片文件
			initImgpath();
			// 获取图片bitmap
			Bitmap bitmap = data.getParcelableExtra("data");
			// 保存在Sd卡目录下
			ImageUtil.saveFileAndRecycle(PangkerConstant.DIR_UPLOADPIC,
					currPhotoName, bitmap, 100);
			// 如果进行了剪裁，那么将图片交给剪裁进行处理
			if (ifnoZoom) {
				doImageBitmap(tempPhotoPath);
				return;
			}
			// >>>>>>判断是否剪裁
			if (flag_zoom) {
				File file = new File(tempPhotoPath); // 设置文件保存路径
				zoomPicture(Uri.fromFile(file));
			} else {
				doImageBitmap(tempPhotoPath);
			}
		}
		// 如果没有找到Bitmap，那么试试通过Uri找到地址，正常情况下不会走这里
		else {
			// 获取uri
			Uri iuiri = data.getData();
			if (iuiri != null) {
				Log.v(Tag, "iuiri != null");
				tempPhotoPath = getPhotoPath(iuiri);
				File file = new File(tempPhotoPath); // 设置文件保存路径
				if (file.exists()) {
					if (ifnoZoom) {
						doImageBitmap(tempPhotoPath);
						return;
					}
					if (flag_zoom) {
						zoomPicture(Uri.fromFile(file));
					} else {
						doImageBitmap(tempPhotoPath);
					}
				}
			}
		}
	}

	// >>>>>>>>>>获取拍照得到的图片
	public void getCameraPhoto() {

		try {
			// >>>>>>>判断路径是否正确
			if (this.tempPhotoPath == null || this.tempPhotoPath.length() <= 0) {
				return;
			}
			// >>>>>>判断是否剪裁
			if (flag_zoom) {
				File file = new File(tempPhotoPath); // 设置文件保存路径
				zoomPicture(Uri.fromFile(file));
			}
			// >>>>直接拍照上传
			else {
				// >>>>>>>获取图片
				doImageBitmap(tempPhotoPath);
			}

		} catch (OutOfMemoryError e) {
			showToast(R.string.res_get_photo_fail);
		} catch (Exception e) {
			showToast(R.string.res_get_photo_fail);
		}
	}

	public abstract void doImageBitmap(String imageLocalPath);

}
