package com.wachoo.pangker.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.ActivityStackManager;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.image.PKIconFetcher;
import com.wachoo.pangker.image.PKIconLoader;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.server.HttpReqCode;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @author wangxin
 * 
 */
public class CommonPopActivity extends Activity {
	private ActivityStackManager stackManager;

	protected String TAG = "CommonPopActivity";// log tag

	protected static final Logger logger = LoggerFactory.getLogger();
	// >>>>>>获取图片
	protected ImageResizer mImageWorker;
	protected ImageCache mImageCache;
	/**
	 * 
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		stackManager = PangkerManager.getActivityStackManager();
		stackManager.pushActivity(this);
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC); // 让音量键固定为媒体音量控制
	}
	
	protected PKIconFetcher initPKIconFetcher(int sideLength) {
		// TODO Auto-generated method stub
		ImageCacheParams imageCacheParams = new ImageCacheParams("usericons");
		imageCacheParams.imageSideLength = sideLength;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		
		PKIconFetcher mImageResizer = new PKIconFetcher(this, PKIconLoader.iconSideLength);
		mImageResizer.setLoadingImage(R.drawable.nav_head);
		mImageResizer.setmSaveDiskCache(true);
		mImageResizer.setImageCache(mImageCache);
		return mImageResizer;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		stackManager.popActivity(this);
		// >>>>>>>>清空缓存
		if (mImageCache != null) {
			Log.i("ImageCache", "CommonPopActivity>>>ImageCache.removeMemoryCaches");
			mImageCache.removeMemoryCaches();
		}
	}

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public void launch(Context From, Class To, int StartFlag) {
		Util.launch(From, To, StartFlag);
	}

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public void launch(Intent intent, Context From) {
		Util.launch(intent, From);
		// overridePendingTransition(R.anim.popup_show, R.anim.popup_close);
	}

	/**
	 * 直接启动activity
	 * 
	 * @param From
	 * @param To
	 */
	public void launch(Context From, Class To) {
		Util.launch(From, To);
	}

	/**
	 * 根据字符串显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final String toastText) {
		Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
	}

	protected void launch(Class<?> toClass) {
		Intent intent = new Intent(this, toClass);
		startActivity(intent);
	}

	protected void launch(Class<?> toClass, int code) {
		Intent intent = new Intent(this, toClass);
		startActivityForResult(intent, code);
	}

	/**
	 * 根据Id显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final int toastText) {
		Toast.makeText(CommonPopActivity.this, toastText, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 取得资源文件的message
	 * 
	 * @param id
	 * @return
	 */
	public String getResourcesMessage(int id) {
		return getResources().getString(id);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public String getRawText(int id) {
		return Util.getRawText(CommonPopActivity.this, id);
	}

	public String getStringByID(int id) {
		return getResources().getString(id);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// Log.i("popPangker", "OnStart--ComActivity:" +
		// this.getLocalClassName());
		// NotificationManager notificationManager = (NotificationManager)
		// getSystemService(NOTIFICATION_SERVICE);
		// notificationManager.cancel(PangkerConstant.FLAG_ONGOING_EVENT);
	}

	// @Override
	// protected void onStop() {
	// // TODO Auto-generated method stub
	// super.onStop();
	// if (!isActivated(this)) {
	// Log.i("popPangker", "OnStop--ComActivity:" + this.getLocalClassName());
	// NotificationManager notificationManager = (NotificationManager)
	// getSystemService(NOTIFICATION_SERVICE);
	// int icon = R.drawable.pangker; // 通知图标
	// CharSequence tickerText = "旁客正在运行"; // 状态栏显示的通知文本提示
	// long when = System.currentTimeMillis(); // 通知产生的时间，会在通知信息里显示
	// // 用上面的属性初始化Nofification
	// Notification notification = new Notification(icon, tickerText, when);
	// notification.flags = Notification.FLAG_ONGOING_EVENT;
	// // 实例化Intent
	// Intent intent = new Intent(this, this.getClass());
	// intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
	//
	// // 获得PendingIntent
	// PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);
	//
	// PangkerApplication app = (PangkerApplication) getApplication();
	// // 设置事件信息，显示在拉开的里面
	// notification.setLatestEventInfo(this, app.getMySelf().getUserName(),
	// "旁客软件正在运行……", pi);
	// // 发出通知
	// notificationManager.notify(PangkerConstant.FLAG_ONGOING_EVENT,
	// notification);
	// }
	// }

	/**
	 * 
	 * @param context
	 * @return
	 * 
	 * @author wangxin 2012-2-10 上午11:33:37
	 */
	private boolean isActivated(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
		ComponentName task_info = manager.getRunningTasks(2).get(0).topActivity;
		if (task_info.getClassName().equals(context.getClass().getName())) {
			return true;
		} else if (task_info.getPackageName().equals(PangkerConstant.PANGKER_PACKAGE_NAME)) {
			return true;
		}
		return false;
	}

	public void dealFailResult(BaseResult result) {
		if (result != null) {
			showToast(result.getErrorMessage());
		} else {
			showToast(R.string.res_getphoto_fail);
		}
	}

	public boolean HttpResponseStatus(Object supportResponse, boolean showToast) {
		if (supportResponse == null && showToast) {
			showToast(R.string.to_server_fail);
			return false;
		}
		if (supportResponse instanceof HttpReqCode) {
			HttpReqCode httpReqCode = (HttpReqCode) supportResponse;
			if (httpReqCode.equals(HttpReqCode.no_network) && showToast) {
				showToast("请检查您的网络！");
			} else if (httpReqCode.equals(HttpReqCode.error) && showToast) {
				showToast(R.string.to_server_fail);
			}
			return false;
		} else {
			return true;
		}
	}

	public boolean HttpResponseStatus(Object supportResponse) {
		return HttpResponseStatus(supportResponse, true);
	}

	/**
	 * void TODO 设置新注册用户引导
	 * 
	 * @param className
	 * @param resDrawableId
	 */
	public void addGuideImage(final String className, int resDrawableId) {
		View view = getParent().findViewById(R.id.my_content_view);
		// View view =
		// getWindow().getDecorView().findViewById(resounceView);//查找通过setContentView上的根布局
		if (view == null)
			return;
		if (SharedPreferencesUtil.activityIsGuided(this, className)) {
			// 引导过了
			return;
		}
		ViewParent viewParent = view.getParent();
		if (viewParent instanceof FrameLayout) {
			final FrameLayout frameLayout = (FrameLayout) viewParent;
			if (resDrawableId != 0) {// 设置了引导图片
				final ImageView guideImage = new ImageView(this);
				FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.FILL_PARENT);
				guideImage.setLayoutParams(params);
				guideImage.setScaleType(ScaleType.FIT_XY);
				guideImage.setImageResource(resDrawableId);
				guideImage.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						frameLayout.removeView(guideImage);
						SharedPreferencesUtil.setIsGuided(getApplicationContext(), className);// 设为已引导
					}
				});
				frameLayout.addView(guideImage);// 添加引导图片
			}
		}
	}

	protected void hideSoftInput(View v) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}
}
