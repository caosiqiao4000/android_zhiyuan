package com.wachoo.pangker.activity.root;

import mobile.encrypt.EncryptUtils;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.util.Util;

public class SecurityCenterActivity extends CommonPopActivity {

	private WebView vb_webview;
	private WebSettings ws_WebSettings;

	private Button btn_back;
	private TextView tv_title;

	private String imsi = null;
	private String userid = null;
	private String securityurl = null;
	private ProgressBar progressBar1;
	private PangkerApplication application;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.PROGRESS_VISIBILITY_ON);
		setContentView(R.layout.securitycenter);
		application = (PangkerApplication) getApplication();
		imsi = getIntent().getStringExtra("imsi");
		userid = getIntent().getStringExtra("userid");
		securityurl = getIntent().getStringExtra("securityurl");

		try {
			// 获取参数
			progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

			findViewById(R.id.iv_more).setVisibility(View.GONE);
			btn_back = (Button) findViewById(R.id.btn_back);
			btn_back.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SecurityCenterActivity.this.finish();
				}
			});
			tv_title = (TextView) findViewById(R.id.mmtitle);

			vb_webview = (WebView) findViewById(R.id.vb_webview);
			vb_webview.setWebChromeClient(new WebChromeClient() {
				public void onProgressChanged(WebView view, int progress) {
					// Activity和Webview根据加载程度决定进度条的进度大小
					// 当加载到100%的时候 进度条自动消失
					progressBar1.setProgress(progress);
					if (progressBar1.getProgress() == 100) {
						progressBar1.setVisibility(View.GONE);
					}
				}
			});
			// 如果访问的页面中有Javascript，则webview必须设置支持Javascript
			vb_webview.requestFocus();
			ws_WebSettings = vb_webview.getSettings();

			ws_WebSettings.setAllowFileAccess(true);// 设置允许访问文件数据
			ws_WebSettings.setJavaScriptEnabled(true);// 设置支持javascript脚本
			// ws_WebSettings.setBuiltInZoomControls(true);// 设置支持缩放
			vb_webview.setWebViewClient(new WebViewClient() {
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					// 当有新连接时，使用当前的 WebView
					progressBar1.setProgress(0);
					progressBar1.setVisibility(View.VISIBLE);
					view.loadUrl(url);
					return true;
				}
			});

			String securityCenterURL = null;
			if (!Util.isEmpty(securityurl)) {
				tv_title.setText("旁客号码");
				securityCenterURL = EncryptUtils.getAuthString(
						PangkerConstant.APPID, userid, imsi,
						application.getServerTime(), securityurl);
			} else {
				tv_title.setText("安全中心");
				securityCenterURL = EncryptUtils.getAuthString(
						PangkerConstant.APPID, userid != null ? userid : "",
						imsi != null ? imsi : "", application.getServerTime(),
						Configuration.getEnterPassword());
			}
			if (securityCenterURL != null && securityCenterURL.length() > 0) {
				vb_webview.loadUrl(securityCenterURL);
			} else {
				showToast("参数不正确！！");
				this.finish();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @TODO动态在标题栏显示进度条
	 * 
	 * @author zhengjy
	 * 
	 *         2012-11-1
	 * 
	 */
	class WebClient extends WebChromeClient {
		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			// 动态在标题栏显示进度条
			SecurityCenterActivity.this.setProgress(newProgress * 100);
			super.onProgressChanged(view, newProgress);
		}

		@Override
		public void onReceivedTitle(WebView view, String title) {
			SecurityCenterActivity.this.setTitle(title);
			super.onReceivedTitle(view, title);
		}
	}

}
