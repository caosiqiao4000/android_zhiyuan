package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.StartActivity;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.image.AsynImageLoader;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.ResetPhoneResult;
import com.wachoo.pangker.server.response.UserinfoQueryResult;
import com.wachoo.pangker.util.Util;

/**
 * 换卡换号申请
 * 
 * @author zhengjy
 * 
 *         2012-08-30
 * 
 */
public class ReplaceCardNumActivity extends CommonPopActivity {

	private static final Logger logger = LoggerFactory.getLogger();
	private String TAG = "ReplaceCardNumActivity";// log tag
	private EditText etOldPhone;
	private EditText etPass;
	private Button btnSubmit;
	private String oldPhone;
	private String password;

	private int count = 6;
	private Dialog popupDialog;
	private String newIMSI;
	private UserInfo mUserInfo;
	private PangkerApplication application;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.replace_card_num);

		newIMSI = Util.getImsiNum(this);
		application = ((PangkerApplication) getApplication());

		initView();
	}

	private void initView() {

		etOldPhone = (EditText) findViewById(R.id.et_oldphone);
		etPass = (EditText) findViewById(R.id.et_pass);
		btnSubmit = (Button) findViewById(R.id.btn_submit);
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ResetPhoneApply();
			}
		});

	}

	/**
	 * 换卡换号申请
	 * 
	 * 
	 * 接口：ResetPhoneApply.json
	 */
	private void ResetPhoneApply() {
		oldPhone = etOldPhone.getText().toString();
		password = etPass.getText().toString();
		if (Util.isEmpty(oldPhone)) {
			showToast("旧手机号不能为空!");
			return;
		}
		if (Util.isEmpty(password)) {
			showToast("安全密码不能为空!");
			return;
		}

		ServerSupportManager serverMana = new ServerSupportManager(this,
				new IUICallBackInterface() {

					@Override
					public void uiCallBack(Object supportResponse, int caseKey) {
						// TODO Auto-generated method stub
						if (!HttpResponseStatus(supportResponse))
							return;
						ResetPhoneResult baseResult = JSONUtil.fromJson(
								supportResponse.toString(),
								ResetPhoneResult.class);
						if (baseResult != null
								&& baseResult.errorCode == baseResult.SUCCESS
								&& baseResult.getUserInfo() != null) {
							showUserInfo(baseResult.getUserInfo());
						} else if (baseResult != null
								&& baseResult.errorCode == baseResult.FAILED) {
							showToast(baseResult.getErrorMessage());
						} else
							showToast(R.string.to_server_fail);
					}
				});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("oldPhone", oldPhone));
		paras.add(new Parameter("password", password));// 密码
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getResetPhoneApply(), paras,
				true, mess);
	}

	/**
	 * 到服务器获取用户信息
	 * 
	 * @author wangxin 2012-3-21 下午06:37:05
	 */
	private UserInfo loadHomeUserFromServer() {
		UserInfo info = null;
		try {
			// 调用后台接口，根据IMSI号码返回用户信息的接口 ,返回UserInfo对象
			SyncHttpClient client = new SyncHttpClient();
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", "1"));
			paras.add(new Parameter("imsi", newIMSI));
			String supportResponse = client.httpPost(
					Configuration.getUserinfoQueryByIMSI(), paras,
					application.getServerTime());
			UserinfoQueryResult result = JSONUtil.fromJson(
					supportResponse.toString(), UserinfoQueryResult.class);
			if (result != null && result.getErrorCode() == result.SUCCESS) {
				info = result.getUserInfo();
				Util.writeHomeUser(this, info);
				application.setMySelf(info);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		}
		return info;
	}

	private void showUserInfo(final UserInfo userInfo) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		View view = LayoutInflater.from(this).inflate(
				R.layout.dialog_show_userinfo, null);
		LinearLayout userView = (LinearLayout) view
				.findViewById(R.id.to_userinfo);
		final ImageView ivUserIcon = (ImageView) view.findViewById(R.id.icon);
		TextView tvUserName = (TextView) view.findViewById(R.id.nickname);
		TextView tvSign = (TextView) view.findViewById(R.id.sign);
		builder.setView(view);
		builder.setTitle("确认是自己的账号?");
		builder.setPositiveButton(R.string.submit,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (dialog != null) {
							dialog.dismiss();
						}
						ReplaceCard(userInfo.getUserId(), userInfo.getImsi());
					}
				});
		builder.setNegativeButton(R.string.cancel, null);
		builder.show();
		/*
		 * userView.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Intent intent = new Intent(ReplaceCardNumActivity.this,
		 * EditUserInfoActivity.class); intent.putExtra("userid",
		 * userInfo.getUserId()); intent.putExtra("username",
		 * userInfo.getUserName()); intent.putExtra("usersign",
		 * userInfo.getSign());
		 * ReplaceCardNumActivity.this.startActivity(intent); } });
		 */
		tvUserName.setText(userInfo.getUserName());
		tvSign.setText(userInfo.getSign() != null ? userInfo.getSign() : "");
		AsynImageLoader imageLoader = AsynImageLoader.getAsynImageLoader();
		String iconUrl = Configuration.getIconUrl() + userInfo.getUserId();
		imageLoader.loadDrawable(ivUserIcon, iconUrl, loaderListener);
	}

	AsynImageLoader.OnLoaderListener loaderListener = new AsynImageLoader.OnLoaderListener() {

		@Override
		public void onImgLoader(ImageView iImageView, Bitmap bitmap) {
			// TODO Auto-generated method stub
			if (bitmap != null) {
				iImageView.setImageBitmap(bitmap);
			}
		}

		@Override
		public void onInitLenght(int lenght) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onProgress(int done) {
			// TODO Auto-generated method stub
			Log.d("done", "" + done);
		}

		@Override
		public void onSpeed(String speed) {
			// TODO Auto-generated method stub
		}
	};

	/**
	 * 
	 */
	private void ReplaceCard(String userId, String oldIMSI) {
		count = 6;
		popupDialog = new Dialog(ReplaceCardNumActivity.this,
				android.R.style.Theme_Translucent_NoTitleBar);
		popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		popupDialog.setContentView(R.layout.loading_dialog);
		TextView txtMsg = (TextView) popupDialog
				.findViewById(R.id.left_textView);
		txtMsg.setText("正在重新绑定,请稍候...");
		popupDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		popupDialog.show();

		String phoneNum = PangkerConstant.CHINA_UNICOM_PORT;
		String content = "#REPLACECARD#" + userId + "#" + oldPhone + "#"
				+ oldIMSI + "#" + newIMSI + "#" + password + "#"
				+ application.getPangkerTimeDiff(); // 发送信息的内容

		SmsManager manager = SmsManager.getDefault();
		if (content.length() > 70) {
			ArrayList<String> contentList = manager.divideMessage(content);
			manager.sendMultipartTextMessage(phoneNum, null, contentList, null,
					null);
		} else
			manager.sendTextMessage(phoneNum, null, content, null, null);

		new WaitThread().start();
	}

	public class WaitThread extends Thread {
		@Override
		public void run() {
			super.run();
			while (count > 0 && mUserInfo == null) {
				try {
					Thread.sleep(3000);
					mUserInfo = loadHomeUserFromServer();
					count--;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					logger.error(TAG, e);
				}
			}
			Message message = handler.obtainMessage();
			message.what = 1;
			message.sendToTarget();
		}
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {

			if (msg.what == 1) {
				if (popupDialog != null && popupDialog.isShowing()) {
					popupDialog.dismiss();
				}
				if (mUserInfo == null) {
					showToast("绑定失败!");
				} else {
					showToast("绑定成功!");
					Intent intent = new Intent(ReplaceCardNumActivity.this,
							StartActivity.class);
					startActivity(intent);
					ReplaceCardNumActivity.this.finish();
				}
			}
		};
	};

}
