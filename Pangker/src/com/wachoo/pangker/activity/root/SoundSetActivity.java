package com.wachoo.pangker.activity.root;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.ui.SwitchView;
import com.wachoo.pangker.ui.SwitchView.OnCheckedChangeListener;
import com.wachoo.pangker.util.SharedPreferencesUtil;

public class SoundSetActivity extends CommonPopActivity{

	private LinearLayout soundLayout;//ly_sound
	private LinearLayout shockLayout;
	private SwitchView soundSwitchView;
	private SwitchView shockSwitchView;
	
	private PangkerApplication application;
	private SharedPreferencesUtil mPreferencesUtil;
	private String userId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sound_set);
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		mPreferencesUtil =  new SharedPreferencesUtil(this);
		
		Button button = (Button)findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("消息通知设置");
        findViewById(R.id.iv_more).setVisibility(View.GONE);
		
		soundLayout = (LinearLayout) findViewById(R.id.ly_sound);
		soundLayout.setOnClickListener(listener);
		soundSwitchView = (SwitchView) findViewById(R.id.soundSwitchView);
		soundSwitchView.setOnClickListener(listener);
		soundSwitchView.setOnCheckedChangeListener(onCheckedChangeListener1);
		soundSwitchView.setChecked(mPreferencesUtil.getBoolean(PangkerConstant.SP_SOUND_SET + userId, true));
		
		shockLayout = (LinearLayout) findViewById(R.id.ly_shock);
		shockLayout.setOnClickListener(listener);
		shockSwitchView = (SwitchView) findViewById(R.id.shockSwitchView);
		shockSwitchView.setOnClickListener(listener);
		shockSwitchView.setOnCheckedChangeListener(onCheckedChangeListener2);
		shockSwitchView.setChecked(mPreferencesUtil.getBoolean(PangkerConstant.SP_SHOCK_SET + userId, false));
	}
	
	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == soundLayout || v == soundSwitchView) {
				soundSwitchView.setChecked(!soundSwitchView.isChecked());
				mPreferencesUtil.saveBoolean(PangkerConstant.SP_SOUND_SET + userId, soundSwitchView.isChecked());
			}
            else if (v == shockLayout || v == shockSwitchView) {
            	shockSwitchView.setChecked(!shockSwitchView.isChecked());
            	mPreferencesUtil.saveBoolean(PangkerConstant.SP_SHOCK_SET + userId, shockSwitchView.isChecked());
			}
		}
	};
	
	OnCheckedChangeListener onCheckedChangeListener1 = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(boolean isChecked) {
			// TODO Auto-generated method stub
			mPreferencesUtil.saveBoolean(PangkerConstant.SP_SOUND_SET + userId, soundSwitchView.isChecked());
		}
	};
	
	OnCheckedChangeListener onCheckedChangeListener2 = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(boolean isChecked) {
			// TODO Auto-generated method stub
			mPreferencesUtil.saveBoolean(PangkerConstant.SP_SHOCK_SET + userId, shockSwitchView.isChecked());
		}
	};
}
