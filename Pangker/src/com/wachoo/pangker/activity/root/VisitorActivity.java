package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.InvitedUserAdapter;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.SearchUnderUsersResult;
import com.wachoo.pangker.server.response.SimpleUserInfo;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;

public class VisitorActivity extends CommonPopActivity implements IUICallBackInterface {

	private ServerSupportManager serverMana;
	private ListView usersListView;
	private PullToRefreshListView pullToRefreshListView;//
	private String mUserId;
	private UserItem userItem;
	private EmptyView emptyView;
	private InvitedUserAdapter adapter;
	private PKIconResizer mImageResizer; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blacklist_manager);
		init();
		initView();
		getVisitorInfo();
	}

	private void init() {
		// TODO Auto-generated method stub
		userItem = (UserItem) getIntent().getSerializableExtra("UserItem");
		PangkerApplication application = (PangkerApplication) getApplication();
		mUserId = application.getMyUserID();
		serverMana = new ServerSupportManager(this, this);
		
		mImageResizer = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);
	}

	private void initView() {
		// TODO Auto-generated method stub
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("访客信息");
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		usersListView = pullToRefreshListView.getRefreshableView();
		emptyView = new EmptyView(this);
		pullToRefreshListView.setEmptyView(emptyView.getView());

		adapter = new InvitedUserAdapter(this, InvitedUserAdapter.VISIT_USERS, mImageResizer);
		usersListView.setAdapter(adapter);

		usersListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				SimpleUserInfo currChooseUser = (SimpleUserInfo) arg0.getItemAtPosition(arg2);
				Intent intent = new Intent(VisitorActivity.this, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, currChooseUser.getUserId());
				intent.putExtra(UserItem.USERNAME, currChooseUser.getUserName());
				intent.putExtra(UserItem.USERSIGN,
						currChooseUser.getSign() != null ? currChooseUser.getSign() : "");
				startActivity(intent);
			}
		});
	}

	private void getVisitorInfo() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", mUserId));
		paras.add(new Parameter("opUid", mUserId));
		paras.add(new Parameter("uid", userItem == null ? mUserId : userItem.getUserId()));
		paras.add(new Parameter("limit", "0,20"));

		serverMana.supportRequest(Configuration.getQueryRecentVisitor(), paras);
		pullToRefreshListView.setRefreshing(true);
	}

	OnRefreshListener<ListView> onRefreshListener = new OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			getVisitorInfo();
		}
	};

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(supportResponse)) {
			emptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
		}

		SearchUnderUsersResult result = JSONUtil.fromJson(supportResponse.toString(),
				SearchUnderUsersResult.class);
		if (result == null || result.getErrorCode() == 999) {
			emptyView.showView(R.drawable.emptylist_icon, R.string.return_value_999);
			return;
		}
		if (result.getErrorCode() == 0) {
			emptyView.showView(R.drawable.emptylist_icon, result.getErrorMessage());
			return;
		}
		if (result.getErrorCode() == 1) {
			adapter.setmInvitedUsers(result.getUsers());
			if (result.getUsers() == null || result.getUsers().size() == 0) {
				emptyView.showView(R.drawable.emptylist_icon, R.string.no_visitor_prompt);
			}
		}
	}
}
