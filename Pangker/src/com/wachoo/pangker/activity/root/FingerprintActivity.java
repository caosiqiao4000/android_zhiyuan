package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import mobile.encrypt.EncryptUtils;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;

public class FingerprintActivity extends CommonPopActivity implements IUICallBackInterface {

	private String userId;
	private String account;

	private ScrollView noseting;
	private EditText ed_account;
	private EditText ed_password1;
	private EditText ed_password2;

	private ScrollView hasseting;
	private TextView tv_set_account;
	private TextView tv_title;

	private Button btn_submit;
	private Button btn_secretsecurity;
	private Button btn_editpwd;
	private Button btn_back;
	PangkerApplication application;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.finger_printer);
		application = (PangkerApplication) getApplication();

		userId = application.getMyUserID();
		account = application.getImAccount();
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		// title bar
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(onClickListener);
		tv_title = (TextView) findViewById(R.id.mmtitle);
		tv_title.setText("旁客号码");
		findViewById(R.id.iv_more).setVisibility(View.GONE);

		// 未设置账号界面
		noseting = (ScrollView) findViewById(R.id.noseting);
		hasseting = (ScrollView) findViewById(R.id.hasseting);

		if (account == null || account.length() == 0) {
			noseting.setVisibility(View.VISIBLE);
			hasseting.setVisibility(View.GONE);
			ed_account = (EditText) findViewById(R.id.et_account);
			//ed_account.addTextChangedListener(new AccountEditTextWather(ed_account));
			
			ed_password1 = (EditText) findViewById(R.id.et_pass1);
			ed_password2 = (EditText) findViewById(R.id.et_pass2);
			btn_submit = (Button) findViewById(R.id.btn_submit);
			btn_submit.setOnClickListener(onClickListener);

		} else {
			initHasSetingView();
		}
	}

	private void initHasSetingView() {
		noseting.setVisibility(View.GONE);
		hasseting.setVisibility(View.VISIBLE);
		// 已经设置账号界面

		tv_set_account = (TextView) findViewById(R.id.tv_set_account);
		tv_set_account.setText(account);

		btn_editpwd = (Button) findViewById(R.id.btn_editpwd);
		btn_editpwd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 修改密码
				launch(FingerprintActivity.this, PassSafetySetActivity.class);
			}
		});

		btn_secretsecurity = (Button) findViewById(R.id.btn_secretsecurity);
		btn_secretsecurity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FingerprintActivity.this, SecurityCenterActivity.class);
				intent.putExtra("imsi", Util.getImsiNum(FingerprintActivity.this));
				intent.putExtra("userid", application.getMyUserID());
				intent.putExtra("securityurl", Configuration.getEnterAnswer());
				startActivity(intent);
			}
		});
	}

	public void onBackPressed() {
		// 隐藏输入法
		if (account == null || account.length() == 0) {
			hideSoftInput(ed_account);
		}
		if (!Util.isEmpty(account)) {
			setResult(RESULT_OK);
		}
		finish();
	};

	View.OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.btn_back) {
				onBackPressed();
			}
			if (v.getId() == R.id.btn_submit) {
				setFingerAccount();
			}
		}
	};

	private boolean checkThree(String str) {
		char a = str.charAt(0);
		for (int i = 1; i < str.length(); i++) {
			if (a != str.charAt(i)) {
				return false;
			}
		}
		return true;
	}

	private boolean checkAccount() {
		String account = ed_account.getEditableText().toString();
		if (Util.isEmpty(account)) {
			showToast("请输入旁客号码!");
			return false;
		}
		if (account.length() < 8) {
			showToast("确认旁客号码不低于8位!");
			return false;
		}
		for (int i = 0; i < account.length() - 2; i++) {
			if (checkThree(account.substring(i, i + 3))) {
				showToast("为了确保号码安全，请不要连续输入3位相同的数字!");
				return false;
			}
		}
		return true;
	}

	private boolean checkPwd() {
		String pwd1 = ed_password1.getEditableText().toString();
		String pwd2 = ed_password2.getEditableText().toString();
		if (!pwd1.equals(pwd2)) {
			showToast("请确认两次输入的密码一致!");
			return false;
		}
		if (!pwd1.matches(PangkerConstant.PANGKER_REGEX)) {
			showToast("请输入6-20位密码,同时包含数字和字母!");
			return false;
		}
		return true;
	}

	private void setFingerAccount() {
		// TODO Auto-generated method stub
		if (checkAccount() && checkPwd()) {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("userid", userId));
			ServerSupportManager serverMana = new ServerSupportManager(this, this);

			String account = ed_account.getEditableText().toString().trim();
			String pwd = ed_password1.getEditableText().toString().trim();

			try {
				paras.add(new Parameter("imAccount", account));
				paras.add(new Parameter("imPassword", EncryptUtils.encrypt(pwd)));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.debug(e);
			}
			String mess = getResourcesMessage(R.string.please_wait);
			serverMana.supportRequest(Configuration.getSetIMAccount(), paras, true, mess);
		}
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		BaseResult baseResult = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
		if (baseResult != null && baseResult.errorCode == BaseResult.SUCCESS) {
			showToast(R.string.set_success);
			account = ed_account.getEditableText().toString().trim();
			application.getMySelf().setAccount(account);
			Util.writeHomeUser(this, application.getMySelf());
			initHasSetingView();
		} else if (baseResult != null && baseResult.errorCode == BaseResult.FAILED) {
			showToast(baseResult.getErrorMessage());
		} else {
			showToast(R.string.to_server_fail);
		}
	}
	/**
	 * 
	 * @TODO 限制只能输入数字和字母
	 * 
	 */
	class AccountEditTextWather implements TextWatcher {

		private EditText editText;
		private int maxLenght = 20;// 限制字数
		private CharSequence temp;

		public AccountEditTextWather(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void onTextChanged(CharSequence ss, int start, int before, int count) {
			temp = ss;
			String editable = editText.getText().toString();
			String str = stringFilter(editable.toString());
			if (!editable.equals(str)) {
				editText.setText(str);
				// 设置新的光标所在位置
				editText.setSelection(str.length());
			}
		}

		@Override
		public void afterTextChanged(Editable s) {
			try {
				Log.i(TAG, "cutBefore=" + s.toString());
				if (Util.getHalfStringLength(temp.toString()) > maxLenght) {
					int index = Util.cutStringHalfLength(temp.toString(), maxLenght);
					s.delete(index, s.length());
					this.editText.setText(s);
					this.editText.setSelection(s.length());
				}
				Log.i(TAG, "cutAfter=" + s.toString());
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

	}

	/**
	 * 
	 * String TODO过滤群组名称字符输入
	 * 
	 * @param str
	 * @return
	 * @throws PatternSyntaxException
	 */
	public String stringFilter(String str) throws PatternSyntaxException {
		// 只允许中英文和数字
		String regex = "[^a-zA-Z0-9]";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(str);
		return m.replaceAll("").trim();
	}

}
