package com.wachoo.pangker.activity.root;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;

public class ResRecommendSetingActivity extends CommonPopActivity {

	private Button mBtnBack;// >>>>>>返回按钮

	private TextView txtTitle;// >>>>>>>>>标题

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// >>>>>>>>>>加载view
		setContentView(R.layout.panker_influence);
		mBtnBack = (Button) findViewById(R.id.btn_back);// >>>>>>>>>返回按钮
		findViewById(R.id.iv_more).setVisibility(View.GONE);// >>>>>>>>>隐藏功能按钮

		txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("资源推荐设置");
		
	}
}
