package com.wachoo.pangker.activity.group;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.GroupInfoUserAdapter;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;

/**
 * 
 * @TODO 群组信息
 * 
 * @author zhengjy
 * 
 *         2012-9-6
 * 
 */
public class GroupInfoActivity extends CommonPopActivity implements IUICallBackInterface,
		OnFocusChangeListener {

	PangkerApplication pangkerApplication;
	private com.wachoo.pangker.ui.MyGridView gvUuseritem;
	private LinearLayout lyGroupinfo;
	private LinearLayout lyChatlog;
	private LinearLayout lyClearChatlog;
	private GroupInfoUserAdapter userAdapter;
	private PKIconResizer pkIconResizer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.group_info);
		List<UserItem> selectedUser = (List<UserItem>) this.getIntent().getSerializableExtra("selectedUser");

		pkIconResizer = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);
		initView();

		if (selectedUser != null && selectedUser.size() > 0) {
			userAdapter.setUserList(selectedUser);
		}
	}

	private void initView() {
		gvUuseritem = (com.wachoo.pangker.ui.MyGridView) findViewById(R.id.gv_useritem);
		lyGroupinfo = (LinearLayout) findViewById(R.id.ly_qroupinfo);
		lyChatlog = (LinearLayout) findViewById(R.id.ly_chatlog);
		lyClearChatlog = (LinearLayout) findViewById(R.id.ly_clear_chatlog);
		lyGroupinfo.setOnClickListener(clickListener);
		lyChatlog.setOnClickListener(clickListener);
		lyClearChatlog.setOnClickListener(clickListener);

		userAdapter = new GroupInfoUserAdapter(this, new ArrayList<UserItem>(), pkIconResizer);
		// 添加成员监听
		userAdapter.setmAddClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showToast("添加成员监听");
				GroupInfoActivity.this.finish();
			}
		});
		userAdapter.setCreater(true);
		gvUuseritem.setAdapter(userAdapter);

		gvUuseritem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				if (position < userAdapter.getCount() - 2) {
					UserItem userItem = (UserItem) arg0.getAdapter().getItem(position);
					// 移除成员
					if (userAdapter.isDeleteState()) {
						userAdapter.getUserList().remove(userItem);
						userAdapter.notifyDataSetChanged();
						showToast("移除成员");
					}
					// 查看成员信息
					else {
						showToast("查看成员信息");
						Intent intent = new Intent(GroupInfoActivity.this, UserWebSideActivity.class);
						intent.putExtra(UserItem.USERID, userItem.getUserId());
						intent.putExtra(UserItem.USERNAME, userItem.getUserName());
						intent.putExtra(UserItem.USERSIGN, userItem.getSign());
						GroupInfoActivity.this.startActivity(intent);
					}
				}

			}
		});
		gvUuseritem.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (v != gvUuseritem) {
					userAdapter.setDeleteState(false);
				}
			}
		});
		LinearLayout lyGridView = (LinearLayout) findViewById(R.id.ly_gridview);
		lyGridView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				userAdapter.setDeleteState(false);
			}
		});

	}

	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// 查看群组信息
			if (v == lyGroupinfo) {

			}
			// 查看群组聊天记录
			else if (v == lyChatlog) {

			}
			// 清空群聊信息
			else if (v == lyClearChatlog) {

			}
		}
	};

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v != gvUuseritem && hasFocus) {
			userAdapter.setDeleteState(false);
		}
	}

}
