package com.wachoo.pangker.activity.group;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapController;
import com.amap.mapapi.map.MapView;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonMapActivity;
import com.wachoo.pangker.activity.HomeActivity;
import com.wachoo.pangker.activity.MainActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.adapter.ChatRoomMapUserAdapter;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.impl.UserMsgDaoImpl;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.group.P2PUserInfo;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.map.app.LocateManager;
import com.wachoo.pangker.map.app.LocateManager.LocateType;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CallMeSetResult;
import com.wachoo.pangker.server.response.DriftGrantResult;
import com.wachoo.pangker.service.MeetRoomService;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.HorizontalListView;
import com.wachoo.pangker.ui.OverUserItem;
import com.wachoo.pangker.ui.QuickActionBar;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.WaitingDialog;
import com.wachoo.pangker.util.CopiedIterator;
import com.wachoo.pangker.util.MapUtil;
import com.wachoo.pangker.util.MeetRoomUtil;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

public class GroupMapActivity extends CommonMapActivity implements ITabSendMsgListener {

	private PangkerApplication application;
	private String mMyUserID;
	private IUserMsgDao userMsgDao;
	private PKIconResizer mImageResizer;
	private LocateManager locateManager;
	private Location roomLocation;

	private List<P2PUserInfo> mapUserInfos = new ArrayList<P2PUserInfo>();
	private MapView mMapView;
	private MapController mMapController;
	private Bitmap mBitmap;
	private HorizontalListView horizontalListView;
	private ChatRoomMapUserAdapter mapUserAdapter;

	private UIUserInfo uiUserInfo;
	private WaitingDialog bar;

	private PoiaddressLoader poiaddressLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (onSaveGroup(savedInstanceState)) {
			finish();
			return;
		}
		this.setMapMode(MAP_MODE_VECTOR);// 设置地图为矢量模式
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_group_mapmembers);
		init();
		initView();
		showMemberMap(application.getCurrunGroup().getMaplist());

		poiaddressLoader = new PoiaddressLoader(application);
	}

	private void init() {
		// TODO Auto-generated method stub
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		PangkerManager.getTabBroadcastManager().addMsgListener(this);
		application = (PangkerApplication) getApplication();
		mMyUserID = application.getMyUserID();
		locateManager = new LocateManager(this);
		if (!locateManager.isOpenlocate()) {
			locateManager.onLocate(locateListener);
		}
		// 发送定位请求包
		if (!application.getCurrunGroup().isInMapMember(Long.parseLong(mMyUserID))) {
			sendPack(Configuration.CROOM2SP_STATUS_REPORT_LOCAYION_ADD_FLAG);
			showWaitingBar(R.string.waiting);
		}
	}

	private boolean onSaveGroup(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (savedInstanceState != null) {
			Boolean isSaveIn = savedInstanceState.getBoolean("isJoin");
			if (isSaveIn != null && isSaveIn) {
				showToast("您已经断开连接!");
				Log.d("onSaveGroup", "onSaveGroup==>Logout.");
				return true;
			}
		}
		return false;
	}

	private void initView() {
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("群组地图");
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		// TODO Auto-generated method stub
		mMapView = (MapView) findViewById(R.id.mapview);
		mMapController = mMapView.getController();// 得到mMapView
		// 的控制权,可以用它控制和驱动平移和缩放
		mMapView.setBuiltInZoomControls(true);// 设置启用内置的缩放控件
		mMapController.setZoom(17);// 设置地图zoom 级别
		mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.map_placemark);// 得到需要标在地图上的资源
		// 定义位置和边界
		horizontalListView = (HorizontalListView) findViewById(R.id.horizontalListView);
		mapUserAdapter = new ChatRoomMapUserAdapter(this);
		horizontalListView.setAdapter(mapUserAdapter);
		horizontalListView.setOnItemClickListener(itemClickListener);

		uiUserInfo = new UIUserInfo();
	}

	public void sendPack(int flag) {
		Intent meetService = new Intent(this, MeetRoomService.class);
		meetService.putExtra(MeetRoomService.MEETING_ROOM_INTENT, flag);
		if (flag == Configuration.CROOM2SP_STATUS_REPORT_LOCAYION_ADD_FLAG && roomLocation != null) {
			meetService.putExtra("Location", roomLocation);
		}
		startService(meetService);
	}

	// 定位监听
	LocateManager.LocateListener locateListener = new LocateManager.LocateListener() {
		@Override
		public void onLocationChanged(Location location, int locateResult, LocateType locateType) {
			// TODO Auto-generated method stub
			Log.i(TAG, "onLocation==>" + location.getLongitude() + "," + location.getLatitude());
			roomLocation = new com.wachoo.pangker.entity.Location(location.getLongitude(), location.getLatitude());
			sendPack(Configuration.CROOM2SP_STATUS_REPORT_LOCAYION_ADD_FLAG);
		}
	};

	Handler mapHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MeetRoomUtil.handler_SP2CRoom_Status_Notify:// 房间参与者
				showMemberMap(application.getCurrunGroup().getMaplist());
				break;
			case MeetRoomUtil.handler_SP2CRoom_Disconnect:// 退出房间
				onBackPressed();
				break;
			case MeetRoomUtil.handler_SP2CRoom_Map_Enter:
				enterGroupMap();
				break;
			case MeetRoomUtil.handler_SP2CRoom_Map_Refresh:
				showMemberMap(application.getCurrunGroup().getMaplist());
				break;
			}
		};
	};

	private void enterGroupMap() {
		// TODO Auto-generated method stub
		if (bar != null && bar.isShowing()) {
			showToast("进入群组地图成功");
			bar.dismiss();
		}
		showMemberMap(application.getCurrunGroup().getMaplist());
	}

	private void showWaitingBar(int mess) {
		// TODO Auto-generated method stub
		if (bar == null) {
			bar = new WaitingDialog(this);
			bar.setOnDismissListener(onDismissListener);
		}
		bar.setWaitMessage(mess);
		bar.show();
	}

	DialogInterface.OnDismissListener onDismissListener = new DialogInterface.OnDismissListener() {
		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub

		}
	};

	private void logoutGroupMap(boolean isSend) {
		// TODO Auto-generated method stub
		// 发包停止Service定位
		locateManager.disableMyLocation();
		if (isSend) {
			sendPack(Configuration.CROOM2SP_STATUS_REPORT_LOCAYION_DEL_FLAG);
		}
		PangkerManager.getTabBroadcastManager().removeUserListenter(this);
		finish();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		showTip();
	}

	private MessageTipDialog tipDialog;

	private void showTip() {
		// TODO Auto-generated method stub
		if (tipDialog == null) {
			tipDialog = new MessageTipDialog(resultCallback, this);
		}
		tipDialog.setButtonName(R.string.makefriend_confirmreg, R.string.makefriend_cancelreg);
		tipDialog.showDialog("退出后，是否需要清除您在群组地图中的位置信息？清除后别人将不能在群组地图中看到您！", false);
	}

	MessageTipDialog.DialogMsgCallback resultCallback = new DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			logoutGroupMap(isSubmit);
		}
	};

	public class UIUserInfo {

		private LinearLayout layoutInfo;
		private ImageView imgUserIcon;
		private TextView textViewLocation;
		private TextView txtName;
		private P2PUserInfo mP2PUserInfo;

		public UIUserInfo() {
			// TODO Auto-generated constructor stub
			layoutInfo = (LinearLayout) findViewById(R.id.ly_userinfo);
			textViewLocation = (TextView) findViewById(R.id.textViewLocation);
			imgUserIcon = (ImageView) findViewById(R.id.iv_usericon);
			txtName = (TextView) findViewById(R.id.trace_username);
		}

		public void showData(P2PUserInfo p2pUserInfo) {
			// TODO Auto-generated method stub
			this.mP2PUserInfo = p2pUserInfo;
			layoutInfo.setVisibility(View.VISIBLE);
			txtName.setText(p2pUserInfo.getUserName());
			mImageResizer.loadImage(String.valueOf(p2pUserInfo.getUID()), imgUserIcon);

			if (!Util.isEmpty(p2pUserInfo.getLocationInfo())) {
				poiaddressLoader.onPoiLoader(p2pUserInfo.getLocation(), GroupMapActivity.this, textViewLocation);
			} else {
				textViewLocation.setText("位置未知");
			}
		}

		public boolean isShowing() {
			// TODO Auto-generated method stub
			return layoutInfo.getVisibility() == View.VISIBLE;
		}

	}

	private void showMemberMap(List<P2PUserInfo> userInfos) {
		// TODO Auto-generated method stub
		mapUserInfos.clear();
		if (userInfos == null || userInfos.size() == 0) {
			return;
		}
		synchronized (mapUserInfos) {
			for (P2PUserInfo uInfo : userInfos) {
				if (!Util.isEmpty(uInfo.getLocationInfo())) {
					mapUserInfos.add(uInfo);
				}
			}
			mapUserAdapter.setMembers(mapUserInfos);
		}
		// >>>>>>>>>判断是否是第一次进入
		if (!uiUserInfo.isShowing()
				|| (uiUserInfo.isShowing() && !application.getCurrunGroup().isInMapMember(
						uiUserInfo.mP2PUserInfo.getUID()))) {
			GeoPoint geoPoint = MapUtil.toGeoPoint(mapUserInfos.get(0).getLocationInfo());
			setMapViewCenter(geoPoint);
			uiUserInfo.showData(mapUserInfos.get(0));
		}
		drawMapView(mapUserInfos);
	}

	/**
	 * // * 地图模式显示 // * @param userInfos //
	 */
	private void drawMapView(List<P2PUserInfo> userInfos) {
		// >>>>>>>情况所有的view
		mMapView.removeAllViews();
		CopiedIterator mcIterator = new CopiedIterator(userInfos.iterator());
		synchronized (mcIterator) {
			// >>>>>>>将用户绘制到地图上
			while (mcIterator.hasNext()) {
				P2PUserInfo info = (P2PUserInfo) mcIterator.next();
				Log.i(TAG, info.toString());
				GeoPoint geoPoint = MapUtil.toGeoPoint(info.getLocationInfo());
				OverUserItem overUserItem = new OverUserItem(this, geoPoint, String.valueOf(info.getUID()), mBitmap, 0);
				overUserItem.addToMap(mMapView);
				UserItem userItem = new UserItem();
				userItem.setUserName(info.getUserName());
				userItem.setUserId(String.valueOf(info.getUID()));
				overUserItem.setmOnClickListener(initClickListener(userItem));
			}
		}
	}

	OnItemClickListener itemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
			// 点击后要居中显示该用户头像>>>>>>>如果当前不在任何群组当中，
			if (application.getCurrunGroup() == null) {
				return;
			}
			P2PUserInfo p2pUserInfo = application.getCurrunGroup().getMaplist().get(i);
			// 如果该用户不在群组地图中
			if (!application.getCurrunGroup().isInMapMember(p2pUserInfo.getUID())) {
				showToast("还没有改用户的位置信息!");
				return;
			}

			GeoPoint centerPoint = MapUtil.toGeoPoint(p2pUserInfo.getLocationInfo());
			uiUserInfo.showData(p2pUserInfo);
			if (centerPoint != null) {
				if (mMapController != null) {
					mMapController.animateTo(centerPoint);
				}
			}
		}
	};

	/**
	 * TODO 地图模式每个用户头像添加监听 OnClickListener
	 * 
	 * @param userItem
	 * @return
	 */
	private final OnClickListener initClickListener(final UserItem userItem) {
		OnClickListener mOnClickListener = new OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				// 自己
				if (userItem.getUserId().equals(mMyUserID)) {
					showToast(R.string.isyourself);
				} else {
					if (view instanceof ImageView) {
						// /int position = ((Integer) view.getTag()).intValue();
						ActionItem actLookUserInfo = new ActionItem(getResources().getDrawable(
								R.drawable.btn_menu_info_selector), getResources().getString(R.string.userinfo), this);
						ActionItem actMessage = new ActionItem(getResources().getDrawable(
								R.drawable.btn_menu_talk_selector), getResources().getString(R.string.msg_talk), this);
						ActionItem actCall = new ActionItem(getResources().getDrawable(
								R.drawable.btn_menu_call_selector), getResources().getString(R.string.call), this);
						ActionItem actDrift = new ActionItem(getResources().getDrawable(
								R.drawable.btn_menu_follow_selector), getResources().getString(R.string.drift), this);
						ActionItem actTouch = new ActionItem(getResources().getDrawable(
								R.drawable.btn_menu_touch_selector), getResources().getString(R.string.touch), this);
						QuickActionBar qaBar = new QuickActionBar(view, 0);
						qaBar.setEnableActionsLayoutAnim(true);
						qaBar.addActionItem(actLookUserInfo);
						qaBar.addActionItem(actMessage);
						qaBar.addActionItem(actCall);
						qaBar.addActionItem(actDrift);
						qaBar.addActionItem(actTouch);
						qaBar.show();
					} else if (view instanceof LinearLayout) {
						// ActionItem组件
						LinearLayout actionsLayout = (LinearLayout) view;
						QuickActionBar bar = (QuickActionBar) actionsLayout.getTag();
						bar.dismissQuickActionBar();
						TextView txtView = (TextView) actionsLayout.findViewById(R.id.tv_title);
						String actionName = txtView.getText().toString();
						if (actionName.equals(getResources().getString(R.string.userinfo))) {
							showInfo(userItem);
						} else if (actionName.equals(getResources().getString(R.string.msg_talk))) {
							goToMessageTalk(userItem);
						} else if (actionName.equals(getResources().getString(R.string.call))) {
							callCheck(userItem);
						} else if (actionName.equals(getResources().getString(R.string.drift))) {
							driftToUserItem(userItem);
						} else if (actionName.equals(getResources().getString(R.string.touch))) {
							OpenfireManager openfireManager = application.getOpenfireManager();
							long nowtime = new Date().getTime();
							long touchtime = 0;
							if (nowtime - touchtime > 1000 * 10) {
								long messageID = insertChatMessage(userItem, getResourcesMessage(R.string.chat_touch),
										PangkerConstant.RES_TOUCH);
								openfireManager.sendTouchMessage(mMyUserID, userItem.getUserId(), messageID);
								touchtime = nowtime;
							} else {
								showToast(R.string.chatroom_touch_much);
							}
						}
					}
				}
			}
		};
		return mOnClickListener;
	}

	/**
	 * @author wangxin 2012-3-19 下午01:46:51
	 */
	public void showInfo(UserItem user) {
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, user.getUserId());
		intent.putExtra(UserItem.USERNAME, user.getUserName());
		intent.putExtra(UserItem.USERSIGN, user.getSign());
		startActivity(intent);
	}

	/**
	 * 私信会话(跳转)
	 * 
	 * @param intent
	 */
	private void goToMessageTalk(UserItem item) {
		Intent intent = new Intent();
		intent.putExtra(UserItem.USERID, item.getUserId());
		String username = item.getrName() != null ? item.getrName() : item.getUserName();
		intent.putExtra(UserItem.USERNAME, username);
		intent.putExtra(UserItem.USERSIGN, item.getSign());
		intent.setClass(this, MsgChatActivity.class);
		launch(intent, this);
	}

	/**
	 * 漂移
	 * 
	 * @author wubo
	 * @createtime 2012-5-23
	 * @param item
	 */
	private void driftToUserItem(final UserItem item) {
		if (application.getLocateType() == PangkerConstant.LOCATE_DRIFT) {
			UserItem userInfo = application.getDriftUser();
			if (item.getUserId().equals(userInfo.getUserId())) {
				showToast("您在漂移在Ta身边!");
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item);
						}
					}
				}, this);
				dialog.showDialog("您目前漂移在用户" + userInfo.getUserName() + "身边，确定要漂移到用户" + item.getUserName() + "身边？",
						false);
			}
		} else {
			SharedPreferencesUtil spu = new SharedPreferencesUtil(this);
			if (spu.getBoolean(PangkerConstant.SP_DRIFT_SET + application.getMyUserID(), false)) {
				locationCheck(item);
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item);
						}
					}
				}, this);
				dialog.setTextSize(17, 13);
				String data = getResources().getString(R.string.drift_show);
				data = String.format(data, item.getUserName());
				dialog.showDialog(MessageTipDialog.TYPE_DRIFT, data, getResourcesMessage(R.string.drift_tip), true);
			}
		}
	}

	/**
	 * 漂移 void TODO
	 * 
	 * @param userItem
	 */
	private void locationCheck(final UserItem userItem) {
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				if (!HttpResponseStatus(response))
					return;
				DriftGrantResult result = JSONUtil.fromJson(response.toString(), DriftGrantResult.class);
				if (result != null && result.getErrorCode() == 1) {
					// 变更漂移状态
					String data = getResources().getString(R.string.drift_show);
					data = String.format(data, userItem.getUserName());
					showToast(data);
					updateLoaction(userItem, new Location(result.getLon(), result.getLat()),
							PangkerConstant.LOCATE_DRIFT);
				} else if (result != null) {
					showToast(result.errorMessage);
				} else {
					showToast("漂移失败!");
				}
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userItem.getUserId()));
		paras.add(new Parameter("visituid", mMyUserID));
		serverMana
				.supportRequest(Configuration.getDriftCheck(), paras, true, getResourcesMessage(R.string.please_wait));
	}

	/**
	 * void TODO变更漂移状态
	 * 
	 * @param userItem
	 * @param location
	 * @param type
	 */
	private void updateLoaction(final UserItem userItem, Location location, int type) {
		// TODO Auto-generated method stub
		if (type == PangkerConstant.LOCATE_DRIFT) {
			application.setDriftUser(userItem);
		} else {
			application.setDriftUser(null);
		}
		application.setLocateType(type);// 变更漂移状态
		application.setDirftLocation(location);
		Message msg = new Message();
		msg.what = PangkerConstant.DRIFT_NOTICE;
		PangkerManager.getTabBroadcastManager().sendResultMessage(MainActivity.class.getName(), msg);
		PangkerManager.getTabBroadcastManager().sendResultMessage(HomeActivity.class.getName(), msg);
		if (type == PangkerConstant.LOCATE_CLIENT) {
			uploadLocation(application.getCurrentLocation());
		} else {
			uploadLocation(application.getDirftLocation());
		}
		Intent intent = new Intent(this, PangkerService.class);
		intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DRIFT_START);
		startService(intent);
	}

	/**
	 * 上传用户位置信息
	 * 
	 */
	public void uploadLocation(com.wachoo.pangker.entity.Location location) {
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", mMyUserID));
		paras.add(new Parameter("lon", String.valueOf(location.getLongitude())));
		paras.add(new Parameter("lat", String.valueOf(location.getLatitude())));
		serverMana.supportRequest(Configuration.getUpLocation(), paras);
	}

	/**
	 * 进入私聊界面 boolean TODO
	 * 
	 * @param userItem
	 * @param chatmessage
	 * @param type
	 * @return
	 */
	private long insertChatMessage(UserItem userItem, String chatmessage, int type) {
		final ChatMessage msg = new ChatMessage();
		msg.setTime(Util.getSysNowTime()); // 手机当前时间
		msg.setUserId(userItem.getUserId()); // 目标用户UserID
		msg.setContent(chatmessage);// 内容
		msg.setUserName(userItem.getUserName());
		msg.setMyUserId(mMyUserID);// 当前用户userid
		msg.setDirection(ChatMessage.MSG_FROM_ME);// 方向
		msg.setMsgType(type);
		long messageID = userMsgDao.saveUserMsg(msg);
		if (userMsgDao == null) {
			userMsgDao = new UserMsgDaoImpl(this);
		}
		if (messageID > 0) {
			application.getMsgInfoBroadcast().sendNoticeMsg(msg);
			return messageID;
		}
		return messageID;
	}

	/**
	 * 呼叫校验 void
	 */
	public void callCheck(final UserItem userItem) {
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				if (!HttpResponseStatus(response))
					return;
				CallMeSetResult resultCall = JSONUtil.fromJson(response.toString(), CallMeSetResult.class);
				if (resultCall != null && resultCall.getErrorCode() == BaseResult.SUCCESS) {
					application.getMsgInfoBroadcast().sendCallMsg(userItem.getUserId(), userItem.getAvailableName());
					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel://" + resultCall.getMobile()));
					startActivity(intent);
				} else if (resultCall != null
						&& (resultCall.getErrorCode() == BaseResult.FAILED || resultCall.getErrorCode() == 2)) {
					showToast(resultCall.getErrorMessage());
				} else
					showToast(R.string.return_value_999);
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userItem.getUserId()));
		paras.add(new Parameter("visituid", mMyUserID));
		paras.add(new Parameter("password", ""));
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getCallCheck(), paras, true, mess);
	}

	/**
	 * 设置地图的中心点位置
	 * 
	 * @param geoPoint
	 */
	private void setMapViewCenter(GeoPoint geoPoint) {
		try {
			if (mMapController != null && geoPoint != null) {
				mMapController.animateTo(geoPoint);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if (application.getCurrunGroup().isJoin()) {
			outState.putBoolean("isJoin", true);
		}
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		mapHandler.sendMessage(msg);
	}
}
