package com.wachoo.pangker.activity.group;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.os.Handler;
import android.text.ClipboardManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.CommentAdapter;
import com.wachoo.pangker.adapter.NoCommentAdapter;
import com.wachoo.pangker.db.IUserGroupDao;
import com.wachoo.pangker.db.impl.UserGroupDaoIpml;
import com.wachoo.pangker.entity.MessageTip;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.gestures.GestureCommandRegister;
import com.wachoo.pangker.gestures.GestureListener;
import com.wachoo.pangker.gestures.NextCommend;
import com.wachoo.pangker.gestures.PrevCommend;
import com.wachoo.pangker.group.P2PFarther;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CommentResult;
import com.wachoo.pangker.server.response.Commentinfo;
import com.wachoo.pangker.server.response.GroupEnterCheckResult;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.PangkerGroupInfo;
import com.wachoo.pangker.server.response.QueryGroupsByIDResult;
import com.wachoo.pangker.server.response.ResShareResult;
import com.wachoo.pangker.server.response.SearchCommentsResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.CommentBar;
import com.wachoo.pangker.ui.EditTextKeyborad;
import com.wachoo.pangker.ui.EditTextKeyborad.IKeyboardListener;
import com.wachoo.pangker.ui.FooterView.OnLoadMoreListener;
import com.wachoo.pangker.ui.KeyboardLayout;
import com.wachoo.pangker.ui.ListLinearLayout;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.PasswordDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @author wubo
 * @createtime 2012-3-14
 */
public class ChatRoomInfoActivity extends CommonPopActivity implements IUICallBackInterface, GestureListener {

	private ImageResizer mImageWorker;
	private PKIconResizer mImageResizer;

	protected ViewFlipper mViewFlipper;
	protected GestureOverlayView mGestureOverlayView;
	protected ViewFlipper pViewFlipper;

	private ImageView userIcon;
	private TextView username;
	private TextView name;
	private TextView type;
	private TextView desc;
	private TextView tv_qrouplabel;
	private TextView tv_uploadtime;
	private Button btnFavorite;
	private Button btnMenu;
	private QuickAction quickAction;
	private Button btn_forward;
	private ImageView groupIcon;
	// private Button btnReply;
	public UIComment uiComment;
	// 转播人的信息
	private ImageView shareUserIcon;
	private TextView txtShareUsername;
	private TextView txtShareTime;

	protected UserInfo userInfo;
	private String userId;
	String sid = "00";
	private final int LOAD_FLAG = 0x10;
	private final int ADD_FLAG = 0x11;
	private final int FAVORITE_FLAG = 0x14;
	private final int SEARCH_REPLY = 0x16;
	private final int SUBMIT_COMMENT = 0x17;
	private final int DEL_COMMENT = 0x18;
	private final int KEY_TRANS = 0x19;
	private PangkerGroupInfo groupInfo;

	private List<GroupLbsInfo> mGroupInfos;

	P2PFarther farther = new P2PFarther();
	byte[] result = null;// 发送到sp的数据

	ServerSupportManager serverMana;
	IUserGroupDao userGroupDao;
	UserGroup userGroup;
	PangkerApplication application;

	// >>>>>>>>
	private Button btn_jiongroup;// 加入群组
	private TextView mGroupMemberCount;// 当前人数
	private TextView mGroupTotalVisits;// 总访问次数
	private TextView mFavoriteTimes;// 收藏次数
	private TextView mForwardTimes;// 转播次数
	private TextView mGroupChannel;

	private Button mBtnRecommend;// 推荐按钮
	private Button mBtnGroupShare;// >>>>>>群分享
	protected int pageIndex = 0;// 评论的起始位置，在手势滑动之后要=0；
	protected int index;// 当前显示的index；
	protected KeyboardLayout mKeyboardLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.net_group_info);
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		mImageWorker = PangkerManager.getGroupIconResizer(getApplicationContext());

		init();
		initView();
		initGesture();
		initData();
	}

	protected void softKeyboardListener() {
		mKeyboardLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				// >>>>>>如果是隐藏设置为隐藏
				uiComment.setCommentTypeOnlyComment();
			}
		});

	}

	private String getGrouInfoId() {
		return String.valueOf(mGroupInfos.get(index).getSid());
	}

	private GroupLbsInfo getNowGroupLbsInfo() {
		return mGroupInfos.get(index);
	}

	private void init() {
		// TODO Auto-generated method stub
		application = ((PangkerApplication) this.getApplicationContext());
		userId = ((PangkerApplication) this.getApplicationContext()).getMySelf().getUserId();
		mGroupInfos = (List<GroupLbsInfo>) getIntent().getSerializableExtra("GroupInfo");
		index = getIntent().getIntExtra("group_index", 0);
		userGroupDao = new UserGroupDaoIpml(this);
		userInfo = application.getMySelf();
		serverMana = new ServerSupportManager(this, this);
	}

	private void initView() {
		mKeyboardLayout = (KeyboardLayout) findViewById(R.id.kl_group_info);
		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("群组首页");
		// >>>>>>>>>创建人用户信息
		userIcon = (ImageView) findViewById(R.id.iv_res_ownericon);
		username = (TextView) findViewById(R.id.tv_res_ownername);

		// >>>>>>>群组信息
		mGroupMemberCount = (TextView) findViewById(R.id.tv_group_members_count);
		mGroupTotalVisits = (TextView) findViewById(R.id.tv_group_totalvisits);
		mFavoriteTimes = (TextView) findViewById(R.id.tv_favoritetimes);
		mForwardTimes = (TextView) findViewById(R.id.tv_forwardtimes);
		mGroupChannel = (TextView) findViewById(R.id.tv_group_channel);
		// >>>>>>>>>>推荐按钮
		mBtnRecommend = (Button) findViewById(R.id.btn_recommend);
		mBtnRecommend.setOnClickListener(clickListener);
		mBtnGroupShare = (Button) findViewById(R.id.btn_group_share);
		mBtnGroupShare.setOnClickListener(clickListener);
		tv_uploadtime = (TextView) findViewById(R.id.tv_uploadtime);
		name = (TextView) findViewById(R.id.name);
		type = (TextView) findViewById(R.id.type);
		desc = (TextView) findViewById(R.id.desc);
		desc.setOnLongClickListener(descLongClickListener);
		tv_qrouplabel = (TextView) findViewById(R.id.tv_qrouplabel);

		groupIcon = (ImageView) findViewById(R.id.groupicon);

		btn_jiongroup = (Button) findViewById(R.id.btn_jiongroup);
		btn_jiongroup.setOnClickListener(clickListener);
		if (getIntent().getIntExtra("group_type", 0) == 1) {
			btn_jiongroup.setBackgroundResource(R.drawable.form_vipinf_but_enter_p);
			btn_jiongroup.setEnabled(false);
		}
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideSoftInput(v);
				ChatRoomInfoActivity.this.finish();
			}
		});
		btnMenu = (Button) findViewById(R.id.iv_more);
		btnMenu.setBackgroundResource(R.drawable.btn_menu_bg);
		btnFavorite = (Button) findViewById(R.id.btn_favorite);
		btn_forward = (Button) findViewById(R.id.btn_forward);

		uiComment = new UIComment();

		btnMenu.setOnClickListener(clickListener);
		btnFavorite.setOnClickListener(clickListener);
		btn_forward.setOnClickListener(clickListener);
		userIcon.setOnClickListener(clickListener);

		shareUserIcon = (ImageView) findViewById(R.id.iv_res_broadcastericon);
		shareUserIcon.setOnClickListener(clickListener);
		txtShareUsername = (TextView) findViewById(R.id.tv_res_boradcastername);
		txtShareTime = (TextView) findViewById(R.id.tv_boradcaster_uploadtime);
		softKeyboardListener();
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 1) {
				Intent intent = new Intent();
				intent.putExtra("UserGroup", userGroup);
				intent.setClass(ChatRoomInfoActivity.this, GroupShareActivity.class);
				startActivity(intent);
			} else if (actionId == 2) {
				Intent mUpdateIntent = new Intent();
				mUpdateIntent.putExtra("flag", "1");
				mUpdateIntent.putExtra("groupId", userGroup.getGroupId());
				mUpdateIntent.setClass(ChatRoomInfoActivity.this, ChatRoomCreateActivity.class);
				startActivity(mUpdateIntent);
			}
		}
	};

	private void initGesture() {
		// TODO Auto-generated method stub
		GestureCommandRegister register = new GestureCommandRegister();
		mViewFlipper = (ViewFlipper) findViewById(R.id.DownloadViewFlipper);
		mGestureOverlayView = (GestureOverlayView) findViewById(R.id.gestures);

		register.registerCommand("next", new NextCommend(this));
		register.registerCommand("prev", new PrevCommend(this));
		application.getGestureHandler().setRegister(register);

		mGestureOverlayView.addOnGesturePerformedListener(application.getGestureHandler());
		mViewFlipper.setDisplayedChild(0);
	}

	private void showDatainfo() {
		// TODO Auto-generated method stub
		name.setText(groupInfo.getGroupName());
		desc.setText(groupInfo.getGroupDesc());

		tv_uploadtime.setText("创建:" + Util.getShowTimes(groupInfo.getCreateTime()));
		// 获取群组标签
		tv_qrouplabel.setText(groupInfo.getGroupLabel());
		// >>>群组当前人数
		StringBuffer membersBuffer = new StringBuffer(getString(R.string.groupinfo_currentmember_counts));
		membersBuffer.append(groupInfo.getOnlineNum());
		membersBuffer.append(getString(R.string.groupinfo_hum));
		mGroupMemberCount.setText(membersBuffer.toString());
		// >>>群组通道模式
		String channel = getResources().getString(R.string.chatroom_channel);
		channel = String.format(channel, groupInfo.getVoiceChannelType());
		mGroupChannel.setText(channel);
		// >>>群组总访问人数
		StringBuffer visitsBuffer = new StringBuffer(getString(R.string.groupinfo_totalvisits_counts));
		// >>>>>>>等待后台追加字段后修改
		visitsBuffer.append(groupInfo.getVisitUsers());
		visitsBuffer.append(getString(R.string.groupinfo_hums));
		mGroupTotalVisits.setText(visitsBuffer.toString());
		// >>>>>>>>收藏次数
		mFavoriteTimes.setText("(" + groupInfo.getFavorTimes() + ")");
		// >>>>>>>转播次数
		mForwardTimes.setText("(" + groupInfo.getShareTimes() + ")");
		sid = String.valueOf(groupInfo.getGroupSid());
		if (groupInfo.getIfFavor() == 1) {
			btnFavorite.setEnabled(false);
			btnFavorite.setBackgroundResource(R.drawable.form_vipinf_but_keep_c);
		}
		if (groupInfo.getIfShareGroup() == 1) {
			btn_forward.setBackgroundResource(R.drawable.form_vipinf_but_relay_c);
			btn_forward.setEnabled(false);
		}

		if (groupInfo.getCategory() != null) {
			String[] MENU_INDEXS = getResources().getStringArray(R.array.group_category_id);
			String[] MENU_TITLE = getResources().getStringArray(R.array.group_category);
			for (int i = 0; i < MENU_INDEXS.length; i++) {
				if (MENU_INDEXS[i].equals(groupInfo.getCategory().toString())) {
					type.setText(getString(R.string.groupinfo_group_type) + MENU_TITLE[i]);
				}
			}
		}
		mImageResizer.loadImage(groupInfo.getUid().toString(), userIcon);
		username.setText(groupInfo.getUserName());
		// 如果分享人不是群主创建人，那么分享人就是转播的，显示转播人的用户信息
		if (!getNowGroupLbsInfo().getShareUid().equals(String.valueOf(groupInfo.getUid()))) {
			findViewById(R.id.ly_broadcast).setVisibility(View.VISIBLE);
			mImageResizer.loadImage(getNowGroupLbsInfo().getShareUid(), shareUserIcon);
			txtShareUsername.setText(groupInfo.getReshareUserName());
			if (groupInfo.getReShareTimes() != null) {
				txtShareTime.setText(Util.TimeIntervalBtwNow(groupInfo.getReShareTimes()) + "转播");
			}
		} else {
			findViewById(R.id.ly_broadcast).setVisibility(View.GONE);
		}
		if (groupInfo.getUid().toString().equals(userId)) {
			quickAction = new QuickAction(this, QuickAction.VERTICAL);
			quickAction.setOnActionItemClickListener(onActionItemClickListener);
			quickAction.addActionItem(new ActionItem(2, "群组设置"), true);
		} else {
			btnMenu.setVisibility(View.GONE);
		}
	}

	// 初步显示 部分信息
	private void initData() {
		// TODO Auto-generated method stub
		name.setText(getNowGroupLbsInfo().getgName());
		tv_qrouplabel.setText(getNowGroupLbsInfo().getTags());

		final String groupUrl = Configuration.getGroupCover() + getGrouInfoId();
		mImageWorker.setLoadingImage(R.drawable.group_icon);
		mImageWorker.loadImage(groupUrl, groupIcon, getGrouInfoId());

		searchGroupInfo();
		uiComment.showComment();
	}

	private PopMenuDialog descDialog;
	ActionItem[] msgMenu1 = new ActionItem[] { new ActionItem(1, "复制") };

	private void showDescMenu() {
		// TODO Auto-generated method stub
		if (Util.isEmpty(groupInfo.getGroupDesc())) {
			return;
		}
		if (descDialog == null) {
			descDialog = new PopMenuDialog(this);
			descDialog.setListener(descClickListener);
			descDialog.setTitle("描述信息");
			descDialog.setMenus(msgMenu1);
		}
		descDialog.show();
	}

	UITableView.ClickListener descClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			if (actionid == 1) {
				String edsc = groupInfo.getGroupDesc();
				if (!Util.isEmpty(edsc)) {
					ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
					clip.setText(edsc);
					showToast("复制成功");
				}
			}
			descDialog.dismiss();
		}
	};

	// 显示复制描述的功能
	protected View.OnLongClickListener descLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			showDescMenu();
			return true;
		}
	};

	OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (btn_jiongroup == v) {
				jioninGroup();
			} else if (v == btnFavorite) {
				toFavorite();
			} else if (v == btnMenu) {
				quickAction.show(btnMenu);
			}
			// >>>>>>>>>>推荐功能实现
			else if (v == mBtnRecommend) {
				Intent intent = new Intent(ChatRoomInfoActivity.this, ContactsSelectActivity.class);
				intent.putExtra(ContactsSelectActivity.INTENT_SELECT, ContactsSelectActivity.TYPE_RES_RECOMMEND);
				ResShowEntity resShowEntity = new ResShowEntity();
				resShowEntity.setResID(Util.String2Long(userGroup.getGroupId()));
				resShowEntity.setResName(userGroup.getGroupName());
				resShowEntity.setResType(PangkerConstant.RES_BROADCAST);
				resShowEntity.setShareUid(getNowGroupLbsInfo().getShareUid());
				intent.putExtra(ResShowEntity.RES_ENTITY, resShowEntity);
				startActivity(intent);
				return;
			} else if (v == btn_forward) {
				transGroup();
			} else if (v == userIcon) {
				Intent intent = new Intent();
				intent.putExtra(UserItem.USERID, String.valueOf(getNowGroupLbsInfo().getUid()));
				intent.putExtra(UserItem.USERNAME, getNowGroupLbsInfo().getgName());
				intent.setClass(ChatRoomInfoActivity.this, UserWebSideActivity.class);
				startActivity(intent);
			} else if (v == mBtnGroupShare) {
				Intent intent = new Intent();
				intent.putExtra("UserGroup", userGroup);
				intent.setClass(ChatRoomInfoActivity.this, GroupShareActivity.class);
				startActivity(intent);
			}
		}
	};

	// 资源转播
	private void transGroup() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("resid", getGrouInfoId()));
		paras.add(new Parameter("optype", "2"));
		paras.add(new Parameter("type", "2"));
		serverMana.supportRequest(Configuration.getResShare(), paras, true, "正在发送请求...", KEY_TRANS);
	}

	private void searchComment() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("resid", getGrouInfoId()));
		paras.add(new Parameter("type", "2"));
		paras.add(new Parameter("limit", uiComment.adapter.getCount() + ",10"));
		serverMana.supportRequest(Configuration.getSearchComments(), paras, SEARCH_REPLY);
	}

	/**
	 * 加载群信息
	 * 
	 * @author wubo
	 * @createtime 2012-3-15
	 */
	public void searchGroupInfo() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", userId));
		paras.add(new Parameter("groupid", getGrouInfoId()));
		paras.add(new Parameter("shareUid", getNowGroupLbsInfo().getShareUid()));
		serverMana.supportRequest(Configuration.getQueryGroupsByID(), paras, true, "加载群组信息中...", LOAD_FLAG);
	}

	/**
	 * 资源评论接口
	 * 
	 * @param content
	 *            评论内容 请求数目 接口：AddComment.json
	 */
	protected void submitCooment(Commentinfo commentinfo, String restype) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", commentinfo.getUserid()));

		// >>>>当前用户名称
		paras.add(new Parameter("username", commentinfo.getUsername()));
		paras.add(new Parameter("commenttype", String.valueOf(commentinfo.getCommenttype())));
		// >>>>>>>>>>判断是否是回复case
		if (commentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_REPLY) {
			// >>>>>回复的用户ID
			paras.add(new Parameter("replyuserid", commentinfo.getReplyuserid()));
			paras.add(new Parameter("replyusername", commentinfo.getReplyusername()));
			// >>>>>回复的评论ID
			paras.add(new Parameter("commentid", commentinfo.getReplycommentid()));
		}
		paras.add(new Parameter("resid", getGrouInfoId()));
		paras.add(new Parameter("type", restype));// 操作id
		paras.add(new Parameter("content", commentinfo.getContent()));// 回答内容
		serverMana.supportRequest(Configuration.getAddComment(), paras, true, "正在提交...", SUBMIT_COMMENT);
	}

	private void delComment(String commentId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("commentid", commentId));// 问题id
		paras.add(new Parameter("type", "2"));// 操作id
		serverMana.supportRequest(Configuration.getDeleteCommenturl(), paras, true, "正在提交...", DEL_COMMENT);
	}

	/**
	 * 加入群
	 * 
	 * @author wubo
	 * @createtime 2012-3-15
	 */
	public void jioninGroup() {
		if (groupInfo == null) {
			showToast("群组信息加载失败,无法进入!");
			return;
		}
		groupInfo.getLastVisitTime();
		if (application.getCurrunGroup() != null && application.getCurrunGroup().getUid() != null
				&& !application.getCurrunGroup().getGroupId().equals(getGrouInfoId())) {

			MessageTipDialog mDialog = new MessageTipDialog(new DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-genaerated method stub
					if (isSubmit) {
						PangkerManager.getActivityStackManager().popOneActivity(ChatRoomMainActivity.class);
						intoGroup();
					}
				}
			}, this);
			mDialog.showDialog("您已经在一个群组里面,进入此群组会退出当前所在的群组,是否确认进入?", false);
		} else {
			intoGroup();
		}
	}

	PasswordDialog mEditTextDialog;

	private void showPasswordDialog() {
		// TODO Auto-generated method stub
		if (mEditTextDialog == null) {
			mEditTextDialog = new PasswordDialog(this);
			mEditTextDialog.setResultCallback(resultCallback);
		}
		mEditTextDialog.setTitle("请输入密码");
		mEditTextDialog.showDialog();
	}

	MessageTipDialog.DialogMsgCallback resultCallback = new MessageTipDialog.DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if (isSubmit) {
				if (mEditTextDialog.checkPass()) {
					checkIntoGroup(mEditTextDialog.getPassword());
				} else {
					showToast("您输入正确的密码有误!");
				}
			}
		}
	};

	private void intoGroup() {
		//设置了密码，同时并非群组创建人
		if (groupInfo.getAccessType() == 14 && !userId.equals(groupInfo.getUid().toString())) {
			showPasswordDialog();
			return;
		}
		checkIntoGroup(null);
	}

	private void checkIntoGroup(String password) {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", userId));
		paras.add(new Parameter("groupid", getGrouInfoId()));
		// 群组权限0：需要权限验证进入 不等于0：无需权限
		paras.add(new Parameter("optype", "0"));
		if (!Util.isEmpty(password)) {
			paras.add(new Parameter("password", password));
		}
		serverMana.supportRequest(Configuration.getGroupEnterCheck(), paras, true, "正在进入群组...", ADD_FLAG);
	}

	/**
	 * 收藏群
	 * 
	 * @author wubo
	 * @createtime 2012-3-15
	 */
	public void toFavorite() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", getGrouInfoId()));
		paras.add(new Parameter("shareid", sid));
		paras.add(new Parameter("optype", "0"));// 0收藏 1取消收藏
		paras.add(new Parameter("type", "2"));// 0:问答,1:资源,2:群组
		serverMana.supportRequest(Configuration.getCollectResManager(), paras, FAVORITE_FLAG);
	}

	private void saveGroupInfo(PangkerGroupInfo groupInfo) {
		userGroup = new UserGroup();
		userGroup.setGroupId(String.valueOf(groupInfo.getGroupId()));
		userGroup.setGroupSid(String.valueOf(groupInfo.getGroupSid()));
		userGroup.setGroupName(groupInfo.getGroupName());
		userGroup.setGroupLabel(groupInfo.getGroupLabel());
		userGroup.setLotype(String.valueOf(groupInfo.getLoType()));
		userGroup.setUid(String.valueOf(groupInfo.getUid()));
		userGroup.setGroupType(String.valueOf(groupInfo.getGroupType()));
		userGroup.setCreateTime(groupInfo.getCreateTime());
		userGroup.setVoiceChannelType(groupInfo.getVoiceChannelType());
		userGroup.setMaxSpeechTime(groupInfo.getMaxSpeechTime());
		userGroup.setGroupFlag(2);
		userGroupDao.updateGroup(userGroup);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			if (caseKey == SEARCH_REPLY) {
				if (uiComment.adapter.getCount() == 0) {
					uiComment.serachCommentFail();
				} else {
					uiComment.commentList.setShowLoadmore(true);
				}
			}
			return;
		}

		if (caseKey == LOAD_FLAG) {
			QueryGroupsByIDResult result = JSONUtil.fromJson(response.toString(), QueryGroupsByIDResult.class);
			if (result != null && result.getErrorCode() != 999) {
				if (result.getErrorCode() == BaseResult.SUCCESS) {
					groupInfo = result.getGroupinfo();
					saveGroupInfo(groupInfo);
					showDatainfo();
				} else if (result.getErrorCode() == BaseResult.FAILED) {
					showToast(result.getErrorMessage());
					finish();
				} else {
					showToast(R.string.to_server_fail);
					finish();
				}
			} else {
				showToast(R.string.to_server_fail);
				finish();
			}

		} else if (caseKey == ADD_FLAG) {
			GroupEnterCheckResult result = JSONUtil.fromJson(response.toString(), GroupEnterCheckResult.class);
			// 0：无权限，1：具备权限，2：需要提供密码……
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				userGroup.setGroupFlag(2);
				userGroup.setVoiceChannelType(result.getUserGroup().getVoiceChannelType());
				userGroup.setMaxSpeechTime(result.getUserGroup().getMaxSpeechTime());
				userGroup.setLastVisitTime(Util.date2Str(new Date()));
				userGroupDao.addGroup(userGroup);

				// >>>>>wangxin add 进入群组
				Intent intent = new Intent();
				intent.putExtra(ChatRoomMainActivity.JION_TO_GROUP, userGroup);
				intent.setClass(ChatRoomInfoActivity.this, ChatRoomMainActivity.class);
				startActivity(intent);
				// 将进入群组的消息纪律到MsgInfo中
				application.getMsgInfoBroadcast().sendMeetroom(userGroup);
				// >>>>>>>>如果进入群组成功，关闭该界面
				finish();
			} else if (result != null) {
				showToast(result.getErrorMessage());
			} else {
				showToast(R.string.return_value_999);
			}
		} else if (caseKey == FAVORITE_FLAG) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() != 999) {
				showToast(result.getErrorMessage());
				if (result.getErrorCode() == BaseResult.SUCCESS) {
					userGroup.setGroupFlag(1);
					userGroupDao.addGroup(userGroup);
					btnFavorite.setEnabled(false);
					btnFavorite.setBackgroundResource(R.drawable.form_vipinf_but_keep_c);
					mFavoriteTimes.setText(timesText(String.valueOf(groupInfo.getFavorTimes() + 1)));
				}
			} else {
				showToast(R.string.return_value_999);
			}
		} else if (caseKey == SEARCH_REPLY) {
			SearchCommentsResult commentResult = JSONUtil.fromJson(response.toString(), SearchCommentsResult.class);
			if (commentResult != null && commentResult.errorCode == BaseResult.SUCCESS) {
				uiComment.showComments(commentResult);
			}
		} else if (caseKey == SUBMIT_COMMENT) {
			CommentResult submitResult = JSONUtil.fromJson(response.toString(), CommentResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				showToast(submitResult.errorMessage);

				uiComment.etContent.setText("");
				uiComment.getNewCommentinfo().setCommentId(String.valueOf(submitResult.getCommentId()));
				// >>>>>不需要再次获取
				uiComment.addComment(uiComment.getNewCommentinfo());
				Commentinfo commentinfo = uiComment.getNewCommentinfo();
				if (commentinfo != null) {

					String commentReplyUserID = null;
					// >>>>二、对其他评论的评论
					if (commentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_REPLY) {
						commentReplyUserID = commentinfo.getReplyuserid();
						sendMessTip(commentinfo, commentReplyUserID, MessageTip.TIP_COMMENT_REPLY);
					}
					// >>>发送给转播人
					if (!getNowGroupLbsInfo().getShareUid().equals(String.valueOf(groupInfo.getUid()))) {
						if (!userId.equals(getNowGroupLbsInfo().getShareUid())
								&& !getNowGroupLbsInfo().getShareUid().equals(commentReplyUserID))
							sendMessTip(commentinfo, getNowGroupLbsInfo().getShareUid(),
									MessageTip.TIP_COMMENT_BROADCAST_RES);
					}

					// >>>>>>发送给资源上传用户
					if (!userId.equals(String.valueOf(groupInfo.getUid()))
							&& !String.valueOf(groupInfo.getUid()).equals(commentReplyUserID))
						sendMessTip(commentinfo, String.valueOf(groupInfo.getUid()), MessageTip.TIP_COMMENT_RES);

				}
			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
		} else if (caseKey == DEL_COMMENT) {
			BaseResult submitResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				uiComment.delOneComment();
				showToast(submitResult.errorMessage);
			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
		} else if (caseKey == KEY_TRANS) {
			ResShareResult result = JSONUtil.fromJson(response.toString(), ResShareResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showToast("转播成功!");
				mForwardTimes.setText(timesText(String.valueOf(groupInfo.getShareTimes() + 1)));
				btn_forward.setBackgroundResource(R.drawable.form_vipinf_but_relay_c);
				btn_forward.setEnabled(false);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}

	}

	protected void sendMessTip(Commentinfo commentinfo, String userID, int messageTipType) {
		// TODO Auto-generated method stub
		final MessageTip messageTip = new MessageTip();
		messageTip.setCommentID(commentinfo.getCommentId());
		messageTip.setUserId(userID);
		messageTip.setFromId(userId);
		messageTip.setFromName(application.getMyUserName());
		messageTip.setResId(getGrouInfoId());
		messageTip.setResType(PangkerConstant.RES_BROADCAST);
		messageTip.setResName(getNowGroupLbsInfo().getgName());
		messageTip.setTipType(messageTipType);
		messageTip.setContent(commentinfo.getContent());
		messageTip.setCreateTime(Util.getSysNowTime());
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				application.getOpenfireManager().sendMessageTip(messageTip);
			}
		}).start();
	}

	protected String timesText(String times) {
		return "(" + times + ")";
	}

	public class UIComment extends CommentBar implements View.OnClickListener, UITableView.ClickListener,
			OnLongClickListener {

		private Commentinfo commentinfo;
		private CommentAdapter adapter;
		NoCommentAdapter adapter2;
		private TextView txtCount;
		private TextView txtLoad;
		private EditTextKeyborad etContent;
		private ListLinearLayout commentList;
		private LinearLayout comment_layout;
		private Button btn_sendComment;
		private Handler handler = new Handler();

		public UIComment() {
			// TODO Auto-generated constructor stub
			super(ChatRoomInfoActivity.this);
			setClickListener(this);
			setuITableListener(this);
			setLongClickListener(this);

			txtCount = (TextView) findViewById(R.id.txt_count);
			txtLoad = (TextView) findViewById(R.id.txt_load);
			etContent = (EditTextKeyborad) findViewById(R.id.et_foot_editer);
			commentList = (ListLinearLayout) findViewById(R.id.comment_view);
			comment_layout = (LinearLayout) findViewById(R.id.comment_layout);
			btn_sendComment = (Button) findViewById(R.id.btn_foot_pubcomment);
			etContent.setKeyboardListener(keyboardListener);
			commentList.setOnLongClickListener(this);
			commentList.setClickable(false);
			commentList.setFocusable(false);
			commentList.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (adapter.getCount() == 0) {
						return;
					}
					commentinfo = (Commentinfo) v.getTag();

					// >>>>>>>如果是自己的不需要做任何动作
					if (!commentinfo.getUserid().equals(userId)) {
						etContent.requestFocus();
						popSoftKeyboard();
						setCommentType(Commentinfo.COMMENT_TYPE_REPLY);
						setEtCommentHint("回复" + commentinfo.getUsername());
						setReplyCommentinfo(commentinfo);
					} else
						showToast("这是您自己的留言");
				}
			});

			btn_sendComment.setOnClickListener(this);
			adapter = new CommentAdapter(ChatRoomInfoActivity.this, new ArrayList<Commentinfo>());
			adapter2 = new NoCommentAdapter(ChatRoomInfoActivity.this);
		}

		private void showComment() {
			// TODO Auto-generated method stub
			adapter.reset();
			comment_layout.setVisibility(View.GONE);
			txtLoad.setVisibility(View.VISIBLE);
			searchComment();
		}

		public void serachCommentFail() {
			// TODO Auto-generated method stub
			txtLoad.setText("评论加载失败!");
		}

		private OnLoadMoreListener onLoadMoreListener = new OnLoadMoreListener() {
			@Override
			public void onLoadMoreListener() {
				// TODO Auto-generated method stub
				searchComment();
			}
		};

		public void showComments(SearchCommentsResult commentResult) {
			txtLoad.setVisibility(View.GONE);
			comment_layout.setVisibility(View.VISIBLE);
			if (commentResult != null && commentResult.errorCode == BaseResult.SUCCESS) {
				if (commentResult.getCommentinfo() != null && commentResult.getCommentinfo().size() > 0) {

					txtCount.setText(String.valueOf(commentResult.getSumCount()));
					adapter.addCommentinfos(commentResult.getCommentinfo());
					if (commentResult.getSumCount() > commentResult.getCount() + commentList.getCount()) {
						commentList.setShowLoadmore(true);
						commentList.setOnLoadMoreListener(onLoadMoreListener);
					} else {
						commentList.setShowLoadmore(false);
					}
					commentList.setAdapter(adapter);
				} else {
					txtCount.setText("0");
					commentList.setShowLoadmore(false);
					commentList.setAdapter(adapter2);
				}
			} else {
				txtCount.setText("0");
				commentList.setShowLoadmore(false);
				commentList.setAdapter(adapter2);
			}
		}

		private void delOneComment() {
			adapter.delComment(commentinfo);
			if (adapter.getCount() > 0) {
				comment_layout.setVisibility(View.VISIBLE);
				txtLoad.setVisibility(View.GONE);
				commentList.setAdapter(adapter);

			} else {
				txtLoad.setVisibility(View.GONE);
				commentList.setAdapter(adapter2);
				commentList.setShowLoadmore(false);
			}
			commentCountDel();
		}

		// >>>>>>>>>增加评论信息
		public void addComment(final Commentinfo commentinfo) {
			handler.post(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					adapter.addComment(commentinfo);
					comment_layout.setVisibility(View.VISIBLE);
					commentList.setAdapter(adapter);
					commentCountAdd();
				}
			});

		}

		private void commentCountAdd() {
			int commentCount = Util.String2Integer(txtCount.getText().toString()) + 1;
			txtCount.setText(String.valueOf(commentCount));
		}

		private void commentCountDel() {
			int commentCount = Util.String2Integer(txtCount.getText().toString()) - 1;
			if (commentCount > 0)
				txtCount.setText(String.valueOf(commentCount));
			else
				txtCount.setText("0");
		}

		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			if (adapter.getCount() == 0) {
				return false;
			}
			commentinfo = (Commentinfo) v.getTag();
			// >>>>>>如果是自己的评论 或者是自己上传的资源
			if (groupInfo != null && userId.equals(String.valueOf(groupInfo.getUid()))) {
				showCopyAndDeleteMenu();
			} else {
				showCopyMenu();
			}
			return false;
		}

		@Override
		public void onClick(int actionId) {
			// TODO Auto-generated method stub
			// >>>>>>>复制评论操作
			if (actionId == 1) {
				ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clip.setText(commentinfo.getContent());
			}
			// >>>>>>>>删除评论操作
			if (actionId == 2) {
				delComment(commentinfo.getCommentId());
			}
			menuDialog.dismiss();
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btn_sendComment) {
				// TODO Auto-generated method stub
				String comment = etContent.getText().toString();
				if (!Util.isEmpty(comment)) {
					this.newCommentinfo = new Commentinfo();
					this.newCommentinfo.setCommenttype(getCommentType());
					this.newCommentinfo.setContent(comment);
					this.newCommentinfo.setUserid(userId);
					this.newCommentinfo.setUsername(application.getMyUserName());
					this.newCommentinfo.setCommenttime(Util.getNowTime());
					
					// >>>>>是否是回复评论
					if (this.newCommentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_REPLY) {
						this.newCommentinfo.setReplycommentid(getReplyCommentinfo().getCommentId());
						this.newCommentinfo.setReplyuserid(getReplyCommentinfo().getUserid());
						this.newCommentinfo.setReplyusername(getReplyCommentinfo().getUsername());
					}

					submitCooment(this.newCommentinfo, "2");
				} else {
					showToast("发送的内容不能为空!");
				}
			}
		}

		private int commentType;// >>>>>评论类型
		private int softKeyboardPopLocationY = 0;

		// >>>>>>>>当前评论
		private Commentinfo newCommentinfo;
		// >>>>>>>>回复之前的评论
		private Commentinfo replyCommentinfo;

		public void popSoftKeyboard() {
			((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).showSoftInput(this.etContent, 0);
			int[] location = new int[2];
			this.etContent.getLocationOnScreen(location);
			this.softKeyboardPopLocationY = location[1];
		}

		public Commentinfo getNewCommentinfo() {
			return newCommentinfo;
		}

		public void setNewCommentinfo(Commentinfo newCommentinfo) {
			this.newCommentinfo = newCommentinfo;
		}

		public Commentinfo getReplyCommentinfo() {
			return replyCommentinfo;
		}

		public void setReplyCommentinfo(Commentinfo replyCommentinfo) {
			this.replyCommentinfo = replyCommentinfo;
		}

		public void setCommentTypeOnlyComment() {
			int[] location = new int[2];
			this.etContent.getLocationOnScreen(location);
			int softKeyboardLocationY = location[1];

			// >>>>>>>>如果不是弹出的设置为评论
			if (this.softKeyboardPopLocationY != softKeyboardLocationY) {
				setCommentType(Commentinfo.COMMENT_TYPE_ONLY);
				setEtCommentHint(getString(R.string.comment));
			}
		}

		private IKeyboardListener keyboardListener = new IKeyboardListener() {

			@Override
			public void hide() {
				// TODO Auto-generated method stub
				setCommentType(Commentinfo.COMMENT_TYPE_ONLY);
				setEtCommentHint(getString(R.string.comment));
			}
		};

		public int getCommentType() {
			return commentType;
		}

		public void setCommentType(int commentType) {
			this.commentType = commentType;
		}

		public void setEtCommentHint(String etCommentHint) {
			this.etContent.setHint(etCommentHint);
		}

	}

	@Override
	public void gestureNext() {
		// TODO Auto-generated method stub
		pageIndex = 0;
		if (index > 0) {
			index--;
			initData();
		} else {
			showToast("已经是第一个了!");
		}
	}

	@Override
	public void gesturePrev() {
		// TODO Auto-generated method stub
		pageIndex = 0;
		if (index < mGroupInfos.size() - 1) {
			index++;
			initData();
		} else {
			showToast("已经是最后一个!");
		}
	}

	/**
	 * 判断是否是当前资源
	 * 
	 * @param resID
	 * @return
	 */
	public boolean isCurrentGroup(long groupID) {
		if (groupID == mGroupInfos.get(index).getSid())
			return true;
		return false;
	}

}
