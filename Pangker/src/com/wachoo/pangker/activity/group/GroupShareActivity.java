package com.wachoo.pangker.activity.group;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.res.PictureInfoActivity;
import com.wachoo.pangker.activity.res.ResInfoActivity;
import com.wachoo.pangker.activity.res.ResLocalActivity;
import com.wachoo.pangker.activity.res.ResUserStoreActivity;
import com.wachoo.pangker.activity.res.UploadPicActivity;
import com.wachoo.pangker.adapter.GroupShareAdapter;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupsNotifyResult;
import com.wachoo.pangker.server.response.QueryResGroupShareResult;
import com.wachoo.pangker.server.response.ResGroupshare;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;

public class GroupShareActivity extends CommonPopActivity implements IUICallBackInterface {

	private static final int DELETE_GROUP_SHARE = 8;// 删除群组分享
	private PullToRefreshListView pullToRefreshListView;//
	private EmptyView emptyView;
	private String myUserId;
	private int SEARCH_CASEKEY = 0x10;
	private PangkerApplication application;
	// >>>>>>群组资源分享列表界面
	private ListView mGroupShareListView;
	private EmptyView mGroupShareEmptyView;
	private FooterView groupShareFooterView;
	private GroupShareAdapter mGroupShareAdapter;
	private ResGroupshare mResGroupshare;
	private UserGroup mUserGroup;
	private Button btnPhtopShare;
	private EditText etShareText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.group_share_layout);
		application = (PangkerApplication) getApplication();

		mUserGroup = (UserGroup) this.getIntent().getSerializableExtra("UserGroup");
		myUserId = ((PangkerApplication) getApplication()).getMyUserID();

		TextView title = (TextView) findViewById(R.id.mmtitle);
		title.setText("群内分享");

		initGroupShareView();
		QueryResGroupShare(mUserGroup.getGroupId(), true);

	}

	private void initGroupShareView() {
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				GroupShareActivity.this.finish();
			}
		});
		Button btnMore = (Button) findViewById(R.id.iv_more);
		btnMore.setVisibility(View.VISIBLE);
		btnMore.setText("分享资源");
		RelativeLayout.LayoutParams linearParams = (RelativeLayout.LayoutParams) btnMore.getLayoutParams();
		linearParams.width = RelativeLayout.LayoutParams.WRAP_CONTENT;//
		btnMore.setLayoutParams(linearParams);
		btnMore.setPadding(5, 0, 5, 0);
		btnMore.setGravity(Gravity.CENTER);
		btnMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				shareRes2Group();
			}
		});
		btnPhtopShare = (Button) findViewById(R.id.btn_left);
		btnPhtopShare.setBackgroundResource(R.drawable.btn_camera_selector);
		findViewById(R.id.btn_clear).setVisibility(View.GONE);
		btnPhtopShare.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sharePhoto2Group();
			}
		});
		etShareText = (EditText) findViewById(R.id.et_search);
		etShareText.setHint("分享文字");
		etShareText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
						GroupShareActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				TextGroupShare();
			}
		});
		etShareText.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
						GroupShareActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				TextGroupShare();
				return false;
			}
		});

		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setTag("pk_group_share");
		pullToRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				reset();
				QueryResGroupShare(mUserGroup.getGroupId(), false);
			}
		});

		mGroupShareListView = pullToRefreshListView.getRefreshableView();
		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
		mGroupShareAdapter = new GroupShareAdapter(this, new ArrayList<ResGroupshare>(), mImageWorker);
		mGroupShareListView.setAdapter(mGroupShareAdapter);
		mGroupShareListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				ResGroupshare reShareInfo = (ResGroupshare) arg0.getItemAtPosition(position);
				if (reShareInfo.getResId() != null) {
					lookShare(reShareInfo);
				}
			}
		});
		mGroupShareAdapter.setOnDeleteLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				mResGroupshare = (ResGroupshare) v.getTag();
				if (mResGroupshare.getShareUid().toString().equals(myUserId) || mUserGroup.getUid().equals(myUserId)) {
					showMenu();// showMsgMenu
				}
				return true;
			}
		});
		mGroupShareAdapter.setOnCoppyLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				mResGroupshare = (ResGroupshare) v.getTag();

				showMsgMenu(mResGroupshare);// showMsgMenu

				return true;
			}
		});

		mGroupShareEmptyView = new EmptyView(this);
		mGroupShareEmptyView.addToListView(mGroupShareListView);
		mGroupShareEmptyView.showView(R.drawable.emptylist_icon, R.string.no_recommend_groupshare_add);
		groupShareFooterView = new FooterView(this);
		groupShareFooterView.addToListView(mGroupShareListView);
		groupShareFooterView.setOnLoadMoreListener(onLoadMoreListener);

	}

	private PopMenuDialog popMenuDialog;

	ActionItem[] shareResMenu = new ActionItem[] { new ActionItem(1, "拍照上传"), new ActionItem(2, "分享文字"),
			new ActionItem(3, "从网盘资源获取") };

	private void shareRes2Group() {

		// TODO Auto-generated method stub
		if (popMenuDialog == null) {
			popMenuDialog = new PopMenuDialog(this);
			popMenuDialog.setListener(shareRes2GroupListener);
		}
		popMenuDialog.setMenus(shareResMenu);
		popMenuDialog.setTitle("资源上传");
		popMenuDialog.show();

	}

	private UITableView.ClickListener shareRes2GroupListener = new UITableView.ClickListener() {

		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			switch (actionid) {
			case 1:
				sharePhoto2Group();
				break;
			case 2:
				TextGroupShare();
				break;
			case 3:
				groupShareRes();
				break;

			}
			popMenuDialog.dismiss();
		}

	};

	private void sharePhoto2Group() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(GroupShareActivity.this, UploadPicActivity.class);
		intent.putExtra(UserGroup.GROUPID_KEY, mUserGroup.getGroupId());
		intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.upLoad);
		intent.putExtra(UploadPicActivity.ONLY_CAMERA_KEY, true);
		startActivity(intent);

	}

	ActionItem[] msgDeleteMenu = new ActionItem[] { new ActionItem(2, "删除") };
	private PopMenuDialog menuDialog;

	private void showMenu() {
		// TODO Auto-generated method stub
		menuDialog = new PopMenuDialog(this, R.style.MyDialog);
		menuDialog.setListener(mShareClickListener);
		menuDialog.setMenus(msgDeleteMenu);
		menuDialog.setTitle(mResGroupshare.getResName());
		menuDialog.show();
	}

	private PopMenuDialog msgCoppyDialog;
	ActionItem[] msgCoppyMenu;

	private void showMsgMenu(final ResGroupshare item) {
		// TODO Auto-generated method stub
		if (msgCoppyDialog == null) {
			msgCoppyDialog = new PopMenuDialog(this, R.style.MyDialog);
			msgCoppyDialog.setListener(mShareClickListener);
		}

		// >>>>>>>>>判断是否是自己分析或者自己的群组
		if (mResGroupshare.getShareUid().toString().equals(myUserId) || mUserGroup.getUid().equals(myUserId)) {
			msgCoppyMenu = new ActionItem[] { new ActionItem(1, "复制"), new ActionItem(2, "删除") };
		} else {
			msgCoppyMenu = new ActionItem[] { new ActionItem(1, "复制") };
		}
		msgCoppyDialog.setMenus(msgCoppyMenu);
		msgCoppyDialog.setTitle("文字信息");
		msgCoppyDialog.show();

	}

	UITableView.ClickListener mShareClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			// TODO Auto-generated method stub
			if (actionId == 1) {// 复制
				msgCoppyDialog.dismiss();
				ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clip.setText(mResGroupshare.getShareReason());
			} else if (actionId == 2) {// 删除资源
				menuDialog.dismiss();
				deleteResGroupShare(mResGroupshare);
			}

		}
	};

	/**
	 * TODO 删除群内分享资源 QueryResGroupShare.json
	 */
	private void deleteResGroupShare(final ResGroupshare groupShare) {
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				if (response != null) {
					BaseResult deleteResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
					if (deleteResult == null) {
						showToast(R.string.to_server_fail);
						return;
					} else if (deleteResult.getErrorCode() == deleteResult.SUCCESS) {
						mGroupShareAdapter.removeResGroupshares(groupShare);
					} else {
						showToast(deleteResult.getErrorMessage());
					}
				}
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", myUserId));
		paras.add(new Parameter("id", groupShare.getId().toString()));
		serverMana.supportRequest(Configuration.getDeleteResGroupShare(), paras, true, getString(R.string.please_wait));
	}

	private FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			QueryResGroupShare(mUserGroup.getGroupId(), false);
		}
	};

	private void groupShareRes() {
		Intent intent = new Intent(this, ResUserStoreActivity.class);
		intent.putExtra(UserGroup.GROUPID_KEY, mUserGroup.getGroupId());
		intent.putExtra(UserGroup.GROUP_NAME_KEY, mUserGroup.getGroupName());
		intent.putExtra(ResUserStoreActivity.START_TYPE, 1);
		UserItem userItem = new UserItem();
		userItem.setUserId(application.getMyUserID());
		userItem.setUserName(application.getMyUserName());
		intent.putExtra(PangkerConstant.INTENT_UESRITEM, userItem);
		startActivity(intent);
	}

	// 对推荐进行处理
	private void lookShare(ResGroupshare info) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		if (info.getResType() == PangkerConstant.RES_PICTURE) {
			intent.setClass(this, PictureInfoActivity.class);
		} else {
			intent.setClass(this, ResInfoActivity.class);
		}
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();

		ResShowEntity entity = new ResShowEntity();
		entity.setCommentTimes(0);
		entity.setResType(info.getResType());
		entity.setDownloadTimes(0);
		entity.setFavorTimes(0);
		entity.setResID(info.getResId());
		entity.setScore(0);
		entity.setResName(info.getResName());
		entity.setShareUid(String.valueOf(info.getShareUid()));
		reList.add(entity);
		application.setResLists(reList);
		intent.putExtra("resShowEntity", 0);
		application.setViewIndex(0);
		startActivity(intent);
	}

	private void TextGroupShare() {
		Intent intent = new Intent(this, TextGroupShareActivity.class);
		intent.putExtra(UserGroup.GROUPID_KEY, mUserGroup.getGroupId());
		intent.putExtra(UserGroup.GROUP_NAME_KEY, mUserGroup.getGroupName());
		startActivity(intent);

	}

	// 长按的上下文菜单选项点击事件处理
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// 用户子列表长按
		if (item.getGroupId() == 0) {
			if (item.getItemId() == DELETE_GROUP_SHARE) {
				deleteResGroupShare(mResGroupshare.getId().toString());
			}
		}
		return super.onContextItemSelected(item);
	}

	/**
	 * TODO 删除群内分享资源 QueryResGroupShare.json
	 */
	public void deleteResGroupShare(String groupShareId) {
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				if (!HttpResponseStatus(response)) {
					return;
				}
				BaseResult deleteResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
				if (deleteResult == null) {
					showToast(R.string.to_server_fail);
					return;
				} else if (deleteResult.getErrorCode() == deleteResult.SUCCESS) {
					mGroupShareAdapter.removeResGroupshares(mResGroupshare);
					showToast(deleteResult.getErrorMessage());
				} else {
					showToast(deleteResult.getErrorMessage());
				}
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", myUserId));
		paras.add(new Parameter("id", groupShareId));
		serverMana.supportRequest(Configuration.getDeleteResGroupShare(), paras, true,
				getResourcesMessage(R.string.please_wait));
	}

	/**
	 * TODO 查询群内分享资源
	 * 
	 * QueryResGroupShare.json
	 */
	public void QueryResGroupShare(String groupId, boolean isFrist) {
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				pullToRefreshListView.onRefreshComplete();
				if (!HttpResponseStatus(response)) {
					groupShareFooterView.onLoadCompleteErr();
					return;
				}

				QueryResGroupShareResult shareResult = JSONUtil.fromJson(response.toString(),
						QueryResGroupShareResult.class);
				if (shareResult != null && shareResult.getErrorCode() == BaseResult.SUCCESS) {
					groupShareFooterView.onLoadComplete(shareResult.getCount(), shareResult.getSumCount());
					if (startLimit == 0) {
						mGroupShareAdapter.setResGroupshares(shareResult.getResGroupShare());
					} else
						mGroupShareAdapter.addResGroupshares(shareResult.getResGroupShare());
				} else if (shareResult != null && shareResult.getErrorCode() == BaseResult.FAILED) {
					showToast(shareResult.getErrorMessage());
				} else {
					groupShareFooterView.onLoadCompleteErr();
					showToast(R.string.to_server_fail);
				}
				resetStartLimit();
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("groupId", groupId));
		paras.add(getSearchLimit());
		if (isFrist) {
			pullToRefreshListView.setRefreshing(true);
		}
		serverMana.supportRequest(Configuration.getQueryResGroupShare(), paras);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(supportResponse)) {
			emptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
			return;
		}
		if (caseKey == SEARCH_CASEKEY) {
			GroupsNotifyResult result = JSONUtil.fromJson(supportResponse.toString(), GroupsNotifyResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				if (result.getGroups() != null && result.getGroups().size() > 0) {
				} else {
					emptyView.showView(R.drawable.emptylist_icon, R.string.no_data);
				}
			} else {
				showToast(getResourcesMessage(R.string.to_server_fail));
			}
		}
	}

	// >>>>加载更多区间....
	private int startLimit = 0;
	private int endLimit = 0;
	private static final int incremental = 10;

	// >>>>>> getSearchLimit每次加载incremental个
	private Parameter getSearchLimit() {
		endLimit = startLimit + incremental;
		return new Parameter("limit", startLimit + "," + incremental);
	}

	// >>>>>>>>> setStartLimit
	private void resetStartLimit() {
		startLimit = endLimit;
	}

	// 刷新，重新查询
	private void reset() {
		startLimit = 0;
	}

}
