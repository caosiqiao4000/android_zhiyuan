package com.wachoo.pangker.activity.group;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.pangker.RelativesManagerActivity;
import com.wachoo.pangker.adapter.MeetGroupAdapter;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.GroupsNotifyResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;

public class OtherGroupsActivity extends CommonPopActivity implements IUICallBackInterface {

	private PullToRefreshListView pullToRefreshListView;//
	private EmptyView emptyView;
	private ListView groupListView;
	private String uid;
	private String myUserId;
	private int SEARCH_CASEKEY = 0x10;
	private MeetGroupAdapter adapter;
	private PangkerApplication application;
	private RadioGroup mRadioGroup;
	private int status_flag = 0; // 显示界面:0:Ta群组,1转播的群组
	
	private List<UserGroup> taGroups;// Ta的的群组
	private List<UserGroup> braodcastGroups;// ta转播的群组
	private List<UserGroup> favoriteGroups;// 收藏的群组
	private String fromType;
	private List<UserItem> userLists;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.other_group_layout);
		application = (PangkerApplication) getApplication(); 
		fromType = this.getIntent().getStringExtra("fromType");
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		groupListView = pullToRefreshListView.getRefreshableView();
		
		emptyView = new EmptyView(this);
		emptyView.addToListView(groupListView);
		myUserId = ((PangkerApplication)getApplication()).getMyUserID();
		uid = getIntent().getExtras().getString(UserItem.USERID);
		userLists = (List<UserItem>)this.getIntent().getSerializableExtra("UserLists");
		
		groupListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(fromType != null && fromType.equals("inviteToTraceRoom")){
					invite2groupchat((UserGroup)arg0.getItemAtPosition(arg2));
				}
				else 
				   lookChatRoomInfo(arg2 - 1);
			}
		});
		
		Button button = (Button)findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				OtherGroupsActivity.this.finish();
			}
		});
		Button btnMore = (Button) findViewById(R.id.iv_more);
		btnMore.setVisibility(View.GONE);
		TextView  title = (TextView) findViewById(R.id.mmtitle);
		if(fromType != null && fromType.equals("inviteToTraceRoom")){
			title.setText("邀请亲友到群聊");
			//只能邀请到我的群组和收藏的群组
			findViewById(R.id.rb_group_favorite).setVisibility(View.VISIBLE);
			findViewById(R.id.rb_group_braodcast).setVisibility(View.GONE);
		}
		else 
		   title.setText("TA的群组会议室");
		adapter = new MeetGroupAdapter(new ArrayList<UserGroup>(), this);
		groupListView.setAdapter(adapter);
		taGroups = new ArrayList<UserGroup>();
		braodcastGroups = new ArrayList<UserGroup>();
		favoriteGroups = new ArrayList<UserGroup>();
		refreshGroup(new ArrayList<UserGroup>());
		
		
		mRadioGroup = (RadioGroup) findViewById(R.id.rg_group);

		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
				case R.id.rb_group_ta:
					adapter.setUserGroups(taGroups);
					status_flag = 0;
					break;
				case R.id.rb_group_braodcast:
					adapter.setUserGroups(braodcastGroups);
					status_flag = 1;
					break;
				case R.id.rb_group_favorite:
					adapter.setUserGroups(favoriteGroups);
					status_flag = 2;
					break;
				default:
					break;
				}
			}
		});
		
		
		queryGroups();
	}

	//查询群组
	private void queryGroups() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", myUserId));
		paras.add(new Parameter("userId", myUserId));
		paras.add(new Parameter("uid", uid));
		pullToRefreshListView.setRefreshing(true);
		serverMana.supportRequest(Configuration.getGroupInfoQuery(), paras, SEARCH_CASEKEY);
	}
	
	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			queryGroups();
		}
	};
	
	private void lookChatRoomInfo(int position){
		Intent intent = new Intent();
		List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
		for (UserGroup u : adapter.getUserGroups()) {
			GroupLbsInfo lbsInfo = new GroupLbsInfo();
			lbsInfo.setgName(u.getGroupName());
			lbsInfo.setSid(Long.parseLong(u.getGroupId()));
			lbsInfo.setUid(Long.parseLong(u.getUid()));
			lbsInfo.setShareUid(uid);
			groupList.add(lbsInfo);
		}
		intent.putExtra("GroupInfo", (Serializable)groupList);
		intent.putExtra("group_index", position);
		intent.setClass(this, ChatRoomInfoActivity.class);
		startActivity(intent);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(supportResponse)){
			emptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
			return;
		}
		if (caseKey == SEARCH_CASEKEY) {
			GroupsNotifyResult result = JSONUtil.fromJson(supportResponse.toString(), GroupsNotifyResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				if (result.getGroups() != null && result.getGroups().size() > 0) {
					refreshGroup(result.getGroups());
				} else {
					emptyView.showView(R.drawable.emptylist_icon, R.string.no_data);
				}
			} else {
				showToast(getResourcesMessage(R.string.to_server_fail));
			}
		}
	}
	
	private void refreshGroup(List<UserGroup> uGroups) {
		taGroups.clear();
		braodcastGroups.clear();
		favoriteGroups.clear();
		if (uGroups.size() > 0) {
			for (UserGroup uGroup : uGroups) {
				if (uGroup.getGroupFlag() == 0) {
					taGroups.add(uGroup);
				}
				else if(uGroup.getGroupFlag() == 1){
					favoriteGroups.add(uGroup);
				}
				else if (uGroup.getGroupFlag() == 3) {
					braodcastGroups.add(uGroup);
				}
			}
		}
		if (status_flag == 0) {
			adapter.setUserGroups(taGroups);
		} else if (status_flag == 1) {
			adapter.setUserGroups(braodcastGroups);
		}
		else if(status_flag == 2){
			adapter.setUserGroups(favoriteGroups);
		}
	}
	/**
	 * 邀请亲友到群聊
	 * void
	 * TODO
	 * @param usergroup
	 */
	private void invite2groupchat(UserGroup usergroup){
		Intent mIntent = new Intent(this,RelativesManagerActivity.class);
		mIntent.putExtra("UserGroup", usergroup);
		mIntent.putExtra("optionType", "inviteToTraceRoom");
		startActivity(mIntent);
//		OpenfireManager manager = application.getOpenfireManager();
//		if (!application.getOpenfireManager().isLoggedIn()) {
//			showToast(R.string.not_logged_cannot_operating);
//			return;
//		}
//		// 邀请操作
//		String msg = JSONUtil.toJson(usergroup, false);
//		for (UserItem user : userLists) {
//			manager.sendGroupInvite(uid, user.getUserId(), msg);
//		}
//		showToast("已成功发送邀请消息!");
//		finish();
	}

}
