package com.wachoo.pangker.activity.group;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.pangker.NetGroupSearchActivity;

public class GroupSearchActivity extends CommonPopActivity {

	private Button btn_back;
	private Button btn_more;
	private TextView tv_title;

	private LinearLayout ly_qroup_name; // 通过群组名称搜索
	private LinearLayout ly_pangker_num;// 通过旁客号码搜索
	private LinearLayout ly_phone_num;// 通过手机号码搜索
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.group_search_layout);
		

		tv_title = (TextView) findViewById(R.id.mmtitle);
		tv_title.setText(R.string.group_search_title);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(clickListener);
		btn_more = (Button) findViewById(R.id.iv_more);
		btn_more.setVisibility(View.GONE);

		ly_qroup_name = (LinearLayout) findViewById(R.id.ly_qroup_name);
		ly_qroup_name.setOnClickListener(clickListener);
		ly_pangker_num = (LinearLayout) findViewById(R.id.ly_pangker_num);
		ly_pangker_num.setOnClickListener(clickListener);
		ly_phone_num = (LinearLayout) findViewById(R.id.ly_phone_num);
		ly_phone_num.setOnClickListener(clickListener);
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btn_back) {
				GroupSearchActivity.this.finish();
			} else if (v == ly_qroup_name) {
				//通过群组名称搜索
				Intent intent = new Intent(GroupSearchActivity.this, NetGroupSearchActivity.class);
				intent.putExtra("fromtype", 0);
				GroupSearchActivity.this.startActivity(intent);
			} else if (v == ly_pangker_num) {
				//通过旁客号码搜索：根据旁客号码搜索出该用户创建的群组
				Intent intent = new Intent(GroupSearchActivity.this, NetGroupSearchActivity.class);
				intent.putExtra("fromtype", 1);
				GroupSearchActivity.this.startActivity(intent);
			} else if (v == ly_phone_num) {
				//通过手机号码搜索：根据手机号码搜索出该用户创建的群组
				Intent intent = new Intent(GroupSearchActivity.this, NetGroupSearchActivity.class);
				intent.putExtra("fromtype", 2);
				GroupSearchActivity.this.startActivity(intent);
			}
		}
	};
}
