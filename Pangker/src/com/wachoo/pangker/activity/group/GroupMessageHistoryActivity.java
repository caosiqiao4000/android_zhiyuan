package com.wachoo.pangker.activity.group;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.ChatRoomListAdapter;
import com.wachoo.pangker.db.IMeetRoomDao;
import com.wachoo.pangker.db.impl.MeetRoomDaoImpl;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;

public class GroupMessageHistoryActivity extends CommonPopActivity {

	private final int page_length = 10;
	private int page_index = 0;
	private int page_total;
	private int count_total;

	private ListView historyListView;
	private EmptyView emptyView;
	private Button btnLast;
	private Button btnNext;
	private TextView txtCount;
	private ChatRoomListAdapter mChatMessageAdapter; // >>>>>>>私信会话adapter

	private IMeetRoomDao mMeetRoomDao;
	private UserGroup mUserGroup;

	ImageLocalFetcher localFetcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.msghistory);
		init();
		initView();

		initData();
	}

	private void init() {
		// TODO Auto-generated method stub
		// >>>>>>>>>加载
		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = false;
		mImageCache = new ImageCache(this, imageCacheParams);
		localFetcher.setImageCache(mImageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);

		mUserGroup = (UserGroup) getIntent().getSerializableExtra("UserGroup");
		mMeetRoomDao = new MeetRoomDaoImpl(this);
		mChatMessageAdapter = new ChatRoomListAdapter(this, localFetcher);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		mImageCache.removeMemoryCaches();
		super.onDestroy();
	}

	private void initView() {
		// TODO Auto-generated method stub
		RelativeLayout title_layout = (RelativeLayout) findViewById(R.id.title_layout);
		title_layout.setVisibility(View.VISIBLE);
		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText(R.string.group_history_show);
		Button btnRight = (Button) findViewById(R.id.iv_more);
		btnRight.setVisibility(View.GONE);

		historyListView = (ListView) findViewById(R.id.history_list);
		emptyView = new EmptyView(this);
		emptyView.addToListView(historyListView);

		btnLast = (Button) findViewById(R.id.last);
		btnLast.setOnClickListener(onClickListener);

		btnNext = (Button) findViewById(R.id.next);
		btnNext.setOnClickListener(onClickListener);
		txtCount = (TextView) findViewById(R.id.count);
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(onClickListener);

		Button btnClear = (Button) findViewById(R.id.clear_history);
		btnClear.setOnClickListener(onClickListener);
	}

	private void showMsgData(int pageindex) {
		int startLimit = count_total - (page_total - pageindex + 1) * page_length;
		List<MeetRoom> list_item = mMeetRoomDao.getLimitHistory(mUserGroup.getGroupId(), startLimit, page_length);
		txtCount.setText(page_index + "/" + page_total);
		mChatMessageAdapter.setData(list_item);
	}

	private void showNoMsgData() {
		page_index = 0;
		page_total = 0;
		txtCount.setText("0/0");
		mChatMessageAdapter.setData(new ArrayList<MeetRoom>());
		emptyView.showView(R.drawable.emptylist_icon, "没有历史记录 ");
	}

	private void initData() {
		// TODO Auto-generated method stub
		historyListView.setAdapter(mChatMessageAdapter);
		count_total = mMeetRoomDao.getMeetRoomCount(mUserGroup.getGroupId());
		page_index = page_total = (count_total + page_length - 1) / page_length;
		if (page_index > 0) {
			showMsgData(page_index);
		} else {
			showNoMsgData();
		}

	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.btn_back) {
				finish();
			}
			if (v.getId() == R.id.last) {
				if ((page_index + 1) <= page_total) {
					page_index++;
					showMsgData(page_index);
				} else {
					showToast(R.string.toonext);
				}
			}
			if (v.getId() == R.id.next) {
				if (page_index - 1 > 0) {// 上一页
					page_index--;
					showMsgData(page_index);
				} else {
					showToast(R.string.toolast);
				}
			}
			if (v.getId() == R.id.clear_history) {
				MessageTipDialog diaolog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-generated method stub
						mMeetRoomDao.clearRoomHistory(mUserGroup.getGroupId());
						showNoMsgData();
					}
				}, GroupMessageHistoryActivity.this);
				diaolog.showDialog("提示!", "确认清空该群组的所有聊天记录？", false);
			}
		}
	};

}
