package com.wachoo.pangker.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.gesture.GestureOverlayView;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.res.RecommendReceiverAcitvity;
import com.wachoo.pangker.activity.res.UploadMusicActivity;
import com.wachoo.pangker.adapter.LrcAdapter;
import com.wachoo.pangker.db.IDownloadDao;
import com.wachoo.pangker.db.impl.DownloadDaoImpl;
import com.wachoo.pangker.downupload.DownLoadManager;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.gestures.GestureCommandRegister;
import com.wachoo.pangker.gestures.GestureListener;
import com.wachoo.pangker.gestures.NextCommend;
import com.wachoo.pangker.gestures.PrevCommend;
import com.wachoo.pangker.listener.TextButtonWatcher;
import com.wachoo.pangker.music.Lrc;
import com.wachoo.pangker.music.LrcLoader;
import com.wachoo.pangker.music.LrcResponse;
import com.wachoo.pangker.music.Lyric;
import com.wachoo.pangker.music.MusicPlayerListener;
import com.wachoo.pangker.music.MusicPlayerManager;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.Commentinfo;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.ResInfoQueryByIDResult;
import com.wachoo.pangker.server.response.ResShareResult;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.LyricSeekBar;
import com.wachoo.pangker.ui.LyricView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.dialog.ListDialog;
import com.wachoo.pangker.util.Util;

/**
 * @author Administrator 播放器绑定的规则：首先获取正在播放的音乐，
 */
public class MusicPlayActivity extends RecommendReceiverAcitvity implements
		IUICallBackInterface, MusicPlayerListener, GestureListener {

	private final String[] MUSIC_MODEL = { "顺序播放", "单曲循环", "随机播放", "列表循环" };

	private final int RES_RATING = 0x10;
	private final int RES_COLLECT = 0x11;
	private final int RES_TRANS = 0x12;
	private final int RES_COMMENT = 0x13;
	private final int KEY_RESINFO = 0x14;
	public final static int REFRESH_PROGRESS_EVENT = 0x100;//
	// public final static int REFRESH_OPERATE = 0x101;//
	final static String TAG = "MusicPlayActivity";
	private String userId;
	private MusicInfo musicInfo;
	private MediaPlayer mediaPlayer;
	// private MusicTimer musicTimer;
	private PangkerApplication application;
	private MusicPlayerManager playerManager;
	private ServerSupportManager serverMana;// 后台交互

	private QuickAction quickAction;
	private UIMusic uiMusic;
	private UIManager uiManager;
	// 歌词显示
	private UILrc uiLrc;
	private Dialog lrcSearchDialog;
	private ListDialog lrcDialog;
	// 歌词加载 lb
	private LrcLoader lrcLoader;
	// 手势滑动的监听
	protected ViewFlipper mViewFlipper;
	protected GestureOverlayView mGestureOverlayView;

	private IDownloadDao downloadDao;
	private DownLoadManager downLoadManager;
	private int palymodel = 0;

	// >>>>>>>>歌词相关
	private LyricSeekBar mSeekBar;
	private UIUpdateThread mUpdateThread;

	private UIComment uiComment;
	public static final String START_FLAG = "START_FLAG";
	public static final String PLAY_NET_MUSIC_SID = "PLAY_NET_MUSIC_SID";
	public static final int NET_RESINFO = 1;
	private int start_flag = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.music_player);
		start_flag = getIntent().getIntExtra(START_FLAG, 0);
		// >>>>>>>>>>>>>> edit by wangxin start
		playerManager = PangkerManager.getMusicPlayerManager();
		playerManager.addMusicPlayerListenter(this);
		application = (PangkerApplication) getApplication();
		downloadDao = new DownloadDaoImpl(this);
		downLoadManager = application.getDownLoadManager();

		// >>>>>>歌词
		mSeekBar = (LyricSeekBar) findViewById(R.id.lyric_seekbar_progress);
		mSeekBar.setMax(100);
		mSeekBar.setOnSeekBarChangeListener(changeListener);

		// >>>>>评论
		mUpdateThread = new UIUpdateThread();

		serverMana = new ServerSupportManager(this, this);
		mediaPlayer = PangkerManager.getMediaPlayer();

		userId = application.getMyUserID();
		uiMusic = new UIMusic();
		uiLrc = new UILrc();
		uiComment = new UIComment();

		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.addActionItem(new ActionItem(4, "退出播放"), true);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);

		lrcLoader = new LrcLoader();

		bindService(new Intent(this, MusicService.class), conn,
				BIND_AUTO_CREATE);
		// >>>>>>>>>>>>>edit by wangxin end
		initGesture();

		initRecommendView();

	}

	private void leavePlayScreen() {
		playerManager.removeMusicPlayerListenter(this);
		setResult(1);
		MusicPlayActivity.this.finish();
	}

	private LyricSeekBar.OnSeekBarChangeListener changeListener = new LyricSeekBar.OnSeekBarChangeListener() {

		long deviation = 0l;// >>>>>>>偏量
		long currentPosition = 0l;// >>>>>>>>>当前播放进度
		long start = 0l;// >>>>>歌词显示的第一条时间
		long end = 0l;// >>>>>>>>>>歌词显示的最后一条时间
		boolean isSubmit = true;// >>>>>>>是否提交拖动请求

		// >>>>>>>用户最后拖动的位置记录
		long lastCurrentTime = 0;

		boolean isAvailable = false;

		public boolean isAvailable() {
			return isAvailable;
		}

		public void setAvailable(boolean isAvailable) {
			this.isAvailable = isAvailable;
		}

		@Override
		public void onStopTrackingTouch(LyricSeekBar verticalSeekBar) {
			// TODO Auto-generated method stub
			if (!isAvailable())
				return;
			mUpdateThread.proceed();
			// mediaPlayer.seekTo((int) lastCurrentTime);

			uiLrc.lrcView.dismissDragTimerBar();
			Intent intent = new Intent(MusicPlayActivity.this,
					MusicService.class);
			intent.putExtra("control", PlayControl.MUSIC_PLAYER_TIMERTO);

			// >>>>>>获取当前播放的音乐位置
			intent.putExtra("seektotime",
					uiLrc.lrcView.getDragStartTime(lastCurrentTime));
			startService(intent);

		}

		@Override
		public void onStartTrackingTouch(LyricSeekBar verticalSeekBar) {
			// TODO Auto-generated method stub
			if (!isAvailable())
				return;
			mUpdateThread.pause();
			start = uiLrc.lrcView.getFirstShowTime();
			end = uiLrc.lrcView.getEndShowTime();
			currentPosition = mediaPlayer.getCurrentPosition();
			uiLrc.lrcView.showDragTimerBar();
			isSubmit = false;
		}

		@Override
		public void onProgressChanged(LyricSeekBar verticalSeekBar,
				int progress, boolean fromUser) {
			// TODO Auto-generated method stub
			if (!isAvailable())
				return;
			long currentTime = (end - start) / 100 * progress + start;
			if (!isSubmit) {
				isSubmit = true;
				deviation = currentTime - currentPosition;
				return;
			}
			currentTime = currentTime - deviation;
			Log.i(TAG, "Changed:currentTime = " + currentTime
					+ ";currentPosition = " + currentPosition);

			lastCurrentTime = currentTime;
			uiLrc.lrcView.updateIndex(currentTime);
			musicHandler.post(mUpdateResults);
		}

		Runnable mUpdateResults = new Runnable() {
			public void run() {

				uiLrc.tv_durationTime.setText(uiMusic.txtTotalTime.getText());
				uiLrc.tv_currenttime.setText(getPlayTime(lastCurrentTime));
				uiLrc.lrcView.invalidate(); // 更新视图
			}
		};
	};

	// >>>>>>>>>歌词+进度控制线程
	class UIUpdateThread extends Thread {
		long time = 600; // 滚动速度
		boolean submit = true;
		long duration = 0;
		boolean isRun = true;

		public void stopRun() {
			this.isRun = false;
		}

		@Override
		public void run() {

			while (isRun) {
				try {
					if (mediaPlayer.isPlaying()) {
						// >>>>>>>>获取音乐的总长度
						if (musicInfo.isNetMusic()
								&& !musicInfo.isUseLocalCache()) {
							duration = musicInfo.getDuration();
						} else {
							duration = mediaPlayer.getDuration();
						}
						musicHandler.post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								// >>>>>>刷新播放进度
								uiMusic.setPlayInfo(
										mediaPlayer.getCurrentPosition(),
										duration, false);
								if (submit) {
									// >>>>>>刷新歌词进度
									uiLrc.lrcView.updateIndex(mediaPlayer
											.getCurrentPosition());
									uiLrc.lrcView.invalidate();
								}
							}
						});
					}
					Thread.sleep(time);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

		}

		public void pause() {
			submit = false;
		}

		public void proceed() {
			submit = true;
		}

	}

	GestureCommandRegister register = new GestureCommandRegister();

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 4) {

				leavePlayScreen();
				// 停止音乐播放功能
				playControl(PlayControl.MUSIC_PLAYER_STOP);

			}
		}
	};

	public void onBackPressed() {
		leavePlayScreen();
		super.onBackPressed();
	};

	private void initGesture() {
		// TODO Auto-generated method stub
		mViewFlipper = (ViewFlipper) findViewById(R.id.DownloadViewFlipper);
		mGestureOverlayView = (GestureOverlayView) findViewById(R.id.gestures);
		GestureCommandRegister register = new GestureCommandRegister();
		register.registerCommand("next", new NextCommend(this));
		register.registerCommand("prev", new PrevCommend(this));
		application.getGestureHandler().setRegister(register);

		mGestureOverlayView.addOnGesturePerformedListener(application
				.getGestureHandler());
		mViewFlipper.setDisplayedChild(0);
	}

	private Handler musicHandler = new Handler();

	private ServiceConnection conn = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			// TODO Auto-generated method stub
			musicInfo = application.getPlayingMusic();
			// 加载歌词从service
			uiLrc.lrcView.setmLyric(application.getCurrentLyric());
			// 初始化界面
			initUI(((MusicService.MusicPlayerBinder) binder).isPlay());
			// musicTimer.startTimer();
			mUpdateThread.start();
			// >>>>>>加载歌词
			loadLrc(musicInfo);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			showToast("绑定播放服务出错！");
		}
	};

	private void initUI(boolean ifPlaying) {
		if (musicInfo == null) {
			uiManager = new UIManager(false);
		} else {
			uiManager = new UIManager(musicInfo.isNetMusic());
			uiMusic.txtTitle.setText(Util.getStringByLength(
					musicInfo.getMusicName(), 24));
		}

		if (musicInfo != null) {
			// >>>>>>>>如果是网络音乐并且不是使用的本机缓存文件
			if (musicInfo.isNetMusic() && !musicInfo.isUseLocalCache()) {
				uiMusic.setPlayInfo(mediaPlayer.getCurrentPosition(),
						musicInfo.getDuration(), false);
				uiLrc.lrcView.updateIndex(mediaPlayer.getCurrentPosition());
			} else {
				uiMusic.setPlayInfo(mediaPlayer.getCurrentPosition(),
						mediaPlayer.getDuration(), false);
				uiLrc.lrcView.updateIndex(mediaPlayer.getCurrentPosition());
			}
		}

		if (ifPlaying) {
			uiMusic.btnPlay.setBackgroundResource(R.drawable.desktop_pausebt_b);
		} else {
			uiMusic.btnPlay.setBackgroundResource(R.drawable.desktop_playbt_b);
		}

	}

	private void playModel() {
		// TODO Auto-generated method stub
		palymodel++;
		if (palymodel == MUSIC_MODEL.length) {
			palymodel = 0;
		}
		Intent intent = new Intent(this, MusicService.class);
		intent.putExtra("control", PlayControl.MUSIC_PLAYER_MODE);
		switch (palymodel) {
		case 0:
			intent.putExtra(PangkerConstant.MUSIC_PLAYER_MODE,
					PangkerConstant.MPM_ORDER_PLAY);
			uiManager.iv_model
					.setImageResource(R.drawable.icon_playing_mode_normal);
			break;
		case 1:
			intent.putExtra(PangkerConstant.MUSIC_PLAYER_MODE,
					PangkerConstant.MPM_SINGLE_LOOP_PLAY);
			uiManager.iv_model
					.setImageResource(R.drawable.icon_playing_mode_repeat_cur);
			break;
		case 2:
			intent.putExtra(PangkerConstant.MUSIC_PLAYER_MODE,
					PangkerConstant.MPM_RANDOM_PLAY);
			uiManager.iv_model
					.setImageResource(R.drawable.icon_playing_mode_shuffle);
			break;
		case 3:
			intent.putExtra(PangkerConstant.MUSIC_PLAYER_MODE,
					PangkerConstant.MPM_LIST_LOOP_PLAY);
			uiManager.iv_model
					.setImageResource(R.drawable.icon_playing_mode_repeat_all);
			break;
		}
		showToast(MUSIC_MODEL[palymodel]);
		startService(intent);
	}

	// >>>>>>>>>>>> edit by wangxin 播放操作 上一首 、下一首、暂停、播放。。。操作
	private void playControl(PlayControl playControl) {
		Intent intent = new Intent(this, MusicService.class);
		intent.putExtra("control", playControl);
		startService(intent);
	}

	@Override
	public void error() {
		// TODO Auto-generated method stub
		showToast("播放音乐出错！");
		uiMusic.totalTime = 0;
	}

	@Override
	public void pause(MusicInfo info) {
		// TODO Auto-generated method stub
		this.musicInfo = info;
		uiMusic.btnPlay.setBackgroundResource(R.drawable.desktop_playbt_b);
	}

	@Override
	public void play(final MusicInfo info) {
		// TODO Auto-generated method stub
		Log.i(TAG, "play~~~~~~~~ : info = " + info.toString());
		if (info != musicInfo) {
			uiMusic.totalTime = 0;
		}
		// >>>>>>>>音乐长度
		musicHandler.post(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (info != musicInfo) {
					uiMusic.sbProgress.setSecondaryProgress(0);
					uiMusic.sbProgress.setProgress(0);
					uiMusic.setMusicInfo(uiMusic.totalTime, info.getMusicName());
				}
				uiMusic.btnPlay
						.setBackgroundResource(R.drawable.desktop_pausebt_b);
				if (musicInfo != null)
					Log.i(TAG, "musicinfo:" + musicInfo.toString());
				if (info != null)
					Log.i(TAG, "info:" + info.toString());
				// 同时加载歌词
				musicInfo = info;
				loadLrc(info);
				String saveFileName = musicInfo.getMusicName()
						+ DownloadJob.SUFFIX_RESID + musicInfo.getSid() + "."
						+ musicInfo.getFileFormat();

				uiManager.setDownload(Util
						.isExitFileSD(PangkerConstant.DIR_MUSIC + "/"
								+ saveFileName));
				getResInfo();
			}
		});
	}

	@Override
	public void onBufferChanged(final int percent) {
		// TODO Auto-generated method stub

		musicHandler.post(new Runnable() {
			public void run() {
				uiMusic.sbProgress.setSecondaryProgress(percent);
			}
		});
	}

	@Override
	public void playOver() {
		// TODO Auto-generated method stub
		musicHandler.post(new Runnable() {
			public void run() {
				uiMusic.btnPlay
						.setBackgroundResource(R.drawable.desktop_playbt_b);
			}
		});
	}

	@Override
	public void onBufferStatus(final int status) {
		// TODO Auto-generated method stub
		musicHandler.post(new Runnable() {
			public void run() {
				Log.i(TAG, "onBufferStatus>>>>status=" + status);
				if (status == 0) {
					uiMusic.txtBuffer.setVisibility(View.VISIBLE);
					uiMusic.sbProgress.setSecondaryProgress(0);
					uiMusic.sbProgress.setProgress(0);
					uiMusic.txtCurTime.setText("00:00");
					if (uiMusic.totalTime == 0) {
						uiMusic.txtTotalTime.setText("00:00");
					}
				} else if (status == 1) {
					uiMusic.txtBuffer.setVisibility(View.GONE);
				}
			}
		});

	}

	private String getPlayTime(long time) {
		time /= 1000;
		long curminute = time / 60;
		long cursecond = time % 60;
		return String.format("%02d:%02d", curminute, cursecond);
	}

	/**
	 * 播放界面管理
	 * 
	 * @author Administrator
	 */
	class UIMusic implements OnClickListener {

		TextView txtCurTime;
		TextView txtTitle;
		TextView txtTotalTime;
		ImageButton btnPrevious;
		ImageButton btnPlay;
		ImageButton btnNext;
		SeekBar sbProgress;
		TextView txtBuffer;

		private long totalTime = 0;

		public UIMusic() {
			initView();
		}

		private void initView() {
			// TODO Auto-generated method stub
			txtCurTime = (TextView) findViewById(R.id.player_current_time);
			txtTotalTime = (TextView) findViewById(R.id.player_total_time);
			txtTitle = (TextView) findViewById(R.id.mmtitle);
			btnPrevious = (ImageButton) findViewById(R.id.previous_song);
			btnPlay = (ImageButton) findViewById(R.id.playpause_song);
			btnNext = (ImageButton) findViewById(R.id.next_song);
			sbProgress = (SeekBar) findViewById(R.id.seekbar_progress);
			sbProgress.setMax(100);
			btnPrevious.setOnClickListener(this);
			btnPlay.setOnClickListener(this);
			btnNext.setOnClickListener(this);
			sbProgress.setOnSeekBarChangeListener(onSeekBarChangeListener);

			txtBuffer = (TextView) findViewById(R.id.player_buffer);
		}

		public void setMusicInfo(long totalTime, String musicName) {
			this.totalTime = totalTime;
			String totalTimeString = getPlayTime(totalTime);
			txtTotalTime.setText(totalTimeString);
			txtTitle.setText(Util.getStringByLength(musicName, 12));
		}

		public void setPlayInfo(long curTime, long total, boolean fromUser) {
			try {
				if (isOnTrackingTouch) {
					if (fromUser) {
						String curTimeString = getPlayTime(curTime);
						txtCurTime.setText(curTimeString);
						if (totalTime <= 0) {
							this.totalTime = total;
							String totalTimeString = getPlayTime(totalTime);
							txtTotalTime.setText(totalTimeString);
						}
					}
					return;
				}
				String curTimeString = getPlayTime(curTime);
				txtCurTime.setText(curTimeString);

				this.totalTime = total;

				String totalTimeString = getPlayTime(totalTime);
				txtTotalTime.setText(totalTimeString);

				int rate = 0;
				if (totalTime != 0) {
					rate = (int) ((float) curTime / totalTime * 100);
				}
				sbProgress.setProgress(rate);
			} catch (Exception e) {
				// TODO: handle exception
				Object a = e;
			}
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnPrevious) {
				// playControl(PangkerConstant.MUSIC_PLAYER_PRE);
				playControl(PlayControl.MUSIC_PLAYER_PRE);// 上一首
			}
			if (v == btnPlay) {
				if (musicInfo == null) {
					showToast("还没有播放列表!");
					return;
				}
				if (mediaPlayer.isPlaying()) {
					// playControl(PangkerConstant.MUSIC_PLAYER_STOP);
					playControl(PlayControl.MUSIC_PLAYER_PAUSE);// 暂停播放

				} else {
					// playControl(PangkerConstant.MUSIC_PLAYER_PLAY);
					playControl(PlayControl.MUSIC_PLAYER_PLAY);// 播放音乐

				}
			}
			if (v == btnNext) {
				// playControl(PangkerConstant.MUSIC_PLAYER_NEXT);
				playControl(PlayControl.MUSIC_PLAYER_NEXT);// 下一首
			}
		}

		// >>>>>>>音乐进度条是否被用户拖动
		private boolean isOnTrackingTouch = false;

		// >>>>>>>>>> 拖动播放进度条监听
		SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
			// >>>>>>>用户最后拖动的位置记录
			private int lastUserProgress = 0;

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				Log.i(TAG, ">>>>>>onStopTrackingTouch");

				isOnTrackingTouch = false;
				Intent intent = new Intent(MusicPlayActivity.this,
						MusicService.class);
				intent.putExtra("control", PlayControl.MUSIC_PLAYER_SEEK);
				intent.putExtra("progress", lastUserProgress);
				startService(intent);
				Log.i(TAG, "fromUser>>>>>>seekTo:progress=" + lastUserProgress);

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				Log.i(TAG, ">>>>>>>>onStartTrackingTouch");

				isOnTrackingTouch = true;
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, final int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				// >>>>>>>是否是用户操作
				if (fromUser) {
					lastUserProgress = progress;
					musicHandler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							long time;
							if (musicInfo.isNetMusic()
									&& !musicInfo.isUseLocalCache()) {
								time = musicInfo.getDuration();
							} else {
								time = mediaPlayer.getDuration();
							}
							long curTime = (int) ((float) progress / 100 * time);
							uiMusic.setPlayInfo(curTime, time, true);
						}
					});

					Log.i(TAG, "fromUser>>>>>>onProgressChanged:progress="
							+ lastUserProgress);
				}
			}
		};
	}

	// 根据资源Id获取资源信息
	protected void getResInfo() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", String.valueOf(musicInfo.getSid())));
		sendServer(paras, Configuration.getResInfoQueryByID(), KEY_RESINFO);
	}

	protected void sendServer(List<Parameter> paras, String httpurl, int key) {
		serverMana.supportRequest(httpurl, paras, key);
	}

	/**
	 * 资源管理界面：分享，下载，转播，评分……
	 * 
	 * @author Administrator
	 * 
	 */
	class UIManager implements OnClickListener {

		static final String TAG = "MusicUIManager";
		Button btnBack;
		Button btnInfo;

		ImageButton btnDownland;// btn_downland
		ImageButton btnCollect;// btn_collect
		ImageButton btnBoradcast;// btn_boradcast

		ImageButton btnRecommend;// btn_recommend
		ImageButton btnComment;// btn_comment
		private boolean isNet;

		ImageButton btnLocalRecommend;// >>>>>本地推荐
		ImageButton btnShare;// >>>btn_share
		ImageButton btnUpload;// >>>>>>>>btn_upload

		ImageView iv_model;

		public void setDownload(boolean b) {
			Log.i(this.TAG, "Download=" + b);
			if (b)
				btnDownland.setBackgroundResource(R.drawable.btn_downloaded_bg);
			else
				btnDownland.setBackgroundResource(R.drawable.btn_down_bg);
		}

		public void setCollect(boolean b) {
			Log.i(this.TAG, "Collect=" + b);
			if (b) {
				uiManager.btnCollect
						.setBackgroundResource(R.drawable.actionbar_but_keep_ed);
				uiManager.btnCollect.setEnabled(false);
			} else {
				uiManager.btnCollect
						.setBackgroundResource(R.drawable.btn_collect_bg);
				uiManager.btnCollect.setEnabled(true);
			}

		}

		public void setBoradcast(boolean b) {
			Log.i(this.TAG, "Boradcast=" + b);
			if (b) {
				uiManager.btnBoradcast
						.setBackgroundResource(R.drawable.actionbar_but_relay_ed);
				uiManager.btnBoradcast.setEnabled(false);
			} else {
				uiManager.btnBoradcast
						.setBackgroundResource(R.drawable.btn_broadcast_bg);
				uiManager.btnBoradcast.setEnabled(true);
			}
		}

		/**
		 * @param flagshow
		 *            网络true，本地false
		 */
		public UIManager(boolean flagshow) {
			this.isNet = flagshow;
			showManager(isNet);
			btnBack = (Button) findViewById(R.id.btn_back);
			btnInfo = (Button) findViewById(R.id.iv_more);
			// >>>>>>>>网络资源详情界面启动
			if (start_flag == NET_RESINFO)
				btnBack.setBackgroundResource(R.drawable.btn_back_noicon_selector);
			else {
				btnBack.setText("");
				btnBack.setBackgroundResource(R.drawable.btn_back_selector);
			}
			// >>>>>>>>
			btnInfo.setText("停止");
			btnInfo.setBackgroundResource(R.drawable.btn_default_selector);

			// >>>>>>>网络音乐显示工具栏
			btnRecommend = (ImageButton) findViewById(R.id.btn_recommend);
			btnComment = (ImageButton) findViewById(R.id.btn_comment);
			btnCollect = (ImageButton) findViewById(R.id.btn_collect);
			btnBoradcast = (ImageButton) findViewById(R.id.btn_boradcast);
			btnDownland = (ImageButton) findViewById(R.id.btn_downland);

			// >>>>>>>>>本地音乐显示工具栏
			btnShare = (ImageButton) findViewById(R.id.btn_share);
			btnLocalRecommend = (ImageButton) findViewById(R.id.btn_local_recommend);
			btnUpload = (ImageButton) findViewById(R.id.btn_upload);

			iv_model = (ImageView) findViewById(R.id.iv_paly_model);

			btnBack.setOnClickListener(this);
			btnInfo.setOnClickListener(this);
			btnRecommend.setOnClickListener(this);
			btnComment.setOnClickListener(this);
			btnCollect.setOnClickListener(this);
			btnBoradcast.setOnClickListener(this);
			btnDownland.setOnClickListener(this);
			iv_model.setOnClickListener(this);
			btnShare.setOnClickListener(this);
			btnUpload.setOnClickListener(this);

			if (musicInfo != null) {
				String saveFileName = musicInfo.getMusicName()
						+ DownloadJob.SUFFIX_RESID + musicInfo.getSid() + "."
						+ musicInfo.getFileFormat();

				setDownload(Util.isExitFileSD(PangkerConstant.DIR_MUSIC + "/"
						+ saveFileName));
				getResInfo();
			}
		}

		public void showManager(boolean flagshow) {
			if (flagshow) {
				findViewById(R.id.ly_net_bar).setVisibility(View.VISIBLE);
				findViewById(R.id.ly_local_bar).setVisibility(View.GONE);
			} else {
				findViewById(R.id.ly_net_bar).setVisibility(View.GONE);
				findViewById(R.id.ly_local_bar).setVisibility(View.VISIBLE);
			}
		}

		// 发表评分
		protected void ratingServer(int score, String type) {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("userid", userId));
			paras.add(new Parameter("resid", String.valueOf(musicInfo.getSid())));
			paras.add(new Parameter("score", String.valueOf(score)));
			paras.add(new Parameter("optype", type));
			sendServer(paras, Configuration.getAppraiseRes(), RES_RATING);
		}

		// 自己的资源就有收藏和取消收藏
		protected void collectRes() {
			// TODO Auto-generated method stub
			// List<Parameter> paras = new ArrayList<Parameter>();
			// paras.add(new Parameter("uid", userId));
			// paras.add(new Parameter("optype", "0"));
			// paras.add(new Parameter("resid",
			// String.valueOf(musicInfo.getSid())));
			// paras.add(new Parameter("type", "1"));
			// sendServer(paras, Configuration.getCollectResManager(),
			// RES_COLLECT);
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", userId));
			paras.add(new Parameter("resid", String.valueOf(musicInfo.getSid())));
			paras.add(new Parameter("type", "0"));
			paras.add(new Parameter("dirid", application.getRootDir(
					PangkerConstant.RES_MUSIC).getDirId()));
			serverMana.supportRequest(Configuration.getGarnerRes(), paras,
					RES_COLLECT);
		}

		protected void transRes() {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("userid", userId));
			paras.add(new Parameter("resid", String.valueOf(musicInfo.getSid())));
			paras.add(new Parameter("optype", "1"));
			paras.add(new Parameter("type", "2"));
			sendServer(paras, Configuration.getResShare(), RES_TRANS);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				// >>>>停止更新界面UI
				mUpdateThread.stopRun();
				if (start_flag == NET_RESINFO) {
					application.setViewIndex(application.getPlayMusicIndex());
				}
				leavePlayScreen();
			} else if (v == btnInfo) {
				// 停止音乐播放功能
				// quickAction.show(v);
				playControl(PlayControl.MUSIC_PLAYER_STOP);
				leavePlayScreen();
				// >>>>>>>>>移除音乐播放通知
				NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				notificationManager.cancel(MusicService.ID);
			} else if (v == btnRecommend) {
				Intent intent = new Intent(MusicPlayActivity.this,
						ContactsSelectActivity.class);
				intent.putExtra(ContactsSelectActivity.INTENT_SELECT,
						ContactsSelectActivity.TYPE_RES_RECOMMEND);
				ResShowEntity entity = new ResShowEntity();
				entity.setCommentTimes(musicInfo.getCommentTimes());
				entity.setResType(PangkerConstant.RES_MUSIC);
				entity.setDownloadTimes(musicInfo.getDownloadTimes());
				entity.setFavorTimes(musicInfo.getFavorTimes());
				entity.setResID(musicInfo.getSid());
				entity.setScore(Integer.parseInt(String.valueOf(musicInfo
						.getScore())));
				entity.setResName(musicInfo.getMusicName());
				entity.setShareUid(musicInfo.getShareUid());
				entity.setIfshare(musicInfo.getIsShare());
				entity.setSinger(musicInfo.getSinger());
				intent.putExtra(ResShowEntity.RES_ENTITY, entity);
				startActivity(intent);
				return;
			} else if (v == btnComment) {
				uiComment.showHideView(false);
			} else if (v == btnCollect) {
				collectRes();
			} else if (v == btnBoradcast) {
				transRes();
			} else if (v == btnDownland) {
				if (!application.ismExternalStorageAvailable()) {
					showToast("SD卡找不到，不能下载!");
					return;
				}
				String filename = musicInfo.getResName() + "."
						+ musicInfo.getFileFormat();
				String savePath = PangkerConstant.DIR_MUSIC;
				if (!Util.isExitFileSD(savePath + "/" + filename)) {
					downland(false);
				} else {
					showDialog(0);
				}
			} else if (v == iv_model) {
				playModel();
			}
			// >>>>>>分享音乐
			else if (v == btnShare) {
				uploadMusic(musicInfo, true);
			}
			// >>>>>>>>>上传音乐
			else if (v == btnUpload) {
				uploadMusic(musicInfo, false);
			}
		}

		// >>>>> wangxin 上传音乐到后台type:0被选择上传 1：选择上传
		private void uploadMusic(MusicInfo musicInfo, boolean ifShare) {
			// TODO Auto-generated method stub
			if (!application.ismExternalStorageAvailable()) {
				showToast("Sd卡文件找不到,无法上传!");
				return;
			}
			Intent intent = new Intent();
			intent.putExtra("intent_music", musicInfo);
			intent.putExtra("ifShare", ifShare);
			intent.putExtra(DocInfo.FILE_PATH, musicInfo.getMusicPath());
			intent.setClass(MusicPlayActivity.this, UploadMusicActivity.class);

			startActivity(intent);

		}
	}

	/**
	 * 
	 * @param flag
	 *            :是否存在该文件，没用到
	 */
	private void downland(boolean flag) {
		String fileurl = Configuration.getResDownload() + musicInfo.getSid();
		String filename = musicInfo.getMusicName() + "."
				+ musicInfo.getFileFormat();

		String savePath = PangkerConstant.DIR_MUSIC;
		if (!Util.isExitFileSD(savePath + "/" + filename)) {

			// >>>>>>>>>>初始化下载任务
			DownloadJob downloadJob = new DownloadJob();
			double endPos = musicInfo.getFileSize();
			downloadJob.setmResID(musicInfo.getMusicId());
			downloadJob.setEndPos((int) endPos);
			downloadJob.setDoneSize(0);
			downloadJob.setUrl(fileurl);
			downloadJob.setFileName(filename);
			downloadJob.setSaveFileName(musicInfo.getMusicName()
					+ DownloadJob.SUFFIX_RESID + musicInfo.getMusicId() + "."
					+ musicInfo.getFileFormat());

			downloadJob.setSavePath(savePath);
			downloadJob.setType(PangkerConstant.RES_MUSIC);
			downloadJob.setDwonTime(Util.getSysNowTime());
			downloadJob.setStatus(DownloadJob.DOWNLOAD_INIT);
			// >>>>>>设置下载任务的id
			long JobID = downloadDao.saveDownloadInfo(downloadJob);
			if (JobID > 0) {
				downloadJob.setId((int) JobID);
				// >>>>>>>添加下载任务
				downLoadManager.addDownloadJob(downloadJob);
				Intent intent = new Intent(this, PangkerService.class);
				intent.putExtra(PangkerConstant.SERVICE_INTENT,
						PangkerConstant.INTENT_DOWNLOAD);
				// >>>>>>>>>>传递上传jobID到service
				intent.putExtra(DownloadJob.DOWNLOAD_JOB_ID,
						downloadJob.getId());
				startService(intent);
			}

		} else {
			showToast("该文件已经下载过了!");
		}
	}

	private Handler handler = new Handler();

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response))
			return;
		if (caseKey == KEY_RESINFO) {// 资源信息
			// >>>>>>>>>>>>.为了显示按钮的状态使用
			final ResInfoQueryByIDResult result = JSONUtil.fromJson(
					response.toString(), ResInfoQueryByIDResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				// showResInfo(result.getResinfo());
				// >>>>>是否收藏 0：未收藏 1：已经收藏
				handler.post(new Runnable() {
					@Override
					public void run() {
						// >>>>>>>>isGarner可能为null
						uiManager.setCollect(1 == result.getResinfo()
								.getIsGarner());
						uiManager.setBoradcast(1 == result.getResinfo()
								.getIsRebroadcast());
						// >>>加载播放时间
						uiMusic.totalTime = result.getResinfo().getDuration() != null ? result
								.getResinfo().getDuration() : 0;
						musicInfo.setDuration(uiMusic.totalTime);
						uiMusic.setMusicInfo(uiMusic.totalTime,
								musicInfo.getMusicName());
					}
				});

			} else {
				// callbackErr(caseKey);
			}
		}
		if (caseKey == RES_RATING // 评分,收藏,评论
				|| caseKey == RES_COLLECT) {

			BaseResult result = JSONUtil.fromJson(response.toString(),
					BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				dealResult(caseKey);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == RES_TRANS) {// 转播
			ResShareResult result = JSONUtil.fromJson(response.toString(),
					ResShareResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				uiManager.btnBoradcast
						.setBackgroundResource(R.drawable.actionbar_but_relay_ed);
				showToast("转播成功!");
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == RES_COMMENT) {// 评论
			BaseResult submitResult = JSONUtil.fromJson(response.toString(),
					BaseResult.class);
			if (submitResult != null
					&& submitResult.errorCode == BaseResult.SUCCESS) {
				uiComment.showHideView(true);
				showToast(submitResult.errorMessage);
			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
		}
	}

	private void dealResult(int caseKey) {
		// TODO Auto-generated method stub
		if (caseKey == RES_RATING) {
			showToast("评价成功!");
		}
		if (caseKey == RES_TRANS) {// 只有网络资源可以转播
			showToast("转播成功!");
		}
		if (caseKey == RES_COLLECT) {
			showToast("收藏成功!");
			uiManager.btnCollect
					.setBackgroundResource(R.drawable.actionbar_but_keep_ed);
			uiManager.btnCollect.setEnabled(false);
		}
	}

	private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			Lrc lrc = (Lrc) parent.getItemAtPosition(position);
			lrcLoader.lrcLoader(lrc, uiLrc);
			lrcDialog.dismiss();
		}
	};

	/**
	 * 歌词控制界面
	 * 
	 * @author lb
	 */
	class UILrc implements LrcLoader.OnLrcLoader, OnClickListener {

		private LyricView lrcView;
		private ImageButton btnLrcSearch;
		private Button btnLrcUp;
		private Button btnLrcDown;
		private Button btnLrcNow;
		private EditText etMusicName;
		private EditText etSinger;
		private ImageView mImageView;
		// >>>>>>歌词拖动时间提示
		private PopupWindow mPopupWindow;
		private ViewGroup mViewGroup;

		private TextView tv_currenttime;
		private TextView tv_durationTime;

		public UILrc() {
			// >>>>>>歌词提示
			LayoutInflater mLayoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			mViewGroup = (ViewGroup) mLayoutInflater.inflate(
					R.layout.layout_musictime, null, true);

			tv_currenttime = (TextView) mViewGroup
					.findViewById(R.id.tv_currenttime);
			tv_durationTime = (TextView) mViewGroup
					.findViewById(R.id.tv_durationTime);
			mPopupWindow = new PopupWindow(mViewGroup,
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			mPopupWindow.update();

			// >>>>>>>>>专辑封面
			mImageView = (ImageView) findViewById(R.id.iv_cover);
			mImageView.setAlpha(600);

			lrcView = (LyricView) findViewById(R.id.lrcView);
			lrcView.setOnClickListener(this);
			lrcView.setDragTimerWindow(mPopupWindow);

			btnLrcSearch = (ImageButton) findViewById(R.id.btn_lrcsearch);
			btnLrcSearch.setOnClickListener(this);
			btnLrcUp = (Button) findViewById(R.id.btn_lrcUp);
			btnLrcUp.setOnClickListener(this);
			btnLrcDown = (Button) findViewById(R.id.btn_lrcDown);
			btnLrcDown.setOnClickListener(this);
			btnLrcNow = (Button) findViewById(R.id.btn_lrcNew);
			btnLrcNow.setOnClickListener(this);

			lrcDialog = new ListDialog(MusicPlayActivity.this, R.style.MyDialog);
			lrcDialog.setOnItemClickListener(onItemClickListener);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnLrcSearch) {
				showLrcSearch();
			}
			if (v == btnLrcUp) {
				lrcView.delayTime(false);
				if (application.getCurrentLyric() != null) {
					showToast("歌词延迟了0.5秒!");
				}
			}
			if (v == btnLrcDown) {
				lrcView.delayTime(true);
				if (application.getCurrentLyric() != null) {
					showToast("歌词提前了0.5秒!");
				}
			}
			if (v == btnLrcNow) {
				lrcView.delayReduction();
				if (application.getCurrentLyric() != null) {
					showToast("歌词还原了!");
				}
			}
		}

		@Override
		public void onLrcLoaded(boolean success, Lyric lyric) {
			// TODO Auto-generated method stub
			if (lyric != null) {

				application.setCurrentLyric(lyric);
				lrcView.setmLyric(lyric);
				lrcView.setSentencelist(lyric.list);
				changeListener.setAvailable(true);// >>>>>>设置歌词可用
			} else {

				application.setCurrentLyric(lyric);
				lrcView.setLyricTitle("歌词加载失败!");
				lrcView.setmLyric(lyric);
				lrcView.setSentencelist(null);
				changeListener.setAvailable(false);// >>>>>>设置歌词可用
			}
		}

	}

	/**
	 * 搜索歌词列表
	 * 
	 * @param name
	 * @param singer
	 */
	private void searchLrc(String name, String singer) {
		// TODO Auto-generated method stub
		lrcLoader.searchLrc(name, singer, onLrcResponse);
	}

	private LrcLoader.OnLrcResponse onLrcResponse = new LrcLoader.OnLrcResponse() {
		@Override
		public void onLrcResponse(LrcResponse lrcResponse) {
			// TODO Auto-generated method stub
			if (lrcResponse != null && lrcResponse.getCount() > 0) {
				showLrcDialog(lrcResponse.getLrcList());
			} else {
				showToast("没有搜索到歌词!");
			}
		}
	};

	protected void showLrcDialog(List<Lrc> lrcList) {
		// TODO Auto-generated method stub
		LrcAdapter lrcAdapter = new LrcAdapter(this, lrcList);
		lrcDialog.show();
		lrcDialog.setAdapter(lrcAdapter);
	}

	private void showLrcSearch() {
		if (lrcSearchDialog == null) {
			lrcSearchDialog = new Dialog(this, R.style.MyDialog);
			lrcSearchDialog.setContentView(R.layout.dialog_lrc_search);

			uiLrc.etMusicName = (EditText) lrcSearchDialog
					.findViewById(R.id.et_musicer);
			uiLrc.etSinger = (EditText) lrcSearchDialog
					.findViewById(R.id.et_singer);
			Button btnSure = (Button) lrcSearchDialog
					.findViewById(R.id.btn_sure);
			btnSure.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String name = uiLrc.etMusicName.getEditableText()
							.toString().trim();
					if (Util.isEmpty(name)) {
						showToast("请输入歌词名");
						return;
					}
					String singer = uiLrc.etSinger.getEditableText().toString()
							.trim();
					searchLrc(name, singer);
					lrcSearchDialog.dismiss();
				}
			});
			Button btnCancel = (Button) lrcSearchDialog
					.findViewById(R.id.btn_cancel);
			btnCancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					lrcSearchDialog.dismiss();
				}
			});
		}
		uiLrc.etMusicName.setText(musicInfo.getMusicName());
		uiLrc.etSinger.setText(musicInfo.getSinger());
		lrcSearchDialog.show();
	}

	/**
	 * 为当前播放曲目加载歌词
	 * 
	 * @param info
	 */
	private void loadLrc(MusicInfo info) {
		// TODO Auto-generated method stub
		// >>>>>如果歌曲为NULL不需要搜索歌词
		String musicName = "";
		if (info == null)
			musicName = musicInfo.getMusicName();
		else
			musicName = info.getMusicName();
		// >>>>>>>>判断是否需要加载歌词
		if (needLoadLRC(musicName)) {
			// >>>>>>>判断是否需要加载歌词
			changeListener.setAvailable(false);// >>>>>>>设置歌词拖动不可用
			uiLrc.lrcView.setmLyric(null);
			uiLrc.lrcView.resetLyricTitle();
			uiLrc.lrcView.updateIndex(0);
			lrcLoader.lrcLoader(musicName, info.getSinger(), uiLrc);
		} else {
			if (application.getCurrentLyric() != null) {
				uiLrc.lrcView.setmLyric(application.getCurrentLyric());
				uiLrc.lrcView
						.setSentencelist(application.getCurrentLyric().list);
				changeListener.setAvailable(true);// >>>>>>设置歌词可用
			}
		}
	}

	/**
	 * 判断是否需要加载歌词
	 * 
	 * @param musicName
	 * @return
	 */
	private boolean needLoadLRC(String musicName) {
		// >>>>>>>判断当前是否有歌词 、 判断是否是当前歌曲的歌词
		if (musicName == null)
			return false;
		if (application.getCurrentLyric() != null
				&& application.getCurrentLyric().getMusicName() != null
				&& application.getCurrentLyric().getMusicName()
						.contains(musicName))
			return false;
		else
			return true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Log.i(TAG, "MusicPlay>>>onDestroy");
		unbindService(conn);
		playerManager.removeMusicPlayerListenter(this);
		mGestureOverlayView.removeAllOnGesturePerformedListeners();
		super.onDestroy();
	}

	class UIComment implements View.OnClickListener {

		private View commentLayout;
		private Button btnComment;
		private EditText etComment;

		public UIComment() {
			commentLayout = (View) findViewById(R.id.comment_layout);
			etComment = (EditText) findViewById(R.id.et_foot_editer);
			btnComment = (Button) findViewById(R.id.btn_foot_pubcomment);
			btnComment.setEnabled(false);
			btnComment.setOnClickListener(this);
			etComment.addTextChangedListener(new TextButtonWatcher(btnComment));
		}

		private void showHideView(boolean isclear) {
			// TODO Auto-generated method stub
			if (commentLayout.getVisibility() == View.VISIBLE) {
				commentLayout.setVisibility(View.GONE);
			} else {
				commentLayout.setVisibility(View.VISIBLE);
			}
			if (isclear) {
				etComment.setText(null);
			}
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnComment) {
				submitCooment(etComment.getText().toString());
			}
		}

		/**
		 * 资源评论接口
		 * @param content 评论内容 请求数目 接口：AddComment.json
		 */
		protected void submitCooment(String content) {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("userid", userId));
			paras.add(new Parameter("resid", String.valueOf(musicInfo.getSid())));
			paras.add(new Parameter("type", "1"));// 操作id
			paras.add(new Parameter("commenttype", String.valueOf(Commentinfo.COMMENT_TYPE_ONLY)));
			// >>>>当前用户名称
			paras.add(new Parameter("username", application.getMyUserName()));
			paras.add(new Parameter("content", content));// 回答内容
			serverMana.supportRequest(Configuration.getAddComment(), paras,
					true, "正在提交...", RES_COMMENT);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if (id == 0) {
			builder.setMessage("该文件已经存在,要删除原文件重新下载吗?")
					.setCancelable(false)
					.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									String filename = musicInfo.getResName()
											+ "." + musicInfo.getFileFormat();
									String savePath = PangkerConstant.DIR_MUSIC;
									if (Util.deleteFileSD(savePath + "/"
											+ filename)) {
										downland(true);
									}
								}
							}).setNegativeButton("取消", null);
			AlertDialog alert = builder.create();
			return alert;
		} else {
			return super.onCreateDialog(id);
		}
	}

	@Override
	public void gestureNext() {
		// TODO Auto-generated method stub
		if (application.getPlayMusicIndex() > 0) {
			playControl(PlayControl.MUSIC_PLAYER_PRE);// 上一首
		} else {
			showToast("已经是第一首了");
		}
	}

	@Override
	public void gesturePrev() {
		// TODO Auto-generated method stub
		if (application.getPlayMusicIndex() < application.getMusiList().size()) {
			playControl(PlayControl.MUSIC_PLAYER_NEXT);// 下一首
		} else {
			showToast("已经是最后一首");
		}
	}

	@Override
	public void refreshRecommendView(RecommendInfo recommendInfo) {
		// TODO Auto-generated method stub
		refreshUI(recommendInfo);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		Intent intentSerivce = new Intent(this, MusicService.class);
		intentSerivce.putExtra("control", PlayControl.MUSIC_START_PLAY);
		startService(intentSerivce);
	}

}
