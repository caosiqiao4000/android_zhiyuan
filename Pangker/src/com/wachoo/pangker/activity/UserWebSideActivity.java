package com.wachoo.pangker.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Message;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.contact.ContactActivity;
import com.wachoo.pangker.activity.contact.ContactAttentionActivity;
import com.wachoo.pangker.activity.contact.ContactFollowActivity;
import com.wachoo.pangker.activity.group.OtherGroupsActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.activity.pangker.MapLocationActivity;
import com.wachoo.pangker.activity.res.ResShareActivity;
import com.wachoo.pangker.activity.res.ResSpeakActivity;
import com.wachoo.pangker.activity.res.ResUserStoreActivity;
import com.wachoo.pangker.activity.root.FingerprintActivity;
import com.wachoo.pangker.adapter.RecentUserAdapter;
import com.wachoo.pangker.api.ContactUserListenerManager;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.db.IAttentsDao;
import com.wachoo.pangker.db.IBlacklistDao;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.impl.AttentsDaoImpl;
import com.wachoo.pangker.db.impl.BlacklistDaoImpl;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.db.impl.PKUserDaoImpl;
import com.wachoo.pangker.db.impl.UserMsgDaoImpl;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.BlackManagerResult;
import com.wachoo.pangker.server.response.CallMeSetResult;
import com.wachoo.pangker.server.response.CountQueryResult;
import com.wachoo.pangker.server.response.LocationQueryCheckResult;
import com.wachoo.pangker.server.response.UserBasicInfoQueryResult;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.HorizontalListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.dialog.ImageShowDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * 用户基本信息
 * 
 * 
 */
public class UserWebSideActivity extends CommonPopActivity implements IUICallBackInterface {

	private ImageView icon;
	private TextView nickname;
	private TextView sign;
	private LinearLayout btn_call;
	private LinearLayout btn_addfriend;
	private LinearLayout btn_tofans;
	private LinearLayout btn_look_res;
	private LinearLayout btn_speak_res;
	private LinearLayout btn_look_privateres;
	private LinearLayout btn_look_share;
	private Button btn_himlocation;
	private LinearLayout driftLayout;
	private RatingBar rating_bar_show;
	private LinearLayout btn_look_group;
	private LinearLayout touchLayout;
	private LinearLayout attentLayout;
	private RelativeLayout ly_pk_num;
	private LinearLayout fanLayout;
	private LinearLayout btn_talkto;
	private ImageView ivFriendSign;
	private TextView tv_pangker_num;
	private TextView tv_what;

	private Button btn_userinfomation;// >>>>>>>查看用户详细信息

	private UserItem toUeserItem;
	private final int IMPLICIT_ADDATTENT_CASEKEY = 0x10;
	private final int ADDATTENT_CASEKEY = 0x11;
	private final int CALL_CHECK_CASEKEY = 0x12;
	private final int LOCATION_CHECK = 0x13;
	private final int DRIF_CHECK = 0x14;
	private final int CANCELDRIFT = 0x15;
	private final int DIALOG_BLACKLIST = 0x16;
	private final int LOAD_USERINFO = 0x17;
	private final int DELE_BLACKLIST_FRIEND = 0x18;
	private final int DELE_BLACKLIST_ATTENTION = 0x19;
	private final int DELE_BLACKLIST_ATTENTION_IMPLICIT = 0x1A;
	private final int DIALOG_ADDATTENT = 0x1C;
	private final int COUNT_USER_CASEKEY = 0x1D;

	public static final String SEARCH_ATTENTION = "0";
	public static final String SEARCH_FANS = "1";
	private String userId;
	private long touchtime = 0;
	private IUserMsgDao userMsgDao;
	private IPKUserDao pkUserDao;
	private IAttentsDao iAttentsDao;
	private IFriendsDao iFriendsDao;
	private IBlacklistDao iBlacklistDao;
	private PangkerApplication application;
	private int currentDialogId = -1;
	private PKIconResizer mImageResizer;
	private HorizontalListView recentListView;
	private RecentUserAdapter recentUsers;

	private ContactUserListenerManager userListenerManager;
	private ImageShowDialog imageshowDialog;

	private Button btn_menu;
	private QuickAction quickAction;

	// >>>>>>>启动来源
	private int start_from = 0;
	private static final String START_FORM = "START_FORM";
	private static final int START_FORM_SELF = 10;

	private UserBasicInfoQueryResult result;

	private ServerSupportManager serverMana;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.userinfo);
		pkUserDao = new PKUserDaoImpl(this);
		iAttentsDao = new AttentsDaoImpl(this);
		iFriendsDao = new FriendsDaoImpl(this);
		userMsgDao = new UserMsgDaoImpl(this);
		iBlacklistDao = new BlacklistDaoImpl(this);
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		userListenerManager = PangkerManager.getContactUserListenerManager();
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());

		serverMana = new ServerSupportManager(this, this);
		// >>>>>>>>启动来源
		start_from = getIntent().getIntExtra("START_FORM", 0);
		init();
		initClickListener();
		loadUserInfo(toUeserItem.getUserId());

		PangkerManager.getActivityStackManager().pushUserInfoActivity(this);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		PangkerManager.getActivityStackManager().pushUserInfoActivity(this);
	}

	/**
	 * 点击监听，如果加载用户信息失败，不设置监听
	 */
	private void initClickListener() {
		btn_call.setOnClickListener(listener);
		btn_addfriend.setOnClickListener(listener);

		btn_tofans.setOnClickListener(listener);
		btn_look_res.setOnClickListener(listener);
		btn_speak_res.setOnClickListener(listener);
		btn_look_privateres.setOnClickListener(listener);
		btn_look_share.setOnClickListener(listener);
		btn_himlocation.setOnClickListener(listener);
		touchLayout.setOnClickListener(listener);
		driftLayout.setOnClickListener(listener);
		btn_look_group.setOnClickListener(listener);
		attentLayout.setOnClickListener(listener);
		fanLayout.setOnClickListener(listener);
		// tv_what.setOnClickListener(listener);
	}

	private long insertChatMessage(String chatmessage, int type) {
		final ChatMessage msg = new ChatMessage();
		msg.setTime(Util.getSysNowTime()); // 手机当前时间
		msg.setUserId(toUeserItem.getUserId()); // 目标用户UserID
		msg.setContent(chatmessage);// 内容
		msg.setUserName(toUeserItem.getUserName());
		msg.setMyUserId(userId);// 当前用户userid
		msg.setDirection(ChatMessage.MSG_FROM_ME);// 方向
		msg.setMsgType(type);
		long messageID = userMsgDao.saveUserMsg(msg);
		if (messageID > 0) {
			application.getMsgInfoBroadcast().sendNoticeMsg(msg);
			return messageID;
		}
		return messageID;
	}

	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == icon) {
				showUserIcon();
			} else if (v == btn_call) {// 呼叫
				callCheck(toUeserItem.getUserId());
			} else if (v == btn_addfriend) {// 加为好友
				if (toUeserItem.getUserId().equals(userId)) {
					showToast("不能添加自己为好友!");
					return;
				}
				if (application.IsFriend(toUeserItem.getUserId())) {
					showToast(PangkerConstant.MSG_FRIEND_ISEXIST);
				} else {
					// 如果在黑名单中，首先移除黑名单
					if (application.IsBlacklistExist(toUeserItem.getUserId())) {
						BlacklistManager("2", DELE_BLACKLIST_FRIEND);
					} else {
						addFriend();
					}
				}

			} else if (v == btn_tofans) {// 关注他
				mShowDialog(DIALOG_ADDATTENT);
			} else if (v == btn_look_res) {
				Intent mIntent = new Intent();
				mIntent.putExtra(PangkerConstant.INTENT_UESRITEM, toUeserItem);
				mIntent.setClass(UserWebSideActivity.this, ResShareActivity.class);
				launch(mIntent, UserWebSideActivity.this);
			} else if (v == btn_look_privateres) {
				if (result != null && result.getIsFriend() != null && result.getIsFriend() == 1) {
					Intent mIntent = new Intent();
					mIntent.putExtra(PangkerConstant.INTENT_UESRITEM, toUeserItem);
					mIntent.setClass(UserWebSideActivity.this, ResUserStoreActivity.class);
					launch(mIntent, UserWebSideActivity.this);
				} else {
					showToast("你还不是他的好友，还无法查看他的网盘资源！");
				}
			} else if (v == btn_look_share) {
				Intent mIntent = new Intent();
				mIntent.putExtra(UserItem.USERID, toUeserItem.getUserId());
				String userNmae = (toUeserItem.getrName() != null && toUeserItem.getrName().length() > 0) ? toUeserItem
						.getrName() : toUeserItem.getUserName();
				mIntent.putExtra(UserItem.USERNAME, userNmae);
				mIntent.putExtra(UserItem.USERSIGN, toUeserItem.getSign());
				mIntent.setClass(UserWebSideActivity.this, ResShareActivity.class);
				launch(mIntent, UserWebSideActivity.this);
			} else if (v == driftLayout) {// 漂移鉴权
				driftToUserItem(toUeserItem);
			} else if (v == btn_himlocation) {// 位置信息
				locationCheck(toUeserItem.getUserId(), LOCATION_CHECK);
			} else if (v == btn_look_group) {
				Intent mIntent = new Intent(UserWebSideActivity.this, OtherGroupsActivity.class);
				mIntent.putExtra(UserItem.USERID, toUeserItem.getUserId());
				launch(mIntent, UserWebSideActivity.this);
			} else if (v == touchLayout) {
				OpenfireManager openfireManager = application.getOpenfireManager();
				long nowtime = new Date().getTime();
				if (nowtime - touchtime > 1000 * 10) {
					long messageID = insertChatMessage("您碰了对方一下!", PangkerConstant.RES_TOUCH);
					if (messageID > 0) {
						openfireManager.sendTouchMessage(userId, toUeserItem.getUserId(), messageID);

					}

					touchtime = nowtime;
				} else {
					showToast("您的touch太过频繁,请间隔10s~");
				}
			} else if (v == attentLayout) {
				Intent mIntent = new Intent(UserWebSideActivity.this, ContactFollowActivity.class);
				mIntent.putExtra(UserItem.USERID, toUeserItem.getUserId());
				mIntent.putExtra("userType", 1);
				UserWebSideActivity.this.startActivity(mIntent);
			} else if (v == fanLayout) {
				Intent mIntent = new Intent(UserWebSideActivity.this, ContactFollowActivity.class);
				mIntent.putExtra(UserItem.USERID, toUeserItem.getUserId());
				UserWebSideActivity.this.startActivity(mIntent);
			} else if (v == btn_talkto) {
				Intent intent = new Intent();
				intent.putExtra(UserItem.USERID, toUeserItem.getUserId());
				String userNmae = (toUeserItem.getrName() != null && toUeserItem.getrName().length() > 0) ? toUeserItem
						.getrName() : toUeserItem.getUserName();
				intent.putExtra(UserItem.USERNAME, userNmae);
				intent.putExtra(UserItem.USERSIGN, toUeserItem.getSign());
				intent.setClass(UserWebSideActivity.this, MsgChatActivity.class);
				launch(intent, UserWebSideActivity.this);
			} else if (v == btn_menu) {
				PangkerManager.getActivityStackManager().popAllUserInfoActivity();
			} else if (v == btn_userinfomation) {
				getMinute();
			} else if (v == btn_speak_res) {
				Intent intent = new Intent();
				intent.putExtra("UserItem", toUeserItem);
				intent.setClass(UserWebSideActivity.this, ResSpeakActivity.class);
				launch(intent, UserWebSideActivity.this);
			}
		}
	};

	ClickableSpan clickableSpan = new ClickableSpan() {
		@Override
		public void onClick(View widget) {
			// TODO Auto-generated method stub
			getMinute();
		}
	};

	private void init() {
		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("旁客个人网站");
		final String uId = getIntent().getStringExtra(UserItem.USERID);

		// >>>>>>>>>与自己私信会话禁止
		if (uId.equals(application.getMyUserID())) {
			this.finish();
			showToast(R.string.user_webside_notice_yourself);
		}

		final String userName = getIntent().getStringExtra(UserItem.USERNAME);
		final String userSign = getIntent().getStringExtra(UserItem.USERSIGN);
		icon = (ImageView) findViewById(R.id.icon);
		mImageResizer.loadImageFromServer(uId, icon);
		icon.setOnClickListener(listener);
		rating_bar_show = (RatingBar) findViewById(R.id.rating_bar_show);

		// >>>>>>>查看用户相信信息按钮(超链接)
		nickname = (TextView) findViewById(R.id.nickname);
		sign = (TextView) findViewById(R.id.sign);
		tv_pangker_num = (TextView) findViewById(R.id.tv_pangker_num);
		btn_userinfomation = (Button) findViewById(R.id.btn_userinfo);
		btn_userinfomation.setOnClickListener(listener);
		// >>>>提示
		tv_what = (TextView) findViewById(R.id.tv_what);
		if (!Util.isEmpty(application.getImAccount()) && application.getImAccount().length() > 0) {
			tv_what.setVisibility(View.GONE);
		}
		String whatText = Util.getHtmlLimitString(getResources().getString(R.string.userinfo_what_is_pk), 10);

		tv_what.setText(Html.fromHtml(whatText));
		tv_what.setMovementMethod(LinkMovementMethod.getInstance());
		CharSequence text = tv_what.getText();
		if (text instanceof Spannable) {

			int end = text.length();
			Spannable sp = (Spannable) tv_what.getText();
			URLSpan[] urls = sp.getSpans(0, end, URLSpan.class);

			SpannableStringBuilder style = new SpannableStringBuilder(text);
			style.clearSpans(); // should clear old spans
			for (URLSpan url : urls) {

				style.setSpan(new ClickableSpan() {

					@Override
					public void onClick(View widget) {
						// TODO Auto-generated method stub
						// showToast(R.string.userinfo_what_is_pk);

						MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
							@Override
							public void buttonResult(boolean isSubmit) {
								// TODO Auto-genaerated method
								// stub
								if (isSubmit) {
									Intent intent = new Intent();
									intent.setClass(UserWebSideActivity.this, FingerprintActivity.class);
									startActivity(intent);
								}
							}
						}, UserWebSideActivity.this);
						dialog.setTextSize(17, 13);
						dialog.setButtonName(R.string.pangker_number_set, R.string.pangker_number_iknow);
						dialog.showDialog(getResourcesMessage(R.string.finger_prompt),
								getResourcesMessage(R.string.finger_attention), false);

					}
				}, sp.getSpanStart(url), sp.getSpanEnd(url), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				style.setSpan(new ForegroundColorSpan(Color.WHITE), sp.getSpanStart(url), sp.getSpanEnd(url),
						Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// 设置前景色为白色
			}
			tv_what.setText(style);
		}

		btn_talkto = (LinearLayout) findViewById(R.id.btn_talkto);
		btn_talkto.setOnClickListener(listener);

		btn_call = (LinearLayout) findViewById(R.id.btn_call);
		ivFriendSign = (ImageView) findViewById(R.id.iv_friend_sign);
		btn_addfriend = (LinearLayout) findViewById(R.id.btn_addfriend);
		btn_tofans = (LinearLayout) findViewById(R.id.btn_tofans);
		btn_look_res = (LinearLayout) findViewById(R.id.btn_look_res);
		btn_speak_res = (LinearLayout) findViewById(R.id.btn_speak_res);
		btn_look_privateres = (LinearLayout) findViewById(R.id.btn_look_privateres);
		btn_look_share = (LinearLayout) findViewById(R.id.btn_look_share);
		btn_himlocation = (Button) findViewById(R.id.btn_location);
		btn_look_group = (LinearLayout) findViewById(R.id.btn_group);
		touchLayout = (LinearLayout) findViewById(R.id.btn_touch);
		attentLayout = (LinearLayout) findViewById(R.id.ly_attent);
		fanLayout = (LinearLayout) findViewById(R.id.ly_fans);
		ly_pk_num = (RelativeLayout) findViewById(R.id.ly_pk_num);

		if (toUeserItem == null) {
			toUeserItem = new UserItem();
			toUeserItem.setUserId(uId);
			toUeserItem.setUserName(userName);
			toUeserItem.setSign(userSign);
		}

		nickname.setText(toUeserItem.getrName() != null ? toUeserItem.getrName() : toUeserItem.getUserName());
		sign.setText(toUeserItem.getSign());

		// 如果是交友用户，显示交友用户标示
		if (toUeserItem != null && toUeserItem.getFriendSign() != null
				&& toUeserItem.getFriendSign() == String.valueOf(PangkerConstant.IS_MAKEFRIEND_USER)) {
			ivFriendSign.setVisibility(View.VISIBLE);
		}

		driftLayout = (LinearLayout) findViewById(R.id.btn_drift);

		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				UserWebSideActivity.this.finish();
			}
		});

		// >>>>>>>>>>是否显示该按钮
		btn_menu = (Button) findViewById(R.id.iv_more);
		if (start_from == 0) {
			btn_menu.setVisibility(View.GONE);
		} else {
			btn_menu.setVisibility(View.VISIBLE);
			btn_menu.setText("离开");
			btn_menu.setOnClickListener(listener);
		}

		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.addActionItem(new ActionItem(1, "查看详细资料"));

		quickAction.setOnActionItemClickListener(onActionItemClickListener);

		if (application.IsFriend(toUeserItem.getUserId())) {
			btn_addfriend.setBackgroundResource(R.drawable.contentinf_but_add_ed);
			btn_addfriend.setEnabled(false);
		}
	}

	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {

		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			// TODO Auto-generated method stub
			if (actionId == 1) {
				getMinute();
			}
		}
	};

	private void showUserIcon() {
		// TODO Auto-generated method stub
		if (imageshowDialog == null) {
			imageshowDialog = new ImageShowDialog(this, R.style.MyDialog);
		}
		imageshowDialog.show();
		String iconUrl = Configuration.getIconUrl() + toUeserItem.getUserId();
		imageshowDialog.setImgurl(iconUrl, toUeserItem.getUserId());
	}

	/**
	 * 添加好友
	 */
	private void addFriend() {
		OpenfireManager openfireManager = application.getOpenfireManager();
		// 保存用户头像
		// Util.getImageFromURL(toUeserItem.getPortraitIconUrl());
		pkUserDao.saveUser(toUeserItem);
		if (openfireManager.isLoggedIn()) {
			if (openfireManager.createEntry(toUeserItem.getUserId(), toUeserItem.getUserName(), null)) {
				showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_SUECCESS);
			} else {
				showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_FAIL);
			}
		} else {
			showToast(R.string.not_logged_cannot_operating);
		}
	}

	// 查看详细信息
	public void getMinute() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra("userid", toUeserItem.getUserId());
		intent.putExtra("userName", toUeserItem.getUserName());
		intent.putExtra("userSign", toUeserItem.getUserId());
		intent.setClass(UserWebSideActivity.this, UserInfoEditActivity.class);
		launch(intent, UserWebSideActivity.this);
	}

	/**
	 * Dialog监听器
	 */
	private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (dialog != null) {
				dialog.cancel();
			}
			if (which == DialogInterface.BUTTON_POSITIVE) {
				BlacklistManager("0", DIALOG_BLACKLIST);
			}
		}
	};

	/**
	 * 显示dialog前的设置
	 * 
	 * @param id
	 */
	private void mShowDialog(int id) {
		if (currentDialogId != -1) {
			removeDialog(currentDialogId);
		}
		currentDialogId = id;
		showDialog(currentDialogId);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(this);
		if (DIALOG_BLACKLIST == id) {
			builder.setTitle(toUeserItem.getUserName()).setMessage(R.string.if_addto_blacklist)
					.setPositiveButton(R.string.submit, mDialogClickListener).setNegativeButton(R.string.cancel, null);
		} else if (DIALOG_ADDATTENT == id) {
			String[] choiceItems = new String[] { "显式关注", "隐式关注" };
			builder.setSingleChoiceItems(choiceItems, -1, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					if (dialog != null) {
						dialog.dismiss();
					}

					if (application.IsBlacklistExist(toUeserItem.getUserId())) {
						if (which == 0)
							BlacklistManager("2", DELE_BLACKLIST_ATTENTION);
						else if (which == 1)
							BlacklistManager("2", DELE_BLACKLIST_ATTENTION_IMPLICIT);
					} else {
						if (which == 0)
							addAttention("1", ADDATTENT_CASEKEY);
						else if (which == 1)
							addAttention("0", IMPLICIT_ADDATTENT_CASEKEY);
					}
				}
			});
		}
		return builder.create();
	}

	/**
	 * 加载用户基本信息
	 * 
	 */
	public void loadUserInfo(String otherUserId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", userId));
		paras.add(new Parameter("uid", otherUserId));
		// 制造商 + 型号+ 系统版本
		String clientSystem = Build.MANUFACTURER + " " + Build.MODEL + " Android " + VERSION.RELEASE;
		paras.add(new Parameter("clientSystem", clientSystem)); // 客户端系统版本信息
		paras.add(new Parameter("clientBrand", Build.BRAND));// 品牌商
		paras.add(new Parameter("meno", ""));

		serverMana.supportRequest(Configuration.getUserBasicInfoQuery(), paras, false,
				getResourcesMessage(R.string.please_wait), LOAD_USERINFO);
	}

	/**
	 * 漂移鉴权
	 * 
	 * @author wubo
	 * @createtime 2012-5-23
	 * @param item
	 */
	private void driftToUserItem(final UserItem item) {
		if (application.getLocateType() == PangkerConstant.LOCATE_DRIFT) {
			UserItem userInfo = application.getDriftUser();
			if (item.getUserId().equals(userInfo.getUserId())) {
				showToast("您已经漂移在Ta身边!");
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item.getUserId(), DRIF_CHECK);
						}
					}
				}, this);
				dialog.showDialog("您目前在用户" + userInfo.getUserName() + "身边，确定要漂移到用户" + item.getUserName() + "身边？", false);
			}
		} else {
			SharedPreferencesUtil spu = new SharedPreferencesUtil(this);
			if (spu.getBoolean(PangkerConstant.SP_DRIFT_SET + application.getMyUserID(), false)) {
				locationCheck(item.getUserId(), DRIF_CHECK);
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item.getUserId(), DRIF_CHECK);
						}
					}
				}, this);
				dialog.setTextSize(17, 13);
				String data = getResources().getString(R.string.drift_show);
				data = String.format(data, item.getUserName());
				dialog.showDialog(MessageTipDialog.TYPE_DRIFT, data, getResourcesMessage(R.string.drift_tip), true);
			}
		}
	}

	/**
	 * 添加关注
	 */
	public void addAttention(String attentType, int caseKey) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("appuserid", userId));
		paras.add(new Parameter("relationuserid", toUeserItem.getUserId()));
		paras.add(new Parameter("type", "1"));
		paras.add(new Parameter("attentType", attentType));// 关注类型 0 : 隐式关注
		// 1：显式关注
		serverMana.supportRequest(Configuration.getAttentManager(), paras, caseKey);
	}

	/**
	 * 呼叫校验 void
	 */
	public void callCheck(String friendId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", friendId));
		paras.add(new Parameter("visituid", userId));
		paras.add(new Parameter("password", ""));
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getCallCheck(), paras, true, mess, CALL_CHECK_CASEKEY);
	}

	/**
	 * 位置鉴权
	 * 
	 * @author wubo
	 * @createtime May 15, 2012
	 * @param friendId
	 */
	public void locationCheck(String friendId, int caseKey) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", friendId));
		paras.add(new Parameter("visituid", userId));
		if (caseKey == LOCATION_CHECK) {
			paras.add(new Parameter("optype", "1"));
		} else if (caseKey == DRIF_CHECK) {
			paras.add(new Parameter("optype", "2"));
		}
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getLocationQueryCheck(), paras, true, mess, caseKey);
	}

	/**
	 * 黑名单管理
	 */
	public void BlacklistManager(String type, int caseKey) {
		if (!application.getOpenfireManager().isLoggedIn()) {
			showToast(R.string.not_logged_cannot_operating);
			return;
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("ruid", toUeserItem.getUserId()));
		paras.add(new Parameter("type", type)); // type 操作类型：0：添加，1：查询，2：删除。
		String mess = getResourcesMessage(R.string.requesting);
		serverMana.supportRequest(Configuration.getBlackManager(), paras, true, mess, caseKey);
	}

	/**
	 * 查询关注人 ，粉丝个数接口 接口：CountQuery.json
	 * 
	 */
	public void CountQuery() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", toUeserItem.getUserId()));
		serverMana.supportRequest(Configuration.getCountQuery(), paras, COUNT_USER_CASEKEY);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			return;
		}

		if (caseKey == ADDATTENT_CASEKEY || caseKey == IMPLICIT_ADDATTENT_CASEKEY) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				showToast(result.getErrorMessage());
				// 设置已关注
				btn_tofans.setBackgroundResource(R.drawable.contentinf_but_care_ed);
				btn_tofans.setEnabled(false);
				if (caseKey == ADDATTENT_CASEKEY) {
					application.getOpenfireManager().sendContactsChangedPacket(userId, toUeserItem.getUserId(),
							BusinessType.addattent);
					toUeserItem.setAttentType(UserItem.ATTENT_TYPE_COMMON);
				} else
					toUeserItem.setAttentType(UserItem.ATTENT_TYPE_IMPLICIT);

				pkUserDao.saveUser(toUeserItem);
				iAttentsDao.addAttent(toUeserItem, String.valueOf(PangkerConstant.DEFAULT_GROUPID));
				userListenerManager.refreshUI(ContactAttentionActivity.class.getName(),
						PangkerConstant.STATE_CHANGE_REFRESHUI);
			} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
			} else {
				showToast(R.string.to_server_fail);
			}
		} else if (caseKey == CALL_CHECK_CASEKEY) {
			CallMeSetResult result = JSONUtil.fromJson(response.toString(), CallMeSetResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS && result.getMobile() != null) {
				// 语音呼叫,记录到MsgInfo信息里面
				application.getMsgInfoBroadcast().sendCallMsg(toUeserItem.getUserId(), toUeserItem.getAvailableName());
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel://" + result.getMobile()));
				startActivity(intent);
			} else if (result != null && (result.getErrorCode() == BaseResult.FAILED || result.getErrorCode() == 2)) {
				showToast(result.getErrorMessage());
			} else
				showToast(R.string.return_value_999);
		} else if (caseKey == LOCATION_CHECK) {
			LocationQueryCheckResult result = JSONUtil.fromJson(response.toString(), LocationQueryCheckResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				if (result.getLat() == null || result.getLon() == null || result.getLat().equals("")
						|| result.getLon().equals("")) {
					showToast("获取" + toUeserItem.getUserName() + "的位置信息失败,Sorry~");
				} else {
					if (result.getUserStatus() == 12) {
						showToast(toUeserItem.getUserName() + "正在穿越中,位置不可查看...");
					} else {
						if (result.getUserStatus() == 11) {
							showToast(toUeserItem.getUserName() + "正在漂移,正在锁定对方即将漂移到的地点!");
							toUeserItem.setLocateType(PangkerConstant.LOCATE_DRIFT);
						} else {
							toUeserItem.setLocateType(PangkerConstant.LOCATE_CLIENT);
						}

						Intent intent = new Intent();
						toUeserItem.setLatitude(Double.parseDouble(result.getLat()));
						toUeserItem.setLongitude(Double.parseDouble(result.getLon()));

						intent.putExtra("userinfo", toUeserItem);
						intent.setClass(this, MapLocationActivity.class);
						launch(intent, this);
					}
				}
			} else if (result != null && (result.getErrorCode() == BaseResult.FAILED || result.getErrorCode() == 2)) {
				showToast(result.getErrorMessage());
			} else
				showToast(R.string.return_value_999);
		} else if (caseKey == DRIF_CHECK) {
			LocationQueryCheckResult result = JSONUtil.fromJson(response.toString(), LocationQueryCheckResult.class);
			if (result != null) {
				if (result.getErrorCode() == BaseResult.FAILED) {// 未通过
					showToast(result.getErrorMessage());
				} else if (result.getErrorCode() == BaseResult.SUCCESS) {// 通过
					// 判断位置信息是否有效
					if (result.getLat() != null && !result.getLat().equals("") && result.getLon() != null
							&& !result.getLon().equals("")) {
						application.setLocateType(PangkerConstant.LOCATE_DRIFT);// 改变定位类型
						application.setDriftUser(toUeserItem);

						showToast("漂移成功!");
						application.setDirftLocation(new Location(Double.valueOf(result.getLon()), Double
								.valueOf(result.getLat())));
						// 发生消息给HomeActivity改变漂移状态
						Message msg = new Message();
						msg.what = PangkerConstant.DRIFT_NOTICE;
						PangkerManager.getTabBroadcastManager().sendResultMessage(MainActivity.class.getName(), msg);
						PangkerManager.getTabBroadcastManager().sendResultMessage(HomeActivity.class.getName(), msg);
						Intent intent = new Intent(UserWebSideActivity.this, PangkerService.class);
						intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DRIFT_START);
						startService(intent);
					} else
						showToast("漂移失败");

				} else if (result.getErrorCode() == 2) {// 需要对方同意
					// 未实现
				} else {
					showToast(R.string.return_value_999);
				}
			} else {
				showToast(R.string.return_value_999);
			}
		} else if (caseKey == CANCELDRIFT) {
			BaseResult cancelresult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (cancelresult == null) {
				return;
			}
			if (cancelresult.getErrorCode() == BaseResult.SUCCESS) {
				application.setLocateType(PangkerConstant.LOCATE_CLIENT);// 变更漂移状态
				// application.getDriftListener().driftEnd();// 取消漂移
				application.setDriftUser(null);
			}
		}
		// 添加用户到黑名单
		else if (DIALOG_BLACKLIST == caseKey || DELE_BLACKLIST_FRIEND == caseKey || DELE_BLACKLIST_ATTENTION == caseKey
				|| DELE_BLACKLIST_ATTENTION_IMPLICIT == caseKey) {
			BlackManagerResult blackResult = JSONUtil.fromJson(response.toString(), BlackManagerResult.class);
			if (null == blackResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (blackResult.getErrorCode() == BaseResult.SUCCESS) {
				if (DELE_BLACKLIST_FRIEND == caseKey) {
					iBlacklistDao.removeUser(toUeserItem.getUserId());
					addFriend();
				} else if (DELE_BLACKLIST_ATTENTION == caseKey) {
					iBlacklistDao.removeUser(toUeserItem.getUserId());
					addAttention("1", ADDATTENT_CASEKEY);
				} else if (DELE_BLACKLIST_ATTENTION_IMPLICIT == caseKey) {
					iBlacklistDao.removeUser(toUeserItem.getUserId());
					addAttention("0", IMPLICIT_ADDATTENT_CASEKEY);
				} else {
					showToast(blackResult.getErrorMessage());
					iBlacklistDao.insertUser(toUeserItem.getUserId());
					OpenfireManager openfireManager = application.getOpenfireManager();
					openfireManager.sendContactsChangedPacket(userId, toUeserItem.getUserId(), BusinessType.delattent);
					userListenerManager.refreshUI(ContactAttentionActivity.class.getName(),
							PangkerConstant.STATE_CHANGE_REFRESHUI);
					openfireManager.sendContactsChangedPacket(userId, toUeserItem.getUserId(), BusinessType.delfans);
					userListenerManager.refreshUI(ContactFollowActivity.class.getName(),
							PangkerConstant.STATE_CHANGE_REFRESHUI);
					if (application.IsFriend(toUeserItem.getUserId())) {
						boolean delefriendSuccess = openfireManager.removefriend(openfireManager.getConnection()
								.getRoster(), toUeserItem.getUserId());
						if (delefriendSuccess) {
							iFriendsDao.delFriend(toUeserItem.getUserId(), PangkerConstant.DEFAULT_GROUPID);
							openfireManager.sendContactsChangedPacket(userId, toUeserItem.getUserId(),
									BusinessType.delfriend);
							userListenerManager.refreshUI(ContactActivity.class.getName(),
									PangkerConstant.STATE_CHANGE_REFRESHUI);
						}
					}

				}
			} else if (blackResult.getErrorCode() == BaseResult.FAILED) {
				if (DIALOG_BLACKLIST == caseKey) {
					showToast(blackResult.getErrorMessage());
				}
			}
		} else if (caseKey == LOAD_USERINFO) {
			result = JSONUtil.fromJson(response.toString(), UserBasicInfoQueryResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				toUeserItem.setUserName(result.getUsername());
				toUeserItem.setSign(result.getSign());
				nickname.setText(result.getUsername());
				sign.setText(toUeserItem.getSign());
				// // 旁客号是否存在
				if (!Util.isEmpty(result.getAccount())) {
					ly_pk_num.setVisibility(View.VISIBLE);
					tv_pangker_num.setText(result.getAccount());
				}
				TextView tvAttendNum = (TextView) findViewById(R.id.attent_num);
				TextView tvFanNum = (TextView) findViewById(R.id.fan_num);
				tvAttendNum.setText("(" + result.getAttentCount() + ")");
				tvFanNum.setText("(" + result.getFansCount() + ")");
				if (result.getIsFriend() != null && result.getIsFriend() == 1) {
					btn_addfriend.setBackgroundResource(R.drawable.contentinf_but_add_ed);
					btn_addfriend.setEnabled(false);
				}
				if (result.getIsAttent() != null && result.getIsAttent() == 1) {
					btn_tofans.setBackgroundResource(R.drawable.contentinf_but_care_ed);
					btn_tofans.setEnabled(false);
				}
				if (result.getVipLevel() != null) {
					if (result.getVipLevel() == 1) {
						rating_bar_show.setNumStars(2);
					} else if (result.getVipLevel() == 2) {
						rating_bar_show.setNumStars(3);
					}
				}
				if (result.getRecentUsers() != null && result.getRecentUsers().size() > 0) {
					recentListView = (com.wachoo.pangker.ui.HorizontalListView) findViewById(R.id.recentUsers);
					recentUsers = new RecentUserAdapter(this, result.getRecentUsers());
					recentListView.setAdapter(recentUsers);
					recentListView.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
							// TODO Auto-generated method stub
							UserItem userItem = (UserItem) arg0.getItemAtPosition(arg2);
							if (userItem.getUserId().equals(userId)) {
								showToast(R.string.isyourself);
							} else {
								Intent intent = new Intent(UserWebSideActivity.this, UserWebSideActivity.class);
								intent.putExtra(UserItem.USERID, userItem.getUserId());
								intent.putExtra(UserItem.USERNAME, userItem.getUserName());
								intent.putExtra(START_FORM, START_FORM_SELF);
								startActivity(intent);
							}

						}
					});
				}

			} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
				finish();
			} else {
				showToast(R.string.load_userinfo_failed);
			}
		} else if (caseKey == COUNT_USER_CASEKEY) {
			CountQueryResult result = JSONUtil.fromJson(response.toString(), CountQueryResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				TextView tvAttendNum = (TextView) findViewById(R.id.attent_num);
				TextView tvFanNum = (TextView) findViewById(R.id.fan_num);
				tvAttendNum.setText("(" + result.getAttentCount() + ")");
				tvFanNum.setText("(" + result.getFansCount() + ")");
			}
			// else if(result != null && result.getErrorCode() ==
			// result.FAILED){
			// showToast(result.getErrorMessage());
			// }
		}
	}
}
