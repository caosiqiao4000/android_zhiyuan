package com.wachoo.pangker.activity;

import java.util.List;

import android.app.ActivityManager;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.ActivityStackManager;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.contact.ContactActivity;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.activity.group.MyGroupListsActivity;
import com.wachoo.pangker.activity.msg.MsgInfoActivity;
import com.wachoo.pangker.activity.pangker.PangkerActivity;
import com.wachoo.pangker.activity.res.ResActivity;
import com.wachoo.pangker.activity.res.ResLocalActivity;
import com.wachoo.pangker.activity.res.ResLocalAppActivity;
import com.wachoo.pangker.activity.res.ResLocalDocActivity;
import com.wachoo.pangker.activity.res.ResLocalMusicActivity;
import com.wachoo.pangker.activity.res.UploadPicActivity;
import com.wachoo.pangker.activity.res.UploadSpeakActivity;
import com.wachoo.pangker.activity.root.PangkerAccountActivity;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.chat.ILoggedChecker;
import com.wachoo.pangker.chat.ILoginIMServerThreadManager;
import com.wachoo.pangker.chat.IMessageListener;
import com.wachoo.pangker.chat.MessageNotification;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.map.app.LocateManager;
import com.wachoo.pangker.server.HttpReqCode;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.service.MeetRoomService;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.service.PangkerService.PangkerServiceBinder;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.BadgeView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.ConfrimDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;
import com.wifi.locate.manager.WifiLocateManager;

/**
 * MainActivity
 * 
 * @author wangxin
 */
public class MainActivity extends TabActivity implements ITabSendMsgListener, ILoggedChecker {

	int[] ids = { R.id.radio_message, R.id.radio_contacts, R.id.radio_search, R.id.radio_news, R.id.radio_home };
	private String TAG = "MainActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	/** Called when the activity is first created. */
	private TabHost mth;
	private String tabHome;
	private String tabConversation;
	private String tabChat;
	private String tabReveiceMessage;
	private String tabPangker;
	private String tabRes;
	private String tabContacts;
	private String tabDisconnectNonetwork;// 没有tab_disconnect_nonetwork;
	private String tabDisConnected;// >>>>>>>>>>断开连接
	private ProgressBar mReceiveMessageBar;

	private RadioButton[] radioButton = new RadioButton[5];
	private PKIconResizer mImageResizer;
	private TextView txtTab;
	private TextView txtUserName;

	private ImageView imgGroup;
	private ImageView imgUserIcon;

	private PangkerApplication application;
	private ActivityStackManager stackManager;

	private MainReceiver mainReceiver;
	private IntentFilter filter;

	private QuickAction quickAction;
	private ImageButton btnMenu;

	private boolean isStopPangkerService = false; // 是否停止服务
	private BadgeView imgUnread;
	private ImageView ivDirft;
	// 资源上传的菜单
	private PopMenuDialog popMenuDialog;
	private int tabIndex = 2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		application = (PangkerApplication) getApplication();
		// >>>>获取当前用户信息
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		// >>>>>>>>>绑定启动service
		bindService(new Intent(MainActivity.this, PangkerService.class), conn, BIND_AUTO_CREATE);
		if (savedInstanceState != null) {
			tabIndex = savedInstanceState.getInt("TABINDEX");
		}
		init();
		initActivity();// 初始化Main界面
		registerReceiver();
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC); // 让音量键固定为媒体音量控制

		// 检测版本
		// checkVersion();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		ActivityManager activityManger = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> list = activityManger.getRunningAppProcesses();
		if (list != null)
			for (int i = 0; i < list.size(); i++) {
				ActivityManager.RunningAppProcessInfo apinfo = list.get(i);
				if ((apinfo.processName.contains("ccom.wachoo.pangker.activity"))) {
					apinfo.importance = 100;// 这样子就非常安全了
				}
			}

	}

	/**
	 * 初始化
	 */
	private void init() {
		// TODO Auto-generated method stub
		tabHome = getResources().getString(R.string.tab_home);
		tabReveiceMessage = getResources().getString(R.string.tab_receive_message);
		tabChat = getResources().getString(R.string.tab_conversation);
		tabConversation = getResources().getString(R.string.tab_conversation);
		tabPangker = getResources().getString(R.string.tab_pangker);
		tabRes = getResources().getString(R.string.tab_res);
		tabContacts = getResources().getString(R.string.tab_contacts);
		tabDisconnectNonetwork = getResources().getString(R.string.tab_disconnect_nonetwork);
		tabDisConnected = getResources().getString(R.string.tab_disconnected);

		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		stackManager = PangkerManager.getActivityStackManager();

		PangkerManager.getTabBroadcastManager().addMsgListener(this);
		stackManager.pushActivity(MainActivity.this);

		// 存放监听
		application.setiLoggedChecker(this);
		PangkerManager.getMessageChangedManager().addListenter(messageListener);
	}

	public void reSetMsgcount() {
		application.setUnReadMessageCount(0);
		showMsgIcon(application.getUnReadMessageCount());
	}

	public void showMsgIcon(int unreadcount) {
		if (unreadcount > 0) {
			imgUnread.show();
			imgUnread.setText(String.valueOf(unreadcount));
		} else {
			imgUnread.hide();
		}
	}

	// 检查IM账号，指纹账号 add by lb
	public void checkImAccount() {
		if (Util.isEmpty(application.getImAccount())) {
			radioButton[radioButton.length - 1].setBackgroundResource(R.drawable.main_nav_home_7_bg);
		} else {
			radioButton[radioButton.length - 1].setBackgroundResource(R.drawable.main_nav_home_bg);
		}
	}

	private void initTitle() {
		// TODO Auto-generated method stub
		mReceiveMessageBar = (ProgressBar) findViewById(R.id.pb_receive_message);
		txtTab = (TextView) findViewById(R.id.tab_title);

		imgUserIcon = (ImageView) findViewById(R.id.usericon);
		mImageResizer.loadImage(getMyUserInfo().getUserId(), imgUserIcon);
		txtUserName = (TextView) findViewById(R.id.username);

		btnMenu = (ImageButton) findViewById(R.id.locate);
		btnMenu.setOnClickListener(onClickListener);
		ivDirft = (ImageView) findViewById(R.id.img_drift);

		// >>>>>>>>>主界面返回当前群组图标
		imgGroup = (ImageView) findViewById(R.id.img_group);
		imgGroup.setVisibility(View.GONE);
		imgGroup.setOnClickListener(onClickListener);

		imgUserIcon.setOnClickListener(onClickListener);

		quickAction = new QuickAction(this, QuickAction.VERTICAL, QuickAction.COLOR_BLACK);
		quickAction
				.addActionItem(new ActionItem(6, "发布说说", ActionItem.buildDrawable(this, R.drawable.popmenu_but_talk)));
		quickAction
				.addActionItem(new ActionItem(2, "拍照", ActionItem.buildDrawable(this, R.drawable.popmenu_but_camera)));
		quickAction.addActionItem(new ActionItem(3, "上传资源", ActionItem.buildDrawable(this,
				R.drawable.popmenu_but_upload)));
		quickAction.addActionItem(new ActionItem(4, "召开会议", ActionItem.buildDrawable(this,
				R.drawable.popmenu_but_meeting)));
		quickAction.addActionItem(new ActionItem(1, "定位", ActionItem.buildDrawable(this, R.drawable.popmenu_but_gps)),
				true);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);
	}

	protected void initTitleDate() {
		setUserIcon(SET_USERICON);
		txtUserName.setText(Util.getStringByLength(getMyUserInfo().getUserName(), 10));
		String[] tabs = { tabConversation, tabContacts, tabPangker, tabRes, tabHome };
		Log.e(TAG, "apinfo.initTitleDate=" + tabIndex + ">>>>" + tabs[tabIndex]);
		txtTab.setText(tabs[tabIndex]);
		if (tabIndex == 0) {
			hideUserinfo(false);
		}
	}

	// >>>>>>>>>>登陆到openfire服务器监控
	private ILoginIMServerThreadManager mImServerThreadManager;
	// >>>>>>>>>>>>> 绑定PangkerService
	private ServiceConnection conn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			Log.e(TAG, ">>>>>>>>>>onServiceConnected");
			// >>>获取监听管理器
			mImServerThreadManager = ((PangkerServiceBinder) service).getILoginIMServerThreadManager();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			Log.e(TAG, ">>>>>>>>>>onServiceDisconnected");
		}
	};

	private View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			hideSoftInput(v);
			if (v == imgUserIcon) {
				goHome();
			}
			if (v == btnMenu) {
				quickAction.show(btnMenu);
			}
			if (v == imgGroup) {
				goChatRoom();
			}
		}
	};

	protected void hideSoftInput(View v) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	ActionItem[] msgMenu = new ActionItem[] { new ActionItem(1, "图片上传"), new ActionItem(2, "音乐上传"),
			new ActionItem(3, "应用上传"), new ActionItem(4, "阅读上传") };

	private void showPopMenu() {
		// TODO Auto-generated method stub
		if (popMenuDialog == null) {
			popMenuDialog = new PopMenuDialog(this);
			popMenuDialog.setListener(mClickListener);
		}
		popMenuDialog.setMenus(msgMenu);
		popMenuDialog.setTitle("资源上传");
		popMenuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			if (actionId == 1) {// 图片上传
				Intent intent = new Intent(MainActivity.this, UploadPicActivity.class);
				intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.upLoad);
				startActivity(intent);
			} else if (actionId == 2) {
				Intent intent = new Intent(MainActivity.this, ResLocalMusicActivity.class);
				intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.upLoad);
				startActivity(intent);
			} else if (actionId == 3) {
				Intent intent = new Intent(MainActivity.this, ResLocalAppActivity.class);
				intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.upLoad);
				startActivity(intent);
			} else if (actionId == 4) {
				Intent intent = new Intent(MainActivity.this, ResLocalDocActivity.class);
				intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.upLoad);
				startActivity(intent);
			}
			popMenuDialog.dismiss();
		}
	};

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 1) {
				switch (application.getLocateType()) {
				case PangkerConstant.LOCATE_CLIENT:
					Intent intent2 = new Intent(MainActivity.this, PangkerService.class);
					intent2.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.LOCATE_INTENT);
					startService(intent2);
					Toast.makeText(MainActivity.this, "定位中...", Toast.LENGTH_SHORT).show();
					break;
				case PangkerConstant.LOCATE_DRIFT:
					// 否：不做任何改变
					// showToast("您正在漂移不能终端定位");
					MessageTipDialog messageTipDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {

						@Override
						public void buttonResult(boolean isSubmit) {
							// TODO Auto-generated method stub
							if (isSubmit) {
								Intent intent = new Intent(MainActivity.this, PangkerService.class);
								intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DRIFT);
								startService(intent);
							}
						}
					}, MainActivity.this);

					messageTipDialog.showDialog("您正在漂移,是否取消漂移并定位?", false);

					break;
				}
			} else if (actionId == 2) {// 拍照上传
				Intent intent = new Intent(MainActivity.this, UploadPicActivity.class);
				intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.upLoad);
				intent.putExtra(UploadPicActivity.ONLY_CAMERA_KEY, true);
				startActivity(intent);
			} else if (actionId == 3) {// 资源上传
				showPopMenu();
			} else if (actionId == 4) {// 群组
				Intent intent = new Intent(MainActivity.this, MyGroupListsActivity.class);
				startActivity(intent);
			} else if (actionId == 6) {// 发表说说
				Intent intent = new Intent(MainActivity.this, UploadSpeakActivity.class);
				MainActivity.this.startActivity(intent);
			}
		}
	};

	public void showToast(final int toastText) {
		Toast.makeText(MainActivity.this, toastText, Toast.LENGTH_SHORT).show();
	}

	public boolean HttpResponseStatus(Object supportResponse) {
		if (supportResponse == null) {
			showToast(R.string.to_server_fail);
			return false;
		}
		if (supportResponse instanceof HttpReqCode) {
			HttpReqCode httpReqCode = (HttpReqCode) supportResponse;
			if (httpReqCode.equals(HttpReqCode.no_network)) {
				showToast("请检查您的网络！");
			} else if (httpReqCode.equals(HttpReqCode.error)) {
				showToast(R.string.to_server_fail);
			}
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 进入主页
	 */
	private void goHome() {
		mth.setCurrentTabByTag(tabHome);
		txtTab.setText(tabHome);
		hideUserinfo(true);
		// radioGroup.check(R.id.radio_search);
		for (int i = 0; i < ids.length; i++) {
			radioButton[i].setChecked(false);
		}
		radioButton[4].setChecked(true);
	}

	/**
	 * 进入群聊界面
	 */
	private void goChatRoom() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ChatRoomMainActivity.class);
		startActivity(intent);
	}

	/**
	 * @author wangxin 2012-3-13 上午11:27:33
	 */
	private void stopService() {
		// >>>>初始化音乐service意图
		Intent musicService = new Intent(MainActivity.this, MusicService.class);
		stopService(musicService);

		// >>>群组服务
		Intent meetroomService = new Intent(MainActivity.this, MeetRoomService.class);
		stopService(meetroomService);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Log.i(TAG, "main--onDestroy");
		super.onDestroy();
		try {
			unregisterReceiver();

			if (!isStopPangkerService) {
				// >>>>解绑成功后停止服务
				Intent pangkerServce = new Intent(MainActivity.this, PangkerService.class);
				stopService(pangkerServce);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void unregisterReceiver() {
		// TODO Auto-generated method stub
		unregisterReceiver(mainReceiver);
	}

	private void registerReceiver() {
		// TODO Auto-generated method stub
		mainReceiver = new MainReceiver();
		filter = new IntentFilter();

		filter.addAction(PangkerConstant.ACTTION_LOCATE_STATE);// 定位状态
		filter.addAction(PangkerConstant.ACTTION_ICON);// 用户图像修改

		filter.addAction(PangkerConstant.ACTION_GROUPLOGIN);// 群组运行
		filter.addAction(PangkerConstant.ACTION_GROUPLOGOUT);// 群组运行
		registerReceiver(mainReceiver, filter);
	}

	View.OnClickListener checkOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for (int i = 0; i < ids.length; i++) {
				radioButton[i].setChecked(false);
			}
			((RadioButton) v).setChecked(true);
			switch (v.getId()) {
			case R.id.radio_message:
				tabIndex = 0;
				mth.setCurrentTabByTag(tabChat);
				txtTab.setText(tabConversation);
				hideUserinfo(false);
				break;
			case R.id.radio_contacts:
				tabIndex = 1;
				mth.setCurrentTabByTag(tabContacts);
				txtTab.setText(tabContacts);
				hideUserinfo(true);
				break;
			case R.id.radio_search:
				tabIndex = 2;
				mth.setCurrentTabByTag(tabPangker);
				txtTab.setText(tabPangker);
				hideUserinfo(true);
				break;
			case R.id.radio_news:
				tabIndex = 3;
				mth.setCurrentTabByTag(tabRes);
				txtTab.setText(tabRes);
				hideUserinfo(true);
				break;
			case R.id.radio_home:
				tabIndex = 4;
				mth.setCurrentTabByTag(tabHome);
				txtTab.setText(tabHome);
				hideUserinfo(true);
				break;
			}
			if (v != radioButton[0]) {
				mReceiveMessageBar.setVisibility(View.GONE);
			}
		}
	};

	/**
	 * initActivity
	 */
	private void initActivity() {
		// TODO Auto-generated method stub
		mth = this.getTabHost();

		TabSpec ts3 = mth.newTabSpec(tabPangker).setIndicator(tabPangker);
		ts3.setContent(new Intent(MainActivity.this, PangkerActivity.class));
		mth.addTab(ts3);

		TabSpec ts1 = mth.newTabSpec(tabChat).setIndicator(tabChat);
		ts1.setContent(new Intent(MainActivity.this, MsgInfoActivity.class));
		mth.addTab(ts1);

		TabSpec ts2 = mth.newTabSpec(tabContacts).setIndicator(tabContacts);
		ts2.setContent(new Intent(MainActivity.this, ContactActivity.class));
		mth.addTab(ts2);

		TabSpec ts4 = mth.newTabSpec(tabRes).setIndicator(tabRes);
		ts4.setContent(new Intent(MainActivity.this, ResActivity.class));
		mth.addTab(ts4);

		TabSpec ts5 = mth.newTabSpec(tabHome).setIndicator(tabHome);
		ts5.setContent(new Intent(MainActivity.this, HomeActivity.class));
		mth.addTab(ts5);

		mth.setCurrentTabByTag(tabPangker);

		for (int i = 0; i < ids.length; i++) {
			radioButton[i] = (RadioButton) findViewById(ids[i]);
			radioButton[i].setOnClickListener(checkOnClickListener);
		}
		// imgUnread = (TextView) findViewById(R.id.img_unread);
		imgUnread = new BadgeView(this, radioButton[0]);
		imgUnread.setBackgroundResource(R.drawable.talk_list_pushbox);
		imgUnread.setGravity(Gravity.CENTER);
		showMsgIcon(application.getUnReadMessageCount());

		initTitle();

		// 检查IM账号，指纹账号
		checkImAccount();

		// 判断是否漂移
		adjustDrift();

		checkWebsiteNameAndPromt();
	}

	private void adjustDrift() {
		if (application.getLocateType() == PangkerConstant.LOCATE_DRIFT && application.getDriftUser() != null) {
			ivDirft.setVisibility(View.VISIBLE);
		} else {
			ivDirft.setVisibility(View.GONE);
		}
	}

	/**
	 * 检测VIP用户是否已经设置网站名称，如果没有则予以提示信息。
	 */
	private void checkWebsiteNameAndPromt() {
		UserInfo userInfo = application.getMySelf();
		if (Util.isEmpty(userInfo.getWebsiteName()) && userInfo.getVipLevel() > 1000) {
			SharedPreferencesUtil spu = new SharedPreferencesUtil(this);
			if(!spu.getBoolean(PangkerConstant.SP_WEB_SET + application.getMyUserID(), false, true)){
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							Intent intent = new Intent();
							intent.setClass(MainActivity.this, PangkerAccountActivity.class);
							startActivity(intent);
						}
					}
				}, this);
				dialog.setButtonName("设置", "取消");
				dialog.showDialog(MessageTipDialog.TYPE_WEBSET, "","您还没有设置网站名称哦！", true);
			}
		}
	}

	private void checkVersion() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(MainActivity.this, PangkerService.class);
		intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_VERSION);
		startService(intent);
	}

	private void hideUserinfo(boolean flag) {
		if (flag) {
			findViewById(R.id.infolayout).setVisibility(View.INVISIBLE);
		} else {
			findViewById(R.id.infolayout).setVisibility(View.VISIBLE);
		}
	}

	private class MainReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();
			// 定位信息
			if (PangkerConstant.ACTTION_LOCATE_STATE.equals(action)) {
				dealLocation(intent);
			}// 图片修改
			else if (PangkerConstant.ACTTION_ICON.equals(action)) {
				setUserIcon(REFRESH_USERICON);
			} else if (action.equals(PangkerConstant.ACTION_GROUPLOGIN)) {

				imgGroup.setVisibility(View.VISIBLE);

			} else if (action.equals(PangkerConstant.ACTION_GROUPLOGOUT)) {
				imgGroup.setVisibility(View.GONE);
			}
		}
	};

	private void changeAccount() {
		if ((application.getDownLoadManager() != null && application.getDownLoadManager().haveDownloadingJob())
				|| (application.getUpLoadManager() != null && application.getUpLoadManager().haveUploadingJob())) {
			showLogout(1);
		} else {
			Logout(false);
			Intent intent = new Intent(MainActivity.this, LoginByFingerprint.class);
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, Menu.FIRST + 1, Menu.NONE, "退出").setIcon(android.R.drawable.ic_menu_close_clear_cancel);
		menu.add(Menu.NONE, Menu.FIRST + 2, Menu.NONE, "切换账号").setIcon(R.drawable.setmenu_changeid);
		return true;
	}

	private void showLogout(int type) {
		// TODO Auto-generated method stub
		ConfrimDialog logoutDialog = new ConfrimDialog(this);
		if (type == 0) {
			String dialogMsg = checkManager("退出旁客");
			logoutDialog.showConfrimDialog("退出", dialogMsg, new ConfrimDialog.OnDialogClickListener() {
				@Override
				public void onClick(Dialog dialog, boolean positive) {
					// TODO Auto-generated method stub
					Logout(true);
				}
			});
		} else if (type == 1) {
			String dialogMsg = checkManager("切换账号");
			logoutDialog.showConfrimDialog("切换账号", dialogMsg, new ConfrimDialog.OnDialogClickListener() {
				@Override
				public void onClick(Dialog dialog, boolean positive) {
					// TODO Auto-generated method stub
					Logout(true);
				}
			});
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case Menu.FIRST + 1:
			showLogout(0);
			break;
		case Menu.FIRST + 2:
			changeAccount();
			break;
		}
		// TODO Auto-generated method stub
		return true;
	}

	// >>>>>>>>>>>>设置当前用户头像
	private void setUserIcon(int type) {
		Message message = handler.obtainMessage(type);
		message.sendToTarget();
	}

	// >>>>>>>>>>>>显示提示位置信息
	private void dealLocation(Intent intent) {
		// TODO Auto-generated method stub
		int status = intent.getIntExtra(PangkerConstant.ACTTION_LOCATE_STATE, LocateManager.LOCATE_FAILED);
		Location location = (Location) intent.getSerializableExtra("Location");
		String msg = "";
		if (status == LocateManager.LOCATE_FAILED) {
			msg = "定位失败! 获取的预先制定的位置信息(" + location.getLongitude() + "," + location.getLatitude() + ")";
		}
		if (status == LocateManager.LOCATE_LAST) {
			msg = "定位失败! 获取上一次的位置信息(" + location.getLongitude() + "," + location.getLatitude() + ")";
		}
		if (status == LocateManager.LOCATE_SUCCESS) {
			msg = "定位成功! (" + location.getLongitude() + "," + location.getLatitude() + ")";
		}
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	private String checkManager(String outtype) {
		String dialogMsg = "你确认要" + outtype + "吗?";
		if ((application.getDownLoadManager() != null && application.getDownLoadManager().haveDownloadingJob())
				&& (application.getUpLoadManager() != null && application.getUpLoadManager().haveUploadingJob())) {
			dialogMsg = "您有正在上传和下载的任务,退出后将结束这些任务,你确认要" + outtype + "吗?";
			return dialogMsg;
		}
		if (application.getDownLoadManager() != null && application.getDownLoadManager().haveDownloadingJob()) {
			dialogMsg = "您有正在下载的任务,退出后将结束这些任务,你确认要" + outtype + "吗?";
			return dialogMsg;
		}
		if (application.getUpLoadManager() != null && application.getUpLoadManager().haveUploadingJob()) {
			dialogMsg = "您有正在上传的任务,退出后将结束这些任务,你确认要" + outtype + "吗?";
			return dialogMsg;
		}
		return dialogMsg;
	}

	static final int LOGIN_RESULT = 0x100;// 登录结果
	static final int SET_USERICON = 0x101;// 设置头像
	static final int REFRESH_USERICON = 0x102;// 重新获取头像
	static final int MUSIC_PLAYER = 0x103;
	static final int UNREAD_SMS_ADD = 0x104;
	static final int UNREAD_SMS_REMOVE = 0x105;
	private boolean lineStatus = false;// >>>>>>>>>当前显示 true：在线 false：离线
	private IMState mIMState = IMState.unLogged;

	enum IMState {
		connect, logged, unLogged, noNetwork
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message message) {
			switch (message.what) {
			// >>>>>>>连接会话服务器结果通知
			case LOGIN_RESULT:
				String login_result = message.obj.toString();
				// >>>>>>>>>>连接IM服务器成功
				if (PangkerConstant.OF_LOGIN_SUCESS.equals(login_result)) {
					lineStatus = true;
					if (tabIndex == 0) {
						tabConversation = tabChat;
						txtTab.setText(tabConversation);
					}
					Log.d("mReceiveMessageBar", "PangkerConstant.OF_LOGIN_SUCESS");
					mReceiveMessageBar.setVisibility(View.GONE);
				}
				// >>>>>>>>>>连接IM服务器失败
				else if (PangkerConstant.OF_LOGIN_FAIL.equals(login_result)) {
					lineStatus = false;
					if (tabIndex == 0) {
						tabConversation = tabDisConnected;
						txtTab.setText(tabDisConnected);
					}
					Log.d("mReceiveMessageBar", "PangkerConstant.OF_LOGIN_FAIL");
					mReceiveMessageBar.setVisibility(View.GONE);
				}

				// >>>>>>>>>>无网络
				else if (PangkerConstant.OF_LOGIN_NONETWORK.equals(login_result)) {
					if (tabIndex == 0) {
						tabConversation = tabDisconnectNonetwork;
						txtTab.setText(tabDisconnectNonetwork);
					}
					mReceiveMessageBar.setVisibility(View.GONE);
				}

				// >>>>>连接中
				else if (PangkerConstant.OF_LOGIN_LOGDING.equals(login_result)) {
					if (tabIndex == 0) {
						tabConversation = tabReveiceMessage;
						txtTab.setText(tabReveiceMessage);
						mReceiveMessageBar.setVisibility(View.VISIBLE);
					}
				}

				break;
			case SET_USERICON:
				// TODO Auto-generated method stub
				mImageResizer.loadImage(getMyUserInfo().getUserId(), imgUserIcon);
				break;
			case REFRESH_USERICON:
				// TODO Auto-generated method stub
				Log.e("MAINERROR", "mImageResizer=" + mImageResizer);
				Log.e("MAINERROR", "myself=" + getMyUserInfo().getUserId());
				mImageResizer.loadImageFromServer(getMyUserInfo().getUserId(), imgUserIcon);
				break;

			case UNREAD_SMS_ADD:
				int addCount = Integer.parseInt(message.obj.toString());
				Log.i("unreadmessage", "UNREAD_SMS_ADD>>>count=" + addCount);
				if (addCount > 0) {
					application.addUnReadMessage();
					showMsgIcon(application.getUnReadMessageCount());
				}
				break;
			case UNREAD_SMS_REMOVE:
				int removeCount = Integer.parseInt(message.obj.toString());
				Log.i("unreadmessage", "UNREAD_SMS_REMOVE>>>count=" + removeCount);
				if (removeCount > 0) {
					application.removeUnReadMessage(removeCount);
					showMsgIcon(application.getUnReadMessageCount());
				}
				break;
			default:
				break;
			}
		};
	};

	IMessageListener messageListener = new IMessageListener() {
		@Override
		public void messageChanged(MsgInfo msgInfo) {
			// TODO Auto-generated method stub
			if (msgInfo.getUncount() > 0)
				addUnReadMessage(msgInfo.getUncount());

		}
	};

	public void addUnReadMessage(int count) {
		Message msg = new Message();
		msg.what = UNREAD_SMS_ADD;
		msg.obj = count;
		handler.sendMessage(msg);
	}

	public void removeUnReadMessgae(int count) {
		Message msg = new Message();
		msg.what = UNREAD_SMS_REMOVE;
		msg.obj = count;
		handler.sendMessage(msg);
	}

	private void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onRestoreInstanceState(Bundle state) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(state);
		initTitleDate();
	}

	// check downing or uploading res
	// 退出的时候要停止上传下载
	public void Logout(boolean ifover) {
		// TODO Auto-generated method stu
		// 关闭自动定位
		WifiLocateManager mWifiLocateManager = new WifiLocateManager(getApplicationContext(),
				new IUICallBackInterface() {
					@Override
					public void uiCallBack(Object supportResponse, int caseKey) {
						// TODO Auto-generated method stub
						Log.e(TAG, "WifiLocateManager-->>>>>>>>>>stopWifiLocate");
					}
				});
		mWifiLocateManager.removeUserWifiInfo(getMyUserInfo().getUserId());

		// 停止所有的上传、下载任务
		if (application.getDownLoadManager() != null) {
			application.getDownLoadManager().stopAllDownloadingJobs();
		}
		// >>>>>>>>>取消所有上传任务
		if (application.getUpLoadManager() != null) {
			application.getUpLoadManager().cancelAllUploadingJobs();
		}
		// >>>>>>>登出群组服务器
		if (application.getCurrunGroup() != null && application.getmSpLoginManager() != null) {
			application.getmSpLoginManager().logout(true, "");
		}
		// >>>>解绑定之后，停止监听线程
		if (mImServerThreadManager != null) {
			mImServerThreadManager.stopThreadMonitor();
		}

		// 退出openfire
		application.getOpenfireManager().loginOut();
		
		
		Log.e(TAG, "ImServerThreadManager-->>>>>>>>>>stopThreadMonitor");

		// >>>>解绑成功后停止服务
		Intent pangkerServce = new Intent(MainActivity.this, PangkerService.class);
		stopService(pangkerServce);

		new LogoutTask().execute();
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		manager.killBackgroundProcesses(getPackageName());
		// 退出时清除私信通知
		if (application.getMsgInfoBroadcast() != null)
			application.getMsgInfoBroadcast().canceNotify();
		stackManager.popAllActivitys();

		// >>>>>>清空app数据
		application.clearData();
		// >>>>>>>>>移除音乐播放通知
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.cancel(MusicService.ID);

		// >>>>>>>>解绑操作
		unbindService(conn);
		PangkerManager.exitManager();
		if (ifover) {
			Log.d(TAG, "myPid:" + android.os.Process.myPid());
			android.os.Process.killProcess(android.os.Process.myPid());
			System.exit(0);
		}
		stopService();
		if (!isFinishing()) {
			finish();
		}
	}

	// >>>>>>>>>>>>> wangxin logout task
	private class LogoutTask extends AsyncTask<Object, Integer, Object> {
		@Override
		protected Object doInBackground(Object... params) {
			// TODO Auto-generated method stub
			try {
				if (application.ofIsInit()) {
					application.getOpenfireManager().loginOut();
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.error(TAG, e);
			}
			return true;
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// >>>>>>>>>去掉消息通知栏“未读消息的提示”
		// >>>>>>>>>移除音乐播放通知
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.cancel(MessageNotification.NOTIFICATION_FLAG);

	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		if (msg.what == PangkerConstant.DRIFT_NOTICE) {
			adjustDrift();
		}
	}

	// >>>>>>>>>>>>登录结果
	private void loginResult(String loginResult) {
		Message message = handler.obtainMessage(LOGIN_RESULT);
		message.obj = loginResult;
		message.sendToTarget();
	}

	@Override
	public boolean isShowLogged() {
		// TODO Auto-generated method stub
		return lineStatus;
	}

	@Override
	public void loggedNotice() {
		// TODO Auto-generated method stub
		mIMState = IMState.logged;
		// 如果没有显示在线
		if (!lineStatus) {
			loginResult(PangkerConstant.OF_LOGIN_SUCESS);
		}
	}

	@Override
	public void noLoggedNotice() {
		// TODO Auto-generated method stub
		mIMState = IMState.unLogged;
		loginResult(PangkerConstant.OF_LOGIN_FAIL);

	}

	@Override
	public void noNetworkNotice() {
		// TODO Auto-generated method stub
		mIMState = IMState.noNetwork;
		loginResult(PangkerConstant.OF_LOGIN_NONETWORK);
	}

	@Override
	public void connectingNotice() {
		// TODO Auto-generated method stub
		mIMState = IMState.connect;
		loginResult(PangkerConstant.OF_LOGIN_LOGDING);
	}

	private UserInfo getMyUserInfo() {
		return application.getMySelf();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putInt("TABINDEX", tabIndex);
		super.onSaveInstanceState(outState);
	}
}