package com.wachoo.pangker.activity.about;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;

/**
 * 使用协议
 * 
 * @author wangxin
 * 
 */
public class AgreementActivity extends CommonPopActivity {

	private TextView tv_readme;
	private Button btn_back;
	private TextView tv_agreement_title;
	public static String START_FLAG = "START_FLAG";
	public static int REGISTER = 0;
	public static int UPLOAD_RES = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.agreement);
		int startFlag = getIntent().getIntExtra(START_FLAG, REGISTER);
		tv_agreement_title = (TextView) findViewById(R.id.tv_agreement_title);
		tv_readme = (TextView) findViewById(R.id.txt_readme);
		if (startFlag == REGISTER) {
			tv_agreement_title.setText(getString(R.string.agreement));
			tv_readme.setText(getRawText(R.raw.readme));
		} else {
			tv_agreement_title.setText(getString(R.string.termsagreement));
			tv_readme.setText(getRawText(R.raw.readme));
		}

		btn_back = (Button) findViewById(R.id.register_back);

		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
