package com.wachoo.pangker.activity.about;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.api.VersionUpdateCheck;
import com.wachoo.pangker.api.VersionUpdateCheck.VersionCheckType;

public class PangkerWizardActivity extends CommonPopActivity {

	private LinearLayout wellcomeLayout;
	// >>>>>>>>>>>意见和建议
	private LinearLayout mFeedbackLayout;

	// >>>>>>>>旁客版本内容
	private LinearLayout mVersionLayout;
	// >>>>>>>当前版本号
	private TextView mTxtVersionTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pangker_wizard);

		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText(R.string.pangker_wizard);

		mTxtVersionTextView = (TextView) findViewById(R.id.tv_version);
		// >>>>>>>>>>>意见和建议
		mFeedbackLayout = (LinearLayout) findViewById(R.id.ly_feedback);
		// >>>>>>>>旁客版本内容
		mVersionLayout = (LinearLayout) findViewById(R.id.ly_verson);

		wellcomeLayout = (LinearLayout) findViewById(R.id.ly_res_welcome);

		mVersionLayout.setOnClickListener(onClickListener);
		mFeedbackLayout.setOnClickListener(onClickListener);
		wellcomeLayout.setOnClickListener(onClickListener);
		initData();
	}

	private void initData() {
		// TODO Auto-generated method stub
		mTxtVersionTextView.setText("旁客" + getVerson());
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.btn_back) {
				finish();
			}

			// >>>>>>>>>>意见和反馈
			else if (v == mFeedbackLayout) {
				Intent intent = new Intent();
				intent.setClass(PangkerWizardActivity.this, FeedbackActivity.class);
				startActivity(intent);
			}

			// >>>>>>版本更新检查
			else if (v == mVersionLayout) {
				VersionUpdateCheck updateCheck = new VersionUpdateCheck(PangkerWizardActivity.this,
						VersionCheckType.AUTO_CONTROL);
				updateCheck.versionUpdateCheck(true);
			} else if (v == wellcomeLayout) {
				Intent intent = new Intent(PangkerWizardActivity.this, PangkerIntroducedActivity.class);
				intent.putExtra("start_flag", 1);
				startActivity(intent);
			}
		}
	};

	// 收集设备信息
	private String getVerson() {
		PackageManager pm = getPackageManager();
		try {
			PackageInfo pi = pm.getPackageInfo(getPackageName(), PackageManager.GET_ACTIVITIES);
			if (pi != null) {
				return pi.versionName == null ? "not set" : pi.versionName;
			}
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "not set";
	}
}
