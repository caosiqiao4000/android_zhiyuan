package com.wachoo.pangker.activity.contact;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.activity.res.LocalContactRecommendActivity;
import com.wachoo.pangker.adapter.ContactSelectAdapter;
import com.wachoo.pangker.api.IAddressBookBind;
import com.wachoo.pangker.api.LoadLocalAddressBook;
import com.wachoo.pangker.api.LoadLocalAddressBook.ILoadLocalAddressBook;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.listener.OnArrowChang;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.FriendManagerResult;
import com.wachoo.pangker.server.response.InviteRegisterResult;
import com.wachoo.pangker.server.response.InviteResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.ContactLetterListView;
import com.wachoo.pangker.ui.ContactLetterListView.OnTouchingLetterChangedListener;
import com.wachoo.pangker.ui.QuickActionBar;
import com.wachoo.pangker.util.SMSUtil;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @author zhegnjy
 * 
 */
public class LocalContactsSelectActivity extends CommonPopActivity implements OnItemClickListener, IAddressBookBind,
		android.view.View.OnClickListener, IUICallBackInterface, ILoadLocalAddressBook {

	private String TAG = "LocalContactsActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();

	private Handler handler;
	private DisapearThread disapearThread;
	/** 标识List的滚动状态。 */
	private int scrollState;
	// private LocalContactsAdapter listAdapter;
	private ContactSelectAdapter contactSelectAdapter;
	private ListView listMain;
	private TextView txtOverlay;
	private TextView tvTitle;
	private WindowManager windowManager;
	private List<LocalContacts> allcontacts;
	private PangkerApplication application;
	private EditText etSearch; // 搜索框
	private ContactLetterListView mLetterListView;

	private HashMap<String, Integer> alphaIndexer;// 存放存在的汉语拼音首字母和与之对应的列表位置
	private String[] sections;// 存放存在的汉语拼音首字母
	private String userId;
	private String mPhoneNum;
	private final int ADD_TO_FRIEND = 0x100;
	private final int INVITE_TO_PANGKER = 0x101;
	private ServerSupportManager serverMana;

	private int fromType = 0;// 标识来自不同的功能界面
	private int intentType;

	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_client_contacts);

		fromType = this.getIntent().getIntExtra("fromType", 0);
		intentType = getIntent().getIntExtra("intentType", 0);
		handler = new Handler();
		serverMana = new ServerSupportManager(this, this);
		userId = ((PangkerApplication) getApplication()).getMyUserID();
		mPhoneNum = ((PangkerApplication) getApplication()).getMySelf().getPhone();
		alphaIndexer = new HashMap<String, Integer>();

		application = (PangkerApplication) getApplication();

		tvTitle = (TextView) findViewById(R.id.mmtitle);
		if (fromType == 1) {
			tvTitle.setText("资源推荐联系人选择");
		} else if (fromType == 2) {
			// 添加单位通讯录成员界面、根据手机号码搜索
			tvTitle.setText("联系人选择");
		} else
			tvTitle.setText("通讯录邀请");

		// 初始化首字母悬浮提示框
		txtOverlay = (TextView) LayoutInflater.from(this).inflate(R.layout.popup_char_hint, null);
		txtOverlay.setVisibility(View.INVISIBLE);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_APPLICATION,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
				PixelFormat.TRANSLUCENT);
		windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		windowManager.addView(txtOverlay, lp);

		mLetterListView = (ContactLetterListView) findViewById(R.id.lv_letter);
		mLetterListView.setOnTouchingLetterChangedListener(new LetterListViewListener());
		// listAdapter = new LocalContactsAdapter(this, allcontacts, this);
		listMain = (ListView) findViewById(R.id.listInfo);

		contactSelectAdapter = new ContactSelectAdapter(this, allcontacts);
		// 如何需求页面只要单选联系人，adapter不需要显示多选框控件
		if (fromType == 2) {
			contactSelectAdapter.setIfCheck(false);
		}
		listMain.setAdapter(contactSelectAdapter);
		listMain.setOnItemClickListener(this);

		disapearThread = new DisapearThread();

		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LocalContactsSelectActivity.this.finish();
			}
		});
		final OpenfireManager openfireManager = application.getOpenfireManager();

		if (fromType == 2) {
			findViewById(R.id.iv_more).setVisibility(View.GONE);
		} else {
			Button btnLeft = (Button) findViewById(R.id.iv_more);
			btnLeft.setBackgroundResource(R.drawable.btn_default_selector);
			btnLeft.setText(R.string.submit);
			btnLeft.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (contactSelectAdapter.getSelectMember() == null
							|| contactSelectAdapter.getSelectMember().size() <= 0) {
						showToast("请选择联系人!");
						return;
					}
					// 资源推荐
					if (fromType == 1) {
						Intent intent = new Intent(LocalContactsSelectActivity.this,
								LocalContactRecommendActivity.class);
						intent.putExtra("selectedUser", (Serializable) contactSelectAdapter.getSelectMember());
						ResShowEntity resShowEntity = (ResShowEntity) getIntent().getSerializableExtra(
								ResShowEntity.RES_ENTITY);
						intent.putExtra(ResShowEntity.RES_ENTITY, resShowEntity);
						if (intentType == 1) {
							setResult(RESULT_OK, intent);
						} else {
							startActivity(intent);
						}
						finish();
					} else if (fromType == 3) {
						String users = "";
						for (LocalContacts locals : contactSelectAdapter.getSelectMember()) {
							if (locals.getRelation() == LocalContacts.RELATION_CONTACTS) {
								if (Util.isEmpty(users)) {
									users = users + locals.getPhoneNum();
								} else
									users = users + "," + locals.getPhoneNum();
							}
						}
						invite2Panker(users);
						finish();

					}
					// 好友申请
					else {
						List<LocalContacts> contacts = new ArrayList<LocalContacts>();
						String users = "";
						for (LocalContacts locals : contactSelectAdapter.getSelectMember()) {
							if (locals.getRelation() == LocalContacts.RELATION_P_USER) {
								if (Util.isEmpty(users)) {
									users = users + locals.getUserName();
								} else
									users = users + "," + locals.getUserName();

								openfireManager.createEntry(locals.getUserId(), locals.getUserName(), null);
							} else if (locals.getRelation() != LocalContacts.RELATION_P_FRIEND) {
								contacts.add(locals);
							}
						}
						if (!Util.isEmpty(users)) {
							showToast("已经向旁客用户" + users + "申请好友");
						}

						if (contacts.size() > 0) {

							if (getIntent().getStringExtra("fromActivity") != null
									&& getIntent().getStringExtra("fromActivity").equals(
											"LocalContactFriendAddActivity")) {
								Intent intent = new Intent(LocalContactsSelectActivity.this,
										LocalContactFriendAddActivity.class);
								intent.putExtra("selectedUser", (Serializable) contacts);
								setResult(RESULT_OK, intent);
							} else {
								Intent intent = new Intent(LocalContactsSelectActivity.this,
										LocalContactFriendAddActivity.class);
								intent.putExtra("selectedUser", (Serializable) contacts);
								startActivity(intent);
							}
							finish();

						}
					}

				}
			});
		}

		Button btnsearchLeft = (Button) findViewById(R.id.btn_left);
		btnsearchLeft.setVisibility(View.GONE);
		etSearch = (EditText) findViewById(R.id.et_search);
		etSearch.addTextChangedListener(editTextChangeListener);
		ImageView ivClear = (ImageView) findViewById(R.id.btn_clear);
		ivClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				etSearch.setText("");
			}
		});
		LinearLayout search_view = (LinearLayout) findViewById(R.id.contact_view);
		search_view.setVisibility(View.VISIBLE);

		if (application.isLocalContactsInit()) {
			loadAddressbookOK();
		} else {
			new LoadLocalAddressBook(this, this).execute();
		}
	}

	/** OnItemClickListener */
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		if (fromType == 2) {
			LocalContacts contact = (LocalContacts) parent.getAdapter().getItem(position);
			if (contact.getPhoto() != null) {
				contact.setPhoto(null);
			}
			Intent intent = new Intent();
			intent.putExtra(LocalContacts.LOCALCONTACTS_KEY, contact);
			LocalContactsSelectActivity.this.setResult(RESULT_OK, intent);
			LocalContactsSelectActivity.this.finish();
		} else {
			CheckBox cb = (CheckBox) view.findViewById(R.id.mtrace_checkbox);
			contactSelectAdapter.selectByIndex(position, !cb.isChecked());
			cb.setChecked(!cb.isChecked());
		}

	}

	/**
	 * onClick into 按钮点击事件
	 */
	public void onClick(View view) {
		if (view instanceof ImageView) {
			((ImageView) view).setImageResource(R.drawable.content_list_but_popmenu_arrow_p);
			int position = ((Integer) view.getTag()).intValue();
			LocalContacts localCon = contactSelectAdapter.getSelectMember().get(position); // showcontacts.get(position);
			QuickActionBar qaBar = new QuickActionBar(view, position);
			qaBar.setEnableActionsLayoutAnim(true);

			// 旁客用户
			if (localCon.getRelation() == LocalContacts.RELATION_P_USER) {

				ActionItem actAddFriend = new ActionItem(getResources().getDrawable(R.drawable.ib_add), getResources()
						.getString(R.string.add_friend), this);

				qaBar.addActionItem(actAddFriend);

			}
			// 好友
			else if (localCon.getRelation() == LocalContacts.RELATION_P_FRIEND) {

				ActionItem actLookUserInfo = new ActionItem(getResources().getDrawable(
						R.drawable.comp_list_menubut_inf_p), getResources().getString(R.string.userinfo), this);
				qaBar.addActionItem(actLookUserInfo);

				ActionItem actTalk = new ActionItem(getResources().getDrawable(android.R.drawable.stat_notify_chat),
						getResources().getString(R.string.msg_talk), this);
				qaBar.addActionItem(actTalk);

			}
			// 非旁客用户
			else {
				// 邀请开通
				ActionItem inviteToPangker = new ActionItem(getResources().getDrawable(
						R.drawable.commonbar_but_addcontents), getResources().getString(R.string.invite_to_pangker),
						this);
				qaBar.addActionItem(inviteToPangker);
			}
			ActionItem actCallUser = new ActionItem(getResources().getDrawable(R.drawable.comp_list_menubut_call),
					getResources().getString(R.string.call), this);
			ActionItem actSendMessage = new ActionItem(getResources().getDrawable(android.R.drawable.sym_action_email),
					getResources().getString(R.string.message), this);
			qaBar.addActionItem(actCallUser);
			qaBar.addActionItem(actSendMessage);
			qaBar.setOnDismissListener(new OnArrowChang(view));
			qaBar.show();
		} else if (view instanceof LinearLayout) {
			// ActionItem组件
			LinearLayout actionsLayout = (LinearLayout) view;
			QuickActionBar bar = (QuickActionBar) actionsLayout.getTag();
			bar.dismissQuickActionBar();
			int listItemIdx = bar.getListItemIndex();
			TextView txtView = (TextView) actionsLayout.findViewById(R.id.tv_title);
			String actionName = txtView.getText().toString();
			LocalContacts contacts = allcontacts.get(listItemIdx);// showcontacts.get(listItemIdx);
			String personalName = contacts.getUserName();
			// String url = personalName.replace(" ", "%20");
			// 查看用户信息
			if (actionName.equals(getResources().getString(R.string.userinfo))) {
				showInfo(contacts);
			}
			// 发送私信
			else if (actionName.equals(getResources().getString(R.string.msg_talk))) {
				// go2Web(url);
				sendMsgTalk(contacts);

			}
			// 邀请注册旁客
			else if (actionName.equals(getResources().getString(R.string.invite_to_pangker))) {
				invite2Panker(contacts.getPhoneNum());
			}
			// 打电话
			else if (actionName.equals(getResources().getString(R.string.call))) {
				callContact(contacts);
			}
			// 添加好友
			else if (actionName.equals(getResources().getString(R.string.add_friend))) {
				// friendManager(contacts.getUserid());
				addFriend(contacts);

			} else if (actionName.equals(getResources().getString(R.string.message))) {
				Uri smsToUri = Uri.parse("smsto:" + contacts.getPhoneNum());
				Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
				startActivity(mIntent);
			}
		}
	}

	/**
	 * 
	 * @param name
	 * @param url
	 * @author wangxin 2012-3-19 下午01:46:51
	 */
	public void showInfo(LocalContacts user) {
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra("userid", user.getUserId());
		intent.putExtra("username", user.getUserName());
		intent.putExtra("usersign", user.getSign());
		startActivity(intent);
	}

	/**
	 * 发送私信
	 * 
	 * @param contacts
	 *            void
	 */
	private void sendMsgTalk(LocalContacts contacts) {
		Intent intent = new Intent(this, MsgChatActivity.class);
		intent.putExtra("userid", contacts.getUserId());
		String username = contacts.getrName() != null ? contacts.getrName() : contacts.getUserName();
		intent.putExtra("username", username);
		intent.putExtra("usersign", contacts.getSign());
		startActivity(intent);
	}

	/**
	 * 添加好友
	 */
	private void addFriend(LocalContacts contacts) {
		OpenfireManager openfireManager = application.getOpenfireManager();

		if (openfireManager.isLoggedIn()) {
			if (openfireManager.createEntry(contacts.getUserId(), contacts.getUserName(), null)) {
				showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_SUECCESS);
			} else {
				showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_FAIL);
			}
		} else {
			showToast(R.string.not_logged_cannot_operating);
		}
	}

	/**
	 * 打电话
	 * 
	 * @param contacts
	 *            void
	 */
	private void callContact(LocalContacts contacts) {
		Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contacts.getPhoneNum()));
		startActivity(intent);
	}

	/**
	 * TODO系统发送短信界面
	 * 
	 * @param mobileNum
	 *            接受人（支持多人，分号“；”隔离）
	 * @param smsStr
	 *            短信内容
	 */
	private void sendSms(String mobileNum) {
		if (mobileNum != null && mobileNum.length() > 0) {
			SMSUtil.sendInviteSMS(mobileNum, application.getImAccount(), this);
		}
	}

	private class DisapearThread implements Runnable {
		public void run() {
			// 避免在1.5s内，用户再次拖动时提示框又执行隐藏命令。
			if (scrollState == ListView.OnScrollListener.SCROLL_STATE_IDLE) {
				txtOverlay.setVisibility(View.INVISIBLE);
			}
		}
	}

	/**
	 * 搜索框字符串改变监听器
	 */
	private final TextWatcher editTextChangeListener = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			searchUserList(s);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
	};

	protected void searchUserList(CharSequence s) {
		List<LocalContacts> contacts = new ArrayList<LocalContacts>();
		if (s == null || s.length() < 1) {
			contacts = allcontacts;
		}
		if (s != null && s.length() > 0) {
			for (LocalContacts contactUser : allcontacts) {
				if ((contactUser.getUserName() != null && contactUser.getUserName().contains(s))
						|| (contactUser.getPhoneNum() != null && contactUser.getPhoneNum().contains(s))) {
					contacts.add(contactUser);
				}
			}
		}
		contactSelectAdapter.setmUserList(contacts);
	}

	public void onDestroy() {
		super.onDestroy();
		// 将txtOverlay删除。
		application.setAddressBookBind(null);
		txtOverlay.setVisibility(View.INVISIBLE);
		windowManager.removeView(txtOverlay);
	}

	/**
	 * 通讯录右边英文字母快速定位列表触摸监听器
	 */
	private class LetterListViewListener implements OnTouchingLetterChangedListener {

		@Override
		public void onTouchingLetterChanged(final String s) {
			if (alphaIndexer.get(s) != null) {
				int position = alphaIndexer.get(s);
				listMain.setSelection(position);
				txtOverlay.setText(sections[position]);
				txtOverlay.setVisibility(View.VISIBLE);
				handler.removeCallbacks(disapearThread);
				// 延迟一秒后执行，让overlay为不可见
				handler.postDelayed(disapearThread, 1000);
			}
		}
	}

	private Handler handler2 = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1:
				allcontacts = application.getLocalContacts();
				contactSelectAdapter.setmUserList(allcontacts);
				break;
			default:
				break;
			}
		}
	};

	/**
	 * 邀请通讯录好友注册旁客
	 * 
	 * @param phoneNum
	 *            被邀请人手机号 接口：InviteRegister.json
	 */
	private void invite2Panker(final String phoneNum) {

		showToast(R.string.invite_is_sended);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("inviteuid", userId));
		paras.add(new Parameter("invitemobile", mPhoneNum != null ? mPhoneNum : ""));
		paras.add(new Parameter("beinvitemobiles", phoneNum));
		serverMana.supportRequest(Configuration.getInviteRegister(), paras, INVITE_TO_PANGKER);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		try {
			if (response != null) {
				switch (caseKey) {
				// 申请添加好友或、删除管理操作返回结果处理
				case ADD_TO_FRIEND:
					FriendManagerResult fMResult = JSONUtil.fromJson(response.toString(), FriendManagerResult.class);
					if (fMResult != null && fMResult.getErrorCode() == 1) {
						showToast(fMResult.getErrorMessage());
					} else if (fMResult != null && fMResult.getErrorCode() == 0) {
						showToast(fMResult.getErrorMessage());
					} else
						showToast(R.string.operate_fail);
					break;
				// 邀请通讯录好友注册旁客
				case INVITE_TO_PANGKER:
					InviteRegisterResult inviteResult = JSONUtil.fromJson(response.toString(),
							InviteRegisterResult.class);
					if (inviteResult != null && inviteResult.errorCode == BaseResult.SUCCESS) {
						List<InviteResult> inviteResultList = inviteResult.getInviteResult();
						StringBuffer mobilePhones = new StringBuffer();
						for (InviteResult inviteResultItem : inviteResultList) {
							if (inviteResultItem.getCode() == InviteResult.SUCCESS || inviteResultItem.getCode() == 2) {
								if (mobilePhones.length() == 0) {
									mobilePhones.append(inviteResultItem.getMobile());
								} else
									mobilePhones.append(";").append(inviteResultItem.getMobile());

							}
						}
						if (mobilePhones.length() > 0) {
							sendSms(mobilePhones.toString());
						}

					} else
						showToast(R.string.invite_failed);
					break;
				}
			} else
				showToast(R.string.to_server_fail);

		} catch (Exception e) {
			logger.error(TAG, e);
			showToast(R.string.load_data_exception);
		}
	}

	@Override
	public void refreshAddressBook() {
		// TODO Auto-generated method stub
		Message message = handler2.obtainMessage(1);
		handler2.handleMessage(message);
	}

	@Override
	public void loadResult(boolean result) {
		// TODO Auto-generated method stub
		loadAddressbookOK();
	}

	private void loadAddressbookOK() {

		alphaIndexer = new HashMap<String, Integer>();
		allcontacts = application.getLocalContacts();
		contactSelectAdapter.setmUserList(allcontacts);

		alphaIndexer = application.getAlphaIndexer();
		sections = application.getSections();
		application.setAddressBookBind(LocalContactsSelectActivity.this);
	}
}