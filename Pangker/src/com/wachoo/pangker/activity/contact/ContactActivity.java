package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonActivity;
import com.wachoo.pangker.activity.HomeActivity;
import com.wachoo.pangker.activity.MainActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.adapter.ComUserListAdapter;
import com.wachoo.pangker.adapter.ComUserListAdapter.ViewHolder;
import com.wachoo.pangker.adapter.ContactGroupAdapter;
import com.wachoo.pangker.adapter.LocalContactsAdapter;
import com.wachoo.pangker.adapter.UnitsContactsAdapter;
import com.wachoo.pangker.api.ContactUserListenerManager;
import com.wachoo.pangker.api.IAddressBookBind;
import com.wachoo.pangker.api.IContactUserChangedListener;
import com.wachoo.pangker.api.LoadLocalAddressBook;
import com.wachoo.pangker.api.LoadLocalAddressBook.ILoadLocalAddressBook;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.db.IAttentsDao;
import com.wachoo.pangker.db.IContactsGroupDao;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.INetAddressBookDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.impl.AttentsDaoImpl;
import com.wachoo.pangker.db.impl.ContactsGroupDaoIpml;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.db.impl.NetAddressBookDaoImpl;
import com.wachoo.pangker.db.impl.PKUserDaoImpl;
import com.wachoo.pangker.db.impl.UserMsgDaoImpl;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.listener.OnArrowChang;
import com.wachoo.pangker.listener.TextCountLimitWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBookManagerResult;
import com.wachoo.pangker.server.response.AddressBookResult;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CallMeSetResult;
import com.wachoo.pangker.server.response.FriendManagerResult;
import com.wachoo.pangker.server.response.GroupManagerResult;
import com.wachoo.pangker.server.response.GroupRelationManagerResult;
import com.wachoo.pangker.server.response.InviteRegisterResult;
import com.wachoo.pangker.server.response.InviteResult;
import com.wachoo.pangker.server.response.LocationQueryCheckResult;
import com.wachoo.pangker.server.response.SearchFriends;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.ContactLetterListView;
import com.wachoo.pangker.ui.ContactLetterListView.OnTouchingLetterChangedListener;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.QuickActionBar;
import com.wachoo.pangker.ui.SlidingMenu;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.ui.dialog.EditTextDialog.DialogResultCallback;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.ContactsUtil;
import com.wachoo.pangker.util.SMSUtil;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * 联系人界面
 * 
 * @author wangxin
 * 
 */
public class ContactActivity extends CommonActivity implements IUICallBackInterface, 
        IContactUserChangedListener, IAddressBookBind, ILoadLocalAddressBook {

	private String TAG = "ContactFriendActivity";// log tag
	private static final Logger logger = LoggerFactory.getLogger();
	private RadioGroup mRadioGroup;
	private ServerSupportManager serverMana;
	private PangkerApplication application;// application 旁客应用
	private String userId;
	private int currentDialogId = -1;
	private OpenfireManager openfireManager;
	private int pageIndex;
	private PopMenuDialog menuDialog;
	// ***************************************好友界面******************************************************//
	private SlidingMenu slidingMenu;
	private List<UserItem> userlist = new ArrayList<UserItem>();// 当前显示的list
	private ComUserListAdapter mFriendsAdapter;
	private PullToRefreshListView refreshListView;
	private ListView mFriendsListView;

	private EditText etFriendSearch;
	private Button btnGroup;
	private Button btnAddFriend;
	private ImageView btnClear;

	private EditText etSearch_nodata;
	private Button btnGroup_nodata;
	private Button btnAddFriend_nodata;
	private ImageView btnClear_nodata;
	// private EditText etRemarkName;

	private ImageView iv_empty_icon;
	private TextView tv_empty_prompt;

	private ListView mPupupList;
	private EditText etGroup;
	private ImageView newGroup;
	private TextView editGroup;
	private ContactGroupAdapter mGroupAdapter;
	private List<ContactGroup> friendGroups;

	private String groupId = PangkerConstant.DEFAULT_GROUPID;// 当前显示的组id，当组不同的时候，显示的方式不一样
	private int currentUserPosition = 0;
	private ContactGroup currentGroup;// 把好友添加到分组时的被选择的group
	private UserItem currUserItem;
	private final int LOOK_FAN = 0x101;
	private final int DELE_FRIEND = 0x102;
	private final int ADD_TO_GROUP = 0x103;
	private final int ADD_TO_ATTENTION = 0x104;
	private final int ADD_FRIEND = 0x105;
	private final int ADD_TO_TRACE = 0x106;
	private final int ADD_REMARK_NAME = 0x107;
	private final int DELETE_GROUP = 0x108;
	private final int UPDATE_GROUP = 0x109;
	private final int ADD_GROUP = 0x10A;
	private final int LOOK_DRIFT = 0x10B;
	private final int EDIT_GROUP = 0x10C;
	private final int MOVE_OUT_GROUP = 0x10D;
	private final int DRIF_CHECK = 0x10E;
	private final int CANCELDRIFT = 0x10F;
	private final int CALL_CHECK_CASEKEY = 0x110;
	private final int ADD_TO_ATTENTION_IMPLICIT = 0x111;
	private final int FRIEND_REFRESH = 0x112;

	private IUserMsgDao userMsgDao;
	private IContactsGroupDao iGroupDao;
	private IFriendsDao iFriendsDao;
	private IPKUserDao pkUserDao;
	private IAttentsDao iAttentsDao;

	private ContactUserListenerManager userListenerManager;

	// *********************************************通讯录界面***********************************************//
	private final int ADD_TO_FRIEND = 0x200;
	private final int INVITE_TO_PANGKER = 0x201;
	private final int[] MENU_INDEXS = { 0x101, 0x102, 0x103 };
	private final String[] MENU_TITLE = { "旁客用户", "未注册旁客", "所有联系人" };
	private QuickAction popopQuickAction;
	private Handler contactHandler;
	private DisapearThread disapearThread;
	/** 标识List的滚动状态。 */
	private int scrollState;
	private LocalContactsAdapter listAdapter;
	private ListView contactListView;
	private TextView txtOverlay;
	private WindowManager windowManager;
	private List<LocalContacts> allcontacts;

	private ContactLetterListView mLetterListView;

	private LinearLayout contactsMainLayout;

	private EditText etContactSearch; // 搜索框
	private Button btnMenu;

	private HashMap<String, Integer> alphaIndexer;// 存放存在的汉语拼音首字母和与之对应的列表位置
	private String[] sections;// 存放存在的汉语拼音首字母

	// ******************************************单位通讯录界面***********************************************//
	private final int QUERY_CONTACT = 0x310;
	private final int UPDATE_CONTACT = 0x311;
	private final int DELE_CONTACT = 0x312;
	private final int MOVE_OUT_CONTACT = 0x313;

	private PullToRefreshListView pullToRefreshListView;
	private ImageView iv_unit_empty_icon;
	private TextView tv_unit_empty_prompt;

	private ListView unitListView;
	private UnitsContactsAdapter mContactAdapter;
	private EditText etEditName;
	// seach bar 控件
	private Button btnMenuShow;
	private EditText etSearch;
	private ImageView btnUnitClear;
	private Button btnMenuShow_nodata;
	private EditText etUnitSearch_nodata;
	private ImageView btnUnitClear_nodata;

	private AddressBooks currABook;
	private INetAddressBookDao iNetAddressBookDao;

	private ContactsUtil contactsUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact);
		init();
		initView();// 加载画面
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		openfireManager = application.getOpenfireManager();
		pageIndex = R.id.radio_friend;
		contactsUtil = new ContactsUtil(this, userId);
		initFriend();
		// initContact();
		initUnit();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		addGuideImage(ContactActivity.class.getName(), R.drawable.pangke_01);
	}

	/**
	 * *************************************************************************
	 * ***********************************************
	 */
	private void initFriend() {
		serverMana = new ServerSupportManager(this, this);
		iFriendsDao = new FriendsDaoImpl(this);
		pkUserDao = new PKUserDaoImpl(this);
		iAttentsDao = new AttentsDaoImpl(this);
		iGroupDao = new ContactsGroupDaoIpml(this);

		friendGroups = application.getFriendGroup();
		userlist = application.getFriendList();
		userListenerManager = PangkerManager.getContactUserListenerManager();
		userListenerManager.addUserListenter(this);
		slidingMenu = (SlidingMenu) findViewById(R.id.sliding_menu);
		slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
	}

	private void initFriendView() {
		refreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_contacts);
		refreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				pharseFriend();
			}
		});
		refreshListView.setTag("pk_contacts_friends");

		mFriendsListView = refreshListView.getRefreshableView();
		mFriendsListView.setOnTouchListener(listviewOnTouchListener);

		View view1 = LayoutInflater.from(this).inflate(R.layout.contact_search, null);
		mFriendsListView.addHeaderView(view1);
		etFriendSearch = (EditText) view1.findViewById(R.id.et_search);
		btnGroup = (Button) view1.findViewById(R.id.btn_left);
		btnGroup.setVisibility(View.VISIBLE);
		btnGroup.setBackgroundResource(R.drawable.actionbar_but_circle_bg);
		FooterView footerView = new FooterView(this);
		footerView.addToListView(mFriendsListView);
		footerView.hideFoot();
		btnAddFriend = (Button) view1.findViewById(R.id.btn_right);
		btnAddFriend.setVisibility(View.VISIBLE);
		btnAddFriend.setBackgroundResource(R.drawable.btn_add_user_selector);
		btnClear = (ImageView) view1.findViewById(R.id.btn_clear);

		btnClear.setOnClickListener(mClickListener);
		btnAddFriend.setOnClickListener(mClickListener);

		etFriendSearch.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				return false;
			}
		});
		etFriendSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s != null && !s.toString().trim().equals(etSearch_nodata.getText().toString().trim())) {
					etSearch_nodata.setText(s.toString().trim());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				groupId = PangkerConstant.DEFAULT_GROUPID;
				etFriendSearch.setHint(R.string.hint_search);
				searchUserList(s.toString().trim());
			}
		});
		btnGroup.setOnClickListener(mClickListener);

		mFriendsListView.setOnItemClickListener(mOnItemClickListener);
		mFriendsListView.setOnItemLongClickListener(mOnItemLongClickListener);

		// 空白界面
		View emptyView = LayoutInflater.from(this).inflate(R.layout.managerbar_empty_view, null, false);
		iv_empty_icon = (ImageView) emptyView.findViewById(R.id.iv_empty_icon);
		iv_empty_icon.setImageDrawable(null);
		tv_empty_prompt = (TextView) emptyView.findViewById(R.id.textViewEmpty);
		tv_empty_prompt.setText("");

		etSearch_nodata = (EditText) emptyView.findViewById(R.id.et_search);
		btnGroup_nodata = (Button) emptyView.findViewById(R.id.btn_left);
		btnGroup_nodata.setVisibility(View.VISIBLE);
		btnGroup_nodata.setBackgroundResource(R.drawable.actionbar_but_circle_bg);

		btnAddFriend_nodata = (Button) emptyView.findViewById(R.id.btn_right);
		btnAddFriend_nodata.setVisibility(View.VISIBLE);
		btnAddFriend_nodata.setBackgroundResource(R.drawable.btn_add_user_selector);
		btnClear_nodata = (ImageView) emptyView.findViewById(R.id.btn_clear);

		btnClear_nodata.setOnClickListener(mClickListener);
		btnAddFriend_nodata.setOnClickListener(mClickListener);

		etSearch_nodata.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (s != null && !s.toString().trim().equals(etFriendSearch.getText().toString().trim())) {
					etFriendSearch.setText(s.toString().trim());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
		btnGroup_nodata.setOnClickListener(mClickListener);
		mFriendsListView.setEmptyView(emptyView);
	}

	View.OnTouchListener listviewOnTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			hideSoftInput(v);
			return false;
		}
	};

	/**
	 * 初始化
	 */
	private void initView() {
		initTabHost();
		// **Friend**//
		initFriendView();
		initGroupView();
		setListView();
		// **Contact**//
		initContactView();
		initUnitView();
	}

	/**
	 * 初始化TabHost
	 */
	private void initTabHost() {
		mRadioGroup = (RadioGroup) findViewById(R.id.contact_radio);
		mRadioGroup.setOnCheckedChangeListener(mOnCheckedChangedListener);
	}

	private final OnCheckedChangeListener mOnCheckedChangedListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(final RadioGroup group, final int checkedId) {
			/** 关闭软键盘 ***/
			InputMethodManager imm = (InputMethodManager) ContactActivity.this
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(contactsMainLayout.getWindowToken(), 0);
			/*****/
			pageIndex = checkedId;
			switch (checkedId) {
			case R.id.radio_friend:
				txtOverlay.setVisibility(View.GONE);
				slidingMenu.setVisibility(View.VISIBLE);
				findViewById(R.id.local_layout).setVisibility(View.GONE);
				pullToRefreshListView.setVisibility(View.GONE);
				break;
			case R.id.radio_contact:
				// >>>>>>>加载本地联系人用户
				initContact();
				slidingMenu.setVisibility(View.GONE);
				findViewById(R.id.local_layout).setVisibility(View.VISIBLE);
				pullToRefreshListView.setVisibility(View.GONE);
				break;
			case R.id.radio_units_contact:
				txtOverlay.setVisibility(View.GONE);
				slidingMenu.setVisibility(View.GONE);
				findViewById(R.id.local_layout).setVisibility(View.GONE);
				pullToRefreshListView.setVisibility(View.VISIBLE);
				break;
			}
		}
	};

	/**
	 * TODO 旁客用户首次登录设置向导
	 * 
	 * @param className
	 *            进入页面的key
	 * @param resDrawableId
	 *            向导图片
	 */
	public void addGuideImage(final String className, int resDrawableId) {
		View view = getParent().findViewById(R.id.my_content_view);
		// View view =
		// getWindow().getDecorView().findViewById(resounceView);//查找通过setContentView上的根布局
		if (view == null)
			return;
		if (SharedPreferencesUtil.activityIsGuided(this, className)) {
			// 引导过了
			return;
		}
		ViewParent viewParent = view.getParent();
		if (viewParent instanceof FrameLayout) {
			final FrameLayout frameLayout = (FrameLayout) viewParent;
			if (resDrawableId != 0) {// 设置了引导图片
				final ImageView guideImage = new ImageView(this);
				FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.FILL_PARENT);
				guideImage.setLayoutParams(params);
				guideImage.setScaleType(ScaleType.FIT_XY);
				guideImage.setImageResource(resDrawableId);
				guideImage.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						frameLayout.removeView(guideImage);
						SharedPreferencesUtil.setIsGuided(getApplicationContext(), className);// 设为已引导
					}
				});
				frameLayout.addView(guideImage);// 添加引导图片
			}
		}
	}

	private void pharseFriend() {
		etSearch_nodata.setText("");
		etFriendSearch.setText("");
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", application.getMyUserID()));
		paras.add(new Parameter("limit", "0,100"));
		serverMana.supportRequest(Configuration.getSearchFriend(), paras, FRIEND_REFRESH);
	}

	/**
	 * 刷新列表数据
	 * 
	 * @param userList
	 */
	private void setListView() {

		mFriendsAdapter = new ComUserListAdapter(this, userlist, menuClickListener);
		mFriendsListView.setAdapter(mFriendsAdapter);

		mGroupAdapter.setGroups(friendGroups);
		if (userlist.size() == 0) {
			showNoFriend();
		}
	}

	/**
	 * initPopupWindow
	 * 
	 * @author zhengjy
	 */
	private void initGroupView() {
		// TODO Auto-generated method stub
		View btnView = LayoutInflater.from(this).inflate(R.layout.button_layout, null);
		mPupupList = (ListView) findViewById(R.id.contact_group);
		mPupupList.addHeaderView(btnView);
		newGroup = (ImageView) btnView.findViewById(R.id.btn_center);
		newGroup.setVisibility(View.GONE);
		findViewById(R.id.btn_groupedit).setVisibility(View.GONE);
		editGroup = (TextView) btnView.findViewById(R.id.tv_agreement);
		mGroupAdapter = new ContactGroupAdapter(getParent(), new ArrayList<ContactGroup>());
		mPupupList.setAdapter(mGroupAdapter);
		mPupupList.setItemsCanFocus(true);
		// newGroup.setOnClickListener(mClickListener);
		editGroup.setOnClickListener(mClickListener);
		mPupupList.setLongClickable(true);
		// mPupupList.setOnItemLongClickListener(pOnItemLongClickListener);
		mPupupList.setOnItemClickListener(pOnItemClickListener);
	}

	/**
	 * @author wangxin 2012-3-19 下午01:46:51
	 */
	public void showInfo(UserItem user) {
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, user.getUserId());
		intent.putExtra(UserItem.USERNAME, user.getUserName());
		intent.putExtra(UserItem.USERSIGN, user.getSign());
		intent.putExtra(PangkerConstant.STATE_KEY, user.getState());
		startActivity(intent);
	}

	private void showNoFriend() {
		// TODO Auto-generated method stub
		iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
		tv_empty_prompt.setText(R.string.no_friends_prompt);
	}

	private long insertChatMessage(String chatmessage, int type) {
		final ChatMessage msg = new ChatMessage();
		msg.setTime(Util.getSysNowTime()); // 手机当前时间
		msg.setUserId(currUserItem.getUserId()); // 目标用户UserID
		msg.setContent(chatmessage);// 内容
		msg.setUserName(currUserItem.getUserName());
		msg.setMyUserId(userId);// 当前用户userid
		msg.setDirection(ChatMessage.MSG_FROM_ME);// 方向
		msg.setMsgType(type);
		if (userMsgDao == null) {
			userMsgDao = new UserMsgDaoImpl(this);
		}
		long messageID = userMsgDao.saveUserMsg(msg);
		if (messageID > 0) {
			application.getMsgInfoBroadcast().sendNoticeMsg(msg);
			return messageID;
		}
		return messageID;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		return super.onTouchEvent(event);
	}

	private OnClickListener menuClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// TODO Auto-generated method stub
			if (view instanceof ImageView) {
				((ImageView) view).setImageResource(R.drawable.content_list_but_popmenu_arrow_p);
				int position = ((Integer) view.getTag()).intValue();
				ActionItem actLookUserInfo = new ActionItem(getResources().getDrawable(
						R.drawable.btn_menu_info_selector), getResources().getString(R.string.website), this);
				ActionItem actMessage = new ActionItem(getResources().getDrawable(R.drawable.btn_menu_talk_selector),
						getResources().getString(R.string.msg_talk), this);
				ActionItem actSetRemarkName = new ActionItem(getResources().getDrawable(
						R.drawable.btn_menu_rename_selector), getResources().getString(R.string.remark), this);
				ActionItem actCall = new ActionItem(getResources().getDrawable(R.drawable.btn_menu_call_selector),
						getResources().getString(R.string.call), this);
				ActionItem actDrift = new ActionItem(getResources().getDrawable(R.drawable.btn_menu_follow_selector),
						getResources().getString(R.string.drift), this);
				ActionItem actTouch = new ActionItem(getResources().getDrawable(R.drawable.btn_menu_touch_selector),
						getResources().getString(R.string.touch), this);
				QuickActionBar qaBar = new QuickActionBar(view, position);
				qaBar.setOnDismissListener(new OnArrowChang(view));
				qaBar.setEnableActionsLayoutAnim(true);
				qaBar.addActionItem(actLookUserInfo);
				qaBar.addActionItem(actMessage);
				qaBar.addActionItem(actSetRemarkName);
				qaBar.addActionItem(actCall);
				qaBar.addActionItem(actDrift);
				qaBar.addActionItem(actTouch);
				qaBar.show();
			} else if (view instanceof LinearLayout) {
				// ActionItem组件
				LinearLayout actionsLayout = (LinearLayout) view;
				QuickActionBar bar = (QuickActionBar) actionsLayout.getTag();
				bar.dismissQuickActionBar();
				currentUserPosition = bar.getListItemIndex();
				TextView txtView = (TextView) actionsLayout.findViewById(R.id.tv_title);
				String actionName = txtView.getText().toString();
				currUserItem = mFriendsAdapter.getItem(currentUserPosition);
				if (actionName.equals(getResources().getString(R.string.website))) {
					showInfo(currUserItem);
				} else if (actionName.equals(getResources().getString(R.string.msg_talk))) {
					goToMessageTalk(currUserItem);
				} else if (actionName.equals(getResources().getString(R.string.remark))) {
					mShowDialog(ADD_REMARK_NAME);
				} else if (actionName.equals(getResources().getString(R.string.call))) {
					callCheck(currUserItem.getUserId());
				} else if (actionName.equals(getResources().getString(R.string.drift))) {
					driftToUserItem(currUserItem);
				} else if (actionName.equals(getResources().getString(R.string.touch))) {
					OpenfireManager openfireManager = application.getOpenfireManager();
					long nowtime = new Date().getTime();
					if (nowtime - touchtime > 1000 * 10) {
						long messageID = insertChatMessage("您碰了对方一下!", PangkerConstant.RES_TOUCH);
						openfireManager.sendTouchMessage(userId, currUserItem.getUserId(), messageID);

						touchtime = nowtime;
					} else {
						showToast("您的touch太过频繁,请间隔10s~");
					}
				}
			}
		}
	};

	private long touchtime = 0;

	OnItemLongClickListener pOnItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			currentGroup = (ContactGroup) arg0.getAdapter().getItem(arg2);
			if (currentGroup != null && !currentGroup.getGid().equals(PangkerConstant.DEFAULT_GROUPID)) {
				mShowDialog(EDIT_GROUP);
			}
			return true;
		}
	};

	OnItemClickListener pOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			if (position > 0) {
				currentGroup = (ContactGroup) arg0.getAdapter().getItem(position);
				etFriendSearch.setHint(currentGroup.getName());
				// 如果是分组用户，则isGroupUser为true（提供移除分组功能需求）
				groupId = currentGroup.getGid();
				userlist = currentGroup.getUserItems();//
				if (userlist != null) {
					userlist = getUpdateUserList(userlist);
					mFriendsAdapter.setmUserList(userlist);
				} else {
					mFriendsAdapter.setmUserList(new ArrayList<UserItem>());
				}
				mGroupAdapter.setIndex(position - 1);
				slidingMenu.showAbove();
			}
		}
	};
	/**
	 * 列表项单击监听器
	 */
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
			currUserItem = (UserItem) adapterView.getAdapter().getItem(arg2);
			currentUserPosition = arg2;
			goToMessageTalk(currUserItem);
		}
	};
	/**
	 * 列表项长按监听器
	 */
	private OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View view, int position, long arg3) {
			currentUserPosition = position;
			currUserItem = (UserItem) arg0.getAdapter().getItem(position);
			if (currUserItem != null) {
				showPopMenu();
			}
			return true;
		}
	};

	/**
	 * Dialog事件监听
	 */
	private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (pageIndex == R.id.radio_friend) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					if (currentDialogId == ADD_TO_GROUP) {// 用户添加到分组
						if (currentGroup.userItemIsExist(currUserItem.getUserId())) {
							showToast(PangkerConstant.MSG_FRIEND_GROUP_ISEXIST);
						} else {
							toGroupManager(currentGroup.getGid(), "01", ADD_TO_GROUP);
						}
					} else if (currentDialogId == ADD_GROUP) {
						String groupName = etGroup.getText().toString();
						groupManager(groupName, PangkerConstant.GROUP_CREATE, ADD_GROUP);
					} else if (currentDialogId == UPDATE_GROUP) {
						String groupName = etGroup.getText().toString();
						groupManager(groupName, PangkerConstant.GROUP_UPDATE, UPDATE_GROUP);
					} else if (currentDialogId == MOVE_OUT_GROUP) {
						toGroupManager(currentGroup.getGid(), "02", MOVE_OUT_GROUP);
					}
				}
				// 把用户添加到分组时，选择分组情况which >= 0 modify by lb,去掉默认好友组,仅仅是屏蔽，而不是删除
				else if (which >= 0) {
					currentGroup = friendGroups.get(which + 1);
				}
			} else if (pageIndex == R.id.radio_units_contact) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					if (currentDialogId == UPDATE_CONTACT) {
						contactManager(String.valueOf(currABook.getId()), "1", UPDATE_CONTACT);
					} else if (currentDialogId == DELE_CONTACT) {//
						contactManager(String.valueOf(currABook.getId()), "2", DELE_CONTACT);
					} else if (currentDialogId == MOVE_OUT_CONTACT) {
						exitAddressBook();
						// AddressBookInOut(String.valueOf(currABook.getId()));
					}
				}
			}
		}
	};
	/**
	 * 搜索框字符串改变监听器 好友
	 */
	private TextWatcher editFriendTextChangeListener = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			groupId = PangkerConstant.DEFAULT_GROUPID;
			etFriendSearch.setHint(R.string.hint_search);
			searchUserList(s);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
	};
	/**
	 * 搜索按钮监听器
	 */
	private OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			if (view == btnGroup || view == btnGroup_nodata) {
				slidingMenu.showBehind();
			}
			if (view.getId() == R.id.btn_clear) {
				groupId = PangkerConstant.DEFAULT_GROUPID;
				etFriendSearch.setText("");
			}
			// if (view == newGroup) {
			// mShowDialog(ADD_GROUP);
			// }
			if (view == btnAddFriend || view == btnAddFriend_nodata) {
				// popopQuickAction.show(btnAddFriend);
				Intent intent = new Intent(ContactActivity.this, FriendAddActivity.class);
				startActivity(intent);
			}
			if (view == editGroup) {
				Intent intent = new Intent(ContactActivity.this, FriendGroupActivity.class);
				startActivityForResult(intent, 1);
			}
		}
	};

	/**
	 * 创建新的Dialog
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = null;
		Builder builder = new AlertDialog.Builder(this).setNegativeButton(R.string.cancel, null).setPositiveButton(
				R.string.submit, mDialogClickListener);

		if (id == DELE_FRIEND) {

			MessageTipDialog messageTipDialog = new MessageTipDialog(new DialogMsgCallback() {

				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-generated method stub
					if (!isSubmit)
						return;
					if (!openfireManager.isLoggedIn()) {
						showToast(R.string.not_logged_cannot_operating);
						return;
					}
					String ruserid = currUserItem.getUserId();
					boolean isDelSuccess = openfireManager.removefriend(openfireManager.getConnection().getRoster(),
							ruserid);
					if (isDelSuccess) {
						openfireManager.sendContactsChangedPacket(userId, ruserid, BusinessType.delfriend);
						iFriendsDao.delFriend(ruserid, PangkerConstant.DEFAULT_GROUPID);
						userlist = application.getFriendList();

						mFriendsAdapter.setmUserList(userlist);
					}
				}
			}, this);

			messageTipDialog.showDialog("", "确认删除好友" + currUserItem.getUserName() + "?", false);

		}
		// >>>>>>>添加到分组
		else if (id == ADD_TO_GROUP) {
			if (friendGroups == null || friendGroups.size() <= 1) {
				showToast(PangkerConstant.MSG_GROUP_NOHAVE);
			} else {
				currentGroup = friendGroups.get(1);
				dialog = builder.setTitle("把用户  " + currUserItem.getUserName() + "添加到分组")
						.setSingleChoiceItems(getGroupName(), 0, mDialogClickListener).create();
			}
		}
		// >>>>>>>添加备注名
		else if (id == ADD_REMARK_NAME) {
			EditTextDialog editTextDialog = new EditTextDialog(ContactActivity.this);
			editTextDialog.setLine(1);

			// >>>>>>>>老的备注名
			String oldRemarkName = currUserItem.getAvailableName();
			editTextDialog.showDialog(R.string.user_dialog_remark, oldRemarkName, R.string.user_dialog_remark, 18,
					new DialogResultCallback() {
						@Override
						public void buttonResult(String result, boolean isSubmit) {
							// TODO Auto-generated method stub
							if (isSubmit && result != null) {

								if (!Util.isEmpty(result)) {
									addRemarkName(result);
								}
							}
						}
					});
		}

		// >>>>>>>>删除群组
		else if (id == DELETE_GROUP) {
			MessageTipDialog messageTipDialog = new MessageTipDialog(new DialogMsgCallback() {

				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-generated method stub
					if (!isSubmit)
						return;
					groupManager("", PangkerConstant.GROUP_DELETE, DELETE_GROUP);
				}
			}, this);

			messageTipDialog.showDialog("", getResources().getString(R.string.tip_dele_mess), false);
		}

		// >>>>>>将好友移动到指定分组
		else if (id == MOVE_OUT_GROUP) {
			dialog = builder.setMessage(R.string.is_moveout_group).create();
		} else if (id == UPDATE_CONTACT) {
			View view = LayoutInflater.from(getParent()).inflate(R.layout.dialog_editview, null);
			etEditName = (EditText) view.findViewById(R.id.et_dialog);
			etEditName.addTextChangedListener(new TextCountLimitWatcher(10, etEditName));
			etEditName.setText(currABook.getName());
			builder.setTitle(R.string.units_contact_create_hint).setView(view);
			dialog = builder.create();
		}
		// >>>>>>删除单位通讯录
		else if (id == DELE_CONTACT) {

			MessageTipDialog messageTipDialog = new MessageTipDialog(new DialogMsgCallback() {

				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-generated method stub
					if (!isSubmit)
						return;
					// >>>>>>>>删除单位通讯录
					contactManager(String.valueOf(currABook.getId()), "2", DELE_CONTACT);
				}
			}, this);
			messageTipDialog.setDialogTitle(currABook.getName());
			messageTipDialog.showDialog("", getString(R.string.units_delete_ifornot), false);

		}
		// >>>>>>>>>>申请退出单位通讯录
		else if (id == MOVE_OUT_CONTACT) {

			MessageTipDialog messageTipDialog = new MessageTipDialog(new DialogMsgCallback() {

				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-generated method stub
					if (!isSubmit)
						return;
					// >>>>>>>>申请退出单位通讯录
					exitAddressBook();
				}
			}, this);
			messageTipDialog.setDialogTitle(currABook.getName());
			messageTipDialog.showDialog("", getString(R.string.units_move_out_ifornot), false);

		}
		return dialog;
	}

	/**
	 * 获取好友分组列表名称，供用户选择添加好友到分组
	 * 
	 * @return String[]
	 */
	private String[] getGroupName() {
		String[] groupNames = new String[friendGroups.size() - 1];
		for (int i = 1; i < friendGroups.size(); i++) {
			groupNames[i - 1] = friendGroups.get(i).getName();
		}
		return groupNames;
	}

	/**
	 * 不与服务器交流
	 * 
	 * @param currUserItem
	 */
	private void addTraceList(UserItem userItem) {
		// TODO Auto-generated method stub
		if (application.IsTracelistExist(userItem.getUserId())) {
			showToast("Ta已经在您的亲友组里!");
			return;
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "pangker"));
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("ruid", userItem.getUserId()));
		paras.add(new Parameter("type", "0"));
		serverMana.supportRequest(Configuration.getRelativeManager(), paras, true, "正在提交...", ADD_TO_TRACE);
	}

	/**
	 * 漂移鉴权
	 * 
	 * @author wubo
	 * @createtime 2012-5-23
	 * @param item
	 */
	private void driftToUserItem(final UserItem item) {
		if (application.getLocateType() == PangkerConstant.LOCATE_DRIFT) {
			UserItem userInfo = application.getDriftUser();
			if (item.getUserId().equals(userInfo.getUserId())) {
				showToast("您已经漂移在Ta身边!");
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item.getUserId());
						}
					}
				}, getParent());
				dialog.showDialog("您目前在用户" + userInfo.getUserName() + "身边，确定要漂移到用户" + item.getUserName() + "身边？", false);
			}
		} else {
			SharedPreferencesUtil spu = new SharedPreferencesUtil(this);
			if (spu.getBoolean(PangkerConstant.SP_DRIFT_SET + application.getMyUserID(), false)) {
				locationCheck(item.getUserId());
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item.getUserId());
						}
					}
				}, this);
				dialog.setTextSize(17, 13);
				String data = getResources().getString(R.string.drift_show);
				data = String.format(data, item.getUserName());
				dialog.showDialog(MessageTipDialog.TYPE_DRIFT, data, getResourcesMessage(R.string.drift_tip), true);
			}
		}
	}

	/**
	 * 位置鉴权
	 * 
	 * @author wubo
	 * @createtime May 15, 2012
	 * @param friendId
	 */
	public void locationCheck(String friendId) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", friendId));
		paras.add(new Parameter("visituid", userId));
		paras.add(new Parameter("optype", "2"));
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getLocationQueryCheck(), paras, true, mess, DRIF_CHECK);
	}

	/**
	 * 私信会话(跳转)
	 * 
	 * @param intent
	 */
	private void goToMessageTalk(UserItem item) {
		Intent intent = new Intent();
		intent.putExtra(UserItem.USERID, item.getUserId());
		String username = item.getrName() != null ? item.getrName() : item.getUserName();
		intent.putExtra(UserItem.USERNAME, username);
		intent.putExtra(UserItem.USERSIGN, item.getSign());
		intent.putExtra(PangkerConstant.STATE_KEY, item.getState());
		intent.setClass(this, MsgChatActivity.class);
		launch(intent, this);
	}

	/**
	 * 显示dialog前的设置
	 * 
	 * @param id
	 */
	private void mShowDialog(int id) {
		if (currentDialogId != -1) {
			removeDialog(currentDialogId);
		}
		currentDialogId = id;
		showDialog(currentDialogId);
	}

	/**
	 * @param s
	 *            void
	 */
	protected void searchUserList(CharSequence s) {
		List<UserItem> searchList = new ArrayList<UserItem>();
		if (s == null || s.length() < 1) {
			searchList = userlist;
		}
		if (s != null && s.length() > 0) {
			for (UserItem contactUser : userlist) {
				if (contactUser.getAvailableName().toLowerCase().contains(s.toString().toLowerCase())) {
					searchList.add(contactUser);
				}
			}
		}
		mFriendsAdapter.setmUserList(searchList);
		if(mFriendsAdapter.getCount() == 0){
			etSearch_nodata.requestFocus();
			etSearch_nodata.setSelection(s.length());
		} else {
			etFriendSearch.requestFocus();
			etFriendSearch.setSelection(s.length());
		}
	}

	/**
	 * @param s
	 *            void
	 */
	protected void searchContactList(CharSequence s) {
		List<LocalContacts> searchList = new ArrayList<LocalContacts>();
		if (s == null || s.length() < 1) {
			searchList = allcontacts;
		}
		if (s != null && s.length() > 0) {
			for (LocalContacts contactUser : allcontacts) {
				if ((contactUser.getPhoneNum() != null && contactUser.getPhoneNum().contains(s))
						|| (contactUser.getUserName() != null && contactUser.getUserName().contains(s))
						|| (contactUser.getrName() != null && contactUser.getrName().contains(s))) {
					searchList.add(contactUser);
				}
			}
		}
		listAdapter.setmUserList(searchList);
	}

	/**
	 * 呼叫校验 void
	 */
	public void callCheck(String friendId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", friendId));
		paras.add(new Parameter("visituid", userId));
		paras.add(new Parameter("password", ""));
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getCallCheck(), paras, true, mess, CALL_CHECK_CASEKEY);
	}

	/**
	 * 关注管理、添加或者删除 接口：AttentManager.json
	 * 
	 * @param groupid
	 *            操作对象Id
	 * @param groupid
	 *            被操作组Id
	 */
	private void attentionManager(String attentionId, String attentType, int caseKey) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("appuserid", userId));
		paras.add(new Parameter("relationuserid", attentionId));
		paras.add(new Parameter("groupid", ""));
		paras.add(new Parameter("type", "1"));
		paras.add(new Parameter("attentType", attentType));// 关注类型 0 : 隐式关注
		// 1：显式关注
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getAttentManager(), paras, true, mess, caseKey);
	}

	/**
	 * 添加用户到分组:接口：GroupRelationManager.json
	 * 
	 * @param groupid
	 *            组Id
	 */
	private void toGroupManager(String groupId, String optype, int caseKey) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("ruserids", currUserItem.getUserId()));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("type", optype));// 01：复制分组关系;02：删除分组关系
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getGroupRelationManager(), paras, true, mess, caseKey);
	}

	private String mRemarkName = "";

	/**
	 * 给好友添加备注名
	 * 
	 * @param groupid
	 *            组Id 接口：SettingRemarkName.json
	 */
	private void addRemarkName(String remarkName) {
		mRemarkName = remarkName;
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("relationuserid", currUserItem.getUserId()));
		paras.add(new Parameter("remarkname", remarkName));
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getSettingRemarkName(), paras, true, mess, ADD_REMARK_NAME);
	}

	/**
	 * 组管理服务请求方法，包括添加、修改、删除
	 * 
	 * @param gId
	 * @param gName
	 * @param gType
	 * @param key
	 */
	private void groupManager(String gName, String type, int key) {
		String groupId = "";
		if (!type.equals("01")) {
			groupId = currentGroup.getGid();
		}

		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("type", type));// 01：添加分组,02：删除分组,03:修改分组名称
		paras.add(new Parameter("groupname", gName));
		paras.add(new Parameter("grouptype", String.valueOf(PangkerConstant.GROUP_FRIENDS)));// 好友组管理,12：本站好友，14：关注人
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getGroupManager(), paras, true, mess, key);
	}

	/**
	 * 好友管理操作返回,添加、删除
	 * 
	 * @param response
	 *            后台返回结果
	 */
	private void friendMResult(Object response, int caseKey) {
		FriendManagerResult fMResult = JSONUtil.fromJson(response.toString(), FriendManagerResult.class);
		if (fMResult != null && fMResult.getErrorCode() == BaseResult.SUCCESS) {
			if (caseKey == DELE_FRIEND) {
				boolean isDelete = iFriendsDao.delFriend(currUserItem.getUserId(), "0");
				if (isDelete) {
					mFriendsAdapter.getmUserList().remove(currUserItem);
					mFriendsAdapter.notifyDataSetChanged();
					if (mFriendsAdapter.getCount() == 0) {
						showNoFriend();
					}
				}
			}
			mGroupAdapter.notifyDataSetChanged();
			showToast(fMResult.getErrorMessage());
		} else if (fMResult != null && fMResult.getErrorCode() == BaseResult.FAILED) {
			showToast(fMResult.getErrorMessage());
		} else
			showToast(R.string.operate_fail);
	}

	/**
	 * 组管理服务器请求结果返回处理
	 * 
	 * @param result
	 * @param key
	 */
	private void groupManagerResult(String result, int key) {
		try {
			GroupManagerResult groupResult = JSONUtil.fromJson(result.toString(), GroupManagerResult.class);
			if (groupResult != null && groupResult.getErrorCode() == 1) {
				showToast(groupResult.getErrorMessage());
				switch (key) {
				case ADD_GROUP:
					ContactGroup group = new ContactGroup();
					group.setGid(groupResult.getGroupid());
					group.setCount(0);
					group.setName(etGroup.getText().toString());
					group.setType(PangkerConstant.GROUP_FRIENDS);
					group.setUid(userId);
					group.setClientgid(userId);
					group.setUserItems(new ArrayList<UserItem>());
					iGroupDao.addGroup(group, userId);
					break;
				case DELETE_GROUP:
					iGroupDao.delGroup(currentGroup, userId);
					break;
				case UPDATE_GROUP:
					currentGroup.setName(etGroup.getText().toString());
					iGroupDao.updateGroup(currentGroup, userId);
					break;
				}
				mGroupAdapter.notifyDataSetChanged();
			} else if (groupResult != null && groupResult.getErrorCode() == 0) {
				showToast(groupResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);

		} catch (Exception e) {
			logger.error(TAG, e);
			showToast(R.string.to_server_fail);
		}
	}

	private List<UserItem> getUpdateUserList(List<UserItem> updateList) {
		for (UserItem userItem : updateList) {
			for (UserItem user : application.getFriendList()) {
				if (userItem.getUserId().equals(user.getUserId()) && userItem.getState() != user.getState()) {
					userItem.setState(user.getState());
					Log.d("Line", "UserId:" + user.getUserId() + "||State" + user.getState());
				}
			}
		}
		return updateList;
	}

	/**
	 * *************************************************************************
	 * ***********************************************
	 */

	private void initContact() {
		// TODO Auto-generated method stub
		contactHandler = new Handler();
		if (application.isLocalContactsInit()) {
			loadAddressbookOK();
		} else {
			new LoadLocalAddressBook(ContactActivity.this, ContactActivity.this).execute();
		}
	}

	private void initContactView() {
		// TODO Auto-generated method stub
		contactsMainLayout = (LinearLayout) findViewById(R.id.contacts_main);
		mLetterListView = (ContactLetterListView) findViewById(R.id.lv_letter);
		mLetterListView.setOnTouchingLetterChangedListener(new LetterListViewListener());
		listAdapter = new LocalContactsAdapter(this, allcontacts, null);
		contactListView = (ListView) findViewById(R.id.listInfo);
		contactListView.setOnTouchListener(listviewOnTouchListener);

		View view1 = LayoutInflater.from(this).inflate(R.layout.contact_search, null);
		contactListView.addHeaderView(view1);
		RelativeLayout edittext_ly = (RelativeLayout) view1.findViewById(R.id.edittext_ly);
		edittext_ly.setPadding(0, 0, 25, 0);
		etContactSearch = (EditText) view1.findViewById(R.id.et_search);
		etContactSearch.addTextChangedListener(editContactTextChangeListener);
		disapearThread = new DisapearThread();
		ImageView btnClear = (ImageView) view1.findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				etContactSearch.setText("");
			}
		});
		btnMenu = (Button) view1.findViewById(R.id.btn_left);
		btnMenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				popopQuickAction.show(btnMenu);
				hideSoftInput(arg0);
			}
		});

		contactListView.setAdapter(listAdapter);
		popopQuickAction = new QuickAction(this, QuickAction.VERTICAL);
		popopQuickAction.setOnActionItemClickListener(onActionItemClickListener);
		for (int i = 0; i < MENU_INDEXS.length; i++) {
			if (i == MENU_INDEXS.length - 1) {
				popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[i], MENU_TITLE[i]), true);
			} else {
				popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[i], MENU_TITLE[i]));
			}
		}

		contactListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				LocalContacts contact = (LocalContacts) arg0.getAdapter().getItem(arg2);
				Intent intent = new Intent(ContactActivity.this, UtitsContactUserDetailActivity.class);
				contact.setPhoto(null);
				intent.putExtra(LocalContacts.LOCALCONTACTS_KEY, contact);
				startActivity(intent);
			}
		});

		// 初始化首字母悬浮提示框
		txtOverlay = (TextView) LayoutInflater.from(ContactActivity.this).inflate(R.layout.popup_char_hint, null);
		txtOverlay.setVisibility(View.INVISIBLE);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_APPLICATION,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
				PixelFormat.TRANSLUCENT);
		windowManager = (WindowManager) getParent().getSystemService(Context.WINDOW_SERVICE);
		windowManager.addView(txtOverlay, lp);
	}

	/**
	 * 通讯录右边英文字母快速定位列表触摸监听器
	 */
	private class LetterListViewListener implements OnTouchingLetterChangedListener {
		@Override
		public void onTouchingLetterChanged(final String s) {
			hideSoftInput();
			if (!application.isLocalContactsInit()) {
				return;
			}
			if (alphaIndexer.get(s) != null) {
				int position = alphaIndexer.get(s);
				contactListView.setSelection(position);
				txtOverlay.setText(sections[position]);
				txtOverlay.setVisibility(View.VISIBLE);
				contactHandler.removeCallbacks(disapearThread);
				// 延迟一秒后执行，让overlay为不可见
				contactHandler.postDelayed(disapearThread, 1000);
			}
		}
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == MENU_INDEXS[0]) { // 旁客用户
				searchUserType(LocalContacts.RELATION_P_USER);
			} else if (actionId == MENU_INDEXS[1]) { // 未注册
				searchUserType(LocalContacts.RELATION_CONTACTS);
			} else if (actionId == MENU_INDEXS[2]) { // 所有联系人
				listAdapter.setmUserList(allcontacts);
				listAdapter.notifyDataSetChanged();
			}

		}
	};

	private Handler handler2 = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1:
				allcontacts = application.getLocalContacts();
				listAdapter.setmUserList(allcontacts);
				listAdapter.notifyDataSetChanged();
				break;
			default:
				break;
			}
		}
	};

	/**
	 * 搜索框字符串改变监听器
	 * 
	 * 通讯录
	 */
	private final TextWatcher editContactTextChangeListener = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			searchContactList(s.toString().trim());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
	};

	protected void searchUserType(int userRelation) {
		List<LocalContacts> contacts = new ArrayList<LocalContacts>();
		for (LocalContacts localContact : allcontacts) {
			if (localContact.getRelation() == userRelation) {
				contacts.add(localContact);
			}
			// 如果是好友关系，则该用户也是旁客用户
			if (userRelation == LocalContacts.RELATION_P_USER
					&& localContact.getRelation() == LocalContacts.RELATION_P_FRIEND) {
				contacts.add(localContact);
			}
		}

		listAdapter.setmUserList(contacts);
		listAdapter.notifyDataSetChanged();
	}

	/**
	 * 发送私信
	 * 
	 * @param contacts
	 *            void
	 */
	private void sendMsg(String phoneNum, String smsUrl) {
		if (phoneNum != null && phoneNum.length() > 0) {
			SMSUtil.sendInviteSMS(phoneNum, application.getImAccount(), this);
		}
	}

	/**
	 * *************************************************************************
	 * ***********************************************
	 */

	private void initUnit() {
		// TODO Auto-generated method stub
		//
	}

	private void initUnitView() {
		// TODO Auto-generated method stub
		iNetAddressBookDao = new NetAddressBookDaoImpl(this);

		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_units_contacts);
		pullToRefreshListView.setTag("pk_utitscontacts");
		pullToRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				AddressBookQuery();
			}
		});
		unitListView = pullToRefreshListView.getRefreshableView();
		unitListView.setOnTouchListener(listviewOnTouchListener);
		View search_bar = LayoutInflater.from(this).inflate(R.layout.contact_search, null);
		unitListView.addHeaderView(search_bar);

		View view = LayoutInflater.from(this).inflate(R.layout.managerbar_empty_view, null, false);
		iv_unit_empty_icon = (ImageView) view.findViewById(R.id.iv_empty_icon);
		iv_unit_empty_icon.setImageDrawable(null);
		tv_unit_empty_prompt = (TextView) view.findViewById(R.id.textViewEmpty);
		tv_unit_empty_prompt.setText("");
		etUnitSearch_nodata = (EditText) view.findViewById(R.id.et_search);
		btnMenuShow_nodata = (Button) view.findViewById(R.id.btn_left);

		btnMenuShow_nodata.setBackgroundResource(R.drawable.btn_common_add_selector);
		btnMenuShow_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ContactActivity.this, UtitsContactCreateActivity.class);
				startActivityForResult(intent, 3);
			}
		});

		btnUnitClear_nodata = (ImageView) view.findViewById(R.id.btn_clear);
		btnUnitClear_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				etUnitSearch_nodata.setText("");
			}
		});

		unitListView.setEmptyView(view);
		new FooterView(this).addToListView(unitListView);
		etSearch = (EditText) search_bar.findViewById(R.id.et_search);
		btnMenuShow = (Button) search_bar.findViewById(R.id.btn_left);
		btnMenuShow.setBackgroundResource(R.drawable.btn_common_add_selector);
		btnUnitClear = (ImageView) search_bar.findViewById(R.id.btn_clear);

		btnUnitClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				etSearch.setText("");
			}
		});

		mContactAdapter = new UnitsContactsAdapter(this, new ArrayList<AddressBooks>());
		unitListView.setAdapter(mContactAdapter);
		unitListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				currABook = (AddressBooks) arg0.getAdapter().getItem(arg2);
				Intent intent = new Intent(ContactActivity.this, UtitsContactUserActivity.class);
				intent.putExtra(AddressBooks.BOOK_KEY, currABook);
				startActivity(intent);
			}
		});

		unitListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				currABook = (AddressBooks) arg0.getAdapter().getItem(arg2);
				showPopMenu();
				return true;
			}
		});

		btnMenuShow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideSoftInput(v);
				Intent intent = new Intent(ContactActivity.this, UtitsContactCreateActivity.class);
				startActivityForResult(intent, 3);
			}
		});
		etSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (etUnitSearch_nodata != null
						&& !s.toString().trim().equals(etUnitSearch_nodata.getEditableText().toString().trim())) {
					etUnitSearch_nodata.setText(s.toString().trim());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				searchUnitUserList(s.toString().trim());
			}
		});
		etUnitSearch_nodata.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (etSearch != null && !s.toString().trim().equals(etSearch.getEditableText().toString().trim())) {
					etSearch.setText(s.toString().trim());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
		if (application.getNetAddressBooks() != null && application.getNetAddressBooks().size() > 0) {
			mContactAdapter.setmContactList(application.getNetAddressBooks());
		} else
			AddressBookQuery();
	}

	private void AddressBookQuery() {
		// TODO Auto-generated method stub
		if(etEditName != null){
			etEditName.setText("");
		}
		if(etUnitSearch_nodata != null){
			etUnitSearch_nodata.setText("");
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", userId)); // 查询对象UID
		serverMana.supportRequest(Configuration.getAddressBookQuery(), paras, QUERY_CONTACT);
		pullToRefreshListView.setRefreshing(true);
	}

	//快速搜索单位通讯录
	protected void searchUnitUserList(CharSequence s) {
		List<AddressBooks> searchList = new ArrayList<AddressBooks>();
		if (s == null || s.length() < 1) {
			searchList = application.getNetAddressBooks();
		}
		if (s != null && s.length() > 0) {
			for (AddressBooks addressBook : application.getNetAddressBooks()) {
				if (addressBook.getName().toLowerCase().contains(s.toString().toLowerCase())
						|| (addressBook.getUsername() != null 
						    && addressBook.getUsername().toLowerCase().contains(s.toString().toLowerCase()))) {
					searchList.add(addressBook);
				}
			}
		}
		mContactAdapter.setmContactList(searchList);
		if (mContactAdapter.getCount() == 0) {
			etUnitSearch_nodata.requestFocus();
			etUnitSearch_nodata.setSelection(s.length());
		} else {
			etSearch.requestFocus();
			etSearch.setSelection(s.length());
		}
	}

	/**
	 * 单位通讯录管理 接口：AddressBookManager.json
	 */
	private void contactManager(String bookid, String optype, int caseKey) {
		String name = "";
		if (optype.equals("1")) {
			name = etEditName.getText().toString();
			if (Util.isEmpty(name)) {
				showToast(R.string.units_contact_name_nonull);
				return;
			}
			currABook.setName(name);
		}

		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("type", optype));// 操作类型(必填)：0：创建通讯录，1更改通讯录，2：删除通讯录。
		paras.add(new Parameter("name", name));// 通讯录名称，optype=0，1时必填
		paras.add(new Parameter("id", bookid));
		String mess = getResourcesMessage(R.string.please_wait);

		serverMana.supportRequest(Configuration.getAddressBookManager(), paras, true, mess, caseKey);
	}

	/**
	 * 退出单位通讯录，通知管理员
	 */
	private void exitAddressBook() {
		if (openfireManager.isLoggedIn()) {
			openfireManager.sendNetAddressBook(userId, String.valueOf(currABook.getUid()), "", "out", currABook,
					BusinessType.applyExitNetAddressBook);
			showToast(R.string.net_addressbook_exit_message);
		} else {
			showToast(PangkerConstant.MSG_NO_ONLINE);
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		if (!HttpResponseStatus(response)) {
			refreshListView.onRefreshComplete();
			pullToRefreshListView.onRefreshComplete();
			if (caseKey == FRIEND_REFRESH) {
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				tv_empty_prompt.setText(R.string.no_network);
			}
			if (caseKey == QUERY_CONTACT) {
				iv_unit_empty_icon.setImageResource(R.drawable.emptylist_icon);
				tv_unit_empty_prompt.setText(R.string.no_network);
			}
			return;
		}
		switch (caseKey) {
		case DELE_FRIEND:
			friendMResult(response, DELE_FRIEND);
			break;
		case ADD_FRIEND:
			friendMResult(response, ADD_FRIEND);
			break;
		case DELETE_GROUP:
			groupManagerResult(response.toString(), DELETE_GROUP);
			break;
		case ADD_GROUP:
			groupManagerResult(response.toString(), ADD_GROUP);
			break;
		case UPDATE_GROUP:
			groupManagerResult(response.toString(), UPDATE_GROUP);
			break;
		// 显式关注和隐式关注
		case ADD_TO_ATTENTION:
		case ADD_TO_ATTENTION_IMPLICIT:
			BaseResult addAttent = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (addAttent != null && addAttent.getErrorCode() == BaseResult.SUCCESS) {
				showToast(addAttent.getErrorMessage());
				if (caseKey == ADD_TO_ATTENTION) {
					currUserItem.setAttentType(UserItem.ATTENT_TYPE_COMMON);
					application.getOpenfireManager().sendContactsChangedPacket(userId, currUserItem.getUserId(),
							BusinessType.addattent);
				} else {
					currUserItem.setAttentType(UserItem.ATTENT_TYPE_IMPLICIT);
				}

				pkUserDao.saveUser(currUserItem);
				iAttentsDao.addAttent(currUserItem, String.valueOf(PangkerConstant.DEFAULT_GROUPID));// 保存到本地
				contactsUtil.setAttentCount(1);
				userListenerManager.refreshUI(ContactAttentionActivity.class.getName(),
						PangkerConstant.STATE_CHANGE_REFRESHUI);
			} else if (addAttent.getErrorCode() == BaseResult.FAILED) {
				showToast(addAttent.getErrorMessage());
			} else
				showToast(R.string.add_attent_fail);
			break;
		case MOVE_OUT_GROUP:
		case ADD_TO_GROUP:
			GroupRelationManagerResult toGroup = JSONUtil.fromJson(response.toString(),
					GroupRelationManagerResult.class);
			if (toGroup != null && toGroup.getErrorCode() == BaseResult.SUCCESS) {
				if (caseKey == ADD_TO_GROUP) {
					iFriendsDao.addFriend(currUserItem, currentGroup.getGid());
					currentGroup.addUserItem(currUserItem);
				} else if (caseKey == MOVE_OUT_GROUP) {
					iFriendsDao.delFriend(currUserItem.getUserId(), currentGroup.getGid());
					mFriendsAdapter.getmUserList().remove(currUserItem);
					mFriendsAdapter.notifyDataSetChanged();
				}
				mGroupAdapter.notifyDataSetChanged();
				showToast(toGroup.getErrorMessage());
			} else if (toGroup != null && toGroup.getErrorCode() == BaseResult.FAILED) {
				showToast(toGroup.getErrorMessage());
			} else {
				showToast(R.string.operate_fail);
			}
			break;
		case ADD_REMARK_NAME:
			BaseResult baseResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (baseResult != null && baseResult.errorCode == BaseResult.SUCCESS) {
				currUserItem.setrName(mRemarkName);
				boolean isUpdate = iFriendsDao.updateFriend(currUserItem);// 修改本地数据
				if (isUpdate) {
					mFriendsAdapter.notifyDataSetChanged();
				}
				showToast(baseResult.errorMessage);
			} else
				showToast(R.string.operate_fail);
			break;
		case DRIF_CHECK:
			LocationQueryCheckResult result = JSONUtil.fromJson(response.toString(), LocationQueryCheckResult.class);
			if (result != null) {
				if (result.getErrorCode() == 0) {// 未通过
					showToast("无权限漂移到目标处!");
				} else if (result.getErrorCode() == 1) {// 通过
					// 判断位置信息是否有效
					if (result.getLat() != null && !result.getLat().equals("") && result.getLon() != null
							&& !result.getLon().equals("")) {
						application.setLocateType(PangkerConstant.LOCATE_DRIFT);// 改变定位类型
						showToast("漂移成功!");
						application.setDriftUser(currUserItem);
						Message msg = new Message();
						msg.what = PangkerConstant.DRIFT_NOTICE;
						PangkerManager.getTabBroadcastManager().sendResultMessage(MainActivity.class.getName(), msg);
						PangkerManager.getTabBroadcastManager().sendResultMessage(HomeActivity.class.getName(), msg);
						application.setDirftLocation(new Location(Double.valueOf(result.getLon()), Double
								.valueOf(result.getLat())));
						Intent intent = new Intent(ContactActivity.this, PangkerService.class);
						intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DRIFT_START);
						startService(intent);
					} else
						showToast("漂移失败");

				} else if (result.getErrorCode() == 2) {// 需要对方同意
					// 未实现
				} else {
					showToast(R.string.return_value_999);
				}
			} else {
				showToast(R.string.return_value_999);
			}
			break;

		case CANCELDRIFT:
			BaseResult cancelresult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (cancelresult == null) {
				break;
			}
			if (cancelresult.getErrorCode() == 1) {
				application.setLocateType(PangkerConstant.LOCATE_CLIENT);// 变更漂移状态
				application.setDriftUser(null);
			}
			break;
		case CALL_CHECK_CASEKEY:
			CallMeSetResult resultCall = JSONUtil.fromJson(response.toString(), CallMeSetResult.class);
			if (resultCall != null && resultCall.getErrorCode() == BaseResult.SUCCESS) {
				application.getMsgInfoBroadcast()
						.sendCallMsg(currUserItem.getUserId(), currUserItem.getAvailableName());
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel://" + resultCall.getMobile()));
				startActivity(intent);
			} else if (resultCall != null
					&& (resultCall.getErrorCode() == BaseResult.FAILED || resultCall.getErrorCode() == 2)) {
				showToast(resultCall.getErrorMessage());
			} else
				showToast(R.string.return_value_999);
			break;
		case FRIEND_REFRESH:
			SearchFriends friendsResult = JSONUtil.fromJson(response.toString(), SearchFriends.class);
			if (friendsResult != null && friendsResult.getErrorCode() != 999) {
				if (friendsResult.getErrorCode() == BaseResult.SUCCESS) {
					iFriendsDao.refreshFriends(friendsResult.getGroup());
					List<UserItem> userlist2 = iFriendsDao.getAllFriendsByUID(userId);
					// 同步在线状态
					if (userlist2 != null) {
						userlist2 = getUpdateUserList(userlist2);
						userlist = userlist2;
						application.setFriendList(userlist);
						mFriendsAdapter.setmUserList(userlist);
						groupId = PangkerConstant.DEFAULT_GROUPID;
						mGroupAdapter.setIndex(0);
						mGroupAdapter.setGroups(application.getFriendGroup());
						etFriendSearch.setHint(R.string.hint_search);
						groupId = PangkerConstant.DEFAULT_GROUPID;
					}
					if (userlist == null || userlist.size() == 0) {
						showNoFriend();
					}
				}
			} else {
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				tv_empty_prompt.setText(R.string.return_value_999);
			}
			refreshListView.onRefreshComplete();
			break;
		// 申请添加好友或、删除管理操作返回结果处理
		case ADD_TO_FRIEND:
			FriendManagerResult fMResult = JSONUtil.fromJson(response.toString(), FriendManagerResult.class);
			if (fMResult != null && fMResult.getErrorCode() == 1) {
				showToast(fMResult.getErrorMessage());
			} else if (fMResult != null && fMResult.getErrorCode() == 0) {
				showToast(fMResult.getErrorMessage());
			} else
				showToast(R.string.operate_fail);
			break;
		// 邀请通讯录好友注册旁客
		case INVITE_TO_PANGKER:
			InviteRegisterResult inviteResult = JSONUtil.fromJson(response.toString(), InviteRegisterResult.class);
			if (inviteResult != null && inviteResult.errorCode == BaseResult.SUCCESS) {
				List<InviteResult> inviteResultList = inviteResult.getInviteResult();
				for (InviteResult inviteResultItem : inviteResultList) {
					if (inviteResultItem.getCode() == InviteResult.SUCCESS || inviteResultItem.getCode() == 2) {
						sendMsg(inviteResultItem.getMobile(), inviteResultItem.getMessage());
					}
				}
			} else
				showToast(R.string.invite_failed);
			break;
		case QUERY_CONTACT:
			pullToRefreshListView.onRefreshComplete();
			AddressBookResult addressResult = JSONUtil.fromJson(response.toString(), AddressBookResult.class);
			if (null == addressResult) {
				iv_unit_empty_icon.setImageResource(R.drawable.emptylist_icon);
				tv_unit_empty_prompt.setText(R.string.to_server_fail);
				return;
			}
			if (addressResult.getErrorCode() == BaseResult.SUCCESS && addressResult.getBooks() != null) {

				iNetAddressBookDao.clearAddressBook(userId);
				iNetAddressBookDao.saveAddressBooks(userId, addressResult.getBooks());
				List<AddressBooks> queryAddressBooks = iNetAddressBookDao.getAddressBookList(userId);

				application.setNetAddressBooks(queryAddressBooks);
				if (addressResult.getBooks() == null || addressResult.getBooks().size() == 0) {
					iv_unit_empty_icon.setImageResource(R.drawable.emptylist_icon);
					tv_unit_empty_prompt.setText(R.string.no_utitscontact_prompt);
				} else {
					mContactAdapter.setmContactList(addressResult.getBooks());
				}

			}
			break;
		case DELE_CONTACT:
		case UPDATE_CONTACT:
			AddressBookManagerResult managerResult = JSONUtil.fromJson(response.toString(),
					AddressBookManagerResult.class);
			if (managerResult == null) {
				showToast(R.string.to_server_fail);
				return;
			}
			if (managerResult.getErrorCode() == BaseResult.SUCCESS) {
				showToast(managerResult.getErrorMessage());
				if (caseKey == UPDATE_CONTACT) {
					iNetAddressBookDao.updateAddressBook(userId, currABook);
					mContactAdapter.updateBooks(currABook);
				} else if (caseKey == DELE_CONTACT) {
					iNetAddressBookDao.deleAddressBook(userId, String.valueOf(currABook.getId()));
					// List<AddressMembers> addressMembers =
					// application.getNetAddressMembers(currABook.getId());
					// if (addressMembers != null && addressMembers.size() > 0)
					// {
					// INetAddressBookUserDao iNetAddressBookUserDao = new
					// NetAddressBookUserDaoImpl(this);
					// final OpenfireManager openfireManager =
					// application.getOpenfireManager();
					// for (AddressMembers member : addressMembers) {
					// if (!member.getUid().toString().equals(userId)) {
					// iNetAddressBookUserDao.deleAddressMember(member.getUid().toString());
					// openfireManager.sendBeDelNetAddressBook(userId,
					// member.getUid().toString(),
					// String.valueOf(currABook.getId()), "");
					// }
					// }
					// }

					mContactAdapter.removeBooks(currABook);
				}
			} else if (managerResult.getErrorCode() == BaseResult.FAILED) {
				showToast(managerResult.getErrorMessage());
			}
			break;
		case MOVE_OUT_CONTACT:
			BaseResult nbaseResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (null == nbaseResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (nbaseResult.getErrorCode() == BaseResult.SUCCESS) {
				showToast(nbaseResult.getErrorMessage());
				iNetAddressBookDao.deleAddressBook(userId, String.valueOf(currABook.getId()));
				mContactAdapter.removeBooks(currABook);
			} else if (nbaseResult.getErrorCode() == BaseResult.FAILED) {
				showToast(nbaseResult.getErrorMessage());
			}
			break;
		case ADD_TO_TRACE:
			BaseResult tResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (tResult != null && BaseResult.SUCCESS == tResult.errorCode) {
				showToast(tResult.getErrorMessage());
				if (iFriendsDao.updateRelation(currUserItem.getUserId(), "2")) {
					application.addTracelist(currUserItem);
				}
				// >>>发送亲友的添加的监听
			} else {
				showToast(R.string.deal_fail);
			}
			break;
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		userListenerManager.removeUserListenter(this);
		application.setAddressBookBind(null);
		txtOverlay.setVisibility(View.INVISIBLE);
		windowManager.removeView(txtOverlay);
		super.onDestroy();
	}

	@Override
	public boolean isHaveUser(UserItem item) {
		// TODO Auto-generated method stub

		return false;
	}

	@Override
	public void refreshUser(int what) {
		mHandler.sendEmptyMessage(what);
		Log.i("contact", "online/offline");
	}

	protected Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case PangkerConstant.STATE_CHANGE_REFRESHUI:
				userlist = mFriendsAdapter.getmUserList();
				if (userlist != null) {
					userlist = getUpdateUserList(userlist);
					mFriendsAdapter.setmUserList(userlist);
				}
				break;
			case PangkerConstant.USERINFO_CHANGE_REFRESHUI:
				int postion = msg.arg1;
				final UserItem userItem = userlist.get(postion);
				View convertView = mFriendsListView.getChildAt(postion + 1);
				ViewHolder holder = (ViewHolder) convertView.getTag();
				holder.refresh(userItem);
				break;
			case PangkerConstant.REFRESHUI_ADD_FRIEND:
			case PangkerConstant.REFRESHUI_DEL_FRIEND:
				// userlist = application.getFriendList();
				// 当用户处于默认的组时候改变界面
				friendGroups = application.getFriendGroup();

				mGroupAdapter.setGroups(friendGroups);
				for (ContactGroup group : friendGroups) {
					if (groupId.equals(group.getGid())) {
						userlist = getUpdateUserList(group.getUserItems());
						mFriendsAdapter.setmUserList(userlist);
					}
				}
				break;
			}
		}
	};

	private class DisapearThread implements Runnable {
		public void run() {
			// 避免在1.5s内，用户再次拖动时提示框又执行隐藏命令。
			if (scrollState == ListView.OnScrollListener.SCROLL_STATE_IDLE) {
				txtOverlay.setVisibility(View.INVISIBLE);
			}
		}
	}

	@Override
	public void refreshAddressBook() {
		// TODO Auto-generated method stub
		Message message = handler2.obtainMessage(1);
		handler2.handleMessage(message);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 3 && resultCode == RESULT_OK) {
			mContactAdapter.setmContactList(application.getNetAddressBooks());
		}
		if (requestCode == 2 && resultCode == RESULT_OK) {
			allcontacts = application.getLocalContacts();
			listAdapter.setmUserList(allcontacts);
		}
		if (requestCode == 1 && resultCode == RESULT_OK) {

			friendGroups = application.getFriendGroup();
			mGroupAdapter.setGroups(friendGroups);
		}
	}

	@Override
	public void loadResult(boolean result) {
		// TODO Auto-generated method stub
		loadAddressbookOK();
	}

	private void loadAddressbookOK() {

		alphaIndexer = new HashMap<String, Integer>();
		allcontacts = application.getLocalContacts();
		listAdapter.setmUserList(allcontacts);

		alphaIndexer = application.getAlphaIndexer();
		sections = application.getSections();
		application.setAddressBookBind(ContactActivity.this);
	}

	private ActionItem[] getActionItems() {
		// TODO Auto-generated method stub
		ActionItem[] msgMenu = null;
		if (pageIndex == R.id.radio_friend) {
			if (!groupId.equals(PangkerConstant.DEFAULT_GROUPID)) {
				msgMenu = new ActionItem[] { new ActionItem(LOOK_FAN, getString(R.string.title_look_friend)),
						new ActionItem(ADD_TO_ATTENTION, getString(R.string.add_to_attent)),
						new ActionItem(MOVE_OUT_GROUP, getString(R.string.moveout_group)),
						new ActionItem(ADD_TO_GROUP, getString(R.string.add_to_group)),
						new ActionItem(ADD_TO_TRACE, getString(R.string.add_to_trace)),
						new ActionItem(LOOK_DRIFT, getString(R.string.title_drift)) };
			} else {
				msgMenu = new ActionItem[] { new ActionItem(LOOK_FAN, getString(R.string.title_look_friend)),
						new ActionItem(ADD_TO_ATTENTION, getString(R.string.add_to_attent)),
						new ActionItem(DELE_FRIEND, getString(R.string.title_delete_friend)),
						new ActionItem(ADD_TO_GROUP, getString(R.string.add_to_group)),
						new ActionItem(ADD_TO_TRACE, getString(R.string.add_to_trace)),
						new ActionItem(LOOK_DRIFT, getString(R.string.title_drift)) };
			}
		}
		if (pageIndex == R.id.radio_units_contact) {
			if (String.valueOf(currABook.getUid()).equals(userId)) {
				msgMenu = new ActionItem[] { new ActionItem(UPDATE_CONTACT, getString(R.string.units_contact_update)),
						new ActionItem(DELE_CONTACT, getString(R.string.units_contact_delete)) };
			} else {
				msgMenu = new ActionItem[] { new ActionItem(MOVE_OUT_CONTACT, getString(R.string.units_move_out)) };
			}
		}
		return msgMenu;
	}

	private void showPopMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(popmenuClickListener);
		}
		if (pageIndex == R.id.radio_friend) {
			menuDialog.setTitle(currUserItem.getUserName());
		} else if (pageIndex == R.id.radio_units_contact) {
			menuDialog.setTitle(currABook.getName());
		}
		menuDialog.setMenus(getActionItems());
		menuDialog.show();
	}

	UITableView.ClickListener popmenuClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			case LOOK_FAN:
				Intent intent = new Intent();
				intent.putExtra(UserItem.USERID, currUserItem.getUserId());
				intent.putExtra(UserItem.USERNAME, currUserItem.getrName() != null ? currUserItem.getrName()
						: currUserItem.getUserName());
				intent.putExtra(UserItem.USERSIGN, currUserItem.getSign());
				intent.putExtra(PangkerConstant.STATE_KEY, currUserItem.getState());
				intent.setClass(ContactActivity.this, UserWebSideActivity.class);
				startActivity(intent);
				break;
			case ADD_TO_GROUP:
				if (friendGroups.size() == 0) {
					showToast("您没有好友分组!");
				} else
					mShowDialog(ADD_TO_GROUP);
				break;
			case ADD_TO_ATTENTION:
				showAttentionDialog();
				break;
			case DELE_FRIEND:
				mShowDialog(DELE_FRIEND);
				break;
			case MOVE_OUT_GROUP:
				mShowDialog(MOVE_OUT_GROUP);
				break;

			case LOOK_DRIFT:
				SharedPreferencesUtil spu = new SharedPreferencesUtil(ContactActivity.this);
				if (spu.getBoolean(PangkerConstant.SP_DRIFT_SET + application.getMyUserID(), false)) {
					driftToUserItem(currUserItem);
				} else {
					MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
						@Override
						public void buttonResult(boolean isSubmit) {
							// TODO Auto-genaerated method stub
							if (isSubmit) {
								driftToUserItem(currUserItem);
							}
						}
					}, ContactActivity.this);
					dialog.setTextSize(17, 13);
					String data = getResources().getString(R.string.drift_show);
					data = String.format(data, currUserItem.getUserName());
					dialog.showDialog(MessageTipDialog.TYPE_DRIFT, data, getResourcesMessage(R.string.drift_tip), true);
				}
				break;
			// 添加为友踪
			case ADD_TO_TRACE:
				addTraceList(currUserItem);
				break;
			/************************************************************************************************/
			case UPDATE_CONTACT:
				mShowDialog(UPDATE_CONTACT);
				break;
			case DELE_CONTACT:
				mShowDialog(DELE_CONTACT);
				break;
			case MOVE_OUT_CONTACT:
				mShowDialog(MOVE_OUT_CONTACT);
				break;
			}
			menuDialog.dismiss();
		}
	};
	private PopMenuDialog attentionDialog;

	private void showAttentionDialog() {
		// TODO Auto-generated method stub
		if (attentionDialog == null) {
			attentionDialog = new PopMenuDialog(this);
			attentionDialog.setTitle("关注");
			attentionDialog.setListener(attentionClickListener);
			ActionItem[] attenActions = new ActionItem[] {
					new ActionItem(ADD_TO_ATTENTION, getString(R.string.add_to_attent_explicit)),
					new ActionItem(ADD_TO_ATTENTION_IMPLICIT, getString(R.string.add_to_attent_implicit)) };
			attentionDialog.setMenuChoose(attenActions, true);
		}
		attentionDialog.show();
	}

	UITableView.ClickListener attentionClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			attentionDialog.dismiss();
			switch (actionid) {
			case ADD_TO_ATTENTION:
				attentionManager(currUserItem.getUserId(), "1", ADD_TO_ATTENTION);
				break;
			case ADD_TO_ATTENTION_IMPLICIT:
				attentionManager(currUserItem.getUserId(), "0", ADD_TO_ATTENTION_IMPLICIT);
				break;
			}
		}
	};

}
