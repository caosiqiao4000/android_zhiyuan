package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.HomeActivity;
import com.wachoo.pangker.activity.MainActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.ContactGroupAdapter;
import com.wachoo.pangker.adapter.ContactsAdapter;
import com.wachoo.pangker.api.ContactUserListenerManager;
import com.wachoo.pangker.api.IContactUserChangedListener;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.PKIconFetcher;
import com.wachoo.pangker.image.PKIconLoader;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.listener.TextCountLimitWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.FriendManagerResult;
import com.wachoo.pangker.server.response.GroupManagerResult;
import com.wachoo.pangker.server.response.GroupRelationManagerResult;
import com.wachoo.pangker.server.response.LocationQueryCheckResult;
import com.wachoo.pangker.server.response.SearchAttents;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.SlidingMenu;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * ContactFollowActivity
 * 
 * @author wangxin
 * 
 */
public class ContactAttentionActivity extends UserSearchActivity implements IUICallBackInterface,
		IContactUserChangedListener {

	private String TAG = "ContactAttentionActivity";// log tag
	private static final Logger logger = LoggerFactory.getLogger();
	private SlidingMenu slidingMenu;
	private Button btnBack;

	private ImageView iv_empty_icon;
	private TextView tv_empty_prompt;

	private List<UserItem> userlist = new ArrayList<UserItem>();
	private ContactsAdapter mAttentsAdapter;
	private PullToRefreshListView pullToRefreshListView;
	private ListView mAttentsListView;

	private EditText et_search;
	private Button btnGroup;

	private EditText et_search_nodata;
	private Button btnGroup_nodata;

	private ServerSupportManager serverMana;

	private PangkerApplication application;// application 旁客应用
	private List<ContactGroup> contactGroup = new ArrayList<ContactGroup>();

	private ImageView newGroup;
	private EditText etGroup;
	private TextView editGroup;

	private ListView mPupupList;
	private ContactGroupAdapter mGroupAdapter;
	private ContactGroup defaultGroup = null;

	private String groupId = PangkerConstant.DEFAULT_GROUPID;
	private int currentDialogId = -1;
	private ContactGroup currentGroup;// 把好友添加到分组时的被选择的group
	private final int LOOK_FAN = 0x101;
	private final int ADD_TO_GROUP = 0x102;
	private final int ADD_TO_FRIEND = 0x103;
	private final int CANCEL_ATTENTION = 0x104;
	private final int DELETE_GROUP = 0x105;
	private final int UPDATE_GROUP = 0x106;
	private final int ADD_GROUP = 0x107;
	private final int EDIT_GROUP = 0x108;
	private final int MOVE_OUT_GROUP = 0x109;
	private final int DRIF_CHECK = 0x10A;
	private final int LOOK_DRIFT = 0x10B;
	private final int CANCELDRIFT = 0x10C;
	private final int REFRESH = 0x10D;

	private String userId = "";
	private UserItem userItem;
	private ContactUserListenerManager userListenerManager;
	private PopMenuDialog menuDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_attention);
		init();
		initAttentionView();
		initGroupView();
		setListView();
		reFresh(false);
	}

	private void init() {
		// TODO Auto-generated method stub
		userId = ((PangkerApplication) getApplication()).getMyUserID();
		serverMana = new ServerSupportManager(this, this);
		application = (PangkerApplication) getApplication();
		userListenerManager = PangkerManager.getContactUserListenerManager();
		userListenerManager.addUserListenter(this);
		
		mImageResizer = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);
	}

	protected Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case PangkerConstant.STATE_CHANGE_REFRESHUI:
				break;
			default:
				break;
			}
		}
	};

	// 菜单打开的监听
	SlidingMenu.OnOpenListener onOpenListener = new SlidingMenu.OnOpenListener() {
		@Override
		public void onOpen() {
			// TODO Auto-generated method stub

		}
	};
	// 菜单关闭的监听
	SlidingMenu.OnCloseListener onCloseListener = new SlidingMenu.OnCloseListener() {
		@Override
		public void onClose() {
			// TODO Auto-generated method stub

		}
	};

	private void initAttentionView() {
		slidingMenu = (SlidingMenu) findViewById(R.id.sliding_menu);
		slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		slidingMenu.setOnOpenListener(onOpenListener);
		slidingMenu.setOnCloseListener(onCloseListener);

		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(mOnClickListener);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("关注人");

		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_contacts);
		pullToRefreshListView.setTag("pk_contacts_attents");
		pullToRefreshListView.setRefreshing(true);
		pullToRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				reset();
				reFresh(false);
			}
		});
		mAttentsListView = pullToRefreshListView.getRefreshableView();

		View view1 = LayoutInflater.from(this).inflate(R.layout.contact_search, null);
		mAttentsListView.addHeaderView(view1);

		et_search = (EditText) view1.findViewById(R.id.et_search);
		btnGroup = (Button) view1.findViewById(R.id.btn_left);
		btnGroup.setBackgroundResource(R.drawable.actionbar_but_circle_bg);
		ImageView btnClear = (ImageView) view1.findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(btnClearListener);
		et_search.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (s != null && !s.toString().trim().equals(et_search_nodata.getText().toString().trim())) {
					et_search_nodata.setText(s.toString().trim());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				groupId = PangkerConstant.DEFAULT_GROUPID;
				et_search.setHint(R.string.hint_search);
				et_search_nodata.setHint(R.string.hint_search);
				searchUserList(s.toString().trim());
			}
		});
		btnGroup.setOnClickListener(mOnClickListener);

		mAttentsListView.setOnItemClickListener(mOnItemClickListener);
		mAttentsListView.setOnItemLongClickListener(mOnItemLongClickListener);

		initPressBar(mAttentsListView);
		loadmoreView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (lyLoadPressBar.getVisibility() == View.GONE && lyLoadText.getVisibility() == View.VISIBLE) {
					if (isLoadAll()) {
						loadText.setText(R.string.nomore_data);
					} else {
						reFresh(false);
					}
				}
			}
		});

		View emptView = LayoutInflater.from(this).inflate(R.layout.managerbar_empty_view, null, false);
		iv_empty_icon = (ImageView) emptView.findViewById(R.id.iv_empty_icon);
		iv_empty_icon.setImageDrawable(null);
		tv_empty_prompt = (TextView) emptView.findViewById(R.id.textViewEmpty);
		tv_empty_prompt.setText("");

		et_search_nodata = (EditText) emptView.findViewById(R.id.et_search);
		btnGroup_nodata = (Button) emptView.findViewById(R.id.btn_left);
		btnGroup_nodata.setBackgroundResource(R.drawable.actionbar_but_circle_bg);

		ImageView btnClear_nodata = (ImageView) emptView.findViewById(R.id.btn_clear);
		btnClear_nodata.setOnClickListener(btnClearListener);

		et_search_nodata.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (s != null && !s.toString().equals(et_search.getText().toString())) {
					et_search.setText(s);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
		btnGroup_nodata.setOnClickListener(mOnClickListener);
		mAttentsListView.setEmptyView(emptView);
	}

	private OnClickListener btnClearListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			groupId = PangkerConstant.DEFAULT_GROUPID;
			et_search.setText("");
			et_search_nodata.setText("");
		}
	};

	/**
	 * 刷新列表数据
	 * 
	 * @param userList
	 */
	private void setListView() {
		mAttentsAdapter = new ContactsAdapter(this, userlist, mImageResizer);
		mAttentsListView.setAdapter(mAttentsAdapter);
		mAttentsAdapter.notifyDataSetChanged();
	}

	/**
	 * @author zhengjy
	 */
	private void initGroupView() {
		// TODO Auto-generated method stub
		View btnView = LayoutInflater.from(this).inflate(R.layout.button_layout, null);
		mPupupList = (ListView) findViewById(R.id.contact_group);
		mPupupList.addHeaderView(btnView);
		contactGroup = application.getAttentGroup();// iGroupDao.getMyGroupByType(String.valueOf(PangkerConstant.GROUP_ATTENTS));
		if (contactGroup == null) {
			contactGroup = new ArrayList<ContactGroup>();
		}
		mGroupAdapter = new ContactGroupAdapter(this, contactGroup);
		mPupupList.setAdapter(mGroupAdapter);
		mGroupAdapter.notifyDataSetChanged();
		mPupupList.setItemsCanFocus(true);

		newGroup = (ImageView) btnView.findViewById(R.id.btn_center);
		newGroup.setVisibility(View.GONE);
		findViewById(R.id.btn_groupedit).setVisibility(View.GONE);
		editGroup  = (TextView) btnView.findViewById(R.id.tv_agreement);
		editGroup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ContactAttentionActivity.this, FriendGroupActivity.class);
				intent.putExtra("fromType", 2);
				startActivityForResult(intent, 1);
			}
		});
		mPupupList.setLongClickable(true);
		mPupupList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				if (position > 0) {
					currentGroup = (ContactGroup) arg0.getItemAtPosition(position);
					et_search.setHint("分组》" + currentGroup.getName());
					et_search_nodata.setHint(R.string.hint_search);
					groupId = currentGroup.getGid();
					if (currentGroup != null) {
						List<UserItem> userItem = getGroupUser(application.getAttentGroup().get(0).getMembers(),currentGroup.getMembers());//currentGroup.getMembers();
						if (userItem != null) {
							mAttentsAdapter.setmUserList(userItem);
						} else
							mAttentsAdapter.setmUserList(new ArrayList<UserItem>());
					}
					mGroupAdapter.setIndex(position - 1);
					slidingMenu.showAbove();
				}
			}
		});
	}
	/**
	 * TODO 由于分组中的用户信息不全，需要在默认分组中复制用户信息到分组用户信息中
	 * List<UserItem>
	 * 
	 * @param defaultUser
	 * @param currenUser
	 * @return
	 */
	private List<UserItem> getGroupUser(List<UserItem> defaultUser,List<UserItem> currenUser){
		for (UserItem userItem : currenUser) {
			for (UserItem item : defaultUser) {
				if(userItem.getUserId().equals(item.getUserId())){
					userItem.setUserName(item.getUserName());
					userItem.setLatitude(item.getLatitude());
					userItem.setLongitude(item.getLongitude());
					userItem.setSign(item.getSign());
					continue;
				}
			}
		}
		return currenUser;
	}

	/**
	 * 关注管理、添加或者删除
	 * 
	 * @param groupid
	 *            被操作组Id
	 * @param fanId
	 *            被添加或删除对象 接口：AttentManager.json
	 */
	private void attentionManager(String attentionId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("appuserid", userId));
		paras.add(new Parameter("relationuserid", attentionId));
		paras.add(new Parameter("groupid", ""));
		paras.add(new Parameter("type", "2"));
		paras.add(new Parameter("attentType", "1"));// 关注类型 0 : 隐式关注 1：显式关注
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getAttentManager(), paras, true, mess, CANCEL_ATTENTION);
	}

	/**
	 * 添加用户到分组
	 * 
	 * @param groupid
	 *            组Id 接口：GroupRelationManager.json
	 */
	private void userToGroupManager(String groupId, String optype, int caseKey) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("ruserids", userItem.getUserId()));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("type", optype));// 01：复制分组关系;02：删除分组关系
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getGroupRelationManager(), paras, true, mess, caseKey);
	}

	/**
	 * 列表项单击监听器
	 */
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			userItem = (UserItem) arg0.getAdapter().getItem(arg2);
			// goToMessageTalk();
			toUserInfo(userItem);
		}
	};
	
	private void showPopMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
		}
		menuDialog.setTitle(userItem.getUserName());
		ActionItem[] msgMenu = null;
		if (!groupId.equals(PangkerConstant.DEFAULT_GROUPID)) {
			msgMenu = new ActionItem[] {new ActionItem(LOOK_FAN, getString(R.string.title_look_friend)), 
					new ActionItem(MOVE_OUT_GROUP, getString(R.string.moveout_group)), 
					new ActionItem(ADD_TO_FRIEND, getString(R.string.add_to_friend)),
					new ActionItem(ADD_TO_GROUP, getString(R.string.add_to_group)),
					new ActionItem(LOOK_DRIFT, getString(R.string.title_drift))};
		} else {
			msgMenu = new ActionItem[] {new ActionItem(LOOK_FAN, getString(R.string.title_look_friend)), 
					new ActionItem(CANCEL_ATTENTION, getString(R.string.title_delete_attent)), 
					new ActionItem(ADD_TO_FRIEND, getString(R.string.add_to_friend)),
					new ActionItem(ADD_TO_GROUP, getString(R.string.add_to_group)),
					new ActionItem(LOOK_DRIFT, getString(R.string.title_drift))};
		}
		menuDialog.setMenus(msgMenu);
		menuDialog.show();
	}
	
	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			case LOOK_FAN:
				toUserInfo(userItem);
				break;
			case ADD_TO_GROUP:
				contactGroup = application.getAttentGroup();// iGroupDao.getMyGroupByType(String.valueOf(PangkerConstant.GROUP_ATTENTS));
				// 移除客户端默认分组
				if (contactGroup != null && contactGroup.size() > 0) {
					for (ContactGroup group : contactGroup) {
						if (group.getGid().equals(PangkerConstant.DEFAULT_GROUPID)) {
							defaultGroup = group;
						}
					}
					if (defaultGroup != null) {
						contactGroup.remove(defaultGroup);
					}
				}
				if (contactGroup.size() == 0) {
					showToast("您没有关注人分组!");
				} else
					mShowDialog(ADD_TO_GROUP);
				break;
			case ADD_TO_FRIEND:
				// friendManager(userid);
				if (application.IsFriend(userItem.getUserId())) {
					showToast(PangkerConstant.MSG_FRIEND_ISEXIST);
				} else {
					final OpenfireManager openfireManager = application.getOpenfireManager();
					if (openfireManager.isLoggedIn()) {
						if (openfireManager.createEntry(userItem.getUserId(), userItem.getUserId(), null)) {
							showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_SUECCESS);
						} else {
							showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_FAIL);
						}
					} else {
						showToast(R.string.not_logged_cannot_operating);
					}
				}
				break;
			case CANCEL_ATTENTION:
				mShowDialog(CANCEL_ATTENTION);
				//
				break;
			case MOVE_OUT_GROUP:
				mShowDialog(MOVE_OUT_GROUP);
				break;
			case LOOK_DRIFT:
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							driftToUserItem(userItem);
						}
					}
				}, ContactAttentionActivity.this);
				dialog.setTextSize(17,13);
				String data = getResources().getString(R.string.drift_show);
				data = String.format(data, userItem.getUserName());
				dialog.showDialog(data , getResourcesMessage(R.string.drift_tip), false);
				break;
			}
			menuDialog.dismiss();
		}
	};

	 
	/**
	 * 列表项长按监听器
	 */
	private OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			userItem = (UserItem) arg0.getAdapter().getItem(position);
            showPopMenu();				 
			return true;
		}
	};
	/**
	 * 添加和编辑分组Dialog监听器
	 */
	private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				if (currentDialogId == CANCEL_ATTENTION) {
					attentionManager(userItem.getUserId());
				} else if (currentDialogId == ADD_TO_GROUP) {// 用户添加到分组
					userToGroupManager(currentGroup.getGid(), "01", ADD_TO_GROUP);
				} else if (currentDialogId == DELETE_GROUP) {
					groupManager("", PangkerConstant.GROUP_DELETE, DELETE_GROUP);
				} else if (currentDialogId == ADD_GROUP) {
					String groupName = etGroup.getText().toString();

					groupManager(groupName, PangkerConstant.GROUP_CREATE, ADD_GROUP);
				} else if (currentDialogId == UPDATE_GROUP) {
					String groupName = etGroup.getText().toString();
					groupManager(groupName, PangkerConstant.GROUP_UPDATE, UPDATE_GROUP);
				} else if (currentDialogId == MOVE_OUT_GROUP) {
					userToGroupManager(currentGroup.getGid(), "02", MOVE_OUT_GROUP);
				}
			} else if (which >= 0) {
				currentGroup = contactGroup.get(which);
			}
		}
	};

	/**
	 * 搜索按钮监听器
	 */
	private OnClickListener mOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			if (view == btnGroup || view == btnGroup_nodata) {
				slidingMenu.showBehind();
			}
			if (view == btnBack) {
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
						ContactAttentionActivity.this.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
				finish();
			}
		}
	};

	/**
	 * 位置鉴权
	 * 
	 * @author wubo
	 * @createtime May 15, 2012
	 * @param friendId
	 */
	public void locationCheck(String friendId) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", friendId));
		paras.add(new Parameter("visituid", userId));
		paras.add(new Parameter("optype", "2"));
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getLocationQueryCheck(), paras, true, mess, DRIF_CHECK);
	}

	/**
	 * 漂移鉴权
	 * 
	 * @author wubo
	 * @createtime 2012-5-23
	 * @param item
	 */
	private void driftToUserItem(final UserItem item) {
		if (application.getLocateType() == PangkerConstant.LOCATE_DRIFT) {
			UserItem userInfo = application.getDriftUser();
			if (item.getUserId().equals(userInfo.getUserId())) {
				showToast("您在漂移在Ta身边!");
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item.getUserId());
						}
					}
				}, this);
				dialog.showDialog("",
						"您目前在用户" + userInfo.getUserName() + "身边，确定要漂移到用户" + item.getUserName() + "身边？", false);
			}
		} else {
			SharedPreferencesUtil spu = new SharedPreferencesUtil(this);
			if(spu.getBoolean(PangkerConstant.SP_DRIFT_SET + application.getMyUserID(), false)){
				locationCheck(item.getUserId());
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item.getUserId());
						}
					}
				}, this);
				dialog.setTextSize(17, 13);
				String data = getResources().getString(R.string.drift_show);
				data = String.format(data, item.getUserName());
				dialog.showDialog(MessageTipDialog.TYPE_DRIFT, data, getResourcesMessage(R.string.drift_tip), true);
			}
		}
	}

	/**
	 * 取消漂移
	 * 
	 * @author wubo
	 * @createtime 2012-5-23
	 */
	private void cancelDrift() {
		ServerSupportManager serverMana = new ServerSupportManager(ContactAttentionActivity.this,
				ContactAttentionActivity.this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", application.getMyUserID()));
		paras.add(new Parameter("sysdate", Util.getNowTime()));
		serverMana.supportRequest(Configuration.getCancelDrift(), paras, CANCELDRIFT);
	}

	/**
	 * 创建新的Dialog
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = null;
		Builder builder = new AlertDialog.Builder(this).setNegativeButton(R.string.cancel, null)
				.setPositiveButton(R.string.submit, mDialogClickListener);
		if (id == CANCEL_ATTENTION) {
			dialog = builder.setTitle(R.string.tip).setMessage(R.string.is_dele_attention).create();
		} else if (id == ADD_TO_GROUP) {
			currentGroup = contactGroup.get(0);
			dialog = builder.setTitle("把用户  " + userItem.getUserName() + "添加到分组")
					.setSingleChoiceItems(getGroupName(), 0, mDialogClickListener).create();
		} else if (id == DELETE_GROUP) {
			dialog = builder.setTitle(R.string.tip).setMessage(R.string.tip_dele_mess).create();
		} else if (id == ADD_GROUP) {
			View view = LayoutInflater.from(this).inflate(R.layout.dialog_editview, null);
			etGroup = (EditText) view.findViewById(R.id.et_dialog);
			etGroup.addTextChangedListener(new TextCountLimitWatcher(18, etGroup));
			etGroup.setHint(R.string.inputing_group_name);
			etGroup.setText("");
			dialog = builder.setTitle(R.string.add_group).setView(view).create();
		} else if (id == UPDATE_GROUP) {
			View view = LayoutInflater.from(this).inflate(R.layout.dialog_editview, null);
			etGroup = (EditText) view.findViewById(R.id.et_dialog);
			etGroup.addTextChangedListener(new TextCountLimitWatcher(18, etGroup));
			etGroup.setHint(R.string.inputing_group_name);
			etGroup.setText(currentGroup.getName());
			dialog = builder.setTitle(R.string.edit_group).setView(view).create();
		} else if (id == EDIT_GROUP) {
			String[] edit_item = new String[2];
			edit_item[0] = getResourcesMessage(R.string.title_dele_group);
			edit_item[1] = getResourcesMessage(R.string.title_edit_group);
			dialog = new AlertDialog.Builder(this).setTitle(currentGroup.getName())
					.setNegativeButton(R.string.cancel, null)
					.setItems(edit_item, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							if (arg1 == 0) {
								mShowDialog(DELETE_GROUP);
							} else if (arg1 == 1) {
								mShowDialog(UPDATE_GROUP);
							}
						}

					}).create();
		} else if (id == MOVE_OUT_GROUP) {
			dialog = builder.setMessage(R.string.is_moveout_group).create();
		}

		return dialog;
	}

	/**
	 * 获取关注人分组列表名称，供用户选择添加关注人到分组
	 * 
	 * @return String[]
	 */
	private String[] getGroupName() {

		String[] groupNames = new String[contactGroup.size()];
		for (int i = 0; i < groupNames.length; i++) {
			groupNames[i] = contactGroup.get(i).getName();
		}
		return groupNames;
	}

	/**
	 * 查看基本信息
	 * 
	 * @param user
	 *            void
	 */
	private void toUserInfo(UserItem user) {
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra("userid", user.getUserId());
		intent.putExtra("username", user.getUserName());
		intent.putExtra("usersign", user.getSign());
		startActivity(intent);
	}

	/**
	 * 显示dialog前的设置
	 * 
	 * @param id
	 */
	private void mShowDialog(int id) {
		if (currentDialogId != -1) {
			removeDialog(currentDialogId);
		}
		currentDialogId = id;
		showDialog(currentDialogId);
	}

	/**
	 * 
	 * 
	 * @param s
	 *            void
	 */
	protected void searchUserList(CharSequence s) {
		List<UserItem> searchList = new ArrayList<UserItem>();
		if (s == null || s.length() < 1) {
			searchList = userlist;
		}
		if (s != null && s.length() > 0) {
			for (UserItem contactUser : userlist) {
				if (contactUser.getUserName().contains(s)) {
					searchList.add(contactUser);
				}
			}
		}
		mAttentsAdapter.setmUserList(searchList);
	}

	/**
	 * 申请添加好友或、删除管理操作返回结果处理
	 * 
	 * @param response
	 *            后台返回结果
	 */
	private void friendMResult(Object response) {
		FriendManagerResult fMResult = JSONUtil.fromJson(response.toString(), FriendManagerResult.class);
		if (fMResult != null && fMResult.getErrorCode() == 1) {
			showToast(fMResult.getErrorMessage());
		} else if (fMResult != null && fMResult.getErrorCode() == 0) {
			showToast(fMResult.getErrorMessage());
		} else
			showToast(R.string.operate_fail);
	}

	/**
	 * 组管理服务请求方法，包括添加、修改、删除
	 * 
	 * @param gId
	 * @param gName
	 * @param gType
	 * @param key
	 */
	private void groupManager(String gName, String type, int key) {
		String groupId = "";
		if (!type.equals("01")) {
			groupId = currentGroup.getGid();
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("type", type));// 01：添加分组,02：删除分组,03:修改分组名称
		paras.add(new Parameter("groupname", gName));
		paras.add(new Parameter("grouptype", String.valueOf(PangkerConstant.GROUP_ATTENTS)));// 好友组管理,12：本站好友，14：关注人
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getGroupManager(), paras, true, mess, key);
	}

	/**
	 * 组管理服务器请求结果返回处理
	 * 
	 * @param result
	 * @param key
	 */
	private void groupManagerResult(String result, int key) {
		try {
			GroupManagerResult groupResult = JSONUtil.fromJson(result.toString(), GroupManagerResult.class);
			if (groupResult != null && groupResult.getErrorCode() == 1) {
				showToast(groupResult.getErrorMessage());
				switch (key) {
				case ADD_GROUP:
					ContactGroup group = new ContactGroup();
					group.setGid(groupResult.getGroupid());
					group.setCount(0);
					group.setName(etGroup.getText().toString());
					group.setType(PangkerConstant.GROUP_ATTENTS);
					group.setUid(userId);
					group.setClientgid(userId);
					group.setUserItems(new ArrayList<UserItem>());
					application.getAttentGroup().add(group);
					break;
				case DELETE_GROUP:
					application.getAttentGroup().remove(currentGroup);
					break;
				case UPDATE_GROUP:
					currentGroup.setName(etGroup.getText().toString());
					break;
				}
				contactGroup = application.getAttentGroup();
				if (contactGroup != null) {
					mGroupAdapter.setGroups(contactGroup);
				}
			} else if (groupResult != null && groupResult.getErrorCode() == 0) {
				showToast(groupResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);

		} catch (Exception e) {
			logger.error(TAG, e);
			showToast(R.string.load_data_exception);
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {

		if (!HttpResponseStatus(response)) {
			if (caseKey == REFRESH) {
				pullToRefreshListView.onRefreshComplete();
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				tv_empty_prompt.setText(R.string.no_network);
			}
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			return;
		}

		switch (caseKey) {
		case ADD_TO_FRIEND:
			friendMResult(response);
			break;
		case CANCEL_ATTENTION:
			BaseResult deleAttent = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (deleAttent != null && deleAttent.getErrorCode() == BaseResult.SUCCESS) {
				showToast(deleAttent.getErrorMessage());
				String ruid = userItem.getUserId();
				userlist.remove(userItem);
				mAttentsAdapter.getmUserList().remove(userItem);
				mAttentsAdapter.notifyDataSetChanged();
				application.removeAttentList(userItem);
				// 普通关注的用户取消后，通知对方
				if (userItem.getAttentType() == UserItem.ATTENT_TYPE_COMMON) {
					application.getOpenfireManager().sendContactsChangedPacket(userId, ruid,
							BusinessType.delattent);
				}
			} else if (deleAttent.getErrorCode() == 0) {
				showToast(deleAttent.getErrorMessage());
			} else
				showToast(R.string.add_attent_fail);
			break;
		case MOVE_OUT_GROUP:
		case ADD_TO_GROUP:
			GroupRelationManagerResult addToGroup = JSONUtil.fromJson(response.toString(),
					GroupRelationManagerResult.class);
			if (addToGroup != null && addToGroup.getErrorCode() == BaseResult.SUCCESS) {
				if (caseKey == ADD_TO_GROUP) {
					application.add2AttentGroup(userItem, currentGroup.getGid());
					application.getAttentGroup().add(0,defaultGroup);
				} else if (caseKey == MOVE_OUT_GROUP) {
					application.deleAttentGroupUser(userItem, currentGroup.getGid());
					mAttentsAdapter.getmUserList().remove(userItem);
					mAttentsAdapter.notifyDataSetChanged();
				}
				showToast(addToGroup.getErrorMessage());
				mGroupAdapter.notifyDataSetChanged();
			} else if (addToGroup != null && addToGroup.getErrorCode() == BaseResult.FAILED) {
				showToast(addToGroup.getErrorMessage());
			} else {
				showToast(R.string.operate_fail);
			}
			break;
		case DELETE_GROUP:
			groupManagerResult(response.toString(), DELETE_GROUP);
			break;
		case ADD_GROUP:
			groupManagerResult(response.toString(), ADD_GROUP);
			break;
		case UPDATE_GROUP:
			groupManagerResult(response.toString(), UPDATE_GROUP);
			break;
		case DRIF_CHECK:
			LocationQueryCheckResult result = JSONUtil.fromJson(response.toString(), LocationQueryCheckResult.class);
			if (result != null) {
				if (result.getErrorCode() == 0) {// 未通过
					showToast("无权限漂移到目标处!");
				} else if (result.getErrorCode() == 1) {// 通过
					// 判断位置信息是否有效
					if (result.getLat() != null && !result.getLat().equals("")
							&& result.getLon() != null && !result.getLon().equals("")) {
						
						application.setLocateType(PangkerConstant.LOCATE_DRIFT);// 改变定位类型
						showToast("漂移成功!");
						application.setDriftUser(userItem);
						Message msg = new Message();
						msg.what = PangkerConstant.DRIFT_NOTICE;
						PangkerManager.getTabBroadcastManager().sendResultMessage(MainActivity.class.getName(), msg);
						PangkerManager.getTabBroadcastManager().sendResultMessage(HomeActivity.class.getName(), msg);
						application.setDirftLocation(new Location(Double.valueOf(result.getLon()), Double.valueOf(result.getLat())));
						Intent intent = new Intent(ContactAttentionActivity.this, PangkerService.class);
						intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DRIFT_START);
						startService(intent);
					} else {
						showToast("漂移失败");
						// 通知后台解除漂移关系
						cancelDrift();
					}
				} else if (result.getErrorCode() == 2) {// 需要对方同意
					// 未实现
				} else {
					showToast(R.string.return_value_999);
				}
			} else {
				showToast(R.string.return_value_999);
			}
			break;
		case CANCELDRIFT:
			BaseResult cancelresult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (cancelresult == null) {
				break;
			}
			if (cancelresult.getErrorCode() == 1) {
				application.setLocateType(PangkerConstant.LOCATE_CLIENT);// 变更漂移状态
				// application.getDriftListener().driftEnd();// 取消漂移
				application.setDriftUser(null);
			}
			break;
		case REFRESH:
			pullToRefreshListView.onRefreshComplete();
			SearchAttents friendsResult = JSONUtil.fromJson(response.toString(), SearchAttents.class);
			if (friendsResult != null && friendsResult.getErrorCode() == BaseResult.SUCCESS) {
				application.setAttentGroup(friendsResult.getAttentgroup());
				if (friendsResult.getAttentgroup().get(0).getMembers() != null
						&& friendsResult.getAttentgroup().get(0).getMembers().size() > 0) {
					if (friendsResult.getAttentgroup().get(0).getMembers().size() <= incremental) {
						setLoadAll(true);
						lyLoadPressBar.setVisibility(View.GONE);
						lyLoadText.setVisibility(View.GONE);
					} else {
						lyLoadPressBar.setVisibility(View.GONE);
						lyLoadText.setVisibility(View.GONE);
					}
					// 如果不是从0开始加载
					if (startLimit != 0) {
						userlist.addAll(friendsResult.getAttentgroup().get(0).getMembers());
					}
					// 首次加载
					else {
						if (!isLoadAll) {
							lyLoadPressBar.setVisibility(View.GONE);
							lyLoadText.setVisibility(View.GONE);
						}
						userlist = friendsResult.getAttentgroup().get(0).getMembers();
						if (userlist.size() == 0) {
							iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
							tv_empty_prompt.setText(R.string.no_attents_prompt);
						}
					}
					resetStartLimit();
					
					// 更新列表数据
					mAttentsAdapter.setmUserList(userlist);
					application.setAttentList(userlist);
					if (userlist == null || userlist.size() == 0) {
						iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
						tv_empty_prompt.setText(R.string.no_attents_prompt);
					}

				} else if (friendsResult.getAttentgroup().get(0).getMembers() == null
						|| friendsResult.getAttentgroup().get(0).getMembers().size() == 0) {
					if (startLimit == 0) {
						iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
						tv_empty_prompt.setText(R.string.no_attents_prompt);
					} else {
						showToast(R.string.nomore_data);
					}

					setLoadAll(true);
					lyLoadPressBar.setVisibility(View.GONE);
					lyLoadText.setVisibility(View.GONE);
				} else {
					loadText.setText(R.string.load_data_fail);
					lyLoadPressBar.setVisibility(View.GONE);
					lyLoadText.setVisibility(View.VISIBLE);
				}
			}
			mGroupAdapter.setIndex(0);
			mGroupAdapter.setGroups(application.getAttentGroup());
			break;
		}

	}

	protected void onDestroy() {
		userListenerManager.removeUserListenter(this);
		super.onDestroy();
	}

	@Override
	public boolean isHaveUser(UserItem item) {
		// TODO Auto-generated method stub
		for (int i = 0; i < userlist.size(); i++) {
			if (userlist.get(i).getUserId().equals(item.getUserId())) {
				userlist.set(i, item);
				Message message = new Message();
				message.what = PangkerConstant.STATE_CHANGE_REFRESHUI;
				mHandler.sendMessage(message);
				return true;
			}
		}
		return false;
	}

	@Override
	public void refreshUser(int what) {
		// TODO Auto-generated method stub
		Message message = new Message();
		message.what = what;
		mHandler.sendMessage(message);
	}

	/**
	 * 刷新信息
	 */
	private void reFresh(boolean isTip) {
		if (mAttentsAdapter.getCount() > incremental) {
			lyLoadPressBar.setVisibility(View.VISIBLE);
			lyLoadText.setVisibility(View.GONE);
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", application.getMyUserID()));
		paras.add(getSearchLimit());
        String mees = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getSearchAttents(), paras,isTip,mees, REFRESH);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1 && resultCode == RESULT_OK) {
			contactGroup= application.getAttentGroup();
			mGroupAdapter.setGroups(contactGroup);
		}
	}

}
