package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.FriendGroupAdapter;
import com.wachoo.pangker.adapter.FriendGroupAdapter.ViewHolder;
import com.wachoo.pangker.db.IContactsGroupDao;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.impl.ContactsGroupDaoIpml;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupManagerResult;
import com.wachoo.pangker.server.response.GroupRelationManagerResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

/**
 * @author zxxcos
 */
public class FriendGroupActivity extends CommonPopActivity implements
		IUICallBackInterface {

	private final int[] MENU_INDEXS = { 0x100 };
	private final String[] MENU_TITLE = { "添加分组" };
	private QuickAction popopQuickAction;
	private final int DELETE_GROUP = 0x101;
	private final int EDIT_GROUP = 0x104;

	private final int UPDATE_GROUP = 0x102;
	private final int ADD_GROUP = 0x103;
	private final int ADD_TO_GROUP = 0x105;

	private final int ROOT_GROUP = 0x201;

	private int currentDialogId = -1;
	private ListView driftlistView;
	private PangkerApplication application;
	private ImageView iv_empty_icon;
	private TextView tv_empty_prompt;

	private IContactsGroupDao iGroupDao;
	private IFriendsDao friendsDao;
	private FriendGroupAdapter mGroupAdapter;
	private List<ContactGroup> userGroups;
	private ContactGroup currentGroup;// 把好友添加到分组时的被选择的group
	private List<UserItem> selectedUser;
	private int fromType; // 1：好友； 2：关注人；3：好友组设置
	private int isUpdate = 0; // 分组是否编辑修改过，0无修改，1有修改
	private String mdGroupName;

	// *******权限设置相关************
	private String dirid;

	// >>>>>>>>用户当前设置的分组ID
	private String mSelectedGroupID;
	// >>>>>>>>选中的好友分组ID
	public static final String SELECT_CONTACT_GROUP_ID = "SELECT_CONTACT_GROUP_ID";
	// >>>>>>>资源目录选择分组权限目前只针对好友分组
	public static final int RES_DIR_PERMISSION = 3;
	// >>>>>>>群组访问权限 目前只针对好友分组
	public static final int GROUP_JION_PERMISSION = 4;

	// >>>>>>>>>群组设置好友分组访问
	public static final String GROUP_JION_PEMISSION_KEY = "GROUP_JION_PEMISSION_KEY";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friendgrouplist);
		application = (PangkerApplication) getApplication();
		iGroupDao = new ContactsGroupDaoIpml(this);
		friendsDao = new FriendsDaoImpl(this);
		fromType = this.getIntent().getIntExtra("fromType", 0);
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isUpdate == 1) {
					setResult(RESULT_OK);
				}
				finish();
			}
		});
		final Button btnAdd = (Button) findViewById(R.id.iv_more);
		// >>>>>>>>目录权限设置
		if (fromType == RES_DIR_PERMISSION) {
			btnAdd.setText(R.string.confrim);

			// >>>>>获取当前设置的分组ID
			mSelectedGroupID = getIntent().getStringExtra(
					SELECT_CONTACT_GROUP_ID);

			dirid = getIntent().getStringExtra("dirid");
		}
		// >>>>>>>群组进入权限设置
		else if (fromType == GROUP_JION_PERMISSION) {
			btnAdd.setText(R.string.confrim);
			// >>>>>获取当前设置的分组ID
			mSelectedGroupID = getIntent().getStringExtra(
					SELECT_CONTACT_GROUP_ID);

		}
		// >>>>>>>>其他
		else {
			btnAdd.setBackgroundResource(R.drawable.btn_menu_bg);
		}

		btnAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// >>>>>>>目录权限
				if (fromType == RES_DIR_PERMISSION) {
					rootGroup();
				}
				// >>>>>>>>群组加入权限
				else if (fromType == GROUP_JION_PERMISSION) {

					if (mGroupAdapter.getSelectGroups().size() == 0) {
						showToast("请选择一个分组!");
						return;
					}
					Intent intent = new Intent();
					intent.putExtra(GROUP_JION_PEMISSION_KEY, getGrantByType());
					intent.putExtra(SELECT_CONTACT_GROUP_ID, mGroupAdapter
							.getSelectGroups().get(0).getGid());
					setResult(RESULT_OK, intent);
					finish();
				} else {
					popopQuickAction.show(btnAdd);
				}
			}
		});

		driftlistView = (ListView) findViewById(R.id.list_members);
		View view = LayoutInflater.from(this).inflate(R.layout.empty_view,
				null, false);
		iv_empty_icon = (ImageView) view.findViewById(R.id.iv_empty_icon);
		iv_empty_icon.setImageDrawable(null);
		tv_empty_prompt = (TextView) view.findViewById(R.id.textViewEmpty);
		tv_empty_prompt.setText("");
		driftlistView.setEmptyView(view);

		// >>>>>如果是关注人分组
		if (fromType == 2) {
			txtTitle.setText("关注人分组");
			userGroups = application.getAttentGroup();
		}

		// >>>>>>>>好友分组
		else {
			userGroups = new ArrayList<ContactGroup>();
			for (ContactGroup item : application.getFriendGroup()) {
				if (!item.getGid().equals("0")) {
					userGroups.add(item);
				}
			}
			txtTitle.setText("好友分组");
		}

		// >>>>>>>>初始化数据
		if (userGroups == null || userGroups.size() <= 0) {
			userGroups = new ArrayList<ContactGroup>();
		}
		// >>>>>>>>>初始化数据
		mGroupAdapter = new FriendGroupAdapter(this, userGroups);

		// >>>>>>>>>设置当前选中的分组
		if (!Util.isEmpty(mSelectedGroupID) && fromType != 2) {
			mGroupAdapter.selectByGroupID(mSelectedGroupID);
		}
		// >>>>>>>>>>>>设置显示模式
		mGroupAdapter.setFlag(fromType == RES_DIR_PERMISSION
				|| fromType == GROUP_JION_PERMISSION);
		driftlistView.setAdapter(mGroupAdapter);
		driftlistView.setOnItemClickListener(pOnItemClickListener);

		popopQuickAction = new QuickAction(this, QuickAction.VERTICAL);
		popopQuickAction
				.setOnActionItemClickListener(onActionItemClickListener);
		for (int i = 0; i < MENU_INDEXS.length; i++) {
			popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[i],
					MENU_TITLE[i]), true);
		}

	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == MENU_INDEXS[0]) { // 添加分组
				showDialogs(ADD_GROUP);
			}
		}
	};
	private PopMenuDialog menuDialog;
	OnItemClickListener pOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position,
				long arg3) {
			// >>>>>>>>>>>如果是设置权限 目录权限 、 群组访问权限
			if (fromType == RES_DIR_PERMISSION
					|| fromType == GROUP_JION_PERMISSION) {
				ViewHolder vHollder = (ViewHolder) view.getTag();
				vHollder.mCheckBox.toggle();
				mGroupAdapter.selectByIndex(position,
						vHollder.mCheckBox.isChecked());
			} else {
				currentGroup = (ContactGroup) arg0.getAdapter().getItem(
						position);
				// 如果是分组用户，则isGroupUser为true（提供移除分组功能需求）
				if (currentGroup != null
						&& !currentGroup.getGid().equals(
								PangkerConstant.DEFAULT_GROUPID)) {

					if (menuDialog == null) {
						menuDialog = new PopMenuDialog(
								FriendGroupActivity.this, R.style.MyDialog);
						menuDialog.setListener(popmenuClickListener);
					}
					menuDialog.setTitle(currentGroup.getName());

					// >>>>>>>>>>功能选项
					ActionItem[] actionItems = new ActionItem[3];
					// >>>>>>>>>>>修改分组
					actionItems[0] = new ActionItem(1,
							getString(R.string.title_edit_group));
					// >>>>>>>>判断是关注人分组还是好友分组
					if (fromType == 2) {
						actionItems[1] = new ActionItem(2,
								getString(R.string.title_addattent_group));
					} else {
						actionItems[1] = new ActionItem(2,
								getString(R.string.title_addfriend_group));
					}
					// >>>>>>删除分组
					actionItems[2] = new ActionItem(3,
							getString(R.string.title_dele_group));

					menuDialog.setMenus(actionItems);
					menuDialog.show();

				} else if (currentGroup != null
						&& currentGroup.getGid().equals(
								PangkerConstant.DEFAULT_GROUPID)) {
					showToast("默认分组，不能编辑!");
				}
			}

		}
	};

	private UITableView.ClickListener popmenuClickListener = new UITableView.ClickListener() {

		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			menuDialog.dismiss();
			switch (actionid) {
			case 1:// >>>>>>>修改分组
				showDialogs(UPDATE_GROUP);
				break;
			case 2:// >>>>>>>>>>>添加用户到分组 好友+关注人
				if (fromType == 2) {
					Intent intent = new Intent(FriendGroupActivity.this,
							ContactsSelectActivity.class);
					intent.putExtra(ContactsSelectActivity.INTENT_SELECT,
							ContactsSelectActivity.TYPE_ADDATTENT_GROUP);
					startActivityForResult(intent, 1);
				} else {
					Intent intent = new Intent(FriendGroupActivity.this,
							ContactsSelectActivity.class);
					intent.putExtra(ContactsSelectActivity.INTENT_SELECT,
							ContactsSelectActivity.TYPE_ADDFRIEND_GROUP);
					startActivityForResult(intent, 1);
				}
				break;

			case 3:// >>>>>>删除分组
				MessageTipDialog messageTipDialog = new MessageTipDialog(
						new MessageTipDialog.DialogMsgCallback() {
							@Override
							public void buttonResult(boolean isSubmit) {
								// TODO Auto-generated method stub
								if (isSubmit) {
									if (fromType == 2) {
										groupManager("",
												PangkerConstant.GROUP_DELETE,
												PangkerConstant.GROUP_ATTENTS,
												DELETE_GROUP);
									} else {
										groupManager("",
												PangkerConstant.GROUP_DELETE,
												PangkerConstant.GROUP_FRIENDS,
												DELETE_GROUP);
									}
								}

							}
						}, FriendGroupActivity.this);
				messageTipDialog.showDialog(MessageTipDialog.TYPE_DRIFT, "",
						getString(R.string.tip_dele_mess), false);
				break;

			}

		}

	};

	EditTextDialog mEditTextDialog;

	private void showDialogs(int dialogId) {
		// TODO Auto-generated method stub
		currentDialogId = dialogId;
		if (mEditTextDialog == null) {
			mEditTextDialog = new EditTextDialog(this);
		}
		if (dialogId == ADD_GROUP) {
			mEditTextDialog.setTitle("添加分组");
			mEditTextDialog.showDialog("", R.string.inputing_group_name, 18,
					resultCallback);
		} else if (dialogId == UPDATE_GROUP) {
			mEditTextDialog.setTitle("修改分组");
			mEditTextDialog.showDialog(currentGroup.getName(),
					R.string.inputing_group_name, 18, resultCallback);
		}

	}

	EditTextDialog.DialogResultCallback resultCallback = new EditTextDialog.DialogResultCallback() {
		@Override
		public void buttonResult(String result, boolean isSubmit) {
			// TODO Auto-generated method stub
			if (!isSubmit) {
				return;
			}
			if (currentDialogId == ADD_GROUP) {
				if (Util.isEmpty(result)) {
					showToast("请输入分组名称!");
					return;
				}
				currentGroup = new ContactGroup();
				currentGroup.setName(result);
				if (fromType == 2) {
					groupManager(result, PangkerConstant.GROUP_CREATE,
							PangkerConstant.GROUP_ATTENTS, ADD_GROUP);
				} else {
					groupManager(result, PangkerConstant.GROUP_CREATE,
							PangkerConstant.GROUP_FRIENDS, ADD_GROUP);
				}
			} else if (currentDialogId == UPDATE_GROUP) {
				if (Util.isEmpty(result)
						|| result.equals(currentGroup.getName())) {
					return;
				}
				mdGroupName = result;
				if (fromType == 2) {
					groupManager(result, PangkerConstant.GROUP_UPDATE,
							PangkerConstant.GROUP_ATTENTS, UPDATE_GROUP);
				} else {
					groupManager(result, PangkerConstant.GROUP_UPDATE,
							PangkerConstant.GROUP_FRIENDS, UPDATE_GROUP);
				}
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1 && resultCode == RESULT_OK) {
			selectedUser = (List<UserItem>) data
					.getSerializableExtra("selectedUser");
			if (selectedUser != null && selectedUser.size() > 0) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < selectedUser.size(); i++) {
					if (i == 0) {
						sb.append(selectedUser.get(i).getUserId());
					} else
						sb.append(",").append(selectedUser.get(i).getUserId());
				}
				toGroupManager(sb.toString(), "01", ADD_TO_GROUP);
			}
		}
	}

	/**
	 * 添加用户到分组:接口：GroupRelationManager.json
	 * 
	 * @param groupid
	 *            组Id
	 */
	private void toGroupManager(String ruserids, String optype, int caseKey) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", application.getMyUserID()));
		paras.add(new Parameter("ruserids", ruserids));
		paras.add(new Parameter("groupid", currentGroup.getGid()));
		paras.add(new Parameter("type", optype));// 01：复制分组关系;02：删除分组关系
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getGroupRelationManager(),
				paras, true, mess, caseKey);
	}

	/**
	 * 组管理服务器请求结果返回处理
	 * 
	 * @param result
	 * @param key
	 */
	private void groupManagerResult(String result, int key) {
		isUpdate = 1;
		GroupManagerResult groupResult = JSONUtil.fromJson(result.toString(),
				GroupManagerResult.class);
		if (groupResult != null && groupResult.getErrorCode() == 1) {
			switch (key) {
			case ADD_GROUP:
				currentGroup.setGid(groupResult.getGroupid());
				currentGroup.setCount(0);
				if (fromType == 2) {
					currentGroup.setType(PangkerConstant.GROUP_ATTENTS);
				} else {
					currentGroup.setType(PangkerConstant.GROUP_FRIENDS);
				}
				currentGroup.setUid(application.getMyUserID());
				currentGroup.setClientgid(application.getMyUserID());
				currentGroup.setUserItems(new ArrayList<UserItem>());
				if (iGroupDao.addGroup(currentGroup, application.getMyUserID())) {
					showToast(groupResult.getErrorMessage());
				}
				break;
			case DELETE_GROUP:
				if (iGroupDao.delGroup(currentGroup, application.getMyUserID())) {
					showToast(groupResult.getErrorMessage());
				}
				break;
			case UPDATE_GROUP:
				currentGroup.setName(mdGroupName);
				iGroupDao.updateGroup(currentGroup, application.getMyUserID());
				showToast(groupResult.getErrorMessage());
				break;
			}
			mGroupAdapter.notifyDataSetChanged();
		} else if (groupResult != null && groupResult.getErrorCode() == 0) {
			showToast(groupResult.getErrorMessage());
		} else
			showToast(R.string.to_server_fail);

		if (key == ADD_GROUP && fromType == 2) {
			Intent intent = new Intent(FriendGroupActivity.this,
					ContactsSelectActivity.class);
			intent.putExtra(ContactsSelectActivity.INTENT_SELECT,
					ContactsSelectActivity.TYPE_ADDATTENT_GROUP);
			startActivityForResult(intent, 1);
		}
	}

	/**
	 * 组管理服务请求方法，包括添加、修改、删除
	 * 
	 * @param gId
	 * @param gName
	 * @param gType
	 * @param key
	 */
	private void groupManager(String gName, String type, int contactType,
			int key) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		String groupId = "";
		if (!type.equals("01")) {
			groupId = currentGroup.getGid();
		}

		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", application.getMyUserID()));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("type", type));// 01：添加分组,02：删除分组,03:修改分组名称
		paras.add(new Parameter("groupname", gName));
		paras.add(new Parameter("grouptype", String.valueOf(contactType)));// 好友组管理,12：本站好友，14：关注人
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getGroupManager(), paras, true,
				mess, key);
	}

	private String getGrantByType() {
		// TODO Auto-generated method stub

		mSelectGroupNames = new StringBuffer();
		StringBuffer grantBuffer = new StringBuffer();
		if (fromType == RES_DIR_PERMISSION || fromType == GROUP_JION_PERMISSION) {
			grantBuffer.append("{\"12\":[");
			for (int i = 0; i < mGroupAdapter.getSelectGroups().size(); i++) {
				if (i == mGroupAdapter.getSelectGroups().size() - 1) {
					grantBuffer.append(mGroupAdapter.getSelectGroups().get(i)
							.getGid());
					mSelectGroupNames.append(mGroupAdapter.getSelectGroups()
							.get(i).getName());
				} else {
					grantBuffer.append(mGroupAdapter.getSelectGroups().get(i)
							.getGid()
							+ ",");
					mSelectGroupNames.append(mGroupAdapter.getSelectGroups()
							.get(i).getName());
					mSelectGroupNames.append(";");
				}
			}
			grantBuffer.append("]}");
		}
		return grantBuffer.toString();
	}

	private StringBuffer mSelectGroupNames;

	// >>>>>>>>>根据好友分组设置权限
	private void rootGroup() {
		// TODO Auto-generated method stub
		if (mGroupAdapter.getSelectGroups().size() == 0) {
			showToast("请选择一个分组!");
			return;
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", application.getMyUserID()));
		paras.add(new Parameter("dirid", dirid));
		paras.add(new Parameter("grantinfo", getGrantByType()));
		paras.add(new Parameter("type", String
				.valueOf(PangkerConstant.Role_CONTACT_GROUP)));
		serverMana.supportRequest(Configuration.getResInfoQuerySet(), paras,
				true, "正在处理...", ROOT_GROUP);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
			tv_empty_prompt.setText(R.string.no_network);
			return;
		}

		switch (caseKey) {
		case DELETE_GROUP:
		case ADD_GROUP:
		case UPDATE_GROUP:
			groupManagerResult(response.toString(), caseKey);
			break;

		case ADD_TO_GROUP:
			GroupRelationManagerResult toGroup = JSONUtil.fromJson(
					response.toString(), GroupRelationManagerResult.class);
			if (toGroup != null && toGroup.getErrorCode() == BaseResult.SUCCESS) {
				for (UserItem item : selectedUser) {
					friendsDao.addFriend(item, currentGroup.getGid());
				}
				currentGroup.addUserList(selectedUser);
				mGroupAdapter.notifyDataSetChanged();
				showToast(toGroup.getErrorMessage());
			} else if (toGroup != null
					&& toGroup.getErrorCode() == BaseResult.FAILED) {
				showToast(toGroup.getErrorMessage());
			} else {
				showToast(R.string.operate_fail);
			}
			break;
			
		case ROOT_GROUP:
			BaseResult result = JSONUtil.fromJson(response.toString(),
					BaseResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				showToast(result.getErrorMessage());
				Intent intent = new Intent();
				// >>>>>>当前选择群组名称
				intent.putExtra("select_group_name",
						mSelectGroupNames.toString());
				// >>>>>当前选择群组ID

				intent.putExtra(SELECT_CONTACT_GROUP_ID, mGroupAdapter
						.getSelectGroups().get(0).getGid());
				setResult(10, intent);
				finish();
			} else if (result != null
					&& result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
			} else {
				showToast(R.string.operate_fail);
			}
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (isUpdate == 1) {
			setResult(RESULT_OK);
			finish();
		}
		super.onBackPressed();
	}

}
