package com.wachoo.pangker.activity.contact;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;

public class FriendAddActivity extends CommonPopActivity {

	private Button btn_back;
	private Button btn_more;
	private TextView tv_title;

	private LinearLayout ly_pkno_add; // 旁客号码添加
	private LinearLayout ly_phone_add;// 手机号码添加
	private LinearLayout ly_localcontacts_add;// 本地通讯录邀请
	private LinearLayout ly_web_add;// 个人网站
	private String fromactivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_friend_add);
		
		fromactivity = getIntent().getStringExtra("fromactivity");

		tv_title = (TextView) findViewById(R.id.mmtitle);
		if(fromactivity != null){
			tv_title.setText("搜索用户");
		}
		else 
		   tv_title.setText("添加好友");
		
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(clickListener);
		btn_more = (Button) findViewById(R.id.iv_more);
		btn_more.setVisibility(View.GONE);

		ly_pkno_add = (LinearLayout) findViewById(R.id.ly_pkno_add);
		ly_pkno_add.setOnClickListener(clickListener);
		ly_phone_add = (LinearLayout) findViewById(R.id.ly_phone_add);
		ly_phone_add.setOnClickListener(clickListener);
		ly_localcontacts_add = (LinearLayout) findViewById(R.id.ly_localcontacts_add);
		ly_localcontacts_add.setOnClickListener(clickListener);
		ly_web_add = (LinearLayout) findViewById(R.id.ly_web_add);
		ly_web_add.setOnClickListener(clickListener);
		//如果是在附近行人搜索用户，则不需要查找通讯录联系人
		if(fromactivity != null && fromactivity.equals("NearUserActivity")){
			ly_localcontacts_add.setVisibility(View.GONE);
			ly_web_add.setBackgroundResource(R.drawable.item_bg_bottom_selector);
		}
		else ly_localcontacts_add.setVisibility(View.VISIBLE);
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btn_back) {
				FriendAddActivity.this.finish();
			} else if (v == ly_phone_add) {
				Intent intent = new Intent(FriendAddActivity.this, ContactFriendInviteActivity.class);
				intent.putExtra("type", 0);
				FriendAddActivity.this.startActivity(intent);
			} else if (v == ly_pkno_add) {
				Intent intent = new Intent(FriendAddActivity.this, ContactFriendInviteActivity.class);
				intent.putExtra("type", 1);
				FriendAddActivity.this.startActivity(intent);
			} else if (v == ly_localcontacts_add) {
				Intent intent = new Intent(FriendAddActivity.this, LocalContactsSelectActivity.class);
				FriendAddActivity.this.startActivity(intent);
			}
			else if (v == ly_web_add) {
				Intent intent = new Intent(FriendAddActivity.this, ContactFriendInviteActivity.class);
				intent.putExtra("type", 2);
				FriendAddActivity.this.startActivity(intent);
			}
		}
	};
}
