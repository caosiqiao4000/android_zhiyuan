package com.wachoo.pangker.activity.contact;

import mobile.http.Parameter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.PKIconResizer;

//>>>>> wangxin 
public class UserSearchActivity extends CommonPopActivity {

	protected View loadmoreView;
	protected LinearLayout lyLoadPressBar;
	protected LinearLayout lyLoadText;
	protected TextView loadText;

	protected int startLimit = 0;
	protected int endLimit = 0;
	public static final int incremental = 10;
	protected boolean isLoadAll = false;

	protected PKIconResizer mImageResizer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	// >>>>>> getSearchLimit每次加载incremental个
	protected Parameter getSearchLimit() {
		endLimit = startLimit + incremental;
		return new Parameter("limit", startLimit + "," + incremental);
	}

	// >>>>>>>>>> setStartLimit
	public void setStartLimit(int startLimit) {
		this.startLimit = startLimit;
	}

	// >>>>>>>>> setStartLimit
	public void resetStartLimit() {
		startLimit = endLimit;
	}

	// 刷新，重新查询
	public void reset() {
		startLimit = 0;
		isLoadAll = false;
	}

	/**
	 * 后台数据是否已经加载完成
	 * 
	 * @return
	 */
	public boolean isLoadAll() {
		return isLoadAll;
	}

	public void setLoadAll(boolean isLoadAll) {
		this.isLoadAll = isLoadAll;
	}

	public int getStartLimit() {
		return startLimit;
	}

	
	protected void initPressBar(ListView mListView) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		loadmoreView = inflater.inflate(R.layout.loadmore_footerview, null);
		mListView.addFooterView(loadmoreView);
		lyLoadPressBar = (LinearLayout) loadmoreView.findViewById(R.id.ly_loadmore_probar);
		lyLoadText = (LinearLayout) loadmoreView.findViewById(R.id.ly_loadmore_text);
		loadText = (TextView) loadmoreView.findViewById(R.id.tv_loadmore);
	}

	protected void setPresdBarVisiable(boolean isShow) {
		if (isShow) {
			lyLoadPressBar.setVisibility(View.VISIBLE);
			lyLoadText.setVisibility(View.GONE);
		} else {
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.VISIBLE);
		}

	}


}
