package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.TraceInAdapter;
import com.wachoo.pangker.adapter.UtitsInviteAdapter;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.HorizontalListView;

public class UtitsContactInviteActivity extends CommonPopActivity {

	private final String[] GroupName = { "我的好友" };
	private String msg = "邀请加入Ta的单位通讯录";
	private List<UserItem> inviteLits;
	private PangkerApplication application;

	private ListView inviteListView;
	private UtitsInviteAdapter utitsInviteAdapter;
	private HorizontalListView horizontalListView;
	private TraceInAdapter traceInAdpater;
	private Button btnConfrim;
	private Button btnCancel;
	private TextView txtCount;
	private AddressBooks addressBooks;
	private EmptyView emptyView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.units_invite);
		application = (PangkerApplication) getApplication();
		addressBooks = (AddressBooks) this.getIntent().getSerializableExtra(AddressBooks.BOOK_KEY);
		initView();
		initDatas();
	}

	private void initView() {
		// TODO Auto-generated method stub
		inviteListView = (ListView) findViewById(R.id.traceMemberList);
		inviteListView.setOnItemClickListener(vOnItemClickListener);
		emptyView = new EmptyView(this);
		emptyView.addToListView(inviteListView);
		horizontalListView = (HorizontalListView) findViewById(R.id.traceHorizontalListView);
		traceInAdpater = new TraceInAdapter(this);
		horizontalListView.setAdapter(traceInAdpater);

		btnConfrim = (Button) findViewById(R.id.traceIn_confrim);
		btnCancel = (Button) findViewById(R.id.traceIn_cancel);
		btnConfrim.setOnClickListener(mClickListener);
		btnCancel.setOnClickListener(mClickListener);
		txtCount = (TextView) findViewById(R.id.traceIn_count);
	}

	private void initDatas() {
		// TODO Auto-generated method stub
		inviteLits = new ArrayList<UserItem>();
		// 加载好友
		if (application.getFriendList() != null) {
			inviteLits = application.getFriendList();
		}
		if (inviteLits != null && inviteLits.size() <= 0) {
			emptyView.showView(R.drawable.emptylist_icon, "你还没有好友！赶快去添加吧。");
		}
		utitsInviteAdapter = new UtitsInviteAdapter(this, inviteLits);
		inviteListView.setAdapter(utitsInviteAdapter);
	}

	AdapterView.OnItemClickListener vOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			UtitsInviteAdapter.ViewHolder vHollder = (UtitsInviteAdapter.ViewHolder) view.getTag();
			vHollder.selected.toggle();
			utitsInviteAdapter.selectByIndex(position, vHollder.selected.isChecked());
			traceInAdpater.dealUserItem(inviteLits.get(position), vHollder.selected.isChecked());
			changeCount();
		}
	};

	private OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			if (view == btnConfrim) {
				inviteMembers();
			}
			if (view == btnCancel) {
				UtitsContactInviteActivity.this.finish();
			}
		}
	};

	private void changeCount() {
		// TODO Auto-generated method stub
		String countString = "(" + (traceInAdpater.getCount() - 1) + ")";
		txtCount.setText(countString);
	}

	private void inviteMembers() {
		// TODO Auto-generated method stub
		String uid = application.getMyUserID();
		final OpenfireManager openfireManager = application.getOpenfireManager();
		if (openfireManager.isLoggedIn()) {
			if (traceInAdpater.getCount() > 1) {
				for (UserItem userItem : traceInAdpater.getUserItems()) {
					openfireManager.sendNetAddressBookInviteReq(uid, userItem.getUserId(), addressBooks, msg);
				}
				showToast(R.string.net_addressbook_apply_mess);
				finish();
			}

		} else {
			showToast(R.string.not_logged_cannot_operating);
		}

	}

}
