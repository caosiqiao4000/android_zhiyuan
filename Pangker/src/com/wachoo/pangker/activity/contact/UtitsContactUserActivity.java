package com.wachoo.pangker.activity.contact;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.activity.group.MyGroupListsActivity;
import com.wachoo.pangker.adapter.ContactsSelectAdapter;
import com.wachoo.pangker.adapter.ContactsSelectedAdapter;
import com.wachoo.pangker.adapter.UnitsContactUserAdapter;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBookResult;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.AddressMembers;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupEnterCheckResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.HorizontalListView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

/**
 * 单位通讯录成员列表界面
 * @author zhengjy 2012-6-12
 * 
 */
public class UtitsContactUserActivity extends CommonPopActivity implements IUICallBackInterface {

	private final int[] MENU_INDEXS = { 0x100, 0x101, 0x103, 0x104, 0x105 };
	private final String[] MENU_TITLE = { "添加联系人", "邀请好友", "召开会议", "退出单位", "导出到手机通讯录" };
	private final int QUERY_CONTACT = 0x10;
	private final int DELETE_USER = 0x11;
	private final int REMARK_USER = 0x12;
	private final int EXIT_BOOKS = 0x13;
	private final int UPDATE_USER = 0x14;
	private int currDialogId = -1;

	private PangkerApplication application;
	private PullToRefreshListView pullToRefreshListView;
	private ListView mListView;
	private TextView tvTitle;
	private UnitsContactUserAdapter mContactAdapter;
	private QuickAction popopQuickAction;
	private Button btnMenuShow;
	private Button btn_back;// 返回按钮

	private String mUserid;
	private AddressMembers addressMember;
	private AddressBooks currBooks;
	
	//群组邀请的Adapter
	private UserGroup groupInfo;
	private ArrayList<UserItem> memberList;
	private LinearLayout selected_layout;
	private HorizontalListView lv_selected_members;// 下方选择头像区域
	private ContactsSelectedAdapter selectedAdapter; // 选中的人员
	private ContactsSelectAdapter memberAdapter;// 选择人员的列表
	private Button btn_select_over; // btn_select_over

	private PKIconResizer mImageResizer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blacklist_manager);
		//初始化数
		init();
		//初始化控件
		initView();
		initGroupView();
		//查询单位通讯录
		AddressBookMembers(String.valueOf(currBooks.getId()), "-1");
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		mUserid = application.getMyUserID();
		currBooks = (AddressBooks) this.getIntent().getSerializableExtra(AddressBooks.BOOK_KEY);

		IntentFilter filter = new IntentFilter();
		filter.addAction(PangkerConstant.ACTION_ADDRESSBOOK_EXIT);// 广播通知
		registerReceiver(addressBookReceiver, filter);
		
		mImageResizer = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);
	}

	private BroadcastReceiver addressBookReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			if (action == null)
				return;
			if (action.equals(PangkerConstant.ACTION_ADDRESSBOOK_EXIT)) {
				long bookId = intent.getLongExtra(AddressBooks.BOOK_ID_KEY, -1);
				if (bookId == currBooks.getId()) {
					application.removeNetAddressBook(bookId);
					UtitsContactUserActivity.this.finish();
				}
			}
		}
	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver(addressBookReceiver);
		super.onDestroy();
	}

	private void initView() {
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setTag("pk_utitscontacts_members");
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		mListView = pullToRefreshListView.getRefreshableView();
		FooterView footerView = new FooterView(this);
		footerView.addToListView(mListView);
		// footerView.hideFoot();
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(mOnClickListener);
		tvTitle = (TextView) findViewById(R.id.mmtitle);
		btnMenuShow = (Button) findViewById(R.id.iv_more);
		btnMenuShow.setBackgroundResource(R.drawable.btn_menu_bg);
		tvTitle.setText(currBooks.getName());

		mContactAdapter = new UnitsContactUserAdapter(this, new ArrayList<AddressMembers>(), mImageResizer);
		mListView.setAdapter(mContactAdapter);

		popopQuickAction = new QuickAction(this, QuickAction.VERTICAL);
		popopQuickAction.setOnActionItemClickListener(onActionItemClickListener);
		if (String.valueOf(currBooks.getUid()).equals(mUserid)) {
			popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[0], MENU_TITLE[0]));
			popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[1], MENU_TITLE[1]));
		} else {
			popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[3], MENU_TITLE[3]));
		}
		popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[2], MENU_TITLE[2]));
		popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[4], MENU_TITLE[4]), true);
		btnMenuShow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popopQuickAction.show(btnMenuShow);
			}
		});

		mListView.setOnItemClickListener(onItemClickListener);
		mListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				addressMember = (AddressMembers) arg0.getAdapter().getItem(arg2);
				if (String.valueOf(currBooks.getUid()).equals(mUserid)) {
					if (!String.valueOf(addressMember.getUid()).equals(mUserid)) {
						showPopMenu(2);
					} else {
						showPopMenu(1);
					}
				}
				// 不是自己创建的单位通讯录，但当前用户是自己时可以修改信息
				else if (String.valueOf(addressMember.getUid()).equals(mUserid)) {
					showPopMenu(0);
				}
				return true;
			}
		});
	}
	
	private OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			// TODO Auto-generated method stub
			if(groupInfo == null){
				addressMember = (AddressMembers) arg0.getAdapter().getItem(position);
				Intent intent = new Intent(UtitsContactUserActivity.this, UtitsContactUserDetailActivity.class);
				intent.putExtra(AddressMembers.ADDRESSMEMBER_KEY, addressMember);
				intent.putExtra(AddressBooks.BOOK_KEY, currBooks);
				startActivityForResult(intent, UPDATE_USER);
			} else {
				selectedAdapter.dealUserItem(memberList.get(position - 1), memberAdapter.onCheckBoxClick(position - 1));
				if (selectedAdapter.getCount() > 0) {
					btn_select_over.setText("邀请(" + (selectedAdapter.getCount() - 1) + ")");
				}
			}
		}
	};

	PopMenuDialog menuDialog;
	UITableView.ClickListener clickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			if (actionid == 1) {
				Intent intent = new Intent(UtitsContactUserActivity.this, UtitsContactUserEditActivity.class);
				intent.putExtra(AddressMembers.ADDRESSMEMBER_KEY, addressMember);
				startActivityForResult(intent, 100);
			} else if (actionid == 2) {
				mShowDialog(DELETE_USER);
			}
			menuDialog.dismiss();
		}
	};
	ActionItem[] mActionItems0 = new ActionItem[] { new ActionItem(1, "修改") };
	ActionItem[] mActionItems1 = new ActionItem[] { new ActionItem(2, "删除") };
	ActionItem[] mActionItems2 = new ActionItem[] { new ActionItem(1, "修改"), new ActionItem(2, "删除") };

	private void showPopMenu(int type) {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this);
			menuDialog.setListener(clickListener);
		}
		menuDialog.setTitle(addressMember.getRemarkname());
		if (type == 0) {
			menuDialog.setMenus(mActionItems0);
		}
		if (type == 1) {
			menuDialog.setMenus(mActionItems1);
		}
		if (type == 2) {
			menuDialog.setMenus(mActionItems2);
		}
		menuDialog.show();
	}

	/**
	 * 如果是自己创建的单位通讯录，可以添加非旁客用户联系人到本通讯录
	 */
	private void addContactUser() {
		Intent intent = new Intent(this, UtitsContactUserAddActivity.class);
		intent.putExtra(AddressBooks.BOOK_KEY, currBooks);
		this.startActivity(intent);
	}

	/**
	 * @param name
	 * @param url
	 */
	public void showInfo(AddressMembers user) {
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, user.getUid().toString());
		intent.putExtra(UserItem.USERNAME, user.getRemarkname());
		intent.putExtra(UserItem.USERSIGN, "");
		startActivity(intent);
	}
	
	private View.OnClickListener mOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v == btn_back){
				if(groupInfo != null){
					groupInfo = null;
					showGroupView(false);
				} else {
					finish();
				}
			}
			if(v == btn_select_over){
				inviteIntoGroup(groupInfo.getGroupId());
			}
		}
	};
	
	OnRefreshListener<ListView> onRefreshListener = new OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			if(groupInfo == null){
				AddressBookMembers(String.valueOf(currBooks.getId()), String.valueOf(currBooks.getVersion()));
			}
		}
	};
	
	private ArrayList<UserItem> getJion2Members() {
		// TODO Auto-generated method stub
		ArrayList<UserItem> jion2Members = new ArrayList<UserItem>();
		// >>>>>>>>选择有效用户留下
		for (AddressMembers addressMembers : mContactAdapter.getmContactList()) {
			// >>>>>>>判断UID是否有效 同时不能是自己
			if (addressMembers.getUid() != null && addressMembers.getUid().longValue() != 0l
					&& !addressMembers.getUid().equals(Util.String2Long(mUserid)) ) {
				UserItem item = new UserItem();
				item.setUserId(String.valueOf(addressMembers.getUid()));
				item.setUserName(addressMembers.getRemarkname());
				jion2Members.add(item);
			}
		}
		return jion2Members;
	}
	
	// 邀请联系人
	private void inviteContacts() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ContactsSelectActivity.class);
		intent.putExtra(ContactsSelectActivity.INTENT_SELECT, ContactsSelectActivity.TYPE_UNIT_INVITE);
		intent.putExtra(AddressBooks.BOOK_KEY, currBooks);
		startActivity(intent);
	}
	
	private void inviteGroup() {
		// TODO Auto-generated method stub
		if (mContactAdapter.getmContactList() == null || mContactAdapter.getmContactList().size() == 0) {
			showToast("请等待成员加载完毕");
			return;
		}
		Intent intent = new Intent(this, MyGroupListsActivity.class);
		intent.putExtra(MyGroupListsActivity.FROM_TYPE, MyGroupListsActivity.INVITE_2_UTITSROME);
		 
		startActivityForResult(intent, 2);
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == MENU_INDEXS[0]) { // 添加联系人
				addContactUser();
			} else if (actionId == MENU_INDEXS[1]) { // 邀请联系人
				inviteContacts();
			} else if (actionId == MENU_INDEXS[2]) {// >>>>>>>>>召开会议
				inviteGroup();
			} else if (actionId == MENU_INDEXS[3]) { // 退出通讯录
				mShowDialog(EXIT_BOOKS);
			} else if (actionId == MENU_INDEXS[4]) {// 导出道本地通讯录
				Intent intent = new Intent(UtitsContactUserActivity.this, UtitsContactAddtoLocalActivity.class);
				intent.putExtra("contactList", (Serializable) mContactAdapter.getmContactList());
				startActivity(intent);
			}
		}
	};

	private MessageTipDialog mTipDialog;
	/**
	 * 显示dialog前的设置
	 * @param id
	 */
	private void mShowDialog(int id) {
		currDialogId = id;
		if(mTipDialog == null){
			mTipDialog = new MessageTipDialog(resultCallback, this);
		}
		if(EXIT_BOOKS == currDialogId){
			mTipDialog.showDialog(currBooks.getName(), getString(R.string.units_move_out_ifornot));
		} else if (DELETE_USER == currDialogId) {
			mTipDialog.showDialog(currBooks.getName(), getString(R.string.units_delete_user_ifornot));
		}
		
	}
	
	MessageTipDialog.DialogMsgCallback resultCallback = new DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if(isSubmit){
				if(currDialogId == DELETE_USER){
					AddressBookDeleMember(String.valueOf(addressMember.getId()));
				}
				if(EXIT_BOOKS == currDialogId){
					sendNetAddressBookMessage(EXIT_BOOKS);
				}
			}
		}
	};


	/**
	 * 单位通讯录成员查询
	 * 
	 * 
	 * 接口：AddressBookMembers.json
	 */
	private void AddressBookMembers(String contactId, String version) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", mUserid)); // 操作者UID。
		paras.add(new Parameter("gid", contactId));// 通讯录唯一标示ID。
		if (version == null || version.equals("") || version.equals("null")) {
			paras.add(new Parameter("version", "-1"));// 查询的版本号，默认为空，查询所有成员信息列表。
		} else
			paras.add(new Parameter("version", version));// 查询的版本号，默认为空，查询所有成员信息列表。
		serverMana.supportRequest(Configuration.getAddressBookMembers(), paras, QUERY_CONTACT);
		pullToRefreshListView.setRefreshing(true);
	}

	/**
	 * 删除单位通讯录成员接口
	 * 接口：AddressBookDeleMember.json
	 */
	private void AddressBookDeleMember(String rids) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", mUserid)); // 当前操作用户ID。
		paras.add(new Parameter("rids", rids)); // 关系ID。格式：“id1,id2….”
		paras.add(new Parameter("bookid", "")); // 通信录ID，表明rids从属某个通讯录，初期一个用户只支持一个通讯录，该字段保留，不启用。
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getAddressBookDeleMember(), paras, true, mess, DELETE_USER);
	}

	/**
	 * 删除单位通讯录用户通知
	 */
	private void sendNetAddressBookMessage(final int optype) {
		OpenfireManager openfireManager = application.getOpenfireManager();
		if (openfireManager.isLoggedIn()) {
			if (DELETE_USER == optype) {
				openfireManager.sendBeDelNetAddressBook(mUserid, String.valueOf(addressMember.getUid()),
						String.valueOf(currBooks.getId()), "");
			} else if (EXIT_BOOKS == optype) {
				showToast(R.string.net_addressbook_apply_mess);
				openfireManager.sendNetAddressBook(mUserid, String.valueOf(currBooks.getUid()), "", "ok",
						currBooks, BusinessType.applyExitNetAddressBook);
			}
		} else {
			showToast(PangkerConstant.MSG_NO_ONLINE);
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (caseKey == QUERY_CONTACT) {
			pullToRefreshListView.onRefreshComplete();
		}
		if (!HttpResponseStatus(response)) {
			return;
		}
		if (caseKey == QUERY_CONTACT) {
			AddressBookResult addressResult = JSONUtil.fromJson(response.toString(), AddressBookResult.class);
			if (null == addressResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (addressResult.getErrorCode() == BaseResult.SUCCESS && addressResult.getMembers() != null) {
				mContactAdapter.setmContactList(addressResult.getMembers());
				if (currBooks.getVersion() != null && addressResult.getVersion() != currBooks.getVersion().longValue()) {
					currBooks.setVersion(addressResult.getVersion());
					currBooks.setNum(addressResult.getMembers().size());
				}
			} else if (addressResult.getErrorCode() == BaseResult.FAILED) {
				showToast(addressResult.getErrorMessage());
			} else if (addressResult.getErrorCode() == AddressBookResult.VERSION_RESULT) {
				showToast(R.string.contact_user_refresh);
			} else {
				showToast(R.string.to_server_fail);
			}
		} else if (caseKey == REMARK_USER) {
//			AddressBookResult baseResult = JSONUtil.fromJson( response.toString(), AddressBookResult.class);
//			if (null == baseResult) {
//				showToast(R.string.to_server_fail);
//				return;
//			} else if (baseResult.getErrorCode() == BaseResult.SUCCESS) {
//				showToast(baseResult.getErrorMessage());
//				addressMember.setRemarkname(etEditName.getText().toString());
//				mContactAdapter.updateListData(addressMember);
//			} else if (baseResult.getErrorCode() == BaseResult.FAILED) {
//				showToast(baseResult.getErrorMessage());
//			} else
//				showToast(R.string.to_server_fail);
		}

		else if (caseKey == DELETE_USER) {
			AddressBookResult baseResult = JSONUtil.fromJson( response.toString(), AddressBookResult.class);
			if (null == baseResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (baseResult.getErrorCode() == BaseResult.SUCCESS) {
				// 如果当前用户是旁客用户，则使用旁客消息通知用户
				if (addressMember.getUid() != null) {
					sendNetAddressBookMessage(DELETE_USER);
				}
				showToast(baseResult.getErrorMessage());
				mContactAdapter.removeBooks(addressMember);

			} else if (baseResult.getErrorCode() == BaseResult.FAILED) {
				showToast(baseResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}
	}
	/**
	 * 是否显示群组邀请的
	 * @param isInvite
	 * @param groupInfo
	 */
	private void showGroupView(boolean isInvite) {
		// TODO Auto-generated method stub
		pullToRefreshListView.setPullToRefreshEnabled(!isInvite);
        if(isInvite){
        	selected_layout.setVisibility(View.VISIBLE);
        	btnMenuShow.setVisibility(View.GONE);
        	memberList = getJion2Members();
        	memberAdapter = new ContactsSelectAdapter(this, memberList);
        	mListView.setAdapter(memberAdapter);
        	selectedAdapter.clearUserItems();
        } else {
        	selected_layout.setVisibility(View.GONE);
        	mListView.setAdapter(mContactAdapter);
        	btnMenuShow.setVisibility(View.VISIBLE);
        }
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		//从群组列表获取返回的群组
		if(requestCode == 2 && resultCode == RESULT_OK){
			this.groupInfo = (UserGroup) data.getSerializableExtra("UserGroup");
			showGroupView(true);
		} else if((requestCode == UPDATE_USER || requestCode == 100)&& resultCode == RESULT_OK){
			AddressMembers mMember = (AddressMembers) data.getSerializableExtra(AddressMembers.ADDRESSMEMBER_KEY);
			cloneAddressMembers(mMember);
		}  
	}
	
	private void cloneAddressMembers(AddressMembers mMember) {
		// TODO Auto-generated method stub
		addressMember.setAddress(mMember.getAddress());
		addressMember.setRemarkname(mMember.getRemarkname());
		addressMember.setMobile(mMember.getMobile());
		addressMember.setPhone1(mMember.getPhone1());
		addressMember.setPhone2(mMember.getPhone2());
		addressMember.setPhone3(mMember.getPhone3());
		addressMember.setBookId(mMember.getBookId());
		addressMember.setDepartment(mMember.getDepartment());
		addressMember.setPhoneCount(mMember.getPhoneCount());
		addressMember.setEmail(mMember.getEmail());
		addressMember.setZipcode(mMember.getZipcode());
		mContactAdapter.notifyDataSetChanged();
	}
	
	public void gotoRoom(UserGroup userGroup) {
		Intent intent = new Intent();
		// >>>>>wangxin add 进入群组
		if (userGroup != null) {
			intent.putExtra(ChatRoomMainActivity.JION_TO_GROUP, userGroup);
			// 将进入群组的消息纪律到MsgInfo中
			application.getMsgInfoBroadcast().sendMeetroom(userGroup);
		} else {
			application.getMsgInfoBroadcast().sendMeetroom(application.getCurrunGroup());
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		}
		intent.setClass(this, ChatRoomMainActivity.class);
		startActivity(intent);
		// 进入之前进行邀请动作
		sendInvite();
	}
    //发送邀请信息
	private void sendInvite() {
		// TODO Auto-generated method stub
		if (selectedAdapter.getUserItems().size() < 1) {
			showToast(R.string.rootwho_null);
		} else if (selectedAdapter.getUserItems().size() > 200) {
			showToast(R.string.rootwho_spill);
		} else {
			if (!application.getOpenfireManager().isLoggedIn()) {
				showToast(R.string.not_logged_cannot_operating);
				return;
			}
			// 邀请操作msg===>Id#groupname
			String msg = groupInfo.getGroupId() + "#" + groupInfo.getGroupName();
			// >>>>>>>>发送邀请通知
			for (int i = 0; i < selectedAdapter.getUserItems().size(); i++) {
				application.getOpenfireManager().sendGroupInvite(application.getMyUserID(),
						selectedAdapter.getUserItems().get(i).getUserId(), msg);
			}
			showToast("已成功发送邀请消息!");
		}
		finish();
	}

	private void inviteIntoGroup(final String groupId) {
		// TODO Auto-generated method stub
		if (application.getCurrunGroup() != null // 3：就在当前的群组中
				&& application.getCurrunGroup().getUid() != null
				&& application.getCurrunGroup().getGroupId().equals(groupId)) {
			gotoRoom(null);
		} else if (application.getCurrunGroup() != null // 2：在一个群组中，但不在点击的群组
				&& application.getCurrunGroup().getUid() != null
				&& !application.getCurrunGroup().getGroupId().equals(groupId)) {

			MessageTipDialog mDialog = new MessageTipDialog(new DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-genaerated method stub
					if (isSubmit) {
						// 首先要关闭之前的群组界面
						PangkerManager.getActivityStackManager().popOneActivity(ChatRoomMainActivity.class);
						groupEnterCheck(groupId);
					}
				}
			}, this);
			mDialog.showDialog("您已经在一个群组里面,进入此群组会退出当前所在的群组,是否确认进入?", false);
		} else {// 1:自己不在任何一个群组中
			groupEnterCheck(groupId);
		}
	}
	
	private void groupEnterCheck(String groupId) {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", application.getMyUserID()));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("optype", "2"));//
		// 1：申请进入群组（必填）；2:被邀请加入；3：直接进入（自己群组，收藏的群组等等 ）
		paras.add(new Parameter("password", ""));// 群组密码，需要密码校验时使用
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				GroupEnterCheckResult result = JSONUtil.fromJson(response.toString(),
						GroupEnterCheckResult.class);
				if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
					UserGroup enterGroup = result.getUserGroup();
					if (enterGroup != null) {
						enterGroup.setLastVisitTime(Util.date2Str(new Date()));
						gotoRoom(enterGroup);
					} else {
						showToast("查询失败,还无法登录该群组!");
					}
				} else {
					showToast(R.string.return_value_999);
				}
			}
		});
		serverMana.supportRequest(Configuration.getGroupEnterCheck(), paras, true, "正在进入群组...", 0);
	}

	private void initGroupView() {
		// 选择的人的头像列表
		selected_layout = (LinearLayout) findViewById(R.id.selected_layout);
		lv_selected_members = (HorizontalListView) findViewById(R.id.lv_selected_members);
		lv_selected_members.setOnItemClickListener(donItemClickListener);
		selectedAdapter = new ContactsSelectedAdapter(this);
		lv_selected_members.setAdapter(selectedAdapter);
		btn_select_over = (Button) findViewById(R.id.btn_select_over);
		btn_select_over.setOnClickListener(mOnClickListener);
	}

	OnItemClickListener donItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
			// TODO Auto-generated method stub
			String userId = selectedAdapter.removeUser(position);
			if (!Util.isEmpty(userId)) {
				memberAdapter.onUnCheckedClick(userId);
				if (selectedAdapter.getCount() > 0) {
					btn_select_over.setText("邀请(" + (selectedAdapter.getCount() - 1) + ")");
				}
			}
		}
	};
	
}
