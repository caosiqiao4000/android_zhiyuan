package com.wachoo.pangker.activity.contact;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.ContactsSelectAdapter;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.ui.EmptyView;

/**
 * @author zxx
 * 选择一个好友，在onActivityResult返回一个UserItem，次界面不做任何业务操作
 */
public class FriendSelectActivity extends CommonPopActivity{

	private ListView friendListView;
	private EmptyView mEmptyView;
	private ContactsSelectAdapter memberAdapter;// 选择人员的列表
	private PangkerApplication application;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_mmusic);
		initView();
		initData();
	}

	private void initView() {
		// TODO Auto-generated method stub
		((TextView) findViewById(R.id.mmtitle)).setText("我的好友");
	    findViewById(R.id.btn_back).setOnClickListener(onClickListener);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		
		friendListView = (ListView) findViewById(R.id.res_musicListView);
		friendListView.setOnItemClickListener(onItemClickListener);
		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(friendListView);
	}
	
	private void initData() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		memberAdapter = new ContactsSelectAdapter(this, application.getFriendList());
		memberAdapter.setCanSelect(false);
		friendListView.setAdapter(memberAdapter);
		mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_friend);
	}
	
	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.btn_back) {
				onBackPressed();
			}
		}
	};
	
	/* checkbox选中一个/单击上传 */
	AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			UserItem userItem = (UserItem) parent.getItemAtPosition(position);
			dealUserItem(userItem);
		}
	};

	private void dealUserItem(UserItem userItem) {
		// TODO Auto-generated method stub
		Intent data = new Intent();
		data.putExtra("UserItem", userItem);
		setResult(RESULT_OK, data);
		finish();
	}
}
