package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.InvitedUserAdapter;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.InviteRegisterResult;
import com.wachoo.pangker.server.response.InviteResult;
import com.wachoo.pangker.server.response.SearchUnderUsersResult;
import com.wachoo.pangker.server.response.SimpleUserInfo;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.util.SMSUtil;

/**
 * 获取邀请用户的接口
 * 
 * @author wangxin
 * 
 */
public class UnderUserActivity extends CommonPopActivity implements IUICallBackInterface {

	private final int GETUNDERINFO = 0x10;
	private final int INVITE_TO_PANGKER = 0x11;
	private ListView usersListView;
	private PullToRefreshListView pullToRefreshListView;//
	private String mUserId;
	private InvitedUserAdapter adapter;

	private EmptyView mEmptyView;
	private TextView tv_tips;

	private Button mBtnMenu;
	private QuickAction quickAction;
	private PKIconResizer mImageResizer;
	private PangkerApplication application;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blacklist_manager);
		init();
		initView();

		getUnderInfo();
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		mUserId = application.getMyUserID();
		mImageResizer = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);
	}

	private void initView() {
		// TODO Auto-generated method stub
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText(R.string.invite_user_title);
		tv_tips = (TextView) findViewById(R.id.tv_tips);
		mBtnMenu = (Button) findViewById(R.id.iv_more);
		mBtnMenu.setBackgroundResource(R.drawable.btn_menu_bg);
		mBtnMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				quickAction.show(mBtnMenu);
			}
		});
		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.addActionItem(new ActionItem(1, getString(R.string.invite_user)));

		quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {

			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {
				// TODO Auto-generated method stub
				if (actionId == 1) {
					Intent intent = new Intent(UnderUserActivity.this, LocalContactsSelectActivity.class);
					intent.putExtra("fromType", 3);
					UnderUserActivity.this.startActivity(intent);
				}
			}
		});
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		usersListView = pullToRefreshListView.getRefreshableView();

		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(usersListView);

		adapter = new InvitedUserAdapter(this, InvitedUserAdapter.INVITED_USERS, mImageResizer);

		usersListView.setAdapter(adapter);

		usersListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				final SimpleUserInfo currChooseUser = (SimpleUserInfo) arg0.getItemAtPosition(arg2);
				if (currChooseUser.getIsPangker() == 1) {
					Intent intent = new Intent(UnderUserActivity.this, UserWebSideActivity.class);
					intent.putExtra(UserItem.USERID, currChooseUser.getUserId());
					intent.putExtra(UserItem.USERNAME, currChooseUser.getUserName());
					intent.putExtra(UserItem.USERSIGN, currChooseUser.getSign() != null ? currChooseUser.getSign() : "");
					startActivity(intent);
				} else {
					MessageTipDialog tipDialog = new MessageTipDialog(new DialogMsgCallback() {
						@Override
						public void buttonResult(boolean isSubmit) {
							// TODO Auto-generated method stub
							if (isSubmit) {
								invite2Panker(currChooseUser.getPhone());
							}
						}
					}, UnderUserActivity.this);
					tipDialog.showDialog(currChooseUser.getNickName(), "该联系人还没有注册，是否重新邀请注册？", false);
				}
			}
		});

	}

	private void getUnderInfo() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", mUserId));
		paras.add(new Parameter("uid", mUserId));
		paras.add(new Parameter("limit", "0,100"));
		serverMana.supportRequest(Configuration.getSearchUnderUsers(), paras, GETUNDERINFO);
		pullToRefreshListView.setRefreshing(true);
	}

	/**
	 * 邀请通讯录好友注册旁客
	 * 
	 * @param phoneNum
	 *            被邀请人手机号 接口：InviteRegister.json
	 */
	private void invite2Panker(String phoneNum) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		String mPhoneNum = ((PangkerApplication) getApplication()).getMySelf().getPhone();
		showToast(R.string.invite_is_sended);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("inviteuid", mUserId));
		paras.add(new Parameter("invitemobile", mPhoneNum != null ? mPhoneNum : ""));
		paras.add(new Parameter("beinvitemobiles", phoneNum));
		serverMana.supportRequest(Configuration.getInviteRegister(), paras, INVITE_TO_PANGKER);
	}

	OnRefreshListener<ListView> onRefreshListener = new OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			getUnderInfo();
		}
	};

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(supportResponse)) {
			mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
		}

		if (caseKey == GETUNDERINFO) {
			SearchUnderUsersResult result = JSONUtil.fromJson(supportResponse.toString(), SearchUnderUsersResult.class);
			if (result == null || result.getErrorCode() == 999) {
				mEmptyView.showView(R.drawable.emptylist_icon, R.string.return_value_999);
				return;
			}
			if (result.getErrorCode() == 0) {
				mEmptyView.showView(R.drawable.emptylist_icon, result.getErrorMessage());
				return;
			}
			if (result.getErrorCode() == 1) {
				adapter.setmInvitedUsers(result.getUsers());

				if (result.getPangkerCount() < 10) {
					tv_tips.setText("你已经成功邀请" + result.getPangkerCount() + "位用户注册，再邀请"
							+ (10 - result.getPangkerCount()) + "位就可以获得金牌会员身份！");
				} else
					tv_tips.setText("你已经成功邀请" + result.getPangkerCount() + "位用户注册，超过十位，是金牌会员！");
				tv_tips.setVisibility(View.VISIBLE);
				if (result.getUsers() == null || result.getUsers().size() == 0) {
					mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_underusers_prompt);
				}
			}
		}
		if (caseKey == INVITE_TO_PANGKER) {
			InviteRegisterResult inviteResult = JSONUtil.fromJson(supportResponse.toString(),
					InviteRegisterResult.class);
			if (inviteResult != null && inviteResult.errorCode == BaseResult.SUCCESS) {
				List<InviteResult> inviteResultList = inviteResult.getInviteResult();
				for (InviteResult inviteResultItem : inviteResultList) {
					if (inviteResultItem.getCode() == InviteResult.SUCCESS || inviteResultItem.getCode() == 2) {
						sendMsg(inviteResultItem.getMobile(), inviteResultItem.getMessage());
					}
				}
			} else
				showToast(R.string.invite_failed);
		}
	}

	/**
	 * 发送私信
	 * 
	 * @param contacts
	 *            void
	 */
	private void sendMsg(String phoneNum, String smsUrl) {
		if (phoneNum != null && phoneNum.length() > 0) {
			SMSUtil.sendInviteSMS(phoneNum, application.getImAccount(), this);
		}
	}
}
