package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.INetAddressBookUserDao;
import com.wachoo.pangker.db.impl.NetAddressBookUserDaoImpl;
import com.wachoo.pangker.listener.TextCountLimitWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBookResult;
import com.wachoo.pangker.server.response.AddressMembers;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;

/**
 * 单位通讯录成员信息修改
 * @author zhengjy  2012-6-15
 * 
 */
public class UtitsContactUserEditActivity extends CommonPopActivity implements IUICallBackInterface {

	private final static int CONTACT_USER_EDIT = 0x10;

	private PangkerApplication application;
	private Button btnSubmit;
	private EditText etName;
	private EditText etEmail;
	private EditText etAddress;
	private EditText etZipcode;
	private EditText etDepartment;
	private LinearLayout mLayoutMobile1;
	private LinearLayout mLayoutMobile2;
	private LinearLayout mLayoutMobile3;
	private Set<LinearLayout> mobileLists = new HashSet<LinearLayout>();
	private RelativeLayout btnAddMobile;

	private String mUserid;
	private AddressMembers addressMember;
	private INetAddressBookUserDao iNetAddressBookUserDao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.units_contacts_user_edit);
		application = (PangkerApplication) getApplication();
		iNetAddressBookUserDao = new NetAddressBookUserDaoImpl(this);
		mUserid = application.getMyUserID();
		addressMember = (AddressMembers) getIntent().getSerializableExtra(AddressMembers.ADDRESSMEMBER_KEY);

		initView();
	}

	private void initView() {
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText(R.string.units_title_contact_detail);
		btnSubmit = (Button) findViewById(R.id.btn_submit);
		etName = (EditText) findViewById(R.id.et_name);
		etEmail = (EditText) findViewById(R.id.et_email);
		etAddress = (EditText) findViewById(R.id.et_address);
		etZipcode = (EditText) findViewById(R.id.et_zipcode);
		etDepartment = (EditText) findViewById(R.id.et_department);

		mLayoutMobile1 = (LinearLayout) findViewById(R.id.layout_mobile1);
		mLayoutMobile2 = (LinearLayout) findViewById(R.id.layout_mobile2);
		mLayoutMobile3 = (LinearLayout) findViewById(R.id.layout_mobile3);
		btnAddMobile = (RelativeLayout) findViewById(R.id.btn_add_more_info);

		etName.addTextChangedListener(new TextCountLimitWatcher(18, etName));
		etEmail.setFilters(new InputFilter[] { new InputFilter.LengthFilter(100) });
		etAddress.setFilters(new InputFilter[] { new InputFilter.LengthFilter(100) });
		etZipcode.addTextChangedListener(new TextCountLimitWatcher(10, etZipcode));
		etDepartment.setFilters(new InputFilter[] { new InputFilter.LengthFilter(100) });

		etName.setText(addressMember.getRemarkname());
		etEmail.setText(addressMember.getEmail());
		etAddress.setText(addressMember.getAddress());
		etZipcode.setText(addressMember.getZipcode());
		etDepartment.setText(addressMember.getDepartment());

		btnSubmit.setOnClickListener(mClickListener);
		btnAddMobile.setOnClickListener(mClickListener);
		setPhoneClickListener(mLayoutMobile1, addressMember.getPhone1());
		setPhoneClickListener(mLayoutMobile2, addressMember.getPhone2());
		setPhoneClickListener(mLayoutMobile3, addressMember.getPhone3());
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UtitsContactUserEditActivity.this.finish();
			}
		});

	}

	private final OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// TODO Auto-generated method stub
			if (view == btnAddMobile) {
				if (mobileLists.size() >= 3) {
					showToast("最多能添加三个号码!");
					return;
				}
				if (mobileLists.add(mLayoutMobile1)) {
					mLayoutMobile1.setVisibility(View.VISIBLE);
				} else {
					if (mobileLists.add(mLayoutMobile2)) {
						mLayoutMobile2.setVisibility(View.VISIBLE);
					} else if (mobileLists.add(mLayoutMobile3)) {
						mLayoutMobile3.setVisibility(View.VISIBLE);
					}
				}
			} else if (view == btnSubmit) {
				AddressBookMemberEdit();
			}
		}
	};

	private void setPhoneClickListener(final LinearLayout view, String phoneNum) {
		ImageView btnClear = (ImageView) view.findViewById(R.id.iv_item_clear);
		final EditText etMobileAdd = (EditText) view.findViewById(R.id.et_mobile_add);
		if (!Util.isEmpty(phoneNum)) {
			view.setVisibility(View.VISIBLE);
			mobileLists.add(view);
			etMobileAdd.setText(phoneNum);
		}
		btnClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				view.setVisibility(View.GONE);
				etMobileAdd.setText("");
				mobileLists.remove(view);
			}
		});

	}

	/**
	 * 单位通讯录成员信息修改
	 * 
	 * 
	 * 接口：AddressBookModify.json
	 */
	private void AddressBookMemberEdit() {
		String name = etName.getText().toString();
		if (Util.isEmpty(name)) {
			showToast(R.string.units_name_cannotnull);
			return;
		}
		addressMember.setRemarkname(name);
		String phone1 = addressMember.getPhone1();
		String phone2 = addressMember.getPhone2();
		String phone3 = addressMember.getPhone3();
		if (mobileLists.contains(mLayoutMobile1) || mobileLists.contains(mLayoutMobile2)
				|| mobileLists.contains(mLayoutMobile3)) {
			final Iterator<LinearLayout> iter = mobileLists.iterator();
			int i = 0;
			while (iter.hasNext()) {
				LinearLayout item = (LinearLayout) iter.next();
				EditText etPhone = (EditText) item.findViewById(R.id.et_mobile_add);
				if (i == 0) {
					phone1 = etPhone.getText().toString();
					addressMember.setPhone1(phone1);
				}
				if (i == 1) {
					phone2 = etPhone.getText().toString();
					addressMember.setPhone2(phone2);
				}
				if (i == 2) {
					phone3 = etPhone.getText().toString();
					addressMember.setPhone3(phone3);
				}
				i++;
			}
			// 如果已经被移除某个号码，则相应号码设置为空
			if (mobileLists.size() == 0) {
				addressMember.setPhone1("");
				addressMember.setPhone2("");
				addressMember.setPhone3("");
			} else if (mobileLists.size() == 1) {
				addressMember.setPhone2("");
				addressMember.setPhone3("");
			} else if (mobileLists.size() == 2) {
				addressMember.setPhone3("");
			}

		}
		String email = etEmail.getText().toString();
		addressMember.setEmail(email);
		String address = etAddress.getText().toString();
		addressMember.setAddress(address);
		String zipcode = etZipcode.getText().toString();
		addressMember.setZipcode(zipcode);
		String department = etDepartment.getText().toString();
		addressMember.setDepartment(department);
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", mUserid));// 操作者Uid.
		paras.add(new Parameter("rid", addressMember.getId().toString()));// 关系ID标识。
		paras.add(new Parameter("mobile", addressMember.getMobile()));// 用户备注名称
		paras.add(new Parameter("remarkname", name));// 用户备注名称
		paras.add(new Parameter("phone1", phone1 != null ? phone1 : ""));//
		paras.add(new Parameter("phone2", phone2 != null ? phone2 : ""));
		paras.add(new Parameter("phone3", phone3 != null ? phone3 : ""));//
		paras.add(new Parameter("email", email));//
		paras.add(new Parameter("address", address));
		paras.add(new Parameter("zipcode", zipcode));// 邮编
		paras.add(new Parameter("department", department));// 部门名称
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getAddressBookModify(), paras, true, mess, CONTACT_USER_EDIT);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		if (!HttpResponseStatus(supportResponse))
			return;
		if (caseKey == CONTACT_USER_EDIT) {
			AddressBookResult baseResult = JSONUtil.fromJson(supportResponse.toString(), AddressBookResult.class);
			if (null == baseResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (baseResult.getErrorCode() == BaseResult.SUCCESS) {
				showToast(baseResult.getErrorMessage());
				iNetAddressBookUserDao.updateAddressMember(addressMember);
				Intent intent = getIntent();
				intent.putExtra(AddressMembers.ADDRESSMEMBER_KEY, addressMember);
				setResult(RESULT_OK, intent);
				this.finish();
			} else if (baseResult.getErrorCode() == BaseResult.FAILED) {
				showToast(baseResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}

	}

}
