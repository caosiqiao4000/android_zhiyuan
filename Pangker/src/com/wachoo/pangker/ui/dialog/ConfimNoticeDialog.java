package com.wachoo.pangker.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * 系统通知对话框
 * 
 * @author wangxin
 * 
 */
public class ConfimNoticeDialog extends Dialog {

	private TextView mDialogTitle;// >>>>>>对话框标题
	private TextView mTitle;// >>>>>子标题
	private TextView mContent;// >>>>内容
	private Button mBtnConfim;
	private Button mBtnCancel;
	private CheckBox mCheckBox;
	private RelativeLayout mRelativeLayout;
	private SharedPreferencesUtil apu;

	public ConfimNoticeDialog(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pangker_system_dialog);
		// 系统中关机对话框就是这个属性
		Window window = getWindow();
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.type = WindowManager.LayoutParams.TYPE_APPLICATION_ATTACHED_DIALOG;
		window.setAttributes(lp);
		// window.addFlags(WindowManager.LayoutParams.FLAGS_CHANGED);
		window.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		mDialogTitle = (TextView) findViewById(R.id.tv_dialog_title);
		mTitle = (TextView) findViewById(R.id.tv_notice_title);
		mContent = (TextView) findViewById(R.id.tv_notice_content);
		mContent.setVisibility(View.GONE);
		mBtnConfim = (Button) findViewById(R.id.btn_dialog_confirm);
		mBtnCancel = (Button) findViewById(R.id.btn_dialog_cancel);
		mBtnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});

		// >>>>>>>>>>记住选择checkbox
		mRelativeLayout = (RelativeLayout) findViewById(R.id.ry_check_notice);
		mRelativeLayout.setVisibility(View.GONE);
		mCheckBox = (CheckBox) findViewById(R.id.ck_checkbox);
	}

	@Override
	public void setCanceledOnTouchOutside(boolean cancel) {
		// TODO Auto-generated method stub
		super.setCanceledOnTouchOutside(cancel);
	}

	public void setTitle(String title) {
		if(!Util.isEmpty(title)){
			this.mTitle.setVisibility(View.VISIBLE);
			this.mTitle.setText(title);
		}
		else this.mTitle.setVisibility(View.GONE);
	}

	public void setTitleVisibility(int v) {
		this.mTitle.setVisibility(v);
	}

	public void setTitle(int id) {
		if(id > 0){
			this.mTitle.setVisibility(View.VISIBLE);
			this.mTitle.setText(id);
		}
		else this.mTitle.setVisibility(View.GONE);
	}

	public void setContent(String title) {
		mContent.setVisibility(View.VISIBLE);
		this.mContent.setText(title);
	}

	public void setContent(int id) {
		if(id > 0){
			mContent.setVisibility(View.VISIBLE);
			this.mContent.setText(id);
		}
	}

	public void setDialogTitle(int id) {
		this.mDialogTitle.setText(id);
	}

	public void setDialogTitle(String mDialogTitle) {
		this.mDialogTitle.setText(mDialogTitle);
	}

	public void setConfimOnClickListener(View.OnClickListener clickListener) {
		this.mBtnConfim.setVisibility(View.VISIBLE);
		this.mBtnConfim.setOnClickListener(clickListener);
	}

	public void setCancelOnClickListener(View.OnClickListener clickListener) {
		this.mBtnCancel.setVisibility(View.VISIBLE);
		this.mBtnCancel.setOnClickListener(clickListener);
	}
	
	public void setConfimOnClickListener(String postiveTitle, View.OnClickListener clickListener) {
		this.mBtnConfim.setVisibility(View.VISIBLE);
		this.mBtnConfim.setText(postiveTitle);
		this.mBtnConfim.setOnClickListener(clickListener);
	}

	public void setCancelOnClickListener(String navgetiveTitle, View.OnClickListener clickListener) {
		this.mBtnCancel.setVisibility(View.VISIBLE);
		this.mBtnCancel.setText(navgetiveTitle);
		this.mBtnCancel.setOnClickListener(clickListener);
	}

	public void RememberSelected(CompoundButton.OnCheckedChangeListener checkedChangeListener) {
		this.mRelativeLayout.setVisibility(View.VISIBLE);
		this.mCheckBox.setOnCheckedChangeListener(checkedChangeListener);
	}
	
	public void RememberSelected() {
		// TODO Auto-generated method stub
		this.mRelativeLayout.setVisibility(View.VISIBLE);
	}
	
	public boolean isRememberSelected(){
		return mRelativeLayout.getVisibility() == View.VISIBLE;
	}
	
	public boolean isSelected(){
		return mCheckBox.isChecked();
	}

	public void setRememberText(String rememberText) {
		this.mCheckBox.setText(rememberText);
	}

	public void setRememberText(int id) {
		this.mCheckBox.setText(id);
	}
	
	protected void showToast(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(getContext(), string, Toast.LENGTH_SHORT).show();
	}
	
	protected void showToast(int stringId) {
		// TODO Auto-generated method stub
		Toast.makeText(getContext(), stringId, Toast.LENGTH_SHORT).show();
	}
	
	public void setTextSize(int titleSize,int contentSize){
    	mTitle.setTextSize(titleSize);
    	mContent.setTextSize(contentSize);
	}
	public void setButtonName(int mBtnConfimName,int mBtnCancelName){
		mBtnConfim.setText(mBtnConfimName);
		mBtnCancel.setText(mBtnCancelName);
	}
	
	public void setButtonName(String mBtnConfimName,String mBtnCancelName){
		mBtnConfim.setText(mBtnConfimName);
		mBtnCancel.setText(mBtnCancelName);
	}
	
	protected boolean getSpBoolean(String spIndex) {
		// TODO Auto-generated method stub
		if(apu == null){
        	apu = new SharedPreferencesUtil(getContext());
        }
		return apu.getBoolean(spIndex, false);
	}
	
	protected void saveSPBoolean(String spIndex, boolean flag) {
		// TODO Auto-generated method stub
        if(apu == null){
        	apu = new SharedPreferencesUtil(getContext());
        }
        apu.saveBoolean(spIndex, flag);
	}
	
}
