package com.wachoo.pangker.ui.dialog;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.view.View;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.msg.MsgInfoActivity;
import com.wachoo.pangker.activity.res.PictureInfoActivity;
import com.wachoo.pangker.activity.res.ResInfoActivity;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.util.Util;

/**
 * 系统通知对话框
 * 
 * @author wangxin
 * 
 */
public class NoticeDialog extends ConfimNoticeDialog {

	private Context mContext;
	private RecommendInfo recommendInfo;

	public NoticeDialog(Context context, RecommendInfo info, int i) {
		super(context);
		this.mContext = context;
		this.recommendInfo = info;
		// >>>>>>资源推荐
		setDialogTitle(mContext.getString(R.string.res_recommend_dialogtitle));

		// >>>>>>>资源名称：XXXX
		String title = mContext.getString(R.string.res_recommend_resname) + info.getResName();
		setTitle(title);

		String content = info.getFromName() + mContext.getString(R.string.res_recommend_from);

		// >>>>>>推荐内容: xxxx向您推荐资源；TA说：xxxxxxxx
		String recommendReason = info.getReason();
		if (Util.isEmpty(recommendReason)) {
			recommendReason = mContext.getResources().getString(R.string.res_recommend_noreason);
		}
		recommendReason = mContext.getString(R.string.res_recommend_he_say) + recommendReason;

		content = content + ";" + recommendReason;
		setContent(content);
		setConfimOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				readRes(recommendInfo.getResId());
				lookResInfo(recommendInfo);
				dismiss();
			}
		});
	}

	private void lookResInfo(RecommendInfo info) {
		Intent intent = new Intent();
		PangkerApplication application = (PangkerApplication) mContext.getApplicationContext();
		if (info.getType() == PangkerConstant.RES_MUSIC) {
			List<MusicInfo> musicInfos = new ArrayList<MusicInfo>();// 播放列表
			MusicInfo musicInfo = new MusicInfo();
			musicInfo.setSid(Util.String2Long(info.getResId()));
			musicInfo.setMusicName(info.getResName());
			musicInfo.setSinger(info.getSinger());
			musicInfos.add(musicInfo);
			application.setMusiList(musicInfos);
			application.setPlayMusicIndex(0);

			Intent intentSerivce = new Intent(mContext, MusicService.class);
			intentSerivce.putExtra("control", PlayControl.MUSIC_START_PLAY);
			mContext.startService(intentSerivce);
			intent.setClass(mContext, MusicPlayActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		} else {
			if (info.getType() == PangkerConstant.RES_PICTURE) {
				intent.setClass(mContext, PictureInfoActivity.class);
			} else {
				intent.setClass(mContext, ResInfoActivity.class);
			}
		}
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		ResShowEntity entity = new ResShowEntity();
		entity.setCommentTimes(0);
		entity.setResType(info.getType());
		entity.setDownloadTimes(0);
		entity.setFavorTimes(0);
		entity.setResID(Long.parseLong(info.getResId()));
		entity.setScore(0);
		entity.setResName(info.getResName());
		entity.setShareUid(info.getFromId());
		reList.add(entity);

		application.setResLists(reList);
		application.setViewIndex(0);
		PangkerManager.getActivityStackManager().currentMainActivity().startActivity(intent);
		// 同时通知MsginfoActivity界面改变uncount数量
		refreshMsgInfoActivityById();
	}

	private void refreshMsgInfoActivityById() {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(recommendInfo.getFromId())) {
			Message msg = new Message();
			msg.what = 2;
			msg.arg1 = MsgInfo.MSG_RECOMMEND;
			msg.obj = recommendInfo.getFromId();
			PangkerManager.getTabBroadcastManager().sendResultMessage(MsgInfoActivity.class.getName(), msg);
		}
	}

	/**
	 * 删除资源或者标识资源为已读
	 */
	private void readRes(String resId) {
		ServerSupportManager serverMana = new ServerSupportManager(mContext, new IUICallBackInterface() {

			@Override
			public void uiCallBack(Object supportResponse, int caseKey) {
				// TODO Auto-generated method stub
				System.err.println();
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		String userId = ((PangkerApplication) mContext.getApplicationContext()).getMyUserID();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", resId));
		paras.add(new Parameter("optype", "2"));
		serverMana.supportRequest(Configuration.getDeleteBoxRes(), paras, false, "");
	}
}
