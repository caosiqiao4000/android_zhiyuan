package com.wachoo.pangker.ui.dialog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.entity.NoticeInfo;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CallMeSetResult;
import com.wachoo.pangker.server.response.GroupEnterCheckResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.util.Util;

/**
 * 群组内，进行音乐播放的Dialog
 * 
 * @author zxx
 */
public class ConfrimDialog extends ConfimNoticeDialog {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private Activity activity;

	private PangkerApplication application;

	public ConfrimDialog(Activity context) {
		super(context);
		this.activity = context;
		application = (PangkerApplication) context.getApplication();

	}

	public ConfrimDialog(Activity context, int attrs) {
		super(context);
		this.activity = context;
		application = (PangkerApplication) context.getApplication();
	}

	/****************************************************************************************************/
	public void showAddFriendDialog(final String userid) {
		// TODO Auto-generated method stub
		setDialogTitle("添加好友申请");
		if (!getSpBoolean(PangkerConstant.SP_FRIEND_ADD + application.getMyUserID())) {
			setContent("添加对方为好友后，TA将可能有权限给您打电话，并可访问您的网盘资源。");
			RememberSelected();
		}
		setContent("是否同意添加对方为好友？");

		setConfimOnClickListener("同意", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dealCheckBox();
				if (application.IsFriend(userid)) {
					dismiss();
					return;
				}
				try {
					final OpenfireManager openfireManager = application.getOpenfireManager();
					if (openfireManager.isLoggedIn()) {
						openfireManager.sendPacketAdded(userid);
						openfireManager.sendPacketAdd(userid);
					} else {
						Toast.makeText(activity, PangkerConstant.MSG_NO_ONLINE, Toast.LENGTH_SHORT).show();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch b lock
					logger.error(TAG, e);
				} finally {
					dismiss();
				}
			}
		});

		setCancelOnClickListener("拒绝", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dealCheckBox();
				if (application.IsFriend(userid)) {
					dismiss();
					return;
				}
				final OpenfireManager openfireManager = application.getOpenfireManager();
				if (openfireManager.isLoggedIn()) {
					openfireManager.refuseFriendRequest(userid);
					Log.d(TAG, "Del Friend::" + userid + " iOpenFireAccess.removeEntry,iOpenFireAccess.sendPacketDel");
				} else {
					Toast.makeText(activity, PangkerConstant.MSG_NO_ONLINE, Toast.LENGTH_SHORT).show();
				}
				dismiss();
			}
		});
		show();
	}

	private void dealCheckBox() {
		// TODO Auto-generated method stub
		if (isRememberSelected()) {
			saveSPBoolean(PangkerConstant.SP_FRIEND_ADD + application.getMyUserID(), isSelected());
		}
	}

	/****************************************************************************************************/
	/**
	 * 进群记录
	 */
	private void groupEnterCheck(String groupId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", application.getMyUserID()));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("optype", "2"));//
		// 1：申请进入群组（必填）；2:被邀请加入；3：直接进入（自己群组，收藏的群组等等 ）
		paras.add(new Parameter("password", ""));// 群组密码，需要密码校验时使用
		ServerSupportManager serverMana = new ServerSupportManager(activity, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				GroupEnterCheckResult result = JSONUtil.fromJson(response.toString(), GroupEnterCheckResult.class);
				if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
					UserGroup enterGroup = result.getUserGroup();
					if (enterGroup != null) {
						enterGroup.setLastVisitTime(Util.date2Str(new Date()));
						gotoRoom(enterGroup);
					} else {
						showToast("查询失败,还无法登录该群组!");
					}
				} else {
					showToast(R.string.return_value_999);
				}
			}
		});
		serverMana.supportRequest(Configuration.getGroupEnterCheck(), paras, true, "正在进入群组...", 0);
	}

	private void checkGroup(final String groupId) {
		if (application.getCurrunGroup() != null // 3：就在当前的群组中
				&& application.getCurrunGroup().getUid() != null
				&& application.getCurrunGroup().getGroupId().equals(groupId)) {
			gotoRoom(null);
		} else if (application.getCurrunGroup() != null // 2：在一个群组中，但不在点击的群组
				&& application.getCurrunGroup().getUid() != null
				&& !application.getCurrunGroup().getGroupId().equals(groupId)) {

			MessageTipDialog mDialog = new MessageTipDialog(new DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-genaerated method stub
					if (isSubmit) {
						// 首先要关闭之前的群组界面
						PangkerManager.getActivityStackManager().popOneActivity(ChatRoomMainActivity.class);
						groupEnterCheck(groupId);
					}
				}
			}, activity);
			mDialog.showDialog("您已经在一个群组里面,进入此群组会退出当前所在的群组,是否确认进入?", false);
		} else {// 1:自己不在任何一个群组中
			groupEnterCheck(groupId);
		}
	}

	/**
	 * userGroup==null :表示当前用户在群组中，直接返回
	 * 
	 * @param userGroup
	 */
	public void gotoRoom(UserGroup userGroup) {
		// 启动service
		Intent intent = new Intent();
		// >>>>>wangxin add 进入群组
		if (userGroup != null) {
			intent.putExtra(ChatRoomMainActivity.JION_TO_GROUP, userGroup);
			// 将进入群组的消息纪律到MsgInfo中
			application.getMsgInfoBroadcast().sendMeetroom(userGroup);
		} else {
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			application.getMsgInfoBroadcast().sendMeetroom(application.getCurrunGroup());
		}
		intent.setClass(activity, ChatRoomMainActivity.class);
		activity.startActivity(intent);
	}

	public void showIntoGroupDialog(final NoticeInfo noticeInfo, final OnDialogClickListener listener) {
		// TODO Auto-generated method stub
		final String[] remarks = noticeInfo.getRemark().split("#");
		setDialogTitle("群组邀请");
		String label = "用户：" + noticeInfo.getOpeUsername() + "邀请您加入Ta的群组--" + remarks[1] + "?";
		setTitleVisibility(View.GONE);
		setContent(label);

		setConfimOnClickListener("同意", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
				final OpenfireManager openfireManager = application.getOpenfireManager();
				if (openfireManager.isLoggedIn()) {
					if (!application.getOpenfireManager().isLoggedIn()) {
						return;
					}
					openfireManager.sendGroupResponse(application.getMyUserID(), noticeInfo.getOpeUserid(), "1",
							"同意您的邀请！");
					checkGroup(remarks[0]);
				} else {
					Toast.makeText(activity, PangkerConstant.MSG_NO_ONLINE, Toast.LENGTH_SHORT).show();
				}
			}
		});
		setCancelOnClickListener("稍后处理", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if (listener != null) {
					listener.onClick(ConfrimDialog.this, false);
				}
				// final OpenfireManager openfireManager =
				// application.getOpenfireManager();
				// if (openfireManager.isLoggedIn()) {
				// openfireManager.sendGroupResponse(userId,
				// noticeInfo.getOpeUserid(), "0", "拒绝您的邀请！");
				// } else {
				// Toast.makeText(activity, PangkerMessage.MSG_NO_ONLINE,
				// Toast.LENGTH_SHORT).show();
				// }
			}
		});
		show();
	}

	/****************************************************************************************************/
	public void showLoginDialog(String noticeMsg, final OnDialogClickListener onClickListener) {
		// TODO Auto-generated method stub
		setDialogTitle("提示消息");
		setTitle("系统消息");
		setContent(noticeMsg);

		setConfimOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (onClickListener != null) {
					onClickListener.onClick(ConfrimDialog.this, true);
				}
				if (activity != null) {
					activity.finish();
				}
				dismiss();
			}
		});

		show();
	}

	/****************************************************************************************************/
	public void showWifiDialog(String noticeMsg, final OnDialogClickListener onClickListener) {
		// TODO Auto-generated method stub
		setDialogTitle("温馨提示");
		setTitle("WIFI邻居开关");
		setContent(noticeMsg);

		setCancelOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (onClickListener != null) {
					onClickListener.onClick(ConfrimDialog.this, false);
				}
				dismiss();
			}
		});

		setConfimOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (onClickListener != null) {
					onClickListener.onClick(ConfrimDialog.this, true);
				}
				dismiss();
			}
		});

		show();
	}

	/****************************************************************************************************/
	public void showCallDialog(final String userid, String userName) {
		setDialogTitle("温馨提示");
		setTitle("确认拨打用户：" + userName + "?");

		setCancelOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});

		setConfimOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callCheck(userid);
				dismiss();
			}
		});
		show();
	}

	/**
	 * 呼叫校验 void
	 */
	private void callCheck(String friendId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", friendId));
		paras.add(new Parameter("visituid", application.getMyUserID()));
		paras.add(new Parameter("password", ""));
		String mess = "权限校验中...";
		ServerSupportManager serverMana = new ServerSupportManager(activity, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				CallMeSetResult result = JSONUtil.fromJson(response.toString(), CallMeSetResult.class);
				if (result != null && result.getErrorCode() == BaseResult.SUCCESS && result.getMobile() != null) {
					// 语音呼叫,记录到MsgInfo信息里面
					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel://" + result.getMobile()));
					activity.startActivity(intent);
				} else if (result != null && (result.getErrorCode() == BaseResult.FAILED || result.getErrorCode() == 2)) {
					showToast(result.getErrorMessage());
				} else
					showToast(R.string.return_value_999);
			}
		});
		serverMana.supportRequest(Configuration.getCallCheck(), paras, true, mess, 0);
	}

	/****************************************************************************************************/
	public void showConfrimDialog(String title, String noticeMsg, final OnDialogClickListener onClickListener) {
		// TODO Auto-generated method stub
		setDialogTitle(title);
		setContent(noticeMsg);

		setConfimOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (onClickListener != null) {
					onClickListener.onClick(ConfrimDialog.this, true);
				}
				dismiss();
			}
		});
		setCancelOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});
		show();
	}

	/****************************************************************************************************/
	public interface OnDialogClickListener {
		public void onClick(Dialog dialog, boolean positive);
	}

}
