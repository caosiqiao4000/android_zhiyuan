package com.wachoo.pangker.ui;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.wachoo.pangker.entity.LrcSentence;
import com.wachoo.pangker.util.ImageUtil;

public class LrcView extends TextView {

	private final String LRC_TITLE = "正在加载歌词......";

	private float high;
	private float width;
	private Paint CurrentPaint;
	private Paint NotCurrentPaint;
	private float TextHigh = 25;
	private float TextSize = 16;
	private int Index = 0;
	private int delay = 0;

	private List<LrcSentence> mSentenceEntities;
	private String msgtitle = LRC_TITLE;
	private Context context;

	public void setSentenceEntities(List<LrcSentence> mSentenceEntities) {
		this.mSentenceEntities = mSentenceEntities;
	}

	public void setMsgtitle(String msgtitle) {
		this.msgtitle = msgtitle;
	}

	public void initMsgtitle() {
		this.msgtitle = LRC_TITLE;
	}

	public LrcView(Context context) {
		super(context);
		this.context = context;
		init();
		// TODO Auto-generated constructor stub
	}

	public LrcView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
		// TODO Auto-generated constructor stub
	}

	public LrcView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
		// TODO Auto-generated constructor stub
	}

	public void delayTime(boolean ifDelay) {
		if (ifDelay) {
			delay++;
		} else {
			delay--;
		}
	}

	public void reduction() {
		delay = 0;
	}

	private void init() {
		setFocusable(true);
		float density = ImageUtil.getDensity((Activity)getContext());
		if (density > 0.75f && density <= 1.0f) {
			TextSize = 18;
			TextHigh += 5;
		} else if (density > 1.0f && density <= 1.5f) {
			TextSize = 24;
			TextHigh += 10;
		} else {
			TextSize = 36;
			TextHigh += 18;
		}
		Log.d("TextSize", "density:" + density + "--TextSize:" + TextSize);
		// 高亮部分
		CurrentPaint = new Paint();
		CurrentPaint.setAntiAlias(true);
		CurrentPaint.setColor(Color.parseColor("#4B0082"));
		CurrentPaint.setTextAlign(Paint.Align.CENTER);
		CurrentPaint.setTextSize(TextSize);
		CurrentPaint.setTypeface(Typeface.SERIF);
		// 非高亮部分
		NotCurrentPaint = new Paint();
		NotCurrentPaint.setAntiAlias(true);
		NotCurrentPaint.setColor(Color.GRAY);
		NotCurrentPaint.setTextAlign(Paint.Align.CENTER);
		NotCurrentPaint.setTextSize(TextSize);
		NotCurrentPaint.setTypeface(Typeface.SERIF);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);

		if (canvas == null || mSentenceEntities == null || Index < 0) {
			canvas.drawText(msgtitle, width / 2, high / 2, CurrentPaint);
			return;
		}
		if (mSentenceEntities.size() <= Index) {
			return;
		}
		canvas.drawText(mSentenceEntities.get(Index).getContent(), width / 2, high / 2, CurrentPaint);

		float tempY = high / 2;
		// 画出本句之前的句子
		for (int i = Index - 1; i >= 0; i--) {
			// 向上推移
			tempY = tempY - TextHigh;
			if (tempY <= 20) {
				break;
			}
			canvas.drawText(mSentenceEntities.get(i).getContent(), width / 2, tempY, NotCurrentPaint);
		}
		tempY = high / 2;
		// 画出本句之后的句子
		for (int i = Index + 1; i < mSentenceEntities.size(); i++) {
			// 往下推移
			tempY = tempY + TextHigh;
			if (tempY >= high) {
				break;
			}
			canvas.drawText(mSentenceEntities.get(i).getContent(), width / 2, tempY, NotCurrentPaint);
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		this.high = h;
		this.width = w;
	}

	public void SetIndex(int index) {
		this.Index = index;
		// System.out.println(index);
	}

	private int getIndex(long currentTime, long countTime) {
		int index = -1;
		if (mSentenceEntities == null) {
			return -1;
		}
		if (currentTime < countTime) {
			for (int i = 0; i < mSentenceEntities.size(); i++) {
				if (i < mSentenceEntities.size() - 1) {
					if (currentTime < mSentenceEntities.get(i).getLyricTime() && i == 0) {
						index = i;
					}

					if (currentTime > mSentenceEntities.get(i).getLyricTime()
							&& currentTime < mSentenceEntities.get(i + 1).getLyricTime()) {
						index = i;
					}
				}

				if (i == mSentenceEntities.size() - 1
						&& currentTime > mSentenceEntities.get(i).getLyricTime()) {
					index = i;
				}
			}
		}
		return index;
	}

	public void refleshIndex(long currentTime, long countTime) {
		Log.d("Lmc", "currentTime:" + currentTime + "countTime" + countTime);
		currentTime += delay * 500;
		SetIndex(getIndex(currentTime, countTime));
	}

}
