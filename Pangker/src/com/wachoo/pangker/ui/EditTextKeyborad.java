package com.wachoo.pangker.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;

public class EditTextKeyborad extends EditText {
	private IKeyboardListener keyboardListener;

	public EditTextKeyborad(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public EditTextKeyborad(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public EditTextKeyborad(Context context) {
		super(context);
	}

	public IKeyboardListener getKeyboardListener() {
		return keyboardListener;
	}

	public void setKeyboardListener(IKeyboardListener keyboardListener) {
		this.keyboardListener = keyboardListener;
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_ENTER) {

			Log.v("xpf", "--------------------+onKeyDown---------软键盘弹出");
		}
		return super.onKeyDown(keyCode, event);
	}

	public boolean onKeyPreIme(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {

			Log.v("xpf", "--------------------+onKeyPreIme---------软键盘退出");
			if (keyboardListener != null) {
				keyboardListener.hide();
			}
		}

		return super.onKeyPreIme(keyCode, event);
	}

	public interface IKeyboardListener {
		void hide();
	}
}
