package com.wachoo.pangker.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.ui.FooterView.OnLoadMoreListener;

public class ListLinearLayout extends LinearLayout {
	private BaseAdapter adapter;
	private OnClickListener onClickListener = null;
	private OnTouchListener onTouchListener = null;
	private OnLongClickListener onLongClickListener = null;
	private FooterView mFooterView;
	private boolean isShowLoadmore = false;
	private OnLoadMoreListener onLoadMoreListener = null;
	private View mLoadMoreView;
	private int count = 0;

	public int getCount() {
		return count;
	}

	public boolean isShowLoadmore() {
		return isShowLoadmore;
	}

	public void setShowLoadmore(boolean isShowLoadmore) {
		this.isShowLoadmore = isShowLoadmore;
	}

	public OnLoadMoreListener getOnLoadMoreListener() {
		return onLoadMoreListener;
	}

	public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
		this.onLoadMoreListener = onLoadMoreListener;
	}

	public ListLinearLayout(Context context) {
		super(context);
		mFooterView = new FooterView(context);
		// TODO Auto-generated constructor stub
	}

	public ListLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mFooterView = new FooterView(context);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 绑定布局
	 */
	public void bindLinearLayout() {
		removeAllViews();
		if (adapter == null) {
			return;
		}

		for (int i = 0; i < count; i++) {
			View view = adapter.getView(i, null, null);
			view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT));
			view.setTag(adapter.getItem(i));
			view.setOnTouchListener(onTouchListener);
			view.setOnClickListener(onClickListener);
			view.setOnLongClickListener(onLongClickListener);
			view.setId(i);
			addView(view, i);
		}
		if (isShowLoadmore && onLoadMoreListener != null) {
			showLoadmore();
		}

	}

	public void bindLinearLayout(BaseAdapter adapter) {
		if (mLoadMoreView != null) {
			removeView(mLoadMoreView);
		}
		for (int i = 0; i < adapter.getCount(); i++) {
			View view = adapter.getView(i, null, null);
			view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT));
			view.setOnTouchListener(this.onTouchListener);
			view.setOnClickListener(this.onClickListener);
			view.setOnLongClickListener(onLongClickListener);
			view.setId(i + count);
			addView(view, i + count);
		}

		this.count = this.count + adapter.getCount();
		
		if (isShowLoadmore && onLoadMoreListener != null) {
			showLoadmore();
		}
	}

	private void showLoadmore() {
		if (mLoadMoreView == null) {
			mLoadMoreView = mFooterView.getLoadmoreView();
			mLoadMoreView.setLayoutParams(new LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			mFooterView.setPresdBarVisiable(false);
			mFooterView.setOnLoadMoreListener(onLoadMoreListener);
			mLoadMoreView.setBackgroundColor(R.color.white);
		}
		mFooterView.onLoadComplete();

		addView(mLoadMoreView, count);
	}

	public void clear() {
		removeAllViews();
	}

	public void addAdapter(BaseAdapter adapter) {
		bindLinearLayout(adapter);

	}

	public void setAdapter(BaseAdapter adapter) {
		this.adapter = adapter;
		this.count = adapter.getCount();
		bindLinearLayout();
	}

	public void setOnClickListener(OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}

	public void setOnTouchListener(OnTouchListener onTouchListener) {
		this.onTouchListener = onTouchListener;
	}

	public void setOnLongClickListener(OnLongClickListener onLongClickListener) {
		this.onLongClickListener = onLongClickListener;
	}

}
