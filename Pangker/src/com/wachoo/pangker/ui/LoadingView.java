package com.wachoo.pangker.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.MainActivity;
import com.wachoo.pangker.activity.register.RegisterSendMsgActivity;

public class LoadingView extends ImageView implements Runnable, IUserInfomationCallBack {
	private int loading = 0;
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	public static final int RUN = 0;

	public static final int LOAD_USER_SUCESS = 1;
	public static final int LOAD_USER_FAIL = 2;
	public static final int LOAD_USER_NOUSER = 3;
	public static final int NO_NETWORK = 4;
	public static final int LOAD_CANCEL_USER = 5;// 注销用户
	public static final int NETWORK_STABLE = 6;// 校验异常，有可能：服务器异常、网络连接超时，连接异常
	private int[] imageIds;
	private int index = 0;
	private int length = 1;
	private Context context;

	public LoadingView(Context context) {
		this(context, null);
		this.context = context;
	}

	public LoadingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	public void setImageIds(int[] imageId) {
		this.imageIds = imageId;
		if (imageIds != null && imageIds.length > 0) {
			length = imageIds.length;
		}
	}

	@Override
	protected void onDetachedFromWindow() {
		// TODO Auto-generated method stub
		super.onDetachedFromWindow();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		if (imageIds != null && imageIds.length > 0) {
			this.setImageResource(imageIds[index]);
		}
	}

	@Override
	public void run() {

		while (loading == RUN) {
			index = ++index % length;
			postInvalidate();
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				logger.error(TAG, e);
			}
		}
		Intent intent = null;
		switch (loading) {
		case LOAD_USER_SUCESS:
			// >>>>>>>>>进入旁客
			intent = new Intent(context, MainActivity.class);
			context.startActivity(intent);
			((PangkerApplication) context.getApplicationContext()).isLocalContactsInit();
			((Activity) context).finish();
			break;
		case LOAD_USER_NOUSER:
		case LOAD_USER_FAIL:
			intent = new Intent(context, RegisterSendMsgActivity.class);
			context.startActivity(intent);
			((Activity) context).finish();
			break;
		default:
			break;
		}
	}

	private Thread loadingThread = new Thread(this);

	public void startAnim() {
		loadingThread.start();
	}

	@Override
	public void loadingResult(int result) {
		// TODO Auto-generated method stub
		this.loading = result;
	}

	public int getLoading() {
		return this.loading;
	}

}
