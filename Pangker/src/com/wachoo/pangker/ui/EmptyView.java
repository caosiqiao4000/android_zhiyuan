package com.wachoo.pangker.ui;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;

public class EmptyView {

	private View view;
	private ImageView empty_icon;
	private TextView empty_prompt;
	private Button btnOne;
	private Button btnTwo;
	
	private Button btnMenu;
	private Button btnSearch;
	private EditText txtInput;
	private boolean isshow = false;
	private OnSearchListener onSearchListener;
	
	
	public EmptyView(Context context) {
		// TODO Auto-generated constructor stub
		this(context , false);
	}
	
	public String getSearch() {
		return txtInput.getText().toString().trim();
	}
	
	public EmptyView(Context context, boolean ifBar) {
		// TODO Auto-generated constructor stub
		if(ifBar){
			view = LayoutInflater.from(context).inflate(R.layout.managerbar_empty_view, null, false);
			txtInput = (EditText) view.findViewById(R.id.et_search);
			btnMenu = (Button) view.findViewById(R.id.btn_left);
			btnSearch = (Button) view.findViewById(R.id.btn_right);
			txtInput.addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// TODO Auto-generated method stub
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					if (s != null && !s.toString().trim().equals("")) {
						btnSearch.setVisibility(View.VISIBLE);
					} else {
						btnSearch.setVisibility(View.GONE);
					}
				}
			});
			btnSearch.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(onSearchListener != null){
						onSearchListener.onSearch(txtInput.getText().toString().trim());
					}
				}
			});
			btnSearch.setBackgroundResource(R.drawable.btn_search_selector);
		} else {
			view = LayoutInflater.from(context).inflate(R.layout.empty_view, null, false);
			btnOne = (Button) view.findViewById(R.id.btn_submit1);
			btnTwo = (Button) view.findViewById(R.id.btn_submit2);
		}
		empty_icon = (ImageView) view.findViewById(R.id.iv_empty_icon);
		empty_prompt = (TextView) view.findViewById(R.id.textViewEmpty);
		
		empty_icon.setImageBitmap(null);
		empty_prompt.setText("");
	}
	
	public void setBtnOne(String title, View.OnClickListener onClickListener){
		if(btnOne != null){
			btnOne.setVisibility(View.VISIBLE);
			btnOne.setText(title);
			btnOne.setOnClickListener(onClickListener);
		}
	}
	
	public void setBtnTwo(String title, View.OnClickListener onClickListener){
		if(btnTwo != null){
			btnTwo.setVisibility(View.VISIBLE);
			btnTwo.setText(title);
			btnTwo.setOnClickListener(onClickListener);
		}
	}
	
	public OnSearchListener getOnSearchListener() {
		return onSearchListener;
	}

	public void setOnSearchListener(OnSearchListener onSearchListener) {
		this.onSearchListener = onSearchListener;
	}

	public Button getBtnMenu() {
		return btnMenu;
	}

	public Button getBtnSearch() {
		return btnSearch;
	}

	public EditText getTxtInput() {
		return txtInput;
	}

	public View getView() {
		return view;
	}
	
	public void addToListView(ListView listView){
		LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		((ViewGroup)listView.getParent()).addView(view, lp); 
		listView.setEmptyView(view);
	}
	
	public void addToGridView(GridView gridView){
		LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		((ViewGroup)gridView.getParent()).addView(view, lp); 
		gridView.setEmptyView(view);
	}
	
	public void reset(){
		isshow = false;
		empty_icon.setImageBitmap(null);
		empty_prompt.setText("");
	}
	
	public void showView(int drawable, int msg){
		isshow = true;
		empty_icon.setImageResource(drawable);
		empty_prompt.setText(msg);
	}
	
	public void showView(int drawable, String msg){
		isshow = true;
		empty_icon.setImageResource(drawable);
		empty_prompt.setText(msg);
	}
	
	
	public boolean isIsshow() {
		return isshow;
	}

	public interface OnSearchListener{
		public abstract void onSearch(String search);
	}
	
}
