package com.wachoo.pangker.ui;

import android.content.Context;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;

public class CommentBar {

	protected PopMenuDialog menuDialog;

	private Context context;
	private OnClickListener clickListener;
	private UITableView.ClickListener uITableListener;
	private OnLongClickListener LongClickListener;

	public CommentBar() {

	}

	public CommentBar(Context context) {
		this.context = context;
	}

	public OnClickListener getClickListener() {
		return clickListener;
	}

	public void setClickListener(OnClickListener clickListener) {
		this.clickListener = clickListener;
	}

	public UITableView.ClickListener getuITableListener() {
		return uITableListener;
	}

	public void setuITableListener(UITableView.ClickListener uITableListener) {
		this.uITableListener = uITableListener;
	}

	public OnLongClickListener getLongClickListener() {
		return LongClickListener;
	}

	public void setLongClickListener(OnLongClickListener longClickListener) {
		LongClickListener = longClickListener;
	}

	ActionItem[] copyMenu = new ActionItem[] { new ActionItem(1, "复制") };

	protected void showCopyMenu() {
		menuDialog = new PopMenuDialog(context, R.style.MyDialog);
		menuDialog.setListener(this.uITableListener);
		menuDialog.setTitle("评论信息");
		menuDialog.setMenus(copyMenu);
		menuDialog.show();
	}

	ActionItem[] showCopyAndDeleteMenu = new ActionItem[] { new ActionItem(1, "复制"), new ActionItem(2, "删除") };

	protected void showCopyAndDeleteMenu() {
		menuDialog = new PopMenuDialog(context, R.style.MyDialog);
		menuDialog.setListener(this.uITableListener);
		menuDialog.setTitle("评论信息");
		menuDialog.setMenus(showCopyAndDeleteMenu);
		menuDialog.show();
	}

}
