package com.wachoo.pangker.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.wachoo.pangker.activity.R;

public class PushListView extends ListView{
	
	private View loadmoreView;
	private LinearLayout lyLoadPressBar;
	private LinearLayout lyLoadText;
	private OnLoadMoreListener onLoadMoreListener;
	private boolean ifLoading = false;

	public PushListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		loadmoreView = inflater.inflate(R.layout.loadmore_footerview, null);
		lyLoadPressBar = (LinearLayout)loadmoreView.findViewById(R.id.ly_loadmore_probar);
		lyLoadText = (LinearLayout)loadmoreView.findViewById(R.id.ly_loadmore_text);
		
		addFooterView(loadmoreView);
		
		lyLoadText.setOnClickListener(onClickListener);
	}
	
	public void removerFooter(){
		removeFooterView(loadmoreView);
	}
	
	public void addFooter(){
		addFooterView(loadmoreView);
	}
	
	public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
		this.onLoadMoreListener = onLoadMoreListener;
	}

	private View.OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(onLoadMoreListener != null && !ifLoading){
				ifLoading = true;
				setPresdBarVisiable(true);
				onLoadMoreListener.onLoadMoreListener();
			}
		}
	};
	
	public interface OnLoadMoreListener {
		/**
		 * onRefresh will be called for both Pull Down from top, and Pull Up
		 * from Bottom
		 */
		public void onLoadMoreListener();

	}
	
	public final void onLoadComplete(int count, int sum) {
		ifLoading = false;
		if(getAdapter() == null){
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			return;
		}
		if(count >= sum){
			hideFoot();
		} else {
			setPresdBarVisiable(false);
		}
	}
	
	public void hideFoot(){
		lyLoadPressBar.setVisibility(View.GONE);
		lyLoadText.setVisibility(View.GONE);
	}
	
	public void setPresdBarVisiable(boolean isShow){
		if(isShow){
			lyLoadPressBar.setVisibility(View.VISIBLE);
			lyLoadText.setVisibility(View.GONE);
		}
		else {
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.VISIBLE);
		}
	}
}
