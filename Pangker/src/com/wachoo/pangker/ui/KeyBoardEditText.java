package com.wachoo.pangker.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * 
 * 键盘隐藏
 * 
 * @author wangxin
 * 
 */
public class KeyBoardEditText extends EditText {

	private Context context;
	private int softKeyboardPopLocationY = 0;

	public KeyBoardEditText(Context context) {
		super(context);
		this.context = context;
		// TODO Auto-generated constructor stub
	}

	public KeyBoardEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		// TODO Auto-generated constructor stub
	}

	public KeyBoardEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		// TODO Auto-generated constructor stub
	}

	/**
	 * 显示键盘
	 */
	public void showSoftKeyboard() {
		((InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE)).showSoftInput(this, 0);
		int[] location = new int[2];
		this.getLocationOnScreen(location);
		this.softKeyboardPopLocationY = location[1];
	}

	/**
	 * 设置内容
	 * 
	 * @param hintString
	 */
	public void setCommentTypeOnlyComment(String hintString) {
		int[] location = new int[2];
		this.getLocationOnScreen(location);
		int softKeyboardLocationY = location[1];

		// >>>>>>>>如果不是弹出的设置为评论
		if (this.softKeyboardPopLocationY != softKeyboardLocationY) {
			this.setHint(hintString);
		}
	}

}
