package com.wachoo.pangker.ui;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.wachoo.pangker.music.Lyric;
import com.wachoo.pangker.music.Sentence;
import com.wachoo.pangker.util.ImageUtil;

/**
 * @author wangxin
 * 
 */
public class LyricView extends TextView {

	private final String LOADING_LYRIC = "正在加载歌词......";

	private final String LOADLYRIC_FAIL = "歌词加载失败";

	private String lyricTitle = LOADING_LYRIC;

	private Paint NotCurrentPaint; // 非当前歌词画笔
	private Paint CurrentPaint; // 当前歌词画笔

	private int notCurrentPaintColor = Color.WHITE;// 非当前歌词画笔 颜色
	private int CurrentPaintColor = Color.parseColor("#009ad6"); // 当前歌词画笔 颜色

	private Typeface Texttypeface = Typeface.SERIF;
	private Typeface CurrentTexttypeface = Typeface.SERIF;
	private float width;
	private Lyric mLyric;
	private int brackgroundcolor = 0xf000000; // 背景颜色
	private float lrcTextSize = 22; // 歌词大小
	private float CurrentTextSize = 24;
	// private Align = Paint.Align.CENTER；

	public float mTouchHistoryY;
	private int rowCount = 0;// >>>>>>一页显示行数
	private int height;
	private long currentDunringTime; // 当前行歌词持续的时间，用该时间来sleep
	// private float middleY;// y轴中间
	private int TextHeight = 50; // 每一行的间隔
	private boolean lrcInitDone = false;// 是否初始化完毕了
	public int index = 0;
	private int lastIndex = 0;
	private List<Sentence> Sentencelist; // 歌词列表

	// // >>>>>>>>>显示歌词的第一条和最后一条
	// private int firstShowIndex = 0;
	// private int endShowIndex = 0;

	// >>>>>>>>>歌词提前延时控制
	private int delay = 0;

	public String getLyricTitle() {
		return lyricTitle;
	}

	public void setLyricTitle(String lyricTitle) {
		this.lyricTitle = lyricTitle;
	}

	public void resetLyricTitle() {
		this.lyricTitle = LOADING_LYRIC;
	}

	private long currentTime;

	private long sentenctTime;

	public Paint getNotCurrentPaint() {
		return NotCurrentPaint;
	}

	public void setNotCurrentPaint(Paint notCurrentPaint) {
		NotCurrentPaint = notCurrentPaint;
	}

	public boolean isLrcInitDone() {
		return lrcInitDone;
	}

	public Typeface getCurrentTexttypeface() {
		return CurrentTexttypeface;
	}

	public void setCurrentTexttypeface(Typeface currrentTexttypeface) {
		CurrentTexttypeface = currrentTexttypeface;
	}

	public void setLrcInitDone(boolean lrcInitDone) {
		this.lrcInitDone = lrcInitDone;
	}

	public float getLrcTextSize() {
		return lrcTextSize;
	}

	public void setLrcTextSize(float lrcTextSize) {
		this.lrcTextSize = lrcTextSize;
	}

	public float getCurrentTextSize() {
		return CurrentTextSize;
	}

	public void setCurrentTextSize(float currentTextSize) {
		CurrentTextSize = currentTextSize;
	}

	public Lyric getmLyric() {
		return mLyric;
	}

	public void setmLyric(Lyric mLyric) {
		this.mLyric = mLyric;
	}

	public Paint getCurrentPaint() {
		return CurrentPaint;
	}

	public void setCurrentPaint(Paint currentPaint) {
		CurrentPaint = currentPaint;
	}

	public List<Sentence> getSentencelist() {
		return Sentencelist;
	}

	public void setSentencelist(List<Sentence> sentencelist) {
		Sentencelist = sentencelist;
	}

	// public void reset() {
	// this.firstShowIndex = -1;
	// this.endShowIndex = -1;
	// }

	public int getNotCurrentPaintColor() {
		return notCurrentPaintColor;
	}

	public void setNotCurrentPaintColor(int notCurrentPaintColor) {
		this.notCurrentPaintColor = notCurrentPaintColor;
	}

	public int getCurrentPaintColor() {
		return CurrentPaintColor;
	}

	public void setCurrentPaintColor(int currrentPaintColor) {
		CurrentPaintColor = currrentPaintColor;
	}

	public Typeface getTexttypeface() {
		return Texttypeface;
	}

	public void setTexttypeface(Typeface texttypeface) {
		Texttypeface = texttypeface;
	}

	public int getBrackgroundcolor() {
		return brackgroundcolor;
	}

	public void setBrackgroundcolor(int brackgroundcolor) {
		this.brackgroundcolor = brackgroundcolor;
	}

	public int getTextHeight() {
		return TextHeight;
	}

	public void setTextHeight(int textHeight) {
		TextHeight = textHeight;
	}

	// >>>>>>>>>歌词拖动显示工具栏
	private PopupWindow dragTimerWindow;

	public PopupWindow getDragTimerWindow() {
		return dragTimerWindow;
	}

	public void setDragTimerWindow(PopupWindow dragTimerWindow) {
		this.dragTimerWindow = dragTimerWindow;
	}

	public void showDragTimerBar() {
		if (dragTimerWindow != null) {
			int[] loc = new int[2];
			getLocationOnScreen(loc);
			this.dragTimerWindow.showAtLocation(this, Gravity.NO_GRAVITY, (int) width / 2, (int) (loc[1]
					+ height / 2 - TextHeight * 0.5));
		}
	}

	public void dismissDragTimerBar() {
		if (dragTimerWindow != null)
			this.dragTimerWindow.dismiss();
	}

	// >>>>>>>>>>>>>>歌词工具栏
	private UILrcOffsetView mLrcOffsetView;

	public UILrcOffsetView getmLrcOffsetView() {
		return mLrcOffsetView;
	}

	public void setmLrcOffsetView(UILrcOffsetView mLrcOffsetView) {
		this.mLrcOffsetView = mLrcOffsetView;
		this.mLrcOffsetView.setLrcDOWNOnClickListener(clickListener);
		this.mLrcOffsetView.setLrcUPOnClickListener(clickListener);
		this.mLrcOffsetView.setLrcResetOnClickListener(clickListener);
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == mLrcOffsetView.getBtnLrcUp()) {
				delayTime(false);
			}
			if (v == mLrcOffsetView.getBtnLrcDown()) {
				delayTime(true);
			}
			if (v == mLrcOffsetView.getBtnLrcNow()) {
				delayReduction();
			}
		}
	};

	public void showLrcOffSetView() {
		this.mLrcOffsetView.showAtLocation(this, Gravity.CENTER | Gravity.RIGHT, 0, 0);
	}

	public void lrcOffSetViewDismiss() {
		this.mLrcOffsetView.dismiss();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		return gestureDetector.onTouchEvent(event);
	}

	// >>>>>手势
	GestureDetector gestureDetector = new GestureDetector(new OnGestureListener() {

		// 抬起，手指离开触摸屏时触发(长按、滚动、滑动时，不会触发这个手势)
		@Override
		public boolean onSingleTapUp(MotionEvent arg0) {
			// TODO Auto-generated method stub
			lrcOffSetViewDismiss();
			return false;
		}

		// 短按，触摸屏按下后片刻后抬起，会触发这个手势，如果迅速抬起则不会
		@Override
		public void onShowPress(MotionEvent arg0) {
			// TODO Auto-generated method stub
			showLrcOffSetView();
		}

		// 滚动，触摸屏按下后移动
		@Override
		public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
			// TODO Auto-generated method stub
			return false;
		}

		// 长按，触摸屏按下后既不抬起也不移动，过一段时间后触发
		@Override
		public void onLongPress(MotionEvent arg0) {
			// TODO Auto-generated method stub

		}

		// 滑动，触摸屏按下后快速移动并抬起，会先触发滚动手势，跟着触发一个滑动手势
		@Override
		public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
			// TODO Auto-generated method stub
			return false;
		}

		// 单击，触摸屏按下时立刻触发
		@Override
		public boolean onDown(MotionEvent arg0) {
			// TODO Auto-generated method stub
			return false;
		}
	});

	//
	private Context mContext;

	public LyricView(Context context) {
		super(context);
		mContext = context;
		init();
	}

	public LyricView(Context context, AttributeSet attr) {
		super(context, attr);
		mContext = context;
		init();
	}

	public LyricView(Context context, AttributeSet attr, int i) {
		super(context, attr, i);
		mContext = context;
		init();
	}

	private void init() {
		Log.i("MusicPlayActivity", "init");
		setFocusable(true);
		float density = ImageUtil.getDensity((Activity)getContext());

		if (density > 0.75f && density <= 1.0f) {
			lrcTextSize = 20;
			CurrentTextSize = 20;
			TextHeight = 39;
		} else if (density > 1.0f && density <= 1.5f) {
			lrcTextSize = 23;
			CurrentTextSize = 23;
			TextHeight = 38;
		} else {
			lrcTextSize = 34;
			CurrentTextSize = 34;
			TextHeight = 47;
		}

		Log.i("LyricView", "lrcTextSize = " + lrcTextSize);

		// 非高亮部分
		NotCurrentPaint = new Paint();
		NotCurrentPaint.setAntiAlias(true);
		NotCurrentPaint.setTextAlign(Paint.Align.CENTER);
		// >>>>>>>非高亮部分歌词
		NotCurrentPaint.setColor(notCurrentPaintColor);
		NotCurrentPaint.setTextSize(lrcTextSize);
		NotCurrentPaint.setTypeface(Texttypeface);

		// >>>>>>>>>>高亮部分 当前歌词
		CurrentPaint = new Paint();
		CurrentPaint.setAntiAlias(true);
		CurrentPaint.setColor(CurrentPaintColor);
		CurrentPaint.setTextSize(CurrentTextSize);
		CurrentPaint.setTypeface(CurrentTexttypeface);
		CurrentPaint.setTextAlign(Paint.Align.CENTER);

	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		canvas.drawColor(brackgroundcolor);

		if (index == -1 || Sentencelist == null || Sentencelist.size() == 0 || index > Sentencelist.size()) {
			canvas.drawText(lyricTitle, width / 2, height / 2, CurrentPaint);
			return;
		}

		// float plus = currentDunringTime == 0 ? 30 : 30
		// + (((float) currentTime - (float) sentenctTime) / (float)
		// currentDunringTime) * (float) 30;

		// 向上滚动 这个是根据歌词的时间长短来滚动，整体上移
		// canvas.translate(0, -plus);
		// 先画当前行，之后再画他的前面和后面，这样就保持当前行在中间的位置
		try {
			canvas.drawText(Sentencelist.get(index).getContent(), width / 2, height / 2, CurrentPaint);
			//canvas.translate(0, plus);

			float tempY = height / 2;
			// 画出本句之前的句子
			for (int i = index - 1; i >= 0; i--) {
				// 向上推移
				tempY = tempY - TextHeight;
				if (tempY < 0) {
					break;
				}
				canvas.drawText(Sentencelist.get(i).getContent(), width / 2, tempY, NotCurrentPaint);
				//canvas.translate(0, TextHeight);
			}

			tempY = height / 2;
			// 画出本句之后的句子
			for (int i = index + 1; i < Sentencelist.size(); i++) {
				// 往下推移
				tempY = tempY + TextHeight;
				if (tempY > height) {
					break;
				}
				canvas.drawText(Sentencelist.get(i).getContent(), width / 2, tempY, NotCurrentPaint);
				//canvas.translate(0, TextHeight);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	protected void onSizeChanged(int w, int h, int ow, int oh) {
		super.onSizeChanged(w, h, ow, oh);
		Log.i("MusicPlayActivity", "onSizeChanged");
		width = w; // remember the center of the screen
		height = h;
		rowCount = height / TextHeight;
		// middleY = h * 0.5f;
	}

	//
	/**
	 * @param time
	 *            当前歌词的时间轴
	 * 
	 * @return null
	 */
	public void updateIndex(long time) {

		int first = 0;
		first = index - rowCount / 2;

		this.currentTime = time;
		currentTime += delay * 500;
		// 歌词序号
		if (mLyric == null || Sentencelist == null)
			return;
		index = mLyric.getNowSentenceIndex(currentTime);
		if (index < 0) {
			if (first < 0)
				index = 1;
			else
				index = Sentencelist.size() - 1;
		}
		// >>>>>>>>>>定位时间
		if (index != -1 && Sentencelist.size() > index) {
			Sentence sen = Sentencelist.get(index);
			sentenctTime = sen.getFromTime();
			currentDunringTime = sen.getDuring();
		}
	}

	public long getDragStartTime(long time) {
		int dragIndex = mLyric.getNowSentenceIndex(time);
		if (dragIndex != -1) {
			return Sentencelist.get(dragIndex).getFromTime();
		}
		return time;
	}

	public long getFirstShowTime() {
		if (!isAvailable())
			return 0l;

		int first = 0;
		int end = 0;
		first = index - rowCount / 2;
		end = index + rowCount / 2;

		Log.e("MusicPlayActivity", "getFirstShowTime>>>first=" + first + ";index=" + index + ";end=" + end);
		if (first <= 0) {
			return -(Sentencelist.get(end).getFromTime() - Sentencelist.get(index).getFromTime());
		} else {
			return Sentencelist.get(first - 1).getFromTime();
		}
	}

	//
	/**
	 * @param time
	 *            当前歌词的时间轴
	 * 
	 * @return null
	 */
	public long getEndShowTime() {
		if (!isAvailable())
			return 0l;
		// Log.e("MusicPlayActivity", "getEndShowTime:first=" + firstShowIndex +
		// ";index=" + index + ";end="
		// + endShowIndex);
		// if (endShowIndex == -1 || index - firstShowIndex > endShowIndex -
		// index) {
		// return Sentencelist.get(index).getFromTime()
		// + (Sentencelist.get(index).getFromTime() -
		// Sentencelist.get(firstShowIndex).getFromTime());
		// }
		// if (endShowIndex < Sentencelist.size())
		// return Sentencelist.get(endShowIndex).getFromTime();
		// else
		// return 0l;

		// int first = 0;
		int end = 0;
		// first = index - rowCount / 2;
		end = index + rowCount / 2;
		Log.e("MusicPlayActivity", "getEndShowTime>>>>index=" + index + ";end=" + end);
		if (end > Sentencelist.size()) {
			return Sentencelist.get(Sentencelist.size() - 1).getFromTime();
		} else {
			return Sentencelist.get(end - 1).getFromTime();
		}

	}

	public void delayTime(boolean ifDelay) {
		if (ifDelay) {
			delay++;
		} else {
			delay--;
		}
	}

	public void delayReduction() {
		delay = 0;
	}

	public boolean isAvailable() {
		if (mLyric != null && Sentencelist != null && Sentencelist.size() > 0)
			return true;
		else
			return false;
	}
}