package com.wachoo.pangker.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.wachoo.pangker.activity.R;

public class FooterView implements View.OnClickListener {

	private View loadmoreView;
	private LinearLayout lyLoadPressBar;
	private LinearLayout lyLoadText;
	private boolean ifLoading = false;
	private OnLoadMoreListener onLoadMoreListener;
	private ListView listView;
	private int sum = 0;

	public View getLoadmoreView() {
		return loadmoreView;
	}

	public FooterView(Context context) {
		// TODO Auto-generated constructor stub
		loadmoreView = LayoutInflater.from(context).inflate(
				R.layout.loadmore_footerview, null);
		lyLoadPressBar = (LinearLayout) loadmoreView
				.findViewById(R.id.ly_loadmore_probar);
		lyLoadText = (LinearLayout) loadmoreView
				.findViewById(R.id.ly_loadmore_text);
		lyLoadText.setOnClickListener(this);
	}

	public void addToListView(ListView listView) {
		this.listView = listView;
		listView.addFooterView(loadmoreView);
	}

	public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
		this.onLoadMoreListener = onLoadMoreListener;
	}

	public void onLoadCompleteErr() {
		// TODO Auto-generated method stub
		if (listView.getAdapter() == null) {
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			return;
		}
		int count = listView.getAdapter().getCount();
		onLoadComplete(count, sum);
	}

	/***
	 * 
	 * @param count
	 *            数量
	 * @param sum
	 *            总数
	 */
	public final void onLoadComplete(int count, int sum) {
		this.sum = sum;
		ifLoading = false;
		if (listView != null) {
			if (listView.getAdapter() == null) {
				hideFoot();
				return;
			}
		}
       
		if (count >= sum) {
			hideFoot();
		} else {
			setPresdBarVisiable(false);
		}
	}

	public final void onLoadComplete() {
		ifLoading = false;
		setPresdBarVisiable(false);
	}
	
	public void hideFoot() {
		lyLoadPressBar.setVisibility(View.GONE);
		lyLoadText.setVisibility(View.GONE);
	}

	public void setPresdBarVisiable(boolean isShow) {
		if (isShow) {
			lyLoadPressBar.setVisibility(View.VISIBLE);
			lyLoadText.setVisibility(View.GONE);
		} else {
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (onLoadMoreListener != null && !ifLoading) {
			ifLoading = true;
			setPresdBarVisiable(true);
			onLoadMoreListener.onLoadMoreListener();
		}
	}

	public interface OnLoadMoreListener {
		public void onLoadMoreListener();
	}

}
