package com.wachoo.pangker.notice;

import android.app.Service;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

/**
 * 
 * 
 * @author wangxin
 * 
 */
public class MessageNotifier {

	// >>>>>>震动提示方式
	private static final long[] VIBRATE = { 200, 200, 200, 200 };

	// >>>>>>消息提示时间间隔
	private static final long NOTICE_INTERVAL = 2000;

	// >>>>>>>最后一次提示时间 声音
	private long lastSoundNoticeTime;

	// >>>>>>>最后一次提示时间 震动
	private long lastVibratorNoticeTime;

	// >>>>>>>>
	private long lastNoticeTime;
	private Context mContext;

	// >>>>>>>加载成功的声音id
	private Ringtone defaultNotification;
	private Vibrator vibrator;

	public MessageNotifier(Context mContext) {
		this.mContext = mContext;
		vibrator = (Vibrator) this.mContext.getSystemService(Service.VIBRATOR_SERVICE);
	}

	private void playDefaultNotification() {
		// >>>>>>获取系统提示声音
		Uri uri = RingtoneManager.getActualDefaultRingtoneUri(this.mContext, RingtoneManager.TYPE_NOTIFICATION);
		defaultNotification = RingtoneManager.getRingtone(this.mContext, uri);
		if(defaultNotification != null){
			defaultNotification.play();
		}
	}

	// >>>>>>>声音提示
	public void messageSoundNotice() {
		if (needSoundNotice())
			playDefaultNotification();
	}

	// >>>>>>>震动提示
	public void messageVibratorNotice() {
		if (needVibratorNotice())
			vibrator.vibrate(VIBRATE, -1);
	}

	// >>>>>>>是否需要声音提示
	private boolean needSoundNotice() {
		if (System.currentTimeMillis() - lastSoundNoticeTime > NOTICE_INTERVAL) {
			lastSoundNoticeTime = System.currentTimeMillis();
			return true;
		}
		return false;
	}

	// >>>>>>>是否需要震动提示
	private boolean needVibratorNotice() {
		if (System.currentTimeMillis() - lastVibratorNoticeTime > NOTICE_INTERVAL) {
			lastVibratorNoticeTime = System.currentTimeMillis();
			return true;
		}
		return false;
	}

	/**
	 * 根据类型判断是否需要通知
	 * 
	 * @param notifyType
	 */
	public void messageSoundAndVibratorNotice(MessageNotifyType notifyType) {
		if (needNotice()) {
			playDefaultNotification();
			vibrator.vibrate(VIBRATE, -1);
		}
	}

	private boolean needNotice() {
		if (System.currentTimeMillis() - lastNoticeTime > NOTICE_INTERVAL) {
			lastNoticeTime = System.currentTimeMillis();
			return true;
		}
		return false;
	}

	public enum MessageNotifyType {
		SOUND, VIBRATOR, BOTH
	}
}
