package com.wachoo.pangker.gestures;

public class NextCommend implements GestureCommand{

	GestureListener gestureListener;
	
	public NextCommend(GestureListener gestureListener) {
		super();
		this.gestureListener = gestureListener;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		if(gestureListener != null){
			gestureListener.gestureNext();
		}
	}

}
