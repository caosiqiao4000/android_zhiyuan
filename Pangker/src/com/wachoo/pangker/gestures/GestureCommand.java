package com.wachoo.pangker.gestures;

public interface GestureCommand {

	public void execute();
}
