package com.wachoo.pangker.group;

public class P2PFarther {

	// pakage type
	public static final int TYPE_SP2MROOM_STATUS_RESP = 0x051C;  //进入房间 服务器返回群组结果
	public static final int TYPE_SP2MROOM_STATUS_NOTIFY = 0x053B; // 用户在线时,服务器通知用户房间成员状态变化
	public static final int TYPE_SP2NEXTHOP_STREAM = 0x051B; //接收服务器的 文字,图片聊天 语音流
	public static final int TYPE_SP2CMANAGER_LOGIN_RESP = 0x0512; // 登陆GSP返回
	public static final int TYPE_SP2CROOM_SPEECH_NOTIFY = 0x0529;  //发言状态通知 0x61：开始发言；0x62：暂停发言
	public static final int TYPE_SP2CROOM_SPEECH_RESP = 0x051A;  // 麦序操作回应0x51：管理员不同意；0x52：管理员同意
	public static final int TYPE_SP2CROOM_SPEECK_LIST_RESP = 0x051E;  //服务器返回麦序结果
	public static final int TYPE_SP2CROOM_SPEECHLISTSTATUS_NOTIFY = 0x053D; //麦序列表增减通知
	public static final int TYPE_SP2CROOM_USERLIST = 0x0528;  // 服务器返回成员列表
	public static final int TYPE_SP2CLIENT_NOTREAD_RESP = 0x052C;  //服务器返回离线消息结果
	public static final int TYPE_SP2FILE_UPLOAD_RESP = 0x052E;  //服务器返回上传图片结果
	public static final int TYPE_SP2CLIENT_HELLO_ACK = 0x052A;  //心跳包回应
	public static final int TYPE_SP2CLIENT_CONF_RESP = 0x0532;  //群组设置的响应结果

	public final short RKEY = 8194; // 0x2002
	public final short WKEY = 4097; // 0x1001

	public native int getResp_Size(int p2pType);

	public native int getMsgHead_Size();

	public native int getUserInfos_Size(int MembersNum);

	public native short getMsg_Type(byte[] MsgHead);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-7 05:49:03
	 * @param Sender_UID
	 * @param Receiver_UID
	 * @param Authenticator
	 * @return Error-Code
	 */
	public native byte[] getCmanager2SP_Login_Req(Cmanager2SP_Login_Req obj);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-7 06:26:15
	 * @param Sender_UID
	 * @param Receiver_UID
	 */
	public native byte[] getCmanager2SP_Hello(Cmanager2SP_Hello obj);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-7 06:26:17
	 * @param OwnerUID
	 * @param OwnerSID
	 * @param RKey
	 * @param CRoomOperate
	 * @param MembersOperate
	 * @param AddType
	 * @param MembersInfo
	 */
	public native byte[] getCRoom2SP_Status_Report(CRoom2SP_Status_Report obj);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-7 06:26:24
	 * @param Sender_UID
	 * @param Receiver_UID
	 * @param OwnerUID
	 * @param OwnerSID
	 * @param S_Type
	 * @param PayLoadLen
	 * @param PayLoad
	 */
	public native byte[] getCP2SP_Stream(CP2SP_Stream obj, byte[] transform);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-7 06:26:30
	 * @param OwnerUID
	 * @param OwnerSID
	 * @param SpeekerUID
	 */
	public native byte[] getCP2SP_Stream_Stop(CP2SP_Stream_Stop obj);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-7 06:26:32
	 * @param Sender_UID
	 * @param Receiver_UID
	 * @param OwnerUID
	 * @param OwnerSID
	 * @param WKEY
	 * @param SpeechOp
	 * @param SpeekerInfo
	 */
	public native byte[] getCRoom2SP_Speech_Req(CRoom2SP_Speech_Req obj);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-7 06:26:40
	 * @param Sender_UID
	 * @param Receiver_UID
	 * @param OwnerUID
	 * @param OwnerSID
	 * @param RKEY
	 */
	public native byte[] getCRoom2SP_SpeechList_Req(CRoom2SP_SpeechList_Req obj);

	/**
	 * 
	 * @author wubo
	 * @createtime Jun 14, 2012
	 * @param obj
	 * @return
	 */
	public native byte[] getCRoom2SP_LoginOut_Req(CRoom2SP_LoginOut_Req obj);

	/**
	 * 
	 * @author wubo
	 * @createtime Jun 14, 2012
	 * @param ojb
	 * @return
	 */
	public native byte[] getCRoom2SP_UserList_Req(CRoom2SP_UserList_Req ojb);

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public native byte[] getClient2SP_NotRead_Req(Client2SP_NotRead_Req obj);

	/**
	 * @author wubo
	 * @createtime 2012-3-8 11:13:38
	 * @param bytes
	 */
	public native void ConvertPackage(byte[] bytes, P2PInterface user_data, Object Sock);

	public native byte[] getSP2Cmanager_Login_Resp(SP2Cmanager_Login_Resp obj);

	public native P2PUserInfo[] getstUsersInfo(byte[] UsersInfo, int UsersInfoSize);

	public native byte[] setMsg_Length(byte[] bytes, short sMsg_Length);

	public native short getMsg_Length(byte[] bytes);

	public native int getNoReadMsgSize(int len);

	public native ChatRecordInfo[] getChatRecordInfo(byte[] msg, int len);

	public native byte[] getFile2SP_Upload_Req(File2SP_Upload_Req obj);

	public native byte[] getFilePacketInfo(FilePacketInfo obj);

	/**
	 * 通过JNI把文件转换成BYTE[]
	 * 
	 * @param obj
	 * @param length
	 * @return
	 */
	public native byte[] getFilePacketInfo(FilePacketInfo obj, short length);

	public native FilePacketInfo fomatFilePacketInfo(byte[] bytes);

	public native int byteToInt(byte[] bytes);

	public native int getCompressFlag(byte[] bytes);
	
	//>>>>>>>>>>>>废弃
	public native byte[] getCRoom2SP_Conf_Report(CRoom2SP_Conf_Report obj); 
	
}
