package com.wachoo.pangker.group;

public class Cmanager2SP_Login_Req 
{
	private long Sender_UID; 
	private long Receiver_UID;
	private String Authenticator;
	
	public long getSender_UID() 
	{
		return Sender_UID;
	}
	public void setSender_UID(long senderUID)
	{
		Sender_UID = senderUID;
	}
	public long getReceiver_UID() 
	{
		return Receiver_UID;
	}
	public void setReceiver_UID(long receiverUID) 
	{
		Receiver_UID = receiverUID;
	}
	public String getAuthenticator() 
	{
		return Authenticator;
	}
	public void setAuthenticator(String authenticator) 
	{
		Authenticator = authenticator;
	}
	public Cmanager2SP_Login_Req(long senderUID, long receiverUID,
			String authenticator) {
		super();
		Sender_UID = senderUID;
		Receiver_UID = receiverUID;
		Authenticator = authenticator;
	}
	
	
};
