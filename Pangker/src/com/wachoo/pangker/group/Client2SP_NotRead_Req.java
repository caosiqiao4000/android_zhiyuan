package com.wachoo.pangker.group;

public class Client2SP_NotRead_Req {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private int IsLocalDBExist;
	private long NoReadBeginTime; // 拉取末读的开始时间

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public int getIsLocalDBExist() {
		return IsLocalDBExist;
	}

	public void setIsLocalDBExist(int isLocalDBExist) {
		IsLocalDBExist = isLocalDBExist;
	}

	public long getNoReadBeginTime() {
		return NoReadBeginTime;
	}

	public void setNoReadBeginTime(long noReadBeginTime) {
		NoReadBeginTime = noReadBeginTime;
	}

	public Client2SP_NotRead_Req(long senderUID, long receiverUID, long ownerUID, long ownerSID,
			int IsLocalDBExist, long NoReadBeginTime) {
		Sender_UID = senderUID;
		Receiver_UID = receiverUID;
		OwnerUID = ownerUID;
		OwnerSID = ownerSID;
		this.IsLocalDBExist = IsLocalDBExist;
		this.NoReadBeginTime = NoReadBeginTime;
	}

}
