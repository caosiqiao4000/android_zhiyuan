package com.wachoo.pangker.group;

public class SP2CRoom_Conf_Resp {
	
	private long Sender_UID;
	
	private long Receiver_UID;
	
	private long OwnerUID;
	
	private long OwnerSID;
	
	private short Error_Code;

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long sender_UID) {
		Sender_UID = sender_UID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiver_UID) {
		Receiver_UID = receiver_UID;
	}

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public short getError_Code() {
		return Error_Code;
	}

	public void setError_Code(short error_Code) {
		Error_Code = error_Code;
	}
	
}
