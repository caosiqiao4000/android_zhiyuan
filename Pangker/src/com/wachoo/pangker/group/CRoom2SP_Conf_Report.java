package com.wachoo.pangker.group;

/**
 * Sender_UID	ULONGLONG
Receiver_UID	ULONGLONG
OwnerUID	ULONGLONG
OwnerSID	ULONGLONG
MaxSpeechTime	SHORT
 * @author zxx
 *
 */
public class CRoom2SP_Conf_Report {

    private long Sender_UID;
	
	private long Receiver_UID;
	
	private long OwnerUID;
	
	private long OwnerSID;
	
	//-1：无限制； >=0：最大发言限制时间（默认为300S）
	private short MaxSpeechTime;
	
	private short VoiceChannelType;

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long sender_UID) {
		Sender_UID = sender_UID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiver_UID) {
		Receiver_UID = receiver_UID;
	}

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public short getMaxSpeechTime() {
		return MaxSpeechTime;
	}

	public void setMaxSpeechTime(short maxSpeechTime) {
		MaxSpeechTime = maxSpeechTime;
	}

	public short getVoiceChannelType() {
		return VoiceChannelType;
	}

	public void setVoiceChannelType(short voiceChannelType) {
		VoiceChannelType = voiceChannelType;
	}

	public CRoom2SP_Conf_Report(long sender_UID, long receiver_UID,
			long ownerUID, long ownerSID, short maxSpeechTime,
			short voiceChannelType) {
		super();
		Sender_UID = sender_UID;
		Receiver_UID = receiver_UID;
		OwnerUID = ownerUID;
		OwnerSID = ownerSID;
		MaxSpeechTime = maxSpeechTime;
		VoiceChannelType = voiceChannelType;
	}
	
}
