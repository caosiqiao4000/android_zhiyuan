package com.wachoo.pangker.group;

public class CP2SP_Stream {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private short S_Type;
	private int PayLoadLen;
	private int MsgId;
	private byte[] PayLoad;

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public short getS_Type() {
		return S_Type;
	}

	public void setS_Type(short sType) {
		S_Type = sType;
	}

	public int getPayLoadLen() {
		return PayLoadLen;
	}

	public void setPayLoadLen(int payLoadLen) {
		PayLoadLen = payLoadLen;
	}

	public byte[] getPayLoad() {
		return PayLoad;
	}

	public void setPayLoad(byte[] payLoad) {
		PayLoad = payLoad;
	}

	public int getMsgId() {
		return MsgId;
	}

	public void setMsgId(int msgId) {
		MsgId = msgId;
	}

};
