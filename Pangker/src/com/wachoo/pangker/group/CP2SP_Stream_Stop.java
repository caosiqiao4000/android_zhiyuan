package com.wachoo.pangker.group;

public class CP2SP_Stream_Stop {

	private long OwnerUID;
	private long OwnerSID;
	private long SpeekerUID;
	private long Sender_UID;
	private long Receiver_UID;


	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public long getSpeekerUID() {
		return SpeekerUID;
	}

	public void setSpeekerUID(long speekerUID) {
		SpeekerUID = speekerUID;
	}

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}

	public CP2SP_Stream_Stop(long ownerUID, long ownerSID, long speekerUID,
			long senderUID, long receiverUID) {
		super();
		OwnerUID = ownerUID;
		OwnerSID = ownerSID;
		SpeekerUID = speekerUID;
		Sender_UID = senderUID;
		Receiver_UID = receiverUID;
	}
	
	

};
