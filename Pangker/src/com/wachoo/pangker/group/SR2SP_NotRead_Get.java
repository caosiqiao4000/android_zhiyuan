package com.wachoo.pangker.group;

public class SR2SP_NotRead_Get {

	private long Sender_UID;
	private long Receiver_UID;
	private int NoReadBegin;
	public long getSender_UID() {
		return Sender_UID;
	}
	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}
	public long getReceiver_UID() {
		return Receiver_UID;
	}
	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}
	public int getNoReadBegin() {
		return NoReadBegin;
	}
	public void setNoReadBegin(int noReadBegin) {
		NoReadBegin = noReadBegin;
	}
	
	
};
