package com.wachoo.pangker.group;

public class CRoom2SP_Speech_Req {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private short WKEY;
	private short SpeechOp;
	private short SpeechType; //麦序操作者类型：0x00：无操作；0x31：管理员麦序操作；0x32：成员麦序操作；
	private P2PUserInfo SpeekerInfo;
	public long getSender_UID() {
		return Sender_UID;
	}
	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}
	public long getReceiver_UID() {
		return Receiver_UID;
	}
	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}
	public long getOwnerUID() {
		return OwnerUID;
	}
	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}
	public long getOwnerSID() {
		return OwnerSID;
	}
	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}
	public short getWKEY() {
		return WKEY;
	}
	public void setWKEY(short wKEY) {
		WKEY = wKEY;
	}
	
	public short getSpeechOp() {
		return SpeechOp;
	}
	public void setSpeechOp(short speechOp) {
		SpeechOp = speechOp;
	}
	public P2PUserInfo getSpeekerInfo() {
		return SpeekerInfo;
	}
	public void setSpeekerInfo(P2PUserInfo speekerInfo) {
		SpeekerInfo = speekerInfo;
	}
	public short getSpeechType() {
		return SpeechType;
	}
	public void setSpeechType(short speechType) {
		SpeechType = speechType;
	}
	public CRoom2SP_Speech_Req(long senderUID, long receiverUID, long ownerUID,
			long ownerSID, short wKEY, short speechOp,short speechType, P2PUserInfo speekerInfo) {
		super();
		Sender_UID = senderUID;
		Receiver_UID = receiverUID;
		OwnerUID = ownerUID;
		OwnerSID = ownerSID;
		WKEY = wKEY;
		SpeechType = speechType;
		SpeechOp = speechOp;
		SpeekerInfo = speekerInfo;
	}
	
	
};
