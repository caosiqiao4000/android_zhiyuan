package com.wachoo.pangker.group;

/**
 * com.wachoo.pangker.group.SP2Client_Hello_ACK
 * 
 * @author wubo
 * @date 2012-12-12
 * 
 */
public class SP2Client_Hello_ACK {

	private long Receiver_UID;

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}

}
