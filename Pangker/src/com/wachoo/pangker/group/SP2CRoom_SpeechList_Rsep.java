package com.wachoo.pangker.group;

public class SP2CRoom_SpeechList_Rsep {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private short QueueSize;
	private short Error_Code;
	private P2PUserInfo[] SpeekerQueue;
	private short AdminSpeakStatus;
	
	public long getSender_UID() {
		return Sender_UID;
	}
	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}
	public long getReceiver_UID() {
		return Receiver_UID;
	}
	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}
	public long getOwnerUID() {
		return OwnerUID;
	}
	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}
	public long getOwnerSID() {
		return OwnerSID;
	}
	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}
	public short getQueueSize() {
		return QueueSize;
	}
	public void setQueueSize(short queueSize) {
		QueueSize = queueSize;
	}
	
	public short getError_Code() {
		return Error_Code;
	}
	public void setError_Code(short errorCode) {
		Error_Code = errorCode;
	}
	public P2PUserInfo[] getSpeekerQueue() {
		return SpeekerQueue;
	}
	public void setSpeekerQueue(P2PUserInfo[] speekerQueue) {
		SpeekerQueue = speekerQueue;
	}
	public short getAdminSpeakStatus() {
		return AdminSpeakStatus;
	}
	public void setAdminSpeakStatus(short adminSpeakStatus) {
		AdminSpeakStatus = adminSpeakStatus;
	}
	
};
