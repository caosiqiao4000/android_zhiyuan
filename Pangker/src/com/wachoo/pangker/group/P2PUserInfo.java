package com.wachoo.pangker.group;

import java.io.Serializable;

import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.util.Util;

public class P2PUserInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long UID;
	private int sex;
	private int locateType = 0;
	private String sign;
	private String UserName;
	private String locationInfo;

	public P2PUserInfo() {
	}

	public P2PUserInfo(long uID, String locationInfo) {
		super();
		UID = uID;
		this.locationInfo = locationInfo;
	}

	public long getUID() {
		return UID;
	}

	public void setUID(long uID) {
		UID = uID;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getLocationInfo() {
		return locationInfo;
	}

	public void setLocationInfo(String locationInfo) {
		this.locationInfo = locationInfo;
	}

	public Location getLocation() {
		Location location = new Location();
		String[] latlon = getLocationInfo().split(",");
		if (latlon.length == 2) {
			location.setLatitude(Util.String2Double(latlon[0]));
			location.setLongitude(Util.String2Double(latlon[1]));
		}
		return location;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getLocateType() {
		return locateType;
	}

	public void setLocateType(int locateType) {
		this.locateType = locateType;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public P2PUserInfo(long uID, int sex, int locateType, String sign, String userName, String locationInfo) {
		super();
		UID = uID;
		this.sex = sex;
		this.locateType = locateType;
		this.sign = sign;
		UserName = userName;
		this.locationInfo = locationInfo;
	}

	@Override
	public String toString() {
		return "P2PUserInfo [UID=" + UID + ", sex=" + sex + ", locateType=" + locateType + ", sign=" + sign
				+ ", UserName=" + UserName + ", locationInfo=" + locationInfo + "]";
	}

};
