package com.wachoo.pangker.group;

public class SP2CRoom_SpeechListStatus_Notify {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private short SpeakerOp;
	private P2PUserInfo SpeakerInfo;

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public short getSpeakerOp() {
		return SpeakerOp;
	}

	public void setSpeakerOp(short speakerOp) {
		SpeakerOp = speakerOp;
	}

	public P2PUserInfo getSpeakerInfo() {
		return SpeakerInfo;
	}

	public void setSpeakerInfo(P2PUserInfo speakerInfo) {
		SpeakerInfo = speakerInfo;
	}

};
