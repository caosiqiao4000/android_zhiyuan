package com.wachoo.pangker.group;

/**
 *@author wubo
 *@createtime Apr 12, 2012
 */
public class P2PUserInfoCase {

	private short speakerOp;//增删标识
	private P2PUserInfo info;
	public short getSpeakerOp() {
		return speakerOp;
	}
	public void setSpeakerOp(short speakerOp) {
		this.speakerOp = speakerOp;
	}
	public P2PUserInfo getInfo() {
		return info;
	}
	public void setInfo(P2PUserInfo info) {
		this.info = info;
	}
	public P2PUserInfoCase(short speakerOp, P2PUserInfo info) {
		this.speakerOp = speakerOp;
		this.info = info;
	}
	

	
}
