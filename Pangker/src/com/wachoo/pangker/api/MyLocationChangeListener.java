package com.wachoo.pangker.api;

import java.util.EventListener;
/**
 * Location 变化使用监听接口。
 * @author wangxin
 *
 */
public interface MyLocationChangeListener extends EventListener {
	public void LocationEvent(LocationEvent event);
}
