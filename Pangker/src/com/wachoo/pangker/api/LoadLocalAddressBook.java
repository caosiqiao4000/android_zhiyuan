package com.wachoo.pangker.api;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.ui.dialog.WaitingDialog;

public class LoadLocalAddressBook extends AsyncTask<Object, Integer, Boolean> {
	private Context mContext;
	private WaitingDialog mWaitingDialog;
	private PangkerApplication application;
	private ILoadLocalAddressBook localAddressBook;

	// >>>>>>>等待加载进度条
	public LoadLocalAddressBook(Context mContext, ILoadLocalAddressBook localAddressBook) {
		this.mContext = mContext;
		this.localAddressBook = localAddressBook;
		mWaitingDialog = new WaitingDialog(this.mContext);
		mWaitingDialog.setCancelable(false);
		application = (PangkerApplication) this.mContext.getApplicationContext();
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mWaitingDialog.show();
	}

	@Override
	protected Boolean doInBackground(Object... params) {
		// TODO Auto-generated method stub

		try {

			// >>>>>>>通讯录匹配加载
			while (!application.isLocalContactsInit()) {
				Thread.sleep(500);
			}

			return true;

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("loadLocalAddressBook", e.getMessage());
		}
		return true;

	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		mWaitingDialog.dismiss();
		if (localAddressBook != null)
			localAddressBook.loadResult(result);

	}

	public interface ILoadLocalAddressBook {
		public void loadResult(boolean result);
	}
}