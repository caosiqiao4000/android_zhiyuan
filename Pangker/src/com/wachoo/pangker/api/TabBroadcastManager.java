package com.wachoo.pangker.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.os.Message;

public class TabBroadcastManager {

	private static TabBroadcastManager tabBroadcastManager;

	public static TabBroadcastManager getTabBroadcastManager() {
		if (tabBroadcastManager == null) {
			tabBroadcastManager = new TabBroadcastManager();
		}
		return tabBroadcastManager;
	}

	private TabBroadcastManager() {
		// TODO Auto-generated constructor stub
	}

	public void exitManager() {
		// TODO Auto-generated method stub
		if (listenerList != null) {
			listenerList.clear();
		}
	}

	protected List<ITabSendMsgListener> listenerList = new ArrayList<ITabSendMsgListener>();

	/**
	 * 添加一个Listener监听器
	 */
	public synchronized void addMsgListener(ITabSendMsgListener listener) {
		if (listenerList != null && !listenerList.contains(listener)) {
			listenerList.add(listener);
		}
	}

	/**
	 * 移除一个已注册的Listener监听器. 如果监听器列表中已有相同的监听器listener1、listener2,
	 * 并且listener1==listener2, 那么只移除最近注册的一个监听器。
	 */
	public synchronized void removeUserListenter(ITabSendMsgListener listener) {
		if (listenerList != null)
			listenerList.remove(listener);
	}

	/**
	 * 启动监听
	 */
	public boolean sendResultMessage(String className, Message msg) {
		ITabSendMsgListener listener;
		if (listenerList != null) {
			Iterator<ITabSendMsgListener> it = listenerList.iterator();// 这步要注意同步问题
			while (it.hasNext()) {
				listener = it.next();
				if (listener != null && className.equals(listener.getClass().getName())) {
					listener.onMessageListener(msg);
					return true;
				}
			}
		}
		return false;
	}
}
