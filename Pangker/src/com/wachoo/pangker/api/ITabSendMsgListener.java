package com.wachoo.pangker.api;

import android.os.Message;

public interface ITabSendMsgListener {

	public void onMessageListener(Message msg);
}
