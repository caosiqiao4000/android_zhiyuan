package com.wachoo.pangker.api;

import com.wachoo.pangker.entity.UserItem;

public interface IContactUserChangedListener {
	boolean isHaveUser(UserItem item);

	void refreshUser(int what);
}
