package com.wachoo.pangker.api;

/**
 * pangker exception
 * 
 * @author wangxin
 * 
 */
public class PangkerException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int statusCode = -1;

	public PangkerException(String msg) {
		super(msg);
	}

	public PangkerException(Exception cause) {
		super(cause);
	}

	public PangkerException(String msg, int statusCode) {
		super(msg);
		this.statusCode = statusCode;

	}

	public PangkerException(String msg, Exception cause) {
		super(msg, cause);
	}

	public PangkerException(String msg, Exception cause, int statusCode) {
		super(msg, cause);
		this.statusCode = statusCode;

	}

	public int getStatusCode() {
		return this.statusCode;
	}
}
