package com.wachoo.pangker.api;

import java.util.EventObject;

import com.wachoo.pangker.entity.UserItem;

@SuppressWarnings("serial")
public class ContactUserEvent extends EventObject {

	private UserItem item;

	public ContactUserEvent(Object source, UserItem item) {
		super(source);
		this.item = item;
		// TODO Auto-generated constructor stub
	}

	public UserItem getItem() {
		return item;
	}

	public void setItem(UserItem item) {
		this.item = item;
	}
	
	

}
