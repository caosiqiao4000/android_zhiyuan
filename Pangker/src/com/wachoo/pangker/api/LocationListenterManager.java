package com.wachoo.pangker.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.location.Location;
/**
 * 位置更改监听类，同步提示类
 * @author wangxin
 *
 */
public class LocationListenterManager {
	protected static List<MyLocationChangeListener> listenerList = new ArrayList<MyLocationChangeListener>();
	private static LocationEvent event;
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	/**
	 * 添加一个Listener监听器
	 */
	public synchronized static void addLoacationListenter(MyLocationChangeListener listener) {
		listenerList.add(listener);
	}

	/**
	 * 移除一个已注册的Listener监听器. 如果监听器列表中已有相同的监听器listener1、listener2,
	 * 并且listener1==listener2, 那么只移除最近注册的一个监听器。
	 */
	public synchronized void removeLoacationListenter(MyLocationChangeListener listener) {
		listenerList.remove(listener);
	}

	/**
	 * 启动监听
	 */
	public void startListenterAction() {
		MyLocationChangeListener listener;
		Iterator it = listenerList.iterator();// 这步要注意同步问题
		while (it.hasNext()) {
			listener = (MyLocationChangeListener) it.next();
			if (event == null)
				event = new LocationEvent(this);
			listener.LocationEvent(event);
		}
	}

	/**
	 * Location变化通知类,如果oldlocation和newlocation相同不启动监听,不同启动监听
	 * 
	 * @param oldlocation
	 * @param newlocation
	 */
	public void locationHasChanged(Location oldlocation, Location newlocation) {
		if (newlocation == null || !newlocation.hasAccuracy()) {
			return;
		} else if (oldlocation == null) {
			startListenterAction();
		} else if (oldlocation != null 
				&& oldlocation.getLongitude() != newlocation.getLongitude() 
				&& oldlocation.getLatitude() != newlocation.getLatitude()) {
			startListenterAction();
		}
	}
}
