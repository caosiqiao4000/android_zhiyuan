package com.wachoo.pangker.music;

import java.util.EventListener;

import com.wachoo.pangker.server.response.MusicInfo;
/**
 * MusicPlayerListener
 * @author wangxin
 *
 */
public interface MusicPlayerListener extends EventListener {
	// >>> 播放
	void play(MusicInfo info);
	// >>> 暂停
	void pause(MusicInfo info);
	// >>> 错误
	void error();
	// >>> 播放全部完成
	void playOver();
	//缓存进度
	public void onBufferChanged(int percent);
	//缓存状态
	public void onBufferStatus(int status);
}
