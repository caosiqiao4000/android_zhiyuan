package com.wachoo.pangker.music;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;

import com.wachoo.pangker.PangkerConstant;

/**
 * 音乐播放类
 * 
 * @author wangxin
 * 
 */
public class MusicMediaPlayer extends MediaPlayer {

	private static final String TAG = "MusicMediaPlayer";
	// >>>>>>>临时文件后缀（文件名称）
	private static final String TEMP_FILE_SUFFIX = ".dat";

	// >>>>>>>>>>缓存大小，如果大于这个区间就开发播放音乐
	private static final int INTIAL_KB_BUFFER = 100 * 10 / 8;

	// >>>>>>>>>文件的大小,文件的时长
	private long mediaLengthInKb, mediaLengthInSeconds;

	// >>>>>>>>下载文件
	private File downloadingMediaFile;

	// >>>>>>>>>文件下载保存地址 默认地址，如果没有设置，使用该地址
	private static final String downloadPath_default = PangkerConstant.DIR_MUSIC;

	// >>>>>>>>下载完成的文件名
	private String downloadFileName;
	// >>>>>>>>>下载临时文件名称
	private String downloadFileNameTemp;
	// >>>>>>>>>>>下载路径
	private String downloadPath = downloadPath_default;
	// >>>>>>>网络音乐地址
	private String httpMusicPath;

	// >>>>>>>>>>>>以下是网络音乐相关监听
	private OnBufferingUpdateListener bufferingUpdateListener;

	private OnPreparedListener preparedListener;

	private OnBufferingOverListener bufferingOverListener;

	// >>>>>>>>下载音乐线程
	private DownloadAudioThread downloadAudioThread;

	// >>>>>>>>>启用本地缓存
	private boolean userLocalCahe = true;

	/**
	 * 设置网络缓存数据源
	 * 
	 * @param httpPath
	 */
	public void setNetDateSource(String httpPath) {
		this.httpMusicPath = httpPath;
	}

	// >>>>>>>>>使用handler实现线程管理,否则会出现冲突问题
	private Handler mHandler = new Handler();

	// >>>>>>>>>预缓冲
	@Override
	public synchronized void prepareAsync() throws IllegalStateException {
		this.isPrepared = false;

		// TODO Auto-generated method stub
		if (downloadAudioThread != null) {
			downloadAudioThread.interrup();
		}
		downloadAudioThread = new DownloadAudioThread(httpMusicPath);
		downloadAudioThread.start();

	}

	// >>>>>>>>网络音乐是否播放态
	public boolean isNetMusicPlaying() {
		if (downloadAudioThread != null && !downloadAudioThread.isDownloadOver())
			return true;
		else
			return isPlaying();
	}

	// >>>>>>>>当前缓冲音乐是否下载完毕
	public boolean isDownloadOver() {
		if (this.downloadAudioThread != null)
			return this.downloadAudioThread.isDownloadOver();
		return true;
	}

	// >>>>>>>>是否通知过缓存ok
	private boolean isPrepared = false;

	public void netMusicPlayonUpdate() {
		Runnable updater = new Runnable() {
			public void run() {

				// >>>>>>判断长度(长度不准确，该方法无法进入)
				playNetMusiconUpdate();
			}
		};
		mHandler.post(updater);
	}

	// 测试缓冲的文件大小是否大于INTIAL_KB_BUFFER，
	// 如果大于的话就播放>>>>>>>>>播放音乐(将文件转换成媒体流，mediaplay使用媒体流数据源播放音乐)
	public void netMusicStartPlay() {
		Runnable updater = new Runnable() {
			public void run() {
				// >>>>>>如果是第一次播放
				playNetMusicStart();
			}
		};
		mHandler.post(updater);
	}

	// >>>>>>>开始播放网络音乐
	private void playNetMusicStart() {
		try {
			try {
				if (isPlaying())
					stop();
				reset(); // 恢复到初始状态
			} catch (Exception e) {
				Log.i(TAG, "mediaPlayer.isPlaying()" + e.getMessage());
				reset(); // 恢复到初始状态
			}

			// FileInputStream fis = new FileInputStream(downloadingMediaFile);
			// setDataSource(fis.getFD());// 此方法返回与流相关联的文件说明符。
			if (downloadingMediaFile != null && downloadingMediaFile.exists()) {
				setDataSource(downloadingMediaFile.getAbsolutePath());
				prepare();
				setAudioStreamType(AudioManager.STREAM_MUSIC);
				start();
			} else {

			}

		} catch (IllegalArgumentException e) {
			// TODO: handle exception
		} catch (IllegalStateException e) {
			// TODO: handle exception
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	// >>>>>>>开始播放网络音乐
	private void playNetMusiconUpdate() {
		Log.i(TAG, "playNetMusiconUpdate");
		try {
			int curPosition = getCurrentPosition();
			pause();
			reset();
			// FileInputStream fis = new FileInputStream(downloadingMediaFile);
			if (downloadingMediaFile != null && downloadingMediaFile.exists()) {
				setDataSource(downloadingMediaFile.getAbsolutePath());// 此方法返回与流相关联的文件说明符。
				prepare();
				setAudioStreamType(AudioManager.STREAM_MUSIC);
				seekTo(curPosition);
				start();
			} else {

			}
		} catch (IllegalArgumentException e) {
			// TODO: handle exception
		} catch (IllegalStateException e) {
			// TODO: handle exception
		} catch (IOException e) {
			// TODO: handle exception
		}
	}

	// >>>>>>>停止下载
	public synchronized void interrrupted() {
		if (this.downloadAudioThread != null)
			this.downloadAudioThread.interrup();
	}

	public boolean isUserLocalCahe() {
		return userLocalCahe;
	}

	public void setUserLocalCahe(boolean userLocalCahe) {
		this.userLocalCahe = userLocalCahe;
	}

	public String getDownloadFileName() {
		return downloadFileName;
	}

	public void setDownloadFileName(String downloadFileName) {
		this.downloadFileName = downloadFileName;
		this.downloadFileNameTemp = downloadFileName + TEMP_FILE_SUFFIX;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public OnBufferingUpdateListener getBufferingUpdateListener() {
		return bufferingUpdateListener;
	}

	public void setBufferingUpdateListener(OnBufferingUpdateListener bufferingUpdateListener) {
		this.bufferingUpdateListener = bufferingUpdateListener;
	}

	public OnPreparedListener getPreparedListener() {
		return preparedListener;
	}

	public void setPreparedListener(OnPreparedListener preparedListener) {
		this.preparedListener = preparedListener;
	}

	public OnBufferingOverListener getBufferingOverListener() {
		return bufferingOverListener;
	}

	public void setBufferingOverListener(OnBufferingOverListener bufferingOverListener) {
		this.bufferingOverListener = bufferingOverListener;
	}

	/**
	 * >>>>>>>>>>网络音乐缓存ok通知接口
	 * 
	 * @author wangxin
	 * 
	 */
	public interface OnPreparedListener {
		/**
		 * 
		 * @param mp
		 */
		public void onPrepared(MediaPlayer mp);
	}

	/**
	 * >>>>>>>>>>音乐缓存进度更接口
	 * 
	 * @author wangxin
	 * 
	 */
	public interface OnBufferingUpdateListener {
		/**
		 * 
		 * @param mp
		 * @param percent
		 */
		public void onBufferingUpdate(MediaPlayer mp, int percent);
	}

	/**
	 * >>>>>>>>>>音乐缓存进度更接口(文件下载完成)
	 * 
	 * @author wangxin
	 * 
	 */
	public interface OnBufferingOverListener {

		/**
		 * 
		 * @param mp
		 * @param percent
		 * @param path
		 * @param localfile
		 *            true:本地文件直接加载 ，false : 下载文件
		 */
		public void onBufferingUpdate(MediaPlayer mp, int percent, String path, boolean localfile);
	}

	public enum PlayStatus {
		Idel, // 当一个MediaPlayer对象被刚刚用new操作符创建或是调用了reset()方法后，它就处于Idle状态，如果成功调用了重载的create()方法，那么这些对象已经是Prepare状
		Initialized, // MediaPlayer.setDataSource方法调用之后进入初始化完成
		Preparing, // mediaPlayer.prepareAsync();
		Prepared, // mediaPlayer.prepare();
		Started, // mediaPlayer.start();
		Paused, // mediaPlayer.pause();
		Stoped, // mediaPlayer.stop();
		End, // mediaPlayer.release();
		Error, // error
		PlaybackCompleted
		// mediaPlayer.seekTo();
	}

	private class DownloadAudioThread extends Thread {

		// >>>>>>>>>>下载路径
		private String mediaUrl;
		// >>>>>>>>>是否取消下载true : 取消下载
		private boolean isInterrupted = false;
		// >>>>>>>>是否下载完成标记
		private boolean downloadOver = false;
		// >>>>>>>>>目前总体缓存进度
		private long hasReadKBlength = 0l;

		public DownloadAudioThread(String mediaUrl) {
			this.mediaUrl = mediaUrl;
		}

		public void interrup() {
			Log.i(TAG, "DownloadAudioThread:interrup>>>ID=" + this.getId() + ";mediaUrl = " + mediaUrl);
			this.isInterrupted = true;
		}

		public boolean isDownloadOver() {
			return downloadOver;
		}

		private void downloadOver() {
			this.downloadOver = true;
		}

		/**
		 * 是否使用本地缓存文件
		 * 
		 * @return
		 */
		private boolean useLocalCacheFullfile() {
			// >>>>>>>>>判断实体文件是否存在
			if (isUserLocalCahe()) {
				File downloadOverFile = new File(downloadPath, downloadFileName);
				if (downloadOverFile.exists()) {
					downloadOver();
					Log.i(TAG, "isUserLocalCahe==true");
					localMediaBuffering(downloadOverFile.getAbsolutePath(), true);
					return true;

				}
			}
			return false;
		}

		// >>>>>>>通知下载ok，播放本地文件
		private void localMediaBuffering(String filePath, boolean localfile) {
			Log.i(TAG, "localMediaBuffering>>>>ThreadID = " + getId() + ";isInterrupted = " + isInterrupted);
			// >>>>>>>>下载完毕，并且没有取消下载
			if (bufferingOverListener != null && !isInterrupted)
				bufferingOverListener.onBufferingUpdate(MusicMediaPlayer.this, 100, filePath, localfile);
		}

		private void preparedNotify(long kbRead) {
			// >>>>>>>>判断是否满足缓冲进度
			if (kbRead / 1000 >= INTIAL_KB_BUFFER && !isPrepared) {
				if (preparedListener != null) {
					isPrepared = true;
					// >>>>>>>是否播放标记 ,是否停止线程
					Log.i(TAG, "preparedNotify>>>>ThreadID = " + getId() + ";isInterrupted = "
							+ isInterrupted);
					if (!isInterrupted)
						preparedListener.onPrepared(MusicMediaPlayer.this);
				}
			}
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			// >>>>>>>打印线程的ID和下载音乐的URL
			Log.i(TAG, "DownloadAudioThread:run>>>>ID=" + this.getId() + ";mediaUrl = " + mediaUrl);
			try {

				// >>>>>>>是否使用本地缓存文件
				if (useLocalCacheFullfile())
					return;

				// >>>>>>>>创建目录
				File path = new File(downloadPath);
				if (!path.exists()) {
					path.mkdirs();
				}

				// >>>>>>>>下载文件
				URL url = null;
				try {
					url = new URL(mediaUrl);
				} catch (MalformedURLException e1) {
					Log.e(TAG, "==>MalformedURLException");
					return;
				}
				URLConnection cn = url.openConnection();

				// >>>>>>>>>如果使用本机文件，判断是否有文件存在，实现断点续传功能
				// >>>>>>>>>如果没有本地文件，进行文件下载下载音乐到临时文件 (文件名由文件名加上后缀.dat格式)
				downloadingMediaFile = new File(downloadPath, downloadFileNameTemp);

				mediaLengthInKb = cn.getContentLength();
				// >>>>>>判断文件是否存在
				if (downloadingMediaFile.exists()) {
					downloadingMediaFile.delete(); // 如果下载完成则删除
				}

				cn.connect();

				InputStream stream = cn.getInputStream();
				// >>>>>>>网络音乐地址不正确，无法获取流
				if (stream == null) {
					Log.d(TAG, "Unable to create InputStream for mediaUrl:" + mediaUrl);
				}

				// >>>>>>>>>解析文件流
				FileOutputStream out = new FileOutputStream(downloadingMediaFile);
				// >>>>>>每次写入字节流
				byte buf[] = new byte[16384];
				// >>>>>>>已经下载字节
				int totalBytesRead = 0;
				int len;
				// >>>>>>是否还有字节流传输 同时 判断是否暂停下载，暂停播放
				while ((len = stream.read(buf)) != -1 && !isInterrupted) {
					out.write(buf, 0, len);
					totalBytesRead += len;

					// >>>>>>>>>缓存ok通知(如果没有之前下载的文件大小不足够播放 没有达到默认的准备字节数)
					preparedNotify(totalBytesRead + hasReadKBlength);
					// >>>>>>>>通知下载量更新
					if (bufferingUpdateListener != null && !isInterrupted) {
						// >>>>>>>计算下载的具体比例 ,按照MAX=100来计算
						float loadProgress = ((float) totalBytesRead / (float) mediaLengthInKb) * 100;
						bufferingUpdateListener.onBufferingUpdate(MusicMediaPlayer.this, (int) loadProgress);
					}
				}
				stream.close();

				// >>>>>>>下载完毕
				downloadOver();
				if (!isInterrupted)
					// >>>>>>>通知下载结束
					downLoadFileSucess();
			} catch (IOException e) {

			}
		}

		// >>>>>>>>音乐文件下载完毕，通知播放
		private void downLoadFileSucess() {
			File downloadOverFile = new File(downloadPath, downloadFileName);
			downloadingMediaFile.renameTo(downloadOverFile);
			localMediaBuffering(downloadOverFile.getAbsolutePath(), false);
		}
	}
}
