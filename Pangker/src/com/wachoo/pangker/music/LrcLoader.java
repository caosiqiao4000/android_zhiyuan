package com.wachoo.pangker.music;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;

import android.os.AsyncTask;
import android.util.Log;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.util.Util;

public class LrcLoader {

	private String TAG = "LrcLoader";// log tag
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	// private static final String URL_LRC =
	// "http://qqmusic.qq.com/fcgi-bin/qm_getLyricId.fcg?name={0}&singer={1}&from=qqplayer";
	private static final String URL_LRC = "http://shopcgi.qqmusic.qq.com/fcgi-bin/shopsearch.fcg?value={0}&artist={1}&type=qry_song&out=xml&page_no=1&page_record_num=10";
	private static final String URL_SONG = "http://music.qq.com/miniportal/static/lyric/{0}/{1}.xml";
	private static final Pattern pattern = Pattern.compile("(?<=\\[).*?(?=\\])");
	private static final String encodout = "gb2312";

	// 搜索歌词
	public void searchLrc(String name, String singer, OnLrcResponse onLrcResponse) {
		try {
			String lrcurl = MessageFormat.format(URL_LRC, URLEncoder.encode(name, encodout),
					URLEncoder.encode(singer, encodout));
			Log.d("Lrc", lrcurl);
			new SearchTask(onLrcResponse).execute(lrcurl);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class SearchTask extends AsyncTask<String, Void, LrcResponse> {

		private OnLrcResponse onLrcResponse;

		public SearchTask(OnLrcResponse onLrcResponse) {
			super();
			this.onLrcResponse = onLrcResponse;
		}

		@Override
		protected LrcResponse doInBackground(String... params) {
			// TODO Auto-generated method stub
			String dxml = httpGet(params[0]);
			if(dxml == null){
				return null;
			}
			return parseLrc(dxml);
		}

		@Override
		protected void onPostExecute(LrcResponse result) {
			// TODO Auto-generated method stub
			onLrcResponse.onLrcResponse(result);
		}
	};

	private static LrcResponse parseLrc(String xmldoc) {
		LrcResponse lrcResponse = new LrcResponse();
		SAXReader reader = new SAXReader();
		Document doc = null;
		StringReader read = new StringReader(xmldoc);
		InputSource source = new InputSource(read);
		try {
			doc = reader.read(source);
			if (doc != null) {
				Element root = doc.getRootElement();
				if (root != null) {
					Element one = root.element("songlist");
					if (one != null) {
						List<Element> songs = one.elements("song");
						if (songs != null) {
							System.out.println("总数--" + songs.size());
							lrcResponse.setCount(songs.size());
							if (songs.size() > 0) {
								List<Lrc> lrcs = new ArrayList<Lrc>();
								for (Element element : songs) {
									Lrc lrc = new Lrc();
									lrc.setLrcid(element.elementText("xqusic_id"));
									lrc.setName(element.elementText("xsong_name"));
									lrc.setSinger(element.elementText("xsinger_name"));
									lrcs.add(lrc);
								}
								lrcResponse.setLrcList(lrcs);
							}
						}
					}
				}
			}

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			return lrcResponse;
		} finally {
			if (source != null) {
				try {
					read.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
				}
			}
		}
		return lrcResponse;
	}

	// 歌词加载，默认先加载本地的Lrc文件，如果本地没有，或者让用户进行搜索，或者默认到网络加载第一条。
	public void lrcLoader(String name, String singer, OnLrcLoader onLrcLoader) {
		if (Util.isEmpty(name)) {
			onLrcLoader.onLrcLoaded(false, null);
			return;
		}

		Log.i(TAG, "musicname=" + name + ";singer=" + singer);
		String lrcpath = PangkerConstant.DIR_MUSIC + "/" + name + ".lrc";
		File lrcfile = new File(lrcpath);
		if (lrcfile.exists()) {
			Log.i(TAG, "lrcfile.exists()=true：歌词存在本地");
			new ParseTask(onLrcLoader).execute(lrcpath, name);
		} else {
			Log.i(TAG, "lrcfile.exists()=false!");
			new LrcTask(name, singer, onLrcLoader).execute();
		}
	}

	// 异步解析任务
	class ParseTask extends AsyncTask<String, Void, Lyric> {

		private OnLrcLoader onLrcLoader;

		public ParseTask(OnLrcLoader onLrcLoader) {
			super();
			this.onLrcLoader = onLrcLoader;
		}

		@Override
		protected Lyric doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				String filePath = params[0];
				String musicName = params[1];
				// >>>>>>>返回文件
				return new Lyric(new File(filePath), musicName);
			} catch (Exception e) {
				// TODO: handle exception
				return null;
			}

		}

		@Override
		protected void onPostExecute(Lyric result) {
			// TODO Auto-generated method stub
			if (result == null)
				onLrcLoader.onLrcLoaded(false, result);
			else
				onLrcLoader.onLrcLoaded(true, result);
		}
	}

	// 根据歌词和歌手搜索到Lrc列表，如果有的话取第一条Lrc，并进行歌词下载和解析
	class LrcTask extends AsyncTask<Void, Void, Lyric> {

		private String name;
		private String singer;
		private OnLrcLoader onLrcLoader;

		public LrcTask(String name, String singer, OnLrcLoader onLrcLoader) {
			super();
			this.name = name;
			if (singer == null) {
				singer = "";
			}
			this.singer = singer;
			this.onLrcLoader = onLrcLoader;
		}

		@Override
		protected Lyric doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String lrcurl = null;
			try {
				lrcurl = MessageFormat.format(URL_LRC, URLEncoder.encode(name, encodout),
						URLEncoder.encode(singer, encodout));

				String dxml = httpGet(lrcurl);
				if(dxml == null){
					return null;
				}
				LrcResponse response = parseLrc(dxml);
				if (response.getCount() <= 0) {
					return null;
				}
				Lrc lrc = response.getLrcList().get(0);
				File lrcDir = new File(PangkerConstant.DIR_MUSIC);
				if (!lrcDir.exists()) {
					lrcDir.mkdirs();
				}
				String lrcpath = PangkerConstant.DIR_MUSIC + "/" + name + ".lrc";
				String lrcid = lrc.getLrcid();
				String id = lrcid.substring(lrcid.length() - 2);
				String songurl = MessageFormat.format(URL_SONG, id, lrcid);
				// >>>>>>>下载歌词
				if (loaderParseLrc(songurl, lrcpath)) {
					return new Lyric(new File(lrcpath), "");
				} else {
					return null;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return null;
			}
		}

		@Override
		protected void onPostExecute(Lyric result) {
			// TODO Auto-generated method stub
			if (result == null)
				onLrcLoader.onLrcLoaded(false, result);
			else
				onLrcLoader.onLrcLoaded(true, result);
		}
	}

	/**
	 * 根据歌词结果到QQ搜索歌词内容
	 * 
	 * @param lrc
	 * @param onLrcLoader
	 */
	public void lrcLoader(Lrc lrc, OnLrcLoader onLrcLoader) {
		String lrcid = lrc.getLrcid();
		String id = lrcid.substring(lrcid.length() - 2);
		String songurl = MessageFormat.format(URL_SONG, id, lrcid);
		String lrcpath = PangkerConstant.DIR_MUSIC + "/" + lrc.getName() + ".lrc";

		new LoaderTask(songurl, lrcpath, onLrcLoader).execute();
	}

	/**
	 * 搜索到歌词之后，先保存在SD卡，然后解析
	 * 
	 * @author zxx
	 */
	class LoaderTask extends AsyncTask<Void, Void, Lyric> {

		private OnLrcLoader onLrcLoader;
		private String lrcpath;
		private String songurl;

		public LoaderTask(String songurl, String lrcpath, OnLrcLoader onLrcLoader) {
			this.onLrcLoader = onLrcLoader;
			this.lrcpath = lrcpath;
			this.songurl = songurl;
		}

		@Override
		protected Lyric doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {

				if (loaderParseLrc(songurl, lrcpath)) {
					return new Lyric(new File(lrcpath), "");
				} else {
					return null;
				}
			} catch (Exception e) {
				// TODO: handle exception
				return null;
			}
		}

		protected void onPostExecute(Lyric result) {
			if (result == null)
				onLrcLoader.onLrcLoaded(false, result);
			else
				onLrcLoader.onLrcLoaded(true, result);
		};

	}

	/**
	 * 下载歌词方法
	 * 
	 * @param songurl
	 *            歌词的httpURL
	 * @param lrcSavePath
	 *            歌词的本地保存路径
	 * @return
	 */
	private boolean loaderParseLrc(String songurl, String lrcSavePath) {
		InputStream inputStream = null;
		FileOutputStream fos = null;
		BufferedReader br = null;
		try {
			URL indexUrl = new URL(songurl);
			HttpURLConnection httpUrlConnection = (HttpURLConnection) indexUrl.openConnection();
			httpUrlConnection.setDoOutput(true);
			httpUrlConnection.setConnectTimeout(5000);
			httpUrlConnection.setReadTimeout(5000);
			httpUrlConnection.connect();
			inputStream = httpUrlConnection.getInputStream();

			// 如果Sd卡存在，先保存在解析，否则直接解析

			File lrcFile = new File(lrcSavePath);
			if (!lrcFile.exists()) {
				lrcFile.createNewFile();
			}
			fos = new FileOutputStream(lrcFile);
			byte[] buf = new byte[1204];
			int len = 0;
			while ((len = inputStream.read(buf)) != -1) {
				fos.write(buf, 0, len);
			}
			fos.flush();
			// 保存之后对文件进行解析
			return true;

		} catch (Exception e) {
			logger.error(TAG, e);
			try {
				if (inputStream != null) {
					inputStream.close();
					inputStream = null;
				}

				if (fos != null) {
					fos.flush();
					fos.close();
					fos = null;
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				logger.error(TAG, e1);
			}
			return false;
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
					inputStream = null;
				}
				if (br != null) {
					br.close();
					br = null;
				}
				if (fos != null) {
					fos.flush();
					fos.close();
					fos = null;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(TAG, e);
			}
		}
	}

	// /**
	// * 解析本地Lrc文件
	// *
	// * @param filepath
	// * @param lrcCallback
	// */
	// private List<LrcSentence> readLrc(String filepath) {
	// if (Util.IsEmpty(filepath) || !Util.isExitFileSD(filepath)) {
	// return null;
	// }
	// Log.d("SearchLRC", "s = loacl");
	// List<LrcSentence> lrcContents = new ArrayList<LrcSentence>();
	// BufferedReader reader = null;
	// String temp = null;
	// FileInputStream ins = null;
	//
	// BufferedInputStream bis = null;
	// try {
	// ins = new FileInputStream(filepath);
	// bis = new BufferedInputStream(ins);
	//
	// bis.mark(4);
	// byte[] first3bytes = new byte[3];
	// bis.read(first3bytes);
	// bis.reset();
	//
	// if (first3bytes[0] == (byte) 0xEF && first3bytes[1] == (byte) 0xBB
	// && first3bytes[2] == (byte) 0xBF) {
	// // utf-8
	// reader = new BufferedReader(new InputStreamReader(bis, "utf-8"));
	// } else if (first3bytes[0] == (byte) 0xFF && first3bytes[1] == (byte)
	// 0xFE) {
	// reader = new BufferedReader(new InputStreamReader(bis, "unicode"));
	// } else if (first3bytes[0] == (byte) 0xFE && first3bytes[1] == (byte)
	// 0xFF) {
	// reader = new BufferedReader(new InputStreamReader(bis, "utf-16be"));
	// } else if (first3bytes[0] == (byte) 0xFF && first3bytes[1] == (byte)
	// 0xFF) {
	// reader = new BufferedReader(new InputStreamReader(bis, "utf-16le"));
	// } else {
	// reader = new BufferedReader(new InputStreamReader(bis, "GBK"));
	// }
	//
	// while ((temp = reader.readLine()) != null) {
	// Log.d("SearchLRC", temp);
	// LrcSentence lrcSentence = parseLine(temp.trim());
	// if (lrcSentence != null) {
	// lrcContents.add(lrcSentence);
	// }
	// }
	// if (lrcContents.size() > 0) {
	// return lrcContents;
	// } else {
	// return null;
	// }
	// } catch (IOException e) {
	// return null;
	// } finally {
	// try {
	// ins.close();
	// reader.close();
	// } catch (IOException e) {
	// logger.error(TAG, e);
	// }
	// }
	// }
	//
	// /**
	// * 解析一行歌词
	// *
	// * @param line
	// * @return
	// */
	// private LrcSentence parseLine(String line) {
	// LrcSentence lrcSentence = null;
	// try {
	// if (line.equals("")) {
	// return null;
	// }
	// if (line.indexOf("http:") > 0) {
	// line = line.substring(0, line.indexOf("http:"));
	// }
	// Matcher matcher = pattern.matcher(line);
	// List<String> temp = new ArrayList<String>();
	// int lastIndex = -1;
	// int lastLength = -1;
	// while (matcher.find()) {
	// String s = matcher.group();
	// int index = line.indexOf("[" + s + "]");
	// if (lastIndex != -1 && index - lastIndex > lastLength + 2) {
	// String content = line.substring(lastIndex + lastLength + 2, index);
	// for (String str : temp) {
	// int t = parseTime(str);
	// if (t != -1) {
	// lrcSentence = new LrcSentence(t, content);
	// }
	// }
	// temp.clear();
	// }
	// temp.add(s);
	// lastIndex = index;
	// lastLength = s.length();
	// }
	// if (temp.isEmpty()) {
	// return null;
	// }
	// int length = lastLength + 2 + lastIndex;
	// String content = line.substring(length > line.length() ? line.length() :
	// length);
	// for (String s : temp) {
	// int t = parseTime(s);
	// if (t != -1) {
	// lrcSentence = new LrcSentence(t, content);
	// }
	// }
	//
	// } catch (Exception e) {
	// // TODO: handle exception
	// }
	// return lrcSentence;
	// }
	//
	// // 解析时间
	// private int parseTime(String time) {
	// String[] ss = time.split("\\:|\\.");
	// if (ss.length < 2) {
	// return -1;
	// } else if (ss.length == 2) {
	// try {
	// int min = Integer.parseInt(ss[0]);
	// int sec = Integer.parseInt(ss[1]);
	// if (min < 0 || sec < 0 || sec >= 60) {
	// throw new RuntimeException("数字不合法!");
	// }
	// return (min * 60 + sec) * 1000;
	// } catch (Exception e) {
	// logger.error(TAG, e);
	// return -1;
	// }
	// } else if (ss.length == 3) {
	// try {
	// int min = Integer.parseInt(ss[0]);
	// int sec = Integer.parseInt(ss[1]);
	// int mm = Integer.parseInt(ss[2]);
	// if (min < 0 || sec < 0 || sec >= 60 || mm < 0 || mm > 99) {
	// throw new RuntimeException("数字不合法!");
	// }
	// return (min * 60 + sec) * 1000 + mm * 10;
	// } catch (Exception e) {
	// logger.error(TAG, e);
	// return -1;
	// }
	// } else {
	// return -1;
	// }
	// }

	public String httpGet(String url) {
		HttpGet get = new HttpGet(url.toString());
		String strResult = "";
		try {
			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);

			HttpResponse httpResponse = null;
			httpResponse = httpClient.execute(get);

			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				strResult = EntityUtils.toString(httpResponse.getEntity(), encodout);
			}
		} catch (Exception e) {
			return null;
		}
		return strResult;
	}

	// private StringBuffer getResponse(String url) {
	// StringBuffer sb = new StringBuffer();
	// InputStreamReader isr = null;
	// BufferedReader br = null;
	// HttpURLConnection httpUrlConnection = null;
	// try {
	// URL indexUrl = new URL(url);
	// httpUrlConnection = (HttpURLConnection) indexUrl.openConnection();
	// httpUrlConnection.setDoOutput(true);
	// httpUrlConnection.setConnectTimeout(5000);
	// httpUrlConnection.setReadTimeout(5000);
	// httpUrlConnection.connect();
	//
	// isr = new InputStreamReader(httpUrlConnection.getInputStream(),
	// encodout);
	// br = new BufferedReader(isr);
	// String s;
	// while ((s = br.readLine()) != null) {
	// sb.append(s + "\r\n");
	// }
	// return sb;
	// } catch (Exception e) {
	// logger.error(TAG, e);
	// return sb;
	// } finally {
	// try {
	// if (isr != null) {
	// isr.close();
	// }
	// if (br != null) {
	// br.close();
	// }
	// httpUrlConnection.disconnect();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// logger.error(TAG, e);
	// }
	// }
	// }

	public interface OnLrcResponse {
		public void onLrcResponse(LrcResponse lrcResponse);
	}

	public interface OnLrcLoader {
		public void onLrcLoaded(boolean success, Lyric lyric);
	}

}
