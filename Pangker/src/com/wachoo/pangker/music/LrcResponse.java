package com.wachoo.pangker.music;

import java.util.List;

public class LrcResponse {

	private int count;
	private List<Lrc> lrcList;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<Lrc> getLrcList() {
		return lrcList;
	}
	public void setLrcList(List<Lrc> lrcList) {
		this.lrcList = lrcList;
	}
	
}
