package com.wachoo.pangker.music;

import java.util.EventObject;

import com.wachoo.pangker.server.response.MusicInfo;

@SuppressWarnings("serial")
public class MusicPlayerEvent extends EventObject {
	private MusicInfo info;

	public MusicPlayerEvent(Object source, MusicInfo info) {
		super(source);
		this.info = info;
	}

	public MusicInfo getInfo() {
		return info;
	}

	public void setInfo(MusicInfo info) {
		this.info = info;
	}

}
