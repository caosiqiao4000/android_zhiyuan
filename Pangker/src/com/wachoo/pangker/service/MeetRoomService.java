package com.wachoo.pangker.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.google.gson.Gson;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.activity.group.GroupMapActivity;
import com.wachoo.pangker.audio.AudioPlayManager;
import com.wachoo.pangker.audio.AudioRecordThread;
import com.wachoo.pangker.audio.HelloThread;
import com.wachoo.pangker.audio.HelloThread.OnHelloListener;
import com.wachoo.pangker.chat.IGroupSPLoginManager;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.group.CP2SP_Stream;
import com.wachoo.pangker.group.CP2SP_Stream_Stop;
import com.wachoo.pangker.group.CRoom2SP_LoginOut_Req;
import com.wachoo.pangker.group.CRoom2SP_SpeechList_Req;
import com.wachoo.pangker.group.CRoom2SP_Speech_Req;
import com.wachoo.pangker.group.CRoom2SP_Status_Report;
import com.wachoo.pangker.group.CRoom2SP_UserList_Req;
import com.wachoo.pangker.group.ChatRecordInfo;
import com.wachoo.pangker.group.Client2SP_NotRead_Req;
import com.wachoo.pangker.group.Cmanager2SP_Hello;
import com.wachoo.pangker.group.Cmanager2SP_Login_Req;
import com.wachoo.pangker.group.IRoomMessageSender;
import com.wachoo.pangker.group.P2PFarther;
import com.wachoo.pangker.group.P2PInterface;
import com.wachoo.pangker.group.P2PUserInfo;
import com.wachoo.pangker.group.SP2CRoom_Conf_Resp;
import com.wachoo.pangker.group.SP2CRoom_SpeechListStatus_Notify;
import com.wachoo.pangker.group.SP2CRoom_SpeechList_Rsep;
import com.wachoo.pangker.group.SP2CRoom_Speech_Notify;
import com.wachoo.pangker.group.SP2CRoom_Speech_Resp;
import com.wachoo.pangker.group.SP2CRoom_Status_Notify;
import com.wachoo.pangker.group.SP2CRoom_Status_Resp;
import com.wachoo.pangker.group.SP2Client_Hello_ACK;
import com.wachoo.pangker.group.SP2Client_NotRead_Resp;
import com.wachoo.pangker.group.SP2Cmanager_Login_Resp;
import com.wachoo.pangker.group.SP2Croom_UserList_Resp;
import com.wachoo.pangker.group.SP2File_Upload_Resp;
import com.wachoo.pangker.group.SP2NextHop_Stream;
import com.wachoo.pangker.receiver.NetChangeReceiver;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.socket.CRecvData;
import com.wachoo.pangker.socket.CSock;
import com.wachoo.pangker.socket.JavaSock;
import com.wachoo.pangker.socket.ParseMsgInface;
import com.wachoo.pangker.util.ConnectivityHelper;
import com.wachoo.pangker.util.MeetRoomUtil;
import com.wachoo.pangker.util.Util;
import com.wachoo.pangker.util.MeetRoomUtil.SpeechAllow;

/**
 * 群组服务
 * 
 * @author 王鑫
 * @createtime 2012-3-28
 *             群组要进行语音屏蔽功能，具体见application.getSysMark()；用来判断是否接受语音，暂时没有。
 */
public class MeetRoomService extends Service implements IGroupSPLoginManager,
		IRoomMessageSender, ParseMsgInface, P2PInterface {

	private String TAG = "MeetRoomService";// log tag
	private final Logger logger = LoggerFactory.getLogger();

	public static final String MEETING_ROOM_INTENT = "MEETING_ROOM_INTENT";// 意图
	public static final int DEFAULT_CODE = -1;// 断开群组连接

	private P2PFarther farther = new P2PFarther();
	protected CSock conn_trust_sock;
	protected CSock conn_notrust_sock;
	private JavaSock javaSock;

	// >>>>>>>语音播放线程
	// private AudioPlayThread audioPlayThread;
	private AudioPlayManager audioPlayManager;
	// >>>>>>>>语音采集线程
	private AudioRecordThread recordThread;
	private NetCheckThread netCheckThread;
	private NetChangeReceiver mChangeReceiver;
	private HelloThread mHelloThread;
	private Location roomLocation;

	private PangkerApplication application;
	private String myuserid;

	private byte[] sendAmr;
	private byte[] receiveAmr;// 数据流

	private long NoReadEndTime;
	// >>>>>>发送文字流
	private String chatTxtMessage;

	public CSock getConn_trust_sock() {
		return conn_trust_sock;
	}

	public CSock getConn_notrust_sock() {
		return conn_notrust_sock;
	}
	
	public void setConn_trust_sock(CSock conn_trust_sock) {
		this.conn_trust_sock = conn_trust_sock;
	}

	public void setConn_notrust_sock(CSock conn_notrust_sock) {
		this.conn_notrust_sock = conn_notrust_sock;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.i(TAG, "onCreate MeetService!");
		application = (PangkerApplication) getApplicationContext();
		myuserid = application.getMyUserID();
		application.setmSpLoginManager(this);
		// >>>>>>>>>>>>eppllID只能创建一次，不管进入与否
		javaSock = new JavaSock();
		eppllID = javaSock.JavaSock_EpollCreate(MeetRoomUtil.TYPE_SOCK_TYPE);
		// 创建一个客户端
		javaSock.JavaSock_ServListen(eppllID, MeetRoomUtil.getIp(), 0, this, MeetRoomUtil.TYPE_SOCK_TYPE);
	    //非可靠的监听
		javaSock.JavaSock_Session_ServListen(MeetRoomUtil.getIp(), (short)0, this);
		mChangeReceiver = new NetChangeReceiver();
		audioPlayManager = new AudioPlayManager();
	}

	// >>>>>>>>>判断是否在录音，如果录音停止
	private void stopRecordThread() {
		// TODO Auto-generated method stub
		if (recordThread != null) {
			recordThread.stopRecoding();
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent == null) {
			return START_NOT_STICKY;
		}
		int sendId = intent.getIntExtra(MEETING_ROOM_INTENT, -1);
		switch (sendId) {
		// >>>>>>>>>>>>>登录群组 LB
		case Configuration.CMANAGER2SP_LOGIN_REQ_FLAG:
			Log.d("Login", "startHello()");
			startHello();
			break;
		// >>>>>>>>>>>>>加入群组 wangxin
		case Configuration.CROOM2SP_STATUS_REPORT_MEMBER_ADD_FLAG:
			Log.i("login", ">>>>>加入群组申请");
			getAppCurrunGroup().clearData();
			sendGroupBroadcast(PangkerConstant.ACTION_GROUPLOGIN);
			sendPack(Configuration.CROOM2SP_STATUS_REPORT_MEMBER_ADD_FLAG);
			break;

		// >>>>>>>>>>退出群组wangxin
		case Configuration.CROOM2SP_STATUS_REPORT_MEMBER_DEL_FLAG:
			Log.i(TAG, ">>>>>退出群组申请");
			// >>>>>>>>>判断是否在录音，如果录音停止
			stopRecordThread();
			sendPack(Configuration.CROOM2SP_STATUS_REPORT_MEMBER_DEL_FLAG);
			getAppCurrunGroup().leaveGroup();

			sendGroupBroadcast(PangkerConstant.ACTION_GROUPLOGOUT);
			break;

		case Configuration.START_AUDIO_FLAG:
			Log.i(TAG, ">>>>>开始发送采集的语音数据");
			sendAudio(application.getCurrunGroup().isAdmin(myuserid));
			break;
		case Configuration.STOP_AUDIO_FLAG:
			Log.i(TAG, ">>>>>停止发言（语音）申请");
			stopAudioRecode();
			break;
		case Configuration.CMANAGER2SP_LOGINOUT_REQ_FLAG:
			logout(true, null);
			break;
		// >>>>>>>>>文字信息 + 图片信息 （文字信息：文字内容 ///图片信息：图片下载的key httpURL的name值）
		case Configuration.CP2SP_STREAM_TEXT_FLAG:
		case Configuration.CP2SP_STREAM_PIC_FLAG:
			chatTxtMessage = intent.getStringExtra(MeetRoomUtil.MEETROOM_CHAT_TXT_MESSAGE_CONTENT);
			sendPack(sendId);
			break;
		case Configuration.CP2SP_STREAM_LOCATION_FLAG:
			roomLocation = (Location) intent.getSerializableExtra(MeetRoomUtil.MEETROOM_CHAT_LOCATION_CONTENT);
			sendPack(sendId);
			// >>>>>>>>>>暂停语音功能
		case Configuration.CROOM2SP_STATUS_RADIO_STOP_FLAG:
			stopAudioRecode();
			break;
		// >>>>>>>>>>开启音乐功能
		case Configuration.CROOM2SP_STATUS_RADIO_PLAY_FLAG:
			break;
		// >>>>>>>>>>定位意图
		case Configuration.CROOM2SP_STATUS_REPORT_LOCAYION_ADD_FLAG:
			roomLocation = (Location) intent.getSerializableExtra("Location");
			sendPack(sendId);
			break;
		case Configuration.CROOM2SP_STATUS_REPORT_MEMBER_UPD_FLAG:
			Log.e(TAG, "CROOM2SP_STATUS_REPORT_MEMBER_UPD_FLAG>>>>>>>>"
					+ sendId);
			sendPack(sendId);
			break;
		case Configuration.CROOM2SP_SPEECH_REQ_FLAG_DOWN:
			stopRecordThread();
			sendPack(sendId);
			break;
		default:
			if (sendId > 0) {
				sendPack(sendId);
			}
			break;
		}
		return START_NOT_STICKY;
	}

	/*******************************************************************************************************************/
	NetChangeReceiver.OnNetChangeListener onNetChangeListener = new NetChangeReceiver.OnNetChangeListener() {
		@Override
		public void onNetStatus(boolean connect) {
			// TODO Auto-generated method stub
			if (connect) {
				stopHello();
				// >>>>>>>>>业务处理
				Log.d("Login", "Net connect change!==>" + Util.getSysNowTime());
				if (application.getCurrunGroup() == null) {
					return;
				}
				// >>>>>设置已经登出
				application.setGroupSPLogged(false);

				callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_ReLogin, 0, 0, "正在连接服务器...");
				Log.d("Login", "connect change!==>Login:" + Util.getSysNowTime());
				startHello();
			} else {
				logout(false, "网络不可用,无法连接服务器!");
			}

		}
	};

	private OnHelloListener mOnHelloListener = new OnHelloListener() {
		@Override
		public void onLoginoutListener(boolean isLogin) {
			// TODO Auto-generated method stub
			if (isLogin) {
				login();
			} else {
				logout(false, "您与服务器断开连接!");
			}
		}

		@Override
		public int onHelloSendListener() {
			// TODO Auto-generated method stub
			Cmanager2SP_Hello chello = new Cmanager2SP_Hello(getP2PUserInfo().getUID());
			byte[] helloBytes = farther.getCmanager2SP_Hello(chello);
			return javaSock.JavaSock_Send(getConn_trust_sock(), helloBytes, helloBytes.length);
		}
	};

	/*******************************************************************************************************************/
	private boolean isHelloResping() {
		if (mHelloThread != null && mHelloThread.isHasHelloResp()) {
			return true;
		}
		return false;
	}

	private void stopHello() {
		if (mHelloThread != null) {
			mHelloThread.setHasHelloResp(false);
			mHelloThread.stopHelloing();
		}
	}

	// 连接参数初始化,5秒之内不能执行两次
	private synchronized void startHello() {
		// TODO Auto-generated method stub
		stopHello();
		mHelloThread = new HelloThread(mOnHelloListener);
		mHelloThread.start();

	}

	// >>>>>>>>>>>线程接收句柄
	int eppllID = 0;

	// 连接group server
	private synchronized boolean connConnect() {
		Log.d("Login", "connConnect");
		if (isHelloResping()) {
			return true;
		}
		try {
			// >>>>>>>>>如果老的连接存在，通知JNI删除老的连接
			if (getConn_trust_sock() != null) {
				Log.d("Login", "JavaSock_DelSock 关闭sock:");
				javaSock.JavaSock_DelSock(eppllID, getConn_trust_sock());
				Log.d("Login", "JavaSock_close 关闭sock:");
				javaSock.JavaSock_Close(getConn_trust_sock());
				if (mHelloThread != null) {
					mHelloThread.setConnected(false);
				}
				setConn_notrust_sock(null);
				setConn_trust_sock(null);
				Log.d("Login", "setConn_sock 关闭sock:" + Util.getSysNowTime());
			}
			// >>>>>>>>创建一个连接
			Log.i("Login", "newconn_sock IP->" + MeetRoomUtil.getIp());
			Log.i("Login", "newconn_sock PServiceIp->" + MeetRoomUtil.getP2PServiceIp(this));
			Log.i("Login", "newconn_sock GroupSpPort->" + MeetRoomUtil.getGroupSpTUPPort(this));
			Log.i("Login", "newconn_sock TYPE_SOCK_TYPE->" + MeetRoomUtil.TYPE_SOCK_TYPE);
			
			CSock newconn_sock = javaSock.JavaSock_ConnectTo(MeetRoomUtil.getIp(), MeetRoomUtil.getP2PServiceIp(this),
					MeetRoomUtil.getGroupSpTUPPort(this), MeetRoomUtil.TYPE_SOCK_TYPE);
			// >>>>>>>>创建非可靠的一个连接
			if (newconn_sock == null) {
				Log.i("Login", "newconn_sock is null...");
				return false;
			}
			CSock newSession_sock = javaSock.JavaSock_SessionConnect(MeetRoomUtil.getIp(), MeetRoomUtil.getP2PServiceIp(this), 
					MeetRoomUtil.getGroupSpSessionPort(this), newconn_sock);
			// >>>>>>>判断新创建的sock是否为空
			if (newSession_sock == null) {
				Log.i("Login", "newSession_sock is null...");
				return false;
			} else {
				Log.i("Login", "newconn_sock:" + newconn_sock.toString());
				Log.i("Login", "newSession_sock:" + newSession_sock.toString());
			}
			setConn_trust_sock(newconn_sock);
			setConn_notrust_sock(newSession_sock);
			// >>>>>>>>设置
			javaSock.JavaSock_AddSock(eppllID, newconn_sock);
			Log.i("Login", "JavaSock_AddSock成功:" + Util.getSysNowTime());
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		}
	}

	/**
	 * 判断JNI发包结果 如果NUM = -1 则连接中断，为失效的！！！
	 * 
	 * @param num
	 * @return
	 */
	private boolean judgeSendBySockID(int num, int sendFlag) {
		// >>>>>>>如果NUM = -1 则连接中断，为失效的！！！
		if (num < 0) {
			Log.e(TAG, "发包失败  ..." + num + " |||sendFlag = " + sendFlag);
			mHelloThread.setConnected(false);
			mHelloThread.setHasHelloResp(false);
			// TODO 提示用户 与服务器的连接中断 对发包失败的事件做出回应 ,改变一些状态
			return false;
		}
		return true;
	}

	/**
	 * 登陆结果返回
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:54
	 * @param obj
	 */
	@Override
	public void setSP2Cmanager_Login_Resp(SP2Cmanager_Login_Resp obj) {
		Log.d("Login", "登陆结果返回---fromSP>>>setSP2Cmanager_Login_Resp,time"
				+ Util.getSysNowTime());
		if (obj == null) {
			return;
		}
		if (obj.getError_Code() == 0) {
			if (getAppCurrunGroup() != null && getAppCurrunGroup().getUid() != null) {
				Log.d("Login", "登陆后进入群组,groupsid:" + getAppCurrunGroup().getGroupSid());
				getAppCurrunGroup().clearData();
				// 广播前台登录群组成功
				sendGroupBroadcast(PangkerConstant.ACTION_GROUPLOGIN);
				// 获取群组信息和成员列表
				sendPack(Configuration.CROOM2SP_STATUS_REPORT_MEMBER_ADD_FLAG);
				callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_Login_Resp, 0, 0, null);
				// >>>>>>>>>>..同步groupSP系统时间
				application.setGroupSPTimeDiff(obj.getServerTime() - System.currentTimeMillis());
				// 设置已经登陆 >>>>>>>设置登陆到群组服务器（groupSP）
				application.setGroupSPLogged(true);
				// >>>>>>注册
				mChangeReceiver.registerNetChangeReceiver(MeetRoomService.this, onNetChangeListener);
				//如果用户正在发言，那么登录之后继续发言
				if(getAppCurrunGroup().getSpeechStatus() == SpeechAllow.SPEECHING){
					sendPack(Configuration.CROOM2SP_SPEECH_REQ_FLAG_UP);
				}
			} else {
				Log.i(TAG, "登陆后进入群组,groupsid为空");
			}
		} else {
			Toast.makeText(application, "登录群组失败,请稍后再试!", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * hello包响应
	 */
	@Override
	public void setSP2Client_Hello_ACK(SP2Client_Hello_ACK obj) {

	}

	/**
	 * Gsp返回请求的响应
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:57
	 * @param obj
	 */
	public void setSP2CRoom_Status_Resp(SP2CRoom_Status_Resp obj) {
		if (obj.getError_Code() == 0x0000) {
			// 进入房间：
			if (obj.getMembersOp() == 0x11) {
				Log.i(TAG, "进入房间响应---fromSP>>>setSP2CRoom_Status_Resp,errorcode:" + obj.getError_Code());
				// >>>>>>>>启动语音播放
				audioPlayStart();
				application.setCurrunGroup(getAppCurrunGroup());
				NoReadEndTime = obj.getServerTime();
				// 获取
				mBinder.setOldUserGroupSid(getAppCurrunGroup().getGroupSid());
				mBinder.setOldUserGroupUid(getAppCurrunGroup().getUid());
				Log.i("infos", "第一次使用的未读时间 是  " + NoReadEndTime + " 使用的偏移时间量是= " + application.getGroupSPTimeDiff());

				sendPack(Configuration.CROOM2SP_USERLIST_REQ);
				sendPack(Configuration.CROOM2SP_SPEECHLIST_REQ_FLAG);
				// >>>>>进入群组成功（房间）
				getAppCurrunGroup().setJoin(true);
			}
			// 修改用户信息,同时修改用户自己的信息
			else if (obj.getMembersOp() == 0x13) {
				Log.i(TAG, "进入房间响应---fromSP>>>信息修改,time" + Util.getSysNowTime());
				getAppCurrunGroup().updateMember(getP2PUserInfo());
				callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_Status_Notify, 0x13, 0, null);
				return;
			}
			// 进入群组地图
			else if (obj.getMembersOp() == 0x14) {
				Log.i(TAG, "SP2CRoom_Status_Resp---fromSP>>>enter map");
				getAppCurrunGroup().addMapMember(getP2PUserInfo());
				// 通知GroupMap进入成功
				callMapHander(MeetRoomUtil.handler_SP2CRoom_Map_Enter, 0, null);
				return;
			}
			// 离开群组地图
			else if (obj.getMembersOp() == 0x15) {
				Log.i(TAG, "SP2CRoom_Status_Resp---fromSP>>>Leave Map");
				getAppCurrunGroup().removeMapMember(getP2PUserInfo().getUID());
				// 通知GroupMap离开
				return;
			}
			// 关闭语音接受
			else if (obj.getMembersOp() == 0x17) {
				Log.i(TAG, "SP2CRoom_Status_Resp---fromSP>>>close Audio");
				getAppCurrunGroup().setAcceptVoice(false);
				audioPlayManager.clear();
			}
			// 开启语音接受
			else if (obj.getMembersOp() == 0x16) {
				Log.i(TAG, "SP2CRoom_Status_Resp---fromSP>>>play Audio");
				getAppCurrunGroup().setAcceptVoice(true);
			}
			//通知界面更新
			callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_Status_Resp, 0, 0, obj);
		}
	}

	/**
	 * 房间成员状态修改是否成功
	 * 
	 * @author wubo
	 * @createtime 2012-3-13 02:05:44
	 * @param obj
	 */
	public void setSP2CRoom_Status_Notify(SP2CRoom_Status_Notify obj, Object Sock) {
		Log.e(TAG, "房间成员状态---fromSP>>>setSP2CRoom_Status_Notify,Operate:" + obj.getMembersOperate());
		List<P2PUserInfo> arrayList = new ArrayList<P2PUserInfo>();
		if (obj.getCRoomOp() == 0x00) {
			if (obj.getMembersNum() > 0) {
				// JavaSock jSock = new JavaSock();
				int membersSize = farther.getUserInfos_Size((int) obj
						.getMembersNum());
				CRecvData membersInfos = javaSock.JavaSock_Recv((CSock) Sock,
						membersSize);
				if (membersInfos == null) {
					return;
				}
				byte[] bSpeekerInfo = membersInfos.getData();
				P2PUserInfo[] infos = farther.getstUsersInfo(bSpeekerInfo,
						obj.getMembersNum());
				arrayList = new ArrayList<P2PUserInfo>();
				if (infos != null) {
					arrayList = new ArrayList<P2PUserInfo>(Arrays.asList(infos));
				}
			}

			if (arrayList != null && arrayList.size() > 0) {
				UserGroup userGroup = getAppCurrunGroup();
				// >>>>>>>>>>成员增加 wangxin
				if (obj.getMembersOperate() == 0x11) {
					// >>>>>>>>判断是谁加入
					if (arrayList.size() == 1) {
						// >>>>>>>>判断是否加入成功
						userGroup.addMember(arrayList.get(0));
					} else {
						userGroup.setConlist(arrayList);
					}
				}
				// 成员删除 wangixn
				else if (obj.getMembersOperate() == 0x12) {
					// >>>>>>>>判断是否删除成功
					userGroup.removeMember(arrayList.get(0).getUID());
					// 防止成员 异常退出时,麦序不更新CASE
					if (getAppCurrunGroup().removeGroupMics(
							arrayList.get(0).getUID())) {
						callChatRoomHander(
								MeetRoomUtil.handler_SP2CRoom_SpeechListStatus_Notify,
								0, 0, null);
					}
				}
				// 成员更新 wangxin
				else if (obj.getMembersOperate() == 0x13) {
					Log.d(TAG, "房间成员状态---fromSP>>>setSP2CR修改。。。。");
					userGroup.updateMember(arrayList.get(0));
				}
				// 成员进入地图模式，附带位置信息
				else if (obj.getMembersOperate() == 0x14) {
					Log.d(TAG, "房间成员状态---fromSP>>>进入群组地图。。。。");
					userGroup.addMapMember(arrayList.get(0));
					callMapHander(MeetRoomUtil.handler_SP2CRoom_Map_Refresh, 0,
							null);
					return;
				}
				// 成员离开地图模式
				else if (obj.getMembersOperate() == 0x15) {
					Log.d(TAG, "房间成员状态---fromSP>>>离开群组地图。。。。");
					userGroup.removeMapMember(arrayList.get(0).getUID());
					callMapHander(MeetRoomUtil.handler_SP2CRoom_Map_Refresh, 0,
							null);
					return;
				}
			}
			callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_Status_Notify,
					obj.getMembersOperate(), 0, arrayList);
			callMapHander(MeetRoomUtil.handler_SP2CRoom_Status_Notify, 0, null);
		} else if (obj.getCRoomOp() == 0x03) {
			Log.i(TAG, "remove");
		}

	}

	private void callMapHander(int what, int args1, Object bean) {
		// TODO Auto-generated method stub
		Message msg = new Message();
		msg.what = what;
		msg.arg1 = args1;
		msg.obj = bean;
		PangkerManager.getTabBroadcastManager().sendResultMessage(
				GroupMapActivity.class.getName(), msg);
	}

	private void callChatRoomHander(int what, int args1, int args2, Object bean) {
		Message msg = new Message();
		msg.what = what;
		msg.arg1 = args1;
		msg.arg2 = args2;
		msg.obj = bean;
		PangkerManager.getTabBroadcastManager().sendResultMessage(ChatRoomMainActivity.class.getName(), msg);
	}

	/**
	 * 接收媒体流,
	 * 
	 * @author wubo
	 * @createtime 2012-3-13 02:07:17
	 * @param obj
	 */
	public void setSP2NextHop_Stream(final SP2NextHop_Stream obj, Object sock) {
		Exception t = new Exception("obj.getS_Type()" + obj.getS_Type());
		logger.error("recvDataAfter", t);
		CRecvData cData = null;
		byte[] sendData = null;
		if (obj != null && sock != null) {
			if (obj.getS_Type() != MeetRoom.s_type_amr) {
				Log.d("Login", "JavaSock_Recv:" + Util.getSysNowTime());
				cData = javaSock.JavaSock_Recv((CSock) sock, obj.getPayLoadLen());
				if (cData == null) {
					return;
				}
				sendData = cData.getData();
				if (sendData == null) {
					return;
				}
			}
			Exception x = new Exception(String.valueOf(obj.getS_Type()) + ", time:" + Util.getSysNowTime());
			logger.error("SP2NextHop_Stream", x);
			switch (obj.getS_Type()) {
			case MeetRoom.s_type_text: // >>>>>>>>>文字媒体流
			case MeetRoom.s_type_loc: // >>>>>>>>>>位置信息传输
				Log.i(TAG, ">>>>>>>>>SP2NextHop_Stream:s_type_text=" + obj.getMsgId());
				try {
					MeetRoom mr = new MeetRoom(myuserid);
					mr.setUserId(String.valueOf(obj.getCP_UID()));
					mr.setContent(new String(sendData, "utf-8"));
					mr.setGid(getAppCurrunGroup().getGroupId());
					mr.setTalkTime(obj.getServerTime());
					mr.setMsgId(obj.getMsgId());
					mr.setGroupname(getAppCurrunGroup().getGroupName());
					mr.setMsgType(obj.getS_Type());
					Iterator<P2PUserInfo> mrIter = getAppCurrunGroup().getConlist().iterator();
					while (mrIter.hasNext()) {
						P2PUserInfo pi = mrIter.next();
						if (pi.getUID() == obj.getCP_UID()) {
							mr.setNickname(pi.getUserName());
							break;
						}
					}
					callChatRoomHander(MeetRoomUtil.handler_SP2NextHop_Text_Stream, 0, 0, mr);
				} catch (UnsupportedEncodingException e) {
					logger.error(TAG, e);
				}
				break;
			case MeetRoom.s_type_amr: // >>>>>>>>语音媒体流
				// if (AudioRecordThread.flag_compress) {
				// pushAmrByteStack((byte[]) sock);
				// } else {
				//
				// // 取出播放语音的数据流
				// Log.d("PayLoad", "obj.getPayLoad():" + obj.getPayLoad());
				// // cData = jSock.JavaSock_Recv((CSock) sock,
				// // obj.getPayLoadLen());
				// pushAmrByteStack(obj.getPayLoad());
				// }
				break;
			case MeetRoom.s_type_pic: // >>>>图片媒体流
				// >>>>>>>通知客户端进行图片下载
				Log.i(TAG, ">>>>>>>>>SP2NextHop_Stream:s_type_pic=" + obj.getMsgId());
				try {
					MeetRoom mr = new MeetRoom(myuserid);
					mr.setUserId(String.valueOf(obj.getCP_UID()));
					mr.setContent(new String(sendData, "utf-8"));
					mr.setGid(getAppCurrunGroup().getGroupId());
					mr.setTalkTime(obj.getServerTime());
					mr.setMsgId(obj.getMsgId());
					mr.setGroupname(getAppCurrunGroup().getGroupName());
					mr.setStatus(DownloadJob.DOWNLOAD_INIT);
					Log.d(TAG, "*******>>" + mr.getContent());
					mr.setMsgType(MeetRoom.s_type_pic);// >>>>>>>图片类型发言
					mr.setDirection(MeetRoom.MSG_TO_ME);// >>>>>>>>>>接收图片方向，客户端根据此标记进行图片下载
					Iterator<P2PUserInfo> mrIter = getAppCurrunGroup().getConlist().iterator();
					while (mrIter.hasNext()) {
						P2PUserInfo pi = mrIter.next();
						if (pi.getUID() == obj.getCP_UID()) {
							mr.setNickname(pi.getUserName());
							break;
						}
					}
					// >>>>>>>更新界面UI
					callChatRoomHander( MeetRoomUtil.handler_SP2NextHop_Text_Stream, 0, 0, mr);
				} catch (UnsupportedEncodingException e) {
					logger.error(TAG, e);
				}
				break;
			}
		}
	}

	// >>>>>>>通知当前用户发言
	// >>>>>>>>增加管理员发言通知 需要判断是不是自己发言 speekerUID == myUserID
	// true：按照原来流程 false：界面多显示一个用户发言的view
	public void setSP2CRoom_Speech_Notify(SP2CRoom_Speech_Notify obj) {
		if (obj == null) {
			return;
		}
		Log.e(TAG, "发言响应---fromSP>>>setSP2CRoom_Speech_Notify,time" + obj.toString());
		// >>>>>>>>>>判断是否是当前群组 ！！！！！！！
		if (obj.getOwnerSID() == Util.String2Long(getAppCurrunGroup()
				.getGroupSid())) {
			Log.e("infos", "--->>>是否是当前群组判断通过");
			// 开始发言 wagnxin
			if (obj.getSpeechType() == 0x61) {
				if (obj.isSpeecher(Long.valueOf(myuserid))) {
					if (getAppCurrunGroup().getSpeechStatus() == MeetRoomUtil.SpeechAllow.CAN_SPEAK) {
						return;
					}
				}
				if (getAppCurrunGroup().isAdmin(
						String.valueOf(obj.getSpeekerUID()))) {
					getAppCurrunGroup().setAdminSpeaking(true);
				}
				Log.i(TAG, "--->>>up,time" + Util.getSysNowTime());
				callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_Speech_Notify,
						(int) obj.getSpeekerUID(), 0, null);
				// 如果是管理员则直接发送。
				if (myuserid.equals(String.valueOf(obj.getSpeekerUID()))) {
					startAudioRecode();
				}
			}
			// >>>>>>>>>停止发言通知
			else if (obj.getSpeechType() == 0x62) {// >>>>>>>>停止发言 wagnxin
				Log.i(TAG, "--->>>down,time" + Util.getSysNowTime());
				audioPlayManager.removeAudioPlay(obj.getSpeekerUID());
				if (obj.getSpeekerUID() != obj.getOwnerUID()) {
					// >>>>>>>>判断是否在发言，如果发言（录音中）停止录音
					stopRecordThread();
				}
				if (getAppCurrunGroup().isAdmin(
						String.valueOf(obj.getSpeekerUID()))) {
					getAppCurrunGroup().setAdminSpeaking(false);
				}
				callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_MicCatch,
						(int) obj.getSpeekerUID(), 0, null);
			}
		}
	}

	/**
	 * 抢麦通知,群主的发言通知
	 */
	public void setSP2CRoom_Speech_Resp(SP2CRoom_Speech_Resp obj) {
		Log.e("infos",
				"---fromSP>>>抢麦通知 setSP2CRoom_Speech_Resp "
						+ obj.getError_Code());
		if (application.getCurrunGroup().isAdmin(myuserid)) {
			startAudioRecode();
		}
		callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_Speech_Resp,
				obj.getError_Code(), 0, null);
	}

	/**
	 * 麦序通知
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:45
	 * @param obj
	 */
	public void setSP2CRoom_SpeechList_Rsep(SP2CRoom_SpeechList_Rsep obj,
			Object Sock) {
		Log.e("infos",
				"---fromSP>>>setSP2CRoom_SpeechList_Rsep,time"
						+ Util.getSysNowTime());
		if (obj == null || Sock == null) {
			return;
		}
		if (obj.getQueueSize() > 0) {
			JavaSock jSock = new JavaSock();
			int speekerSize = farther.getUserInfos_Size((int) obj
					.getQueueSize());
			CRecvData SpeekerInfos = jSock.JavaSock_Recv((CSock) Sock,
					speekerSize);
			if (SpeekerInfos != null) {
				byte[] bSpeekerInfo = SpeekerInfos.getData();
				P2PUserInfo[] infos = farther.getstUsersInfo(bSpeekerInfo,
						obj.getQueueSize());
				getAppCurrunGroup().setGroupMics(
						new ArrayList<P2PUserInfo>(Arrays.asList(infos)));
			}
		}
		// >>>>>>>>>>>>>>>设置管理Uid，同时判断这个Uid是否在发言；
		getAppCurrunGroup().setUid(String.valueOf(obj.getOwnerUID()));
		// 0x00：无操作； 0x41：管理员正在发言； 0x42：管理员暂停发言；
		Log.e("infos",
				"---fromSP>>>setSP2CRoom_SpeechList_Rsep:"
						+ obj.getAdminSpeakStatus());
		if (obj.getAdminSpeakStatus() == 0x41) {
			getAppCurrunGroup().setAdminSpeaking(true);
		}
		callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_SpeechList_Rsep,
				(int) obj.getOwnerUID(), 0, null);
		// >>>>判断是否是自己发言
		for (int i = 0; i < getAppCurrunGroup().getGroupMics().size(); i++) {
			if (getAppCurrunGroup().getGroupMics().get(i).getUID() == getP2PUserInfo()
					.getUID()) {
				// >>>>>>>>>>最后
				callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_MicUp, i, 0,
						recordThread != null ? recordThread.isRecoding()
								: false);

				// >>>>>>>>>>>当i==0的情况说明我在麦序的第一位置上
				if (i == 0
						&& !String.valueOf(
								getAppCurrunGroup().getGroupMics().get(0)
										.getUID()).equals(
								getAppCurrunGroup().getUid())) {
					// new SpeeckStatusThread().start();
				}
				break;
			}
		}
	}

	/**
	 * 麦序列表增减通知
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:49
	 * @param obj
	 */
	public void setSP2CRoom_SpeechListStatus_Notify(
			SP2CRoom_SpeechListStatus_Notify sssn_obj) {
		// >>>>>>其他case不需要处理
		if (sssn_obj == null) {
			return;
		}
		Log.d(TAG, "---fromSP>>>setSP2CRoom_SpeechListStatus_Notify, "
				+ "sssn_obj.getSpeakerOp()这个数只能是 0 , 49加入 ,50离开麦序列表 "
				+ sssn_obj.getSpeakerOp());
		// >>>>>>>>>加入麦序列表
		if (sssn_obj.getSpeakerOp() == 0x31) {
			// >>>>>>>判断是否已经在麦序列表当中 只处理不在的case
			if (!getAppCurrunGroup().isAtGroupMics(
					sssn_obj.getSpeakerInfo().getUID())) {
				// >>>>>>如果是群组创建者
				if (sssn_obj.getSpeakerInfo().getUID() == Util
						.String2Long(getAppCurrunGroup().getUid())) {
					getAppCurrunGroup().addGroupMics(sssn_obj.getSpeakerInfo(),
							0);
				} else {
					getAppCurrunGroup().addGroupMics(sssn_obj.getSpeakerInfo());
				}
			}
		}
		// >>>>>>>离开麦序列表
		else if (sssn_obj.getSpeakerOp() == 0x32) {
			// >>>>>>>>>>>从麦序列表中删除
			getAppCurrunGroup().removeGroupMics(
					sssn_obj.getSpeakerInfo().getUID());
		}
		callChatRoomHander( MeetRoomUtil.handler_SP2CRoom_SpeechListStatus_Notify, 0, 0, null);
	}

	/**
	 * 离线消息推送
	 */
	@Override
	public void setSP2Client_NotRead_Resp(SP2Client_NotRead_Resp obj,
			Object Sock) {
		// TODO Auto-generated method stub
		Log.i(TAG, "---fromSP离线消息>>>setSP2Client_NotRead_Resp,未读消息最后一条时间  =  "
				+ NoReadEndTime + " 转换后为 " + Util.FormatTime(NoReadEndTime)
				+ " 一次读取了多少条 = " + obj.getEveryNoReadSize());
		try {
			if (obj.getErrCode() != 0x000) {
				// 失败
				return;
			}
			NoReadEndTime = obj.getNoReadEndTime();
			if (obj.getEveryNoReadSize() > 0) {
				int len = farther.getNoReadMsgSize(obj.getEveryNoReadSize());
				Log.d("Login", "JavaSock_Recv:" + Util.getSysNowTime() + " 要接收的长度是len = " + len);
				CRecvData chatroomData = javaSock.JavaSock_Recv((CSock) Sock, len);
				if (chatroomData != null) {

					byte[] chatroomInfos = chatroomData.getData();
					ChatRecordInfo[] infos = farther.getChatRecordInfo(chatroomInfos, obj.getEveryNoReadSize());
					List<MeetRoom> mrs = new ArrayList<MeetRoom>();
					for (int i = 0; i < infos.length; i++) {
						if (infos[i].getS_Type() == MeetRoom.s_type_text || infos[i].getS_Type() == MeetRoom.s_type_pic) {
							MeetRoom mr = new MeetRoom(myuserid);
							mr.setUserId(String.valueOf(infos[i].getSpeakerUID()));
							mr.setTalkTime(infos[i].getSpeakerTime());
							mr.setNickname(infos[i].getSpeakerName());
							mr.setMsgType(infos[i].getS_Type());
							try {
								if (infos[i].getSpeakerContext() == null) {
									continue;
								} else {
									mr.setContent(new String(infos[i].getSpeakerContext().getBytes(), "utf-8"));
								}
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (infos[i].getS_Type() == MeetRoom.s_type_pic) {
								mr.setStatus(DownloadJob.DOWNLOAD_INIT);
							}
							mr.setMsgId(infos[i].getMsgID());
							mr.setGid(getAppCurrunGroup().getGroupId());
							mr.setGroupname(getAppCurrunGroup().getGroupName());
							mrs.add(mr);
						}
					}
					// arg1 为未读消息 的总数
					callChatRoomHander(
							MeetRoomUtil.handler_SP2Client_NotRead_Resp,
							obj.getTotalNoReadSize(), 0, mrs);
				}
			} else {
				callChatRoomHander(MeetRoomUtil.handler_SP2Client_NotRead_Resp,
						0, 0, null);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public CP2SP_Stream initCP2SP_Stream() {
		CP2SP_Stream cp2SP_Stream = new CP2SP_Stream();
		cp2SP_Stream.setSender_UID(getP2PUserInfo().getUID());
		cp2SP_Stream.setReceiver_UID(MeetRoomUtil.getSpUid(this));
		cp2SP_Stream
				.setOwnerUID(Util.String2Long(getAppCurrunGroup().getUid()));
		cp2SP_Stream.setOwnerSID(Util.String2Long(getAppCurrunGroup()
				.getGroupSid()));
		return cp2SP_Stream;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.i(TAG, "MeetService.onDestroy()");
		// >>>>>>>>登出群组服务器
		unregisterReceiver(mChangeReceiver);
		super.onDestroy();
	}

	@Override
	public void startAudioRecode() {
		// >>>>>>判断当前录音线程是否可用，如果是群组，5秒之内没有进行发言，那么要停止发言
		if (recordThread == null || !recordThread.isRecoding()) {
			recordThread = new AudioRecordThread(initCP2SP_Stream(), this,
					getAppCurrunGroup().getMaxSpeechTime());
			recordThread.start();
		}
		// 如果是群主则直接发言
		if (application.getCurrunGroup().isAdmin(myuserid)) {
			sendAudio(true);
		}
	}

	private void sendAudio(boolean isAdmin) {
		if (recordThread != null && recordThread.isRecoding()
				&& !recordThread.isIfsendArm()) {
			recordThread.startSendArm(isAdmin);
		}
	}

	/*
	 * 同步群组在线列表 (non-Javadoc)
	 * com.wachoo.pangker.group.P2PInterface#setSP2Croom_UserList_Resp
	 * (com.wachoo .pangker.group.SP2Croom_UserList_Resp, java.lang.Object)
	 */
	@Override
	public void setSP2Croom_UserList_Resp(SP2Croom_UserList_Resp obj, Object Sock) {
		Log.i("infos", "---fromSP>>>setSP2Croom_UserList_Resp");
		List<P2PUserInfo> userList = new ArrayList<P2PUserInfo>();
		if (obj == null || Sock == null) {
			return;
		}
		if (obj.getQueueSize() > 0) {
			JavaSock jSock = new JavaSock();
			int speekerSize = farther.getUserInfos_Size((int) obj
					.getQueueSize());
			CRecvData SpeekerInfos = jSock.JavaSock_Recv((CSock) Sock,
					speekerSize);
			if (SpeekerInfos != null) {
				byte[] bSpeekerInfo = SpeekerInfos.getData();
				P2PUserInfo[] infos = farther.getstUsersInfo(bSpeekerInfo,
						obj.getQueueSize());
				userList = new ArrayList<P2PUserInfo>(Arrays.asList(infos));
			}
		}
		getAppCurrunGroup().setConlist(userList);
		// 加载地图模式用户
		for (P2PUserInfo pInfo : userList) {
			if (pInfo.getLocateType() == 1) {
				getAppCurrunGroup().addMapMember(pInfo);
			}
		}
		callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_Status_Notify, 0x13,
				0, userList);
	}

	@Override
	public void stopAudioRecode() {
		Log.d("CP2SP_STREAM_STOP_FLAG", "891>>>>>停止发言（语音）申请");
		stopRecordThread();
		sendPack(Configuration.CP2SP_STREAM_STOP_FLAG);
		getAppCurrunGroup().setSpeechStatus(
				MeetRoomUtil.SpeechAllow.INITIALIZATION);
		callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_StreamStop, 0, 0, null);
	}

	@Override
	public void setSendAmr(byte[] sendAmr) {
		// TODO Auto-generated method stub
		this.sendAmr = sendAmr;
		sendPack(Configuration.CP2SP_STREAM_AMR_FLAG);
	}

	@Override
	public void showRecordTime(int second) {
		// TODO Auto-generated method stub
		callChatRoomHander(MeetRoomUtil.handler_SetTime_Flag, second, 0, null);
	}

	class NetCheckThread extends Thread {

		private boolean isrun = true;

		@Override
		public void run() {
			// 设置丢包检测时长检测;
			Log.d("Login", "SetUnreliablePara:" + Util.getSysNowTime());
			javaSock.JavaSock_SetUnreliablePara(50, 0);
			try {
				while (isrun) {
					if (recordThread != null && recordThread.isRecoding()) {
						int lossUplink = javaSock.JavaSock_GetNetLoss(
								getConn_notrust_sock().getSockID(), 1, getConn_notrust_sock().getSessionNo());
						Log.d("NetCheckThread", "NetCheckThread==>lossUplink:" + lossUplink);
						if (lossUplink > 0) {
							Exception t = new Exception("NetCheckThread==>lossUplink:" + lossUplink);
							logger.error("NetCheckThread", t);
						}
					}
					if (audioPlayManager.speakUserCount() > 0) {
						int lossDownlink = javaSock.JavaSock_GetNetLoss(
								getConn_notrust_sock().getSockID(), 0, getConn_notrust_sock().getSessionNo());
						Log.d("NetCheckThread",
								"NetCheckThread==>lossDownlink:" + lossDownlink);
						if (lossDownlink > 0) {
							Exception t = new Exception("NetCheckThread==>lossDownlink:" + lossDownlink);
							logger.error("NetCheckThread", t);
						}
					}
					Thread.sleep(300);
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.error(TAG, e);
			}
		}

		public void stopRun() {
			// TODO Auto-generated method stub
			isrun = false;
		}
	}

	/**
	 * 链路断开
	 */
	@Override
	public void connDown() {
		// TODO Auto-generated method stub
		Log.i(TAG, "ConnDown!");
	}

	/**
	 * TODO 通知MainActivity界面title栏显示群组进入的按钮 void
	 */
	private void sendGroupBroadcast(String action) {
		Intent intent = new Intent(action);
		this.sendBroadcast(intent);
	}

	// >>>>>>>> add by wangxin 使用绑定service的方式来进入群组

	private MeetRoomServiceBinder mBinder = new MeetRoomServiceBinder();

	/**
	 * meetroom service binder
	 * 
	 * @author wangxin
	 * 
	 */
	public class MeetRoomServiceBinder extends Binder {
		// private int speechState; // 说话状态,解决在自己在发言时,返回后再进来,按钮显示错误的CASE
		private String oldUserGroupUid; // 在进入群组时设置,在退出群组后清空 ,解决切换群组时,退出群组包传错群组
										// ID的CASE
		private String oldUserGroupSid;

		public UserGroup getCurrenGroup() {
			return getAppCurrunGroup();
		}

		public String getOldUserGroupUid() {
			return oldUserGroupUid;
		}

		public void setOldUserGroupUid(String oldUserGroupUid) {
			this.oldUserGroupUid = oldUserGroupUid;
		}

		public String getOldUserGroupSid() {
			return oldUserGroupSid;
		}

		public void setOldUserGroupSid(String oldUserGroupSid) {
			this.oldUserGroupSid = oldUserGroupSid;
		}

	}

	private P2PUserInfo getP2PUserInfo() {
		return application.getP2PUserInfo();
	}

	public UserGroup getAppCurrunGroup() {
		return application.getCurrunGroup();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	@Override
	public void login() {
		mHelloThread.setConnected(false);
		// 如果还在群组里面，就进行连接，modify by Lb
		if (getAppCurrunGroup() != null) {
			boolean con_flag = connConnect();
			mHelloThread.setConnected(con_flag);
			if (con_flag) {
				Log.d("Login", "connConnect>>>>>>>>connConnect to groupSP");
				sendPack(Configuration.CMANAGER2SP_LOGIN_REQ_FLAG);
			} else {
				Log.i("Login", "connConnect>>>>>>>>connConnect to groupSP fail!!");
				logout(false, "登陆群组服务器失败!");
			}
		} else {
			logout(false, "获取群组信息失败！");
		}
	}

	@Override
	public void logout(boolean isSendPacket, String message) {
		mChangeReceiver.registerNetChangeReceiver(this, null);

		Log.d(TAG, "====>CMANAGER2SP_LOGINOUT_REQ_FLAG:" + isSendPacket);
		// >>>>>>>>>发送离开群组包
		if (getConn_trust_sock() != null && isSendPacket) {
			Log.d(TAG, "====>CMANAGER2SP_LOGINOUT_REQ_FLAG:");
			sendPack(Configuration.CMANAGER2SP_LOGINOUT_REQ_FLAG);
		}
		// 用户正在发言中,,,
		stopRecordThread();
		// /将语音播放线程停掉
		if (audioPlayManager != null) {
			audioPlayManager.clear();
		}
		// 关闭检测线程
		if (netCheckThread != null) {
			netCheckThread.stopRun();
		}
		// >>>>>>>>>停止hello包发送
		stopHello();
		// >>>>>>>通知离开群组
		sendGroupBroadcast(PangkerConstant.ACTION_GROUPLOGOUT);
		// >>>退出群组
		if (getAppCurrunGroup() != null)
			getAppCurrunGroup().leaveGroup();
		application.setCurrunGroup(null);

		callChatRoomHander(MeetRoomUtil.handler_SP2CRoom_Disconnect, 0, 0, message);
		callMapHander(MeetRoomUtil.handler_SP2CRoom_Disconnect, 0, null);
		// >>>>>>>>>>>>连接没有成功的case不需要删除sock
		if (isSendPacket) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
			}
			// 关闭socket连接
			Log.d("Login", "JavaSock_DelSock-->");
			javaSock.JavaSock_DelSock(eppllID, getConn_trust_sock());
			Log.d("Login", "JavaSock_close-->");
			javaSock.JavaSock_Close(getConn_trust_sock());
		}
		// 释放资源
		setConn_notrust_sock(null);
		setConn_trust_sock(null);
	}

	private long getSpUid() {
		// TODO Auto-generated method stub
		return MeetRoomUtil.getSpUid(this);
	}

	/*
	 * 发送消息包 (non-Javadoc)
	 * 
	 * @see com.wachoo.pangker.group.IRoomMessageSender#sendPack(int)
	 */
	public void sendPack(int flag) {
		// add by wangxin 异常捕获
		try {
			// >>>连接群组服务器
			if (mHelloThread == null || !mHelloThread.isConnected() || getConn_trust_sock() == null) {
				stopRecordThread();
				Log.i(TAG, "消息发送失败===>" + flag);
				return;
			}
			if (getAppCurrunGroup() == null && flag != Configuration.CMANAGER2SP_LOGIN_REQ_FLAG) {
				logout(false, "获取群组信息失败");
				return;
			}
			switch (flag) {
			// >>>登陆群组服务器,###############这个要做超时检测
			case Configuration.CMANAGER2SP_LOGIN_REQ_FLAG:
				// >>>如果已经登陆
				Log.i("Login", "---toSP>>>登陆CMANAGER2SP_LOGIN_REQ_FLAG");
				Cmanager2SP_Login_Req clr = new Cmanager2SP_Login_Req(
						getP2PUserInfo().getUID(), MeetRoomUtil.getSpUid(this),
						"123456");
				Log.d("Login", "toSP>>>登陆CMANAGER2SP_LOGIN_REQ_FLAG==>" + clr.getSender_UID() + "," + getSpUid());
				receiveAmr = farther.getCmanager2SP_Login_Req(clr);
				break;

			// >>>登出群组服务器
			case Configuration.CMANAGER2SP_LOGINOUT_REQ_FLAG:
				Log.i(TAG, "---toSP>>>登出CMANAGER2SP_LOGINOUT_REQ_FLAG");
				CRoom2SP_LoginOut_Req clor;
				if (getAppCurrunGroup().isJoin()) {
					clor = new CRoom2SP_LoginOut_Req(getP2PUserInfo().getUID(),
							getSpUid(), Util.String2Long(getAppCurrunGroup().getUid()),
							Util.String2Long(getAppCurrunGroup().getGroupSid()));
				} else {
					clor = new CRoom2SP_LoginOut_Req(getP2PUserInfo().getUID(), getSpUid(), 0, 0);
				}
				receiveAmr = farther.getCRoom2SP_LoginOut_Req(clor);
				if (receiveAmr == null || receiveAmr.length <= 0) {
					callChatRoomHander(MeetRoomUtil.handler_SP2Login_Fail, 0, 0, null);
					return;
				}
				Log.d("Login", "JavaSock_Send Logout-->");
				javaSock.JavaSock_Send(getConn_trust_sock(), receiveAmr, receiveAmr.length);
				return;
				// >>>麦序获取
			case Configuration.CROOM2SP_SPEECHLIST_REQ_FLAG:
				Log.i(TAG, "---toSP>>>CROOM2SP_SPEECHLIST_REQ_FLAG");
				CRoom2SP_SpeechList_Req cslr = new CRoom2SP_SpeechList_Req(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(getAppCurrunGroup().getUid()),
						Util.String2Long(application.getCurrunGroup()
								.getGroupSid()), farther.RKEY);
				receiveAmr = farther.getCRoom2SP_SpeechList_Req(cslr);
				break;
			// >>> 上麦消息包
			case Configuration.CROOM2SP_SPEECH_REQ_FLAG_UP:
				Log.i(TAG, "---toSP>>>CROOM2SP_SPEECH_REQ_FLAG_UP");
				CRoom2SP_Speech_Req csr = new CRoom2SP_Speech_Req(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(getAppCurrunGroup().getUid()),
						Util.String2Long(application.getCurrunGroup()
								.getGroupSid()), (short) 4097, (short) 0x41,
						judgeIsMyselfSpeechType(), getP2PUserInfo());
				receiveAmr = farther.getCRoom2SP_Speech_Req(csr);
				break;

			// >>> 下麦消息包
			case Configuration.CROOM2SP_SPEECH_REQ_FLAG_DOWN:
				Log.i(TAG, "---toSP>>>CROOM2SP_SPEECH_REQ_FLAG_DOWN");
				CRoom2SP_Speech_Req downcsr = new CRoom2SP_Speech_Req(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(getAppCurrunGroup().getUid()),
						Util.String2Long(application.getCurrunGroup()
								.getGroupSid()), (short) 4097, (short) 0x42,
						(short) 0x32, getP2PUserInfo());
				receiveAmr = farther.getCRoom2SP_Speech_Req(downcsr);
				break;

			// >>>停止发言
			case Configuration.CP2SP_STREAM_STOP_FLAG:
				Log.i("CP2SP_STREAM_STOP_FLAG", "toSP>>>CP2SP_STREAM_STOP_FLAG");
				CP2SP_Stream_Stop css = new CP2SP_Stream_Stop(
						Util.String2Long(getAppCurrunGroup().getUid()),
						Util.String2Long(getAppCurrunGroup().getGroupSid()),
						getP2PUserInfo().getUID(), getP2PUserInfo().getUID(),
						getSpUid());
				receiveAmr = farther.getCP2SP_Stream_Stop(css);
				break;

			// >>>发手机语音包（发言中）
			case Configuration.CP2SP_STREAM_AMR_FLAG:
				receiveAmr = sendAmr;
				break;

			// >>>文字发言
			case Configuration.CP2SP_STREAM_TEXT_FLAG:
			case Configuration.CP2SP_STREAM_PIC_FLAG:
				if (StringUtils.isEmpty(chatTxtMessage)) {
					return;
				}
				CP2SP_Stream cs = initCP2SP_Stream();
				byte[] msgbyte = chatTxtMessage.getBytes();
				cs.setPayLoad(msgbyte);
				cs.setPayLoadLen(msgbyte.length);
				if(flag == Configuration.CP2SP_STREAM_TEXT_FLAG){
					Log.i(TAG, "---toSP>>>CP2SP_STREAM_TEXT_FLAG:" + chatTxtMessage);
					cs.setS_Type(MeetRoom.s_type_text);
				} else {
					Log.i(TAG, "---toSP>>>CP2SP_STREAM_PIC_FLAG:" + chatTxtMessage);
					cs.setS_Type(MeetRoom.s_type_pic);
				}
				receiveAmr = farther.getCP2SP_Stream(cs, new byte[2048]);
				receiveAmr = farther.setMsg_Length(receiveAmr, (short) (receiveAmr.length - farther.getMsgHead_Size()));
				break;
			// >>>地理位置信息消息包
			case Configuration.CP2SP_STREAM_LOCATION_FLAG:
				Gson gson = new Gson();
				Log.i(TAG, "---toSP>>>CP2SP_STREAM_LOCATION_FLAG:" + gson.toJson(roomLocation));
				CP2SP_Stream cs2 = initCP2SP_Stream();
				byte[] locbyte = new String(gson.toJson(roomLocation)).getBytes();
				cs2.setPayLoad(locbyte);
				cs2.setPayLoadLen(locbyte.length);
				cs2.setS_Type(MeetRoom.s_type_loc);
				receiveAmr = farther.getCP2SP_Stream(cs2, new byte[2048]);
				receiveAmr = farther.setMsg_Length(receiveAmr, (short) (receiveAmr.length - farther.getMsgHead_Size()));
				break;

			// >>>>加入群组 (真正的加入到群组当中)
			case Configuration.CROOM2SP_STATUS_REPORT_MEMBER_ADD_FLAG:
				Log.i("Login", "---toSP>>>CROOM2SP_STATUS_REPORT_MEMBER_ADD_FLAG,time" + Util.getSysNowTime());
				CRoom2SP_Status_Report csmr = new CRoom2SP_Status_Report(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(getAppCurrunGroup().getUid()),
						Util.String2Long(application.getCurrunGroup()
								.getGroupSid()), farther.RKEY, (short) 0x00,
						(short) 0x11, (short) 0x21, getP2PUserInfo());
				receiveAmr = farther.getCRoom2SP_Status_Report(csmr);

				break;
			// >>>>>>>>>>>>>>修改个人信息
			case Configuration.CROOM2SP_STATUS_REPORT_MEMBER_UPD_FLAG:
				Log.i("SpeechListStatus", "---toSP>>>CROOM2SP_STATUS_REPORT_MEMBER_UPD_FLAG,time"
								+ Util.getSysNowTime() + ",userName:"
								+ getP2PUserInfo().getUserName());
				CRoom2SP_Status_Report ucsmrd = new CRoom2SP_Status_Report(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupUid()),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupSid()), farther.RKEY,
						(short) 0x00, (short) 0x13, (short) 0x00,
						getP2PUserInfo());
				receiveAmr = farther.getCRoom2SP_Status_Report(ucsmrd);
				break;
			// >>>>>>>>>>>>>>退出群组 wangxin edit start
			case Configuration.CROOM2SP_STATUS_REPORT_MEMBER_DEL_FLAG:
				Log.i(TAG, "---toSP>>>CROOM2SP_STATUS_REPORT_MEMBER_DEL_FLAG,time" + Util.getSysNowTime());
				// >>>>>>>>>>>>>>> 退出群组消息包 wangxin
				CRoom2SP_Status_Report csmrd = new CRoom2SP_Status_Report(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupUid()),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupSid()), farther.RKEY,
						(short) 0x00, (short) 0x12, (short) 0x00,
						getP2PUserInfo());
				receiveAmr = farther.getCRoom2SP_Status_Report(csmrd);
				// >>>>>>>清空数据 wangxin
				break;
			// 加入群组地图，修改位置信息
			case Configuration.CROOM2SP_STATUS_REPORT_LOCAYION_ADD_FLAG:
				Log.i(TAG, "---toSP>>>CROOM2SP_STATUS_REPORT_LOCAYION_ADD_FLAG==>LOCAYION_ADD");
				P2PUserInfo p2pUserInfo = getP2PUserInfo();
				p2pUserInfo.setLocateType(1);
				if (roomLocation == null) {
					roomLocation = application.getCurrentLocation();
				}
				p2pUserInfo.setLocationInfo(roomLocation.getLatitude() + ","
						+ roomLocation.getLongitude());
				CRoom2SP_Status_Report lsmrd = new CRoom2SP_Status_Report(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupUid()),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupSid()), farther.RKEY,
						(short) 0x00, (short) 0x14, (short) 0x00, p2pUserInfo);
				receiveAmr = farther.getCRoom2SP_Status_Report(lsmrd);
				break;
			// 离开群组地图
			case Configuration.CROOM2SP_STATUS_REPORT_LOCAYION_DEL_FLAG:
				Log.i(TAG, "---toSP>>>CROOM2SP_STATUS_REPORT_LOCAYION_DEL_FLAG==>LOCAYION_DEL");
				P2PUserInfo p2pUserInfo2 = getP2PUserInfo();
				p2pUserInfo2.setLocateType(0);
				CRoom2SP_Status_Report ldsmrd = new CRoom2SP_Status_Report(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupUid()),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupSid()), farther.RKEY,
						(short) 0x00, (short) 0x15, (short) 0x00, p2pUserInfo2);
				receiveAmr = farther.getCRoom2SP_Status_Report(ldsmrd);
				break;
			// 群组开启接受语音
			case Configuration.CROOM2SP_STATUS_RADIO_ACCEPT_PLAY_FLAG:
				Log.i(TAG, "---toSP>>>CROOM2SP_STATUS_RADIO_ACCEPT_PLAY_FLAG");
				// >>>>>>>>>>>>>>> 退出群组消息包 wangxin
				CRoom2SP_Status_Report adio_stop_csmrd = new CRoom2SP_Status_Report(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupUid()),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupSid()), farther.RKEY,
						(short) 0x00, (short) 0x16, (short) 0x00,
						getP2PUserInfo());
				receiveAmr = farther.getCRoom2SP_Status_Report(adio_stop_csmrd);
				break;
			// 群组停止接受语音
			case Configuration.CROOM2SP_STATUS_RADIO_ACCEPT_STOP_FLAG:
				Log.i(TAG, "---toSP>>>CROOM2SP_STATUS_RADIO_ACCEPT_STOP_FLAG");
				// >>>>>>>>>>>>>>> 退出群组消息包 wangxin
				CRoom2SP_Status_Report adio_play_csmrd = new CRoom2SP_Status_Report(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupUid()),
						Util.String2Long(((MeetRoomServiceBinder) mBinder)
								.getOldUserGroupSid()), farther.RKEY,
						(short) 0x00, (short) 0x17, (short) 0x00,
						getP2PUserInfo());
				receiveAmr = farther.getCRoom2SP_Status_Report(adio_play_csmrd);
				break;
			// >>>>>>>>>>>>>>退出群组 wangxin edit end
			// >>>>>>>>>>>>获取成员列表wangxin start
			case Configuration.CROOM2SP_USERLIST_REQ:
				Log.i(TAG, "---toSP>>>CROOM2SP_USERLIST_REQ");
				CRoom2SP_UserList_Req cur = new CRoom2SP_UserList_Req(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(getAppCurrunGroup().getUid()),
						Util.String2Long(application.getCurrunGroup()
								.getGroupSid()), farther.RKEY);
				receiveAmr = farther.getCRoom2SP_UserList_Req(cur);
				break;

			// >>>>>>>>> 群组离线消息wangxin
			case Configuration.CLIENT2SP_NOTREAD_REQ:
				if (NoReadEndTime == 0) {
					callChatRoomHander( MeetRoomUtil.handler_SP2Client_NotRead_Resp, 0, 0, null);
					Log.e("infos", "NoReadEndTime  is " + NoReadEndTime);
					return;
				}
				Log.i("infos", "---toSP>>>离线消息Client2SP_NotRead_Req,取未读消息的开始时间 是 = " + NoReadEndTime);
				// >>>>>>>.....获取离线消息
				Client2SP_NotRead_Req cnr = new Client2SP_NotRead_Req(
						getP2PUserInfo().getUID(), getSpUid(),
						Util.String2Long(getAppCurrunGroup().getUid()),
						Util.String2Long(application.getCurrunGroup().getGroupSid()), 0, NoReadEndTime);

				receiveAmr = farther.getClient2SP_NotRead_Req(cnr);
				break;
			}
			// >>>>>>>>>>>检测发送失败
			if (receiveAmr == null || receiveAmr.length <= 0) {
				Toast.makeText(application, "发送数据失败!", Toast.LENGTH_SHORT) .show();
				return;
			}
			int num = 0;
			// 语音的时候是非可靠传输,非语音是可靠传输
			if (flag == Configuration.CP2SP_STREAM_AMR_FLAG) {
				Log.d("JavaSock_Send", getConn_notrust_sock().toString());
				
				num = javaSock.JavaSock_Send(getConn_notrust_sock(), receiveAmr, receiveAmr.length);
			} else {
				num = javaSock.JavaSock_Send(getConn_trust_sock(), receiveAmr, receiveAmr.length);
			}
			judgeSendBySockID(num, flag);
		} catch (Exception e) {
			Log.e(TAG, ">>>>>>>>>Exception!!!!!!!!packetSend:type=" + flag + ";e=" + e);
			logger.error(e);
		}
	}

	/**
	 * 判断用户是不是群组创建者
	 */
	private short judgeIsMyselfSpeechType() {
		short isMySelfSpeechType; // 0x31：管理员麦序操作；0x32：成员麦序操作；
		if (application.getMyUserID().equals(getAppCurrunGroup().getUid())) {
			isMySelfSpeechType = 0x31;
		} else {
			isMySelfSpeechType = 0x32;
		}
		return isMySelfSpeechType;
	}

	// >>>>>>>开始语音播放,同时开始进行检测丢包率
	private void audioPlayStart() {
		if (netCheckThread != null) {
			netCheckThread.stopRun();
		}
		netCheckThread = new NetCheckThread();
		netCheckThread.start();
	}

	private int noSpeechMsg = 0;
	
	/**
	 * JNI回叫方法，用来获取groupSP push过来的包
	 */
	@Override
	public void ParseMsgInface_ParseMsg(int SockID, int ptlType, int sessionId, boolean trust) {
		// >>>>>>>判断网络
		if (!ConnectivityHelper.isConnectivityAvailable(this) && getAppCurrunGroup() != null) {
			logout(false, "检查您的网络，与服务器断开连接");
			return;
		}
		// >>>>>>>每次初始化new一个CSock
		CSock revcSock = new CSock();
		revcSock.setType(ptlType);
		revcSock.setSockID(SockID);
		revcSock.setTrust(trust);
		revcSock.setSessionNo(sessionId);
		CRecvData recvData = null;
		int MsgHeadLen = farther.getMsgHead_Size();
		CRecvData MsgHead = javaSock.JavaSock_Recv(revcSock, MsgHeadLen);
		if (null == MsgHead) {
			// 关闭socket连接
			Log.d("Login", "JavaSock_DelSock-->");
			javaSock.JavaSock_DelSock(eppllID, revcSock);
			Log.d("Login", "JavaSock_close-->");
			javaSock.JavaSock_Close(revcSock);
			return;
		}
		// >>>>>>>>设置心跳包(返回)
		mHelloThread.setHasHelloResp(true);

		byte[] bMsgHead = MsgHead.getData();
		short P2P_Type = farther.getMsg_Type(bMsgHead);
		// 取出第二次要接收的长度, 消息头获取一个字段取出;
		short sencondNeedRecLeng = farther.getMsg_Length(bMsgHead);
		
		if (P2P_Type == P2PFarther.TYPE_SP2NEXTHOP_STREAM) {
			if (!revcSock.isTrust() && getAppCurrunGroup().isAcceptVoice()) {
				
				recvData = javaSock.JavaSock_Recv(revcSock, sencondNeedRecLeng);
				byte[] btmp = recvData.getData();
				int responseLenght = farther.getResp_Size(P2PFarther.TYPE_SP2NEXTHOP_STREAM);
				// >>>>>>>>>获取发言用户UID speakUID
				byte[] bytSpeakUID = new byte[8];
				System.arraycopy(btmp, 32, bytSpeakUID, 0, bytSpeakUID.length);
				long speakUID = MeetRoomUtil.byteToLong(bytSpeakUID);
				
				// >>>>>>>>>获取发言信息Id
				byte[] messageIds = new byte[4];
				System.arraycopy(btmp, 44, messageIds, 0, messageIds.length);
				int messId = MeetRoomUtil.byteToInt(messageIds);

				// >>>>>>>>>获取语音消息包。
				byte[] arms = new byte[btmp.length - (responseLenght - MsgHeadLen)];
				System.arraycopy(btmp, responseLenght - MsgHeadLen, arms, 0, arms.length);
				Log.e("RecVoice", "JavaSock_Recv, messId>>>" + messId + "Play>>>" + arms.length);
				// >>>>>>>>进行语音播放
				audioPlayManager.audioPlay(speakUID, arms);
				return;
			} else {
				recvData = javaSock.JavaSock_Recv(revcSock, farther.getResp_Size(P2P_Type) - MsgHeadLen);
				// 排除那些由于窜包引发的语音异常!modify by Lb
				if (recvData.getData()[40] != 18 && trust == false) {
					Exception x2 = new Exception(String.valueOf(recvData.getData()[40]));
					logger.error("recvDataLoss", x2);
					return;
				}
			}
		} else {
			recvData = javaSock.JavaSock_Recv(revcSock, farther.getResp_Size(P2P_Type)- MsgHeadLen);
		}
			
		if (recvData == null) {
			Log.i("noSpeechMsg", P2P_Type + "||接收非语音的 消息体 recvData is null,共 " + (++noSpeechMsg) + "次");
			return;
		}

		byte[] btmp = recvData.getData();

		byte[] totalPayLoad = new byte[bMsgHead.length + btmp.length];
		System.arraycopy(bMsgHead, 0, totalPayLoad, 0, bMsgHead.length);
		System.arraycopy(btmp, 0, totalPayLoad, bMsgHead.length, btmp.length);

		farther.ConvertPackage(totalPayLoad, this, revcSock);
	}

	@Override
	public void setSP2File_Upload_Resp(SP2File_Upload_Resp obj) {
		Log.i(TAG, "---fromSP>>>setSP2File_Upload_Resp,time" + Util.getSysNowTime());
	}

	@Override
	public void setSP2CRoom_Conf_Resp(SP2CRoom_Conf_Resp obj) {
		// TODO Auto-generated method stub
		Log.i("setSP2CRoom_Conf_Resp", "---fromSP>>>" + obj.getError_Code());
	}

}