package com.wachoo.pangker.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.pangker.xmpp.SystemNotice.SystemNoticeType;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.HomeActivity;
import com.wachoo.pangker.activity.MainActivity;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.contact.ContactActivity;
import com.wachoo.pangker.activity.contact.ContactAttentionActivity;
import com.wachoo.pangker.activity.contact.ContactFollowActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.activity.pangker.RelativesDynamicActivity;
import com.wachoo.pangker.activity.res.RecommendReceiverAcitvity;
import com.wachoo.pangker.activity.res.ResNetManagerActivity;
import com.wachoo.pangker.activity.res.ResRecommendActivity;
import com.wachoo.pangker.activity.res.UploadAppActivity;
import com.wachoo.pangker.activity.res.UploadDocActivity;
import com.wachoo.pangker.activity.res.UploadMusicActivity;
import com.wachoo.pangker.activity.res.UploadPicActivity;
import com.wachoo.pangker.api.ContactUserListenerManager;
import com.wachoo.pangker.api.VersionUpdateCheck;
import com.wachoo.pangker.api.VersionUpdateCheck.VersionCheckType;
import com.wachoo.pangker.chat.ILoginIMServerThreadManager;
import com.wachoo.pangker.chat.MessageBroadcast;
import com.wachoo.pangker.chat.MessageReceivedFilter;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.chat.PacketBusinessFilter;
import com.wachoo.pangker.chat.PacketChatmsgFilter;
import com.wachoo.pangker.chat.VCardManager;
import com.wachoo.pangker.db.IAttentsDao;
import com.wachoo.pangker.db.IFansDao;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.ILocalContactsDao;
import com.wachoo.pangker.db.INetAddressBookDao;
import com.wachoo.pangker.db.INetAddressBookUserDao;
import com.wachoo.pangker.db.INoticeDao;
import com.wachoo.pangker.db.IRecommendDao;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.ITipsDao;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.impl.AttentsDaoImpl;
import com.wachoo.pangker.db.impl.FansDaoImpl;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.db.impl.LocalContactsDaoImpl;
import com.wachoo.pangker.db.impl.NetAddressBookDaoImpl;
import com.wachoo.pangker.db.impl.NetAddressBookUserDaoImpl;
import com.wachoo.pangker.db.impl.NoticeDaoImpl;
import com.wachoo.pangker.db.impl.RecommendDaoImpl;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.db.impl.TipDaoImpl;
import com.wachoo.pangker.db.impl.UserMsgDaoImpl;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.MessageTip;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.NoticeInfo;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.map.app.LocateManager;
import com.wachoo.pangker.map.app.LocateManager.LocateType;
import com.wachoo.pangker.receiver.StorageEventFilter;
import com.wachoo.pangker.server.HttpReqCode;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.AddressMembers;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.LocationQueryResult;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.PhoneBookResult;
import com.wachoo.pangker.server.response.ResDirQueryResult;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.ui.dialog.ConfimNoticeDialog;
import com.wachoo.pangker.util.ConnectivityHelper;
import com.wachoo.pangker.util.ContactsUtil;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;
import com.wifi.locate.manager.WifiLocateManager;

/**
 * openfire packet listenter
 * 
 * @author wangxin
 * 
 */
public class PangkerService extends Service implements IUICallBackInterface {

	private String TAG = "PangkerService";// log tag
	private static final Logger logger = LoggerFactory.getLogger();
	private final int DIR_SEARCH = 0x03;
	private final int BIND_CONTACT = 0x05;
	private final int UNBIND_CONTACT = 0x07;
	// 旁客 登录用户Application
	private VCardManager vCardManager;
	private PangkerApplication myApplication; // myapplication
	// 消息提示
	private SharedPreferencesUtil preferencesUtil;
	// 本机数据库操作类
	private IFansDao fansDao;
	private INoticeDao noticeDao;
	private IAttentsDao attentDao;
	private IFriendsDao friendsDao;
	private ILocalContactsDao localContactsDao;
	private INetAddressBookDao iNetAddressBookDao;
	private INetAddressBookUserDao iNetAddressBookUserDao;
	private IRecommendDao iRecommendDao;
	private IUserMsgDao iUserMsgDao;
	private ITipsDao tipsDao;
	private MessageBroadcast msgInfoBroadcast;
	private String myuserid;// 当前用户用户id
	private String password;// 密码

	private LocateManager locateManager;
	
	private Thread operateDBThread = null;
	// ************************************************************************************************//
	private UploadjobService jobService;
	private DownloadjobService downloadjobService;

	private ContactsUtil contactsUtil;

	private ServerSupportManager serverMana;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	// >>>登陆到服务器线程
	private ServiceListnenerThread mListnenerThread;
	private GuardListenerThread mGuardListenerThread;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.i(TAG, "onCreate");

		myApplication = (PangkerApplication) getApplication();// myapplication
		locateManager = new LocateManager(this);
		serverMana = new ServerSupportManager(this, this);
		startThread();
		fansDao = new FansDaoImpl(this);
		attentDao = new AttentsDaoImpl(this);
		noticeDao = new NoticeDaoImpl(this);

		msgInfoBroadcast = myApplication.getMsgInfoBroadcast();

		friendsDao = new FriendsDaoImpl(this);
		localContactsDao = new LocalContactsDaoImpl(this);
		iNetAddressBookDao = new NetAddressBookDaoImpl(this);
		iNetAddressBookUserDao = new NetAddressBookUserDaoImpl(this);
		iRecommendDao = new RecommendDaoImpl(this);
		iUserMsgDao = new UserMsgDaoImpl(this);
		tipsDao = new TipDaoImpl(this);
		preferencesUtil = new SharedPreferencesUtil(this);

		jobService = new UploadjobService(this);
		jobService.bindService(this);

		downloadjobService = new DownloadjobService();
		downloadjobService.bindService(this);

		myApplication = (PangkerApplication) getApplication();

		myuserid = myApplication.getMyUserID();
		password = myApplication.getMyPassWord();
		contactsUtil = new ContactsUtil(this, myuserid);
		vCardManager = new VCardManager(this);
		userListenerManager = PangkerManager.getContactUserListenerManager();
		externalStorageCheck();
		initLocaiton();
		initAutoLocationTask();
		addressBookBinging();

		initAutoWifiLocationTask();
	}

	private void externalStorageCheck() {
		StorageEventFilter intentFilter = new StorageEventFilter();
		// >>>>注册
		registerReceiver(broadcastReceiver, intentFilter);
	}

	// >>>>>>>>>>>标记当前SD卡状态
	private void updateExternalStorageState() {
		// 获取sdcard卡状态
		String state = Environment.getExternalStorageState();

		// >>>>>>>>>>>SD卡有效
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			myApplication.setmExternalStorageAvailable(true);
			myApplication.setmExternalStorageWriteable(true);
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			myApplication.setmExternalStorageAvailable(true);
			myApplication.setmExternalStorageWriteable(false);
		} else {
			showToast("SD卡无效，暂时不能使用语音图片消息、资源上传下载、音乐播放、网络图片浏览等功能!");
			myApplication.setmExternalStorageAvailable(false);
			myApplication.setmExternalStorageWriteable(false);
			myApplication.getDownLoadManager().stopAllDownloadingJobs();
		}
		myApplication.setmExternalStorageSetting(true);
	}

	// >>>>>>>>>sd卡状态监听
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			updateExternalStorageState();
		}
	};

	private void addressBookBinging() {
		// TODO Auto-generated method stub
		if (myApplication.isAddressBooksMacth() && !preferencesUtil.getBoolean(PangkerConstant.IS_BIND_KEY, false)) {
			ConfimNoticeDialog noticeDialog = new ConfimNoticeDialog(this);

			noticeDialog.setDialogTitle("通讯录匹配");
			noticeDialog.setConfimOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					contactsBind();
				}
			});
			noticeDialog.setCancelOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					contactsUnbind();
				}
			});
		}
	}

	/**
	 * TODO 检测旁客版本更新
	 * 
	 * @param ifShowToast
	 *            是否显示toast提示： 1.ifShowToast ==true ,用户手动检测,显示提示 ； 2.ifShowToast
	 *            ==false ,登录后台自动检测 ，无需提示进度。
	 * 
	 */
	private void checkPangkerVersion(final boolean ifShowToast) {
		// TODO Auto-generated method stub
		VersionUpdateCheck updateCheck = new VersionUpdateCheck(this, VersionCheckType.AUTO_CONTROL);
		updateCheck.versionUpdateCheck(false);
	}

	private void startThread() {
		// TODO Auto-generated method stub
		// >>>>>登陆线程启动
		myApplication.setOpenfireManager(new OpenfireManager());
		mListnenerThread = new ServiceListnenerThread();
		mListnenerThread.start();
		mGuardListenerThread = new GuardListenerThread();
		mGuardListenerThread.start();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		if (intent == null || intent.getExtras() == null)
			return;

		String serviceIntent = intent.getExtras().getString(PangkerConstant.SERVICE_INTENT);
		// >>>>>>>>手动定位意图
		if (PangkerConstant.LOCATE_INTENT.equals(serviceIntent)) {
			initLocaiton();
		}

		// >>>>>>>>>自定定位意图
		if (PangkerConstant.AUTOLOCATE_INTENT.equals(serviceIntent)) {
			initAutoLocationTask();
		}

		// >>>>>>>>>>>取消漂移
		if (PangkerConstant.INTENT_DRIFT.equals(serviceIntent)) {
			cancelDrift();
		}

		// >>>>检查登陆线程是否死掉
		if (PangkerConstant.LOGIN_IM_SERVER_THREAD_CHECK.equals(serviceIntent)) {
			checkServiceListnenerThread();
			checkGuardListenerThread();
		}
		if (PangkerConstant.INTENT_DRIFT_START.equals(serviceIntent)) {
			driftTime.schedule(new DrifTask(), sleepTime, sleepTime);
		}
		// >>>>版本检测
		if (PangkerConstant.INTENT_VERSION.equals(serviceIntent)) {
			checkPangkerVersion(false);
		}
		// >>>>初始化顶层目录
		if (PangkerConstant.INTENT_RES_INIT.equals(serviceIntent)) {
			initRootDir();
		}
		// 开启自动wifi定位
		if (PangkerConstant.AUTOWIFI_LACATE_INTENT.equals(serviceIntent)) {
			initAutoWifiLocationTask();
		}
		if (PangkerConstant.AUTOWIFI_LACATE_CLOSE_INTENT.equals(serviceIntent)) {
			android.os.Message message = new android.os.Message();
			message.what = 3;// 2表示自动Wifi定位，3表示取消自动Wifi定位
			handler.sendMessage(message);
		}
		// >>>>>>>>>>> 上传文件意图
		if (PangkerConstant.INTENT_UPLOAD.equals(serviceIntent)) {
			// >>>>>>>>>>>获取上传文件
			int jobId = intent.getIntExtra(UploadJob.UPLOAD_JOB_ID, 0);
			jobService.dealuploadJob(jobId, intent.getIntExtra(UploadJob.UPLOADJOB_TYPE, UploadJob.UPLOAD_START));
		}
		// >>>>>>>>>>>>>>下载文件意图
		if (PangkerConstant.INTENT_DOWNLOAD.equals(serviceIntent)) {

			// >>>>>>>>>>>获取下载任务
			int jobID = intent.getIntExtra(DownloadJob.DOWNLOAD_JOB_ID, -1);
			downloadjobService.dealDownloadJob(jobID);

		}
	}

	// >>>>>>>>>>>>>>>> wangxin presenceListener
	PacketListener presenceListener = new PacketListener() {
		@Override
		public void processPacket(Packet packet) {
			// TODO Auto-generated method stub
			presenceChanged((Presence) packet);
		}
	};
	// >>>>>>>>>>>>>>>> wangxin chatMessageListener
	PacketListener chatMessageListener = new PacketListener() {

		@Override
		public void processPacket(Packet packet) {
			// TODO Auto-generated method stub
			chatMessage((Message) packet);
		}
	};
	// >>>>>>>>>>>>>>>> wangxin businessMessageListener
	PacketListener businessMessageListener = new PacketListener() {
		@Override
		public void processPacket(Packet packet) {
			// TODO Auto-generated method stub
			Object property = packet.getProperty(PangkerConstant.BUSINESS_MESSAGE);
			if (property instanceof BusinessType) {

				new BusinessThread(handler2, (Message) packet).run();
			} else {
				SystemNoticeMessage((Message) packet);
			}
		}
	};

	// >>>>>>>>>>>>消息发送反馈监听
	PacketListener sendMessage = new PacketListener() {

		@Override
		public void processPacket(Packet packet) {
			// TODO Auto-generated method stub
			Message message = (Message) packet;

			Collection<PacketExtension> extens = message.getExtensions();
			for (PacketExtension p : extens) {
				if (p.getElementName().equals("received")) {
					// >>>>>>>接到message回复消息!!更新接收状态

					String messageID = message.getBody();
					boolean b = false;
					if (!Util.isEmpty(messageID)) {
						b = iUserMsgDao.messageReceived(messageID);
						PangkerManager.getUserMsgManager().chatMessageReceived(Integer.valueOf(messageID));
					}
					Log.i("MessageReceived", "received-messageID=" + message.getBody() + ";update=" + b);
					return;
				}
			}
		}

	};

	private Handler handler2 = new Handler();

	class BusinessThread extends Thread {
		Handler mHandler;
		Message packet;

		public BusinessThread(Handler handler, Message message) {
			this.mHandler = handler;
			this.packet = message;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			// >>>>>>>>>业务处理内容
			BusinessType businessType = (BusinessType) packet.getProperty(PangkerConstant.BUSINESS_MESSAGE);
			String rUserID = Util.trimUserIDDomain(packet.getFrom());

			UserItem rUserItem = vCardManager.getUserItem(rUserID);
			// 处理设置，是否发出声音和振动
			// 对方成为当前用户的fans
			if (businessType == BusinessType.addattent) {
				fansDao.saveFans(rUserItem);
				contactsUtil.addFanCount(1);
				userListenerManager.refreshUI(ContactFollowActivity.class.getName(),
						PangkerConstant.STATE_CHANGE_REFRESHUI);
				sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_ATTENT_ADD, rUserItem.getUserName() + "关注您!",
						MsgInfo.MSG_NOTICE);
			}
			// 对方取消对您的关注，不再是您的fans
			else if (businessType == BusinessType.delattent) {
				fansDao.removeFans(rUserID);
				contactsUtil.subFanCount(1);
				userListenerManager.refreshUI(ContactFollowActivity.class.getName(),
						PangkerConstant.STATE_CHANGE_REFRESHUI);
				sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_ATTENT_DEL, rUserItem.getUserName() + "取消对您的关注!",
						MsgInfo.MSG_NOTICE);
			}
			// 移除了您对TA的关注
			else if (businessType == BusinessType.delfans) {
				attentDao.delAttent(rUserID);
				contactsUtil.subAttentCount(1);
				userListenerManager.refreshUI(ContactAttentionActivity.class.getName(),
						PangkerConstant.STATE_CHANGE_REFRESHUI);
				sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_FANS_DEL, rUserItem.getUserName() + "移除了您对TA的关注!",
						MsgInfo.MSG_NOTICE);

			}
			// 对方将您删除好友，不再是好友关系（******
			// 在线时不需要，只有当当前用户不在线，没有收到删除好友的通知的情况下走此流程。）
			else if (businessType == BusinessType.delfriend) {
				// 对方将您删除好友，不再是好友关系（******
				// 在线时不需要，只有当当前用户不在线，没有收到删除好友的通知的情况下走此流程。）
				Log.i("friendChanged", "Presence.Type.delfriend 好友关系已经解除 :userid---" + rUserID);
				if (myApplication.IsFriend(rUserID)) {
					friendsDao.delFriend(rUserID, String.valueOf(PangkerConstant.DEFAULT_GROUPID));
					userListenerManager
							.refreshUI(ContactActivity.class.getName(), PangkerConstant.REFRESHUI_DEL_FRIEND);
					myApplication.removeTraceklist(rUserID);
					android.os.Message msg = new android.os.Message();
					msg.what = 1;
					PangkerManager.getTabBroadcastManager().sendResultMessage(RelativesDynamicActivity.class.getName(),
							msg);
					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_FRIEND_DELED, rUserItem.getUserName()
							+ "删除你们之间的好友关系!", MsgInfo.MSG_NOTICE);
				}
			}
			// 群组:其他用户邀请您加入
			else if (businessType == BusinessType.groupInvite) {
				String remark = String.valueOf(packet.getBody());
				String[] groupInfo = remark.split("#");
				String groupName = (groupInfo != null && groupInfo.length == 2) ? groupInfo[1] : "";
				sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_TRACE_REQUEST, "" + rUserItem.getUserName()
						+ "邀请您加入群组\"" + groupName + "\"!", remark, MsgInfo.MSG_INVITION);
			}
			// 群组:拒绝了您的邀请
			else if (businessType == BusinessType.groupResponse) {
				int result = Integer.parseInt(String.valueOf(packet.getProperty("result")));
				if (result == 1) {
					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_TRACE_RESPONSE, rUserItem.getUserName()
							+ ":应邀加入群组!", MsgInfo.MSG_NOTICE);
				} else {
					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_TRACE_RESPONSE, rUserItem.getUserName()
							+ ":拒绝了您的群组邀请 !", MsgInfo.MSG_NOTICE);
				}
			}
			// 单位通讯录:申请加入
			else if (businessType == BusinessType.applyNetAddressBookReq) {
				AddressBooks addressBooks = (AddressBooks) packet.getProperty(AddressBooks.BOOK_KEY);
				String remark = JSONUtil.toJson(addressBooks, false);
				sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_RESPONSE, rUserItem.getUserName()
						+ ":申请加入您创建的单位通讯录", remark, MsgInfo.MSG_INVITION);
			}
			// 单位通讯录:被删除
			else if (businessType == BusinessType.beDelNetAddressBook) {
				String addressId = packet.getSubject();
				iNetAddressBookDao.deleAddressBook(myuserid, addressId);
				iNetAddressBookUserDao.deleAddressMemberByBookid(addressId);
				Intent addressIntent = new Intent(PangkerConstant.ACTION_ADDRESSBOOK_EXIT);
				addressIntent.putExtra(AddressBooks.BOOK_ID_KEY, Long.parseLong(addressId));
				sendBroadcast(addressIntent);

				sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_DELETED, rUserItem.getUserName()
						+ ":把您移除出了Ta的单位通讯录", MsgInfo.MSG_NOTICE);
			}
			// 单位通讯录:退出申请
			else if (businessType == BusinessType.applyExitNetAddressBook) {
				AddressBooks addressBooks = (AddressBooks) packet.getProperty(AddressBooks.BOOK_KEY);
				String remark = JSONUtil.toJson(addressBooks, false);
				sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_APPLY_EXIT, rUserItem.getUserName()
						+ ":申请退出单位通讯录\"" + addressBooks.getName() + "\"", remark, MsgInfo.MSG_APPLY);
			}
			// 单位通讯录:被邀请
			else if (businessType == BusinessType.inviteNetAddressbookReq) {
				AddressBooks addressBooks = (AddressBooks) packet.getProperty(AddressBooks.BOOK_KEY);
				String remark = JSONUtil.toJson(addressBooks, false);
				sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_INVITE_RESPONSE, rUserItem.getUserName()
						+ ":邀请您加入单位通讯录\"" + addressBooks.getName() + "\"", remark, MsgInfo.MSG_INVITION);
			} else if (businessType == BusinessType.applyNetAddressBookResp) {
				String result = packet.getProperty("result").toString();
				AddressBooks addressBook = (AddressBooks) packet.getProperty(AddressBooks.BOOK_KEY);
				if (result.equals("1")) {
					iNetAddressBookDao.insertAddressBook(myuserid, addressBook);
					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_APPLY_AGRESS, rUserItem.getUserName()
							+ ":同意并把您加入了单位通讯录\"" + addressBook.getName() + "\"" + addressBook.getName(),
							MsgInfo.MSG_NOTICE);
				} else {
					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_APPLY_DEAGRESS,
							rUserItem.getUserName() + ":拒绝加入单位通讯录\"" + addressBook.getName() + "\"", MsgInfo.MSG_NOTICE);
				}

			}
			// 被邀请者同意了加入通讯录，将其加入自己通讯录的名单中
			else if (businessType == BusinessType.inviteNetAddressbookResp) {
				String result = packet.getProperty("result").toString();
				if ("1".equals(result)) {
					AddressMembers member = (AddressMembers) packet.getProperty(AddressMembers.ADDRESSMEMBER_KEY);
					// 将其加入自己通讯录的名单中 ,暂时没有memberId无法本地添加
					iNetAddressBookUserDao.insertAddressMember(member);

					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_INVITE_AGRESS, rUserItem.getUserName()
							+ ":同意加入您的单位通讯录", MsgInfo.MSG_NOTICE);
				} else {
					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_INVITE_REFUSE, rUserItem.getUserName()
							+ ":拒绝了加入您的单位通讯录", MsgInfo.MSG_NOTICE);
				}
			}
			// >>>>>>>>>同意/拒绝:您退出单位通讯录
			else if (businessType == BusinessType.applyExitNetAddressBookAgree) {
				String result = packet.getProperty("result").toString();
				AddressBooks addressBook = (AddressBooks) packet.getProperty(AddressBooks.BOOK_KEY);
				if (result.equals("1")) {
					iNetAddressBookDao.deleAddressBook(myuserid, String.valueOf(addressBook.getId()));
					iNetAddressBookUserDao.deleAddressMemberByBookid(String.valueOf(addressBook.getId()));
					Intent addressIntent = new Intent(PangkerConstant.ACTION_ADDRESSBOOK_EXIT);
					addressIntent.putExtra(AddressBooks.BOOK_ID_KEY, addressBook.getId().longValue());
					sendBroadcast(addressIntent);

					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_EXIT_AGREE, rUserItem.getUserName()
							+ ":同意您退出单位通讯录\"" + addressBook.getName() + "\"", MsgInfo.MSG_NOTICE);
				} else {
					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_NET_ADDRESS_EXIT_DEAGREE, rUserItem.getUserName()
							+ ":拒绝您退出单位通讯录\"" + addressBook.getName() + "\"", MsgInfo.MSG_NOTICE);
				}

			}
			// >>>>>>资源推荐
			else if (businessType == BusinessType.resRecommend) {
				String resId = packet.getProperty("result").toString();
				String msg = packet.getBody();
				final RecommendInfo recommendInfo = new RecommendInfo();
				recommendInfo.setFromId(rUserItem.getUserId());
				recommendInfo.setDatetime(Util.getSysNowTime());
				recommendInfo.setReason(msg);
				recommendInfo.setStatus(0);
				int type = Integer.parseInt(packet.getProperty("type").toString());
				recommendInfo.setType(type);
				recommendInfo.setFromName(rUserItem.getUserName());
				recommendInfo.setResId(resId);
				recommendInfo.setResName(packet.getSubject());
				String shareUid = packet.getProperty("shareUid").toString();
				recommendInfo.setShareUid(shareUid);
				recommendInfo.setUserId(myuserid);
				long id = iRecommendDao.saveRecommendInfo(recommendInfo);
				if (id > 0) {
					recommendInfo.setId((int) id);
					boolean isplay = false;

					// >>>>>>>>>>>如果是音乐资源并且设置默认打开
					if (recommendInfo.getType() == PangkerConstant.RES_MUSIC) {
						isplay = checkResPlay(rUserItem);
						// >>>>>>>启动音乐服务
						if (isplay) {
							List<MusicInfo> musicInfos = new ArrayList<MusicInfo>();
							MusicInfo musicInfo = new MusicInfo();
							musicInfo.setSid(Util.String2Long(recommendInfo.getResId()));
							musicInfo.setMusicName(recommendInfo.getResName());
							musicInfo.setSinger(recommendInfo.getSinger());
							musicInfos.add(musicInfo);
							myApplication.setMusiList(musicInfos);
							myApplication.setPlayMusicIndex(0);
							Intent intentSerivce = new Intent(PangkerService.this, MusicService.class);
							intentSerivce.putExtra("control", PlayControl.MUSIC_START_PLAY);
							startService(intentSerivce);
							// >>>>>>>>>>判断当前播放器界面是否在最上
							readRes(recommendInfo.getResId());
							Activity activity = PangkerManager.getActivityStackManager().isMusicPlayActivityShow();
							if (activity == null) {

								// >>>>>>>启动音乐播放界面
								final Intent intentActivity = new Intent(PangkerService.this, MusicPlayActivity.class);
								// >>>>>推荐启动音乐播放器
								intentActivity.putExtra(RecommendReceiverAcitvity.RES_OPEN_TYPE,
										RecommendReceiverAcitvity.RECOMMEND_OPEN);

								// >>>>>推荐人信息
								intentActivity.putExtra(MusicPlayActivity.RECOMMEND_INFO, recommendInfo);

								this.mHandler.post(new Runnable() {

									@Override
									public void run() {
										PangkerManager.getActivityStackManager().currentMainActivity()
												.startActivity(intentActivity);
									}
								});
							} else {
								((MusicPlayActivity) activity).refreshRecommendView(recommendInfo);
							}
						}
					}
					int count = isplay ? 0 : 1;
					msgInfoBroadcast.sendRecommendInfo(recommendInfo, count);
				}
			}
			// 碰一下业务
			else if (businessType == BusinessType.touch) {
				// 黑名单用户
				if (!myApplication.IsBlacklistExist(rUserID)) {
					ChatMessage msg = new ChatMessage();
					msg.setContent(rUserItem.getUserName() + "碰了你一下");
					msg.setDirection("1");
					msg.setMsgType(PangkerConstant.RES_TOUCH);
					msg.setMyUserId(myuserid);
					msg.setUserId(rUserID);
					msg.setUserName(rUserItem.getAvailableName());
					Intent intent = new Intent(PangkerService.this, MsgChatActivity.class);
					intent.putExtra(ChatMessage.USERID, rUserID);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

					// >>>>>>消息栏通知、保存消息到DB、声音+震动提示
					msgInfoBroadcast.sendChatUserMsg(msg, intent);
				}
			}

			// >>>>>>>>>>>>>消息提示通知
			else if (businessType == BusinessType.messagetip) {
				// >>>资源id
				String resId = packet.getProperty(OpenfireManager.OPENFIRE_NOTICE_FIELD_RESID).toString();
				String commentId = packet.getProperty(OpenfireManager.OPENFIRE_TIP_COMMENTID).toString();
				// >>>资源类型
				int resType = Util.String2Integer(packet.getProperty(OpenfireManager.OPENFIRE_NOTICE_FIELD_RESTYPE)
						.toString());
				int tipTpye = Util.String2Integer(packet.getProperty(OpenfireManager.OPENFIRE_TIP_TYPE).toString());
				final MessageTip messageTip = new MessageTip();
				messageTip.setUserId(myuserid);
				messageTip.setCommentID(commentId);
				messageTip.setUserName(myApplication.getMyUserName());
				messageTip.setFromId(rUserID);
				messageTip.setFromName(rUserItem.getUserName());
				messageTip.setResId(resId);
				messageTip.setTipType(tipTpye);
				messageTip.setResType(resType);
				messageTip.setResName(packet.getSubject());
				messageTip.setContent(packet.getBody());
				messageTip.setCreateTime(Util.getSysNowTime());

				StringBuffer messagetip = new StringBuffer(messageTip.getFromName());
				messagetip.append(" ");
				switch (messageTip.getTipType()) {
				case MessageTip.TIP_COMMENT_RES:
					messagetip.append("评论了");
					messagetip.append(" ");
					messagetip.append(messageTip.getResName());
					break;
				case MessageTip.TIP_COMMENT_BROADCAST_RES:
					messagetip.append("评论了您转播的");
					messagetip.append(" ");
					messagetip.append(messageTip.getResName());
					break;
				case MessageTip.TIP_COMMENT_REPLY:
					messagetip.append("回复您");
					break;
				default:
					break;
				}

				messagetip.append(":");
				messagetip.append(messageTip.getContent());
				messageTip.setMessagetip(messagetip.toString());
				long id = tipsDao.saveMessageTip(messageTip);
				if (id > 0) {
					messageTip.setId((int) id);
					msgInfoBroadcast.sendMessageTip(messageTip);
				}
			}
		}
	}

	private boolean checkResPlay(UserItem rUserItem) {
		int[] recommendIds = getResources().getIntArray(R.array.res_recommend_ids);
		int isOpenType = preferencesUtil.getInt(ResRecommendActivity.KEY_RECOMMEND_OPEN + myuserid, recommendIds[0]);
		// 是否是任何人的都播放(只能有好友)
		boolean isFriendOpen = isOpenType == recommendIds[1];
		boolean isQinyouOpen = false;
		if (!isFriendOpen) {
			// 亲友发送的资源自动打开
			isQinyouOpen = isOpenType == recommendIds[0] && myApplication.IsTracelistExist(rUserItem.getUserId());
		}
		return isQinyouOpen || isFriendOpen;
	}

	/**
	 * 
	 * @param message
	 * @author wangxin 2012-3-15 下午02:28:48
	 */
	private void chatMessage(Message message) {
		final XMPPConnection connection = myApplication.getConnection();
		String userid = message.getFrom();
		if (message.getError() != null) {
			if (message.getError().getCode() == 404) {
				// Check to see if the user is online to recieve this
				// message.
			}
			return;
		}
		// Do not accept Administrative messages.
		final String host = connection.getServiceName();
		if (host.equals(message.getFrom())) {
			return;
		}
		Log.i("chatMessage", "packet.getXmlns()=" + message.getXmlns() + "\nadd_username=" + userid);
		// If the message is not from the current agent. Append to chat.
		if (message.getBody() != null) {
			if (message != null) {
				ChatMessage msg = new ChatMessage();
				msg.setContent(message.getBody());// 内容
				msg.setDirection(ChatMessage.MSG_TO_ME);// 接受信息
				msg.setMsgType(PangkerConstant.RES_SPEAK);// >>>>文字会话
				if (message.getProperty("messagetype") != null) {
					Integer type = (Integer) message.getProperty("messagetype");
					msg.setMsgType(type);
				}
				if (message.getProperty("filepath") != null) {
					String filepath = message.getProperty("filepath").toString();
					msg.setFilepath(filepath);
					msg.setStatus(DownloadJob.DOWNLOAD_INIT);
				}
				msg.setMyUserId(myuserid);// 当前用户id
				msg.setUserId(Util.trimUserIDDomain(message.getFrom()));// 发送消息用户id
				UserItem user = vCardManager.getUserItem(msg.getUserId());
				if (user != null) {
					msg.setUserId(user.getUserId());
					msg.setIconType(user.getIconType());
					msg.setUserName(user.getAvailableName());
					msg.setSign(user.getSign());
				}
				// >>>>>>消息栏通知、保存消息到DB、声音+震动提示
				msgInfoBroadcast.sendChatUserMsg(msg, null);
			}
		}
	}

	/**
	 * 标识资源为已读
	 */
	private void readRes(String resId) {
		ServerSupportManager serverMana = new ServerSupportManager(PangkerService.this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object supportResponse, int caseKey) {
				// TODO Auto-generated method stub
				System.err.println();
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", myuserid));
		paras.add(new Parameter("resid", resId));
		paras.add(new Parameter("optype", "2"));
		serverMana.supportRequest(Configuration.getDeleteBoxRes(), paras);
	}

	private ContactUserListenerManager userListenerManager;

	/**
	 * 
	 * @param presence
	 * @author wangxin 2012-3-15 下午02:18:43
	 */
	private void presenceChanged(Presence presence) {
		final OpenfireManager openfireManager = myApplication.getOpenfireManager();

		final String userid = presence.getFrom();
		// >>>>>>>>>拒绝添加好友业务(对方拒绝了您的添加好友请求!!!)
		if (presence.getType() == Presence.Type.unsubscribe) {
			Log.i("friendChanged", "Presence.Type.unsubscribe:userid---" + userid);
			openfireManager.removeEntry(userid);
		}
		// >>>>>>>>>>>>>添加好友请求，对方添加您为好友
		else if (presence.getType() == Presence.Type.subscribe) {
			// >>>>>>判断是否在黑名单
			if (myApplication.IsBlacklistExist(Util.trimUserIDDomain(userid))) {
				// >>>>>如果是黑名单用户直接拒绝
				openfireManager.refuseFriendRequest(userid);
			} else {
				// >>>>>不是黑名单用户
				if (operateDBThread.isAlive()) {
					operateDBThread.yield();
					operateDBThread = null;
				}
				operateDBThread = new Thread(new Runnable() {
					@Override
					public void run() {
						RosterEntry entry = openfireManager.getConnection().getRoster().getEntry(userid);
						Log.i("friendChanged", "Presence.Type.subscribe 添加好友申请 -- :userid---" + userid + ";entry="
								+ entry);
						// >>>>>>判断您是否有过申请,如果有过,直接添加对方为好友
						if (entry != null) {
							openfireManager.sendPacketAdded(userid);
						}
						// >>>>>>如果没有提示当前用户，有其他用户添加您为好友!
						else {
							// TODO Auto-generated method stub
							UserItem item = vCardManager.getUserItem(Util.trimUserIDDomain(userid));
							sentNoticeBroadcast(item, NoticeInfo.NOTICE_FRIEND_ADD, item.getUserName() + "申请添加您为他的好友!",
									MsgInfo.MSG_APPLY);
						}
					}
				});
				operateDBThread.start();
			}
		}
		// >>>>>>>>>>>>>您和对方好友关系建立成功
		else if (presence.getType() == Presence.Type.subscribed) {
			Log.i("friendChanged", "Presence.Type.subscribed 好友关系建立成功 :userid---" + userid);
			openfireManager.sendPacketAdded(userid);
			new Thread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 添加至本地数据库（如果已经存在不会重复添加---wangxin）
					UserItem item = vCardManager.getUserItem(Util.trimUserIDDomain(userid));
					friendsDao.addFriend(item, String.valueOf(PangkerConstant.DEFAULT_GROUPID));
					userListenerManager.refreshUI(ContactActivity.class.getName(), PangkerConstant.REFRESHUI_ADD_FRIEND);

					ChatMessage msg = new ChatMessage();
					StringBuffer content = new StringBuffer("我们已经成为好友,我可能有权限呼叫您的电话、查看您的位置。具体权限在账号管理-权限设置中设置!");
					msg.setContent(content.toString());// 内容
					msg.setDirection(ChatMessage.MSG_TO_ME);// 接受信息
					msg.setMyUserId(myuserid);// 当前用户id
					msg.setMsgType(PangkerConstant.RES_SPEAK);// >>>>文字会话
					msg.setUserId(item.getUserId());
					msg.setUserName(item.getUserName());
					msg.setSign(item.getSign());

					// >>>>>>消息栏通知、保存消息到DB、声音+震动提示
					msgInfoBroadcast.sendChatUserMsg(msg, null);
				}
			}).start();

		}
		// >>>>>>>>>您和对方好友关系解除
		else if (presence.getType() == Presence.Type.unsubscribed) {
			Log.i("friendChanged", "Presence.Type.unsubscribed 好友关系已经解除 :userid---" + userid);
			// ,解除好友成功本地删除好友记录(如果删除成功，在客户端同样删除),同样要删除对应的亲友//modify by lb
			if (!myApplication.IsFriend(userid))
				return;
			new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					String memberUid = Util.trimUserIDDomain(userid);

					friendsDao.delFriend(memberUid, String.valueOf(PangkerConstant.DEFAULT_GROUPID));
					myApplication.removeTraceklist(memberUid);
					android.os.Message msg = new android.os.Message();
					msg.what = 1;
					PangkerManager.getTabBroadcastManager().sendResultMessage(RelativesDynamicActivity.class.getName(),
							msg);
					userListenerManager.refreshUI(ContactActivity.class.getName(), PangkerConstant.REFRESHUI_DEL_FRIEND);
					UserItem item = vCardManager.getUserItem(Util.trimUserIDDomain(userid));
					sentNoticeBroadcast(item, NoticeInfo.NOTICE_FRIEND_DEL, item.getUserName() + "解除和您的好友关系!",
							MsgInfo.MSG_NOTICE);
				}
			}).start();

		}
	}

	/**
	 * 后台服务器返回IM消息 void TODO
	 * 
	 * @param packet
	 */
	private void SystemNoticeMessage(final Message packet) {
		// TODO Auto-generated method stub
		final SystemNoticeType businessType = (SystemNoticeType) packet.getProperty(PangkerConstant.BUSINESS_MESSAGE);
		final String rUserID = packet.getProperty("RUserid").toString();
		final UserItem rUserItem = vCardManager.getUserItem(Util.trimUserIDDomain(rUserID));
		new Thread(new Runnable() {
			@Override
			public void run() {
				// 本地通讯录用户
				if (businessType == SystemNoticeType.contacts_register) {
					String phoneNum = (String) packet.getProperty(PangkerConstant.CONTACTS_REGISTER_PHONNUM);
					LocalContacts contact = localContactsDao.getLocalContact(phoneNum);// myApplication.getLocalContactByPhone(phoneNum);
					if (contact != null && contact.getUserName() != null) {
						rUserItem.setUserName(contact.getUserName());
					}
					localContactsDao.insertData(rUserID, phoneNum);
					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_CONTACTS_REGISTER, "你通讯录中有人注册了旁客，赶快加他为好友吧!",
							MsgInfo.MSG_NOTICE);
				}

				// 亲友邀请通信录成员注册后后台返回注册消息（成为亲友 关系）
				if (businessType == SystemNoticeType.traceRelativeRecommend) {
					String mobile = packet.getSubject();
					rUserItem.setPhoneNum(mobile);
					LocalContacts contact = localContactsDao.getLocalContact(mobile);// myApplication.getLocalContactByPhone(mobile);
					if (contact != null && contact.getUserName() != null) {
						rUserItem.setUserName(contact.getUserName());
					}
					sentNoticeBroadcast(rUserItem, NoticeInfo.NOTICE_TRACERELATIVE, "通讯录联系人接受你的邀请加入了旁客，并成为你的亲友",
							MsgInfo.MSG_INVITION);
				}
			}
		}).start();
	}

	// 发送消息广播 // 需要保存到本地数据库
	private void sentNoticeBroadcast(UserItem rUserItem, int nType, String nContent, int msgType) {
		sentNoticeBroadcast(rUserItem, nType, nContent, null, msgType);
	}

	// 发送消息广播 // 需要保存到本地数据库
	private void sentNoticeBroadcast(UserItem rUserItem, int nType, String nContent, String remark, int msgType) {
		if (Util.isEmpty(rUserItem.getUserId())) {
			Log.d(TAG, "NoticeBroadcast Error!");
			return;
		}
		NoticeInfo info = new NoticeInfo();
		info.setOpeUserid(rUserItem.getUserId());
		info.setType(nType);
		info.setUserId(myuserid);
		info.setNotice(nContent);
		info.setMsgType(msgType);
		info.setOpeUsername(rUserItem.getUserName());
		info.setStatus(0);// 未读取状态
		info.setTime(Util.getSysNowTime());
		if (remark != null) {
			info.setRemark(remark);
		}
		// 先插入sqlit表，好友删除的case要判断是否插入过，如果插入过，就不再插入数据，且不再发广播通知---待李博追加
		long id = noticeDao.saveNoticeInfo(info);
		if (id >= 0) {
			// 广播通知
			msgInfoBroadcast.sendNotice(info);
		}
	}

	// 网络状况判断
	public enum NetWork {
		no_network, has_network, error
	}

	// 状态
	public enum LoginStatus {
		no_connected, connected, no_logged, logged, error
	}

	private IBinder mBinder = new PangkerServiceBinder();

	public class PangkerServiceBinder extends Binder {

		public ILoginIMServerThreadManager getILoginIMServerThreadManager() {
			return mLoginIMServerThreadManager;
		}
	}

	private Timer autoLocationTimer;
	private TimerTask autoLocationTask;
	Handler handler = new Handler(new Callback() {
		@Override
		public boolean handleMessage(android.os.Message msg) {
			// TODO Auto-generated method stub
			// 自动定位
			if (msg.what == 1) {
				initLocaiton();
			}
			// 取消自动定位
			else if (msg.what == 0) {
				if (autoLocationTimer != null) {
					autoLocationTimer.cancel();
					autoLocationTimer = null;
				}
				if (autoLocationTask != null) {
					autoLocationTask.cancel();
					autoLocationTask = null;
				}
			} else if (msg.what == 2) {
				initWifiLocate();
			} else if (msg.what == 3) {
				closeWifiLocate();
			}
			return false;
		}
	});

	/**
	 * void TODO 如果用户在主页中设置了自动定位， 则启动计时器每隔10分钟自动定位一次
	 */
	private void initAutoLocationTask() {
		// 如果用户设置了自动定位
		boolean isAutoLocate = preferencesUtil.getBoolean(LOCATION_SERVICE + myuserid, true);
		if (isAutoLocate) {
			if (autoLocationTask != null) {
				autoLocationTask.cancel();
				autoLocationTask = null;
			}
			autoLocationTask = new TimerTask() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (!Util.isBackground(PangkerService.this)) {
						Log.i("locate", "not background!");
						boolean isAutoLocation = preferencesUtil.getBoolean(LOCATION_SERVICE + myuserid, true);
						android.os.Message message = new android.os.Message();
						message.what = isAutoLocation ? 1 : 0;
						handler.sendMessage(message);
					} else
						Log.i("locate", "background!");

				}
			};
			// 如果用户设置了自动定位，根据用户设置的时间间隔timeCount定位一次
			int timeCount = preferencesUtil.getInt(PangkerConstant.AUTO_LOCATE_TIME_KEY + myuserid, 10);
			if (autoLocationTimer != null) {
				autoLocationTimer.cancel();
				autoLocationTimer = null;
			}
			autoLocationTimer = new Timer();
			autoLocationTimer.schedule(autoLocationTask, 1000 * 60 * timeCount, 1000 * 60 * timeCount);

		}

	}

	private WifiLocateManager mWifiLocateManager;

	/**
	 * void TODO wifi自动定位同步
	 */
	private void initWifiLocate() {
		if (mWifiLocateManager == null) {
			mWifiLocateManager = new WifiLocateManager(this, new IUICallBackInterface() {
				@Override
				public void uiCallBack(Object supportResponse, int caseKey) {
					// TODO Auto-generated method stub
					Log.i("AutoWifiLocationTask", "collectResult=" + supportResponse);
				}
			});
		}
		mWifiLocateManager.collectUserWifiInfo(myuserid);
	}

	/**
	 * void TODO 关闭wifi自动定位数据同步
	 */
	private void closeWifiLocate() {
		if (autoWifiLocateTimer != null) {
			autoWifiLocateTimer.cancel();
			autoWifiLocateTimer = null;
		}
		if (autoWifiTask != null) {
			autoWifiTask.cancel();
			autoWifiTask = null;
		}
		if (mWifiLocateManager != null) {
			mWifiLocateManager.removeUserWifiInfo(myuserid);
		}
	}

	private Timer autoWifiLocateTimer;
	private TimerTask autoWifiTask;

	/**
	 * void TODO 初始化、开启自动wifi定位(同步)
	 */
	private void initAutoWifiLocationTask() {
		// 如果用户设置了自动wifi定位
		boolean isAutoWifiLocate = preferencesUtil.getBoolean(PangkerConstant.WIFI_LOCATE_KEY + myuserid, true);
		if (isAutoWifiLocate) {
			if (autoWifiTask != null) {
				autoWifiTask.cancel();
				autoWifiTask = null;
			}
			autoWifiTask = new TimerTask() {
				@Override
				public void run() {
					// TODO Auto-generated method stubrang
					Log.i("AutoWifiLocationTask", "collectWIFIInfos!");
					if (!Util.isBackground(PangkerService.this)) {
						boolean isAutoWifi = preferencesUtil.getBoolean(PangkerConstant.WIFI_LOCATE_KEY + myuserid,
								true);
						android.os.Message message = new android.os.Message();
						message.what = isAutoWifi ? 2 : 3;// 2表示同步Wifi定位信息，3表示取消自动Wifi定位
						handler.sendMessage(message);
					} else
						Log.i("locate", "background!");
				}
			};
			// 如果用户设置了自动wifi定位，根据用户设置的时间间隔timeCount定位一次，默认5分钟
			int timeCount = preferencesUtil.getInt(PangkerConstant.WIFI_AUTO_LOCATE_TIME_KEY + myuserid, 5);
			if (autoWifiLocateTimer != null) {
				autoWifiLocateTimer.cancel();
				autoWifiLocateTimer = null;
			}
			autoWifiLocateTimer = new Timer();
			autoWifiLocateTimer.schedule(autoWifiTask, 0, 1000 * 60 * timeCount);
		} else {
			android.os.Message message = new android.os.Message();
			message.what = 3;// 2表示同步Wifi定位信息，3表示取消自动Wifi定位
			handler.sendMessage(message);
		}

	}

	int logintime = 0;// 登录次数

	// >>>>>>>>>>>>>>>>> openfire登录守护线程
	public class ServiceListnenerThread extends Thread {

		private boolean isRun = true;
		private int sleeptime = 2000;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			// status = 0;
			while (isRun) {
				try {
					// >>>>>检查checkGuardListenerThread
					checkGuardListenerThread();

					// >>>>>>>>>>>IM服务器登录管理类
					final OpenfireManager ofManager = myApplication.getOpenfireManager();
					// 无网络情况client network
					if (!hasInternet()) {
						// >>>>>>>>>>>> add by wangxin
						if (myApplication.getiLoggedChecker() != null) {
							// >>>>>>>>通知用户没有网络无法登陆
							myApplication.getiLoggedChecker().noNetworkNotice();
						}
					}

					// >>>>>>>>>>有网络的case
					else {
						// openfire connection status : logined
						if (ofManager.isLoggedIn()) {
							if (myApplication.getiLoggedChecker() != null) {
								myApplication.getiLoggedChecker().loggedNotice();
							}
							Thread.sleep(5000);
							continue;
						}
						// >>>>>>>通知用户开始登陆IM服务器
						if (myApplication.getiLoggedChecker() != null) {
							myApplication.getiLoggedChecker().connectingNotice();
						}
						// states no logged
						if (!ofManager.isLoggedIn()) {

							// login to openfire
							if (loginToIM()) {

								// >>>>>通知登录成功
								if (myApplication.getiLoggedChecker() != null) {
									myApplication.getiLoggedChecker().loggedNotice();
								}
							} else {

								// >>>>>通知登录失败
								Log.i("ServiceListnenerThread", ">>>>>>>>>>login---fail!");
								if (myApplication.getiLoggedChecker() != null) {
									myApplication.getiLoggedChecker().noLoggedNotice();
								}
							}
						}
					}
					Thread.sleep(sleeptime);

				} catch (InterruptedException e) {
					logger.error(TAG, e);
					Log.i("ServiceListnenerThread", "error:" + e.getMessage());
					logger.error("ServiceListnenerThread", e);
				}
			}
		}

		public void stopRun() {
			// TODO Auto-generated method stub
			isRun = false;
		}
	}

	// >>>>>>>>>检查守护线程运行情况
	private void checkGuardListenerThread() {
		// >>>>>>>检查守护线程是否启动
		if (mGuardListenerThread == null) {
			mGuardListenerThread = new GuardListenerThread();
			mGuardListenerThread.start();
		} else if (!mGuardListenerThread.isAlive()) {
			// mGuardListenerThread.stop();
			mGuardListenerThread = new GuardListenerThread();
			mGuardListenerThread.start();
		}
	}

	// >>>>>>>>>>检查登陆线程运行情况
	private void checkServiceListnenerThread() {
		// >>>检查登陆线程是否启动
		if (mListnenerThread == null) {
			mListnenerThread = new ServiceListnenerThread();
			mListnenerThread.start();
		} else if (!mListnenerThread.isAlive()) {
			mListnenerThread = new ServiceListnenerThread();
			mListnenerThread.start();
		}
	}

	public boolean HttpResponseStatus(Object supportResponse) {
		if (supportResponse == null) {
			return false;
		}
		if (supportResponse instanceof HttpReqCode) {
			return false;
		} else {
			return true;
		}
	}

	// >>>>> 登录到IM服务器
	private boolean loginToIM() {
		// Log.i("ServiceListnenerThread", "loginToIM!");
		try {

			boolean isLogged = false;
			OpenfireManager ofManager = null;
			logintime++;
			// 判断是第几次登录，如果是第一次不需要重新初始化XMPPConnection
			if (logintime == 0) {
				ofManager = myApplication.getOpenfireManager();
				Log.i("ServiceListnenerThread", "logintoim--logintime=0");
			} else {
				ofManager = myApplication.getResetOpenfireManager(connectionListener);
				Log.i("ServiceListnenerThread", "logintoim--logintime>0");
			}
			// >>>>>>>>>判断是否连接到服务器
			if (!ofManager.isConnected()) {
				Log.i("ServiceListnenerThread", "logintoim--未连接");

				if (ofManager.connectOpenfire()) {
					Log.i("ServiceListnenerThread", "logintoim--连接success");
					if (ofManager.Login(myuserid, password)) {
						Log.i("ServiceListnenerThread", "logintoim--登陆success");
						// >>>>>>>>加载监听
						InitOpenfireListener(ofManager.getConnection());
						isLogged = true;
					} else {
						Log.i("ServiceListnenerThread", "logintoim--登陆Fail");
						ofManager.loginOut();
						myApplication.getMyUserID();
					}
				}
			}
			// >>>>>>>>>>>>连接到服务器，直接登陆即可
			else {
				if (ofManager.Login(myuserid, password)) {
					Log.i("ServiceListnenerThread", "logintoim--登陆success");
					InitOpenfireListener(ofManager.getConnection());
					isLogged = true;
				} else {
					Log.i("ServiceListnenerThread", "logintoim--登陆Fail");
					ofManager.loginOut();
				}
			}

			return isLogged;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	/**
	 * 启动openfire服务监听
	 */
	private void InitOpenfireListener(XMPPConnection connection) {
		try {
			// Packet 监听（目前包含message和presence）
			PacketFilter presencePacketFilter = new PacketTypeFilter(Presence.class);
			connection.addPacketListener(presenceListener, presencePacketFilter);

			PacketFilter chatmsgPacketFilter = new PacketChatmsgFilter(this);
			connection.addPacketListener(chatMessageListener, chatmsgPacketFilter);

			PacketFilter businessPacketFilter = new PacketBusinessFilter();
			connection.addPacketListener(businessMessageListener, businessPacketFilter);

			// >>>>>>>>add by wangxin 会话回执监听
			PacketFilter packetFilter = new MessageReceivedFilter();
			connection.addPacketListener(sendMessage, packetFilter);

			// 设置好友添加方式
			final Roster roster = connection.getRoster();
			roster.setSubscriptionMode(Roster.SubscriptionMode.manual);

			// 断线（网络监听）
			connection.addConnectionListener(connectionListener);
			// 用户状态监听
			// roster.addRosterListener(rosterListener);

		} catch (Exception e) {
			logger.error(TAG, e);
		}
	}

	ConnectionListener connectionListener = new ConnectionListener() {

		@Override
		public void connectionClosed() {
			// TODO Auto-generated method stub
			Log.d("ServiceListnenerThread", "connectionClosed");

			if (myApplication.getiLoggedChecker() != null) {
				myApplication.getiLoggedChecker().noLoggedNotice();
			}

			if (myApplication.getOpenfireManager() != null) {
				myApplication.getOpenfireManager().getConnection().removeConnectionListener(connectionListener);
			}
		}

		@Override
		public void connectionClosedOnError(Exception exception) {
			// TODO Auto-generated method stub
			Log.d("ServiceListnenerThread", "connectionClosedOnError");

			if (myApplication.getiLoggedChecker() != null) {
				myApplication.getiLoggedChecker().noLoggedNotice();
			}
			if (myApplication.getOpenfireManager() != null) {
				myApplication.getOpenfireManager().getConnection().removeConnectionListener(connectionListener);
			}

		}

		@Override
		public void reconnectingIn(int i) {

			Log.d("ServiceListnenerThread", "reconnectingIn:-->" + i);
		}

		@Override
		public void reconnectionFailed(Exception exception) {
			// TODO Auto-generated method stub

			Log.d("ServiceListnenerThread", "reconnectionFailed:-->" + exception);
		}

		@Override
		public void reconnectionSuccessful() {
			// TODO Auto-generated method stub
			Log.d(TAG, "reconnectionSuccessful");
			if (myApplication.getiLoggedChecker() != null) {
				myApplication.getiLoggedChecker().loggedNotice();
			}
		}
	};

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i(TAG, ">>>>onDestroy");
		unregisterReceiver(broadcastReceiver);
		jobService.destory();
		downloadjobService.destory();
		try {
			this.stopSelf();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private boolean hasInternet() {
		return ConnectivityHelper.isConnectivityAvailable(this);
	}

	// 这里可以对定位进行广播，定位的状态status包含了定位的状态。
	// 可以对此发送广播
	public void initLocaiton() {
		try {
			if (locateManager.isLocating()) {
				return;
			}
			locateManager.onLocate(new LocateManager.LocateListener() {
				@Override
				public void onLocationChanged(Location location, int locateResult, LocateType locateType) {
					// TODO Auto-generated method stub
					locateManager.disableMyLocation();
					if (location != null && myApplication.getLocateType() != PangkerConstant.LOCATE_DRIFT) {
						myApplication.setCurrentLocation(location);
						uploadLocation(location);
					}
					sendLoactionBroadCast(location, locateResult);
				}
			});
		} catch (Exception e) {
		}
	}

	private void sendLoactionBroadCast(Location location, int status) {
		Intent intent = new Intent(PangkerConstant.ACTTION_LOCATE_STATE);
		intent.putExtra("Location", location);
		intent.putExtra(PangkerConstant.ACTTION_LOCATE_STATE, status);
		sendBroadcast(intent);
	}

	/**
	 * 上传用户位置信息
	 * 
	 */
	public void uploadLocation(com.wachoo.pangker.entity.Location location) {
		SyncHttpClient http = new SyncHttpClient();
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", myuserid));
		paras.add(new Parameter("lon", String.valueOf(location.getLongitude())));
		paras.add(new Parameter("lat", String.valueOf(location.getLatitude())));
		Log.i("location", "upload location - " + location.toJson());
		try {
			http.httpPost(Configuration.getUpLocation(), paras, myApplication.getServerTime());
		} catch (Exception e) {
			Log.d(TAG, e.getMessage());
		}
	}

	private String phones;

	private void contactsBind() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));
		paras.add(new Parameter("uid", myApplication.getMyUserID()));
		phones = localContactsDao.getAllPhoneNumbers();
		paras.add(new Parameter("phones", phones.toString()));
		if (phones == null || phones.length() == 0) {
			showToast(R.string.phonenumber_unbind_noneed_message);
			return;
		}
		serverMana.supportRequest(Configuration.getAddressBookBinding(), paras, BIND_CONTACT);
	}

	// 取消漂移
	private void cancelDrift() {
		SyncHttpClient http = new SyncHttpClient();
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", myApplication.getMyUserID()));
		try {
			String canceldriftResult = http.httpPost(Configuration.getCancelDrift(), paras,
					myApplication.getServerTime());
			BaseResult result = JSONUtil.fromJson(canceldriftResult, BaseResult.class);
			if (result == null) {
				return;
			}
			if (result.getErrorCode() == BaseResult.SUCCESS) {
				myApplication.setLocateType(PangkerConstant.LOCATE_CLIENT);// 取消漂移状态,重新开始定位
				myApplication.setDriftUser(null);
				android.os.Message msg = new android.os.Message();
				msg.what = PangkerConstant.DRIFT_NOTICE;
				PangkerManager.getTabBroadcastManager().sendResultMessage(MainActivity.class.getName(), msg);
				PangkerManager.getTabBroadcastManager().sendResultMessage(HomeActivity.class.getName(), msg);
				// >>>>>>>>>>>>重新定位
				initLocaiton();
			}
		} catch (Exception e) {
			Log.d(TAG, e.getMessage());
		}

	}

	private void contactsUnbind() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));
		paras.add(new Parameter("uid", myApplication.getMyUserID()));
		serverMana.supportRequest(Configuration.getAddressBookUNBinding(), paras, UNBIND_CONTACT);
	}

	// 解绑操作
	private void unBindResult(PhoneBookResult bookResult) {
		preferencesUtil.saveBoolean(PangkerConstant.IS_BIND_KEY, false);
		localContactsDao.deletePUsers("");

		showToast(R.string.phonenumber_unbind_sucess_message);
		myApplication.setLocalContacts(localContactsDao.initLocalAddressBooks());
		if (myApplication.getAddressBookBind() != null)
			myApplication.getAddressBookBind().refreshAddressBook();
	}

	public void showToast(final int toastText) {
		Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
	}

	public void showToast(String toastText) {
		Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
	}

	// 绑定操作
	private void bindResult(PhoneBookResult bookResult) {
		localContactsDao.notifyPUsers(bookResult.getPks());
		preferencesUtil.saveBoolean(PangkerConstant.IS_BIND_KEY, true);

		showToast(R.string.phonenumber_bind_sucess_message);
		myApplication.setLocalContacts(localContactsDao.initLocalAddressBooks());
		if (myApplication.getAddressBookBind() != null)
			myApplication.getAddressBookBind().refreshAddressBook();
	}

	Timer timingLocateTime = new Timer();

	// >>>>登陆线程守护类
	private class GuardListenerThread extends Thread {
		// >>>>>>是否要继续进行
		private boolean isRun = true;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while (isRun) {
				try {
					Thread.sleep(30000);
					checkServiceListnenerThread();
				} catch (Exception e) {
					// TODO: handle exception
					Log.e("ServiceListnenerThread", e.getMessage());
				}
			}
		}

		public void stopRun() {
			// TODO Auto-generated method stub
			isRun = false;
		}
	}

	Timer driftTime = new Timer();
	private long sleepTime = 1000 * 60 * 5;

	class DrifTask extends TimerTask {
		@Override
		public void run() {
			try {
				if (myApplication.getLocateType() != PangkerConstant.LOCATE_DRIFT) {
					if (driftTime != null) {
						driftTime.cancel();
					}
				} else {
					SyncHttpClient http = new SyncHttpClient();
					List<Parameter> paras = new ArrayList<Parameter>();
					paras.add(new Parameter("uid", myuserid));
					String driftResult = http.httpPost(Configuration.getDriftLocationQuery(), paras,
							myApplication.getServerTime());
					LocationQueryResult result = JSONUtil.fromJson(driftResult, LocationQueryResult.class);
					if (result != null) {
						if (result.getErrorCode() == BaseResult.SUCCESS) {
							if (result.getLat() != null && !result.getLat().equals("") && result.getLon() != null
									&& !result.getLon().equals("")) {
								myApplication.setCurrentLocation(new Location(Double.parseDouble(result.getLon()),
										Double.parseDouble(result.getLat())));
								uploadLocation(myApplication.getCurrentLocation());
							} else {
								cancelDrift();
							}
						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error(TAG, e);
			}
		}
	}

	private String getPathByIndex(int type) {
		if (PangkerConstant.RES_APPLICATION == type) {
			return PangkerConstant.DIR_APP;
		}
		if (PangkerConstant.RES_DOCUMENT == type) {
			return PangkerConstant.DIR_DOCUMENT;
		}
		if (PangkerConstant.RES_PICTURE == type) {
			return PangkerConstant.DIR_PICTURE;
		}
		if (PangkerConstant.RES_MUSIC == type) {
			return PangkerConstant.DIR_MUSIC;
		}
		return null;
	}

	// 将返回的信息初始化到本地
	private void initDirInfo(String resResponse) {
		IResDao iResDao = new ResDaoImpl(this);
		ResDirQueryResult result = JSONUtil.fromJson(resResponse, ResDirQueryResult.class);
		if (result != null && BaseResult.SUCCESS == result.errorCode) {
			List<DirInfo> dirinfos = result.getDirinfos();
			if (dirinfos == null) {
				return;
			}
			for (DirInfo dirInfo : dirinfos) {
				dirInfo.setUid(myuserid);
				dirInfo.setIsfile(0);
				dirInfo.setPath(getPathByIndex(dirInfo.getResType()));
				if (iResDao.saveResInfo(dirInfo) > 0) {
					if (dirInfo.getPath() == null) {
						continue;
					}
					File resFile = new File(dirInfo.getPath());
					if (!resFile.exists()) {
						resFile.mkdirs();
					}
				}
			}
			myApplication.setRootDirs(dirinfos);
			// 进行会叫，通知顶层目录加载完成
			android.os.Message msg = new android.os.Message();
			msg.what = 9;
			PangkerManager.getTabBroadcastManager().sendResultMessage(UploadAppActivity.class.getName(), msg);
			PangkerManager.getTabBroadcastManager().sendResultMessage(UploadDocActivity.class.getName(), msg);
			PangkerManager.getTabBroadcastManager().sendResultMessage(UploadMusicActivity.class.getName(), msg);
			PangkerManager.getTabBroadcastManager().sendResultMessage(UploadPicActivity.class.getName(), msg);
			PangkerManager.getTabBroadcastManager().sendResultMessage(ResNetManagerActivity.class.getName(), msg);
		}
	}

	// 初始化本地目录
	private void initRootDir() {
		// TODO Auto-generated method stub
		Log.d(TAG, "initRootDir, send");
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appuid", "0"));
		paras.add(new Parameter("uid", myuserid));
		paras.add(new Parameter("dirid", "0"));
		paras.add(new Parameter("limit", "0,4"));
		serverMana.supportRequest(Configuration.getResDirQueryByDir(), paras, DIR_SEARCH);
	}

	private ILoginIMServerThreadManager mLoginIMServerThreadManager = new ILoginIMServerThreadManager() {
		@Override
		public void startThreadMonitor() {
			// TODO Auto-generated method stub
			synchronized (mGuardListenerThread) {
				mGuardListenerThread = new GuardListenerThread();
				mGuardListenerThread.start();
			}
			synchronized (mListnenerThread) {
				mListnenerThread = new ServiceListnenerThread();
				mListnenerThread.start();
			}
		}

		@Override
		public void stopThreadMonitor() {
			// TODO Auto-generated method stub
			// >>>>登陆监控线程
			synchronized (mGuardListenerThread) {
				if (mGuardListenerThread != null && mGuardListenerThread.isAlive())
					mGuardListenerThread.stopRun();
			}

			// >>>>保持在线线程
			synchronized (mListnenerThread) {
				if (mListnenerThread != null && mListnenerThread.isAlive())
					mListnenerThread.stopRun();
			}
		}

		@Override
		public void reStartThreadMonitor() {
			// TODO Auto-generated method stub
			// >>>>登陆监控线程
			synchronized (mGuardListenerThread) {
				if (mGuardListenerThread != null && mGuardListenerThread.isAlive())
					mGuardListenerThread.stopRun();
				// >>>>重新初始化
				mGuardListenerThread = new GuardListenerThread();
				mGuardListenerThread.start();
			}
			// >>>>保持在线线程
			synchronized (mListnenerThread) {

				if (mListnenerThread != null && mListnenerThread.isAlive())
					mListnenerThread.stopRun();
				// >>>>重新初始化
				mListnenerThread = new ServiceListnenerThread();
				mListnenerThread.start();
			}
		}
	};

	/*
	 * 以下代码是用户在线状态管理，暂时不需要了。
	 */
	private RosterListener rosterListener = new RosterListener() {
		// Called when the presence of a roster entry is changed；监听好友信息的更改状态
		@Override
		public void presenceChanged(Presence presence) {
			// TODO Auto-generated method stub
			Log.i("friendonlinestate", "changed:userid ---" + presence.getFrom());
			int iState;
			if (presence.getType() == Presence.Type.available) {
				iState = PangkerConstant.STATE_TYPE_ONLINE;
				refreshFriendUI(Util.trimUserIDDomain(presence.getFrom()), iState);

			} else if (presence.getType() == Presence.Type.unavailable) {
				iState = PangkerConstant.STATE_TYPE_OFFLINE;
				refreshFriendUI(Util.trimUserIDDomain(presence.getFrom()), iState);

			} else {
				iState = PangkerConstant.STATE_TYPE_NOTHING;
			}
			Log.i("presenceChanged", "userid = " + presence.getFrom() + "; istate = " + presence.getTo());
		}

		// Called when a roster entries are updated
		@Override
		public void entriesUpdated(Collection<String> collection) {
			// TODO Auto-generated method stub
			String user = "";
			Iterator<String> it = collection.iterator();
			if (it.hasNext()) {
				user = it.next();
				Log.i("entriesUpdated", "好友状态表更：entriesUpdated:userid--" + user);
			}
		}

		// Called when a roster entries are removed.
		@Override
		public void entriesDeleted(Collection<String> collection) {
			// TODO Auto-generated method stub
			String user = "";
			Iterator<String> it = collection.iterator();
			if (it.hasNext()) {
				user = it.next();
				Log.i(TAG, "好友删除申请成功发送：entriesDeleted:userid--" + user);
			}
		}

		// Called when roster entries are added.监听openfire服务器是否成功接收添加好友请求。
		@Override
		public void entriesAdded(Collection<String> collection) {
			// TODO Auto-generated method stub
			String user = "";
			Iterator<String> it = collection.iterator();
			if (it.hasNext()) {
				user = it.next();
				Log.i(TAG, "添加好友申请成功发送：entriesAdded:userid--" + user);
			}
		}
	};

	private void refreshFriendUI(String sWho, int iState) {
		// TODO Auto-generated method stub
		Log.d("Lineservice", "UserId:" + Util.trimUserIDDomain(sWho) + "||State" + iState);
		for (int i = 0; i < myApplication.getFriendList().size(); i++) {
			if (Util.trimUserIDDomain(sWho).equals(myApplication.getFriendList().get(i).getUserId())) {
				myApplication.getFriendList().get(i).setState(iState);
				userListenerManager.refreshUI(ContactActivity.class.getName(), PangkerConstant.STATE_CHANGE_REFRESHUI);
				userListenerManager.refreshUI(MsgChatActivity.class.getName(), PangkerConstant.STATE_CHANGE_REFRESHUI);
				break;
			}
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response))
			return;
		if (caseKey == DIR_SEARCH) {
			Log.d(TAG, "initRootDir, response");
			initDirInfo(response.toString());
		}
		if (caseKey == BIND_CONTACT) {
			PhoneBookResult bookResult = JSONUtil.fromJson(response.toString(), PhoneBookResult.class);
			if (bookResult == null || bookResult.getErrorCode() != BaseResult.SUCCESS) {
				showToast(R.string.operate_fail);
				return;
			}
			bindResult(bookResult);
		}
		if (caseKey == UNBIND_CONTACT) {
			PhoneBookResult bookResult = JSONUtil.fromJson(response.toString(), PhoneBookResult.class);
			if (bookResult == null || bookResult.getErrorCode() != BaseResult.SUCCESS) {
				showToast(R.string.operate_fail);
				return;
			}
			unBindResult(bookResult);
		}
	}

}
