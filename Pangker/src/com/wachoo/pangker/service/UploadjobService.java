package com.wachoo.pangker.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Service;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.SqliteMusicQuery;
import com.wachoo.pangker.db.UploadDao;
import com.wachoo.pangker.db.impl.UploadDaoImpl;
import com.wachoo.pangker.downupload.UpLoadManager;
import com.wachoo.pangker.entity.LocalAppInfo;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.receiver.NetChangeReceiver;
import com.wachoo.pangker.receiver.NetChangeReceiver.OnNetChangeListener;
import com.wachoo.pangker.server.HttpReqCode;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UploadResult;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.util.ConnectivityHelper;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * @author zxx 将所有的UploadJob都扔到这里进行处理，
 */
public class UploadjobService implements OnNetChangeListener {

	private final String Tag = "UploadjobService";
	private Context mService;
	private SqliteMusicQuery mSqliteMusicQuery;
	private UploadDao mUploadDao;
	private UpLoadManager mUpLoadManager;
	private PangkerApplication myApplication;
	private NetChangeReceiver mChangeReceiver;
	private MessageTipDialog netDialog;
	private String myUserId;
	private SharedPreferencesUtil spu;
	
	//Build.MODEL

	public UploadjobService(Context mService) {
		// TODO Auto-generated constructor stub
		this.mService = mService;
		myApplication = (PangkerApplication) mService.getApplicationContext();
		myUserId = myApplication.getMyUserID();
		mUploadDao = new UploadDaoImpl(mService);
		mSqliteMusicQuery = new SqliteMusicQuery(mService);
		mUpLoadManager = myApplication.getUpLoadManager();
		mChangeReceiver = new NetChangeReceiver();
		spu = new SharedPreferencesUtil(mService);
	}

	public void bindService(Service mService) {
		// TODO Auto-generated method stub
		mChangeReceiver.registerNetChangeReceiver(mService, this);
	}

	@Override
	public void onNetStatus(boolean connect) {
		// TODO Auto-generated method stub
		if (connect) {
			startWaitingJob();
		}
	}

	private void startWaitingJob() {
		// TODO Auto-generated method stub
		for (UploadJob job : mUpLoadManager.getmUploadJobs()) {
			if (job.getUploadType() == UploadJob.UPLOAD_WAIT) {
				severFileCheck(job, UploadJob.UPLOAD_WAIT);
			}
		}
	}

	public void destory() {
		// TODO Auto-generated method stub
		mService.unregisterReceiver(mChangeReceiver);
		mChangeReceiver = null;
	}

	/**
	 * 上传处理的唯一入口
	 * 
	 * @param job
	 */
	public void dealuploadJob(int jobId, int job_type) {
		if (jobId > 0) {
			Log.d(Tag, "dealuploadJob-->" + jobId);
			UploadJob uploadJob = mUpLoadManager.getUploadJobByID(jobId);
			severFileCheck(uploadJob, job_type);
		}
	}

	private void severFileCheck(final UploadJob job, final int uploadCode) {
		File file = new File(job.getFilePath());
		String format = Util.getFileformat(job.getFilePath());
		if (file.exists()) {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", myUserId));
			paras.add(new Parameter("md5", job.getMd5Value())); // 文件md5值
			switch (job.getType()) {
			case PangkerConstant.RES_APPLICATION:
				paras.add(new Parameter("fileformat", "apk")); // 文件md5值
				break;
			case PangkerConstant.RES_MUSIC:
			case PangkerConstant.RES_PICTURE:
			case PangkerConstant.RES_DOCUMENT:
				paras.add(new Parameter("fileformat", format));
				break;
			}
			// 文件大小（手机客户端获取文件大小，用于校验用户是否具备上传权限，如会员用户才可上传大于20M的资源）
			paras.add(new Parameter("filesize", String.valueOf(file.length())));// int
			ServerSupportManager serverMana = new ServerSupportManager(mService, new IUICallBackInterface() {
				@Override
				public void uiCallBack(Object response, int caseKey) {
					Log.d(Tag, "Check-->" + response.toString());
					if (response instanceof HttpReqCode) {
						// 如果检测失败，便放到上传队列中去
						intoWaitUploadJobsQuee(job);
						return;
					}
					BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
					//已经上传过了
					if (result != null && (result.errorCode == 2)) {
						showToast(result.errorMessage);
						mUpLoadManager.removeCompleteJob(job);
					}
					//没有上传过
					else if (result != null && (result.errorCode == 0 || result.errorCode == 1)) {
						job.setFlagCheckFileExist(result.errorCode);
						checkUploadSet(job, uploadCode);
					} else {
						// 如果检测失败，便放到上传队列中去
						intoWaitUploadJobsQuee(job);
					}
				}
			});
			serverMana.supportExpRequest(Configuration.getUploadCheck(), paras, 0);
		} else {
			showToast(R.string.res_get_file_failed);
		}
	}

	private void intoWaitUploadJobsQuee(UploadJob job) {
		// TODO Auto-generated method stub
		Log.d(Tag, "intoWaitUploadJobsQuee-->");
		showToast("由于网络原因暂时无法上传，已存放到上传列表!");
		job.setUploadType(UploadJob.UPLOAD_WAIT);
		mUploadDao.updateUploadInfo(job);
		mUpLoadManager.addUploadJob(job);
	}

	// >>>>>>>>文件结束上传
	private void updateOverUploadInfo(UploadJob uploadInfo, int status, String resId) {
		Log.d("DownloadService", "==>status :" + status + "resId:" + resId);
		if (!Util.isEmpty(resId)) {
			uploadInfo.setResId(resId);
		}
		uploadInfo.setStatus(status);
		mUploadDao.updateUploadInfo(uploadInfo);
		// >>>>文件上传处理结束
		mUpLoadManager.startListenterAction();
	}

	protected void checkUploadSet(final UploadJob job, final int uploadCode) {
		// 如果是Wifi网络，直接上传
		if (ConnectivityHelper.getAPNType(mService) == ConnectivityHelper.WIFI) {
			startUpload(job, uploadCode);
		}
		// 如果不是wifi网络
		else if (ConnectivityHelper.getAPNType(mService) == ConnectivityHelper.CMNET
				|| ConnectivityHelper.getAPNType(mService) == ConnectivityHelper.CMWAP) {
			// 检测用户设置,如果允许在2/3G网络下允许上传
			if (spu.getInt(PangkerConstant.SP_UPLOAD_SET_NTE + myUserId, 0) == 1) {
				startUpload(job, uploadCode);
			}
			// 2/3G网络进行询问
			else if (spu.getInt(PangkerConstant.SP_UPLOAD_SET_NTE + myUserId, 0) == 0) {

				netDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-generated method stub
						if (isSubmit) {
							startUpload(job, uploadCode);
						} else {
							intoWaitUploadJobsQuee(job);
						}
					}
				}, mService);

				netDialog
						.showDialog("",
								mService.getResources().getString(R.string.net_prompt_upload) + "\"" + job.getResName()
										+ "\"?");
			}
			// 在非wifi网络下提示用户，放入等待队列中
			else {
				intoWaitUploadJobsQuee(job);
			}
		}
	}

	// >>>>>>>>>>>上传文件开始
	private void startUpload(final UploadJob uploadJob, int uploadCode) {
		Log.d(Tag, "startUpload-->");

		if (uploadCode == UploadJob.UPLOAD_START)
			showToast("\"" + uploadJob.getResName() + "\"资源开始上传!");
		else if (uploadCode == UploadJob.UPLOAD_WAIT)
			showToast("等待上传的资源\"" + uploadJob.getResName() + "\"开始上传!");

		// >>>>>>>设置文件开始上传标记
		uploadJob.setStatus(UploadJob.UPLOAD_UPLOAGINDG);
		uploadJob.setUploadType(UploadJob.UPLOAD_START);
		uploadJob.setUpLoadManager(mUpLoadManager);
		// >>>>>更新标记到db
		mUploadDao.updateUploadInfo(uploadJob);

		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", myUserId));
		paras.add(new Parameter("name", uploadJob.getResName()));// 逻辑名称，若为空则根据filename获取文件名称
		paras.add(new Parameter("type", uploadJob.getResType())); // 资源细分类，如流行，古典…
		paras.add(new Parameter("ifshare", uploadJob.getIfShare())); // 是否分享(0：不分享
																		// 1：分享)
		paras.add(new Parameter("filename", uploadJob.getFilePath()));
		paras.add(new Parameter("fileformat", uploadJob.getFileFormat()));
		paras.add(new Parameter("dirid", uploadJob.getDirId()));
		if (!Util.isEmpty(uploadJob.getCtrlat())) {
			paras.add(new Parameter("ctrlat", uploadJob.getCtrlat())); // 23.11747
		}
		if (!Util.isEmpty(uploadJob.getCtrlon())) {
			paras.add(new Parameter("ctrlon", uploadJob.getCtrlon())); // 113.363457
		}
		paras.add(new Parameter("md5", uploadJob.getMd5Value()));
		paras.add(new Parameter("filesize", String.valueOf(uploadJob.getFileSize())));
		paras.add(new Parameter("desc", uploadJob.getDesc()));// 详细描述
		if (!Util.isEmpty(uploadJob.getGroupId())) {
			paras.add(new Parameter("groupId", uploadJob.getGroupId()));// 群组Id
		}
		// >>>>>>>>>如果上传的是应用文件,需要上传应用的图标和包名
		if (uploadJob.getType() == PangkerConstant.RES_APPLICATION) {
			LocalAppInfo mAppInfo = Util.getApkFileInfo(mService, uploadJob.getFilePath());
			paras.add(new Parameter("packageinfo", mAppInfo.getAppPackage()));// 应用包
		}
		// >>>>>>>>>如果上传的是音乐文件,需要上传音乐的专辑封面
		else if (uploadJob.getType() == PangkerConstant.RES_MUSIC) {
			paras.add(new Parameter("author", uploadJob.getAuthor()));
			int duration = mSqliteMusicQuery.getDruation(uploadJob.getMusicId());
			Log.d("duration", "duration:" + duration);
			paras.add(new Parameter("duration", String.valueOf(duration)));// 播放长度
		} 
		else if (uploadJob.getType() == PangkerConstant.RES_DOCUMENT) {
			paras.add(new Parameter("author", uploadJob.getAuthor()));
		}
		//上传地址信息
		if(!Util.isEmpty(uploadJob.getUpPoi())){
			Log.d(Tag, "upPoi==>" + uploadJob.getUpPoi());
			paras.add(new Parameter("upPoi", uploadJob.getUpPoi()));
		}
        if(!Util.isEmpty(uploadJob.getRealPoi())){
        	Log.d(Tag, "realPoi==>" + uploadJob.getRealPoi());
        	paras.add(new Parameter("realPoi", uploadJob.getRealPoi()));
		}
		//参数添加，手机型号，
		paras.add(new Parameter("uplaodModel", Build.MODEL));//终端型号
		paras.add(new Parameter("uploadNumber", ""));//终端号码
		int net = ConnectivityHelper.getAPNType(mService);
		paras.add(new Parameter("uploadNet", String.valueOf(net)));//终端网络类型
		try {
			// >>>>>>封面图标
			byte[] coverByte = null;
			// >>>>>>>上传文件流
			MyFilePart uploader = null;

			// >>>>>>>文件是否存在
			if (uploadJob.getFlagCheckFileExist() == 0) {
				// >>>>>>>>>如果上传的是应用文件,需要上传应用的图标和包名
				if (uploadJob.getType() == PangkerConstant.RES_APPLICATION) {
					LocalAppInfo mAppInfo = Util.getApkFileInfo(mService, uploadJob.getFilePath());
					coverByte = ImageUtil.BitmaptoBytes(ImageUtil.drawableToBitmap(mAppInfo.appIcon));
				}
				// >>>>>>>>>如果上传的是音乐文件,需要上传音乐的专辑封面
				else if (uploadJob.getType() == PangkerConstant.RES_MUSIC) {
					coverByte = getAblum(uploadJob.getMusicId());
				}
				// >>>>>>>>初始化上传,从uploadinfo获取上传流
				uploader = uploadJob.initUpload();
			}

			// >>>>>>>>>>>上传异步请求类
			ServerSupportManager serverMana = new ServerSupportManager(mService, new IUICallBackInterface() {
				@Override
				public void uiCallBack(Object supportResponse, int caseKey) {
					// >>>>>>网络异常
					if (!HttpResponseStatus(supportResponse)) {
						showToast(uploadJob.getResName() + mService.getString(R.string.upload_fail));
						updateOverUploadInfo(uploadJob, UploadJob.UPLOAD_FAILED, null);
					}
					// >>>>>>>>>>网络ok
					else {
						UploadResult result = JSONUtil.fromJson(supportResponse.toString(), UploadResult.class);
						// >>>>>>>>>>>上传成功
						if (result != null && result.errorCode == 1) {
							showToast(uploadJob.getResName() + mService.getString(R.string.upload_success));
							updateOverUploadInfo(uploadJob, UploadJob.UPLOAD_SUCCESS, result.getResId());
						}
						// >>>>>>>>>>>上传失败
						else {
							showToast(uploadJob.getResName() + mService.getString(R.string.upload_fail));
							updateOverUploadInfo(uploadJob, UploadJob.UPLOAD_FAILED, null);
						}
					}
				}
			});
			serverMana.supportRequest(getUploadUrl(uploadJob.getType()), paras, uploader, coverByte, uploadJob.getId());
		} catch (Exception e) {
			// TODO: handle exception
			updateOverUploadInfo(uploadJob, UploadJob.UPLOAD_FAILED, null);
		}
	}

	public boolean HttpResponseStatus(Object supportResponse) {
		if (supportResponse == null) {
			return false;
		}
		if (supportResponse instanceof HttpReqCode) {
			return false;
		} else {
			return true;
		}
	}

	// >>>>>>>>获取上传文件地址
	private String getUploadUrl(int type) {
		switch (type) {
		case PangkerConstant.RES_APPLICATION:
			return Configuration.getUpApp();
		case PangkerConstant.RES_DOCUMENT:
			return Configuration.getResDocUpload();
		case PangkerConstant.RES_MUSIC:
			return Configuration.getResMusicUpload();
		case PangkerConstant.RES_PICTURE:
			return Configuration.getResPicUpload();
		}
		return null;
	}

	private byte[] getAblum(int musicId) throws IOException {
		String url = Util.getAlbumArt(mService, musicId);
		if (url == null) {
			return null;
		}
		byte[] data = null;
		Bitmap bm = ImageUtil.decodeFile(url);
		data = ImageUtil.BitmaptoBytes(bm);
		if (bm != null && !bm.isRecycled()) {
			bm.recycle();
			bm = null;
		}
		return data;
	}

	public void showToast(final int toastText) {
		Toast.makeText(mService, toastText, Toast.LENGTH_SHORT).show();
	}

	public void showToast(String toastText) {
		Toast.makeText(mService, toastText, Toast.LENGTH_SHORT).show();
	}

	//获取手机型号
}
