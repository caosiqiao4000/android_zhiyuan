package com.wachoo.pangker.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.music.MusicMediaPlayer;
import com.wachoo.pangker.music.MusicMediaPlayer.PlayStatus;
import com.wachoo.pangker.music.MusicPlayerEvent;
import com.wachoo.pangker.music.MusicPlayerManager;
import com.wachoo.pangker.receiver.StorageEventFilter;
import com.wachoo.pangker.receiver.StorageEventReceiver;
import com.wachoo.pangker.receiver.StorageEventReceiver.StorageEventListener;
import com.wachoo.pangker.server.response.MusicInfo;

/**
 * @author libo 而界面的播放进度都是通过定时器的方式来实现
 */
public class MusicService extends Service implements StorageEventListener {
	private String TAG = "MusicMediaPlayer";// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	// public static final String ACTION_PLAY = "ACTION_PLAY";
	// public static final String ACTION_INIT = "ACTION_INIT";

	private Random mRandom;
	private MusicMediaPlayer mediaPlayer;
	private PangkerApplication pangkerApp;
	private MusicPlayerManager playerManager;
	private TelephonyManager mTelephonyManager;// >>>>lb 监听打电话

	private MusicInfo musicInfo;
	private int playModel = PangkerConstant.MPM_ORDER_PLAY;// 播放模式
	private int playIndex = -1;// 播放的Index
	private int tryMaxTime = 2;// 播放音尝试最大次数
	private int trytime = 0;// 播放音乐尝试次数
	private boolean tryPlay = false;
	private boolean isPlayErr = false;

	private PlayStatus playStatus;// add by lb

	// >>>>>>>>sd卡监听
	private StorageEventFilter mStorageEventFilter;
	private StorageEventReceiver mStorageEventReceiver;

	// 播放器的状态，android自带的播放器获取不到状态，没办法，只能自己加了，

	/**
	 * 初始化播放器 initMusicPath:播放器地址 from:地址来源,false为本地，true为网络
	 */
	private boolean playMusic() {
		try {
			try {
				if (playStatus != PlayStatus.Idel && mediaPlayer.isPlaying())
					mediaPlayer.stop();
				mediaPlayer.reset(); // 恢复到初始状态
				playStatus = PlayStatus.Idel;
			} catch (Exception e) {
				logger.error(TAG, e);
				mediaPlayer.reset(); // 恢复到初始状态
				playStatus = PlayStatus.Idel;
			}

			String musicPath = null;
			musicInfo = pangkerApp.getPlayingMusic();
			mediaPlayer.interrrupted();
			// >>>>>>>>>网络音乐播放
			if (musicInfo.isNetMusic()) {
				musicPath = Configuration.getResDownload() + musicInfo.getSid();
				// >>>>>>>>>通知前台界面
				playerManager.playerOnBufferStatus(0);
				playerManager.playerMusicAction(new MusicPlayerEvent(this,
						musicInfo));
				musicPlayNotification(musicInfo);

				// >>>>>>>>>>>开始下载
				mediaPlayer.setNetDateSource(musicPath);
				mediaPlayer.setDownloadFileName(musicInfo.getMusicName()
						+ DownloadJob.SUFFIX_RESID
						+ musicInfo.getSid().toString() + ".mp3");

				playStatus = PlayStatus.Preparing;
				mediaPlayer.prepareAsync();

			} else {
				musicPath = musicInfo.getMusicPath();
				mediaPlayer.setDataSource(musicPath);
				mediaPlayer.prepare();
				playStatus = PlayStatus.Prepared;
				playerManager.playerMusicAction(new MusicPlayerEvent(this,
						musicInfo));
				play();
			}
			Log.d("sdPath", "Service:" + musicPath);
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			// sendBroadcast(-1);
			playerManager.playerErrorAction(new MusicPlayerEvent(this, null));
			return false;
		}
	}

	// >>>>>>>> add by wangxin PlayControls
	public enum PlayControl {
		MUSIC_START_PLAY, // 开始播放,在不同的歌曲之间的切换
		MUSIC_PLAYER_PRE, // 上一首
		MUSIC_PLAYER_PLAY, // 继续播放
		MUSIC_PLAYER_NEXT, // 下一首
		MUSIC_PLAYER_PAUSE, // 暂停播放
		MUSIC_PLAYER_STOP, // 停止
		MUSIC_PLAYER_SEEK, // 进度条拖拽
		MUSIC_PLAYER_TIMERTO, // 歌词定位
		MUSIC_PLAYER_MODE
		// 播放模式
	}

	@Override
	public void onStart(Intent intent, int startId) {

		playIndex = pangkerApp.getPlayMusicIndex();

		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		if (intent == null || intent.getExtras() == null)
			return;

		if (PlayControl.MUSIC_START_PLAY.equals(intent.getExtras().get(
				"control"))) {
			playMusic();
		} else if (PlayControl.MUSIC_PLAYER_PRE.equals(intent.getExtras().get(
				"control"))) {
			playPre();
		} else if (PlayControl.MUSIC_PLAYER_PLAY.equals(intent.getExtras().get(
				"control"))) {
			if (playStatus == PlayStatus.Stoped) {
				playMusic();
			} else {
				play();
			}
		} else if (PlayControl.MUSIC_PLAYER_NEXT.equals(intent.getExtras().get(
				"control"))) {
			playNext();
		} else if (PlayControl.MUSIC_PLAYER_SEEK.equals(intent.getExtras().get(
				"control"))) {
			// 进度拖拽
			int seek = intent.getExtras().getInt("progress");
			seekTo(seek);
		}
		// >>>>>>歌词定位
		else if (PlayControl.MUSIC_PLAYER_TIMERTO.equals(intent.getExtras()
				.get("control"))) {
			int seek = (int) intent.getExtras().getLong("seektotime");
			mediaPlayer.seekTo(seek);

		} else if (PlayControl.MUSIC_PLAYER_MODE.equals(intent.getExtras().get(
				"control"))) {

			// 播放模式（暂时没实现）
			playModel = intent.getExtras().getInt(
					PangkerConstant.MUSIC_PLAYER_MODE);
		} else if (PlayControl.MUSIC_PLAYER_PAUSE.equals(intent.getExtras()
				.get("control"))) {
			pause();
		} else if (PlayControl.MUSIC_PLAYER_STOP.equals(intent.getExtras().get(
				"control"))) {
			playerManager.playerPauseAction(new MusicPlayerEvent(this,
					pangkerApp.getMusiList().get(playIndex)));
			mediaPlayer.interrrupted();
			mediaPlayer.stop();
			playStatus = PlayStatus.Stoped;
		}
	}

	private void seekTo(int seek) {
		// TODO Auto-generated method stub
		try {
			// seekTo(int)方法也可以在其它状态下调用，比如Prepared，Paused和PlaybackCompleted状态
			if (playStatus == PlayStatus.Paused
					|| playStatus == PlayStatus.Prepared
					|| playStatus == PlayStatus.PlaybackCompleted
					|| mediaPlayer.isPlaying()) {
				synchronized (mediaPlayer) {
					int time = mediaPlayer.getDuration();
					int curTime = (int) ((float) seek / 100 * time);
					mediaPlayer.seekTo(curTime);
					playStatus = PlayStatus.PlaybackCompleted;
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(TAG, e);
		}
	}

	private void playNext() {
		// TODO Auto-generated method stub
		if (playIndex >= pangkerApp.getMusiList().size() - 1) {
			playIndex = 0;
			pangkerApp.setPlayMusicIndex(playIndex);
		} else {
			pangkerApp.setPlayMusicIndex(++playIndex);
		}
		playMusic();
	}

	private void pause() {
		// TODO Auto-generated method stub
		if (mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
			playStatus = PlayStatus.Paused;
			playerManager.playerPauseAction(new MusicPlayerEvent(this,
					pangkerApp.getMusiList().get(playIndex)));
		}
	}

	private void play() {
		// TODO Auto-generated method stub
		if (!mediaPlayer.isPlaying()) {
			mediaPlayer.start();
			playStatus = PlayStatus.Started;
			playerManager.playerMusicAction(new MusicPlayerEvent(this,
					pangkerApp.getMusiList().get(playIndex)));

			// >>>>>>>>>>通知显示通知栏
			musicPlayNotification(pangkerApp.getMusiList().get(playIndex));
		}
	}

	private void playPre() {
		// TODO Auto-generated method stub
		if (playIndex <= 0) {
			playIndex = pangkerApp.getMusiList().size() - 1;
			pangkerApp.setPlayMusicIndex(playIndex);
		} else {
			pangkerApp.setPlayMusicIndex(--playIndex);
		}
		playMusic();
	}

	private int getRandomIndex() {
		int size = pangkerApp.getMusiList().size();
		if (size == 0) {
			return -1;
		}
		return Math.abs(mRandom.nextInt() % size);
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.i(TAG, "onCreate");
		mediaPlayer = PangkerManager.getMediaPlayer();
		mTelephonyManager = (TelephonyManager) this
				.getSystemService(Context.TELEPHONY_SERVICE);
		mTelephonyManager.listen(mPhoneStateListener,
				PhoneStateListener.LISTEN_CALL_STATE);

		playStatus = PlayStatus.Idel;
		pangkerApp = (PangkerApplication) getApplication();
		mediaPlayer.setOnCompletionListener(onCompletionListener);
		mediaPlayer.setBufferingUpdateListener(onBufferingUpdateListener);
		mediaPlayer.setPreparedListener(onPreparedListener);

		mediaPlayer
				.setBufferingOverListener(new MusicMediaPlayer.OnBufferingOverListener() {

					@Override
					public void onBufferingUpdate(MediaPlayer mp, int percent,
							String path, boolean localfile) {
						// >>>>>>>下载完成
						try {
							musicInfo.setUseLocalCache(true);
							if (localfile) {
								// >>>>>>>通知缓存结束
								playerManager.playerOnBufferStatus(1);
								// >>>>>>>>>>>开始播放音乐
								mediaPlayer.reset();
								mediaPlayer.setDataSource(path);
								Log.i(TAG, "localfile>>>loaclpath=" + path);
								mediaPlayer.prepare();
								mediaPlayer.start();
							} else {
								int curPosition = mediaPlayer
										.getCurrentPosition();
								mediaPlayer.pause();
								mediaPlayer.reset();
								mediaPlayer.setDataSource(path);
								mediaPlayer.prepare();
								mediaPlayer.seekTo(curPosition);
								mediaPlayer.start();
							}
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalStateException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});

		mediaPlayer.setOnErrorListener(onErrorListener);
		playerManager = PangkerManager.getMusicPlayerManager();
		mRandom = new Random();

		mStorageEventFilter = new StorageEventFilter();
		mStorageEventReceiver = new StorageEventReceiver(this);
		registerReceiver(mStorageEventReceiver, mStorageEventFilter);
		intent = new Intent(MusicService.this, MusicPlayActivity.class);
	}

	/**
	 * 播放完成的监听
	 */
	MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
		@Override
		public void onCompletion(MediaPlayer mp) {
			// TODO Auto-generated method stub
			Log.i(TAG, "setOnCompletionListener!!!!");
			if (isPlayErr) {
				Log.i(TAG, "tryPlay song!! times =" + trytime);
				if (tryPlay)
					mediaPlayer.netMusicStartPlay();
				isPlayErr = false;
				tryPlay = false;
				return;
			}

			// >>>>>>>如果没有下载成功，继续播放音乐
			if (!mediaPlayer.isDownloadOver()) {
				Log.i(TAG, "netMusicPlayonUpdate!!!");
				mediaPlayer.netMusicPlayonUpdate();
			} else {
				isPlayErr = false;
				tryPlay = false;
				Log.i(TAG, "next song!!");
				switch (playModel) {
				case PangkerConstant.MPM_ORDER_PLAY:
					if (playIndex < pangkerApp.getMusiList().size() - 1) {
						playNext();
					} else {
						playerManager.playOver();
					}
					break;
				case PangkerConstant.MPM_SINGLE_LOOP_PLAY:
					play();
					break;
				case PangkerConstant.MPM_LIST_LOOP_PLAY:
					if (playIndex == pangkerApp.getMusiList().size()) {
						playIndex = 0;
					}
					playNext();
					break;
				case PangkerConstant.MPM_RANDOM_PLAY:
					playIndex = getRandomIndex();
					pangkerApp.setPlayMusicIndex(playIndex);
					playMusic();
					break;
				}
			}

		}
	};

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.i(TAG, ">>>>onDestroy");
		unregisterReceiver(mStorageEventReceiver);
		mTelephonyManager.listen(mPhoneStateListener,
				PhoneStateListener.LISTEN_NONE);
		try {
			if (playStatus == PlayStatus.Idel) {
				mediaPlayer.release();
				return;
			}
			if (mediaPlayer.isPlaying())
				mediaPlayer.stop();

		} catch (Exception e) {
			// TODO: handle exception
			logger.error(TAG, e);
		} finally {
			mediaPlayer.release();
			mediaPlayer = null;
		}
		super.onDestroy();
	}

	/**
	 * 缓冲完成，预备好的监听
	 */
	MusicMediaPlayer.OnPreparedListener onPreparedListener = new MusicMediaPlayer.OnPreparedListener() {
		@Override
		public void onPrepared(MediaPlayer mp) {
			// TODO Auto-generated method stub
			Log.i(TAG, "setPreparedListener:OK!!!!");
			playStatus = PlayStatus.Started;
			playerManager.playerOnBufferStatus(1);
			mediaPlayer.netMusicStartPlay();
		}
	};

	/**
	 * 缓冲更新的监听，针对网络平台
	 */
	MusicMediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener = new MusicMediaPlayer.OnBufferingUpdateListener() {
		@Override
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
			// TODO Auto-generated method stub

			playerManager.playeronBufferChanged(percent);
		}
	};
	/**
     *  
     */
	MediaPlayer.OnErrorListener onErrorListener = new MediaPlayer.OnErrorListener() {
		@Override
		public boolean onError(MediaPlayer mp, int what, int extra) {
			// TODO Auto-generated method stub
			Log.w(TAG, "PlayerEngineImpl fail, what (" + what + ") extra ("
					+ extra + ")");

			// if(what == MediaPlayer.MEDIA_ERROR_UNKNOWN){
			// playerManager.playerErrorAction(new MusicPlayerEvent(this,
			// null));
			// mediaPlayer.stop();
			// return true;
			// }
			// if(what == -1){
			//
			// }
			mediaPlayer.reset();
			isPlayErr = true;

			if (trytime <= tryMaxTime) {
				trytime++;
				tryPlay = true;
			} else {
				trytime = 0;
				tryPlay = false;
				Log.i(TAG, "send error times = " + trytime);

				playerManager
						.playerErrorAction(new MusicPlayerEvent(this, null));
			}
			Log.i(TAG, "what = " + what + "; detra = " + extra);
			return false;
		}
	};

	private IBinder mBinder = new MusicPlayerBinder();

	// >>>>>>> MusicPlayerBinder
	public class MusicPlayerBinder extends Binder {

		public MusicInfo getMusicInfo() {
			return musicInfo;
		}

		// 出于Idel状态的MediaPlay是不能reset modify by lb
		public boolean isPlay() {
			try {
				if (playStatus == PlayStatus.Idel) {
					return false;
				}
				if (musicInfo.isNetMusic())
					return mediaPlayer.isNetMusicPlaying();
				else
					return mediaPlayer.isPlaying();
			} catch (Exception e) {
				logger.error(TAG, e);
				// modify by lb 对于通过调用reset()方法进入idle状态的MediaPlayer对象
				if (playStatus != PlayStatus.Idel) {
					mediaPlayer.reset();
					playStatus = PlayStatus.Idel;
				}
				// TODO: handle exception
				return false;
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			if (state == TelephonyManager.CALL_STATE_IDLE) {
				// resume playback
			} else {
				if (mediaPlayer != null) {
					if (playStatus != PlayStatus.Idel
							&& mediaPlayer.isPlaying()) {
						mediaPlayer.pause();
						playerManager.playerPauseAction(new MusicPlayerEvent(
								this, pangkerApp.getMusiList().get(playIndex)));
					}
				}
			}
		}

	};

	@Override
	public void onStorageStateChanged(boolean isAvailable, boolean writeAble) {
		// TODO Auto-generated method stub
		// >>>>>>>>如果不能使用
		if (!isAvailable || !writeAble) {
			mediaPlayer.interrrupted();
			mediaPlayer.stop();
		}
	}

	private NotificationManager nm;
	private Notification n;
	// >>>>>>>音乐通知栏ID
	public static final int ID = 10;
	// 实例化Intent
	Intent intent;

	private void musicPlayNotification(MusicInfo musicInfo) {
		String service = NOTIFICATION_SERVICE;
		nm = (NotificationManager) getSystemService(service);
		n = new Notification();
		// 设置显示图标
		int icon = R.drawable.music_play_icon;
		// 设置提示信息
		String tickerText = musicInfo.getMusicName();

		n.icon = icon;
		n.tickerText = tickerText;

		// 显示在“正在进行中”
		n.flags = Notification.FLAG_ONGOING_EVENT;

		if (isMusicActivity()) {
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		} else {
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		}
		// 获得PendingIntent
		PendingIntent pi = PendingIntent.getActivity(MusicService.this, 0,
				intent, 0);

		String contentTitle = musicInfo.getMusicName();
		String contentText = "歌手未知";
		if (!StringUtils.isEmpty(musicInfo.getSinger()))
			contentText = musicInfo.getSinger();

		// 设置事件信息，显示在拉开的里面
		n.setLatestEventInfo(MusicService.this, contentTitle, contentText, pi);

		// 发出通知
		nm.notify(ID, n);

	}

	private boolean isMusicActivity() {
		if (intent != null
				&& intent.getComponent().getClassName()
						.contains("MsgChatActivity")) {
			return true;
		}
		return false;
	}
}
