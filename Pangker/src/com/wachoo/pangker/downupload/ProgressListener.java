package com.wachoo.pangker.downupload;

public interface ProgressListener {

	// >>>>>>>进度更新
	public void transferred(int size, int progress);

	// >>>>>>>下载情况更新
	public void onTransfState(int state);

}
