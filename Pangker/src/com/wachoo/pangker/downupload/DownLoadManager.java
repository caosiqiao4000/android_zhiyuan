package com.wachoo.pangker.downupload;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.res.ResInfoActivity;
import com.wachoo.pangker.db.IDownloadDao;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.DownloadDaoImpl;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

public class DownLoadManager {

	private Context mContext;
	private IDownloadDao mDownloadDao;
	private IResDao iResDao;
	private List<DownloadListenter> mDownloadListenters = new ArrayList<DownloadListenter>();
	private SharedPreferencesUtil apu;

	public DownLoadManager(Context mContext) {
		super();
		this.mContext = mContext;
		mDownloadDao = new DownloadDaoImpl(mContext);
		iResDao = new ResDaoImpl(mContext);
		addDownloadJobs(mDownloadDao.getDownloadInfos());
		apu = new SharedPreferencesUtil(mContext);
		
		
	}

	/**
	 * 添加一个Listener监听器
	 */
	public synchronized void addDownloadListenter(DownloadListenter listener) {
		mDownloadListenters.add(listener);
	}

	/**
	 * 移除一个已注册的Listener监听器. 如果监听器列表中已有相同的监听器listener1、listener2,
	 * 并且listener1==listener2, 那么只移除最近注册的一个监听器。
	 */
	public synchronized void removeDownloadListenter(DownloadListenter listener) {
		mDownloadListenters.remove(listener);
	}

	/**
	 * 启动监听
	 */
	public void startListenterAction(DownloadAsyncTask mTask) {
		DownloadListenter listener;
		synchronized (mDownloadListenters) {
			Iterator<DownloadListenter> it = mDownloadListenters.iterator();// 这步要注意同步问题
			try {
				while (it.hasNext()) {
					listener = (DownloadListenter) it.next();
					// 根据下载文件的地址
					listener.refreshProgressUI(mTask);
				}
			} catch (Exception e) {

			}
		}
	}
	
	private void checkDownSet(DownloadJob job){
		String userId = ((PangkerApplication)mContext.getApplicationContext()).getMyUserID();
		if(apu.getBoolean(PangkerConstant.SP_DOWNLOAD_SET_SOUND + userId, false)){
			Intent intent = new Intent(mContext, PangkerService.class);
			intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_PLAY_SOUND);
			mContext.startService(intent);
		}
		if(job.getType() == PangkerConstant.RES_APPLICATION){
			if (job.isIsopen()) {
				Util.instalApplication(mContext, job.getAbsolutePath());
			} else {
				if(apu.getBoolean(PangkerConstant.SP_DOWNLOAD_SET_INSTALL + userId, false)){
					Util.instalApplication(mContext, job.getAbsolutePath());
				}
			}
		}
	}
	
	/**
	 * 下载完成之后保存在本地资源，同时进行声音播放和应用安装 
	 * @param mDownloadJob
	 */
	public void saveResAsLoacl(DownloadJob mDownloadJob) {
		// TODO Auto-generated method stub
		DirInfo dirInfo = new DirInfo();
		dirInfo.setPath(mDownloadJob.getAbsolutePath());
		dirInfo.setDeal(DirInfo.DEAL_SELECT);
		Log.d("isSelected", "==>" + mDownloadJob.getAbsolutePath());
		PangkerApplication app = (PangkerApplication) mContext.getApplicationContext();
		String userId = app.getMyUserID();
		dirInfo.setUid(userId);
		dirInfo.setIsfile(DirInfo.ISFILE);
		dirInfo.setDirName(mDownloadJob.getName());
		dirInfo.setFileCount(mDownloadJob.getEndPos());
		dirInfo.setResType(mDownloadJob.getType());
		dirInfo.setRemark(0);
		if (!iResDao.isSelected(userId, dirInfo.getPath(), dirInfo.getResType())) {
			iResDao.saveSelectResInfo(dirInfo);
		}
		checkDownSet(mDownloadJob);
		//通知下载完成
		Message msg = new Message();
		msg.what = 1;
		msg.obj = mDownloadJob;
		PangkerManager.getTabBroadcastManager().sendResultMessage(ResInfoActivity.class.getName(), msg);
	}
	
	public int getDownloadOverCount(){
		int count = 0;
		for (DownloadJob downloadJob : mDownloadJobs) {
			if(downloadJob.getStatus() == DownloadJob.DOWNLOAD_SUCCESS){
				count ++ ;
			}
		}
		return count;
	}

	// >>>>>>>>>>>>>>>>>>>>>以下为下载管理集合>>>>>>>>>>>>>>>>>>>>>>
	private List<DownloadJob> mDownloadJobs = new ArrayList<DownloadJob>();

	// >>>>>>>>>>初始化时调用添加下载结合
	private void addDownloadJobs(List<DownloadJob> downloadInfos) {
		// TODO Auto-generated method stub
		this.mDownloadJobs.addAll(downloadInfos);
	}

	// >>>>>>>>>>添加一个下载任务 具有排重功能
	public void addDownloadJob(DownloadJob mQueuedJob) {
		synchronized (mDownloadJobs) {
			DownloadJob oldJob = getDownloadJobByID(mQueuedJob.getId());
			if (oldJob == null)
				mDownloadJobs.add(mQueuedJob);
			else {
				mDownloadJobs.remove(oldJob);
				mDownloadJobs.add(mQueuedJob);
			}
		}
	}

	public List<DownloadJob> getmDownloadJobs() {
		return mDownloadJobs;
	}

	// >>>>>>>>>>>>>根据id获取JOB
	public DownloadJob getDownloadJobByID(int JobID) {
		DownloadJob job;
		Iterator<DownloadJob> it = mDownloadJobs.iterator();
		while (it.hasNext()) {
			job = (DownloadJob) it.next();
			if (job.getId() == JobID) {
				return job;
			}
		}
		return null;
	}
	
	public DownloadJob getDownloadJobByUrl(String url) {
		DownloadJob job;
		Iterator<DownloadJob> it = mDownloadJobs.iterator();
		while (it.hasNext()) {
			job = (DownloadJob) it.next();
			if (url.equals(job.getUrl())) {
				return job;
			}
		}
		return null;
	}
	
	//>>>>
	public void removeCompleteJob(DownloadJob job) {
		// TODO Auto-generated method stub
		synchronized (mDownloadJobs) {
			mDownloadJobs.remove(job);
		}
		mDownloadDao.delete(job.getUrl());
	}

	// >>>>>>>>删除所有完成任务
	public void removeAllCompleteJobs() {
		synchronized (mDownloadJobs) {
			for (int i = mDownloadJobs.size() - 1; i >= 0; i--) {
				DownloadJob job = mDownloadJobs.get(i);
				if (job.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
					mDownloadJobs.remove(i);
				}
			}
		}
		// >>>>>从db中删除数据
		mDownloadDao.deleteDownloadJobsBYStatus(DownloadJob.DOWNLOAD_SUCCESS);
	}

	// >>>>>>>>是否有正在进行的任务
	public boolean haveDownloadingJob() {
		Iterator<DownloadJob> it = mDownloadJobs.iterator();
		while (it.hasNext()) {
			DownloadJob job = (DownloadJob) it.next();
			if (job.getStatus() == DownloadJob.DOWNLOADING || job.getStatus() == DownloadJob.DOWNLOAD_PAUSE) {
				return true;
			}
		}
		return false;
	}

	// >>>>>>>>取消所有任务
	public void cancelAllDownloadingJobs() {
		Iterator<DownloadJob> it = mDownloadJobs.iterator();
		while (it.hasNext()) {
			DownloadJob job = (DownloadJob) it.next();
			if (job.getStatus() == DownloadJob.DOWNLOADING || job.getStatus() == DownloadJob.DOWNLOAD_PAUSE) {
				job.cancelJob();// 取消上传工作
				mDownloadDao.updataDownloadInfo(job); // 更新通知db
			}
		}
	}

	// >>>>>>>>>>>>>>停止所有任务(只针对暂停+下载中的)
	public void stopAllDownloadingJobs() {
		Iterator<DownloadJob> it = mDownloadJobs.iterator();
		while (it.hasNext()) {
			DownloadJob job = (DownloadJob) it.next();
			if (job.getStatus() == DownloadJob.DOWNLOADING || job.getStatus() == DownloadJob.DOWNLOAD_PAUSE) {
				job.stopJob();// 取消上传工作
				mDownloadDao.updataDownloadInfo(job); // 更新通知db
			}
		}
	}

	// >>>>>>>>>>>>>根据下载路径判断是否有文件正在下载(只针对暂停+下载中的)
	public boolean isHttpFileDowning(String httpPath) {
		Iterator<DownloadJob> it = mDownloadJobs.iterator();
		while (it.hasNext()) {
			DownloadJob job = (DownloadJob) it.next();
			if ((job.getUrl().equals(httpPath) && job.getStatus() == DownloadJob.DOWNLOADING)
					|| job.getUrl().equals(httpPath) && job.getStatus() == DownloadJob.DOWNLOAD_PAUSE) {
				return true;
			}
		}
		return false;
	}
}
