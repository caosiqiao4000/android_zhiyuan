package com.wachoo.pangker.downupload;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Activity;
import android.view.View;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.adapter.MsgTalkAdapter.MessageViewHolder;
import com.wachoo.pangker.audio.VoicePlayer;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UploadOfflineResult;

public class ChatSendVoiceUI extends ChatFileUI {

	MessageViewHolder voiceHolder;
	private ChatMessage userMsg;
	private boolean isPlay = false;
	private VoicePlayer voicePlayer;

	public ChatSendVoiceUI(MessageViewHolder voiceHolder, Activity context, IUserMsgDao msgDao,
			ImageApkIconFetcher imageFetcher) {
		super(context, imageFetcher);
		this.voiceHolder = voiceHolder;
		this.activity = context;
		this.mUserMsgDao = msgDao;
		this.application = (PangkerApplication) context.getApplicationContext();
		voicePlayer = VoicePlayer.getVoicePlayer();
		voiceHolder.tv_content.setOnClickListener(this);

		voiceHolder.iv_sms_message.setVisibility(View.VISIBLE);
		voiceHolder.iv_sms_message.setImageResource(R.drawable.record_me_normal);
	}

	public void initView(ChatMessage userMsg) {
		// TODO Auto-generated method stub

		this.userMsg = userMsg;
		voiceHolder.tv_content.setText(userMsg.getContent());
		sendFile(null);

	}

	private void showResult() {
		if (userMsg.getStatus() == UploadJob.UPLOAD_SUCCESS) {
		} else {
			voiceHolder.tv_content.setText("发送失败!");
		}
	}

	private void handleResult(Object response) {
		application.removeFilePart(getFilePartID(userMsg));
		if (response != null && userMsg.getStatus() != UploadJob.UPLOAD_CANCEL) {
			UploadOfflineResult result = JSONUtil.fromJson(response.toString(), UploadOfflineResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				((MsgChatActivity) activity).sendFileResult(userMsg, result.getFileName());
				userMsg.setStatus(UploadJob.UPLOAD_SUCCESS);
				mUserMsgDao.updateUserMsgStatus(userMsg);
				showResult();
			} else {
				userMsg.setStatus(UploadJob.UPLOAD_FAILED);
				mUserMsgDao.updateUserMsgStatus(userMsg);
				showResult();
			}
		} else {
			userMsg.setStatus(UploadJob.UPLOAD_FAILED);
			mUserMsgDao.updateUserMsgStatus(userMsg);
			showResult();
		}
	}

	private void sendFile(final ProgressListener listener) {
		File file = new File(userMsg.getFilepath());
		if (!file.exists()) {
			return;
		}
		if (userMsg.getStatus() == UploadJob.UPLOAD_INITOK) {
			try {
				MyFilePart uploader = new MyFilePart(userMsg.getFilepath(), file,
						new MimetypesFileTypeMap().getContentType(file), PangkerConstant.CONTENT_CHARSET, listener);
				List<Parameter> paras = new ArrayList<Parameter>();
				paras.add(new Parameter("uid", application.getMyUserID()));
				ServerSupportManager serverMana = new ServerSupportManager(activity, new IUICallBackInterface() {
					@Override
					public void uiCallBack(Object response, int caseKey) {
						// TODO Auto-generated method stub
						handleResult(response);
					}
				});
				// 存放上传线程文件
				application.putFilePart(getFilePartID(userMsg), uploader);
				serverMana.supportRequest(Configuration.getUpOfflineFile(), paras, uploader, null, 0);
				userMsg.setStatus(UploadJob.UPLOAD_UPLOAGINDG);
				mUserMsgDao.updateUserMsgStatus(userMsg);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				application.removeFilePart(getFilePartID(userMsg));
				// logger.error(TAG, e);
			}
		}
	}

	@Override
	protected void reDownload() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		isPlay = !isPlay;
		if (isPlay) {
			voicePlayer.play(userMsg.getFilepath(), new VoicePlayer.OnVoiceListener() {
				@Override
				public void onVoiceStatusListener(int state) {
					// TODO Auto-generated method stub
					if (state == -1) {
						showToast("抱歉，文件找不到!");
					} else if (state == 0) {
						isPlay = false;
						voiceHolder.iv_sms_message.setImageResource(R.drawable.record_me_normal);
					} else if (state == 1) {
						voiceHolder.iv_sms_message.setImageResource(R.drawable.ptt_action_r_1);
					} else if (state == 2) {
						voiceHolder.iv_sms_message.setImageResource(R.drawable.ptt_action_r_2);
					} else if (state == 3) {
						voiceHolder.iv_sms_message.setImageResource(R.drawable.ptt_action_r_3);
					}
				}
			});
		} else {
			voicePlayer.stop();
		}
	}

}
