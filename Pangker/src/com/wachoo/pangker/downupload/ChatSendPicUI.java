package com.wachoo.pangker.downupload;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.adapter.MsgTalkAdapter.MessageViewHolder;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UploadOfflineResult;
import com.wachoo.pangker.util.Util;

public class ChatSendPicUI extends ChatFileUI {

	private MessageViewHolder fileHolder;
	private ImageLocalFetcher mImageWorker;

	public ChatSendPicUI(MessageViewHolder fileHolder, Activity context, IUserMsgDao msgDao,
			PangkerApplication application, ImageLocalFetcher mImageWorker, ImageApkIconFetcher imageFetcher) {
		super(context, imageFetcher);
		this.fileHolder = fileHolder;
		this.activity = context;
		this.mUserMsgDao = msgDao;
		this.application = application;
		this.mImageWorker = mImageWorker;

		fileHolder.msgRLayout.setOnClickListener(this);
	}

	private void showSendResult(int type) {
		fileHolder.pb_progressBar.setVisibility(View.GONE);
		fileHolder.iv_status.setVisibility(View.VISIBLE);
		if (type == UploadJob.UPLOAD_SUCCESS) {
			fileHolder.iv_status.setImageResource(R.drawable.icon_download_small_over);
		} else {
			fileHolder.iv_status.setImageResource(R.drawable.download_failure_icon);
		}
	}

	final Handler handler = new Handler() {
		public void handleMessage(Message handMessage) {
			if (handMessage.what == 1) {
				int progress = (int) (((1.0 * handMessage.arg2) / handMessage.arg1) * 100);
				if (progress > 0 && progress < 100) {
					fileHolder.pb_progressBar.setProgress(progress);
				} else if (progress >= 100) {
					fileHolder.pb_progressBar.setVisibility(View.GONE);
				}
			}
			if (handMessage.what == 2) {
				showSendResult(handMessage.arg1);
			}
		};
	};

	public void initView(ChatMessage msg) {
		this.userMsg = msg;

		String sdpath = userMsg.getFilepath();
		mImageWorker.loadImage(sdpath, fileHolder.iv_imagecover, sdpath);
		fileHolder.pb_progressBar.setMax(100);

		if ((userMsg.getStatus() == UploadJob.UPLOAD_INITOK) && !Util.isEmpty(userMsg.getFilepath())) {
			fileHolder.pb_progressBar.setVisibility(View.VISIBLE);
			sendFile(new ProgressListener() {
				@Override
				public void transferred(int size, int progress) {
					Message ms = new Message();
					ms.what = 1;
					ms.arg1 = size;
					ms.arg2 = progress;
					handler.sendMessage(ms);
				}

				@Override
				public void onTransfState(int state) {
					// TODO Auto-generated method stub
					Message ms = new Message();
					ms.what = 2;
					ms.arg1 = state;
					handler.sendMessage(ms);
				}
			});
		} else {
			showSendResult(userMsg.getStatus());
		}

		fileHolder.tv_time.setText(Util.getDateDiff(Util.strToDate(userMsg.getTime())));
	}

	private void sendFile(final ProgressListener listener) {
		File file = new File(userMsg.getFilepath());
		if (!file.exists()) {
			return;
		}
		if (userMsg.getStatus() == UploadJob.UPLOAD_INITOK) {
			try {
				MyFilePart uploader = new MyFilePart(userMsg.getFilepath(), file,
						new MimetypesFileTypeMap().getContentType(file), PangkerConstant.CONTENT_CHARSET, listener);
				List<Parameter> paras = new ArrayList<Parameter>();
				paras.add(new Parameter("uid", application.getMyUserID()));
				ServerSupportManager serverMana = new ServerSupportManager(activity, new IUICallBackInterface() {
					@Override
					public void uiCallBack(Object response, int caseKey) {
						// TODO Auto-generated method stub
						application.removeFilePart(getFilePartID(userMsg));
						if (response != null) {
							UploadOfflineResult result = JSONUtil.fromJson(response.toString(),
									UploadOfflineResult.class);
							if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
								((MsgChatActivity) activity).sendFileResult(userMsg, result.getFileName());
								userMsg.setStatus(UploadJob.UPLOAD_SUCCESS);
								mUserMsgDao.updateUserMsgStatus(userMsg);
							} else {
								userMsg.setStatus(UploadJob.UPLOAD_FAILED);
								mUserMsgDao.updateUserMsgStatus(userMsg);
								listener.onTransfState(UploadJob.UPLOAD_FAILED);
							}
							showSendResult(userMsg.getStatus());
						} else {
							userMsg.setStatus(UploadJob.UPLOAD_FAILED);
							mUserMsgDao.updateUserMsgStatus(userMsg);
							listener.onTransfState(UploadJob.UPLOAD_FAILED);
						}
					}
				});
				// 存放上传线程文件
				application.putFilePart(getFilePartID(userMsg), uploader);
				serverMana.supportRequest(Configuration.getUpOfflineFile(), paras, uploader, null, 0);
				userMsg.setStatus(UploadJob.UPLOAD_UPLOAGINDG);
				mUserMsgDao.updateUserMsgStatus(userMsg);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				application.removeFilePart(getFilePartID(userMsg));
				// logger.error(TAG, e);
			}
		} else if (userMsg.getStatus() == UploadJob.UPLOAD_UPLOAGINDG) {
			MyFilePart uploader = application.getFilePart(getFilePartID(userMsg));
			if (uploader != null) {
				uploader.setListener(listener);
				Message ms = new Message();
				ms.what = 1;
				ms.arg1 = uploader.getFileLen();
				ms.arg2 = uploader.getDone();
				handler.sendMessage(ms);
			}
		}
	}

	@Override
	protected void reDownload() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		lookPic(userMsg.getFilepath());
	}

}
