package com.wachoo.pangker.downupload;

import java.util.EventListener;

import android.os.Message;

public interface UpLoadlistenter extends EventListener {

	public long getUploadID();

	public void refreshProgressUI(Message message);
	
}
