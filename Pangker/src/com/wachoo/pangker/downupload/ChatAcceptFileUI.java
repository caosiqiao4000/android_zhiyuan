package com.wachoo.pangker.downupload;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.view.View;
import android.view.View.OnClickListener;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.adapter.MsgTalkAdapter.MessageViewHolder;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.util.Util;

public class ChatAcceptFileUI extends ChatFileUI {

	private MessageViewHolder acceptfileHolder;
	private PKIconResizer mImageResizer;

	public ChatAcceptFileUI(MessageViewHolder fileHolder, Activity context, IUserMsgDao msgDao,
			PangkerApplication application, PKIconResizer mImageResizer, ImageApkIconFetcher imageFetcher) {
		super(context, imageFetcher);
		this.acceptfileHolder = fileHolder;
		this.activity = context;
		this.mUserMsgDao = msgDao;
		this.application = application;
		this.mImageResizer = mImageResizer;

		acceptfileHolder.msgRLayout.setOnClickListener(this);
	}

	MsgDownloader.OnDownloadProgressListener onDownloadProgressListener = new MsgDownloader.OnDownloadProgressListener() {
		@Override
		public void onDownloadResultListener(int result, String filepath) {
			// TODO Auto-generated method stub
			Message msg = new Message();
			msg.what = 1;
			msg.arg1 = result;
			msg.obj = filepath;
			downHandler.sendMessage(msg);
		}

		@Override
		public void onDownloadSizeListener(int len, int progress) {
			// TODO Auto-generated method stub
			Message msg = new Message();
			msg.what = 2;
			msg.arg1 = progress;
			msg.arg2 = len;
			downHandler.sendMessage(msg);
		}

	};

	final Handler downHandler = new Handler() {
		@Override
		public void handleMessage(Message mssg) {
			// TODO Auto-generated method stub
			if (mssg.what == 1) {
				acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
				acceptfileHolder.btn_reject.setVisibility(View.GONE);
				acceptfileHolder.tv_fileize.setVisibility(View.VISIBLE);
				if (mssg.arg1 == DownloadJob.DOWNLOAD_SUCCESS) {
					acceptfileHolder.tv_fileize.setText("接收成功!");
					String sdpath = mssg.obj.toString();
					userMsg.setFilepath(sdpath);
					userMsg.setStatus(DownloadJob.DOWNLOAD_SUCCESS);
					loadMsgCover(acceptfileHolder.iv_imagecover);
					acceptfileHolder.btn_accept.setVisibility(View.GONE);
					((MsgChatActivity) activity).receiveFileResult(userMsg);
				} else {
					userMsg.setStatus(DownloadJob.DOWNLOAD_FAILE);
					acceptfileHolder.btn_accept.setVisibility(View.VISIBLE);
					acceptfileHolder.btn_reject.setVisibility(View.VISIBLE);
					acceptfileHolder.btn_accept.setEnabled(true);
					acceptfileHolder.tv_fileize.setText("接收失败!");
				}
				application.removeMsgDownloader(getFilePartID(userMsg));
			}
			if (mssg.what == 2) {
				String size = Formatter.formatFileSize(activity, mssg.arg2);
				String progress = Formatter.formatFileSize(activity, mssg.arg1);
				acceptfileHolder.tv_fileize.setText(progress + "/" + size);
				acceptfileHolder.pb_progressBar.setProgress(mssg.arg1 * 100 / mssg.arg2);
			}
		}
	};

	private void downloadFile() {
		acceptfileHolder.btn_accept.setEnabled(false);
		acceptfileHolder.pb_progressBar.setVisibility(View.VISIBLE);
		downloadFile(userMsg, onDownloadProgressListener);
	};

	public void initView(ChatMessage msg) {
		this.userMsg = msg;
		mImageResizer.loadImage(userMsg.getUserId(), acceptfileHolder.iv_usericon);
		acceptfileHolder.pb_progressBar.setMax(100);
		acceptfileHolder.pb_progressBar.setProgress(0);
		loadMsgCover(acceptfileHolder.iv_imagecover);
		acceptfileHolder.btn_accept.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// sendingFileOperate(bar, fileTsfKey,
				if (userMsg.getStatus() == DownloadJob.DOWNLOAD_INIT
						|| userMsg.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
					downloadFile();
				}
			}
		});

		acceptfileHolder.btn_reject.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// sendingFileOperate(bar, fileTsfKey,
				// PangkerConstant.INTENT_SENDFILE_REJ);
				if (downloader != null) {
					downloader.cancel();
				}
				userMsg.setStatus(DownloadJob.DOWNLOAD_REFUSE);
				((MsgChatActivity) activity).receiveFileResult(userMsg);
				acceptfileHolder.btn_reject.setVisibility(View.GONE);
				acceptfileHolder.btn_accept.setVisibility(View.GONE);
				acceptfileHolder.tv_fileize.setText("接收拒绝!");
			}
		});
		if (userMsg.getStatus() == DownloadJob.DOWNLOAD_INIT) {
			acceptfileHolder.tv_fileize.setText("开始接收...");
			acceptfileHolder.btn_accept.setVisibility(View.VISIBLE);
			acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
			acceptfileHolder.btn_reject.setVisibility(View.VISIBLE);
		} else if (userMsg.getStatus() == DownloadJob.DOWNLOADING) {
			downloadFile();
		} else if (userMsg.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
			acceptfileHolder.tv_fileize.setText("接收成功!");
			acceptfileHolder.btn_accept.setVisibility(View.GONE);
			acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
			acceptfileHolder.btn_reject.setVisibility(View.GONE);
		} else if (userMsg.getStatus() == DownloadJob.DOWNLOAD_REFUSE) {
			acceptfileHolder.tv_fileize.setText("接收拒绝!");
			acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
			acceptfileHolder.btn_accept.setVisibility(View.GONE);
			acceptfileHolder.btn_reject.setVisibility(View.GONE);
		} else if (userMsg.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
			acceptfileHolder.btn_accept.setText("重新接受");
			acceptfileHolder.btn_accept.setEnabled(true);
			acceptfileHolder.btn_accept.setVisibility(View.VISIBLE);
			acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
			acceptfileHolder.btn_reject.setVisibility(View.VISIBLE);
			acceptfileHolder.tv_fileize.setText("接收失败!");
		}
		acceptfileHolder.tv_content.setText(userMsg.getContent());
	}

	@Override
	protected void reDownload() {
		// TODO Auto-generated method stub
		userMsg.setStatus(DownloadJob.DOWNLOAD_INIT);
		acceptfileHolder.btn_accept.setVisibility(View.VISIBLE);
		acceptfileHolder.btn_accept.setEnabled(true);
		acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
		acceptfileHolder.btn_accept.setText("重新接受");
		mUserMsgDao.updateUserMsgStatus(userMsg);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (userMsg.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
			if (userMsg.getMsgType() == PangkerConstant.RES_APPLICATION) {
				Util.instalApplication(activity, userMsg.getFilepath());
			} else if (userMsg.getMsgType() == PangkerConstant.RES_DOCUMENT) {
				openReader(userMsg.getFilepath());
			} else if (userMsg.getMsgType() == PangkerConstant.RES_MUSIC) {
				openMusic();
			}
		}
	}

}
