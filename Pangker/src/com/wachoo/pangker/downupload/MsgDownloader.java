package com.wachoo.pangker.downupload;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.util.Log;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.util.Util;

public class MsgDownloader extends Thread {

	private String TAG = "MsgDownloader";// log tag
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private boolean isCancel = false;
	private int noticeTime = 500;// 通知进度时间间隔
	// private int state = 0;//
	private String fileName;
	private OnDownloadProgressListener onDownloadProgressListener;
	private String saveFullPath;
	private String savePath;
	private String downLoadPath;

	public void setOnDownloadProgressListener(OnDownloadProgressListener onDownloadProgressListener) {
		this.onDownloadProgressListener = onDownloadProgressListener;
	}

	public MsgDownloader(String savePath, String fileName, String downLoadPath,
			OnDownloadProgressListener onDownloadProgressListener) {
		super();
		this.savePath = savePath;
		this.fileName = fileName;
		this.downLoadPath = downLoadPath;
		this.onDownloadProgressListener = onDownloadProgressListener;
	}

	public void cancel() {
		// TODO Auto-generated method stub
		isCancel = true;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if (Util.isEmpty(fileName) || Util.isEmpty(downLoadPath) || Util.isEmpty(savePath)) {
			onDownloadProgressListener.onDownloadResultListener(DownloadJob.DOWNLOAD_FAILE, null);
			return;
		}
		String fileUrl = Configuration.getGetFile() + downLoadPath;
		HttpURLConnection connection = null;
		// FileOutputStream fos = null;
		RandomAccessFile randomAccessFile = null;
		InputStream is = null;
		BufferedInputStream bis = null;
		URL url = null;
		try {
			url = new URL(fileUrl);
		} catch (MalformedURLException e1) {
			Log.d("Downloader1", "==>error0");
			onDownloadProgressListener.onDownloadResultListener(DownloadJob.DOWNLOAD_FAILE, null);
			return;
		}
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.setConnectTimeout(5 * 1000);
			connection.setRequestMethod("GET");
			// 设置范围，格式为Range：bytes x-y;
			connection.connect();
			File resDir = new File(savePath);
			if (!resDir.exists()) {
				if (!resDir.mkdirs()) {
					onDownloadProgressListener.onDownloadResultListener(DownloadJob.DOWNLOAD_FAILE, null);
					return;
				}
			}
			saveFullPath = savePath + "/" + fileName;
			File downFile = new File(saveFullPath);
			if (!downFile.exists()) {
				if (!downFile.createNewFile()) {
					onDownloadProgressListener.onDownloadResultListener(DownloadJob.DOWNLOAD_FAILE, null);
					return;
				}
			}
			randomAccessFile = new RandomAccessFile(saveFullPath, "rwd");
			randomAccessFile.seek(0);

			// 将要下载的文件写到保存在保存路径下的文件中
			is = connection.getInputStream();
			bis = new BufferedInputStream(is);

			byte[] buffer = new byte[1024 * 4];
			int doneSize = 0;
			long lastnoticetime = 0;
			int length = -1;
			int slen = connection.getContentLength();
			// >>>>>>>>>by wangxin add e
			while ((length = bis.read(buffer)) != -1) {
				randomAccessFile.write(buffer, 0, length);
				doneSize += length;
				// >>>>>>>>>>> edit by wangxin start
				if (doneSize == slen || System.currentTimeMillis() - lastnoticetime >= noticeTime) {
					// 下载过程中
					lastnoticetime = System.currentTimeMillis();
					onDownloadProgressListener.onDownloadSizeListener(slen, doneSize);
					Log.d(TAG, "doneSize:" + doneSize);
				}
				if (isCancel) {
					break;
				}
			}
			if (isCancel) {
				onDownloadProgressListener
						.onDownloadResultListener(DownloadJob.DOWNLOAD_CANCEL, saveFullPath);
			} else {
				onDownloadProgressListener.onDownloadResultListener(DownloadJob.DOWNLOAD_SUCCESS,
						saveFullPath);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			onDownloadProgressListener.onDownloadResultListener(DownloadJob.DOWNLOAD_FAILE, saveFullPath);
			Log.d("Downloader", "==>ERROR e:" + e);
			// 发送下载失败消息
			return;
		} finally {
			try {
				if (is != null) {
					is.close();
					is = null;
				}
				if (bis != null) {
					bis.close();
					bis = null;
				}
				if (randomAccessFile != null) {
					randomAccessFile.close();
				}
				connection.disconnect();
			} catch (Exception e) {
				logger.error(TAG, e);
			}
		}
	}

	public interface OnDownloadProgressListener {
		// 下载进度
		public void onDownloadSizeListener(int size, int progress);

		// 下载结果
		public void onDownloadResultListener(int result, String filepath);
	}

}
