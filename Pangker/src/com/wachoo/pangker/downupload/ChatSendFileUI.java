package com.wachoo.pangker.downupload;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.adapter.MsgTalkAdapter.MessageViewHolder;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UploadOfflineResult;
import com.wachoo.pangker.util.Util;

public class ChatSendFileUI extends ChatFileUI {

	private MessageViewHolder fileHolder;

	public ChatSendFileUI(MessageViewHolder fileHolder, Activity context, IUserMsgDao msgDao,
			PangkerApplication application, ImageApkIconFetcher imageFetcher) {
		super(context, imageFetcher);
		this.fileHolder = fileHolder;
		this.activity = context;
		this.mUserMsgDao = msgDao;
		this.application = application;
		fileHolder.msgRLayout.setOnClickListener(this);
	}

	ProgressListener progressListener = new ProgressListener() {
		@Override
		public void transferred(int size, int progress) {
			Message ms = new Message();
			ms.what = 1;
			ms.arg1 = size;
			ms.arg2 = progress;
			handler.sendMessage(ms);
		}

		@Override
		public void onTransfState(int state) {
			// TODO Auto-generated method stub
			Message ms = new Message();
			ms.what = 2;
			ms.arg1 = state;
			handler.sendMessage(ms);
		}
	};

	private void showResult() {
		fileHolder.pb_progressBar.setVisibility(View.GONE);
		fileHolder.btn_cancel.setVisibility(View.GONE);
		if (userMsg.getStatus() == UploadJob.UPLOAD_SUCCESS) {
			fileHolder.tv_fileize.setText("发送成功!");
		} else if (userMsg.getStatus() == UploadJob.UPLOAD_CANCEL) {
			fileHolder.tv_fileize.setText("发送取消!");
		} else {
			fileHolder.tv_fileize.setText("发送失败!");
		}
	}

	final Handler handler = new Handler() {
		public void handleMessage(Message handMessage) {
			if (handMessage.what == 1 && userMsg.getStatus() != UploadJob.UPLOAD_CANCEL) {
				int progress = (int) (((1.0 * handMessage.arg2) / handMessage.arg1) * 100);
				if (progress >= 0 && progress <= 100) {
					fileHolder.pb_progressBar.setProgress(progress);
					String appSize = Formatter.formatFileSize(activity, handMessage.arg2);
					String doneSize = Formatter.formatFileSize(activity, handMessage.arg1);
					fileHolder.tv_fileize.setText(appSize + "/" + doneSize);
				}
			}
			if (handMessage.what == 2) {
				showResult();
			}
		};
	};

	public void initView(ChatMessage msg) {
		this.userMsg = msg;

		loadMsgCover(fileHolder.iv_imagecover);
		String filesize = Formatter.formatFileSize(activity, userMsg.getFileSize());
		fileHolder.tv_fileize.setText("0KB/" + filesize);
		fileHolder.btn_cancel.setVisibility(View.VISIBLE);
		fileHolder.pb_progressBar.setMax(100);
		fileHolder.pb_progressBar.setProgress(0);

		fileHolder.btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MyFilePart uploader = application.getFilePart(getFilePartID(userMsg));
				if (uploader != null) {
					uploader.cancel();
				}
				userMsg.setStatus(UploadJob.UPLOAD_CANCEL);
				mUserMsgDao.updateUserMsgStatus(userMsg);
				application.removeFilePart(getFilePartID(userMsg));
				showResult();
			}
		});

		if ((userMsg.getStatus() == UploadJob.UPLOAD_INITOK || userMsg.getStatus() == UploadJob.UPLOAD_UPLOAGINDG)
				&& !Util.isEmpty(userMsg.getFilepath())) {
			fileHolder.pb_progressBar.setVisibility(View.VISIBLE);
			sendFile(progressListener);
		} else {
			showResult();
		}
		// String mycontent =
		// userMsg.getFilepath().substring(userMsg.getFilepath().lastIndexOf("/")
		// + 1);
		fileHolder.tv_content.setText(userMsg.getContent());

		fileHolder.tv_time.setText(Util.getDateDiff(Util.strToDate(userMsg.getTime())));
	}

	private void sendFile(final ProgressListener listener) {
		File file = new File(userMsg.getFilepath());
		if (!file.exists()) {
			fileHolder.tv_fileize.setText("文件不存在!");
			return;
		}
		if (userMsg.getStatus() == UploadJob.UPLOAD_INITOK) {
			try {
				MyFilePart uploader = new MyFilePart(userMsg.getFilepath(), file,
						new MimetypesFileTypeMap().getContentType(file), PangkerConstant.CONTENT_CHARSET, listener);
				List<Parameter> paras = new ArrayList<Parameter>();
				paras.add(new Parameter("uid", application.getMyUserID()));
				ServerSupportManager serverMana = new ServerSupportManager(activity, new IUICallBackInterface() {
					@Override
					public void uiCallBack(Object response, int caseKey) {
						// TODO Auto-generated method stub
						Log.d("uiCallBack", response.toString());
						application.removeFilePart(getFilePartID(userMsg));
						if (response != null && userMsg.getStatus() != UploadJob.UPLOAD_CANCEL) {
							UploadOfflineResult result = JSONUtil.fromJson(response.toString(),
									UploadOfflineResult.class);
							if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
								((MsgChatActivity) activity).sendFileResult(userMsg, result.getFileName());
								userMsg.setStatus(UploadJob.UPLOAD_SUCCESS);
								mUserMsgDao.updateUserMsgStatus(userMsg);
								showResult();
							} else {
								userMsg.setStatus(UploadJob.UPLOAD_FAILED);
								mUserMsgDao.updateUserMsgStatus(userMsg);
								showResult();
							}
						} else {
							userMsg.setStatus(UploadJob.UPLOAD_FAILED);
							mUserMsgDao.updateUserMsgStatus(userMsg);
							showResult();
						}
					}
				});
				// 存放上传线程文件
				application.putFilePart(getFilePartID(userMsg), uploader);
				serverMana.supportRequest(Configuration.getUpOfflineFile(), paras, uploader, null, 0);
				userMsg.setStatus(UploadJob.UPLOAD_UPLOAGINDG);
				mUserMsgDao.updateUserMsgStatus(userMsg);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				application.removeFilePart(getFilePartID(userMsg));
				// logger.error(TAG, e);
			}
		} else if (userMsg.getStatus() == UploadJob.UPLOAD_UPLOAGINDG) {
			MyFilePart uploader = application.getFilePart(getFilePartID(userMsg));
			if (uploader != null) {
				uploader.setListener(listener);
				Message ms = new Message();
				ms.what = 1;
				ms.arg1 = uploader.getFileLen();
				ms.arg2 = uploader.getDone();
				handler.sendMessage(ms);
			}
		}
	}

	@Override
	protected void reDownload() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (userMsg.getMsgType() == PangkerConstant.RES_APPLICATION) {
			Util.instalApplication(activity, userMsg.getFilepath());
		} else if (userMsg.getMsgType() == PangkerConstant.RES_DOCUMENT) {
			openReader(userMsg.getFilepath());
		} else if (userMsg.getMsgType() == PangkerConstant.RES_MUSIC) {
			openMusic();
		}
	}
}
