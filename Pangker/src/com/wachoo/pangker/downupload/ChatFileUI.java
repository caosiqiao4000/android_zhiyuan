package com.wachoo.pangker.downupload;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.DocReaderActivity;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.IMeetRoomDao;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.util.Util;

/**
 * View.OnClickListener，实现打开，已经查询下载的功能， 所有的打开操作都在各自实现的子类进行操作，不在Activity进行统一管理
 * 
 * @author zxx
 */
public abstract class ChatFileUI implements View.OnClickListener {

	public static final int MSGTYPE_P2P_MESSAGE = 0;
	public static final int MSGTYPE_GROUP_MESSAGE = 1;
	protected int messageType = MSGTYPE_P2P_MESSAGE;// 聊天记录类别 0 ：私人 1：群组
	protected ChatMessage userMsg;
	protected IMeetRoomDao mMeetRoomDao;
	protected IUserMsgDao mUserMsgDao;
	protected Activity activity;
	protected PangkerApplication application;
	protected MsgDownloader downloader = null;
	private ImageApkIconFetcher apkIconFetcher;

	public ChatFileUI(Activity activity, ImageApkIconFetcher apkIconFetcher) {
		this.activity = activity;
		this.apkIconFetcher = apkIconFetcher;
	}

	/**
	 * 获取上传文件ID
	 * 
	 * @param msg
	 * @return
	 */
	public String getFilePartID(ChatMessage msg) {
		return msg.getId() + "_" + msg.getUserId();
	}

	public void loadMsgCover(ImageView mView) {
		// TODO Auto-generated method stub
		switch (userMsg.getMsgType()) {
		case PangkerConstant.RES_MUSIC: // 音乐文件
			mView.setImageResource(R.drawable.icon46_music);
			break;
		case PangkerConstant.RES_APPLICATION:// 应用文件

			if ((ChatMessage.MSG_TO_ME.equals(userMsg.getDirection()) && userMsg.getStatus() == DownloadJob.DOWNLOAD_SUCCESS)
					|| ChatMessage.MSG_FROM_ME.equals(userMsg.getDirection()) && apkIconFetcher != null) {
				apkIconFetcher.loadImage(userMsg.getFilepath(), mView, userMsg.getFilepath());
			} else {
				mView.setImageResource(R.drawable.icon46_apk);
			}
			break;
		case PangkerConstant.RES_DOCUMENT:// 文本文件
			mView.setImageResource(R.drawable.attachment_doc);
		}
	}

	protected void downloadFile(ChatMessage msg, MsgDownloader.OnDownloadProgressListener listener) {
		if (msg.getStatus() == DownloadJob.DOWNLOAD_INIT || msg.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
			downloader = application.getMsgDownloader(getFilePartID(msg));
			if (downloader == null) {
				String fileName = "";
				String savePath = "";
				if (msg.getMsgType() == PangkerConstant.RES_VOICE) {
					fileName = msg.getFilepath();
					savePath = PangkerConstant.DIR_REC_VOICE;
				} else if (msg.getMsgType() == PangkerConstant.RES_APPLICATION) {
					fileName = msg.getFileName();
					savePath = PangkerConstant.DIR_REC_APP;
				} else if (msg.getMsgType() == PangkerConstant.RES_MUSIC) {
					fileName = msg.getFileName();
					savePath = PangkerConstant.DIR_REC_MUSIC;
				} else if (msg.getMsgType() == PangkerConstant.RES_DOCUMENT) {
					fileName = msg.getFileName();
					savePath = PangkerConstant.DIR_REC_DOC;
				} else if (msg.getMsgType() == PangkerConstant.RES_PICTURE
						|| msg.getMsgType() == MeetRoom.s_type_pic) {
					fileName = msg.getFilepath();
					savePath = PangkerConstant.DIR_REC_PIC;
				}  
				downloader = new MsgDownloader(savePath, fileName, msg.getFilepath(), listener);
				msg.setStatus(DownloadJob.DOWNLOADING);
				application.putMsgDownloader(getFilePartID(msg), downloader);
				downloader.start();
			}
			
			if (this.messageType == MSGTYPE_GROUP_MESSAGE) {
				mMeetRoomDao.updateRoomHistory((MeetRoom) msg);
			} else if (this.messageType == MSGTYPE_P2P_MESSAGE) {
				mUserMsgDao.updateUserMsgStatus(msg);
			}
		} else {
			downloader = application.getMsgDownloader(getFilePartID(msg));
			if (downloader != null) {
				downloader.setOnDownloadProgressListener(listener);
			} else {// 接受的时候退出或其他原因导致接受失败，那么就要重新接受
				reDownload();
			}
		}
	}

	protected void showToast(String toast) {
		// TODO Auto-generated method stub
		Toast.makeText(activity, toast, Toast.LENGTH_SHORT).show();
	}

	protected void showToast(int toast) {
		// TODO Auto-generated method stub
		Toast.makeText(activity, toast, Toast.LENGTH_SHORT).show();
	}

	// 接受的时候退出或其他原因导致接受失败，那么就要重新接受
	protected abstract void reDownload();

	// >>>>> wangxin 播放音乐
	private void playMusic(PlayControl playcontrol) {
		// TODO Auto-generated method stub
		Intent intentSerivce = new Intent(activity, MusicService.class);
		intentSerivce.putExtra("control", playcontrol);
		activity.startService(intentSerivce);
	}

	// >>>>>> wangxin 跳转到音乐播放界面
	private void toMusicPlayerActivity() {
		Intent intent = new Intent(activity, MusicPlayActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		activity.startActivity(intent);
	}

	private MusicInfo toMusicInfo(ChatMessage msgItem) {
		MusicInfo musicInfo = new MusicInfo();
		musicInfo.setMusicPath(msgItem.getFilepath());
		File file = new File(msgItem.getFilepath());
		musicInfo.setMusicName(file.getName());
		return musicInfo;
	}

	protected void openMusic() {
		// TODO Auto-generated method stub
		List<MusicInfo> musicInfos = new ArrayList<MusicInfo>();
		musicInfos.add(toMusicInfo(userMsg));
		application.setMusiList(musicInfos);
		application.setPlayMusicIndex(0);
		playMusic(PlayControl.MUSIC_START_PLAY);// >>>> wangxin add 启动音乐服务
		toMusicPlayerActivity();// >>>>>> wangxin add 跳转到音乐播放界面
	}

	protected void openReader(String path) {
		if (!Util.isExitFileSD(path)) {
			showToast("抱歉，文件不存在!");
			return;
		}
		IResDao resDao = new ResDaoImpl(activity);
		DirInfo dirInfo = resDao.getResByPath(path, application.getMyUserID());
		Intent intent = new Intent(activity, DocReaderActivity.class);
		if (dirInfo != null) {
			intent.putExtra("DirInfo", dirInfo);
		} else {
			intent.putExtra("sdPath", path);
		}
		activity.startActivity(intent);
	}

	protected void lookPic(String path) {
		// TODO Auto-generated method stub
		ArrayList<PictureViewInfo> imagePathes = new ArrayList<PictureViewInfo>();

		PictureViewInfo pictureViewInfo = new PictureViewInfo();
		pictureViewInfo.setPicPath(path);
		imagePathes.add(pictureViewInfo);
		Intent it = new Intent(activity, PictureBrowseActivity.class);
		it.putExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY, imagePathes);
		it.putExtra("index", 0);
		activity.startActivity(it);
	}

}
