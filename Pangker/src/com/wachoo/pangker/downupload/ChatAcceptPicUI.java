package com.wachoo.pangker.downupload;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.adapter.MsgTalkAdapter.MessageViewHolder;
import com.wachoo.pangker.db.IMeetRoomDao;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.image.PKIconResizer;

public class ChatAcceptPicUI extends ChatFileUI {

	private MessageViewHolder acceptfileHolder;
	private ImageLocalFetcher mImageWorker;
	private PKIconResizer mImageResizer;

	public ChatAcceptPicUI(MessageViewHolder acceptfileHolder, Activity context, Object msgDao,
			PangkerApplication application, ImageLocalFetcher mImageWorker, PKIconResizer mImageResizer,
			int messageType, ImageApkIconFetcher imageFetcher) {
		super(context, imageFetcher);
		this.acceptfileHolder = acceptfileHolder;
		this.activity = context;
		this.messageType = messageType;
		if (this.messageType == MSGTYPE_GROUP_MESSAGE) {
			this.mMeetRoomDao = (IMeetRoomDao) msgDao;
		} else if (this.messageType == MSGTYPE_P2P_MESSAGE) {
			this.mUserMsgDao = (IUserMsgDao) msgDao;
		}
		this.application = application;
		this.mImageWorker = mImageWorker;
		this.mImageResizer = mImageResizer;

		acceptfileHolder.msgRLayout.setOnClickListener(this);
		acceptfileHolder.iv_imagecover.setAdjustViewBounds(true);
		acceptfileHolder.iv_imagecover.setMinimumHeight(100);
		acceptfileHolder.iv_imagecover.setMinimumWidth(100);
	}

	final Handler downHandler = new Handler() {
		@Override
		public void handleMessage(Message mssg) {
			// TODO Auto-generated method stub
			if (mssg.what == 1) {
				acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
				if (mssg.arg1 == DownloadJob.DOWNLOAD_SUCCESS) {
					String sdpath = mssg.obj.toString();
					userMsg.setStatus(DownloadJob.DOWNLOAD_SUCCESS);
					userMsg.setFilepath(sdpath);
					acceptfileHolder.iv_status.setVisibility(View.GONE);
					mImageWorker.loadImage(sdpath, acceptfileHolder.iv_imagecover, sdpath);
				} else {
					acceptfileHolder.iv_imagecover.setImageResource(R.drawable.chat_balloon_break);
					acceptfileHolder.iv_status.setVisibility(View.VISIBLE);
					acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
					userMsg.setStatus(DownloadJob.DOWNLOAD_FAILE);
				}
				application.removeMsgDownloader(getFilePartID(userMsg));
				if (messageType == MSGTYPE_P2P_MESSAGE) {
					((MsgChatActivity) activity).receiveFileResult(userMsg);
				}
			}
			if (mssg.what == 2) {
				acceptfileHolder.pb_progressBar.setProgress(mssg.arg1 * 100 / mssg.arg2);
			}
		}
	};

	private void downloadPic() {
		// TODO Auto-generated method stub
		acceptfileHolder.iv_imagecover.setImageResource(R.drawable.listmod_bighead_photo_default);
		acceptfileHolder.pb_progressBar.setVisibility(View.VISIBLE);
		downloadFile(userMsg, new MsgDownloader.OnDownloadProgressListener() {
			@Override
			public void onDownloadResultListener(int result, String filepath) {
				// TODO Auto-generated method stub
				Message msg = new Message();
				msg.what = 1;
				msg.arg1 = result;
				msg.obj = filepath;
				downHandler.sendMessage(msg);
			}

			@Override
			public void onDownloadSizeListener(int len, int progress) {
				// TODO Auto-generated method stub
				Message msg = new Message();
				msg.what = 2;
				msg.arg1 = progress;
				msg.arg2 = len;
				downHandler.sendMessage(msg);
			}
		});
	}

	public void initView(ChatMessage userMsg) {
		// TODO Auto-generated method stub
		this.userMsg = userMsg;
		mImageResizer.loadImage(userMsg.getUserId(), acceptfileHolder.iv_usericon);
		acceptfileHolder.pb_progressBar.setMax(100);
		acceptfileHolder.iv_imagecover.setImageResource(R.drawable.photolist_head);
		acceptfileHolder.iv_status.setVisibility(View.GONE);

		if (userMsg.getStatus() == DownloadJob.DOWNLOADING || userMsg.getStatus() == DownloadJob.DOWNLOAD_INIT) {
			downloadPic();
		} else if (userMsg.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
			String sdpath = userMsg.getFilepath();
			acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
			mImageWorker.loadImage(sdpath, acceptfileHolder.iv_imagecover, sdpath);
		} else if (userMsg.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
			acceptfileHolder.iv_imagecover.setImageResource(R.drawable.chat_balloon_break);
			acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
		} else {
			acceptfileHolder.pb_progressBar.setVisibility(View.GONE);
		}
	}

	@Override
	protected void reDownload() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// 如果文件下载成功!
		if (userMsg.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
			lookPic(userMsg.getFilepath());
		}
		// 如果文件下载失败!
		else if (userMsg.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
			downloadPic();
		}
	}
}
