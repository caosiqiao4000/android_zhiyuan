package com.wachoo.pangker.downupload;

import java.util.EventListener;

public interface DownloadListenter extends EventListener {

	public long getDownloaderID();

	public void refreshProgressUI(DownloadAsyncTask mDownloadAsyncTask);
}
