package com.wachoo.pangker.socket;

public class CSock {
	
	private int sessionNo = 0;
	private int m_type;
	private int m_sockID;
	private boolean trust;

	public CSock() {
		m_type = 0;
		m_sockID = 0;
	}
	
	public CSock(int m_type, int m_sockID, int sessionNo, boolean trust) {
		super();
		this.sessionNo = sessionNo;
		this.m_type = m_type;
		this.m_sockID = m_sockID;
		this.trust = trust;
	}

	public CSock(int m_type, int m_sockID, boolean trust) {
		super();
		this.m_type = m_type;
		this.m_sockID = m_sockID;
		this.trust = trust;
	}

	public static CSock getSock(CSock sock, boolean trust) {
		sock.setTrust(trust);
		return sock;
	}

	public void setType(int type) {
		m_type = type;
	}

	public void setSockID(int sockID) {
		m_sockID = sockID;
	}

	public int getType() {
		return m_type;
	}

	public int getSockID() {
		return m_sockID;
	}

	public boolean isTrust() {
		return trust;
	}

	public void setTrust(boolean trust) {
		this.trust = trust;
	}

	@Override
	public String toString() {
		return "CSock [m_type=" + m_type + ", m_sockID=" + m_sockID + ", trust=" + trust + ", sessionNo=" + sessionNo + "]";
	}

	public int getSessionNo() {
		return sessionNo;
	}

	public void setSessionNo(int sessionNo) {
		this.sessionNo = sessionNo;
	}

}
