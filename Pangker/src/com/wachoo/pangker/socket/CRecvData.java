package com.wachoo.pangker.socket;

public class CRecvData
{
	private int		m_Size;
	private byte[]	m_Data;
	
	public CRecvData()
	{
		m_Size	= 0;
//		m_Data	= '\0';
	}
	public void setData(byte[] Data)
	{
		m_Data	= Data;
	}
	
	public void setSize(int Size)
	{
		m_Size	= Size;
	}
	
	public byte[] getData()
	{
		return m_Data;
	}
	
	public int getSize()
	{
		return m_Size;
	}
};
