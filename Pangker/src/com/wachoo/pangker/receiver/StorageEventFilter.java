package com.wachoo.pangker.receiver;

import android.content.Intent;
import android.content.IntentFilter;

/**
 * 
 * @author wangxin
 * 
 */
public class StorageEventFilter extends IntentFilter {

	// 在IntentFilter中选择你要监听的行为
	// MEDIA_BAD_REMOVAL:表明SDCard 被卸载前己被移除
	// MEDIA_CHECKING:表明对象正在磁盘检查
	// MEDIA_MOUNTED:表明sd对象是存在并具有读/写权限
	// MEDIA_MOUNTED_READ_ONLY:表明对象权限为只读
	// MEDIA_NOFS:表明对象为空白或正在使用不受支持的文件系统
	// MEDIA_REMOVED:如果不存在 SDCard 返回
	// MEDIA_SHARED:如果 SDCard 未安装 ，并通过 USB 大容量存储共享 返回
	// MEDIA_UNMOUNTABLE:返回 SDCard 不可被安装 如果 SDCard 是存在但不可以被安装
	// MEDIA_UNMOUNTED:返回 SDCard 已卸掉如果 SDCard 是存在但是没有被安装
	public StorageEventFilter() {
		// TODO Auto-generated constructor stub

		addAction(Intent.ACTION_MEDIA_BAD_REMOVAL);// 表明SDCard
													// 被卸载前己被移除
		addAction(Intent.ACTION_MEDIA_MOUNTED);// 表明sd对象是存在并具有读/写权限
		addAction(Intent.ACTION_MEDIA_REMOVED);// 如果不存在 SDCard 返回
		addAction(Intent.ACTION_MEDIA_UNMOUNTED);// 返回 SDCard 已卸掉如果
													// SDCard
													// 是存在但是没有被安装
		addDataScheme("file");
	}
}
