package com.wachoo.pangker.listener;

import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.wachoo.pangker.util.Util;

public class TextButtonWatcher implements TextWatcher {

	private Button btnSend;
	private int maxLenght = 0;// 限制字数
	private CharSequence temp;
	public EditText mEditText;

	public TextButtonWatcher(Button btnSend) {
		super();
		this.btnSend = btnSend;
		btnSend.setTextColor(Color.GRAY);
	}

	public TextButtonWatcher(Button btnSend, int maxLenght, EditText mEditText) {
		super();
		this.btnSend = btnSend;
		btnSend.setTextColor(Color.GRAY);
		this.maxLenght = maxLenght;
		this.mEditText = mEditText;
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		if (Util.isEmpty(arg0.toString())) {
			btnSend.setTextColor(Color.GRAY);
			btnSend.setEnabled(false);
		} else {
			btnSend.setEnabled(true);
			btnSend.setTextColor(Color.parseColor("#333333"));
		}
		if (mEditText == null || maxLenght <= 0) {
			return;
		}
		try {
			if (Util.getHalfStringLength(temp.toString()) > maxLenght) {
				int index = Util.cutStringHalfLength(temp.toString(), maxLenght);
				arg0.delete(index, arg0.length());
				this.mEditText.setText(arg0);
				this.mEditText.setSelection(arg0.length());
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		if (maxLenght > 0) {
			temp = arg0;
		}
	}

}
