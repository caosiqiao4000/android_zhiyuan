package com.wachoo.pangker.listener;

import java.io.UnsupportedEncodingException;

import android.text.InputFilter;
import android.text.Spanned;

import com.wachoo.pangker.util.Util;

public class EdInputFilter implements InputFilter {

	private int max_limit = 0;
	
	public EdInputFilter(int max_limit) {
		super();
		this.max_limit = max_limit;
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end,
			Spanned dest, int dstart, int dend) {
		// TODO Auto-generated method stub
		try {
			// 转换成中文字符集的长度
			int destLen = Util.getHalfStringLength(dest.toString());
			int sourceLen = Util.getHalfStringLength(source.toString());
			// 如果超过100个字符
			if (destLen + sourceLen > max_limit) {
				return "";
			}
			// 如果按回退键
			if (source.length() < 1 && (dend - dstart >= 1)) {
				return dest.subSequence(dstart, dend - 1);
			}
			// 其他情况直接返回输入的内容
			return source;

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

}
