package com.wachoo.pangker.listener;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * 
 * 碰撞灵敏度，可以通过调节：MIN_FORCE、MIN_DIRECTION_CHANGE、
 * MAX_PAUSE_BETHWEEN_DIRECTION_CHANGE和MAX_TOTAL_DURATION_OF_SHAKE实现
 * 
 * @author chensw
 * 
 */
public class ShakeEventListener implements SensorEventListener {
	/**
	 * Minimum movement force to consider.
	 * 
	 * 碰撞力度（传感器的偏移量）
	 * 
	 */
	private static final int MIN_FORCE = 15;

	/**
	 * Minimum times in a shake gesture that the direction of movement needs to
	 * change.
	 * 
	 * 一次碰撞中的震荡次数(方向改变次数)
	 * 
	 */
	private static final int MIN_DIRECTION_CHANGE = 3;

	/**
	 * Maximum pause between movements.
	 * 
	 * 最大反向停顿时长
	 */
	private static final int MAX_PAUSE_BETHWEEN_DIRECTION_CHANGE = 200;

	/**
	 * Maximum allowed time for shake gesture.
	 * 
	 * 一次碰撞允许的最大时长
	 */
	private static final int MAX_TOTAL_DURATION_OF_SHAKE = 400;

	/**
	 * Time when the gesture started.
	 * 
	 * 首次有效偏移量发生时间
	 */
	private long mFirstDirectionChangeTime = 0;

	/**
	 * Time when the last movement started.
	 * 
	 * 最后一次有效偏移量发生时间
	 */
	private long mLastDirectionChangeTime;

	/**
	 * How many movements are considered so far.
	 * 
	 * 一共有多少次有效偏移量
	 */
	private int mDirectionChangeCount = 0;

	/** The last x position. */
	private float lastX = 0;

	/** The last y position. */
	private float lastY = 0;

	/** The last z position. */
	private float lastZ = 0;

	/** OnShakeListener that is called when shake is detected. */
	private OnShakeListener mShakeListener;

	/**
	 * Interface for shake gesture.
	 */
	public interface OnShakeListener {
		/**
		 * Called when shake gesture is detected.
		 */
		void onShake();
	}

	public void setOnShakeListener(OnShakeListener listener) {
		mShakeListener = listener;
	}

	public void onSensorChanged(SensorEvent se) {
		// get sensor data
		// 取得传感器参数值
		float x = se.values[SensorManager.DATA_X];
		float y = se.values[SensorManager.DATA_Y];
		float z = se.values[SensorManager.DATA_Z];

		// calculate movement
		// 计算偏移量
		float totalMovement = Math.abs(x + y + z - lastX - lastY - lastZ);

		// 如果偏移量大于最小偏移量
		if (totalMovement > MIN_FORCE) {

			Log.d("System.out", "totalMovement: " + totalMovement);

			// get time
			long now = System.currentTimeMillis();

			// store first movement time
			// 保存首次有效偏移量发生时间
			if (mFirstDirectionChangeTime == 0) {
				mFirstDirectionChangeTime = now;
				mLastDirectionChangeTime = now;
			}

			// check if the last movement was not long ago
			long lastChangeWasAgo = now - mLastDirectionChangeTime;

			// 单次震荡的时长要小于最大停顿时长
			if (lastChangeWasAgo < MAX_PAUSE_BETHWEEN_DIRECTION_CHANGE) {

				// store movement data
				mLastDirectionChangeTime = now;
				mDirectionChangeCount++;

				// store last sensor data
				lastX = x;
				lastY = y;
				lastZ = z;

				// check how many movements are so far
				// 有效震荡次数大于最小震荡次数
				if (mDirectionChangeCount >= MIN_DIRECTION_CHANGE) {

					// check total duration
					long totalDuration = now - mFirstDirectionChangeTime;

					// 震荡时长小于最大碰撞时长
					if (totalDuration < MAX_TOTAL_DURATION_OF_SHAKE) {

						Log.d("System.out", "totalDuration: " + totalDuration);

						mShakeListener.onShake();
						resetShakeParameters();
					}
				}

			} else {
				resetShakeParameters();
			}
		}
	}

	/**
	 * Resets the shake parameters to their default values.
	 * 
	 * 重置碰撞参数
	 * 
	 */
	private void resetShakeParameters() {
		mFirstDirectionChangeTime = 0;
		mDirectionChangeCount = 0;
		mLastDirectionChangeTime = 0;
		lastX = 0;
		lastY = 0;
		lastZ = 0;
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

}
