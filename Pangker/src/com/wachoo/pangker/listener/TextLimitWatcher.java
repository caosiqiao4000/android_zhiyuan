package com.wachoo.pangker.listener;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

public class TextLimitWatcher implements TextWatcher{
	
	private int maxlimit;
	private TextView txtCount;
	private EditText mEditText;
	CharSequence temp;

	public TextLimitWatcher(int maxlimit, TextView txtCount, EditText mEditText) {
		super();
		this.maxlimit = maxlimit;
		this.txtCount = txtCount;
		this.mEditText = mEditText;
		String strCount = mEditText.getEditableText().toString().trim();
		if (strCount.length() > maxlimit) {
			this.mEditText.setText(strCount.substring(0, maxlimit));
		}
		this.txtCount.setText("" + (maxlimit - strCount.length()));
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		txtCount.setText("" + (maxlimit - s.length()));
		int selectionStart = mEditText.getSelectionStart();
		int selectionEnd = mEditText.getSelectionEnd();
		if (temp.length() > maxlimit) {
			selectionStart = maxlimit + 1;
			if (selectionStart - 1 >= 0 && selectionStart - 1 <= selectionEnd) {
				s.delete(selectionStart - 1, selectionEnd);
				mEditText.setText(s);
				mEditText.setSelection(s.length());// 设置光标在最后
			}
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub
		temp = s;
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
	}
}
