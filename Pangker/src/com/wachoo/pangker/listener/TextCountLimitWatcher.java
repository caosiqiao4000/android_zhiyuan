package com.wachoo.pangker.listener;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import com.wachoo.pangker.util.Util;

/**
 * 
 * EditText 字数限制
 * 
 * @author zhengjy
 * 
 */
public class TextCountLimitWatcher implements TextWatcher {

	private String TAG = "TextHalfCountLimitWatcher";// log tag
	private int maxLenght = 18;// 限制字数
	private CharSequence temp;
	public EditText mEditText;

	public TextCountLimitWatcher() {
	}

	public TextCountLimitWatcher(int maxLenght, EditText mEditText) {
		this.mEditText = mEditText;
		this.maxLenght = maxLenght;
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		temp = s;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void afterTextChanged(Editable s) {
		try {
			Log.i(TAG, "cutBefore=" + s.toString());
			if (Util.getHalfStringLength(temp.toString()) > maxLenght) {
				int index = Util.cutStringHalfLength(temp.toString(), maxLenght);
				s.delete(index, s.length());
				this.mEditText.setText(s);
				this.mEditText.setSelection(s.length());
			}
			Log.i(TAG, "cutAfter=" + s.toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
