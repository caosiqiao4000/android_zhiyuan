package com.wachoo.pangker.adapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;

public class LocalDocAdapter extends BaseAdapter{

	private Context context;
	private List<File> docList;
	private List<Boolean> mchecked;
	private boolean flagShow = true;

	public LocalDocAdapter(Context context, boolean flagShow) {
		super();
		this.context = context;
		this.flagShow = flagShow;
		docList = new ArrayList<File>();
		mchecked = new ArrayList<Boolean>();
	}
	
	public void addDoc(File ofile) {
		docList.add(ofile);
		mchecked.add(false);
		notifyDataSetChanged();
	}
	
	public void selectByIndex(int index, boolean flag){
		mchecked.set(index, flag);
		notifyDataSetChanged();
	}
	
	public List<File> getSeletedMembers() {
		List<File> selectedMembers = new ArrayList<File>();
		int temp = docList.size();
		for (int i = 0; i < temp; i++) {
			if(mchecked.get(i)){
				selectedMembers.add(docList.get(i));
			}
		}
		return selectedMembers;
	}
	
	public void setSelectedAll(boolean isSelectedAll) {
		int temp = docList.size();
		for (int i = 0; i < temp; i++) {
			mchecked.set(i, isSelectedAll);
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return docList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return docList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		File item = (File) getItem(position);
		ViewHolderDoc holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.select_doc_item, null);
			holder = new ViewHolderDoc();
			holder.iv_musicCover = (ImageView) convertView.findViewById(R.id.ImageViewMusicCover);
			holder.tv_musicName = (TextView)convertView.findViewById(R.id.tv_musicName);
			holder.tv_musicPlayer = (TextView) convertView.findViewById(R.id.tv_musicPlayer);
			holder.selected = (CheckBox) convertView.findViewById(R.id.Music_CheckBox);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolderDoc) convertView.getTag();
		}
		if(item != null){
			if(!flagShow){
				holder.selected.setVisibility(View.INVISIBLE);
			} else {
				holder.selected.setVisibility(View.VISIBLE);
				holder.selected.setChecked(mchecked.get(position));
			}
			if(item.getName().contains(".doc")){
				holder.iv_musicCover.setImageResource(R.drawable.attachment_doc);
			} else if(item.getName().contains(".txt")){
				holder.iv_musicCover.setImageResource(R.drawable.attachment_doc);
			} else {
				holder.iv_musicCover.setImageResource(R.drawable.attachment_doc);
			}
			holder.tv_musicName.setText(item.getName());
			String fileSize = Formatter.formatFileSize(context,item.length());
			holder.tv_musicPlayer.setText("文件大小:"+ fileSize);
			//simpleDateFormat.format(new java.util.Date(item.lastModified()))
		}
		return convertView;
	}
	
	public class ViewHolderDoc{
		public ImageView iv_musicCover; //封面 
		public TextView tv_musicName;   //歌曲名
		public TextView tv_musicPlayer;   //歌手
		public CheckBox selected;  //是否选择一组
	}

}
