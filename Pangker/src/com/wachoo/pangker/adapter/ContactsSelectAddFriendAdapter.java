package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.image.PKIconResizer;

public class ContactsSelectAddFriendAdapter extends BaseAdapter {

	static final String TAG = "ContactsSelectAdapter";
	private List<LocalContacts> members;
	private Context context;
	private List<Boolean> mchecked;
	private boolean isSelectedAll = false;
	private int index;
	private PKIconResizer mImageResizer;

	public ContactsSelectAddFriendAdapter(Context context, List<LocalContacts> arrayList) {
		this.members = arrayList;
		this.context = context;
		mchecked = new ArrayList<Boolean>();
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		for (int i = 0; i < members.size(); i++) {
			mchecked.add(false);
		}
	}

	public List<LocalContacts> getMembers() {
		return members;
	}

	public void setMembers(List<LocalContacts> members) {
		this.members = members;
	}

	public int getSelectCount() {
		int count = 0;
		Iterator<Boolean> iter = mchecked.iterator();
		while (iter.hasNext()) {
			if (iter.next()) {
				count++;
			}
		}
		return count;
	}

	/**
	 * checkbox 点击效果
	 * 
	 * @param index
	 */
	public boolean onCheckBoxClick(int index) {
		boolean isChecked = !mchecked.get(index);
		mchecked.set(index, isChecked);
		this.notifyDataSetChanged();
		return isChecked;
	}

	public void onUnCheckedClick(String userid) {
		for (int i = 0; i < members.size(); i++) {
			if (members.get(i).getUserId().equals(userid)) {
				mchecked.set(i, false);
				this.notifyDataSetChanged();
				break;
			}
		}
	}

	public List<LocalContacts> getSeletedMembers() {
		List<LocalContacts> selectedMembers = new ArrayList<LocalContacts>();
		for (int i = 0; i < members.size(); i++) {
			if (mchecked.get(i)) {
				selectedMembers.add(members.get(i));
			}
		}
		return selectedMembers;
	}

	public boolean isSelectedAll() {
		isSelectedAll = true;
		for (int i = 0; i < members.size(); i++) {
			if (!mchecked.get(i)) {
				isSelectedAll = false;
				break;
			}
		}
		return isSelectedAll;
	}

	public int getIndex() {
		return index;
	}

	public void setSelectedAll(boolean isSelectedAll) {
		this.isSelectedAll = isSelectedAll;
		if (isSelectedAll) {
			for (int i = 0; i < members.size(); i++) {
				mchecked.set(i, true);
			}
			notifyDataSetChanged();
		}
	}

	public boolean getIsChecked(int index) {
		return mchecked.get(index);
	}

	public List<Boolean> getMchecked() {
		return mchecked;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return members.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return members.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolder holder;

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final LocalContacts userItem = members.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.tracegroup_member_item, null);
			holder = new ViewHolder();
			holder.selected = (CheckBox) convertView.findViewById(R.id.mtrace_checkbox);
			holder.userName = (TextView) convertView.findViewById(R.id.tv_name);
			holder.userSign = (TextView) convertView.findViewById(R.id.tv_left_bottom);
			holder.isAttention = (TextView) convertView.findViewById(R.id.tv_right_bottom);
			holder.userIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (userItem != null) {
			holder.userName.setText(userItem.getUserName() + "(" + userItem.getUserId() + ")");
			holder.selected.setChecked(mchecked.get(position));
			holder.userSign.setText(userItem.getSign());
			holder.selected.setChecked(getIsChecked(position));
			mImageResizer.loadImage(userItem.getUserId(), holder.userIcon);
		}
		return convertView;
	}

	public class ViewHolder {
		public CheckBox selected; // 是否选择一组
		public TextView userName; // 好友名称
		public TextView userSign; // 好友名称
		public TextView isAttention;
		public ImageView userIcon; // 用户头像
	}
}
