package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.response.AddressMembers;

/**
 * 
 * 单位通讯录列表
 * 
 */
public class UnitsContactUserAdapter extends BaseAdapter {
	
	private LayoutInflater layoutInflater;
	private List<AddressMembers> mContactList;
	private PKIconResizer mImageResizer;

	public UnitsContactUserAdapter(Context context, List<AddressMembers> users, PKIconResizer mImageResizer) {
		layoutInflater = LayoutInflater.from(context);
		this.mContactList = users;
		this.mImageResizer = mImageResizer;
	}

	public int getCount() {
		return mContactList == null ? 0 : mContactList.size();
	}

	public Object getItem(int position) {
		return mContactList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	ViewHolder holder = null;

	public View getView(int position, View convertView, ViewGroup parent) {
		final AddressMembers item = mContactList.get(position);

		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.contacts_address_item, null);
			holder = new ViewHolder();
			holder.ivUserIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			holder.tvContactName = (TextView) convertView.findViewById(R.id.tv_username);
			holder.tvCreateUser = (TextView) convertView.findViewById(R.id.tv_usersign);
			holder.iv_into = (ImageView) convertView.findViewById(R.id.iv_into);
			holder.iv_identity = (ImageView) convertView.findViewById(R.id.iv_identity);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tvContactName.setText(item.getRemarkname());
		holder.tvCreateUser.setText(String.valueOf(item.getMobile()));

		if (item.getUid() != null && item.getUid() != 0) {
			holder.iv_identity.setVisibility(View.VISIBLE);
			holder.iv_identity.setImageResource(R.drawable.pangker);
			mImageResizer.loadImage(String.valueOf(item.getUid()), holder.ivUserIcon);
		} else {
			holder.iv_identity.setVisibility(View.GONE);
			holder.ivUserIcon.setImageResource(R.drawable.nav_head);
		}
		return convertView;
	}

	class ViewHolder {
		public ImageView ivUserIcon;
		public TextView tvContactName;
		public TextView tvCreateUser;
		public ImageView iv_into;
		public ImageView iv_identity;
	}

	public List<AddressMembers> getmContactList() {
		return mContactList;
	}

	public void setmContactList(List<AddressMembers> mContactList) {
		this.mContactList = mContactList;
		this.notifyDataSetChanged();
	}

	/**
	 * 修改备注，页面更新
	 * 
	 * @param member
	 */
	public void updateListData(AddressMembers member) {
		for (AddressMembers element : mContactList) {
			if (element.getId() == member.getId()) {
				element.setRemarkname(member.getRemarkname());
				element.setMobile(member.getMobile());
			}
		}
		this.notifyDataSetChanged();
	}

	/**
	 * 移除某个用户，页面刷新
	 * 
	 * @param member
	 */
	public void removeBooks(AddressMembers member) {
		mContactList.remove(member);
		this.notifyDataSetChanged();
	}

}
