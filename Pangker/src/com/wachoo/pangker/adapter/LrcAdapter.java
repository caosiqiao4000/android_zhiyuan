package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.music.Lrc;

public class LrcAdapter extends BaseAdapter{

	private Context context;
	private List<Lrc> lrcList;
	
	public LrcAdapter(Context context, List<Lrc> lrcList) {
		super();
		this.context = context;
		this.lrcList = lrcList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lrcList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return lrcList.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Lrc item = (Lrc) getItem(position);
		TextView txtCoent;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.dir_item, null);
			convertView.findViewById(R.id.app_img).setVisibility(View.GONE);
			txtCoent = (TextView) convertView.findViewById(R.id.app_label);
			convertView.setTag(txtCoent);
		} else {
			txtCoent = (TextView) convertView.getTag();
		}
		txtCoent.setText(item.getName() + "(" + item.getSinger() + ")");
		return convertView;
	}

}
