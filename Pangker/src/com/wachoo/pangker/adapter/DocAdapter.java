package com.wachoo.pangker.adapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DirInfo;

public class DocAdapter extends BaseAdapter{

	private Context context;
	private List<DirInfo> dirInfoLists;
	private List<Boolean> mchecked;
	private boolean flagShow = false; //ture表示在编辑状态,要显示checkbox
	
	public DocAdapter(Context context, List<DirInfo> dirInfoLists) {
		super();
		this.context = context;
		this.dirInfoLists = dirInfoLists;
		mchecked = new ArrayList<Boolean>();
		for (int i = 0; i < dirInfoLists.size(); i++) {
			mchecked.add(false);
		}
	}
	
	public void init() {
		mchecked.clear();
		int temp = dirInfoLists.size();
		for (int i = 0; i < temp; i++) {
			mchecked.add(i,false);
		}
	}
	
	public void remove(DirInfo dirInfo) {
		// TODO Auto-generated method stub
		dirInfoLists.remove(dirInfo);
		notifyDataSetChanged();
	}
	
	public void setDirInfoLists(List<DirInfo> appList) {
		// TODO Auto-generated method stub
		this.dirInfoLists = appList;
		if(dirInfoLists == null){
			dirInfoLists = new ArrayList<DirInfo>();
		}
		mchecked.clear();
		for (int i = 0; i < dirInfoLists.size(); i++) {
			mchecked.add(false);
		}
		notifyDataSetChanged();
	}
	
	public void setFlagShow(boolean flagShow) {
		this.flagShow = flagShow;
		notifyDataSetChanged();
	}
	
	public void selectByIndex(int index, boolean flag){
		mchecked.set(index, flag);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dirInfoLists.size();
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dirInfoLists.get(position);
	}
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DirInfo item = (DirInfo) getItem(position);
		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.local_music_item, null);
			holder = new ViewHolder();
			holder.iv_musicCover = (ImageView) convertView.findViewById(R.id.ImageViewMusicCover);
			holder.tv_musicName = (TextView)convertView.findViewById(R.id.tv_musicName);
			holder.tv_proress = (TextView) convertView.findViewById(R.id.tv_musicScore);
			convertView.findViewById(R.id.tv_musicPlayer).setVisibility(View.GONE);
			holder.selected = (CheckBox) convertView.findViewById(R.id.Music_CheckBox);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolder) convertView.getTag();
		}
		if(item != null){
			if(!flagShow){ 
				holder.selected.setVisibility(View.INVISIBLE);
			} else {
				holder.selected.setVisibility(View.VISIBLE);
				holder.selected.setChecked(mchecked.get(position));
			}
			if(item.getPath().contains(".doc")){
				holder.iv_musicCover.setImageResource(R.drawable.attachment_doc);
			} else if(item.getPath().contains(".txt")){
				holder.iv_musicCover.setImageResource(R.drawable.attachment_doc);
			} else {
				holder.iv_musicCover.setImageResource(R.drawable.attachment_doc);
			}
			holder.tv_musicName.setText(item.getDirName());
			
			float fPercent = (float) (item.getRemark() * 1.0 / item.getFileCount());
			DecimalFormat df = new DecimalFormat("#0.00");
			String strPercent = df.format(fPercent * 100) + "%";
			holder.tv_proress.setText("阅读进度:" + strPercent);
		}
		return convertView;
	}
	/* 全选/全不选 */
	public int setSelectedAll(boolean isSelectedAll) {
		for (int i = 0; i < dirInfoLists.size(); i++) {
			mchecked.set(i, isSelectedAll);
		}
		notifyDataSetChanged();
		return dirInfoLists.size();
	}
	
	public List<DirInfo> getSeletedMembers() {
		List<DirInfo> selectedMembers = new ArrayList<DirInfo>();
		for (int i = 0; i < dirInfoLists.size(); i++) {
			if(mchecked.get(i)){
				selectedMembers.add(dirInfoLists.get(i));
			}
		}
		return selectedMembers;
	}
	
	public class ViewHolder{
		public ImageView iv_musicCover; //封面 
		public TextView tv_musicName;   //文件名称
		public TextView tv_proress;   //阅读进度
		public CheckBox selected;  //是否选择一组
	}

	public void addDirInfo(DirInfo dirInfo) {
		// TODO Auto-generated method stub
		mchecked.add(false);
		notifyDataSetChanged();
	}

}
