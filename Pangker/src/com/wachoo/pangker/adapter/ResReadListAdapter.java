package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.response.DocInfo;

public class ResReadListAdapter extends BaseAdapter {
	
	private Context context;
	private List<DocInfo> mDocs;

	public ResReadListAdapter(Context context, List<DocInfo> mDocs) {
		this.context = context;
		this.mDocs = mDocs;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mDocs.size();
	}

	@Override
	public DocInfo getItem(int position) {
		// TODO Auto-generated method stub
		return mDocs.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		DocInfo docInfo = getItem(position);
		ViewHolder viewHolder;

		// 缓存
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.res_file_list_item, null);
			viewHolder.iv_fileicon = (ImageView) convertView.findViewById(R.id.iv_fileicon);
			viewHolder.tv_file_name = (TextView) convertView.findViewById(R.id.tv_file_name);
			viewHolder.tv_file_size = (TextView) convertView.findViewById(R.id.tv_file_size);
			viewHolder.res_tv_collect = (TextView) convertView.findViewById(R.id.res_tv_collect);
			viewHolder.res_tv_comment = (TextView) convertView.findViewById(R.id.res_tv_comment);
			viewHolder.res_tv_loadtimes = (TextView) convertView.findViewById(R.id.res_tv_loadtimes);
			viewHolder.mRatingBar = (RatingBar) convertView.findViewById(R.id.rating_bar_show);
			viewHolder.iv_ifShare = (ImageView) convertView.findViewById(R.id.iv_share);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		// 设置数据
		if (docInfo != null) {
			viewHolder.setResInfo(docInfo);
			viewHolder.tv_file_name.setText(docInfo.getDocname());
			String fileSize = Formatter
					.formatFileSize(context, new Double(docInfo.getFileSize()).longValue());
			// String fileSize = "123KB";
			viewHolder.tv_file_size.setText(fileSize);
			// viewHolder.tv_file_detail.setText("添加时间 : " +
			// Util.TimeIntervalBtwNow(docInfo.getIssuetime()));

			viewHolder.res_tv_collect.setText("(" + docInfo.getFavorTimes() + ")");
			viewHolder.res_tv_comment.setText("(" + docInfo.getCommentTimes() + ")");
			viewHolder.res_tv_loadtimes.setText("(" + docInfo.getDownloadTimes() + ")");
			if (docInfo.getIsShare()) {
				viewHolder.iv_ifShare.setVisibility(View.VISIBLE);
			} else {
				viewHolder.iv_ifShare.setVisibility(View.GONE);
			}

			viewHolder.mRatingBar.setRating(((float) docInfo.getScore()) / 2);
			viewHolder.iv_fileicon.setImageResource(R.drawable.attachment_doc);
		}
		return convertView;
	}

	public List<DocInfo> getmDocs() {
		return mDocs;
	}

	public void setmDocs(List<DocInfo> mDocs) {
		this.mDocs = mDocs;
		notifyDataSetChanged();
	}

	class ViewHolder extends ViewBaseHolder {
		ImageView iv_fileicon;
		TextView tv_file_name;
		TextView tv_file_size;
		ImageView iv_ifShare;
		RatingBar mRatingBar;
		TextView res_tv_collect;
		TextView res_tv_comment;
		TextView res_tv_loadtimes;
	}
}
