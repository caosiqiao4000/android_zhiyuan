package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.LocalAppInfo;

public class LocalAppAdapter extends BaseAdapter{

	private Context context;
	private List<LocalAppInfo> appList;
	private List<Boolean> mchecked;
	
	public LocalAppAdapter(Context context) {
		super();
		this.context = context;
		appList = new ArrayList<LocalAppInfo>();
		mchecked = new ArrayList<Boolean>();
	}
	
	public List<Boolean> getMchecked() {
		return mchecked;
	}

	public void setMchecked(List<Boolean> mchecked) {
		this.mchecked = mchecked;
		notifyDataSetChanged();
	}

	public void addAppInfo(LocalAppInfo appInfo) {
		appList.add(appInfo);
		mchecked.add(false);
		notifyDataSetChanged();
	}
	
	public void selectByIndex(int index, boolean flag){
		mchecked.set(index, flag);
		notifyDataSetChanged();
	}
	
	public List<LocalAppInfo> getSeletedMembers() {
		List<LocalAppInfo> selectedMembers = new ArrayList<LocalAppInfo>();
		for (int i = 0; i < appList.size(); i++) {
			if(mchecked.get(i)){
				selectedMembers.add(appList.get(i));
			}
		}
		return selectedMembers;
	}
	
	public void setSelectedAll(boolean isSelectedAll) {
		for (int i = 0; i < appList.size(); i++) {
			mchecked.set(i, isSelectedAll);
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return appList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return appList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LocalAppInfo item = (LocalAppInfo) getItem(position);
		ViewHolderApp holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.local_music_item, null);
			holder = new ViewHolderApp();
			holder.iv_musicCover = (ImageView) convertView.findViewById(R.id.ImageViewMusicCover);
			holder.tv_musicName = (TextView)convertView.findViewById(R.id.tv_musicName);
			holder.tv_musicPlayer = (TextView) convertView.findViewById(R.id.tv_musicPlayer);
//			TextView marginImageView = (TextView) convertView.findViewById(R.id.tv_musicMargin);
//			marginImageView.setVisibility(View.GONE);
			holder.tv_musicScore = (TextView) convertView.findViewById(R.id.tv_musicScore);
			holder.selected = (CheckBox) convertView.findViewById(R.id.Music_CheckBox);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolderApp) convertView.getTag();
		}
		if(item != null){
			holder.tv_musicScore.setVisibility(View.GONE);
			holder.selected.setChecked(mchecked.get(position));
			holder.iv_musicCover.setImageDrawable(item.appIcon);
			holder.tv_musicName.setText(item.appName);
			String fileSize = Formatter.formatFileSize(context, item.getSize());
			holder.tv_musicPlayer.setText("文件大小:" + fileSize);
		}
		return convertView;
	}
	
	public class ViewHolderApp{
		public ImageView iv_musicCover; //封面 
		public TextView tv_musicName;   //歌曲名
		public TextView tv_musicPlayer;   //歌手
		public TextView tv_musicScore;   //歌手
		public CheckBox selected;  //是否选择一组
	}

}
