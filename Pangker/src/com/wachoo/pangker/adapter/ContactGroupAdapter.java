package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.ContactGroup;

public class ContactGroupAdapter extends BaseAdapter {
	
	private Context context;
	private List<ContactGroup> groups;
	private int index = 0;//表明用户选择的是哪一组

	public ContactGroupAdapter(Context context, List<ContactGroup> groups) {
		this.context = context;
		this.groups = groups;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return groups.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return groups.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;// Long.parseLong(groupList.get(position).groupId);
	}

	public void setIndex(int index) {
		this.index = index;
		this.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null){
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.contact_group_item, null);
			holder.tvGroupName = (TextView) convertView.findViewById(R.id.tv_group_name);
			holder.tvUserCount = (TextView) convertView.findViewById(R.id.tv_user_count);
			holder.imgIcon = (ImageView) convertView.findViewById(R.id.icon);
			holder.layout = (LinearLayout) convertView.findViewById(R.id.widget10);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
        if(position == index){
        	holder.layout.setBackgroundResource(R.drawable.circle_list_bg_p);
        	holder.imgIcon.setVisibility(View.VISIBLE);
        } else {
        	holder.layout.setBackgroundResource(R.drawable.circle_list_bg);
        	holder.imgIcon.setVisibility(View.GONE);
        }
        final ContactGroup group = groups.get(position);
        
		String groupName = group.getName();
		holder.tvGroupName.setText(groupName != null ? groupName : "");
		
		int userCount = 0;
		if(group.getMembers() != null){
			userCount = group.getMembers().size();
		}
		holder.tvUserCount.setText(userCount + "名联系人");

		return convertView;
	}
	
	class ViewHolder {
		LinearLayout layout;
		TextView tvGroupName;
		TextView tvUserCount;
		ImageView imgIcon;
	}

	public List<ContactGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<ContactGroup> groups) {
		this.groups = groups;
		this.notifyDataSetChanged();
	}
}
