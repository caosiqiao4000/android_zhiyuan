package com.wachoo.pangker.adapter;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.msg.MsgInfoActivity;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.util.SmileyParser;
import com.wachoo.pangker.util.Util;

public class MsgInfoAdapter extends BaseAdapter {

	private Activity context;
	private List<MsgInfo> msgInfos;
	private SmileyParser parser;
	private PKIconResizer mImageResizer;
	private ImageResizer mImageWorker;

	public MsgInfoAdapter(Activity context, List<MsgInfo> msgInfos) {
		super();
		this.context = context;
		this.msgInfos = msgInfos;
		parser = new SmileyParser(context);
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		mImageWorker = PangkerManager.getGroupIconResizer(context.getApplicationContext());
	}

	public void delMsgInfo(MsgInfo msgInfo) {
		msgInfos.remove(msgInfo);
		notifyDataSetChanged();
	}

	public boolean ifReadAll() {
		for (MsgInfo mInfo : msgInfos) {
			if (mInfo.getUncount() > 0) {
				return false;
			}
		}
		return true;
	}

	public List<MsgInfo> getMsgInfos() {
		return msgInfos;
	}

	public void updateMsgInfo(MsgInfo msgInfo) {
		boolean ifEixt = false;
		for (MsgInfo mInfo : msgInfos) {
			if (msgInfo.getMsgId() == mInfo.getMsgId()) {
				ifEixt = true;
				mInfo.setFromId(msgInfo.getFromId());
				mInfo.setContent(msgInfo.getContent());
				mInfo.setTime(msgInfo.getTime());
				mInfo.setTheme(msgInfo.getTheme());
				mInfo.setResType(msgInfo.getResType());
				mInfo.setUncount(mInfo.getUncount() + msgInfo.getUncount());
				break;
			}
		}
		if (!ifEixt) {
			msgInfos.add(0, msgInfo);
		}
		Collections.sort(msgInfos, MsgInfoc);
		notifyDataSetChanged();
	}

	public void clearMsgInfoCount(long msgId) {
		for (MsgInfo mInfo : msgInfos) {
			if (msgId == mInfo.getMsgId()) {
				mInfo.setUncount(0);
			}
		}
		notifyDataSetChanged();
	}

	public void clearMsgIns() {
		// TODO Auto-generated method stub
		msgInfos.clear();
		notifyDataSetChanged();
	}

	Comparator<MsgInfo> MsgInfoc = new Comparator<MsgInfo>() {
		@Override
		public int compare(MsgInfo object1, MsgInfo object2) {
			// TODO Auto-generated method stub
			return Util.formatDate(object2.getTime()).compareTo(Util.formatDate(object1.getTime()));
		}
	};

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return msgInfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return msgInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final MsgInfo item = (MsgInfo) getItem(position);
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.notice_item, null);
			holder.imgSys = (ImageView) convertView.findViewById(R.id.iv_sysicon);
			holder.msgIcon = (ImageView) convertView.findViewById(R.id.iv_msgicon);
			holder.txtTheme = (TextView) convertView.findViewById(R.id.tv_name);
			holder.txtTime = (TextView) convertView.findViewById(R.id.tv_time);
			holder.txtContent = (TextView) convertView.findViewById(R.id.tv_content);
			holder.txtCount = (TextView) convertView.findViewById(R.id.txt_status);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.imgSys.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((MsgInfoActivity) context).lookUserInfo(item);
			}
		});

		Date oldDate = Util.formatDate(item.getTime());
		holder.txtTime.setText(Util.getDateDiff(oldDate));

		if (item.getUncount() > 0) {
			holder.txtCount.setText(String.valueOf(item.getUncount()));
			holder.txtCount.setVisibility(View.VISIBLE);
		} else {
			holder.txtCount.setVisibility(View.GONE);
		}

		holder.txtContent.setText(parser.setSmileySpans(item.getContent() + " "));

		if (item.getResType() == PangkerConstant.RES_PICTURE) {
			holder.msgIcon.setVisibility(View.VISIBLE);
			holder.msgIcon.setImageResource(R.drawable.icon46_photo);
		} else if (item.getResType() == PangkerConstant.RES_LOCATION) {
			holder.msgIcon.setVisibility(View.VISIBLE);
			holder.msgIcon.setImageResource(R.drawable.toolbar_but_gps_p);
		} else if (item.getResType() == PangkerConstant.RES_APPLICATION) {
			holder.msgIcon.setVisibility(View.VISIBLE);
			holder.msgIcon.setImageResource(R.drawable.icon46_apk);
		} else if (item.getResType() == PangkerConstant.RES_DOCUMENT) {
			holder.msgIcon.setVisibility(View.VISIBLE);
			holder.msgIcon.setImageResource(R.drawable.attachment_doc);
		} else if (item.getResType() == PangkerConstant.RES_MUSIC) {
			holder.msgIcon.setVisibility(View.VISIBLE);
			holder.msgIcon.setImageResource(R.drawable.icon46_music);
		} else if (item.getResType() == PangkerConstant.RES_SPEAK_SMS) {
			holder.msgIcon.setVisibility(View.VISIBLE);
			holder.msgIcon.setImageResource(R.drawable.chat_sms);
		} else if (item.getResType() == PangkerConstant.RES_VOICE) {
			holder.msgIcon.setVisibility(View.VISIBLE);
			holder.msgIcon.setImageResource(R.drawable.record_other_normal);
		} else if (item.getResType() == PangkerConstant.RES_CALL) {
			holder.msgIcon.setVisibility(View.VISIBLE);
			holder.msgIcon.setImageResource(R.drawable.icon46_call);
		} else {
			holder.msgIcon.setVisibility(View.GONE);
		}

		switch (item.getType()) {
		case MsgInfo.MSG_PERSONAL_CHAT:
			mImageResizer.loadImage(item.getFromId(), holder.imgSys);
			holder.txtTheme.setText(item.getTheme());
			break;
		case MsgInfo.MSG_GROUP_CHAT:
			String groupUrl = Configuration.getGroupCover() + item.getFromId();
			mImageWorker.loadImage(groupUrl, holder.imgSys, item.getFromId());
			holder.txtTheme.setText(item.getTheme());
			break;

		case MsgInfo.MSG_RECOMMEND:
			holder.imgSys.setImageResource(R.drawable.sys_res_recommend);
			holder.txtTheme.setText("推荐消息");
			break;

		case MsgInfo.MSG_TIP:
			holder.imgSys.setImageResource(R.drawable.message_tip_head);
			holder.msgIcon.setVisibility(View.GONE);
			holder.txtTheme.setText("消息提示");
			break;
		default:
			holder.imgSys.setImageResource(R.drawable.sys_infos);
			holder.txtTheme.setText("系统消息");
			break;
		}

		return convertView;
	}

	class ViewHolder {
		ImageView imgSys;
		ImageView msgIcon;
		TextView txtContent;
		TextView txtTheme;
		TextView txtTime;
		TextView txtCount;
	}

}
