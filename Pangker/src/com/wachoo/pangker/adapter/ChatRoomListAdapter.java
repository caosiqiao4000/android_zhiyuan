package com.wachoo.pangker.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.pangker.PoiInfoActivity;
import com.wachoo.pangker.db.IMeetRoomDao;
import com.wachoo.pangker.db.impl.MeetRoomDaoImpl;
import com.wachoo.pangker.downupload.GroupAcceptPicUI;
import com.wachoo.pangker.downupload.GroupSendPicUI;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.map.poi.Poi_concise;
import com.wachoo.pangker.util.SmileyParser;
import com.wachoo.pangker.util.Util;

public class ChatRoomListAdapter extends BaseAdapter {

	private Activity context;
	private List<MeetRoom> list;
	private PKIconResizer mImageResizer = null;
	TextView nickname;
	TextView time;
	TextView content;
	SimpleDateFormat sdf;
	private SmileyParser parser; // 表情转化辅助类

	private IMeetRoomDao mMeetRoomDao;
	private OnLongClickListener msgOnLongClickListener;
	private Gson gson = new Gson();
	private ImageLocalFetcher localFetcher;

	public ChatRoomListAdapter(Activity context, ImageLocalFetcher localFetcher) {
		this.context = context;
		this.sdf = new SimpleDateFormat("MM-dd HH:mm:ss");
		this.parser = new SmileyParser(context);// 表情转化辅助工具
		this.mImageResizer = PangkerManager.getUserIconResizer(this.context.getApplicationContext());
		this.mMeetRoomDao = new MeetRoomDaoImpl(context);
		this.localFetcher = localFetcher;
	}

	public OnLongClickListener getMsgOnLongClickListener() {
		return msgOnLongClickListener;
	}

	public void setMsgOnLongClickListener(OnLongClickListener msgOnLongClickListener) {
		this.msgOnLongClickListener = msgOnLongClickListener;
	}

	public void setData(List<MeetRoom> list) {
		this.list = list;
		Collections.sort(this.list);
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class SpeakHolder {

		public LinearLayout ly_message_pic;
		public LinearLayout ly_content;
		public ImageView iv_usericon;
		public TextView tv_content;
		public TextView tv_time;
		public TextView tv_username;

		public TextView tv_fileize;
		public ImageView iv_imagecover;
		public ProgressBar pb_progressBar;
		public Button btn_accept;
		public Button btn_reject;
		public Button btn_cancel;
		public ImageView iv_status;
		public ImageView iv_location;
	}

	// >>>>>>>>最后一次对话时间(如果时间超过2分钟，显示时间，如果不超过，不需要显示)
	private long lastSayTime = 0l;
	SpeakHolder speakHolder = null;

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		final MeetRoom msg = list.get(position);
		// >>>>>>>计算上次说话时间
		if (position == 0) {
			lastSayTime = 0l;
		} else {
			lastSayTime = list.get(position - 1).getTalkTime();
		}
		if (convertView == null) {
			speakHolder = new SpeakHolder();

			convertView = LayoutInflater.from(context).inflate(R.layout.chatroom_talk_view_from, null);

			speakHolder.ly_message_pic = (LinearLayout) convertView.findViewById(R.id.msg_layout_pic);
			speakHolder.ly_content = (LinearLayout) convertView.findViewById(R.id.ly_contentfrom);
			speakHolder.iv_usericon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			speakHolder.tv_content = (TextView) convertView.findViewById(R.id.contentfrom);
			speakHolder.tv_username = (TextView) convertView.findViewById(R.id.tv_username);
			speakHolder.iv_imagecover = (ImageView) convertView.findViewById(R.id.imageCover);
			speakHolder.tv_time = (TextView) convertView.findViewById(R.id.timefrom);
			speakHolder.iv_location = (ImageView) convertView.findViewById(R.id.iv_location);
			speakHolder.pb_progressBar = (ProgressBar) convertView.findViewById(R.id.dwownload_pb);

			convertView.setTag(speakHolder);

		} else {
			speakHolder = (SpeakHolder) convertView.getTag();
		}
		// >>>>>>>>....文字信息
		if (msg.getMsgType() == MeetRoom.s_type_text) {
			speakHolder.ly_message_pic.setVisibility(View.GONE);
			speakHolder.iv_location.setVisibility(View.GONE);
			speakHolder.ly_content.setVisibility(View.VISIBLE);
			speakHolder.tv_content.setText(parser.addSmileySpans(msg.getContent()));
			speakHolder.ly_content.setTag(msg);
			speakHolder.ly_content.setOnLongClickListener(msgOnLongClickListener);
		}
		// >>>>>地理信息>>>....文字信息
		if (msg.getMsgType() == MeetRoom.s_type_loc) {
			speakHolder.ly_message_pic.setVisibility(View.GONE);
			speakHolder.ly_content.setVisibility(View.VISIBLE);
			speakHolder.iv_location.setVisibility(View.VISIBLE);
			final Location location = gson.fromJson(msg.getContent(), Location.class);
			speakHolder.tv_content.setText(location.getAddress());
			speakHolder.ly_content.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					toMapLocation(location);
				}
			});
		}
		// >>>>>>>>>>>>.图片信息
		else if (msg.getMsgType() == MeetRoom.s_type_pic) {
			speakHolder.ly_message_pic.setVisibility(View.VISIBLE);
			speakHolder.ly_content.setVisibility(View.GONE);
			Log.i("meetroomPic", "showPhoto>>>>messageID = " + msg.getMsgId() + "isRecvOK = " + msg.isReceivedOK());
			msg.setFilepath(msg.getContent());

			if (MeetRoom.MSG_FROM_ME.equals(msg.getDirection())) {
				new GroupSendPicUI(speakHolder, context, localFetcher).initView(msg, mMeetRoomDao);
			} else {
				new GroupAcceptPicUI(speakHolder, context, localFetcher).initView(msg, mMeetRoomDao);
			}
			speakHolder.ly_message_pic.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// >>>>>>>>..判断是否接收完毕，如果没有接收完毕，点击不能查看
					ArrayList<PictureViewInfo> imagePathes = new ArrayList<PictureViewInfo>();
					PictureViewInfo pictureViewInfo = new PictureViewInfo();
					pictureViewInfo.setPicPath(msg.getContent());
					imagePathes.add(pictureViewInfo);
					Intent it = new Intent(context, PictureBrowseActivity.class);
					it.putExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY, imagePathes);
					it.putExtra("index", 0);
					context.startActivity(it);
				}
			});

		}

		// >>>>>>>..其他相同信息
		mImageResizer.loadImage(msg.getUserId(), speakHolder.iv_usericon);
		speakHolder.iv_usericon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				goToUserInfo(position);
			}
		});
		speakHolder.tv_time.setVisibility(View.VISIBLE);
		// >>>>>>>>显示对话时间
		showSpeakTime(msg.getTalkTime(), speakHolder.tv_time);
		speakHolder.tv_username.setText(msg.getNickname());

		return convertView;
	}

	private void showSpeakTime(long speakTime, TextView mTimeTextView) {
		Date speakDate = new Date(speakTime);
		if (speakTime - lastSayTime >= PangkerConstant.SPLIT_TIME) {
			mTimeTextView.setVisibility(View.VISIBLE);
			mTimeTextView.setText(Util.getDateDiff(speakDate));
		} else {
			mTimeTextView.setVisibility(View.GONE);
		}
		lastSayTime = speakTime;
	}

	/**
	 * 查看用户信息
	 * 
	 * @param intent
	 */
	private void goToUserInfo(int location) {
		Intent intent = new Intent(context, UserWebSideActivity.class);
		intent.putExtra("userid", list.get(location).getUserId());
		intent.putExtra("username", list.get(location).getUserName());
		intent.putExtra("usersign", list.get(location).getSign());
		context.startActivity(intent);
	}

	private void toMapLocation(Location location) {
		// TODO Auto-generated method stub
		Poi_concise Poi = new Poi_concise();
		Poi.setAddress(location.getAddress());
		Poi.setLat(String.valueOf(location.getLatitude()));
		Poi.setLon(String.valueOf(location.getLongitude()));

		Intent intent = new Intent();
		intent.setClass(context, PoiInfoActivity.class);
		intent.putExtra("PoiItem", Poi);
		context.startActivity(intent);
	}
}
