package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.net.Uri;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.util.Util;

public class MusicInfoAdapter extends BaseAdapter {
	// 用来获得ContentProvider(共享数据库)
	// 歌曲信息列表
	private List<MusicInfo> musicList;
	private Context context;
	// 音乐信息
	/* 1、歌曲名，2、歌手，3、歌曲时间，4、专辑（专辑图片，专辑名称，专辑ID[用来获取图片])，5、歌曲大小 */

	public MusicInfoAdapter(Context context) {
		super();
		this.context = context;
		musicList = new ArrayList<MusicInfo>();
	}
	
	public void setMusicList(List<MusicInfo> musicList) {
		this.musicList = musicList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return musicList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return musicList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MusicInfo item = (MusicInfo) getItem(position);
		ViewHolderMusic holder;
		if (convertView == null) {
			holder = new ViewHolderMusic();
			convertView = LayoutInflater.from(context).inflate(R.layout.music_list_item, null);
			holder.iv_musicCover = (ImageView) convertView.findViewById(R.id.iv_musicCover);
			holder.tv_musicName = (TextView) convertView .findViewById(R.id.tv_musicName);
			holder.tv_musicPlayer = (TextView) convertView .findViewById(R.id.tv_musicPlayer);
			holder.tv_musicScore = (TextView) convertView.findViewById(R.id.tv_musicScore);
//			holder.iv_musicMore = (ImageView) convertView.findViewById(R.id.iv_musicMore);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolderMusic) convertView.getTag();
		}
		if(item != null){
			String url = Util.getAlbumArt(context, item.getMusicId());
			if(url != null){
				holder.iv_musicCover.setImageURI(Uri.parse(url));
			} else {
				holder.iv_musicCover.setImageResource(R.drawable.icon46_music);
			}
			
			holder.tv_musicName.setText(item.getMusicName());
			holder.tv_musicPlayer.setText(item.getSinger());
			String fileSize = Formatter.formatFileSize(context, item.getMusicSize());
			holder.tv_musicScore.setText("文件大小:" + fileSize);
		}
		return convertView;
	}

	public class ViewHolderMusic{
		ImageView iv_musicCover; //封面 
		TextView tv_musicName;   //歌曲名
		TextView tv_musicPlayer;   //歌手
		TextView tv_musicScore;   //歌曲得分
//		ImageView iv_musicMore; //更多
	}
	
}
