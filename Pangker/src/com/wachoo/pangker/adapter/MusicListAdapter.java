package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.response.MusicInfo;

/**
 * 
 * @ClassName: MusicListAdapter
 * @Description: 从服务器获取音乐数据的适配器(这里用一句话描述这个类的作用)
 * @author longxianwen
 * @date Mar 8, 2012 3:07:44 PM
 */
public class MusicListAdapter extends BaseAdapter {

	private Context context;
	private List<MusicInfo> rowContent;
	private ImageResizer mImageWorker;

	public MusicListAdapter(Context context, List<MusicInfo> rowContent, ImageResizer mImageWorker) {
		super();
		this.context = context;
		this.rowContent = rowContent;
		this.mImageWorker = mImageWorker;
	}

	public List<MusicInfo> getRowContent() {
		return rowContent;
	}

	public void setRowContent(List<MusicInfo> rowContent) {
		this.rowContent = rowContent;
		notifyDataSetChanged();
	}

	/**
	 * position: 该视图在适配器数据中的位置 convertView：旧视图 parent: 此视图最终会被附加到的父级视图
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MusicInfo musicInfo = (MusicInfo) getItem(position);
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.music_list_item, null);
			viewHolder.tv_musicName = (TextView) convertView.findViewById(R.id.tv_musicName);
			viewHolder.tv_musicSize = (TextView) convertView.findViewById(R.id.tv_musicSize);
			viewHolder.iv_musicCover = (ImageView) convertView.findViewById(R.id.iv_musicCover);
			viewHolder.tvCollect = (TextView) convertView.findViewById(R.id.res_tv_collect);
			viewHolder.tvComment = (TextView) convertView.findViewById(R.id.res_tv_comment);
			viewHolder.tvDownloadTimes = (TextView) convertView.findViewById(R.id.res_tv_loadtimes);
			viewHolder.mRatingBar = (RatingBar) convertView.findViewById(R.id.rating_bar_show);
			viewHolder.iv_ifShare = (ImageView) convertView.findViewById(R.id.iv_share);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.setResInfo(musicInfo);
		viewHolder.tv_musicName.setText(musicInfo.getMusicName());
		String musicSize = Formatter.formatFileSize(context, new Double(musicInfo.getFileSize()).longValue());
		viewHolder.tv_musicSize.setText(musicSize);
		// viewHolder.tv_musicPlayer.setText(musicInfo.getMusicSinger());
		Long score = musicInfo.getFavorTimes();
		viewHolder.tvCollect.setText("(" + String.valueOf(score) + ")");
		Long commentTimes = musicInfo.getCommentTimes();
		viewHolder.tvComment.setText("(" + String.valueOf(commentTimes) + ")");
		Long downloadTimes = musicInfo.getDownloadTimes();
		viewHolder.tvDownloadTimes.setText("(" + String.valueOf(downloadTimes) + ")");
		viewHolder.mRatingBar.setRating(((float) musicInfo.getScore()) / 2);

		if (musicInfo.getIsShare()) {
			viewHolder.iv_ifShare.setVisibility(View.VISIBLE);
		} else {
			viewHolder.iv_ifShare.setVisibility(View.GONE);
		}
		String IconUrl = Configuration.getCoverDownload() + musicInfo.getSid();

		mImageWorker.setLoadingImage(R.drawable.icon46_music);
		mImageWorker.loadImage(IconUrl, viewHolder.iv_musicCover, String.valueOf(musicInfo.getSid()));

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return rowContent.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return rowContent.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder extends ViewBaseHolder {
		ImageView iv_musicCover;
		TextView tv_musicName; // 歌曲名
		ImageView iv_ifShare; //
		TextView tv_musicSize;
		TextView tvCollect;
		TextView tvComment;
		TextView tvDownloadTimes;
		RatingBar mRatingBar;
	}
}
