package com.wachoo.pangker.adapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.group.ChatRoomInfoActivity;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.util.Util;

/**
 * @author wubo
 * @createtime Apr 24, 2012
 */
public class MeetGroupAdapter extends BaseAdapter {

	private List<UserGroup> userGroups;
	private Context context;
	private ImageResizer mImageWorker;
	
	public MeetGroupAdapter(List<UserGroup> userGroups, Context context) {
		this.userGroups = userGroups;
		this.context = context;
		mImageWorker = PangkerManager.getGroupIconResizer(context.getApplicationContext());
	}

	public List<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<UserGroup> groups) {
		this.userGroups = groups;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userGroups.size();
	}

	@Override
	public UserGroup getItem(int position) {
		// TODO Auto-generated method stub
		return userGroups.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final UserGroup group = userGroups.get(position);
		ViewHolder viewHolder = null;

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.group_lbs_list_item, null);
			viewHolder = new ViewHolder();
			// 群组分类标签
			viewHolder.groupLabel = (TextView) convertView.findViewById(R.id.tv_first_char_hint);
			// 群组图标
			viewHolder.groupIcon = (ImageView) convertView.findViewById(R.id.iv_icon);
			// 是否转播的图标
			viewHolder.iv_share = (ImageView) convertView.findViewById(R.id.iv_share);
			// 是否转播的图标
			viewHolder.iv_channel = (ImageView) convertView.findViewById(R.id.iv_channel);
			// 群组名称
			viewHolder.groupName = (TextView) convertView.findViewById(R.id.tv_name);
			// 群组人员数
			viewHolder.visitUsers = (TextView) convertView.findViewById(R.id.tv_right_top);
			// 群组标签
			viewHolder.groupLable = (TextView) convertView.findViewById(R.id.tv_left_bottom);
			viewHolder.totalMembers = (TextView) convertView.findViewById(R.id.tv_right_bottom);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if(Util.isEmpty(group.getLabel())){
			viewHolder.groupLabel.setVisibility(View.GONE);
		} else {
			viewHolder.groupLabel.setVisibility(View.VISIBLE);
			viewHolder.groupLabel.setText(group.getLabel());
		}
		viewHolder.groupIcon.setTag(position);
		viewHolder.groupName.setText(group.getGroupName());
		viewHolder.groupLable.setText(group.getGroupLabel());
		viewHolder.totalMembers.setText(String.valueOf(group.getOnlineNum()) + "人在线");
		viewHolder.visitUsers.setText("累计" + group.getVisitUsers() + "人次");

		if (group.isSingleVoiceChannel()) {
			viewHolder.iv_channel.setImageResource(R.drawable.radio_unselect);
		} else {
			viewHolder.iv_channel.setImageResource(R.drawable.radio_select);
		}
		if (group.getGroupFlag() == 3) {
			viewHolder.iv_share.setImageResource(R.drawable.icon16_forward);
			viewHolder.iv_share.setVisibility(View.VISIBLE);
		} else if (group.getGroupFlag() == 1) {
			viewHolder.iv_share.setVisibility(View.VISIBLE);
			viewHolder.iv_share.setImageResource(R.drawable.icon16_keep);
		} else if (group.getGroupFlag() == 0) {
			if (group.getIfShare() != null && group.getIfShare() == 1) { // 0不分享
																			// 1：分享
				viewHolder.iv_share.setVisibility(View.VISIBLE);
				viewHolder.iv_share.setImageResource(R.drawable.icon32_share);
			} else {
				viewHolder.iv_share.setVisibility(View.VISIBLE);
				viewHolder.iv_share.setImageResource(R.drawable.icon32_unshare);
			}
		} else {
			viewHolder.iv_share.setVisibility(View.GONE);
		}

		final String groupUrl = Configuration.getGroupCover() + group.getGroupId();
		mImageWorker.loadImage(groupUrl, viewHolder.groupIcon, group.getGroupId());
		viewHolder.groupIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				lookChatRoomInfo(group);
			}
		});

		return convertView;
	}
	
	private class ViewHolder {
		public ImageView groupIcon; //
		public ImageView iv_share; //
		public ImageView iv_channel; //
		public TextView groupName;
		public TextView visitUsers; //
		public TextView groupLable; //
		public TextView totalMembers;
		//群组分类标签
		public TextView groupLabel;
	}

	private void lookChatRoomInfo(UserGroup userGroup) {
		Intent intent = new Intent();
		List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
		GroupLbsInfo lbsInfo = new GroupLbsInfo();
		lbsInfo.setgName(userGroup.getGroupName());
		lbsInfo.setSid(Long.parseLong(userGroup.getGroupId()));
		lbsInfo.setUid(Long.parseLong(userGroup.getUid()));
		lbsInfo.setShareUid(userGroup.getUid());
		groupList.add(lbsInfo);
		intent.putExtra("GroupInfo", (Serializable) groupList);
		intent.putExtra("group_index", 0);
		intent.setClass(this.context, ChatRoomInfoActivity.class);
		this.context.startActivity(intent);
	}
}
