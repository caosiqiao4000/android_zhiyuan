package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;

/**
 * FedestrianAdapter 行人Adapter
 * 
 * @author zhengjy
 * 
 *         edit by wangxin
 * 
 */
public class NearUserGVAdapter extends BaseAdapter {
	private Context mContext;
	private List<UserItem> mUserList;
	private static String TAG = "NearUserListAdapter";// log tag
	private String mUserId;
	private PKIconResizer mImageResizer = null;

	/**
	 * 构造函数
	 * 
	 * @param context
	 * @param list
	 */
	public NearUserGVAdapter(Context context, List<UserItem> list) {
		this.mContext = context;
		this.mUserList = list;
		this.mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		mUserId = ((PangkerApplication) context.getApplicationContext()).getMyUserID();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		Log.i(TAG, "getCount=" + (mUserList.size() + 2) / 3);
		// TODO Auto-generated method stub
		return (mUserList.size() + 2) / 3;
	}

	@Override
	public UserItem getItem(int position) {
		// TODO Auto-generated method stub
		return mUserList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolder holder;

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final int location = position * 3;
		UserItem item_one = null;
		UserItem item_two = null;
		UserItem item_three = null;

		// 获取项目数据源
		if (location <= mUserList.size() - 1) {
			item_one = getItem(location);
		}
		if (location + 1 <= mUserList.size() - 1) {
			item_two = getItem(location + 1);
		}
		if (location + 2 <= mUserList.size() - 1) {
			item_three = getItem(location + 2);
		}

		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.nearuser_gv_item, null);
			holder.ry_user_item1 = (RelativeLayout) convertView.findViewById(R.id.ly_user_item1);
			holder.iv_usericon1 = (ImageView) convertView.findViewById(R.id.iv_usericon1);
			holder.iv_sex1 = (ImageView) convertView.findViewById(R.id.iv_sex1);
			holder.tv_username1 = (TextView) convertView.findViewById(R.id.tv_username1);
			holder.iv_wifi1  = (ImageView) convertView.findViewById(R.id.iv_wifi1);

			holder.ry_user_item2 = (RelativeLayout) convertView.findViewById(R.id.ly_user_item2);
			holder.iv_usericon2 = (ImageView) convertView.findViewById(R.id.iv_usericon2);
			holder.iv_sex2 = (ImageView) convertView.findViewById(R.id.iv_sex2);
			holder.tv_username2 = (TextView) convertView.findViewById(R.id.tv_username2);
			holder.iv_wifi2  = (ImageView) convertView.findViewById(R.id.iv_wifi2);

			holder.ry_user_item3 = (RelativeLayout) convertView.findViewById(R.id.ly_user_item3);
			holder.iv_usericon3 = (ImageView) convertView.findViewById(R.id.iv_usericon3);
			holder.iv_sex3 = (ImageView) convertView.findViewById(R.id.iv_sex3);
			holder.tv_username3 = (TextView) convertView.findViewById(R.id.tv_username3);
			holder.iv_wifi3  = (ImageView) convertView.findViewById(R.id.iv_wifi3);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		initViewData(item_one, holder.ry_user_item1, holder.iv_usericon1, holder.iv_sex1,holder.iv_wifi1,
				holder.tv_username1, location);

		initViewData(item_two, holder.ry_user_item2, holder.iv_usericon2, holder.iv_sex2,holder.iv_wifi2,
				holder.tv_username2, location + 1);

		initViewData(item_three, holder.ry_user_item3, holder.iv_usericon3, holder.iv_sex3,holder.iv_wifi3,
				holder.tv_username3, location + 2);

		return convertView;
	}

	private void initViewData(final UserItem item, View relativeLayout, ImageView iv_userIcon, ImageView iv_sex,ImageView iv_wifi,
			TextView tv_userName, final int location) {
		if (item != null) {
			relativeLayout.setVisibility(View.VISIBLE);
			relativeLayout.setBackgroundResource(R.drawable.itemlist_bg);
			iv_userIcon.setVisibility(View.VISIBLE);
			iv_sex.setVisibility(View.VISIBLE);
			tv_userName.setVisibility(View.VISIBLE);

			mImageResizer.loadImage(item.getUserId(), iv_userIcon);
			iv_userIcon.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(item.getUserId().equals(mUserId)){
						Toast.makeText(mContext, R.string.isyourself, Toast.LENGTH_SHORT).show();
					}else 
						goToUserInfo(location);
					
				}
			});
			if (PangkerConstant.NEAR_USER_GIRLS.equals(item.getSex())) {
				iv_sex.setImageResource(R.drawable.icon24_g);
			} else if (PangkerConstant.NEAR_USER_BOYS.equals(item.getSex())) {
				iv_sex.setImageResource(R.drawable.icon24_m);
			}
			
			if(item.getIsWifiLocate() != null && item.getIsWifiLocate() == 1){
				iv_wifi.setVisibility(View.VISIBLE);
			}
			else iv_wifi.setVisibility(View.GONE);
			
			tv_userName.setText(item.getUserName());
		} else {
			relativeLayout.setBackgroundResource(R.drawable.main_bg);
			iv_userIcon.setVisibility(View.GONE);
			iv_sex.setVisibility(View.GONE);
			tv_userName.setVisibility(View.GONE);
		}
	}

	/**
	 * 查看用户信息
	 * 
	 * @param intent
	 */
	private void goToUserInfo(int location) {

		Intent intent = new Intent(mContext, UserWebSideActivity.class);
		intent.putExtra("userid", mUserList.get(location).getUserId());
		intent.putExtra("username", mUserList.get(location).getUserName());
		intent.putExtra("usersign", mUserList.get(location).getSign());
		mContext.startActivity(intent);
	}

	class ViewHolder {

		public RelativeLayout ry_user_item1;
		public ImageView iv_usericon1;
		public ImageView iv_sex1;// 性别
		public TextView tv_username1;
		public ImageView iv_wifi1;// wifi邻居

		public RelativeLayout ry_user_item2;
		public ImageView iv_usericon2;
		public ImageView iv_sex2;// 性别
		public TextView tv_username2;
		public ImageView iv_wifi2;// wifi邻居

		public RelativeLayout ry_user_item3;
		public ImageView iv_usericon3;
		public ImageView iv_sex3;// 性别
		public TextView tv_username3;
		public ImageView iv_wifi3;// wifi邻居
	}

	public List<UserItem> getmUserList() {
		return mUserList;
	}

	public void setmUserList(List<UserItem> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
	}
	
	public void addWifiUserList(List<UserItem> wifiUserList) {
		for (UserItem userWifiItem : wifiUserList) {
			for (UserItem userItem : this.mUserList) {
				if(userWifiItem.getUserId().equals(userItem.getUserId())){
					this.mUserList.remove(userItem);
					break;
				}
			}
		}
		this.mUserList.addAll(0, wifiUserList);
		this.notifyDataSetChanged();
	}

}
