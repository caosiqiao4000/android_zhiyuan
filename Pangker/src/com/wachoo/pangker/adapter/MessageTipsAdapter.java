package com.wachoo.pangker.adapter;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.MessageTip;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.util.Util;

public class MessageTipsAdapter extends BaseAdapter {

	private Activity context;
	private List<MessageTip> messageTips;
	private PKIconResizer mImageResizer;

	public MessageTipsAdapter(Activity context, List<MessageTip> messageTips, PKIconResizer mImageResizer) {
		super();
		this.context = context;
		this.messageTips = messageTips;
		this.mImageResizer = mImageResizer;
	}

	public void addMessageTip(MessageTip tip) {
		// TODO Auto-generated method stub
		messageTips.add(tip);
		notifyDataSetChanged();
	}

	public void delMessageTip(MessageTip tip) {
		// TODO Auto-generated method stub
		messageTips.remove(tip);
		notifyDataSetChanged();
	}

	public void clearMessageTip() {
		// TODO Auto-generated method stub
		messageTips.clear();
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return messageTips.size();
	}

	@Override
	public MessageTip getItem(int position) {
		// TODO Auto-generated method stub
		return messageTips.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewholder;
		final MessageTip item = getItem(position);
		if (view == null) {
			view = LayoutInflater.from(context).inflate(R.layout.msgtip_item, null);
			viewholder = new ViewHolder();
			viewholder.userIcon = (ImageView) view.findViewById(R.id.iv_sysicon);
			viewholder.txtUserName = (TextView) view.findViewById(R.id.tv_name);
			viewholder.txtContent = (TextView) view.findViewById(R.id.tv_content);
			viewholder.msgIcon = (ImageView) view.findViewById(R.id.iv_msgicon);
			viewholder.txtResName = (TextView) view.findViewById(R.id.tv_resname);
			viewholder.txtTime = (TextView) view.findViewById(R.id.tv_time);

			viewholder.txtCommentType = (TextView) view.findViewById(R.id.tv_comment_type);
			viewholder.txtCommentResName = (TextView) view.findViewById(R.id.tv_comment_reaname);

			view.setTag(viewholder);
		} else {
			viewholder = (ViewHolder) view.getTag();
		}
		// 资源信息
		mImageResizer.loadImage(item.getFromId(), viewholder.userIcon);
		viewholder.userIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				lookUserInfo(item);
			}
		});
		viewholder.txtUserName.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				lookUserInfo(item);
			}
		});

		viewholder.txtUserName.setText(item.getFromName());
		viewholder.msgIcon.setVisibility(View.GONE);
		viewholder.txtContent.setVisibility(View.VISIBLE);
		// >>>>>>>查看评论类型
		switch (item.getTipType()) {
		case MessageTip.TIP_COMMENT_RES:
			viewholder.txtCommentType.setText("评论了");
			break;
		case MessageTip.TIP_COMMENT_BROADCAST_RES:
			viewholder.txtCommentType.setText("评论了您转播的");
			break;
		case MessageTip.TIP_COMMENT_REPLY:
			viewholder.txtCommentType.setText("回复了您的评论");
			break;
		default:
			break;
		}

		viewholder.txtContent.setText(item.getContent());
		// 时间
		Date oldDate = Util.formatDate(item.getCreateTime());
		viewholder.txtTime.setText(Util.getDateDiff(oldDate));

		viewholder.txtCommentResName.setText(item.getResName());
		return view;
	}

	public class ViewHolder {
		ImageView userIcon;
		ImageView msgIcon;
		TextView txtUserName;
		TextView txtContent;
		TextView txtResName;
		TextView txtTime;

		TextView txtCommentType;
		TextView txtCommentResName;
	}

	private void lookUserInfo(MessageTip item) {
		Intent intent = new Intent(context, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, item.getFromId());
		context.startActivity(intent);
	}
}
