package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.util.MapUtil;

/**
 * 
 * @author zhengjy edit by wangxin ///
 * 
 */
public class ContactsAdapter extends BaseAdapter {
	private Context mContext;
	private List<UserItem> mUserList;
	private PKIconResizer mImageResizer;

	/**
	 * 构造函数
	 * @param context
	 * @param list
	 */
	public ContactsAdapter(Context context, List<UserItem> list, PKIconResizer mImageResizer) {
		this.mContext = context;
		this.mUserList = list;
		this.mImageResizer = mImageResizer;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mUserList == null ? 0 : mUserList.size();
	}

	@Override
	public UserItem getItem(int position) {
		// TODO Auto-generated method stub
		return mUserList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	private ViewHolder holder;

	@Override
	public View getView(int position, View rowView, ViewGroup parent) {
		final UserItem item = getItem(position);
		// rowView = this.viewMap.get(item.hashCode());
		if (rowView == null) {
			holder = new ViewHolder();
			rowView = LayoutInflater.from(mContext).inflate(R.layout.contacts_address_item, null);
			holder.tv_username = (TextView) rowView.findViewById(R.id.tv_username);
			holder.tv_usersign = (TextView) rowView.findViewById(R.id.tv_usersign);
			holder.iv_usericon = (ImageView) rowView.findViewById(R.id.iv_usericon);
			holder.tv_distance = (TextView) rowView.findViewById(R.id.tv_distance);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}
		holder.tv_username.setText(item.getUserName());
		holder.tv_usersign.setText(item.getSign());
		if(item.getLatitude() != null && item.getLongitude() != null){
			holder.tv_distance.setVisibility(View.VISIBLE);
			double distance = getDistance(item.getLatitude(), item.getLongitude());
			holder.tv_distance.setText(MapUtil.getUserDistance(distance));// 距离
		}
		else holder.tv_distance.setVisibility(View.GONE);

		mImageResizer.loadImage(item.getUserId(), holder.iv_usericon);
		return rowView;
	}

	public final class ViewHolder {
		public TextView tv_username;
		public TextView tv_usersign;
		public ImageView iv_usericon;
		public TextView tv_distance;
	}

	public List<UserItem> getmUserList() {
		return mUserList;
	}

	public void setmUserList(List<UserItem> mUserList) {
		this.mUserList = mUserList;
		// this.viewMap = new HashMap<Integer, View>();
		this.notifyDataSetChanged();
	}

	/**
	 * 更新时需要还原viewMap中保留的数据
	 */
	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		// this.viewMap = new HashMap<Integer, View>();
		super.notifyDataSetChanged();
	}
	
	private double getDistance(double lat, double lon){
		double myLat = 0;
		double myLon = 0;
		if(((PangkerApplication) mContext.getApplicationContext()).getLocateType() == PangkerConstant.LOCATE_CLIENT){
			myLat = ((PangkerApplication) mContext.getApplicationContext()).getCurrentLocation().getLatitude();
			myLon = ((PangkerApplication) mContext.getApplicationContext()).getCurrentLocation().getLongitude();
		} else {
			myLat = ((PangkerApplication) mContext.getApplicationContext()).getDirftLocation().getLatitude();
			myLon = ((PangkerApplication) mContext.getApplicationContext()).getDirftLocation().getLongitude();
		}
		return MapUtil.DistanceOfTwoPoints(myLat, myLon , lat, lon);
	}

}
