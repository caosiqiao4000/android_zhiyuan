package com.wachoo.pangker.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.GroupChatMsg;

/**
 * 
 * @author wangxin
 * 
 */
public class GroupChatAdapter extends BaseAdapter {
	Context context;
	public List<GroupChatMsg> msgs = null;
	public GroupChatAdapter(Context context, List<GroupChatMsg> msgs) {
		this.context = context;
		this.msgs = msgs;
	}
	class MessageSingleViewHolder {
		TextView username;
		TextView mycontent;
		TextView time;
	}

	public void addMsg(GroupChatMsg msg) {
		msgs.add(msg);
		notifyDataSetChanged();
	}

	public int getCount() {
		return msgs.size();
	}

	public Object getItem(int position) {
		return msgs.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		MessageSingleViewHolder holder = null;
		if(convertView == null){
			holder = new MessageSingleViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.group_chat_item_view, null);
			holder.username = (TextView) convertView.findViewById(R.id.group_chat_username);
			holder.mycontent = (TextView) convertView.findViewById(R.id.group_chat_content);
			holder.time = (TextView) convertView.findViewById(R.id.group_chat_time);
			convertView.setTag(holder);
		}
		else holder =(MessageSingleViewHolder)convertView.getTag();
		
		holder.username.setText(msgs.get(position).getUserName());
		holder.mycontent.setText(msgs.get(position).getTalkmessage());

		// 计算时间
		Long nowDate = new Date().getTime();
		Long oldDate = Long.parseLong(msgs.get(position).getTalktime());
		Long differ = (nowDate - oldDate) / 1000 / 60 / 60 / 24;
		SimpleDateFormat sdf;
		String time;
		if (differ < 1) {
			sdf = new SimpleDateFormat("HH:mm:ss");
			time = sdf.format(new Date(oldDate));
			holder.time.setText(context.getResources().getString(R.string.today) + time);
		} else if (differ >= 1 && differ < 2) {
			sdf = new SimpleDateFormat("HH:mm:ss");
			time = sdf.format(new Date(oldDate));
			holder.time.setText(context.getResources().getString(R.string.yesterday) + time);
		} else if (differ >= 2 && differ < 3) {
			sdf = new SimpleDateFormat("HH:mm:ss");
			time = sdf.format(new Date(oldDate));
			holder.time.setText(context.getResources().getString(R.string.ago) + time);
		} else {
			sdf = new SimpleDateFormat("MM-dd HH:mm:ss");
			time = sdf.format(new Date(oldDate));
			holder.time.setText(time);
		}

		return convertView;
	}
}
