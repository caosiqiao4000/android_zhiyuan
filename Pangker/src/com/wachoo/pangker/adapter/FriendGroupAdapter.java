package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.ContactGroup;

public class FriendGroupAdapter extends BaseAdapter {

	private Context context;
	private List<ContactGroup> groups;
	private int index = 0;// 表明用户选择的是哪一组
	private boolean isFlag = false;// 是否是选择
	private List<Boolean> mBooleans;

	public FriendGroupAdapter(Context context, List<ContactGroup> groups) {
		this.context = context;
		this.groups = groups;
		mBooleans = new ArrayList<Boolean>();
		init();
	}

	public void init() {
		mBooleans.clear();
		int temp = groups.size();
		for (int i = 0; i < temp; i++) {
			mBooleans.add(i, false);
		}
	}

	public void setFlag(boolean isFlag) {
		this.isFlag = isFlag;
	}

	public void selectByIndex(int index, boolean flag) {
		if (flag) {
			for (int i = 0; i < mBooleans.size(); i++) {
				mBooleans.set(i, false);
			}
		}
		mBooleans.set(index, flag);
		notifyDataSetChanged();
	}

	public void selectByGroupID(String mSelectedGroupID) {
		for (int i = 0; i < groups.size(); i++) {
			if (groups.get(i).getGid().equals(mSelectedGroupID)) {
				mBooleans.set(i, true);
				break;
			}
		}

		notifyDataSetChanged();
	}

	public List<ContactGroup> getSelectGroups() {
		List<ContactGroup> selectedMembers = new ArrayList<ContactGroup>();
		for (int i = 0; i < groups.size(); i++) {
			if (mBooleans.get(i)) {
				selectedMembers.add(groups.get(i));
			}
		}
		return selectedMembers;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return groups.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return groups.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;// Long.parseLong(groupList.get(position).groupId);
	}

	public void setIndex(int index) {
		this.index = index;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.friend_group_item, null);
			holder.tvGroupName = (TextView) convertView
					.findViewById(R.id.tv_group_name);
			holder.tvUserCount = (TextView) convertView
					.findViewById(R.id.tv_user_count);
			holder.mCheckBox = (CheckBox) convertView
					.findViewById(R.id.checkBox);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if (isFlag) {
			holder.mCheckBox.setVisibility(View.VISIBLE);
			holder.mCheckBox.setChecked(mBooleans.get(position));
		} else {
			holder.mCheckBox.setVisibility(View.GONE);
		}
		String groupName = groups.get(position).getName();
		holder.tvGroupName.setText(groupName != null ? groupName : "");

		int userCount = groups.get(position).getCount();
		holder.tvUserCount.setText(userCount + "名联系人");

		return convertView;
	}

	public class ViewHolder {
		public TextView tvGroupName;
		public TextView tvUserCount;
		public CheckBox mCheckBox;
	}

	public List<ContactGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<ContactGroup> groups) {
		this.groups = groups;
		this.notifyDataSetChanged();
	}
}
