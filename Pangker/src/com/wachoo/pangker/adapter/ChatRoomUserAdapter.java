package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.group.P2PUserInfo;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.util.MapUtil;
import com.wachoo.pangker.util.Util;

public class ChatRoomUserAdapter extends BaseAdapter {
	
	private Context mContext;
	private List<P2PUserInfo> mAllUserList = new ArrayList<P2PUserInfo>();
	private List<P2PUserInfo> mShowUserList = new ArrayList<P2PUserInfo>();
	private PKIconResizer mImageResizer;
	private int mSexFilter = 2;

	public ChatRoomUserAdapter(Context mContext, PKIconResizer mImageResizer) {
		this.mContext = mContext;
		this.mImageResizer = mImageResizer;
	}

	public List<P2PUserInfo> getmUserList() {
		return mAllUserList;
	}

	public void setmUserList(List<P2PUserInfo> mUserList) {
		this.mAllUserList = mUserList;
		if (this.mAllUserList == null || this.mAllUserList.size() == 0)
			return;
		switch (mSexFilter) {
		case 0:
			showBoys();
			break;
		case 1:
			showGrils();
			break;
		default:
			showAllUsers();
			break;
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mShowUserList.size();
	}

	@Override
	public Object getItem(int location) {
		// TODO Auto-generated method stub
		return mShowUserList.get(location);
	}

	@Override
	public long getItemId(int location) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void showBoys() {
		mSexFilter = UserItem.USER_SEX_BOY;
		this.mShowUserList.clear();
		for (P2PUserInfo info : this.mAllUserList) {
			if (info.getSex() == UserItem.USER_SEX_BOY)
				this.mShowUserList.add(info);
		}
		this.notifyDataSetChanged();
	}

	public void showGrils() {
		mSexFilter = UserItem.USER_SEX_GRIL;
		this.mShowUserList.clear();
		for (P2PUserInfo info : this.mAllUserList) {
			if (info.getSex() == UserItem.USER_SEX_GRIL)
				this.mShowUserList.add(info);
		}
		this.notifyDataSetChanged();
	}

	public void showAllUsers() {
		mSexFilter = 2;
		this.mShowUserList.clear();
		this.mShowUserList.addAll(mAllUserList);
		this.notifyDataSetChanged();
	}

	@Override
	public View getView(final int position, View contactView, ViewGroup viewGroup) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		final P2PUserInfo item = (P2PUserInfo) getItem(position);
		if (contactView == null) {
			contactView = LayoutInflater.from(mContext).inflate(R.layout.user_list_item, null);
			holder = new ViewHolder();

			holder.ly_list_user_item = (LinearLayout) contactView.findViewById(R.id.ly_list_user_item);
			holder.userName = (TextView) contactView.findViewById(R.id.tv_name); // 昵称
			holder.sign = (TextView) contactView.findViewById(R.id.tv_sign);//
			holder.age = (TextView) contactView.findViewById(R.id.tv_age);// 年龄
			holder.sex = (ImageView) contactView.findViewById(R.id.iv_sex); // 性别

			holder.status = (ImageView) contactView.findViewById(R.id.iv_status);
			holder.distance = (TextView) contactView.findViewById(R.id.tv_distance);// 距离
			holder.usericon = (ImageView) contactView.findViewById(R.id.iv_usericon); // 用户头像
			holder.time = (TextView) contactView.findViewById(R.id.tv_time); // 最近登录时间
			contactView.setTag(holder);
		} else {
			holder = (ViewHolder) contactView.getTag();
		}
		holder.userName.setText(item.getUserName());// 用户名
		if (PangkerConstant.NEAR_USER_BOYS.equals(String.valueOf(item.getSex()))) {
			holder.sex.setImageResource(R.drawable.icon24_m);
		} else if (PangkerConstant.NEAR_USER_GIRLS.equals(String.valueOf(item.getSex()))) {
			holder.sex.setImageResource(R.drawable.icon24_g);
		}
		holder.sign.setText(item.getSign());// 签名
		if (item.getLocationInfo() != null) {
			String[] location = item.getLocationInfo().split(",");
			if (location != null && location.length == 2) {

				Double lat = Util.String2Double(location[0]);
				Double lon = Util.String2Double(location[1]);
				if (lat != null && lon != null) {
					double distance = getDistance(lat, lon);
					holder.distance.setText(MapUtil.getUserDistance(distance));// 距离
				} else
					holder.distance.setText("距离未知");// 距离
			} else {
				holder.distance.setText("距离未知");// 距离
			}

		}

		holder.time.setVisibility(View.GONE);
		holder.age.setVisibility(View.GONE);// 年龄

		mImageResizer.loadImage(String.valueOf(item.getUID()), holder.usericon);

		holder.ly_list_user_item.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				goToUserInfo(item);

			}
		});
		return contactView;
	}

	/**
	 * 查看用户信息
	 * 
	 * @param intent
	 */
	private void goToUserInfo(P2PUserInfo item) {
		String mUserId = ((PangkerApplication) mContext.getApplicationContext()).getMyUserID();
		if (String.valueOf(item.getUID()).equals(mUserId)) {
			Toast.makeText(mContext, R.string.isyourself, Toast.LENGTH_SHORT).show();
		} else {
			Intent intent = new Intent(mContext, UserWebSideActivity.class);
			intent.putExtra("userid", String.valueOf(item.getUID()));
			intent.putExtra("username", item.getUserName());
			intent.putExtra("usersign", item.getSign());
			mContext.startActivity(intent);
		}
	}

	private double getDistance(double lat, double lon) {
		double myLat = 0;
		double myLon = 0;
		if (((PangkerApplication) mContext.getApplicationContext()).getLocateType() == PangkerConstant.LOCATE_CLIENT) {
			myLat = ((PangkerApplication) mContext.getApplicationContext()).getCurrentLocation()
					.getLatitude();
			myLon = ((PangkerApplication) mContext.getApplicationContext()).getCurrentLocation()
					.getLongitude();
		} else {
			myLat = ((PangkerApplication) mContext.getApplicationContext()).getDirftLocation().getLatitude();
			myLon = ((PangkerApplication) mContext.getApplicationContext()).getDirftLocation().getLongitude();
		}
		return MapUtil.DistanceOfTwoPoints(myLat, myLon, lat, lon);
	}

	class ViewHolder {
		public LinearLayout ly_list_user_item;
		public TextView userName; // 昵称
		public TextView sign;// 交友宣言
		public TextView age;// 年龄
		public ImageView sex; // 性别
		public ImageView status;
		public TextView time;
		public TextView distance;
		public ImageView usericon;
	}

}
