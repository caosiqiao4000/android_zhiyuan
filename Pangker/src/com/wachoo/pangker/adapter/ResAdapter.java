package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.util.Util;

public class ResAdapter extends BaseAdapter {

	private Context context;
	private List<DirInfo> resinfos;
	// private AsyncImageLoader imageLoader = AsyncImageLoader.getAsyInstance();
	private Set<String> pathList = new HashSet<String>();
	private boolean ifLock = false;
	private ImageResizer mImageWorker;

	public ResAdapter(Context context, List<DirInfo> resinfos, ImageResizer mImageWorker) {
		super();
		this.context = context;
		this.resinfos = resinfos;
		this.mImageWorker = mImageWorker;
	}

	public int getResCount() {
		int index = 0;
		Iterator<DirInfo> iter = resinfos.iterator();
		while (iter.hasNext()) {
			DirInfo item = iter.next();
			if (item.getIsfile() == DirInfo.ISFILE) {
				index++;
			}
		}
		return index;
	}

	public void clear() {
		resinfos.clear();
		notifyDataSetChanged();
	}

	public List<DirInfo> getResInfos() {
		return resinfos;
	}

	public void setIfLock(boolean ifLock) {
		this.ifLock = ifLock;
	}

	public int getSize() {
		if (ifhaveNav()) {
			return getCount() - 1;
		}
		return getCount();
	}

	public List<DirInfo> getDirInfos() {
		List<DirInfo> dirInfos = new ArrayList<DirInfo>();
		for (DirInfo dirInfo : resinfos) {
			if (dirInfo.getIsfile() == 0) {
				dirInfos.add(dirInfo);
			}
		}
		return dirInfos;
	}

	public void setResInfos(List<DirInfo> resinfos) {
		this.resinfos = resinfos;
		notifyDataSetChanged();
	}

	private boolean ifhaveNav() {
		for (DirInfo dirInfo : resinfos) {
			if (dirInfo.getIsfile() == -1) {
				return true;
			}
		}
		return false;
	}

	private DirInfo getNavDirinfo(DirInfo dirinfo) {
		DirInfo dir = new DirInfo();
		dir.setDirId(dirinfo.getDirId());
		dir.setParentDirId(dirinfo.getParentDirId());
		dir.setDirName("返回上一层目录...");
		dir.setIsfile(-1);
		return dir;
	}

	public void setUserResInfos(List<DirInfo> resinfos, DirInfo dir) {
		this.resinfos = resinfos;
		if (dir != null && !"0".equals(dir.getDirId()) && !ifhaveNav()) {
			this.resinfos.add(0, getNavDirinfo(dir));
		}
		notifyDataSetChanged();
	}

	public void setResInfos(List<DirInfo> resinfos, DirInfo dir) {
		this.resinfos = resinfos;
		if (dir != null && !"0".equals(dir.getParentDirId()) && !ifhaveNav()) {
			this.resinfos.add(0, getNavDirinfo(dir));
		}
		notifyDataSetChanged();
	}

	public void addResinfos(List<DirInfo> resinfos) {
		this.resinfos.addAll(resinfos);
		notifyDataSetChanged();
	}

	private boolean contains(DirInfo resinfo) {
		for (DirInfo dir : resinfos) {
			if (dir.getDirId().equals(resinfo.getDirId())) {
				return true;
			}
		}
		return false;
	}

	public void addDirInfo(DirInfo resinfo) {
		if (!contains(resinfo)) {
			resinfos.add(0, resinfo);
			notifyDataSetChanged();
		}
	}

	public void removeDirInfo(DirInfo resinfo) {
		if (resinfos.contains(resinfo)) {
			resinfos.remove(resinfo);
		}
		notifyDataSetChanged();
	}

	public void delDirInfo(DirInfo resinfo) {
		if (resinfos.contains(resinfo)) {
			resinfos.remove(resinfo);
			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return resinfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return resinfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolder viewHolder;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		DirInfo item = resinfos.get(position);
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.contr_app_item, null);
			viewHolder.mAppIcon = (ImageView) convertView.findViewById(R.id.app_img);
			viewHolder.mAppName = (TextView) convertView.findViewById(R.id.app_label);
			viewHolder.mAppContent = (TextView) convertView.findViewById(R.id.app_content);
			viewHolder.mAppShare = (ImageView) convertView.findViewById(R.id.app_share);
			viewHolder.mAppNext = (ImageView) convertView.findViewById(R.id.app_next);
			viewHolder.mAppLock = (ImageView) convertView.findViewById(R.id.app_lock);
			viewHolder.mAppSize = (TextView) convertView.findViewById(R.id.tv_res_size);
			viewHolder.mAppUpload = (ImageView) convertView.findViewById(R.id.app_upload);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		if (item != null) {
			// 如果是文件
			if (item.getIsfile() == 1) {
				viewHolder.mAppContent.setVisibility(View.VISIBLE);
				viewHolder.mAppSize.setVisibility(View.VISIBLE);
				viewHolder.mAppUpload.setVisibility(View.VISIBLE);
				viewHolder.mAppShare.setVisibility(View.VISIBLE);
				viewHolder.mAppUpload.setVisibility(View.VISIBLE);
				viewHolder.mAppNext.setVisibility(View.GONE);
				viewHolder.mAppLock.setVisibility(View.GONE);

				if (PangkerConstant.RES_DOCUMENT == item.getResType()) {
					viewHolder.mAppIcon.setImageResource(R.drawable.attachment_doc);
				} else {
					String iconUrl = Configuration.getCoverDownload() + item.getDirId();
					if (PangkerConstant.RES_APPLICATION == item.getResType()) {
						mImageWorker.setLoadingImage(R.drawable.icon46_apk);
					} else if (PangkerConstant.RES_MUSIC == item.getResType()) {
						mImageWorker.setLoadingImage(R.drawable.icon46_music);
					} else {
						viewHolder.mAppIcon.setBackgroundResource(R.drawable.listmod_bighead_photo_default);
					}
					if (!pathList.contains(iconUrl)) {
						mImageWorker.loadImage(iconUrl, viewHolder.mAppIcon, String.valueOf(item.getDirId()));
					}
				}

				viewHolder.mAppName.setText(item.getDirName());
				String fileSize = Formatter.formatFileSize(context, item.getFileCount());
				viewHolder.mAppSize.setText(fileSize);
				if (item.getDeal() == 1) {
					viewHolder.mAppShare.setImageResource(R.drawable.icon32_share);
				} else {
					viewHolder.mAppShare.setImageResource(R.drawable.icon32_unshare);
				}

				// >>>>>>>
				if (item.getSource() == 0) {
					viewHolder.mAppUpload.setImageResource(R.drawable.icon46_d);
					viewHolder.mAppShare.setVisibility(View.VISIBLE);
					viewHolder.mAppContent.setText("上传时间:" + item.getAppraisement());
				} else if (item.getSource() == 1) {
					viewHolder.mAppUpload.setImageResource(R.drawable.icon16_keep);
					viewHolder.mAppShare.setVisibility(View.GONE);
					viewHolder.mAppContent.setText("收藏时间:" + item.getAppraisement());
				} else {
					viewHolder.mAppUpload.setVisibility(View.GONE);
					viewHolder.mAppContent.setText("上传时间:" + item.getAppraisement());
				}

			}
			// 如果是目录
			else if (item.getIsfile() == 0) {
				viewHolder.mAppContent.setVisibility(View.VISIBLE);
				viewHolder.mAppNext.setVisibility(View.VISIBLE);
				viewHolder.mAppSize.setVisibility(View.GONE);
				viewHolder.mAppShare.setVisibility(View.GONE);
				viewHolder.mAppUpload.setVisibility(View.GONE);

				viewHolder.mAppIcon.setImageResource(R.drawable.icon46_file);
				viewHolder.mAppName.setText(item.getDirName());
				if (PangkerConstant.RES_APPLICATION == item.getResType()) {
					viewHolder.mAppContent.setText("有" + item.getFileCount() + "个应用");
				} else if (PangkerConstant.RES_MUSIC == item.getResType()) {
					viewHolder.mAppContent.setText("有" + item.getFileCount() + "个音乐");
				} else {
					viewHolder.mAppContent.setText("有" + item.getFileCount() + "个文件");
				}

				viewHolder.mAppNext.setImageResource(R.drawable.sc_openablum);
				if (ifLock) {
					if (Util.ifShowLock(item.getVisitType())) {
						viewHolder.mAppLock.setVisibility(View.VISIBLE);
					} else {
						viewHolder.mAppLock.setVisibility(View.GONE);
					}
				}
			} else {
				viewHolder.mAppIcon.setImageResource(R.drawable.toolbar_uplevel);
				viewHolder.mAppShare.setVisibility(View.GONE);
				viewHolder.mAppSize.setVisibility(View.GONE);
				viewHolder.mAppLock.setVisibility(View.GONE);
				viewHolder.mAppUpload.setVisibility(View.GONE);
				viewHolder.mAppName.setText(item.getDirName());
				viewHolder.mAppContent.setVisibility(View.GONE);
				viewHolder.mAppNext.setVisibility(View.GONE);
			}
		}
		return convertView;
	}

	/**
	 * 每个应用显示的内容，包括图标和名称
	 * 
	 * @author Yao.GUET
	 * 
	 */
	class ViewHolder {
		ImageView mAppIcon;
		TextView mAppName;
		ImageView mAppNext;
		TextView mAppContent;
		ImageView mAppShare;
		ImageView mAppLock;
		ImageView mAppUpload;
		TextView mAppSize;
	}

}
