package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.image.ImageLocalFetcher;

public class MyAlbumAdapter extends BaseAdapter {

	private Activity context;
	private List<DirInfo> localDirInfos;
	private ArrayList<Boolean> mchecked;
	private ImageLocalFetcher imageLocalFetcher;
	private boolean ifShow = false;

	public MyAlbumAdapter(Activity context, ImageLocalFetcher localFetcher) {
		super();
		this.context = context;
		localDirInfos = new ArrayList<DirInfo>();
		mchecked = new ArrayList<Boolean>();
		this.imageLocalFetcher = localFetcher;
	}

	public void init() {
		// TODO Auto-generated method stub
		mchecked.clear();
		for (int i = 0; i < localDirInfos.size(); i++) {
			mchecked.add(false);
		}
	}

	public void setLocalDirInfos(List<DirInfo> localDirInfos) {
		this.localDirInfos = localDirInfos;
		init();
		notifyDataSetChanged();
	}

	public void addPidInfo(DirInfo dirInfo) {
		// localDirInfos.add(dirInfo);
		mchecked.add(false);
		notifyDataSetChanged();
	}

	public void remove(DirInfo dirInfo) {
		// TODO Auto-generated method stub
		localDirInfos.remove(dirInfo);
		notifyDataSetChanged();
	}

	public void setSelectedAll(boolean isSelectedAll) {
		for (int i = 0; i < localDirInfos.size(); i++) {
			mchecked.set(i, isSelectedAll);
		}
		notifyDataSetChanged();
	}

	public void selectByIndex(int index, boolean flag) {
		mchecked.set(index, flag);
	}

	public void setIfShow(boolean ifShow) {
		this.ifShow = ifShow;
		notifyDataSetChanged();
	}

	public List<DirInfo> getSeletedMembers() {
		int temp = mchecked.size();
		List<DirInfo> selectedMembers = new ArrayList<DirInfo>();
		for (int i = 0; i < temp; i++) {
			if (mchecked.get(i)) {
				selectedMembers.add(localDirInfos.get(i));
			}
		}
		return selectedMembers;
	}

	public List<DirInfo> getLocalDirInfos() {
		return localDirInfos;
	}

	public int getSeletedCount() {
		int count = 0;
		Iterator<Boolean> iter = mchecked.iterator();
		while (iter.hasNext()) {
			if (iter.next()) {
				count++;
			}
		}
		return count;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return localDirInfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return localDirInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolderAlbum viewHolderAlbum;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			viewHolderAlbum = new ViewHolderAlbum();
			convertView = LayoutInflater.from(context).inflate(R.layout.album_item, null);
			viewHolderAlbum.selected = (CheckBox) convertView.findViewById(R.id.Album_CheckBox);
			viewHolderAlbum.imageView = (ImageView) convertView.findViewById(R.id.Album_img);
			convertView.setTag(viewHolderAlbum);
		} else {
			viewHolderAlbum = (ViewHolderAlbum) convertView.getTag();
		}

		DirInfo item = (DirInfo) getItem(position);
		if (ifShow) {
			viewHolderAlbum.selected.setVisibility(View.VISIBLE);
		} else {
			viewHolderAlbum.selected.setVisibility(View.GONE);
		}
		// >>>>>>加载图片
		imageLocalFetcher.loadImage(item.getPath(), viewHolderAlbum.imageView, item.getPath());
		viewHolderAlbum.selected.setChecked(mchecked.get(position));
		return convertView;
	}

	public class ViewHolderAlbum {
		public CheckBox selected; // 是否选择一组
		public ImageView imageView; // 用户头像
	}

}
