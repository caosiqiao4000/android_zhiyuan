package com.wachoo.pangker.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;

public class RootWhoAdapter extends BaseAdapter {

	private List<UserItem> cgList;
	private Context context;
	Holder holder;
	private PKIconResizer mImageResizer;

	public Map<Integer, Boolean> isSelected;

	public RootWhoAdapter(Context context, List<UserItem> cgList, String[] uids) {

		this.cgList = cgList;
		this.context = context;
		isSelected = new HashMap<Integer, Boolean>();
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		for (int i = 0; i < cgList.size(); i++) {
			isSelected.put(i, false);
		}
		if (uids != null && uids.length > 0) {
			for (int i = 0; i < cgList.size(); i++) {
				for (int j = 0; j < uids.length; j++) {
					if (cgList.get(i).getUserId().equals(uids[j])) {
						isSelected.put(i, true);
					}
				}
			}
		}
	}

	public final class Holder {
		public TextView name;
		public CheckBox check;
		public ImageView icon;
		public TextView sign;
		public String id;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return cgList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return cgList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		UserItem item = cgList.get(position);
		holder = null;
		if (convertView == null) {
			holder = new Holder();
			convertView = LayoutInflater.from(context).inflate(R.layout.tracegroup_member_item, null);
			holder.name = (TextView) convertView.findViewById(R.id.tv_name);
			holder.sign = (TextView) convertView.findViewById(R.id.tv_left_bottom);
			holder.check = (CheckBox) convertView.findViewById(R.id.mtrace_checkbox);
			holder.icon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.id = item.getUserId();
		holder.name.setText(item.getUserName());
		holder.check.setChecked(isSelected.get(position));
		holder.sign.setText(item.getSign());

		mImageResizer.loadImage(item.getUserId(), holder.icon);

		return convertView;
	}

}
