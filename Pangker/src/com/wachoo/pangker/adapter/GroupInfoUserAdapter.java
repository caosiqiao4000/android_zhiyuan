package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;

/**
 * 
 */
public class GroupInfoUserAdapter extends BaseAdapter {

	private Context context;
	private List<UserItem> userList;
	private boolean isDeleteState = false;// GridView是否在选择删除状态
	private boolean isCreater = false; // 当前用户是否创建者
	private OnClickListener mAddClickListener;

	private PKIconResizer mImageResizer;

	public GroupInfoUserAdapter(Context context, List<UserItem> userList, PKIconResizer mImageResizer) {
		this.context = context;
		this.userList = userList;
		this.mImageResizer = mImageResizer;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userList.size() + 2;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return userList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {
		public ImageView ivExpress;
		public TextView tvUsername;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.res_gv_useritem, null);
			viewHolder.ivExpress = (ImageView) convertView
					.findViewById(R.id.iv_usericon);
			viewHolder.tvUsername = (TextView) convertView
					.findViewById(R.id.tv_username);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		if (position < getCount() - 2) {
			final UserItem mUserItem = userList.get(position);
			viewHolder.tvUsername.setText(mUserItem.getUserName());
			ImageView btnClear = (ImageView) convertView
					.findViewById(R.id.btn_clear);
			if (isDeleteState) {
				btnClear.setVisibility(View.VISIBLE);
			}
			viewHolder.ivExpress.setImageResource(R.drawable.nav_head);

			mImageResizer
					.loadImage(mUserItem.getUserId(), viewHolder.ivExpress);

		} else if (position == getCount() - 2) {
			convertView.setOnClickListener(mAddClickListener);
			viewHolder.ivExpress.setImageResource(R.drawable.btn_add_selector);
		} else if (position == getCount() - 1) {
			if (isCreater) {
				convertView.setVisibility(View.VISIBLE);
			} else
				convertView.setVisibility(View.GONE);
			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					setDeleteState(!isDeleteState);
				}
			});
			viewHolder.ivExpress.setImageResource(R.drawable.btn_dele_selector);
		}

		return convertView;
	}

	public List<UserItem> getUserList() {
		return userList;
	}

	public void setUserList(List<UserItem> userList) {
		this.userList = userList;
		this.notifyDataSetChanged();
	}

	public void addUserList(List<UserItem> mUserList) {
		// this.userList.addAll(mUserList);
		for (UserItem userItem : mUserList) {
			if (!isExist(userItem)) {
				this.userList.add(userItem);
			}
		}
		this.notifyDataSetChanged();
	}

	private boolean isExist(UserItem user) {
		for (UserItem userItem : this.userList) {
			if (userItem.getUserId().equals(user.getUserId())) {
				return true;
			}
		}
		return false;
	}

	public boolean isDeleteState() {
		return isDeleteState;
	}

	/**
	 * TODO 设置是否在删除状态
	 * 
	 */
	public void setDeleteState(boolean isDeleteState) {
		this.isDeleteState = isDeleteState;
		this.notifyDataSetChanged();
	}

	public OnClickListener getmAddClickListener() {
		return mAddClickListener;
	}

	/**
	 * void TODO添加成员监听
	 * 
	 * @param mAddClickListener
	 */
	public void setmAddClickListener(OnClickListener mAddClickListener) {
		this.mAddClickListener = mAddClickListener;
	}

	public boolean isCreater() {
		return isCreater;
	}

	/**
	 * void TODO 设置是否群组创建者
	 * 
	 * @param isCreater
	 */
	public void setCreater(boolean isCreater) {
		this.isCreater = isCreater;
	}

}
