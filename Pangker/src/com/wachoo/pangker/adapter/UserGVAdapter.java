package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;

public class UserGVAdapter extends BaseAdapter {

	private Context context;
	private List<UserItem> user;

	private PKIconResizer mImageResizer;

	public UserGVAdapter(Context context, List<UserItem> user) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.user = user;
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext().getApplicationContext());
	}

	@Override
	public int getCount() {
		return user.size();
	}

	@Override
	public UserItem getItem(int position) {
		// TODO Auto-generated method stub
		return user.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final UserItem item = getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.gv_user_item, null);
			viewHolder = new ViewHolder();
			viewHolder.usericon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			viewHolder.iv_sex = (ImageView) convertView.findViewById(R.id.iv_sex);
			viewHolder.textview = (TextView) convertView.findViewById(R.id.tv_gridview);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (PangkerConstant.NEAR_USER_GIRLS.equals(item.getSex())) {
			viewHolder.iv_sex.setImageResource(R.drawable.icon24_g);
		} else if (PangkerConstant.NEAR_USER_BOYS.equals(item.getSex())) {
			viewHolder.iv_sex.setImageResource(R.drawable.icon24_m);
		}

		mImageResizer.loadImage(item.getUserId(), viewHolder.usericon);
		viewHolder.textview.setText(item.getUserName());
		return convertView;
	}
	
	class ViewHolder {
		public ImageView usericon; // 图标
		public ImageView iv_sex; // 名称
		public TextView textview; // 时间
	}

	ViewHolder viewHolder = null;

	public List<UserItem> getUser() {
		return user;
	}

	public void setUser(List<UserItem> user) {
		this.user = user;
		this.notifyDataSetChanged();
	}

}
