package com.wachoo.pangker.adapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;

public class FileAdapter extends BaseAdapter {

	private Context context;
	private File nowFile;
	private List<File> fileLists;

	public FileAdapter(Context context) {
		super();
		this.context = context;
		fileLists = new ArrayList<File>();
	}

	public void setFileLists(File file) {
		if (!file.exists()) {
			return;
		}
		this.nowFile = file;
		fileLists.clear();
		Collections.addAll(fileLists, file.listFiles());
		if (!nowFile.getAbsolutePath().equals(PangkerConstant.DIR_ROOT)) {
			fileLists.add(0, new File("..."));
		}
		notifyDataSetChanged();
	}

	public File getNowFile() {
		return nowFile;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return fileLists.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return fileLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		File item = null;
		ViewHolder viewHolder;
		item = (File) getItem(position);

		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.file_item, null);
			viewHolder.appIcon = (ImageView) convertView.findViewById(R.id.app_img);
			viewHolder.appName = (TextView) convertView.findViewById(R.id.app_content);
			viewHolder.appNext = (ImageView) convertView.findViewById(R.id.app_next);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (item != null) {
			if (!item.exists()) {
				viewHolder.appName.setText("...上一级");
				viewHolder.appNext.setVisibility(View.GONE);
				viewHolder.appIcon.setImageResource(R.drawable.icon46_file);
			} else {
				if (item.isDirectory()) {
					int len = 0;
					if (item.listFiles() != null) {
						len = item.listFiles().length;
					}
					viewHolder.appName.setText(item.getName() + " (" + len + ")");
					viewHolder.appIcon.setImageResource(R.drawable.icon46_file);
					viewHolder.appNext.setVisibility(View.VISIBLE);
				} else {
					viewHolder.appName.setText(item.getName());
					viewHolder.appNext.setVisibility(View.GONE);
					viewHolder.appIcon.setImageResource(getCover(item.getName()));
				}
			}
		}
		return convertView;
	}

	private int getCover(String filename) {
		if (filename.endsWith(".apk")) {
			return R.drawable.icon46_apk;
		} else if (filename.endsWith(".jpeg") || filename.endsWith(".png")) {
			return R.drawable.listmod_bighead_photo_default;
		} else if (filename.endsWith(".jpg")) {
			return R.drawable.listmod_bighead_photo_default;
		} else if (filename.endsWith(".mp3")) {
			return R.drawable.icon46_music;
		} else if (filename.endsWith(".txt")) {
			return R.drawable.attachment_doc;
		}
		return R.drawable.attachment_doc;
	}

	class ViewHolder {
		ImageView appIcon;
		TextView appName;
		ImageView appNext;
	}
}
