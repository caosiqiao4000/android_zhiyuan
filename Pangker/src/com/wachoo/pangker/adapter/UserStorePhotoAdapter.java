package com.wachoo.pangker.adapter;

import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class UserStorePhotoAdapter extends BaseAdapter {

	private Activity context;
	private List<DirInfo> resinfos;

	// >>>>>>获取图片
	protected ImageResizer mImageWorker;

	private View.OnClickListener onClickListener;
	private View.OnLongClickListener onLongClickListener;
	private boolean ismore;
	private boolean isloading = false;

	public UserStorePhotoAdapter(Activity context, List<DirInfo> resinfos, ImageResizer mImageWorker) {
		super();
		this.context = context;
		this.resinfos = resinfos;
		this.mImageWorker = mImageWorker;
	}

	private boolean ifhaveNav() {
		for (DirInfo dirInfo : resinfos) {
			if (dirInfo.getIsfile() == -1) {
				return true;
			}
		}
		return false;
	}

	private DirInfo getNavDirinfo(DirInfo dirinfo) {
		DirInfo dir = new DirInfo();
		dir.setDirId(dirinfo.getDirId());
		dir.setParentDirId(dirinfo.getParentDirId());
		dir.setDirName("返回上一层目录...");
		dir.setIsfile(-1);
		return dir;
	}

	/**
	 * 给ResUserStoreActivity界面使用
	 * 
	 * @param resinfos
	 * @param dir
	 * @param ifmore
	 */
	public void setUserResinfos(List<DirInfo> resinfos, DirInfo dir, boolean ifmore) {
		this.resinfos = resinfos;
		if (dir != null && !"0".equals(dir.getDirId()) && !ifhaveNav()) {
			this.resinfos.add(0, getNavDirinfo(dir));
		}
		this.ismore = ifmore;
		isloading = false;
		notifyDataSetChanged();
	}

	/**
	 * 给ResNetManagerActivity界面使用
	 * 
	 * @param resinfos
	 * @param dir
	 * @param ifmore
	 */
	public void setResinfos(List<DirInfo> resinfos, DirInfo dir, boolean ifmore) {
		this.resinfos = resinfos;
		if (dir != null && !"0".equals(dir.getParentDirId()) && !ifhaveNav()) {
			this.resinfos.add(0, getNavDirinfo(dir));
		}
		this.ismore = ifmore;
		isloading = false;
		notifyDataSetChanged();
		notifyDataSetChanged();
	}

	public void setResinfos(List<DirInfo> resinfos) {
		this.resinfos = resinfos;
		notifyDataSetChanged();
	}

	public int getResCount() {
		int index = 0;
		Iterator<DirInfo> iter = resinfos.iterator();
		while (iter.hasNext()) {
			DirInfo item = iter.next();
			if (item.getIsfile() == DirInfo.ISFILE) {
				index++;
			}
		}
		return index;
	}

	public void delDirInfo(DirInfo resinfo) {
		// TODO Auto-generated method stub
		if (resinfos.contains(resinfo)) {
			resinfos.remove(resinfo);
			notifyDataSetChanged();
		}
	}

	public void addDirInfo(DirInfo resinfo) {
		if (!resinfos.contains(resinfo)) {
			this.resinfos.add(0, resinfo);
			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (!ismore) {
			return resinfos.size();
		}
		return resinfos.size() + 1;
	}

	public int getSize() {
		if (ifhaveNav()) {
			return resinfos.size() - 1;
		}
		return resinfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if (position >= resinfos.size()) {
			return null;
		}
		return resinfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.store_photo_item, null);
			viewHolder = new ViewHolder();

			viewHolder.viewLayout = (RelativeLayout) convertView.findViewById(R.id.view_layout);
			viewHolder.imgShow = (ImageView) convertView.findViewById(R.id.iv_gridview1);
			viewHolder.imgIcon = (ImageView) convertView.findViewById(R.id.load_more_icon);
			viewHolder.pgLoading = (ProgressBar) convertView.findViewById(R.id.loading_pb);
			viewHolder.imgShare = (ImageView) convertView.findViewById(R.id.iv_share1);
			viewHolder.imgKeep = (ImageView) convertView.findViewById(R.id.iv_keep1);
			viewHolder.txtName = (TextView) convertView.findViewById(R.id.tv_gridview1);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
		DirInfo item = (DirInfo) getItem(position);
		viewHolder.viewLayout.setVisibility(View.VISIBLE);
		viewHolder.viewLayout.setOnClickListener(new MoreOnClickClickListener(viewHolder.pgLoading, viewHolder.imgIcon,
				viewHolder.txtName, item));
		if (item != null) {
			viewHolder.viewLayout.setTag(R.id.tag_index, position);
			viewHolder.viewLayout.setTag(R.id.tag_shareinfo, item);
			viewHolder.viewLayout.setOnLongClickListener(onLongClickListener);
			// 显示目录
			viewHolder.pgLoading.setVisibility(View.GONE);
			if (item.getIsfile() == 0) {
				viewHolder.imgShow.setVisibility(View.GONE);
				viewHolder.imgIcon.setVisibility(View.VISIBLE);
				viewHolder.imgKeep.setVisibility(View.GONE);
				viewHolder.imgIcon.setImageResource(R.drawable.icon88_file);
				if (Util.ifShowLock(item.getVisitType())) {
					viewHolder.imgShare.setImageResource(R.drawable.icon46_lock);
					viewHolder.imgShare.setVisibility(View.VISIBLE);
				} else {
					viewHolder.imgShare.setVisibility(View.GONE);
				}
				viewHolder.txtName.setText(item.getDirName() + "(" + item.getFileCount() + ")");
			}
			// 显示加载图片
			else if (item.getIsfile() == 1) {
				// >>>>>.获取网络地址，根据边长
				viewHolder.imgShow.setVisibility(View.VISIBLE);
				viewHolder.imgIcon.setVisibility(View.GONE);
				viewHolder.imgShare.setVisibility(View.VISIBLE);
				viewHolder.imgKeep.setVisibility(View.VISIBLE);
				int sideLength = ImageUtil.getPicPreviewResolution(context);
				String iconUrl = ImageUtil.getWebImageURL(item.getDirId(), sideLength);
				mImageWorker.loadImage(iconUrl, viewHolder.imgShow, String.valueOf(item.getDirId()) + "_" + sideLength);
				if (item.getDeal() == 1) {
					viewHolder.imgShare.setImageResource(R.drawable.icon32_share);
				} else {
					viewHolder.imgShare.setImageResource(R.drawable.icon32_unshare);
				}
				viewHolder.txtName.setText(item.getDirName());

				// >>>>>>>
				if (item.getSource() == 0) {
					viewHolder.imgKeep.setVisibility(View.GONE);
				} else if (item.getSource() == 1) {
					viewHolder.imgKeep.setVisibility(View.VISIBLE);
					viewHolder.imgKeep.setImageResource(R.drawable.icon16_keep);
					viewHolder.imgShare.setVisibility(View.GONE);
				} else {
					viewHolder.imgKeep.setVisibility(View.GONE);
				}
			}
			// 显示进入上层目录
			else if (item.getIsfile() == -1) {
				viewHolder.imgShow.setVisibility(View.GONE);
				viewHolder.imgIcon.setVisibility(View.VISIBLE);
				viewHolder.imgIcon.setImageResource(R.drawable.toolbar_uplevel);
				viewHolder.txtName.setText(item.getDirName());
				viewHolder.imgShare.setVisibility(View.GONE);
				viewHolder.imgKeep.setVisibility(View.GONE);
			}
		}
		// 显示加载更多
		else {
			viewHolder.imgShow.setVisibility(View.GONE);
			viewHolder.imgKeep.setVisibility(View.GONE);
			if (isloading) {
				viewHolder.imgIcon.setVisibility(View.GONE);
				viewHolder.pgLoading.setVisibility(View.VISIBLE);
				viewHolder.txtName.setText("正在加载");
			}
			if (ismore) {
				viewHolder.imgIcon.setVisibility(View.VISIBLE);
				viewHolder.imgIcon.setImageResource(R.drawable.talkgroup_crew_get);
				viewHolder.txtName.setText("显示更多");
				viewHolder.imgShare.setVisibility(View.GONE);
			} else {
				viewHolder.viewLayout.setVisibility(View.GONE);
			}
		}
		return convertView;
	}

	public void setOnClickListener(View.OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}

	public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
		this.onLongClickListener = onLongClickListener;
	}

	class ViewHolder {
		RelativeLayout viewLayout;
		ProgressBar pgLoading;
		ImageView imgIcon;
		ImageView imgShow;
		ImageView imgShare;
		ImageView imgKeep;
		TextView txtName;
	}

	private FooterView.OnLoadMoreListener onLoadMoreListener;

	public void setOnLoadMoreListener(FooterView.OnLoadMoreListener onLoadMoreListener) {
		this.onLoadMoreListener = onLoadMoreListener;
	}

	class MoreOnClickClickListener implements View.OnClickListener {

		private ProgressBar pgLoading;
		private ImageView imgShow;
		private TextView txtName;
		private DirInfo item;

		public MoreOnClickClickListener(ProgressBar pgLoading, ImageView imgShow, TextView txtName, DirInfo item) {
			super();
			this.pgLoading = pgLoading;
			this.imgShow = imgShow;
			this.txtName = txtName;
			this.item = item;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (item == null) {
				// 有加载跟多，同时并没有进行加载
				if (ismore && !isloading) {
					isloading = true;
					imgShow.setVisibility(View.GONE);
					pgLoading.setVisibility(View.VISIBLE);
					txtName.setText("正在加载");
					if (onLoadMoreListener != null) {
						onLoadMoreListener.onLoadMoreListener();
					}
				}
			} else {
				if (onClickListener != null) {
					onClickListener.onClick(v);
				}
			}
		}

	}

}
