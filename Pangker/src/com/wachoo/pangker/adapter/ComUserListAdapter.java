package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;

/**
 * @author wangxin
 * @version Time：2011-5-1 下午03:22:00
 */
public class ComUserListAdapter extends BaseAdapter {

	private LayoutInflater layoutInflater;
	private OnClickListener intoOnClickListener;
	private List<UserItem> mUserList;
	private Context context;
	private ViewHolder holder = null;
	private PKIconResizer mImageResizer = null;

	/**
	 * @return the mUserList
	 */
	public List<UserItem> getmUserList() {
		return mUserList;
	}

	/**
	 * @param mUserList
	 *            the mUserList to set
	 */
	public void setmUserList(List<UserItem> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	public ComUserListAdapter(Context context, List<UserItem> users, OnClickListener intoOnClickListener) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.intoOnClickListener = intoOnClickListener;
		this.mUserList = users;
		this.mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	public int getCount() {
		return mUserList == null ? 0 : mUserList.size();
	}

	public UserItem getItem(int position) {
		if (mUserList != null) {
			return mUserList.get(position);
		}
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.contacts_address_item, null);
			holder.iv_userIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			holder.tv_userName = (TextView) convertView.findViewById(R.id.tv_username);
			holder.tv_sign = (TextView) convertView.findViewById(R.id.tv_usersign);
			holder.iv_into = (ImageView) convertView.findViewById(R.id.iv_into);
			holder.iv_into.setVisibility(View.VISIBLE);
			holder.iv_status = (ImageView) convertView.findViewById(R.id.iv_status);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		// 加入用户在线状态

		final UserItem item = mUserList.get(position);

		if (item.getState() == PangkerConstant.STATE_TYPE_ONLINE) {
			holder.iv_status.setImageResource(R.drawable.icon_online);
		} else {
			holder.iv_status.setImageResource(R.drawable.icon_offline);
		}
		// 备注为空则取用户名
		String userNmae = item.getAvailableName();
		holder.tv_userName.setText(userNmae);
		holder.tv_sign.setText(item.getSign());
		holder.iv_into.setOnClickListener(intoOnClickListener);
		holder.iv_into.setTag(position);

		holder.iv_userIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, item.getUserId());
				intent.putExtra(UserItem.USERNAME, item.getUserName());
				intent.putExtra(UserItem.USERSIGN, item.getSign());
				intent.putExtra(PangkerConstant.STATE_KEY, item.getState());
				context.startActivity(intent);
			}
		});

		mImageResizer.loadImage(item.getUserId(), holder.iv_userIcon);

		return convertView;
	}

	public final class ViewHolder {
		public ImageView iv_userIcon;
		public TextView tv_firstCharHint;
		public ImageView iv_order;
		public TextView tv_userName;
		public TextView tv_sign;
		public ImageView iv_status;
		public ImageView iv_into;

		public void refresh(final UserItem item) {
			// imageLoader.loadDrawable(Configuration.getIconUrl() +
			// item.getUserId(), new ImageCallback() {
			// public void imageLoaded(Drawable imageDrawable, String imageUrl)
			// {
			// if (imageDrawable != null) {
			// // 此处应该判断是否在线
			// Bitmap bitmap = Util.drawableToBitmap(imageDrawable);
			// iv_userIcon.setImageBitmap(bitmap);
			// if (bitmap != null) {
			// Util.saveUserIcon(bitmap, item.getUserId());
			// }
			// }
			// }
			// }, context);
			mImageResizer.loadImage(item.getUserId(), holder.iv_userIcon);
			tv_userName.setText(item.getUserName());
			tv_sign.setText(item.getSign());
		}
	}

}