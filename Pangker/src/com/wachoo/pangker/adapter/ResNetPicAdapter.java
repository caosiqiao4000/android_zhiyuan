package com.wachoo.pangker.adapter;

import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.response.PicInfo;
import com.wachoo.pangker.util.ImageUtil;

public class ResNetPicAdapter extends BaseAdapter {
	private final static String TAG = "ResNetPhotoAdapter";

	private Activity context;
	private List<PicInfo> picInfos;

	private ImageResizer mImageWorker;
	private PangkerApplication myApplication;
	private View.OnClickListener picOnClickListener;
	private View.OnLongClickListener picOnLongClickListener;

	public ResNetPicAdapter(Activity context, List<PicInfo> result, ImageResizer mImageWorker) {

		this.context = context;
		this.picInfos = result;

		this.mImageWorker = mImageWorker;
		myApplication = (PangkerApplication) context.getApplicationContext();

	}

	public void clear() {
		picInfos.clear();
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		Log.i(TAG, "getCount=" + (picInfos.size() + 2) / 3);
		// TODO Auto-generated method stub
		return (picInfos.size() + 2) / 3;
	}

	@Override
	public PicInfo getItem(int position) {
		// TODO Auto-generated method stub
		return picInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {

		public LinearLayout ly_gridview_item1;
		public ImageView iv_gridview_item1;
		public TextView tv_gridview_score1;
		public TextView tv_gridview_item1;

		public LinearLayout ly_gridview_item2;
		public ImageView iv_gridview_item2;
		public TextView tv_gridview_score2;
		public TextView tv_gridview_item2;

		public LinearLayout ly_gridview_item3;
		public ImageView iv_gridview_item3;
		public TextView tv_gridview_score3;
		public TextView tv_gridview_item3;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		final int location = position * 3;
		PicInfo item_one = null;
		PicInfo item_two = null;
		PicInfo item_three = null;

		// 获取项目数据源
		if (location <= picInfos.size() - 1) {
			item_one = getItem(location);
		}
		if (location + 1 <= picInfos.size() - 1) {
			item_two = getItem(location + 1);
		}
		if (location + 2 <= picInfos.size() - 1) {
			item_three = getItem(location + 2);
		}

		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.pk_res_pic, null);
			holder.ly_gridview_item1 = (LinearLayout) convertView.findViewById(R.id.ly_gridview_item1);
			holder.iv_gridview_item1 = (ImageView) convertView.findViewById(R.id.iv_gridview_item1);
			holder.tv_gridview_item1 = (TextView) convertView.findViewById(R.id.tv_gridview_item1);
			holder.tv_gridview_score1 = (TextView) convertView.findViewById(R.id.tv_gridview_score1);

			holder.ly_gridview_item2 = (LinearLayout) convertView.findViewById(R.id.ly_gridview_item2);
			holder.iv_gridview_item2 = (ImageView) convertView.findViewById(R.id.iv_gridview_item2);
			holder.tv_gridview_item2 = (TextView) convertView.findViewById(R.id.tv_gridview_item2);
			holder.tv_gridview_score2 = (TextView) convertView.findViewById(R.id.tv_gridview_score2);

			holder.ly_gridview_item3 = (LinearLayout) convertView.findViewById(R.id.ly_gridview_item3);
			holder.iv_gridview_item3 = (ImageView) convertView.findViewById(R.id.iv_gridview_item3);
			holder.tv_gridview_item3 = (TextView) convertView.findViewById(R.id.tv_gridview_item3);
			holder.tv_gridview_score3 = (TextView) convertView.findViewById(R.id.tv_gridview_score3);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
		initViewData(item_one, holder.ly_gridview_item1, holder.iv_gridview_item1, holder.tv_gridview_score1,
				holder.tv_gridview_item1, location);

		initViewData(item_two, holder.ly_gridview_item2, holder.iv_gridview_item2, holder.tv_gridview_score2,
				holder.tv_gridview_item2, location + 1);

		initViewData(item_three, holder.ly_gridview_item3, holder.iv_gridview_item3,
				holder.tv_gridview_score3, holder.tv_gridview_item3, location + 2);

		return convertView;
	}

	private void initViewData(final PicInfo item, View relativeLayout, ImageView iv_imageView,
			TextView tv_resScore, TextView tv_resName, final int location) {
		if (item != null) {
			relativeLayout.setVisibility(View.VISIBLE);
			iv_imageView.setVisibility(View.VISIBLE);
			tv_resName.setVisibility(View.VISIBLE);
			int sideLength = ImageUtil.getPicPreviewResolution(context);
			String photoUrl = ImageUtil.getWebImageURL(item.getSid(), sideLength);
			// >>>>>>>>>设置默认背景图片
			mImageWorker.setLoadingImage(R.drawable.photolist_head);
			mImageWorker.loadImage(photoUrl, iv_imageView, String.valueOf(item.getSid()) + "_" + sideLength);
			iv_imageView.setTag(location);
			iv_imageView.setOnClickListener(picOnClickListener);
			iv_imageView.setOnLongClickListener(picOnLongClickListener);
			tv_resName.setText(item.getResName());

		} else {

			relativeLayout.setBackgroundResource(R.drawable.main_bg);
			iv_imageView.setVisibility(View.GONE);
			tv_resName.setVisibility(View.GONE);
		}
		tv_resScore.setVisibility(View.GONE);
	}

	public void setPhotoResult(List<PicInfo> photoResult) {
		this.picInfos = photoResult;
		notifyDataSetChanged();
	}

	public void setPicOnClickListener(View.OnClickListener picOnClickListener) {
		this.picOnClickListener = picOnClickListener;
	}

	public void setPicOnLongClickListener(View.OnLongClickListener picOnLongClickListener) {
		this.picOnLongClickListener = picOnLongClickListener;
	}

}