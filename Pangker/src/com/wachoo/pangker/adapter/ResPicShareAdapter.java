package com.wachoo.pangker.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.response.ShareInfo;
import com.wachoo.pangker.util.ImageUtil;

public class ResPicShareAdapter extends BaseAdapter {

	private Activity context;
	private List<ShareInfo> shareinfos;
	private ImageResizer mImageWorker;
	private View.OnClickListener onClickListener;
	private View.OnLongClickListener onLongClickListener;

	public ResPicShareAdapter(Activity context, List<ShareInfo> collectRes, ImageResizer mImageWorker) {
		super();
		this.context = context;
		this.shareinfos = collectRes;
		this.mImageWorker = mImageWorker;
	}

	public void setOnClickListener(View.OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}

	public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
		this.onLongClickListener = onLongClickListener;
	}

	public void setShareinfos(List<ShareInfo> shareinfos) {
		this.shareinfos = shareinfos;
		notifyDataSetChanged();
	}

	public void delShareInfo(ShareInfo shareInfo) {
		// TODO Auto-generated method stub
		shareinfos.remove(shareInfo);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return (shareinfos.size() + 2) / 3;
	}

	public int getSize() {
		return shareinfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return shareinfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolder viewHolder = null;
	private final int[] ID_SHOW = { R.id.iv_gridview1, R.id.iv_gridview2, R.id.iv_gridview3 };
	private final int[] ID_NAME = { R.id.tv_gridview1, R.id.tv_gridview2, R.id.tv_gridview3 };
	private final int[] ID_SHARE = { R.id.iv_share1, R.id.iv_share2, R.id.iv_share3 };

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		int location = position * 3;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.resmamnger_pic_item, null);
			for (int i = 0; i < ID_SHOW.length; i++) {
				viewHolder.imgShow[i] = (ImageView) convertView.findViewById(ID_SHOW[i]);
				viewHolder.imgShare[i] = (ImageView) convertView.findViewById(ID_SHARE[i]);
				viewHolder.txtName[i] = (TextView) convertView.findViewById(ID_NAME[i]);
			}
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		// final ShareInfo item = collectRes.get(position);
		ShareInfo item[] = new ShareInfo[3];
		for (int i = 0; i < ID_SHOW.length; i++) {
			// 获取项目数据源
			if (location <= getSize() - 1 - i) {
				item[i] = (ShareInfo) getItem(location + i);
			}
		}
		for (int i = 0; i < item.length; i++) {
			if (item[i] != null) {
				viewHolder.imgShow[i].setVisibility(View.VISIBLE);
				viewHolder.imgShow[i].setTag(R.id.tag_index, location + i);
				viewHolder.imgShow[i].setTag(R.id.tag_shareinfo, item[i]);

				viewHolder.txtName[i].setVisibility(View.VISIBLE);
				viewHolder.imgShow[i].setOnClickListener(onClickListener);
				viewHolder.imgShow[i].setOnLongClickListener(onLongClickListener);
				int sideLength = ImageUtil.getPicPreviewResolution(context);
				String iconUrl = ImageUtil.getWebImageURL(item[i].getResId(), sideLength);

				viewHolder.txtName[i].setText(item[i].getName());
				mImageWorker.setLoadingImage(R.drawable.photolist_head);
				mImageWorker.loadImage(iconUrl, viewHolder.imgShow[i], String.valueOf(item[i].getResId())
						+ "_" + sideLength);
				if (item[i].getFlag() == 1) {
					viewHolder.imgShare[i].setVisibility(View.VISIBLE);
					viewHolder.imgShare[i].setImageResource(R.drawable.icon16_forward);
				} else {
					viewHolder.imgShare[i].setVisibility(View.GONE);
				}
			} else {
				viewHolder.imgShow[i].setVisibility(View.GONE);
				viewHolder.txtName[i].setVisibility(View.GONE);
				viewHolder.imgShare[i].setVisibility(View.GONE);
			}
		}
		return convertView;
	}

	public class ViewHolder {
		ImageView[] imgShow = new ImageView[3];
		ImageView[] imgShare = new ImageView[3];
		TextView[] txtName = new TextView[3];
	}

}
