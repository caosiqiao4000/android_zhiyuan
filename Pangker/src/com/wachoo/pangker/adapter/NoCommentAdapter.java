package com.wachoo.pangker.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;

public class NoCommentAdapter extends BaseAdapter {

	private Context mContext;

	public NoCommentAdapter(Context mContext) {
		this.mContext = mContext;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		TextView mTextView = new TextView(mContext);
		mTextView.setText(R.string.res_no_comment);
		return mTextView;
	}
}
