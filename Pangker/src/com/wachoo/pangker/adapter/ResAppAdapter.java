package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.response.AppInfo;

public class ResAppAdapter extends BaseAdapter {

	private Context context;
	List<AppInfo> mApps;
	private ImageResizer mImageWorker;

	public ResAppAdapter(Context context, List<AppInfo> mApps, ImageResizer mImageWorker) {
		this.context = context;
		this.mApps = mApps;
		this.mImageWorker = mImageWorker;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mApps.size();
	}

	@Override
	public AppInfo getItem(int position) {
		// TODO Auto-generated method stub
		return mApps.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		AppInfo info = mApps.get(position);

		ViewHolder viewHolder;

		// 缓存
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.res_app_list_item, null);
			viewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_appicon);
			viewHolder.tvAppName = (TextView) convertView.findViewById(R.id.tv_app_name);
			viewHolder.tvAppSize = (TextView) convertView.findViewById(R.id.tv_app_size);
			viewHolder.ivShare = (ImageView) convertView.findViewById(R.id.iv_share);
			viewHolder.tvCollect = (TextView) convertView.findViewById(R.id.res_tv_collect);
			viewHolder.tvComment = (TextView) convertView.findViewById(R.id.res_tv_comment);
			viewHolder.tvDownloadTimes = (TextView) convertView.findViewById(R.id.res_tv_loadtimes);
			viewHolder.ratinBar = (RatingBar) convertView.findViewById(R.id.rating_bar_show);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		// viewHolder.ivIcon.setImageResource(R.drawable.attachment_doc);
		String iconUrl = Configuration.getCoverDownload() + info.getSid();

		mImageWorker.setLoadingImage(R.drawable.icon46_apk);
		mImageWorker.loadImage(iconUrl, viewHolder.ivIcon, String.valueOf(info.getSid()));

		if (info.getFileSize() != null) {
			String appSize = Formatter.formatFileSize(context, new Double(info.getFileSize()).longValue());
			viewHolder.tvAppSize.setText(appSize != null ? appSize : "");
		}
		viewHolder.tvAppName.setText(info.getAppname());
		if (info.getIsShare()) {
			viewHolder.ivShare.setVisibility(View.VISIBLE);
		} else {
			viewHolder.ivShare.setVisibility(View.GONE);
		}
		viewHolder.ratinBar.setRating(((float) info.getScore()) / 2);

		Long score = info.getFavorTimes();
		viewHolder.tvCollect.setText("(" + String.valueOf(score) + ")");
		// 评论次数
		Long commentTimes = info.getCommentTimes();
		viewHolder.tvComment.setText("(" + String.valueOf(commentTimes) + ")");

		Long downloadTimes = info.getDownloadTimes();
		viewHolder.tvDownloadTimes.setText("(" + String.valueOf(downloadTimes) + ")");

		return convertView;
	}

	public List<AppInfo> getmApps() {
		return mApps;
	}

	public void setmApps(List<AppInfo> mApps) {
		this.mApps = mApps;
		this.notifyDataSetChanged();
	}

	class ViewHolder extends ViewBaseHolder {
		ImageView ivIcon;
		TextView tvAppName;
		TextView tvAppSize;
		ImageView ivShare;
		RatingBar ratinBar;
		TextView tvCollect;
		TextView tvComment;
		TextView tvDownloadTimes;
	}

}
