package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.image.PKIconLoader;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class DownloadAdapter extends BaseAdapter {

	private Activity mContext;
	private List<DownloadJob> mDownloadJobs = new ArrayList<DownloadJob>();
	private ImageResizer mImageWorker;
	private ImageApkIconFetcher apkIconFetcher;
	private ImageCache imageCache;

	public DownloadAdapter(Activity mContext, ImageResizer mImageWorker, ImageApkIconFetcher apkIconFetcher) {
		super();
		this.mContext = mContext;
		this.mImageWorker = mImageWorker;
		this.apkIconFetcher = apkIconFetcher;
	}

	public List<DownloadJob> getmDownloadJobs() {
		return mDownloadJobs;
	}

	public void setmDownloadJobs(List<DownloadJob> mDownloadJobs) {
		this.mDownloadJobs = mDownloadJobs;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mDownloadJobs.size();
	}

	@Override
	public DownloadJob getItem(int position) {
		// TODO Auto-generated method stub
		return mDownloadJobs.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {
		public ImageView resIcon; // 图标
		public TextView fileName; // 名称
		public TextView progress;// 下载百分比
		public TextView uploadTime; // 时间
		public ProgressBar progressBar; // 进度条
		public TextView mBtnPause;
	}

	@Override
	public View getView(int position, View currentView, ViewGroup parent) {
		final DownloadJob downloadJob = getItem(position);
		// >>>>>>>>>如果是下载成功的显示

		ViewHolder viewHolder = null;
		// 获取view
		if (currentView == null) {
			currentView = LayoutInflater.from(mContext).inflate(R.layout.download_item, null);
			viewHolder = new ViewHolder();
			viewHolder.resIcon = (ImageView) currentView.findViewById(R.id.dwownload_logo);
			viewHolder.uploadTime = (TextView) currentView.findViewById(R.id.download_time);
			viewHolder.fileName = (TextView) currentView.findViewById(R.id.download_name);
			viewHolder.progress = (TextView) currentView.findViewById(R.id.download_progress);
			viewHolder.progressBar = (ProgressBar) currentView.findViewById(R.id.dwownload_pb);
			viewHolder.mBtnPause = (TextView) currentView.findViewById(R.id.btn_pause_resume);

			currentView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) currentView.getTag();
		}

		// >>>>>>赋值
		viewHolder.fileName.setText(downloadJob.getName());
		viewHolder.uploadTime.setText(Util.getShowTimes(downloadJob.getDwonTime()));

		// >>>>>>>>>图标下载地址
		String IconUrl = Configuration.getCoverDownload() + downloadJob.getmResID();

		switch (downloadJob.getType()) {
		case PangkerConstant.RES_APPLICATION:

			// >>>>>>>>>获取apk的图标
			mImageWorker.setLoadingImage(R.drawable.icon46_apk);
			mImageWorker.loadImage(IconUrl, viewHolder.resIcon, String.valueOf(downloadJob.getmResID()));
			break;
		case PangkerConstant.RES_MUSIC:
			mImageWorker.setLoadingImage(R.drawable.icon46_music);
			mImageWorker.loadImage(IconUrl, viewHolder.resIcon, String.valueOf(downloadJob.getmResID()));
			break;
		case PangkerConstant.RES_DOCUMENT:
			viewHolder.resIcon.setImageResource(R.drawable.attachment_doc);
			break;
		case PangkerConstant.RES_PICTURE:
			// >>>>>>>>获取图源
			viewHolder.resIcon.setImageResource(R.drawable.icon46_photo);
			break;
		default:
			break;
		}

		// >>>>>>如果下载中
		if (downloadJob.getStatus() == DownloadJob.DOWNLOADING && downloadJob.getmDownloadAsyncTask() != null) {

			initProcessBar(downloadJob, viewHolder.progressBar, viewHolder.progress);
			viewHolder.mBtnPause.setVisibility(View.VISIBLE);
			viewHolder.mBtnPause.setText("暂停");

			viewHolder.mBtnPause.setOnClickListener(new DownloadAction(viewHolder.mBtnPause) {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Log.e("DownloadAsyncTask", ">>>>>>>暂停下载");
					if (downloadJob.getmDownloadAsyncTask() != null) {
						downloadJob.getmDownloadAsyncTask().pause();
						downloadJob.setStatus(DownloadJob.DOWNLOAD_PAUSE);
					}
					getmAction().setText("继续");
				}
			});

		}

		// >>>>>>>下载失败
		else if (downloadJob.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
			viewHolder.progress.setText("下载失败");
			initProcessBar(downloadJob, viewHolder.progressBar, viewHolder.progress);
			viewHolder.mBtnPause.setVisibility(View.VISIBLE);
			viewHolder.mBtnPause.setText("继续");
			viewHolder.mBtnPause.setOnClickListener(new DownloadAction(viewHolder.mBtnPause) {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					handler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							// >>>>>>>>>>重新开启下载任务
							Intent intent = new Intent(mContext, PangkerService.class);
							intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DOWNLOAD);
							// >>>>>>>>>>传递上传jobID到service
							intent.putExtra(DownloadJob.DOWNLOAD_JOB_ID, downloadJob.getId());
							mContext.startService(intent);
							Log.e("DownloadAsyncTask", ">>>>>>>继续下载，停止后继续下载");
							getmAction().setText("暂停");
						}
					});
				}
			});
		}
		// >>>>>>>下载取消
		else if (downloadJob.getStatus() == DownloadJob.DOWNLOAD_CANCEL) {
			viewHolder.progress.setText("取消下载");
			viewHolder.progressBar.setVisibility(View.GONE);
			viewHolder.mBtnPause.setVisibility(View.GONE);
		}
		// >>>>>>>下载成功
		else if (downloadJob.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
			viewHolder.progress.setText("下载成功");
			viewHolder.progressBar.setVisibility(View.GONE);
			viewHolder.mBtnPause.setVisibility(View.GONE);
			// >>>>>>>>如果是图片
			if (downloadJob.getType() == PangkerConstant.RES_PICTURE) {
				// >>>>>>>>设置本地图像
				viewHolder.resIcon.setImageBitmap(ImageUtil.decodeFile(downloadJob.getAbsolutePath(),
						PKIconLoader.iconSideLength, PKIconLoader.iconSideLength * PKIconLoader.iconSideLength));
			} else if (downloadJob.getType() == PangkerConstant.RES_APPLICATION) {
				apkIconFetcher.loadImage(downloadJob.getAbsolutePath(), viewHolder.resIcon,
						downloadJob.getAbsolutePath());
			}
		}
		// >>>>>>>初始化
		else if (downloadJob.getStatus() == DownloadJob.DOWNLOAD_INIT) {
			viewHolder.progress.setText("等待下载");
			viewHolder.progressBar.setVisibility(View.GONE);
		}
		// >>>>>>>暂停下载 （下载任务停止时，下载任务任然存在）
		else if (downloadJob.getStatus() == DownloadJob.DOWNLOAD_PAUSE && downloadJob.getmDownloadAsyncTask() != null) {
			initProcessBar(downloadJob, viewHolder.progressBar, viewHolder.progress);
			viewHolder.mBtnPause.setVisibility(View.VISIBLE);
			viewHolder.mBtnPause.setText("继续");

			DownloadAction(viewHolder.mBtnPause, new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					handler.post(new Runnable() {
						public void run() {
							if (downloadJob.getmDownloadAsyncTask() != null) {
								downloadJob.getmDownloadAsyncTask().resume();
								downloadJob.setStatus(DownloadJob.DOWNLOADING);
							}
							Log.e("DownloadAsyncTask", ">>>>>>>继续下载，暂停后继续下载");
						}
					});

				}
			});

		}
		// >>>>>>>暂停下载 （下载任务结束，下载任务不存在）
		else if (downloadJob.getStatus() == DownloadJob.DOWNLOAD_PAUSE && downloadJob.getmDownloadAsyncTask() == null) {
			initProcessBar(downloadJob, viewHolder.progressBar, viewHolder.progress);
			viewHolder.mBtnPause.setVisibility(View.VISIBLE);
			viewHolder.mBtnPause.setText("继续");
			viewHolder.mBtnPause.setOnClickListener(new DownloadAction(viewHolder.mBtnPause) {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					handler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							// >>>>>>>>>>重新开启下载任务
							Intent intent = new Intent(mContext, PangkerService.class);
							intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DOWNLOAD);
							// >>>>>>>>>>传递上传jobID到service
							intent.putExtra(DownloadJob.DOWNLOAD_JOB_ID, downloadJob.getId());
							mContext.startService(intent);
							Log.e("DownloadAsyncTask", ">>>>>>>继续下载，停止后继续下载");
							getmAction().setText("暂停");
						}
					});
				}
			});
		}

		// >>>>>>>>>>>停止下载
		else if (downloadJob.getStatus() == DownloadJob.DOWNLOAD_STOP) {
			initProcessBar(downloadJob, viewHolder.progressBar, viewHolder.progress);
			viewHolder.mBtnPause.setVisibility(View.VISIBLE);
			viewHolder.mBtnPause.setText("继续");
			viewHolder.mBtnPause.setOnClickListener(new DownloadAction(viewHolder.mBtnPause) {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					handler.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							// >>>>>>>>>>重新开启下载任务
							Intent intent = new Intent(mContext, PangkerService.class);
							intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DOWNLOAD);
							// >>>>>>>>>>传递上传jobID到service
							intent.putExtra(DownloadJob.DOWNLOAD_JOB_ID, downloadJob.getId());
							mContext.startService(intent);
							Log.e("DownloadAsyncTask", ">>>>>>>继续下载，停止后继续下载");
							getmAction().setText("暂停");
						}
					});
				}
			});
		}
		// >>>>>>>>>>>下载异常
		else {
			viewHolder.progress.setText("下载异常");
			viewHolder.progressBar.setVisibility(View.GONE);
		}

		return currentView;
	}

	private void initProcessBar(DownloadJob downloadJob, ProgressBar progressBar, TextView progress) {
		// TODO Auto-generated method stub
		// >>>>>>>>显示下载百分比例
		if (downloadJob.getEndPos() == 0) {
			progress.setText("0/0");
			progressBar.setVisibility(View.VISIBLE);
			progressBar.setProgress(0);
		} else {
			String donesize = Formatter.formatFileSize(mContext, downloadJob.getDoneSize());
			String size = Formatter.formatFileSize(mContext, downloadJob.getEndPos());
			String proString = donesize + "/" + size + "(" + (downloadJob.getDoneSize()) * 100
					/ downloadJob.getEndPos() + "%)";
			progress.setText(proString);
			int percent = (int) (downloadJob.getDoneSize() / (float) downloadJob.getEndPos() * 100);
			progressBar.setVisibility(View.VISIBLE);
			progressBar.setProgress(percent);
		}
	}

	private void DownloadAction(TextView mBtnPause, OnClickListener clickListener) {
		mBtnPause.setOnClickListener(clickListener);
	}

	abstract class DownloadAction implements View.OnClickListener {
		private TextView mAction;

		public DownloadAction(TextView mAction) {
			super();
			this.mAction = mAction;
		}

		public TextView getmAction() {
			return mAction;
		}

		public void setmAction(TextView mAction) {
			this.mAction = mAction;
		}
	}

	private Handler handler = new Handler();
}
