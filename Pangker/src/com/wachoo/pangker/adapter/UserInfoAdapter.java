package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;

/**
 * @author zhengjy
 * 
 *         2012-6-304
 */
public class UserInfoAdapter extends BaseAdapter {

	private List<UserItem> items;
	private Context context;
	private PKIconResizer mImageResizer;

	public UserInfoAdapter(Context context, List<UserItem> items, PKIconResizer mImageResizer) {
		this.items = items;
		this.context = context;
		this.mImageResizer = mImageResizer;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items == null ? 0 : items.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void setUserList(List<UserItem> items) {
		this.items = items;
		this.notifyDataSetChanged();
	}

	class ViewHoler {
		TextView userName;
		TextView sign;
		ImageView usericon;
	}

	ViewHoler holer = null;

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		// TODO Auto-generated method stub
		final UserItem item = items.get(position);
		if (v == null) {
			holer = new ViewHoler();
			v = LayoutInflater.from(context).inflate(R.layout.contacts_address_item, null);
			holer.userName = (TextView) v.findViewById(R.id.tv_username);
			holer.sign = (TextView) v.findViewById(R.id.tv_usersign);
			holer.usericon = (ImageView) v.findViewById(R.id.iv_usericon);
			v.setTag(holer);
		} else {
			holer = (ViewHoler) v.getTag();
		}
		holer.userName.setText(item.getUserName());
		holer.sign.setText(item.getSign());

		mImageResizer.loadImage(item.getUserId(), holer.usericon);
		holer.usericon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, item.getUserId());
				intent.putExtra(UserItem.USERNAME, item.getUserName());
				intent.putExtra(UserItem.USERSIGN, item.getSign());
				context.startActivity(intent);
			}
		});

		return v;
	}

	public List<UserItem> getItems() {
		return items;
	}

	public void setItems(List<UserItem> items) {
		this.items = items;
		this.notifyDataSetChanged();
	}

}
