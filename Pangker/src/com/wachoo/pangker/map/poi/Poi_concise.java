package com.wachoo.pangker.map.poi;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Poi_concise implements Serializable, Comparable<Poi_concise> {
	private String id;
	private String category_id;
	private String name;
	private String address;
	private String lon;
	private String lat;
	private String phone;
	private String country;
	private String city;
	private String province;
	private String area;
	private int distance;

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public Poi_concise() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public Poi_concise(String id, String category_id, String name,
			String address, String lon, String lat, String phone,
			String country, String city, String province) {
		super();
		this.id = id;
		this.category_id = category_id;
		this.name = name;
		this.address = address;
		this.lon = lon;
		this.lat = lat;
		this.phone = phone;
		this.country = country;
		this.city = city;
		this.province = province;
        this.area = "";
	}

	@Override
	public int compareTo(Poi_concise another) {
		return distance > another.getDistance() ? 1 : 0;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{").append("\"poiId\":\"").append(id).append("\",\"poiCategory\":\"").append(category_id)
		  .append("\",\"poiName\":\"").append(name).append("\",\"poiAddress\":\"").append(address).append("\",\"poiLat\":\"")
		  .append(lat).append("\",\"poiLon\":\"").append(lon).append("\",\"poiPhone\":\"").append(phone).append("\",\"poiCountry\":\"")
		  .append(country).append("\",\"poiCity\":\"").append(city).append("\",\"poiProvince\":\"").append(province).append("\",\"poiArea\":\"").append(area).append("\"}");
		return sb.toString();
		// TODO Auto-generated method stub
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

}
