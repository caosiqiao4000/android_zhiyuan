package com.wachoo.pangker.map.poi;


/**
 * 使用驴博士查询Geo的response
 * 
 * @author wangxin
 * 
 */
public class Geo_Search_Response extends Search_Response{
	private Geo_info geoinfo;

	public Geo_info getGeoinfo() {
		return geoinfo;
	}

	public void setGeoinfo(Geo_info geoinfo) {
		this.geoinfo = geoinfo;
	}

}
