package com.wachoo.pangker.map.poi;

public abstract class Search_Response {
	public boolean isError;
	
	public void setError(boolean isError) {
		this.isError = isError;
	}
	
	public boolean isError() {
		return isError;
	}
	
}
