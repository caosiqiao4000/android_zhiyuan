package com.wachoo.pangker.map.poi;

import java.util.List;

/**
 * 使用驴博士查询poi的response
 * 
 * @author wangxin
 * 
 */
public class Poi_Search_Response extends Search_Response{
	private List<Poi_concise> pois;

	public List<Poi_concise> getPois() {
		return pois;
	}

	public void setPois(List<Poi_concise> pois) {
		this.pois = pois;
	}
}
