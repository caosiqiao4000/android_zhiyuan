package com.wachoo.pangker.map.poi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.widget.TextView;

import com.wachoo.pangker.db.IDetailedAddress;
import com.wachoo.pangker.db.impl.DetailedAddressImpl;
import com.wachoo.pangker.entity.DetailedAddress;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.util.Util;

public class PoiaddressLoader {

	private final String slpit = " 邮政编码";
	public static final String geourl = "http://maps.google.com/maps/api/geocode/json?latlng=%s,%s&language=zh-CN&sensor=false";

	private AtomicBoolean mStopped = new AtomicBoolean(Boolean.FALSE);

	private ThreadPoolExecutor mQueue;

	private Context mContext;
	private IDetailedAddress iDetailedAddress;

	public PoiaddressLoader(Context mContext) {
		this.mContext = mContext;
		this.iDetailedAddress = new DetailedAddressImpl(this.mContext);
		mQueue = new ThreadPoolExecutor(10, 10, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(),
				sThreadFactory);
	}

	private Runnable runnable;

	public void stopLoader() {
		// TODO Auto-generated method stub
		if (runnable != null) {
			new ThreadPoolExecutor.DiscardOldestPolicy().rejectedExecution(runnable, mQueue);
		}
	}

	public String responserString(String urlString) throws IOException {
		URL url = new URL(urlString);
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxy.lizongbo.com", 8080));
		proxy = Proxy.NO_PROXY;
		HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection(proxy);
		httpUrlConnection.setDoOutput(false);
		httpUrlConnection.setDoInput(true);
		httpUrlConnection.setUseCaches(false);
		httpUrlConnection.setRequestProperty("Content-type", "application/x-java-serialized-object");
		httpUrlConnection.setRequestMethod("GET");
		httpUrlConnection.setConnectTimeout(5000);
		httpUrlConnection.connect();

		StringBuffer sb = new StringBuffer();
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream(), "UTF-8"));
		String lines;
		while ((lines = reader.readLine()) != null) {
			sb.append(lines);
		}
		reader.close();
		// 断开连接
		httpUrlConnection.disconnect();
		return sb.toString();
	}

	private String removeZip(String address) {
		// TODO Auto-generated method stub
		if (address.contains(slpit)) {
			address = address.substring(0, address.indexOf(slpit));
		}
		return address;
	}

	private String getAddressTypes(JSONObject jsonObject) {
		try {
			return jsonObject.getJSONArray(DetailedAddress.TYPES).get(0).toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	private String getAddressLongName(JSONObject jsonObject) {
		try {
			return jsonObject.getString(DetailedAddress.LONG_NAME);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 获取地理位置
	 * 
	 * @throws Exception
	 */
	private DetailedAddress getDetailedAddressByLocation(Location location, Activity activity) {
		DetailedAddress detailedAddress = new DetailedAddress();
		/** 这里采用get方法，直接将参数加到URL上 */
		String urlString = String.format(geourl, location.getLatitude(), location.getLongitude());
		/** 新建HttpClient */
		try {
			String strResult = responserString(urlString);
			/** 解析JSON数据，获得物理地址 */
			if (strResult != null && strResult.length() > 0) {
				JSONObject jsonobject = new JSONObject(strResult);
				// >>>>>>获取结果
				JSONArray jsonArray = new JSONArray(jsonobject.get("results").toString());

				// >>>>详细地址
				JSONObject detailJsonObject = jsonArray.getJSONObject(0);
				// >>>获取详细属性
				JSONArray addressComponents = detailJsonObject.getJSONArray("address_components");

				// 获取详细地址
				String addressStreetZip = detailJsonObject.getString("formatted_address");
				detailedAddress.setAddressStreetZip(addressStreetZip);
				detailedAddress.setAddressStreet(removeZip(addressStreetZip));

				for (int i = 0; i < addressComponents.length(); i++) {
					JSONObject object = addressComponents.getJSONObject(i);
					String types = getAddressTypes(object).toLowerCase();
					String longName = getAddressLongName(object);
					// >>>>>街道
					if (DetailedAddress.ROUTE.equals(types)) {
						detailedAddress.setStreet(longName);
					}
					// >>>>>区县
					if (DetailedAddress.SUBLOCALITY.equals(types)) {
						detailedAddress.setArea(longName);
					}
					// >>>>>城市
					if (DetailedAddress.LOCALITY.equals(types)) {
						detailedAddress.setCity(longName);
					}
					// >>>>>省份
					if (DetailedAddress.ADMINISTRATICE_AREA_LEVEL_1.equals(types)) {
						detailedAddress.setProvince(longName);
					}
					// >>>>>国家
					if (DetailedAddress.COUNTRY.equals(types)) {
						detailedAddress.setCountry(longName);
					}
				}
			}
		} catch (Exception e) {
		} finally {
			// >>>>>>保存
			if (!Util.isEmpty(detailedAddress.getAddressProvinceCityArea())
					&& !detailedAddress.getAddressProvinceCityArea().contains(DetailedAddress.ADDRESS_UN_KNOW)) {
				detailedAddress.setId(location.toResString());
				iDetailedAddress.saveDitailedAddress(detailedAddress);
			}
		}

		return detailedAddress;
	}

	/**
	 * 获取完整的地址
	 * 
	 * @param location
	 * @param activity
	 * @param textView
	 */
	public void onPoiLoader(final Location location, final Activity activity, final TextView textView) {
		DetailedAddress detailedAddress = iDetailedAddress.getDetailedAddress(location.toResString());

		if (detailedAddress != null) {
			textView.setText(detailedAddress.getAddressStreet());
		} else {
			textView.setText("正在加载地址...");
			textView.setTag(location.toResString());
			runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					final DetailedAddress detailedAddress = getDetailedAddressByLocation(location, activity);
					setTextView(detailedAddress.getAddressStreet(), activity, textView, location.toResString());
				}
			};
			mQueue.execute(runnable);
		}
	}

	/**
	 * 获取地址的城市和区县
	 * 
	 * @param location
	 * @param activity
	 * @param textView
	 */
	public void onAddressCityAreaLoad(final Location location, final Activity activity, final TextView textView) {
		DetailedAddress detailedAddress = iDetailedAddress.getDetailedAddress(location.toResString());
		if (detailedAddress != null) {
			textView.setText(detailedAddress.getAddressCityArea());
		} else {
			textView.setText("正在加载地址...");
			textView.setTag(location.toResString());
			runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					final DetailedAddress detailedAddress = getDetailedAddressByLocation(location, activity);
					setTextView(detailedAddress.getAddressCityArea(), activity, textView, location.toResString());
				}
			};
			mQueue.execute(runnable);
		}
	}

	/**
	 * 获取地址的城市和区县
	 * 
	 * @param location
	 * @param activity
	 * @param textView
	 */
	public void onAddressCityChangedLoad(final Location location, final Activity activity, final TextView textView,
			final TextView changedCity) {
		DetailedAddress detailedAddress = iDetailedAddress.getDetailedAddress(location.toResString());
		if (detailedAddress != null) {
			textView.setText(detailedAddress.getAddressCityArea());
			changedCity.setText(detailedAddress.getAddressCityChanged());
		} else {
			textView.setText("正在加载地址...");
			textView.setTag(location.toResString());
			changedCity.setTag(location.toResString());
			runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					final DetailedAddress detailedAddress = getDetailedAddressByLocation(location, activity);
					setTextView(detailedAddress.getAddressCityArea(), activity, textView, location.toResString());
					setTextView(detailedAddress.getAddressCityChanged(), activity, changedCity, location.toResString());
				}
			};
			mQueue.execute(runnable);
		}
	}

	/**
	 * 
	 * @param location
	 * @param activity
	 * @param geoCallback
	 */
	public void onPoiLoader(final Location location, final Activity activity, final GeoCallback geoCallback) {
		DetailedAddress detailedAddress = iDetailedAddress.getDetailedAddress(location.toResString());
		if (detailedAddress != null) {
			callbacks(activity, detailedAddress.getAddressStreet(), geoCallback);
		} else {
			runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					final DetailedAddress detailedAddress = getDetailedAddressByLocation(location, activity);
					callbacks(activity, detailedAddress.getAddressStreet(), geoCallback);
				}
			};
			mQueue.execute(runnable);
		}
	}

	/**
	 * onPoiLoader
	 * 
	 * @param location
	 * @param activity
	 * @param geoCallback
	 * @param name
	 */
	public void onPoiLoader(final Location location, final Activity activity, final GeoCallback geoCallback,
			final String name) {
		DetailedAddress detailedAddress = iDetailedAddress.getDetailedAddress(location.toResString());
		if (detailedAddress != null) {
			callbacks(activity, detailedAddress.getAddressStreet(), geoCallback);
		} else {
			runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					final DetailedAddress detailedAddress = getDetailedAddressByLocation(location, activity);
					callbacks2(activity, detailedAddress.getAddressStreet(), geoCallback, name);
				}
			};
			mQueue.execute(runnable);
		}
	}

	private void setTextView(final String address2, Activity activity, final TextView textView, String id) {
		// TODO Auto-generated method stub
		Object tag = textView.getTag();
		if (tag.equals(id)) {
			if (activity != null) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						textView.setText(address2);
					}
				});
			}
		}
	}

	public void stopThread() {
		if (!mStopped.get()) {
			mQueue.shutdownNow();
			mStopped.set(Boolean.TRUE);
		}
	}

	private static final ThreadFactory sThreadFactory = new ThreadFactory() {
		private final AtomicInteger mCount = new AtomicInteger(1);

		@Override
		public Thread newThread(Runnable r) {
			return new Thread(r, "PoiLoader #" + mCount.getAndIncrement());
		}
	};

	public interface GeoCallback {
		void onGeoLoader(String address, int code);
	}

	private void callbacks(Activity activity, final String address, final GeoCallback geoCallback) {
		// TODO Auto-generated method stub
		if (activity != null) {
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (geoCallback != null) {
						geoCallback.onGeoLoader(address, 1);
					}
				}
			});
		}
	}

	private void callbacks2(Activity activity, final String address, final GeoCallback geoCallback, final String name) {
		// TODO Auto-generated method stub
		if (activity != null) {
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (geoCallback != null) {
						geoCallback.onGeoLoader(name + "在:" + address, 1);
					}
				}
			});
		}
	}
}
