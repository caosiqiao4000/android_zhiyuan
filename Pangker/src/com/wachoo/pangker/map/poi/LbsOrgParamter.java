package com.wachoo.pangker.map.poi;

public class LbsOrgParamter {
	private LBSTYPE type;

	public LBSTYPE getType() {
		return type;
	}

	public void setType(LBSTYPE type) {
		this.type = type;
	}

	public LBS_Request getLbs_Request() {
		return lbs_Request;
	}

	public void setLbs_Request(LBS_Request lbsRequest) {
		lbs_Request = lbsRequest;
	}

	public enum LBSTYPE {
		POI, ADDRESS;
	}

	private LBS_Request lbs_Request;

	public LbsOrgParamter(LBSTYPE poi, LBS_Request lbs_Request) {
		// TODO Auto-generated constructor stub
		this.type = poi;
		this.lbs_Request = lbs_Request;
	}
	
	
}
