package com.wachoo.pangker.map.poi;

/**
 * 使用驴博士查询poi的request
 * 
 * @author wangxin
 * 
 */
public class Geo_Search_Request extends LBS_Request {
	/**
	 * <locationinfo ver='2' key='f6b5d78c0a175ec2c0c9dfb99be5a53f'> <location
	 * lat='23.06001' lon='113.401819'/> </locationinfo>;
	 * 
	 */
	@Override
	public String getLbsSearchReques() {
		StringBuilder requesBuilder = new StringBuilder("<locationinfo ");
		requesBuilder.append("ver='" + getVer() + "' ");
		requesBuilder.append("key='" + getKey() + "'>");
		requesBuilder.append("<location lat='" + getLat());
		requesBuilder.append("' lon='" + getLon() + "'/>");
		requesBuilder.append("</locationinfo>");
		return requesBuilder.toString();
	}

	public Geo_Search_Request(double lon, double lat) {

		this.lon = lon;
		this.lat = lat;
	}

}
