package com.wachoo.pangker.map.app;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.Overlay;
import com.wachoo.pangker.entity.UserItem;

public class TracesManager {

	private List<TraceOverlay> overlays;
	private MapView mapview;
	
	public TracesManager(MapView mapview) {
		// TODO Auto-generated constructor stub
		this.mapview = mapview;
		overlays = new ArrayList<TraceOverlay>();
	}
	
	public void deleteTraceOverlay(TraceOverlay traceOverlay){
		List<Overlay> mapOverlays = mapview.getOverlays();
		mapOverlays.remove(traceOverlay);
	}
	
	public void addTraceOverlay(TraceOverlay traceOverlay){
		if(overlays != null){
			overlays.add(traceOverlay);
			List<Overlay> mapOverlays = mapview.getOverlays();
			mapOverlays.add(traceOverlay);
		}
	}
	
	public void deleteTraceOverlay(String userId){
		TraceOverlay overlay = getOverlayById(userId);
		if(overlays.contains(overlay)){
			overlays.remove(overlay);
			List<Overlay> mapOverlays = mapview.getOverlays();
			mapOverlays.remove(overlay);
		}
	}
	
	private TraceOverlay getOverlayById(String Id){
		TraceOverlay traceOverlay = null;
		Iterator<TraceOverlay> iterator = overlays.iterator();
		while(iterator.hasNext()){
//			TraceOverlay overlay = iterator.next();
//			if(Id.equals(overlay.getUser().getUID())){
//				traceOverlay = overlay;
//				break;
//			}
		}
		return traceOverlay;
	}
	
	public void addLocation(String msgFrom, String msgBody){
		TraceOverlay traceOverlay = getOverlayById(msgFrom);
		if(traceOverlay != null){
			//traceOverlay.addLocation(MapUtil.toGeoPoint(msgBody));
		}
	}
	
	public void selectOverlay(UserItem userItem) {
		// TODO Auto-generated method stub
		TraceOverlay traceOverlay = getOverlayById(userItem.getUserId());
		selectOverlay(traceOverlay);
	}
	
	public void selectOverlay(TraceOverlay traceOverlay){
		if(!overlays.contains(traceOverlay)){
			return;
		}
		for (int i = 0; i < overlays.size(); i++) {
			//overlays.get(i).setSelected(false);
		}
		//traceOverlay.setSelected(true);
	}
	
}
