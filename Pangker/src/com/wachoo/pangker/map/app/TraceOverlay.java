package com.wachoo.pangker.map.app;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.core.OverlayItem;
import com.amap.mapapi.map.ItemizedOverlay;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.Projection;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.util.MapUtil;

public class TraceOverlay extends ItemizedOverlay<OverlayItem> {

	private List<OverlayItem> geoList = new ArrayList<OverlayItem>();
	private Drawable marker;
	private Activity mContext;
	private PoiaddressLoader poiLoader;

	public TraceOverlay(Drawable marker, List<OverlayItem> geoList, Activity context) {
		super(boundCenterBottom(marker));
		this.marker = marker;
		this.mContext = context;
		this.geoList = geoList;
		poiLoader = new PoiaddressLoader(mContext.getApplicationContext());
		populate(); // createItem(int)方法构造item。一旦有了数据，在调用其它方法前，首先调用这个方法
	}

	public void setGeoList(List<OverlayItem> geoList) {
		this.geoList = geoList;
		populate();
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		// Projection 接口用于屏幕像素点坐标系统和地球表面经纬度点坐标系统之间的变换
		Projection projection = mapView.getProjection();
		for (int index = size() - 1; index >= 0; index--) { // 遍历GeoList
			OverlayItem overLayItem = getItem(index); // 得到给定索引的item
			String title = overLayItem.getTitle();

			// 把经纬度变换到相对于MapView 左上角的屏幕像素坐标
			Point point = projection.toPixels(overLayItem.getPoint(), null);
			// 可在此处添加您的绘制代码
			Paint paintText = new Paint();
			paintText.setTextSize(18);
			if (title != null && !title.equals("")) {
				String uid = overLayItem.getSnippet();
				if (uid != null && uid.equals(((PangkerApplication) mContext.getApplicationContext()).getMyUserID())) {
					paintText.setColor(Color.RED);
				} else {
					paintText.setColor(Color.BLUE);
				}
				canvas.drawText(title, point.x - 30, point.y - marker.getIntrinsicHeight(), paintText); // 绘制文本
			}
		}
		super.draw(canvas, mapView, shadow);
		// 调整一个drawable 边界，使得（0，0）是这个drawable 底部最后一行中心的一个像素
		boundCenterBottom(marker);
	}

	@Override
	protected OverlayItem createItem(int i) {
		// TODO Auto-generated method stub
		return geoList.get(i);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return geoList.size();
	}

	@Override
	// 处理当点击事件
	protected boolean onTap(int i) {
		setFocus(geoList.get(i));
		poiLoader.onPoiLoader(getLocation(i), mContext, iCallback, geoList.get(i).getTitle());
		return true;
	}

	private Location getLocation(int index) {
		// TODO Auto-generated method stub
		Location location = MapUtil.toLocation(geoList.get(index).getPoint());
		return location;
	}

	@Override
	public boolean onTap(GeoPoint point, MapView mapView) {
		// TODO Auto-generated method stub
		return super.onTap(point, mapView);
	}

	PoiaddressLoader.GeoCallback iCallback = new PoiaddressLoader.GeoCallback() {
		@Override
		public void onGeoLoader(String address, int err) {
			// TODO Auto-generated method stub
			Toast.makeText(mContext, address, Toast.LENGTH_SHORT).show();
		}
	};
}
