package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.UserItem;

public interface IBlacklistDao {
	
	List<UserItem> getBlacklist();
	
	void insertUser(String memberUid);
	
	void removeUser(String memberUid);
	
	boolean isExist(String memberUid);
	
	void saveBlacklist(List<UserItem> blacklists);
	
	boolean insertBlacklist(UserItem user,MyDataBaseAdapter adapter);
	
	boolean isExist(String memberUid,MyDataBaseAdapter adapter);
}
