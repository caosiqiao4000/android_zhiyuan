package com.wachoo.pangker.db;

public interface IAreaDao {

	public String getAreaIDByName(String areaName);
	
	
	public String getAreaNameByID(String areaID);
}
