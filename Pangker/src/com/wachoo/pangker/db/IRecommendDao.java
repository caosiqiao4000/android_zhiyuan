package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.RecommendInfo;

public interface IRecommendDao {

	/**
     * 记录一条消息
     * @param DirInfo
     * @return
     */
	public long saveRecommendInfo(RecommendInfo recommendInfo);
	/**
	 * 获取自己收到的推荐信息
	 * @param userId
	 * @return
	 */
	public List<RecommendInfo> getRecommendInfos(String userId, boolean isAccept, int page);
	/**
	 * 修改一条推荐信息，状态
	 * @param recommendInfo
	 * @return
	 */
	public boolean updateRecommendInfo(RecommendInfo recommendInfo);
	/**
	 * 根据id删除一条推荐信息
	 * @param id
	 * @return
	 */
	public boolean deleteRecommendInfo(int id);
	/**
	 * 删除用户收到的所有推荐消息 
	 * @param userId
	 * @return
	 */
	public boolean truncRecommendInfo(String userId);
	/**
	 * 获取记录的总条数
	 * @param userId
	 * @param isAccept,true:接受，false:发送
	 * @return
	 */
	public int getRecommendInfoCount(String userId, boolean isAccept);
	
	/**
	 * 批量保存推荐资源
	 * 
	 * @param addressBookLists
	 */
	public void saveRecommends(String userId,List<RecommendInfo> mRecommendList);
	
	
	public boolean isExist(String resId, MyDataBaseAdapter adapter);
}
