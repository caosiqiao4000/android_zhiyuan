package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.UserItem;

/**
 * 
 * attents db manager
 * 
 * @author wangxin
 * 
 */
public interface IAttentsDao {

	/**
	 * saveAttent
	 * 
	 * @param user
	 * @param groupid
	 */
	boolean addAttent(UserItem user, String groupid);
	
	/**
	 * 
	 * 
	 * 
	 * @param user
	 * @param groupid
	 * @param adapter
	 * @return
	 */
	boolean addAttent(UserItem user, String groupid,MyDataBaseAdapter adapter);

	/**
	 * 
	 * delAttent
	 * 
	 * @param user
	 * @param groupid
	 */
	boolean delAttent(UserItem user, String groupid);
	
	
	
	/**
	 * 
	 * delAttent
	 * 
	 * @param user
	 * @param groupid
	 * @param adapter
	 * @return
	 */
	boolean delAttent(UserItem user, String groupid,MyDataBaseAdapter adapter);
	
	public boolean delAttent(String attendId);
	
	/**
	 * getAllAttents(distinct)
	 * @return
	 */
	List<UserItem> getAllAttents();
	
	/**
	 * 通过分组Id获取关注人
	 * @param groupId
	 * @return
	 */
	public List<UserItem> getAttentsByGroupId(String groupId);

	public boolean refreshFriends(List<ContactGroup> list);
}
