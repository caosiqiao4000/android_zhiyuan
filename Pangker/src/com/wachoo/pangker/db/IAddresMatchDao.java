package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.server.response.PUser;

public interface IAddresMatchDao {

	public void insertPUser(PUser pUser);

	public void deletePUser(String uid);

	public void notifyPUsers(List<PUser> pUsers);
}
