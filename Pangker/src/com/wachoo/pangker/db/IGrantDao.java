package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.Grant;

/**
 * @author wubo
 * @createtime May 7, 2012
 */
public interface IGrantDao {

	public boolean saveGrant(Grant grant);

	/**
	 * 初始化权限设置
	 * 
	 * @author wubo
	 * @createtime May 7, 2012
	 * @param grants
	 */
	public boolean saveGrants(List<Grant> grants);

	/**
	 * 更新权限设置
	 * 
	 * @author wubo
	 * @createtime May 7, 2012
	 * @param grant
	 */
	public boolean updateGrant(String uid, String name, int value);

	/**
	 * 查询权限
	 * 
	 * @author wubo
	 * @createtime May 7, 2012
	 * @param uid
	 * @param name
	 */
	public Grant getGrantValue(String uid, String name);

	/**
	 * 获取全部权限信息
	 * 
	 * @author wubo
	 * @createtime May 8, 2012
	 * @param uid
	 * @return
	 */
	public List<Grant> getGrantValues(String uid);

	/**
	 * 分组权限
	 * 
	 * @author wubo
	 * @createtime 2012-8-15
	 * @param uid
	 * @param name
	 * @param value
	 * @param gid
	 */
	public void updateGrantGid(String uid, String name, int value, String gids);

	/**
	 * 联系人权限
	 * 
	 * @author wubo
	 * @createtime 2012-8-15
	 * @param uid
	 * @param name
	 * @param value
	 * @param gid
	 */
	public void updateGrantUid(String uid, String name, int value, String uids);

}
