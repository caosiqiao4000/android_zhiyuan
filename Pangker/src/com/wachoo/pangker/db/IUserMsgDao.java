package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.ChatMessage;

/**
 * 私信管理
 * 
 * @author wubo
 * @createtime 2011-12-6 上午10:29:19
 */
public interface IUserMsgDao {

	/**
	 * 获取私信
	 * 
	 * @author wubo
	 * @createtime 2011-12-6 上午10:32:16
	 * @param myuid
	 *            登录用户uid
	 * @param himuid
	 *            聊天对象uid
	 * @return
	 */
	public List<ChatMessage> getUserMsgs(String myuid, String himuid);

	/**
	 * 根据区间获取私信聊天信息
	 * 
	 * @param myuid
	 * @param himuid
	 * @param startLimit
	 * @param length
	 * @return
	 */
	public List<ChatMessage> getUserMsgsByLimit(String myuid, String himuid, int startLimit, int length);

	public List<ChatMessage> getUserMsgsByLimit(String myuid, String himuid, int length);

	public int getMsgCount(String myuid, String himuid);

	/**
	 * 添加私信
	 * 
	 * @author wubo
	 * @createtime 2011-12-6 上午10:34:50
	 * @param msg
	 */
	public long saveUserMsg(ChatMessage msg);

	/**
	 * 根据私信ID删除私信
	 * 
	 * @author wubo
	 * @createtime 2011-12-6 上午10:36:38
	 * @param id
	 * @return
	 */
	public boolean deleteUserMsgById(int id);

	/**
	 * 删除与某用户的私信
	 * 
	 * @author wubo
	 * @createtime 2011-12-6 上午10:38:22
	 * @param myuid
	 *            登录用户uid
	 * @param himuid
	 *            聊天对象uid
	 * @return
	 */
	public boolean deleteUserMsgByUid(String myuid, String himuid);

	/**
	 * 查找所以聊天对象
	 * 
	 * @author wubo
	 * @createtime 2011-12-7 下午04:37:12
	 * @param uid
	 * @return
	 */
	public List<ChatMessage> getUserMessages();

	/**
	 * 修改信息状态，发送接收是否成功！
	 * 
	 * @param msg
	 */
	public boolean updateUserMsgStatus(ChatMessage msg);

	public boolean messageReceived(String messageID);

	public boolean updateMessageReceived(ChatMessage message);

}
