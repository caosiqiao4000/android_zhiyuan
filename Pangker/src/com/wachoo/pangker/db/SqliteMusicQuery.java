package com.wachoo.pangker.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.Media;
import android.util.Log;

import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.util.Util;

/**
 * MediaStore
 * 搜索音乐相关信息
 * @author Administrator
 *Cursor cursor = db.rawQuery("select * from person where personid=?", 
 *new String[]{String.valueOf(id)});
 *Cursor cursor = db.query("person", new String[]{"personid","name","phone"}, 
				"personid=?", new String[]{String.valueOf(id)}, null, null, null);
 */
public class SqliteMusicQuery {
	
	private final String[] OUT_FORMAT = {"m4a", ".amr", ".3gp"};
	
	
	private Context mContext;
	private final Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
	private String[] columns;
	private ContentResolver cr;

	public SqliteMusicQuery(Context cnt) {
		mContext = cnt;
		cr = mContext.getContentResolver();
	}
	
	public boolean delMusic(int id){
		return cr.delete(uri, MediaStore.Audio.Media._ID + "=?", new String[]{String.valueOf(id)}) > 0;
	}

	//搜索唱片集信息
	public List<String> getAlbumList() {
		columns = new String[] { "DISTINCT " + MediaStore.Audio.Media.ALBUM};
		Cursor cursor = cr.query(
				uri,columns, null, null, null);
		List<String> albums = new ArrayList<String>();
		if(cursor != null){
			while (cursor.moveToNext()) {
				String album = cursor.getString(cursor
						.getColumnIndex(MediaStore.Audio.AudioColumns.ALBUM));
				albums.add(album);
			}
			cursor.close();
		}
		return albums;
	}

	//搜索所有歌手信息
	public List<String> getSingerList() {
		columns = new String[]{ "DISTINCT " + MediaStore.Audio.Media.ARTIST};
		Cursor cursor = cr.query(
				uri,columns, null, null, null);
		List<String> singers = new ArrayList<String>();
		if(cursor != null){
			while (cursor.moveToNext()) {
				String singer = cursor.getString(cursor
						.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST));
				singers.add(singer);
			}
			cursor.close();
		}
		return singers;
	}
	
	public boolean existMusic(String path){
		columns = new String[]{ "DISTINCT " + MediaStore.Audio.Media._ID};
		Cursor cursor = null;
		try {
			cursor = cr.query(uri,columns, MediaStore.Audio.Media.DATA + "=?", new String[]{String.valueOf(path)}, null);
			if(cursor != null){
				return cursor.getCount() > 0;
			}
		} catch (Exception e) {
			if(cursor != null){
				cursor.close();
			}
		}
		
		return false;
	}
	
	private boolean infliteFormat(String path) {
		// TODO Auto-generated method stub
        if(Util.isEmpty(path)){
        	return true;
        }
        for (String format : OUT_FORMAT) {
        	if(path.endsWith(format)){
            	return true;
            }
		}
        return false;
	}
	/**
	 * @param musicId 本地音乐资源
	 * @return 获取播放长度
	 */
	public int getDruation(int musicId){
		Cursor cursor = null;
		if(musicId > 0){
			try {
				StringBuilder where = new StringBuilder();
				where.append(MediaStore.Audio.Media._ID + " = ?");
				cursor = mContext.getContentResolver().query(
						MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, where.toString(), new String[] {String.valueOf(musicId) }, null);
				if(cursor != null && cursor.getCount() == 1){
					cursor.moveToFirst();
					int druation = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION));
					return druation;
				}
			} catch (Exception e) {
				return 0;
			} finally {
				if(cursor != null){
					cursor.close();
				}
			}
		}
		return 0;
	}
	
	//搜索歌曲信息
	public List<MusicInfo> getMusicList() {
		/**
		 * 1、歌曲id， 2、歌曲名， 
		 * 3、歌手， 4、专辑（专辑图片，专辑名称，专辑ID[用来获取图片])， 
		 * 5、歌曲大小 6.歌曲时间 7.歌曲路径
		 */
		columns = new String[] { "DISTINCT " +
				MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
						MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM,
						MediaStore.Audio.Media.SIZE, MediaStore.Audio.Media.DURATION,
						MediaStore.Audio.Media.DATA };
		
		Cursor cursor = cr.query(uri,columns, null, null, null);
		
		List<MusicInfo> musicList = new ArrayList<MusicInfo>();
		if(cursor != null){
			while (cursor.moveToNext()) {
				String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA));
			    if(infliteFormat(path)){
				    continue;
				}
			    MusicInfo mInfo = new MusicInfo();
				mInfo.setMusicId(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns._ID)));
				mInfo.setMusicName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE)));
				mInfo.setSinger(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST)));
				mInfo.setMusicAlubm(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ALBUM)));
				mInfo.setMusicSize(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.SIZE)));
				mInfo.setDuration(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION)));
				mInfo.setMusicPath(path);
				Log.d("MusicInfo", path);
				musicList.add(mInfo);
			}
			cursor.close();
		}
		return musicList;
	}
	
	//按指定条件搜索歌曲信息--歌手
	public List<MusicInfo> getMusicListBySinger(String condition) {
		StringBuilder where = new StringBuilder();
		where.append(MediaStore.Audio.Media.ARTIST + " like ?");
		/**
		 * 1、歌曲id， 2、歌曲名， 
		 * 3、歌手， 4、专辑（专辑图片，专辑名称，专辑ID[用来获取图片])， 
		 * 5、歌曲大小 6.歌曲时间 7.歌曲路径
		 */
		columns = new String[] { "DISTINCT " +
				MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
						MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM,
						MediaStore.Audio.Media.SIZE, MediaStore.Audio.Media.DURATION,
						MediaStore.Audio.Media.DATA };
		
		Cursor cursor = mContext.getContentResolver().query(
				Media.EXTERNAL_CONTENT_URI, null, where.toString(),
				new String[] { "%"+condition.toString()+"%" }, null);
		
		List<MusicInfo> musicList = new ArrayList<MusicInfo>();
		MusicInfo mInfo;
		if(cursor != null){
			while (cursor.moveToNext()) {
				String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA));
			    if(infliteFormat(path)){
				    continue;
				}
				mInfo = new MusicInfo();
				mInfo.setMusicId(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns._ID)));
				mInfo.setMusicName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE)));
				mInfo.setSinger(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST)));
				mInfo.setMusicAlubm(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ALBUM)));
				mInfo.setMusicSize(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.SIZE)));
				mInfo.setDuration(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION)));
				mInfo.setMusicPath(path);
			    musicList.add(mInfo);
			}
			cursor.close();
		}
		return musicList;
	}
	
	//按指定条件搜索歌曲信息--唱片集
	public List<MusicInfo> getMusicListByAlbum(String condition) {
		StringBuilder where = new StringBuilder();
		where.append(MediaStore.Audio.Media.ALBUM + " like ?");
		/**
		 * 1、歌曲id， 2、歌曲名， 
		 * 3、歌手， 4、专辑（专辑图片，专辑名称，专辑ID[用来获取图片])， 
		 * 5、歌曲大小 6.歌曲时间 7.歌曲路径
		 */
		columns = new String[] { "DISTINCT " +
				MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
				MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM,
				MediaStore.Audio.Media.SIZE, MediaStore.Audio.Media.DURATION,
				MediaStore.Audio.Media.DATA };
		
		Cursor cursor = mContext.getContentResolver().query(
				Media.EXTERNAL_CONTENT_URI, null, where.toString(),
				new String[] { "%"+condition.toString()+"%" }, null);
		
		List<MusicInfo> musicList = new ArrayList<MusicInfo>();
		MusicInfo mInfo;
		if(cursor != null){
		    while (cursor.moveToNext()) {
			    mInfo = new MusicInfo();
			    mInfo.setMusicId(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns._ID)));
			    mInfo.setMusicName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE)));
			    mInfo.setSinger(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST)));
			    mInfo.setMusicAlubm(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ALBUM)));
		      	mInfo.setMusicSize(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.SIZE)));
		    	mInfo.setDuration(cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION)));
		    	mInfo.setMusicPath(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA)));
			    musicList.add(mInfo);
		    }
		    cursor.close();
		}
		return musicList;
	}
}
