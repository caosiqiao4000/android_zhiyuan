package com.wachoo.pangker.db;


/**
 * chat 会话历史记录
 * 
 * @author wangxin
 * 
 */
public class RecentContact {
	private int id;
	private String me_uid;
	private String him_uid;
	
	private Integer count;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMe_uid() {
		return me_uid;
	}

	public void setMe_uid(String me_uid) {
		this.me_uid = me_uid;
	}

	public String getHim_uid() {
		return him_uid;
	}

	public void setHim_uid(String him_uid) {
		this.him_uid = him_uid;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
