package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.server.response.UserStatus;


/**
 * 
 * @TODO 保存 推荐资源接受者
 * 
 * @author zhengjy
 *
 *  2013-4-7
 *
 */
public interface IUserStatusDao {

    /**
     * TODO 通过time 和 resId标识唯一性
     * @param resId
     * @param time
     * @param userStatus
     * @return
     */
	public void insertUserStatus(String userId ,String resId,String time, List<UserStatus> userStatus);

	public void clearUser(String uid);

	public List<UserStatus> getUserStatus(String userid,String resId,String time);
	
	public boolean isExist(String userId ,String resId,String time);
	

}
