package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.server.response.AddressMembers;

public interface INetAddressBookUserDao {
	
	/**
	 * 添加单位通讯录成员
	 * @param member
	 */
	public void insertAddressMember(AddressMembers member);
	/**
	 * 删除单位通讯录中的某个成员
	 * @param memberId 根据成员Id
	 */
	public void deleAddressMember(String memberId);
	/**
	 * 修改成员信息
	 * @param member
	 */
	public void updateAddressMember(AddressMembers member);
	/**
	 * 根据关系Id来判断是否存在单位通讯录
	 * @param mobile
	 * @return
	 */
	public boolean isExist(String rId);
	/**
	 * 保存成员数据
	 * @param adressMemberLists
	 */
	public void saveAddressMember(List<AddressMembers> adressMemberLists,String addressbookId);
	/**
	 * 或者所有成员
	 * @return
	 */
	public List<AddressMembers> getAddressMemberList();
	/**
	 * 获取某个单位通讯录下成员总数
	 * @param bookId
	 * @return
	 */
	public Integer getAddressMemberNum(String bookId);
	/**
	 * 通过单位通讯录Id删除某一单位通讯录下的所有成员
	 * @param bookId
	 */
	public void deleAddressMemberByBookid(String bookId);
	
	/**
	 * 删除单位通讯录中的某个成员
	 * @param userId 根据用户Id ，只针对旁课用户
	 */
	public void deleAddressMemberByUserid(String userId);
}
