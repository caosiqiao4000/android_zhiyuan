package com.wachoo.pangker.db;

import com.wachoo.pangker.entity.DetailedAddress;

public interface IDetailedAddress {

	boolean saveDitailedAddress(DetailedAddress detailedAddress);

	DetailedAddress getDetailedAddress(String locationString);
}
