package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.server.response.UserGroup;

/**
 * 
 * @author wangxin
 * 
 */
public interface IUserGroupDao {
	/**
	 * 
	 * @param group
	 */
	public boolean addGroup(UserGroup group);

	/**
	 * 
	 * @param group
	 */
	public boolean delGroup(UserGroup group);

	/**
	 * 
	 * saveGroups
	 * 
	 * @param groups
	 */
	public void saveGroups(List<UserGroup> groups);

	/**
	 * getGroups
	 */
	public List<UserGroup> getUserGroups();

	
	/**
	 * 修改群组信息
	 * @author wubo
	 * @createtime 2012-5-17 
	 * @param group
	 * @return
	 */
	public boolean updateGroup(UserGroup group);
	
	/**
	 * 修改封面
	 * @author wubo
	 * @createtime Jun 8, 2012 
	 * @param group
	 * @return
	 */
	public boolean updateGroupCover(String uid,String gid);
	
	/**
	 * 修改群组信息
	 * @author wubo
	 * @createtime 2012-5-17 
	 * @param group
	 * @return
	 */
	public boolean updateGroupBase(UserGroup group);
	/**
	 * By id getGroup
	 * @author wubo
	 * @createtime Jun 8, 2012 
	 * @param group
	 * @return
	 */
	public UserGroup getGroupById(String gid);
	/**
	 * 
	 * void
	 * TODO 清除登录用户本地群组数据
	 */
	public void clearGroupData(String userid);
}
