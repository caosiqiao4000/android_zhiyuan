package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.Limit;
import com.wachoo.pangker.server.response.RelativesResInfo;

public interface IIntimateDao {

	boolean saveIntimateData(List<RelativesResInfo> relativesResInfos);

	boolean saveIntimateData(RelativesResInfo relativesResInfo);

	List<RelativesResInfo> getIntimateData(Limit limit);

	boolean deleteAllData();

	boolean deleteData(String RUid);
}
