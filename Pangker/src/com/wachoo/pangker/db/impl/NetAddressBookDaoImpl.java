package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.INetAddressBookDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.server.response.AddressBooks;

/**
 * 单位通讯录Dao实现
 * 
 * @author zhengjy
 * 
 *         对应表MyDataBaseAdapter.TABLE_NET_CONTACT
 */
public class NetAddressBookDaoImpl implements INetAddressBookDao {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private Context mContext;
	private MyDataBaseAdapter mDataBaseAdapter;
	private PangkerApplication application;

	public NetAddressBookDaoImpl() {
	}

	public NetAddressBookDaoImpl(Context mContext) {
		this.mContext = mContext;
		application = (PangkerApplication) mContext.getApplicationContext();
	}

	@Override
	public void insertAddressBook(String mUserId ,AddressBooks book) {
		try {

			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			if (isExist(mUserId,String.valueOf(book.getId()), mDataBaseAdapter))
				return;
			ContentValues cValues = new ContentValues();
			cValues.put(MyDataBaseAdapter.NET_CONTACT_AID, String.valueOf(book.getId()));
			cValues.put(MyDataBaseAdapter.NET_CONTACT_NAME, book.getName());
			cValues.put(MyDataBaseAdapter.NET_CONTACT_CREATER, book.getUsername());
			cValues.put(MyDataBaseAdapter.NET_CONTACT_CID, String.valueOf(book.getUid()));
			cValues.put(MyDataBaseAdapter.NET_MY_USERID, mUserId);
			cValues.put(MyDataBaseAdapter.NET_CONTACT_MOBILE, book.getMobile());
			cValues.put(MyDataBaseAdapter.NET_CONTACT_CREATETIME, book.getCreatetime());
			cValues.put(MyDataBaseAdapter.NET_CONTACT_NUM, book.getNum());
			cValues.put(MyDataBaseAdapter.NET_CONTACT_VERSION, "-1");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_FLAG, book.getFlag());

			long isInsertSuccess = mDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_NET_CONTACT, cValues);
			if (isInsertSuccess > 0) {
				application.addNetAddressBook(book);

			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
	}

	@Override
	public void deleAddressBook(String mUserId ,String bookId) {
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			if (!isExist(mUserId,bookId, mDataBaseAdapter))
				return;
			String[] key = {MyDataBaseAdapter.NET_MY_USERID,MyDataBaseAdapter.NET_CONTACT_AID};
			String[] value = {mUserId, bookId };
			boolean isDeleSuccess = mDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_NET_CONTACT, key, value);
			if (isDeleSuccess) {
				application.removeNetAddressBook(Long.parseLong(bookId));

			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
	}

	@Override
	public void updateAddressBook(String mUserId ,AddressBooks book) {
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			if (!isExist(mUserId,String.valueOf(book.getId()), mDataBaseAdapter))
				return;
			ContentValues cValues = new ContentValues();
			cValues.put(MyDataBaseAdapter.NET_CONTACT_NAME, book.getName());
			cValues.put(MyDataBaseAdapter.NET_CONTACT_NUM, book.getNum());
			cValues.put(MyDataBaseAdapter.NET_CONTACT_VERSION, String.valueOf(book.getVersion()));
			String whereClause = MyDataBaseAdapter.NET_CONTACT_AID +" = ? and "+ MyDataBaseAdapter.NET_MY_USERID + "=?";
			String[] whereArgs = { String.valueOf(book.getId()),mUserId};
			boolean isUpdateSuccess = mDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_NET_CONTACT,
					cValues, whereClause, whereArgs);
			if (isUpdateSuccess) {
				application.updateNetAddressBook(book);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
	}

	@Override
	public boolean isExist(String mUserId ,String bookId, MyDataBaseAdapter adapter) {
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_NET_CONTACT + " where "
				+ MyDataBaseAdapter.NET_CONTACT_AID + "='" + bookId + "' and "+ MyDataBaseAdapter.NET_MY_USERID +" ='" + mUserId + "'";
		Cursor mCursor = null;
		boolean isExist = false;
		try {
			mCursor = adapter.rawQuery(selectSQL);
			isExist = mCursor != null && mCursor.getCount() > 0;

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return isExist;
	}

	@Override
	public void saveAddressBooks(String mUserId ,List<AddressBooks> addressBookLists) {
		// TODO Auto-generated method stub
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			for (AddressBooks addressBooks : addressBookLists) {
				if (!isExist(mUserId,String.valueOf(addressBooks.getId()), mDataBaseAdapter)) {
					ContentValues cValues = new ContentValues();
					cValues.put(MyDataBaseAdapter.NET_CONTACT_AID, String.valueOf(addressBooks.getId()));
					cValues.put(MyDataBaseAdapter.NET_CONTACT_NAME, addressBooks.getName());
					cValues.put(MyDataBaseAdapter.NET_CONTACT_CREATER, addressBooks.getUsername());
					cValues.put(MyDataBaseAdapter.NET_MY_USERID,mUserId);
					cValues.put(MyDataBaseAdapter.NET_CONTACT_CID, String.valueOf(addressBooks.getUid()));
					cValues.put(MyDataBaseAdapter.NET_CONTACT_MOBILE, addressBooks.getMobile());
					cValues.put(MyDataBaseAdapter.NET_CONTACT_CREATETIME, addressBooks.getCreatetime());
					cValues.put(MyDataBaseAdapter.NET_CONTACT_NUM, addressBooks.getNum());
					cValues.put(MyDataBaseAdapter.NET_CONTACT_VERSION, "-1");
					cValues.put(MyDataBaseAdapter.NET_CONTACT_FLAG, addressBooks.getFlag());
					mDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_NET_CONTACT, cValues);
				}
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}

	}

	@Override
	public List<AddressBooks> getAddressBookList(String mUserId) {
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_NET_CONTACT + " where "+ MyDataBaseAdapter.NET_MY_USERID +" ='"+mUserId+"' order by "
				+ MyDataBaseAdapter.NET_CONTACT_FLAG + " asc";
		Cursor mCursor = null;
		List<AddressBooks> addressBooks = new ArrayList<AddressBooks>();
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			mCursor = mDataBaseAdapter.rawQuery(selectSQL);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
					AddressBooks addressBook = new AddressBooks();
					addressBook.setId(mCursor.getLong(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_AID)));
					addressBook.setName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_NAME)));
					addressBook.setMobile(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_MOBILE)));
					addressBook.setUsername(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_CREATER)));
					addressBook.setCreatetime(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_CREATETIME)));
					addressBook.setUid(mCursor.getLong(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_CID)));
					addressBook.setVersion(mCursor.getLong(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_VERSION)));
					addressBook.setFlag(mCursor.getInt(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_FLAG)));
					// int num =
					// iAddressBookDao.getAddressMemberNum(String.valueOf(addressBook.getId()));//
					// 此处添加查找成员总数
					addressBook.setNum(mCursor.getInt(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_NUM)));
					addressBooks.add(addressBook);
				}
			}
			return addressBooks;
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
		return null;
	}

	@Override
	public boolean clearAddressBook(String mUserId) {
		// TODO Auto-generated method stub
		boolean isClear = false;
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			String sqlClear = "delete from " + MyDataBaseAdapter.TABLE_NET_CONTACT + " where "+ MyDataBaseAdapter.NET_MY_USERID +" ='" + mUserId + "'";
			isClear = mDataBaseAdapter.execSql(sqlClear);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
		return isClear;
	}

}
