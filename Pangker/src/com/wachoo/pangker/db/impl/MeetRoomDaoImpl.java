package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IMeetRoomDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.MeetRoom;

/**
 * @author wubo edit by wangxin
 * @createtime 2012-3-19
 */
public class MeetRoomDaoImpl implements IMeetRoomDao {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private MyDataBaseAdapter mdb;
	private Context context;
	private String myuserid;

	public MeetRoomDaoImpl(Context context) {
		this.context = context;
		myuserid = ((PangkerApplication) context.getApplicationContext())
				.getMyUserID();
	}

	@Override
	public boolean saveRoomHistory(MeetRoom mr) {
		// TODO Auto-generated method stub
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			ContentValues cv = new ContentValues();
			cv.put(MyDataBaseAdapter.MEETROOM_GID, mr.getGid());
			cv.put(MyDataBaseAdapter.MEETROOM_UID, mr.getUserId());
			cv.put(MyDataBaseAdapter.MEETROOM_NICKNAME, mr.getNickname());
			cv.put(MyDataBaseAdapter.MEETROOM_CONTENT, mr.getContent());
			cv.put(MyDataBaseAdapter.MEETROOM_TALKTIME, mr.getTalkTime());
			cv.put(MyDataBaseAdapter.MEETROOM_MSGID, mr.getMsgId());
			cv.put(MyDataBaseAdapter.MEETROOM_MSGTYPE, mr.getMsgType());
			cv.put(MyDataBaseAdapter.MEETROOM_MYUID, mr.getMyUserId());
			cv.put(MyDataBaseAdapter.MEETROOM_STATUS, mr.getStatus());
			long _id = mdb.insertData(MyDataBaseAdapter.TABLE_MEETROOM, cv);
			if (_id > 0) {
				mr.setId((int) _id);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
	}
	
	private MeetRoom getMeetRoomByCursor(Cursor mCursor){
		MeetRoom msg = new MeetRoom("");
		msg.setId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_ID)));
		msg.setGid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_GID)));
		msg.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_UID)));
		msg.setNickname(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_NICKNAME)));
		msg.setContent(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_CONTENT)));
		msg.setStatus(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_STATUS)));
		msg.setTalkTime(mCursor.getLong(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_TALKTIME)));
		msg.setMsgId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_MSGID)));
		msg.setMsgType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_MSGTYPE)));
		msg.setMyUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_MYUID)));
		return msg;
	}
	
	
	@Override
	public int getMeetRoomCount(String groupId) {
		String sql = "SELECT COUNT(*) FROM " + MyDataBaseAdapter.TABLE_MEETROOM + " WHERE " + MyDataBaseAdapter.MEETROOM_GID + " =" + groupId + " AND "
				+ MyDataBaseAdapter.MEETROOM_MYUID + " =" + myuserid;
		Cursor mCursor = null;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			mCursor = mdb.rawQuery(sql);
			 if(mCursor!= null && mCursor.moveToNext()){
		          return mCursor.getInt(0);
		   } 
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mdb != null) {
				mdb.close();
			}
		}
		return 0;
	}

	@Override
	public List<MeetRoom> getRoomHistory(String groupId) {
		// TODO Auto-generated method stub
		List<MeetRoom> msgs = new ArrayList<MeetRoom>();
		if (groupId == null) {
			return msgs;
		}
		Cursor mCursor = null;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			mCursor = mdb.getMeetRoomHistory(groupId, myuserid);
			if (mCursor == null) {
				return msgs;
			}
			if (!mCursor.moveToFirst()) {
				return msgs;
			}
			do {
				msgs.add(getMeetRoomByCursor(mCursor));
			} while (mCursor.moveToNext());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mdb != null) {
				mdb.close();
			}
		}
		return msgs;
	}

	@Override
	public boolean clearRoomHistory(String groupId) {
		// TODO Auto-generated method stub
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			boolean isDeleSuccess = mdb.deleteData( MyDataBaseAdapter.TABLE_MEETROOM,
					MyDataBaseAdapter.MEETROOM_GID, groupId);
			return isDeleSuccess;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
		return false;
	}

	@Override
	public List<MeetRoom> getLimitHistory(String groupId, int start, int length) {
		// TODO Auto-generated method stub
		List<MeetRoom> msgs = new ArrayList<MeetRoom>();
		Cursor mCursor = null;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			mCursor = mdb.getMeetRoomHistoryLimit(groupId, start, length, myuserid);
			if (mCursor == null) {
				return msgs;
			}
			if (!mCursor.moveToFirst()) {
				return msgs;
			}
			do {
				int msgid = mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_MSGID));
				if (msgid==0) { // 是通知
					continue;
				}
				msgs.add(getMeetRoomByCursor(mCursor));
			} while (mCursor.moveToNext());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mdb != null) {
				mdb.close();
			}
		}
		return msgs;
	}

	@Override
	public int getMaxNum(String groupId) {
		int maxNum = 0;
		Cursor mCursor = null;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			mCursor = mdb.getMsgMaxNum(groupId);
			if (mCursor == null) {
				return maxNum;
			}
			if (!mCursor.moveToFirst()) {
				return maxNum;
			}
			maxNum = mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MEETROOM_MSGID));
			return maxNum;
		} catch (Exception e) {
			logger.error(TAG, e);
			return maxNum;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mdb != null) {
				mdb.close();
			}
		}
	}

	@Override
	public boolean saveMsgs(List<MeetRoom> msgs) {
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			Iterator<MeetRoom> msgiter = msgs.iterator();
			mdb.mSQLiteDatabase.beginTransaction();
			while (msgiter.hasNext()) {
				MeetRoom mr = msgiter.next();
				ContentValues cv = new ContentValues();
				cv.put(MyDataBaseAdapter.MEETROOM_GID, mr.getGid());
				cv.put(MyDataBaseAdapter.MEETROOM_UID, mr.getUserId());
				cv.put(MyDataBaseAdapter.MEETROOM_NICKNAME, mr.getNickname());
				cv.put(MyDataBaseAdapter.MEETROOM_CONTENT, mr.getContent());
				cv.put(MyDataBaseAdapter.MEETROOM_TALKTIME, mr.getTime());
				cv.put(MyDataBaseAdapter.MEETROOM_MSGID, mr.getMsgId());
				cv.put(MyDataBaseAdapter.MEETROOM_MSGTYPE, mr.getMsgType());
				cv.put(MyDataBaseAdapter.MEETROOM_MYUID, mr.getMyUserId());
				cv.put(MyDataBaseAdapter.MEETROOM_STATUS, mr.getStatus());
				mdb.insertData(MyDataBaseAdapter.TABLE_MEETROOM, cv);
			}
			mdb.mSQLiteDatabase.setTransactionSuccessful();
			mdb.mSQLiteDatabase.endTransaction();
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
	}

	@Override
	public boolean updateRoomHistory(MeetRoom mr) {
		// TODO Auto-generated method stub
		mdb = new MyDataBaseAdapter(context);
		mdb.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.MEETROOM_STATUS, mr.getStatus());
		initialValues.put(MyDataBaseAdapter.MEETROOM_CONTENT, mr.getContent());
		try {
			return mdb.updateData(MyDataBaseAdapter.TABLE_MEETROOM,
					initialValues, MyDataBaseAdapter.MEETROOM_ID,
					new String[] { String.valueOf(mr.getId()) });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
	}
	
	

}
