package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.db.IAttentsDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.util.Util;

/**
 * 
 * AttentsDaoImpl
 * 
 * @author wangxin
 * 
 */
public class AttentsDaoImpl implements IAttentsDao {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Context context;
	private String myUserID;
	private PangkerApplication application;
	private IPKUserDao userDao;

	public AttentsDaoImpl(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		userDao = new PKUserDaoImpl(context);
		application = (PangkerApplication) context.getApplicationContext();
		myUserID = application.getMyUserID();
	}

	@Override
	public boolean addAttent(UserItem user, String groupid) {
		// TODO Auto-generated method stub
		try {

			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();

			if (m_MyDataBaseAdapter.checkAttentIsExist(user.getUserId(), groupid, myUserID))
				return false;
			if(!groupid.equals("0")){
				user=userDao.getUserItemByUserID(user.getUserId());
			}
			ContentValues friendsValues = new ContentValues();
			friendsValues.put(MyDataBaseAdapter.ATTENTS_MYUID, myUserID);
			friendsValues.put(MyDataBaseAdapter.ATTENTS_ATTENTUID, Util.trimUserIDDomain(user.getUserId()));
			friendsValues.put(MyDataBaseAdapter.ATTENTS_GROUPID, groupid);
			friendsValues.put(MyDataBaseAdapter.ATTENTS_TYPE, user.getAttentType());
			long _userid = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_ATTENTS, friendsValues);
			if (_userid > 0) {
				return true;
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return true;
	}

	@Override
	public boolean delAttent(UserItem user, String groupid) {
		// TODO Auto-generated method stub
		boolean result = false;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();

			String delAttent = "delete from " + MyDataBaseAdapter.TABLE_ATTENTS + " where "
					+ MyDataBaseAdapter.ATTENTS_GROUPID + " = '" + groupid + "' and "
					+ MyDataBaseAdapter.ATTENTS_ATTENTUID + " = '" + user.getUserId() + "' and "
					+ MyDataBaseAdapter.FANS_MYUID + " = '" + myUserID + "'";
			result = m_MyDataBaseAdapter.execSql(delAttent);
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return result;
	}

	@Override
	public boolean addAttent(UserItem user, String groupid, MyDataBaseAdapter adapter) {

		try {
			if (adapter.checkAttentIsExist(user.getUserId(), groupid, myUserID))
				return false;
			ContentValues friendsValues = new ContentValues();
			friendsValues.put(MyDataBaseAdapter.ATTENTS_MYUID, myUserID);
			friendsValues.put(MyDataBaseAdapter.ATTENTS_ATTENTUID, Util.trimUserIDDomain(user.getUserId()));
			friendsValues.put(MyDataBaseAdapter.ATTENTS_GROUPID, groupid);
			friendsValues.put(MyDataBaseAdapter.ATTENTS_TYPE, user.getAttentType());
			// friendsValues.put(m_MyDataBaseAdapter.FRIENDS_SOURCE, "1");
			long _userid = adapter.insertData(MyDataBaseAdapter.TABLE_ATTENTS, friendsValues);
			if (_userid > 0)
				return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		}
		return false;

	}
	
	public boolean addAttent(String attuid,Integer attentType, String groupid, MyDataBaseAdapter adapter) {

		try {
			if (adapter.checkAttentIsExist(attuid, groupid, myUserID))
				return false;
			ContentValues friendsValues = new ContentValues();
			friendsValues.put(MyDataBaseAdapter.ATTENTS_MYUID, myUserID);
			friendsValues.put(MyDataBaseAdapter.ATTENTS_ATTENTUID, Util.trimUserIDDomain(attuid));
			friendsValues.put(MyDataBaseAdapter.ATTENTS_GROUPID, groupid);
			friendsValues.put(MyDataBaseAdapter.ATTENTS_TYPE, attentType);
			// friendsValues.put(m_MyDataBaseAdapter.FRIENDS_SOURCE, "1");
			long _userid = adapter.insertData(MyDataBaseAdapter.TABLE_ATTENTS, friendsValues);
			if (_userid > 0)
				return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		}
		return false;

	}

	@Override
	public boolean delAttent(UserItem user, String groupid, MyDataBaseAdapter adapter) {
		try {
			// TODO Auto-generated method stub
			String delAttent = "delete from " + MyDataBaseAdapter.TABLE_ATTENTS + " where "
					+ MyDataBaseAdapter.ATTENTS_GROUPID + " = " + groupid + " and "
					+ MyDataBaseAdapter.ATTENTS_ATTENTUID + " = " + user.getUserId();
			return adapter.execSql(delAttent);
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		}
	}

	@Override
	public boolean delAttent(String attendId) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();

			String delAttent = "delete from " + MyDataBaseAdapter.TABLE_ATTENTS + " where "
					+ MyDataBaseAdapter.ATTENTS_ATTENTUID + " = '" + attendId + "' and "
					+ MyDataBaseAdapter.ATTENTS_MYUID + " = '" + myUserID + "'";
			boolean isSuccess = m_MyDataBaseAdapter.execSql(delAttent);
			return isSuccess;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}

	@Override
	public List<UserItem> getAllAttents() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		List<UserItem> attents = new ArrayList<UserItem>();
		Cursor mCursor = null;
		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getAllAttents();
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToFirst();
			attents = new ArrayList<UserItem>();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				UserItem friend = new UserItem();

				friend = new UserItem();
				friend.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_ID)));// 用户id
				friend.setUserName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));// 用户名
				friend.setNickName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
				friend.setIconType(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识
				friend.setAttentType(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.ATTENTS_TYPE)));
				friend.setSign(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));// 用户签名
				attents.add(friend);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
		}
		return attents;
	}

	@Override
	public List<UserItem> getAttentsByGroupId(String groupId) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		List<UserItem> attents = new ArrayList<UserItem>();
		Cursor mCursor = null;
		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getAttentsByGroupId(groupId,myUserID);
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToFirst();
			attents = new ArrayList<UserItem>();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				UserItem friend = new UserItem();

				friend = new UserItem();
				friend.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_ID)));// 用户id
				friend.setUserName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));// 用户名
				friend.setNickName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
				friend.setIconType(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识

				friend.setSign(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));// 用户签名
				friend.setAttentType(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.ATTENTS_TYPE)));
				attents.add(friend);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
		}
		return attents;
	}
	
	@Override
	public boolean refreshFriends(List<ContactGroup> list) {
		// TODO Auto-generated method stub
		Cursor mCursor = null;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			String sql1 = "delete from "
					+ MyDataBaseAdapter.TABLE_CONTACTS_GROUP + " where "
					+ MyDataBaseAdapter.GROUP_TYPE + " = "
					+ PangkerConstant.GROUP_ATTENTS;
			m_MyDataBaseAdapter.execSql(sql1);
			String sql2 = "delete from " + MyDataBaseAdapter.TABLE_ATTENTS;
			m_MyDataBaseAdapter.execSql(sql2);
			for (ContactGroup group : list) {
				ContentValues groupValues = new ContentValues();
				groupValues.put(MyDataBaseAdapter.GROUP_GROUPID, group
						.getGid());
				groupValues.put(MyDataBaseAdapter.GROUP_CREATER, group
						.getUid());
				groupValues.put(MyDataBaseAdapter.GROUP_GROUPNAME, group
						.getName());
				groupValues.put(MyDataBaseAdapter.GROUP_MEMBER_COUNT, group
						.getCount());
				groupValues.put(MyDataBaseAdapter.GROUP_MYUID, myUserID);
				groupValues
						.put(MyDataBaseAdapter.GROUP_TYPE, group.getType());
				// group
				if (!m_MyDataBaseAdapter.checkContactsGroupIsExist(group
						.getGid(), myUserID, group.getType())) {
					long _frdgroupsid = m_MyDataBaseAdapter.insertData(
							MyDataBaseAdapter.TABLE_CONTACTS_GROUP,
							groupValues);
					System.out.println("Sava groupInfo to GroupTable ====>"
							+ _frdgroupsid);
				}
				
				for (UserItem user : group.getMembers()) {
					boolean result = addAttent(user.getUserId(),user.getAttentType(), group.getGid(),
							m_MyDataBaseAdapter);
					System.out.println("Sava FriendInfo to FriendTable ====>"
							+ result);
					result = userDao.saveUser(user, m_MyDataBaseAdapter);
					System.out.println("Sava FriendInfo to UserTable ====>"
							+ result);
				}
			}
			
			mCursor = m_MyDataBaseAdapter.getMyGroupByType(myUserID,
					String.valueOf(PangkerConstant.GROUP_ATTENTS));
			List<ContactGroup> groups = new ArrayList<ContactGroup>();
			if (mCursor != null || mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
					ContactGroup group = new ContactGroup();
					group.setClientgid(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.GROUP_ID)));
					String groupId = mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.GROUP_GROUPID));
					group.setGid(groupId);
					group
							.setName(mCursor
									.getString(mCursor
											.getColumnIndex(MyDataBaseAdapter.GROUP_GROUPNAME)));
					group.setType(mCursor.getInt(mCursor
							.getColumnIndex(MyDataBaseAdapter.GROUP_TYPE)));
					group.setUid(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.GROUP_CREATER)));
					List<UserItem> mUserItem = new ArrayList<UserItem>();
					mUserItem = getAttentsByGroupId(groupId);
					group.setCount(mUserItem != null ? mUserItem.size() : 0);
					if (mUserItem != null) {
						group.setUserItems(mUserItem);
					}
					groups.add(group);
				}
			}
			application.setAttentGroup(groups);
			return true;

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}
}
