package com.wachoo.pangker.db.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IFansDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.util.Util;

/**
 * 
 * FansDaoImpl
 * 
 * @author wangxin
 * 
 */
public class FansDaoImpl implements IFansDao {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Context context;
	private String myUserID;
	private PangkerApplication application;

	public FansDaoImpl(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		application = (PangkerApplication) context.getApplicationContext();
		if (application != null)
			myUserID = application.getMyUserID();
	}

	@Override
	public List<UserItem> getUserFans() {
		// TODO Auto-generated method stub
		List<UserItem> fans = new ArrayList<UserItem>();
		Cursor fansCursor = null;
		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			fansCursor = m_MyDataBaseAdapter.getUserFans(MyDataBaseAdapter.FANS_MYUID, myUserID);
			if (fansCursor == null)
				return fans;
			if (fansCursor.getCount() == 0)
				return fans;
			fansCursor.moveToFirst();
			for (; !fansCursor.isAfterLast(); fansCursor.moveToNext()) {

				UserItem user = new UserItem();
				user.setUserId(fansCursor.getString(fansCursor.getColumnIndex(MyDataBaseAdapter.USER_ID)));
				user.setUserName(fansCursor.getString(fansCursor
						.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
				user.setNickName(fansCursor.getString(fansCursor
						.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
				user.setIconType(fansCursor.getInt(fansCursor
						.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识

				user.setSign(fansCursor.getString(fansCursor.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));
				fans.add(user);
			}

		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			if (fansCursor != null) {
				fansCursor.close();
			}
		}
		// Log.i(tag, "get UserInfo Form database over ");
		return fans;
	}

	@Override
	public boolean saveFans(List<UserItem> fans) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			for (UserItem user : fans) {
				// check fans exist
				if (m_MyDataBaseAdapter.checkFansIsExist(Util.trimUserIDDomain(user.getUserId()), myUserID))
					continue;
				ContentValues fansValues = new ContentValues();
				fansValues.put(MyDataBaseAdapter.FANS_MYUID, myUserID);
				fansValues.put(MyDataBaseAdapter.FANS_FANUID, Util.trimUserIDDomain(user.getUserId()));
				fansValues.put(MyDataBaseAdapter.FANS_SOURCE, "1");
				long _fansid = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_FANS, fansValues);
				// check user is exist
				if (m_MyDataBaseAdapter.checkUserIsExist(Util.trimUserIDDomain(user.getUserId())))
					continue;
				ContentValues userValues = new ContentValues();
				userValues.put(MyDataBaseAdapter.USER_ID, Util.trimUserIDDomain(user.getUserId()));
				userValues.put(MyDataBaseAdapter.USER_USERNAME, user.getUserName());
				userValues.put(MyDataBaseAdapter.USER_NICKNAME, user.getNickName());
				userValues.put(MyDataBaseAdapter.USER_PORTARITTYPE, user.getIconType());

				userValues.put(MyDataBaseAdapter.USER_SIGNATURE, user.getSign());
				userValues.put(MyDataBaseAdapter.USER_TIME, new Timestamp(System.currentTimeMillis()) + "");
				long _userid = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_USERS, userValues);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return true;
	}

	@Override
	public boolean saveFans(UserItem fans) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			if (m_MyDataBaseAdapter.checkFansIsExist(Util.trimUserIDDomain(fans.getUserId()), myUserID))
				return false;
			ContentValues friendsValues = new ContentValues();
			friendsValues.put(MyDataBaseAdapter.FANS_MYUID, myUserID);
			friendsValues.put(MyDataBaseAdapter.FANS_FANUID, Util.trimUserIDDomain(fans.getUserId()));
			// friendsValues.put(m_MyDataBaseAdapter.FRIENDS_SOURCE, "1");
			long _userid = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_FANS, friendsValues);
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return true;
	}

	@Override
	public boolean saveFans(UserItem fans, MyDataBaseAdapter myDataBaseAdapter) {
		// TODO Auto-generated method stub
		try {
			if (myDataBaseAdapter.checkFansIsExist(Util.trimUserIDDomain(fans.getUserId()), myUserID))
				return false;
			ContentValues fansValues = new ContentValues();
			fansValues.put(MyDataBaseAdapter.FANS_MYUID, myUserID);
			fansValues.put(MyDataBaseAdapter.FANS_FANUID, Util.trimUserIDDomain(fans.getUserId()));
			fansValues.put(MyDataBaseAdapter.FANS_SOURCE, "1");
			long _fansid = myDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_FANS, fansValues);
			if (_fansid > 0)
				return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		}
		return false;
	}

	@Override
	public boolean removeFans(String userID) {
		// TODO Auto-generated method stub
		boolean result = false;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();

			String delAttent = "delete from " + MyDataBaseAdapter.TABLE_FANS + " where "
					+ MyDataBaseAdapter.FANS_FANUID + " = '" + userID + "' and "
					+ MyDataBaseAdapter.FANS_MYUID + " = '" + myUserID + "'";
			result = m_MyDataBaseAdapter.execSql(delAttent);

		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return result;
	}

	@Override
	public boolean refreshFriends(List<UserItem> list) {
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			String sql = "delete from " + MyDataBaseAdapter.TABLE_FANS;
			m_MyDataBaseAdapter.execSql(sql);
			for (UserItem user : list) {
				// check fans exist
				if (m_MyDataBaseAdapter.checkFansIsExist(Util.trimUserIDDomain(user.getUserId()), myUserID))
					continue;
				ContentValues fansValues = new ContentValues();
				fansValues.put(MyDataBaseAdapter.FANS_MYUID, myUserID);
				fansValues.put(MyDataBaseAdapter.FANS_FANUID, Util.trimUserIDDomain(user.getUserId()));
				fansValues.put(MyDataBaseAdapter.FANS_SOURCE, "1");
				long _fansid = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_FANS, fansValues);
				// check user is exist
				if (m_MyDataBaseAdapter.checkUserIsExist(Util.trimUserIDDomain(user.getUserId())))
					continue;
				ContentValues userValues = new ContentValues();
				userValues.put(MyDataBaseAdapter.USER_ID, Util.trimUserIDDomain(user.getUserId()));
				userValues.put(MyDataBaseAdapter.USER_USERNAME, user.getUserName());
				userValues.put(MyDataBaseAdapter.USER_REALNAME, user.getRealName());
				userValues.put(MyDataBaseAdapter.USER_NICKNAME, user.getUserName());
				userValues.put(MyDataBaseAdapter.USER_PORTARITTYPE, user.getIconType());
				// userValues.put(m_MyDataBaseAdapter.USER_NET_PORTARIT,
				// Util.getImageBytesFromURL(user
				// .getPortraitIconUrl()));
				userValues.put(MyDataBaseAdapter.USER_SIGNATURE, user.getSign());
				userValues.put(MyDataBaseAdapter.USER_TIME, new Timestamp(System.currentTimeMillis()) + "");
				long _userid = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_USERS, userValues);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

}
