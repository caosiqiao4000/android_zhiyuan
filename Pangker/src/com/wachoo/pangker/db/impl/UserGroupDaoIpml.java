package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IUserGroupDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.server.response.UserGroup;

/**
 * 
 * @author wangxin
 * 
 */
public class UserGroupDaoIpml implements IUserGroupDao {
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Context context;
	private String myUserID;
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	/**
	 * 
	 * @param context
	 */
	public UserGroupDaoIpml(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		myUserID = ((PangkerApplication) context.getApplicationContext()).getMyUserID();
	}

	@Override
	public boolean addGroup(UserGroup group) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			ContentValues groupValues = new ContentValues();

			groupValues.put(MyDataBaseAdapter.USER_GROUP_GROUPID, group.getGroupId());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_CREATER, group.getUid());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_GROUPSID, group.getGroupSid());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_GROUPNAME, group.getGroupName());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_CREATETIME, group.getCreateTime());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_MYUID, myUserID);
			groupValues.put(MyDataBaseAdapter.USER_GROUP_TYPE, group.getGroupType());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_LOTYPE, group.getLotype());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_LASTVISITTIME, group.getLastVisitTime());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_FLAG, group.getGroupFlag());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_COVERFLAG, group.getConverFlag());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_LABLE, group.getGroupLabel());
			groupValues.put(MyDataBaseAdapter.USER_GROUP_CATEGORY, group.getCategory());
			// 组的id由后台接口返回。
			if (m_MyDataBaseAdapter.checkUserGroupIsExist(group.getGroupId(), myUserID, group.getGroupType(),
					group.getGroupFlag())) {
				m_MyDataBaseAdapter.updateGroupIntoTime(group);
				return true;
			}
			long _frdgroupsid = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_USER_GROUP,
					groupValues);
			if (_frdgroupsid > 0)
				return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	public boolean delGroup(UserGroup group) {
		// TODO Auto-generated method stub
		boolean result = false;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();

			if (group.getGroupFlag() == 0) {
				m_MyDataBaseAdapter.deleteUserGroup(MyDataBaseAdapter.TABLE_USER_GROUP,
						MyDataBaseAdapter.USER_GROUP_GROUPID, group.getGroupId());
			} else {
				m_MyDataBaseAdapter.deleteUserGroup(MyDataBaseAdapter.TABLE_USER_GROUP,
						MyDataBaseAdapter.USER_GROUP_ID, group.getClientId());
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return result;
	}

	@Override
	public void saveGroups(List<UserGroup> groups) {
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			for (UserGroup group : groups) {
				ContentValues groupValues = new ContentValues();
				groupValues.put(MyDataBaseAdapter.USER_GROUP_GROUPID, group.getGroupId());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_CREATER, group.getUid());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_GROUPSID, group.getGroupSid());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_GROUPNAME, group.getGroupName());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_CREATETIME, group.getCreateTime());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_MYUID, myUserID);
				groupValues.put(MyDataBaseAdapter.USER_GROUP_TYPE, group.getGroupType());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_LOTYPE, group.getLotype());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_LASTVISITTIME, group.getLastVisitTime());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_FLAG, group.getGroupFlag());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_COVERFLAG, group.getConverFlag());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_LABLE, group.getGroupLabel());
				groupValues.put(MyDataBaseAdapter.USER_GROUP_CATEGORY, group.getCategory());

				m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_USER_GROUP, groupValues);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}

	}

	@Override
	public List<UserGroup> getUserGroups() {
		// TODO Auto-generated method stub
		List<UserGroup> groups = new ArrayList<UserGroup>();
		Cursor mCursor = null;
		Cursor mCursor2 = null;
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getUserGroups(MyDataBaseAdapter.USER_GROUP_MYUID, myUserID);

			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
					UserGroup group = new UserGroup();
					group.setClientId(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_ID)));
					group.setGroupSid(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_GROUPSID)));
					group.setGroupId(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_GROUPID)));
					group.setGroupName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_GROUPNAME)));
					group.setGroupType(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_TYPE)));
					group.setLotype(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_LOTYPE)));
					group.setUid(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_CREATER)));
					group.setCreateTime(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_CREATETIME)));
					group.setLastVisitTime(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_LASTVISITTIME)));
					group.setGroupFlag(mCursor.getInt(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_FLAG)));
					group.setConverFlag(mCursor.getInt(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_COVERFLAG)));
					group.setGroupLabel(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_LABLE)));
					group.setCategory(mCursor.getInt(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_CATEGORY)));
					groups.add(group);
				}
			}

			if (mCursor != null) {
				mCursor.close();
			}

			mCursor2 = m_MyDataBaseAdapter.getComeUserGroups(MyDataBaseAdapter.GROUP_MYUID, myUserID);
			if (mCursor2 != null && mCursor2.getCount() > 0) {
				mCursor2.moveToFirst();
				for (; !mCursor2.isAfterLast(); mCursor2.moveToNext()) {
					UserGroup group = new UserGroup();
					group.setClientId(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_ID)));
					group.setGroupSid(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_GROUPSID)));
					group.setGroupId(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_GROUPID)));
					group.setGroupName(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_GROUPNAME)));
					group.setGroupType(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_TYPE)));
					group.setLotype(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_LOTYPE)));
					group.setUid(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_CREATER)));
					group.setCreateTime(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_CREATETIME)));
					group.setLastVisitTime(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_LASTVISITTIME)));
					group.setGroupFlag(mCursor2.getInt(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_FLAG)));
					group.setConverFlag(mCursor2.getInt(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_COVERFLAG)));
					group.setGroupLabel(mCursor2.getString(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_LABLE)));
					group.setCategory(mCursor2.getInt(mCursor2
							.getColumnIndex(MyDataBaseAdapter.USER_GROUP_CATEGORY)));
					groups.add(group);
				}
			}

			if (mCursor2 != null) {
				mCursor2.close();
			}

		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		Log.i(TAG, "get UserInfo Form database over ");
		return groups;
	}

	@Override
	public boolean updateGroup(UserGroup group) {
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			m_MyDataBaseAdapter.updateGroup(group);
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}

	@Override
	public boolean updateGroupBase(UserGroup group) {
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			m_MyDataBaseAdapter.updateGroupBase(group);
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}

	@Override
	public boolean updateGroupCover(String uid, String gid) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			m_MyDataBaseAdapter.updateGroupCover(uid, gid);
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}

	@Override
	public UserGroup getGroupById(String gid) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_USER_GROUP + " where "
				+ MyDataBaseAdapter.USER_GROUP_GROUPID + " = '" + gid + "'";

		Cursor mCursor = null;
		try {
			mCursor = m_MyDataBaseAdapter.rawQuery(selectSQL);
			if (mCursor != null && mCursor.getCount() == 1) {
				UserGroup group = new UserGroup();
				group.setClientId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_GROUP_ID)));
				group.setGroupSid(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_GROUP_GROUPSID)));
				group.setGroupId(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_GROUP_GROUPID)));
				group.setGroupName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_GROUP_GROUPNAME)));
				group.setGroupType(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_GROUP_TYPE)));
				group.setLotype(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_GROUP_LOTYPE)));
				group.setUid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_GROUP_CREATER)));
				group.setCreateTime(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_GROUP_CREATETIME)));
				group.setLastVisitTime(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_GROUP_LASTVISITTIME)));
				group.setGroupFlag(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USER_GROUP_FLAG)));
				group.setConverFlag(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_GROUP_COVERFLAG)));
				group.setGroupLabel(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_GROUP_LABLE)));
				group.setCategory(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_GROUP_CATEGORY)));
				return group;
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return null;
	}

	@Override
	public void clearGroupData(String userid) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		String sql = "delete from " + MyDataBaseAdapter.TABLE_USER_GROUP + " where "
				+ MyDataBaseAdapter.USER_GROUP_MYUID + " = '" + userid + "'";

		try {
			m_MyDataBaseAdapter.execSql(sql);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}

	}
}
