package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.INetAddressBookUserDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.server.response.AddressMembers;

/**
 * 单位通讯录成员Dao实现
 * 
 * @author zhengjy
 * 
 *         对应表 MyDataBaseAdapter.TABLE_NET_CONTACT_USER
 */
public class NetAddressBookUserDaoImpl implements INetAddressBookUserDao {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private Context mContext;
	private MyDataBaseAdapter mDataBaseAdapter;
	private PangkerApplication application;

	public NetAddressBookUserDaoImpl() {
	}

	public NetAddressBookUserDaoImpl(Context mContext) {
		this.mContext = mContext;
		application = (PangkerApplication) mContext.getApplicationContext();
	}

	@Override
	public void insertAddressMember(AddressMembers member) {
		// TODO Auto-generated method stub
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			if (isExist(String.valueOf(member.getId())))
				return;
			ContentValues cValues = new ContentValues();
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_RID, String.valueOf(member.getId()));
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_AID, member.getBookId());
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_MOBILE, member.getMobile());
			if (member.getUid() != null) {
				cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_UID, member.getUid());
			}
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_PHONE1,
					member.getPhone1() != null ? member.getPhone1() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_PHONE2,
					member.getPhone2() != null ? member.getPhone2() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_PHONE3,
					member.getPhone3() != null ? member.getPhone3() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_EMAIL,
					member.getEmail() != null ? member.getEmail() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_ADDRESS,
					member.getAddress() != null ? member.getAddress() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_ZIPCODE,
					member.getZipcode() != null ? member.getZipcode() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_DEPARTMENT,
					member.getDepartment() != null ? member.getDepartment() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_RNAME,
					member.getRemarkname() != null ? member.getRemarkname() : "");
			long isInsertSuccess = mDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_NET_CONTACT_USER,
					cValues);
			

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}

	}

	@Override
	public void deleAddressMember(String memberId) {
		// TODO Auto-generated method stub
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			if (!isExist(memberId))
				return;
			boolean isDeleSuccess = mDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_NET_CONTACT_USER,
					MyDataBaseAdapter.NET_CONTACT_USER_RID, memberId);

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}

	}

	@Override
	public void updateAddressMember(AddressMembers member) {
		try {
			if (!isExist(String.valueOf(member.getId())))
				return;
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			ContentValues cValues = new ContentValues();
			if (member.getUid() != null) {
				cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_UID, member.getUid());
			}
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_PHONE1,
					member.getPhone1() != null ? member.getPhone1() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_PHONE2,
					member.getPhone2() != null ? member.getPhone2() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_PHONE3,
					member.getPhone3() != null ? member.getPhone3() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_EMAIL,
					member.getEmail() != null ? member.getEmail() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_ADDRESS,
					member.getAddress() != null ? member.getAddress() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_ZIPCODE,
					member.getZipcode() != null ? member.getZipcode() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_DEPARTMENT,
					member.getDepartment() != null ? member.getDepartment() : "");
			cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_RNAME,
					member.getRemarkname() != null ? member.getRemarkname() : "");

			String[] whereArgs = { String.valueOf(member.getId()) };
			boolean isUpdateSuccess = mDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_NET_CONTACT_USER,
					cValues, MyDataBaseAdapter.NET_CONTACT_USER_RID, whereArgs);
			
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}

	}

	@Override
	public void saveAddressMember(List<AddressMembers> adressMemberLists, String addressbookId) {
		// TODO Auto-generated method stub
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			for (AddressMembers addressBooks : adressMemberLists) {
				if (!isExist(String.valueOf(addressBooks.getId()))) {
					ContentValues cValues = new ContentValues();
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_RID, String.valueOf(addressBooks.getId()));
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_AID, addressbookId);
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_MOBILE, addressBooks.getMobile());
					if (addressBooks.getUid() != null) {
						cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_UID, addressBooks.getUid());
					}
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_PHONE1,
							addressBooks.getPhone1() != null ? addressBooks.getPhone1() : "");
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_PHONE2,
							addressBooks.getPhone2() != null ? addressBooks.getPhone2() : "");
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_PHONE3,
							addressBooks.getPhone3() != null ? addressBooks.getPhone3() : "");
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_EMAIL,
							addressBooks.getEmail() != null ? addressBooks.getEmail() : "");
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_ADDRESS,
							addressBooks.getAddress() != null ? addressBooks.getAddress() : "");
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_ZIPCODE,
							addressBooks.getZipcode() != null ? addressBooks.getZipcode() : "");
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_DEPARTMENT,
							addressBooks.getDepartment() != null ? addressBooks.getDepartment() : "");
					cValues.put(MyDataBaseAdapter.NET_CONTACT_USER_RNAME,
							addressBooks.getRemarkname() != null ? addressBooks.getRemarkname() : "");
					mDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_NET_CONTACT_USER, cValues);

				}
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}

	}

	@Override
	public List<AddressMembers> getAddressMemberList() {
		// TODO Auto-generated method stub
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_NET_CONTACT_USER + " order by "
				+ MyDataBaseAdapter.NET_CONTACT_USER_ID + " asc";
		Cursor mCursor = null;
		List<AddressMembers> addressMembers = new ArrayList<AddressMembers>();
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			mCursor = mDataBaseAdapter.rawQuery(selectSQL);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
					AddressMembers addressMember = new AddressMembers();
					addressMember.setId(mCursor.getLong(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_RID)));
					addressMember.setBookId(mCursor.getLong(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_AID)));
					addressMember.setUid(mCursor.getLong(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_UID)));
					addressMember.setMobile(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_MOBILE)));
					addressMember.setPhone1(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_PHONE1)));
					addressMember.setPhone2(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_PHONE2)));
					addressMember.setPhone3(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_PHONE3)));
					addressMember.setAddress(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_ADDRESS)));
					addressMember.setEmail(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_EMAIL)));
					addressMember.setZipcode(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_ZIPCODE)));
					addressMember.setDepartment(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_DEPARTMENT)));
					addressMember.setRemarkname(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.NET_CONTACT_USER_RNAME)));
					addressMembers.add(addressMember);
				}
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
		return addressMembers;
	}

	@Override
	public boolean isExist(String rId) {
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_NET_CONTACT_USER + " where "
				+ MyDataBaseAdapter.NET_CONTACT_USER_RID + "='" + rId + "'";
		Cursor mCursor = null;
		boolean isExist = false;
		MyDataBaseAdapter mDataBaseAdapter = null;
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			mCursor = mDataBaseAdapter.rawQuery(selectSQL);
			isExist = mCursor != null && mCursor.getCount() > 0;
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
		return isExist;
	}

	@Override
	public Integer getAddressMemberNum(String bookId) {
		// TODO Auto-generated method stub
		String selectSQL = "select COUNT(*) from " + MyDataBaseAdapter.TABLE_NET_CONTACT_USER + " where "
				+ MyDataBaseAdapter.NET_CONTACT_USER_AID + "='" + bookId + "'";
		Cursor mCursor = null;
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			mCursor = mDataBaseAdapter.rawQuery(selectSQL);

			if (mCursor != null) {
				return mCursor.getCount();
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
		return 0;
	}

	@Override
	public void deleAddressMemberByBookid(String bookId) {
		// TODO Auto-generated method stub
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			boolean isDeleSuccess = mDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_NET_CONTACT_USER,
					MyDataBaseAdapter.NET_CONTACT_USER_AID, bookId);
			
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
	}

	@Override
	public void deleAddressMemberByUserid(String userId) {
		// TODO Auto-generated method stub
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();

			boolean isDeleSuccess = mDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_NET_CONTACT_USER,
					MyDataBaseAdapter.NET_CONTACT_USER_UID, userId);
			
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
	}

}
