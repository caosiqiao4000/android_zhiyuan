package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.wachoo.pangker.db.IUserStatusDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.server.response.UserStatus;

public class UserStatusDaoImpl implements IUserStatusDao {
	
	private String TAG = "CallPhoneDaoImpl";// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private MyDataBaseAdapter dbAdapter;
	private Context mContext;

	public UserStatusDaoImpl(Context context) {
		this.mContext = context;
	}

	@Override
	public void insertUserStatus(String userId, String resId, String time,
			List<UserStatus> userStatus) {
		try {
			dbAdapter = new MyDataBaseAdapter(this.mContext);
			dbAdapter.open();
			for (UserStatus userstatu : userStatus) {
				ContentValues cValues = new ContentValues();
				cValues.put(MyDataBaseAdapter.USERSTATUS_USERID, userId);
				cValues.put(MyDataBaseAdapter.USERSTATUS_RESID, resId);
				cValues.put(MyDataBaseAdapter.USERSTATUS_TIME, time);
				cValues.put(MyDataBaseAdapter.USERSTATUS_BE_USERID,String.valueOf(userstatu.getUid()));
				cValues.put(MyDataBaseAdapter.USERSTATUS_USERNAME,userstatu.getNickname());
				cValues.put(MyDataBaseAdapter.USERSTATUS_STATUS,userstatu.getStatus());
				dbAdapter.insertData(MyDataBaseAdapter.TABLE_USERSTATUS, cValues);
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (dbAdapter != null) {
				dbAdapter.close();
			}
		}
	}

	@Override
	public void clearUser(String uid) {
		// TODO Auto-generated method stub
		String deleteSql = "delete from " +  MyDataBaseAdapter.TABLE_USERSTATUS + " where " + MyDataBaseAdapter.USERSTATUS_USERID + " = '"
		+ uid + "'"; 
		
		try {
			dbAdapter = new MyDataBaseAdapter(this.mContext);
			dbAdapter.open();
			dbAdapter.execSql(deleteSql);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (dbAdapter != null) {
				dbAdapter.close();
				dbAdapter = null;
			}
		}
		
	}

	@Override
	public List<UserStatus> getUserStatus(String userid, String resId,
			String time) {
		// TODO Auto-generated method stub
		List<UserStatus> userStatusList = new ArrayList<UserStatus>();
		Cursor mCursor = null;
		
		try {
			this.dbAdapter = new MyDataBaseAdapter(mContext);
			dbAdapter.open();
			String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_USERSTATUS + " where " + MyDataBaseAdapter.USERSTATUS_USERID + " = '"
			+ userid + "' and " + MyDataBaseAdapter.USERSTATUS_RESID + " = '" + resId + "' and " + MyDataBaseAdapter.USERSTATUS_TIME +" = '" + time + "'"; 
			mCursor  = dbAdapter.rawQuery(selectSQL);
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return userStatusList;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				UserStatus mUserStatus = new UserStatus();
				String beUserId = mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERSTATUS_BE_USERID));
				mUserStatus.setUid(Long.parseLong(beUserId));
				String userName = mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERSTATUS_USERNAME));
				mUserStatus.setNickname(userName);
				int status = mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERSTATUS_STATUS));
				mUserStatus.setStatus(status);
				userStatusList.add(mUserStatus);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
			if (dbAdapter != null) {
				dbAdapter.close();
			}
		}
		return userStatusList;
	}

	@Override
	public boolean isExist(String userId, String resId, String time) {
		// TODO Auto-generated method stub
		
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_USERSTATUS + " where " + MyDataBaseAdapter.USERSTATUS_USERID + " = '"
		+ userId + "' and " + MyDataBaseAdapter.USERSTATUS_RESID + " = '" + resId + "' and " + MyDataBaseAdapter.USERSTATUS_TIME +" = '" + time + "'";
        Cursor mCursor = null;

        try {
	         dbAdapter = new MyDataBaseAdapter(this.mContext);
	         dbAdapter.open();
	         mCursor = dbAdapter.rawQuery(selectSQL);
	         return mCursor != null && mCursor.getCount() > 0;
        } catch (Exception e) {
	         Log.e(TAG, e.getMessage());
        } finally {
	         if (mCursor != null) {
	         mCursor.close();
		     mCursor = null;
	       }
         }
      return false;
	}

}
