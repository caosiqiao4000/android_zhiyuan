package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.util.Util;

/**
 * @author Administrator 说明：parentId 与本地自增长的Id没有关系
 */
public class ResDaoImpl implements IResDao {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Cursor mCursor;
	private Context context;
	private PangkerApplication application;

	public ResDaoImpl(Context context) {
		super();
		this.context = context;
		application = (PangkerApplication) context.getApplicationContext();
	}

	private boolean existDirInfo(DirInfo resinfo) {
		try {

			m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
			m_MyDataBaseAdapter.open();
			if (!m_MyDataBaseAdapter.checkExistById(MyDataBaseAdapter.TABLE_RES, MyDataBaseAdapter.RES_ID,
					resinfo.getDirId()))
				return false;
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			close();
		}
		return true;
	}

	@Override
	public boolean deleteResInfo(DirInfo resinfo) {
		// TODO Auto-generated method stub
		if (!existDirInfo(resinfo)) {
			return false;
		}
		DirInfo pDirInfo = getResById(resinfo.getParentDirId());
		if (pDirInfo == null) {
			return false;
		} else {
			pDirInfo.setFileCount(pDirInfo.getFileCount() - 1);
			updateResInfo(pDirInfo);
		}
		boolean result = false;
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		try {
			result = m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_RES, MyDataBaseAdapter.RES_ID,
					resinfo.getDirId());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return result;
	}

	@Override
	public DirInfo getResById(String id) {
		// TODO Auto-generated method stub
		DirInfo resInfo = null;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
			m_MyDataBaseAdapter.open();
			mCursor = m_MyDataBaseAdapter.getAllDatas(MyDataBaseAdapter.TABLE_RES, MyDataBaseAdapter.RES_ID,
					id);
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				resInfo = getDirInfoByCursor();
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			close();
		}

		return resInfo;
	}

	@Override
	public List<DirInfo> getResByIndex(String userId) {
		// TODO Auto-generated method stub
		List<DirInfo> resInfos = new ArrayList<DirInfo>();
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTopRess(MyDataBaseAdapter.RES_USERID, userId);
			if (mCursor == null)
				return resInfos;
			if (mCursor.getCount() == 0)
				return resInfos;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				DirInfo resInfo = getDirInfoByCursor();
				resInfos.add(resInfo);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return resInfos;
	}

	@Override
	public long saveResInfo(DirInfo resinfo) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.RES_ID, resinfo.getDirId());
		initialValues.put(MyDataBaseAdapter.RES_USERID, resinfo.getUid());
		initialValues.put(MyDataBaseAdapter.RES_PARENT_ID, resinfo.getParentDirId());
		initialValues.put(MyDataBaseAdapter.RES_PATH, resinfo.getPath());
		initialValues.put(MyDataBaseAdapter.RES_NAME, resinfo.getDirName());
		initialValues.put(MyDataBaseAdapter.RES_TYPE, resinfo.getResType());
		initialValues.put(MyDataBaseAdapter.RES_LOCALID, resinfo.getLocalid());
		initialValues.put(MyDataBaseAdapter.RES_ROOT, resinfo.getVisitType());
		initialValues.put(MyDataBaseAdapter.RES_ISFILE, resinfo.getIsfile());
		initialValues.put(MyDataBaseAdapter.RES_LATLNG, resinfo.getLatlng());
		initialValues.put(MyDataBaseAdapter.RES_GROUP, resinfo.getVisitGroups());
		initialValues.put(MyDataBaseAdapter.RES_DEAL, resinfo.getDeal());
		initialValues.put(MyDataBaseAdapter.RES_FORMAT, resinfo.getDirFormat());
		initialValues.put(MyDataBaseAdapter.RES_APPRAISEMENT, resinfo.getAppraisement());
		initialValues.put(MyDataBaseAdapter.RES_FILECOUNT, resinfo.getFileCount());
		initialValues.put(MyDataBaseAdapter.RES_CREATETIME, resinfo.getCreatetime());
		initialValues.put(MyDataBaseAdapter.RES_REMARK, resinfo.getRemark());
		try {
			long _id = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_RES, initialValues);
			return _id;
		} catch (Exception e) {
			logger.error(TAG, e);
			return -1;
		} finally {
			close();
		}
	}

	@Override
	public List<DirInfo> getDirByType(String userId, int dealkey, int type) {
		List<DirInfo> resInfos = new ArrayList<DirInfo>();
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_RES, new String[] {
					MyDataBaseAdapter.RES_USERID, MyDataBaseAdapter.RES_DEAL, MyDataBaseAdapter.RES_TYPE,
					MyDataBaseAdapter.RES_ISFILE },
					new String[] { userId, String.valueOf(dealkey), String.valueOf(type), "1" },
					MyDataBaseAdapter.RES_CREATETIME, true);
			if (mCursor == null)
				return resInfos;
			if (mCursor.getCount() == 0)
				return resInfos;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				DirInfo resInfo = getDirInfoByCursor();
				resInfos.add(resInfo);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return resInfos;
	}

	@Override
	public boolean updateResInfo(DirInfo resinfo) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.RES_NAME, resinfo.getDirName());
		initialValues.put(MyDataBaseAdapter.RES_DEAL, resinfo.getDeal());
		initialValues.put(MyDataBaseAdapter.RES_FILECOUNT, resinfo.getFileCount());
		initialValues.put(MyDataBaseAdapter.RES_REMARK, resinfo.getRemark());
		initialValues.put(MyDataBaseAdapter.RES_ROOT, resinfo.getVisitType());
		initialValues.put(MyDataBaseAdapter.RES_GROUP, resinfo.getVisitGroups());
		try {
			return m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_RES, initialValues,
					MyDataBaseAdapter.RES_CID, new String[] { String.valueOf(resinfo.get_id()) });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close();
		}
	}

	public boolean isExitByDeal(String userId, String resid, int dealkey) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_RES, new String[] {
					MyDataBaseAdapter.RES_USERID, MyDataBaseAdapter.RES_ID, MyDataBaseAdapter.RES_DEAL },
					new String[] { userId, String.valueOf(resid), String.valueOf(dealkey) });
			if (mCursor == null)
				return false;
			if (mCursor.getCount() > 0)
				return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close();
		}
		return false;
	}

	@Override
	public boolean isSelected(String userId, String path, int type) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_RES, new String[] {
					MyDataBaseAdapter.RES_USERID, MyDataBaseAdapter.RES_PATH, MyDataBaseAdapter.RES_DEAL,
					MyDataBaseAdapter.RES_TYPE },
					new String[] { userId, path, String.valueOf(DirInfo.DEAL_SELECT), String.valueOf(type) });
			if (mCursor == null)
				return false;
			if (mCursor.getCount() > 0)
				return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close();
		}
		return false;
	}

	private DirInfo getDirInfoByCursor() {
		DirInfo resInfo = new DirInfo();
		resInfo.set_id(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RES_CID)));
		resInfo.setDirId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_ID)));
		resInfo.setVisitGroups(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_GROUP)));
		resInfo.setCreatetime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_CREATETIME)));
		resInfo.setParentDirId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_PARENT_ID)));
		resInfo.setPath(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_PATH)));
		resInfo.setAppraisement(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_APPRAISEMENT)));
		resInfo.setDirName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_NAME)));
		resInfo.setUid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_USERID)));
		resInfo.setVisitType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RES_ROOT)));
		resInfo.setDirFormat(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_FORMAT)));
		resInfo.setLocalid(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RES_LOCALID)));
		resInfo.setResType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RES_TYPE)));
		resInfo.setLatlng(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RES_LATLNG)));
		resInfo.setIsfile(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RES_ISFILE)));
		resInfo.setDeal(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RES_DEAL)));
		resInfo.setFileCount(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RES_FILECOUNT)));
		resInfo.setRemark(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RES_REMARK)));
		return resInfo;
	}

	private void close() {
		if (m_MyDataBaseAdapter != null) {
			m_MyDataBaseAdapter.close();
			m_MyDataBaseAdapter = null;
		}
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}
	}

	@Override
	public boolean saveSelectResInfo(DirInfo resinfo) {
		//统一设置创建时间
		resinfo.setCreatetime(Util.getSysNowTime());
		
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.RES_ID, resinfo.getDirId());
		initialValues.put(MyDataBaseAdapter.RES_USERID, resinfo.getUid());
		initialValues.put(MyDataBaseAdapter.RES_PARENT_ID, resinfo.getParentDirId());
		initialValues.put(MyDataBaseAdapter.RES_PATH, resinfo.getPath());
		initialValues.put(MyDataBaseAdapter.RES_NAME, resinfo.getDirName());
		initialValues.put(MyDataBaseAdapter.RES_TYPE, resinfo.getResType());
		initialValues.put(MyDataBaseAdapter.RES_LOCALID, resinfo.getLocalid());
		initialValues.put(MyDataBaseAdapter.RES_ROOT, resinfo.getVisitType());
		initialValues.put(MyDataBaseAdapter.RES_ISFILE, resinfo.getIsfile());
		initialValues.put(MyDataBaseAdapter.RES_LATLNG, resinfo.getLatlng());
		initialValues.put(MyDataBaseAdapter.RES_GROUP, resinfo.getVisitGroups());
		initialValues.put(MyDataBaseAdapter.RES_DEAL, resinfo.getDeal());
		initialValues.put(MyDataBaseAdapter.RES_FORMAT, resinfo.getDirFormat());
		initialValues.put(MyDataBaseAdapter.RES_APPRAISEMENT, resinfo.getAppraisement());
		initialValues.put(MyDataBaseAdapter.RES_FILECOUNT, resinfo.getFileCount());
		initialValues.put(MyDataBaseAdapter.RES_CREATETIME, resinfo.getCreatetime());
		initialValues.put(MyDataBaseAdapter.RES_REMARK, resinfo.getRemark());
		
		try {
			long _id = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_RES, initialValues);
			System.out.println("resdao -- insert:_id = " + _id);
			if (_id > 0) {
				resinfo.set_id((int) _id);
				application.putListDir(resinfo.getResType(), resinfo);
				return true;
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return false;
	}

	@Override
	public boolean deleteSelectResInfo(DirInfo resinfo) {
		// TODO Auto-generated method stub
		boolean result = false;
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		try {
			result = m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_RES, MyDataBaseAdapter._ID,
					resinfo.get_id());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return result;
	}

	@Override
	public DirInfo getResByPath(String path, String userId) {
		// TODO Auto-generated method stub
		DirInfo resInfo = null;
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_RES, new String[] {
					MyDataBaseAdapter.RES_USERID, MyDataBaseAdapter.RES_PATH, MyDataBaseAdapter.RES_DEAL },
					new String[] { userId, path, String.valueOf(DirInfo.DEAL_SELECT) });
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				resInfo = getDirInfoByCursor();
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return resInfo;
	}

}
