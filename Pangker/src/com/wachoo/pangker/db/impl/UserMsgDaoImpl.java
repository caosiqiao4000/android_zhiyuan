package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.util.Util;

public class UserMsgDaoImpl implements IUserMsgDao {

	private MyDataBaseAdapter mdb;
	private Context context;
	private String myUserID;
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	public UserMsgDaoImpl(Context context) {
		this.context = context;
		myUserID = ((PangkerApplication) context.getApplicationContext()).getMyUserID();
	}

	@Override
	public boolean deleteUserMsgById(int id) {
		boolean flag = false;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			flag = mdb.deleteData(MyDataBaseAdapter.TABLE_USERMSG, "_id", id);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
		return flag;
	}

	@Override
	public boolean deleteUserMsgByUid(String myuid, String himuid) {
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			String sql = "delete from " + MyDataBaseAdapter.TABLE_USERMSG + " where " + MyDataBaseAdapter.USERMSG_MYUID
					+ " = '" + myuid + "' and " + MyDataBaseAdapter.USERMSG_HIMUID + " = '" + himuid + "'";
			return mdb.execSql(sql);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
		return false;
	}

	@Override
	public List<ChatMessage> getUserMsgs(String myuid, String himuid) {
		List<ChatMessage> msgs = new ArrayList<ChatMessage>();
		Cursor mCursor = null;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			mCursor = mdb.getUserMsg(Util.trimUserIDDomain(myuid), Util.trimUserIDDomain(himuid));
			if (mCursor == null) {
				return msgs;
			}
			if (!mCursor.moveToFirst()) {
				return msgs;
			}
			do {
				ChatMessage msg = new ChatMessage();
				msg.setId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_ID)));
				msg.setMyUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_MYUID)));
				msg.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_HIMUID)));
				msg.setDirection(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_DIRECTION)));
				msg.setContent(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_CONTENT)));
				msg.setTime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_TALKTIME)));
				msg.setStatus(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_STATUS)));
				msg.setMsgType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_TYPE)));
				msg.setFilepath(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_FILEPATH)));
				if (msg.getDirection().equals(ChatMessage.MSG_TO_ME)) {
					Cursor userCursor = mdb.getUserItemByUserID(MyDataBaseAdapter.USER_ID,
							Util.trimUserIDDomain(himuid));
					if (userCursor == null)
						continue;
					if (userCursor.getCount() == 0)
						continue;
					userCursor.moveToLast();
					msg.setUserName(userCursor.getString(userCursor.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
					if (userCursor.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME) != -1) {
						msg.setrName(userCursor.getString(userCursor
								.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME)));
					}
					if (userCursor != null) {
						userCursor.close();
					}
				}

				msgs.add(msg);
			} while (mCursor.moveToNext());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mdb != null) {
				mdb.close();
			}
		}
		return msgs;
	}

	@Override
	public long saveUserMsg(ChatMessage msg) {
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			ContentValues cv = new ContentValues();
			cv.put(MyDataBaseAdapter.USERMSG_MYUID, Util.trimUserIDDomain(msg.getMyUserId()));
			cv.put(MyDataBaseAdapter.USERMSG_HIMUID, Util.trimUserIDDomain(msg.getUserId()));
			cv.put(MyDataBaseAdapter.USERMSG_DIRECTION, msg.getDirection());
			cv.put(MyDataBaseAdapter.USERMSG_CONTENT, msg.getContent());
			cv.put(MyDataBaseAdapter.USERMSG_TALKTIME, msg.getTime());
			cv.put(MyDataBaseAdapter.USERMSG_FILEPATH, msg.getFilepath());
			cv.put(MyDataBaseAdapter.USERMSG_STATUS, msg.getStatus());
			cv.put(MyDataBaseAdapter.USERMSG_TYPE, msg.getMsgType());
			cv.put(MyDataBaseAdapter.USERMSG_RECV, msg.getMessageRevc());
			return mdb.insertData(MyDataBaseAdapter.TABLE_USERMSG, cv);
		} catch (Exception e) {
			logger.error(TAG, e);
			return -1;
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
	}

	@Override
	public List<ChatMessage> getUserMessages() {
		// TODO Auto-generated method stub
		List<ChatMessage> msgs = new ArrayList<ChatMessage>();
		Cursor mCursor = null;
		Cursor userCursor = null;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			mCursor = mdb.getUserList(myUserID);
			if (mCursor == null) {
				return msgs;
			}
			if (!mCursor.moveToFirst()) {
				return msgs;
			}
			do {

				ChatMessage msg = new ChatMessage();
				msg.setId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_ID)));
				msg.setMyUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_MYUID)));
				msg.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_HIMUID)));
				msg.setDirection(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_DIRECTION)));
				msg.setContent(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_CONTENT)));
				msg.setTime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_TALKTIME)));
				msg.setStatus(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_STATUS)));
				msg.setFilepath(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_FILEPATH)));
				msg.setMsgType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_TYPE)));
				if (userCursor != null && !userCursor.isClosed()) {
					userCursor.close();
				}
				userCursor = mdb.getUserItemByUserID(MyDataBaseAdapter.USER_ID, Util.trimUserIDDomain(msg.getUserId()));
				if (userCursor == null)
					continue;
				if (userCursor.getCount() == 0) {
					msgs.add(msg);
					continue;
				}
				userCursor.moveToLast();
				msg.setIconType(userCursor.getInt(userCursor.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));

				// msg.setIcon(Util.BytestoBitmap(userCursor.getBlob(userCursor.getColumnIndex(mdb.USER_NET_PORTARIT))));
				msg.setUserName(userCursor.getString(userCursor.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
				if (userCursor.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME) != -1) {
					msg.setrName(userCursor.getString(userCursor.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME)));
				}
				msg.setSign(userCursor.getString(userCursor.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));
				if (userCursor != null) {
					userCursor.close();
				}
				// msg.setHim_nickname(userCursor.getString(userCursor.getColumnIndex(mdb.USER_NICKNAME)));
				msgs.add(msg);
			} while (mCursor.moveToNext());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (userCursor != null) {
				userCursor.close();
			}
			if (mdb != null) {
				mdb.close();
			}
		}
		return msgs;
	}

	@Override
	public boolean updateUserMsgStatus(ChatMessage msg) {
		// TODO Auto-generated method stub
		mdb = new MyDataBaseAdapter(context);
		mdb.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.USERMSG_STATUS, msg.getStatus());
		initialValues.put(MyDataBaseAdapter.USERMSG_FILEPATH, msg.getFilepath());
		try {
			return mdb.updateData(MyDataBaseAdapter.TABLE_USERMSG, initialValues, MyDataBaseAdapter.USERMSG_ID,
					new String[] { String.valueOf(msg.getId()) });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
	}

	@Override
	public int getMsgCount(String myuid, String himuid) {
		Cursor mCursor = null;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			mCursor = mdb.getUserMsg(Util.trimUserIDDomain(myuid), Util.trimUserIDDomain(himuid));
			if (mCursor != null) {
				return mCursor.getCount();
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mdb != null) {
				mdb.close();
			}
		}
		return 0;
	}

	@Override
	public List<ChatMessage> getUserMsgsByLimit(String myuid, String himuid, int length) {
		int start = getMsgCount(myuid, himuid) - length;
		return getUserMsgsByLimit(myuid, himuid, start, length);
	}

	@Override
	public List<ChatMessage> getUserMsgsByLimit(String myuid, String himuid, int startLimit, int length) {
		// TODO Auto-generated method stub
		List<ChatMessage> msgs = new ArrayList<ChatMessage>();
		Cursor mCursor = null;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			mCursor = mdb.getUserMsgByLimit(Util.trimUserIDDomain(myuid), Util.trimUserIDDomain(himuid), startLimit,
					length);
			if (mCursor == null) {
				return msgs;
			}
			if (!mCursor.moveToFirst()) {
				return msgs;
			}
			do {
				ChatMessage msg = new ChatMessage();
				msg.setId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_ID)));
				msg.setMyUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_MYUID)));
				msg.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_HIMUID)));
				msg.setDirection(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_DIRECTION)));
				msg.setContent(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_CONTENT)));
				msg.setTime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_TALKTIME)));
				msg.setStatus(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_STATUS)));
				msg.setMsgType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_TYPE)));
				msg.setFilepath(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_FILEPATH)));
				msg.setMessageRevc(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USERMSG_RECV)));
				if (msg.getDirection().equals(ChatMessage.MSG_TO_ME)) {
					Cursor userCursor = mdb.getUserItemByUserID(MyDataBaseAdapter.USER_ID,
							Util.trimUserIDDomain(himuid));
					if (userCursor == null)
						continue;
					if (userCursor.getCount() == 0)
						continue;
					userCursor.moveToLast();
					msg.setUserName(userCursor.getString(userCursor.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
					if (userCursor.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME) != -1) {
						msg.setrName(userCursor.getString(userCursor
								.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME)));
					}
					if (userCursor != null) {
						userCursor.close();
					}
				}

				msgs.add(msg);
			} while (mCursor.moveToNext());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mdb != null) {
				mdb.close();
			}
		}
		return msgs;
	}

	@Override
	public boolean messageReceived(String messageID) {
		// TODO Auto-generated method stub
		mdb = new MyDataBaseAdapter(context);
		mdb.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.USERMSG_RECV, 1);

		try {
			return mdb.updateData(MyDataBaseAdapter.TABLE_USERMSG, initialValues, MyDataBaseAdapter.USERMSG_ID,
					new String[] { String.valueOf(messageID) });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
	}

	@Override
	public boolean updateMessageReceived(ChatMessage message) {
		// TODO Auto-generated method stub
		mdb = new MyDataBaseAdapter(context);
		mdb.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.USERMSG_RECV, message.getMessageRevc());

		try {
			return mdb.updateData(MyDataBaseAdapter.TABLE_USERMSG, initialValues, MyDataBaseAdapter.USERMSG_ID,
					new String[] { String.valueOf(message.getId()) });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
	}
}
