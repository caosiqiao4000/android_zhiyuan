package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.db.ITipsDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.MessageTip;

public class TipDaoImpl implements ITipsDao {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Cursor mCursor;
	private Context context;
	
	public TipDaoImpl(Context context) {
		super();
		this.context = context;
	}
	
	private MessageTip getMessageTipByCursor() {
		MessageTip messageTip = new MessageTip();
		messageTip.setId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_ID)));
		messageTip.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_USERID)));
		messageTip.setFromId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_FROM)));
		messageTip.setFromName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_FROMNAME)));
		messageTip.setResId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_RESID)));
		messageTip.setTipType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_TYPE)));
		messageTip.setResType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_RESTYPE)));
		messageTip.setResName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_RESNAME)));
		messageTip.setContent(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_CONTENT)));
		messageTip.setCreateTime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.TIP_TIME)));
		
		return messageTip;
	}
	
	private void close() {
		// TODO Auto-generated method stub
		if(mCursor != null){
			mCursor.close();
		} 
		if(m_MyDataBaseAdapter != null){
			m_MyDataBaseAdapter.close();
		}

	}

	@Override
	public long saveMessageTip(MessageTip messageTip) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.TIP_USERID, messageTip.getUserId());
		initialValues.put(MyDataBaseAdapter.TIP_FROM, messageTip.getFromId());
		initialValues.put(MyDataBaseAdapter.TIP_FROMNAME, messageTip.getFromName());
		initialValues.put(MyDataBaseAdapter.TIP_RESID, messageTip.getResId());
		initialValues.put(MyDataBaseAdapter.TIP_TYPE, messageTip.getTipType());
		initialValues.put(MyDataBaseAdapter.TIP_RESNAME, messageTip.getResName());
		initialValues.put(MyDataBaseAdapter.TIP_RESTYPE, messageTip.getResType());
		initialValues.put(MyDataBaseAdapter.TIP_CONTENT, messageTip.getContent());
		initialValues.put(MyDataBaseAdapter.TIP_TIME, messageTip.getCreateTime());
		try {
			return m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_TIPINFO, initialValues);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return -1;
	}

	@Override
	public List<MessageTip> getMessageTips(String userId) {
		// TODO Auto-generated method stub
		List<MessageTip> messageTips = new ArrayList<MessageTip>();
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getAllDatas(MyDataBaseAdapter.TABLE_TIPINFO, MyDataBaseAdapter.TIP_USERID, 
					userId, MyDataBaseAdapter.TIP_TIME, true);
			if (mCursor == null)
				return messageTips;
			if (mCursor.getCount() == 0)
				return messageTips;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				MessageTip oneTip = getMessageTipByCursor();
				messageTips.add(oneTip);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return messageTips;
	}

	@Override
	public boolean delMessageTip(MessageTip messageTip) {
		// TODO Auto-generated method stub
		boolean result = false;
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			result = m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_TIPINFO, MyDataBaseAdapter.TIP_ID,
					messageTip.getId());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return result;
	}

	@Override
	public boolean updateMessageTip(MessageTip messageTip) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean clearMessageTips(String userId) {
		// TODO Auto-generated method stub
		boolean result = false;
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			result = m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_TIPINFO, MyDataBaseAdapter.TIP_USERID, userId);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return result;
	}

}
