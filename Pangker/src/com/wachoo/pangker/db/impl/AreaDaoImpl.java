package com.wachoo.pangker.db.impl;

import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.db.IAreaDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;

public class AreaDaoImpl implements IAreaDao {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	private MyDataBaseAdapter dbAdapter;
	private Context context;

	public AreaDaoImpl(Context context) {
		this.context = context;
	}

	@Override
	public String getAreaIDByName(String areaName) {
		// TODO Auto-generated method stub
		Cursor mCursor = null;
		String areaID = "";
		try {
			dbAdapter = new MyDataBaseAdapter(this.context);
			dbAdapter.open();

			mCursor = dbAdapter.getAreaIDByName(areaName);
			if (mCursor == null)
				return areaID;
			if (mCursor.getCount() == 0)
				return areaID;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				areaID = mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.AREA_AREAID));
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (dbAdapter != null) {
				dbAdapter.close();
			}
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return areaID;
	}

	@Override
	public String getAreaNameByID(String areaID) {
		// TODO Auto-generated method stub
		Cursor mCursor = null;
		String areaName = "";
		try {
			dbAdapter = new MyDataBaseAdapter(this.context);
			dbAdapter.open();

			mCursor = dbAdapter.getAreaNameByID(areaID);
			if (mCursor == null)
				return areaName;
			if (mCursor.getCount() == 0)
				return areaName;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				areaName = mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.AREA_AREANAME));
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (dbAdapter != null) {
				dbAdapter.close();
			}
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return areaName;
	}

}
