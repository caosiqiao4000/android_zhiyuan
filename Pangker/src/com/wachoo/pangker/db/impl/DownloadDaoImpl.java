package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IDownloadDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.util.Util;

public class DownloadDaoImpl implements IDownloadDao {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Cursor mCursor;
	private String userId;
	private Context context;

	public DownloadDaoImpl(Context context) {
		super();
		this.context = context;
		userId = ((PangkerApplication) context.getApplicationContext()).getMyUserID();
	}

	private DownloadJob getDownloadInfo() {
		DownloadJob downloadInfo = new DownloadJob();
		downloadInfo.setId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.DOWN_ID)));
		downloadInfo.setmResID(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.DOWN_RES_ID)));
		downloadInfo.setUserid(userId);
		downloadInfo.setDwonTime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.DOWN_TIME)));
		downloadInfo.setDoneSize(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.DONE_SIZE)));
		downloadInfo.setEndPos(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.END_POS)));
		downloadInfo.setFileName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.FILE_NAME)));
		// >>>>>>>文件真实名称
		downloadInfo.setSaveFileName(mCursor.getString(mCursor
				.getColumnIndex(MyDataBaseAdapter.SAVE_FILE_NAME)));
		downloadInfo.setSavePath(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.SAVE_PATH)));
		downloadInfo.setStatus(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.DOWN_STATUS)));
		downloadInfo.setType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.DOWN_TYPE)));
		downloadInfo.setUrl(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.DOWNLOAD_URL)));
		downloadInfo.setDownloadType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.DOWN_NET_TYPE)));
		return downloadInfo;
	}

	@Override
	public void delete(String url) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.DOWNLOAD_TABLE, MyDataBaseAdapter.DOWNLOAD_URL,
					url);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
	}

	/**
	 * 根据文件网络地址获取相应下载信息
	 */
	@Override
	public DownloadJob getDownloadInfo(String fileUrl) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.DOWNLOAD_TABLE, new String[] {
					MyDataBaseAdapter.DOWNLOAD_URL, MyDataBaseAdapter.DOWN_USERID }, new String[] { fileUrl,
					userId });
			if (mCursor == null) {
				return null;
			}
			if (mCursor.getCount() <= 0) {
				return null;
			}
			mCursor.moveToFirst();
			return getDownloadInfo();
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			close();
		}
	}

	/**
	 * 根据文件网络地址获取相应下载信息
	 */
	@Override
	public List<DownloadJob> getDownloadInfos(String fileUrl) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		List<DownloadJob> downloadInfos = new ArrayList<DownloadJob>();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.DOWNLOAD_TABLE, new String[] {
					MyDataBaseAdapter.DOWNLOAD_URL, MyDataBaseAdapter.DOWN_USERID }, new String[] { fileUrl,
					userId });
			if (mCursor == null || mCursor.getCount() <= 0) {
				return null;
			}
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				downloadInfos.add(getDownloadInfo());
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			close();
		}
		return downloadInfos;
	}

	/**
	 * 获取所有下载文件信息
	 */
	@Override
	public List<DownloadJob> getDownloadInfos() {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		List<DownloadJob> downloadInfos = new ArrayList<DownloadJob>();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.DOWNLOAD_TABLE,
					new String[] { MyDataBaseAdapter.DOWN_USERID }, new String[] { userId });
			if (mCursor == null) {
				return downloadInfos;
			}
			if (mCursor.getCount() <= 0) {
				return downloadInfos;
			}
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				downloadInfos.add(getDownloadInfo());
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return downloadInfos;
		} finally {
			close();
		}
		return downloadInfos;
	}

	/**
	 * 判断下载信息是否存在
	 */
	@Override
	public boolean isExitDownloadInfos(String fileUrl) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.DOWNLOAD_TABLE, new String[] {
					MyDataBaseAdapter.DOWNLOAD_URL, MyDataBaseAdapter.DOWN_USERID }, new String[] { fileUrl,
					userId });
			if (mCursor == null) {
				return false;
			}
			if (mCursor.getCount() > 0) {
				return true;
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return false;
	}

	/**
	 * 保存 下载的具体信息,如果已经下载过了，要覆盖原有的下载信息
	 */
	@Override
	public long saveDownloadInfo(DownloadJob info) {
		// TODO Auto-generated method stub
		DownloadJob downloadedInfo = getDownloadInfo(info.getUrl());
		if (downloadedInfo != null) {
			downloadedInfo.setDoneSize(0);
			downloadedInfo.setDwonTime(Util.getSysNowTime());
			downloadedInfo.setStatus(DownloadJob.DOWNLOAD_INIT);

			updataDownloadInfo(downloadedInfo);
			return downloadedInfo.getId();
		}
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.THREAD_ID, 0);
		initialValues.put(MyDataBaseAdapter.END_POS, info.getEndPos());
		initialValues.put(MyDataBaseAdapter.DOWN_RES_ID, info.getmResID());
		initialValues.put(MyDataBaseAdapter.DONE_SIZE, info.getDoneSize());
		initialValues.put(MyDataBaseAdapter.DOWN_STATUS, info.getStatus());
		initialValues.put(MyDataBaseAdapter.DOWNLOAD_URL, info.getUrl());
		initialValues.put(MyDataBaseAdapter.DOWN_USERID, userId);
		initialValues.put(MyDataBaseAdapter.FILE_NAME, info.getFileName());
		initialValues.put(MyDataBaseAdapter.DOWN_NET_TYPE, info.getDownloadType());
		// >>>>>>文件真实名称
		initialValues.put(MyDataBaseAdapter.SAVE_FILE_NAME, info.getSaveFileName());
		initialValues.put(MyDataBaseAdapter.SAVE_PATH, info.getSavePath());
		initialValues.put(MyDataBaseAdapter.DOWN_TYPE, info.getType());
		initialValues.put(MyDataBaseAdapter.DOWN_TIME, info.getDwonTime());

		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			return m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.DOWNLOAD_TABLE, initialValues);
		} catch (Exception e) {
			logger.error(TAG, e);
			return -1;
		} finally {
			close();
		}
	}

	/**
	 * 更新下载信息
	 */
	@Override
	public void updataDownloadInfo(DownloadJob downloadInfo) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.SAVE_PATH, downloadInfo.getSavePath());
		initialValues.put(MyDataBaseAdapter.DOWN_STATUS, downloadInfo.getStatus());
		initialValues.put(MyDataBaseAdapter.DONE_SIZE, downloadInfo.getDoneSize());
		initialValues.put(MyDataBaseAdapter.DOWN_TIME, downloadInfo.getDwonTime());
		initialValues.put(MyDataBaseAdapter.FILE_NAME, downloadInfo.getFileName());
		initialValues.put(MyDataBaseAdapter.DOWN_NET_TYPE, downloadInfo.getDownloadType());
		// >>>>>>文件真实名称
		initialValues.put(MyDataBaseAdapter.SAVE_FILE_NAME, downloadInfo.getSaveFileName());
		try {
			m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.DOWNLOAD_TABLE, initialValues,
					MyDataBaseAdapter.DOWN_ID, new String[] { String.valueOf(downloadInfo.getId()) });
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
	}

	/**
	 * 更新下载信息状态
	 */
	@Override
	public void updataDownloadStatus(long id, int status) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.DOWN_STATUS, status);
		try {
			m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.DOWNLOAD_TABLE, initialValues,
					MyDataBaseAdapter.DOWN_ID, new String[] { String.valueOf(id) });
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
	}

	private void close() {
		if (m_MyDataBaseAdapter != null) {
			m_MyDataBaseAdapter.close();
			m_MyDataBaseAdapter = null;
		}
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.DOWNLOAD_TABLE, MyDataBaseAdapter.DOWN_USERID,
					userId);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
	}

	@Override
	public void deleteDownloadJobsBYStatus(int status) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		try {
			m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.DOWNLOAD_TABLE, MyDataBaseAdapter.DOWN_STATUS,
					String.valueOf(status));
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
	}

	@Override
	public void updateDownloadDoneSize(int downloadID, long doneSize) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.DONE_SIZE, doneSize);
		try {
			m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.DOWNLOAD_TABLE, initialValues,
					MyDataBaseAdapter.DOWN_ID, new String[] { String.valueOf(downloadID) });
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
	}

}
