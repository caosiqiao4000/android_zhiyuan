package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IBlacklistDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.UserItem;

/**
 * 黑名单
 * 
 * @author zhengjy
 * 
 */
public class BlacklistDaoImpl implements IBlacklistDao {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private Context mContext;
	private String mUserId;
	private MyDataBaseAdapter mDataBaseAdapter;
	private PangkerApplication application;
	private IPKUserDao iPKUserDao;

	public BlacklistDaoImpl() {
	}

	public BlacklistDaoImpl(Context mContext) {
		this.mContext = mContext;
		application = (PangkerApplication) mContext.getApplicationContext();
		mUserId = application.getMyUserID();
		iPKUserDao = new PKUserDaoImpl(mContext);
	}

	/**
	 * 获取所有黑名单用户
	 */
	@Override
	public List<UserItem> getBlacklist() {

		String selectSQL = "select DISTINCT A.* ,B.* from " + MyDataBaseAdapter.TABLE_BLACKLIST + " A,"
				+ MyDataBaseAdapter.TABLE_USERS + " B where A." + MyDataBaseAdapter.BLACK_RUSERID + "=B."
				+ MyDataBaseAdapter.USER_ID + " and " + MyDataBaseAdapter.BLACK_MUSERID + " = '" + mUserId + "'";
		Cursor mCursor = null;
		List<UserItem> blacks = new ArrayList<UserItem>();
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			mCursor = mDataBaseAdapter.rawQuery(selectSQL);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
					UserItem blackUser = new UserItem();
					blackUser.setUserId(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.BLACK_RUSERID)));
					blackUser.setUserName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
					blackUser.setNickName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
					blackUser.setIconType(mCursor.getInt(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));

					blackUser.setSign(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));
					blacks.add(blackUser);
				}
			}
			return blacks;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
		return null;
	}

	/**
	 * 保存所有黑名单用户列表
	 */
	@Override
	public void saveBlacklist(List<UserItem> blacklists) {
		// TODO Auto-generated method stub
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			for (UserItem userItem : blacklists) {
				if (!isExist(userItem.getUserId())) {
					ContentValues cValues = new ContentValues();
					cValues.put(MyDataBaseAdapter.BLACK_MUSERID, mUserId);
					cValues.put(MyDataBaseAdapter.BLACK_RUSERID, userItem.getUserId());
					boolean isInsertSuccess = mDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_BLACKLIST,
							cValues) > 0;
					if (isInsertSuccess) {
						iPKUserDao.saveUser(userItem);
					}
				}
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (mDataBaseAdapter != null) {
				if (mDataBaseAdapter.mSQLiteDatabase != null) {
					mDataBaseAdapter.mSQLiteDatabase.close();
				}
				mDataBaseAdapter.close();
			}
		}
	}

	/**
	 * 添加黑名单
	 */
	@Override
	public void insertUser(String memberUid) {
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			ContentValues cValues = new ContentValues();
			cValues.put(MyDataBaseAdapter.BLACK_MUSERID, mUserId);
			cValues.put(MyDataBaseAdapter.BLACK_RUSERID, memberUid);
			if (isExist(memberUid))
				return;
			long isInsertSuccess = mDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_BLACKLIST, cValues);
			if (isInsertSuccess > 0) {
				UserItem userItem = iPKUserDao.getUserItemByUserID(memberUid);
				if (userItem != null)
					application.addBlacklist(userItem);
			}

		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
	}

	/**
	 * 移除黑名单
	 */
	@Override
	public void removeUser(String memberUid) {
		try {
			mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
			mDataBaseAdapter.open();
			if (!isExist(memberUid))
				return;
			boolean isDeleSuccess = mDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_BLACKLIST, new String[]{MyDataBaseAdapter.BLACK_RUSERID,MyDataBaseAdapter.BLACK_MUSERID }, new String[]{memberUid,mUserId});
			if (isDeleSuccess) {
				application.removeBlacklist(memberUid);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
	}

	/**
	 * 判断该当前用户是否存在黑名单表中
	 */
	@Override
	public boolean isExist(String memberUid) {
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_BLACKLIST + " where "
				+ MyDataBaseAdapter.BLACK_RUSERID + "='" + memberUid + "' and " + MyDataBaseAdapter.BLACK_MUSERID + " = '" + mUserId + "'";
		Cursor mCursor = null;
		mDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
		mDataBaseAdapter.open();
		try {
			mCursor = mDataBaseAdapter.rawQuery(selectSQL);

			return mCursor != null && mCursor.getCount() > 0;

		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return false;
	}

	@Override
	public boolean insertBlacklist(UserItem user, MyDataBaseAdapter adapter) {
		// TODO Auto-generated method stub
		try {
			ContentValues cValues = new ContentValues();
			cValues.put(MyDataBaseAdapter.BLACK_MUSERID, mUserId);
			cValues.put(MyDataBaseAdapter.BLACK_RUSERID, user.getUserId());
			if (isExist(user.getUserId(), adapter))
				return false;
			long isInsertSuccess = adapter.insertData(MyDataBaseAdapter.TABLE_BLACKLIST, cValues);
			return isInsertSuccess > 0;

		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}

	@Override
	public boolean isExist(String memberUid, MyDataBaseAdapter adapter) {
		// TODO Auto-generated method stub
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_BLACKLIST + " where "
				+ MyDataBaseAdapter.BLACK_RUSERID + "='" + memberUid + "' and " + MyDataBaseAdapter.BLACK_MUSERID + " = '" + mUserId + "'";
		Cursor mCursor = null;
		try {
			mCursor = adapter.rawQuery(selectSQL);

			return mCursor != null && mCursor.getCount() > 0;

		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return false;
	}

}
