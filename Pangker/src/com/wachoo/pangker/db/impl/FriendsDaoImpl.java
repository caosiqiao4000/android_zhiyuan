package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.util.Util;

/**
 * 
 * friends manager
 * 
 * @author wangxin
 * 
 */
public class FriendsDaoImpl implements IFriendsDao {
	private String TAG = "FriendsDaoImpl";// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Context context;
	private String myUserID;
	private PangkerApplication application;
	private IPKUserDao userDao;

	//

	public FriendsDaoImpl(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		application = (PangkerApplication) context.getApplicationContext();
		userDao = new PKUserDaoImpl(context);
		if (application != null)
			myUserID = application.getMyUserID();
	}

	@Override
	public synchronized List<UserItem> getAllFriendsByUID(String myUid) {
		// TODO Auto-generated method stub
		List<UserItem> friends = new ArrayList<UserItem>();
		Cursor friendsCursor = null;
		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			friendsCursor = m_MyDataBaseAdapter.getAllFriends(myUid);
			if (friendsCursor == null)
				return friends;
			if (friendsCursor.getCount() == 0)
				return friends;
			friendsCursor.moveToFirst();
			friends = new ArrayList<UserItem>();
			for (; !friendsCursor.isAfterLast(); friendsCursor.moveToNext()) {
				UserItem friend = new UserItem();

				friend = new UserItem();
				friend.setUserId(friendsCursor.getString(friendsCursor
						.getColumnIndex(MyDataBaseAdapter.USER_ID)));// 用户id
				friend.setRealName(friendsCursor.getString(friendsCursor
						.getColumnIndex(MyDataBaseAdapter.USER_REALNAME)));
				friend.setUserName(friendsCursor.getString(friendsCursor
						.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));// 用户名
				friend.setNickName(friendsCursor.getString(friendsCursor
						.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
				friend.setIconType(friendsCursor.getInt(friendsCursor
						.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识

				try {
					friend.setrName(friendsCursor.getString(friendsCursor
							.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME)));// 好友备注
				} catch (Exception e) {
					logger.error(TAG, e);
				}

				friend.setSign(friendsCursor.getString(friendsCursor
						.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));// 用户签名

				friends.add(friend);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (friendsCursor != null) {
				friendsCursor.close();
				friendsCursor = null;
			}
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return friends;
	}

	public void saveFriends(List<ContactGroup> groups) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			for (ContactGroup group : groups) {

				for (UserItem user : group.getUserItems()) {
					ContentValues friendsValues = new ContentValues();
					friendsValues
							.put(MyDataBaseAdapter.FRIENDS_MYUID, myUserID);
					friendsValues.put(MyDataBaseAdapter.FRIENDS_FRIENDUID,
							Util.trimUserIDDomain(user.getUserId()));
					friendsValues.put(MyDataBaseAdapter.FRIENDS_GROUPID,
							group.getGid());
					m_MyDataBaseAdapter.insertData(
							MyDataBaseAdapter.TABLE_USERS, friendsValues);
				}
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}

	@Override
	public boolean addFriend(UserItem user, String groupid) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			if (m_MyDataBaseAdapter.checkFriendIsExist(
					Util.trimUserIDDomain(user.getUserId()), groupid, myUserID))
				return false;
			ContentValues friendsValues = new ContentValues();
			friendsValues.put(MyDataBaseAdapter.FRIENDS_MYUID, myUserID);
			friendsValues.put(MyDataBaseAdapter.FRIENDS_FRIENDUID,
					Util.trimUserIDDomain(user.getUserId()));
			friendsValues.put(MyDataBaseAdapter.FRIENDS_RELATIONTYPE,
					user.getRelationType());
			friendsValues.put(MyDataBaseAdapter.FRIENDS_GROUPID, groupid);
			friendsValues.put(MyDataBaseAdapter.FRIENDS_REMARKNAME,
					user.getrName());
			// friendsValues.put(m_MyDataBaseAdapter.FRIENDS_SOURCE, "1");
			long _userid = m_MyDataBaseAdapter.insertData(
					MyDataBaseAdapter.TABLE_FRIENDS, friendsValues);
			if (_userid > 0) {
				application.addFriend(user, groupid);
				return true;
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	public boolean addFriend(UserItem user, String groupid,
			MyDataBaseAdapter myDataBaseAdapter) {
		// TODO Auto-generated method stub
		try {
			if (myDataBaseAdapter.checkFriendIsExist(user.getUserId(), groupid,
					myUserID))
				return false;
			ContentValues friendsValues = new ContentValues();
			friendsValues.put(MyDataBaseAdapter.FRIENDS_MYUID, myUserID);
			friendsValues.put(MyDataBaseAdapter.FRIENDS_FRIENDUID,
					Util.trimUserIDDomain(user.getUserId()));
			friendsValues.put(MyDataBaseAdapter.FRIENDS_GROUPID, groupid);
			friendsValues.put(MyDataBaseAdapter.FRIENDS_REMARKNAME,
					user.getrName());
			// friendsValues.put(m_MyDataBaseAdapter.FRIENDS_SOURCE, "1");
			long _frienduid = myDataBaseAdapter.insertData(
					MyDataBaseAdapter.TABLE_FRIENDS, friendsValues);
			if (_frienduid > 0)
				return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		}
		return false;
	}

	@Override
	public boolean delFriend(String userid, String groupid) {
		// TODO Auto-generated method stub
		boolean result = false;
		try {
			if (groupid.equals(PangkerConstant.DEFAULT_GROUPID)) {
				// 删除好友，从所有分组中删除
				m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
				m_MyDataBaseAdapter.open();
				String sql = "delete from " + MyDataBaseAdapter.TABLE_FRIENDS
						+ " where " + MyDataBaseAdapter.FRIENDS_MYUID + " = '"
						+ myUserID + "' and "
						+ MyDataBaseAdapter.FRIENDS_FRIENDUID + "='" + userid
						+ "'";
				result = m_MyDataBaseAdapter.execSql(sql);
				if (result) {
					application.removeFriend(userid, groupid);
				}
			} else {
				// 指定分组删除
				m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
				m_MyDataBaseAdapter.open();
				String sql = "delete from " + MyDataBaseAdapter.TABLE_FRIENDS
						+ " where " + MyDataBaseAdapter.FRIENDS_MYUID + " = '"
						+ myUserID + "' and "
						+ MyDataBaseAdapter.FRIENDS_FRIENDUID + "='" + userid
						+ "' and " + MyDataBaseAdapter.FRIENDS_GROUPID + " = '"
						+ groupid + "'";
				// friendsValues.put(m_MyDataBaseAdapter.FRIENDS_SOURCE, "1");
				result = m_MyDataBaseAdapter.execSql(sql);
				if (result) {
					application.removeFriend(userid, groupid);
				}
			}

		} catch (Exception e) {
			logger.error(TAG, e);
			result = false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return result;
	}

	@Override
	public boolean updateRelation(String rUserid, String relationtype) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			String sql = "update " + MyDataBaseAdapter.TABLE_FRIENDS + " set "
					+ MyDataBaseAdapter.FRIENDS_RELATIONTYPE + " = '"
					+ relationtype + "' where " + MyDataBaseAdapter.FANS_MYUID
					+ " = '" + myUserID + "' and "
					+ MyDataBaseAdapter.FRIENDS_FRIENDUID + "='" + rUserid
					+ "'";
			// friendsValues.put(m_MyDataBaseAdapter.FRIENDS_SOURCE, "1");
			return m_MyDataBaseAdapter.execSql(sql);

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	public boolean updateFriendLocation(UserItem user) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
		m_MyDataBaseAdapter.open();

		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.TRACE_LAT, user.getLatitude());
		initialValues.put(MyDataBaseAdapter.TRACE_LNG, user.getLongitude());
		initialValues.put(MyDataBaseAdapter.TRACE_TIME,
				user.getLastUpdateTime());
		try {
			return m_MyDataBaseAdapter.updateData(
					MyDataBaseAdapter.TABLE_FRIENDS, initialValues,
					MyDataBaseAdapter.FRIENDS_FRIENDUID,
					new String[] { user.getUserId() });
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	/**
	 * 修改用户备注
	 */
	@Override
	public boolean updateFriend(UserItem user) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
		m_MyDataBaseAdapter.open();

		ContentValues initialValues = new ContentValues();
		initialValues
				.put(MyDataBaseAdapter.FRIENDS_REMARKNAME, user.getrName());
		try {
			return m_MyDataBaseAdapter.updateData(
					MyDataBaseAdapter.TABLE_FRIENDS, initialValues,
					MyDataBaseAdapter.FRIENDS_FRIENDUID,
					new String[] { user.getUserId() });
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	private List<UserItem> getFriendsByGroupId(String groupId,
			MyDataBaseAdapter m_MyDataBaseAdapter) {
		// TODO Auto-generated method stub
		List<UserItem> attents = new ArrayList<UserItem>();
		Cursor mCursor = null;
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter
					.getFriendsByGroupId(groupId, myUserID);
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToFirst();
			attents = new ArrayList<UserItem>();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				UserItem friend = new UserItem();

				friend = new UserItem();
				friend.setUserId(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_ID)));// 用户id
				friend.setUserName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));// 用户名
				friend.setNickName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
				friend.setRealName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_REALNAME)));
				friend.setIconType(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识

				try {
					friend.setrName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME)));
				} catch (Exception e) {
					logger.error(TAG, e);
				}
				friend.setSign(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));// 用户签名
				attents.add(friend);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
		}
		return attents;
	}

	@Override
	public List<UserItem> getFriendsByGroupId(String groupId) {
		// TODO Auto-generated method stub
		List<UserItem> attents = new ArrayList<UserItem>();
		Cursor mCursor = null;
		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter
					.getFriendsByGroupId(groupId, myUserID);
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToFirst();
			attents = new ArrayList<UserItem>();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				UserItem friend = new UserItem();

				friend = new UserItem();
				friend.setUserId(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_ID)));// 用户id
				friend.setUserName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));// 用户名
				friend.setNickName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
				friend.setRealName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_REALNAME)));
				friend.setIconType(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识

				try {
					friend.setrName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME)));
				} catch (Exception e) {
					logger.error(TAG, e);
				}
				friend.setSign(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));// 用户签名
				attents.add(friend);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return attents;
	}

	@Override
	public boolean refreshFriends(List<ContactGroup> list) {
		// TODO Auto-generated method stub
		Cursor mCursor = null;
		try {
			List<UserItem> items = new ArrayList<UserItem>();
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			String sql1 = "delete from "
					+ MyDataBaseAdapter.TABLE_CONTACTS_GROUP + " where "
					+ MyDataBaseAdapter.GROUP_TYPE + " = "
					+ PangkerConstant.GROUP_FRIENDS;
			m_MyDataBaseAdapter.execSql(sql1);
			String sql2 = "delete from " + MyDataBaseAdapter.TABLE_FRIENDS;
			m_MyDataBaseAdapter.execSql(sql2);
			for (ContactGroup group : list) {
				ContentValues groupValues = new ContentValues();
				groupValues
						.put(MyDataBaseAdapter.GROUP_GROUPID, group.getGid());
				groupValues
						.put(MyDataBaseAdapter.GROUP_CREATER, group.getUid());
				groupValues.put(MyDataBaseAdapter.GROUP_GROUPNAME,
						group.getName());
				groupValues.put(MyDataBaseAdapter.GROUP_MEMBER_COUNT,
						group.getCount());
				groupValues.put(MyDataBaseAdapter.GROUP_MYUID, myUserID);
				groupValues.put(MyDataBaseAdapter.GROUP_TYPE, group.getType());
				// group
				if (!m_MyDataBaseAdapter.checkContactsGroupIsExist(
						group.getGid(), myUserID, group.getType())) {
					long _frdgroupsid = m_MyDataBaseAdapter
							.insertData(MyDataBaseAdapter.TABLE_CONTACTS_GROUP,
									groupValues);
					System.out.println("Sava groupInfo to GroupTable ====>"
							+ _frdgroupsid);
				}
				if (group.getGid().equals("0")) {
					items = group.getMembers();
				}
				for (UserItem user : group.getMembers()) {
					for (UserItem useritem : items) {
						if (user.getUserId().equals(useritem.getUserId())) {
							user = useritem;
							break;
						}
					}
					boolean result = addFriend(user, group.getGid(),
							m_MyDataBaseAdapter);
					System.out.println("Sava FriendInfo to FriendTable ====>"
							+ result);
					result = userDao.saveUser(user, m_MyDataBaseAdapter);
					System.out.println("Sava FriendInfo to UserTable ====>"
							+ result);
				}
			}

			mCursor = m_MyDataBaseAdapter.getMyGroupByType(myUserID,
					String.valueOf(PangkerConstant.GROUP_FRIENDS));
			List<ContactGroup> groups = new ArrayList<ContactGroup>();
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
					ContactGroup group = new ContactGroup();
					group.setClientgid(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.GROUP_ID)));
					String groupId = mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.GROUP_GROUPID));
					group.setGid(groupId);
					group.setName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.GROUP_GROUPNAME)));
					group.setType(mCursor.getInt(mCursor
							.getColumnIndex(MyDataBaseAdapter.GROUP_TYPE)));
					group.setUid(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.GROUP_CREATER)));
					List<UserItem> mUserItem = new ArrayList<UserItem>();
					mUserItem = getFriendsByGroupId(groupId,
							m_MyDataBaseAdapter);
					group.setCount(mUserItem != null ? mUserItem.size() : 0);
					if (mUserItem != null) {
						group.setUserItems(mUserItem);
					}
					groups.add(group);
				}
			}
			application.setFriendGroup(groups);
			return true;

		} catch (Exception e) {
			logger.error(TAG, e);
			Log.d(TAG, e.getMessage());
		} finally {
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			Log.d(TAG, "refreshFriends Over!!!!!!");
		}
		return false;
	}

	@Override
	public List<UserItem> getTracelist(String myUid) {

		String selectSQL = "select  A.*, B.* from "
				+ MyDataBaseAdapter.TABLE_FRIENDS + " A,"
				+ MyDataBaseAdapter.TABLE_USERS + " B where A."
				+ MyDataBaseAdapter.FRIENDS_FRIENDUID + "= B."
				+ MyDataBaseAdapter.USER_ID + " AND A."
				+ MyDataBaseAdapter.FRIENDS_MYUID + "= " + myUid + " AND A."
				+ MyDataBaseAdapter.FRIENDS_RELATIONTYPE + "='2'"
				+ " GROUP BY A." + MyDataBaseAdapter.FRIENDS_FRIENDUID;

		List<UserItem> traceslists = new ArrayList<UserItem>();
		Cursor mCursor = null;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
			m_MyDataBaseAdapter.open();
			mCursor = m_MyDataBaseAdapter.rawQuery(selectSQL);
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
					UserItem traceUser = new UserItem();
					traceUser
							.setUserId(mCursor.getString(mCursor
									.getColumnIndex(MyDataBaseAdapter.FRIENDS_FRIENDUID)));
					traceUser.setUserName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
					traceUser.setNickName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
					traceUser
							.setIconType(mCursor.getInt(mCursor
									.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));
					traceUser
							.setrName(mCursor.getString(mCursor
									.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME)));
					String dLat = mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.TRACE_LAT));
					if (!Util.isEmpty(dLat)) {
						traceUser.setLatitude(Double.parseDouble(dLat));
					}
					String dLng = mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.TRACE_LNG));
					if (!Util.isEmpty(dLng)) {
						traceUser.setLongitude(Double.parseDouble(dLng));
					}
					traceUser.setSign(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));
					traceslists.add(traceUser);
				}
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			Log.d(TAG, "refreshTracelits Over Sum==>" + traceslists.size());
		}
		return traceslists;
	}

	@Override
	public boolean clearTraceUsers(String myUid) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			return m_MyDataBaseAdapter.updateData(
					MyDataBaseAdapter.TABLE_FRIENDS,
					MyDataBaseAdapter.FRIENDS_RELATIONTYPE, "1",
					MyDataBaseAdapter.FRIENDS_MYUID, myUid);
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			Log.d(TAG, "refreshFriends Over!!!!!!");
		}
		return false;
	}

	@Override
	public boolean pharseTracelist(UserItem user) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
		m_MyDataBaseAdapter.open();

		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.FRIENDS_RELATIONTYPE, "2");
		initialValues.put(MyDataBaseAdapter.TRACE_LAT, user.getLatitude());
		initialValues.put(MyDataBaseAdapter.TRACE_LNG, user.getLongitude());
		initialValues.put(MyDataBaseAdapter.TRACE_TIME,
				user.getLastUpdateTime());
		try {
			return m_MyDataBaseAdapter.updateData(
					MyDataBaseAdapter.TABLE_FRIENDS, initialValues,
					MyDataBaseAdapter.FRIENDS_FRIENDUID,
					new String[] { user.getUserId() });
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	public boolean pharseTracelist(List<UserItem> users) {
		// TODO Auto-generated method stub
		if (users == null) {
			return false;
		}
		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
		m_MyDataBaseAdapter.open();

		try {
			for (UserItem user : users) {
				ContentValues initialValues = new ContentValues();
				initialValues.put(MyDataBaseAdapter.FRIENDS_RELATIONTYPE, "2");
				initialValues.put(MyDataBaseAdapter.TRACE_LAT,
						user.getLatitude());
				initialValues.put(MyDataBaseAdapter.TRACE_LNG,
						user.getLongitude());
				initialValues.put(MyDataBaseAdapter.TRACE_TIME,
						user.getLastUpdateTime());
				m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_FRIENDS,
						initialValues, MyDataBaseAdapter.FRIENDS_FRIENDUID,
						new String[] { user.getUserId() });
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}

		return true;
	}
}
