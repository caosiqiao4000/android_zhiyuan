package com.wachoo.pangker.db.impl;

import android.content.ContentValues;
import android.content.Context;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.ITracerDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.util.Util;

/**
 * 
 * TraceDaoIpml
 * 
 * @author wangxin
 * 
 */
public class TraceDaoImpl implements ITracerDao {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Context context;
	private String myUserID;
	public TraceDaoImpl(Context context) {
		// TODO Auto-generated constructor stub
		// TODO Auto-generated constructor stub
		this.context = context;
		myUserID = ((PangkerApplication)context.getApplicationContext()).getMySelf().getUserId();
	}

	@Override
	public void addTracer(UserItem user, String groupid) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			ContentValues values = new ContentValues();
			values.put(MyDataBaseAdapter.TRACE_MYUID, myUserID);
			values.put(MyDataBaseAdapter.TRACE_RUID, Util.trimUserIDDomain(user.getUserId()));
			values.put(MyDataBaseAdapter.TRACE_GROUPID, groupid);
			// friendsValues.put(m_MyDataBaseAdapter.FRIENDS_SOURCE, "1");
			m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_TRACE, values);

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}

	@Override
	public boolean addTracer(UserItem user, String groupid, MyDataBaseAdapter myDataBaseAdapter) {
		// TODO Auto-generated method stub
		try {
			if (myDataBaseAdapter.checkFriendIsExist(user.getUserId(), groupid, myUserID))
				return false;
			ContentValues values = new ContentValues();
			values.put(MyDataBaseAdapter.TRACE_MYUID, myUserID);
			values.put(MyDataBaseAdapter.TRACE_RUID, Util.trimUserIDDomain(user.getUserId()));
			values.put(MyDataBaseAdapter.TRACE_GROUPID, groupid);
			long _userid = myDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_TRACE, values);
			if (_userid > 0)
				return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		}
		return false;
	}

	@Override
	public void delTracer(UserItem user, String groupid) {
		// TODO Auto-generated method stub

	}
}
