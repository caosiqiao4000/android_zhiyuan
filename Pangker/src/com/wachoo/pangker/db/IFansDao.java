package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.UserItem;

/**
 * 
 * 获取用户的粉丝列表
 * 
 * @author wangxin
 * 
 */
public interface IFansDao {

	/**
	 * 
	 * save user fans list
	 * 
	 * @param fans
	 * @return
	 */
	boolean saveFans(List<UserItem> fans);

	/**
	 * 
	 * 
	 * 
	 * @return
	 */
	List<UserItem> getUserFans();

	/**
	 * 
	 * saveFans
	 * 
	 * @param fansID
	 * @return
	 */
	boolean saveFans(UserItem fans);
	
	
	/**
	 * 
	 * @param userID
	 * @return
	 * @author wangxin 2012-3-15 下午01:45:58
	 */
	boolean removeFans(String userID);

	/**
	 * 
	 * saveFans
	 * 
	 * @param fansID
	 * @param myDataBaseAdapter
	 * @return
	 */
	boolean saveFans(UserItem fans, MyDataBaseAdapter myDataBaseAdapter);
	
	public boolean refreshFriends(List<UserItem> list);
}
