package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.server.response.PUser;

public interface ILocalContactsDao {

	public List<LocalContacts> initLocalAddressBooks();

	public boolean insertData(String uid, String phone);

	public void deletePUsers(String uid);

	public void notifyPUsers(List<PUser> pUsers);

	public String getAllPhoneNumbers();

	public List<LocalContacts> matchRegisterUsers(List<LocalContacts> list);

	public LocalContacts getLocalContact(String phoneNum);

}
