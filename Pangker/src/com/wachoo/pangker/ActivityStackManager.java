package com.wachoo.pangker;

import java.util.Stack;

import android.app.Activity;
import android.util.Log;

import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.group.ChatRoomInfoActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.activity.res.PictureInfoActivity;
import com.wachoo.pangker.activity.res.ResInfoActivity;
import com.wachoo.pangker.activity.res.ResSpeaktInfoActivity;

/**
 * 
 * ActivityManager 栈管理
 * 
 * @author wangxin 2012-2-8 下午03:06:43
 * 
 */
public class ActivityStackManager {

	private Stack<Activity> activityStack = new Stack<Activity>();
	private Activity topActivity;

	/**
	 * 
	 * 
	 * 
	 * @author wangxin 2012-2-8 下午03:06:43
	 */
	public void popActivity() {
		Activity activity = activityStack.lastElement();
		if (activity != null) {
			activity.finish();
			activity = null;
		}
	}

	public void popAllActivitys() {
		while (true) {
			Activity activity = currentActivity();
			if (activity == null) {
				break;
			}
			popActivity(activity);
		}
	}

	/**
	 * 
	 * @param activity
	 * 
	 * @author wangxin 2012-2-8 下午03:07:15
	 */
	public void popActivity(Activity activity) {
		Log.i("ActivityStackManager", activity.getClass().toString());
		if (activity != null) {
			if (!activity.isFinishing()) {
				activity.finish();
			}
			activityStack.remove(activity);
			activity = null;
		}
	}

	/**
	 * 
	 * @return
	 * 
	 * @author wangxin 2012-2-10 上午10:55:20
	 */
	public Activity getTopActivity() {
		return topActivity;
	}

	/**
	 * 
	 * @param topActivity
	 * 
	 * @author wangxin 2012-2-10 上午10:55:23
	 */
	public void setTopActivity(Activity topActivity) {
		this.topActivity = topActivity;
	}

	/**
	 * 
	 * currentActivity
	 * 
	 * @return
	 * 
	 * @author wangxin 2012-2-8 下午03:07:28
	 */
	public Activity currentActivity() {
		if (activityStack.isEmpty()) {
			return null;
		}
		Activity activity = activityStack.lastElement();

		return activity;
	}

	/**
	 * 获取最大mian界面
	 * 
	 * @return
	 */
	public Activity currentMainActivity() {
		Activity currentActivity = currentActivity();
		if (currentActivity == null)
			return currentActivity;
		while (currentActivity.getParent() != null) {
			currentActivity = currentActivity.getParent();
		}
		return currentActivity;
	}

	/**
	 * 
	 * pushActivity
	 * 
	 * @param activity
	 * 
	 * @author wangxin 2012-2-8 下午03:07:39
	 */
	public void pushActivity(Activity activity) {
		if (activityStack == null) {
			activityStack = new Stack<Activity>();
		}
		activityStack.add(activity);
	}

	public boolean isChatActivityShow(String userId) {
		Activity activity = currentActivity();
		if (activity.getClass().equals(MsgChatActivity.class)) {
			MsgChatActivity msgChatActivity = (MsgChatActivity) activity;
			if (msgChatActivity.getTopeer() != null && msgChatActivity.getTopeer().getUserId().equals(userId)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断当前显示的是否是消息通知评论的图片的评论
	 * 
	 * @param resID
	 * @return
	 */
	public PictureInfoActivity getPicInfoActivityByResID(long resID) {
		Activity activity = currentActivity();
		if (activity.getClass().equals(PictureInfoActivity.class)) {
			PictureInfoActivity pictureInfoActivity = (PictureInfoActivity) activity;
			if (pictureInfoActivity.isCurrentResource(resID))
				return pictureInfoActivity;
		}
		return null;
	}

	/**
	 * 判断当前显示的是否是消息通知评论的图片的评论
	 * 
	 * @param resID
	 * @return
	 */
	public ResInfoActivity getResInfoActivityByResID(long resID) {
		Activity activity = currentActivity();
		if (activity.getClass().equals(ResInfoActivity.class)) {
			ResInfoActivity resInfoActivity = (ResInfoActivity) activity;
			if (resInfoActivity.isCurrentResource(resID))
				return resInfoActivity;
		}
		return null;
	}

	/**
	 * 判断当前显示的是否是消息通知评论的图片的评论
	 * 
	 * @param resID
	 * @return
	 */
	public ResSpeaktInfoActivity getResSpeaktInfoActivityByResID(long resID) {
		Activity activity = currentActivity();
		if (activity.getClass().equals(ResSpeaktInfoActivity.class)) {
			ResSpeaktInfoActivity speaktInfoActivity = (ResSpeaktInfoActivity) activity;
			if (speaktInfoActivity.isCurrentResource(resID))
				return speaktInfoActivity;
		}
		return null;
	}

	/**
	 * 判断当前显示的是否是消息通知评论的图片的评论
	 * 
	 * @param resID
	 * @return
	 */
	public ChatRoomInfoActivity getChatRoomInfoActivityByGroupID(long resID) {
		Activity activity = currentActivity();
		if (activity.getClass().equals(ChatRoomInfoActivity.class)) {
			ChatRoomInfoActivity chatRoomInfoActivity = (ChatRoomInfoActivity) activity;
			if (chatRoomInfoActivity.isCurrentGroup(resID))
				return chatRoomInfoActivity;
		}
		return null;
	}

	/**
	 * 图片查看器是否在最上显示
	 * 
	 * @return
	 */
	public boolean isActivityShow(Activity mActivity) {
		Activity activity = currentActivity();
		if (activity.equals(mActivity)) {
			return true;
		}
		return false;
	}

	/**
	 * 图片查看器是否在最上显示
	 * 
	 * @return
	 */
	public Activity isPicBrowseActivityShow() {
		Activity activity = currentActivity();
		if (activity.getClass().equals(PictureBrowseActivity.class)) {
			return activity;
		}
		return null;
	}

	/**
	 * 音乐播放器是否在最上显示
	 * 
	 * @return
	 */
	public Activity isMusicPlayActivityShow() {
		Activity activity = currentActivity();
		if (activity.getClass().equals(MusicPlayActivity.class)) {
			return activity;
		}
		return null;
	}

	/**
	 * 
	 * popAllActivityExceptOne
	 * 
	 * @param cls
	 * 
	 * @author wangxin 2012-2-8 下午03:07:51
	 */
	public void popAllActivityExceptOne(Class<?> cls) {
		while (true) {
			Activity activity = currentActivity();
			if (activity == null) {
				break;
			}
			if (activity.getClass().equals(cls)) {
				break;
			}
			popActivity(activity);
		}
	}

	public void popOneActivity(Class<?> cls) {
		for (int i = activityStack.size() - 1; i > 0; i--) {
			Activity activity = activityStack.elementAt(i);
			if (activity == null) {
				break;
			}
			if (activity.getClass().equals(cls)) {
				popActivity(activity);
				break;
			}
		}
	}

	/**
	 * 处理关闭所有用户信息界面
	 */
	private Stack<Activity> userInfoActivities = new Stack<Activity>();

	public void pushUserInfoActivity(UserWebSideActivity activity) {
		if (userInfoActivities == null) {
			userInfoActivities = new Stack<Activity>();
		}
		userInfoActivities.add(activity);
	}

	public void popUserInfoActivity(UserWebSideActivity activity) {
		if (activity != null) {
			activity.finish();
			userInfoActivities.remove(activity);
			activity = null;
		}
	}

	public void popAllUserInfoActivity() {
		while (!userInfoActivities.empty()) {
			Activity infoActivity = userInfoActivities.pop();
			if (infoActivity != null)
				infoActivity.finish();
		}
	}
}
