package com.wachoo.pangker.audio;

import java.util.HashMap;
import java.util.Map;

import android.util.Log;

/**
 * 发言管理
 * 
 * @author wangxin
 * 
 */
public class AudioPlayManager {

	private static final String TAG = "AudioPlayManager";
	private Map<Long, AudioPlayThread> playThread = new HashMap<Long, AudioPlayThread>();

	/**
	 * 添加音频播放
	 * 
	 * @param speakUID
	 * @param bs
	 */
	public void audioPlay(long speakUID, byte[] bs) {
		// >>>>>>>判断是否存在播放线程
		if (speakUID <= 0)
			return;
		getSpeakThread(speakUID).pushAmrByteStack(bs);
	}

	public AudioPlayThread getSpeakThread(long speakUID) {
		AudioPlayThread audioPlayThread = playThread.get(speakUID);
		// >>>>>>>>判断是否可用
		if (audioPlayThread != null && audioPlayThread.isRun && audioPlayThread.getSpeakUID() == speakUID) {
			Log.i(TAG, "OldSpeaker--UID=" + speakUID);
			return audioPlayThread;
		}
		// >>>>>>>>如果不可用
		else {
			Log.i(TAG, "NewSpeaker--UID=" + speakUID);
			audioPlayThread = new AudioPlayThread(speakUID);
			audioPlayThread.start();
			this.playThread.put(speakUID, audioPlayThread);

		}
		return audioPlayThread;
	}

	public void clear() {
		Log.i(TAG, "clearSpeaker");
		for (long key : this.playThread.keySet()) {
			this.playThread.get(key).stopRun();
		}
		this.playThread.clear();
	}

	public int speakUserCount() {
		return this.playThread.size();
	}

	/**
	 * 移除音频播放器
	 * 
	 * @param speakUID
	 * @return
	 */
	public synchronized boolean removeAudioPlay(long speakUID) {
		for (long key : this.playThread.keySet()) {
			if (key == speakUID) {
				Log.i(TAG, "removeSpeaker--UID=" + speakUID);
				this.playThread.get(key).stopRun();
				this.playThread.remove(speakUID);
				return true;
			}
		}
		return false;
	}
}
