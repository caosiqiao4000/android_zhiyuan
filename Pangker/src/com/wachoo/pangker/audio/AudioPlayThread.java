package com.wachoo.pangker.audio;

import java.io.ByteArrayInputStream;
import java.util.Stack;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

import com.opaque.project.AmrCodec;

public class AudioPlayThread extends Thread {

	private ByteArrayInputStream inputStream;
	byte[] outBytes = new byte[320];
	byte[] inBytes = new byte[32];
	int outLen = 0;
	// >>>>>>>>语音流
	private Stack<byte[]> amrByteStack = new Stack<byte[]>();
	// >>>>>>>>播放语音工具类
	private AudioTrack audoiTrack;
	private int decHandle = AmrCodec.nbInit();;
	// >>>>>是否需要语音播放
	boolean isRun = true;
	private long speakUID;

	public AudioPlayThread(long speakUID) {
		super();
		this.speakUID = speakUID;
	}

	public long getSpeakUID() {
		return speakUID;
	}

	public void setSpeakUID(long speakUID) {
		this.speakUID = speakUID;
	}

	public boolean isRun() {
		return isRun;
	}

	public void stopRun() {
		// TODO Auto-generated method stub
		isRun = false;
	}

	/**
	 * init Stream play
	 * 
	 * @return
	 */
	private AudioTrack initTrack() {
		return new AudioTrack(AudioManager.STREAM_MUSIC,// stream
				// type
				8000,// sample rate
				AudioFormat.CHANNEL_CONFIGURATION_MONO,// channel config
				AudioFormat.ENCODING_PCM_16BIT,// audio format
				320 * 100,// buffer size: 8KB
				AudioTrack.MODE_STREAM);// buffer mode

	}

	// >>>>>>停止语音播放
	public void stopAudioPlay() {
		this.isRun = false;
		synchronized (this) {
			notifyAll();
		}
	}

	/**
	 * 语音流
	 * 
	 * @param bs
	 */
	public void pushAmrByteStack(byte[] bs) {
		this.amrByteStack.push(bs);
		synchronized (this) {
			notifyAll();
		}
	}

	public void run() {
		if (audoiTrack == null) {
			audoiTrack = initTrack();
		}

		synchronized (this) {
			try {
				while (isRun) {
					audoiTrack.play();
					if (amrByteStack.size() > 0) {
						Log.i("AudioPlayThread",">>>>>>>amrByteStack.size()="+amrByteStack.size());	
						byte[] amrBytes = popAll();
						Log.i("AudioPlayThread",">>>>>>>amrBytes.size()="+amrBytes.length);	
						inputStream = new ByteArrayInputStream(amrBytes);
						while (isRun) {
							outLen = inputStream.read(inBytes);
							if (outLen != -1) {
								outLen = AmrCodec.nbDecode(decHandle, inBytes, inBytes.length, outBytes, outBytes.length);
								audoiTrack.write(outBytes, 0, outBytes.length);
							} else {
								break;
							}
						}
					} else {
						synchronized (this) {
							audoiTrack.stop();
							wait();
						}
					}
				}
				audoiTrack.stop();
				audoiTrack.release();
				audoiTrack = null;
				if (inputStream != null) {
					inputStream.close();
				}
				return;
			} catch (Exception e) {
				return;
			}
		}
	}
	
	private byte[] popAll() {
		// TODO Auto-generated method stub
		byte[] amrBytes = new byte[640 * amrByteStack.size()];
		int i = 0;
        while (amrByteStack.size() > 0) {
        	byte[] bts = amrByteStack.pop();
        	System.arraycopy(bts, 0, amrBytes, i * bts.length, bts.length);
        	i ++; 
		}
        return amrBytes;
	}
}
