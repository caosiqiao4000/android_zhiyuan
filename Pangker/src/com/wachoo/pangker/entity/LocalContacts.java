package com.wachoo.pangker.entity;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;

import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.util.Util;

public class LocalContacts extends UserItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String LOCALCONTACTS_KEY = "LocalContacts";

	// 于当前登录用户的关系 0 : 通讯录联系人 1： 旁客用户 2：旁客用户（并且是好友关系）
	public static final int RELATION_CONTACTS = 0;
	public static final int RELATION_P_USER = 1;
	public static final int RELATION_P_FRIEND = 2;

	public static final String NO_LETTER = "NO_LETTER";
	private String contactId;// 本地通讯录联系人ID，标识联系人唯一性

	// private String phoneNum;
	private int relation; // 于当前登录用户的关系 0 : 通讯录联系人 1： 旁客用户 2：旁客用户（并且是好友关系）
	private boolean hasPhoto;
	private Bitmap photo;
	private String sortkey;
	private String firstLetter;
	private List<String> phoneNums;// 同一个联系人可能会有多个号码，add by zhengjy

	/**
	 * @return the relation
	 */
	public int getRelation() {
		return relation;
	}

	/**
	 * @param relation
	 *            the relation to set
	 */
	public void setRelation(int relation) {
		this.relation = relation;
	}

	/**
	 * @return the firstLetter
	 */
	public String getFirstLetter() {
		return firstLetter;
	}

	/**
	 * @param firstLetter
	 *            the firstLetter to set
	 */
	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}

	/**
	 * @return the sortkey
	 */
	public String getSortkey() {
		return sortkey;
	}

	/**
	 * @param sortkey
	 *            the sortkey to set
	 */
	public void setSortkey(String sortkey) {
		this.sortkey = sortkey;
	}

	/**
	 * @return the hasPhoto
	 */
	public boolean isHasPhoto() {
		return hasPhoto;
	}

	/**
	 * @param hasPhoto
	 *            the hasPhoto to set
	 */
	public void setHasPhoto(boolean hasPhoto) {
		this.hasPhoto = hasPhoto;
	}

	/**
	 * @return the photo
	 */
	public Bitmap getPhoto() {
		return photo;
	}

	/**
	 * @param photo
	 *            the photo to set
	 */
	public void setPhoto(Bitmap photo) {
		this.photo = photo;
	}

	public List<String> getPhoneNums() {
		return phoneNums;
	}

	public void setPhoneNums(List<String> phoneNums) {
		this.phoneNums = phoneNums;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public void initSelf(final ContentResolver contentResolver,
			final String myUserID, final MyDataBaseAdapter adapter,
			final boolean isBandAdress) {

		Cursor phoneCur = null;
		try {
			phoneCur = contentResolver.query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = "
							+ getContactId(), null, null);
			List<String> phoneNums = new ArrayList<String>();
			if (phoneCur == null)
				return;
			while (phoneCur.moveToNext()) {
				String strPhoneNumber = phoneCur
						.getString(phoneCur
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)); // 手机号码字段联系人可能不止一个
				String phoneTemp = Util.delSpace(strPhoneNumber);
				// >>>>去掉86前缀
				if (phoneTemp.contains("+86")) {
					phoneTemp = phoneTemp.substring(3, phoneTemp.length());
				}
				// >>>>>>添加电话号码
				phoneNums.add(phoneTemp);
				// >>>>>>>设置主号码
				if (Util.isMobile(phoneTemp)) {

					setPhoneNum(phoneTemp);
				}

				// 当手机号码为空的或者为空字段 跳过当前循环
				if (TextUtils.isEmpty(getPhoneNum()))
					continue;

				if (isBandAdress) {
					LocalContacts contact = adapter.getContactsRelation(
							phoneTemp, myUserID);
					setRelation(contact.getRelation());
					setUserId(contact.getUserId());
				}
			}
			setPhoneNums(phoneNums);
			phoneCur.close();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("LocalContactsDaoImpl", e.getMessage(), e);
		} finally {
			if (phoneCur != null)
				phoneCur.close();
		}

	}
}
