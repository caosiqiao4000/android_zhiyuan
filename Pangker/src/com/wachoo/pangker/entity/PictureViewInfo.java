package com.wachoo.pangker.entity;

import java.io.Serializable;

import com.wachoo.pangker.Configuration;

public class PictureViewInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String PICTURE_VIEW_INTENT_KEY = "PICTURE_VIEW_INTENT_KEY";

	private String picName;
	private String picPath;
	private String uploadLocation;
	private boolean isNet = false;

	public boolean isNet() {
		return isNet;
	}

	public void setNet(boolean isNet) {
		this.isNet = isNet;
	}

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
		if (this.picPath.startsWith(Configuration.getBaseUrl()))
			setNet(true);
		else
			setNet(false);
	}

	public String getUploadLocation() {
		return uploadLocation;
	}

	public void setUploadLocation(String uploadLocation) {
		this.uploadLocation = uploadLocation;
	}

}
