package com.wachoo.pangker.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * 联系人组
 * 
 * @author wangxin
 * 
 */
public class ContactGroup {

	private String clientgid;// 本机的group自动增长键
	private String gid;// 组唯一标识.
	private String name;// 联系人类别名称，如我的好友分组1，好友分组2
	private int count;// 当前分组下记录总数
	private int type;// 组类别标识：用于区分组属于那些联系人类别，如：12：我的好友，14：关注人，15：我的粉丝（粉丝无分组信息）

	private String uid;// 分组所属用户ID
	//private List<UserItem> userItems = new ArrayList<UserItem>();
    private List<UserItem> members = new ArrayList<UserItem>();
	/**
	 * @return the clientgid
	 */
	public String getClientgid() {
		return clientgid;
	}

	/**
	 * @param clientgid
	 *            the clientgid to set
	 */
	public void setClientgid(String clientgid) {
		this.clientgid = clientgid;
	}

	/**
	 * @return the gid
	 */
	public String getGid() {
		return gid;
	}

	/**
	 * @param gid
	 *            the gid to set
	 */
	public void setGid(String gid) {
		this.gid = gid;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		if (members != null)
			return members.size();
		return 0;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	public void setUserItems(List<UserItem> UserItems) {
		this.members = UserItems;
	}

	/**
	 * 
	 * @param userid
	 * 
	 * @author wangxin
	 * @date 2012-2-22 下午02:44:10
	 */
	public void removeUserItem(String userId) {
		final Iterator<UserItem> iter = new ArrayList<UserItem>(members).iterator();
		while (iter.hasNext()) {
			UserItem item = iter.next();
			if (item.getUserId().equals(userId)) {
				this.members.remove(item);
				break;
			}
		}
	}

	/**
	 * Returns a <code>UserItem</code> by the nickname the user has been
	 * assigned.
	 * 
	 * @param nickname
	 *            the nickname of the user.
	 * @return the UserItem.
	 */
	public UserItem getUserItemByNickname(String nickname) {
		final Iterator<UserItem> iter = new ArrayList<UserItem>(members).iterator();
		while (iter.hasNext()) {
			UserItem item = iter.next();
			if (item.getNickName().equals(nickname)) {
				return item;
			}
		}
		return null;
	}

	/**
	 * 判断一个用户是否在一个组内
	 * 
	 * @param userid
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-22 下午03:25:57
	 */
	public boolean userItemIsExist(String userid) {
		final Iterator<UserItem> iter = new ArrayList<UserItem>(members).iterator();
		while (iter.hasNext()) {
			UserItem item = iter.next();
			if (item.getUserId().equals(userid)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * 加入一个联系人
	 * 
	 * @param item
	 */
	public void addUserItem(UserItem item) {
		if (userItemIsExist(item.getUserId()))
			return;
		item.setGroupName(this.name);
		members.add(item);
		Collections.sort(this.members, itemComparator);
	}
	
	
	/**
	 * 加入一个联系人
	 * 
	 * @param item
	 */
	public void addUserList(List<UserItem> item) {
		for (UserItem userItem : item) {
			if (userItemIsExist(userItem.getUserId()))
				continue;
			userItem.setGroupName(this.name);
			members.add(userItem);
		}
		Collections.sort(this.members, itemComparator);
	}

	/**
	 * Returns a <code>UserItem</code> by the users bare bareJID.
	 * 
	 * @param bareJID
	 *            the bareJID of the user.
	 * @return the UserItem.
	 */
	public UserItem getUserItemByJID(String userId) {
		final Iterator iter = new ArrayList(members).iterator();
		while (iter.hasNext()) {
			UserItem item = (UserItem) iter.next();
			if (item.getUserId().equals(userId)) {
				return item;
			}
		}
		return null;
	}

	/**
	 * 根据昵称获取联系人
	 * 
	 * @param nickName
	 * @return
	 */
	public UserItem getUserItemByNickName(String nickName) {
		final Iterator iter = new ArrayList(members).iterator();
		while (iter.hasNext()) {
			UserItem item = (UserItem) iter.next();
			if (item.getNickName().equals(nickName)) {
				return item;
			}
		}
		return null;
	}

	/**
	 * 获取组内所有的UserItems
	 * 
	 * @return
	 */
	public List<UserItem> getUserItems() {
		final List<UserItem> list = new ArrayList<UserItem>(members);
		Collections.sort(list, itemComparator);
		return list;
	}

	/**
	 * Sorts UserItems.
	 */
	final Comparator<UserItem> itemComparator = new Comparator() {
		public int compare(Object UserItemOne, Object UserItemTwo) {
			final UserItem item1 = (UserItem) UserItemOne;
			final UserItem item2 = (UserItem) UserItemTwo;
			if (item1.getNickName() != null && item2.getNickName() != null) {
				return item1.getNickName().toLowerCase().compareTo(item2.getNickName().toLowerCase());
			} else {
				return 0;
			}

		}
	};

	/**
	 * 
	 * @param index
	 * @return
	 */
	public UserItem getUserItemByIndex(int index) {
		return this.members.get(index);
	}

	public List<UserItem> getMembers() {
		return members;
	}

	public void setMembers(List<UserItem> members) {
		this.members = members;
	}
}
