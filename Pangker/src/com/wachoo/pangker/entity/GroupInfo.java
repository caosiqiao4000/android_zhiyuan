package com.wachoo.pangker.entity;

import java.util.Date;

import com.wachoo.pangker.util.Util;

/**
 *@author: wangxin
 *@createtime: 2012-1-12下午2:53:58
 *@desc:GroupInfo对象定义,查询用户参加过的群组
 */
public class GroupInfo implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String groupId;// 组唯一标识
	private String uid;// 群组创建者
	private String groupSid;// 组SID标识
	private String groupName;// 组名称
	private String groupType;// 组类别：10:会议室，11：直播室，12：群组讨论组，13：友踪组
	private String createTime;// 群组创建时间
	private String lastVisitTime;// 群组访问时间

	public GroupInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GroupInfo(Long groupId, Long uid, Long groupSid, String groupName, Integer groupType, Date addTime) {
		super();
		this.groupId = groupId.toString();
		this.uid = uid.toString();
		this.groupSid = groupSid.toString();
		this.groupName = groupName;
		this.groupType = String.valueOf(groupType);
		this.createTime = addTime == null ? "" : Util.FormatDate(addTime, "yyyy-MM-dd HH:mm:ss");
	}

	public GroupInfo(Long groupId, Long uid, Long groupSid, String groupName, Integer groupType,
			Date addTime, Date lastVisitTime) {
		super();
		this.groupId = groupId.toString();
		this.uid = uid.toString();
		this.groupSid = groupSid.toString();
		this.groupName = groupName;
		this.groupType = String.valueOf(groupType);
		this.createTime = addTime == null ? "" : Util.FormatDate(addTime, "yyyy-MM-dd HH:mm:ss");
		this.lastVisitTime = lastVisitTime == null ? "" : Util.FormatDate(lastVisitTime,
				"yyyy-MM-dd HH:mm:ss");
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getGroupSid() {
		return groupSid;
	}

	public void setGroupSid(String groupSid) {
		this.groupSid = groupSid;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastVisitTime() {
		return lastVisitTime;
	}

	public void setLastVisitTime(String lastVisitTime) {
		this.lastVisitTime = lastVisitTime;
	}

}
