package com.wachoo.pangker.entity;

import java.util.List;

import com.wachoo.pangker.server.response.MusicInfo;

public class PlayingMusiclist {
	private List<MusicInfo> infos = null;

	public List<MusicInfo> getInfos() {
		return infos;
	}

	public void setInfos(List<MusicInfo> infos) {
		this.infos = infos;
	}

}
