package com.wachoo.pangker.entity;

import java.io.Serializable;

public class TraceInvite implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String roomSubject;
	private String roomid;
	private String inivteReson;
	private String inivteUserID;
	private String inviter;
	private String connID;
	private String roomName;
	
	

	/**
	 * @return the roomSubject
	 */
	public String getRoomSubject() {
		return roomSubject;
	}

	/**
	 * @param roomSubject the roomSubject to set
	 */
	public void setRoomSubject(String roomSubject) {
		this.roomSubject = roomSubject;
	}

	/**
	 * @return the roomName
	 */
	public String getRoomName() {
		return roomName;
	}

	/**
	 * @param roomName the roomName to set
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	/**
	 * @return the inviter
	 */
	public String getInviter() {
		return inviter;
	}

	/**
	 * @param inviter
	 *            the inviter to set
	 */
	public void setInviter(String inviter) {
		this.inviter = inviter;
	}

	/**
	 * @return the connID
	 */
	public String getConnID() {
		return connID;
	}

	/**
	 * @param connID
	 *            the connID to set
	 */
	public void setConnID(String connID) {
		this.connID = connID;
	}

	/**
	 * @return the roomid
	 */
	public String getRoomid() {
		return roomid;
	}

	/**
	 * @param roomid
	 *            the roomid to set
	 */
	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}

	/**
	 * @return the inivteReson
	 */
	public String getInivteReson() {
		return inivteReson;
	}

	/**
	 * @param inivteReson
	 *            the inivteReson to set
	 */
	public void setInivteReson(String inivteReson) {
		this.inivteReson = inivteReson;
	}

	/**
	 * @return the inivteUserID
	 */
	public String getInivteUserID() {
		return inivteUserID;
	}

	/**
	 * @param inivteUserID
	 *            the inivteUserID to set
	 */
	public void setInivteUserID(String inivteUserID) {
		this.inivteUserID = inivteUserID;
	}

}
