package com.wachoo.pangker.entity;
/**
 * 附近的用户，针对好友实现
 * @author wangxin
 *
 */
public class NearUser {
	private String userId;
	private Location location;
	private double distance;
	
	
	
	
	public NearUser(String userId, Location location, double distance) {
		super();
		this.userId = userId;
		this.location = location;
		this.distance = distance;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
	/**
	 * @return the distance
	 */
	public double getDistance() {
		return distance;
	}
	/**
	 * @param distance the distance to set
	 */
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	
}
