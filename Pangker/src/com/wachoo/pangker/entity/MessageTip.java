package com.wachoo.pangker.entity;

/**
 * 消息通知类
 * 
 * @author wangxin
 * 
 */
public class MessageTip {

	public final static int TIP_COMMENT_RES = 0x1001;// 评论资源
	public final static int TIP_RECOMMEND = 0x1002;// 推荐
	public final static int TIP_SCORE = 0x1003;// 评分
	public final static int TIP_DOWNLOAD = 0x1004;// 下载
	public final static int TIP_BROADCAST = 0x1005;// 转播
	public final static int TIP_COLLECT = 0x1006;// 收藏
	public final static int TIP_COMMENT_REPLY = 0x1007;// 回复评论
	public final static int TIP_COMMENT_BROADCAST_RES = 0x1008;// 评论转播的资源

	private int id;
	private String commentID; // >>>>>>>资源评论ID
	private String userId;// 备注：当前用户UserID
	private String userName;// >>>当前用户名称
	private String fromId;// 备注：通知用户的UserID
	private String fromName;// 推荐人的名称

	private String resId;// 备注：资源id 根据类型判断是资源ID or 群组ID ，文字描述是没有ID的，不需要
	private String resName;// 资源的名称
	private int resType;// 资源的类型
	private String content;// 备注：对方的内容
	private String createTime;// 备注：插入时间

	private String messagetip;
	private int tipType;// 操作类型

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCommentID() {
		return commentID;
	}

	public void setCommentID(String commentID) {
		this.commentID = commentID;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFromId() {
		return fromId;
	}

	public void setFromId(String fromId) {
		this.fromId = fromId;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getResId() {
		return resId;
	}

	public void setResId(String resId) {
		this.resId = resId;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public int getResType() {
		return resType;
	}

	public void setResType(int resType) {
		this.resType = resType;
	}

	public int getTipType() {
		return tipType;
	}

	public void setTipType(int tipType) {
		this.tipType = tipType;
	}

	public String getMessagetip() {
		return messagetip;
	}

	public void setMessagetip(String messagetip) {
		this.messagetip = messagetip;
	}

}
