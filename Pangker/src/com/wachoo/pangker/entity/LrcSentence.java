package com.wachoo.pangker.entity;

public class LrcSentence {

	private String conetnt;
	private int LyricTime;
	
	public LrcSentence(int lyricTime, String conetnt) {
		super();
		this.conetnt = conetnt;
		this.LyricTime = lyricTime;
	}

	public String getContent() {
		return conetnt;
	}

	public void setContent(String conetnt) {
		this.conetnt = conetnt;
	}

	public int getLyricTime() {
		return LyricTime;
	}

	public void setLyricTime(int lyricTime) {
		LyricTime = lyricTime;
	}

}
