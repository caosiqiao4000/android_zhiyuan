package com.wachoo.pangker.entity;

import java.util.List;

public class ResShowObject {
	private List<ResShowEntity> entities = null;

	public List<ResShowEntity> getEntities() {
		return entities;
	}

	public void setEntities(List<ResShowEntity> entities) {
		this.entities = entities;
	}

}
