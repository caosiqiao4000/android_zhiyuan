package com.wachoo.pangker.entity;

import java.io.Serializable;

public class MsgInfo implements Serializable {

	public final static int MSG_NOTICE = 0;// 推荐消息
	public final static int MSG_PERSONAL_CHAT = 1;// 私聊消息
	public final static int MSG_GROUP_CHAT = 2;// 群聊消息
	public final static int MSG_RECOMMEND = 3;// 推荐消息
	public final static int MSG_APPLY = 0;// 系统消息
	public final static int MSG_INVITION = 0;// 系统消息

	public final static int MSG_TIP = 4;// 消息提示

	// >>> Voice prompts
	public static int[] NOT_VOICE_PROMPTS_MESSAGE = new int[] { MSG_TIP };
	public static int[] NOT_NOTIFICATION_PROMPTS_MESSAGE = new int[] { MSG_TIP };

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long msgId;
	private String content;
	private String theme;
	private String userId;
	private String fromId;// 可能是群组id，也可能是用户Id
	private int type;// 1）好友申请，其他用户关注你的提示。 //2）推荐资源的统一管理。 //3）邀请加入群组统一管理。
						// 4）显示私聊（一对一）的历史记录 //5）显示群组聊天的历史记录 //6）通讯录有人注册消息
	private String time;
	private int uncount;
	private int resType;// 资源类型,默认为0

	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFromId() {
		return fromId;
	}

	public void setFromId(String fromId) {
		this.fromId = fromId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getUncount() {
		return uncount;
	}

	public void setUncount(int uncount) {
		this.uncount = uncount;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public int getResType() {
		return resType;
	}

	public void setResType(int resType) {
		this.resType = resType;
	}

}
