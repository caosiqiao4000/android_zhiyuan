package com.wachoo.pangker.entity;

import java.io.Serializable;

import com.wachoo.pangker.util.Util;

@SuppressWarnings("serial")
public class ResShowEntity implements Serializable {

	public static final String RES_ENTITY = "resShowEntity";

	private long shareTimes;// 分享次数
	private long downloadTimes;// 下载次数
	private long favorTimes;// 收藏次数
	private long commentTimes;// 评论次数
	private long resID;
	private String resName;
	private String dirid;
	private String shareId;//资源转播的Id，针对资源而言
	private int resType;
	private int score;
	private String shareUid;// 转播人id （分享用户的UserID）
	private boolean ifshare;
	private String singer;
	private long duration;
	private double fileSize;//文件大小，用于网络音乐播放进行下载
	private String fromat;
	private int source;//来源
	private boolean isKeep = false;

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getResType() {
		return resType;
	}

	public void setResType(int resType) {
		this.resType = resType;
	}

	public long getShareTimes() {
		return shareTimes;
	}

	public void setShareTimes(long shareTimes) {
		this.shareTimes = shareTimes;
	}

	public long getDownloadTimes() {
		return downloadTimes;
	}

	public void setDownloadTimes(long downloadTimes) {
		this.downloadTimes = downloadTimes;
	}

	public long getFavorTimes() {
		return favorTimes;
	}

	public void setFavorTimes(long favorTimes) {
		this.favorTimes = favorTimes;
	}

	public long getCommentTimes() {
		return commentTimes;
	}

	public void setCommentTimes(long commentTimes) {
		this.commentTimes = commentTimes;
	}

	public long getResID() {
		return resID;
	}

	public void setResID(long resID) {
		this.resID = resID;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getShareUid() {
		if (Util.isEmpty(shareUid)) {
			return "";
		}
		return shareUid;
	}

	public void setShareUid(String shareUid) {
		this.shareUid = shareUid;
	}

	public boolean isIfshare() {
		return ifshare;
	}

	public void setIfshare(boolean ifshare) {
		this.ifshare = ifshare;
	}

	public String getDirid() {
		return dirid;
	}

	public void setDirid(String dirid) {
		this.dirid = dirid;
	}

	public String getShareId() {
		return shareId;
	}

	public void setShareId(String shareId) {
		this.shareId = shareId;
	}

	public double getFileSize() {
		return fileSize;
	}

	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}

	public String getFromat() {
		return fromat;
	}

	public void setFromat(String fromat) {
		this.fromat = fromat;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public boolean isKeep() {
		return isKeep;
	}

	public void setKeep(boolean isKeep) {
		this.isKeep = isKeep;
	}
	
}
