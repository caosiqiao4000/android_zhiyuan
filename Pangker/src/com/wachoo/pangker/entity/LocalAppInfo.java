package com.wachoo.pangker.entity;

import android.graphics.drawable.Drawable;

/**
 * @author zhengjy
 *
 * @version 2012-2-20 
 */

public class LocalAppInfo implements java.io.Serializable  {
	
	public Drawable appIcon;
	public String appName;
	public String appPackage;
	public String appVersion;
	public String appVersioncode;
	public String appPath;
	public long size;
	public int resourceId;
	
	public LocalAppInfo(){
		
	}

	public LocalAppInfo(Drawable appIcon, String appName, String appPackage,
			String appVersion, String appVersioncode, String appPath, long size,int resourceId) {
		super();
		this.appIcon = appIcon;
		this.appName = appName;
		this.appPackage = appPackage;
		this.appVersion = appVersion;
		this.appVersioncode = appVersioncode;
		this.appPath = appPath;
		this.size = size;
		this.resourceId = resourceId;
	}

	public Drawable getAppIcon() {
		return appIcon;
	}

	public void setAppIcon(Drawable appIcon) {
		this.appIcon = appIcon;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppPackage() {
		return appPackage;
	}

	public void setAppPackage(String appPackage) {
		this.appPackage = appPackage;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getAppVersioncode() {
		return appVersioncode;
	}

	public void setAppVersioncode(String appVersioncode) {
		this.appVersioncode = appVersioncode;
	}

	public String getAppPath() {
		return appPath;
	}

	public void setAppPath(String appPath) {
		this.appPath = appPath;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public int getResourceId() {
		return resourceId;
	}

	public void setResourceId(int resourceId) {
		this.resourceId = resourceId;
	}
	

}
