package com.wachoo.pangker.entity.index;

import java.io.Serializable;
import java.util.List;

import com.wachoo.pangker.entity.UserItem;

public class data implements Serializable {
	private int total;
	private boolean exact;
	private List<UserItem> data;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public boolean isExact() {
		return exact;
	}

	public void setExact(boolean exact) {
		this.exact = exact;
	}

	public List<UserItem> getData() {
		return data;
	}

	public void setData(List<UserItem> data) {
		this.data = data;
	}
	

}
