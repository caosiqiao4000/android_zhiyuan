package com.wachoo.pangker.entity.index;

import com.wachoo.pangker.entity.Location;

/**
 * @author wangxin
 *用户位置采集接口的参数对象
 */
public class ParameterUserCollect {
	private String userid;
	private Location lastlocation;
	private User userinfo;
	private boolean pushnotice;
	private String authenticator;
	
	
	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/**
	 * @return the authenticator
	 */
	public String getAuthenticator() {
		return authenticator;
	}
	/**
	 * @param authenticator the authenticator to set
	 */
	public void setAuthenticator(String authenticator) {
		this.authenticator = authenticator;
	}
	/**
	 * @return the lastlocation
	 */
	public Location getLastlocation() {
		return lastlocation;
	}
	/**
	 * @param lastlocation the lastlocation to set
	 */
	public void setLastlocation(Location lastlocation) {
		this.lastlocation = lastlocation;
	}
	
	/**
	 * @return the userinfo
	 */
	public User getUserinfo() {
		return userinfo;
	}
	/**
	 * @param userinfo the userinfo to set
	 */
	public void setUserinfo(User userinfo) {
		this.userinfo = userinfo;
	}
	/**
	 * @return the pushnotice
	 */
	public boolean isPushnotice() {
		return pushnotice;
	}
	/**
	 * @param pushnotice the pushnotice to set
	 */
	public void setPushnotice(boolean pushnotice) {
		this.pushnotice = pushnotice;
	}

}
