package com.wachoo.pangker.entity;

import java.io.File;
import java.io.Serializable;

import mobile.http.MyFilePart;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.downupload.ProgressListener;
import com.wachoo.pangker.downupload.UpLoadManager;

@SuppressWarnings("serial")
public class UploadJob implements Serializable {

	public static final String UPLOAD_JOB_ID = "UPLOAD_JOB_ID";
	public static final String UPLOADJOB_TYPE = "UPLOADJOB_TYPE";
	public static final int UPLOAD_START = 0;// 上传
	public static final int UPLOAD_WAIT = 1;// 准备上传

	public static final int UPLOAD_INITOK = 10;// 准备上传
	public static final int UPLOAD_UPLOAGINDG = 11;// 上传中
	public static final int UPLOAD_SUCCESS = 12;// 成功
	public static final int UPLOAD_FAILED = 13;// 失败
	public static final int UPLOAD_CANCEL = 14;// 取消

	private int id;// 上传Id自增长
	private String userId;// 用户id
	private int type;// 资源类型
	private String filePath;// 本地文件地址
	private String resName;// 资源名称
	private String md5Value;// MD5值
	private String upTime;// 上传时间
	private String ifShare;// 是否分享
	private String desc;// 描述
	private int status;// 上传结果和状态
	private String fileFormat;// 文件格式
	private int fileSize;// 文件长度
	private String resType;// 资源选择类型
	private String dirId;// 目录Id
	private String author;
	private int musicId;
	private String resId;
	private int flagCheckFileExist;
	private MyFilePart filePart;
	private String groupId; // 一次性，并不保存到数据库，因此获取的不一定有值
	private int uploadType = UPLOAD_START;// 上传的网络方式 选择 0:上传；1：等待上传
	private String ctrlon;
	private String ctrlat;
	
	private String upPoi;//对应数据库表字段remark3
	private String realPoi;//对应数据库表字段remark4
	
	public UploadJob() {
		// TODO Auto-generated constructor stub
	}

	// >>>>>>>>>>获取当前上传文件
	public MyFilePart getFilePart() {
		return filePart;
	}

	public MyFilePart initUpload() {
		try {
			this.filePart = new MyFilePart(new File(getFilePath()), PangkerConstant.CONTENT_CHARSET, listener);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return this.filePart;
	}

	public void setFilePart(MyFilePart filePart) {
		this.filePart = filePart;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getMd5Value() {
		return md5Value;
	}

	public void setMd5Value(String md5Value) {
		this.md5Value = md5Value;
	}

	public String getUpTime() {
		return upTime;
	}

	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}

	public String getIfShare() {
		return ifShare;
	}

	public void setIfShare(String ifShare) {
		this.ifShare = ifShare;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getStatus() {
		return status;
	}

	public String getCtrlon() {
		return ctrlon;
	}

	public void setCtrlon(String ctrlon) {
		this.ctrlon = ctrlon;
	}

	public String getCtrlat() {
		return ctrlat;
	}

	public void setCtrlat(String ctrlat) {
		this.ctrlat = ctrlat;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public String getDirId() {
		return dirId;
	}

	public void setDirId(String dirId) {
		this.dirId = dirId;
	}

	public String getAuthor() {
		if (author == null) {
			return "";
		}
		return author;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getMusicId() {
		return musicId;
	}

	public void setMusicId(int musicId) {
		this.musicId = musicId;
	}

	public String getResId() {
		return resId;
	}

	public void setResId(String resId) {
		this.resId = resId;
	}
	
	public String getUpPoi() {
		return upPoi;
	}

	public void setUpPoi(String upPoi) {
		this.upPoi = upPoi;
	}

	public String getRealPoi() {
		return realPoi;
	}

	public void setRealPoi(String realPoi) {
		this.realPoi = realPoi;
	}

	public int getFlagCheckFileExist() {
		return flagCheckFileExist;
	}

	public void setFlagCheckFileExist(int flagCheckFileExist) {
		this.flagCheckFileExist = flagCheckFileExist;
	}

	private UpLoadManager upLoadManager;

	public UpLoadManager getUpLoadManager() {
		return upLoadManager;
	}

	public void setUpLoadManager(UpLoadManager upLoadManager) {
		this.upLoadManager = upLoadManager;
	}

	private ProgressListener listener = new ProgressListener() {

		@Override
		public void transferred(int size, int progress) {
			// TODO Auto-generated method stub
			if (upLoadManager != null)
				upLoadManager.startListenterAction();
		}

		@Override
		public void onTransfState(int state) {
			// TODO Auto-generated method stub
			status = state;
			if (upLoadManager != null)
				upLoadManager.startListenterAction();
		}
	};

	// >>>>>>取消任务
	public void cancelJob() {
		// >>>>>>>>设置取消状态
		setStatus(UPLOAD_CANCEL);
		setUploadType(UPLOAD_START);
		// >>>>>>>通知上传取消
		if (this.filePart != null) {
			filePart.cancel();
		}
	}

	public int getUploadType() {
		return uploadType;
	}

	public void setUploadType(int uploadType) {
		this.uploadType = uploadType;
	}

}
