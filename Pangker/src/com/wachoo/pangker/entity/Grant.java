package com.wachoo.pangker.entity;

import java.io.Serializable;

/**
 *@author wubo
 *@createtime May 7, 2012
 */
public class Grant implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String GRANT_TYPE_CALL = "0";
	public static final String GRANT_TYPE_DRIFT = "1";
	public static final String GRANT_TYPE_YOUZONG = "2";
	public static final String GRANT_TYPE_LOCATION = "3";

	private int grantId;// 标识
	private String uid;// uid
	private String grantName;// 权限名称-->0呼叫1漂移2友踪3查询我的位置信息
	private int grantValue;// 权限值
	private String grantGId;
	private String grantUid;

	public int getGrantId() {
		return grantId;
	}

	public void setGrantId(int grantId) {
		this.grantId = grantId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getGrantName() {
		return grantName;
	}

	public void setGrantName(String grantName) {
		this.grantName = grantName;
	}

	public int getGrantValue() {
		return grantValue;
	}

	public void setGrantValue(int grantValue) {
		this.grantValue = grantValue;
	}

	public Grant() {
		// TODO Auto-generated constructor stub
	}
	
	

	public String getGrantGId() {
		return grantGId;
	}

	public void setGrantGId(String grantGId) {
		this.grantGId = grantGId;
	}

	public String getGrantUid() {
		return grantUid;
	}

	public void setGrantUid(String grantUid) {
		this.grantUid = grantUid;
	}

	public Grant(String uid, String grantName, int grantValue) {
		super();
		this.uid = uid;
		this.grantName = grantName;
		this.grantValue = grantValue;
	}

	public Grant(String uid, String grantName, int grantValue, String grantGId,
			String grantUid) {
		super();
		this.uid = uid;
		this.grantName = grantName;
		this.grantValue = grantValue;
		this.grantGId = grantGId;
		this.grantUid = grantUid;
	}
	
	

}
