package com.wachoo.pangker.image;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.util.ByteArrayBuffer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

public final class AsynImageLoader {

	// dm.density,这个值是因硬件分辨率而异的，如果是屏幕硬件宽为320像素，那么这个值就是1,如果是480,这个值就是480/320,就是1.5
	public static final int PIC_TYPE_MAX = 10000;// 最大 （原图）
	public static final int PIC_TYPE_HD_MAX = 1000; // 超大
	public static final int PIC_TYPE_HD_MIN = 640; // 超大
	public static final int PIC_TYPE_HDPI = 480; // 中大
	public static final int PIC_TYPE_HIPI = 360; // 大
	public static final int PIC_TYPE_MIPI = 300; // 中
	public static final int PIC_TYPE_MDPI = 240; // 小
	public static final int PIC_TYPE_LDPI = 120; // 微小
	public static final int PIC_TYPE_MIN = 60; // 最小

	private static AsynImageLoader asynImageLoader;
	private int mTimerInterval = 300;

	private AsynImageLoader() {
		// TODO Auto-generated constructor stub
		imageCache = new HashMap<String, Bitmap>();
	}

	private Map<String, Bitmap> imageCache;

	public void loadDrawable(ImageView view, String url, OnLoaderListener onLoaderListener) {
		if (imageCache.containsKey(url)) {
			onLoaderListener.onImgLoader(view, imageCache.get(url));
		} else {
			LoadTask task = new LoadTask(view, onLoaderListener);
			AsyncView asyncPoi = new AsyncView(task);
			view.setTag(asyncPoi);
			task.execute(url);
		}
	}

	private LoadTask getLoaderTask(ImageView view) {
		if (view != null) {
			Object obj = view.getTag();
			if (obj instanceof AsyncView) {
				final AsyncView asyncPoi = (AsyncView) view.getTag();
				return asyncPoi.getTask();
			}
		}
		return null;
	}

	private static class AsyncView {

		private final WeakReference<LoadTask> weakReferences;

		public AsyncView(LoadTask task) {
			// TODO Auto-generated constructor stub
			weakReferences = new WeakReference<LoadTask>(task);
		}

		public LoadTask getTask() {
			return weakReferences.get();
		}
	}

	class LoadTask extends AsyncTask<String, String, Bitmap> {

		private final WeakReference<ImageView> txtViewReference;
		private String httpurl;
		private OnLoaderListener onLoaderListener;
		private TimerTask mTimerTask; // 定时器任务
		private Timer mTimer;
		ByteArrayBuffer aba = new ByteArrayBuffer(1024);

		public LoadTask(ImageView view, OnLoaderListener onLoaderListener) {
			super();
			txtViewReference = new WeakReference<ImageView>(view);
			this.onLoaderListener = onLoaderListener;
			mTimerTask = new DownloaderTimerTask();
			mTimer = new Timer();
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			// TODO Auto-generated method stub
			httpurl = params[0];

			BufferedInputStream bis = null;
			InputStream in = null;
			try {
				URL url = new URL(httpurl);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setConnectTimeout(5 * 1000);
				conn.setRequestMethod("GET");
				// 设置范围，格式为Range：bytes x-y;
				// conn.setRequestProperty("Range", "bytes=" + 200 + "-");
				conn.connect();

				mTimer.schedule(mTimerTask, 0, mTimerInterval);
				in = conn.getInputStream();
				bis = new BufferedInputStream(in);
				onLoaderListener.onInitLenght(conn.getContentLength());

				byte[] buffer = new byte[1024];
				int length = 0;
				int done = 0;
				long lastnoticetime = System.currentTimeMillis();
				while ((length = bis.read(buffer)) != -1) {
					done += length;
					aba.append(buffer, 0, length);
					if (System.currentTimeMillis() - lastnoticetime >= 100) {
						lastnoticetime = System.currentTimeMillis();
						publishProgress(String.valueOf(done), null);
					}
				}
				mTimerTask.cancel();
				mTimerTask = null;
				mTimer = null;
				return BitmapFactory.decodeByteArray(aba.toByteArray(), 0, done);
			} catch (Exception e) {
				return null;
			} finally {
				try {
					if (in != null) {
						in.close();
					}
					if (bis != null) {
						bis.close();
					}
				} catch (IOException e) {

				}
			}
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			if (values[0] != null) {
				onLoaderListener.onProgress(Integer.parseInt(values[0]));
			}
			if (values[1] != null) {
				onLoaderListener.onSpeed(values[1]);
			}
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			imageCache.put(httpurl, result);
			onLoaderListener.onImgLoader(getAttachedView(), result);
			aba = null;
		}

		class DownloaderTimerTask extends TimerTask {

			private int doneLen = 0;
			NumberFormat format = NumberFormat.getInstance();

			@Override
			public void run() {
				// TODO Auto-generated method stub
				format.setMaximumFractionDigits(2);
				int len = aba.length();
				double speed = ((double) (len - doneLen)) / mTimerInterval * 1000 / 1024;
				publishProgress(null, format.format(speed) + "KB/S");
				doneLen = len;
			}
		}

		private ImageView getAttachedView() {
			final ImageView view = txtViewReference.get();
			final LoadTask task = getLoaderTask(view);
			if (this == task) {
				return view;
			}
			return null;
		}
	}

	public static AsynImageLoader getAsynImageLoader() {
		if (asynImageLoader == null) {
			asynImageLoader = new AsynImageLoader();
		}
		return asynImageLoader;
	}

	public interface OnLoaderListener {

		public void onInitLenght(int lenght);

		public void onProgress(int done);

		public void onImgLoader(ImageView view, Bitmap bitmap);

		public void onSpeed(String speed);
	}

}
