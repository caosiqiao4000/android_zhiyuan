/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wachoo.pangker.image;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.LruCache;
import android.util.Log;

/**
 * This class holds our bitmap caches (memory and disk).
 */
public class ImageCache {

	private static final String TAG = "ImageCache";
	// Default memory cache size
	private static final int DEFAULT_MEM_CACHE_SIZE = 1024 * 1024 * 20; // 10MB

	// Default disk cache size
	private static final int DEFAULT_DISK_CACHE_SIZE = 1024 * 1024 * 30; // 20MB

	// Compression settings when writing images to disk cache
	private static final CompressFormat DEFAULT_COMPRESS_FORMAT = CompressFormat.JPEG;
	private static final int DEFAULT_COMPRESS_QUALITY = 100;
	private static final int DEFAULT_IMAGE_SIDELENGTH = 150;

	// Constants to easily toggle various caches
	private static final boolean DEFAULT_MEM_CACHE_ENABLED = true;
	private static final boolean DEFAULT_DISK_CACHE_ENABLED = true;
	private static final boolean DEFAULT_CLEAR_DISK_CACHE_ON_START = false;

	private DiskLruCache mDiskCache;
	private LruCache<String, Bitmap> mMemoryCache;
	private ImageCacheParams mImageCacheParams;

	/**
	 * Creating a new ImageCache object using the specified parameters.
	 * 
	 * @param context
	 *            The context to use
	 * @param cacheParams
	 *            The cache parameters to use to initialize the cache
	 */
	public ImageCache(Context context, ImageCacheParams cacheParams) {
		init(context, cacheParams);
	}

	/**
	 * Creating a new ImageCache object using the default parameters.
	 * 
	 * @param context
	 *            The context to use
	 * @param uniqueName
	 *            A unique name that will be appended to the cache directory
	 */
	public ImageCache(Context context, String uniqueName) {
		init(context, new ImageCacheParams(uniqueName));
	}

	/**
	 * Find and return an existing ImageCache stored in a {@link RetainFragment}
	 * , if not found a new one is created with defaults and saved to a
	 * {@link RetainFragment}.
	 * 
	 * @param activity
	 *            The calling {@link FragmentActivity}
	 * @param uniqueName
	 *            A unique name to append to the cache directory
	 * @return An existing retained ImageCache object or a new one if one did
	 *         not exist.
	 */
	public static ImageCache findOrCreateCache(final FragmentActivity activity, final String uniqueName) {
		return findOrCreateCache(activity, new ImageCacheParams(uniqueName));
	}

	/**
	 * Find and return an existing ImageCache stored in a {@link RetainFragment}
	 * , if not found a new one is created using the supplied params and saved
	 * to a {@link RetainFragment}.
	 * 
	 * @param activity
	 *            The calling {@link FragmentActivity}
	 * @param cacheParams
	 *            The cache parameters to use if creating the ImageCache
	 * @return An existing retained ImageCache object or a new one if one did
	 *         not exist
	 */
	public static ImageCache findOrCreateCache(final FragmentActivity activity, ImageCacheParams cacheParams) {

		// Search for, or create an instance of the non-UI RetainFragment
		final RetainFragment mRetainFragment = RetainFragment.findOrCreateRetainFragment(activity
				.getSupportFragmentManager());

		// See if we already have an ImageCache stored in RetainFragment
		ImageCache imageCache = (ImageCache) mRetainFragment.getObject();

		// No existing ImageCache, create one and store it in RetainFragment
		if (imageCache == null) {
			imageCache = new ImageCache(activity, cacheParams);
			mRetainFragment.setObject(imageCache);
		}

		return imageCache;
	}

	/**
	 * Initialize the cache, providing all parameters.
	 * 
	 * @param context
	 *            The context to use
	 * @param cacheParams
	 *            The cache parameters to initialize the cache
	 */
	private void init(Context context, ImageCacheParams mImageCacheParams) {
		this.mImageCacheParams = mImageCacheParams;
		final File diskCacheDir = DiskLruCache.getDiskCacheDir(context, this.mImageCacheParams.uniqueName);

		// Set up disk cache
		if (this.mImageCacheParams.diskCacheEnabled) {
			mDiskCache = DiskLruCache.openCache(context, diskCacheDir, this.mImageCacheParams.diskCacheSize);
			if (mDiskCache != null) {
				mDiskCache.setCompressParams(this.mImageCacheParams.compressFormat,
						this.mImageCacheParams.compressQuality);
				if (this.mImageCacheParams.clearDiskCacheOnStart) {
					mDiskCache.clearCache();
				}
			}
		}

		// Set up memory cache
		if (this.mImageCacheParams.memoryCacheEnabled) {
			mMemoryCache = new LruCache<String, Bitmap>(this.mImageCacheParams.memCacheSize) {
				/**
				 * Measure item size in bytes rather than units which is more
				 * practical for a bitmap cache
				 */
				@Override
				protected int sizeOf(String key, Bitmap bitmap) {
					return bitmap.getRowBytes() * bitmap.getHeight();
				}
			};
		}
	}

	public void addBitmapToCache(String data, Bitmap bitmap, boolean mSaveDiskCache) {
		// Log.i("addbitmap", "path:data" + data + ";bitmap = " + bitmap);
		if (data == null || bitmap == null) {
			return;
		}

		// Add to memory cache
		if (mMemoryCache != null) {
			Log.d(TAG, "mMemoryCache.put=>" + data);
			mMemoryCache.put(data, bitmap);
		}

		// Add to disk cache
		if (mDiskCache != null && !mDiskCache.containsKey(data) && mSaveDiskCache) {
			mDiskCache.put(data, bitmap);
		}
	}

	/**
	 * Get from memory cache.
	 * 
	 * @param data
	 *            Unique identifier for which item to get
	 * @return The bitmap if found in cache, null otherwise
	 */
	public Bitmap getBitmapFromMemCache(String data) {
		if (mMemoryCache != null) {
			final Bitmap memBitmap = mMemoryCache.get(data);
			if (memBitmap != null) {
				Log.i(TAG, "getFromMemCache>>>data=" + data);
				return memBitmap;
			}
		}

		return null;
	}

	/**
	 * Get from disk cache.
	 * 
	 * @param data
	 *            Unique identifier for which item to get
	 * @return The bitmap if found in cache, null otherwise
	 */
	public Bitmap getBitmapFromDiskCache(String data) {
		Log.i(TAG, "DiskCache>>>data=" + data);
		if (mDiskCache != null) {
			return mDiskCache.get(data, this.mImageCacheParams.imageSideLength);
		}
		return null;
	}

	private void clearCaches() {
		mDiskCache.clearCache();
		mMemoryCache.evictAll();
	}

	/**
	 * A holder class that contains cache parameters.
	 */
	public static class ImageCacheParams {
		public String uniqueName;
		public int memCacheSize = DEFAULT_MEM_CACHE_SIZE;
		public int diskCacheSize = DEFAULT_DISK_CACHE_SIZE;
		public CompressFormat compressFormat = DEFAULT_COMPRESS_FORMAT;
		public int compressQuality = DEFAULT_COMPRESS_QUALITY;
		public boolean memoryCacheEnabled = DEFAULT_MEM_CACHE_ENABLED;
		public boolean diskCacheEnabled = DEFAULT_DISK_CACHE_ENABLED;
		public boolean clearDiskCacheOnStart = DEFAULT_CLEAR_DISK_CACHE_ON_START;
		public int imageSideLength = DEFAULT_IMAGE_SIDELENGTH;

		public ImageCacheParams(String uniqueName) {
			this.uniqueName = uniqueName;
		}
	}

	public void removeData(Object data) {
		if (data != null) {
			mMemoryCache.remove(data.toString());
		}
		if (mDiskCache != null) {
			mDiskCache.remove(data.toString());
		}
	}

	public void removeMemoryCache(Object data) {
		if (data != null) {
			Bitmap mBitmap = mMemoryCache.get(data.toString());
			if (mBitmap != null && !mBitmap.isRecycled()) {
				Log.i("ImageCache", "removeMemoryCache>>>data=" + data);
				mBitmap.recycle();
				mBitmap = null;
				mMemoryCache.remove(data.toString());
			}

		}

	}

	public ImageCacheParams getmImageCacheParams() {
		return mImageCacheParams;
	}

	public void setmImageCacheParams(ImageCacheParams mImageCacheParams) {
		this.mImageCacheParams = mImageCacheParams;
	}

	public void removeMemoryCaches() {
		for (String key : mMemoryCache.snapshot().keySet()) {
			Bitmap mBitmap = mMemoryCache.get(key);
			if (mBitmap != null && !mBitmap.isRecycled()) {
				Log.i("ImageCache", "removeMemoryCache>>>key=" + key);
				mBitmap.recycle();
				mBitmap = null;
				mMemoryCache.remove(key);
			}
		}
	}
}
