package com.wifi.locate.model;


public class BluetoothModel {
	public BluetoothModel(){}
	private String name;
	private String address;
	private Integer bondState;
	
	/**
	 * 蓝牙名称 ,地址 , 连接状态 ,搜索和保存的蓝牙集合
	 * @param name
	 * @param address
	 * @param bondState
	 */
	public BluetoothModel(String name, String address, Integer bondState) {
		super();
		this.name = name;
		this.address = address;
		this.bondState = bondState;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getBondState() {
		return bondState;
	}
	public void setBondState(Integer bondState) {
		this.bondState = bondState;
	}
	
}
