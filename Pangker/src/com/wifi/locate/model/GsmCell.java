package com.wifi.locate.model;

/** 基站信息Gsm结构体 */
public class GsmCell {
	// public String mcc = "460"; //
	// public String mnc = "03";
	// public int lac = -1;

	public GsmCell(){}
	public GsmCell(int cid, int signal) {
		this.cid = cid;
		this.signal = signal;
	}
	
	
	private int cid = -1; // 基站ID
	private int signal = -1; // 信号必需
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public int getSignal() {
		return signal;
	}
	public void setSignal(int signal) {
		this.signal = signal;
	}
	
	
}
