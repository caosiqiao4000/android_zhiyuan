package com.wifi.locate.model;

/** 基站信息Cdma结构体 */
public class CdmaCell {
	// public int lon;
	// public int lat;b
	// public int lac;
	// public String mcc = "460";
	// public String mnc = "03";
	// public int systemId = -1;
	// public int networkId = -1;
	// public int stationId = -1;
	public CdmaCell(){}
	public CdmaCell(int cid, int signal) {
		this.cid = cid;
		this.signal = signal;
	}
	
	private  int cid;          // 基站ID
	private int signal = -1;  // 信号必需
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public int getSignal() {
		return signal;
	}
	public void setSignal(int signal) {
		this.signal = signal;
	}
}