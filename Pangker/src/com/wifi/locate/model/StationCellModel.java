package com.wifi.locate.model;

public class StationCellModel {
	
	public StationCellModel() {}
	/**
	 * 0 表示未知, 1 表示GSM基站 2 .表示 CDMA基站
	 */
	private int cellType = 0;
	
	private int cid = -1;    // 单个基站小区的编号 ID
	private int signal = -1; // 信号必需
	
	public StationCellModel(int cellType, int cid, int signal) {
		super();
		this.cellType = cellType;
		this.cid = cid;
		this.signal = signal;
	}
	public int getCellType() {
		return cellType;
	}
	public void setCellType(int cellType) {
		this.cellType = cellType;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public int getSignal() {
		return signal;
	}
	public void setSignal(int signal) {
		this.signal = signal;
	}
}
