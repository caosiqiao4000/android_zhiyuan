package com.wifi.locate.model;

public class SimpleWifiInfo {
	public SimpleWifiInfo(){}
	
	/**
	 * 
	 * @param mac          连接到AP时是AP 的MAC 无连接时是手机WIFI的MAC
	 * @param rSSI         信号强度
	 * @param ssid         SSID（Service Set Identifier）AP唯一的ID码，用来区分不同的网络，
	 * 最多可以有32个字符，无线终端和AP的SSID必须相同方可通信
	 * @param bssid        通常的bssid就是mac地址 
	 * @param primaryType  是WIFI路由还是 wifi-direct
	 */
	public SimpleWifiInfo(String mac, Integer rSSI, String ssid, String bssid) {
		super();
		this.mac = mac;
		this.RSSI = rSSI;
		this.ssid = ssid;
		this.bssid = bssid;
	}

	private  String mac;
	private Integer RSSI = -1;   
	private String ssid;   
	private String bssid;

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public Integer getRSSI() {
		return RSSI;
	}

	public void setRSSI(Integer rSSI) {
		RSSI = rSSI;
	}

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public String getBssid() {
		return bssid;
	}

	public void setBssid(String bssid) {
		this.bssid = bssid;
	}
}

