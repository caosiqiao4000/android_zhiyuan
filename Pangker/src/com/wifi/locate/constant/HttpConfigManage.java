package com.wifi.locate.constant;

/**
 * 网络相关参数
 * @author Administrator
 * 
 */
public class HttpConfigManage {
	/** 用户同步查询基本接口地址 */
	public static final String REQUEST_BASE_IP = "119.147.1.212:8077";
	public static final String REQUEST_BASE_URI = "http://"+REQUEST_BASE_IP+"/CTRL/";

	public static final String PACKAGE_PREFIX = "ctowo.activity.";
	/** 程序的包名空间 */
	public static final String PACKAGE_NAME = "com.gps.ctowo";

	/** 查询周边用户接口 */
	public static final String USER_CLICK_UPDATA_URI = REQUEST_BASE_URI
			+ "getnearusers.json";
	/** 终端信息同步接口 */
	public static final String USER_SYNC_URI = REQUEST_BASE_URI
			+ "collectSignal.json";
	/** 旁客用户信息同步接口 */
	public static final String PKUSER_SYNC_URI = REQUEST_BASE_URI
			+ "pkuser2ctrl.json";
	/** 用户信息退出接口 */
	public static final String USER_EXIT_URI = REQUEST_BASE_URI
			+ "stopservice.json";

	/** 用户信息同步等待提示 */
	public static final String HTTP_REQUEST_WAIT = "正在发送请求,请等待...";

	/** 同步指纹数据*/
	public static final Integer REQUEST_NOTIFYDATA = 1;
	/** 查询请求 */
	public static final Integer REQUEST_SEARCHNEARUSERS = 2;
	/** 退出 */
	public static final Integer REQUEST_CASEkEY_THREE = 3;

	/** 服务器响应的返回数据 (这是邻近集合) */
	public static final String RESPONSE_BUNDLE_DATA = PACKAGE_PREFIX + "response.json";

	public static final String EMPTY_MESSAGE = PACKAGE_PREFIX + "empty_message";
	/**
	 * 额外的响应数据
	 */
	public static final String EXTRA_RESPONSE_DATA = "extraResponse";
	/**
	 * 设置会议模式
	 */
	public static final String SET_MEETING_ACTION = "set_Meeting_Action";
	public static final String SET_MEETING_SILENCE = "set_Meeting_Silence";

	/**
	 * 返回的邻近集合标识
	 */
	public static final String RESPONE_NEAR_LIST = "responeNearList";
	/**
	 * 返回的传递集合标识
	 */
	public static final String RESPONE_TRANSMIT_LIST = "responeTransmitList";

	// *********************************** 程序用的各种字段
	// *****************************************************************
	/**
	 * 设置页面的字段
	 */
	public static final String FUNCTION_STATU_YES = "已启用";
	public static final String FUNCTION_STATU_NO = "已停用";
	public static final String FUNCTION_STATU_NOKNOW = "末知";
	/**
	 * 更新本程序的用户名
	 */
	public static final String ACTION_LOCAL_USER_NAME_CHANGED = "updateUserName";
	/**
	 * 额外的 传递次数更改 查询KEY
	 */
	public static final String NEARLEVEL = "nearlevel";
	
	
	/**
	 * 
	 */
	public static final String ACTION_WIFI_NO_NETWORK = "wifi_no_network";
	public static final String ACTION_WIFI_STATU_ON = "wifi_on";
	public static final String ACTION_WIFI_STATU_OFF = "wifi_off";
	public static final String ACTION_GPS_STATU_ON = "gps_on";
	public static final String ACTION_GPS_STATU_OFF = "gps_off";
	public static final String ACTION_BLUETOOTH_STATE_ON = "blue_on";
	public static final String ACTION_BLUETOOTH_STATE_OFF = "blue_off";
	public static final String FILED_USER_NAME = "userName";
	
	/**
	 * 邻近用户数据更新  用户重新定位后 要重新发送会议通知
	 */
	public static final String NEARMAP_DATA_CHANAGED = "nearDataChanaged";
	/**
	 *   常量字段 userid
	 */
	public static final String FILED_USERID = "userid";
	/**
	 *   常量字段 limit
	 */
	public static final String FILED_LIMIT = "limit";
	/**
	 *   常量字段 signalinfo
	 */
	public static final String FILED_SIGNALINFO_NAME = "signalinfo";
	/**
	 *   常量字段 isExit
	 */
	public static final String FILED_ISEXIT_NAME = "isExit";
	

	/**
	 *  ==================================================================================openfire
	 * 用户发送消息的OPENFIRE基本接口地址
	 */
	 public static final String OPENFIRE_BASE_URI = "192.168.3.112";
//	public static final String OPENFIRE_BASE_URI = "121.32.89.218";
	/**
	 * 用户发送消息的OPENFIRE基本端口
	 */
	public static final int OPENFIRE_BASE_PORT = 5222;

	/**
	 * 应用标识ID
	 */
	public static final String OPENFIRE_DOMAIN_ID = "relativePosition";
	public static final String OPENFIRE_HOST_NAME = "@4dzv2rfpmoflcyk/";
	/**
	 * 接收人的operfire后缀
	 */
	public static final String OPENFIRE_PANGKER_DOMAIN = OPENFIRE_HOST_NAME + OPENFIRE_DOMAIN_ID;
	public static final String BUSINESS_MESSAGE = "BUSINESS_MESSAGE"; // 联系人改变消息类型0：问答，1：资源，2：群组


	

}
