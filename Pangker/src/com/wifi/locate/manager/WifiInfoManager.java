package com.wifi.locate.manager;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.wifi.locate.model.SimpleWifiInfo;

/**
 * wifi信息获取管理类
 * 
 * @author Administrator
 */
public class WifiInfoManager {
	private final static String TAG = "WifiInfoManager";
	private WifiManager mWifiManager;
	private Context context;

	private long startScan;
	private boolean isCleanWifiSet = false;
	// 判断使用哪个集合
	private List<SimpleWifiInfo> cacheList;
	private List<String> cacheListSet;

	// private final static String actionString =
	// WifiManager.SCAN_RESULTS_AVAILABLE_ACTION;

	public WifiInfoManager(Context context, WifiManager mWifiManager) {
		this.context = context;
		this.mWifiManager = mWifiManager;
		cacheList = new ArrayList<SimpleWifiInfo>();
		cacheListSet = new ArrayList<String>();
		Log.i(TAG, "-----------------WifiInfoAdmin......init...");
	}

	/**
	 * 
	 */
	private void getWifiResult() {

		// final WifiInfo mWifiInfo = mWifiManager.getConnectionInfo();
		// final String connectWifiBssid;
		// if (mWifiInfo != null) {
		// connectWifiBssid = mWifiInfo.getBSSID();
		// SimpleWifiInfo connectWifi = new SimpleWifiInfo();
		// connectWifi.setMac(mWifiInfo.getMacAddress());
		// connectWifi.setRSSI(mWifiInfo.getRssi());
		// connectWifi.setSsid(mWifiInfo.getSSID());
		// connectWifi.setBssid(connectWifiBssid);
		// if (cacheListSet.add(connectWifi)) {
		// cacheList.add(connectWifi);
		// }
		// wifiFirstList.add(connectWifi);
		// } else {
		// connectWifiBssid = "";
		// }

		List<ScanResult> sr = mWifiManager.getScanResults();
		for (ScanResult s : sr) {
			// 网络连接列表
			// if (s.BSSID.equals(connectWifiBssid)) {
			// continue;
			// }
			SimpleWifiInfo wifiInfo = new SimpleWifiInfo();
			wifiInfo.setSsid(s.SSID);
			wifiInfo.setRSSI(s.level);
			wifiInfo.setBssid(s.BSSID);
			final String a = s.BSSID;
			if (!cacheListSet.contains(a)) {
				cacheListSet.add(a);
				cacheList.add(wifiInfo);
			}
		}
	}

	/**
	 * wifi 连接时 是取得 AP的mac
	 * "mac":"40:4D:8E:E0:39:D3","ssid":"CTOWO","bssid":"14:d6:4d:80:d6:c4",
	 * 末连接时取得 是 手机的 MAC {"ssid":"CTOWO","bssid":"14:d6:4d:80:d6:c4"}
	 * {"mac":"00:26:fc:f8:33:e3","
	 * 
	 * @return
	 */
	public List<SimpleWifiInfo> getWifiInfo() {
		try {
			// if (mWifiManager.startScan()) { // 开始扫描周边
			// wifiFirstList.clear();
			mWifiManager.startScan();
			if (System.currentTimeMillis() - startScan > 5 * 60 * 2 * 1000) {
				isCleanWifiSet = true;
			} else {
				isCleanWifiSet = false;
			}

			if (isCleanWifiSet) {
				startScan = System.currentTimeMillis();
				cacheList.clear();
				cacheListSet.clear();
			}

			getWifiResult();
			/**
			 * 可能因为安卓手机操作系统源码被改过的原因, WIFI扫描完成的广播在有些手机上无法正常接收
			 */
			// 为了加强WIFI源的稳定性
			Log.i(TAG, "return addWifiFirstList... size = " + cacheList.size());
			return cacheList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void closeReceiver() {
		// context.unregisterReceiver(receiver);
	}
}
