package com.wifi.locate.manager;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.util.Log;

import com.wachoo.pangker.entity.Limit;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wifi.locate.constant.HttpConfigManage;
import com.wifi.locate.model.UserInfoModel;

public class WifiLocateManager {
	private static final String TAG = "WifiLocateManager";
	private Context mContext;

	private WifiLocateTool mWifiLocateTool;
	private ServerSupportManager mSupportManager;

	public WifiLocateManager(Context mContext, IUICallBackInterface callBackInterface) {
		this.mContext = mContext;
		mWifiLocateTool = new WifiLocateTool(this.mContext);
		mSupportManager = new ServerSupportManager(this.mContext, callBackInterface);
	}

	/**
	 * 发起HTTP异步请求
	 * 
	 * @param requestType
	 *            1 用户同步请求 2 用户查询请求 3 用户退出请求
	 * @param model 用户信息(包含指纹数据)
	 * @param limit 查询区间 可为null
	 * @param nearLevel
	 *            传递查询级别(默认为1 表示不传递查询)
	 */
	private void requestHttp(int requestType, UserInfoModel model, Limit limit, int nearLevel) {
		final List<Parameter> parsa = new ArrayList<Parameter>();
		if (requestType == 1) {// 数据同步
			final String json = JSONUtil.toJson(model, false);

			parsa.add(new Parameter(HttpConfigManage.FILED_SIGNALINFO_NAME, json));

			mSupportManager.supportRequest(HttpConfigManage.USER_SYNC_URI, parsa, false, "", requestType);

			Log.i(TAG, "发送同步请求: " + requestType);
		} else if (requestType == 2) {// 查询周边用户
			// >>>>>>>>>>递归次数
			parsa.add(new Parameter(HttpConfigManage.NEARLEVEL, String.valueOf(nearLevel)));// 传递次数

			// >>>>>>>区间
			String limitString = "";
			if(limit != null){
				limitString = JSONUtil.toJson(limit, false);
			}
			parsa.add(new Parameter(HttpConfigManage.FILED_LIMIT, limitString));// 查询区间

			// >>>>>>>用户信息
			final String json = JSONUtil.toJson(model, false);
			parsa.add(new Parameter(HttpConfigManage.FILED_SIGNALINFO_NAME, json));

			// >>>>>>>接口调用
			mSupportManager.supportRequest(HttpConfigManage.USER_CLICK_UPDATA_URI, parsa, false, "",
					requestType);
			Log.i(TAG, "发送查询请求: " + requestType);
		} else if (requestType == 3) { // 退出服务
			parsa.add(new Parameter(HttpConfigManage.FILED_USERID, model.getUserid()));
			parsa.add(new Parameter(HttpConfigManage.FILED_ISEXIT_NAME, "yes"));
			mSupportManager.supportRequest(HttpConfigManage.USER_EXIT_URI, parsa, false, "", requestType);

		}
	}

	/**
	 * 通过wifi信息查询当前用户附近的用户
	 * 
	 * @param userID
	 * @param limit
	 * @param nearlevel
	 */
	public void searchNearUsers(String userID, Limit limit, int nearlevel) {
		UserInfoModel model = mWifiLocateTool.obtainUserInfos(userID);
		if(model != null && model.getWifis() != null){
			requestHttp(HttpConfigManage.REQUEST_SEARCHNEARUSERS, model, limit,
					nearlevel);
		}
		
	}

	/**
	 * 删除相对位置信息
	 */
	public void removeUserWifiInfo(String userID) {
		UserInfoModel model = new UserInfoModel(userID);
		requestHttp(HttpConfigManage.REQUEST_CASEkEY_THREE, model, null, 1);
	}

	public void collectUserWifiInfo(String userID) {
		UserInfoModel model = mWifiLocateTool.obtainUserInfos(userID);
		if(model != null && model.getWifis() != null){
			// 向服务器发起数据同步请求
			requestHttp(HttpConfigManage.REQUEST_NOTIFYDATA, model, null, 1);
		}
	}
}
