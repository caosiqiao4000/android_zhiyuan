package com.wifi.locate.manager;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.telephony.CellLocation;

import com.wifi.locate.constant.HttpConfigManage;
import com.wifi.locate.model.SimpleWifiInfo;
import com.wifi.locate.model.UserInfoModel;

/**
 * 用户信息 获取类
 * 
 * @author Administrator
 */
public class WifiLocateTool {
	private final Context context;

	private CellLocation cellLocation;

	// wifi and wifiDirect
	private final WifiManager mWifiManager;
	private final WifiInfoManager wifiInfoAdmin;
	//
	// private BluetoothAdmin bluetoothAdmin;
	// private final BluetoothAdapter mBluetoothAdapter;

	private UserInfoModel model;

	// private static boolean isPing = false;
	private final static String TAG = "WifiLocateTool";

	/*
	 * 当 监测到功能状态改变时发出通知
	 * 
	 * @author caosq
	 */
	public WifiLocateTool(Context context) {
		this.context = context;
		mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		wifiInfoAdmin = new WifiInfoManager(context, mWifiManager);
	}

	// >>>>>>>>>.判断wifi是否开启
	public boolean isWifiEnabled() {
		return mWifiManager.isWifiEnabled();
	}

	/**
	 * 此方法获取用户的 wifi 基站 bluetooth 和 sensor信息
	 * 
	 * @return
	 */
	public UserInfoModel obtainUserInfos(String userId) {
		try {
			final Context context = WifiLocateTool.this.context;
			if (model == null) {
				model = new UserInfoModel(userId);
			}
			// =======================================================
			model.setTime(System.currentTimeMillis());

			try {
				// WIFI 信息
				if (mWifiManager.isWifiEnabled()) {
					// >>>>>>>通知用户wifi没有连上
					context.sendBroadcast(new Intent(HttpConfigManage.ACTION_WIFI_STATU_ON));

					final ArrayList<SimpleWifiInfo> cWifiInfos = (ArrayList<SimpleWifiInfo>) wifiInfoAdmin
							.getWifiInfo();
					if (!cWifiInfos.isEmpty()) {
						model.setWifis(cWifiInfos);
					} else {
						model.setWifis(null);
					}
					// 这是要设置 否则与直连功能轮换时会不为空
					model.setWifip2p(null);
				} else {
					context.sendBroadcast(new Intent(HttpConfigManage.ACTION_WIFI_STATU_OFF));
					model.setWifis(null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return model;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
