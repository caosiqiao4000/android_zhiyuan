package com.wifi.locate.model;

import java.io.Serializable;

public class Parameter implements Serializable,Comparable<Object>{
	private static final long serialVersionUID = 201207020942L;

	public String mName;

	public Parameter() {
		super();
	}

	public String mValue;

	public Parameter(String name, String value) {
		this.mName = name;
		this.mValue = value;
	}
	
	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmValue() {
		return mValue;
	}

	public void setmValue(String mValue) {
		this.mValue = mValue;
	}

	public boolean equals(Object arg0) {
		if (null == arg0) {
			return false;
		}
		if (this == arg0) {
			return true;
		}
		if (arg0 instanceof Parameter) {
			Parameter param = (Parameter) arg0;
			return this.mName.equals(param.mName)
					&& this.mValue.equals(param.mValue);
		}
		return false;
	}

	public int compareTo(Object o) {
		int compared;
		Parameter param = (Parameter) o;
		compared = mName.compareTo(param.mName);
		if (0 == compared) {
			compared = mValue.compareTo(param.mValue);
		}
		return compared;
	}
}
