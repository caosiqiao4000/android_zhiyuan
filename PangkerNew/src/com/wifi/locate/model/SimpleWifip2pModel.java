package com.wifi.locate.model;


/*
 * wifi-direct 简单模型
 */
public class SimpleWifip2pModel {
	public SimpleWifip2pModel(){}
	private String name;
	private String mac;
	private Integer bondState;
	
	/**
	 * wifi direct 名称 ,地址 , 连接状态 ,搜索和保存的
	 * @param name
	 * @param address
	 * @param bondState
	 */
	public SimpleWifip2pModel(String name, String mac, Integer bondState) {
		super();
		this.name = name;
		this.mac = mac;
		this.bondState = bondState;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public Integer getBondState() {
		return bondState;
	}
	public void setBondState(Integer bondState) {
		this.bondState = bondState;
	}
	
}
