package com.wifi.locate.model;

import java.util.List;



/**
 * 终端请求类
 * 
 * @author Administrator
 */
@SuppressWarnings("serial")
public class UserInfoModel implements java.io.Serializable {
	public UserInfoModel() {
	}

	private String userid; // 用户标识
	private String userName; // 手机型号或用户名
	private long time;

	private List<SimpleWifiInfo> wifis;
	private List<SimpleWifip2pModel> wifip2p;
	private List<StationCellModel> stationCell;
	private List<BluetoothModel> bluetoothList;
	private GPSInfoModel gpsInfoModel;

	public UserInfoModel(String userid, String userName, long time, List<SimpleWifiInfo> wifis,
			List<StationCellModel> stationCell, List<BluetoothModel> bluetoothList,
			List<SimpleWifip2pModel> wifip2p, GPSInfoModel gpsInfoModel) {
		super();
		this.userid = userid;
		this.userName = userName;
		this.time = time;
		this.wifis = wifis;
		this.setStationCell(stationCell);
		this.wifip2p = wifip2p;
		this.bluetoothList = bluetoothList;
		this.gpsInfoModel = gpsInfoModel;
	}
	
	public UserInfoModel(String userid) {
		super();
		this.userid = userid;
	}

	public List<SimpleWifip2pModel> getWifip2p() {
		return wifip2p;
	}

	public void setWifip2p(List<SimpleWifip2pModel> wifip2p) {
		this.wifip2p = wifip2p;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

 	public GPSInfoModel getGpsInfoModel() {
 		return gpsInfoModel;
 	}

 	public void setGpsInfoModel(GPSInfoModel gpsInfoModel) {
 		this.gpsInfoModel = gpsInfoModel;
 	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public List<SimpleWifiInfo> getWifis() {
		return wifis;
	}

	public void setWifis(List<SimpleWifiInfo> wifis) {
		this.wifis = wifis;
	}

	public List<BluetoothModel> getBluetoothList() {
		return bluetoothList;
	}

	public void setBluetoothList(List<BluetoothModel> bluetoothList) {
		this.bluetoothList = bluetoothList;
	}

	@Override
	public String toString() {

		return getUserid() + "  " + getUserName() + "  " + getBluetoothList() + "  "
				+ getStationCell() + "  " + getWifis() + "  " + getGpsInfoModel();
	}

	public List<StationCellModel> getStationCell() {
		return stationCell;
	}

	public void setStationCell(List<StationCellModel> stationCell) {
		this.stationCell = stationCell;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUserid() {
		return userid;
	}

}
