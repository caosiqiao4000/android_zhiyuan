package com.wifi.locate.model;

public class GPSInfoModel {
	public GPSInfoModel(){}
	
	private Double  latitude; //维度
	private Double longitude; // 经度
	private String provider;  // 提供者
	private String chinaAddress;
	
	public String getChinaAddress() {
		return chinaAddress;
	}
	public void setChinaAddress(String chinaAddress) {
		this.chinaAddress = chinaAddress;
	}
	/**
	 *  维度  经度  提供者
	 * @param latitude
	 * @param longitude
	 * @param provider
	 */
	public GPSInfoModel(Double latitude, Double longitude, String provider) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.provider = provider;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	
}
