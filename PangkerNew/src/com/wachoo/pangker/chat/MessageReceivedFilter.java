package com.wachoo.pangker.chat;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Packet;

public class MessageReceivedFilter implements PacketFilter {

	@Override
	public boolean accept(Packet paramPacket) {
		// TODO Auto-generated method stub
		if (paramPacket instanceof Message) {
			Message message = (Message) paramPacket;
			if (message.getType() == Type.headline) {
				return true;
			}
		}
		return false;

	}
}
