package com.wachoo.pangker.chat;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Packet;

import android.content.Context;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.util.Util;

public class PacketChatmsgFilter implements PacketFilter {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	private Context context;
	private PangkerApplication application;

	public PacketChatmsgFilter(Context context) {
		this.context = context;
		this.application = (PangkerApplication)this.context.getApplicationContext();
	}

	@Override
	public boolean accept(Packet paramPacket) {
		// TODO Auto-generated method stub
		if (paramPacket instanceof Message) {
			Message message = (Message) paramPacket;
			Object property = message.getProperty(PangkerConstant.BUSINESS_MESSAGE);
			if (message.getType() == Type.chat && property == null) {
				// 黑名单判断
				String userid = Util.trimUserIDDomain(message.getFrom());
				if (!application.IsBlacklistExist(userid))
					return true;
			}
		}
		return false;
	}
}
