package com.wachoo.pangker.chat;

import org.jivesoftware.smack.packet.PacketExtension;

public class MessageRecivedPacketExtension implements PacketExtension {

	private long sendMessageID = 0l;
	private String ElementName = "request";
	private String Namespace = "urn:xmpp:receipts";

	public long getSendMessageID() {
		return sendMessageID;
	}

	public void setSendMessageID(long sendMessageID) {
		this.sendMessageID = sendMessageID;
	}

	public MessageRecivedPacketExtension(long sendMessageID) {
		super();
		this.sendMessageID = sendMessageID;
	}

	@Override
	public String getElementName() {
		// TODO Auto-generated method stub
		return ElementName;
	}

	@Override
	public String getNamespace() {
		// TODO Auto-generated method stub
		return Namespace;
	}

	@Override
	public String toXML() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\" id=\"")
				.append(getSendMessageID()).append("\"/>");
		return sb.toString();
	}

}
