package com.wachoo.pangker.chat;

import com.wachoo.pangker.entity.MsgInfo;

public interface IMessageListener {
	public void messageChanged(MsgInfo msgInfo);
}
