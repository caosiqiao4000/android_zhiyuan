package com.wachoo.pangker.chat;

public interface ILoggedChecker {

	// >>>>>>>判断是否显示在线
	boolean isShowLogged();

	// >>>>>>>在线通知
	void loggedNotice();

	// >>>>>>>离线通知
	void noLoggedNotice();

	// >>>>>>>>没有网络
	void noNetworkNotice();

	// >>>>>>正在连接的通知
	void connectingNotice();

}
