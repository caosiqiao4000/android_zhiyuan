package com.wachoo.pangker.chat;


public class PresenceManager {

	private static String TAG = com.wachoo.pangker.util.Util.getClassName();// log
																			// tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

//	/**
//	 * Returns true if the user is online.
//	 * 
//	 * @param jid
//	 *            the jid of the user.
//	 * @return true if online.
//	 */
//	public static boolean isFriendOnline(String jid, OpenfireManager manager) {
//		if (!manager.isLoggedIn())
//			return false;
//		// 上面方法只支撑好友的在线
//		final Roster roster = manager.getConnection().getRoster();
//		Presence presence = roster.getPresence(Util.addUserIDDomain(jid));
//		return presence.isAvailable();
//
//	}
//
//	/**
//	 * 判断openfire用户的状态 strUrl : url格式 -
//	 * http://my.openfire.com:9090/plugins/presence
//	 * /status?jid=user1@my.openfire.com&type=xml 返回值 : -1 - 用户不存在; 2 - 用户在线; 6
//	 * - 用户离线 说明 ：必须要求 openfire加载 presence 插件，同时设置任何人都可以访问
//	 */
//	public static int IsUserOnLine(String userid) {
//		short shOnLineState = -1; // -不存在-
//		try {
//			userid = Util.addUserIDDomain(userid);
//			// http://192.168.3.22:9090/plugins/presence/status
//			String strUrl = "http://119.147.1.212:9090/plugins/presence/status?jid=" + userid + "&type=text";
//			URL oUrl = new URL(strUrl);
//			URLConnection oConn = oUrl.openConnection();
//			if (oConn != null) {
//				BufferedReader oIn = new BufferedReader(new InputStreamReader(oConn.getInputStream()));
//				if (null != oIn) {
//					String strFlag = oIn.readLine();
//					oIn.close();
//
//					// if (strFlag.indexOf("type=\"unavailable\"") >= 0) {
//					// shOnLineState = 6;
//					// }
//					// if (strFlag.indexOf("type=\"error\"") >= 0) {
//					// shOnLineState = -1;
//					// } else if (strFlag.indexOf("priority") >= 0 ||
//					// strFlag.indexOf("id=\"") >= 0) {
//					// shOnLineState = 2;
//					// }
//
//					//
//					oIn.close();
//					if (strFlag.equals("Unavailable")) {
//						shOnLineState = 6;
//					} else {
//						shOnLineState = 2;
//					}
//				}
//			}
//		} catch (Exception e) {
//			logger.error(TAG, e);
//		}
//
//		return shOnLineState;
//	}
//
//	/**
//	 * Returns true if the user is online and their status is available or free
//	 * to chat.
//	 * 
//	 * @param jid
//	 *            the jid of the user.
//	 * @return true if the user is online and available.
//	 */
//	public static boolean isAvailable(String jid, OpenfireManager manager) {
//		// final Roster roster = manager.getRoster();
//		// 暂时测试使用后缀
//		// Presence presence = roster.getPresence(jid + "/Spark 2.6.3");
//		// return presence.isAvailable() && !presence.isAway();
//		return false;
//	}
//
//	/**
//	 * Returns true if the user is online and their mode is available or free to
//	 * chat.
//	 * 
//	 * @param presence
//	 *            the users presence.
//	 * @return true if the user is online and their mode is available or free to
//	 *         chat.
//	 */
//	public static boolean isAvailable(Presence presence) {
//		return presence.isAvailable() && !presence.isAway();
//	}
//
//	/**
//	 * Returns the presence of a user.
//	 * 
//	 * @param jid
//	 *            the users jid.
//	 * @return the users presence.
//	 */
//	public static Presence getPresence(String jid, OpenfireManager manager) {
//		// final Roster roster = manager.getRoster();
//		return null;
//	}
//
//	/**
//	 * Returns the fully qualified jid of a user.
//	 * 
//	 * @param jid
//	 *            the users bare jid (ex. derek@jivesoftware.com)
//	 * @return the fully qualified jid of a user (ex. derek@jivesoftware.com -->
//	 *         derek@jivesoftware.com/spark)
//	 */
//	public static String getFullyQualifiedJID(String jid, OpenfireManager manager) {
//		// final Roster roster = manager.getRoster();
//		// Presence presence = roster.getPresence(jid);
//		return "";
//	}
//
//	/**
//	 * Returns the Drawable associated with a users presence.
//	 * 
//	 * @param presence
//	 *            the users presence.
//	 * @return the Drawable associated with it.
//	 */
//	public static Drawable getIconFromPresence(Presence presence) {
//		return null;
//	}
}
