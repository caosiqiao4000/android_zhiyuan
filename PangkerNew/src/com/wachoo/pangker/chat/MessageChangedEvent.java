package com.wachoo.pangker.chat;

import java.util.EventObject;

import com.wachoo.pangker.entity.MsgInfo;

public class MessageChangedEvent extends EventObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MessageChangedEvent(Object source) {
		super(source);
		// TODO Auto-generated constructor stub
	}

	public MessageChangedEvent(Object source, MsgInfo info) {
		super(source);
		this.info = info;
	}

	public MsgInfo getInfo() {
		return info;
	}

	public void setInfo(MsgInfo info) {
		this.info = info;
	}

	private MsgInfo info;

}
