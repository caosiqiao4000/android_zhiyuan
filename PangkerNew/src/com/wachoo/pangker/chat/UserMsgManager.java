package com.wachoo.pangker.chat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.util.Log;

import com.wachoo.pangker.entity.ChatMessage;

public class UserMsgManager {

	final static String TAG = "UserMsgManager";
	protected List<IUserMsgListener> listenerList = new ArrayList<IUserMsgListener>();

	/**
	 * 添加一个Listener监听器
	 */
	public synchronized void addListenter(IUserMsgListener listener) {
		Log.i(TAG, "add:listener" + listener);
		listenerList.add(listener);
	}
	
	public void exitManager() {
		// TODO Auto-generated method stub
		if(listenerList != null){
			listenerList.clear();
		}
	}

	/**
	 * 移除一个已注册的Listener监听器. 如果监听器列表中已有相同的监听器listener1、listener2,
	 * 并且listener1==listener2, 那么只移除最近注册的一个监听器。
	 */
	public synchronized void removeListenter(IUserMsgListener listener) {
		Log.i(TAG, "remove:listener" + listener);
		listenerList.remove(listener);
	}

	public boolean chatMessageChanged(ChatMessage msg) {
		IUserMsgListener listener = null;
		Iterator<IUserMsgListener> it = listenerList.iterator();// 这步要注意同步问题
		while (it.hasNext()) {

			listener = (IUserMsgListener) it.next();
			Log.i(TAG, "while:listener" + listener);
			if (listener != null)
				return listener.onReceiveUserMsg(msg);
		}
		return false;
	}

	public boolean chatMessageReceived(int messageID) {
		IUserMsgListener listener = null;
		Iterator<IUserMsgListener> it = listenerList.iterator();// 这步要注意同步问题
		while (it.hasNext()) {

			listener = (IUserMsgListener) it.next();
			Log.i(TAG, "while:listener" + listener);
			if (listener != null)
				listener.onMessageReceived(messageID);
		}
		return false;
	}
}
