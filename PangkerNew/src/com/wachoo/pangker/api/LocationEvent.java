package com.wachoo.pangker.api;

import java.util.EventObject;

/**
 * LocationEvent
 * 用户location变化监听使用event
 * @author wangxin
 * 
 */
public class LocationEvent extends EventObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LocationEvent(Object source) {
		super(source);
	}
}
