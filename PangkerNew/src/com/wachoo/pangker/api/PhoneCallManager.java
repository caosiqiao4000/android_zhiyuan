package com.wachoo.pangker.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

/**
 * 用于监听来电的管理类，在Application进行初始化
 * @author zxx
 *
 */
public class PhoneCallManager {

	private TelephonyManager mTelephonyManager;// >>>>lb 监听打电话
	private List<PhoneCallListener> phoneStateListeners;
	
	public PhoneCallManager(Context context) {
		// TODO Auto-generated constructor stub
		mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		phoneStateListeners = new ArrayList<PhoneCallListener>();
		mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
	}
	
	private synchronized void callStateChanged(int state) {
		// TODO Auto-generated method stub
		Iterator<PhoneCallListener> it = phoneStateListeners.iterator();// 这步要注意同步问题
		while (it.hasNext()) {
			PhoneCallListener listener = it.next();
			if(state == TelephonyManager.CALL_STATE_IDLE){
				listener.onCallIdel();
			} else {
				listener.onCallOffHook();
			}
		}
	}
	
	public synchronized void listen(PhoneCallListener phoneCallListener) {
		// TODO Auto-generated method stub
		phoneStateListeners.add(phoneCallListener);
	}
	
	public void destory() {
		// TODO Auto-generated method stub
		mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
		phoneStateListeners.clear();
		phoneStateListeners = null;
	}
	
	PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			// TODO Auto-generated method stub
			callStateChanged(state);
		}
	};
	
	public interface PhoneCallListener {
		 // 另外的情况，来电；拨号；通话。
		public void onCallOffHook();
		 // 当处于待机状态中  
		public void onCallIdel();
	}
	
}
