package com.wachoo.pangker.api;

public interface PlayListener {

	public void onPlayProgress(int seconds);
	

	public void onPlayStop();
	/**
	 * Callback invoked when track is paused
	 */
	public void onPlayPause();
	
	
}
