package com.wachoo.pangker.api;

import java.util.HashMap;
import java.util.List;

import android.util.Log;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.ILocalContactsDao;
import com.wachoo.pangker.db.impl.LocalContactsDaoImpl;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.util.Util;

/**
 * 加载本地通讯录线程
 * 
 * @author wangxin
 * 
 */
public class loadMatchLocalAddressBookThread extends Thread {
	private HashMap<String, Integer> alphaIndexer = new HashMap<String, Integer>();// 存放存在的汉语拼音首字母和与之对应的列表位置
	private String[] sections;
	private PangkerApplication application;
	private ILocalContactsDao localContactsDao;

	public loadMatchLocalAddressBookThread(PangkerApplication application) {
		super();
		this.application = application;

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Log.i("LocalContacts", "LocalContacts>>StartInit");
		localContactsDao = new LocalContactsDaoImpl(application);
		List<LocalContacts> localContacts = localContactsDao
				.initLocalAddressBooks();
		initSectionStr(localContacts);
		application.setAlphaIndexer(alphaIndexer);
		application.setSections(sections);
		application.setLocalContacts(localContacts);
		application.setLocalContactsInit(true);
		new Thread() {
			public void run() {
				application.setLocalContacts(localContactsDao
						.matchRegisterUsers(application.getLocalContacts()));
			};
		}.start();
		Log.i("LocalContacts", "LocalContacts>>initOK!");
	}

	public void initSectionStr(List<LocalContacts> list) {
		int size = list.size();
		sections = new String[size];
		// 存放存在的汉语拼音首字母sections = new String[size];
		for (int i = 0; i < list.size(); i++) {
			// 当前汉语拼音首字母
			String currentStr = Util.getAlpha(list.get(i).getSortkey());
			// 上一个汉语拼音首字母，如果不存在为" "
			String previewStr = (i - 1) >= 0 ? Util.getAlpha(list.get(i - 1)
					.getSortkey()) : " ";
			if (!previewStr.equals(currentStr)) {
				String name = Util.getAlpha(list.get(i).getSortkey());
				alphaIndexer.put(name, i);
				sections[i] = name;
			}
		}
	}
}