package com.wachoo.pangker.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ContactUserListenerManager {
	protected List<IContactUserChangedListener> listenerList = new ArrayList<IContactUserChangedListener>();
	/**
	 * 添加一个Listener监听器
	 */
	public synchronized void addUserListenter(IContactUserChangedListener listener) {
		listenerList.add(listener);
	}
	
	public void exitManager() {
		// TODO Auto-generated method stub
		if(listenerList != null){
			listenerList.clear();
		}
	}

	/**
	 * 移除一个已注册的Listener监听器. 如果监听器列表中已有相同的监听器listener1、listener2,
	 * 并且listener1==listener2, 那么只移除最近注册的一个监听器。
	 */
	public synchronized void removeUserListenter(IContactUserChangedListener listener) {
		listenerList.remove(listener);
	}

	/**
	 * 启动监听
	 */
	public void userInfoChanged(ContactUserEvent event) {
		IContactUserChangedListener listener;
		Iterator<IContactUserChangedListener> it = listenerList.iterator();// 这步要注意同步问题
		while (it.hasNext()) {
			listener = (IContactUserChangedListener) it.next();
			listener.getClass();
			// 根据下载文件的地址
			if (listener != null)
				listener.isHaveUser(event.getItem());
		}
	}

	/**
	 * 启动监听
	 */
	public void refreshUI(String className, int what) {
		IContactUserChangedListener listener;
		Iterator<IContactUserChangedListener> it = listenerList.iterator();// 这步要注意同步问题
		while (it.hasNext()) {
			listener = (IContactUserChangedListener) it.next();
			if (listener != null && className.equals(listener.getClass().getName()))
				listener.refreshUser(what);
		}
	}
}
