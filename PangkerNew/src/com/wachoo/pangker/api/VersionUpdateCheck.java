package com.wachoo.pangker.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.server.HttpReqCode;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.VersionResult;
import com.wachoo.pangker.ui.dialog.ConfimNoticeDialog;
import com.wachoo.pangker.util.Util;

public class VersionUpdateCheck implements IUICallBackInterface {

	private static final String TAG = "VersionUpdateCheck";

	private Context mContext;
	private String versionCode;// >>>>>>>版本CODE
	private String versionName;// >>>>>>>版本名称
	private ServerSupportManager mSupportManager;// >>>>>>后台接口访问管理类
	private VersionCheckType mCheckType;// >>>>>>>检查版本方式
	private String mDownloadURL = Configuration.getDownloadPangkerURL();// >>>>>>>新版本下载地址

	/**
	 * 构造方法
	 * 
	 * @param mContext
	 *            Context
	 * @param mCheckType
	 *            检查方式
	 */
	public VersionUpdateCheck(Context mContext, VersionCheckType mCheckType) {
		this.mContext = mContext;
		this.mCheckType = mCheckType;
		this.mSupportManager = new ServerSupportManager(this.mContext, this);

	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		Log.i(TAG, "versionUpdateCheck>>result=" + supportResponse);
		if (!HttpResponseStatus(supportResponse)) {
			showToast("检查版本失败!");
			return;
		}
		try {
			// >>>>>>>>>>检查结果转换
			VersionResult bookResult = JSONUtil.fromJson(supportResponse.toString(), VersionResult.class);
			// >>>>>>>判断返回结果是否正确
			if (bookResult != null && bookResult.SUCCESS == bookResult.getErrorCode()) {
				// >>>>>>>判断当前安装的版本号是否是最新的
				if (Double.parseDouble(bookResult.getVersioncode()) > Integer.parseInt(versionCode)) {
					// 提示用户有新版本出现，提供按钮 1、更新 2、取消
					final ConfimNoticeDialog noticeDialog = new ConfimNoticeDialog(this.mContext);

					noticeDialog.setDialogTitle("版本更新提示");
					noticeDialog.setTitle("有新版本，点击更新下载");
					noticeDialog.setContent("最新版本V2.0,追加了很多新功能，点击确定下载!");
					noticeDialog.setConfimOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							// contactsBind();
							noticeDialog.dismiss();
							new DownloadLastVersionAsyncTask().execute();
						}
					});
					noticeDialog.setCancelOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							// contactsUnbind();
							noticeDialog.dismiss();
						}
					});
					noticeDialog.show();
				}
				// >>>>>>>>当前版本号与服务器一直，是最新的case
				else {
					// >>>>>判断是自动检查 or 手动检查
					if (mCheckType == VersionCheckType.MANUAL_CONTROL)
						showToast("已经是最新版本，无需更新!");
				}
			}
			// >>>>>> 检查失败case
			else {
				if (mCheckType == VersionCheckType.MANUAL_CONTROL)
					showToast("检查版本失败!");
			}
		} catch (Exception e) {
			// TODO: handle exception
			if (mCheckType == VersionCheckType.MANUAL_CONTROL)
				showToast("检查版本失败!");
		}
	}

	private void showToast(String text) {
		Toast.makeText(this.mContext, text, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 判断是否成功
	 * 
	 * @param supportResponse
	 * @return
	 */
	public boolean HttpResponseStatus(Object supportResponse) {
		if (supportResponse == null) {
			return false;
		}
		if (supportResponse instanceof HttpReqCode) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 检查版本更新
	 * 
	 * @throws Exception
	 */
	public void versionUpdateCheck(boolean pdShow) {

		Log.i(TAG, "versionUpdateCheck");
		try {

			VersionResult versionPanker = Util.getVersion(this.mContext);
			versionCode = versionPanker.getVersioncode();// >>>>>>>>pk版本CODE
			versionName = versionPanker.getIllustrate();
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", "0"));
			paras.add(new Parameter("versioncode", versionCode));
			paras.add(new Parameter("versionname", versionName));
			mSupportManager.supportExpRequest(Configuration.getCheckVsionCode(), paras, pdShow, "请稍后...", 0);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// >>>>>>>>检查更新的动作方式
	public enum VersionCheckType {
		AUTO_CONTROL, MANUAL_CONTROL

	}

	private class DownloadLastVersionAsyncTask extends AsyncTask<Void, Integer, Boolean> {
		// >>>>>>>>下载文件
		File file = null;
		// >>>>>>>网络URL
		URL url = null;
		// >>>>>>>>本地保存地址
		String savePath = null;
		InputStream inputStream = null;
		FileOutputStream outputStream = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			showToast("已经开始后台下载,下载后会提示您安装!");
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				savePath = PangkerConstant.DIR_VERSION;
				url = new URL(mDownloadURL);
				// >>>>>>>删除文件
				file = new File(savePath, "pangker.apk");
				if (file.exists()) {
					file.delete();
				} else {
					// >>>>>>>>创建目录
					File path = new File(savePath);
					if (!path.exists()) {
						path.mkdirs();
					}
				}

				// >>>>>>>>下载
				URLConnection cn = url.openConnection();
				cn.connect();

				inputStream = cn.getInputStream();
				// >>>>>>>网络音乐地址不正确，无法获取流
				if (inputStream == null) {
					// Log.d(TAG, "Unable to create InputStream for mediaUrl:" +
					// mediaUrl);
				}

				// >>>>>>>>>解析文件流
				outputStream = new FileOutputStream(file);
				// >>>>>>每次写入字节流
				byte buf[] = new byte[16384];
				// >>>>>>>已经下载字节
				int len;

				// >>>>>>下载
				while ((len = inputStream.read(buf)) != -1) {
					outputStream.write(buf, 0, len);
				}
				inputStream.close();
				return true;
			} catch (IOException e) {
				// TODO: handle exception
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			// >>>>>>>>进行安装
			if (result)
				openFile(file);
			else
				showToast("非常抱歉，新版本下载失败!");
		}

		private void openFile(File file) {
			// TODO Auto-generated method stub
			Log.e("OpenFile", file.getName());
			Intent intent = new Intent();
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setAction(android.content.Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
			mContext.startActivity(intent);
		}

	}

}
