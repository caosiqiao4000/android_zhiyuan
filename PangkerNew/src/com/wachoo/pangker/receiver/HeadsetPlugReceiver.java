package com.wachoo.pangker.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class HeadsetPlugReceiver extends BroadcastReceiver{
	
	private OnHeadsetListener onHeadsetListener;
	
	public void registerHeadsetPlugReceiver(Activity activity, OnHeadsetListener onHeadsetListener) {
		// TODO Auto-generated method stub
		this.onHeadsetListener = onHeadsetListener;
		IntentFilter intentFilter = new IntentFilter(); 
        intentFilter.addAction("android.intent.action.HEADSET_PLUG"); 
        activity.registerReceiver(this, intentFilter);
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.hasExtra("state")) {
			if (intent.getIntExtra("state", 0) == 0) {
				if(onHeadsetListener != null){
					onHeadsetListener.onHeadsetConnectListener(false);
				}
			} else if (intent.getIntExtra("state", 0) == 1) {
				if(onHeadsetListener != null){
					onHeadsetListener.onHeadsetConnectListener(true);
				}
			}
		} else {
			onHeadsetListener.onHeadsetConnectListener(false);
		}
	}
	
	public interface OnHeadsetListener{
		public void onHeadsetConnectListener(boolean connect);
	}
	
}
