package com.wachoo.pangker.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;

public class StorageEventReceiver extends BroadcastReceiver {
	private StorageEventListener mStorageEventListener;

	public StorageEventReceiver(StorageEventListener mStorageEventListener) {
		super();
		this.mStorageEventListener = mStorageEventListener;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		// 获取sdcard卡状态
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			if (this.mStorageEventListener != null)
				this.mStorageEventListener.onStorageStateChanged(true, true);
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			if (this.mStorageEventListener != null)
				this.mStorageEventListener.onStorageStateChanged(true, false);
		} else {
			if (this.mStorageEventListener != null)
				this.mStorageEventListener.onStorageStateChanged(false, false);
		}
	}

	/**
	 * 监听
	 * 
	 * @author wangxin
	 * 
	 */
	public interface StorageEventListener {
		void onStorageStateChanged(boolean isAvailable, boolean writeAble);
	}

}
