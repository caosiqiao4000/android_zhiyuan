package com.wachoo.pangker.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import com.wachoo.pangker.util.ConnectivityHelper;

public class NetChangeReceiver extends BroadcastReceiver {

	private OnNetChangeListener onNetChangeListener;
	
	private int netStatus = -2;
	private Context context;
	private long registerTime = 0l;
	private static final long WAIT_TIME = 2000;

	public void registerNetChangeReceiver(Context context, OnNetChangeListener onNetChangeListener) {
		// TODO Auto-generated method stub
		this.context = context;
		IntentFilter mFilter = new IntentFilter();
		mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		context.registerReceiver(this, mFilter);
		this.onNetChangeListener = onNetChangeListener;
		this.registerTime = System.currentTimeMillis();
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		String action = intent.getAction();
		if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
			int status = ConnectivityHelper.getAPNType(context);
			if (netStatus != -2) {
				if (netStatus != status) {
					if (onNetChangeListener != null) {
						boolean hasNet = ConnectivityHelper
								.isConnectivityAvailable(this.context);
						if (hasNet) {
							if (System.currentTimeMillis() - registerTime > WAIT_TIME) {
								onNetChangeListener.onNetStatus(true);
							}
						} else
							onNetChangeListener.onNetStatus(false);
					}
				}
			}
			netStatus = status;
		}
	}

	/**
	 * @author zxx
	 * 
	 * 
	 */
	public interface OnNetChangeListener {
		public void onNetStatus(boolean connect);
	}
}
