package com.wachoo.pangker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Properties;

import mobile.encrypt.Des3;

import com.wachoo.pangker.util.Util;

/**
 * pangker Properties class
 * 
 * @author wangxin
 * 
 */
public class Configuration {

	private static Properties pangkerProperties;

	static final String SERVER = "server";
	static final String SCHEME = "scheme";
	static final String HOME = "home";
	static final String DEVICENO = "deviceno";

	/**
	 * 驴博士交互
	 */
	static final String LBS_ORG_POI_URL = "LBS_ORG_POI_URL";
	static final String LBS_ORG_GEO_URL = "LBS_ORG_GEO_URL";

	/**
	 * 私信对话
	 */

	static final String CHAT_SERVER = "119.147.1.212";
	// static final String CHAT_SERVER = "192.168.3.88";
	// static final String CHAT_SERVER = "192.168.3.109";

	static final int CHAT_PORT = 5222;
	static final String CHAT_DOMAIN = "pangker";

	static final String INDEX_SERVER = "119.147.1.212:8080";
	// static final String INDEX_SERVER = "192.168.3.109:8080";

	static final String PANGKER_SERVER = "119.147.1.212";

	/**
	 * P2P群组标识
	 */
	public final static int CMANAGER2SP_LOGIN_REQ_FLAG = 0x101;// 登陆
	public final static int CMANAGER2SP_LOGINOUT_REQ_FLAG = 0x102;// 登出
	public final static int CROOM2SP_STATUS_REPORT_MEMBER_ADD_FLAG = 0x103;// 加入群组
	public final static int CROOM2SP_STATUS_REPORT_MEMBER_DEL_FLAG = 0x104;// 退出群组
	public final static int CROOM2SP_STATUS_REPORT_MEMBER_UPD_FLAG = 0x1040;// 修改个人信息
	public final static int CROOM2SP_STATUS_REPORT_LOCAYION_ADD_FLAG = 0x1041;// 修改群组地图信息
	public final static int CROOM2SP_STATUS_REPORT_LOCAYION_DEL_FLAG = 0x1042;// 离开群组地图信息
	public final static int CMANAGER2SP_HELLO_FLAG = 0x105;// 心跳包
	public final static int CROOM2SP_SPEECHLIST_REQ_FLAG = 0x106;// 麦序
	public final static int CROOM2SP_SPEECH_REQ_FLAG_UP = 0x107;// 上麦
	public final static int CROOM2SP_SPEECH_REQ_FLAG_DOWN = 0x108;// 下麦
	public final static int CP2SP_STREAM_TEXT_FLAG = 0x109;// 发文字包
	public final static int CP2SP_STREAM_STOP_FLAG = 0x110;// 停止发言
	public final static int CP2SP_STREAM_AMR_FLAG = 0x111;// 发手机语音包
	public final static int START_AUDIO_FLAG = 0x113;// 开始发言
	public final static int STOP_AUDIO_FLAG = 0x114;// 停止发言
	public final static int CROOM2SP_USERLIST_REQ = 0x115;// 获取成员列表
	public final static int CP2SP_STREAM_LOCATION_FLAG = 0x116;// 发地址消息包
	public final static int CLIENT2SP_NOTREAD_REQ = 0x117;// 群组离线消息
	public final static int FILE2SP_UPLOAD_REQ = 0x118;// 图片上传
	public final static int CP2SP_STREAM_PIC_FLAG = 0x119;// 发图片包

	public final static int CROOM2SP_STATUS_RADIO_STOP_FLAG = 0x203;// 群组内音乐播放，暂停语音功能
	public final static int CROOM2SP_STATUS_RADIO_PLAY_FLAG = 0x204;// 群组内音乐停止，开启语音功能0x0909
	public final static int CROOM2SP_STATUS_RADIO_ACCEPT_PLAY_FLAG = 0x205;// 群组停止接受语音
	public final static int CROOM2SP_STATUS_RADIO_ACCEPT_STOP_FLAG = 0x206;// 群组开启接受语音

	public final static int CROOM2SP_CONF_FLAG = 0x0909;// 群组设置

	/**
	 * 广播
	 */
	// 漂移在我周边的用户 广播
	public final static String DRIFTUSER_ACTION = "com.wachoo.pangker.activity.driftUserBroadcast";
	/**
	 * 后台交互接口地址
	 */
	static final String LoginUrl = "Login.json";
	static final String RegisterUrl = "Register.json";
	static final String SignInUrl = "SignIn.json";
	static final String SignInCountUrl = "QuerySignTimes.json";
	static final String AddPoiCommentUrl = "AddPoiComment.json";
	static final String QueryPoiCommentUrl = "QueryPoiComment.json";
	static final String AddPoiCommentReplyUrl = "AddPoiReply.json";
	static final String QueryPoiCommentReplyUrl = "QueryPoiReply.json";
	static final String QueryPoiMultiMediaUrl = "AddMultimedia.json";
	static final String AddPoiMultiMediaUrl = "QueryMultimedia.json";
	static final String SearchContactInfo = "SearchContactInfo.json";
	static final String GroupsNotify = "GroupsNotify.json";
	static final String SearchFriend = "SearchFriends.json";
	static final String SearchFans = "SearchFans.json";
	static final String SearchAttents = "SearchAttents.json";
	static final String GroupManager = "GroupManager.json";
	static final String AttentManager = "AttentManager.json";
	static final String ConfirmFriend = "ConfirmFriend.json";
	static final String FriendManager = "FriendManager.json";
	static final String GroupRelationManager = "GroupRelationManager.json";
	static final String UserinfoQuery = "UserinfoQuery.json";
	static final String UserBasicInfoQuery = "UserBasicInfoQuery.json";
	static final String FriendStatusManager = "FriendStatusManager.json";
	static final String SearchAsk = "SearchAsk.json";
	static final String SubmitAsk = "SubmitAsk.json";
	static final String SearchAskAnswer = "SearchAskAnswer.json";
	static final String ReplyAsk = "ReplyAsk.json";
	static final String CloseAsk = "CloseAsk.json";
	static final String ConfirmAnswer = "ConfirmAnswer.json";
	static final String IdentityQuery = "IdentityQuery.json";
	static final String FriendInfoRefresh = "FriendInfoRefresh.json";
	// static final String FriendInfoSearch = "FriendInfoSearch.json";
	static final String UploadCheck = "UploadCheck.json";
	static final String ResMusicUpload = "UpMusic";
	static final String ResDocUpload = "UpDoc";
	static final String ResPicUpload = "UpPic";
	static final String CollectionFriendUser = "CollectionFriendUser.json";
	static final String SearchFriendUserAttentions = "SearchFriendUserAttentions.json";
	static final String iocnurl = "GetHead?uid=";
	static final String SearchPicture = "/lbsindex/picsearch";
	static final String SearchOtherAsk = "/lbsindex/qasearch";
	static final String UserTrace = "/lbsindex/usertrace";
	static final String UserinfoRefresh = "UserinfoRefresh.json";
	static final String ResQueryByType = "ResQueryByType.json";
	static final String QueryGroups = "QueryGroups.json";
	static final String QueryVisitedGroups = "QueryVisitedGroups.json";
	static final String ResDownload = "GetRes?rid=";
	static final String GetImage = "GetImage?rid=";
	static final String CollectResManager = "CollectResManager.json";
	static final String PangkerGroupSpIpInfo = "Client2UMC.json";
	static final String PangkerGroupManager = "PangkerGroupManager.json";
	static final String JoinGroup = "JoinGroup.json";
	static final String uploadHead = "UpHead";
	static final String CallMeSet = "CallMeSet.json";
	static final String DriftSet = "DriftSet.json";
	static final String FollowSet = "FollowSet.json";
	static final String GroupEnterSet = "GroupEnterSet.json";
	static final String LocationQuerySet = "LocationQuerySet.json";
	static final String UserInfoQuerySet = "UserInfoQuerySet.json";
	static final String ResDirManager = "ResDirManager.json";
	static final String ResSiteQuery = "ResSiteQuery.json";
	static final String UserSearch = "/lbsindex/usersearch";
	static final String AppraiseRes = "AppraiseRes.json";
	static final String ResPropertySetting = "ResPropertySetting.json";
	static final String DeleteRes = "DeleteRes.json";
	static final String MoveResToDir = "MoveResToDir.json";
	static final String ResInfoQueryByID = "ResInfoQueryByID.json";
	static final String ResShare = "ResShare.json";
	static final String CoverDownload = "GetResCover?rid=";
	static final String AppendAsk = "AppendAsk.json";
	static final String SearchLbsApp = "/lbsindex/appsearch";
	static final String DeleAskReply = "DeleAskReply.json";
	static final String searchMusic = "/lbsindex/musicsearch";
	static final String searchDoc = "/lbsindex/docsearch";
	static final String QueryCollectRes = "QueryCollectRes.json";
	static final String UpApp = "UpApp";
	static final String SearchAskByAskId = "SearchAskByAskId.json";
	static final String AddComment = "AddComment.json";
	static final String DeleteComment = "DeleteComment.json";
	static final String SearchComments = "SearchComments.json";
	static final String QueryGroupsByID = "QueryGroupsByID.json";
	static final String GroupEnterCheck = "GroupEnterCheck.json";
	static final String CancelShare = "CancelShare.json";
	static final String ResQueryByDir = "ResQueryByDir.json";
	static final String ResDirQueryByDir = "ResDirQueryByDir.json";
	static final String DirQueryByType = "DirQueryByType.json";
	static final String QueryShare = "QueryShare.json";
	static final String QueryGroupMember = "QueryGroupMember.json";
	static final String InviteRegister = "InviteRegister.json";
	static final String downloadHead = "GetHead?uid=";
	static final String UserinfoQueryByIMSI = "UserinfoQueryByIMSI.json";
	static final String searchGroupFromLbs = "/lbsindex/groupsearch";
	static final String SettingRemarkName = "SettingRemarkName.json";
	static final String CallCheck = "CallMeCheck.json";
	static final String LocationQueryCheck = "LocationQueryCheck.json";
	static final String SearchNearFriends = "SearchNearFriends.json";
	static final String DriftLocationQuery = "DriftLocationQuery.json";
	static final String DriftCheck = "DriftCheck.json";
	static final String DriftUserQuery = "DriftUserQuery.json";
	static final String CancelDrift = "CancelDrift.json";
	static final String UpLocation = "UpLocation.json";
	static final String GetRegisterCode = "GetRegisterCode.json";
	static final String Register = "Register.json";
	static final String UserinfoByMobile = "GetUserinfo.json";
	static final String BlackManager = "BlackManager.json";
	static final String CLotluck = "CLotluck.json";
	static final String GarnerRes = "GarnerRes.json";
	static final String ResEventCountQuery = "ResEventCountQuery.json";
	static final String ResEventInfoQuery = "ResEventInfoQuery.json";
	static final String ResEventInfoLastQuery = "ResEventInfoLastQuery.json";
	static final String ResInfoQuerySet = "ResInfoQuerySet.json";
	static final String UploadGroupCover = "UploadGroupCover";
	static final String getGroupCover = "GetGroupCover?gid=";
	static final String AddressBookManager = "AddressBookManager.json";
	static final String AddressBookQueryByAccount = "AddressBookQueryByAccount.json";
	static final String QueryListLocation = "QueryListLocation.json";
	static final String AddressBookQuery = "AddressBookQuery.json";
	static final String AddressBookMembers = "AddressBookMembers.json";
	static final String AddressBookModify = "AddressBookModify.json";
	static final String AddressBookDeleMember = "AddressBookDeleMember.json";
	static final String AddressBookInOut = "AddressBookInOut.json";
	static final String AddressBookAddMember = "AddressBookAddMember.json";
	static final String AddressAddMemberCheck = "AddressAddMemberCheck.json";
	static final String AddressBookBinding = "AddressBookBinding.json";
	static final String AddressBookUNBinding = "AddressBookUNBinding.json";
	static final String BlackToAttent = "BlackToAttent.json";
	static final String GrantNotify = "GrantNotify.json";
	static final String LoginValidate = "LoginValidate.json";
	static final String RelativeManager = "RelativeManager.json";
	static final String SearchRelative = "SearchRelative.json";
	static final String CheckUser = "CheckUser.json";
	static final String RecommendRes = "RecommendRes.json";
	static final String InviteRelative = "InviteRelative.json";
	static final String UserSignRefresh = "UserSignRefresh.json";
	static final String UserinfoModify = "UserinfoModify.json";
	static final String UpOfflineFile = "UpOfflineFile";
	static final String GetFile = "GetFile?name=";
	static final String Setsecret = "Setsecret.json";
	static final String ResetPhoneApply = "ResetPhoneApply.json";
	static final String CheckSecret = "CheckSecret.json";
	static final String CountQuery = "CountQuery.json";
	static final String SearchUnderUsers = "SearchUnderUsers.json";
	static final String SetIMAccount = "SetIMAccount.json";
	static final String LoginByIMAccount = "LoginByIMAccount.json";
	static final String UpdateIMAccountPassword = "UpdateIMAccountPassword.json";
	static final String ContactsResQuery = "ContactsResQuery.json";
	static final String GetSystime = "GetSystime.json";
	static final String ResRankQuery = "ResRankQuery.json";
	static final String ENTER_PASSWORD = "views/enterPassword.do";
	static final String ENTER_ANSWER = "views/enterAnswer.do";
	static final String SetCallRedirect = "SetCallRedirect.json";
	static final String CrashException = "CrashException.json";
	static final String GroupInfoQuery = "GroupInfoQuery.json";
	static final String CheckVsionCode = "CheckVsionCode.json";
	static final String ResInfoUpdate = "ResInfoUpdate.json";
	static final String QueryRelationCount = "QueryRelationCount.json";
	static final String QueryRecentVisitor = "QueryRecentVisitor.json";
	static final String AddResGroupShare = "AddResGroupShare.json";
	static final String QueryResGroupShare = "QueryResGroupShare.json";
	static final String DeleteResGroupShare = "DeleteResGroupShare.json";
	static final String AddBoxRes = "AddBoxRes.json";
	static final String QueryBoxRes = "QueryBoxRes.json";
	static final String DeleteBoxRes = "DeleteBoxRes.json";
	static final String SysReport = "SysReport.json";
	static final String GroupSearch = "GroupSearch.json";
	static final String SearchRelativesRes = "SearchRelativesRes.json";
	static final String SearchUserByWebSite = "SearchUserByWebSite.json";
	static final String UserWebsiteNameModify = "SetUserWebSite.json";
	static final String ResMainShare = "PublishTalk.json";// 主界面说说功能接口
	static final String SearchTalk = "SearchTalk.json";
	static final String DeleteTalk = "DeleteTalk.json";
	static final String SearchTalkById = "SearchTalkById.json";
	static final String SetInvitation = "SetInvitation.json";

	static final String DownloadPangker = "Pangker.apk";
	/**
	 * 加密defaultIV
	 */
	private static byte[] defaultIV = { 1, 2, 3, 4, 5, 6, 7, 8 };
	static String baseUrl;

	/**
	 * 
	 * @return
	 */
	public static String getBaseUrl() {
		// if (("".equals(baseUrl)) || baseUrl == null)
		// baseUrl = getBaseHomeURL();
		return "http://119.147.1.212/pangker/";
	}

	public static String getGroupSpBaseUrl() {
		// if (("".equals(baseUrl)) || baseUrl == null)
		// baseUrl = getBaseHomeURL();
		return "http://119.147.1.212/umc/";
	}

	static {
		init();
	}

	/**
	 * 初始化 pangkerProoerties
	 */
	private static void init() {
		// 添加基本属性，访问地址等
		pangkerProperties = new Properties();
		pangkerProperties.setProperty(SCHEME, "http://");
		// pangkerProperties.setProperty(SERVER, "121.32.89.215:8088");
		pangkerProperties.setProperty(SERVER, PANGKER_SERVER);

		pangkerProperties.setProperty(HOME, "/pangker/");

		/**
		 * 加密相关
		 */
		// 加密key
		pangkerProperties.setProperty("encryptkey", "6DC7BC0758C8E310015E922C0DA87F9DA76B8040A8AB67F8");
		// 分割字符串
		pangkerProperties.setProperty("separator", "$");
		// device no
		pangkerProperties.setProperty(DEVICENO, "3500000000000001");
		// lbs org server url
		pangkerProperties.setProperty(LBS_ORG_POI_URL, "http://lbs.org.cn/api/lbs/poi.php");
		pangkerProperties.setProperty("POI_SEARCH_KEY", "f6b5d78c0a175ec2c0c9dfb99be5a53f");
		pangkerProperties.setProperty("POI_SEARCH_VER", "1.0");
		pangkerProperties.setProperty(LBS_ORG_GEO_URL, "http://lbs.org.cn/api/lbs/geo.php");
		/**
		 * 私信对话
		 */
		pangkerProperties.setProperty("chat_server", CHAT_SERVER);
		pangkerProperties.setProperty("chat_domain", CHAT_DOMAIN);
		pangkerProperties.setProperty("chat_truststorePath", "/system/etc/security/cacerts.bks");

	}

	public static String getChatTruststorePath() {
		return getProperty("chat_truststorePath");
	}

	public static String getChatServer() {
		return getProperty("chat_server");
	}

	public static String getChatDomain() {
		return getProperty("chat_domain");
	}

	public static int getChatPort() {
		return CHAT_PORT;
	}

	/**
	 * POI_SEARCH_KEY
	 * 
	 * @return
	 */
	public static String getPoiSearchKey() {
		return getProperty("POI_SEARCH_KEY");
	}

	/**
	 * POI_SEARCH_VER
	 * 
	 * @return
	 */
	public static String getPoiSearchVer() {
		return getProperty("POI_SEARCH_VER");
	}

	/**
	 * 分割字符串
	 * 
	 * @return
	 */
	public static String getSeparator() {
		return getProperty("separator");
	}

	/**
	 * 取得enctypt 的key
	 * 
	 * @return
	 */
	public static String getEncryptKey() {
		return getProperty("encryptkey");
	}

	/**
	 * 取得authenticator
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String getAuthenticator(String userid) throws Exception {
		String authenticator = getProperty(DEVICENO) + getSeparator() + userid + getSeparator() + Util.getSysNowTime();
		return Des3.Encrypt(authenticator, getEncryptKey(), getDefaultIV());
	}

	/**
	 * defaultIV
	 * 
	 * @return
	 */
	public static byte[] getDefaultIV() {
		return defaultIV;
	}

	/**
	 * 
	 * @return
	 */
	public static String getLoginURL() {
		return getBaseUrl() + LoginUrl;
	}

	public static String getRegisterurl() {
		return getBaseUrl() + RegisterUrl;
	}

	public static String getSignInurl() {
		return getBaseUrl() + SignInUrl;
	}

	public static String getAddCommenturl() {
		return getBaseUrl() + AddPoiCommentUrl;
	}

	public static String getDeleteCommenturl() {
		return getBaseUrl() + DeleteComment;
	}

	/**
	 * @http://
	 * @return http://
	 */
	public static String getScheme() {
		return getProperty("scheme");
	}

	/**
	 * server url
	 * 
	 * @return
	 */
	public static String getBaseHomeURL() {
		return getProperty(SCHEME) + getProperty(SERVER) + getProperty(HOME);
	}

	/**
	 * get pangker property deault value ""
	 * 
	 * @param name
	 * @return
	 */
	public static String getProperty(String name) {
		return getProperty(name, "");
	}

	/**
	 * get pangker property
	 * 
	 * @param name
	 * @param defaultValue
	 * @return
	 */
	public static String getProperty(String name, String defaultValue) {
		defaultValue = pangkerProperties.getProperty(name, defaultValue);
		return defaultValue;
	}

	/**
	 * 查询周边POI地址
	 * 
	 * @return
	 */
	public static String getOrgPoiSearchURL() {
		return getProperty(LBS_ORG_POI_URL);
	}

	/**
	 * Geo地址
	 * 
	 * @return
	 */
	public static String getOrgGeoSearchURL() {
		return getProperty(LBS_ORG_GEO_URL);
	}

	/**
	 * 签到次数
	 * 
	 * @return
	 */
	public static String getSignInCounturl() {
		// TODO Auto-generated method stub
		return getBaseUrl() + SignInCountUrl;
	}

	public static String getQueryCommenturl() {
		// TODO Auto-generated method stub
		return getBaseUrl() + QueryPoiCommentUrl;
	}

	public static String getAddCommentReplyurl() {
		// TODO Auto-generated method stub
		return getBaseUrl() + AddPoiCommentReplyUrl;
	}

	public static String getQueryCommentReplyurl() {
		// TODO Auto-generated method stub
		return getBaseUrl() + QueryPoiCommentReplyUrl;
	}

	public static String getAddMeltimedia() {
		// TODO Auto-generated method stub
		return getBaseUrl() + AddPoiMultiMediaUrl;
	}

	public static String getQueryMeltimedia() {
		// TODO Auto-generated method stub
		return getBaseUrl() + QueryPoiMultiMediaUrl;
	}

	public static String getSearchContactInfo() {
		// TODO Auto-generated method stub
		return getBaseUrl() + SearchContactInfo;
	}

	public static String getSearchFriend() {
		// TODO Auto-generated method stub
		return getBaseUrl() + SearchFriend;
	}

	public static String getSearchFans() {
		// TODO Auto-generated method stub
		return getBaseUrl() + SearchFans;
	}

	public static String getSearchAttents() {
		// TODO Auto-generated method stub
		return getBaseUrl() + SearchAttents;
	}

	public static String getGroupManager() {
		// TODO Auto-generated method stub
		return getBaseUrl() + GroupManager;
	}

	public static String getAttentManager() {
		// TODO Auto-generated method stub
		return getBaseUrl() + AttentManager;
	}

	public static String getConfirmFriend() {
		// TODO Auto-generated method stub
		return getBaseUrl() + ConfirmFriend;
	}

	public static String getIconUrl() {
		// TODO Auto-generated method stub
		return getBaseUrl() + iocnurl;
	}

	public static String getFriendManager() {
		// TODO Auto-generated method stub
		return getBaseUrl() + FriendManager;
	}

	public static String getGroupRelationManager() {
		// TODO Auto-generated method stub
		return getBaseUrl() + GroupRelationManager;
	}

	public static String getUserinfoQuery() {
		return getBaseUrl() + UserinfoQuery;
	}

	public static String getUserBasicInfoQuery() {
		return getBaseUrl() + UserBasicInfoQuery;
	}

	public static String getFriendStatusManager() {
		return getBaseUrl() + FriendStatusManager;
	}

	public static String getSearchAsk() {
		return getBaseUrl() + SearchAsk;
	}

	public static String getSubmitAsk() {
		return getBaseUrl() + SubmitAsk;
	}

	public static String getSearchAskAnswer() {
		return getBaseUrl() + SearchAskAnswer;
	}

	public static String getReplyAsk() {
		return getBaseUrl() + ReplyAsk;
	}

	public static String getCloseAsk() {
		return getBaseUrl() + CloseAsk;
	}

	public static String getConfirmAnswer() {
		return getBaseUrl() + ConfirmAnswer;
	}

	public static String getIdentityQuery() {
		return getBaseUrl() + IdentityQuery;
	}

	public static String getFriendInfoRefresh() {
		return getBaseUrl() + FriendInfoRefresh;
	}

	public static String getResMusicUpload() {
		return getBaseUrl() + ResMusicUpload;
	}

	public static String getResPicUpload() {
		return getBaseUrl() + ResPicUpload;
	}

	public static String getResDocUpload() {
		return getBaseUrl() + ResDocUpload;
	}

	public static String getCollectionFriendUser() {
		return getBaseUrl() + CollectionFriendUser;
	}

	public static String getSearchFriendUserAttentions() {
		return getBaseUrl() + SearchFriendUserAttentions;
	}

	/**
	 * 图片搜索引擎接口
	 * 
	 * @return String
	 */
	public static String getSearchPicture() {
		return getProperty(SCHEME) + INDEX_SERVER + SearchPicture;
	}

	/**
	 * 问题搜索引擎接口
	 * 
	 * @return String
	 */
	public static String getSearchOtherAsk() {
		return getProperty(SCHEME) + INDEX_SERVER + SearchOtherAsk;
	}

	public static String getUploadCheck() {
		return getBaseUrl() + UploadCheck;
	}

	public static String getUserinfoRefresh() {
		return getBaseUrl() + UserinfoRefresh;
	}

	public static String getResQueryByType() {
		return getBaseUrl() + ResQueryByType;
	}

	public static String getQueryGroups() {
		return getBaseUrl() + QueryGroups;
	}

	public static String getQueryVisitedGroups() {
		return getBaseUrl() + QueryVisitedGroups;
	}

	public static String getResDownload() {
		return getBaseUrl() + ResDownload;
	}

	public static String getImageUrl() {
		return getBaseUrl() + GetImage;
	}

	public static String getCollectResManager() {
		return getBaseUrl() + CollectResManager;
	}

	public static String getPangkerGroupSpIpInfo() {
		return getGroupSpBaseUrl() + PangkerGroupSpIpInfo;
	}

	public static String getPangkerGroupManager() {
		return getBaseUrl() + PangkerGroupManager;
	}

	public static String getUploadHead() {
		return getBaseUrl() + uploadHead;
	}

	public static String getDownLoadHead() {
		return getBaseUrl() + downloadHead;
	}

	public static String getJoinGroup() {
		return getBaseUrl() + JoinGroup;
	}

	public static String getCallMeSet() {
		return getBaseUrl() + CallMeSet;
	}

	public static String getDriftSet() {
		return getBaseUrl() + DriftSet;
	}

	public static String getFollowSet() {
		return getBaseUrl() + FollowSet;
	}

	public static String getGroupEnterSet() {
		return getBaseUrl() + GroupEnterSet;
	}

	public static String getLocationQuerySet() {
		return getBaseUrl() + LocationQuerySet;
	}

	public static String getUserInfoQuerySet() {
		return getBaseUrl() + UserInfoQuerySet;
	}

	/**
	 * 用户搜索引擎接口
	 * 
	 * @return String
	 */
	public static String getUserSearch() {
		return getProperty(SCHEME) + INDEX_SERVER + UserSearch;
	}

	public static String getResDirManager() {
		return getBaseUrl() + ResDirManager;
	}

	public static String getResSiteQuery() {
		return getBaseUrl() + ResSiteQuery;
	}

	public static String getAppraiseRes() {
		return getBaseUrl() + AppraiseRes;
	}

	public static String getResPropertySetting() {
		return getBaseUrl() + ResPropertySetting;
	}

	public static String getDeleteRes() {
		return getBaseUrl() + DeleteRes;
	}

	public static String getMoveResToDir() {
		return getBaseUrl() + MoveResToDir;
	}

	public static String getResInfoQueryByID() {
		return getBaseUrl() + ResInfoQueryByID;
	}

	public static String getSearchComments() {
		return getBaseUrl() + SearchComments;
	}

	public static String getCancelShare() {
		return getBaseUrl() + CancelShare;
	}

	public static String getRelativeManager() {
		return getBaseUrl() + RelativeManager;
	}

	public static String getSearchRelative() {
		return getBaseUrl() + SearchRelative;
	}

	public static String getUpOfflineFile() {
		return getBaseUrl() + UpOfflineFile;
	}

	/**
	 * 
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-24 下午02:34:31
	 */
	public static String getGroupsNotify() {
		return getBaseUrl() + GroupsNotify;
	}

	public static String getResShare() {
		return getBaseUrl() + ResShare;
	}

	public static String getCoverDownload() {
		return getBaseUrl() + CoverDownload;
	}

	public static String getAppendAsk() {
		return getBaseUrl() + AppendAsk;
	}

	/**
	 * 应用搜索引擎接口
	 * 
	 * @return String
	 */
	public static String getSearchLbsApp() {
		return getProperty(SCHEME) + INDEX_SERVER + SearchLbsApp;
	}

	public static String getDeleAskReply() {
		return getBaseUrl() + DeleAskReply;
	}

	public static String getSearchMusic() {
		return getProperty(SCHEME) + INDEX_SERVER + searchMusic;
	}

	public static String getSearchDoc() {
		return getProperty(SCHEME) + INDEX_SERVER + searchDoc;
	}

	public static String getQueryCollectRes() {
		return getBaseUrl() + QueryCollectRes;
	}

	public static String getUpApp() {
		return getBaseUrl() + UpApp;
	}

	public static String getSearchAskByAskId() {
		return getBaseUrl() + SearchAskByAskId;
	}

	public static String getAddComment() {
		return getBaseUrl() + AddComment;
	}

	public static String getQueryGroupsByID() {
		return getBaseUrl() + QueryGroupsByID;
	}

	public static String getGroupEnterCheck() {
		return getBaseUrl() + GroupEnterCheck;
	}

	public static String getQueryShare() {
		return getBaseUrl() + QueryShare;
	}

	public static String getResQueryByDir() {
		return getBaseUrl() + ResQueryByDir;
	}

	public static String getResDirQueryByDir() {
		return getBaseUrl() + ResDirQueryByDir;
	}

	public static String getDirQueryByType() {
		return getBaseUrl() + DirQueryByType;
	}

	public static String getQueryGroupMember() {
		return getBaseUrl() + QueryGroupMember;
	}

	public static String getInviteRegister() {
		return getBaseUrl() + InviteRegister;
	}

	/**
	 * 采集用户位置信息
	 * 
	 * @return
	 */
	public static String getUserTrace() {
		return getProperty(SCHEME) + INDEX_SERVER + UserTrace;
	}

	public static String getUserinfoQueryByIMSI() {
		return getBaseUrl() + UserinfoQueryByIMSI;
	}

	public static String getGroupFromLbs() {
		return getProperty(SCHEME) + INDEX_SERVER + searchGroupFromLbs;
	}

	public static String getSettingRemarkName() {
		return getBaseUrl() + SettingRemarkName;
	}

	public static String getCallCheck() {
		return getBaseUrl() + CallCheck;
	}

	public static String getLocationQueryCheck() {
		return getBaseUrl() + LocationQueryCheck;
	}

	public static String getSearchNearFriends() {
		return getBaseUrl() + SearchNearFriends;
	}

	public static String getDriftLocationQuery() {
		return getBaseUrl() + DriftLocationQuery;
	}

	public static String getDriftCheck() {
		return getBaseUrl() + DriftCheck;
	}

	public static String getDriftUserQuery() {
		return getBaseUrl() + DriftUserQuery;
	}

	public static String getCancelDrift() {
		return getBaseUrl() + CancelDrift;
	}

	public static String getUpLocation() {
		return getBaseUrl() + UpLocation;
	}

	public static String getGetRegisterCode() {
		return getBaseUrl() + GetRegisterCode;
	}

	public static String getRegister() {
		return getBaseUrl() + Register;
	}

	public static String getUserinfoByMobile() {
		return getBaseUrl() + UserinfoByMobile;
	}

	public static String getBlackManager() {
		return getBaseUrl() + BlackManager;
	}

	public static String getCLotluck() {
		return getBaseUrl() + CLotluck;
	}

	public static String getGarnerRes() {
		return getBaseUrl() + GarnerRes;
	}

	public static String getResEventCountQuery() {
		return getBaseUrl() + ResEventCountQuery;
	}

	public static String getResEventInfoQuery() {
		return getBaseUrl() + ResEventInfoQuery;
	}

	public static String getResInfoQuerySet() {
		return getBaseUrl() + ResInfoQuerySet;
	}

	public static String getUploadGroupCover() {
		return getBaseUrl() + UploadGroupCover;
	}

	public static String getGroupCover() {
		return getBaseUrl() + getGroupCover;
	}

	public static String getAddressBookManager() {
		return getBaseUrl() + AddressBookManager;
	}

	public static String getAddressBookQueryByAccount() {
		return getBaseUrl() + AddressBookQueryByAccount;
	}

	public static String getQueryListLocation() {
		return getBaseUrl() + QueryListLocation;
	}

	public static String getAddressBookQuery() {
		return getBaseUrl() + AddressBookQuery;
	}

	public static String getAddressBookMembers() {
		return getBaseUrl() + AddressBookMembers;
	}

	public static String getAddressBookModify() {
		return getBaseUrl() + AddressBookModify;
	}

	public static String getAddressBookDeleMember() {
		return getBaseUrl() + AddressBookDeleMember;
	}

	public static String getAddressBookInOut() {
		return getBaseUrl() + AddressBookInOut;
	}

	public static String getAddressBookAddMember() {
		return getBaseUrl() + AddressBookAddMember;
	}

	public static String getAddressAddMemberCheck() {
		return getBaseUrl() + AddressAddMemberCheck;
	}

	public static String getAddressBookBinding() {
		return getBaseUrl() + AddressBookBinding;
	}

	public static String getAddressBookUNBinding() {
		return getBaseUrl() + AddressBookUNBinding;
	}

	public static String getBlackToAttent() {
		return getBaseUrl() + BlackToAttent;
	}

	public static String getGrantNotify() {
		return getBaseUrl() + GrantNotify;
	}

	public static String getLoginValidate() {
		return getBaseUrl() + LoginValidate;
	}

	public static String getCheckUser() {
		return getBaseUrl() + CheckUser;
	}

	public static String getRecommendRes() {
		return getBaseUrl() + RecommendRes;
	}

	public static String getInviteRelative() {
		return getBaseUrl() + InviteRelative;
	}

	public static String getUserSignRefresh() {
		return getBaseUrl() + UserSignRefresh;
	}

	public static String getUserinfoModify() {
		return getBaseUrl() + UserinfoModify;
	}

	public static String getResEventInfoLastQuery() {
		return getBaseUrl() + ResEventInfoLastQuery;
	}

	public static String getGetFile() {
		return getBaseUrl() + GetFile;
	}

	public static String getSetsecret() {
		return getBaseUrl() + Setsecret;
	}

	public static String getResetPhoneApply() {
		return getBaseUrl() + ResetPhoneApply;
	}

	public static String getCheckSecret() {
		return getBaseUrl() + CheckSecret;
	}

	public static String getCountQuery() {
		return getBaseUrl() + CountQuery;
	}

	public static String getSearchUnderUsers() {
		// TODO Auto-generated method stub
		return getBaseUrl() + SearchUnderUsers;
	}

	public static String getSetIMAccount() {
		// TODO Auto-generated method stub
		return getBaseUrl() + SetIMAccount;
	}

	public static String getLoginByIMAccount() {
		// TODO Auto-generated method stub
		return getBaseUrl() + LoginByIMAccount;
	}

	public static String getUpdateIMAccountPassword() {
		// TODO Auto-generated method stub
		return getBaseUrl() + UpdateIMAccountPassword;
	}

	public static String getContactsResQuery() {
		// TODO Auto-generated method stub
		return getBaseUrl() + ContactsResQuery;
	}

	public static String getResRankQuery() {
		// TODO Auto-generated method stub
		return getBaseUrl() + ResRankQuery;
	}

	public static String getServerTimeNotify() {
		// TODO Auto-generated method stub
		return getBaseUrl() + GetSystime;
	}

	public static String getEnterPassword() {
		// TODO Auto-generated method stub
		return getBaseUrl() + ENTER_PASSWORD;
	}

	public static String getEnterAnswer() {
		// TODO Auto-generated method stub
		return getBaseUrl() + ENTER_ANSWER;
	}

	public static String getSetCallRedirect() {
		// TODO Auto-generated method stub
		return getBaseUrl() + SetCallRedirect;
	}

	public static String getCrashException() {
		// TODO Auto-generated method stub
		return getBaseUrl() + CrashException;
	}

	public static String getGroupInfoQuery() {
		// TODO Auto-generated method stub
		return getBaseUrl() + GroupInfoQuery;
	}

	public static String getCheckVsionCode() {
		// TODO Auto-generated method stub
		return getBaseUrl() + CheckVsionCode;
	}

	public static String getResInfoUpdate() {
		// TODO Auto-generated method stub
		return getBaseUrl() + ResInfoUpdate;
	}

	public static String getQueryRelationCount() {
		// TODO Auto-generated method stub
		return getBaseUrl() + QueryRelationCount;
	}

	public static String getQueryRecentVisitor() {
		// TODO Auto-generated method stub
		return getBaseUrl() + QueryRecentVisitor;
	}

	public static String getAddResGroupShare() {
		// TODO Auto-generated method stub
		return getBaseUrl() + AddResGroupShare;
	}

	public static String getQueryResGroupShare() {
		// TODO Auto-generated method stub
		return getBaseUrl() + QueryResGroupShare;
	}

	public static String getDeleteResGroupShare() {
		// TODO Auto-generated method stub
		return getBaseUrl() + DeleteResGroupShare;
	}

	public static String getAddBoxRes() {
		// TODO Auto-generated method stub
		return getBaseUrl() + AddBoxRes;
	}

	public static String getQueryBoxRes() {
		// TODO Auto-generated method stub
		return getBaseUrl() + QueryBoxRes;
	}

	public static String getDeleteBoxRes() {
		// TODO Auto-generated method stub
		return getBaseUrl() + DeleteBoxRes;
	}

	public static String getSysReport() {
		return getBaseUrl() + SysReport;
	}

	public static String getGroupSearch() {
		return getBaseUrl() + GroupSearch;
	}

	public static String getSearchRelativesRes() {
		return getBaseUrl() + SearchRelativesRes;
	}

	public static String getSearchUserByWebSite() {
		return getBaseUrl() + SearchUserByWebSite;
	}

	public static String getWebsiteNameModify() {
		return getBaseUrl() + UserWebsiteNameModify;
	}

	public static String getResMainShare() {
		return getBaseUrl() + ResMainShare;
	}

	public static String getSearchTalk() {
		return getBaseUrl() + SearchTalk;
	}

	public static String getDeleteTalk() {
		return getBaseUrl() + DeleteTalk;
	}

	public static String getSearchTalkById() {
		return getBaseUrl() + SearchTalkById;
	}

	public static String getSetInvitation() {
		return getBaseUrl() + SetInvitation;
	}

	public static String getDownloadPangkerURL() {
		return "http://119.147.1.212/" + DownloadPangker;
	}

	/**
	 * 把字符串写入文本中
	 * 
	 * @param fileName
	 *            生成的文件绝对路径
	 * @param content
	 *            文件要保存的内容
	 * @param enc
	 *            文件编码
	 * @return
	 */
	public static boolean writeStringToFile(String fileName, String content, String enc) {
		File file = new File(fileName);
		try {
			if (file.isFile()) {
				file.deleteOnExit();
				file = new File(file.getAbsolutePath());
			}
			OutputStreamWriter os = null;
			if (enc == null || enc.length() == 0) {
				os = new OutputStreamWriter(new FileOutputStream(file));
			} else {
				os = new OutputStreamWriter(new FileOutputStream(file), enc);
			}
			os.write(content);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
