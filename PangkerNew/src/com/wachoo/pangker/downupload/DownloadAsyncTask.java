package com.wachoo.pangker.downupload;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.RemoteViews;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.res.UpDownLoadManagerActivity;
import com.wachoo.pangker.db.IDownloadDao;
import com.wachoo.pangker.db.impl.DownloadDaoImpl;
import com.wachoo.pangker.entity.DownloadJob;

public class DownloadAsyncTask extends AsyncTask<Void, Integer, Boolean> {

	private final static String TAG = "DownloadAsyncTask";

	private int endPos; // >>>>结束点
	private int doneSize;// >>>>完成点
	private String fileUrl;// >>>>下载路径（网络地址）

	private int fileType; // >>>>>下载的文件类型
	private int state = DownloadJob.DOWNLOAD_INIT;

	private String fullFileName;
	private long lastnoticetime = 0; // >>>>最后一次下载通知时间
	private final static int noticeTime = 1000;// 通知进度时间间隔 ,每次通知间隔2000毫秒

	private DownloadJob mDownloadJob;
	private IDownloadDao mDownloadDao;
	private Context mContext;
	private DownLoadManager mDownLoadManager;

	private DownloadingNoticeView mDownloadingNoticeView;
	private PendingIntent pIntent = null;// 更新显示

	public DownloadAsyncTask(DownloadJob mDownloadJob, Context mContext) {
		super();
		this.mDownloadJob = mDownloadJob;
		this.endPos = mDownloadJob.getEndPos();

		// 修改获取已经下载字节数获取方式this.doneSize = mDownloadJob.getDoneSize();
		File file = new File(mDownloadJob.getAbsolutePath());
		if (file.exists()) {
			this.doneSize = (int) file.length();
		} else {
			this.doneSize = 0;
		}

		this.fileUrl = mDownloadJob.getUrl();
		this.fileType = mDownloadJob.getType();

		// >>>>>>>判断文件是否存在,不存在创建
		File savePath = new File(mDownloadJob.getSavePath());
		if (!savePath.exists())
			savePath.mkdirs();

		// >>>>>>>>fullName格式为 本地保存路径 + 文件名称 +“-”+ 资源id
		// >>>>>验证文件是否下载过：文件名+“-”+资源id，如果相同则为下载过，如果不相同则为未下载
		this.fullFileName = mDownloadJob.getSavePath() + "/" + mDownloadJob.getSaveFileName()
				+ DownloadJob.SUFFIX;

		this.state = mDownloadJob.getStatus();
		this.mContext = mContext;
		this.mDownloadDao = new DownloadDaoImpl(this.mContext);
		mDownloadingNoticeView = new DownloadingNoticeView(this.mContext);
		this.mDownLoadManager = ((PangkerApplication) mContext.getApplicationContext()).getDownLoadManager();
	}

	private boolean checkSd() {
		// TODO Auto-generated method stub
		return ((PangkerApplication) mContext.getApplicationContext()).ismExternalStorageAvailable();
	}

	private void downloadException() {
		// TODO Auto-generated method stub
		state = DownloadJob.DOWNLOAD_FAILE;
		mDownloadJob.setStatus(state);
		Log.d(TAG, "下载失败==>" + state);
		mDownloadDao.updataDownloadStatus(mDownloadJob.getId(), state);
		// >>>>>>>>>>下载失败通知，同时取消消息栏的通知
		mDownLoadManager.startListenterAction(this);
		mDownloadingNoticeView.cancelNotify();
	}

	private void downloadSuccess() {
		// TODO Auto-generated method stub
		this.state = DownloadJob.DOWNLOAD_SUCCESS;
		fileRename();
		Log.i(TAG, "下载完成==>" + state);
		mDownloadJob.setStatus(DownloadJob.DOWNLOAD_SUCCESS);
		mDownloadDao.updataDownloadInfo(mDownloadJob);
		mDownLoadManager.startListenterAction(this);
		mDownloadingNoticeView.notice(DownloadJob.DOWNLOAD_SUCCESS);
		mDownLoadManager.saveResAsLoacl(mDownloadJob);
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		HttpURLConnection connection = null;
		// FileOutputStream fos = null;
		RandomAccessFile randomAccessFile = null;
		InputStream is = null;
		BufferedInputStream bis = null;
		URL url = null;
		try {
			url = new URL(fileUrl);
		} catch (MalformedURLException e1) {
			Log.e(TAG, "==>MalformedURLException");
			return false;
		}

		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.setConnectTimeout(10 * 1000);
			connection.setRequestMethod("GET");
			if (fileType == PangkerConstant.RES_DOCUMENT) {
				connection.setRequestProperty("contentType", "gbk");
			}
			// 设置范围，格式为Range：bytes x-y;
			connection.setRequestProperty("Range", "bytes=" + doneSize + "-" + endPos);
			connection.connect();

			randomAccessFile = new RandomAccessFile(fullFileName, "rwd");
			randomAccessFile.seek(doneSize);

			// 将要下载的文件写到保存在保存路径下的文件中
			is = connection.getInputStream();
			bis = new BufferedInputStream(is);

			byte[] buffer = new byte[1024 * 2];
			int length = -1;

			lastnoticetime = System.currentTimeMillis();

			// int slen = connection.getContentLength();
			while ((length = bis.read(buffer)) != -1) {
				if (!checkSd()) {
					downloadException();
					return false;
				}
				doneSize += length;
				// 设置当前任务的完成量
				mDownloadJob.setDoneSize(doneSize);
				// 更新db(取消更新，严重影响下载效率)
				// mDownloadDao.updateDownloadDoneSize(mDownloadJob.getId(),
				// doneSize);

				if (fileType == PangkerConstant.RES_DOCUMENT) {
					String txtstring = new String(buffer, "gbk");
					randomAccessFile.seek(doneSize);
					randomAccessFile.write(txtstring.getBytes("gbk"));
				} else {
					randomAccessFile.write(buffer, 0, length);
				}

				// >>>>>>>>>>> edit by wangxin start 下载中通知
				if (doneSize == endPos || System.currentTimeMillis() - lastnoticetime >= noticeTime) {
					// 下载过程中
					lastnoticetime = System.currentTimeMillis();
					// >>>>>>>>>>通知更新
					mDownLoadManager.startListenterAction(this);
					mDownloadingNoticeView.notice(DownloadJob.DOWNLOADING);
				}

				// >>>>>>>>>>> 暂停下载
				if (state == DownloadJob.DOWNLOAD_PAUSE) {

					// >>>>>>>>>>通知更新
					mDownloadJob.setStatus(state);
					mDownloadDao.updataDownloadStatus(mDownloadJob.getId(), state);
					mDownLoadManager.startListenterAction(this);
					mDownloadingNoticeView.notice(DownloadJob.DOWNLOAD_PAUSE);

					Log.i(TAG, "PAUSE==>" + doneSize);
					// 使用线程锁锁定该线程
					synchronized (mDownloadDao) {
						try {
							mDownloadDao.wait();
						} catch (InterruptedException e) {
							return false;
						}
					}
				}

				//
				else if (state == DownloadJob.DOWNLOAD_CANCEL || state == DownloadJob.DOWNLOAD_STOP) {
					// >>>>>>>>>>通知更新
					mDownloadJob.setStatus(state);
					mDownloadDao.updataDownloadStatus(mDownloadJob.getId(), state);
					mDownLoadManager.startListenterAction(this);
					mDownloadingNoticeView.notice(state);
					Log.i(TAG, "取消下载==>" + state);
					break;
				}
			}
		} catch (Exception e) {
			// 发送下载失败消息
			downloadException();
		} finally {
			try {
				if (is != null) {
					is.close();
					is = null;
				}
				if (bis != null) {
					bis.close();
					bis = null;
				}
				randomAccessFile.close();
				connection.disconnect();
				Log.i(TAG, "下载完成:endPos==>" + endPos + "|||doneSize:" + doneSize);
				// >>>>>>>>>下载完成进度设置
				if (endPos == doneSize) {
					downloadSuccess();
				}
				Log.i(TAG, "==>state:" + state + "|||||END!");
			} catch (Exception e) {
				downloadException();
			}
		}
		return true;
	}

	// 将下载完全的文件更名，去掉.tp名同时将下载的资源保存到本地
	private void fileRename() {
		String nName = null;
		if (fullFileName.contains(DownloadJob.SUFFIX)) {
			nName = fullFileName.substring(0, fullFileName.lastIndexOf(DownloadJob.SUFFIX));
			File file = new File(fullFileName);
			file.renameTo(new File(nName));
			fullFileName = fullFileName.substring(0, fullFileName.lastIndexOf(DownloadJob.SUFFIX));

		}
	}

	// >>>>>>>>取消下载
	public void cancel() {
		this.state = DownloadJob.DOWNLOAD_CANCEL;
		File file = new File(fullFileName);
		if (file.exists()) {
			if (file.delete()) {
				Log.d("Downloader", "delete==>");
				mDownloadDao.delete(fileUrl);
			}
		}
	}

	public void stop() {
		Log.i(TAG, "DownloadAsyniTask>>>stop==>" + state);
		if (state == DownloadJob.DOWNLOAD_PAUSE) {
			this.state = DownloadJob.DOWNLOAD_STOP;
			// 恢复所有线程
			synchronized (mDownloadDao) {
				mDownloadDao.notifyAll();
			}
		} else {
			this.state = DownloadJob.DOWNLOAD_STOP;
		}
		mDownloadingNoticeView.notice(state);
		this.cancel(true);
	}

	// >>>>>>>>暂停下载
	public void pause() {
		this.state = DownloadJob.DOWNLOAD_PAUSE;
	}

	// >>>>>>>>继续下载
	public void resume() {
		// >>>>>>>>>需要判断是否
		state = DownloadJob.DOWNLOADING;
		// 恢复所有线程
		synchronized (mDownloadDao) {
			mDownloadDao.notifyAll();
		}
	}

	public int getEndPos() {
		return endPos;
	}

	public int getDoneSize() {
		return doneSize;
	}

	public int getState() {
		return state;
	}

	class DownloadingNoticeView {

		private int noticeTime = 4000;
		private long lastNoticeBarTime = 0l;
		private boolean isFirstNotice = true; // 是否是第一次通知，如果是第一次不做判断
		private Notification notification;
		private Context mContext;
		private RemoteViews mRemoteViews;
		private NotificationManager notificationManager = null;
		// Handler传输数据
		private Handler handler = new Handler() {

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				switch (msg.what) {
				case DownloadJob.DOWNLOAD_INIT:
					break;
				case DownloadJob.DOWNLOADING:
					refleshView();
					break;
				case DownloadJob.DOWNLOAD_PAUSE:
					pauseNotify();
					break;
				case DownloadJob.DOWNLOAD_SUCCESS:
				case DownloadJob.DOWNLOAD_STOP:
				case DownloadJob.DOWNLOAD_FAILE:// 下载失败
					cancelNotify();
					break;
				}
			}
		};

		// >>>>>>>>>>>>通知更新
		public void notice(int noticeType) {

			if (noticeType != DownloadJob.DOWNLOADING) {
				isFirstNotice = false;// 第一次通知结束
				Message message = new Message();
				message.what = noticeType;
				handler.handleMessage(message);
			}
			if (System.currentTimeMillis() - lastNoticeBarTime > noticeTime || isFirstNotice) {
				isFirstNotice = false;// 第一次通知结束
				lastNoticeBarTime = System.currentTimeMillis();
				Message message = new Message();
				message.what = noticeType;
				handler.handleMessage(message);
			}
			if (noticeType == DownloadJob.DOWNLOAD_STOP) {
				cancelNotify();
			}
		}

		// >>>>>>>>>>>>取消同步
		public void cancelNotify() {
			Log.i(TAG, "==>state:" + state + "cancelNotify!");
			notificationManager.cancel(mDownloadJob.getId());
		}

		// 下载的过程中异常 to
		private void pauseNotify() {
			// TODO Auto-generated method stub
			Log.i(TAG, "==>state:" + state + "pauseNotify!");
			mRemoteViews.setTextViewText(R.id.tv_Name, mDownloadJob.getName() + "(暂停)");
			notificationManager.notify(mDownloadJob.getId(), notification);
		}

		// >>>>>>>>>>>>初始化
		public DownloadingNoticeView(Context mContext) {
			this.mContext = mContext;
			mRemoteViews = new RemoteViews(mContext.getPackageName(), R.layout.notice_download);
			notificationManager = (NotificationManager) mContext
					.getSystemService(Context.NOTIFICATION_SERVICE);
			notification = new Notification();

			Intent intent = new Intent(mContext, UpDownLoadManagerActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			pIntent = PendingIntent.getActivity(mContext, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

			notification.contentView = mRemoteViews;
			notification.contentIntent = pIntent;
			notification.icon = R.drawable.icon32_d_and_upload;
			notification.flags |= Notification.FLAG_ONGOING_EVENT;
			lastNoticeBarTime = System.currentTimeMillis();
		}

		// 根据下载进度刷新UI
		private void refleshView() {
			// TODO Auto-generated method stub
			mRemoteViews.setProgressBar(R.id.downProgressBar, getEndPos(), doneSize, false);

			String donesize = Formatter.formatFileSize(mContext, doneSize);
			String size = Formatter.formatFileSize(mContext, getEndPos());
			String proString = donesize + "/" + size + "(" + (doneSize * 100 / mDownloadJob.getEndPos())
					+ "%)";
			mRemoteViews.setTextViewText(R.id.tv_progress, proString);
			mRemoteViews.setTextViewText(R.id.tv_Name, mDownloadJob.getName());
			notification.defaults = Notification.FLAG_AUTO_CANCEL;
			// >>>>>>点击清除后不能消失
			notification.flags = Notification.FLAG_ONGOING_EVENT;
			notificationManager.notify(mDownloadJob.getId(), notification);
		}
	}

}
