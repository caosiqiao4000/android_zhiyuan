package com.wachoo.pangker.downupload;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.ChatRoomListAdapter.SpeakHolder;
import com.wachoo.pangker.db.IMeetRoomDao;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.image.ImageLocalFetcher;

public class GroupAcceptPicUI extends ChatFileUI {

	private SpeakHolder groupHolder;
	private MeetRoom msg;
	// >>>>>>>>>获取本地图片
	private ImageLocalFetcher localFetcher;

	public GroupAcceptPicUI(SpeakHolder groupHolder, Activity activity, ImageLocalFetcher localFetcher) {
		super(activity, null);
		this.groupHolder = groupHolder;
		this.activity = activity;
		this.localFetcher = localFetcher;
		// >>>>>>>...设置为群组的聊天记录
		this.messageType = MSGTYPE_GROUP_MESSAGE;

		application = (PangkerApplication) activity.getApplication();
		groupHolder.ly_message_pic.setOnClickListener(this);
	}

	final Handler downHandler = new Handler() {
		@Override
		public void handleMessage(Message mssg) {
			// TODO Auto-generated method stub
			if (mssg.what == 1) {
				groupHolder.pb_progressBar.setVisibility(View.GONE);
				if (mssg.arg1 == DownloadJob.DOWNLOAD_SUCCESS) {
					String sdpath = mssg.obj.toString();
					msg.setStatus(DownloadJob.DOWNLOAD_SUCCESS);
					msg.setContent(sdpath);
					localFetcher.loadImage(sdpath, groupHolder.iv_imagecover, sdpath);
				} else {
					groupHolder.iv_imagecover.setImageResource(R.drawable.chat_balloon_break);
					msg.setStatus(DownloadJob.DOWNLOAD_FAILE);
				}
				application.removeMsgDownloader(getFilePartID(msg));
				mMeetRoomDao.updateRoomHistory(msg);
			}
			if (mssg.what == 2) {
				groupHolder.pb_progressBar.setProgress(mssg.arg1 * 100 / mssg.arg2);
			}
		}
	};

	private void downloadPic() {
		// TODO Auto-generated method stub
		groupHolder.iv_imagecover.setImageResource(R.drawable.listmod_bighead_photo_default);
		groupHolder.pb_progressBar.setVisibility(View.VISIBLE);
		downloadFile(msg, new MsgDownloader.OnDownloadProgressListener() {
			@Override
			public void onDownloadResultListener(int result, String filepath) {
				// TODO Auto-generated method stub
				Message msg = new Message();
				msg.what = 1;
				msg.arg1 = result;
				msg.obj = filepath;
				downHandler.sendMessage(msg);
			}

			@Override
			public void onDownloadSizeListener(int len, int progress) {
				// TODO Auto-generated method stub
				Message msg = new Message();
				msg.what = 2;
				msg.arg1 = progress;
				msg.arg2 = len;
				downHandler.sendMessage(msg);
			}
		});
	}

	public void initView(MeetRoom msg, IMeetRoomDao mMeetRoomDao) {
		// TODO Auto-generated method stub
		this.msg = msg;
		this.mMeetRoomDao = mMeetRoomDao;
		// 开始下载
		if (msg.getStatus() == DownloadJob.DOWNLOAD_INIT) {
			downloadPic();
		}
		// 下载中
		else if (msg.getStatus() == DownloadJob.DOWNLOADING) {
			groupHolder.pb_progressBar.setVisibility(View.VISIBLE);
		}
		// 下载成功
		else if (msg.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
			groupHolder.pb_progressBar.setVisibility(View.GONE);
			localFetcher.loadImage(msg.getContent(), groupHolder.iv_imagecover, msg.getContent());
		}
		// 下载失败
		else if (msg.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
			groupHolder.iv_imagecover.setImageResource(R.drawable.chat_balloon_break);
			groupHolder.pb_progressBar.setVisibility(View.GONE);
		}
	}

	@Override
	protected void reDownload() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (msg.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
			downloadPic();
		}
	}

}
