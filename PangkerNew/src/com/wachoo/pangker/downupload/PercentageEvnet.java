package com.wachoo.pangker.downupload;

import java.util.EventObject;

import android.os.Message;

import com.wachoo.pangker.entity.DownloadJob;

public class PercentageEvnet extends EventObject implements IMessage {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long downloaderID;
	private int what;
	private int fileLen;
	private int doneLen;
	private String filePath;

	public PercentageEvnet(Object source, long downloaderID, int what, int fileLen, int doneLen,
			String filePath) {
		super(source);
		this.downloaderID = downloaderID;
		this.what = what;
		this.fileLen = fileLen;
		this.doneLen = doneLen;
		this.filePath = filePath;
	}

	/**
	 * @return the downloaderID
	 */
	public long getDownloaderID() {
		return downloaderID;
	}

	/**
	 * @param downloaderID
	 *            the downloaderID to set
	 */
	public void setDownloaderID(int downloaderID) {
		this.downloaderID = downloaderID;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the what
	 */
	public int getWhat() {
		return what;
	}

	/**
	 * @param what
	 *            the what to set
	 */
	public void setWhat(int what) {
		this.what = what;
	}

	/**
	 * @return the fileLen
	 */
	public int getFileLen() {
		return fileLen;
	}

	/**
	 * @param fileLen
	 *            the fileLen to set
	 */
	public void setFileLen(int fileLen) {
		this.fileLen = fileLen;
	}

	/**
	 * @return the doneLen
	 */
	public int getDoneLen() {
		return doneLen;
	}

	/**
	 * @param doneLen
	 *            the doneLen to set
	 */
	public void setDoneLen(int doneLen) {
		this.doneLen = doneLen;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PercentageEvnet [doneLen=" + doneLen + ", downloaderID=" + downloaderID + ", fileLen="
				+ fileLen + ", filePath=" + filePath + ", what=" + what + "]";
	}

	@Override
	public Message getSendMessage() {
		Message message = new Message();
		message.what = this.what;
		if(this.what == DownloadJob.DOWNLOADING){
			message.getData().putInt("done", this.doneLen);
			message.arg1 = fileLen;
		}
		return message;
	}
}
