package com.wachoo.pangker.downupload;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.MsgTalkAdapter.MessageViewHolder;
import com.wachoo.pangker.audio.VoicePlayer;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.image.ImageApkIconFetcher;

public class ChatAcceptVoiceUI extends ChatFileUI {

	private MessageViewHolder voiceHolder;
	private VoicePlayer voicePlayer;
	private boolean isPlay = false;

	public ChatAcceptVoiceUI(MessageViewHolder voiceHolder, Activity context, IUserMsgDao msgDao, ImageApkIconFetcher imageFetcher) {
		super(context, imageFetcher);
		this.voiceHolder = voiceHolder;
		this.activity = context;
		this.mUserMsgDao = msgDao;
		this.application = (PangkerApplication) context.getApplicationContext();

		voiceHolder.tv_content.setOnClickListener(this);
		voicePlayer = VoicePlayer.getVoicePlayer();
	}

	protected void downloadFile() {
		// TODO Auto-generated method stub
		voiceHolder.iv_status.setVisibility(View.GONE);
		voiceHolder.pb_progressBar.setVisibility(View.VISIBLE);
		downloadFile(userMsg, onDownloadProgressListener);
	}

	/**
	 * 根据状态显示userMsg的信息
	 * 
	 * @param userMsg
	 */
	public void initView(ChatMessage userMsg) {
		// TODO Auto-generated method stub
		this.userMsg = userMsg;
		voiceHolder.iv_sms_message.setVisibility(View.VISIBLE);
		voiceHolder.iv_sms_message.setImageResource(R.drawable.record_other_normal);
		voiceHolder.tv_content.setText(userMsg.getContent());
		if (userMsg.getStatus() == DownloadJob.DOWNLOAD_INIT) {
			downloadFile();
		} else if (userMsg.getStatus() == DownloadJob.DOWNLOADING) {
			voiceHolder.pb_progressBar.setVisibility(View.VISIBLE);
			voiceHolder.iv_status.setVisibility(View.GONE);
		} else if (userMsg.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
			voiceHolder.pb_progressBar.setVisibility(View.GONE);
			voiceHolder.iv_status.setVisibility(View.GONE);
		}
		// 接受失败!
		else {
			voiceHolder.iv_status.setVisibility(View.VISIBLE);
			voiceHolder.pb_progressBar.setVisibility(View.GONE);
		}
	}

	final Handler downHandler = new Handler() {
		@Override
		public void handleMessage(Message mssg) {
			if (mssg.arg1 == DownloadJob.DOWNLOAD_SUCCESS) {
				String sdpath = mssg.obj.toString();
				Log.d("sdpath", sdpath);
				voiceHolder.iv_status.setVisibility(View.GONE);
				userMsg.setFilepath(sdpath);
				userMsg.setStatus(DownloadJob.DOWNLOAD_SUCCESS);
			} else {
				voiceHolder.iv_status.setVisibility(View.VISIBLE);
				userMsg.setStatus(DownloadJob.DOWNLOAD_FAILE);
			}
			mUserMsgDao.updateUserMsgStatus(userMsg);
			voiceHolder.pb_progressBar.setVisibility(View.GONE);
			application.removeMsgDownloader(getFilePartID(userMsg));
		}
	};

	MsgDownloader.OnDownloadProgressListener onDownloadProgressListener = new MsgDownloader.OnDownloadProgressListener() {
		@Override
		public void onDownloadResultListener(int result, String filepath) {
			// TODO Auto-generated method stub
			Message msg = new Message();
			msg.what = 1;
			msg.arg1 = result;
			msg.obj = filepath;
			downHandler.sendMessage(msg);
		}

		@Override
		public void onDownloadSizeListener(int len, int progress) {
			// TODO Auto-generated method stub
		}
	};

	@Override
	protected void reDownload() {
		// TODO Auto-generated method stub

	}

	private void openVoice() {
		// TODO Auto-generated method stub
		isPlay = !isPlay;
		if (isPlay) {
			voicePlayer.play(userMsg.getFilepath(), new VoicePlayer.OnVoiceListener() {
				@Override
				public void onVoiceStatusListener(int state) {
					// TODO Auto-generated method stub
					if (state == -1) {
						showToast("抱歉，文件找不到!");
					} else if (state == 0) {
						isPlay = false;
						voiceHolder.iv_sms_message.setImageResource(R.drawable.record_other_normal);
					} else if (state == 1) {
						voiceHolder.iv_sms_message.setImageResource(R.drawable.ptt_action_l_1);
					} else if (state == 2) {
						voiceHolder.iv_sms_message.setImageResource(R.drawable.ptt_action_l_2);
					} else if (state == 3) {
						voiceHolder.iv_sms_message.setImageResource(R.drawable.ptt_action_l_3);
					}
				}
			});
		} else {
			voicePlayer.stop();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (userMsg.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
			openVoice();
		}
		// 如果下载失败重新下载
		else if (userMsg.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
			downloadFile();
		}
	}

}
