package com.wachoo.pangker.downupload;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.pangker.PoiInfoActivity;
import com.wachoo.pangker.adapter.MsgTalkAdapter.MessageViewHolder;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.map.poi.Poi_concise;
import com.wachoo.pangker.util.SmileyParser;
import com.wachoo.pangker.util.Util;

/**
 * PangkerConstant.RES_SPEAK:// 文字聊天信息 PangkerConstant.RES_TOUCH: // 碰一下
 * PangkerConstant.RES_LOCATION:// 位置信息 PangkerConstant.RES_SPEAK_SMS://
 * 通过短信通道发送私信（对方不在线case）
 * 
 * @author zxx
 * 
 */
public class ChatSpeakUI extends ChatFileUI {

	private MessageViewHolder speakHolder;
	private SmileyParser mSmileyParser;

	public ChatSpeakUI(MessageViewHolder speakHolder, Activity activity, ImageApkIconFetcher imageFetcher) {
		// TODO Auto-generated constructor stub
		super(activity, imageFetcher);
		this.activity = activity;
		mSmileyParser = new SmileyParser(activity);
		this.speakHolder = speakHolder;
		speakHolder.tv_content.setOnClickListener(this);
		if (speakHolder.pb_progressBar != null) {
			speakHolder.pb_progressBar.setVisibility(View.GONE);
		}
		if (speakHolder.iv_status != null) {
			speakHolder.iv_status.setVisibility(View.GONE);
		}
	}

	public void initView(ChatMessage userMsg) {
		// TODO Auto-generated method stub
		this.userMsg = userMsg;
		if (userMsg.getMsgType() == PangkerConstant.RES_LOCATION) {
			speakHolder.iv_sms_message.setVisibility(View.VISIBLE);
			speakHolder.iv_sms_message.setImageResource(R.drawable.nav_but_gprs_p);
		} else if (userMsg.getMsgType() == PangkerConstant.RES_TOUCH) {
			speakHolder.iv_sms_message.setVisibility(View.VISIBLE);
			speakHolder.iv_sms_message.setImageResource(R.drawable.msg_touch);
		} else if (userMsg.getMsgType() == PangkerConstant.RES_CALL) {
			speakHolder.iv_sms_message.setVisibility(View.VISIBLE);
			speakHolder.iv_sms_message.setImageResource(R.drawable.icon46_call);
		} else {
			if (userMsg.getDirection().equals(ChatMessage.MSG_FROM_ME)) {
				// 是否通过短信发送消息
				if (userMsg.getMsgType() == PangkerConstant.RES_SPEAK_SMS) {
					speakHolder.iv_sms_message.setVisibility(View.VISIBLE);
				} else {
					speakHolder.iv_sms_message.setVisibility(View.GONE);
				}
			} else {
				speakHolder.pb_progressBar.setVisibility(View.GONE);
				speakHolder.iv_sms_message.setVisibility(View.GONE);
			}
		}
		speakHolder.tv_content.setText(mSmileyParser.addSmileySpans(userMsg.getContent()));
	}

	/**
	 * 这里只是实现打开位置方面的信息
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (userMsg.getMsgType() == PangkerConstant.RES_LOCATION) {
			if (Util.isEmpty(userMsg.getFilepath())) {
				return;
			}
			String lls[] = userMsg.getFilepath().split(",");
			Poi_concise Poi = new Poi_concise();
			Poi.setAddress(userMsg.getContent().substring(2));
			Poi.setLat(lls[0]);
			Poi.setLon(lls[1]);

			Intent intent = new Intent();
			intent.setClass(activity, PoiInfoActivity.class);
			intent.putExtra("PoiItem", Poi);
			activity.startActivity(intent);
		}
	}

	@Override
	protected void reDownload() {
		// TODO Auto-generated method stub

	}

}
