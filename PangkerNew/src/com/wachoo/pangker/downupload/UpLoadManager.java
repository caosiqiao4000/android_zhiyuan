package com.wachoo.pangker.downupload;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.os.Message;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.UploadDao;
import com.wachoo.pangker.db.impl.UploadDaoImpl;
import com.wachoo.pangker.entity.UploadJob;

/**
 * 文件上传管理类
 * 
 * @author wangxin
 * 
 */
public class UpLoadManager {

	private UploadDao mUploadDaoDao;
	protected List<UpLoadlistenter> listenerList = new ArrayList<UpLoadlistenter>();

	public UpLoadManager(Context mContext) {
		super();
		mUploadDaoDao = new UploadDaoImpl(mContext);
		addLoadUploadJobs(mUploadDaoDao.getUploadInofs(((PangkerApplication) mContext.getApplicationContext()).getMyUserID()));
	}

	/**
	 * 添加一个Listener监听器
	 */
	public synchronized void addUpLoadListenter(UpLoadlistenter listener) {
		listenerList.add(listener);
	}

	/**
	 * 移除一个已注册的Listener监听器. 如果监听器列表中已有相同的监听器listener1、listener2,
	 * 并且listener1==listener2, 那么只移除最近注册的一个监听器。
	 */
	public synchronized void removeUpLoadListenter(UpLoadlistenter listener) {
		listenerList.remove(listener);
	}

	/**
	 * 启动监听
	 */
	public void startListenterAction() {
		UpLoadlistenter listener;
		synchronized (listenerList) {
			Iterator<UpLoadlistenter> it = listenerList.iterator();// 这步要注意同步问题
			try {
				while (it.hasNext()) {
					listener = (UpLoadlistenter) it.next();
					// 根据下载文件的地址
					listener.refreshProgressUI(new Message());
				}
			} catch (Exception e) {

			}
		}
	}
	
	public int getUploadOverCount(){
		int count = 0 ;
		for (UploadJob uploadJob : mUploadJobs) {
			if(uploadJob.getStatus() == UploadJob.UPLOAD_SUCCESS){
				count ++;
			}
		}
		return count;
	}

	/*
	 * 以下是上传文件管理
	 * 
	 * 包含了所有的上传文件 完成+进行中的
	 * 
	 * wangxin
	 */
	private List<UploadJob> mUploadJobs = new ArrayList<UploadJob>();

	// >>>>>>>>>>添加一个下载任务 具有排重功能
	public void addUploadJob(UploadJob mQueuedJob) {
		synchronized (mUploadJobs) {
			UploadJob oldJob = getUploadJobByID(mQueuedJob.getId());
			if (oldJob == null)
				mUploadJobs.add(mQueuedJob);
			else {
				mUploadJobs.remove(oldJob);
				mUploadJobs.add(mQueuedJob);
			}
		}
	}

	// >>>>>>>>>>>>>根据id获取JOB
	public UploadJob getUploadJobByID(int JobID) {
		UploadJob job;
		Iterator<UploadJob> it = mUploadJobs.iterator();
		while (it.hasNext()) {
			job = (UploadJob) it.next();
			if (job.getId() == JobID) {
				return job;
			}
		}
		return null;
	}

	public List<UploadJob> getmUploadJobs() {
		return mUploadJobs;
	}

	public void addLoadUploadJobs(List<UploadJob> mUploadJobs) {
		this.mUploadJobs.addAll(mUploadJobs);
	}
	
	public void removeCompleteJob(UploadJob job) {
		// TODO Auto-generated method stub
		synchronized (mUploadJobs) {
			mUploadJobs.remove(job);
		}
		// >>>>>从db中删除数据
		mUploadDaoDao.deleteUploadInfo(job);
	}

	// >>>>>>>>删除所有完成任务
	public void removeAllCompleteJobs() {
		synchronized (mUploadJobs) {
			for (int i = mUploadJobs.size() - 1; i >= 0; i--) {
				UploadJob job = mUploadJobs.get(i);
				if (job.getStatus() == UploadJob.UPLOAD_SUCCESS) {
					mUploadJobs.remove(i);
				}
			}
		}
		// >>>>>从db中删除数据
		mUploadDaoDao.deleteUploadInfos(UploadJob.UPLOAD_SUCCESS);
	}
	
	// >>>>>>>>是否有等待进行的任务
	public boolean haveUploadWaitingJob() {
		Iterator<UploadJob> it = mUploadJobs.iterator();
		while (it.hasNext()) {
			UploadJob job = (UploadJob) it.next();
			if (job.getUploadType() == UploadJob.UPLOAD_WAIT) {
				return true;
			}
		}
		return false;
	}

	// >>>>>>>>是否有正在进行的任务
	public boolean haveUploadingJob() {
		Iterator<UploadJob> it = mUploadJobs.iterator();
		while (it.hasNext()) {
			UploadJob job = (UploadJob) it.next();
			if (job.getStatus() == UploadJob.UPLOAD_UPLOAGINDG) {
				return true;
			}
		}
		return false;
	}

	// >>>>>>>>取消所有上传任务
	public void cancelAllUploadingJobs() {
		Iterator<UploadJob> it = mUploadJobs.iterator();
		while (it.hasNext()) {
			UploadJob job = (UploadJob) it.next();
			if (job.getStatus() == UploadJob.UPLOAD_UPLOAGINDG) {
				job.cancelJob();// 取消上传工作
				mUploadDaoDao.updateUploadInfo(job); // 更新通知db
			}
		}
	}

	// >>>>>>>>>>>>取消指定上传任务
	public void cancelUploadingJob(UploadJob uploadJob) {
		Iterator<UploadJob> it = mUploadJobs.iterator();
		while (it.hasNext()) {
			UploadJob job = (UploadJob) it.next();
			if (job.getId() == uploadJob.getId()) {
				job.cancelJob();// 取消上传工作
				mUploadDaoDao.updateUploadInfo(job); // 更新通知db
				break;
			}
		}
	}
	
	public int getUploadJobStatusByPath(String path){
		Iterator<UploadJob> it = mUploadJobs.iterator();
		while (it.hasNext()) {
			UploadJob job = (UploadJob) it.next();
			if (path.equals(job.getFilePath())) {
				return job.getStatus();
			}
		}
		return -1;
	}
}
