package com.wachoo.pangker.downupload;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.adapter.ChatRoomListAdapter.SpeakHolder;
import com.wachoo.pangker.db.IMeetRoomDao;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UploadOfflineResult;

public class GroupSendPicUI extends ChatFileUI {

	private SpeakHolder groupHolder;
	private Activity activity;
	private MeetRoom msg;
	// >>>>>>>>>获取本地图片
	private ImageLocalFetcher localFetcher;

	public GroupSendPicUI(SpeakHolder groupHolder, Activity activity, ImageLocalFetcher localFetcher) {
		super(activity, null);
		this.groupHolder = groupHolder;
		this.activity = activity;
		this.messageType = MSGTYPE_GROUP_MESSAGE;
		this.localFetcher = localFetcher;
		this.application = (PangkerApplication) activity.getApplication();

	}

	// 显示上传结果
	private void showSendResult(int type) {
		groupHolder.pb_progressBar.setVisibility(View.GONE);
	}

	final Handler handler = new Handler() {
		public void handleMessage(Message handMessage) {
			if (handMessage.what == 1) {
				int progress = (int) (((1.0 * handMessage.arg2) / handMessage.arg1) * 100);
				if (progress > 0 && progress < 100) {
					groupHolder.pb_progressBar.setProgress(progress);
				} else if (progress >= 100) {
					groupHolder.pb_progressBar.setVisibility(View.GONE);
				}
			}
			if (handMessage.what == 2) {
				showSendResult(handMessage.arg1);
			}
		};
	};

	public void initView(MeetRoom msg, IMeetRoomDao mMeetRoomDao) {
		this.msg = msg;
		this.mMeetRoomDao = mMeetRoomDao;
		localFetcher.loadImage(msg.getFilepath(), groupHolder.iv_imagecover, msg.getFilepath());
		// 刚刚初始化好信息，进行传送;
		if (msg.getStatus() == UploadJob.UPLOAD_INITOK) {
			groupHolder.pb_progressBar.setVisibility(View.VISIBLE);
			sendFile(new ProgressListener() {
				@Override
				public void transferred(int size, int progress) {
					// TODO Auto-generated method stub
					Message ms = new Message();
					ms.what = 1;
					ms.arg1 = size;
					ms.arg2 = progress;
					handler.sendMessage(ms);
				}

				@Override
				public void onTransfState(int state) {
					// TODO Auto-generated method stub
					Message ms = new Message();
					ms.what = 2;
					ms.arg1 = state;
					handler.sendMessage(ms);
				}
			});
		}
		// 图片正在上传
		else if (msg.getStatus() == UploadJob.UPLOAD_UPLOAGINDG) {
			groupHolder.pb_progressBar.setVisibility(View.VISIBLE);
		} else {
			showSendResult(msg.getStatus());
		}
	}

	private void sendFile(final ProgressListener listener) {
		msg.setStatus(UploadJob.UPLOAD_UPLOAGINDG);
		mMeetRoomDao.updateRoomHistory(msg);
		try {
			File file = new File(msg.getFilepath());
			MyFilePart uploader = new MyFilePart(msg.getFilepath(), file,
					new MimetypesFileTypeMap().getContentType(file), PangkerConstant.CONTENT_CHARSET, listener);
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", application.getMyUserID()));
			ServerSupportManager serverMana = new ServerSupportManager(activity, new IUICallBackInterface() {
				@Override
				public void uiCallBack(Object response, int caseKey) {
					// TODO Auto-generated method stub
					application.removeFilePart(getFilePartID(msg));
					if (response != null) {
						UploadOfflineResult result = JSONUtil.fromJson(response.toString(), UploadOfflineResult.class);
						if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
							((ChatRoomMainActivity) activity).sendFileResult(msg, result.getFileName());
							msg.setStatus(UploadJob.UPLOAD_SUCCESS);
							mMeetRoomDao.updateRoomHistory(msg);
						} else {
							msg.setStatus(UploadJob.UPLOAD_FAILED);
							mMeetRoomDao.updateRoomHistory(msg);
							listener.onTransfState(UploadJob.UPLOAD_FAILED);
						}
					} else {
						msg.setStatus(UploadJob.UPLOAD_FAILED);
						mMeetRoomDao.updateRoomHistory(msg);
						listener.onTransfState(UploadJob.UPLOAD_FAILED);
					}
					groupHolder.pb_progressBar.setVisibility(View.GONE);
				}
			});
			// 存放上传线程文件
			application.putFilePart(getFilePartID(msg), uploader);
			serverMana.supportRequest(Configuration.getUpOfflineFile(), paras, uploader, null, 0);
			msg.setStatus(UploadJob.UPLOAD_UPLOAGINDG);
			mMeetRoomDao.updateRoomHistory(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			application.removeFilePart(getFilePartID(msg));
		}
	}

	@Override
	protected void reDownload() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

}
