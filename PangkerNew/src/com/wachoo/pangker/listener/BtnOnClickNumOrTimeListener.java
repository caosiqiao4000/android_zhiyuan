package com.wachoo.pangker.listener;

import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * 按钮点击 时间管理类 对超时进行管理
 * 
 * @author caosq
 * 
 */
public abstract class BtnOnClickNumOrTimeListener implements OnClickListener {

	public static final int INTERVAL_TIME = 5000; // 超时时间
	private long btnFristTime = 0; // 点击的时间
	private Button button;
	private Handler handler;

	public BtnOnClickNumOrTimeListener(Button button, Handler handler) {
		super();
		this.button = button;
		this.button.setOnClickListener(this);
		this.handler = handler;
	}
	
	Runnable runnable = new Runnable() {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			button.setEnabled(true);
		}
	};
	
	public void resFresh() {
		// TODO Auto-generated method stub
		btnFristTime = 0;
		button.setEnabled(true);
	}

	@Override
	public void onClick(View v) {
		if(btnFristTime == 0){
			doClick(v, 0);
		} else {
			if(System.currentTimeMillis() - btnFristTime >= INTERVAL_TIME){
				doClick(v, 0);
			} else {
				doClick(null, 1);
				button.setEnabled(false);
				handler.postDelayed(runnable, INTERVAL_TIME);
			}
		}
		btnFristTime = System.currentTimeMillis();
	}

	/**
	 * 
	 * @param resultCode 0 无业务影响  other 其他业务处理
	 */
	public abstract void doClick(View v, int resultCode);

}
