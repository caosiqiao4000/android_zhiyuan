package com.wachoo.pangker.listener;

import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.wachoo.pangker.activity.R;

public class OnArrowChang implements PopupWindow.OnDismissListener{

	private View view;
	
	public OnArrowChang(View view) {
		// TODO Auto-generated constructor stub
		this.view = view;
	}
	
	@Override
	public void onDismiss() {
		// TODO Auto-generated method stub
		((ImageView)view).setImageResource(R.drawable.content_list_but_popmenu_arrow);
	}

}
