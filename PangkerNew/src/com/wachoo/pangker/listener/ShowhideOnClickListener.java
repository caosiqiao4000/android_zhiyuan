package com.wachoo.pangker.listener;

import android.text.InputFilter;
import android.view.View;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;

public class ShowhideOnClickListener implements View.OnClickListener {

	// >>>>>>>设置显示长度
	public static InputFilter[] filters = { new InputFilter.LengthFilter(
			PangkerConstant.showLength) };
	private InputFilter[] morefilters = {};
	private TextView mDetail;
	private String des;

	public ShowhideOnClickListener(TextView mDetail, String des) {
		super();
		this.mDetail = mDetail;
		this.des = des;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		TextView mShow = (TextView) v;
		if (mShow.getText().equals(PangkerConstant.SHOW_MORE_TEXT)) {
			mShow.setText(PangkerConstant.SHOW_UP_TEXT);
			mDetail.setFilters(morefilters);
			mDetail.setText(des);
		} else {
			mShow.setText(PangkerConstant.SHOW_MORE_TEXT);
			mDetail.setFilters(filters);
			mDetail.setText(des);
		}
	}

}
