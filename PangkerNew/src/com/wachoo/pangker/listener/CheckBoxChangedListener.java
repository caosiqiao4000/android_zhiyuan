package com.wachoo.pangker.listener;

import android.graphics.Color;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class CheckBoxChangedListener implements OnCheckedChangeListener {
	private Button mButton;
	private IPromptDialog promptDialog;

	public CheckBoxChangedListener(Button mButton, IPromptDialog promptDialog) {
		super();
		this.mButton = mButton;
		this.promptDialog = promptDialog;
	}

	public Button getmButton() {
		return mButton;
	}

	public void setmButton(Button mButton) {
		this.mButton = mButton;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (isChecked) {
			mButton.setTextColor(Color.BLACK);
			mButton.setEnabled(true);
			if (this.promptDialog != null)
				promptDialog.showDialog();
		} else {
			mButton.setTextColor(Color.GRAY);
			mButton.setEnabled(false);
		}
	}

	public interface IPromptDialog {
		void showDialog();
	}

}
