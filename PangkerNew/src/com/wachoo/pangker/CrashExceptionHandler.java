package com.wachoo.pangker;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.service.MeetRoomService;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.util.ImageUtil;
import com.wifi.locate.manager.WifiLocateManager;

public class CrashExceptionHandler implements UncaughtExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger();
	private final static String TAG = "Crash";
	private Context mContext;// 获取application 对象；
	private PendingIntent restartIntent;// 重启的Intent
	private Thread.UncaughtExceptionHandler defaultExceptionHandler;
	// 单例声明CustomException;
	private static CrashExceptionHandler crashException;
	/** 使用Properties来保存设备的信息和错误堆栈信息 */
	List<Parameter> paras = new ArrayList<Parameter>();
	private static final String VERSION_NAME = "versionName";
	private static final String VERSION_CODE = "versionCode";
	private static final String STACK_TRACE = "stackTrace";
	private static final String CRASH_EXCEPTION = "exception";

	PangkerApplication application;

	// SyncHttpClient http = new SyncHttpClient();

	private CrashExceptionHandler() {
	}

	public static CrashExceptionHandler getInstance() {
		if (crashException == null) {
			crashException = new CrashExceptionHandler();
		}
		return crashException;
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		// TODO Auto-generated method stub
		logger.error("crashException", ex);
		Log.e(TAG, ex.getMessage(), ex);

		Logout();

		if (!handleException(ex) && defaultExceptionHandler != null) {
			// 如果用户没有处理则让系统默认的异常处理器来处理
			// defaultExceptionHandler.uncaughtException(thread, ex);
		} else {
			// Sleep一会后结束程序
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				Log.e(TAG, "Error : ", e);
			}
			logout();
		}
	}

	public void initApplication(Context context) {
		mContext = context;
		application = (PangkerApplication) context.getApplicationContext();
		// 以下用来捕获程序崩溃异常
		Intent intent = new Intent();
		// 参数1：包名，参数2：程序入口的activity
		intent.setClassName("com.wachoo.pangker.activity",
				"com.wachoo.pangker.activity.StartActivity");
		restartIntent = PendingIntent.getActivity(mContext, 0, intent,
				Intent.FLAG_ACTIVITY_NEW_TASK);
		defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(this);
	}

	/**
	 * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成. 开发者可以根据自己的情况来自定义异常处理逻辑
	 * 
	 * @param ex
	 * @return true:如果处理了该异常信息;否则返回false
	 */
	private boolean handleException(Throwable ex) {
		if (ex == null) {
			Log.w(TAG, "handleException --- ex==null");
			return true;
		}
		final String msg = ex.getLocalizedMessage();
		if (msg == null) {
			return false;
		}
		Log.d(TAG, "Msg:" + msg);
		AlarmManager mgr = (AlarmManager) mContext
				.getSystemService(Context.ALARM_SERVICE);
		mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000,
				restartIntent); // 1秒钟后重启应用
		// 收集设备信息
		collectCrashDeviceInfo(mContext);
		// 保存错误报告文件
		saveThrowableInfo(ex);
		// 发送错误报告到服务器
		sendCrashReportsToServer();
		logout();
		return true;
	}

	// 收集设备信息
	private void collectCrashDeviceInfo(Context ctx) {
		PackageManager pm = ctx.getPackageManager();
		try {
			PackageInfo pi = pm.getPackageInfo(ctx.getPackageName(),
					PackageManager.GET_ACTIVITIES);
			if (pi != null) {
				paras.add(new Parameter("userid",
						((PangkerApplication) mContext).getMyUserID()));
				paras.add(new Parameter(VERSION_CODE, "" + pi.versionCode));
				paras.add(new Parameter(VERSION_NAME,
						pi.versionName == null ? "not set" : pi.versionName));
			}

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// 收集错误信息
	private void saveThrowableInfo(Throwable ex) {
		Writer info = new StringWriter();
		PrintWriter printWriter = new PrintWriter(info);
		ex.printStackTrace(printWriter);
		Throwable cause = ex.getCause();
		while (cause != null) {
			cause.printStackTrace(printWriter);
			cause = cause.getCause();
		}
		String result = info.toString();
		printWriter.close();
		// int len = result.length() > 500 ? 500 : result.length();
		paras.add(new Parameter(CRASH_EXCEPTION, ex.getLocalizedMessage()));
		paras.add(new Parameter(STACK_TRACE, result));
	}

	// 发送错误报告到服务器
	private void sendCrashReportsToServer() {
		paras.add(new Parameter("ranger", "5"));
		paras.add(new Parameter("phoneModel", Build.MODEL));
		paras.add(new Parameter("versionSDK", Build.VERSION.SDK));
		paras.add(new Parameter("fersionFrimware", Build.VERSION.RELEASE));
		paras.add(new Parameter("pixels", ImageUtil
				.getWidthPixels((Activity) mContext)
				+ "*"
				+ ImageUtil.getHeightPixels((Activity) mContext)));

		new CrashTask().execute();
	}

	private void logout() {
		// TODO Auto-generated method stub
		((PangkerApplication) mContext).clearData();
		PangkerManager.getActivityStackManager().popAllActivitys();
		Intent pangkerService = new Intent(mContext, PangkerService.class);
		mContext.stopService(pangkerService);
		Intent musicService = new Intent(mContext, MusicService.class);
		mContext.stopService(musicService);
		Intent meetroomService = new Intent(mContext, MeetRoomService.class);
		// >>>>>>>>>退出群组服务
		meetroomService.putExtra(MeetRoomService.MEETING_ROOM_INTENT,
				Configuration.CMANAGER2SP_LOGINOUT_REQ_FLAG);
		mContext.startService(meetroomService);
		mContext.stopService(meetroomService);
		ActivityManager activityMgr = (ActivityManager) mContext
				.getSystemService(Context.ACTIVITY_SERVICE);
		activityMgr.restartPackage(mContext.getPackageName());
		activityMgr.killBackgroundProcesses(mContext.getPackageName());
		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(0);
	}

	class CrashTask extends AsyncTask<Void, Void, Void> {
		SyncHttpClient http = new SyncHttpClient();

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				http.httpPost(Configuration.getCrashException(), paras,
						((PangkerApplication) mContext).getServerTime());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.d(TAG, "Msg:Exception");
			}
			Log.d(TAG, "Msg:CrashException");
			android.os.Process.killProcess(android.os.Process.myPid());
			System.exit(2);
			return null;
		}
	}

	// check downing or uploading res
	// 退出的时候要停止上传下载
	private void Logout() {
		// TODO Auto-generated method stu
		// 关闭自动定位
		WifiLocateManager mWifiLocateManager = new WifiLocateManager(
				mContext.getApplicationContext(), new IUICallBackInterface() {
					@Override
					public void uiCallBack(Object supportResponse, int caseKey) {
						// TODO Auto-generated method stub
						Log.e(TAG,
								"WifiLocateManager-->>>>>>>>>>stopWifiLocate");
					}
				});
		mWifiLocateManager.removeUserWifiInfo(((PangkerApplication) mContext
				.getApplicationContext()).getMyUserID());

		// 停止所有的上传、下载任务
		application.getDownLoadManager().stopAllDownloadingJobs();
		// >>>>>>>>>取消所有上传任务
		application.getUpLoadManager().cancelAllUploadingJobs();
		// >>>>>>>登出群组服务器
		if (application.getCurrunGroup() != null
				&& application.getmSpLoginManager() != null) {
			application.getmSpLoginManager().logout(true, "");
		}

		Log.e(TAG, "ImServerThreadManager-->>>>>>>>>>stopThreadMonitor");

		// >>>>解绑成功后停止服务
		Intent pangkerServce = new Intent(mContext, PangkerService.class);
		mContext.stopService(pangkerServce);

		Log.e(TAG, "stopService-->>>>>>>>>>pangkerservice");

		ActivityManager manager = (ActivityManager) mContext
				.getSystemService(mContext.ACTIVITY_SERVICE);
		manager.restartPackage(mContext.getPackageName());
		// 退出时清除私信通知
		application.getMsgInfoBroadcast().canceNotify();
		PangkerManager.getActivityStackManager().popAllActivitys();

		// >>>>>>清空app数据
		application.clearData();
		// >>>>>>>>>移除音乐播放通知
		NotificationManager notificationManager = (NotificationManager) mContext
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(MusicService.ID);

		// >>>>>>>>>发生一次停止所有服务
		Intent pangkerService = new Intent(mContext, PangkerService.class);
		mContext.stopService(pangkerService);
		Intent musicService = new Intent(mContext, MusicService.class);
		mContext.stopService(musicService);
		Intent meetingService = new Intent(mContext, MeetRoomService.class);
		mContext.stopService(meetingService);
		System.exit(0);

	}

}
