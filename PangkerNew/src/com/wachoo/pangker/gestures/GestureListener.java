package com.wachoo.pangker.gestures;

public interface GestureListener {

	void gestureNext();
	
	void gesturePrev();
}
