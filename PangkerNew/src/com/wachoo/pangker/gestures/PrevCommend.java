package com.wachoo.pangker.gestures;

public class PrevCommend implements GestureCommand{

	GestureListener gestureListener;
	
	public PrevCommend(GestureListener gestureListener) {
		super();
		this.gestureListener = gestureListener;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		if(gestureListener != null){
			gestureListener.gesturePrev();
		}
	}

}
