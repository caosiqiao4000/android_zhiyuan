package com.wachoo.pangker.server.response;

import java.util.List;


/**
 * @author: zhengjy
 * @createTime: 2011-12-27
 * @desc :查询资源返回结果对象
 */
public class ResAppResult extends BaseResult {
	
	private List<AppInfo> resinfos;
	private int count;
	private int totalcount;
	
	
	public ResAppResult(){}
	
	public ResAppResult(List<AppInfo> resinfos, int count, int totalcount) {
		super();
		this.resinfos = resinfos;
		this.count = count;
		this.totalcount = totalcount;
	}
	
	public ResAppResult(List<AppInfo> resinfos, int count, int totalcount,int errorCode,String errorMessage) {
		super();
		this.resinfos = resinfos;
		this.count = count;
		this.totalcount = totalcount;
		this.errorCode= errorCode;
		this.errorMessage = errorMessage;
	}

	public List<AppInfo> getResinfos() {
		return resinfos;
	}

	public void setResinfos(List<AppInfo> resinfos) {
		this.resinfos = resinfos;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
	
	
}
