package com.wachoo.pangker.server.response;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author: wangshitong
 * @createtime: 2012-1-12下午2:53:58
 * @desc:GroupInfo对象定义,查询用户参加过的群组
 */
public class PangkerGroupInfo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long groupId;// 组唯一标识
	private Long uid;// 群组创建者
	private String userName;// 群组创建者名称
	private Long groupSid;// 组SID标识
	private String groupName;// 组名称
	private Integer groupType;// 组类别：10:会议室，11：直播室，12：群组讨论组，13：友踪组
	private String createTime;// 群组创建时间
	private String lastVisitTime;// 群组访问时间
	private String groupLabel;// 群组标签
	private Integer ifShare;// 是否分享(广播)
	private Integer accessType;// 群组访问权限类型
	private Long hot;// 人气值
	private Long score;// 评分
	private Long favorTimes;// 收藏次数
	private Long commentTimes;// 评论次数
	private Long shareTimes;// 转播次数
	private Integer totalMembers;// 在线群组成员数
	private String groupDesc;// 群组简介
	private Integer loType;// 位置属性类型 0:fixed(固定的) 1:moved 可移动的 2:还没有
	private Integer category;
	private Integer ifFavor;// 是否收藏，0：未收藏，1：已收藏
	private Integer ifShareGroup;// 是否转播过，0：为转播，1：已转播
	private Integer visitUsers;// 总来访人次
	private Integer onlineNum;// 在线群组成员数
	private String reshareUserName;// 转播者用户名(转播的资源，必须返回)
	private Long reShareTimes;// 转播时间
	private Integer voiceChannelType = 1;
	private Integer maxSpeechTime = 1;
	private String visitPassword;// 查看群组所需密码
	private String authorizedGroup;// 允许访问的分组联系人

	public String getVisitPassword() {
		return visitPassword;
	}

	public void setVisitPassword(String visitPassword) {
		this.visitPassword = visitPassword;
	}

	public String getAuthorizedGroup() {
		return authorizedGroup;
	}

	public void setAuthorizedGroup(String authorizedGroup) {
		this.authorizedGroup = authorizedGroup;
	}

	public PangkerGroupInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PangkerGroupInfo(Long groupId, Long uid, Long groupSid, String groupName, Integer groupType,
			Date addTime) {
		super();
		this.groupId = groupId;
		this.uid = uid;
		this.groupSid = groupSid;
		this.groupName = groupName;
		this.groupType = groupType;
		this.createTime = addTime == null ? "" : FormatDate(addTime, "yyyy-MM-dd HH:mm:ss");
	}

	public PangkerGroupInfo(Long groupId, Long uid, Long groupSid, String groupName, Integer groupType,
			Date addTime, Date lastVisitTime, String groupLabel, Integer ifShare, Integer roleType, Long hot,
			Long score, Long favorTimes, Long commentTimes, Long shareTimes, Integer totalMembers,
			String groupDesc) {
		super();
		this.groupId = groupId;
		this.uid = uid;
		this.groupSid = groupSid;
		this.groupName = groupName;
		this.groupType = groupType;
		this.createTime = addTime == null ? "" : FormatDate(addTime, "yyyy-MM-dd HH:mm:ss");
		this.lastVisitTime = lastVisitTime == null ? "" : FormatDate(lastVisitTime, "yyyy-MM-dd HH:mm:ss");
		this.groupLabel = groupLabel == null ? "" : groupLabel;
		this.ifShare = ifShare;
		this.accessType = roleType;
		this.hot = hot == null ? 0l : hot;
		this.favorTimes = favorTimes == null ? 0l : favorTimes;
		this.commentTimes = commentTimes == null ? 0l : commentTimes;
		this.shareTimes = shareTimes == null ? 0l : shareTimes;
		this.totalMembers = totalMembers == null ? 0 : totalMembers;
		this.groupDesc = groupDesc;
	}

	public PangkerGroupInfo(Long groupId, Long uid, Long groupSid, String groupName, Integer groupType,
			Date addTime, Date lastVisitTime, String groupLabel, Long hot, Long score, Long favorTimes,
			Long commentTimes, Long shareTimes, Integer totalMembers, String groupDesc) {
		super();
		this.groupId = groupId;
		this.uid = uid;
		this.groupSid = groupSid;
		this.groupName = groupName;
		this.groupType = groupType;
		this.createTime = addTime == null ? "" : FormatDate(addTime, "yyyy-MM-dd HH:mm:ss");
		this.lastVisitTime = lastVisitTime == null ? "" : FormatDate(lastVisitTime, "yyyy-MM-dd HH:mm:ss");
		this.groupLabel = groupLabel == null ? "" : groupLabel;
		this.hot = hot == null ? 0l : hot;
		this.favorTimes = favorTimes == null ? 0l : favorTimes;
		this.commentTimes = commentTimes == null ? 0l : commentTimes;
		this.shareTimes = shareTimes == null ? 0l : shareTimes;
		this.totalMembers = totalMembers == null ? 0 : totalMembers;
		this.groupDesc = groupDesc;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Long getGroupSid() {
		return groupSid;
	}

	public void setGroupSid(Long groupSid) {
		this.groupSid = groupSid;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getGroupType() {
		return groupType;
	}

	public void setGroupType(Integer groupType) {
		this.groupType = groupType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastVisitTime() {
		return lastVisitTime;
	}

	public void setLastVisitTime(String lastVisitTime) {
		this.lastVisitTime = lastVisitTime;
	}

	public String getGroupLabel() {
		return groupLabel;
	}

	public void setGroupLabel(String groupLabel) {
		this.groupLabel = groupLabel;
	}

	public Long getHot() {
		return hot;
	}

	public void setHot(Long hot) {
		this.hot = hot;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Long getFavorTimes() {
		return favorTimes;
	}

	public void setFavorTimes(Long favorTimes) {
		this.favorTimes = favorTimes;
	}

	public Long getCommentTimes() {
		return commentTimes;
	}

	public void setCommentTimes(Long commentTimes) {
		this.commentTimes = commentTimes;
	}

	public Long getShareTimes() {
		return shareTimes;
	}

	public void setShareTimes(Long shareTimes) {
		this.shareTimes = shareTimes;
	}

	public Integer getTotalMembers() {
		return totalMembers;
	}

	public void setTotalMembers(Integer totalMembers) {
		this.totalMembers = totalMembers;
	}

	public String getGroupDesc() {
		return groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	public Integer getIfShare() {
		return ifShare;
	}

	public void setIfShare(Integer ifShare) {
		this.ifShare = ifShare;
	}

	public Integer getAccessType() {
		return accessType;
	}

	public void setAccessType(Integer accessType) {
		this.accessType = accessType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getLoType() {
		return loType;
	}

	public void setLoType(Integer loType) {
		this.loType = loType;
	}

	public String FormatDate(Date dt, String format) {

		SimpleDateFormat df = new SimpleDateFormat(format);
		df.setTimeZone(TimeZone.getDefault());
		return df.format(dt);
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public Integer getIfFavor() {
		return ifFavor;
	}

	public void setIfFavor(Integer ifFavor) {
		this.ifFavor = ifFavor;
	}

	public Integer getIfShareGroup() {
		return ifShareGroup;
	}

	public void setIfShareGroup(Integer ifShareGroup) {
		this.ifShareGroup = ifShareGroup;
	}

	public Integer getVisitUsers() {
		return visitUsers;
	}

	public void setVisitUsers(Integer visitUsers) {
		this.visitUsers = visitUsers;
	}

	public Integer getOnlineNum() {
		return onlineNum;
	}

	public void setOnlineNum(Integer onlineNum) {
		this.onlineNum = onlineNum;
	}

	public String getReshareUserName() {
		return reshareUserName;
	}

	public void setReshareUserName(String reshareUserName) {
		this.reshareUserName = reshareUserName;
	}

	public Long getReShareTimes() {
		return reShareTimes;
	}

	public void setReShareTimes(Long reShareTimes) {
		this.reShareTimes = reShareTimes;
	}

	public Integer getVoiceChannelType() {
		return voiceChannelType;
	}

	public void setVoiceChannelType(Integer voiceChannelType) {
		this.voiceChannelType = voiceChannelType;
	}

	public Integer getMaxSpeechTime() {
		return maxSpeechTime;
	}

	public void setMaxSpeechTime(Integer maxSpeechTime) {
		this.maxSpeechTime = maxSpeechTime;
	}

}
