package com.wachoo.pangker.server.response;

import java.util.List;


/**
 * 判断是否注册用户接口返回
 * 
 * 接口CheckUser.json
 * 
 * @author zhengjy
 *
 * @version 2012-7-17
 */

public class CheckUserResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Checkinfo> rs;
	
	public CheckUserResult(){}

	public CheckUserResult(List<Checkinfo> rs) {
		super();
		this.rs = rs;
	}

	public List<Checkinfo> getRs() {
		return rs;
	}

	public void setRs(List<Checkinfo> rs) {
		this.rs = rs;
	}

}
