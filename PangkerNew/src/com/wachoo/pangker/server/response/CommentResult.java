package com.wachoo.pangker.server.response;

public class CommentResult extends BaseResult {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long commentId;

	public long getCommentId() {
		return commentId;
	}

	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}
}
