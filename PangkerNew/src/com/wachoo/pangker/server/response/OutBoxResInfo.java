package com.wachoo.pangker.server.response;

import java.util.List;

/**
 * 
 * @author zhengjy
 * 
 * 推荐资源对象
 *
 */
public class OutBoxResInfo implements java.io.Serializable {
	
	private Long id;
	private Long resid;
	private String resName;
	private Integer type;
	private String datetime;
	private String reason;
	private String singer;
	private Integer boxType;
	private String mo;
	private List<UserStatus> userStatus;
	
	public OutBoxResInfo(){}
	
	

	public OutBoxResInfo(Long id, Long resid, String resName, Integer type,
			String datetime, String reason, String singer, Integer boxType,
			String mo, List<UserStatus> userStatus) {
		super();
		this.id = id;
		this.resid = resid;
		this.resName = resName;
		this.type = type;
		this.datetime = datetime;
		this.reason = reason;
		this.singer = singer;
		this.boxType = boxType;
		this.mo = mo;
		this.userStatus = userStatus;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getResid() {
		return resid;
	}

	public void setResid(Long resid) {
		this.resid = resid;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public Integer getBoxType() {
		return boxType;
	}

	public void setBoxType(Integer boxType) {
		this.boxType = boxType;
	}

	public String getMo() {
		return mo;
	}

	public void setMo(String mo) {
		this.mo = mo;
	}

	public List<UserStatus> getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(List<UserStatus> userStatus) {
		this.userStatus = userStatus;
	}
	
	
	
}
