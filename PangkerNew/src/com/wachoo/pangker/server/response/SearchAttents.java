package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.entity.ContactGroup;
 
public class SearchAttents extends BaseResult {
	private static final long serialVersionUID = 1L;
	private List<ContactGroup> attentgroup = new ArrayList<ContactGroup>();

	public List<ContactGroup> getAttentgroup() {
		return attentgroup;
	}

	public void setAttentgroup(List<ContactGroup> attentgroup) {
		this.attentgroup = attentgroup;
	}

}
