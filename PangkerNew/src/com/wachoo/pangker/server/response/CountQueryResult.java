package com.wachoo.pangker.server.response;



/**
 * 查询关注人、粉丝数目接口
 * 
 * 接口CountQuery.json
 * 
 * @author zhengjy
 *
 * @version 2012-10-10
 */

public class CountQueryResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer attentCount;
	private Integer fansCount;
	 private Integer vipLevel;  //0：普通会员   1：黄金会员   2：砖石会员
	
	public CountQueryResult(){}

	public CountQueryResult(Integer attentCount, Integer fansCount) {
		super();
		this.attentCount = attentCount;
		this.fansCount = fansCount;
	}

	public Integer getAttentCount() {
		return attentCount;
	}

	public void setAttentCount(Integer attentCount) {
		this.attentCount = attentCount;
	}

	public Integer getFansCount() {
		return fansCount;
	}

	public void setFansCount(Integer fansCount) {
		this.fansCount = fansCount;
	}

	public Integer getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(Integer vipLevel) {
		this.vipLevel = vipLevel;
	}
	
}
