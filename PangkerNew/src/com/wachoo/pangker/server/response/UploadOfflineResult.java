package com.wachoo.pangker.server.response;

@SuppressWarnings("serial")
public class UploadOfflineResult extends BaseResult{

	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
