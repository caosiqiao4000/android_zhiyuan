package com.wachoo.pangker.server.response;

import java.util.List;


/**
 * 添加成员校验接口（是否旁客用户）
 * 
 * 接口AddressAddMemberCheck.json
 * 
 * @author zhengjy
 *
 * @version 2012-6-12
 */

public class QueryResGroupShareResult extends BaseResult{

	private Integer count;
	private Integer sumCount;
	private List<ResGroupshare> resGroupShare;
	
	
	public QueryResGroupShareResult(){}
	

	public QueryResGroupShareResult(Integer count, Integer sumCount,
			List<ResGroupshare> resGroupShare) {
		super();
		this.count = count;
		this.sumCount = sumCount;
		this.resGroupShare = resGroupShare;
	}


	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getSumCount() {
		return sumCount;
	}

	public void setSumCount(Integer sumCount) {
		this.sumCount = sumCount;
	}

	public List<ResGroupshare> getResGroupShare() {
		return resGroupShare;
	}

	public void setResGroupShare(List<ResGroupshare> resGroupShare) {
		this.resGroupShare = resGroupShare;
	}
	


	

	
}
