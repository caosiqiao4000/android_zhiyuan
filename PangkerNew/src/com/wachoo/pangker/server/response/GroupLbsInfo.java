package com.wachoo.pangker.server.response;

public class GroupLbsInfo extends MovingRes {
	public static final String GSID_FIELD = "gSid";// group 实现群组功能的sid
	public static final String GNAME_FIELD = "gName";// group name
	public static final String GTYPE_FIELD = "gType";// group type
	public static final String GTAGS_FIELD = "tags";// group tags

	public static final int LOCATION_FIXED = 0;//不移动的群组
	public static final int LOCATION_MOVED = 1;//移动的群组
	public static final int LOCATION_NOTHAVE = 2;//没有位置信息的群组

	private Long gSid;// 实现群组功能的sid
	private String gName;// 组名称
	private int gType;// 组类别：10:会议室，11：直播室，12：群组讨论组，13：友踪组
	private int loType;// 群组的位置属性
	private String tags;// 用空格分割字符串就ok
	private int sum;// 群组内人数
	private Integer category; 

	/**
	 * @return the loType
	 */
	public int getLoType() {
		return loType;
	}

	/**
	 * @param loType
	 *            the loType to set
	 */
	public void setLoType(int loType) {
		this.loType = loType;
	}

	/**
	 * @return the gSid
	 */
	public Long getgSid() {
		return gSid;
	}

	/**
	 * @param gSid
	 *            the gSid to set
	 */
	public void setgSid(Long gSid) {
		this.gSid = gSid;
	}

	/**
	 * @return the gName
	 */
	public String getgName() {
		return gName;
	}

	/**
	 * @param gName
	 *            the gName to set
	 */
	public void setgName(String gName) {
		this.gName = gName;
	}

	/**
	 * @return the gType
	 */
	public Integer getgType() {
		return gType;
	}

	/**
	 * @param gType
	 *            the gType to set
	 */
	public void setgType(int gType) {
		this.gType = gType;
	}

	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * @return the sum
	 */
	public int getSum() {
		return sum;
	}

	/**
	 * @param sum
	 *            the sum to set
	 */
	public void setSum(int sum) {
		this.sum = sum;
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	
}
