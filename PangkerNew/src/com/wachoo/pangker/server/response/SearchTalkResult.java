package com.wachoo.pangker.server.response;

import java.util.List;

import com.wachoo.pangker.entity.TextSpeakInfo;

public class SearchTalkResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int count;
	private int sumCount;
	private List<TextSpeakInfo> speakInfoList;
	
	public SearchTalkResult(){
		
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<TextSpeakInfo> getSpeakInfoList() {
		return speakInfoList;
	}

	public void setSpeakInfoList(List<TextSpeakInfo> speakInfoList) {
		this.speakInfoList = speakInfoList;
	}

	public int getSumCount() {
		return sumCount;
	}

	public void setSumCount(int sumCount) {
		this.sumCount = sumCount;
	}
	
	
}
