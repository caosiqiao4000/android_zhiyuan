package com.wachoo.pangker.server.response;


/**
 * P2pServerconfig entity. @author MyEclipse Persistence Tools
 */

public class P2pServerconfig implements java.io.Serializable {

	// Fields

	private Long id;
	private String uid;
	private String serviceName;
	private Integer serviceType;
	private Long uidStart;
	private Long uidEnd;
	private String ipAddress;
	private Integer port;
	private Integer udpPort;
	private Integer udtPort;
	private Integer tcpPort;
	private String wkey;
	private String rkey;
	private Integer flag;
	private String dealtime;
	private String memo;

	// Constructors

	/** default constructor */
	public P2pServerconfig() {
	}

	/** full constructor */
	public P2pServerconfig(String uid, String serviceName, Integer serviceType, Long uidStart, Long uidEnd,
			String ipAddress, Integer port, Integer udpPort, Integer udtPort, Integer tcpPort, String wkey,
			String rkey, Integer flag, String dealtime, String memo) {
		this.uid = uid;
		this.serviceName = serviceName;
		this.serviceType = serviceType;
		this.uidStart = uidStart;
		this.uidEnd = uidEnd;
		this.ipAddress = ipAddress;
		this.port = port;
		this.udpPort = udpPort;
		this.udtPort = udtPort;
		this.tcpPort = tcpPort;
		this.wkey = wkey;
		this.rkey = rkey;
		this.flag = flag;
		this.dealtime = dealtime;
		this.memo = memo;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUid() {
		return this.uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Integer getServiceType() {
		return this.serviceType;
	}

	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}

	public Long getUidStart() {
		return this.uidStart;
	}

	public void setUidStart(Long uidStart) {
		this.uidStart = uidStart;
	}

	public Long getUidEnd() {
		return this.uidEnd;
	}

	public void setUidEnd(Long uidEnd) {
		this.uidEnd = uidEnd;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getPort() {
		return this.port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getUdpPort() {
		return this.udpPort;
	}

	public void setUdpPort(Integer udpPort) {
		this.udpPort = udpPort;
	}

	public Integer getUdtPort() {
		return this.udtPort;
	}

	public void setUdtPort(Integer udtPort) {
		this.udtPort = udtPort;
	}

	public Integer getTcpPort() {
		return this.tcpPort;
	}

	public void setTcpPort(Integer tcpPort) {
		this.tcpPort = tcpPort;
	}

	public String getWkey() {
		return this.wkey;
	}

	public void setWkey(String wkey) {
		this.wkey = wkey;
	}

	public String getRkey() {
		return this.rkey;
	}

	public void setRkey(String rkey) {
		this.rkey = rkey;
	}

	public Integer getFlag() {
		return this.flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getDealtime() {
		return this.dealtime;
	}

	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

}