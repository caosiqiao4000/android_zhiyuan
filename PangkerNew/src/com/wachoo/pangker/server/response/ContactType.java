package com.wachoo.pangker.server.response;

/**
 * @author: feijinbo
 * @createTime: 2011-12-9 下午10:18:21
 * @version: 1.0
 * @desc :联系人类别对象定义
 */
public class ContactType implements java.io.Serializable {

	private int type;// 联系人类别标识，如10管理员，12好友，13关注人
	private String name;// 联系人类别名称;
	private int count; // 当前类别下联系人总数;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
