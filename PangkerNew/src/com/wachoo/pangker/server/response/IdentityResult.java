package com.wachoo.pangker.server.response;

/**
 * @author: feijinbo
 * @createTime: 2011-12-27 下午10:36:09
 * @version: 1.0
 * @desc :返回的身份类别信息对象
 */
public class IdentityResult {
	//10：查询系统身份列表     11：查询用户身份列表

	public static final String SYS_OPTYPE = "10";
    public static final String USER_OPTYPE = "11";
	private int identityCode;

	private String identityName;

	/**
	 * @return the identityCode
	 */
	public int getIdentityCode() {
		return identityCode;
	}

	/**
	 * @param identityCode
	 *            the identityCode to set
	 */
	public void setIdentityCode(int identityCode) {
		this.identityCode = identityCode;
	}

	/**
	 * @return the identityName
	 */
	public String getIdentityName() {
		return identityName;
	}

	/**
	 * @param identityName
	 *            the identityName to set
	 */
	public void setIdentityName(String identityName) {
		this.identityName = identityName;
	}

}
