package com.wachoo.pangker.server.response;



/**
 * 文件上传
 * @author wubo
 * @createtime 2012-2-2 上午09:46:16
 */
public class HeadUploadResult extends BaseResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String iconName;

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

}
