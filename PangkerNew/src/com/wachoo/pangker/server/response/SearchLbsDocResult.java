package com.wachoo.pangker.server.response;

/**
* @ClassName: SearchLbsDocResult
* @Description: 调用搜索引警搜索问题返回结果<文档>
* @author longxianwen 
* @date Mar 26, 2012 2:26:57 PM
 */
public class SearchLbsDocResult implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private DocResult data;
	private boolean success;
	
	public SearchLbsDocResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SearchLbsDocResult(DocResult data, boolean success) {
		super();
		this.data = data;
		this.success = success;
	}

	public DocResult getData() {
		return data;
	}

	public void setData(DocResult data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}
