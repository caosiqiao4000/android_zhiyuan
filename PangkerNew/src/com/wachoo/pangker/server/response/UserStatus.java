package com.wachoo.pangker.server.response;


public class UserStatus implements java.io.Serializable {
	private Long uid;
	private Integer status;//用户阅读状态
	private String nickname;//用户昵称
	
	public UserStatus(){}
	
	public UserStatus(Long uid, Integer status, String nickname) {
		super();
		this.uid = uid;
		this.status = status;
		this.nickname = nickname;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
}
