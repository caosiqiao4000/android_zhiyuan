package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: feijinbo
 * @createTime: 2011-12-27 下午10:25:20
 * @version: 1.0
 * @desc :用户身份类别查询信息类
 */
public class IdentityQueryResult extends BaseResult{

	private List<IdentityResult> identitys = new ArrayList<IdentityResult>();

	/**
	 * @return the identitys
	 */
	public List<IdentityResult> getIdentitys() {
		return identitys;
	}

	/**
	 * @param identitys the identitys to set
	 */
	public void setIdentitys(List<IdentityResult> identitys) {
		this.identitys = identitys;
	}

	

}
