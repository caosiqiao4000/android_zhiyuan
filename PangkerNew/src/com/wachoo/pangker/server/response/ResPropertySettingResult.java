package com.wachoo.pangker.server.response;

public class ResPropertySettingResult extends BaseResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String result;
	
	public ResPropertySettingResult() {
		// TODO Auto-generated constructor stub
	}
	
	public ResPropertySettingResult(String result) {
		super();
		this.result = result;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
}
