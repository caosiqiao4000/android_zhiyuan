package com.wachoo.pangker.server.response;

public class CallMeSetResult extends BaseResult {
	
	private String mobile;
	
	public CallMeSetResult(){}
	
	public CallMeSetResult(int errorCode,String errorMessage,String mobile){
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.mobile = mobile;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	

}
