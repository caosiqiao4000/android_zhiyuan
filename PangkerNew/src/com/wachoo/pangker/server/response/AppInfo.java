﻿package com.wachoo.pangker.server.response;

/**
 * 
 * @author wangxin
 * 
 */
public class AppInfo extends MovingRes {

	public static final String APPNAME_FIELD = "appname";// 应用名称
	public static final String APPTYPE_FIELD = "apptype";// 应用类型

	private String appname; // 应用名称
	private String apptype; // 应用类型
	private String appicon; // 图片预览地址
	private String appdesc; //应用描述
	
	

	/**
	 * @return the appicon
	 */
	public String getAppicon() {
		return appicon;
	}

	/**
	 * @param appicon the appicon to set
	 */
	public void setAppicon(String appicon) {
		this.appicon = appicon;
	}

	/**
	 * @return the appname
	 */
	public String getAppname() {
		return appname;
	}

	/**
	 * @param appname
	 *            the appname to set
	 */
	public void setAppname(String appname) {
		this.appname = appname;
	}

	/**
	 * @return the apptype
	 */
	public String getApptype() {
		return apptype;
	}

	/**
	 * @param apptype
	 *            the apptype to set
	 */
	public void setApptype(String apptype) {
		this.apptype = apptype;
	}

	public String getAppdesc() {
		return appdesc;
	}

	public void setAppdesc(String appdesc) {
		this.appdesc = appdesc;
	}
}
