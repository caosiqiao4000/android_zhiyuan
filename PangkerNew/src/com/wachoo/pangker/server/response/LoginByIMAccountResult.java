package com.wachoo.pangker.server.response;


@SuppressWarnings("serial")
public class LoginByIMAccountResult extends BaseResult{
	
	private int changedIMSI;
	
	public int getChangedIMSI() {
		return changedIMSI;
	}
	public void setChangedIMSI(int changedIMSI) {
		this.changedIMSI = changedIMSI;
	}
}
