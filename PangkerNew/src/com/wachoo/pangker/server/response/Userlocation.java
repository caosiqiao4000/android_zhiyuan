package com.wachoo.pangker.server.response;

public class Userlocation{
	
	public Userlocation() {
		// TODO Auto-generated constructor stub
	}
	
	private long uid;
	private double ctrlon;
	private double ctrlat;
	private long locationTime;
	
	
	public long getUid() {
		return uid;
	}


	public void setUid(long uid) {
		this.uid = uid;
	}

	public double getCtrlon() {
		return ctrlon;
	}

	public void setCtrlon(double ctrlon) {
		this.ctrlon = ctrlon;
	}

	public double getCtrlat() {
		return ctrlat;
	}

	public void setCtrlat(double ctrlat) {
		this.ctrlat = ctrlat;
	}

	public long getLocationTime() {
		return locationTime;
	}

	public void setLocationTime(long locationTime) {
		this.locationTime = locationTime;
	}

}
