package com.wachoo.pangker.server.response;

import com.wachoo.pangker.entity.TextSpeakInfo;

public class SearchSpeakInfoResult extends BaseResult {

	private static final long serialVersionUID = 1L;

	private TextSpeakInfo textSpeakInfo;

	public TextSpeakInfo getTextSpeakInfo() {
		return textSpeakInfo;
	}

	public void setTextSpeakInfo(TextSpeakInfo textSpeakInfo) {
		this.textSpeakInfo = textSpeakInfo;
	}
	
}
