package com.wachoo.pangker.server.response;

import java.util.List;

import com.wachoo.pangker.entity.UserInfo;

/**
 * @author zhengjy
 * @createTime
 * @desc 查找(通讯录)好友接口
 */
public class ImportContactsFriendresult extends BaseResult {

	private static final long serialVersionUID = 1L;

	private List<UserInfo> data;

	public ImportContactsFriendresult() {

	}

	public ImportContactsFriendresult(List<UserInfo> data) {
		super();
		this.data = data;
	}

	public ImportContactsFriendresult(List<UserInfo> data, int errorCode,
			String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.data = data;

	}

	public List<UserInfo> getData() {
		return data;
	}

	public void setData(List<UserInfo> data) {
		this.data = data;
	}

}
