package com.wachoo.pangker.server.response.poi;

import java.util.List;

import com.wachoo.pangker.server.response.BaseResult;

@SuppressWarnings("serial")
public class PoiReplyResult extends BaseResult{

	private int pagecount;
	
	private List<PoiReply> poiReplyList;

	public int getPagecount() {
		return pagecount;
	}

	public void setPagecount(int pagecount) {
		this.pagecount = pagecount;
	}

	public List<PoiReply> getPoiReplyList() {
		return poiReplyList;
	}

	public void setPoiReplyList(List<PoiReply> poiReplyList) {
		this.poiReplyList = poiReplyList;
	}

}
