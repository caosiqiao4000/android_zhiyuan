package com.wachoo.pangker.server.response;
/**
 * @author zhengjy
 *
 * @version 2012-3-16 
 */

public class ShareInfo implements java.io.Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long resId;// 资源id
	private Long shareId;//分享ID，对应转播有效，广播时为空。
	private String name;// 资源所属用户id
	private Integer type;//资源类型资源类型 10：问答，11：阅读，12：图片，13：音乐，14：应用，15：直播，16：视频.
	private Integer flag;//标识，0：广播（shareId为空），1：转播（shareId不为空）
	private Long score;// 资源评分
	private Long hot;// 人气值
	private Long shareTimes;// 分享次数
	private Long downloadTimes;// 下载次数
	private Long visitedTimes;// 访问次数
	private Long favorTimes;// 收藏次数
	private Long size;//资源大小（对应资源，问答，群组无此属性）
	private String ownername;
	
	
	
	public Long getResId() {
		return resId;
	}
	public void setResId(Long resId) {
		this.resId = resId;
	}
	public Long getShareId() {
		return shareId;
	}
	public void setShareId(Long shareId) {
		this.shareId = shareId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Long getScore() {
		return score;
	}
	public void setScore(Long score) {
		this.score = score;
	}
	public Long getHot() {
		return hot;
	}
	public void setHot(Long hot) {
		this.hot = hot;
	}
	public Long getShareTimes() {
		return shareTimes;
	}
	public void setShareTimes(Long shareTimes) {
		this.shareTimes = shareTimes;
	}
	public Long getDownloadTimes() {
		return downloadTimes;
	}
	public void setDownloadTimes(Long downloadTimes) {
		this.downloadTimes = downloadTimes;
	}
	public Long getVisitedTimes() {
		return visitedTimes;
	}
	public void setVisitedTimes(Long visitedTimes) {
		this.visitedTimes = visitedTimes;
	}
	public Long getFavorTimes() {
		return favorTimes;
	}
	public void setFavorTimes(Long favorTimes) {
		this.favorTimes = favorTimes;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public String getOwnername() {
		return ownername;
	}
	public void setOwnername(String ownername) {
		this.ownername = ownername;
	}

}
