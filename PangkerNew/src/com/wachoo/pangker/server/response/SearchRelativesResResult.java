package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author: zhanghg
 * @createTime 2013-5-10上午9:35:33
 * @version: 1.0
 * @desc:
 * @lastModify:
 */
public class SearchRelativesResResult extends BaseResult {
	private List<RelativesResInfo> relativesResInfo = new ArrayList<RelativesResInfo>();// 亲友资源
	private int count;// 结果数量
	private int sumCount;// 总数量

	public List<RelativesResInfo> getRelativesResInfo() {
		return relativesResInfo;
	}

	public void setRelativesResInfo(List<RelativesResInfo> relativesResInfo) {
		this.relativesResInfo = relativesResInfo;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getSumCount() {
		return sumCount;
	}

	public void setSumCount(int sumCount) {
		this.sumCount = sumCount;
	}
	
}