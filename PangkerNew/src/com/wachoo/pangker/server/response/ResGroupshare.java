package com.wachoo.pangker.server.response;

import android.text.InputFilter;

import com.wachoo.pangker.PangkerConstant;

public class ResGroupshare extends BaseResult {
	/**
	 * @TODO
	 * 
	 * @author zhengjy
	 * 
	 *         2013-1-15 $
	 */
	public ResGroupshare() {
	}

	private Long id;
	private Long groupId;
	private Long shareUid;
	private String nickname;
	private Long resId;
	private Integer resType;
	private String resName;
	private String shareReason;
	private String shareTime;
	private String mo;

	// >>>>>>>>>>>>显示更多
	private String btnShowText = PangkerConstant.SHOW_MORE_TEXT;
	private InputFilter[] filters = null;

	public ResGroupshare(Long id, Long groupId, Long shareUid, String nickname, Long resId, Integer resType,
			String resName, String shareReason, String shareTime, String mo) {
		super();
		this.id = id;
		this.groupId = groupId;
		this.shareUid = shareUid;
		this.nickname = nickname;
		this.resId = resId;
		this.resType = resType;
		this.resName = resName;
		this.shareReason = shareReason;
		this.shareTime = shareTime;
		this.mo = mo;
	}

	public String getBtnShowText() {
		return btnShowText;
	}

	public void setBtnShowText(String btnShowText) {
		this.btnShowText = btnShowText;
	}

	public InputFilter[] getFilters() {
		if (filters == null) {
			filters = new InputFilter[1];
			filters[0] = new InputFilter.LengthFilter(PangkerConstant.showLength);
		}
		return filters;
	}

	public void setFilters(InputFilter[] filters) {
		this.filters = filters;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getShareUid() {
		return shareUid;
	}

	public void setShareUid(Long shareUid) {
		this.shareUid = shareUid;
	}

	public String getNickname() {
		return this.nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Long getResId() {
		return resId;
	}

	public void setResId(Long resId) {
		this.resId = resId;
	}

	public Integer getResType() {
		return resType;
	}

	public void setResType(Integer resType) {
		this.resType = resType;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getShareReason() {
		return shareReason;
	}

	public void setShareReason(String shareReason) {
		this.shareReason = shareReason;
	}

	public String getShareTime() {
		return shareTime;
	}

	public void setShareTime(String shareTime) {
		this.shareTime = shareTime;
	}

	public String getMo() {
		return this.mo;
	}

	public void setMo(String mo) {
		this.mo = mo;
	}

}
