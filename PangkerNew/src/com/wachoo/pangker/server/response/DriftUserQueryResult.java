package com.wachoo.pangker.server.response;

import java.util.List;

import com.wachoo.pangker.entity.UserItem;

/**
 *@author wubo
 *@createtime 2012-5-23
 */
public class DriftUserQueryResult extends BaseResult {

	private List<UserItem> users;

	public List<UserItem> getUsers() {
		return users;
	}

	public void setUsers(List<UserItem> users) {
		this.users = users;
	}
	
	
}
