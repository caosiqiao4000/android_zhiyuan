package com.wachoo.pangker.server.response;

import java.util.List;


/**
 * 
 *  zhengjy
 *  
 *  2012-3-16
 */
public class ShareQueryResult extends BaseResult {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<ShareInfo> resinfos;

	private int count;
	
	private int totalcount;
	
	public ShareQueryResult(){}


	public ShareQueryResult(List<ShareInfo> resinfos) {
		super();
		this.resinfos = resinfos;
	}

	public List<ShareInfo> getResinfos() {
		return resinfos;
	}

	public void setResinfos(List<ShareInfo> resinfos) {
		this.resinfos = resinfos;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}


	public int getTotalcount() {
		return totalcount;
	}


	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
	
    
	
}