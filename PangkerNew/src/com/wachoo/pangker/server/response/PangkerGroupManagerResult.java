package com.wachoo.pangker.server.response;

/**
 * @author: zhengjy
 * @createTime: 2012-1-16 
 * @version: 1.0
 *
 */
public class PangkerGroupManagerResult extends BaseResult {
	
	public GroupInfo groupInfo;   //组信息对象，创建组成功后返回。
	

	public GroupInfo getGroupInfo() {
		return groupInfo;
	}

	public void setGroupInfo(GroupInfo groupInfo) {
		this.groupInfo = groupInfo;
	}
}
