package com.wachoo.pangker.server.response;

import java.util.ArrayList;
import java.util.List;

import com.wachoo.pangker.entity.UserInfo;

public class SearchAttentsResult extends BaseResult {
	private static final long serialVersionUID = 1L;
	public int errorCode = -1;// 未处理
	public String errorMessage;
	private int count;
	private int sumCount;
	private List<UserInfo> users = new ArrayList<UserInfo>();

	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getSumCount() {
		return sumCount;
	}
	public void setSumCount(int sumCount) {
		this.sumCount = sumCount;
	}
	
	public SearchAttentsResult() {

	}

	public SearchAttentsResult(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	public List<UserInfo> getUsers() {
		return users;
	}
	public void setUsers(List<UserInfo> users) {
		this.users = users;
	}

}
