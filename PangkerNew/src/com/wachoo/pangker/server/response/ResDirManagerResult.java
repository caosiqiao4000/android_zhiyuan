package com.wachoo.pangker.server.response;

public class ResDirManagerResult extends BaseResult {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String dirId;

	public ResDirManagerResult() {
		// TODO Auto-generated constructor stub
	}
	
	public ResDirManagerResult(String dirId) {
		super();
		this.dirId = dirId;
	}

	public String getDirId() {
		return dirId;
	}

	public void setDirId(String dirId) {
		this.dirId = dirId;
	}

}
