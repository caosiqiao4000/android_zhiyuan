package com.wachoo.pangker.server.response;

/**
 * 
 * @author wangxin
 * 
 */
public class VideoInfo extends MovingRes {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 
	
	//详细属性
	private String videoname; // 视频名称
	private Integer videotype; // 视频类型

	 

	public String getVideoname() {
		return videoname;
	}

	public void setVideoname(String videoname) {
		this.videoname = videoname;
	}

	public Integer getVideotype() {
		return videotype;
	}

	public void setVideotype(Integer videotype) {
		this.videotype = videotype;
	}

	public VideoInfo() {
	}

	 
}
