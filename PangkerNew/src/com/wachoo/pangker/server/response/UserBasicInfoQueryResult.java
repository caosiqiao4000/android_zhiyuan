package com.wachoo.pangker.server.response;

import java.util.List;

import com.wachoo.pangker.entity.UserItem;


/**
 *查询用户基本信息接口
 *
 *UserBasicInfoQuery.json
 */
public class UserBasicInfoQueryResult extends BaseResult {

	private String username;
	private String account;
	private String sign;
	private Integer attentCount; //关注人数量
	private Integer fansCount;// 粉丝数量
	private Integer isFriend;//  0：NO，不是好友关系 1：YES，好友关系
    private Integer isAttent;//  0：未关注过该用户 1：关注过该用户
    private Integer vipLevel;  //0：普通会员   1：黄金会员   2：砖石会员
    private List<UserItem> recentUsers;
	public UserBasicInfoQueryResult() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserBasicInfoQueryResult(int errorCode, String errorMessage) {
		super(errorCode, errorMessage);
		// TODO Auto-generated constructor stub
	}
	public UserBasicInfoQueryResult(String userName, String account,
			String sign, Integer attentCount, Integer fansCount,
			Integer isFriend, Integer isAttent,List<UserItem> recentUsers) {
		super();
		this.username = userName;
		this.account = account;
		this.sign = sign;
		this.attentCount = attentCount;
		this.fansCount = fansCount;
		this.isFriend = isFriend;
		this.isAttent = isAttent;
		this.recentUsers = recentUsers;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public Integer getAttentCount() {
		return attentCount;
	}
	public void setAttentCount(Integer attentCount) {
		this.attentCount = attentCount;
	}
	public Integer getFansCount() {
		return fansCount;
	}
	public void setFansCount(Integer fansCount) {
		this.fansCount = fansCount;
	}
	public Integer getIsFriend() {
		return isFriend;
	}
	public void setIsFriend(Integer isFriend) {
		this.isFriend = isFriend;
	}
	public Integer getIsAttent() {
		return isAttent;
	}
	public void setIsAttent(Integer isAttent) {
		this.isAttent = isAttent;
	}
	public List<UserItem> getRecentUsers() {
		return recentUsers;
	}
	public void setRecentUsers(List<UserItem> recentUsers) {
		this.recentUsers = recentUsers;
	}
	public Integer getVipLevel() {
		return vipLevel;
	}
	public void setVipLevel(Integer vipLevel) {
		this.vipLevel = vipLevel;
	}
	

}
