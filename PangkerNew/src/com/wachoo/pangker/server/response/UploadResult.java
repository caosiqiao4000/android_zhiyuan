package com.wachoo.pangker.server.response;
/**
 * @author zhengjy
 *
 * @version 2012-1-12 
 */

public class UploadResult extends BaseResult{
	
	private String resId;


	public String getResId() {
		return resId;
	}

	public void setResId(String resId) {
		this.resId = resId;
	}
    
}
