package com.wachoo.pangker.server.response;


/**
 * 单位通讯录管理接口返回
 * 
 * 接口AddressBookManager.json
 * 
 * @author zhengjy
 *
 * @version 2012-6-12
 */

public class AddressBookManagerResult extends BaseResult{

	private String id;
	
	
	public AddressBookManagerResult(){}
	
	public AddressBookManagerResult(int errorCode,String errorMessage, String id) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
}
