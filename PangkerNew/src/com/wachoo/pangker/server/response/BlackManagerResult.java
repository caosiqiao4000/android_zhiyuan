package com.wachoo.pangker.server.response;

import java.util.List;

import com.wachoo.pangker.entity.UserItem;

/**
 * 黑名单管理接口返回
 * 
 * @author zhengjy
 *
 * @version 2012-6-4
 */

public class BlackManagerResult extends BaseResult{

	private List<UserItem> users;
	private int count;//返回记录数
	private int tcount;//符合条件，记录总数
	
	
	public BlackManagerResult(){}
	
	public BlackManagerResult(int errorCode,String errorMessage, List<UserItem> users) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.users = users;
	}

	public List<UserItem> getUsers() {
		return users;
	}

	public void setUsers(List<UserItem> users) {
		this.users = users;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTcount() {
		return tcount;
	}

	public void setTcount(int tcount) {
		this.tcount = tcount;
	}
	
}
