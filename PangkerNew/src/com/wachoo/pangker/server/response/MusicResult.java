package com.wachoo.pangker.server.response;

import java.util.List;


/**
 * 
* @ClassName: MusicResult
* @Description: TODO(这里用一句话描述这个类的作用)
* @author longxianwen 
* @date Mar 8, 2012 5:22:20 PM
*
 */
public class MusicResult implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int total;
	private boolean exact;
	private List<MusicInfo> data;
	
	
	public MusicResult(){}
	
	public MusicResult(int total, boolean exact, List<MusicInfo> data) {
		super();
		this.total = total;
		this.exact = exact;
		this.data = data;
	}
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public boolean isExact() {
		return exact;
	}
	public void setExact(boolean exact) {
		this.exact = exact;
	}
	public List<MusicInfo> getData() {
		return data;
	}
	public void setData(List<MusicInfo> data) {
		this.data = data;
	}
	
}
