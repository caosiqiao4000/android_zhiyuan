package com.wachoo.pangker.server.response;

/**
 * @author: wangshitong
 * @createTime: 2011-12-9 下午10:18:21
 * @version: 1.0
 * @desc :联系人类别对象定义
 */
public class GroupManagerResult extends BaseResult{
	
	private String groupId ; //分组Id,对于创建分组有效

	public String getGroupid() {
		return groupId;
	}

	public void setGroupid(String groupid) {
		this.groupId = groupid;
	}
	
}
