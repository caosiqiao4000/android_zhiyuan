package com.wachoo.pangker.server.response;


/**
 * @author zhengjy
 *
 * @version 2012-6-6 
 */

public class EventResult implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;   //资源id
	private String name;    //资源名称
	private Integer type;   //资源类型
	private String time;   ///创建时间
	private Long uid;  //发布人UID
	private String nickName;  ///发布人昵称
	private Integer ctype; // 联系人类别0：好友，1：关注人
	
	public EventResult(){}

	public EventResult(Long id, String name, Integer type, String time,
			Long uid, String nickName, Integer ctype) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.time = time;
		this.uid = uid;
		this.nickName = nickName;
		this.ctype = ctype;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getCtype() {
		return ctype;
	}

	public void setCtype(Integer ctype) {
		this.ctype = ctype;
	}
	
	
	
}
