package com.wachoo.pangker.server.response;

/**
 * 
 */

public class BoxRes implements java.io.Serializable {

	// Fields

	private Long id;
	private Long userId;
	private Long fromId;
	private String fromName;
	private Long resId;
	private String resName;
	private Integer type;
	private String datetime;
	private String reason;
	private Integer status;
	private String singer;
	private String remark;
	

	// Constructors

	/** default constructor */
	public BoxRes() {
	}

	/** full constructor */
	public BoxRes(Long userId, Long fromId, String fromName, Long resId,
			String resName, Integer type, String datetime, String reason,
			Integer status, String singer, String remark) {
		this.userId = userId;
		this.fromId = fromId;
		this.fromName = fromName;
		this.resId = resId;
		this.resName = resName;
		this.type = type;
		this.datetime = datetime;
		this.reason = reason;
		this.status = status;
		this.singer = singer;
		this.remark = remark;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getFromId() {
		return this.fromId;
	}

	public void setFromId(Long fromId) {
		this.fromId = fromId;
	}

	public String getFromName() {
		return this.fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public Long getResId() {
		return this.resId;
	}

	public void setResId(Long resId) {
		this.resId = resId;
	}

	public String getResName() {
		return this.resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getDatetime() {
		return this.datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getSinger() {
		return this.singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}