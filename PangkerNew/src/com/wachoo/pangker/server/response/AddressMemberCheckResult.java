package com.wachoo.pangker.server.response;


/**
 * 添加成员校验接口（是否旁客用户）
 * 
 * 接口AddressAddMemberCheck.json
 * 
 * @author zhengjy
 *
 * @version 2012-6-12
 */

public class AddressMemberCheckResult extends BaseResult{

	private Long uid;
	private String nickName;
	private String userName;
	
	
	public AddressMemberCheckResult(){}
	
	


	public AddressMemberCheckResult(Long uid, String nickName, String userName) {
		super();
		this.uid = uid;
		this.nickName = nickName;
		this.userName = userName;
	}




	public Long getUid() {
		return uid;
	}


	public void setUid(Long uid) {
		this.uid = uid;
	}


	public String getNickName() {
		return nickName;
	}


	public void setNickName(String nickName) {
		this.nickName = nickName;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	

	
}
