package com.wachoo.pangker.server.response;

import java.util.List;

@SuppressWarnings("serial")
public class QueryLocationResult extends BaseResult{

	private List<Userlocation> result;
	
	public List<Userlocation> getResult() {
		return result;
	}

	public void setResult(List<Userlocation> result) {
		this.result = result;
	}

}
