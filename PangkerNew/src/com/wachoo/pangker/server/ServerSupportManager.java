package com.wachoo.pangker.server;

import java.util.List;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import android.content.Context;

/**
 * 
 * 后台调用服务器类
 * 
 * 
 * @author wangxin
 * 
 */
public class ServerSupportManager {

	private Context context;
	/** server interface */
	private IUICallBackInterface callBackInterface;

	/**
	 * 
	 * ServerSupportManager
	 * 
	 * @param context
	 * @param callBackInterface
	 */
	public ServerSupportManager(Context context, IUICallBackInterface callBackInterface) {
		this.context = context;
		this.callBackInterface = callBackInterface;

	}

	/**
	 * 
	 * 调用后台接口
	 * 
	 * @param url
	 *            接口地址
	 * @param paras
	 *            请求参数
	 * @param pdShow
	 *            是否需要等待框提示 false,不提示;true提示
	 * @param msg
	 *            显示的消息
	 */
	public void supportRequest(String url, List<Parameter> paras, boolean pdShow, String pdMsg) {
		try {

			SeverSupportTask task = new SeverSupportTask(context, callBackInterface, url, paras, pdShow, pdMsg, 999);
			task.execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * 
	 * 调用后台接口
	 * 
	 * @param url
	 *            接口地址
	 * @param paras
	 *            请求参数
	 * @param pdShow
	 *            是否需要等待框提示 false,不提示;true提示
	 * @param msg
	 *            显示的消息
	 * @param caseKey
	 *            case处理，不同业务区分标识
	 */
	public void supportRequest(String url, List<Parameter> paras, boolean pdShow, String pdMsg, int caseKey) {
		try {
			SeverSupportTask task = new SeverSupportTask(context, callBackInterface, url, paras, pdShow, pdMsg, caseKey);
			task.execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * supportRequest
	 * 
	 * @param url
	 *            调用接口地址
	 * @param paras
	 *            调用参数
	 */
	public void supportRequest(String url, List<Parameter> paras) {
		try {
			SeverSupportTask task = new SeverSupportTask(context, callBackInterface, url, paras, false, "", 999);
			task.execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * 
	 * supportRequest
	 * 
	 * @param url
	 *            调用接口地址
	 * @param paras
	 *            调用参数
	 * @param caseKey
	 *            case处理，不同业务区分标识
	 */
	public void supportRequest(String url, List<Parameter> paras, int caseKey) {
		try {
			SeverSupportTask task = new SeverSupportTask(context, callBackInterface, url, paras, false, "", caseKey);
			task.execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * 
	 * supportRequest
	 * 
	 * @param url
	 *            调用接口地址
	 * @param paras
	 *            调用参数
	 * @param caseKey
	 *            case处理，不同业务区分标识
	 */
	public void supportExpRequest(String url, List<Parameter> paras, int caseKey) {
		new SeverExpSupportTask(context, callBackInterface, url, paras, false, "", caseKey).execute();
	}

	/**
	 * 
	 * supportRequest
	 * 
	 * @param url
	 *            调用接口地址
	 * @param paras
	 *            调用参数
	 * @param caseKey
	 *            case处理，不同业务区分标识
	 */
	public void supportExpRequest(String url, List<Parameter> paras, boolean pdShow, String pdMessage, int caseKey) {
		new SeverExpSupportTask(context, callBackInterface, url, paras, pdShow, pdMessage, caseKey).execute();
	}

	/**
	 * 文件上传
	 * 
	 * @param url
	 * @param paras
	 * @param file
	 * @param fileName
	 * @param pdShow
	 * @param mess
	 * @param caseKey
	 *            void
	 */
	public void supportRequest(String url, List<Parameter> paras, MyFilePart uploader, String fileName, boolean pdShow,
			String mess, int caseKey) {
		try {
			SeverSupportResTask task = new SeverSupportResTask(context, callBackInterface, url, paras, uploader,
					fileName, pdShow, mess, caseKey);

			task.execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * 文件上传
	 * 
	 * @param url
	 * @param paras
	 * @param file
	 * @param fileName
	 * @param pdShow
	 * @param mess
	 * @param caseKey
	 *            void
	 */
	public void supportRequest(String url, List<Parameter> paras, MyFilePart uploader, byte[] byteCover, int caseKey) {
		try {
			SeverSupportTResByteTask task = new SeverSupportTResByteTask(context, callBackInterface, url, paras,
					uploader, byteCover, caseKey);
			task.execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
