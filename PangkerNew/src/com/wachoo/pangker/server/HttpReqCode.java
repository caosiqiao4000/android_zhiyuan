package com.wachoo.pangker.server;

public enum HttpReqCode {
	canceled, // 取消
	no_network, // 无网络
	sucess, // 访问成功
	error // 访问失败，异常发生
}
