package com.wachoo.pangker.server;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import android.content.Context;

import com.wachoo.pangker.entity.UserItem;

/**
 * 
 * AsycnLoadDataManager 异步加载用户类
 * 
 * @author wangxin
 * 
 */
public class AsycnLoadDataManager {
	// 起始页码
	int current = 0;
	// 异步显示的总数，暂时限制为50
	int total = 74;
	// 间隔页码
	static final int next = 10;

	// Context context
	private Context context;

	// so that we have a way to communicate with the caller
	ILoadDataResponse responseInterface;

	private SeverSupportTask supportTask;

	// 判断是否在loading
	private boolean loading = false;

	// 判断是否在loading
	public boolean isLoading() {
		return loading;
	}
	private IUICallBackInterface callBackInterface = new IUICallBackInterface() {

		@Override
		public void uiCallBack(Object supportResponse,int caseKey) {
			
			ArrayList<UserItem> items = new ArrayList<UserItem>();
			for (int i = current; i < ((current + next < total) ? current + next : total - current); i++) {
				/*UserInfo item = new UserInfo();
				item.setAcount("10086");
				item.setUserId(i+"");
				item.setUserName(i+"");
				items.add(item);*/
				UserItem item = new UserItem();
				item.setDistance(300+"");
				item.setUserName("昵称");
				item.setSex("1");
				item.setSign("这是签名。。。");
				items.add(item);
			}
			// mark the number of the data we have generated
			current = ((current + next) < total ? current + next : total);
			loading = false;
			responseInterface.onLoadDataComplete(items, total, current);
		}
	};

	public AsycnLoadDataManager(Context context, ILoadDataResponse response) {
		this.context = context;
		this.responseInterface = response;
	}

	/**
	 * LoadNextData
	 * 
	 * @param url
	 * @param paras
	 */
	public void LoadNext(String url, List<Parameter> paras) {
		loading = true;
		paras.add(new Parameter("limit", "[" + 1 + "," + 1 + "]"));
		supportTask = new SeverSupportTask(context, callBackInterface, url, paras, false, null,999);
		supportTask.execute(paras);
	}

	/**
	 * 
	 * ILoadDataResponse
	 * 
	 * @author wangxin
	 * 
	 */
	public interface ILoadDataResponse {
		// invoke this method when everything goes right;
		public void onLoadDataComplete(ArrayList<UserItem> nextItems, int total, int current);

		// tell the caller that ,something wrong happened.
		public void onLoadDataError(String errorMsg);
	}
}
