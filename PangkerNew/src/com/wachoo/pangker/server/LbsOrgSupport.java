package com.wachoo.pangker.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.api.PangkerException;
import com.wachoo.pangker.map.poi.DomParser;
import com.wachoo.pangker.map.poi.Geo_Search_Request;
import com.wachoo.pangker.map.poi.Geo_Search_Response;
import com.wachoo.pangker.map.poi.Poi_Search_Request;
import com.wachoo.pangker.map.poi.Poi_Search_Response;

/**
 * 驴博士实现类
 * @author wangxin
 *
 */
public class LbsOrgSupport implements ILbsOrgSupport {
	
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	
	@Override
	public Geo_Search_Response GeoSearch(Geo_Search_Request request) throws PangkerException {
		String response="";
		try {
			response = httppost(Configuration.getOrgGeoSearchURL(), request.getLbsSearchReques());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
		}
		return DomParser.GeoFromXML(response);
	}

	@Override
	public Poi_Search_Response PoiSearch(Poi_Search_Request request) throws PangkerException {
		// TODO Auto-generated method stub
		String response="";
		try {
			response = httppost(Configuration.getOrgPoiSearchURL(), request.getLbsSearchReques());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
		}
		return DomParser.FromXML(response);
	}
	
	private String httppost(String path, String xml) throws Exception {
		// TODO Auto-generated method stub
		byte[] data = xml.getBytes("UTF-8");// 得到了xml的实体数据
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setConnectTimeout(5000);
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
		conn.setRequestProperty("Content-Length", String.valueOf(data.length));
		OutputStream outStream = conn.getOutputStream();
		outStream.write(data);
		outStream.flush();
		outStream.close();
		
		BufferedReader buffReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		StringBuffer strBuff = new StringBuffer();
		String result = null;
		while ((result = buffReader.readLine()) != null) {
			strBuff.append(result);
		}
		
		return strBuff.toString();
		
	}

}
