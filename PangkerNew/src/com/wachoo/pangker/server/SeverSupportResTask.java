package com.wachoo.pangker.server;

import java.lang.ref.WeakReference;
import java.util.List;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.ui.dialog.WaitingDialog;
import com.wachoo.pangker.util.ConnectivityHelper;
import com.wachoo.pangker.util.Util;

/**
 * 
 * 异步刷新ui类
 * 
 * @author wangxin
 * 
 */
public class SeverSupportResTask extends AsyncTask<Object, Integer, Object> {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private WeakReference<Context> mContextReference;
	private WeakReference<IUICallBackInterface> callBackInterfaceReference;
	// request url
	private String url;
	// Parameter
	private List<Parameter> paras;
	// pdShow isShow ?
	private boolean pdShow;
	// ProgressDialog show message
	private String pdMsg;
	// ProgressDialog
	private WaitingDialog popupDialog;
	private MyFilePart uploader;

	private String fileName;
	private int caseKey;
	protected SyncHttpClient http = new SyncHttpClient();
	// 文件总大小
	private long totalSize;

	private PangkerApplication application;

	// 字符集
	private static final String CONTENT_CHARSET = "UTF-8";

	@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub
		// >>>>>>>>>add by wangxin 弱引用防止回收！
		if (isCancelled() || mContextReference.get() == null
				|| callBackInterfaceReference.get() == null) {
			return HttpReqCode.canceled;
		}

		if (!ConnectivityHelper
				.isConnectivityAvailable(mContextReference.get())) {
			return HttpReqCode.no_network;
		}

		try {
			return http.httpPostWithFile(url, paras, fileName, uploader,
					application.getServerTime());
		} catch (Exception e) {
			logger.error(TAG, e);
			return HttpReqCode.error;
		}

	}

	/***
	 * 
	 * SeverSupportTask
	 * 
	 * @param context
	 * @param pdShow
	 * @param paras
	 * @param url
	 * @param callBackInterface
	 */
	public SeverSupportResTask(Context context,
			IUICallBackInterface callBackInterface, String url,
			List<Parameter> paras2, MyFilePart uploader, String fileName,
			boolean pdShow, String pdMsg, int caseKey) {
		this.application = (PangkerApplication) context.getApplicationContext();
		this.mContextReference = new WeakReference<Context>(context);
		this.callBackInterfaceReference = new WeakReference<IUICallBackInterface>(
				callBackInterface);
		this.url = url;
		this.paras = paras2;
		this.pdShow = pdShow;
		this.pdMsg = pdMsg;
		this.caseKey = caseKey;
		this.uploader = uploader;
		this.fileName = fileName;
	}

	@Override
	protected void onPostExecute(Object result) {
		// Log.i(tag, "result==" + (String) result);
		if (isCancelled() || mContextReference.get() == null
				|| callBackInterfaceReference.get() == null) {
			return;
		}
		if (pdShow) {
			popupDialog.dismiss();
		}
		if (callBackInterfaceReference.get() != null && !isCancelled()) {
			callBackInterfaceReference.get().uiCallBack(result, this.caseKey);
		}
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (isCancelled() || mContextReference.get() == null
				|| callBackInterfaceReference.get() == null)
			return;
		if (pdShow) {
			popupDialog = new WaitingDialog(mContextReference.get());
			if (!Util.isEmpty(pdMsg)) {
				popupDialog.setWaitMessage(pdMsg);
			}
			popupDialog.setCancelable(true);
			popupDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					cancel(true);
				}
			});
			popupDialog.show();
		}
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(progress);
	}
}
