package com.wachoo.pangker.server;

import android.os.AsyncTask;

/**
 * 
 * 用于获取用户在线状态
 * 
 * @author zhengjy
 * 
 */
@SuppressWarnings("unchecked")
public class UserOnlineTask extends AsyncTask {

	private OnlineInterface onlineInterface;
	private String currUserId;

	@Override
	protected Object doInBackground(Object... params) {

		// return PresenceManager.IsUserOnLine(currUserId);
		return null;
	}

	public UserOnlineTask(String userId, OnlineInterface onlineInterface) {
		this.currUserId = userId;
		this.onlineInterface = onlineInterface;
	}

	@Override
	protected void onPostExecute(Object result) {
		int status = (Integer) result;
		onlineInterface.isOnline(status);
	}

	@Override
	protected void onPreExecute() {

	}

	@Override
	protected void onProgressUpdate(Object... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}

	public interface OnlineInterface {
		public void isOnline(int status);
	}
}
