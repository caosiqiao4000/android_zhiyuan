package com.wachoo.pangker.server;

import java.util.List;

import mobile.http.Parameter;
import android.content.Context;

/**
 * 
 * AsycnLoadAskManager 异步加载ListView数据类
 * 
 * @author wangxin
 * 
 */
public class AsycnLoadListDataManager {
	// 起始页码
	private static int current = 0;
	// 异步显示的总数
	private int total = 50;
	// 间隔页码
	private final int next = 2;

	// Context context
	private Context context;

	// so that we have a way to communicate with the caller
	ILoadListDataResponse responseInterface;

	private SeverSupportTask supportTask;

	// 判断是否在loading
	private boolean loading = false;

	// 判断是否在loading
	public boolean isLoading() {
		return loading;
	}

	public AsycnLoadListDataManager(Context context, ILoadListDataResponse response) {
		this.context = context;
		this.responseInterface = response;
	}

	/**
	 * LoadNextData
	 * 
	 * @param url
	 * @param paras
	 */
	public void LoadNext(String url, List<Parameter> paras, int total,int caseKey) {
		//this.total = total;
		loading = true;
		paras.add(new Parameter("limit", current + "," +(next + current)));
		supportTask = new SeverSupportTask(context, callBackInterface, url, paras, false, null,caseKey);
		supportTask.execute(paras);
	}
	
	private IUICallBackInterface callBackInterface = new IUICallBackInterface() {

		@Override
		public void uiCallBack(Object response,int caseKey) {
			if(response !=null){
				current = ((current + next) < total ? current + next : total);
				loading = false;
				responseInterface.onLoadDataComplete(response,current,caseKey);
			}
			else responseInterface.onLoadDataError("Load data fail !");
		}
	};

	/**
	 * 
	 * ILoadDataResponse
	 * 
	 * @author wangxin
	 * 
	 */
	public interface ILoadListDataResponse {
		// invoke this method when everything goes right;
		public void onLoadDataComplete(Object response,int current,int caseKey);

		// tell the caller that ,something wrong happened.
		public void onLoadDataError(String errorMsg);
	}
}
