package com.wachoo.pangker.service;

import android.app.Service;
import android.util.Log;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.IDownloadDao;
import com.wachoo.pangker.db.impl.DownloadDaoImpl;
import com.wachoo.pangker.downupload.DownLoadManager;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.receiver.NetChangeReceiver;
import com.wachoo.pangker.receiver.NetChangeReceiver.OnNetChangeListener;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.util.ConnectivityHelper;
import com.wachoo.pangker.util.SharedPreferencesUtil;

public class DownloadjobService implements OnNetChangeListener {

	private static final String TAG = "DownloadjobService";
	private NetChangeReceiver mChangeReceiver;
	private Service mService;
	private DownLoadManager mDownLoadManager;
	private IDownloadDao mdDownloadDao;
	private SharedPreferencesUtil preferencesUtil;
	private String mCurrentUserID;
	private MessageTipDialog netDialog;

	@Override
	public void onNetStatus(boolean connect) {
		// TODO Auto-generated method stub
		if (connect) {
			// 检测网络ok，进行上传未上传的任务
			for (DownloadJob job : mDownLoadManager.getmDownloadJobs()) {
				if (job.getDownloadType() == DownloadJob.DOWNLOAD_WIAT) {
					dealDownloadJOB(job, DownloadJob.DOWNLOAD_WIAT);
				}
			}
		}
	}

	public DownloadjobService() {
		super();

	}

	public void bindService(Service mService) {
		// TODO Auto-generated method stub
		this.mService = mService;
		this.mChangeReceiver = new NetChangeReceiver(); // >>>>>>>>上传下载管理器
		this.mDownLoadManager = ((PangkerApplication) mService.getApplication()).getDownLoadManager();
		this.mChangeReceiver.registerNetChangeReceiver(mService, this);
		this.mdDownloadDao = new DownloadDaoImpl(mService);
		this.preferencesUtil = new SharedPreferencesUtil(mService);
		this.mCurrentUserID = ((PangkerApplication) mService.getApplication()).getMyUserID();
	}

	public void destory() {
		// TODO Auto-generated method stub
		mService.unregisterReceiver(mChangeReceiver);
		mChangeReceiver = null;
	}

	/**
	 * 上传处理的唯一入口
	 * 
	 * @param job
	 */
	public void dealDownloadJob(int jobID) {
		// >>>>>>>>>>>判断jobID是否有效
		if (jobID > 0) {
			// >>>>>>>>>>>>>获取job任务
			DownloadJob downloadJob = mDownLoadManager.getDownloadJobByID(jobID);
			if (downloadJob != null) {
				// >>>>>>>>>>>开始下载文件
				dealDownloadJOB(downloadJob, DownloadJob.DOWNLOAD_DIRECT);
			}
		}
	}

	private void dealDownloadJOB(final DownloadJob downloadJob, final int downloadCode) {

		// >>>>>>>>仅仅wifi下载
		if (preferencesUtil.getInt(PangkerConstant.SP_DOWNLOAD_SET_NTE + mCurrentUserID,
				DownloadJob.DOWNLOAD_NET_MOBILE_NOTICE) == DownloadJob.DOWNLOAD_NET_ONLY_WIFI) {
			if (ConnectivityHelper.getAPNType(mService) != 1) {
				intoWaitDownJobsQuee(downloadJob);
			} else {
				startDownload(downloadJob, downloadCode);
			}
		}
		// >>>>>>>>移动网络提示下载
		else if (preferencesUtil.getInt(PangkerConstant.SP_DOWNLOAD_SET_NTE + mCurrentUserID,
				DownloadJob.DOWNLOAD_NET_MOBILE_NOTICE) == DownloadJob.DOWNLOAD_NET_MOBILE_NOTICE) {
			if (ConnectivityHelper.getAPNType(mService) != 1) {

				netDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-generated method stub
						if (isSubmit) {
							// >>>>>>>开始下载
							startDownload(downloadJob, downloadCode);
						} else {
							// >>>>>>>>>等待wifi网络下载
							intoWaitDownJobsQuee(downloadJob);
						}
					}
				}, mService);

				netDialog.showDialog("", mService.getResources().getString(R.string.net_prompt_download)
						+ "\"" + downloadJob.getFileName() + "\"?");
			} else {
				startDownload(downloadJob, downloadCode);
			}
		}
		// >>>>>>>>无论什么网络都下载
		else {
			startDownload(downloadJob, downloadCode);
		}
	}

	private void intoWaitDownJobsQuee(DownloadJob downloadJob) {
		// TODO Auto-generated method stub
		Log.d(TAG, "intoWaitDownJobsQuee-->");

		showToast("\"" + downloadJob.getFileName() + "\"已存放到待下载列表,有wifi网络时自动下载!");

		downloadJob.setDownloadType(DownloadJob.DOWNLOAD_WIAT);
		mdDownloadDao.updataDownloadInfo(downloadJob);
		mDownLoadManager.addDownloadJob(downloadJob);
	}

	private void startDownload(DownloadJob downloadJob, int downloadCode) {
		// TODO Auto-generated method stub
		// >>>>>>>设置文件开始下载标记
		if (downloadCode == 10)
			showToast("\"" + downloadJob.getFileName() + "\"开始下载!");
		else if (downloadCode == 11)
			showToast("当前是wifi网络,待下载项目\"" + downloadJob.getFileName() + "\"开始下载!");

		downloadJob.setDownloadType(DownloadJob.DOWNLOAD_DIRECT);
		mdDownloadDao.updataDownloadInfo(downloadJob);
		downloadJob.setStatus(DownloadJob.DOWNLOADING);
		// >>>>>更新标记到db
		mdDownloadDao.updataDownloadStatus(downloadJob.getId(), DownloadJob.DOWNLOADING);
		// >>>>>>>初始化下载任务
		downloadJob.initDownload(this.mService);
		downloadJob.startDownJob();
	}

	public void showToast(final int toastText) {
		Toast.makeText(mService, toastText, Toast.LENGTH_SHORT).show();
	}

	public void showToast(String toastText) {
		Toast.makeText(mService, toastText, Toast.LENGTH_SHORT).show();
	}
}
