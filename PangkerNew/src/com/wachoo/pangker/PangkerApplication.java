package com.wachoo.pangker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mobile.http.MyFilePart;
import mobile.json.JSONUtil;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import com.wachoo.pangker.activity.group.ChatRoomListActivity;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.api.IAddressBookBind;
import com.wachoo.pangker.api.loadMatchLocalAddressBookThread;
import com.wachoo.pangker.chat.IGroupSPLoginManager;
import com.wachoo.pangker.chat.ILoggedChecker;
import com.wachoo.pangker.chat.MessageBroadcast;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.BlacklistDaoImpl;
import com.wachoo.pangker.db.impl.ContactsGroupDaoIpml;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.db.impl.MsgInfoDaoImpl;
import com.wachoo.pangker.db.impl.NetAddressBookDaoImpl;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.downupload.DownLoadManager;
import com.wachoo.pangker.downupload.MsgDownloader;
import com.wachoo.pangker.downupload.UpLoadManager;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.PlayingMusiclist;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.ResShowObject;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.gestures.GesturesHandler;
import com.wachoo.pangker.group.P2PUserInfo;
import com.wachoo.pangker.map.app.LocateManager;
import com.wachoo.pangker.music.Lyric;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.util.MapUtil;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * 
 * 当前用户的信息
 * 
 * @author wangxin
 * 
 */
public class PangkerApplication extends Application {

	private String TAG = "PangkerApplication";// log tag

	private SharedPreferencesUtil preferencesUtil;
	/** 漂移处理 **/

	private int LocateType = -1;// >>>>定位方式 0：终端定位 1：漂移到指定用户 2：漂移到指定的地点 3：穿越

	private List<UserItem> friendList = null;// >>>>>>>> 好友的用户数据
	private List<ContactGroup> friendGroup = null;// >>>>>>>>好友分组
	private List<ContactGroup> attentGroup = null;// >>>>>>>>关注人
	private List<UserItem> attentList = null; // >>>>>>>>关注人
	private List<UserItem> blackList = null; // >>>>>>>>黑名单用户
	private List<UserItem> traceList = new ArrayList<UserItem>(); // >>>>>>>>
																	// 黑名单用户
	private List<AddressBooks> netAddressBooks = null;// 单位通讯录

	private List<DirInfo> rootDirs = null;// >>>>>>>>>>>>>资源根目录
	/** 目录结构包括网络的和本地的 **/
	private Map<Integer, List<DirInfo>> localDirs = null;// 本地选择的所有资源

	private GesturesHandler gesturesHandler = null;// >>>>>>>>手势滑动

	/** 手机本地通讯录 */
	private boolean isLocalContactsInit = false;// >>>>>>>>>>
	private boolean isLocalContactsStratInit = false;// >>>>>>>>>>

	private List<LocalContacts> localContacts = null;// 通讯录的用户数据
	private HashMap<String, Integer> alphaIndexer;// 存放存在的汉语拼音首字母和与之对应的列表位置
	private String[] sections;// 存放存在的汉语拼音首字母

	private Lyric currentLyric = null;// >>>>>>>歌词文件提供一个本地地址给对象，保存起来

	// >>>>>>>当前用户用户信息
	private UserInfo mySelf = null;

	private OpenfireManager openfireManager = null;// >>>>>openfire im管理器
	private MessageBroadcast msgInfoBroadcast = null;// >>>>>>>消息管理器

	// >>>>>>>>>应用未读消息计数器
	private Integer unReadMessageCount = null;

	private boolean mExternalStorageAvailable = false; // sdcard可用状态

	private boolean mExternalStorageWriteable = false; // sdcard可写状态

	private boolean mExternalStorageSetting = false; // >>>>>>>是否可用设置标记

	/** 私信发送文件相关存储 */
	private Map<String, MyFilePart> filePartMap = new HashMap<String, MyFilePart>();// >>>>>>>>>>>私信发送文件存放容器
	private Map<String, MsgDownloader> msgDownloaderMap = new HashMap<String, MsgDownloader>();// >>>>>>>>>私信下载文件存放容器

	// >>>>>>>>>>>登录通知
	private ILoggedChecker iLoggedChecker;

	/** 目录结构包括网络的和本地的 **/
	// 保存本地音乐列表
	private PlayingMusiclist playingMusiclist = new PlayingMusiclist();

	public int playMusicIndex = -1; // >>>>>>>>>>>播放音乐 位置

	private ResShowObject resShowObject = new ResShowObject();

	private Location currentLocation = null;// 用户的当前位置
	private Location dirftLocation = null;// 用户漂移的位置

	public ChatRoomListActivity chatRoomListActivity;

	public ChatRoomMainActivity chatRoomMainActivity;

	// >>>>>>>>>漂移对象保存
	private UserItem driftUser;// 漂移对象
	/**
	 * // 1：是； 0:否，记录是否通过切换账号登陆，用于旁客账号设置提示，add
	 */
	private int isLoginByFingerprint = 0;

	// >>>>>>>>>>>邀请用户加入群组，带邀请用户（单位通讯录）
	private List<UserItem> jion2Membsers = null;

	// >>>>>>>>>当前查看的资源index
	private int viewIndex = -1;

	public int getViewIndex() {
		if (this.viewIndex == -1)
			this.viewIndex = preferencesUtil.getInt(PangkerConstant.PANGKER_VIEW_RESINDEX_KEY, 0);
		return viewIndex;
	}

	public void setViewIndex(int viewIndex) {
		this.viewIndex = viewIndex;
		preferencesUtil.saveInt(PangkerConstant.PANGKER_VIEW_RESINDEX_KEY, this.viewIndex);
	}

	public int viewIndexMinus() {
		this.viewIndex = getViewIndex() - 1;

		return viewIndex;
	}

	public int viewIndexAdd() {
		this.viewIndex = getViewIndex() + 1;

		return viewIndex;
	}

	public List<UserItem> getJion2Membsers() {
		return jion2Membsers;
	}

	public void setJion2Membsers(List<UserItem> jion2Membsers) {
		this.jion2Membsers = jion2Membsers;
	}

	public int getUnReadMessageCount() {
		if (unReadMessageCount == null) {
			unReadMessageCount = new MsgInfoDaoImpl(this).getUnreadCount(getMySelf().getUserId());
		}
		return unReadMessageCount;
	}

	public void setUnReadMessageCount(int unReadMessageCount) {
		this.unReadMessageCount = unReadMessageCount;
	}

	public void addUnReadMessage() {
		this.unReadMessageCount = getUnReadMessageCount();
		this.unReadMessageCount++;
	}

	public void removeUnReadMessage(int removeCount) {
		this.unReadMessageCount = getUnReadMessageCount();
		this.unReadMessageCount = this.unReadMessageCount - removeCount;
		if (this.unReadMessageCount < 0)
			this.unReadMessageCount = 0;
	}

	private void checkExternalStrorage() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			setmExternalStorageAvailable(true);
			setmExternalStorageWriteable(true);
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			setmExternalStorageAvailable(true);
			setmExternalStorageWriteable(false);
		} else {
			setmExternalStorageAvailable(false);
			setmExternalStorageWriteable(false);
		}
		setmExternalStorageSetting(true);
	}

	public boolean ismExternalStorageSetting() {
		return mExternalStorageSetting;
	}

	public void setmExternalStorageSetting(boolean mExternalStorageSetting) {
		this.mExternalStorageSetting = mExternalStorageSetting;
	}

	public boolean ismExternalStorageAvailable() {
		if (!ismExternalStorageSetting()) {
			checkExternalStrorage();
		}
		return mExternalStorageAvailable;
	}

	public void setmExternalStorageAvailable(boolean mExternalStorageAvailable) {
		this.mExternalStorageAvailable = mExternalStorageAvailable;
	}

	public boolean ismExternalStorageWriteable() {
		if (!ismExternalStorageSetting()) {
			checkExternalStrorage();
		}
		return mExternalStorageWriteable;
	}

	public void setmExternalStorageWriteable(boolean mExternalStorageWriteable) {
		this.mExternalStorageWriteable = mExternalStorageWriteable;
	}

	/**
	 * 获取openfiremanager
	 * 
	 * @return
	 */
	public OpenfireManager getOpenfireManager() {
		return openfireManager;
	}

	public void setOpenfireManager(OpenfireManager openfireManager) {
		// >>>>>登陆线程启动
		this.openfireManager = openfireManager;
	}

	public MessageBroadcast getMsgInfoBroadcast() {
		if (msgInfoBroadcast == null)
			msgInfoBroadcast = new MessageBroadcast(this);
		return msgInfoBroadcast;
	}

	/**
	 * Gets the {@link XMPPConnection} instance.
	 * 
	 * @return the {@link XMPPConnection} associated with this session.
	 */
	public XMPPConnection getConnection() {
		return openfireManager.getConnection();
	}

	/**
	 * 是否登录到openfire
	 * 
	 * @return
	 * 
	 * @author wangxin 2012-2-21 下午05:54:28
	 */
	public boolean IsLogin() {
		return openfireManager.isLoggedIn();
	}

	/**
	 * 
	 * 获取openfiremanager
	 * 
	 * @return
	 */
	public OpenfireManager getResetOpenfireManager(ConnectionListener connectionListener) {
		if (openfireManager != null) {
			if (openfireManager.getConnection() != null) {
				openfireManager.getConnection().removeConnectionListener(connectionListener);
			}
			openfireManager.loginOut();
			openfireManager.close();
			openfireManager = null;
		}
		openfireManager = new OpenfireManager();
		return openfireManager;
	}

	public boolean ofIsInit() {
		if (openfireManager != null)
			return true;
		return false;
	}

	/**
	 * 资源根目录相关
	 * 
	 * @return
	 */
	public List<DirInfo> getRootDirs() {
		if (rootDirs == null)
			rootDirs = new ResDaoImpl(this).getResByIndex(getMyUserID());
		return rootDirs;
	}

	public void setRootDirs(List<DirInfo> rootDirs) {
		this.rootDirs = rootDirs;
	}

	// >>>> 根据类型获取根目录
	public DirInfo getRootDir(int resType) {
		final Iterator<DirInfo> iter = new ArrayList<DirInfo>(getRootDirs()).iterator();
		while (iter.hasNext()) {
			DirInfo item = (DirInfo) iter.next();
			if (item.getResType() == resType) {
				return item;
			}
		}
		return null;
	}

	public Lyric getCurrentLyric() {
		return currentLyric;
	}

	public void setCurrentLyric(Lyric currentLyric) {
		this.currentLyric = currentLyric;
	}

	/**
	 * 获取本地资源目录
	 * 
	 * @return
	 */
	public Map<Integer, List<DirInfo>> getLocalDirs() {
		if (localDirs == null) {
			localDirs = new HashMap<Integer, List<DirInfo>>();
			IResDao iResDao = new ResDaoImpl(this);
			for (int i = 0; i < PangkerConstant.RES_SUM.length; i++) {
				List<DirInfo> resInfos = iResDao.getDirByType(getMyUserID(), DirInfo.DEAL_SELECT,
						PangkerConstant.RES_SUM[i]);
				putListDirs(PangkerConstant.RES_SUM[i], resInfos);
			}
		}
		return localDirs;
	}

	public ILoggedChecker getiLoggedChecker() {
		return iLoggedChecker;
	}

	public void setiLoggedChecker(ILoggedChecker iLoggedChecker) {
		this.iLoggedChecker = iLoggedChecker;
	}

	// >>>>>>>>>>>登录通知

	public synchronized void putFilePart(String key, MyFilePart filePart) {
		filePartMap.put(key, filePart);
	}

	public MyFilePart getFilePart(String key) {
		return filePartMap.get(key);
	}

	public synchronized void removeFilePart(String key) {
		filePartMap.remove(key);
	}

	public synchronized void putMsgDownloader(String key, MsgDownloader msgDownloader) {
		msgDownloaderMap.put(key, msgDownloader);
	}

	public MsgDownloader getMsgDownloader(String key) {
		return msgDownloaderMap.get(key);
	}

	public synchronized void removeMsgDownloader(String key) {
		msgDownloaderMap.remove(key);
	}

	public MusicInfo getPlayingMusic() {
		if (getPlayMusicIndex() == -1)
			return null;
		if (getPlayMusicIndex() > getMusiList().size())
			return null;
		return getMusiList().get(getPlayMusicIndex());
	}

	public int getPlayMusicIndex() {
		if (playMusicIndex == -1)
			playMusicIndex = preferencesUtil.getInt(PangkerConstant.PANGKER_MUSICPLAY_INEXE_KEY, -1);
		return playMusicIndex;
	}

	public void setPlayMusicIndex(int playMusicIndex) {
		this.playMusicIndex = playMusicIndex;
		preferencesUtil.saveInt(PangkerConstant.PANGKER_MUSICPLAY_INEXE_KEY, playMusicIndex);
	}

	public List<ResShowEntity> getResLists() {

		if (resShowObject.getEntities() == null) {
			String strReslist = preferencesUtil.getString(PangkerConstant.PANGKER_RESLIST_KEY, "");
			resShowObject = JSONUtil.fromJson(strReslist, ResShowObject.class);

			if (resShowObject == null || resShowObject.getEntities() == null)
				return new ArrayList<ResShowEntity>();
		}
		return resShowObject.getEntities();
	}

	public void setResLists(List<ResShowEntity> resLists) {
		resShowObject.setEntities(resLists);
		String strReslist = JSONUtil.toJson(resShowObject, false);
		preferencesUtil.saveString(PangkerConstant.PANGKER_RESLIST_KEY, strReslist);
	}

	// // >>>>>>>>>>>>>>>> add by wangxin 判断是否是当前播放的音乐
	// public boolean isPlayingMusic(int location) {
	// MusicInfo musicInfo = getMusiList().get(location);
	// if (musicInfo == null)
	// return false;
	// if (getPlayingMusic() != null) {
	// if (musicInfo.isNetMusic()) {
	// if (musicInfo.getSid().equals(getPlayingMusic().getSid()))
	// return true;
	// } else {
	// if (musicInfo.getMusicPath().equals(getPlayingMusic().getMusicPath()))
	// return true;
	// }
	//
	// }
	// return false;
	// }

	/**
	 * @return the locateType
	 */
	public int getLocateType() {
		if (this.LocateType == -1) {
			preferencesUtil.getInt(PangkerConstant.PANGKER_LOCATE_TYPE_KEY, 0);
		}
		return LocateType;
	}

	/**
	 * @param locateType
	 *            the locateType to set
	 */
	public void setLocateType(int locateType) {
		LocateType = locateType;
		preferencesUtil.saveInt(PangkerConstant.PANGKER_LOCATE_TYPE_KEY, locateType);
	}

	public List<MusicInfo> getMusiList() {

		if (playingMusiclist.getInfos() == null) {
			String strMusiclist = preferencesUtil.getString(PangkerConstant.PANGKER_MUSICLIST_KEY, "");
			if (Util.isEmpty(strMusiclist))
				return new ArrayList<MusicInfo>();
			playingMusiclist = JSONUtil.fromJson(strMusiclist, PlayingMusiclist.class);

			if (playingMusiclist == null || playingMusiclist.getInfos() == null)
				return new ArrayList<MusicInfo>();
		}
		return playingMusiclist.getInfos();
	}

	public void setMusiList(List<MusicInfo> musiList) {
		this.playingMusiclist.setInfos(musiList);
		preferencesUtil.saveString(PangkerConstant.PANGKER_MUSICLIST_KEY, JSONUtil.toJson(playingMusiclist, false));
	}

	public boolean isBindPhone() {
		return preferencesUtil.getBoolean(PangkerConstant.PANGKER_ISBIND_PHONE_KEY, false);
	}

	public void setBindPhone(boolean isBindPhone) {
		preferencesUtil.saveBoolean(PangkerConstant.PANGKER_ISBIND_PHONE_KEY, isBindPhone);
	}

	public String getImAccount() {
		return getMySelf().getAccount();
	}

	/**
	 * 是否匹配通讯录
	 */
	private boolean isAddressBooksMacth = false;

	public boolean isAddressBooksMacth() {
		return isAddressBooksMacth;
	}

	public void setAddressBooksMacth(boolean isAddressBooksMacth) {
		this.isAddressBooksMacth = isAddressBooksMacth;
	}

	private IGroupSPLoginManager mSpLoginManager;

	public IGroupSPLoginManager getmSpLoginManager() {
		return mSpLoginManager;
	}

	public void setmSpLoginManager(IGroupSPLoginManager mSpLoginManager) {
		this.mSpLoginManager = mSpLoginManager;
	}

	// >>>>>>>>>是否登录到groupSP服务器
	private boolean isGroupSPLogged = false;

	public boolean isGroupSPLogged() {
		return isGroupSPLogged;
	}

	public void setGroupSPLogged(boolean isGroupSPLogged) {
		this.isGroupSPLogged = isGroupSPLogged;
	}

	/**
	 * 资源上传下载管理
	 */

	// >>>>>>>>>>>>>>>>>>>上传文件管理类
	private UpLoadManager upLoadManager;

	public UpLoadManager getUpLoadManager() {
		if (upLoadManager == null)
			upLoadManager = new UpLoadManager(this);
		return upLoadManager;
	}

	// >>>>>>>>>>>>>下载文件管理类
	private DownLoadManager downLoadManager;

	public DownLoadManager getDownLoadManager() {
		if (downLoadManager == null)
			downLoadManager = new DownLoadManager(this);
		return downLoadManager;
	}

	/**
	 * addBy
	 * wangxin----------------------------------------------------------------
	 * -------
	 */
	// -----------------------------------------------------------------------------------
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		preferencesUtil = new SharedPreferencesUtil(this);
		CrashExceptionHandler customException = CrashExceptionHandler.getInstance();
		customException.initApplication(getApplicationContext());

	}

	/**
	 * 当前的群组
	 */
	private UserGroup currunGroup;

	public UserGroup getCurrunGroup() {
		return currunGroup;
	}

	public void setCurrunGroup(UserGroup currunGroup) {
		this.currunGroup = currunGroup;
	}

	private P2PUserInfo mP2pUserInfo;

	public P2PUserInfo getP2PUserInfo() {
		if (mP2pUserInfo == null) {
			String userId = getMyUserID();
			long userid = Long.parseLong(userId);
			UserInfo my = getMySelf();
			mP2pUserInfo = new P2PUserInfo(userid, my.getSex(), 0, "", my.getUserName(), getCurrentLocation()
					.toResString());
		}
		return mP2pUserInfo;
	}

	/**
	 * @return the alphaIndexer
	 */
	public HashMap<String, Integer> getAlphaIndexer() {
		return alphaIndexer;
	}

	/**
	 * @param alphaIndexer
	 *            the alphaIndexer to set
	 */
	public void setAlphaIndexer(HashMap<String, Integer> alphaIndexer) {
		this.alphaIndexer = alphaIndexer;
	}

	/**
	 * @return the sections
	 */
	public String[] getSections() {
		return sections;
	}

	/**
	 * @param sections
	 *            the sections to set
	 */
	public void setSections(String[] sections) {
		this.sections = sections;
	}

	public String getMyUserID() {
		return getMySelf().getUserId();
	}

	public String getMyPassWord() {
		return getMySelf().getPassword();
	}

	public String getMyUserName() {
		return getMySelf().getUserName();
	}

	/**
	 * @return the currentLocation
	 */
	public Location getCurrentLocation() {
		if (currentLocation == null)
			currentLocation = MapUtil.getLastLocation(this);
		return currentLocation;
	}

	/**
	 * @param currentLocation
	 *            the currentLocation to set
	 */
	public void setCurrentLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}

	public Location getDirftLocation() {
		if (this.dirftLocation == null)
			this.dirftLocation = MapUtil.getLastDirftLocation(this);
		return dirftLocation;
	}

	// >>>>>>>>>>如果漂移的话返回漂移位置信息，如果没有漂移返回真实位置信息
	public Location getLogicLocation() {
		if (getLocateType() == PangkerConstant.LOCATE_CLIENT) {
			return getCurrentLocation();
		} else if (getLocateType() == PangkerConstant.LOCATE_DRIFT) {
			return getDirftLocation();
		}
		return null;
	}

	public void setDirftLocation(Location dirftLocation) {

		this.dirftLocation = dirftLocation;
		LocateManager.saveDirftLocation(dirftLocation, this);
	}

	// 本地通讯录匹配功能
	private IAddressBookBind addressBookBind;

	public IAddressBookBind getAddressBookBind() {
		return addressBookBind;
	}

	public void setAddressBookBind(IAddressBookBind addressBookBind) {
		this.addressBookBind = addressBookBind;
	}

	/**
	 * @return the friendGroup
	 */
	public List<ContactGroup> getFriendGroup() {
		if (friendGroup == null) {
			// >>>>>>>获取数据
			friendGroup = new ContactsGroupDaoIpml(this)
					.getMyGroupByType(String.valueOf(PangkerConstant.GROUP_FRIENDS));
		}
		return friendGroup;
	}

	/**
	 * @param friendGroup
	 *            the friendGroup to set
	 */
	public void setFriendGroup(List<ContactGroup> friendGroup) {
		this.friendGroup = friendGroup;
	}

	public String getFriendGroupNameByID(String groupID) {
		for (ContactGroup group : getFriendGroup()) {
			if (groupID.equals(group.getGid()))
				return group.getName();
		}
		return "";
	}

	/**
	 * @return the attentGroup
	 */
	public List<ContactGroup> getAttentGroup() {
		return attentGroup;
	}

	public void add2AttentGroup(UserItem userItem, String groupId) {
		for (ContactGroup item : getAttentGroup()) {
			if (item.getGid().equals(groupId)) {
				item.addUserItem(userItem);
				break;
			}
		}
	}

	public void deleAttentGroupUser(UserItem userItem, String groupId) {
		for (ContactGroup item : getAttentGroup()) {
			if (item.getGid().equals(groupId)) {
				item.removeUserItem(userItem.getUserId());
				break;
			}
		}
	}

	/**
	 * @param attentGroup
	 *            the attentGroup to set
	 */
	public void setAttentGroup(List<ContactGroup> attentGroup) {
		this.attentGroup = attentGroup;
	}

	/**
	 * @return the mySelf
	 */
	public UserInfo getMySelf() {
		if (mySelf == null)
			mySelf = Util.readHomeUser(this);
		return mySelf;
	}

	/**
	 * @param mySelf
	 *            the mySelf to set 修改了MySelf对象，对应的要修改和MySelf相关的对象，如群组的
	 */
	public void setMySelf(UserInfo mySelf) {
		this.mySelf = mySelf;
		mP2pUserInfo = null;
	}

	/**
	 * @return the friendList
	 */
	public List<UserItem> getFriendList() {
		if (friendList == null) {
			friendList = new FriendsDaoImpl(this).getAllFriendsByUID(getMyUserID());
		}
		return friendList;
	}

	/**
	 * @param friendList
	 *            the friendList to set
	 */
	public void setFriendList(List<UserItem> friendList) {
		if (friendList != null)
			this.friendList = friendList;
	}

	/**
	 * @return the localContacts
	 */
	public List<LocalContacts> getLocalContacts() {
		return localContacts;
	}

	/**
	 * @param localContacts
	 *            the localContacts to set
	 */
	public void setLocalContacts(List<LocalContacts> localContacts) {
		this.localContacts = localContacts;
	}

	public boolean isLocalContactsInit() {
		if (!isLocalContactsInit && !isLocalContactsStratInit) {
			isLocalContactsStratInit = true;
			new loadMatchLocalAddressBookThread(this).start();
		}
		return isLocalContactsInit;
	}

	public void setLocalContactsInit(boolean isLocalContactsInit) {
		this.isLocalContactsInit = isLocalContactsInit;
	}

	public LocalContacts getLocalContactByPhone(String phoneNum) {
		final Iterator<LocalContacts> iter = new ArrayList<LocalContacts>(getLocalContacts()).iterator();
		while (iter.hasNext()) {
			LocalContacts item = (LocalContacts) iter.next();
			if (item.getPhoneNum().equals(phoneNum)) {
				return item;
			}
		}
		return null;
	}

	public String getLocalContactNameByPhone(String phoneNum) {
		final Iterator<LocalContacts> iter = new ArrayList<LocalContacts>(localContacts).iterator();
		while (iter.hasNext()) {
			LocalContacts item = (LocalContacts) iter.next();
			if (item.getPhoneNum() != null && item.getPhoneNum().equals(phoneNum)) {
				return item.getUserName();
			}
		}
		return "";
	}

	/**
	 * 
	 * 添加 modify by lb
	 * 
	 * @param user
	 * @param groupid
	 * 
	 * @author wangxin 2012-2-19 上午11:39:21
	 */
	public void addFriend(UserItem user, String groupid) {
		if (groupid.equals(PangkerConstant.DEFAULT_GROUPID)) {// 是否是默认组
			if (!IsFriend(user.getUserId())) {
				this.getFriendList().add(user);
			}
		}
		for (ContactGroup group : this.friendGroup) {
			if (group.getGid().equals(groupid)) {
				group.addUserItem(user);
			}
		}
	}

	/**
	 * removeFriend
	 * 
	 * @param userid
	 * @param groupid
	 * 
	 * @author wangxin 2012-2-20 上午10:26:15
	 */
	public void removeFriend(String userid, String groupid) {
		if (groupid.equals(PangkerConstant.DEFAULT_GROUPID)) {
			// 从好友列表删除
			removeFriend(userid);
			// 从分组中删除好友
			for (ContactGroup group : this.friendGroup) {
				group.removeUserItem(userid);
				group.setCount(group.getCount() - 1);
			}
		} else {
			for (ContactGroup group : this.friendGroup) {
				if (groupid.equals(group.getGid())) {
					group.removeUserItem(userid);
					group.setCount(group.getCount() - 1);
				}
			}
		}
	}

	/**
	 * 删除好友
	 * 
	 * @param userid
	 * 
	 * @author wangxin
	 * @date 2012-2-22 下午05:48:54
	 */
	private void removeFriend(String userid) {
		final Iterator<UserItem> iter = new ArrayList<UserItem>(getFriendList()).iterator();
		while (iter.hasNext()) {
			UserItem item = iter.next();
			if (item.getUserId().equals(userid)) {
				this.getFriendList().remove(item);
				break;
			}
		}
	}

	/**
	 * 判断时候是好友，添加好友时判断
	 * 
	 * @param userid
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-22 下午02:55:49
	 */
	public boolean IsFriend(String userid) {
		final Iterator<UserItem> iter = new ArrayList<UserItem>(getFriendList()).iterator();
		while (iter.hasNext()) {
			UserItem item = iter.next();
			if (item.getUserId().equals(userid)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param group
	 * @param groupTraces
	 * 
	 * @author wangxin
	 * @date 2012-2-23 下午03:34:31
	 */
	public void addContactsGroup(ContactGroup group) {
		if (!IsGroupExist(group.getGid(), group.getType())) {
			if (group.getType() == PangkerConstant.GROUP_FRIENDS) {
				this.friendGroup.add(group);
			} else if (group.getType() == PangkerConstant.GROUP_ATTENTS) {
				this.attentGroup.add(group);
			}
		}
	}

	public void removeContactsGroup(ContactGroup group) {
		if (IsGroupExist(group.getGid(), group.getType())) {
			if (group.getType() == PangkerConstant.GROUP_FRIENDS) {
				final Iterator<ContactGroup> iter = new ArrayList<ContactGroup>(friendGroup).iterator();
				while (iter.hasNext()) {
					ContactGroup item = iter.next();
					if (item.getGid().equals(group.getGid())) {
						this.friendGroup.remove(item);
						break;
					}
				}
				// this.friendGroup.remove(group);
			} else if (group.getType() == PangkerConstant.GROUP_ATTENTS) {
				// this.attentGroup.remove(group);
				final Iterator<ContactGroup> iter = new ArrayList<ContactGroup>(attentGroup).iterator();
				while (iter.hasNext()) {
					ContactGroup item = iter.next();
					if (item.getGid().equals(group.getGid())) {
						this.attentGroup.remove(item);
						break;
					}
				}
			}
		}
	}

	/**
	 * 修改联系人分组名称
	 * 
	 * @param group
	 */
	public void updateContactsGroup(ContactGroup group) {
		if (IsGroupExist(group.getGid(), group.getType())) {
			if (group.getType() == PangkerConstant.GROUP_FRIENDS) {
				for (ContactGroup newGroup : friendGroup) {
					if (newGroup.getGid().equals(group.getGid())) {
						newGroup.setName(group.getName());
					}
				}
			} else if (group.getType() == PangkerConstant.GROUP_ATTENTS) {
				for (ContactGroup newGroup : attentGroup) {
					if (newGroup.getGid().equals(group.getGid())) {
						newGroup.setName(group.getName());
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param CurrunGroup
	 * @param grouptype
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-23 下午03:35:21
	 */
	public boolean IsGroupExist(String groupid, int grouptype) {
		if (grouptype == PangkerConstant.GROUP_FRIENDS) {
			return IsFriendGroupExist(groupid);
		} else if (grouptype == PangkerConstant.GROUP_ATTENTS) {
			return IsAttentGroupExist(groupid);
		}
		return false;
	}

	/**
	 * 
	 * @param groupid
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-23 下午03:37:02
	 */
	public boolean IsFriendGroupExist(String groupid) {
		final Iterator<ContactGroup> iter = new ArrayList<ContactGroup>(this.friendGroup).iterator();
		while (iter.hasNext()) {
			ContactGroup group = iter.next();
			if (group.getGid().equals(groupid)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param groupid
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-23 下午03:37:02
	 */
	public boolean IsAttentGroupExist(String groupid) {
		final Iterator<ContactGroup> iter = new ArrayList<ContactGroup>(this.attentGroup).iterator();
		while (iter.hasNext()) {
			ContactGroup group = iter.next();
			if (group.getGid().equals(groupid)) {
				return true;
			}
		}
		return false;
	}

	// >>>>>>>>>>更新状态
	public void notifyUserInfo(UserItem userItem) {
		final Iterator<ContactGroup> iter = new ArrayList<ContactGroup>(this.friendGroup).iterator();
		while (iter.hasNext()) {
			ContactGroup group = iter.next();
			// group.getClientgid()
		}

	}

	public void putListDirs(Integer type, List<DirInfo> dirInfos) {
		getLocalDirs().put(type, dirInfos);
	}

	public void putListDir(Integer type, DirInfo dirInfo) {
		Log.i("pangkerapp", "put -- res");
		getLocalDirs().get(type).add(0, dirInfo);
	}

	public List<DirInfo> getResDirInfos(Integer type) {
		return getLocalDirs().get(type);
	}

	/**
	 * 黑名单相关
	 */
	public boolean IsBlacklistExist(String userid) {
		final Iterator<UserItem> iter = getBlackList().iterator();
		while (iter.hasNext()) {
			UserItem user = (UserItem) iter.next();
			if (user.getUserId().equals(userid)) {
				return true;
			}
		}
		return false;
	}

	public List<UserItem> getBlackList() {
		if (blackList == null)
			blackList = new BlacklistDaoImpl(this).getBlacklist();
		return blackList;
	}

	public void setBlackList(List<UserItem> blackList) {
		this.blackList = blackList;
	}

	public List<UserItem> getTraceList() {
		if (traceList == null)
			traceList = new FriendsDaoImpl(this).getTracelist(getMyUserID());
		return traceList;
	}

	public void setTraceList(List<UserItem> traceList) {
		this.traceList = traceList;
	}

	/**
	 * 移除黑名单
	 * 
	 */
	public void removeBlacklist(String userid) {
		for (int i = getBlackList().size() - 1; i >= 0; i--) {
			UserItem item = getBlackList().get(i);
			if (item.getUserId().equals(userid)) {
				getBlackList().remove(i);
				break;
			}
		}
	}

	/**
	 * 添加黑名单
	 */
	public void addBlacklist(UserItem user) {
		if (!IsBlacklistExist(user.getUserId())) {
			this.getBlackList().add(user);
		}
	}

	/**
	 * 是否存在友踪组列表
	 */
	public boolean IsTracelistExist(String userid) {
		final Iterator<UserItem> iter = new ArrayList<UserItem>(getTraceList()).iterator();
		while (iter.hasNext()) {
			UserItem user = iter.next();
			if (user.getUserId().equals(userid)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 移除黑名单
	 * 
	 */
	public void removeTraceklist(String userid) {
		for (int i = getTraceList().size() - 1; i >= 0; i--) {
			UserItem item = getTraceList().get(i);
			if (item.getUserId().equals(userid)) {
				getTraceList().remove(i);
				break;
			}
		}
	}

	/**
	 * 添加黑名单
	 */
	public void addTracelist(UserItem user) {
		if (!IsTracelistExist(user.getUserId())) {
			this.getTraceList().add(user);
		}
	}

	// 单位通讯录
	public List<AddressBooks> getNetAddressBooks() {
		if (this.netAddressBooks == null) {
			new NetAddressBookDaoImpl(this).getAddressBookList(getMyUserID());
		}
		return netAddressBooks;
	}

	public void setNetAddressBooks(List<AddressBooks> netAddressBooks) {

		this.netAddressBooks = netAddressBooks;
	}

	/**
	 * 移除单位通讯录
	 * 
	 */
	public void removeNetAddressBook(long bookId) {
		for (int i = getNetAddressBooks().size() - 1; i >= 0; i--) {
			AddressBooks item = getNetAddressBooks().get(i);
			if (item.getId() == bookId) {
				getNetAddressBooks().remove(i);
				break;
			}
		}
	}

	/**
	 * 添加单位通讯录
	 */
	public void addNetAddressBook(AddressBooks book) {
		if (!IsNetAddressBookExist(book.getId())) {
			this.getNetAddressBooks().add(book);
		}
	}

	/**
	 * 修改单位通讯录信息
	 * 
	 * @param book
	 */
	public void updateNetAddressBook(AddressBooks book) {
		for (AddressBooks element : getNetAddressBooks()) {
			if (element.getId() == book.getId().longValue()) {
				element.setName(book.getName());
				element.setVersion(book.getVersion());
				element.setNum(book.getNum());
			}
		}
	}

	/**
	 * 根据通讯录ID判断是否存在单位通讯录
	 */
	public boolean IsNetAddressBookExist(long bookId) {
		final Iterator<AddressBooks> iter = getNetAddressBooks().iterator();
		while (iter.hasNext()) {
			AddressBooks item = (AddressBooks) iter.next();
			if (item.getId() == bookId) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 根据通讯录ID判断是否存在单位通讯录
	 */
	public AddressBooks getNetAddressBook(AddressBooks addressBook) {
		final Iterator<AddressBooks> iter = getNetAddressBooks().iterator();
		while (iter.hasNext()) {
			AddressBooks item = (AddressBooks) iter.next();
			if (item.getId() == addressBook.getId().longValue()) {
				addressBook.setVersion(item.getVersion());
			}
		}
		return addressBook;
	}

	public AddressBooks getMyAddressBook(String bookId) {
		AddressBooks mAddressBook = null;
		for (AddressBooks element : getNetAddressBooks()) {
			if (element.getFlag() == AddressBooks.FLAG_MYSELF && bookId.equals(String.valueOf(element.getId()))) {
				mAddressBook = element;
			}
		}
		return mAddressBook;
	}

	public ChatRoomListActivity getChatRoomListActivity() {
		return chatRoomListActivity;
	}

	public void setChatRoomListActivity(ChatRoomListActivity chatRoomListActivity) {
		this.chatRoomListActivity = chatRoomListActivity;
	}

	public ChatRoomMainActivity getChatRoomMainActivity() {
		return chatRoomMainActivity;
	}

	public void setChatRoomMainActivity(ChatRoomMainActivity chatRoomMainActivity) {
		this.chatRoomMainActivity = chatRoomMainActivity;
	}

	public UserItem getDriftUser() {
		return driftUser;
	}

	public void setDriftUser(UserItem driftUser) {
		this.driftUser = driftUser;
	}

	public GesturesHandler getGestureHandler() {
		if (gesturesHandler == null) {
			gesturesHandler = new GesturesHandler(this);
		}
		return gesturesHandler;
	}

	public List<UserItem> getAttentList() {
		if (attentList == null) {
			attentList = new ArrayList<UserItem>();
		}
		return attentList;
	}

	public void setAttentList(List<UserItem> attentList) {
		this.attentList = attentList;
	}

	public void removeAttentList(UserItem attentUser) {
		if (null != attentList) {
			this.attentList.remove(attentUser);
		}
	}

	/**
	 * clearData
	 */
	public void clearData() {
		// TODO Auto-generated method stub
		// 设置application初始化数据，通讯录加载
		isLocalContactsInit = false;// >>>>>>>>>>
		isLocalContactsStratInit = false;// >>>>>>>>>>
		// 清空好友组数据
		if (friendGroup != null) {
			friendGroup.clear();
			friendGroup = null;
		}
		// 清空关注人列表
		if (attentGroup != null) {
			attentGroup.clear();
			attentGroup = null;
		}
		// 清空好友列表
		if (friendList != null) {
			friendList.clear();
			friendList = null;
		}
		// 清空本地通讯录
		if (localContacts != null) {
			localContacts.clear();
			localContacts = null;
		}
		// 清空单位通讯录列表
		if (netAddressBooks != null) {
			netAddressBooks.clear();
			netAddressBooks = null;
		}
		// 清空本地资源列表
		if (localDirs != null) {
			localDirs.clear();
			localDirs = null;
		}
		// 清空文件上传列表
		if (filePartMap != null) {
			filePartMap.clear();
			filePartMap = null;
		}
		// 清空信息下载器列表
		if (msgDownloaderMap != null) {
			msgDownloaderMap.clear();
			msgDownloaderMap = null;
		}
		// 清空黑名单列表
		if (blackList != null) {
			blackList.clear();
			blackList = null;
		}
		// 清空亲友列表
		if (traceList != null) {
			traceList.clear();
			traceList = null;
		}
		// 清空播音乐放列表
		if (playingMusiclist.getInfos() != null) {
			playingMusiclist.getInfos().clear();
		}
		mySelf = null;
		currunGroup = null;

		driftUser = null;
		System.gc();
	}

	public int getIsLoginByFingerprint() {
		return isLoginByFingerprint;
	}

	public void setIsLoginByFingerprint(int isLoginByFingerprint) {
		this.isLoginByFingerprint = isLoginByFingerprint;
	}

	@Override
	public void onLowMemory() {
		// TODO Auto-generated method stub
		super.onLowMemory();
		Log.i(TAG, "onLowMemory");
	}

	private static final long timeDelay = 1000;
	// >>>>>>>与后台服务器之间的时间差
	private Long pangkerTimeDiff = null;;

	public Long getPangkerTimeDiff() {
		if (pangkerTimeDiff == null) {
			pangkerTimeDiff = Util.String2Long(preferencesUtil.getString(PangkerConstant.CLIENT_SERVER_TIMEDIFF, "0"));
		}
		return pangkerTimeDiff;
	}

	public void setPangkerTimeDiff(long pangkerTimeDiff) {
		this.pangkerTimeDiff = pangkerTimeDiff;
		preferencesUtil.saveString(PangkerConstant.CLIENT_SERVER_TIMEDIFF, String.valueOf(pangkerTimeDiff));
	}

	/**
	 * 获取旁客server服务器时间
	 * 
	 * @return
	 */
	public String getServerTime() {
		long serverTime = System.currentTimeMillis() + getPangkerTimeDiff() + timeDelay;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new Date(serverTime));
	}

	// >>>>>>>>与群组groupSP服务器之间的时间差
	private Long groupSPTimeDiff;

	public long getGroupSPTimeDiff() {
		if (groupSPTimeDiff == null) {
			groupSPTimeDiff = Util.String2Long(preferencesUtil.getString(PangkerConstant.CLIENT_GSP_TIMEDIFF, "0"));
		}
		return groupSPTimeDiff;
	}

	public void setGroupSPTimeDiff(long groupSPTimeDiff) {
		this.groupSPTimeDiff = groupSPTimeDiff;
		preferencesUtil.saveString(PangkerConstant.CLIENT_GSP_TIMEDIFF, String.valueOf(groupSPTimeDiff));
	}

	/**
	 * 获取groupSPserverTime
	 * 
	 * @return
	 */
	public long getGroupSPServerTime() {
		long serverTime = System.currentTimeMillis() + getGroupSPTimeDiff() + timeDelay;
		return serverTime;
	}

}
