package com.wachoo.pangker.socket;

public class JavaSock {
	
	static {
		System.loadLibrary("ctp");
		System.loadLibrary("udt");
		System.loadLibrary("tup");
		System.loadLibrary("WachooJNI");
	}

	/**
	 * TCP 可靠传输的监听
	 * 
	 * @param ipaddr 本地Ip
	 * @param port 
	 * @param userInterface
	 * @return sockId
	 */
	public native CSock JavaSock_ServListen(int eid, String IPAddr, int Port, ParseMsgInface userInterface, int flag);
	/**
	 * UDP 非可靠的监听
	 * 
	 * @param ipaddr
	 * @param port
	 * @param userInterface
	 * @return SessionId
	 */
	public native CSock JavaSock_Session_ServListen(String ipaddr, short port, ParseMsgInface userInterface);
	//

	/**
	 * Tcp可靠的连接
	 * 
	 * @param serverIp
	 * @param serverPort
	 * @return sockId
	 */
	public native CSock JavaSock_ConnectTo(String Localaddr, String Servaddr, int ServPort, int flag);
	/**
	 * UDP连接
	 * 
	 * @param sockId = JavaSock_connect
	 * @param serverIp
	 * @param serverPort
	 * @return SessionId jni返回
	 */
	public native CSock JavaSock_SessionConnect(String Localaddr, String Servaddr, int ServPort, CSock Sock);
	/**
	 * 发送数据
	 * 
	 * @param sockId 创建的sockId
	 * @param sessionId （可靠的=0，非可靠的=JavaSock_sessionConnect）
	 * @param buff
	 * @param size
	 * @return
	 */
	public native int JavaSock_Send(CSock Sock, byte[] Buff, int Size);
	/**
	 * 接收数据
	 * 
	 * @param sockId
	 * @param sessionId
	 */
	public native CRecvData JavaSock_Recv(CSock Sock, int Size);

	/**
	 * 创建Epoll
	 * @return  
	 */
	public native int JavaSock_EpollCreate(int flag);
	/**
	 * 将Epoll中Sock移除
	 * 
	 * @param eid
	 * @param sockId
	 */
	public native void JavaSock_DelSock(int eid, CSock Sock);
	/**
	 * 添加Sock到Epoll中
	 * 
	 * @param eid
	 * @param sockId
	 */
	public native void JavaSock_AddSock(int eid, CSock Sock);

	public native void JavaSock_SetBlock(CSock Sock, boolean Block);
	/**
	 * 关闭sock链接
	 * 
	 * @param sockId
	 */
	public native void JavaSock_Close(CSock Sock);
	 /**
     * 设置网络检测间隔
     * @param MaxCheckCount
     * @return
     */
	public native int JavaSock_SetUnreliablePara(int MaxCheckCount, int sessionId);
	 /**
     * 获取网络丢包率
     * @param socketId
     * @param type
     * @return
     */
	public native int JavaSock_GetNetLoss(int socketId, int type, int sessionId);
	
	
}
