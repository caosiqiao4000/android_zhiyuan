package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.util.Util;

public class LocalMusicAdapter extends BaseAdapter{

	private Context context;
	private List<MusicInfo> musicList;
	private List<Boolean> mchecked;
	private boolean flagShow = true;

	public LocalMusicAdapter(Context context, boolean flag) {
		super();
		this.context = context;
		this.flagShow = flag;
		musicList = new ArrayList<MusicInfo>();
		mchecked = new ArrayList<Boolean>();
	}
	
	public void setFlagShow(boolean flagShow) {
		this.flagShow = flagShow;
		notifyDataSetChanged();
	}

	public void setMusicList(List<MusicInfo> musicList) {
		this.musicList = musicList;
		notifyDataSetChanged();
	}

	public void addMusicInfo(MusicInfo musicInfo) {
		musicList.add(musicInfo);
		mchecked.add(false);
		notifyDataSetChanged();
	}
	
	public void selectByIndex(int index, boolean flag){
		mchecked.set(index, flag);
		notifyDataSetChanged();
	}
	
	public List<MusicInfo> getSeletedMembers() {
		List<MusicInfo> selectedMembers = new ArrayList<MusicInfo>();
		for (int i = 0; i < musicList.size(); i++) {
			if(mchecked.get(i)){
				selectedMembers.add(musicList.get(i));
			}
		}
		return selectedMembers;
	}
	
	public void setSelectedAll(boolean isSelectedAll) {
		for (int i = 0; i < musicList.size(); i++) {
			mchecked.set(i, isSelectedAll);
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return musicList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return musicList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolderMusic holder;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MusicInfo item = (MusicInfo) getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.local_music_item, null);
			holder = new ViewHolderMusic();
			holder.iv_musicCover = (ImageView) convertView.findViewById(R.id.ImageViewMusicCover);
			holder.tv_musicName = (TextView)convertView.findViewById(R.id.tv_musicName);
			holder.tv_musicPlayer = (TextView) convertView.findViewById(R.id.tv_musicPlayer);
			holder.tv_musicScore = (TextView) convertView.findViewById(R.id.tv_musicScore);
			holder.selected = (CheckBox) convertView.findViewById(R.id.Music_CheckBox);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolderMusic) convertView.getTag();
		}
		if(item != null){
			if(!flagShow){
				holder.selected.setVisibility(View.INVISIBLE);
			} else {
				holder.selected.setVisibility(View.VISIBLE);
			}
			String url = Util.getAlbumArt(context, item.getMusicId());
			if(url != null){
				holder.iv_musicCover.setImageURI(Uri.parse(url));
			} else {
				holder.iv_musicCover.setImageResource(R.drawable.icon46_music);
			}
			holder.selected.setChecked(mchecked.get(position));
			holder.tv_musicName.setText(item.getMusicName());
			holder.tv_musicPlayer.setText(item.getSinger());
			String fileSize = Formatter.formatFileSize(context, item.getMusicSize());
			holder.tv_musicScore.setText("文件大小:" + fileSize);
		}
		return convertView;
	}
	
	public class ViewHolderMusic{
		public ImageView iv_musicCover; //封面 
		public TextView tv_musicName;   //歌曲名
		public TextView tv_musicPlayer;   //歌手
		public TextView tv_musicScore;   //歌手
		public CheckBox selected;  //是否选择一组
	}
	
	public String toMp3(String name) {
		int search = name.indexOf(".mp3");
		String newName = name.substring(0, search);
		return newName;
	}

	/**
	 * 播放器进度条时间处理方法
	 * 
	 * @param time
	 * @return
	 */
	public String toTime(int time) {

		time /= 1000;
		int minute = time / 60;
		int second = time % 60;
		minute %= 60;
		return String.format("%02d:%02d", minute, second);
	}
	
	/**
	 * 歌曲专辑图片显示,如果有歌曲图片，才会返回，否则为null，要注意判断
	 * 
	 * @param trackId
	 * @return 返回类型是String 类型的图片地址，也就是uri
	 */
	public String getAlbumArt(int trackId) {// trackId是音乐的id
		String mUriTrack = "content://media/external/audio/media/#";
		String[] projection = new String[] { "album_id" };
		String selection = "_id = ?";
		String[] selectionArgs = new String[] { Integer.toString(trackId) };
		Cursor cur = context.getContentResolver().query(Uri.parse(mUriTrack),
				projection, selection, selectionArgs, null);
		int album_id = 0;
		if (cur.getCount() > 0 && cur.getColumnCount() > 0) {
			cur.moveToNext();
			album_id = cur.getInt(0);
		}
		cur.close();
		cur = null;

		if (album_id < 0) {
			return null;
		}
		String mUriAlbums = "content://media/external/audio/albums";
		projection = new String[] { "album_art" };
		cur = context.getContentResolver().query(
				Uri.parse(mUriAlbums + "/" + Integer.toString(album_id)),
				projection, null, null, null);

		String album_art = null;
		if (cur.getCount() > 0 && cur.getColumnCount() > 0) {
			cur.moveToNext();
			album_art = cur.getString(0);
		}
		cur.close();
		cur = null;

		return album_art;
	}

}
