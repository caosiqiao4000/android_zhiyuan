package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.image.PKIconLoader;

/**
 * 
 * @author wangxin ///
 * 
 */
public class LocalContactsAdapter extends BaseAdapter {

	private LayoutInflater layoutInflater;
	private OnClickListener intoOnClickListener;
	private List<LocalContacts> mUserList;
	private PKIconLoader pkIconLoader;

	/**
	 * @return the mUserList
	 */
	public List<LocalContacts> getmUserList() {
		return mUserList;
	}

	/**
	 * @param mUserList
	 *            the mUserList to set
	 */
	public void setmUserList(List<LocalContacts> mUserList) {
		this.mUserList = mUserList;
		notifyDataSetChanged();
	}

	public LocalContactsAdapter(Context context, List<LocalContacts> users,
			OnClickListener intoOnClickListener) {
		layoutInflater = LayoutInflater.from(context);
		this.intoOnClickListener = intoOnClickListener;
		this.mUserList = users;
		this.pkIconLoader = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	public int getCount() {
		return mUserList == null ? 0 : mUserList.size();
	}

	public Object getItem(int position) {
		if (mUserList != null) {
			return mUserList.get(position);
		}
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		final LocalContacts item = mUserList.get(position);
		ViewHolder holder = null;

		if (null == convertView) {
			convertView = layoutInflater.inflate(R.layout.local_contacts_item, null);
			holder = new ViewHolder();
			holder.tv_firstCharHint = (TextView) convertView.findViewById(R.id.tv_first_char_hint);
			holder.tv_firstCharHint = (TextView) convertView.findViewById(R.id.tv_first_char_hint);
			holder.iv_userIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			holder.tv_userName = (TextView) convertView.findViewById(R.id.tv_username);
			holder.tv_phone_num = (TextView) convertView.findViewById(R.id.tv_usersign);
			holder.iv_into = (ImageView) convertView.findViewById(R.id.iv_into);
			holder.iv_identity = (ImageView) convertView.findViewById(R.id.iv_identity);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.tv_userName.setText(item.getUserName());
		holder.tv_phone_num.setVisibility(View.GONE);
		if (item.getPhoneNums() != null && item.getPhoneNums().size() > 0) {
			String phoneNums = "";
			for (int i = 0, size = item.getPhoneNums().size(); i < size; i++) {
				if (phoneNums.length() > 0) {
					phoneNums = phoneNums + "," + item.getPhoneNums().get(i);
				} else
					phoneNums = item.getPhoneNums().get(i);
			}
			holder.tv_phone_num.setText(phoneNums);
		}

		if (intoOnClickListener != null) {
			holder.iv_into.setVisibility(View.VISIBLE);
			holder.iv_into.setOnClickListener(intoOnClickListener);
			holder.iv_into.setTag(position);
		}

		if (!item.getFirstLetter().equals(LocalContacts.NO_LETTER)) {
			holder.tv_firstCharHint.setVisibility(View.VISIBLE);
			holder.tv_firstCharHint.setText(item.getFirstLetter());
		} else {
			// 此段代码不可缺：实例化一个CurrentView后，会被多次赋值并且只有最后一次赋值的position是正确
			holder.tv_firstCharHint.setVisibility(View.GONE);
		}
		if (item.isHasPhoto() && item.getPhoto() != null) {
			holder.iv_userIcon.setImageBitmap(item.getPhoto());
		} else {
			holder.iv_userIcon.setImageResource(R.drawable.nav_head);
		}

		if (item.getRelation() == LocalContacts.RELATION_P_USER) {
			holder.iv_identity.setVisibility(View.VISIBLE);
			holder.iv_identity.setImageResource(R.drawable.pangker);
			if (item.getUserId() != null)
				pkIconLoader.loadImage(item.getUserId(), holder.iv_userIcon);
		} else if (item.getRelation() == LocalContacts.RELATION_P_FRIEND) {
			holder.iv_identity.setVisibility(View.VISIBLE);
			holder.iv_identity.setImageResource(R.drawable.nav_head);
			if (item.getUserId() != null)
				pkIconLoader.loadImage(item.getUserId(), holder.iv_userIcon);
		} else {
			holder.iv_identity.setVisibility(View.GONE);
		}
		return convertView;
	}

	public final class ViewHolder {
		public ImageView iv_userIcon;
		public TextView tv_firstCharHint;
		public ImageView iv_order;
		public TextView tv_userName;
		public TextView tv_phone_num;
		public ImageView iv_into;
		public ImageView iv_identity; // 身份识别，是否旁客用户，是否好友
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
	}

}
