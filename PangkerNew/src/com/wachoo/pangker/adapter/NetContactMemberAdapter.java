package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.response.AddressMembers;

public class NetContactMemberAdapter extends BaseAdapter {

	private List<AddressMembers> members;
	private Context context;
	private List<Boolean> mchecked;

	private PKIconResizer mImageResizer;

	public NetContactMemberAdapter(Context context, List<AddressMembers> members) {
		super();
		this.members = members;
		this.context = context;
		mchecked = new ArrayList<Boolean>();
		for (int i = 0; i < members.size(); i++) {
			mchecked.add(false);
		}
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	public void selectByIndex(int index, boolean flag) {
		mchecked.set(index, flag);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return members.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return members.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public boolean isEnabled(int position) {
		AddressMembers lable = (AddressMembers) getItem(position);
		if (lable.getId() == null) {
			return false;
		}
		return super.isEnabled(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		AddressMembers userItem = (AddressMembers) getItem(position);
		if(convertView == null){
			holder = new ViewHolder();
			if (isEnabled(position)) {
				convertView = LayoutInflater.from(context).inflate(R.layout.tracegroup_member_item, null);
				holder.selected = (CheckBox) convertView.findViewById(R.id.mtrace_checkbox);
				holder.userName = (TextView) convertView.findViewById(R.id.tv_name);
				holder.userSign = (TextView) convertView.findViewById(R.id.tv_left_bottom);
				holder.isAttention = (TextView) convertView.findViewById(R.id.tv_right_bottom);
				holder.userIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			} else {
				convertView = LayoutInflater.from(context).inflate(R.layout.app_item, null);
				holder.userIcon = (ImageView) convertView.findViewById(R.id.app_img);
				holder.userName = (TextView) convertView.findViewById(R.id.app_label);
			}
			convertView.setTag(holder);
		}
		else holder = (ViewHolder) convertView.getTag(); 

		holder.userName.setText(userItem.getRemarkname());
		holder.userSign.setText(userItem.getMobile());
		holder.selected.setChecked(mchecked.get(position));
		
		if(userItem != null && userItem.getUid()!=null && userItem.getUid().intValue() != 0){
			mImageResizer.loadImage(String.valueOf(userItem.getUid()), holder.userIcon);
		}else{
			holder.userIcon.setImageResource(R.drawable.nav_head);
		}
		return convertView;
	}

	public class ViewHolder {
		public CheckBox selected; // 是否选择一组
		public TextView userName; // 好友名称
		public TextView userSign; // 好友名称
		public TextView isAttention;
		public ImageView userIcon; // 用户头像
	}

}
