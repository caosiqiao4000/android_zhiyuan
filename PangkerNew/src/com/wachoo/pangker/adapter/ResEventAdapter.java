package com.wachoo.pangker.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.response.EventResult;
import com.wachoo.pangker.util.ImageUtil;

public class ResEventAdapter extends BaseAdapter {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private Activity context;
	private List<EventResult> eventList;
	private PKIconResizer mImageResizer;
	private PangkerApplication application;

	public ResEventAdapter(Activity context, List<EventResult> eventList) {
		this.context = context;
		this.eventList = eventList;
		application = ((PangkerApplication) context.getApplicationContext());
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return eventList.size();
	}

	@Override
	public EventResult getItem(int position) {
		// TODO Auto-generated method stub
		return eventList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		EventResult result = eventList.get(position);
		if (convertView == null) {

			convertView = LayoutInflater.from(context).inflate(R.layout.res_event_item, null);
			final ImageView ivUserIcon = (ImageView) convertView.findViewById(R.id.iv_user_icon);
			TextView tvResName = (TextView) convertView.findViewById(R.id.tv_res_name);

			String userIconUrl = Configuration.getIconUrl() + result.getUid().toString();

			final ImageView ivIcon = (ImageView) convertView.findViewById(R.id.iv_res_icon);
			final ImageView ivImageIcon = (ImageView) convertView.findViewById(R.id.iv_image_icon);
			String iconUrl = Configuration.getCoverDownload() + result.getId();
			final int resType = result.getType();
			if (resType == PangkerConstant.RES_APPLICATION) {
				ivIcon.setImageResource(R.drawable.icon46_apk);
				tvResName.setText("上传应用：" + result.getName());
			} else if (resType == PangkerConstant.RES_PICTURE) {
				ivIcon.setVisibility(View.GONE);
				ivImageIcon.setImageResource(R.drawable.icon46_photo);
				tvResName.setText("上传图片：" + result.getName());
				int sideLength = ImageUtil.getPicPreviewResolution(context);
				iconUrl = ImageUtil.getWebImageURL(result.getId(), sideLength);

			} else if (resType == PangkerConstant.RES_MUSIC) {
				ivIcon.setImageResource(R.drawable.icon46_music);
				tvResName.setText("上传音乐：" + result.getName());
			} else if (resType == PangkerConstant.RES_DOCUMENT) {
				ivIcon.setImageResource(R.drawable.attachment_doc);
				tvResName.setText("上传文档：" + result.getName());
			}

			try {
				// 用户头像
				mImageResizer.loadImage(userIconUrl, ivUserIcon);
			} catch (OutOfMemoryError e) {
				logger.error(TAG, e);
			} catch (Exception e) {
				logger.error(TAG, e);
			}

			TextView tvUsername = (TextView) convertView.findViewById(R.id.tv_username);
			tvUsername.setText(result.getNickName());

			TextView tvResTime = (TextView) convertView.findViewById(R.id.res_tv_time);
			tvResTime.setText(result.getTime());

		}
		return convertView;
	}

	public List<EventResult> getEventList() {
		return eventList;
	}

	public void setEventList(List<EventResult> eventList) {
		this.eventList = eventList;
		this.notifyDataSetChanged();
	}

}
