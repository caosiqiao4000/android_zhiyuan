package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.response.Commentinfo;
import com.wachoo.pangker.util.Util;

public class CommentAdapter extends BaseAdapter {

	private Context mContext;
	private List<Commentinfo> commentinfos;
	private PKIconResizer mImageResizer;

	public CommentAdapter(Context mContext, List<Commentinfo> commentinfos) {
		super();
		this.mContext = mContext;
		this.commentinfos = commentinfos;

		mImageResizer = PangkerManager.getUserIconResizer(mContext.getApplicationContext());
	}

	public void clearCommentinfos() {
		// TODO Auto-generated method stub
		if (commentinfos != null) {
			commentinfos.clear();
		}
	}

	public void setCommentinfos(List<Commentinfo> commentinfos) {
		this.commentinfos = commentinfos;
	}

	public void addCommentinfos(List<Commentinfo> commentinfos) {
		// TODO Auto-generated method stub
		this.commentinfos.addAll(commentinfos);
	}

	public void addComment(Commentinfo commentinfo) {
		// TODO Auto-generated method stub
		commentinfos.add(0, commentinfo);
		this.notifyDataSetChanged();
	}

	public void reset() {
		commentinfos.clear();
	}

	public void delComment(Commentinfo commentinfo) {
		// TODO Auto-generated method stub
		commentinfos.remove(commentinfo);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return commentinfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return commentinfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Commentinfo commentinfo = (Commentinfo) getItem(position);
		ViewHolder holder = null;
		if (convertView == null) {

			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.comment_item, null);
			holder.userIcon = (ImageView) convertView.findViewById(R.id.comment_userincon);
			holder.spliteLine = (LinearLayout) convertView.findViewById(R.id.splite_line);
			holder.userName = (TextView) convertView.findViewById(R.id.comment_username);
			holder.commTime = (TextView) convertView.findViewById(R.id.comment_time);
			holder.commContent = (TextView) convertView.findViewById(R.id.comment_content);

			// >>>>>>回复用户评论
			holder.lyCommentReply = (LinearLayout) convertView.findViewById(R.id.ly_comment_reply);
			holder.txtCommentReplayUName = (TextView) convertView.findViewById(R.id.comment_reply_username);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.userName.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, commentinfo.getUserid());
				intent.putExtra(UserItem.USERNAME, commentinfo.getUsername());
				mContext.startActivity(intent);
			}
		});

		holder.userIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, commentinfo.getUserid());
				intent.putExtra(UserItem.USERNAME, commentinfo.getUsername());
				mContext.startActivity(intent);
			}
		});

		holder.txtCommentReplayUName.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, commentinfo.getReplyuserid());
				intent.putExtra(UserItem.USERNAME, commentinfo.getUsername());
				mContext.startActivity(intent);
			}
		});

		holder.userName.setText(commentinfo.getUsername());
		holder.commTime.setText(Util.TimeIntervalBtwNow(commentinfo.getCommenttime()));
		holder.commContent.setText(commentinfo.getContent());
		if (position == getCount() - 1) {
			holder.spliteLine.setVisibility(View.GONE);
		}
		mImageResizer.loadImage(commentinfo.getUserid(), holder.userIcon);
		if (commentinfo.getCommenttype() == Commentinfo.COMMENT_TYPE_ONLY) {
			holder.lyCommentReply.setVisibility(View.GONE);
		} else {
			holder.lyCommentReply.setVisibility(View.VISIBLE);
			holder.txtCommentReplayUName.setText(commentinfo.getReplyusername());
		}

		return convertView;
	}

	class ViewHolder {
		ImageView userIcon;
		LinearLayout spliteLine;
		TextView userName;
		TextView commTime;
		TextView commContent;
		// >>>>>>>回复对象
		LinearLayout lyCommentReply;
		TextView txtCommentReplayUName;
	}
}
