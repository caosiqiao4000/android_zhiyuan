package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.response.AddressMembers;

public class NetContactMemberSelectAdapter extends BaseAdapter {

	private List<AddressMembers> member;
	private Context context;
	private boolean deleteable = false;

	private PKIconResizer mImageResizer;

	public NetContactMemberSelectAdapter(Context context) {
		super();
		this.context = context;
		member = new ArrayList<AddressMembers>();
		AddressMembers defUserItem = new AddressMembers();
		defUserItem.setId(null);
		member.add(defUserItem);
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	public void dealUserItem(List<AddressMembers> items, boolean flag) {
		for (AddressMembers item : items) {
			dealMember(item, flag);
		}
	}

	public List<AddressMembers> getAddressMembers() {
		member.remove(member.size() - 1);
		return member;
	}

	public void dealMember(AddressMembers item, boolean flag) {
		if (flag) {
			member.add(member.size() - 1, item);
		} else {
			if (member.contains(item)) {
				member.remove(item);
			}
		}
		notifyDataSetChanged();
	}

	public boolean isHide() {
		return deleteable;
	}

	public void setHide(boolean isHide) {
		this.deleteable = isHide;
	}

	public List<String> getTraceIdList() {
		List<String> traceInList = new ArrayList<String>();
		for (AddressMembers item : member) {
			if (item.getId() != null && !String.valueOf(item.getId()).equals(null)) {
				traceInList.add(String.valueOf(item.getId()));
			}
		}
		return traceInList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return member.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return member.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView imageView = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.image_item, null);
			imageView = (ImageView) convertView.findViewById(R.id.imgitem_usericon);
			convertView.setTag(imageView);
		} else {
			imageView = (ImageView) convertView.getTag();
		}

		final AddressMembers member = (AddressMembers) getItem(position);
		if(member != null && member.getUid()!=null && member.getUid().intValue() != 0){
			mImageResizer.loadImage(String.valueOf(member.getUid()), imageView);
		}else{
			imageView.setImageResource(R.drawable.selectcontents_bg);
		}
		return convertView;
	}
}
