package com.wachoo.pangker.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.TextSpeakInfo;
import com.wachoo.pangker.util.Util;

public class ResSpeakAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater layoutInflater;
	private List<TextSpeakInfo> mSpeakList;
	private ViewHolder holder;

	private Handler handler = new Handler();
	// >>>>>>>设置显示长度
	private InputFilter[] filters = { new InputFilter.LengthFilter(PangkerConstant.showLength) };
	private InputFilter[] morefilters = {};

	public ResSpeakAdapter(Activity mActivity, List<TextSpeakInfo> speakList) {
		// TODO Auto-generated constructor stub
		this.context = mActivity.getApplicationContext();
		layoutInflater = LayoutInflater.from(context);
		this.mSpeakList = speakList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mSpeakList == null ? 0 : mSpeakList.size();
	}

	@Override
	public TextSpeakInfo getItem(int position) {
		// TODO Auto-generated method stub
		if (mSpeakList != null) {
			return mSpeakList.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return mSpeakList.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final TextSpeakInfo item = mSpeakList.get(position);

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.res_speak_info, null);
			holder.tv_username = (TextView) convertView.findViewById(R.id.tv_username);
			holder.tv_content = (TextView) convertView.findViewById(R.id.tv_res_detail);
			holder.tv_more = (TextView) convertView.findViewById(R.id.tv_res_show);
			holder.tv_time = (TextView) convertView.findViewById(R.id.tv_res_time);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// >>>>>>>>显示说说内容
		String content = item.getContent();
		holder.tv_content.setFilters(morefilters);
		if (content.length() > PangkerConstant.showLength) {
			holder.tv_more.setVisibility(View.VISIBLE);
			holder.tv_more.setText(item.getBtnShowText());
			if (holder.tv_more.getText().equals(PangkerConstant.SHOW_MORE_TEXT)) {
				holder.tv_content.setFilters(filters);
			}
		} else {
			holder.tv_more.setVisibility(View.GONE);
		}
		holder.tv_content.setText(content);
		holder.tv_username.setText(item.getUserName());
		holder.tv_more.setTag(position);
		holder.tv_more.setOnClickListener(new AcceptOnClickListener(holder.tv_more, holder.tv_content, position));
		holder.tv_time.setText((Util.getDateDiff(Util.strToDate(item.getSpeakTime()))));

		return convertView;
	}

	/**
	 * @return the mSpeakList
	 */
	public List<TextSpeakInfo> getmSpeakList() {
		return mSpeakList;
	}

	/**
	 * @param mUserList
	 *            the mUserList to set
	 */
	public void setmSpeakList(List<TextSpeakInfo> mSpeakList) {
		this.mSpeakList = mSpeakList;
		this.notifyDataSetChanged();
	}

	public final class ViewHolder {
		public TextView tv_username;
		public TextView tv_content;
		public TextView tv_more;
		public TextView tv_time;
	}

	public void addmSpeakList(List<TextSpeakInfo> list) {
		// TODO Auto-generated method stub
		this.mSpeakList.addAll(list);
		this.notifyDataSetChanged();
	}

	public void deletSpeakItem(TextSpeakInfo mTextSpeakInfo) {
		// TODO Auto-generated method stub
		this.mSpeakList.remove(mTextSpeakInfo);
		notifyDataSetChanged();
	}

	class AcceptOnClickListener implements View.OnClickListener {

		private TextView mShow;
		private TextView mDetail;
		private int position;

		public AcceptOnClickListener(TextView mShow, TextView mDetail, int position) {
			super();
			this.mShow = mShow;
			this.mDetail = mDetail;
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (mShow.getText().equals(PangkerConstant.SHOW_MORE_TEXT)) {

				handler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						mShow.setText(PangkerConstant.SHOW_UP_TEXT);
						mDetail.setFilters(morefilters);
						// >>>>>>>>设置按钮显示
						getItem(position).setBtnShowText(PangkerConstant.SHOW_UP_TEXT);
						mDetail.setText(getItem(position).getContent());
					}
				});

			} else {
				handler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						mShow.setText(PangkerConstant.SHOW_MORE_TEXT);
						mDetail.setFilters(filters);
						// >>>>>>>>设置按钮显示
						getItem(position).setBtnShowText(PangkerConstant.SHOW_MORE_TEXT);
						mDetail.setText(getItem(position).getContent());
					}
				});

			}
		}
	}
}
