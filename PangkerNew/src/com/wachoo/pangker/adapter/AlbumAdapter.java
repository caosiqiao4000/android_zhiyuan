package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DirInfo;

public class AlbumAdapter extends BaseAdapter {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	private Context context;
	private ArrayList<DirInfo> localDirInfos;
	private ArrayList<Boolean> mchecked;
	private GridView gridView;

	public AlbumAdapter(Context context, GridView gridView) {
		super();
		this.context = context;
		this.gridView = gridView;
		localDirInfos = new ArrayList<DirInfo>();
		mchecked = new ArrayList<Boolean>();
		int width = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
	}

	public void addPidInfo(DirInfo dirInfo) {
		localDirInfos.add(dirInfo);
		mchecked.add(false);
		notifyDataSetChanged();
	}

	public void setSelectedAll(boolean isSelectedAll) {
		for (int i = 0; i < localDirInfos.size(); i++) {
			mchecked.set(i, isSelectedAll);
		}
		notifyDataSetChanged();
	}

	public void selectByIndex(int index, boolean flag) {
		mchecked.set(index, flag);
	}

	public List<DirInfo> getSeletedMembers() {
		int temp = mchecked.size();
		List<DirInfo> selectedMembers = new ArrayList<DirInfo>();
		for (int i = 0; i < temp; i++) {
			if (mchecked.get(i)) {
				selectedMembers.add(localDirInfos.get(i));
			}
		}
		return selectedMembers;
	}

	public List<DirInfo> getLocalDirInfos() {
		return localDirInfos;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return localDirInfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return localDirInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderAlbum viewHolderAlbum;
		viewHolderAlbum = new ViewHolderAlbum();
		convertView = LayoutInflater.from(context).inflate(R.layout.album_item, null);
		viewHolderAlbum.selected = (CheckBox) convertView.findViewById(R.id.Album_CheckBox);
		viewHolderAlbum.imageView = (ImageView) convertView.findViewById(R.id.Album_img);
		convertView.setTag(position);
		DirInfo item = (DirInfo) getItem(position);
		viewHolderAlbum.imageView.setBackgroundResource(R.drawable.listmod_bighead_photo_default);
		if(item != null){
			 
		}
		return convertView;
	}

	public class ViewHolderAlbum {
		public CheckBox selected; // 是否选择一组
		public ImageView imageView; // 用户头像
	}
	
	private void loadImage() {
		// TODO Auto-generated method stub
		int start = gridView.getFirstVisiblePosition();
		int end = gridView.getLastVisiblePosition();
		if(end >= getCount()){
			end = getCount() -1;
		}
	}
	
	 

}
