package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.response.ShareInfo;

/**
 * 资源（分享）管理adapter zhengjy
 * 
 * 2012-3-19
 */
public class ResShareAdapter extends BaseAdapter {

	private ImageResizer mImageWorker;
	private Context mContext;
	private List<ShareInfo> resList;

	public ResShareAdapter(Context context, List<ShareInfo> resList, ImageResizer mImageWorker) {
		super();
		this.mContext = context;
		this.resList = resList;
		this.mImageWorker = mImageWorker;
	}

	public void delShareInfo(ShareInfo shareInfo) {
		resList.remove(shareInfo);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return resList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return resList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolder viewHolder;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ShareInfo itemInfo = resList.get(position);
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.res_mana_item, null);
			viewHolder.ivType = (ImageView) convertView.findViewById(R.id.iv_type);
			viewHolder.tvResName = (TextView) convertView.findViewById(R.id.tv_res_name);
			viewHolder.tvResTime = (TextView) convertView.findViewById(R.id.tv_res_time);
			viewHolder.appShare = (ImageView) convertView.findViewById(R.id.app_share);
			viewHolder.tvResScore = (TextView) convertView.findViewById(R.id.tv_res_score);
			viewHolder.tvResSize = (TextView) convertView.findViewById(R.id.tv_res_size);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		if (itemInfo.getType() == PangkerConstant.RES_APPLICATION) {
			mImageWorker.setLoadingImage(R.drawable.icon46_apk);
			String iconUrl = Configuration.getCoverDownload() + itemInfo.getResId();
			mImageWorker.loadImage(iconUrl, viewHolder.ivType, String.valueOf(itemInfo.getResId()));
		} else if (itemInfo.getType() == PangkerConstant.RES_MUSIC) {
			mImageWorker.setLoadingImage(R.drawable.icon46_music);
			String iconUrl = Configuration.getCoverDownload() + itemInfo.getResId();
			mImageWorker.loadImage(iconUrl, viewHolder.ivType, String.valueOf(itemInfo.getResId()));
		} else if (itemInfo.getType() == PangkerConstant.RES_DOCUMENT) {
			viewHolder.ivType.setImageResource(R.drawable.attachment_doc);
		}
		viewHolder.tvResName.setText(itemInfo.getName());
		viewHolder.tvResScore.setText("得分:" + itemInfo.getScore());
		String fileSize = Formatter.formatFileSize(mContext, itemInfo.getSize());
		viewHolder.tvResSize.setText(fileSize);
		viewHolder.tvResTime.setText("发布人:" + itemInfo.getOwnername());
		if (itemInfo.getFlag() == 1) {
			viewHolder.appShare.setVisibility(View.VISIBLE);
		} else {
			viewHolder.appShare.setVisibility(View.GONE);
		}
		return convertView;
	}

	public List<ShareInfo> getResList() {
		return resList;
	}

	public void setResList(List<ShareInfo> resList) {
		this.resList = resList;
		notifyDataSetChanged();
	}

	public static class ViewHolder {
		ImageView ivType;
		TextView tvResName;
		TextView tvResTime;
		ImageView appShare;
		TextView tvResScore;
		TextView tvResSize;
	}

}
