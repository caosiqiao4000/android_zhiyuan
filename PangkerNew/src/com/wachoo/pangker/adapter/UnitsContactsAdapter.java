package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.response.AddressBooks;

/**
 * 
 * 单位通讯录列表
 * 
 */
public class UnitsContactsAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	// private OnClickListener mClickListener;
	private List<AddressBooks> mContactList;

	public UnitsContactsAdapter(Context context, List<AddressBooks> users) {
		layoutInflater = LayoutInflater.from(context);
		// this.mClickListener = mClickListener;
		this.mContactList = users;
	}

	public int getCount() {
		return mContactList == null ? 0 : mContactList.size();
	}

	public Object getItem(int position) {
		if (mContactList != null) {
			return mContactList.get(position);
		}
		return null;
	}
	
	public void addItem(AddressBooks item) {
		// TODO Auto-generated method stub
		mContactList.add(0, item);
		notifyDataSetChanged();
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final AddressBooks item = mContactList.get(position);
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.unit_contact_list_item, null);
			holder = new ViewHolder();
			holder.iv_ucontact_icon = (ImageView) convertView.findViewById(R.id.iv_ucontact_icon);
			holder.tv_bookname = (TextView) convertView.findViewById(R.id.tv_bookname);
			holder.tv_username = (TextView) convertView.findViewById(R.id.tv_username);
			holder.tv_usernum = (TextView) convertView.findViewById(R.id.tv_usernum);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.iv_ucontact_icon.setImageResource(R.drawable.companyaddbook_head);
		holder.tv_bookname.setText(item.getName());
		if (item.getFlag() != null && item.getFlag() == AddressBooks.FLAG_MYSELF) {
			holder.tv_username.setTextColor(R.color.blue);
			holder.tv_username.setText("创建人:" + item.getUsername());
		} else
			holder.tv_username.setText("创建人:" + item.getUsername());
		holder.tv_usernum.setText("(" + item.getNum() + "人)");
		return convertView;
	}

	/**
	 * 
	 * @author wangxin
	 * 
	 */
	public final class ViewHolder {
		public ImageView iv_ucontact_icon;
		public TextView tv_bookname;
		public TextView tv_username;
		public TextView tv_usernum;
	}

	public List<AddressBooks> getmContactList() {
		return mContactList;
	}

	public void setmContactList(List<AddressBooks> mContactList) {
		this.mContactList = mContactList;
		this.notifyDataSetChanged();
	}

	/**
	 * 刷新某个单位通讯录的页面信息
	 * 
	 * @param book
	 */
	public void updateBooks(AddressBooks book) {
		for (AddressBooks element : mContactList) {
			if (element.getId() == book.getId()) {
				element.setName(book.getName());
				element.setNum(book.getNum());
				element.setUsername(book.getUsername());
			}
		}
		this.notifyDataSetChanged();
	}

	/**
	 * 移除单位通讯录，页面刷新
	 * 
	 * @param book
	 */
	public void removeBooks(AddressBooks book) {
		mContactList.remove(book);
		this.notifyDataSetChanged();
	}

}
