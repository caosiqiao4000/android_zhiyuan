package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DirInfo;
/**
 * 资源目录的adapter 
 * @author Administrator
 */
public class DirsAdapter extends BaseAdapter{

	private Context context;
	private List<DirInfo> dirinfos;
	
	public DirsAdapter(Context context, List<DirInfo> dirinfos) {
		super();
		this.context = context;
		this.dirinfos = dirinfos;
	}
	
	public void clear() {
		dirinfos.clear();
		this.notifyDataSetChanged();
	}
	
	public void setDirinfos(List<DirInfo> dirinfos) {
		this.dirinfos = dirinfos;
		this.notifyDataSetChanged();
	}
	
	public void addDirInfo(DirInfo dirinfo){
		dirinfos.add(dirinfo);
		this.notifyDataSetChanged();
	}
	
	public void delDirInfo(DirInfo dirinfo){
		dirinfos.remove(dirinfo);
		this.notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dirinfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dirinfos.get(position);
	}
	

	public List<DirInfo> getDirinfos() {
		return dirinfos;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DirInfo item = (DirInfo) getItem(position);
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.dir_item, null);
			viewHolder.mAppName = (TextView) convertView.findViewById(R.id.app_label);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (item != null) {
			viewHolder.setDirInfo(item);
			if ("0".equals(item.getParentDirId())) {
				viewHolder.mAppName.setText(item.getDirName() + "根目录");
			} else {
				viewHolder.mAppName.setText(item.getDirName() + "(" + item.getFileCount() + ")");
			}
		}
		return convertView;
	}

	class ViewHolder extends ViewBaseHolder{
		TextView mAppName;
		ImageView mAppNext;
	}
}
