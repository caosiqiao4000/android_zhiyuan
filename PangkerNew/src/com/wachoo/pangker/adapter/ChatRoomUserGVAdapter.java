package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.group.P2PUserInfo;
import com.wachoo.pangker.image.PKIconResizer;

public class ChatRoomUserGVAdapter extends BaseAdapter {
	private Context mContext;
	private List<P2PUserInfo> mAllUserList = new ArrayList<P2PUserInfo>();
	private List<P2PUserInfo> mShowUserList = new ArrayList<P2PUserInfo>();
	private int mSexFilter = 2;
	private PKIconResizer mImageResizer = null;

	public ChatRoomUserGVAdapter(Context mContext, PKIconResizer mImageResizer) {
		this.mContext = mContext;
		this.mImageResizer = mImageResizer;
	}

	public List<P2PUserInfo> getmUserList() {
		return mAllUserList;
	}

	public void setmUserList(List<P2PUserInfo> mUserList) {
		this.mAllUserList = mUserList;
		switch (mSexFilter) {
		case 0:
			showBoys();
			break;
		case 1:
			showGrils();
			break;
		default:
			showAllUsers();
			break;
		}
	}

	public void showBoys() {
		mSexFilter = UserItem.USER_SEX_BOY;
		this.mShowUserList.clear();
		for (P2PUserInfo info : this.mAllUserList) {
			if (info.getSex() == UserItem.USER_SEX_BOY)
				this.mShowUserList.add(info);
		}
		this.notifyDataSetChanged();
	}

	public void showGrils() {
		mSexFilter = UserItem.USER_SEX_GRIL;
		this.mShowUserList.clear();
		for (P2PUserInfo info : this.mAllUserList) {
			if (info.getSex() == UserItem.USER_SEX_GRIL)
				this.mShowUserList.add(info);
		}
		this.notifyDataSetChanged();
	}

	public void showAllUsers() {
		mSexFilter = 2;
		this.mShowUserList.clear();
		this.mShowUserList.addAll(mAllUserList);
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (mShowUserList == null)
			return 0;
		return (mShowUserList.size() + 2) / 3;
	}

	@Override
	public P2PUserInfo getItem(int position) {
		// TODO Auto-generated method stub
		return mShowUserList.get(position);

	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final int location = position * 3;
		P2PUserInfo item_one = null;
		P2PUserInfo item_two = null;
		P2PUserInfo item_three = null;

		// 获取项目数据源
		if (location <= mShowUserList.size() - 1) {
			item_one = getItem(location);
		}
		if (location + 1 <= mShowUserList.size() - 1) {
			item_two = getItem(location + 1);
		}
		if (location + 2 <= mShowUserList.size() - 1) {
			item_three = getItem(location + 2);
		}

		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.nearuser_gv_item, null);
			holder.ry_user_item1 = (RelativeLayout) convertView.findViewById(R.id.ly_user_item1);
			holder.iv_usericon1 = (ImageView) convertView.findViewById(R.id.iv_usericon1);
			holder.iv_sex1 = (ImageView) convertView.findViewById(R.id.iv_sex1);
			holder.tv_username1 = (TextView) convertView.findViewById(R.id.tv_username1);

			holder.ry_user_item2 = (RelativeLayout) convertView.findViewById(R.id.ly_user_item2);
			holder.iv_usericon2 = (ImageView) convertView.findViewById(R.id.iv_usericon2);
			holder.iv_sex2 = (ImageView) convertView.findViewById(R.id.iv_sex2);
			holder.tv_username2 = (TextView) convertView.findViewById(R.id.tv_username2);

			holder.ry_user_item3 = (RelativeLayout) convertView.findViewById(R.id.ly_user_item3);
			holder.iv_usericon3 = (ImageView) convertView.findViewById(R.id.iv_usericon3);
			holder.iv_sex3 = (ImageView) convertView.findViewById(R.id.iv_sex3);
			holder.tv_username3 = (TextView) convertView.findViewById(R.id.tv_username3);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		initViewData(item_one, holder.ry_user_item1, holder.iv_usericon1, holder.iv_sex1,
				holder.tv_username1, location);

		initViewData(item_two, holder.ry_user_item2, holder.iv_usericon2, holder.iv_sex2,
				holder.tv_username2, location + 1);

		initViewData(item_three, holder.ry_user_item3, holder.iv_usericon3, holder.iv_sex3,
				holder.tv_username3, location + 2);

		return convertView;
	}

	class ViewHolder {

		public RelativeLayout ry_user_item1;
		public ImageView iv_usericon1;
		public ImageView iv_sex1;// 性别
		public TextView tv_username1;

		public RelativeLayout ry_user_item2;
		public ImageView iv_usericon2;
		public ImageView iv_sex2;// 性别
		public TextView tv_username2;

		public RelativeLayout ry_user_item3;
		public ImageView iv_usericon3;
		public ImageView iv_sex3;// 性别
		public TextView tv_username3;
	}

	private void initViewData(P2PUserInfo item, View relativeLayout, ImageView iv_userIcon, ImageView iv_sex,
			TextView tv_userName, final int location) {
		if (item != null) {
			relativeLayout.setVisibility(View.VISIBLE);
			relativeLayout.setBackgroundResource(R.drawable.itemlist_bg);
			iv_userIcon.setVisibility(View.VISIBLE);
			iv_sex.setVisibility(View.VISIBLE);
			tv_userName.setVisibility(View.VISIBLE);

			mImageResizer.loadImage(String.valueOf(item.getUID()), iv_userIcon);
			iv_userIcon.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					goToUserInfo(location);
				}
			});
			if (PangkerConstant.NEAR_USER_GIRLS.equals(item.getSex())) {
				iv_sex.setImageResource(R.drawable.icon24_g);
			} else if (PangkerConstant.NEAR_USER_BOYS.equals(item.getSex())) {
				iv_sex.setImageResource(R.drawable.icon24_m);
			}
			tv_userName.setText(item.getUserName());
		} else {
			relativeLayout.setBackgroundResource(R.drawable.main_bg);
			iv_userIcon.setVisibility(View.GONE);
			iv_sex.setVisibility(View.GONE);
			tv_userName.setVisibility(View.GONE);
		}
	}

	/**
	 * 查看用户信息
	 * 
	 * @param intent
	 */
	private void goToUserInfo(int location) {
		String mUserId =((PangkerApplication)mContext.getApplicationContext()).getMyUserID();
        P2PUserInfo item = mShowUserList.get(location);
		if (String.valueOf(item.getUID()).equals(mUserId)) {
			Toast.makeText(mContext, R.string.isyourself, Toast.LENGTH_SHORT).show();
		} else {
			Intent intent = new Intent(mContext, UserWebSideActivity.class);
			intent.putExtra(UserItem.USERID, String.valueOf(item.getUID()));
			intent.putExtra(UserItem.USERNAME, item.getUserName());
			intent.putExtra(UserItem.USERSIGN, item.getSign());
			mContext.startActivity(intent);
		}
	}
}
