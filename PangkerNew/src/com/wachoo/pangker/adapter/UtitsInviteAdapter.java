package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;

public class UtitsInviteAdapter extends BaseAdapter {

	private List<UserItem> members;
	private Context context;
	private List<Boolean> mchecked;
	private PKIconResizer mImageResizer;

	public UtitsInviteAdapter(Context context, List<UserItem> members) {
		super();
		this.members = members;
		this.context = context;
		mchecked = new ArrayList<Boolean>();
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		for (int i = 0; i < members.size(); i++) {
			mchecked.add(false);
		}
	}

	public void selectByIndex(int index, boolean flag) {
		mchecked.set(index, flag);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return members.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return members.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public boolean isEnabled(int position) {
		UserItem lable = (UserItem) getItem(position);
		if (lable.getUserId() == null) {
			return false;
		}
		return super.isEnabled(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		UserItem userItem = (UserItem) getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.tracegroup_member_item, null);
			holder = new ViewHolder();
			holder.selected = (CheckBox) convertView.findViewById(R.id.mtrace_checkbox);
			holder.userName = (TextView) convertView.findViewById(R.id.tv_name);
			holder.userSign = (TextView) convertView.findViewById(R.id.tv_left_bottom);
			holder.isAttention = (TextView) convertView.findViewById(R.id.tv_right_bottom);
			holder.userIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (userItem != null) {
			if (isEnabled(position)) {
				holder.userName.setText(userItem.getUserName());
				holder.userSign.setText(userItem.getSign());
				holder.selected.setChecked(mchecked.get(position));

				mImageResizer.loadImage(userItem.getUserId(), holder.userIcon);

			} else {
				holder.userIcon.setImageResource(R.drawable.group_icon);
				holder.userName.setText(userItem.getUserName() + " [" + userItem.getSign() + "]");
			}
		}
		return convertView;
	}

	public class ViewHolder {
		public CheckBox selected; // 是否选择一组
		public TextView userName; // 好友名称
		public TextView userSign; // 好友名称
		public TextView isAttention;
		public ImageView userIcon; // 用户头像
	}

}
