package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.impl.UserGroupDaoIpml;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.response.GroupLbsInfo;

public class NearGroupAdapter extends BaseAdapter {
	private Context context;
	private List<GroupLbsInfo> groupList;
	private ImageResizer mImageWorker;

	public NearGroupAdapter(Context context, List<GroupLbsInfo> groupList) {
		this.context = context;
		this.groupList = groupList;
		((PangkerApplication) context.getApplicationContext()).getMyUserID();
		new UserGroupDaoIpml(context);
		this.mImageWorker = PangkerManager.getGroupIconResizer(context.getApplicationContext());
	}

	public int getCount() {
		if (groupList != null) {
			return groupList.size();
		}
		return 0;
	}

	public GroupLbsInfo getItem(int position) {
		return this.groupList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	ViewHolder holder;

	public View getView(int position, View rowView, ViewGroup parent) {
		final GroupLbsInfo item = getItem(position);
		if (rowView == null) {
			rowView = LayoutInflater.from(context).inflate(R.layout.group_lbs_list_item, null);
			holder = new ViewHolder();
			// 群组图标
			holder.groupIcon = (ImageView) rowView.findViewById(R.id.iv_icon);
			holder.iv_share = (ImageView) rowView.findViewById(R.id.iv_share);
			// ImageView addGroupBtn = (ImageView)
			// rowView.findViewById(R.id.icon);
			// 群组名称
			holder.groupName = (TextView) rowView.findViewById(R.id.tv_name);
			// 群组人员数
			holder.groupDistance = (TextView) rowView.findViewById(R.id.tv_right_top);
			// 群组标签
			holder.groupTag = (TextView) rowView.findViewById(R.id.tv_left_bottom);
			holder.userCount = (TextView) rowView.findViewById(R.id.tv_right_bottom);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		final String groupUrl = Configuration.getGroupCover() + item.getSid();
		mImageWorker.setLoadingImage(R.drawable.group_icon);
		mImageWorker.loadImage(groupUrl, holder.groupIcon, String.valueOf(item.getSid()));

		if (item.getIsShare()) {
			holder.iv_share.setVisibility(View.VISIBLE);
		} else {
			holder.iv_share.setVisibility(View.GONE);
		}
		holder.groupName.setText(item.getgName() != null ? item.getgName() : "");
		holder.userCount.setText(String.valueOf(item.getSum()) + "人在线");
		holder.groupTag.setText(item.getTags());
		holder.groupDistance.setText("累计" + item.getVisitedTimes().toString() + "人次");

		return rowView;
	}

	class ViewHolder {
		public ImageView groupIcon;
		public ImageView iv_share;
		public TextView groupName;
		public TextView groupDistance;
		public TextView groupTag;
		public TextView userCount;
	}

	public List<GroupLbsInfo> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<GroupLbsInfo> groupList) {
		this.groupList = groupList;
		this.notifyDataSetChanged();
	}

}
