package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.res.PictureInfoActivity;
import com.wachoo.pangker.activity.res.ResInfoActivity;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.ResGroupshare;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class GroupShareAdapter extends BaseAdapter {

	private Activity context;
	private List<ResGroupshare> resGroupshares;
	private PKIconResizer mImageResizer;

	ImageResizer mImageWorker;
	private PangkerApplication application;
	// >>>>>>>设置显示长度
	private InputFilter[] filters = { new InputFilter.LengthFilter(showLength) };
	private InputFilter[] morefilters = {};
	private static final int showLength = 200;
	private Handler handler = new Handler();

	public GroupShareAdapter(Activity context, List<ResGroupshare> resGroupshares, ImageResizer mImageWorker) {
		super();
		this.context = context;
		this.resGroupshares = resGroupshares;
		application = (PangkerApplication) this.context.getApplicationContext();

		this.mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		this.mImageWorker = mImageWorker;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return resGroupshares.size();
	}

	@Override
	public ResGroupshare getItem(int position) {
		// TODO Auto-generated method stub
		return resGroupshares.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public List<ResGroupshare> getResGroupshares() {
		return resGroupshares;
	}

	public void setResGroupshares(List<ResGroupshare> resGroupshares) {
		this.resGroupshares = resGroupshares;
		this.notifyDataSetChanged();
	}

	public void addResGroupshares(List<ResGroupshare> resGroupshares) {
		this.resGroupshares.addAll(resGroupshares);
		this.notifyDataSetChanged();
	}

	public void removeResGroupshares(ResGroupshare resGroupshare) {
		this.resGroupshares.remove(resGroupshare);
		this.notifyDataSetChanged();
	}

	private OnLongClickListener onDeleteLongClickListener;
	private OnLongClickListener onCoppyLongClickListener;

	public OnLongClickListener getOnDeleteLongClickListener() {
		return onDeleteLongClickListener;
	}

	public void setOnDeleteLongClickListener(OnLongClickListener onDeleteLongClickListener) {
		this.onDeleteLongClickListener = onDeleteLongClickListener;
	}

	public OnLongClickListener getOnCoppyLongClickListener() {
		return onCoppyLongClickListener;
	}

	public void setOnCoppyLongClickListener(OnLongClickListener onCoppyLongClickListener) {
		this.onCoppyLongClickListener = onCoppyLongClickListener;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		final ResGroupshare item = (ResGroupshare) getItem(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.res_recommend_item, null);
			holder.ivUserIcon = (ImageView) convertView.findViewById(R.id.iv_user_icon);
			holder.tvResDetail = (TextView) convertView.findViewById(R.id.tv_res_detail);
			holder.tvResName = (TextView) convertView.findViewById(R.id.tv_res_name);
			holder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_res_icon);
			holder.ivPicIcon = (ImageView) convertView.findViewById(R.id.iv_pic_icon);
			holder.tvUsername = (TextView) convertView.findViewById(R.id.tv_username);
			holder.tvResTime = (TextView) convertView.findViewById(R.id.res_tv_time);
			holder.liPic = (LinearLayout) convertView.findViewById(R.id.ly_pic);
			holder.tvPicName = (TextView) convertView.findViewById(R.id.tv_pic_name);
			holder.tvPicTime = (TextView) convertView.findViewById(R.id.pic_tv_time);
			holder.btnOpen = (TextView) convertView.findViewById(R.id.btn_open);
			holder.btn_talk = (TextView) convertView.findViewById(R.id.btn_talk);
			holder.lyResInfo = (LinearLayout) convertView.findViewById(R.id.ly_resinfo);
			holder.tvResonShow = (TextView) (TextView) convertView.findViewById(R.id.tv_reson_show);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.btn_talk.setVisibility(View.GONE);
		holder.ivUserIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, String.valueOf(item.getShareUid()));
				intent.putExtra(UserItem.USERNAME, item.getNickname());
				context.startActivity(intent);
			}
		});
		holder.btnOpen.setVisibility(View.GONE);

		if (item.getResId() != null) {
			final int resType = item.getResType();
			if (resType == PangkerConstant.RES_APPLICATION) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				String iconUrl = Configuration.getCoverDownload() + item.getResId();
				mImageWorker.setLoadingImage(R.drawable.photolist_head);
				mImageWorker.loadImage(iconUrl, holder.ivIcon, String.valueOf(item.getResId()));
				holder.tvResName.setVisibility(View.VISIBLE);
				holder.tvResName.setText(item.getResName());
			} else if (resType == PangkerConstant.RES_PICTURE) {
				holder.liPic.setVisibility(View.VISIBLE);
				holder.ivIcon.setVisibility(View.GONE);
				holder.ivPicIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setImageResource(R.drawable.icon46_photo);
				holder.tvPicName.setText(item.getResName());
				int sideLength = ImageUtil.getPicPreviewResolution(context);
				String iconUrl = ImageUtil.getWebImageURL(item.getResId(), sideLength);
				mImageWorker.loadImage(iconUrl, holder.ivPicIcon, String.valueOf(item.getResId()) + "_" + sideLength);
				Date date = Util.strToDate(item.getShareTime());
				holder.tvPicTime.setText(Util.getDateDiff(date));
				holder.tvResName.setVisibility(View.VISIBLE);
				holder.tvResName.setText(item.getResName());
				holder.btnOpen.setVisibility(View.VISIBLE);
				holder.btnOpen.setText("浏览");
				holder.btnOpen.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ArrayList<PictureViewInfo> imagePathes = new ArrayList<PictureViewInfo>();
						String imgUrl = Configuration.getImageUrl() + String.valueOf(item.getResId());
						PictureViewInfo pictureViewInfo = new PictureViewInfo();
						pictureViewInfo.setPicPath(imgUrl);
						imagePathes.add(pictureViewInfo);
						Intent intentPic = new Intent(context, PictureBrowseActivity.class);
						intentPic.putExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY, imagePathes);
						intentPic.putExtra("index", 0);
						context.startActivity(intentPic);
					}
				});

			} else if (resType == PangkerConstant.RES_MUSIC) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				holder.ivIcon.setImageResource(R.drawable.icon46_music);
				holder.tvResName.setVisibility(View.VISIBLE);
				holder.tvResName.setText(item.getResName());
				holder.btnOpen.setVisibility(View.VISIBLE);
				holder.btnOpen.setVisibility(View.GONE);
				holder.btnOpen.setText("听歌");
				holder.btnOpen.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						List<MusicInfo> musicInfos = new ArrayList<MusicInfo>();
						MusicInfo musicInfo = new MusicInfo();
						musicInfo.setSid(item.getResId());
						musicInfo.setMusicName(item.getResName());
						musicInfo.setSinger(item.getNickname());
						musicInfos.add(musicInfo);

						application.setMusiList(musicInfos);
						application.setPlayMusicIndex(0);
						Intent intentSerivce = new Intent(context, MusicService.class);
						intentSerivce.putExtra("control", PlayControl.MUSIC_START_PLAY);
						context.startService(intentSerivce);

						// >>>>>>>启动音乐播放界面
						Intent intentActivity = new Intent(context, MusicPlayActivity.class);
						context.startActivity(intentActivity);
					}
				});

			} else if (resType == PangkerConstant.RES_DOCUMENT) {
				holder.tvResName.setVisibility(View.VISIBLE);
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				holder.ivIcon.setImageResource(R.drawable.attachment_doc);
				holder.tvResName.setText(item.getResName());
			}
		} else {
			holder.liPic.setVisibility(View.GONE);
			holder.ivIcon.setVisibility(View.GONE);
			holder.ivPicIcon.setVisibility(View.GONE);
			holder.tvResName.setVisibility(View.GONE);
		}

		mImageResizer.loadImage(String.valueOf(item.getShareUid()), holder.ivUserIcon);
		// >>>>>>>>>>复制功能
		holder.tvResDetail.setTag(item);
		holder.tvResDetail.setOnLongClickListener(onCoppyLongClickListener);
		holder.lyResInfo.setTag(item);
		holder.lyResInfo.setOnLongClickListener(onDeleteLongClickListener);

		// >>>>>>点击查看详情
		holder.lyResInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (item.getResId() != null) {
					lookShare(item);
				}
			}
		});

		// >>>>>>>>推荐理由
		String reaSon = item.getShareReason();
		if (Util.isEmpty(reaSon)) {
			holder.tvResDetail.setVisibility(View.GONE);
			holder.tvResonShow.setVisibility(View.GONE);
		} else {
			holder.tvResDetail.setVisibility(View.VISIBLE);
			holder.tvResDetail.setFilters(item.getFilters());
			holder.tvResDetail.setText(reaSon);
			if (reaSon.length() > showLength) {
				holder.tvResonShow.setText(item.getBtnShowText());
				holder.tvResonShow.setVisibility(View.VISIBLE);
			} else
				holder.tvResonShow.setVisibility(View.GONE);
			holder.tvResonShow.setTag(position);
			// >>>>>>>>设置显示更多
			holder.tvResonShow.setOnClickListener(new AcceptOnClickListener(holder.tvResonShow, holder.tvResDetail,
					position));
		}
		holder.tvUsername.setText(item.getNickname());
		Date date = Util.strToDate(item.getShareTime());
		holder.tvResTime.setText(Util.getDateDiff(date));

		return convertView;
	}

	// 对推荐进行处理
	private void lookShare(final ResGroupshare info) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		if (info.getResType() == PangkerConstant.RES_PICTURE) {
			intent.setClass(context, PictureInfoActivity.class);
		} else {
			intent.setClass(context, ResInfoActivity.class);
		}
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();

		ResShowEntity entity = new ResShowEntity();
		entity.setCommentTimes(0);
		entity.setResType(info.getResType());
		entity.setDownloadTimes(0);
		entity.setFavorTimes(0);
		entity.setResID(info.getResId());
		entity.setScore(0);
		entity.setResName(info.getResName());
		entity.setShareUid(String.valueOf(info.getShareUid()));
		reList.add(entity);
		application.setResLists(reList);
		application.setViewIndex(0);
		context.startActivity(intent);
	}

	class AcceptOnClickListener implements View.OnClickListener {

		private TextView mShow;
		private TextView mDetail;
		private int position;

		public AcceptOnClickListener(TextView mShow, TextView mDetail, int position) {
			super();
			this.mShow = mShow;
			this.mDetail = mDetail;
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (mShow.getText().equals(PangkerConstant.SHOW_MORE_TEXT)) {

				handler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						mShow.setText(PangkerConstant.SHOW_UP_TEXT);
						mDetail.setFilters(morefilters);
						// >>>>>>>>设置按钮显示
						getItem(position).setBtnShowText(PangkerConstant.SHOW_UP_TEXT);
						getItem(position).setFilters(morefilters);
						mDetail.setText(getItem(position).getShareReason());
					}
				});

			} else {
				handler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						mShow.setText(PangkerConstant.SHOW_MORE_TEXT);
						mDetail.setFilters(filters);
						// >>>>>>>>设置按钮显示
						getItem(position).setBtnShowText(PangkerConstant.SHOW_MORE_TEXT);
						getItem(position).setFilters(filters);
						mDetail.setText(getItem(position).getShareReason());
					}
				});

			}
		}

	}

	/** 
	 *  
	 */
	class ViewHolder {
		ImageView ivIcon;
		ImageView ivUserIcon;
		TextView tvResDetail;
		TextView tvResName;
		TextView tvUsername;
		TextView tvResTime;
		LinearLayout liPic;
		LinearLayout lyResInfo;
		ImageView ivPicIcon;
		TextView tvPicName;
		TextView tvPicTime;
		TextView btnOpen;
		TextView tvResonShow;
		TextView btn_talk;
	}

}
