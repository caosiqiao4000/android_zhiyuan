package com.wachoo.pangker.adapter;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.res.FileManagerActivity;
import com.wachoo.pangker.audio.VoicePlayer;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.util.SmileyParser;
import com.wachoo.pangker.util.Util;

public class MsgHistoryAdapter extends BaseAdapter {

	private Activity activity;
	private List<ChatMessage> list;
	private SmileyParser mSmileyParser;
	private String mUserName;
	private UserItem userItem;
	// >>>>>>>>>获取本地图片
	private ImageLocalFetcher localFetcher;

	public MsgHistoryAdapter(Activity context, List<ChatMessage> list, UserItem userItem, ImageLocalFetcher localFetcher) {
		this.activity = context;
		this.list = list;

		mSmileyParser = new SmileyParser(context);
		this.userItem = userItem;
		mUserName = ((PangkerApplication) context.getApplicationContext()).getMySelf().getUserName();
		this.localFetcher = localFetcher;
	}

	public void setData(List<ChatMessage> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public ChatMessage getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {
		TextView txt_name;
		TextView txt_time;
		TextView txt_content;
		ImageView iv_cover;

		LinearLayout voice_layout;
		ImageView iv_voice;
		TextView txt_leng;
	}

	/**
	 * 判断文件是否可用，
	 * 
	 * @param msg
	 * @return
	 */
	private boolean isSuccessful(ChatMessage msg) {
		if (msg.getDirection().equals(ChatMessage.MSG_FROM_ME)) {
			return Util.isExitFileSD(msg.getFilepath());
		} else {
			if (msg.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
				return Util.isExitFileSD(msg.getFilepath());
			}
			return false;
		}
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		ChatMessage msg = getItem(position);
		ViewHolder viewholder;
		if (view == null) {
			view = LayoutInflater.from(activity).inflate(R.layout.msghistory_item, null);
			viewholder = new ViewHolder();
			viewholder.txt_name = (TextView) view.findViewById(R.id.txt_nickname);
			viewholder.txt_time = (TextView) view.findViewById(R.id.txt_time);
			viewholder.txt_content = (TextView) view.findViewById(R.id.txt_content);

			viewholder.iv_cover = (ImageView) view.findViewById(R.id.iv_cover);

			viewholder.voice_layout = (LinearLayout) view.findViewById(R.id.voice_layout);
			viewholder.iv_voice = (ImageView) view.findViewById(R.id.iv_voice);
			viewholder.txt_leng = (TextView) view.findViewById(R.id.contentfrom);
			view.setTag(viewholder);
		} else {
			viewholder = (ViewHolder) view.getTag();
		}
		// 名称
		if (msg.getDirection().equals(ChatMessage.MSG_TO_ME)) {
			viewholder.txt_name.setText(userItem.getUserName());
		} else {
			viewholder.txt_name.setText(mUserName);
		}
		// 时间
		Date oldDate = Util.strToDate(msg.getTime());
		viewholder.txt_time.setText(Util.getDateDiff(oldDate));
		// 内容
		if (msg.getMsgType() == PangkerConstant.RES_PICTURE) {
			viewholder.txt_content.setVisibility(View.GONE);
			viewholder.voice_layout.setVisibility(View.GONE);
			viewholder.iv_cover.setVisibility(View.VISIBLE);

			if (isSuccessful(msg)) {
				localFetcher.loadImage(msg.getFilepath(), viewholder.iv_cover, msg.getFilepath());
			} else {
				viewholder.iv_cover.setImageResource(R.drawable.download_failure_icon);
			}

		} else if (msg.getMsgType() == PangkerConstant.RES_VOICE) {
			viewholder.txt_content.setVisibility(View.GONE);
			viewholder.voice_layout.setVisibility(View.VISIBLE);
			viewholder.iv_cover.setVisibility(View.GONE);

			if (isSuccessful(msg)) {
				new ChatVoiceUI(activity, viewholder).initView(msg);
			} else {
				viewholder.iv_voice.setVisibility(View.GONE);
				viewholder.txt_leng.setText("[语音加载失败]");
			}

		} else {
			viewholder.txt_content.setVisibility(View.VISIBLE);
			viewholder.voice_layout.setVisibility(View.GONE);
			viewholder.iv_cover.setVisibility(View.GONE);
			viewholder.txt_content.setText(mSmileyParser.addSmileySpans(msg.getContent()));
		}
		return view;
	}

	class ChatVoiceUI implements View.OnClickListener {

		private ChatMessage msg;
		private ViewHolder viewholder;
		private boolean isPlay = false;
		private VoicePlayer voicePlayer;

		public ChatVoiceUI(Activity activity, ViewHolder viewholder) {
			// TODO Auto-generated constructor stub
			this.viewholder = viewholder;
			voicePlayer = VoicePlayer.getVoicePlayer();
			viewholder.voice_layout.setOnClickListener(this);
		}

		public void initView(ChatMessage msg) {
			this.msg = msg;
			viewholder.iv_voice.setVisibility(View.VISIBLE);
			viewholder.iv_voice.setImageResource(R.drawable.record_other_normal);
			viewholder.txt_leng.setText(msg.getContent());
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			isPlay = !isPlay;
			if (isPlay) {
				voicePlayer.play(msg.getFilepath(), new VoicePlayer.OnVoiceListener() {
					@Override
					public void onVoiceStatusListener(int state) {
						// TODO Auto-generated method stub
						if (state == -1) {
							showToast("抱歉，文件找不到!");
						} else if (state == 0) {
							isPlay = false;
							viewholder.iv_voice.setImageResource(R.drawable.record_other_normal);
						} else if (state == 1) {
							viewholder.iv_voice.setImageResource(R.drawable.ptt_action_l_1);
						} else if (state == 2) {
							viewholder.iv_voice.setImageResource(R.drawable.ptt_action_l_2);
						} else if (state == 3) {
							viewholder.iv_voice.setImageResource(R.drawable.ptt_action_l_3);
						}
					}
				});
			} else {
				voicePlayer.stop();
			}
		}
	}

	protected void showToast(String toast) {
		// TODO Auto-generated method stub
		Toast.makeText(activity, toast, Toast.LENGTH_SHORT).show();
	}

	protected void showToast(int toast) {
		// TODO Auto-generated method stub
		Toast.makeText(activity, toast, Toast.LENGTH_SHORT).show();
	}
}
