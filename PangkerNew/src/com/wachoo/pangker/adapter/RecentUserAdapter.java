package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;

/**
 * 
 */
public class RecentUserAdapter extends BaseAdapter {

	private Context context;
	private List<UserItem> userList;
	private PKIconResizer mImageResizer;

	public RecentUserAdapter(Context context, List<UserItem> userList) {
		this.context = context;
		this.userList = userList;
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return userList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		convertView = LayoutInflater.from(context).inflate(R.layout.recent_useritem, null);
		final ImageView ivUsericon = (ImageView) convertView.findViewById(R.id.iv_usericon);

		final UserItem userItem = (UserItem) getItem(position);

		mImageResizer.loadImage(userItem.getUserId(), ivUsericon);

		return convertView;
	}

	public List<UserItem> getUserList() {
		return userList;
	}

	public void setUserList(List<UserItem> userList) {
		this.userList = userList;
		this.notifyDataSetChanged();
	}

}
