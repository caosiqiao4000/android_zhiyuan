package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.response.SimpleUserInfo;
import com.wachoo.pangker.util.Util;

/**
 * 邀请用户/访客列表数据源
 * 
 * @author wangxin
 * 
 */
public class InvitedUserAdapter extends BaseAdapter {

	public static final int INVITED_USERS = 0;
	public static final int DRIFT_USERS = 1;
	public static final int VISIT_USERS = 2;

	private List<SimpleUserInfo> mInvitedUsers = new ArrayList<SimpleUserInfo>();
	private Context mContext;
	private PKIconResizer mImageResizer;
	private int ShowType;
	private PangkerApplication application;

	public InvitedUserAdapter(Context mContext, int ShowType, PKIconResizer mImageResizer) {
		this.mContext = mContext;
		this.ShowType = ShowType;
		this.mImageResizer = mImageResizer;
		application = (PangkerApplication) mContext.getApplicationContext();
	}

	public List<SimpleUserInfo> getmInvitedUsers() {
		return mInvitedUsers;
	}

	public void setmInvitedUsers(List<SimpleUserInfo> mInvitedUsers) {
		this.mInvitedUsers = mInvitedUsers;
		this.notifyDataSetChanged();
	}

	public int getShowType() {
		return ShowType;
	}

	public void setShowType(int showType) {
		ShowType = showType;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.mInvitedUsers == null ? 0 : this.mInvitedUsers.size();
	}

	@Override
	public SimpleUserInfo getItem(int location) {
		// TODO Auto-generated method stub
		return mInvitedUsers.get(location);
	}

	@Override
	public long getItemId(int location) {
		// TODO Auto-generated method stub
		return 0;
	}

	class ViewHolder {
		ImageView mUserIocn;
		TextView mUserName;
		TextView mPhone;
		TextView mLastLogintime;
		ImageView mPangker;
		TextView isRegister;
		ImageView mInfo;
	}

	@Override
	public View getView(int position, View currentView, ViewGroup viewGroup) {
		// TODO Auto-generated method stub

		final SimpleUserInfo invitedUser = getItem(position);

		ViewHolder holder = null;
		if (currentView == null) {
			currentView = LayoutInflater.from(mContext).inflate(R.layout.contacts_address_item, null);
			holder = new ViewHolder();
			holder.mUserIocn = (ImageView) currentView.findViewById(R.id.iv_usericon);
			holder.mUserName = (TextView) currentView.findViewById(R.id.tv_username);
			holder.mPhone = (TextView) currentView.findViewById(R.id.tv_user_phone);
			holder.mLastLogintime = (TextView) currentView.findViewById(R.id.tv_usersign);
			holder.isRegister = (TextView) currentView.findViewById(R.id.tv_identity);
			holder.mPangker = (ImageView) currentView.findViewById(R.id.iv_identity);
			holder.mInfo = (ImageView) currentView.findViewById(R.id.iv_into);
			currentView.setTag(holder);
		} else {
			holder = (ViewHolder) currentView.getTag();
		}

		switch (this.ShowType) {
		case INVITED_USERS:// 邀请用户
			holder.mPhone.setVisibility(View.VISIBLE);
			holder.mPhone.setText(invitedUser.getPhone());
			holder.isRegister.setVisibility(View.VISIBLE);
			holder.mLastLogintime.setVisibility(View.GONE);

			// >>>是否是Pangker用户
			if (invitedUser.getIsPangker() == SimpleUserInfo.NOT_PANGKER_USER) {
				holder.isRegister.setText(mContext.getString(R.string.pangker_not_register));
				holder.mPangker.setVisibility(View.GONE);
				holder.mInfo.setVisibility(View.GONE);
				holder.mUserName.setText(application.getLocalContactNameByPhone(invitedUser.getPhone()));
			} else {
				// >>>用户头像
				mImageResizer.loadImage(invitedUser.getUserId(), holder.mUserIocn);
				// >>>用户名
				holder.mUserName.setText(invitedUser.getNickName());
				// >>>最后登录时间
				holder.mPangker.setVisibility(View.VISIBLE);
				holder.mPangker.setImageResource(R.drawable.pangkefriends_icon);
				holder.mInfo.setVisibility(View.GONE);
				// >>>如果为空
				if (!Util.isEmpty(invitedUser.getDealTime())) {
					holder.isRegister.setText(mContext.getString(R.string.invite_last_login_time)
							+ Util.TimeIntervalBtwNow(invitedUser.getDealTime()));
				} else {
					holder.isRegister.setText(mContext.getString(R.string.invite_last_login_time)
							+ mContext.getString(R.string.unknown));
				}
			}
			break;

		case VISIT_USERS: // 访客
			// >>>用户头像
			mImageResizer.loadImage(invitedUser.getUserId(), holder.mUserIocn);
			// >>>用户名
			holder.mUserName.setText(invitedUser.getNickName());
			holder.mPhone.setVisibility(View.GONE);
			// >>>最后登录时间
			if (!Util.isEmpty(invitedUser.getDealTime())) {
				holder.mLastLogintime.setText(mContext.getString(R.string.visit_last_visit_time)
						+ Util.TimeIntervalBtwNow(invitedUser.getDealTime()));
			} else {
				holder.mLastLogintime.setText(mContext.getString(R.string.visit_last_visit_time)
						+ mContext.getString(R.string.unknown));
			}
			break;
		case DRIFT_USERS:
			// >>>用户头像
			mImageResizer.loadImage(invitedUser.getUserId(), holder.mUserIocn);
			// >>>用户名
			holder.mUserName.setText(invitedUser.getNickName());
			holder.mPhone.setVisibility(View.GONE);
			// >>>最后登录时间
			if (!Util.isEmpty(invitedUser.getDealTime())) {
				holder.mLastLogintime.setText(mContext.getString(R.string.drift_time)
						+ Util.TimeIntervalBtwNow(invitedUser.getDealTime()));
			} else {
				holder.mLastLogintime.setText(mContext.getString(R.string.drift_time)
						+ mContext.getString(R.string.unknown));
			}
			break;

		}

		return currentView;
	}
}
