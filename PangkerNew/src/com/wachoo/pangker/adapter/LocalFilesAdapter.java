package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.LocalFileItem;
import com.wachoo.pangker.image.ImageLocalFetcher;

public class LocalFilesAdapter extends BaseAdapter {

	private List<LocalFileItem> mFiles = new ArrayList<LocalFileItem>();
	private ImageLocalFetcher mImageWorker;
	private Activity mContext;

	public LocalFilesAdapter(Activity mContext, List<LocalFileItem> mFiles, ImageLocalFetcher mImageWorker) {
		super();
		this.mContext = mContext;
		this.mFiles = mFiles;
		this.mImageWorker = mImageWorker;
	}

	public List<LocalFileItem> getmFiles() {
		return mFiles;
	}

	public void setmFiles(List<LocalFileItem> mFiles) {
		this.mFiles = mFiles;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mFiles.size();
	}

	@Override
	public LocalFileItem getItem(int position) {
		// TODO Auto-generated method stub
		return mFiles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class LocalFileHolder {
		public ImageView iv_fileicon;
		public TextView tv_filename;
		public TextView tv_filesize;
	}

	@Override
	public View getView(int position, View view, ViewGroup group) {
		// TODO Auto-generated method stub
		LocalFileItem currentItem = getItem(position);
		LocalFileHolder holder = null;
		if (view == null) {
			view = LayoutInflater.from(mContext).inflate(R.layout.item_file_show, null);
			holder = new LocalFileHolder();
			holder.iv_fileicon = (ImageView) view.findViewById(R.id.iv_fileicon);
			holder.tv_filename = (TextView) view.findViewById(R.id.tv_filename);
			holder.tv_filesize = (TextView) view.findViewById(R.id.tv_filesize);
			view.setTag(holder);
		} else {
			holder = (LocalFileHolder) view.getTag();
		}
		if (currentItem.isPic()) {
			mImageWorker.loadImage(currentItem.getFilePath(), holder.iv_fileicon, currentItem.getFilePath());
		} else {
			holder.iv_fileicon.setImageResource(currentItem.getIconResID());
		}
		holder.tv_filename.setText(currentItem.getFileName());
		if (currentItem.getFileType() == LocalFileItem.DIRECTORY_TYPE) {
			holder.tv_filesize.setVisibility(View.INVISIBLE);
		} else {
			String size = Formatter.formatFileSize(mContext, currentItem.getFileSize());
			holder.tv_filesize.setVisibility(View.VISIBLE);
			holder.tv_filesize.setText("文件大小:" + size);
		}

		return view;
	}
}
