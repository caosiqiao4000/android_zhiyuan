package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.group.P2PUserInfo;
import com.wachoo.pangker.image.PKIconResizer;

public class ChatRoomMapUserAdapter extends BaseAdapter {

	private List<P2PUserInfo> members;
	private Context context;
	private boolean deleteable = false;

	private PKIconResizer mImageResizer;

	public ChatRoomMapUserAdapter(Context context) {
		super();
		this.context = context;
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	public List<P2PUserInfo> getMembers() {
		return members;
	}

	public void setMembers(List<P2PUserInfo> members) {
		this.members = members;
		this.notifyDataSetChanged();
	}

	public boolean isHide() {
		return deleteable;
	}

	public void setHide(boolean isHide) {
		this.deleteable = isHide;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return members == null ? 0 : members.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return members.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {
		private ImageView mIcon;
		private ImageView mFollow;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.image_item, null);
			viewHolder.mIcon = (ImageView) convertView.findViewById(R.id.imgitem_usericon);
			viewHolder.mFollow = (ImageView) convertView.findViewById(R.id.iv_follow);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final P2PUserInfo member = (P2PUserInfo) getItem(position);
		viewHolder.mFollow.setVisibility(View.GONE);
		if (member != null && member.getUID() != 0) {
			mImageResizer.loadImage(String.valueOf(member.getUID()), viewHolder.mIcon);
		} else {
			viewHolder.mIcon.setImageResource(R.drawable.selectcontents_bg);
		}

		return convertView;
	}
}
