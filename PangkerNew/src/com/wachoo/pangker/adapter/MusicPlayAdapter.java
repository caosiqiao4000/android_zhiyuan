package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.MusicoverLoader;
import com.wachoo.pangker.server.response.MusicInfo;

/**
 * @ClassName: ResMusicActivity
 * @Description: 音乐资源Activity
 * @author longxianwen
 * @date Mar 2, 2012 10:28:19 AM
 */
public class MusicPlayAdapter extends BaseAdapter {
	
	public static enum PlayerStatus {
		unseleted, paly, pause
	};

	private Activity context;
	private List<MusicInfo> musicList;
	// private List<PlayItem> mchecked;
	private int playingMusicIndex = -1;// >>>>> wangxin 当前播放歌曲的index
	private PlayerStatus playerStatus;// >>>>> wangxin 歌曲播放状态
	private MusicoverLoader musicoverLoader = MusicoverLoader.getMusicoverLoader();

	public MusicPlayAdapter(Activity context) {
		super();
		this.context = context;
		musicList = new ArrayList<MusicInfo>();
		musicoverLoader.setLoadingId(R.drawable.icon46_music);
		// mchecked = new ArrayList<PlayItem>();
	}

	public PlayerStatus getPlayerStatus() {
		return playerStatus;
	}

	public void setPlayerStatus(PlayerStatus playerStatus) {
		this.playerStatus = playerStatus;
	}

	public int getPlayingMusicIndex() {
		return playingMusicIndex;
	}

	public void setPlayingMusicIndex(int playingMusicIndex) {
		this.playingMusicIndex = playingMusicIndex;
	}

	public List<MusicInfo> getMusicList() {
		return musicList;
	}

	public void setMusicList(List<MusicInfo> musicList) {
		this.musicList = musicList;
		notifyDataSetChanged();
	}
	
	public void delMusic(MusicInfo musicInfo) {
		musicList.remove(musicInfo);
		notifyDataSetChanged();
	}
	
	public void addMusic(MusicInfo musicInfo) {
		musicList.add(musicInfo);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return musicList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return musicList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	ViewHolderMusic holder;
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MusicInfo item = musicList.get(position);
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.musicplay_item, null);
			holder = new ViewHolderMusic();
			holder.iv_musicCover = (ImageView) convertView.findViewById(R.id.ImageViewMusicCover);
			holder.tv_musicName = (TextView) convertView.findViewById(R.id.tv_musicName);
			holder.tv_musicPlayer = (TextView) convertView.findViewById(R.id.tv_musicPlayer);
			holder.img_ifPlay = (ImageView) convertView.findViewById(R.id.ImgIfPlay);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolderMusic) convertView.getTag();
		}

		if (item != null) {
			if (position == playingMusicIndex) {
				if (getPlayerStatus() == PlayerStatus.paly) {
					holder.img_ifPlay.setVisibility(View.VISIBLE);
					holder.img_ifPlay.setImageResource(R.drawable.musicplay_play_default);
				} else if (getPlayerStatus() == PlayerStatus.pause) {
					holder.img_ifPlay.setVisibility(View.VISIBLE);
					holder.img_ifPlay.setImageResource(R.drawable.musicplay_pause_default);
				}
			} else {
				holder.img_ifPlay.setVisibility(View.INVISIBLE);
			}
			//加载歌曲图片
			musicoverLoader.displayOneImage(item.getMusicId(), context, holder.iv_musicCover);
			holder.tv_musicName.setText(item.getMusicName());
			String singer = item.getSinger();
			if("<unknown>".equalsIgnoreCase(singer)){
				singer = "未知";
			}
			holder.tv_musicPlayer.setText("演唱者:" + singer);
		}
		return convertView;
	}

	public class ViewHolderMusic {
		public TextView tv_musicName; // 歌曲名
		public TextView tv_musicPlayer; // 歌手
		public ImageView img_ifPlay; // 是否是正在选择的播放或暂停
		public ImageView iv_musicCover; // 是否是正在选择的播放或暂停
	}

	public String toMp3(String name) {
		int search = name.indexOf(".mp3");
		String newName = name.substring(0, search);
		return newName;
	}

	/**
	 * 播放器进度条时间处理方法
	 * 
	 * @param time
	 * @return
	 */
	public String toTime(int time) {

		time /= 1000;
		int minute = time / 60;
		int second = time % 60;
		minute %= 60;
		return String.format("%02d:%02d", minute, second);
	}

}
