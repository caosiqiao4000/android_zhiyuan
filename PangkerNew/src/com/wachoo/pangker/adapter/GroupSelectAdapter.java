package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.UserItem;

public class GroupSelectAdapter extends BaseAdapter{

	private Context context;
	private List<ContactGroup> contactGroups;
	private List<Boolean> mchecked;
	
	public GroupSelectAdapter(Context context, List<ContactGroup> contactGroups) {
		super();
		this.context = context;
		this.contactGroups = contactGroups;
		mchecked = new ArrayList<Boolean>();
		for (int i = 0; i < contactGroups.size(); i++) {
			mchecked.add(false);
		}
	}
	
	public ArrayList<String> getSelectMembersId() {
		ArrayList<String> inviteIds = new ArrayList<String>();
		for (int i = 0; i < contactGroups.size(); i++) {
			if(mchecked.get(i)){
				for (UserItem item : contactGroups.get(i).getUserItems()) {
					inviteIds.add(item.getUserId());
				}
			}
		}
		return inviteIds;
	}
	
	public ArrayList<UserItem> getSelectMembers() {
		ArrayList<UserItem> inviteIds = new ArrayList<UserItem>();
		for (int i = 0; i < contactGroups.size(); i++) {
			if(mchecked.get(i)){
				for (UserItem item : contactGroups.get(i).getUserItems()) {
					inviteIds.add(item);
				}
			}
		}
		return inviteIds;
	}
	
	public int getSelectCount() {
		int count = 0;
		for (int i = 0; i < contactGroups.size(); i++) {
			if(mchecked.get(i)){
				count += contactGroups.get(i).getUserItems().size();
			}
		}
		return count;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return contactGroups.size();
	}
	
	public void selelctGroup(int index, boolean flag){
		mchecked.set(index, flag);
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return contactGroups.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return (contactGroups.get(position).getGid() != null);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ContactGroup item = (ContactGroup) getItem(position);
		ViewHolder holder;
		if(convertView == null){
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.select_group_item, null);
			holder.selected = (CheckBox) convertView.findViewById(R.id.mtrace_checkbox);
			holder.groupName = (TextView) convertView.findViewById(R.id.txtItem);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if(isEnabled(position)){
			holder.selected.setVisibility(View.VISIBLE);
			holder.groupName.setText(item.getName() + " [" + item.getCount() + "]");
		} else {
			holder.selected.setVisibility(View.GONE);
			holder.groupName.setText(item.getName());
		}
		holder.selected.setChecked(mchecked.get(position));
		return convertView;
	}
	
	public class ViewHolder {
		public CheckBox selected; // 是否选择一组
		public TextView groupName; // 好友名称
	}

}
