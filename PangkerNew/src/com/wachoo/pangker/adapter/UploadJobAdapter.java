package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.util.Util;

/**
 * 下载列表显示数据管理器
 * 
 * @author wangxin
 * 
 */
public class UploadJobAdapter extends BaseAdapter {

	private List<UploadJob> uploadJobs = new ArrayList<UploadJob>();

	private Activity mContext;

	// >>>>>>>>>获取本地图片
	private ImageApkIconFetcher apkIconFetcher;
	private ImageLocalFetcher localFetcher;

	public UploadJobAdapter(Activity mContext, ImageLocalFetcher localFetcher, ImageApkIconFetcher apkIconFetcher) {
		super();
		this.mContext = mContext;
		this.localFetcher = localFetcher;
		this.apkIconFetcher = apkIconFetcher;
	}

	public List<UploadJob> getUploadJobs() {
		return uploadJobs;
	}

	public void setUploadJobs(List<UploadJob> uploadJobs) {
		this.uploadJobs = uploadJobs;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return uploadJobs.size();
	}

	@Override
	public UploadJob getItem(int location) {
		// TODO Auto-generated method stub
		return uploadJobs.get(location);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {
		public ImageView resIcon; // 图标
		public TextView fileName; // 名称
		public TextView progress;// 下载百分比
		public TextView uploadTime; // 时间
		public ProgressBar progressBar; // 进度条
		public ImageView jobStatus; // 图标
	}

	ViewHolder viewHolder = null;

	@Override
	public View getView(int position, View currentView, ViewGroup viewGroup) {
		// TODO Auto-generated method stub
		final UploadJob uploadJob = getItem(position);

		// 获取view
		if (currentView == null) {
			currentView = LayoutInflater.from(mContext).inflate(R.layout.download_item, null);
			viewHolder = new ViewHolder();
			viewHolder.resIcon = (ImageView) currentView.findViewById(R.id.dwownload_logo);
			viewHolder.jobStatus = (ImageView) currentView.findViewById(R.id.download_pause);
			viewHolder.uploadTime = (TextView) currentView.findViewById(R.id.download_time);
			viewHolder.fileName = (TextView) currentView.findViewById(R.id.download_name);
			viewHolder.progress = (TextView) currentView.findViewById(R.id.download_progress);
			viewHolder.progressBar = (ProgressBar) currentView.findViewById(R.id.dwownload_pb);
			currentView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) currentView.getTag();
		}

		// >>>>>>赋值
		viewHolder.fileName.setText(uploadJob.getResName());
		viewHolder.uploadTime.setText(Util.getShowTimes(uploadJob.getUpTime()));
		switch (uploadJob.getType()) {
		case PangkerConstant.RES_APPLICATION:
			apkIconFetcher.loadImage(uploadJob.getFilePath(), viewHolder.resIcon, uploadJob.getFilePath());
			break;
		case PangkerConstant.RES_MUSIC:
			viewHolder.resIcon.setImageResource(R.drawable.icon46_music);
			break;
		case PangkerConstant.RES_DOCUMENT:
			viewHolder.resIcon.setImageResource(R.drawable.attachment_doc);
			break;
		case PangkerConstant.RES_PICTURE:
			localFetcher.loadImage(uploadJob.getFilePath(), viewHolder.resIcon, uploadJob.getFilePath());
			break;
		default:
			break;
		}
		if (uploadJob.getUploadType() == UploadJob.UPLOAD_START) {
			viewHolder.jobStatus.setVisibility(View.GONE);
			// >>>>>>如果上传中
			if (uploadJob.getStatus() == UploadJob.UPLOAD_UPLOAGINDG && uploadJob.getFilePart() != null) {
				// >>>>>>>>显示上传百分比例
				String donesize = Formatter.formatFileSize(mContext, uploadJob.getFilePart().getDone());
				String size = Formatter.formatFileSize(mContext, uploadJob.getFileSize());
				String proString = donesize + "/" + size + "("
						+ (uploadJob.getFilePart().getDone() * 100 / uploadJob.getFileSize()) + "%)";
				viewHolder.progress.setText(proString);
				int percent = (int) ((uploadJob.getFilePart().getDone() / (float) uploadJob.getFileSize()) * 100);
				viewHolder.progressBar.setVisibility(View.VISIBLE);
				viewHolder.progressBar.setProgress(percent);
			}
			// >>>>>>>上传失败
			else if (uploadJob.getStatus() == UploadJob.UPLOAD_FAILED) {
				viewHolder.progress.setText("上传失败");
				viewHolder.progressBar.setVisibility(View.GONE);
			}
			// >>>>>>>上传取消
			else if (uploadJob.getStatus() == UploadJob.UPLOAD_CANCEL) {
				viewHolder.progress.setText("取消上传");
				viewHolder.progressBar.setVisibility(View.GONE);
			}
			// >>>>>>>上传成功
			else if (uploadJob.getStatus() == UploadJob.UPLOAD_SUCCESS) {
				viewHolder.progress.setText("上传成功");
				viewHolder.progressBar.setVisibility(View.GONE);
			}
			// >>>>>>>上传成功
			else if (uploadJob.getStatus() == UploadJob.UPLOAD_INITOK) {
				viewHolder.progress.setText("等待上传");
				viewHolder.progressBar.setVisibility(View.GONE);
			}
			// >>>>>>>上传异常
			else {
				viewHolder.progress.setText("上传文件异常");
				viewHolder.progressBar.setVisibility(View.GONE);
			}
		} else {
			viewHolder.progress.setText("等待上传");
			viewHolder.progressBar.setVisibility(View.GONE);
			viewHolder.jobStatus.setVisibility(View.VISIBLE);
			viewHolder.jobStatus.setImageResource(R.drawable.date_icon_lv);
		}
		return currentView;
	}
}
