package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.group.P2PUserInfo;
import com.wachoo.pangker.image.PKIconResizer;

/**
 * 
 */
public class ChatRoomUserListAdapter extends BaseAdapter {
	private Context context;
	private List<P2PUserInfo> userList;
	// private boolean isDeleteState = false;// GridView是否在选择删除状态
	private boolean isCreater = false; // 当前用户是否创建者
	private OnClickListener mAddClickListener;

	private PKIconResizer mImageResizer;

	public ChatRoomUserListAdapter(Context context, List<P2PUserInfo> userList) {
		this.context = context;
		this.userList = userList;
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userList.size() + 1;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return userList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	private class ViewHolder {
		ImageView ivExpress;
		TextView tvUsername;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.gv_useritem, null);
			holder.ivExpress = (ImageView) convertView.findViewById(R.id.iv_usericon);
			holder.tvUsername = (TextView) convertView.findViewById(R.id.tv_username);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.tvUsername.setVisibility(View.GONE);
		if (position < getCount() - 1) {
			P2PUserInfo mUserItem = userList.get(position);
			holder.tvUsername.setText(mUserItem.getUserName());
			holder.tvUsername.setVisibility(View.VISIBLE);
			mImageResizer.loadImage(String.valueOf(mUserItem.getUID()), holder.ivExpress);

		} else if (position == getCount() - 1) {
			convertView.setOnClickListener(mAddClickListener);
			holder.ivExpress.setImageResource(R.drawable.btn_add_selector);
		}

		return convertView;
	}

	public List<P2PUserInfo> getUserList() {
		return userList;
	}

	public void setUserList(List<P2PUserInfo> userList) {
		this.userList = userList;
		this.notifyDataSetChanged();
	}

	public void addUserList(List<P2PUserInfo> mUserList) {
		// this.userList.addAll(mUserList);
		for (P2PUserInfo userItem : mUserList) {
			if (!isExist(userItem)) {
				this.userList.add(userItem);
			}
		}
		this.notifyDataSetChanged();
	}

	private boolean isExist(P2PUserInfo user) {
		for (P2PUserInfo userItem : this.userList) {
			if (userItem.getUID() != user.getUID()) {
				return true;
			}
		}
		return false;
	}

	public OnClickListener getmAddClickListener() {
		return mAddClickListener;
	}

	/**
	 * void TODO添加成员监听
	 * 
	 * @param mAddClickListener
	 */
	public void setmAddClickListener(OnClickListener mAddClickListener) {
		this.mAddClickListener = mAddClickListener;
	}

	public boolean isCreater() {
		return isCreater;
	}

	/**
	 * void TODO 设置是否群组创建者
	 * 
	 * @param isCreater
	 */
	public void setCreater(boolean isCreater) {
		this.isCreater = isCreater;
	}

}
