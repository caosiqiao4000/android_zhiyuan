package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.response.Commentinfo;
import com.wachoo.pangker.util.Util;

public class ResCommentAdapter extends BaseAdapter {

	private Context context;
	private List<Commentinfo> commentinfos;
	private PKIconResizer mImageResizer;

	private View.OnClickListener onClickListener;

	public ResCommentAdapter(Context context, List<Commentinfo> commentinfos) {
		super();
		this.context = context;
		this.commentinfos = new ArrayList<Commentinfo>();
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());

	}

	// 添加一条评论
	public void addComment(Commentinfo commentinfo) {
		commentinfo.setComment(true);
		commentinfos.add(0, commentinfo);
		notifyDataSetChanged();
	}

	// 添加一条评论的回复
	public void addCommentReply(String commentId, Commentinfo commentinfo) {

		notifyDataSetChanged();
	}

	// 初始化列表
	public void setCommentinfos(List<Commentinfo> commentinfos) {
		this.commentinfos.clear();

		notifyDataSetChanged();
	}

	// 加载等多的数据
	public void addCommentinfos(List<Commentinfo> commentinfos) {

		notifyDataSetChanged();
	}

	public void setOnClickListener(View.OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return commentinfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return commentinfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	ViewHolder holder = null;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Commentinfo commentinfo = (Commentinfo) getItem(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.comment_item, null);
			holder.userIcon = (ImageView) convertView.findViewById(R.id.comment_userincon);
			holder.userName = (TextView) convertView.findViewById(R.id.comment_username);
			holder.commTime = (TextView) convertView.findViewById(R.id.comment_time);
			holder.commContent = (TextView) convertView.findViewById(R.id.comment_content);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.userName.setText(commentinfo.getUsername());
		holder.commTime.setText(Util.TimeIntervalBtwNow(commentinfo.getCommenttime()));
		holder.commContent.setText(commentinfo.getContent());
		if (!commentinfo.isComment()) {
			// holder.userIcon.setPadding(30, holder.userIcon.getPaddingTop(),
			// 0, holder.userIcon.getPaddingBottom());
			holder.txtReply.setVisibility(View.GONE);
		}
		holder.txtReply.setTag(commentinfo);
		holder.txtReply.setOnClickListener(onClickListener);

		mImageResizer.loadImage(commentinfo.getUserid(), holder.userIcon);

		return convertView;
	}

	/** 
	 *  
	 *
	 */
	class ViewHolder {
		ImageView userIcon;
		TextView userName;
		TextView commTime;
		TextView commContent;
		TextView txtReply;

	}
}
