package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.LocalContacts;

/**
 * 
 * @author zhengjy
 * 
 */
public class LocalContactSelectAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private List<LocalContacts> mUserList;
	/**
	 * @return the mUserList
	 */
	public List<LocalContacts> getmUserList() {
		return mUserList;
	}

	/**
	 * @param mUserList
	 *            the mUserList to set
	 */
	public void setmUserList(List<LocalContacts> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	public LocalContactSelectAdapter(Context context, List<LocalContacts> users) {
		layoutInflater = LayoutInflater.from(context);
		this.mUserList = users;
	}

	public int getCount() {
		return mUserList == null ? 0 : mUserList.size();
	}

	public Object getItem(int position) {
		if (mUserList != null) {
			return mUserList.get(position);
		}
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder viewHolder = null;
		if(convertView == null){
			viewHolder = new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.local_contact_select_item, null);
			viewHolder.iv_userIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			viewHolder.tv_userName = (TextView) convertView.findViewById(R.id.tv_username);
			viewHolder.tv_phone_num = (TextView) convertView.findViewById(R.id.tv_usersign);
			viewHolder.iv_identity = (ImageView) convertView.findViewById(R.id.iv_identity);
			viewHolder.tv_identity = (TextView) convertView.findViewById(R.id.tv_identity);
			convertView.setTag(viewHolder);
		}
		else viewHolder =(ViewHolder)convertView.getTag();

		LocalContacts item = mUserList.get(position);
		viewHolder.tv_userName.setText(item.getUserName());
		viewHolder.tv_phone_num.setText(item.getPhoneNum());

		if (item.isHasPhoto()) {
			viewHolder.iv_userIcon.setImageBitmap(item.getPhoto());
		}
		else 
			viewHolder.iv_userIcon.setImageResource(R.drawable.nav_head);
		
		if (item.getRelation() == LocalContacts.RELATION_P_USER) {
			viewHolder.iv_identity.setVisibility(View.VISIBLE);
			viewHolder.iv_identity.setImageResource(R.drawable.pangker);
			viewHolder.tv_identity.setText("(旁客)");
		} else if (item.getRelation() == LocalContacts.RELATION_P_FRIEND) {
			viewHolder.iv_identity.setVisibility(View.VISIBLE);
			viewHolder.iv_identity.setImageResource(R.drawable.nav_head);
			viewHolder.tv_identity.setText("(好友)");
		} else{
			viewHolder.iv_identity.setVisibility(View.GONE);
			viewHolder.tv_identity.setText("(未注册)");
		}
			

		return convertView;
	}
	
	private class ViewHolder{
		public ImageView iv_userIcon; //
		public TextView tv_userName;   //
		public TextView tv_phone_num;   
		public ImageView iv_identity;   //
		public TextView tv_identity;  //
	}

}
