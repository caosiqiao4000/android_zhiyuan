package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.util.MapUtil;
import com.wachoo.pangker.util.Util;

/**
 * FedestrianAdapter 行人Adapter
 * 
 * @author zhengjy
 * 
 *         edit by wangxin
 * 
 */
public class NearUserListAdapter extends BaseAdapter {
	private Context mContext;
	private List<UserItem> mUserList;
	private PKIconResizer mImageResizer = null;
	private String mUserId;

	/**
	 * 构造函数
	 * 
	 * @param context
	 * @param list
	 */
	public NearUserListAdapter(Context context, List<UserItem> list) {
		this.mContext = context;
		this.mUserList = list;
		this.mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		mUserId = ((PangkerApplication) context.getApplicationContext()).getMyUserID();

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mUserList.size();
	}

	@Override
	public UserItem getItem(int position) {
		// TODO Auto-generated method stub
		return mUserList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	private double getDistance(double lat, double lon) {
		double myLat = 0;
		double myLon = 0;
		if (((PangkerApplication) mContext.getApplicationContext()).getLocateType() == PangkerConstant.LOCATE_CLIENT) {
			myLat = ((PangkerApplication) mContext.getApplicationContext()).getCurrentLocation().getLatitude();
			myLon = ((PangkerApplication) mContext.getApplicationContext()).getCurrentLocation().getLongitude();
		} else {
			myLat = ((PangkerApplication) mContext.getApplicationContext()).getDirftLocation().getLatitude();
			myLon = ((PangkerApplication) mContext.getApplicationContext()).getDirftLocation().getLongitude();
		}
		return MapUtil.DistanceOfTwoPoints(myLat, myLon, lat, lon);
	}

	ViewHolder holder;

	@Override
	public View getView(final int position, View contactView, ViewGroup parent) {
		final UserItem item = getItem(position);
		if (contactView == null) {
			contactView = LayoutInflater.from(mContext).inflate(R.layout.near_userlist_item, null);
			holder = new ViewHolder();

			holder.ly_list_user_item = (LinearLayout) contactView.findViewById(R.id.ly_list_user_item);
			holder.userName = (TextView) contactView.findViewById(R.id.tv_name); // 昵称
			holder.sign = (TextView) contactView.findViewById(R.id.tv_sign);// 交友宣言
			holder.age = (TextView) contactView.findViewById(R.id.tv_age);// 年龄
			holder.sex = (ImageView) contactView.findViewById(R.id.iv_sex); // 性别

			holder.status = (ImageView) contactView.findViewById(R.id.iv_status);
			holder.distance = (TextView) contactView.findViewById(R.id.tv_distance);// 距离
			holder.usericon = (ImageView) contactView.findViewById(R.id.iv_usericon); // 用户头像
			holder.time = (TextView) contactView.findViewById(R.id.tv_time); // 最近登录时间
			holder.iv_wifi_icon = (ImageView) contactView.findViewById(R.id.iv_wifi_icon); // wifi定位标识
			contactView.setTag(holder);
		} else {
			holder = (ViewHolder) contactView.getTag();
		}
		holder.userName.setText(item.getUserName());// 用户名
		if (PangkerConstant.NEAR_USER_BOYS.equals(item.getSex())) {
			holder.sex.setImageResource(R.drawable.icon24_m);
		} else if (PangkerConstant.NEAR_USER_GIRLS.equals(item.getSex())) {
			holder.sex.setImageResource(R.drawable.icon24_g);
		}
		holder.sign.setText(item.getSign());// 签名
		if (item.getIsWifiLocate() != null && item.getIsWifiLocate() == 1) {
			holder.iv_wifi_icon.setVisibility(View.VISIBLE);
			holder.distance.setText(R.string.near_user_wifi);
			holder.time.setText(R.string.near_user_online); //
		} else {
			holder.iv_wifi_icon.setVisibility(View.GONE);
			if (item.getLocation() != null) {
				double distance = getDistance(item.getLocation().getLatitude(), item.getLocation().getLongitude());
				holder.distance.setText(MapUtil.getUserDistance(distance));// 距离
			} else if (item.getLat() != null && item.getLon() != null) {
				double distance = getDistance(item.getLat(), item.getLon());
				holder.distance.setText(MapUtil.getUserDistance(distance));// 距离
			}
			holder.time.setText(Util.TimeIntervalBtwNow(item.getLastUpdateTime())); // 时间间隔
		}

		if (item.getAge() == null) {
			item.setAge("0");
		}
		holder.age.setText(item.getAge() + "岁");// 年龄

		mImageResizer.loadImage(item.getUserId(), holder.usericon);

		holder.ly_list_user_item.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (item.getUserId().equals(mUserId)) {
					Toast.makeText(mContext, R.string.isyourself, Toast.LENGTH_SHORT).show();
				} else
					goToUserInfo(item);
			}
		});
		return contactView;
	}

	/**
	 * 查看用户信息
	 * 
	 * @param intent
	 */
	private void goToUserInfo(final UserItem item) {

		Intent intent = new Intent(mContext, UserWebSideActivity.class);
		intent.putExtra("userid", item.getUserId());
		intent.putExtra("username", item.getUserName());
		intent.putExtra("usersign", item.getSign());
		mContext.startActivity(intent);
	}

	class ViewHolder {
		public LinearLayout ly_list_user_item;
		public TextView userName; // 昵称
		public TextView sign;// 交友宣言
		public TextView age;// 年龄
		public ImageView sex; // 性别
		public ImageView status;
		public TextView time;
		public TextView distance;
		public ImageView usericon;
		public ImageView iv_wifi_icon;
	}

	public List<UserItem> getmUserList() {
		return mUserList;
	}

	public void setmUserList(List<UserItem> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	public void addWifiUserList(List<UserItem> wifiUserList) {
		for (UserItem userWifiItem : wifiUserList) {
			for (UserItem userItem : this.mUserList) {
				if (userWifiItem.getUserId().equals(userItem.getUserId())) {
					this.mUserList.remove(userItem);
					break;
				}
			}
		}
		this.mUserList.addAll(0, wifiUserList);
		this.notifyDataSetChanged();
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
	}

}
