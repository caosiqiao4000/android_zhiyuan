package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.util.Util;

public class ContactsSelectedAdapter extends BaseAdapter {
	
	private List<UserItem> userItems;
	private Context context;
	private boolean deleteable = false;
	private PKIconResizer mImageResizer;
	private UserItem defUserItem;
	private AddressBooks addressBooks;

	public ContactsSelectedAdapter(Context context) {
		super();
		this.context = context;
		userItems = new ArrayList<UserItem>();
		defUserItem = new UserItem();
		defUserItem.setUserId(null);
		userItems.add(defUserItem);
		mImageResizer =PangkerManager.getUserIconResizer(context.getApplicationContext());
	}
	
	public void clearUserItems() {
		// TODO Auto-generated method stub
		userItems.clear();
		userItems.add(defUserItem);
		notifyDataSetChanged();
	}

	public void dealUserItem(List<UserItem> items, boolean flag) {
		for (UserItem item : items) {
			dealUserItem(item, flag);
		}
	}

	public List<UserItem> getUserItems() {
		userItems.remove(defUserItem);
		return userItems;
	}

	private boolean contains(UserItem item) {
		Iterator<UserItem> iter = userItems.iterator();
		while (iter.hasNext()) {
			UserItem one = iter.next();
			if (item.getUserId().equals(one.getUserId())) {
				return true;
			}
		}
		return false;
	}

	public void dealUserItem(UserItem item, boolean flag) {
		if (flag) {
			if (!contains(item)) {
				userItems.add(userItems.size() - 1, item);
				notifyDataSetChanged();
			}
		} else {
			if (userItems.contains(item)) {
				userItems.remove(item);
				notifyDataSetChanged();
			}
		}
	}

	public boolean isHide() {
		return deleteable;
	}

	public void setHide(boolean isHide) {
		this.deleteable = isHide;
	}

	public ArrayList<String> getTraceIdList() {
		ArrayList<String> traceInList = new ArrayList<String>();
		for (UserItem item : userItems) {
			if (!Util.isEmpty(item.getUserId())) {
				traceInList.add(item.getUserId());
			}
		}
		return traceInList;
	}

	/**
	 * 
	 * @param position
	 * @return
	 */
	public String removeUser(int position) {
		String userid = getItem(position).getUserId();
		if(!Util.isEmpty(userid)){
			userItems.remove(position);
			this.notifyDataSetChanged();
			return userid;
		}
		return null;
	}

	public void setUserItems(List<UserItem> userItems) {
		this.userItems = userItems;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userItems.size();
	}

	@Override
	public UserItem getItem(int position) {
		// TODO Auto-generated method stub
		return userItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView imageView = null;
		final UserItem userItem = (UserItem) getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.image_item, null);

			imageView = (ImageView) convertView.findViewById(R.id.imgitem_usericon);
			convertView.setTag(imageView);
		} else {
			imageView = (ImageView) convertView.getTag();
		}
		convertView.findViewById(R.id.iv_follow).setVisibility(View.GONE);
		
		if (Util.isEmpty(userItem.getUserId()) || "-1".equals(userItem.getUserId())) {
			imageView.setImageResource(R.drawable.selectcontents_bg);
		} else {
			mImageResizer.loadImage(userItem.getUserId(), imageView);
		}
		return convertView;
	}
}
