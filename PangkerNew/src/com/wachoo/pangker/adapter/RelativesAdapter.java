package com.wachoo.pangker.adapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.ClipboardManager;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.group.ChatRoomInfoActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.activity.pangker.RelativesDynamicActivity;
import com.wachoo.pangker.activity.res.PictureInfoActivity;
import com.wachoo.pangker.activity.res.RecommendReceiverAcitvity;
import com.wachoo.pangker.activity.res.ResInfoActivity;
import com.wachoo.pangker.activity.res.ResSpeaktInfoActivity;
import com.wachoo.pangker.entity.DetailedAddress;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.TextSpeakInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.RelativesResInfo;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class RelativesAdapter extends BaseAdapter {

	private List<RelativesResInfo> mResInfosList;
	private Activity mActivity;
	private PKIconResizer mImageResizer;

	// >>>>>>获取图片
	protected ImageFetcher mImageWorker;
	protected ImageCache mImageCache;

	private PangkerApplication application;
	// >>>>>>>设置显示长度
	private InputFilter[] filters = { new InputFilter.LengthFilter(RelativesResInfo.showLength) };
	private InputFilter[] morefilters = {};

	private Handler handler = new Handler();
	private OnLongClickListener onLongClickListener;
	private OnClickListener locationClickListener;

	private PoiaddressLoader poiaddressLoader;

	public void setLocationClickListener(OnClickListener locationClickListener) {
		this.locationClickListener = locationClickListener;
	}

	public OnLongClickListener getOnLongClickListener() {
		return onLongClickListener;
	}

	public void setOnLongClickListener(OnLongClickListener onLongClickListener) {
		this.onLongClickListener = onLongClickListener;
	}

	public RelativesAdapter(List<RelativesResInfo> mResInfosList, Activity mActivity, ImageFetcher mImageWorker) {
		super();
		this.mResInfosList = mResInfosList;
		this.mActivity = mActivity;
		application = (PangkerApplication) this.mActivity.getApplicationContext();
		this.mImageResizer = PangkerManager.getUserIconResizer(mActivity.getApplicationContext());
		this.mImageWorker = mImageWorker;
		this.poiaddressLoader = new PoiaddressLoader(this.mActivity.getApplicationContext());
	}

	public void setRelativesResInfos(List<RelativesResInfo> relativesResInfos) {
		// TODO Auto-generated method stub
		this.mResInfosList = relativesResInfos;
		notifyDataSetChanged();
	}

	public void addRelativesResInfos(List<RelativesResInfo> relativesResInfos) {
		// TODO Auto-generated method stub
		this.mResInfosList.addAll(relativesResInfos);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mResInfosList.size();
	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public RelativesResInfo getItem(int position) {
		// TODO Auto-generated method stub
		return mResInfosList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		Integer viewType = getItem(position).getResType();
		if (viewType != null) {
			if (viewType == PangkerConstant.RES_PICTURE)
				return 0;
			else
				return 1;
		}
		return 1;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		final RelativesResInfo item = (RelativesResInfo) getItem(position);
		if (convertView == null) {
			if (item.getResType() != null && item.getResType() == PangkerConstant.RES_PICTURE) {
				holder = new ViewHolder();
				convertView = LayoutInflater.from(mActivity).inflate(R.layout.res_recommend_pic_item, null);
				holder.ivUserIcon = (ImageView) convertView.findViewById(R.id.iv_user_icon);
				holder.tvResDetail = (TextView) convertView.findViewById(R.id.tv_res_detail);
				holder.tvResName = (TextView) convertView.findViewById(R.id.tv_res_name);
				holder.ivPicIcon = (ImageView) convertView.findViewById(R.id.iv_pic_icon);
				holder.tvUsername = (TextView) convertView.findViewById(R.id.tv_username);
				holder.tv_restype = (TextView) convertView.findViewById(R.id.tv_restype);
				holder.lyPic = (RelativeLayout) convertView.findViewById(R.id.ly_pic);
				holder.tvPicName = (TextView) convertView.findViewById(R.id.tv_pic_name);
				holder.tvPicTime = (TextView) convertView.findViewById(R.id.pic_tv_time);
				holder.tvResonShow = (TextView) convertView.findViewById(R.id.tv_reson_show);
				// >>>>>>>>>>>位置信息
				holder.tvDistance = (TextView) convertView.findViewById(R.id.tv_distance);
				holder.btnLocate = (Button) convertView.findViewById(R.id.btn_locate);
				convertView.setTag(holder);
			} else {
				holder = new ViewHolder();
				convertView = LayoutInflater.from(mActivity).inflate(R.layout.res_recommend_res_item, null);
				holder.lyResInfo = (RelativeLayout) convertView.findViewById(R.id.ly_resinfo);
				holder.ivUserIcon = (ImageView) convertView.findViewById(R.id.iv_user_icon);
				holder.tvResDetail = (TextView) convertView.findViewById(R.id.tv_res_detail);
				holder.tvResName = (TextView) convertView.findViewById(R.id.tv_res_name);
				holder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_res_icon);
				holder.tvUsername = (TextView) convertView.findViewById(R.id.tv_username);
				holder.tvResTime = (TextView) convertView.findViewById(R.id.res_tv_time);
				holder.tvResonShow = (TextView) convertView.findViewById(R.id.tv_reson_show);
				// >>>>>>>>>>>位置信息
				holder.tvDistance = (TextView) convertView.findViewById(R.id.tv_distance);
				holder.btnLocate = (Button) convertView.findViewById(R.id.btn_locate);
				holder.tv_restype = (TextView) convertView.findViewById(R.id.tv_restype);
				convertView.setTag(holder);
			}
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// >>>>>>>亲友与当前用户之间的距离
		holder.tvDistance.setVisibility(View.VISIBLE);

		holder.btnLocate.setVisibility(View.VISIBLE);
		holder.btnLocate.setTag(item);
		holder.btnLocate.setOnClickListener(locationClickListener);
		holder.ivUserIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mActivity, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, String.valueOf(item.getRuid()));
				intent.putExtra(UserItem.USERNAME, item.getUserName());
				mActivity.startActivity(intent);
			}
		});
		holder.tvUsername.setText(item.getUserName());
		holder.tvUsername.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mActivity, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, String.valueOf(item.getRuid()));
				intent.putExtra(UserItem.USERNAME, item.getUserName());
				mActivity.startActivity(intent);
			}
		});

		holder.tvResDetail.setOnClickListener(null);
		// >>>>>>>>>没有资源上传的界面
		if (item.getResId() == null || item.getResType() == null) {
			holder.tvResonShow.setVisibility(View.GONE);
			holder.lyResInfo.setVisibility(View.GONE);
			holder.tvResDetail.setText("他太懒了,还没有上传、转播、收藏过任何资源");
			holder.tv_restype.setVisibility(View.GONE);
		}
		// >>>>>>>>>>上传过资源的亲友
		else {
			holder.tv_restype.setVisibility(View.VISIBLE);
			holder.tvResonShow.setVisibility(View.VISIBLE);
			final int resType = item.getResType();

			// >>>>>>>图片资源
			if (resType == PangkerConstant.RES_PICTURE) {
				// >>>>>>>>>需要判断是否为null
				if (item.getLocation() != null && item.getLocation().getLatitude() > 20
						&& item.getLocation().getLongitude() > 10) {
					poiaddressLoader.onAddressCityAreaLoad(item.getLocation(), mActivity, holder.tvDistance);
				} else {
					holder.tvDistance.setText(DetailedAddress.ADDRESS_UN_KNOW);
				}
				holder.lyPic.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setImageResource(R.drawable.icon46_photo);
				holder.tvPicName.setText(item.getResName());
				int sideLength = ImageUtil.getPicPreviewResolution(mActivity);
				String iconUrl = ImageUtil.getWebImageURL(item.getResId(), sideLength);
				try {
					mImageWorker.loadImage(iconUrl, holder.ivPicIcon, String.valueOf(item.getResId()) + "_"
							+ sideLength);
				} catch (OutOfMemoryError err) {
					err.getStackTrace();
				}

				holder.tvPicTime.setText(Util.getDateDiff(Util.strToDate(item.getCreateTime())));

				holder.tvPicTime.setText(Util.getDateDiff(Util.strToDate(item.getCreateTime())));
				// >>>>>>点击查看详情
				holder.lyPic.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stu
						lookRecommend(item);
					}
				});
				holder.lyPic.setTag(item);
				holder.lyPic.setOnLongClickListener(onLongClickListener);
				if (item.getResSource() == 1) {
					holder.tv_restype.setText("收藏了图片");
				} else if (item.getResSource() == 2) {
					holder.tv_restype.setText("转播了图片");
				} else {
					holder.tv_restype.setText("上传了图片");
				}
			}
			// >>>>>>>>>>其他资源
			else {
				holder.lyResInfo.setVisibility(View.VISIBLE);
				// >>>>>>>>>>>应用资源
				if (resType == PangkerConstant.RES_APPLICATION) {
					holder.tvResName.setVisibility(View.VISIBLE);
					holder.ivIcon.setVisibility(View.VISIBLE);
					holder.lyResInfo.setEnabled(true);
					// >>>>>>>>>需要判断是否为null
					if (item.getLocation() != null && item.getLocation().getLatitude() > 20
							&& item.getLocation().getLongitude() > 10) {
						poiaddressLoader.onAddressCityAreaLoad(item.getLocation(), mActivity, holder.tvDistance);
					} else {
						holder.tvDistance.setText(DetailedAddress.ADDRESS_UN_KNOW);
					}
					String iconUrl = Configuration.getCoverDownload() + item.getResId();
					try {
						mImageWorker.setLoadingImage(R.drawable.photolist_head);
						mImageWorker.loadImage(iconUrl, holder.ivIcon, String.valueOf(item.getResId()));
					} catch (OutOfMemoryError err) {
						err.getStackTrace();
					}
					holder.tvResName.setText(item.getResName());
					if (item.getResSource() == 1) {
						holder.tv_restype.setText("收藏了应用");
					} else if (item.getResSource() == 2) {
						holder.tv_restype.setText("转播了应用");
					} else {
						holder.tv_restype.setText("上传了应用");
					}
				}
				// >>>>>>>>音乐资源
				else if (resType == PangkerConstant.RES_MUSIC) {
					holder.tvResName.setVisibility(View.VISIBLE);
					holder.ivIcon.setVisibility(View.VISIBLE);
					holder.lyResInfo.setEnabled(true);
					// >>>>>>>>>需要判断是否为null
					if (item.getLocation() != null && item.getLocation().getLatitude() > 20
							&& item.getLocation().getLongitude() > 10) {
						poiaddressLoader.onAddressCityAreaLoad(item.getLocation(), mActivity, holder.tvDistance);
					} else {
						holder.tvDistance.setText(DetailedAddress.ADDRESS_UN_KNOW);
					}
					holder.ivIcon.setImageResource(R.drawable.icon46_music);
					holder.tvResName.setText(item.getResName());
					if (item.getResSource() == 1) {
						holder.tv_restype.setText("收藏了音乐");
					} else if (item.getResSource() == 2) {
						holder.tv_restype.setText("转播了音乐");
					} else {
						holder.tv_restype.setText("上传了音乐");
					}
				}
				// >>>>>>>>>>>文档资源
				else if (resType == PangkerConstant.RES_DOCUMENT) {
					holder.tvResName.setVisibility(View.VISIBLE);
					holder.ivIcon.setVisibility(View.VISIBLE);
					holder.lyResInfo.setEnabled(true);
					// >>>>>>>>>需要判断是否为null
					if (item.getLocation() != null && item.getLocation().getLatitude() > 20
							&& item.getLocation().getLongitude() > 10) {
						poiaddressLoader.onAddressCityAreaLoad(item.getLocation(), mActivity, holder.tvDistance);
					} else {
						holder.tvDistance.setText(DetailedAddress.ADDRESS_UN_KNOW);
					}
					holder.ivIcon.setImageResource(R.drawable.attachment_doc);
					holder.tvResName.setText(item.getResName());
					if (item.getResSource() == 1) {
						holder.tv_restype.setText("收藏了文档");
					} else if (item.getResSource() == 2) {
						holder.tv_restype.setText("转播了文档");
					} else {
						holder.tv_restype.setText("上传了文档");
					}
				}
				// >>>>>>>>>说说文字
				else if (resType == PangkerConstant.RES_SPEAK) {
					holder.tvResName.setVisibility(View.GONE);
					holder.ivIcon.setVisibility(View.GONE);
					holder.lyResInfo.setEnabled(false);
					holder.tv_restype.setText("发布了说说");
					// >>>>>>>>>需要判断是否为null
					if (item.getLocation() != null && item.getLocation().getLatitude() > 20
							&& item.getLocation().getLongitude() > 10) {
						poiaddressLoader.onAddressCityAreaLoad(item.getLocation(), mActivity, holder.tvDistance);
					} else {
						holder.tvDistance.setText(DetailedAddress.ADDRESS_UN_KNOW);
					}
					holder.tvResDetail.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							lookSpeakinfo(item);
						}
					});
				}
				// >>>>位置信息
				else if (resType == PangkerConstant.RES_LOCATION) {
					holder.ivIcon.setVisibility(View.GONE);
					holder.tvResName.setVisibility(View.GONE);
					holder.lyResInfo.setEnabled(false);
					holder.tv_restype.setText("更新了位置");
					// >>>>>>>>>需要判断是否为null
					if (item.getLocation() != null && item.getLocation().getLatitude() > 20
							&& item.getLocation().getLongitude() > 10) {
						poiaddressLoader.onAddressCityChangedLoad(item.getLocation(), mActivity, holder.tvDistance,
								holder.tvResDetail);
					} else {
						holder.tvDistance.setText(DetailedAddress.ADDRESS_UN_KNOW);
					}
				} else if (resType == PangkerConstant.RES_SIGN) {
					holder.ivIcon.setVisibility(View.GONE);
					holder.tvResName.setVisibility(View.GONE);
					holder.lyResInfo.setEnabled(false);
					holder.tv_restype.setText("更新了签名");
					holder.tvDistance.setText("");
				}
				holder.tvUsername.setText(item.getUserName());
				holder.tvResTime.setText(Util.getDateDiff(Util.strToDate(item.getCreateTime())));

				if (resType != PangkerConstant.RES_LOCATION && resType != PangkerConstant.RES_SPEAK) {
					// >>>>>>点击查看详情
					holder.lyResInfo.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							lookRecommend(item);
						}
					});
					holder.lyResInfo.setTag(item);
					holder.lyResInfo.setOnLongClickListener(onLongClickListener);
				}
			}

			// >>>>>>>如果是显示位置信息case
			if (resType != PangkerConstant.RES_LOCATION) {
				// >>>>>>>>推荐理由
				String reaSon = item.getResDesc();
				if (Util.isEmpty(reaSon)) {
					reaSon = "他太懒了,没有书写任何描述";
				}
				holder.tvResDetail.setFilters(item.getFilters());

				if (reaSon.length() > RelativesResInfo.showLength) {
					holder.tvResonShow.setVisibility(View.VISIBLE);
					holder.tvResonShow.setText(item.getBtnShowText());
				} else
					holder.tvResonShow.setVisibility(View.GONE);
				holder.tvResonShow.setTag(position);
				// >>>>>>>>设置显示更多
				holder.tvResonShow.setOnClickListener(new AcceptOnClickListener(holder.tvResonShow, holder.tvResDetail,
						position));
				holder.tvResDetail.setText(reaSon);
				// >>>>>>>>>>复制功能
				holder.tvResDetail.setOnLongClickListener(new View.OnLongClickListener() {
					@Override
					public boolean onLongClick(View v) {
						// TODO Auto-generated method stub
						if (!Util.isEmpty(item.getResDesc())) {
							showPopMenu(item.getResDesc());
						}
						return true;
					}
				});
			} else {
				holder.tvResonShow.setVisibility(View.GONE);
			}
		}
		try {
			mImageResizer.loadImage(item.getRuid().toString(), holder.ivUserIcon);
		} catch (OutOfMemoryError err) {
			err.getStackTrace();
		}
		return convertView;
	}

	private class ViewHolder {
		ImageView ivIcon;
		ImageView ivUserIcon;
		TextView tvResDetail;
		TextView tvResName;
		TextView tvUsername;
		TextView tvResTime;
		RelativeLayout lyPic;
		RelativeLayout lyResInfo;
		ImageView ivPicIcon;
		TextView tvPicName;
		TextView tvPicTime;
		TextView tvResonShow;
		Button btnLocate;
		TextView tvDistance;
		TextView tv_restype;
	}

	class AcceptOnClickListener implements View.OnClickListener {

		private TextView mShow;
		private TextView mDetail;
		private int position;

		public AcceptOnClickListener(TextView mShow, TextView mDetail, int position) {
			super();
			this.mShow = mShow;
			this.mDetail = mDetail;
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (mShow.getText().equals(PangkerConstant.SHOW_MORE_TEXT)) {
				handler.post(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						mShow.setText(PangkerConstant.SHOW_UP_TEXT);
						mDetail.setFilters(morefilters);
						// >>>>>>>>设置按钮显示
						getItem(position).setBtnShowText(PangkerConstant.SHOW_UP_TEXT);
						getItem(position).setFilters(morefilters);
						mDetail.setText(getItem(position).getResDesc());
					}
				});
			} else {
				handler.post(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						mShow.setText(PangkerConstant.SHOW_MORE_TEXT);
						mDetail.setFilters(filters);
						// >>>>>>>>设置按钮显示
						getItem(position).setBtnShowText(PangkerConstant.SHOW_MORE_TEXT);
						getItem(position).setFilters(filters);
						mDetail.setText(getItem(position).getResDesc());
					}
				});
			}
		}
	}

	private void lookSpeakinfo(RelativesResInfo item) {
		// TODO Auto-generated method stub
		TextSpeakInfo textSpeakInfo = new TextSpeakInfo();
		textSpeakInfo.setId(item.getResId());
		textSpeakInfo.setUserId(String.valueOf(item.getRuid()));
		textSpeakInfo.setSpeakTime(item.getCreateTime());
		textSpeakInfo.setContent(item.getResDesc());
		textSpeakInfo.setUserName(item.getUserName());
		Intent intent = new Intent();
		intent.setClass(mActivity, ResSpeaktInfoActivity.class);
		intent.putExtra(ResSpeaktInfoActivity.TEXT_SPEAK_INFO_KEY, textSpeakInfo);
		mActivity.startActivity(intent);
	}

	private void lookChatRoomInfo(RelativesResInfo info) {
		Intent intent = new Intent();
		List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
		GroupLbsInfo lbsInfo = new GroupLbsInfo();
		lbsInfo.setgName(info.getResName());
		lbsInfo.setSid(info.getResId());
		lbsInfo.setUid(info.getRuid());
		lbsInfo.setShareUid(String.valueOf(info.getRuid()));
		groupList.add(lbsInfo);
		intent.putExtra("GroupInfo", (Serializable) groupList);
		intent.putExtra("group_index", 0);
		intent.setClass(mActivity, ChatRoomInfoActivity.class);
		mActivity.startActivity(intent);
	}

	// 对推荐进行处理
	private void lookRecommend(RelativesResInfo info) {
		// TODO Auto-generated method stub
		if (info.getResType() == PangkerConstant.RES_BROADCAST) {
			lookChatRoomInfo(info);
		} else {
			Intent intent = new Intent();
			if (info.getResType() == PangkerConstant.RES_PICTURE) {
				intent.setClass(mActivity, PictureInfoActivity.class);
			} else
				intent.setClass(mActivity, ResInfoActivity.class);

			List<ResShowEntity> reList = new ArrayList<ResShowEntity>();

			ResShowEntity entity = new ResShowEntity();
			entity.setKeep(info.getResSource() == 1);
			entity.setCommentTimes(0);
			entity.setResType(info.getResType());
			entity.setDownloadTimes(0);
			entity.setFavorTimes(0);
			entity.setResID(info.getResId());
			entity.setScore(0);
			entity.setResName(info.getResName());
			entity.setShareUid(String.valueOf(info.getRuid()));
			reList.add(entity);
			application.setResLists(reList);
			application.setViewIndex(0);
			mActivity.startActivity(intent);
		}
	}

	private PopMenuDialog dialog;

	private void showPopMenu(final String connent) {
		// TODO Auto-generated method stub
		if (dialog == null) {
			dialog = new PopMenuDialog(mActivity);
			dialog.setListener(new UITableView.ClickListener() {
				@Override
				public void onClick(int actionid) {
					// TODO Auto-generated method stub
					if (actionid == 1) {
						ClipboardManager clip = (ClipboardManager) mActivity
								.getSystemService(Context.CLIPBOARD_SERVICE);
						clip.setText(connent);
					}
					dialog.dismiss();
				}
			});
		}
		dialog.setTitle("文字信息");
		dialog.setMenus(new ActionItem[] { new ActionItem(1, "复制") });
		dialog.show();
	}

	/**
	 * 位置鉴权
	 * 
	 * @author wubo
	 * @createtime May 15, 2012
	 * @param friendId
	 */
	public void locationCheck(final RelativesResInfo item) {
		RelativesDynamicActivity trace = (RelativesDynamicActivity) mActivity;
		trace.locationCheck(item);
	}

}
