package com.wachoo.pangker.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.util.MapUtil;

public class TraceLitsAdapetr extends BaseAdapter {

	private Activity context;
	private List<UserItem> userItems;

	private PangkerApplication application;
	private PKIconResizer mImageResizer;
	private PoiaddressLoader poiLoader;

	public TraceLitsAdapetr(Activity context, List<UserItem> userItems) {
		super();
		this.context = context;
		application = (PangkerApplication) context.getApplicationContext();
		this.userItems = userItems;
		poiLoader = new PoiaddressLoader(application);
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	public void setUserItems(List<UserItem> userItems) {
		this.userItems = userItems;
		notifyDataSetChanged();
	}

	public void delUserItems(UserItem userItem) {
		// TODO Auto-generated method stub
		userItems.remove(userItem);
		notifyDataSetChanged();
	}

	public UserItem getUserItemById(String uid) {
		for (UserItem user : userItems) {
			if (uid.equals(user.getUserId())) {
				return user;
			}
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return userItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	private int getDistance(double lat, double lon) {
		double myLat = 0;
		double myLon = 0;
		if (application.getLocateType() == PangkerConstant.LOCATE_CLIENT) {
			myLat = application.getCurrentLocation().getLatitude();
			myLon = application.getCurrentLocation().getLongitude();
		} else {
			myLat = application.getDirftLocation().getLatitude();
			myLon = application.getDirftLocation().getLongitude();
		}
		return MapUtil.DistanceOfTwoPoints(myLat, myLon, lat, lon);
	}

	ViewHolder holder;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final UserItem item = (UserItem) getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.tracelist_item, null);
			holder = new ViewHolder();
			holder.userIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			holder.userName = (TextView) convertView.findViewById(R.id.trace_username);
			holder.txtDistance = (TextView) convertView.findViewById(R.id.trace_distance);
			holder.txtPoi = (TextView) convertView.findViewById(R.id.trace_poi);
			holder.txtPoi.setSelected(true);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.userName.setText(item.getAvailableName());
		// >>>>>>>>>需要判断是否为null
		if (item.getLatitude() != null && item.getLongitude() != null && item.getLatitude() > 20
				&& item.getLongitude() > 10) {
			int distance = getDistance(item.getLatitude(), item.getLongitude());
			holder.txtDistance.setText(MapUtil.getFriendDistance(distance));
		} else {
			holder.txtDistance.setText("未知");
		}
		mImageResizer.loadImage(item.getUserId(), holder.userIcon);
		holder.userIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, item.getUserId());
				intent.putExtra(UserItem.USERNAME, item.getUserName());
				intent.putExtra(UserItem.USERSIGN, item.getSign());
				context.startActivity(intent);
			}
		});
		poiLoader.onPoiLoader(item.getLocation(), context, holder.txtPoi);
		return convertView;
	}

	public class ViewHolder {
		ImageView userIcon;
		TextView userName;
		TextView txtDistance;
		TextView txtPoi;
	}

}
