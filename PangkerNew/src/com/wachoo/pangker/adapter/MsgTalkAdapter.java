package com.wachoo.pangker.adapter;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.db.IUserMsgDao;
import com.wachoo.pangker.db.impl.UserMsgDaoImpl;
import com.wachoo.pangker.downupload.ChatAcceptFileUI;
import com.wachoo.pangker.downupload.ChatAcceptPicUI;
import com.wachoo.pangker.downupload.ChatAcceptVoiceUI;
import com.wachoo.pangker.downupload.ChatFileUI;
import com.wachoo.pangker.downupload.ChatSendFileUI;
import com.wachoo.pangker.downupload.ChatSendPicUI;
import com.wachoo.pangker.downupload.ChatSendVoiceUI;
import com.wachoo.pangker.downupload.ChatSpeakUI;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.util.Util;

public class MsgTalkAdapter extends BaseAdapter {

	private Activity context;
	public List<ChatMessage> msgs = null;
	private PangkerApplication application;

	private IUserMsgDao msgDao;
	private PKIconResizer mImageResizer = null;

	private OnLongClickListener onLongClickListener;
	// >>>>>>>>>获取本地图片
	private ImageLocalFetcher localFetcher;
	private ImageApkIconFetcher imageApkIconFetcher;

	public MsgTalkAdapter(Activity context, List<ChatMessage> msgs, ImageApkIconFetcher imageApkIconFetcher,
			ImageLocalFetcher localFetcher) {
		this.context = context;
		this.msgs = msgs;
		application = (PangkerApplication) context.getApplicationContext();
		msgDao = new UserMsgDaoImpl(context);
		this.mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		this.imageApkIconFetcher = imageApkIconFetcher;
		this.localFetcher = localFetcher;
	}

	public class MessageSendItemHolder {
		public TextView tv_time;
		public ImageView iv_status;
		public ProgressBar pb_message_received;
	}

	public class MessageViewHolder extends MessageSendItemHolder {
		public LinearLayout msgLLayout;// 所有界面的点击事件的源
		public RelativeLayout msgRLayout;// 所有界面的点击事件的源
		public ImageView iv_usericon;
		public TextView tv_content;
		public TextView tv_fileize;
		public ImageView iv_imagecover;

		public ProgressBar pb_progressBar;
		public ImageView iv_sms_message;
		public Button btn_accept;
		public Button btn_reject;
		public Button btn_cancel;

		public ImageButton imgbtn_send_fail;
	}

	public int getCount() {
		return msgs.size();
	}

	@Override
	public int getItemViewType(int position) {
		int itemViewType = 0;
		ChatMessage msg = msgs.get(position);
		if (msg.getDirection().equals(ChatMessage.MSG_TO_ME)) {

			switch (msg.getMsgType()) {

			case PangkerConstant.RES_SPEAK: // 文字会话
			case PangkerConstant.RES_TOUCH: // 碰一下
			case PangkerConstant.RES_LOCATION: // 位置信息

				itemViewType = 0;
				break;
			case PangkerConstant.RES_MUSIC: // 音乐文件
			case PangkerConstant.RES_APPLICATION:// 应用文件
			case PangkerConstant.RES_DOCUMENT:// 文本文件
				itemViewType = 1;
				break;
			case PangkerConstant.RES_PICTURE:// 图片接受
				itemViewType = 2;
				break;
			case PangkerConstant.RES_VOICE:
				itemViewType = 3;
				break;
			}
		} else {
			switch (msg.getMsgType()) {
			case PangkerConstant.RES_SPEAK:// 文字聊天信息
			case PangkerConstant.RES_TOUCH: // 碰一下
			case PangkerConstant.RES_CALL: // 碰一下
			case PangkerConstant.RES_LOCATION:// 位置信息
			case PangkerConstant.RES_SPEAK_SMS:// 通过短信通道发送私信（对方不在线case）
				itemViewType = 4;
				break;
			case PangkerConstant.RES_MUSIC: // 音乐文件
			case PangkerConstant.RES_APPLICATION:// 应用文件
			case PangkerConstant.RES_DOCUMENT:// 文本文件
				itemViewType = 5;
				break;
			case PangkerConstant.RES_PICTURE:// 图片接受
				itemViewType = 6;
				break;
			case PangkerConstant.RES_VOICE:
				itemViewType = 7;
				break;
			}
		}
		return itemViewType;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 10;
	}

	public Object getItem(int position) {
		return msgs.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public void setOnLongClickListener(OnLongClickListener onLongClickListener) {
		this.onLongClickListener = onLongClickListener;
	}

	/**
	 * 获取当前发送的文件个数
	 * 
	 * @return
	 */
	public int getSendingLenght() {
		int count = 0;
		for (ChatMessage msg : msgs) {
			if (ChatMessage.MSG_FROM_ME.equals(msg.getDirection())
					&& (PangkerConstant.RES_DOCUMENT == msg.getMsgType()
							|| PangkerConstant.RES_APPLICATION == msg.getMsgType()
							|| PangkerConstant.RES_PICTURE == msg.getMsgType() || PangkerConstant.RES_MUSIC == msg
							.getMsgType())) {
				count++;
			}
		}
		return count;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ChatMessage msg = msgs.get(position);
		MessageViewHolder viewHolder = null;
		// >>>>>>>获取说话时间
		Date speakDate = Util.strToDate(msg.getTime());
		if (position == 0) {
			lastSayTime = 0;
		} else {
			// 获取上一次说话时间
			lastSayTime = Util.strToDate(msgs.get(position - 1).getTime()).getTime();
		}
		// >>>>>>判断是否为空

		if (msg.getDirection().equals(ChatMessage.MSG_TO_ME)) {
			convertView = initAcceptView(msg, convertView, speakDate, viewHolder);
		} else if (msg.getDirection().equals(ChatMessage.MSG_FROM_ME)) {
			convertView = initSendView(msg, convertView, speakDate, viewHolder);
		}

		// >>>>>>>>>为了实现消除发送通知的pb
		if (msg.getDirection().equals(ChatMessage.MSG_FROM_ME)) {
			MessageSendItemHolder itemHolder = (MessageSendItemHolder) convertView.getTag();
			messageSendStatus(itemHolder, msg);
		}

		return convertView;
	}

	/**
	 * messageSendStatus
	 * 
	 * @param viewHolder
	 * @param chatMessage
	 */
	private void messageSendStatus(MessageSendItemHolder viewHolder, ChatMessage chatMessage) {
		if (chatMessage.getMessageRevc() == 0) {
			if (viewHolder.pb_message_received != null) {
				viewHolder.pb_message_received.setVisibility(View.VISIBLE);
				// >>>>>>>>>>判断是否大于发送时间
				try {
					if (Util.dateDiffNow(chatMessage.getTime()) > ChatMessage.RESEND_CHATMESSAGE_TIME) {
						chatMessage.setMessageRevc(2);
						msgDao.updateMessageReceived(chatMessage);
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} else {
			if (viewHolder.pb_message_received != null) {
				viewHolder.pb_message_received.setVisibility(View.GONE);
			}
		}
	}

	// >>>>>>>>最后一次对话时间(如果时间超过2分钟，显示时间，如果不超过，不需要显示)
	private long lastSayTime = 0l;

	/**
	 * 发送消息
	 * 
	 * @param msgType
	 * @param msg
	 * @return
	 */
	private View initSendView(final ChatMessage msg, View convertView, Date oldDate, MessageViewHolder viewHolder) {

		switch (msg.getMsgType()) {
		case PangkerConstant.RES_SPEAK:// 文字聊天信息
		case PangkerConstant.RES_TOUCH: // 碰一下
		case PangkerConstant.RES_CALL: // 呼叫
		case PangkerConstant.RES_LOCATION:// 位置信息
			if (convertView == null) {
				viewHolder = new MessageViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.message_talk_view_location_to, null);
				viewHolder.iv_sms_message = (ImageView) convertView.findViewById(R.id.iv_sms_message);
				viewHolder.tv_content = (TextView) convertView.findViewById(R.id.contentto);
				viewHolder.msgLLayout = (LinearLayout) convertView.findViewById(R.id.msg_layout);
				viewHolder.tv_time = (TextView) convertView.findViewById(R.id.timeto);
				viewHolder.pb_message_received = (ProgressBar) convertView.findViewById(R.id.pb_message_received);
				viewHolder.imgbtn_send_fail = (ImageButton) convertView.findViewById(R.id.imgbtn_send_fail);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (MessageViewHolder) convertView.getTag();
			}
			viewHolder.tv_content.setTag(msg);
			viewHolder.tv_content.setOnLongClickListener(onLongClickListener);
			new ChatSpeakUI(viewHolder, context, imageApkIconFetcher).initView(msg);
			showSpeakTime(oldDate, viewHolder.tv_time);
			break;
		case PangkerConstant.RES_SPEAK_SMS:// 通过短信通道发送私信（对方不在线case）
			if (convertView == null) {
				viewHolder = new MessageViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.message_talk_view_to, null);
				viewHolder.iv_sms_message = (ImageView) convertView.findViewById(R.id.iv_sms_message);
				viewHolder.tv_content = (TextView) convertView.findViewById(R.id.contentto);
				viewHolder.msgLLayout = (LinearLayout) convertView.findViewById(R.id.msg_layout);
				viewHolder.tv_time = (TextView) convertView.findViewById(R.id.timeto);
				viewHolder.pb_message_received = (ProgressBar) convertView.findViewById(R.id.pb_message_received);
				viewHolder.imgbtn_send_fail = (ImageButton) convertView.findViewById(R.id.imgbtn_send_fail);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (MessageViewHolder) convertView.getTag();
			}
			viewHolder.tv_content.setTag(msg);
			viewHolder.tv_content.setOnLongClickListener(onLongClickListener);
			new ChatSpeakUI(viewHolder, context, imageApkIconFetcher).initView(msg);
			showSpeakTime(oldDate, viewHolder.tv_time);
			break;

		case PangkerConstant.RES_MUSIC: // 音乐文件
		case PangkerConstant.RES_APPLICATION:// 应用文件
		case PangkerConstant.RES_DOCUMENT:// 文本文件
			if (convertView == null) {
				viewHolder = new MessageViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.message_talk_view_sendfile, null);
				viewHolder.iv_imagecover = (ImageView) convertView.findViewById(R.id.imageCover);
				viewHolder.msgRLayout = (RelativeLayout) convertView.findViewById(R.id.msg_layout);
				viewHolder.tv_fileize = (TextView) convertView.findViewById(R.id.content_size);
				viewHolder.tv_content = (TextView) convertView.findViewById(R.id.content_send);
				viewHolder.tv_time = (TextView) convertView.findViewById(R.id.timefrom);
				viewHolder.pb_progressBar = (ProgressBar) convertView.findViewById(R.id.dwownload_pb);
				viewHolder.btn_cancel = (Button) convertView.findViewById(R.id.btn_file_cancel);
				viewHolder.imgbtn_send_fail = (ImageButton) convertView.findViewById(R.id.imgbtn_send_fail);
				viewHolder.pb_message_received = (ProgressBar) convertView.findViewById(R.id.pb_message_received);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (MessageViewHolder) convertView.getTag();
			}
			viewHolder.msgRLayout.setTag(msg);
			viewHolder.msgRLayout.setOnLongClickListener(onLongClickListener);
			new ChatSendFileUI(viewHolder, context, msgDao, application, imageApkIconFetcher).initView(msg);

			showSpeakTime(oldDate, viewHolder.tv_time);
			break;

		case PangkerConstant.RES_PICTURE:// 图片发送
			if (convertView == null) {
				viewHolder = new MessageViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.message_talk_view_sendpic, null);
				viewHolder.msgRLayout = (RelativeLayout) convertView.findViewById(R.id.msg_layout);
				viewHolder.iv_imagecover = (ImageView) convertView.findViewById(R.id.imageCover);
				viewHolder.tv_time = (TextView) convertView.findViewById(R.id.timefrom);
				viewHolder.iv_status = (ImageView) convertView.findViewById(R.id.iv_status);
				viewHolder.pb_progressBar = (ProgressBar) convertView.findViewById(R.id.dwownload_pb);
				viewHolder.imgbtn_send_fail = (ImageButton) convertView.findViewById(R.id.imgbtn_send_fail);
				viewHolder.pb_message_received = (ProgressBar) convertView.findViewById(R.id.pb_message_received);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (MessageViewHolder) convertView.getTag();
			}
			viewHolder.msgRLayout.setTag(msg);
			viewHolder.msgRLayout.setOnLongClickListener(onLongClickListener);
			new ChatSendPicUI(viewHolder, context, msgDao, application, localFetcher, imageApkIconFetcher)
					.initView(msg);

			// >>>>>>判断时间分隔
			showSpeakTime(oldDate, viewHolder.tv_time);

			break;
		case PangkerConstant.RES_VOICE:// 语音信息

			if (convertView == null) {
				viewHolder = new MessageViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.message_talk_view_to, null);
				viewHolder.iv_sms_message = (ImageView) convertView.findViewById(R.id.iv_sms_message);
				viewHolder.tv_content = (TextView) convertView.findViewById(R.id.contentto);
				viewHolder.msgLLayout = (LinearLayout) convertView.findViewById(R.id.msg_layout);
				viewHolder.tv_time = (TextView) convertView.findViewById(R.id.timeto);
				viewHolder.imgbtn_send_fail = (ImageButton) convertView.findViewById(R.id.imgbtn_send_fail);
				viewHolder.pb_message_received = (ProgressBar) convertView.findViewById(R.id.pb_message_received);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (MessageViewHolder) convertView.getTag();
			}

			// 是否通过短信发送消息
			viewHolder.tv_content.setTag(msg);
			viewHolder.tv_content.setOnLongClickListener(onLongClickListener);

			new ChatSendVoiceUI(viewHolder, context, msgDao, imageApkIconFetcher).initView(msg);
			// >>>>>>判断时间分隔

			showSpeakTime(oldDate, viewHolder.tv_time);

			break;
		}
		// >>>>>>>失败
		if (msg.getMessageRevc() == 2) {
			viewHolder.imgbtn_send_fail.setVisibility(View.VISIBLE);
			viewHolder.imgbtn_send_fail.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					MessageTipDialog noticeDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
						@Override
						public void buttonResult(boolean isSubmit) {
							// TODO Auto-generated method stub
							if (isSubmit) {
								reSendMessage(msg);
							}
						}
					}, context);
					noticeDialog.showDialog("确定重新发送此消息?", false);
				}
			});
		} else {
			viewHolder.imgbtn_send_fail.setVisibility(View.GONE);
		}
		return convertView;
	}

	/**
	 * 从新发送私信会话
	 * 
	 * @param msg
	 */
	private void reSendMessage(ChatMessage msg) {
		application.getOpenfireManager().sendChatMessage(msg);
	}

	private void showSpeakTime(Date speakDate, TextView mTimeTextView) {
		long speakTime = speakDate.getTime();
		if (speakTime - lastSayTime >= PangkerConstant.SPLIT_TIME) {
			mTimeTextView.setVisibility(View.VISIBLE);
			mTimeTextView.setText(Util.getDateDiff(speakDate));
		} else {
			mTimeTextView.setVisibility(View.GONE);
		}
	}

	/**************************************************************************************************/
	private View initAcceptView(final ChatMessage msg, View convertView, Date oldDate, MessageViewHolder viewHolder) {

		switch (msg.getMsgType()) {
		case PangkerConstant.RES_SPEAK: // 文字会话
		case PangkerConstant.RES_TOUCH: // 碰一下
		case PangkerConstant.RES_LOCATION: // 位置信息
			if (convertView == null) {
				viewHolder = new MessageViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.message_talk_view_location_from, null);
				viewHolder.msgLLayout = (LinearLayout) convertView.findViewById(R.id.msg_layout);
				viewHolder.iv_usericon = (ImageView) convertView.findViewById(R.id.iv_usericon);
				viewHolder.iv_sms_message = (ImageView) convertView.findViewById(R.id.iv_status);
				viewHolder.pb_progressBar = (ProgressBar) convertView.findViewById(R.id.refresh_progress);
				viewHolder.tv_content = (TextView) convertView.findViewById(R.id.contentfrom);
				viewHolder.tv_time = (TextView) convertView.findViewById(R.id.timefrom);
				viewHolder.iv_status = (ImageView) convertView.findViewById(R.id.iv_status);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (MessageViewHolder) convertView.getTag();
			}
			mImageResizer.loadImage(msg.getUserId(), viewHolder.iv_usericon);
			goToUserInfo(msg, viewHolder.iv_usericon);

			viewHolder.tv_content.setTag(msg);
			viewHolder.tv_content.setOnLongClickListener(onLongClickListener);
			new ChatSpeakUI(viewHolder, context, imageApkIconFetcher).initView(msg);
			viewHolder.tv_time.setText(Util.getDateDiff(oldDate));
			break;

		case PangkerConstant.RES_MUSIC: // 音乐文件
		case PangkerConstant.RES_APPLICATION:// 应用文件
		case PangkerConstant.RES_DOCUMENT:// 文本文件
			// convertView = messageView.get(ACCEPT_FILE);
			if (convertView == null) {
				viewHolder = new MessageViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.message_talk_view_acceptfile, null);
				viewHolder.iv_imagecover = (ImageView) convertView.findViewById(R.id.imageCover);
				viewHolder.iv_usericon = (ImageView) convertView.findViewById(R.id.iv_usericon);
				viewHolder.msgRLayout = (RelativeLayout) convertView.findViewById(R.id.msg_layout);
				viewHolder.btn_accept = (Button) convertView.findViewById(R.id.btn_file_accept);
				viewHolder.btn_reject = (Button) convertView.findViewById(R.id.btn_file_reject);
				viewHolder.tv_content = (TextView) convertView.findViewById(R.id.contentfrom);
				viewHolder.tv_time = (TextView) convertView.findViewById(R.id.timefrom);
				viewHolder.tv_fileize = (TextView) convertView.findViewById(R.id.content_size);
				viewHolder.pb_progressBar = (ProgressBar) convertView.findViewById(R.id.probar_revicefile);
				convertView.setTag(viewHolder);
				// messageView.put(ACCEPT_FILE, convertView);
			} else {
				viewHolder = (MessageViewHolder) convertView.getTag();
			}
			goToUserInfo(msg, viewHolder.iv_usericon);
			viewHolder.tv_time.setText(Util.getDateDiff(oldDate));
			viewHolder.btn_accept.setEnabled(true);
			viewHolder.msgRLayout.setTag(msg);
			viewHolder.msgRLayout.setOnLongClickListener(onLongClickListener);
			new ChatAcceptFileUI(viewHolder, context, msgDao, application, mImageResizer, imageApkIconFetcher)
					.initView(msg);
			break;
		case PangkerConstant.RES_PICTURE:// 图片接受
			// convertView = messageView.get(ACCEPT_PIC);//
			if (convertView == null) {
				viewHolder = new MessageViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.message_talk_view_acceptpic, null);
				viewHolder.msgRLayout = (RelativeLayout) convertView.findViewById(R.id.msg_layout);
				viewHolder.iv_usericon = (ImageView) convertView.findViewById(R.id.iv_usericon);
				viewHolder.iv_imagecover = (ImageView) convertView.findViewById(R.id.imageCover);
				viewHolder.tv_time = (TextView) convertView.findViewById(R.id.timefrom);
				viewHolder.pb_progressBar = (ProgressBar) convertView.findViewById(R.id.dwownload_pb);
				viewHolder.iv_status = (ImageView) convertView.findViewById(R.id.iv_status);
				convertView.setTag(viewHolder);
				// messageView.put(ACCEPT_PIC, convertView);
			} else {
				viewHolder = (MessageViewHolder) convertView.getTag();
			}
			goToUserInfo(msg, viewHolder.iv_usericon);
			viewHolder.tv_time.setText(Util.getDateDiff(oldDate));

			viewHolder.msgRLayout.setTag(msg);
			viewHolder.msgRLayout.setOnLongClickListener(onLongClickListener);
			new ChatAcceptPicUI(viewHolder, context, msgDao, application, localFetcher, mImageResizer,
					ChatFileUI.MSGTYPE_P2P_MESSAGE, imageApkIconFetcher).initView(msg);
			break;
		case PangkerConstant.RES_VOICE: // 语音会话

			if (convertView == null) {
				viewHolder = new MessageViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.message_talk_view_from, null);
				viewHolder.msgLLayout = (LinearLayout) convertView.findViewById(R.id.msg_layout);
				viewHolder.iv_usericon = (ImageView) convertView.findViewById(R.id.iv_usericon);
				viewHolder.iv_sms_message = (ImageView) convertView.findViewById(R.id.iv_voice);
				viewHolder.pb_progressBar = (ProgressBar) convertView.findViewById(R.id.refresh_progress);
				viewHolder.tv_content = (TextView) convertView.findViewById(R.id.contentfrom);
				viewHolder.tv_time = (TextView) convertView.findViewById(R.id.timefrom);
				viewHolder.iv_status = (ImageView) convertView.findViewById(R.id.iv_status);
				convertView.setTag(viewHolder);
				// messageView.put(this.ACCEPT_SPEAK, convertView);
			} else {
				viewHolder = (MessageViewHolder) convertView.getTag();
			}
			mImageResizer.loadImage(msg.getUserId(), viewHolder.iv_usericon);
			goToUserInfo(msg, viewHolder.iv_usericon);

			viewHolder.tv_content.setTag(msg);
			viewHolder.tv_content.setOnLongClickListener(onLongClickListener);
			viewHolder.tv_time.setText(Util.getDateDiff(oldDate));
			new ChatAcceptVoiceUI(viewHolder, context, msgDao, imageApkIconFetcher).initView(msg);
			break;
		}
		return convertView;
	}

	public List<ChatMessage> getMsgs() {
		return msgs;
	}

	public void setMsgs(List<ChatMessage> msgs) {
		this.msgs = msgs;
		this.notifyDataSetChanged();
	}

	public void addMsg(ChatMessage msg) {
		msgs.add(msg);
		notifyDataSetChanged();
	}

	public void delMsg(ChatMessage msg) {
		msgs.remove(msg);
		notifyDataSetChanged();
	}

	/**
	 * 更新
	 * 
	 * @param messageID
	 */
	public void messageReceived(int messageID) {
		if (messageID > 0) {
			for (int i = msgs.size() - 1; i >= 0; i--) {
				if (msgs.get(i).getId() == messageID) {
					msgs.get(i).setMessageRevc(1);
					break;
				}
			}
		}
	}

	/**
	 * 
	 * void 点击头像查看用户信息 TODO
	 * 
	 * @param msg
	 * @param userIcon
	 */
	private void goToUserInfo(final ChatMessage msg, ImageView userIcon) {
		userIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(context, UserWebSideActivity.class);
				mIntent.putExtra(UserItem.USERID, msg.getUserId());
				mIntent.putExtra(UserItem.USERNAME, msg.getUserName());
				context.startActivity(mIntent);
			}
		});
	}

}
