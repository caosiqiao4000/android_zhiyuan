package com.wachoo.pangker.adapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.ClipboardManager;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.group.ChatRoomInfoActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.activity.res.PictureInfoActivity;
import com.wachoo.pangker.activity.res.RecommendReceiverAcitvity;
import com.wachoo.pangker.activity.res.ResInfoActivity;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.MyListView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class RecommendInfoAdapter extends BaseAdapter {

	private Activity context;
	private List<RecommendInfo> recommendInfos;
	private PKIconResizer mImageResizer;

	// >>>>>>获取图片
	protected ImageResizer mImageWorker;
	private ImageResizer mGroupIconResizer;
	private PangkerApplication application;
	private String userId;
	// >>>>>>>设置显示长度
	private InputFilter[] filters = { new InputFilter.LengthFilter(PangkerConstant.showLength) };
	private InputFilter[] morefilters = {};

	// >>>>>>>>>>内存缓存图片
	private Handler handler = new Handler();
	private OnLongClickListener onLongClickListener;

	public OnLongClickListener getOnLongClickListener() {
		return onLongClickListener;
	}

	public void setmImageResizer(PKIconResizer mImageResizer) {
		this.mImageResizer = mImageResizer;
	}

	public void setOnLongClickListener(OnLongClickListener onLongClickListener) {
		this.onLongClickListener = onLongClickListener;
	}

	public RecommendInfoAdapter(Activity context, List<RecommendInfo> recommendInfos, ImageResizer mImageWorker) {
		super();
		this.context = context;
		this.recommendInfos = recommendInfos;
		this.application = (PangkerApplication) this.context.getApplicationContext();
		this.mGroupIconResizer = PangkerManager.getGroupIconResizer(context.getApplicationContext());
		this.mImageWorker = mImageWorker;
		this.userId = application.getMyUserID();
	}

	public void addRecommendInfo(RecommendInfo info) {
		recommendInfos.add(info);
		notifyDataSetChanged();
	}

	public void delRecommendInfo(RecommendInfo info) {
		recommendInfos.remove(info);
		notifyDataSetChanged();
	}

	public void addRecommendInfos(List<RecommendInfo> recommendInfos) {
		this.recommendInfos.addAll(recommendInfos);
		notifyDataSetChanged();
	}

	public void setRecommendInfos(List<RecommendInfo> recommendInfos) {
		this.recommendInfos = recommendInfos;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return recommendInfos.size();
	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public RecommendInfo getItem(int position) {
		// TODO Auto-generated method stub
		return recommendInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		final RecommendInfo item = (RecommendInfo) getItem(position);
		if (userId.equals(item.getFromId())) {
			if (convertView == null) {// 推荐给别人的资源 mdby LB
				holder = new ViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.res_send_item, null);

				holder.lyResInfo = (LinearLayout) convertView.findViewById(R.id.ly_resinfo);
				holder.tvResDetail = (TextView) convertView.findViewById(R.id.tv_res_detail);
				holder.tvResName = (TextView) convertView.findViewById(R.id.tv_res_name);
				holder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_res_icon);
				holder.ivPicIcon = (ImageView) convertView.findViewById(R.id.iv_pic_icon);
				holder.tvResTime = (TextView) convertView.findViewById(R.id.res_tv_time);
				holder.liPic = (LinearLayout) convertView.findViewById(R.id.ly_pic);
				holder.lyResInfo = (LinearLayout) convertView.findViewById(R.id.ly_resinfo);
				holder.tvPicName = (TextView) convertView.findViewById(R.id.tv_pic_name);
				holder.tvPicTime = (TextView) convertView.findViewById(R.id.pic_tv_time);
				holder.tvResonShow = (TextView) (TextView) convertView.findViewById(R.id.tv_reson_show);
				holder.btnOpen = (TextView) convertView.findViewById(R.id.btn_open);
				holder.lv_user = (MyListView) convertView.findViewById(R.id.lv_user);
				holder.tv_show_alluser = (TextView) convertView.findViewById(R.id.tv_show_alluser);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.btnOpen.setVisibility(View.GONE);
			final int resType = item.getType();
			if (resType == PangkerConstant.RES_APPLICATION) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				String iconUrl = Configuration.getCoverDownload() + item.getResId();
				try {
					mImageWorker.setLoadingImage(R.drawable.icon46_apk);
					mImageWorker.loadImage(iconUrl, holder.ivIcon, item.getResId());
					// 存放KEY
				} catch (OutOfMemoryError err) {
					err.getStackTrace();
				}

				holder.tvResName.setText(item.getResName());
			}
			// >>>>>>>>>>>>图片资源
			else if (resType == PangkerConstant.RES_PICTURE) {
				holder.liPic.setVisibility(View.VISIBLE);
				holder.ivIcon.setVisibility(View.GONE);
				holder.ivPicIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setImageResource(R.drawable.icon46_photo);
				holder.tvPicName.setText(item.getResName());
				int sideLength = ImageUtil.getPicPreviewResolution(context);
				String iconUrl = ImageUtil.getWebImageURL(item.getResId(), sideLength);
				try {
					mImageWorker.loadImage(iconUrl, holder.ivPicIcon, item.getResId() + "_" + sideLength);
				} catch (OutOfMemoryError err) {
					err.getStackTrace();
				}

				holder.tvPicTime.setText(Util.getDateDiff(Util.strToDate(item.getDatetime())));

				holder.btnOpen.setText("浏览");
				holder.btnOpen.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						ArrayList<PictureViewInfo> imagePathes = new ArrayList<PictureViewInfo>();
						String imgUrl = Configuration.getImageUrl() + item.getResId();
						PictureViewInfo pictureViewInfo = new PictureViewInfo();
						pictureViewInfo.setPicPath(imgUrl);
						imagePathes.add(pictureViewInfo);
						Intent intentPic = new Intent(context, PictureBrowseActivity.class);
						intentPic.putExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY, imagePathes);
						intentPic.putExtra("index", 0);
						// >>>>>>推荐图片显示方式
						intentPic.putExtra(RecommendReceiverAcitvity.RES_OPEN_TYPE,
								RecommendReceiverAcitvity.RECOMMEND_OPEN);
						// >>>>>推荐人信息
						intentPic.putExtra(RecommendReceiverAcitvity.RECOMMEND_INFO, item);
						context.startActivity(intentPic);
					}
				});

			}

			// >>>>>>>>>>>>音乐资源
			else if (resType == PangkerConstant.RES_MUSIC) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				holder.ivIcon.setImageResource(R.drawable.icon46_music);
				holder.tvResName.setText(item.getResName());
				holder.btnOpen.setText("听歌");
				holder.btnOpen.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						List<MusicInfo> musicInfos = new ArrayList<MusicInfo>();
						MusicInfo musicInfo = new MusicInfo();
						musicInfo.setSid(Util.String2Long(item.getResId()));
						musicInfo.setMusicName(item.getResName());
						musicInfo.setSinger(item.getSinger());
						musicInfos.add(musicInfo);

						application.setMusiList(musicInfos);
						application.setPlayMusicIndex(0);
						Intent intentSerivce = new Intent(context, MusicService.class);
						intentSerivce.putExtra("control", PlayControl.MUSIC_START_PLAY);
						context.startService(intentSerivce);

						// >>>>>>>启动音乐播放界面
						Intent intentActivity = new Intent(context, MusicPlayActivity.class);
						// >>>>>推荐启动音乐播放器
						intentActivity.putExtra(RecommendReceiverAcitvity.RES_OPEN_TYPE,
								RecommendReceiverAcitvity.RECOMMEND_OPEN);

						// >>>>>推荐人信息
						intentActivity.putExtra(RecommendReceiverAcitvity.RECOMMEND_INFO, item);
						context.startActivity(intentActivity);
					}
				});

			}
			// >>>>>>>>>>>文档资源
			else if (resType == PangkerConstant.RES_DOCUMENT) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				holder.ivIcon.setImageResource(R.drawable.attachment_doc);
				holder.tvResName.setText(item.getResName());
			}
			// >>>>>>>>>>>>群组资源
			else if (resType == PangkerConstant.RES_BROADCAST) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				final String groupUrl = Configuration.getGroupCover() + item.getResId();
				mGroupIconResizer.loadImage(groupUrl, holder.ivIcon, item.getResId());
				holder.tvResName.setText(item.getResName());
			}

			// >>>>>>>>推荐理由
			String reaSon = item.getReason();
			if (Util.isEmpty(reaSon)) {
				holder.tvResDetail.setVisibility(View.GONE);
				holder.tvResonShow.setVisibility(View.GONE);
			} else {
				holder.tvResDetail.setVisibility(View.VISIBLE);
				holder.tvResDetail.setFilters(filters);

				if (reaSon.length() > PangkerConstant.showLength) {
					holder.tvResonShow.setText("显示更多");
					holder.tvResonShow.setVisibility(View.VISIBLE);
				} else
					holder.tvResonShow.setVisibility(View.GONE);

				holder.tvResonShow.setTag(position);
				// >>>>>>>>设置显示更多
				holder.tvResonShow.setOnClickListener(new AcceptOnClickListener(holder.tvResonShow, holder.tvResDetail,
						position));
				holder.tvResDetail.setText(reaSon);
			}
			// >>>>>>>>>>复制功能
			holder.tvResDetail.setOnLongClickListener(new View.OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					showMsgMenu(position);
					return true;
				}

			});

			holder.tvResTime.setText(Util.getDateDiff(Util.strToDate(item.getDatetime())));
			// >>>>>>点击查看详情
			holder.lyResInfo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					lookRecommend(item);
				}
			});
			holder.lyResInfo.setTag(item);
			holder.lyResInfo.setOnLongClickListener(onLongClickListener);
			if (item.getUserStatus() != null) {
				if (item.getUserStatus().size() < 6) {
					holder.tv_show_alluser.setVisibility(View.GONE);
				} else
					holder.tv_show_alluser.setVisibility(View.VISIBLE);

				final ResRecommentUserAdapter mUserAdapter = new ResRecommentUserAdapter(context, item.getUserStatus());
				holder.lv_user.setAdapter(mUserAdapter);

				holder.tv_show_alluser.setOnClickListener(getClickListener(holder.tv_show_alluser, mUserAdapter));
			} else
				holder.tv_show_alluser.setVisibility(View.GONE);

		} else {// 别人推荐来的资源 mdby LB
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = LayoutInflater.from(context).inflate(R.layout.res_recommend_item, null);

				// holder.recommendItem = (LinearLayout)
				// convertView.findViewById(R.id.widget10);
				holder.ivUserIcon = (ImageView) convertView.findViewById(R.id.iv_user_icon);
				holder.tvResDetail = (TextView) convertView.findViewById(R.id.tv_res_detail);
				holder.tvResName = (TextView) convertView.findViewById(R.id.tv_res_name);
				holder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_res_icon);
				holder.ivPicIcon = (ImageView) convertView.findViewById(R.id.iv_pic_icon);
				holder.tvUsername = (TextView) convertView.findViewById(R.id.tv_username);
				holder.tvResTime = (TextView) convertView.findViewById(R.id.res_tv_time);
				holder.liPic = (LinearLayout) convertView.findViewById(R.id.ly_pic);
				holder.lyResInfo = (LinearLayout) convertView.findViewById(R.id.ly_resinfo);
				holder.tvPicName = (TextView) convertView.findViewById(R.id.tv_pic_name);
				holder.tvPicTime = (TextView) convertView.findViewById(R.id.pic_tv_time);
				holder.btnOpen = (TextView) convertView.findViewById(R.id.btn_open);
				holder.btn_talk = (TextView) convertView.findViewById(R.id.btn_talk);
				holder.tvResonShow = (TextView) (TextView) convertView.findViewById(R.id.tv_reson_show);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.btn_talk.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent();
					intent.putExtra(UserItem.USERID, item.getFromId());
					intent.putExtra(UserItem.USERNAME, item.getFromName());
					intent.setClass(context, MsgChatActivity.class);
					context.startActivity(intent);
				}
			});

			holder.ivUserIcon.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(context, UserWebSideActivity.class);
					intent.putExtra(UserItem.USERID, item.getFromId());
					intent.putExtra(UserItem.USERNAME, item.getFromName());
					context.startActivity(intent);
				}
			});

			holder.btnOpen.setVisibility(View.GONE);
			final int resType = item.getType();
			if (resType == PangkerConstant.RES_APPLICATION) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				String iconUrl = Configuration.getCoverDownload() + item.getResId();
				try {
					mImageWorker.setLoadingImage(R.drawable.icon46_apk);
					mImageWorker.loadImage(iconUrl, holder.ivIcon, item.getResId());

				} catch (OutOfMemoryError err) {
					err.getStackTrace();
				}

				holder.tvResName.setText(item.getResName());
			} else if (resType == PangkerConstant.RES_PICTURE) {
				holder.liPic.setVisibility(View.VISIBLE);
				holder.ivIcon.setVisibility(View.GONE);
				holder.ivPicIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setImageResource(R.drawable.icon46_photo);
				holder.tvPicName.setText(item.getResName());

				int sideLength = ImageUtil.getPicPreviewResolution(context);
				String iconUrl = ImageUtil.getWebImageURL(item.getResId(), sideLength);
				try {
					mImageWorker.loadImage(iconUrl, holder.ivPicIcon, item.getResId() + "_" + sideLength);
				} catch (OutOfMemoryError err) {
					err.getStackTrace();
				}

				holder.tvPicTime.setText(Util.getDateDiff(Util.strToDate(item.getDatetime())));

				holder.btnOpen.setText("浏览");
				holder.btnOpen.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (item.getStatus() == 0) {
							readRes(item.getResId());
						}

						ArrayList<PictureViewInfo> imagePathes = new ArrayList<PictureViewInfo>();
						String imgUrl = Configuration.getImageUrl() + item.getResId();
						PictureViewInfo pictureViewInfo = new PictureViewInfo();
						pictureViewInfo.setPicPath(imgUrl);
						imagePathes.add(pictureViewInfo);
						Intent intentPic = new Intent(context, PictureBrowseActivity.class);
						intentPic.putExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY, imagePathes);
						intentPic.putExtra("index", 0);

						// >>>>>>推荐图片显示方式
						intentPic.putExtra(RecommendReceiverAcitvity.RES_OPEN_TYPE,
								RecommendReceiverAcitvity.RECOMMEND_OPEN);
						// >>>>>推荐人信息
						intentPic.putExtra(RecommendReceiverAcitvity.RECOMMEND_INFO, item);
						context.startActivity(intentPic);
					}
				});

				// >>>>>>>>推荐理由
				String reaSon = item.getReason();
				if (Util.isEmpty(reaSon)) {
					reaSon = context.getResources().getString(R.string.res_recommend_noreason);
				}

				// >>>>>>>>>>>过滤器
				holder.tvResDetail.setFilters(item.getFilters());
				if (reaSon.length() > PangkerConstant.showLength) {
					holder.tvResonShow.setVisibility(View.VISIBLE);
					holder.tvResonShow.setText(item.getBtnShowText());
				} else
					holder.tvResonShow.setVisibility(View.GONE);
				holder.tvResonShow.setTag(position);
				// >>>>>>>>设置显示更多
				holder.tvResonShow.setOnClickListener(new AcceptOnClickListener(holder.tvResonShow, holder.tvResDetail,
						position));
				holder.tvResDetail.setText(reaSon);
				holder.tvResTime.setText(Util.getDateDiff(Util.strToDate(item.getDatetime())));
				// >>>>>>点击查看详情
				holder.lyResInfo.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (item.getStatus() == 0) {
							readRes(item.getResId());
						}
						lookRecommend(item);
					}
				});
				holder.lyResInfo.setTag(item);
				holder.lyResInfo.setOnLongClickListener(onLongClickListener);

			} else if (resType == PangkerConstant.RES_MUSIC) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				holder.ivIcon.setImageResource(R.drawable.icon46_music);
				holder.tvResName.setText(item.getResName());
				holder.btnOpen.setText("听歌");
				holder.btnOpen.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						if (item.getStatus() == 0) {
							readRes(item.getResId());
						}
						List<MusicInfo> musicInfos = new ArrayList<MusicInfo>();
						MusicInfo musicInfo = new MusicInfo();
						musicInfo.setSid(Util.String2Long(item.getResId()));
						musicInfo.setMusicName(item.getResName());
						musicInfo.setSinger(item.getSinger());
						musicInfos.add(musicInfo);

						application.setMusiList(musicInfos);
						application.setPlayMusicIndex(0);
						Intent intentSerivce = new Intent(context, MusicService.class);
						intentSerivce.putExtra("control", PlayControl.MUSIC_START_PLAY);
						context.startService(intentSerivce);

						// >>>>>>>启动音乐播放界面
						Intent intentActivity = new Intent(context, MusicPlayActivity.class);
						// >>>>>推荐启动音乐播放器
						intentActivity.putExtra(RecommendReceiverAcitvity.RES_OPEN_TYPE,
								RecommendReceiverAcitvity.RECOMMEND_OPEN);

						// >>>>>推荐人信息
						intentActivity.putExtra(RecommendReceiverAcitvity.RECOMMEND_INFO, item);
						context.startActivity(intentActivity);
					}
				});

			} else if (resType == PangkerConstant.RES_DOCUMENT) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				holder.ivIcon.setImageResource(R.drawable.attachment_doc);
				holder.tvResName.setText(item.getResName());
			}
			// >>>>>>>>>>>>群组资源
			else if (resType == PangkerConstant.RES_BROADCAST) {
				holder.liPic.setVisibility(View.GONE);
				holder.ivIcon.setVisibility(View.VISIBLE);
				holder.ivPicIcon.setVisibility(View.GONE);
				final String groupUrl = Configuration.getGroupCover() + item.getResId();
				mGroupIconResizer.loadImage(groupUrl, holder.ivIcon, item.getResId());
				holder.tvResName.setText(item.getResName());
			}
			try {
				mImageResizer.loadImage(item.getFromId().toString(), holder.ivUserIcon);
			} catch (OutOfMemoryError err) {
				err.getStackTrace();
			}

			// >>>>>>>>>>复制功能
			holder.tvResDetail.setOnLongClickListener(new View.OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					showMsgMenu(position);
					return true;
				}

			});

			// >>>>>>>>推荐理由
			String reaSon = item.getReason();
			if (Util.isEmpty(reaSon)) {
				reaSon = context.getResources().getString(R.string.res_recommend_noreason);
			}

			// >>>>>>>>>>>过滤器
			holder.tvResDetail.setFilters(item.getFilters());
			if (reaSon.length() > PangkerConstant.showLength) {
				holder.tvResonShow.setVisibility(View.VISIBLE);
				holder.tvResonShow.setText(item.getBtnShowText());
			} else
				holder.tvResonShow.setVisibility(View.GONE);
			holder.tvResonShow.setTag(position);
			// >>>>>>>>设置显示更多
			holder.tvResonShow.setOnClickListener(new AcceptOnClickListener(holder.tvResonShow, holder.tvResDetail,
					position));
			holder.tvResDetail.setText(reaSon);
			holder.tvUsername.setText(item.getFromName());
			holder.tvResTime.setText(Util.getDateDiff(Util.strToDate(item.getDatetime())));
			// >>>>>>点击查看详情
			holder.lyResInfo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (item.getStatus() == 0) {
						readRes(item.getResId());
					}
					lookRecommend(item);
				}
			});
			holder.lyResInfo.setTag(item);
			holder.lyResInfo.setOnLongClickListener(onLongClickListener);
		}

		return convertView;
	}

	// 对推荐进行处理
	private void lookRecommend(RecommendInfo info) {
		// TODO Auto-generated method stub
		if (info.getType() == PangkerConstant.RES_BROADCAST) {
			lookChatRoomInfo(info);
		} else {
			Intent intent = new Intent();
			if (info.getType() == PangkerConstant.RES_PICTURE) {
				intent.setClass(context, PictureInfoActivity.class);
			} else
				intent.setClass(context, ResInfoActivity.class);

			List<ResShowEntity> reList = new ArrayList<ResShowEntity>();

			ResShowEntity entity = new ResShowEntity();
			entity.setCommentTimes(0);
			entity.setResType(info.getType());
			entity.setDownloadTimes(0);
			entity.setFavorTimes(0);
			entity.setResID(Long.parseLong(info.getResId()));
			entity.setScore(0);
			entity.setResName(info.getResName());
			entity.setShareUid(info.getShareUid());
			reList.add(entity);
			application.setResLists(reList);
			application.setViewIndex(0);
			context.startActivity(intent);
		}
	}

	private void lookChatRoomInfo(RecommendInfo info) {
		Intent intent = new Intent();
		List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
		GroupLbsInfo lbsInfo = new GroupLbsInfo();
		lbsInfo.setgName(info.getResName());
		lbsInfo.setSid(Long.parseLong(info.getResId()));
		lbsInfo.setUid(Long.parseLong(info.getUserId()));
		lbsInfo.setShareUid(info.getShareUid());
		groupList.add(lbsInfo);
		intent.putExtra("GroupInfo", (Serializable) groupList);
		intent.putExtra("group_index", 0);
		intent.setClass(context, ChatRoomInfoActivity.class);
		context.startActivity(intent);
	}

	/**
	 * 删除资源或者标识资源为已读
	 */
	public void readRes(String resId) {
		ServerSupportManager serverMana = new ServerSupportManager(context, new IUICallBackInterface() {

			@Override
			public void uiCallBack(Object supportResponse, int caseKey) {
				// TODO Auto-generated method stub
				System.err.println();
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", resId));
		paras.add(new Parameter("optype", "2"));
		serverMana.supportRequest(Configuration.getDeleteBoxRes(), paras, false, "");
	}

	/** 
	 *  
	 */
	class ViewHolder {
		ImageView ivIcon;
		ImageView ivUserIcon;
		TextView tvResDetail;
		TextView tvResName;
		TextView tvUsername;
		TextView tvResTime;
		LinearLayout liPic;
		LinearLayout lyResInfo;
		ImageView ivPicIcon;
		TextView tvPicName;
		TextView tvPicTime;
		TextView btnOpen;
		TextView tvResonShow;
		TextView tvUsernameRead;
		MyListView lv_user;
		TextView tv_show_alluser;
		TextView btn_talk;

	}

	private PopMenuDialog msgDialog;
	ActionItem[] msgMenu = new ActionItem[] { new ActionItem(1, "复制") };

	private void showMsgMenu(final int position) {
		// TODO Auto-generated method stub
		if (msgDialog == null) {
			msgDialog = new PopMenuDialog(context, R.style.MyDialog);
			msgDialog.setListener(new UITableView.ClickListener() {

				@Override
				public void onClick(int actionid) {
					// TODO Auto-generated method stub

					ClipboardManager clip = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
					clip.setText(getItem(position).getReason());

					msgDialog.dismiss();
				}
			});
		}
		msgDialog.setMenus(msgMenu);
		msgDialog.setTitle("文字信息");
		msgDialog.show();
	}

	class AcceptOnClickListener implements View.OnClickListener {

		private TextView mShow;
		private TextView mDetail;
		private int position;

		public AcceptOnClickListener(TextView mShow, TextView mDetail, int position) {
			super();
			this.mShow = mShow;
			this.mDetail = mDetail;
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (mShow.getText().equals(PangkerConstant.SHOW_MORE_TEXT)) {

				handler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						mShow.setText(PangkerConstant.SHOW_UP_TEXT);
						mDetail.setFilters(morefilters);
						// >>>>>>>>设置按钮显示
						getItem(position).setBtnShowText(PangkerConstant.SHOW_UP_TEXT);
						getItem(position).setFilters(morefilters);
						mDetail.setText(getItem(position).getReason());
					}
				});

			} else {
				handler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						mShow.setText(PangkerConstant.SHOW_MORE_TEXT);
						mDetail.setFilters(filters);
						// >>>>>>>>设置按钮显示
						getItem(position).setBtnShowText(PangkerConstant.SHOW_MORE_TEXT);
						getItem(position).setFilters(filters);
						mDetail.setText(getItem(position).getReason());
					}
				});

			}
		}
	}

	/**
	 * 
	 * TODO设置是否显示全部用户
	 * 
	 * @param tvLoadAll
	 * @param adapter
	 * @return
	 */
	private OnClickListener getClickListener(final TextView tvLoadAll, final ResRecommentUserAdapter adapter) {
		return new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (adapter.isLoadAll()) {
					adapter.setLoadAll(false);
					tvLoadAll.setText("显示全部");
				} else {
					adapter.setLoadAll(true);
					tvLoadAll.setText("收起显示");
				}
			}
		};
	}

}
