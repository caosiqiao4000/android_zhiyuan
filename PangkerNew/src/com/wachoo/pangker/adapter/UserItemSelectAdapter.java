package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;

/**
 * 
 *
 * 
 */
public class UserItemSelectAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private List<UserItem> mUserList;
	private PKIconResizer mImageResizer;

	/**
	 * @return the mUserList
	 */
	public List<UserItem> getmUserList() {
		return mUserList;
	}

	/**
	 * @param mUserList
	 *            the mUserList to set
	 */
	public void setmUserList(List<UserItem> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	public UserItemSelectAdapter(Context context, List<UserItem> users) {
		layoutInflater = LayoutInflater.from(context);
		this.mUserList = users;
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	public int getCount() {
		return mUserList == null ? 0 : mUserList.size();
	}

	public Object getItem(int position) {
		if (mUserList != null) {
			return mUserList.get(position);
		}
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	ViewHolder holder = null;

	public View getView(int position, View convertView, ViewGroup parent) {
		final UserItem item = mUserList.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.local_contact_select_item, null);
			holder.iv_userIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			holder.tv_userName = (TextView) convertView.findViewById(R.id.tv_username);
			holder.tv_phone_num = (TextView) convertView.findViewById(R.id.tv_usersign);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tv_userName.setText(item.getUserName());
		holder.tv_phone_num.setText(item.getPhoneNum());

		mImageResizer.loadImage(item.getUserId(), holder.iv_userIcon);

		return convertView;
	}

	class ViewHolder {
		ImageView iv_userIcon;
		TextView tv_userName;
		TextView tv_phone_num;
	}

}
