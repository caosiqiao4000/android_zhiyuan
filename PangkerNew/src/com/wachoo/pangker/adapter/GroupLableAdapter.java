package com.wachoo.pangker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
/**
 */
public class GroupLableAdapter extends BaseAdapter {
	
	private Context context;
	private String[] labelList;

	public GroupLableAdapter(Context context,String[] mLabelList) {
		this.labelList = mLabelList;
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return labelList.length;
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return labelList[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = LayoutInflater.from(context).inflate(R.layout.group_label_item, null);
		TextView tvItem = (TextView)convertView.findViewById(R.id.tv_item);
		tvItem.setText(getItem(position));
		return convertView;
	}

	public String[] getLabelList() {
		return labelList;
	}

	public void setLabelList(String[] labelList) {
		this.labelList = labelList;
		this.notifyDataSetChanged();
	}
	public void setLabelList(String label) {
		if(label!= null){
			String[] newLabel = label.split(",");
			if(newLabel != null || newLabel.length>0){
				this.labelList = newLabel;
				this.notifyDataSetChanged();
			}
			
		}
		
	}
	

}
