package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.activity.msg.NoticeInfoActivity;
import com.wachoo.pangker.entity.NoticeInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.util.Util;

public class NoticeinfoAdapter extends BaseAdapter {

	private final int[] APPLYTYPE = { NoticeInfo.NOTICE_FRIEND_ADD, NoticeInfo.NOTICE_DRIFTUSER_REQUEST,
			NoticeInfo.NOTICE_TRACE_REQUEST, NoticeInfo.NOTICE_NET_ADDRESS_RESPONSE,
			NoticeInfo.NOTICE_NET_ADDRESS_INVITE_RESPONSE, NoticeInfo.NOTICE_NET_ADDRESS_APPLY_EXIT };

	private Context context;
	private List<NoticeInfo> noticeInfos;
	private PKIconResizer mImageResizer;
	private View.OnClickListener onClickListener;
	private View.OnLongClickListener onLongClickListener;

	public NoticeinfoAdapter(Context context, List<NoticeInfo> noticeInfos, PKIconResizer mImageResizer) {
		super();
		this.context = context;
		this.noticeInfos = noticeInfos;
		this.mImageResizer = mImageResizer;
	}

	public void clearAll() {
		noticeInfos.clear();
		notifyDataSetChanged();
	}

	public void changPNoticeinfo(NoticeInfo info) {
		// TODO Auto-generated method stub
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return noticeInfos.size();
	}

	public void addNoticeinfo(NoticeInfo noticeInfo) {
		noticeInfos.add(0, noticeInfo);
		notifyDataSetChanged();
	}

	public void delNoticeinfo(NoticeInfo noticeInfo) {
		noticeInfos.remove(noticeInfo);
		notifyDataSetChanged();
	}

	public void clearNoticeInfos() {
		// TODO Auto-generated method stub
		noticeInfos.clear();
		notifyDataSetChanged();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return noticeInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	private boolean isShowType(int type) {
		for (int i = 0; i < APPLYTYPE.length; i++) {
			if (type == APPLYTYPE[i]) {
				return true;
			}
		}
		return false;
	}

	ViewHolder holder;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final NoticeInfo item = (NoticeInfo) getItem(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.noticeinfo_item, null);
			holder = new ViewHolder();
			holder.imgUserIcon = (ImageView) convertView.findViewById(R.id.iv_sysicon);
			holder.msgLayout = (LinearLayout) convertView.findViewById(R.id.msg_layout);
			holder.txtUserName = (TextView) convertView.findViewById(R.id.tv_name);
			holder.txtTime = (TextView) convertView.findViewById(R.id.tv_timeto);
			holder.txtContent = (TextView) convertView.findViewById(R.id.tv_content);
			holder.txtRemark = (TextView) convertView.findViewById(R.id.tv_result);
			holder.iv_into = (ImageView) convertView.findViewById(R.id.iv_into);
			holder.btnTalk = (TextView) convertView.findViewById(R.id.btn_talk);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (item.getType() == NoticeInfo.NOTICE_TRACE_REQUEST
				|| item.getType() == NoticeInfo.NOTICE_FRIEND_ADD) {
			holder.btnTalk.setVisibility(View.VISIBLE);
			holder.btnTalk.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					toTalk(item);
				}
			});
		} else {
			holder.btnTalk.setVisibility(View.GONE);
		}

		holder.msgLayout.setTag(item);
		holder.msgLayout.setOnClickListener(onClickListener);
		holder.msgLayout.setOnLongClickListener(onLongClickListener);
		holder.txtTime.setText(Util.TimeIntervalBtwNow(item.getTime()));
		holder.txtContent.setText(item.getNotice());
		holder.txtUserName.setText(item.getOpeUsername());
		// holder.txtAccount.setText(item.getOpeUserid());
		if (isShowType(item.getType()) && item.getStatus() == 0) {
			holder.iv_into.setVisibility(View.VISIBLE);
		} else {
			holder.iv_into.setVisibility(View.GONE);
		}
		if (item.getStatus() == 0 || item.getStatus() == 1) {
			holder.txtRemark.setVisibility(View.GONE);
		} else {
			holder.txtRemark.setVisibility(View.VISIBLE);
			holder.txtRemark.setText(item.getRemark());
		}
		mImageResizer.loadImage(item.getOpeUserid(), holder.imgUserIcon);
		holder.imgUserIcon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((NoticeInfoActivity) context).lookUserInfo(item);
			}
		});
		return convertView;
	}

	/** 
	 *  
	 */
	class ViewHolder {
		TextView txtTime;
		ImageView imgUserIcon;
		LinearLayout msgLayout;
		TextView txtUserName;
		TextView txtContent;
		TextView txtRemark;
		ImageView iv_into;
		TextView btnTalk;
	}

	private void toTalk(NoticeInfo item) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra(UserItem.USERID, item.getOpeUserid());
		intent.putExtra(UserItem.USERNAME, item.getOpeUsername());
		intent.setClass(context, MsgChatActivity.class);
		context.startActivity(intent);
	}

	public void setOnClickListener(View.OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}

	public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
		this.onLongClickListener = onLongClickListener;
	}

}
