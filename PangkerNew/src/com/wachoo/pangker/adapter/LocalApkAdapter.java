package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageFetcher;

public class LocalApkAdapter extends BaseAdapter {

	private Activity context;
	private List<DirInfo> dirInfoLists;
	private List<Boolean> mchecked;
	private boolean flagShow = false; // ture表示在编辑状态,要显示checkbox
	private ImageApkIconFetcher apkIconFetcher;

	public LocalApkAdapter(Activity context, List<DirInfo> dirInfoLists, ImageApkIconFetcher apkIconFetcher) {
		super();
		this.context = context;
		this.dirInfoLists = dirInfoLists;
		mchecked = new ArrayList<Boolean>();
		this.apkIconFetcher = apkIconFetcher;
		init();
	}

	public void addDirInfo(DirInfo dirInfo) {
		// dirInfoLists.add(dirInfo);
		mchecked.add(false);
		notifyDataSetChanged();
	}

	public void remove(DirInfo dirInfo) {
		// TODO Auto-generated method stub
		dirInfoLists.remove(dirInfo);
		notifyDataSetChanged();
	}

	public void init() {
		mchecked.clear();
		int temp = dirInfoLists.size();
		for (int i = 0; i < temp; i++) {
			mchecked.add(i, false);
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dirInfoLists.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dirInfoLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		DirInfo item = (DirInfo) getItem(position);
		final ViewHolderLocal holder;
		if (convertView == null) {
			holder = new ViewHolderLocal();
			convertView = LayoutInflater.from(context).inflate(R.layout.select_doc_item, null);
			holder.iv_musicCover = (ImageView) convertView.findViewById(R.id.ImageViewMusicCover);
			holder.tv_musicName = (TextView) convertView.findViewById(R.id.tv_musicName);
			holder.tv_musicPlayer = (TextView) convertView.findViewById(R.id.tv_musicPlayer);
			holder.selected = (CheckBox) convertView.findViewById(R.id.Music_CheckBox);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolderLocal) convertView.getTag();
		}
		holder.iv_musicCover.setImageResource(R.drawable.icon46_apk);
		apkIconFetcher.loadImage(item.getPath(), holder.iv_musicCover, item.getPath());
		if (!flagShow) {
			holder.selected.setVisibility(View.INVISIBLE);
		} else {
			holder.selected.setVisibility(View.VISIBLE);
			holder.selected.setChecked(mchecked.get(position));
		}
		String appName = null;
		if (item.getDirName().contains(".apk")) {
			appName = item.getDirName().subSequence(0, item.getDirName().indexOf(".apk")).toString();
		} else {
			appName = item.getDirName();
		}
		holder.tv_musicName.setText(appName);
		String fileSize = Formatter.formatFileSize(context, item.getFileCount());
		holder.tv_musicPlayer.setText("文件大小:" + fileSize);
		return convertView;
	}

	// 选择了一个
	public void selectByIndex(int index, boolean flag) {
		mchecked.set(index, flag);
	}

	public int getSelectCount() {
		int sum = 0;
		for (Boolean flag : mchecked) {
			if (flag) {
				sum++;
			}
		}
		return sum;
	}

	// 已经选择的集合
	public List<DirInfo> getSeletedMembers() {
		List<DirInfo> selectedMembers = new ArrayList<DirInfo>();
		for (int i = 0; i < dirInfoLists.size(); i++) {
			if (mchecked.get(i)) {
				selectedMembers.add(dirInfoLists.get(i));
			}
		}
		return selectedMembers;
	}

	/* 全选/全不选 */
	public void setSelectedAll(boolean isSelectedAll) {
		for (int i = 0; i < dirInfoLists.size(); i++) {
			mchecked.set(i, isSelectedAll);
		}
		notifyDataSetChanged();
	}

	public void setFlagShow(boolean flagShow) {
		this.flagShow = flagShow;
		notifyDataSetChanged();
	}

	public class ViewHolderLocal {
		public ImageView iv_musicCover; // 封面
		public TextView tv_musicName; // 歌曲名/程序名
		public TextView tv_musicPlayer; // 歌手
		public TextView tv_musicScore; // 文件大小
		public ImageView img_ifPlay; // 是否播放/安装
		public CheckBox selected; // 是否选择一组
	}

	public List<DirInfo> getDirInfoLists() {
		return dirInfoLists;
	}

	public void setDirInfoLists(List<DirInfo> dirInfoLists) {
		this.dirInfoLists = dirInfoLists;
		init();
		notifyDataSetChanged();
	}
}
