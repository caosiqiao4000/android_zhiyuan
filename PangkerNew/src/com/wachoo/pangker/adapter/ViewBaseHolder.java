package com.wachoo.pangker.adapter;

import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.server.response.MovingRes;

public class ViewBaseHolder{
	
	MovingRes resInfo;
	DirInfo dirInfo;
    
	public MovingRes getResInfo() {
		return resInfo;
	}
	public void setResInfo(MovingRes resInfo) {
		this.resInfo = resInfo;
	}
	public DirInfo getDirInfo() {
		return dirInfo;
	}
	public void setDirInfo(DirInfo dirInfo) {
		this.dirInfo = dirInfo;
	}
	
	
}
