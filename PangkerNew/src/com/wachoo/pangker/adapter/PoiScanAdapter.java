package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.map.poi.Poi_concise;
import com.wachoo.pangker.util.MapUtil;
import com.wachoo.pangker.util.Util;

public class PoiScanAdapter extends BaseAdapter {
	
	private Context context;
	private List<Poi_concise> Pois;
    private PangkerApplication application;
    
	public PoiScanAdapter(Context context, ArrayList<Poi_concise> arrayList) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.Pois = arrayList;
		application = (PangkerApplication) context.getApplicationContext();
	}

	public void setList(List<Poi_concise> arrayList) {
		Pois = arrayList;
		notifyDataSetChanged();
	}
	
	public void addList(List<Poi_concise> arrayList) {
		Pois.addAll(arrayList);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (Pois != null)
			return Pois.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return Pois.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	ViewHolder holder;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Poi_concise item = (Poi_concise) getItem(position);
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.poi_item_view, null);
			holder = new ViewHolder();
			
			holder.poiIcon = (ImageView) convertView.findViewById(R.id.poi_icon);
			holder.poiName = (TextView) convertView.findViewById(R.id.poi_name);
			holder.poiAddress = (TextView) convertView.findViewById(R.id.poi_address);
			holder.poiDistace = (TextView) convertView.findViewById(R.id.poi_distant);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.poiName.setText(Util.getStringByLength(item.getName(), 30));
		holder.poiAddress.setText(item.getAddress());
		
		String distance = getDistance(Double.parseDouble(item.getLat()), Double.parseDouble(item.getLon())) + "米";
		holder.poiDistace.setText(distance);

		return convertView;
	}
	
	private int getDistance(double lat, double lon){
		double myLat = 0;
		double myLon = 0;
		if(application.getLocateType() == PangkerConstant.LOCATE_CLIENT){
			myLat = application.getCurrentLocation().getLatitude();
			myLon = application.getCurrentLocation().getLongitude();
		} else {
			myLat = application.getDirftLocation().getLatitude();
			myLon = application.getDirftLocation().getLongitude();
		}
		return MapUtil.DistanceOfTwoPoints(myLat, myLon , lat, lon);
	}
	
	private class ViewHolder {
		ImageView poiIcon;// 
		TextView poiName;// 
		TextView poiAddress;
		TextView poiDistace;
	}
	
}
