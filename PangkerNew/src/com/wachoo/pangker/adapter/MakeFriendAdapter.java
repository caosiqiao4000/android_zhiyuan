package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.util.Util;

/**
 * MakeFriendAdapter 交友Adapter
 * 
 * @author zhengjy
 * 
 */
public class MakeFriendAdapter extends BaseAdapter {

	private Context mContext;
	private List<UserItem> mUserList;
	private PKIconResizer mImageResizer;

	/**
	 * 构造函数
	 * 
	 * @param context
	 * @param list
	 */
	public MakeFriendAdapter(Context context, List<UserItem> list) {
		this.mContext = context;
		this.mUserList = list;
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mUserList.size();
	}

	@Override
	public UserItem getItem(int position) {
		// TODO Auto-generated method stub
		return mUserList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View rowView, ViewGroup parent) {
		// TODO Auto-generated method stub

		final UserItem item = getItem(position);
		ViewHolder viewHolder = null;
		if (rowView == null) {
			viewHolder = new ViewHolder();
			rowView = LayoutInflater.from(mContext).inflate(R.layout.user_list_item, null);
			viewHolder.userName = (TextView) rowView.findViewById(R.id.tv_name); // 昵称
			viewHolder.sign = (TextView) rowView.findViewById(R.id.tv_sign);// 交友宣言
			viewHolder.age = (TextView) rowView.findViewById(R.id.tv_age);// 年龄
			viewHolder.sex = (ImageView) rowView.findViewById(R.id.iv_sex); // 性别
			viewHolder.distance = (TextView) rowView.findViewById(R.id.tv_distance);// 距离
			viewHolder.usericon = (ImageView) rowView.findViewById(R.id.iv_usericon); // 用户头像
			viewHolder.time = (TextView) rowView.findViewById(R.id.tv_time); // 最近登录时间
			rowView.setTag(viewHolder);
		} else
			viewHolder = (ViewHolder) rowView.getTag();

		viewHolder.userName.setText(item.getUserName());
		viewHolder.sign.setText(item.getFriendSign());
		viewHolder.distance.setText(Util.getRang(item.getDistance()));

		if (PangkerConstant.NEAR_USER_GIRLS.equals(item.getSex())) {
			viewHolder.sex.setImageResource(R.drawable.icon24_g);
		} else if (PangkerConstant.NEAR_USER_BOYS.equals(item.getSex())) {
			viewHolder.sex.setImageResource(R.drawable.icon24_m);
		}
		viewHolder.age.setText(item.getAge() + "岁");// 年龄
		viewHolder.time.setText(Util.TimeIntervalBtwNow(item.getLastUpdateTime())); // 时间间隔

		mImageResizer.loadImage(item.getUserId(), viewHolder.usericon);
		return rowView;
	}

	public List<UserItem> getmUserList() {
		return mUserList;
	}

	public void setmUserList(List<UserItem> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	private class ViewHolder {
		public ImageView iv_userIcon; //
		public TextView userName; //
		public TextView sign;
		public ImageView sex; //
		public TextView age; //
		public ImageView usericon; //
		public TextView distance; //
		public TextView time;
	}

}
