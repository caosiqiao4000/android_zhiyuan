package com.wachoo.pangker.adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.util.Util;

public class ContactSelectAdapter extends BaseAdapter {

	private Context context;
	private List<LocalContacts> allcontacts;
	private List<Boolean> mchecked;
	private boolean ifCheck = true;

	public ContactSelectAdapter(Context context, List<LocalContacts> allcontacts) {
		super();
		this.context = context;
		this.allcontacts = allcontacts;
		mchecked = new ArrayList<Boolean>();
		init();
	}

	private void init() {
		// TODO Auto-generated method stub
		if (allcontacts != null) {
			for (int i = 0; i < allcontacts.size(); i++) {
				mchecked.add(false);
			}
		}
	}

	public void selectByIndex(int index, boolean flag) {
		mchecked.set(index, flag);
	}

	public int getSelectCount() {
		int count = 0;
		Iterator<Boolean> iter = mchecked.iterator();
		while (iter.hasNext()) {
			if (iter.next()) {
				count++;
			}
		}
		return count;
	}

	public List<LocalContacts> getSelectMember() {
		List<LocalContacts> contacts = new ArrayList<LocalContacts>();
		for (int i = 0; i < allcontacts.size(); i++) {
			if (mchecked.get(i)) {
				contacts.add(allcontacts.get(i));
			}
		}
		return contacts;
	}

	public void setmUserList(List<LocalContacts> contacts) {
		// TODO Auto-generated method stub
		this.allcontacts = contacts;
		init();
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return allcontacts == null ? 0 : allcontacts.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return allcontacts.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public boolean isIfCheck() {
		return ifCheck;
	}

	public void setIfCheck(boolean ifCheck) {
		this.ifCheck = ifCheck;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LocalContacts item = (LocalContacts) getItem(position);
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.select_contact_item, null);
			holder.imgIdentity = (ImageView) convertView.findViewById(R.id.iv_identity);
			holder.selected = (CheckBox) convertView.findViewById(R.id.mtrace_checkbox);
			holder.txtHint = (TextView) convertView.findViewById(R.id.tv_first_char_hint);
			holder.txtIdentity = (TextView) convertView.findViewById(R.id.tv_identity);
			holder.txtName = (TextView) convertView.findViewById(R.id.tv_username);
			holder.txtPhone = (TextView) convertView.findViewById(R.id.tv_usersign);
			holder.userIcon = (ImageView) convertView.findViewById(R.id.iv_usericon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (ifCheck) {
			holder.selected.setVisibility(View.VISIBLE);
		} else {
			holder.selected.setVisibility(View.GONE);
		}
		if (mchecked.get(position)) {
			holder.selected.setChecked(true);
		} else
			holder.selected.setChecked(false);

		holder.txtName.setText(item.getUserName());
		if(Util.isEmpty(item.getPhoneNum())){
			holder.txtPhone.setHint("无手机号");
			holder.txtPhone.setText("");
		}
		else 
		   holder.txtPhone.setText(item.getPhoneNum());
		if (!item.getFirstLetter().equals(LocalContacts.NO_LETTER)) {
			holder.txtHint.setVisibility(View.VISIBLE);
			holder.txtHint.setText(item.getFirstLetter());
		} else {
			// 此段代码不可缺：实例化一个CurrentView后，会被多次赋值并且只有最后一次赋值的position是正确
			holder.txtHint.setVisibility(View.GONE);
		}
		if (item.isHasPhoto()) {
			holder.userIcon.setImageBitmap(item.getPhoto());
		} else
			holder.userIcon.setImageResource(R.drawable.nav_head);

		holder.imgIdentity.setVisibility(View.VISIBLE);

		if (item.getRelation() == LocalContacts.RELATION_P_USER) {
			holder.imgIdentity.setImageResource(R.drawable.pangker);
			holder.txtIdentity.setText("(旁客)");
		} else if (item.getRelation() == LocalContacts.RELATION_P_FRIEND) {
			holder.imgIdentity.setImageResource(R.drawable.nav_head);
			holder.txtIdentity.setText("(好友)");
		} else {
			holder.imgIdentity.setVisibility(View.GONE);
			holder.txtIdentity.setText("(未注册)");
		}

		return convertView;
	}

	public class ViewHolder {
		public CheckBox selected; // 是否选择一组
		public TextView txtName; // 好友名称
		public TextView txtPhone; // 通讯录电话
		public TextView txtHint;//
		public TextView txtIdentity;//
		public ImageView userIcon; // 用户头像
		public ImageView imgIdentity; //
	}

}
