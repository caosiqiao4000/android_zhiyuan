package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.group.P2PUserInfo;

/**
 * @author wubo
 * @createtime 2012-3-27
 */
public class MicListAdapter extends BaseAdapter {

	private Context context;
	private List<P2PUserInfo> infos;

	public MicListAdapter(Context context, List<P2PUserInfo> infos) {
		this.context = context;
		this.infos = infos;
	}

	public void setMicList(List<P2PUserInfo> infos) {
		this.infos = infos;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return infos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return infos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {
		private TextView mNO;
		private TextView mUserName;

	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder = null;
		if (v == null) {
			viewHolder = new ViewHolder();
			v = LayoutInflater.from(context).inflate(R.layout.mic_item, null);
			viewHolder.mNO = (TextView) v.findViewById(R.id.tv_no);
			viewHolder.mUserName = (TextView) v.findViewById(R.id.tv_username);
			v.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) v.getTag();
		}
		viewHolder.mNO.setText((position + 1) + ":");
		viewHolder.mUserName.setText(infos.get(position).getUserName());

		return v;
	}
}
