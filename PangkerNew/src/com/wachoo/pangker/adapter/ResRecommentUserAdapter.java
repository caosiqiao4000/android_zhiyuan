package com.wachoo.pangker.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.response.UserStatus;

/**
 * 
 * @TODO 推荐资源是否已读用户列表
 * 
 * @author zhengjy
 * 
 *         2013-4-3
 * 
 */
public class ResRecommentUserAdapter extends BaseAdapter {

	private Context context;
	private List<UserStatus> userStatus;

	// 是否显示全部用户，如果用户数超过5，默认显示5个用户，为true时则显示全部
	private boolean isLoadAll = false;
	// 当前显示用户
	private List<UserStatus> currUsers;//

	public ResRecommentUserAdapter(Context context, final List<UserStatus> userStatus) {
		super();
		this.context = context;
		this.userStatus = userStatus;
		if (userStatus.size() > 5) {
			this.currUsers = userStatus.subList(0, 5);
		} else
			this.currUsers = userStatus;
	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return currUsers.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return currUsers.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final UserStatus item = (UserStatus) getItem(position);
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.recommend_user_item, null);
			viewHolder.mUserName = (TextView) convertView.findViewById(R.id.tv_username);
			viewHolder.isRead = (TextView) convertView.findViewById(R.id.tv_isread);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.mUserName.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, item.getUid().toString());
				intent.putExtra(UserItem.USERNAME, item.getNickname());
				context.startActivity(intent);
			}
		});
		viewHolder.mUserName.setText(item.getNickname());
		if (item.getStatus() == 0) {
			viewHolder.isRead.setText("未读");
		} else
			viewHolder.isRead.setText("已读");
		return convertView;
	}

	class ViewHolder extends ViewBaseHolder {
		TextView mUserName;
		ImageView ivIsRead;
		TextView isRead;
	}

	public List<UserStatus> getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(List<UserStatus> userStatus) {
		this.userStatus = userStatus;
	}

	public boolean isLoadAll() {
		return isLoadAll;
	}

	public void setLoadAll(boolean isLoadAll) {
		this.isLoadAll = isLoadAll;
		if (!isLoadAll && userStatus.size() > 5) {
			this.currUsers = userStatus.subList(0, 5);
		} else {
			this.currUsers = userStatus;
		}
		this.notifyDataSetChanged();
	}
}
