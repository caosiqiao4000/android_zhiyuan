package com.wachoo.pangker.entity;

import java.text.SimpleDateFormat;

import android.util.Log;

import com.wachoo.pangker.PangkerConstant;

/**
 * 会议室聊天记录
 * 
 * @author wubo
 * @createtime 2012-3-19
 */
public class MeetRoom extends ChatMessage implements Comparable<MeetRoom> {

	public static final short s_type_text = 0x04;// 文字会话流 0x04
	public static final short s_type_system = 0x01;// 系统消息 0x01 add by wx
	public static final short s_type_vedio = 0x11;// 手机视频直播流 0x11
	public static final short s_type_amr = 0x12;// 手机音频直播流 0x12
	public static final short s_type_loc = 0x13;// 地址经纬度信息0x13
	public static final short s_type_pic = 0x05;// 图片 0x05
	
	static final String TAG = "MeetRoomMessage";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String gid; // 会议室编号
	private String nickname; // 发信人的用户名
	private String groupname;
	private int msgType;// 消息类型 4=文字 5=图片
	private int msgId;
	private boolean isReceivedOK = false;// >>>>>>>>>针对文件是否接收结束标记
	private long talkTime; // 群组聊天的时间,区别父类的 time

	SimpleDateFormat sdtime = new SimpleDateFormat(PangkerConstant.DATE_FORMAT);

	public MeetRoom(String loginUserid) {
		setMyUserId(loginUserid);
	}

	public boolean isReceivedOK() {
		return isReceivedOK;
	}

	public void setReceivedOK(boolean isReceivedOK) {
		this.isReceivedOK = isReceivedOK;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public int getMsgId() {
		return msgId;
	}

	public void setMsgId(int msgId) {
		this.msgId = msgId;
		setId(msgId);
		Log.i(TAG, "msgId =" + msgId + "; time = " + getTime() + "; content = " + getContent());
	}

	public int getMsgType() {
		return msgType;
	}

	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}

	public long getTalkTime() {
		return talkTime;
	}

	public void setTalkTime(long talkTime) {
		this.talkTime = talkTime;
	}

	@Override
	public int compareTo(MeetRoom another) {
		// TODO Auto-generated method stub
		try {
			if (talkTime>another.talkTime) {
				return 1;
			}
//			if (sdtime.parse(getTime()).getTime() > sdtime.parse(another.getTime()).getTime()) {
//				return 1;
//			}
			return 0;
		} catch (Exception e) {
			return 0;
		}
	}

}
