package com.wachoo.pangker.entity;

import java.io.Serializable;

import android.text.InputFilter;

import com.wachoo.pangker.PangkerConstant;

/**
 * 说说
 * 
 * @author wangxin
 * 
 */
public class TextSpeakInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;// 资源Id
	private String userId;// 资源发布人Id
	private String userName;//
	private int ifShare;
	private String content;
	private String speakTime;

	private Location location;

	private double ctrlon;// 经度
	private double ctrlat;// 纬度 // >>>>>>>>>>>>显示更多
	private String btnShowText = PangkerConstant.SHOW_MORE_TEXT;

	public static final int showLength = 200;

	public String getBtnShowText() {
		return btnShowText;
	}

	public void setBtnShowText(String btnShowText) {
		this.btnShowText = btnShowText;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getIfShare() {
		return ifShare;
	}

	public void setIfShare(int ifShare) {
		this.ifShare = ifShare;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSpeakTime() {
		return speakTime;
	}

	public void setSpeakTime(String speakTime) {
		this.speakTime = speakTime;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Double getCtrlon() {
		return ctrlon;
	}

	public void setCtrlon(Double ctrlon) {
		this.ctrlon = ctrlon;
	}

	public Double getCtrlat() {
		return ctrlat;
	}

	public void setCtrlat(Double ctrlat) {
		this.ctrlat = ctrlat;
	}

}
