package com.wachoo.pangker.entity.index;

import java.io.Serializable;

public class IndexResponse implements Serializable{
	private boolean success;
	private data data;
	private error error;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public data getData() {
		return data;
	}
	public void setData(data data) {
		this.data = data;
	}
	public error getError() {
		return error;
	}
	public void setError(error error) {
		this.error = error;
	}
	
}

