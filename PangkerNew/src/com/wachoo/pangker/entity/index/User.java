﻿package com.wachoo.pangker.entity.index;

import com.wachoo.pangker.entity.Location;

/**
 * @author 王鑫    用户信息
 */
public class User {
	private int iconType=1;// 是否上传过头像。0:未上传 1：上传过
	private String userId;//用户id
	private String imNo; //Im no
	private String phoneNum;//用户手机号码
	private int userType;//	用户类型
	private String userName;//用户名
	private String nickName;//预留字段
	private int age;
	private long distance;//距离
	private String sign;//签名(普通用户签名)
	private String friendSign;//交友签名
	private String sex;
	private Location location;
	private String lastUpdateTime;//最后上传时间
	private String vipLevel;//会员级别
	
	int online = 0;//是否在线，暂时不用这个来做判断，用openfire提供的功能来验证用户是否在线

	/**
	 * @return the vipLevel
	 */
	public String getVipLevel() {
		return vipLevel;
	}


	/**
	 * @param vipLevel the vipLevel to set
	 */
	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}


	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}


	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}


	/**
	 * @return the friendSign
	 */
	public String getFriendSign() {
		return friendSign;
	}


	/**
	 * @param friendSign the friendSign to set
	 */
	public void setFriendSign(String friendSign) {
		this.friendSign = friendSign;
	}


	/**
	 * @return the lastUpdateTime
	 */
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}


	/**
	 * @param lastUpdateTime the lastUpdateTime to set
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}


	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}


	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}


	/**
	 * @return the iconType
	 */
	public Integer getIconType() {
		return iconType;
	}


	/**
	 * @param iconType the iconType to set
	 */
	public void setIconType(int iconType) {
		this.iconType = iconType;
	}


	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}


	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}


	
	
	/**
	 * @return the imNo
	 */
	public String getImNo() {
		return imNo;
	}


	/**
	 * @param imNo the imNo to set
	 */
	public void setImNo(String imNo) {
		this.imNo = imNo;
	}


	/**
	 * @return the phoneNum
	 */
	public String getPhoneNum() {
		return phoneNum;
	}


	/**
	 * @param phoneNum the phoneNum to set
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}


	/**
	 * @return the userType
	 */
	public int getUserType() {
		return userType;
	}


	/**
	 * @param userType the userType to set
	 */
	public void setUserType(int userType) {
		this.userType = userType;
	}


	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}


	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}


	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}


	


	

	/**
	 * @return the distance
	 */
	public long getDistance() {
		return distance;
	}


	/**
	 * @param distance the distance to set
	 */
	public void setDistance(long distance) {
		this.distance = distance;
	}


	/**
	 * @return the sign
	 */
	public String getSign() {
		return sign;
	}


	/**
	 * @param sign the sign to set
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}


	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}


	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}


	/**
	 * @return the online
	 */
	public int getOnline() {
		return online;
	}


	/**
	 * @param online the online to set
	 */
	public void setOnline(int online) {
		this.online = online;
	}

	
	
}
