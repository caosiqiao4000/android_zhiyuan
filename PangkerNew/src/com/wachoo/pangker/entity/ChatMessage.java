package com.wachoo.pangker.entity;

public class ChatMessage extends UserItem {

	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	public static final long RESEND_CHATMESSAGE_TIME = 300000;
	private int id; // 私信编号
	public static final String MSG_FROM_ME = "0";
	public static final String MSG_TO_ME = "1";

	public static final String MSG_FLAG = "msg_flag";// 是否带着消息进入Chat界面
	public static final String MSG_CONTENT = "msg_filename";// 是否带着消息进入Chat界面
	public static final String MSG_FILEPATH = "msg_filepath";// 是否带着消息进入Chat界面
	public static final String MSG_TYPE = "msg_type";// 是否带着消息进入Chat界面

	private String myUserId;// 登录用户的uid
	private String content; // 信息的内容
	private String time; // 发信息的时间
	private String direction; // 谁发的信息(0表示登陆者,1表示聊天对象)
	private long fileSize;
	private int progress;
	// 信息的状态, 0:文字信息，不用处理，1:开始发送,2:发送失败,3:发送成功,4:拒绝接收,5:接收失败,6:接收成功,
	private int status;
	private String filepath;
	private int msgType;// 发送类型, 对应PangkerConstant----资源类型 //
	private int messageRevc = 0;// >>>>>>>IM服务是否接收到client发送的消息 0:未 1：已经送达 2：失败

	public int getMessageRevc() {
		return messageRevc;
	}

	public void setMessageRevc(int messageRevc) {
		this.messageRevc = messageRevc;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the myUserId
	 */
	public String getMyUserId() {
		return myUserId;
	}

	/**
	 * @param myUserId
	 *            the myUserId to set
	 */
	public void setMyUserId(String myUserId) {
		this.myUserId = myUserId;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * @param direction
	 *            the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public int getMsgType() {
		return msgType;
	}

	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}

	public String getFileName() {
		return getContent().substring(getContent().indexOf("：") + 1);
	}

	@Override
	public String toString() {
		return "UserMsg [content=" + content + ", direction=" + direction + ", msgType=" + msgType + ", myUserId="
				+ myUserId + ", status=" + status + ", time=" + time + "]";
	}

}
