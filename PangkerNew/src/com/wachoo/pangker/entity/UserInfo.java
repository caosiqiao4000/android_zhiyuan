package com.wachoo.pangker.entity;

import java.io.Serializable;

import com.wachoo.pangker.Configuration;

public class UserInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String USERID = "userid";
	public static final String USERNAME = "userName";
	public static final String USERSIGN = "userSign";
	private String userId;// 用户id
	private String account;
	private String imNo;// IM 号码
	private String friendSign;// >>>wangxin add 交友的标签
	private String phone;// 手机号码（旁客用户）
	private String callphone; //被呼叫号码

	private String userName;// 用户名
	private String nickName;// 昵称
	private int passwordEncryType;// 加密方式
	private String password;// 密码内容
	private byte[] iconBytes;// 用户头像
	private String birthday;// 生日 生日格式 “yyyy-MM-dd”

	private int age;// 年龄
	private int identity;// 身份类型
	private int sex;// 性别
	private String sign;// 签名
	private String country;// 国家 （中国）
	private String provinceId;// 省份 汉字
	private String city;// 城市 汉字
	private String area;// 区域 汉字
	private int score;// 积分

	private String websiteName;//网站名称
	
	private int iconType = 0;// 是否上传过头像。0:未上传 1：上传过
	private Integer constellation;// 星座
	private Integer zodiac;// 生肖

	// // 12星座
	// { "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座","射手座", "摩羯座",
	// "水瓶座", "双鱼座" };
	// { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	//
	// //12生肖
	// { "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪" };
	// { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	private String email;// 邮箱
	private String realName;// 真实姓名
	private String work;// 工作 汉字

	private String bloodType;// 血型 A、B、AB、O、其他
	private String address;// 联系地址
	private String pNote;// 个人说明
	private int degree;// 学历
	private String school;// 学校
	private String pHomeUrl;// 个人主页
	private String imsi;

	// 经纬度
	private double ctrlon;
	private double ctrlat;
	//后台不同接口返经纬度字段名称可能不同
	private Double longitude;// 经度
	private Double latitude;// 纬度

	private int friendType;// 是否是交友用户 0 ：不是 1：是
	private int vipLevel;// 会员级别

	private Integer weight;// 体重
	private Integer height;// 身高
	private String hobby;// 爱好
	private Integer mfriendtype;// 交友类别：//0：男， 1： 女， 2 ：普通
	private String intention;// 交友描述
	private Integer userStatus;// 10正常11漂移12穿越
	
	public Integer getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

	/**
	 * @return the friendType
	 */
	public int getFriendType() {
		return friendType;
	}

	/**
	 * @param friendType
	 *            the friendType to set
	 */
	public void setFriendType(int friendType) {
		this.friendType = friendType;
	}

	/**
	 * @return the vipLevel
	 */
	public int getVipLevel() {
		return vipLevel;
	}

	/**
	 * @param vipLevel
	 *            the vipLevel to set
	 */
	public void setVipLevel(int vipLevel) {
		this.vipLevel = vipLevel;
	}

	/**
	 * @return the friendSign
	 */
	public String getFriendSign() {
		return friendSign;
	}

	/**
	 * @param friendSign
	 *            the friendSign to set
	 */
	public void setFriendSign(String friendSign) {
		this.friendSign = friendSign;
	}

	/**
	 * @return the imsi
	 */
	public String getImsi() {
		return imsi;
	}

	/**
	 * @param imsi
	 *            the imsi to set
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @param account
	 *            the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return the imNo
	 */
	public String getImNo() {
		return imNo;
	}

	/**
	 * @param imNo
	 *            the imNo to set
	 */
	public void setImNo(String imNo) {
		this.imNo = imNo;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the pNote
	 */
	public String getpNote() {
		return pNote;
	}

	/**
	 * @param pNote
	 *            the pNote to set
	 */
	public void setpNote(String pNote) {
		this.pNote = pNote;
	}

	/**
	 * @return the degree
	 */
	public int getDegree() {
		return degree;
	}

	/**
	 * @param degree
	 *            the degree to set
	 */
	public void setDegree(int degree) {
		this.degree = degree;
	}

	/**
	 * @return the school
	 */
	public String getSchool() {
		return school;
	}

	/**
	 * @param school
	 *            the school to set
	 */
	public void setSchool(String school) {
		this.school = school;
	}

	/**
	 * @return the pHomeUrl
	 */
	public String getpHomeUrl() {
		return pHomeUrl;
	}

	/**
	 * @param pHomeUrl
	 *            the pHomeUrl to set
	 */
	public void setpHomeUrl(String pHomeUrl) {
		this.pHomeUrl = pHomeUrl;
	}

	/**
	 * @return the work
	 */
	public String getWork() {
		return work;
	}

	/**
	 * @return the bloodType
	 */
	public String getBloodType() {
		return bloodType;
	}

	/**
	 * @param bloodType
	 *            the bloodType to set
	 */
	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	/**
	 * @param work
	 *            the work to set
	 */
	public void setWork(String work) {
		this.work = work;
	}

	/**
	 * @return the iconBytes
	 */
	public byte[] getIconBytes() {
		return iconBytes;
	}

	/**
	 * @param iconBytes
	 *            the iconBytes to set
	 */
	public void setIconBytes(byte[] iconBytes) {
		this.iconBytes = iconBytes;
	}

	/**
	 * @return the constellation
	 */
	public Integer getConstellation() {
		return constellation;
	}

	/**
	 * @param constellation
	 *            the constellation to set
	 */
	public void setConstellation(Integer constellation) {
		this.constellation = constellation;
	}

	/**
	 * @return the zodiac
	 */
	public Integer getZodiac() {
		return zodiac;
	}

	/**
	 * @param zodiac
	 *            the zodiac to set
	 */
	public void setZodiac(Integer zodiac) {
		this.zodiac = zodiac;
	}

	/**
	 * @return the realName
	 */
	public String getRealName() {
		return realName;
	}

	/**
	 * @param realName
	 *            the realName to set
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @return the iconType
	 */
	public int getIconType() {
		return iconType;
	}

	/**
	 * @param iconType
	 *            the iconType to set
	 */
	public void setIconType(int iconType) {
		this.iconType = iconType;
	}

	/**
	 * @return the userid
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * @param nickName
	 *            the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * @return the passwordEncryType
	 */
	public int getPasswordEncryType() {
		return passwordEncryType;
	}

	/**
	 * @param passwordEncryType
	 *            the passwordEncryType to set
	 */
	public void setPasswordEncryType(int passwordEncryType) {
		this.passwordEncryType = passwordEncryType;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the portraitIconUrl
	 */
	public String getIconUrl() {
		return Configuration.getIconUrl() + userId;
	}

	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday
	 *            the birthday to set
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the identity
	 */
	public int getIdentity() {
		return identity;
	}

	/**
	 * @param identity
	 *            the identity to set
	 */
	public void setIdentity(int identity) {
		this.identity = identity;
	}

	/**
	 * @return the sex
	 */
	public int getSex() {
		return sex;
	}

	/**
	 * @param sex
	 *            the sex to set
	 */
	public void setSex(int sex) {
		this.sex = sex;
	}

	/**
	 * @return the sign
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * @param sign
	 *            the sign to set
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

	/**
	 * @param age
	 *            the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score
	 *            the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @return the ctrlon
	 */
	public double getCtrlon() {
		return ctrlon;
	}

	/**
	 * @param ctrlon
	 *            the ctrlon to set
	 */
	public void setCtrlon(double ctrlon) {
		this.ctrlon = ctrlon;
	}

	/**
	 * @return the ctrlat
	 */
	public double getCtrlat() {
		return ctrlat;
	}

	/**
	 * @param ctrlat
	 *            the ctrlat to set
	 */
	public void setCtrlat(double ctrlat) {
		this.ctrlat = ctrlat;
	}

	public String getPortraitIconUrl() {
		return Configuration.getIconUrl() + userId;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public Integer getMfriendtype() {
		return mfriendtype;
	}

	public void setMfriendtype(Integer mfriendtype) {
		this.mfriendtype = mfriendtype;
	}

	public String getIntention() {
		return intention;
	}

	public void setIntention(String intention) {
		this.intention = intention;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getCallphone() {
		return callphone;
	}

	public void setCallphone(String callphone) {
		this.callphone = callphone;
	}

	public String getWebsiteName() {
		return websiteName;
	}

	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}

}
