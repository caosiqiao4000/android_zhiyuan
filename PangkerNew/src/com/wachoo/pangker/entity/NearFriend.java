package com.wachoo.pangker.entity;

import com.wachoo.pangker.util.Util;

public class NearFriend {

	private long noticeIntervalTime = 3600000;//通知间隔
	private String userId;
	private String noticeTime;
	private Location location;
	private boolean isNotice = false;// 是否通知过
	private double distance;// distance

	/**
	 * 
	 * @param userId
	 * @param location
	 * @param distance
	 */
	public NearFriend(String userId, Location location, double distance) {
		super();
		this.userId = userId;
		this.location = location;
		this.distance = distance;
	}

	/**
	 * 
	 * @param userId
	 * @param noticeTime
	 * @param location
	 * @param isNotice
	 * @param distance
	 */
	public NearFriend(String userId, String noticeTime, Location location, boolean isNotice, double distance) {
		super();
		this.userId = userId;
		this.noticeTime = noticeTime;
		this.location = location;
		this.isNotice = isNotice;
		this.distance = distance;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the noticeTime
	 */
	public String getNoticeTime() {
		return noticeTime;
	}

	/**
	 * @param noticeTime
	 *            the noticeTime to set
	 */
	public void setNoticeTime(String noticeTime) {
		this.noticeTime = noticeTime;
	}

	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * @return the isNotice
	 */
	public boolean isNotice() {
		long timeInterval = Util.timeInterval(this.noticeTime, Util.getSysNowTime());		
		if(timeInterval >=noticeIntervalTime )
			return false;
		return isNotice;
	}

	/**
	 * @param isNotice
	 *            the isNotice to set
	 */
	public void setNotice(boolean isNotice) {
		this.isNotice = isNotice;
	}

	/**
	 * @return the distance
	 */
	public double getDistance() {
		return distance;
	}

	/**
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(double distance) {
		this.distance = distance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NearFriend [distance=" + distance + ", isNotice=" + isNotice + ", location=" + location
				+ ", noticeTime=" + noticeTime + ", userId=" + userId + "]";
	}

}
