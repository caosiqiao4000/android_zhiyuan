package com.wachoo.pangker.entity;

/**
 * 
 * @author wangxin
 *
 */
public class NearUserItem extends UserItem{

	private String lastUpdateTime;//最后一次上传位置时间
	private String friendSign;//交友标签
	/**
	 * @return the lastUpdateTime
	 */
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	/**
	 * @param lastUpdateTime the lastUpdateTime to set
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	/**
	 * @return the friendSign
	 */
	public String getFriendSign() {
		return friendSign;
	}
	/**
	 * @param friendSign the friendSign to set
	 */
	public void setFriendSign(String friendSign) {
		this.friendSign = friendSign;
	}
	
}
