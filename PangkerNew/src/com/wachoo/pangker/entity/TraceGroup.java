package com.wachoo.pangker.entity;

import com.wachoo.pangker.server.response.UserGroup;

public class TraceGroup extends UserGroup{

	private String description;//描述
	private String theme;//主题
	private int Max;//最大人数
	private boolean pwd = false;//需要密码才能进入 true :需要 ；false ：不需要 默认不需要
	private String pwdtext;//密码:
	private int seeOwner = 0;//能够发现占有者真实 JID 的角色:
	private int active;//是否是当前使用id
	
	public TraceGroup(){}
	
	public TraceGroup(UserGroup group){
		setClientId(group.getClientId());
		setGroupId(group.getGroupId());
		setGroupName(group.getGroupName());
		setCreateTime(group.getCreateTime());
		setGroupSid(group.getGroupSid());
		setUid(group.getUid());
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the theme
	 */
	public String getTheme() {
		return theme;
	}
	/**
	 * @param theme the theme to set
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}
	/**
	 * @return the max
	 */
	public int getMax() {
		return Max;
	}
	/**
	 * @param max the max to set
	 */
	public void setMax(int max) {
		Max = max;
	}
	/**
	 * @return the pwd
	 */
	public boolean isPwd() {
		return pwd;
	}
	/**
	 * @param pwd the pwd to set
	 */
	public void setPwd(boolean pwd) {
		this.pwd = pwd;
	}
	/**
	 * @return the pwdtext
	 */
	public String getPwdtext() {
		return pwdtext;
	}
	/**
	 * @param pwdtext the pwdtext to set
	 */
	public void setPwdtext(String pwdtext) {
		this.pwdtext = pwdtext;
	}
	/**
	 * @return the seeOwner
	 */
	public int getSeeOwner() {
		return seeOwner;
	}
	/**
	 * @param seeOwner the seeOwner to set
	 */
	public void setSeeOwner(int seeOwner) {
		this.seeOwner = seeOwner;
	}
	/**
	 * @return the active
	 */
	public int getActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(int active) {
		this.active = active;
	}	
}
