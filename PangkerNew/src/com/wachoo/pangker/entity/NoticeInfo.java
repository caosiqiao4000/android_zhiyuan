package com.wachoo.pangker.entity;

import java.io.Serializable;

public class NoticeInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int NOTICE_FRIEND_ADD = 10;//好友申请
	public static final int NOTICE_FRIEND_DEL = 11;//好友解除
	public static final int NOTICE_FRIEND_ADDED = 12;//已经是好友
	public static final int NOTICE_FRIEND_DELED = 13;//已经解除
	public static final int NOTICE_ATTENT_DEL = 14;
	public static final int NOTICE_FANS_ADD = 15;//
	public static final int NOTICE_FANS_DEL = 16;//移除了您对TA的关注
	public static final int NOTICE_CONTACTS_REGISTER = 17;//通讯录中有人注册了旁客
	public static final int NOTICE_ATTENT_ADD = 18;
	
	//追踪相关通知
	public static final int NOTICE_STACK_REQUEST = 21;
	public static final int NOTICE_STACK_START = 22;
	public static final int NOTICE_STACK_STOP = 23;
	public static final int NOTICE_TRACERELATIVE = 24;//亲友注册旁客邀请成功后返回消息
	
	//漂移到指定用户相关通知
	public static final int NOTICE_DRIFTUSER_REQUEST = 31;//申请
	public static final int NOTICE_DRIFTUSER_RESPONSE = 32;//应答
	public static final int NOTICE_DRIFTUSER_LOCATION_CHANGED = 33;//位置信息变更传递
	public static final int NOTICE_DRIFTUSER_SUCCESS = 34;//漂移成功!
	public static final int NOTICE_DRIFTUSER_TERMINATE = 35;//目标用户终止漂移
	public static final int NOTICE_DRIFTUSER_STOP = 36;//申请用户停止漂移
	//群组邀请
	public static final int NOTICE_TRACE_REQUEST = 41;//群组邀请 
	public static final int NOTICE_TRACE_RESPONSE = 42;//回复群组邀请 
	
	public static final int NOTICE_NET_ADDRESS_RESPONSE = 51;//申请加入单位通讯录
	public static final int NOTICE_NET_ADDRESS_INVITE_RESPONSE = 52;//邀请加入单位通讯录
	public static final int NOTICE_NET_ADDRESS_DELETED = 53;//管理员删除用户通知用户
	public static final int NOTICE_NET_ADDRESS_APPLY_EXIT = 54;//申请退出某个单位通讯录
	public static final int NOTICE_NET_ADDRESS_APPLY_AGRESS = 55;//管理员同意当前用户加入单位通讯录
	public static final int NOTICE_NET_ADDRESS_INVITE_REFUSE = 56;//被邀请人拒绝加入单位通讯录
	public static final int NOTICE_NET_ADDRESS_INVITE_AGRESS = 57;//被邀请人同意 加入单位通讯录
	public static final int NOTICE_NET_ADDRESS_EXIT_AGREE = 58;//同意 退出单位通讯录
	public static final int NOTICE_NET_ADDRESS_EXIT_DEAGREE = 59;//不同意 退出单位通讯录
	public static final int NOTICE_NET_ADDRESS_APPLY_DEAGRESS = 60;//管理员不同意当前用户加入单位通讯录
	
	public static final int NOTICE_RES_RECOMMEND = 70;//资源推荐
	
	private String opeUserid; // 用户的UserID
	private String opeUsername; // 用户的名称
	private int type;//通知类型  10:
	private String notice;//当前的通知内容
	private String time;//通知时间
	private int status;//通知状态 0：未读   1：已读  2：已经处理 （主要针对需要用户确认结果的，例如添加好友，回复同意还是不同意）,也可以当做处理结果
	private long id;//数据库ID
	private String remark;//备注
	private String userId;
	private int msgType;
	/**
	 * //指向父消息Id，保存数据时，先看看这个用户来是否有个消息，如果没有就将parentId = 0；
	 * 如果这个用户已经来过了信息，就将这个消息的parentId指向第一次保存消息的id，并将之前信息数加一
	 */
	
	public String getOpeUsername() {
		return opeUsername;
	}
	public void setOpeUsername(String opeUsername) {
		this.opeUsername = opeUsername;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the opeUserid
	 */
	public String getOpeUserid() {
		return opeUserid;
	}
	/**
	 * @param opeUserid the opeUserid to set
	 */
	public void setOpeUserid(String opeUserid) {
		this.opeUserid = opeUserid;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the notice
	 */
	public String getNotice() {
		return notice;
	}
	/**
	 * @param notice the notice to set
	 */
	public void setNotice(String notice) {
		this.notice = notice;
	}
	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	public int getMsgType() {
		return msgType;
	}
	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}

}
