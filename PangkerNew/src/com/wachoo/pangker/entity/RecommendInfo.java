package com.wachoo.pangker.entity;

import java.io.Serializable;
import java.util.List;

import android.text.InputFilter;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.server.response.UserStatus;

@SuppressWarnings("serial")
public class RecommendInfo implements Serializable {

	private int id;
	private String userId;// 信息用户id
	private String fromId;// 推荐人的id
	private String fromName;// 推荐人的名称
	private String resId;// 资源id
	private String datetime;// 收到时间
	private String reason;// 推荐理由
	private String resName;// 资源名称
	private String remark;// 备注
	private String singer;
	private Integer boxType;
	private String mo;
	private int type;// 推荐类型,资源(图片，音乐，应用，文档，视频)，群组==
	private int status;// 是否已经查看
	private List<UserStatus> userStatus;
	/**
	 * modify by lb,
	 * 这个不是消息提示（回复评论除外，资源不是自己发布的，就是自己转播的），
	 * 推荐是可以推荐别人的资源，那么无法知道资源是通过哪个用户发现资源的。
	**/
	private String shareUid;//是从哪个用户知道这个资源的，可能是发布人，可能是转播人，对应数据库表中的remark5

	// >>>>>>>>>>>>显示更多
	private String btnShowText = PangkerConstant.SHOW_MORE_TEXT;
	private InputFilter[] filters = null;

	public RecommendInfo() {
	}

	public RecommendInfo(int id, String userId, String fromId, String fromName, String resId,
			String datetime, String reason, String resName, String remark, String singer, Integer boxType,
			String mo, List<UserStatus> userStatus, int type, int status) {
		super();
		this.id = id;
		this.userId = userId;
		this.fromId = fromId;
		this.fromName = fromName;
		this.resId = resId;
		this.datetime = datetime;
		this.reason = reason;
		this.resName = resName;
		this.remark = remark;
		this.singer = singer;
		this.boxType = boxType;
		this.mo = mo;
		this.userStatus = userStatus;
		this.type = type;
		this.status = status;
	}

	public String getBtnShowText() {
		return btnShowText;
	}

	public void setBtnShowText(String btnShowText) {
		this.btnShowText = btnShowText;
	}

	public InputFilter[] getFilters() {
		if (filters == null) {
			filters = new InputFilter[1];
			filters[0] = new InputFilter.LengthFilter(PangkerConstant.showLength);
		}
		return filters;
	}

	public void setFilters(InputFilter[] filters) {
		this.filters = filters;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFromId() {
		return fromId;
	}

	public void setFromId(String fromId) {
		this.fromId = fromId;
	}

	public String getResId() {
		return resId;
	}

	public void setResId(String resId) {
		this.resId = resId;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "RecommendInfo [userId=" + userId + ", fromName=" + fromName + ", resId=" + resId
				+ ", reason=" + reason + ", resName=" + resName + "]";
	}

	public Integer getBoxType() {
		return boxType;
	}

	public void setBoxType(Integer boxType) {
		this.boxType = boxType;
	}

	public String getMo() {
		return mo;
	}

	public void setMo(String mo) {
		this.mo = mo;
	}

	public List<UserStatus> getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(List<UserStatus> userStatus) {
		this.userStatus = userStatus;
	}

	public String getShareUid() {
		return shareUid;
	}

	public void setShareUid(String shareUid) {
		this.shareUid = shareUid;
	}

}
