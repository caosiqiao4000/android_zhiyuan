package com.wachoo.pangker.entity;

/**
 * @author yuanly
 * 用来指定返回的子列表的参数
 */
public class Limit {
int offset;
int length;

public Limit(){
}

public Limit(int offset,int length){
	this.offset=offset;
	this.length=length;
}
/**
 * @return the offset
 */
public int getOffset() {
	return offset;
}
/**
 * @param offset the offset to set
 */
public void setOffset(int offset) {
	this.offset = offset;
}
/**
 * @return the length
 */
public int getLength() {
	return length;
}
/**
 * @param length the length to set
 */
public void setLength(int length) {
	this.length = length;
}

}
