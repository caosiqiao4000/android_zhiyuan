package com.wachoo.pangker.entity;

import java.io.Serializable;

import com.wachoo.pangker.PangkerConstant;

/**
 * 说明：parentId 与本地自增长的Id没有关系
 * 
 * @author Administrator
 * 
 */
public class DirInfo implements Serializable {

	// 1）所有好友可访问（默认）------
	// 2）所有亲友可访问
	// 3）指定好友组可访问（点击选择某一个好友组，只能选择一个）
	// 4）仅本人可访问-----------
	// 5）通过密码访问（点击可设置目录密码）

	// >>>>>>>>>>所有人
	public static final int[] RES_VISIBLE = { // >>>>>>>>目录访问权限
	PangkerConstant.Role_FRIEND, // 1）所有好友可访问（默认）------
			PangkerConstant.Role_RELATIVES,// 2）所有亲友可访问
			PangkerConstant.Role_CONTACT_GROUP, // 3）指定好友组可访问（点击选择某一个好友组，只能选择一个）
			PangkerConstant.Role_PRIVATE, // 4）仅本人可访问-----------
			PangkerConstant.Role_PASSWORD };// 5）通过密码访问（点击可设置目录密码）

	private static final long serialVersionUID = 1L;

	public final static int DEAL_SHARE = 1;// 资源分享

	public final static int DEAL_COLLECT = 2;// 资源收藏

	public final static int DEAL_TRANS = 3;// 资源转播

	public final static int DEAL_SELECT = 5;// 资源选择
	public final static int DEAL_DOWLAND = 6;// 资源下载
	public final static int DEAL_DOWHALF = 7;// 未下载完成

	public final static int DEAL_COLLECT_TRANS = 8;

	public final static int ISFILE = 1;// 文件资源
	public final static int ISDIR = 0;// 目录资源

	private int _id;
	private String uid;// 目录所属用户uid
	private String dirId; // 目录Id
	private String parentDirId;//
	private String dirName;// 目录名称
	private String dirFormat;// 文件格式
	private int fileCount;// 目录：文件个数；文件：大小
	private String dirCover;// 目录封面，仅仅是对应用资源和音乐
	private String latlng;// 经纬度
	private int resType;// 资源类别
	private int localid;//

	private String path;// 可以是文件名，如果是0就是默认的标签,可以是网络路径
	private int isfile;// 文件类型，是否是文件夹。0表示文件夹，1：文件。
	private int deal;// 处理形式，收藏，还是分享.
	private String createtime;// 创建时间
	private String appraisement;// 资源描述
	private int remark;// 阅读——————书签进度

	private Integer visitType;
	private String visitGroups;
	private long shareTimes;// 分享次数
	private long downloadTimes;// 下载次数
	private long favorTimes;// 收藏次数
	private long commentTimes;// 评论次数
	private int score;//
	private int source;// 0：原创 1：收藏

	private String password;

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public long getShareTimes() {
		return shareTimes;
	}

	public void setShareTimes(long shareTimes) {
		this.shareTimes = shareTimes;
	}

	public long getDownloadTimes() {
		return downloadTimes;
	}

	public void setDownloadTimes(long downloadTimes) {
		this.downloadTimes = downloadTimes;
	}

	public long getFavorTimes() {
		return favorTimes;
	}

	public void setFavorTimes(long favorTimes) {
		this.favorTimes = favorTimes;
	}

	public long getCommentTimes() {
		return commentTimes;
	}

	public void setCommentTimes(long commentTimes) {
		this.commentTimes = commentTimes;
	}

	public DirInfo() {
		// TODO Auto-generated constructor stub
	}

	public DirInfo(String dirId) {
		// TODO Auto-generated constructor stub
		this.dirId = dirId;
	}

	public DirInfo(String dirId, int resType) {
		super();
		this.dirId = dirId;
		this.resType = resType;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int id) {
		_id = id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getDirId() {
		return dirId;
	}

	public void setDirId(String dirId) {
		this.dirId = dirId;
	}

	public String getParentDirId() {
		return parentDirId;
	}

	public void setParentDirId(String parentDirId) {
		this.parentDirId = parentDirId;
	}

	public String getDirName() {
		return dirName;
	}

	public String getDirNames() {
		return dirName + "根目录";
	}

	public void setDirName(String dirName) {
		this.dirName = dirName;
	}

	public int getResType() {
		return resType;
	}

	public void setResType(int resType) {
		this.resType = resType;
	}

	public int getFileCount() {
		return fileCount;
	}

	public void setFileCount(int fileCount) {
		this.fileCount = fileCount;
	}

	public String getDirCover() {
		return dirCover;
	}

	public void setDirCover(String dirCover) {
		this.dirCover = dirCover;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getLatlng() {
		return latlng;
	}

	public void setLatlng(String latlng) {
		this.latlng = latlng;
	}

	public int getIsfile() {
		return isfile;
	}

	public void setIsfile(int isfile) {
		this.isfile = isfile;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public int getDeal() {
		return deal;
	}

	public void setDeal(int deal) {
		this.deal = deal;
	}

	public String getAppraisement() {
		return appraisement;
	}

	public void setAppraisement(String appraisement) {
		this.appraisement = appraisement;
	}

	public int getLocalid() {
		return localid;
	}

	public void setLocalid(int localid) {
		this.localid = localid;
	}

	public String getDirFormat() {
		return dirFormat;
	}

	public void setDirFormat(String dirFormat) {
		this.dirFormat = dirFormat;
	}

	public int getRemark() {
		return remark;
	}

	public void setRemark(int remark) {
		this.remark = remark;
	}

	public Integer getVisitType() {
		if (visitType == null) {
			return -1;
		}
		return visitType;
	}

	public void setVisitType(Integer visitType) {
		this.visitType = visitType;
	}

	public String getVisitGroups() {
		return visitGroups;
	}

	public void setVisitGroups(String visitGroups) {
		this.visitGroups = visitGroups;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

}
