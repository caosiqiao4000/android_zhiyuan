package com.wachoo.pangker.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Area implements Serializable {
	
	public static final String AREA_TAG = "area";
	private String areaAddress;
	private String countyID;
	private String provinceID;
	private String cityID;
	private String areaID;

	/**
	 * @return the areaAddress
	 */
	public String getAreaAddress() {
		return areaAddress;
	}

	/**
	 * @param areaAddress
	 *            the areaAddress to set
	 */
	public void setAreaAddress(String areaAddress) {
		this.areaAddress = areaAddress;
	}

	/**
	 * @return the countyID
	 */
	public String getCountyID() {
		return countyID;
	}

	/**
	 * @param countyID
	 *            the countyID to set
	 */
	public void setCountyID(String countyID) {
		this.countyID = countyID;
	}

	/**
	 * @return the provinceID
	 */
	public String getProvinceID() {
		return provinceID;
	}

	/**
	 * @param provinceID
	 *            the provinceID to set
	 */
	public void setProvinceID(String provinceID) {
		this.provinceID = provinceID;
	}

	/**
	 * @return the cityID
	 */
	public String getCityID() {
		return cityID;
	}

	/**
	 * @param cityID
	 *            the cityID to set
	 */
	public void setCityID(String cityID) {
		this.cityID = cityID;
	}

	/**
	 * @return the areaID
	 */
	public String getAreaID() {
		return areaID;
	}

	/**
	 * @param areaID
	 *            the areaID to set
	 */
	public void setAreaID(String areaID) {
		this.areaID = areaID;
	}
}
