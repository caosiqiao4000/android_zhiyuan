package com.wachoo.pangker.entity;

import java.util.List;

public class DirectoryFiles {
	private List<LocalFileItem> Dirs;// 目录集合
	private List<LocalFileItem> Files;// 文件结合

	public List<LocalFileItem> getDirs() {
		return Dirs;
	}

	public void setDirs(List<LocalFileItem> dirs) {
		Dirs = dirs;
	}

	public List<LocalFileItem> getFiles() {
		return Files;
	}

	public void setFiles(List<LocalFileItem> files) {
		Files = files;
	}

}
