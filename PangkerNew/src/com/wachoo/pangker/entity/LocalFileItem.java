package com.wachoo.pangker.entity;

import com.wachoo.pangker.PangkerConstant;

/**
 * 本地文件类
 * 
 * @author wangxin
 * 
 */
public class LocalFileItem implements Comparable<LocalFileItem> {

	private String filePath;// >>>文件Path >>>lb
	private String fileName; // >>>文件名称
	private int fileType;// >>>文件类型 0 :文件 1：类型
	private int iconResID;// >>>图标对应的资源id
	private long fileSize; // >>>文件大小
	public static final int DIRECTORY_TYPE = 0;
	public static final int FILE_TYPE = 1;

	// >>>>>>>文件类型 旁客支持打开的文件类型
	public static final String TYPE_MP3 = ".mp3";
	public static final String TYPE_APK = ".apk";
	public static final String TYPE_TXT = ".txt";
	public static final String TYPE_DOC = ".doc";
	public static final String TYPE_JPG = ".jpg";
	public static final String TYPE_PNG = ".png";

	public LocalFileItem(String filePath, String fileName, int fileType, int iconResID, long l) {
		super();
		this.filePath = filePath;
		this.fileName = fileName;
		this.fileType = fileType;
		this.iconResID = iconResID;
		this.fileSize = l;
	}

	@Override
	public int compareTo(LocalFileItem another) {
		// TODO Auto-generated method stub
		// >>>>排序
		if (this.fileName != null)
			return this.fileName.compareTo(another.getFileName());
		else
			throw new IllegalArgumentException();

	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	public int getIconResID() {
		return iconResID;
	}

	public void setIconResID(int iconResID) {
		this.iconResID = iconResID;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	
	public boolean isApk() {
		// TODO Auto-generated method stub
        return fileName.endsWith(TYPE_APK) ;
	}
	
	public boolean isDoc() {
		// TODO Auto-generated method stub
        return fileName.endsWith(TYPE_TXT) || fileName.endsWith(TYPE_DOC);
	}
	
	public boolean isMusic(){
		return fileName.endsWith(TYPE_MP3) ;
	}
	
	public boolean isPic(){
		return fileName.endsWith(TYPE_JPG)|| fileName.endsWith(TYPE_PNG) ;
	}

	public boolean isInByType(int type) {
		switch (type) {
		case PangkerConstant.RES_APPLICATION:
            return isApk();
		case PangkerConstant.RES_DOCUMENT:
			return isDoc();
		case PangkerConstant.RES_PICTURE:
			return isPic();
		case PangkerConstant.RES_MUSIC:
			return isMusic();
		}
		return false;
	}
}
