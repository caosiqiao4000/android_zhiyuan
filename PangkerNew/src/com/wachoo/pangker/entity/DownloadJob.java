package com.wachoo.pangker.entity;

import java.io.Serializable;

import android.content.Context;

import com.wachoo.pangker.downupload.DownLoadManager;
import com.wachoo.pangker.downupload.DownloadAsyncTask;

public class DownloadJob implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7846236482364628L;

	public static final int DOWNLOAD_NET_MOBILE_NOTICE = 0;// <item>总是提示（2G/3G网络）</item>
	public static final int DOWNLOAD_NET_ALL = 1;// <item>2G/3G允许下载</item>
	public static final int DOWNLOAD_NET_ONLY_WIFI = 2;// <item>仅wifi网络下载</item>
	
	// >>>>>>>临时文件下载后缀
	public static final String SUFFIX = ".dat";
	public static final String DOWNLOAD_JOB_ID = "DOWNLOAD_JOB_ID";
	public static final String SUFFIX_RESID = "-";

	public static final int DOWNLOAD_INIT = 0;// 定义三种下载的状态：初始化状态，正在下载状态，暂停状态
	public static final int DOWNLOADING = 1;// 下载中
	public static final int DOWNLOAD_FAILE = 9;// 下载失败
	public static final int DOWNLOAD_PAUSE = 2;
	public static final int DOWNLOAD_SUCCESS = 3;
	public static final int DOWNLOAD_CANCEL = 4;
	public static final int SOCKETEX = 6;// 网络异常
	public static final int DOWNLOAD_REFUSE = 7;// 下载拒绝
	public static final int DOWNLOAD_STOP = 8;// 停止下载

	public static final int DOWNLOAD_DIRECT = 10;
	public static final int DOWNLOAD_WIAT = 11;

	private int id;
	private String userId;// >>>>>>>用户id
	private int endPos;// 结束点
	private int doneSize;// 完成度
	private String url;// 下载器网络标识
	private String fileName;// 下载的文件名称，带后缀
	private String saveFileName;// >>>>>>>文件本地保存名称
	private String savePath;
	private int type;// 资源类型
	private String dwonTime;
	private int status;// 资源下载的状态
	private boolean isopen = false;
	private DownLoadManager mDownLoadManager;
	private int mResID;

	private int downloadType = DOWNLOAD_DIRECT;

	public DownLoadManager getmDownLoadManager() {
		return mDownLoadManager;
	}

	public void setmDownLoadManager(DownLoadManager mDownLoadManager) {
		this.mDownLoadManager = mDownLoadManager;
	}

	// >>>>>>>>>>>>>>下载任务
	private DownloadAsyncTask mDownloadAsyncTask;

	public DownloadAsyncTask getmDownloadAsyncTask() {
		return mDownloadAsyncTask;
	}

	public void initDownload(Context mContext) {
		mDownloadAsyncTask = new DownloadAsyncTask(this, mContext);

	}

	// >>>>>>>>>>开始下载任务
	public void startDownJob() {
		mDownloadAsyncTask.execute();
	}

	// >>>>>>取消任务
	public void cancelJob() {
		// >>>>>>>>设置取消状态
		setStatus(DOWNLOAD_CANCEL);
		// >>>>>>>通知上传取消
		if (this.mDownloadAsyncTask != null) {
			mDownloadAsyncTask.cancel();
		}
	}

	// >>>>>>取消任务
	public void stopJob() {
		// >>>>>>>>设置取消状态
		setStatus(DOWNLOAD_STOP);
		// >>>>>>>通知上传取消
		if (this.mDownloadAsyncTask != null) {
			mDownloadAsyncTask.stop();
		}
	}

	// >>>>>>>>暂停下载
	public void pause() {
		mDownloadAsyncTask.pause();
	}

	// >>>>>>>>继续下载
	public void resume() {
		mDownloadAsyncTask.resume();
	}

	public DownloadJob() {
		// TODO Auto-generated constructor stub
	}

	// >>>>>>>>取消任务

	public int getmResID() {
		return mResID;
	}

	public void setmResID(int mResID) {
		this.mResID = mResID;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDwonTime() {
		return dwonTime;
	}

	public void setDwonTime(String dwonTime) {
		this.dwonTime = dwonTime;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the savePath
	 */
	public String getSavePath() {
		return savePath;
	}

	/**
	 * @param savePath
	 *            the savePath to set
	 */
	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public int getEndPos() {
		return endPos;
	}

	public void setEndPos(int endPos) {
		this.endPos = endPos;
	}

	public int getDoneSize() {
		return doneSize;
	}

	public void setDoneSize(int doneSize) {
		this.doneSize = doneSize;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFileName() {
		return fileName;
	}

	public String getName() {
		if (fileName.contains(DownloadJob.SUFFIX)) {
			return fileName.substring(0, fileName.indexOf(DownloadJob.SUFFIX));
		}
		return fileName;
	}

	public String getSaveFileName() {
		return saveFileName;
	}

	public void setSaveFileName(String saveFileName) {
		this.saveFileName = saveFileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "DownloadInfo [threadId=" + id + ", endPos=" + endPos + ", compeleteSize=" + doneSize + "]";
	}

	public String getAbsolutePath() {
		return getSavePath() + "/" + getSaveFileName();
	}

	public String getUserid() {
		return userId;
	}

	public void setUserid(String userid) {
		this.userId = userid;
	}

	public boolean isIsopen() {
		return isopen;
	}

	public void setIsopen(boolean isopen) {
		this.isopen = isopen;
	}

	public int getDownloadType() {
		return downloadType;
	}

	public void setDownloadType(int downloadType) {
		this.downloadType = downloadType;
	}

}
