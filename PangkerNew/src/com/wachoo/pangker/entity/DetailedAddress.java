package com.wachoo.pangker.entity;

public class DetailedAddress {
	public static final String ADDRESS_UN_KNOW = "未知";
	public static final String ADDRESS_CITY_CHANGED = "我现在在";
	public static final String SPLIT = ".";

	public static final String LONG_NAME = "long_name";
	public static final String SHORT_NAME = "short_name";
	public static final String TYPES = "types";

	public static final String ROUTE = "route";
	public static final String SUBLOCALITY = "sublocality";
	public static final String LOCALITY = "locality";
	public static final String ADMINISTRATICE_AREA_LEVEL_1 = "administrative_area_level_1";
	public static final String COUNTRY = "country";

	public static final int ROUTE_INDEX = 0;
	public static final int SUBLOCALITY_INDEX = 1;
	public static final int LOCALITY_INDEX = 2;
	public static final int ADMINISTRATICE_AREA_LEVEL_1_INDEX = 3;
	public static final int COUNTRY_INDEX = 4;

	private String id;

	// >>>>>>详细地址，精确到街道 邮编
	private String addressStreetZip = ADDRESS_UN_KNOW;
	// >>>>>>详细地址，精确到街道 邮编
	private String addressStreet = ADDRESS_UN_KNOW;
	// >>>>>>地址城市区域
	private String street = ADDRESS_UN_KNOW;
	// >>>>>>地址城市区域
	private String area = ADDRESS_UN_KNOW;
	// >>>>>>地址城市
	private String city = ADDRESS_UN_KNOW;
	// >>>>>>地址省份
	private String province = ADDRESS_UN_KNOW;
	// >>>>>>地址国家
	private String country = ADDRESS_UN_KNOW;

	public String getAddressProvinceCityArea() {
		return getProvince() + this.SPLIT + getCity() + getArea();
	}

	public String getAddressCityArea() {
		return getCity() + this.SPLIT + getArea();
	}

	public String getAddressCityChanged() {
		return ADDRESS_CITY_CHANGED + getCity();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddressStreetZip() {
		return addressStreetZip;
	}

	public void setAddressStreetZip(String addressStreetZip) {
		this.addressStreetZip = addressStreetZip;
	}

	public String getAddressStreet() {
		return addressStreet;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
