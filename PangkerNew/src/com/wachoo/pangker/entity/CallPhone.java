package com.wachoo.pangker.entity;

import java.io.Serializable;
/**
 * 
 * @TODO旁客呼叫号码 （用于保存用户曾经设置过的号码）
 * 
 * @author zhengjy
 *
 *  2013-4-2
 *
 */
@SuppressWarnings("serial")
public class CallPhone implements Serializable {
	
	private String id;
	private String callPhone; //呼叫号码
	private String userid;//设置用户ID
	
	public CallPhone(){}
	public CallPhone(String id, String callPhone, String userid) {
		super();
		this.id = id;
		this.callPhone = callPhone;
		this.userid = userid;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCallPhone() {
		return callPhone;
	}
	public void setCallPhone(String callPhone) {
		this.callPhone = callPhone;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}

}
