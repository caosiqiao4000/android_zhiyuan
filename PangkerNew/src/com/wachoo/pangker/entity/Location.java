package com.wachoo.pangker.entity;

import java.io.Serializable;

/**
 * 
 * @author wangxin
 * 
 */
public class Location implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 位置采集的时间戳
	 */
	long timestamp;
	/**
	 * 经度
	 */
	double longitude;
	/**
	 * 纬度
	 */
	double latitude;
	/**
	 * 来源
	 */
	String source;
	/**
	 * 精度（误差范围）
	 */
	double accurate;
	
	private String address;

	public Location() {

	}

	public Location(double longitude, double latitude) {
		// TODO Auto-generated constructor stub
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String toResString() {
		return latitude + "," + longitude;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public double getAccurate() {
		return accurate;
	}

	public void setAccurate(double accurate) {
		this.accurate = accurate;
	}

	public String toJson() {
		return "{\'timestamp\':" + timestamp + ",\'longitude\':" + longitude
				+ ",\'latitude\':" + latitude + ",\'source\':\'" + source
				+ ",\'address\':\'" + address + "\',\'accurate\':" + accurate + "}";
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
