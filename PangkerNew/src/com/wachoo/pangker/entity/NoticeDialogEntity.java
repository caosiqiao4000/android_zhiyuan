package com.wachoo.pangker.entity;

public class NoticeDialogEntity {
	private String mDialogTitle;
	private String mNoticeTitle;
	private String mNoticeContent;

	public String getmDialogTitle() {
		return mDialogTitle;
	}

	public void setmDialogTitle(String mDialogTitle) {
		this.mDialogTitle = mDialogTitle;
	}

	public String getmNoticeTitle() {
		return mNoticeTitle;
	}

	public void setmNoticeTitle(String mNoticeTitle) {
		this.mNoticeTitle = mNoticeTitle;
	}

	public String getmNoticeContent() {
		return mNoticeContent;
	}

	public void setmNoticeContent(String mNoticeContent) {
		this.mNoticeContent = mNoticeContent;
	}

}
