package com.wachoo.pangker.entity;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.util.Util;

/**
 * 
 * 联系人信息contactitem
 * 
 * @author wangxin
 * 
 */
public class UserItem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String tag = "UserItem-->";
	public static final String USERID = "userid";
	public static final String USERNAME = "username";
	public static final String USERSIGN = "usersign";
	public static final int ATTENT_TYPE_IMPLICIT = 0;
	public static final int ATTENT_TYPE_COMMON = 1;

	public static final int USER_SEX_BOY = 0;
	public static final int USER_SEX_GRIL = 1;

	private int iconType = 1;// 是否上传过头像。0:未上传 1：上传过
	private String userId;// 用户id
	private String phoneNum;// 用户手机号码
	private int userType;// 用户类型
	private String userName;// 用户名
	private String rName;// 备注名针对好友使用 >>>>wangxin
	private String realName;// 真实姓名
	private String nickName;
	private String distance;// 距离
	private String sign;// 签名
	private String sex;
	private String age;
	private String relationType;// 关系状态 "0":添加等待对方确认 "1"：好友关系 "99":无关系
	private int state = 0;
	private boolean isGetState = false;// 判断是否获取过在线状态（ false : 没有； true 获取过）
	// private Bitmap icon;//用户
	private boolean isFriend;
	private boolean isPangker;
	private boolean available;
	private String groupName;
	private String lastUpdateTime;// 最后一次上传位置时间
	private String friendSign;// 交友标签
	private String poi;// 用户poi信息
	private Location location;
	private Double longitude;
	private Double latitude;
	private Double lon;
	private Double lat;
	private String vipLevel;// 会员等级
	private Integer attentType;// 关注人类型

	// 最近访客中添加字段，add by zhegnjy
	private String clientSystem;// 客户端系统信息
	private String clientBrand;// 终端版本信息

	private int locateType;// >>>>>>>定位类型 0：自定位 1：漂移
	private String meno; // 备注
	private Integer isWifiLocate;//1:周边wifi定位用户  ；0：非wifi定位用户

	public UserItem() {

	}

	public int getLocateType() {
		return locateType;
	}

	public void setLocateType(int locateType) {
		this.locateType = locateType;
	}

	/**
	 * @return the rName
	 */
	public String getrName() {
		return rName;
	}

	/**
	 * @param rName
	 *            the rName to set
	 */
	public void setrName(String rName) {
		this.rName = rName;
	}

	// /**
	// * @return the icon
	// */
	// public Bitmap getIcon() {
	// return icon;
	// }
	//
	// /**
	// * @param icon
	// * the icon to set
	// */
	// public void setIcon(Bitmap icon) {
	// this.icon = icon;
	// }

	public String getRealName() {
		return realName;
	}
    /**
     * 获取一个有用的名称，以备注名，真是名称，用户名，昵称进行排列
     */
	public String getAvailableName() {
		if (!Util.isEmpty(rName)) {
			return rName;
		}
		if (!Util.isEmpty(userName)) {
			return userName;
		}
		if (!Util.isEmpty(nickName)) {
			return nickName;
		}
		if (!Util.isEmpty(realName)) {
			return realName;
		}
		return "";
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * 
	 * @param userInfo
	 */
	public UserItem(UserInfo userInfo) {
		try {
			setUserId(userInfo.getUserId());
			setUserName(userInfo.getUserName());
			setIconType(userInfo.getIconType());
			setRealName(userInfo.getRealName());
			setSex(String.valueOf(userInfo.getSex()));
			setSign(userInfo.getSign());
		} catch (Exception e) {
			// TODO: handle exception
			setSex("");
		}

	}

	/**
	 * @return the relationType
	 */
	public String getRelationType() {
		return relationType;
	}

	/**
	 * @param relationType
	 *            the relationType to set
	 */
	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * @param age
	 *            the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}

	/**
	 * @return the lastUpdateTime
	 */
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	/**
	 * @param lastUpdateTime
	 *            the lastUpdateTime to set
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	/**
	 * @return the friendSign
	 */
	public String getFriendSign() {
		return friendSign;
	}

	/**
	 * @param friendSign
	 *            the friendSign to set
	 */
	public void setFriendSign(String friendSign) {
		this.friendSign = friendSign;
	}

	/**
	 * @return the userType
	 */
	public int getUserType() {
		return userType;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(int userType) {
		this.userType = userType;
	}

	/**
	 * @return the phoneNum
	 */
	public String getPhoneNum() {
		return phoneNum;
	}

	/**
	 * @param phoneNum
	 *            the phoneNum to set
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	/**
	 * @return the isGetState
	 */
	public boolean isGetState() {
		return isGetState;
	}

	/**
	 * @param isGetState
	 *            the isGetState to set
	 */
	public void setGetState(boolean isGetState) {
		this.isGetState = isGetState;
	}

	/**
	 * @return the iconType
	 */
	public int getIconType() {
		return iconType;
	}

	/**
	 * @param iconType
	 *            the iconType to set
	 */
	public void setIconType(int iconType) {
		this.iconType = iconType;
	}

	/**
	 * @return the portraitIconUrl
	 */
	public String getPortraitIconUrl() {
		return Configuration.getIconUrl() + userId;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}

	public String getDistance() {
		return distance;
	}

	/**
	 * setDistance
	 * 
	 * @param distance
	 */
	public void setDistance(String distance) {
		this.distance = distance;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public boolean isFriend() {
		return isFriend;
	}

	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}

	public boolean isPangker() {
		return isPangker;
	}

	public void setPangker(boolean isPangker) {
		this.isPangker = isPangker;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * @param nickName
	 *            the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * 
	 * @return sign
	 */
	public String getSign() {
		// if (sign == null)
		// return getUserInfo().getSign();
		return sign;
	}

	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param sex
	 *            the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @return the available
	 */
	public boolean isAvailable() {
		return available;
	}

	/**
	 * @param available
	 *            the available to set
	 */
	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}

	public String getPoi() {
		return poi;
	}

	public void setPoi(String poi) {
		this.poi = poi;
	}

	public String toResString() {
		return latitude + "," + longitude;
	}

	public Integer getAttentType() {
		return attentType;
	}

	public void setAttentType(Integer attentType) {
		this.attentType = attentType;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getClientSystem() {
		return clientSystem;
	}

	public void setClientSystem(String clientSystem) {
		this.clientSystem = clientSystem;
	}

	public String getClientBrand() {
		return clientBrand;
	}

	public void setClientBrand(String clientBrand) {
		this.clientBrand = clientBrand;
	}

	public String getMeno() {
		return meno;
	}

	public void setMeno(String meno) {
		this.meno = meno;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public static final Parcelable.Creator<UserItem> CREATOR = new Parcelable.Creator<UserItem>() {
		@Override
		public UserItem createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			UserItem item = new UserItem();
			item.userId = source.readString();
			item.nickName = source.readString();
			item.userName = source.readString();
			item.relationType = source.readString();
			item.sex = source.readString();
			item.iconType = source.readInt();
			return item;
		}

		@Override
		public UserItem[] newArray(int size) {
			// TODO Auto-generated method stub
			return new UserItem[size];
		}

	};

	public Integer getIsWifiLocate() {
		return isWifiLocate;
	}

	public void setIsWifiLocate(Integer isWifiLocate) {
		this.isWifiLocate = isWifiLocate;
	}

}
