package com.wachoo.pangker.entity;

/**
 * 
 * @author wangxin
 *
 */
public class GroupChatMsg {

	private String uid;//用户账号
	private String userName;//用户名
	private String talkmessage;//聊天内容
	private String talktime;//聊天时间
	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}
	/**
	 * @param uid the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the talkmessage
	 */
	public String getTalkmessage() {
		return talkmessage;
	}
	/**
	 * @param talkmessage the talkmessage to set
	 */
	public void setTalkmessage(String talkmessage) {
		this.talkmessage = talkmessage;
	}
	/**
	 * @return the talktime
	 */
	public String getTalktime() {
		return talktime;
	}
	/**
	 * @param talktime the talktime to set
	 */
	public void setTalktime(String talktime) {
		this.talktime = talktime;
	}
	
	
}
