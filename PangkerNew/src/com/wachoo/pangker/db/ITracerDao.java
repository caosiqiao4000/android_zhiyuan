package com.wachoo.pangker.db;

import com.wachoo.pangker.entity.UserItem;

/**
 * 
 * ITraceDao
 * 
 * @author wangxin
 * 
 */
public interface ITracerDao {
	/**
	 * addTracer
	 * 
	 * @param user
	 * @param groupid
	 *
	 * @author wangxin 2012-2-2	下午04:17:13
	 */
	public void addTracer(UserItem user, String groupid);

	/**
	 * addTracer
	 * 
	 * @param user
	 * @param groupid
	 * @param myDataBaseAdapter
	 * @return
	 *
	 * @author wangxin 2012-2-2	下午04:17:24
	 */
	public boolean addTracer(UserItem user, String groupid, MyDataBaseAdapter myDataBaseAdapter);


	/**
	 * delTracer
	 * 
	 * @param user
	 * @param groupid
	 *
	 * @author wangxin 2012-2-2	下午04:17:43
	 */
	public void delTracer(UserItem user, String groupid);
}
