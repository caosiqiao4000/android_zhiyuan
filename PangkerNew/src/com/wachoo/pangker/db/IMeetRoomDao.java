package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.MeetRoom;

/**
 * @author wubo
 * @createtime 2012-3-19
 */
public interface IMeetRoomDao {

	/**
	 * 添加会议室聊天记录
	 * 
	 * @author wubo
	 * @createtime 2012-3-19
	 * @param groupId
	 * @return
	 */
	public boolean saveRoomHistory(MeetRoom mr);

	/**
	 * updateRoomHistory
	 * 
	 * @param mr
	 * @return
	 */
	public boolean updateRoomHistory(MeetRoom mr);

	/**
	 * 查询会议室聊天记录
	 * 
	 * @author wubo
	 * @createtime 2012-3-19
	 * @param groupId
	 * @return
	 */
	public List<MeetRoom> getRoomHistory(String groupId);

	/**
	 * boolean TODO清空聊天记录
	 * 
	 * @param groupId
	 * @return
	 */
	public boolean clearRoomHistory(String groupId);

	/**
	 * 获取少量聊天记录
	 * 
	 * @param groupId
	 * @return
	 */
	public List<MeetRoom> getLimitHistory(String groupId, int start, int lenght);

	/**
	 * 获取最大的序列号
	 * 
	 * @author wubo
	 * @time 2012-11-6
	 * @param groupId
	 * @return
	 * 
	 */
	public int getMaxNum(String groupId);
	/**
	 * 数据总条数
	 * @author wubo
	 * @time 2012-11-6
	 * @param groupId
	 * @return
	 * 
	 */
	public int getMeetRoomCount(String groupId);

	/**
	 * 保存历史记录
	 * 
	 * @author wubo
	 * @time 2012-11-6
	 * @param msgs
	 * @return
	 * 
	 */
	public boolean saveMsgs(List<MeetRoom> msgs);
}
