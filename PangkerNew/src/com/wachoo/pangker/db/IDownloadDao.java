package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.DownloadJob;

public interface IDownloadDao {
	/**
	 * 保存 下载的具体信息
	 */
	public long saveDownloadInfo(DownloadJob info);

	/**
	 * 根据文件网络地址获取相应下载信息
	 */
	public List<DownloadJob> getDownloadInfos(String fileUrl);

	/**
	 * 获取所有下载文件信息
	 */
	public List<DownloadJob> getDownloadInfos();

	/**
	 * 更新下载信息-状态
	 */
	public void updataDownloadStatus(long id, int status);

	/**
	 * 查看数据库中是否有数据
	 */
	public boolean isExitDownloadInfos(String fileUrl);

	/**
	 * 根据文件网络地址获取相应下载信息
	 */
	public DownloadJob getDownloadInfo(String fileUrl);

	/**
	 * 删除下载信息
	 */
	public void delete(String url);

	/**
	 * 根据下载的状态删除下载记录
	 * 
	 * @param status
	 */
	public void deleteDownloadJobsBYStatus(int status);

	/**
	 * 更新下载信息
	 */
	public void updataDownloadInfo(DownloadJob downloadInfo);

	/**
	 * 清空数据库
	 */
	public void deleteAll();

	/**
	 * 更新下载进度
	 * 
	 * @param downloadID
	 * @param doneSize
	 */
	public void updateDownloadDoneSize(int downloadID, long doneSize);
}
