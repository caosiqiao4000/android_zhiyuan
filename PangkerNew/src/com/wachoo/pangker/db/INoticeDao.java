package com.wachoo.pangker.db;

import java.util.Date;
import java.util.List;

import com.wachoo.pangker.entity.NoticeInfo;

public interface INoticeDao {
    /**
     * 记录一条消息
     * @param noticeInfo
     * @return
     */
	public long saveNoticeInfo(NoticeInfo noticeInfo);
	/**
	 * 获取所有的消息
	 * @return
	 */
	public List<NoticeInfo> getAllNoticeInfos(String userId);
	/**
	 * 根据日期获取所有的消息
	 * @param date
	 * @return
	 */
	public List<NoticeInfo> getNoticesByTime(String userId, Date date);
	/**
	 * 删除一条消息
	 * @param noticeInfo
	 * @return
	 */
	public boolean delNoticeInfo(NoticeInfo noticeInfo);
	/**
	 * 根据类型获取所有的消息
	 * @param type
	 * @return
	 */
	public List<NoticeInfo> getNoticesByType(String userId, int type);
	/**
	 * 清空所有消息
	 */
	public boolean delNoticeInfos(String userId);
	/**
	 * 修改一条消息状态
	 * @param noticeInfo
	 */
	public boolean updateNoticeInfo(NoticeInfo noticeInfo);
	/**
	 * 删除某人的所有信息
	 * @param noticeInfo
	 */
	boolean delNoticeInfos(String fromId, int msgtype);
}
