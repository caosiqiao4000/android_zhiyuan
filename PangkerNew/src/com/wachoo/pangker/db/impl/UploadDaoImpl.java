package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.db.UploadDao;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.util.Util;

public class UploadDaoImpl implements UploadDao {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Cursor mCursor;
	private Context context;

	public UploadDaoImpl(Context context) {
		super();
		this.context = context;
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
	}

	@Override
	public void deleteUploadInfos(int status) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		try {
			m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_UPLOADINFO,
					MyDataBaseAdapter.UPLOAD_STATUS, String.valueOf(status));
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
	}

	@Override
	public void deleteUploadInfos(String userId) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		try {
			m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_UPLOADINFO,
					MyDataBaseAdapter.UPLOAD_USERID, userId);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
	}

	@Override
	public void deleteUploadInfo(UploadJob uploadInfo) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		try {
			m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_UPLOADINFO, MyDataBaseAdapter.UPLOAD_ID,
					uploadInfo.getId());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
	}

	@Override
	public List<UploadJob> getUploadInofs(String userId) {
		// TODO Auto-generated method stub
		List<UploadJob> uploadInfos = new ArrayList<UploadJob>();
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_UPLOADINFO,
					new String[] { MyDataBaseAdapter.UPLOAD_USERID }, new String[] { userId });
			if (mCursor == null)
				return uploadInfos;
			if (mCursor.getCount() == 0)
				return uploadInfos;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				UploadJob uploadInfo = getUploadInfoByCursor();
				uploadInfos.add(uploadInfo);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return uploadInfos;
	}

	private UploadJob getUploadInfoByCursor() {
		// TODO Auto-generated method stub
		UploadJob uploadInfo = new UploadJob();
		uploadInfo.setId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_ID)));
		uploadInfo.setDesc(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_DES)));
		uploadInfo.setFileFormat(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_FORMAT)));
		uploadInfo.setFilePath(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_PATH)));
		uploadInfo.setIfShare(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_SHARE)));
		uploadInfo.setMd5Value(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_MD5)));
		uploadInfo.setResName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_NAME)));
		uploadInfo.setStatus(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_STATUS)));
		uploadInfo.setType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_RESTYPE)));
		uploadInfo.setUpTime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_TIME)));
		uploadInfo.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_USERID)));
		uploadInfo.setFileSize(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_SIZE)));
		uploadInfo.setResType(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_STYPE)));
		uploadInfo.setDirId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_DIRID)));
		uploadInfo.setAuthor(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_AUTHOR)));
		uploadInfo.setMusicId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_MUSICD)));
		uploadInfo.setResId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_RESID)));
		uploadInfo.setFlagCheckFileExist(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_EXIST)));
		uploadInfo.setUploadType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_TYPE)));
		uploadInfo.setCtrlat(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_LAT)));
		uploadInfo.setCtrlon(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_LNG)));
		uploadInfo.setUpPoi(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.REMARK3)));
		uploadInfo.setRealPoi(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.REMARK4)));
		return uploadInfo;
	}

	@Override
	public long saveUploadInfo(UploadJob uploadInfo) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.UPLOAD_USERID, uploadInfo.getUserId());
		initialValues.put(MyDataBaseAdapter.UPLOAD_RESTYPE, uploadInfo.getType());
		initialValues.put(MyDataBaseAdapter.UPLOAD_NAME, uploadInfo.getResName());
		initialValues.put(MyDataBaseAdapter.UPLOAD_FORMAT, uploadInfo.getFileFormat());
		initialValues.put(MyDataBaseAdapter.UPLOAD_PATH, uploadInfo.getFilePath());
		initialValues.put(MyDataBaseAdapter.UPLOAD_MD5, uploadInfo.getMd5Value());
		initialValues.put(MyDataBaseAdapter.UPLOAD_SHARE, uploadInfo.getIfShare());
		initialValues.put(MyDataBaseAdapter.UPLOAD_DES, uploadInfo.getDesc());
		initialValues.put(MyDataBaseAdapter.UPLOAD_TIME, uploadInfo.getUpTime());
		initialValues.put(MyDataBaseAdapter.UPLOAD_STATUS, uploadInfo.getStatus());
		initialValues.put(MyDataBaseAdapter.UPLOAD_SIZE, uploadInfo.getFileSize());
		initialValues.put(MyDataBaseAdapter.UPLOAD_STYPE, uploadInfo.getResType());
		initialValues.put(MyDataBaseAdapter.UPLOAD_DIRID, uploadInfo.getDirId());
		initialValues.put(MyDataBaseAdapter.UPLOAD_EXIST, uploadInfo.getFlagCheckFileExist());
		initialValues.put(MyDataBaseAdapter.UPLOAD_TYPE, uploadInfo.getUploadType());
		if (!Util.isEmpty(uploadInfo.getAuthor())) {
			initialValues.put(MyDataBaseAdapter.UPLOAD_AUTHOR, uploadInfo.getAuthor());
		}
		if(!Util.isEmpty(uploadInfo.getCtrlat())){
			initialValues.put(MyDataBaseAdapter.UPLOAD_LAT, uploadInfo.getCtrlat());
		}
        if(!Util.isEmpty(uploadInfo.getCtrlon())){
        	initialValues.put(MyDataBaseAdapter.UPLOAD_LNG, uploadInfo.getCtrlon());
		}
        if(!Util.isEmpty(uploadInfo.getUpPoi())){
        	initialValues.put(MyDataBaseAdapter.REMARK3, uploadInfo.getUpPoi());
		}
        if(!Util.isEmpty(uploadInfo.getRealPoi())){
        	initialValues.put(MyDataBaseAdapter.REMARK4, uploadInfo.getRealPoi());
		}
		initialValues.put(MyDataBaseAdapter.UPLOAD_MUSICD, uploadInfo.getMusicId());
		try {
			long _id = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_UPLOADINFO, initialValues);
			return _id;
		} catch (Exception e) {
			logger.error(TAG, e);
			return -1;
		} finally {
			close();
		}
	}

	@Override
	public UploadJob getUploadInof(String userId, String filepath) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_UPLOADINFO, new String[] {
					MyDataBaseAdapter.UPLOAD_USERID, MyDataBaseAdapter.UPLOAD_PATH }, new String[] { userId,
					filepath });
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 1) {
				mCursor.moveToFirst();
				UploadJob uploadInfo = getUploadInfoByCursor();
				return uploadInfo;
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return null;
	}

	@Override
	public boolean updateUploadInfo(UploadJob uploadInfo) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.UPLOAD_STATUS, uploadInfo.getStatus());
		initialValues.put(MyDataBaseAdapter.UPLOAD_DES, uploadInfo.getDesc());
		initialValues.put(MyDataBaseAdapter.UPLOAD_TIME, uploadInfo.getUpTime());
		initialValues.put(MyDataBaseAdapter.UPLOAD_STATUS, uploadInfo.getStatus());
		initialValues.put(MyDataBaseAdapter.UPLOAD_MD5, uploadInfo.getMd5Value());
		initialValues.put(MyDataBaseAdapter.UPLOAD_STYPE, uploadInfo.getResType());
		initialValues.put(MyDataBaseAdapter.UPLOAD_DIRID, uploadInfo.getDirId());
		if(!Util.isEmpty(uploadInfo.getResId())){
			initialValues.put(MyDataBaseAdapter.UPLOAD_RESID, uploadInfo.getResId());
		}
		initialValues.put(MyDataBaseAdapter.UPLOAD_TYPE, uploadInfo.getUploadType());
		if(!Util.isEmpty(uploadInfo.getCtrlat())){
			initialValues.put(MyDataBaseAdapter.UPLOAD_LAT, uploadInfo.getCtrlat());
		}
        if(!Util.isEmpty(uploadInfo.getCtrlon())){
        	initialValues.put(MyDataBaseAdapter.UPLOAD_LNG, uploadInfo.getCtrlon());
		}
        if(!Util.isEmpty(uploadInfo.getUpPoi())){
        	initialValues.put(MyDataBaseAdapter.REMARK3, uploadInfo.getUpPoi());
		}
        if(!Util.isEmpty(uploadInfo.getRealPoi())){
        	initialValues.put(MyDataBaseAdapter.REMARK4, uploadInfo.getRealPoi());
		}
		try {
			return m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_UPLOADINFO, initialValues,
					MyDataBaseAdapter.UPLOAD_ID, new String[] { String.valueOf(uploadInfo.getId()) });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close();
		}
	}

	private void close() {
		if (m_MyDataBaseAdapter != null) {
			m_MyDataBaseAdapter.close();
			m_MyDataBaseAdapter = null;
		}
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}
	}

	@Override
	public int ifUploading(String userId, String md5, int status) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_UPLOADINFO, new String[] {
					MyDataBaseAdapter.UPLOAD_USERID, MyDataBaseAdapter.UPLOAD_MD5,
					MyDataBaseAdapter.UPLOAD_STATUS }, new String[] { userId, md5, String.valueOf(status) });
			if (mCursor == null)
				return 0;
			return mCursor.getCount();
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return 0;
	}

	@Override
	public String getResPath(String resId) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		String sql = "Select " + MyDataBaseAdapter.UPLOAD_PATH + " From "
				+ MyDataBaseAdapter.TABLE_UPLOADINFO + " Where " + MyDataBaseAdapter.UPLOAD_RESID + " = '"
				+ resId + "'";
		try {
			mCursor = m_MyDataBaseAdapter.rawQuery(sql);
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 1) {
				mCursor.moveToFirst();
				return mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.UPLOAD_PATH));
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return null;
	}

}
