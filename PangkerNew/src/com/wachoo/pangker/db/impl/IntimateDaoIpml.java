package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IIntimateDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.Limit;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.server.response.RelativesResInfo;
import com.wachoo.pangker.util.Util;

public class IntimateDaoIpml implements IIntimateDao {
	private static final Logger logger = LoggerFactory.getLogger();
	private static final String TAG = "IntimateDaoIpml";
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Cursor mCursor;
	private String mUserID;
	private Context context;

	public IntimateDaoIpml(Context context) {
		super();
		this.context = context;
		mUserID = ((PangkerApplication) context.getApplicationContext()).getMyUserID();
	}

	@Override
	public boolean saveIntimateData(List<RelativesResInfo> relativesResInfos) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			// >>>>>>>>>先删除所有数据
			// m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_INTIMATE);

			// >>>>>>>>>>再插入数据
			for (RelativesResInfo info : relativesResInfos) {

				ContentValues contentValues = new ContentValues();
				contentValues.put(MyDataBaseAdapter.INTIMATE_USERID, mUserID);
				contentValues.put(MyDataBaseAdapter.INTIMATE_RUSERID, info.getRuid());
				contentValues.put(MyDataBaseAdapter.INTIMATE_USERNAME, info.getUserName());
				contentValues.put(MyDataBaseAdapter.INTIMATE_RESID, info.getResId());
				contentValues.put(MyDataBaseAdapter.INTIMATE_RESNAME, info.getResName());
				contentValues.put(MyDataBaseAdapter.INTIMATE_RESDESC, info.getResDesc());
				contentValues.put(MyDataBaseAdapter.INTIMATE_TYPE, info.getResType());
				contentValues.put(MyDataBaseAdapter.INTIMATE_RESSOURCE, info.getResSource());
				if (info.getLocation() != null) {
					contentValues.put(MyDataBaseAdapter.INTIMATE_LOCATION_LAT, info.getLocation().getLatitude());
					contentValues.put(MyDataBaseAdapter.INTIMATE_LOCATION_LON, info.getLocation().getLongitude());
				}
				contentValues.put(MyDataBaseAdapter.INTIMATE_DEALTIME, info.getCreateTime());

				m_MyDataBaseAdapter.replaceData(MyDataBaseAdapter.TABLE_INTIMATE, contentValues);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close();
		}
		return true;
	}

	@Override
	public boolean saveIntimateData(RelativesResInfo info) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			// StringBuffer sql = new StringBuffer("Replace Into ");
			// sql.append(MyDataBaseAdapter.TABLE_INTIMATE);
			// sql.append("( ");
			// sql.append(MyDataBaseAdapter.INTIMATE_USERID);
			// sql.append(",");
			// sql.append(MyDataBaseAdapter.INTIMATE_RUSERID);
			// sql.append(",");
			// sql.append(MyDataBaseAdapter.INTIMATE_USERNAME);
			// sql.append(",");
			// sql.append(MyDataBaseAdapter.INTIMATE_RESID);
			// sql.append(",");
			// sql.append(MyDataBaseAdapter.INTIMATE_RESNAME);
			// sql.append(",");
			// sql.append(MyDataBaseAdapter.INTIMATE_RESDESC);
			// sql.append(",");
			// sql.append(MyDataBaseAdapter.INTIMATE_TYPE);
			// sql.append(",");
			// sql.append(MyDataBaseAdapter.INTIMATE_RESSOURCE);
			// sql.append(",");
			// if (info.getLocation() != null) {
			// sql.append(MyDataBaseAdapter.INTIMATE_LOCATION_LAT);
			// sql.append(",");
			// sql.append(MyDataBaseAdapter.INTIMATE_LOCATION_LON);
			// sql.append(",");
			// }
			// sql.append(MyDataBaseAdapter.INTIMATE_DEALTIME);
			// sql.append(" ) valus ( ");
			// sql.append("'");
			// sql.append(mUserID);
			// sql.append("','");
			// sql.append(info.getRuid());
			// sql.append("','");
			// sql.append(info.getUserName());
			// sql.append("',");
			// sql.append(info.getResId());
			// sql.append(",'");
			// sql.append(info.getResName());
			// sql.append("','");
			// sql.append(info.getResDesc());
			// sql.append("',");
			// sql.append(info.getResType());
			// sql.append(",");
			// sql.append(info.getResSource());
			// sql.append(",");
			// if (info.getLocation() != null) {
			// sql.append(info.getLocation().getLatitude());
			// sql.append(",");
			// sql.append(info.getLocation().getLongitude());
			// sql.append(",");
			// }
			// sql.append(info.getCreateTime());
			// sql.append(" )");
			//
			// return m_MyDataBaseAdapter.execSql(sql.toString());
			ContentValues contentValues = new ContentValues();
			contentValues.put(MyDataBaseAdapter.INTIMATE_USERID, mUserID);
			contentValues.put(MyDataBaseAdapter.INTIMATE_RUSERID, info.getRuid());
			contentValues.put(MyDataBaseAdapter.INTIMATE_USERNAME, info.getUserName());
			contentValues.put(MyDataBaseAdapter.INTIMATE_RESID, info.getResId());
			contentValues.put(MyDataBaseAdapter.INTIMATE_RESNAME, info.getResName());
			contentValues.put(MyDataBaseAdapter.INTIMATE_RESDESC, info.getResDesc());
			contentValues.put(MyDataBaseAdapter.INTIMATE_TYPE, info.getResType());
			contentValues.put(MyDataBaseAdapter.INTIMATE_RESSOURCE, info.getResSource());
			if (info.getLocation() != null) {
				contentValues.put(MyDataBaseAdapter.INTIMATE_LOCATION_LAT, info.getLocation().getLatitude());
				contentValues.put(MyDataBaseAdapter.INTIMATE_LOCATION_LON, info.getLocation().getLongitude());
			}
			contentValues.put(MyDataBaseAdapter.INTIMATE_DEALTIME, info.getCreateTime());

			long result = m_MyDataBaseAdapter.replaceData(MyDataBaseAdapter.TABLE_INTIMATE, contentValues);

			if (result > 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close();
		}
	}

	@Override
	public List<RelativesResInfo> getIntimateData(Limit limit) {
		// TODO Auto-generated method stub
		List<RelativesResInfo> relativesResInfos = new ArrayList<RelativesResInfo>();
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		try {

			StringBuffer sql = new StringBuffer("SELECT * FROM ");
			sql.append(MyDataBaseAdapter.TABLE_INTIMATE);
			sql.append(" WHERE ");
			sql.append(MyDataBaseAdapter.INTIMATE_USERID);
			sql.append(" = '");
			sql.append(mUserID);
			sql.append("' ORDER BY ");
			sql.append(MyDataBaseAdapter.INTIMATE_DEALTIME);
			sql.append(" DESC  LIMIT ");
			sql.append(limit.getOffset());
			sql.append(",");
			sql.append(limit.getLength());

			mCursor = m_MyDataBaseAdapter.getDatas(sql.toString());
			if (mCursor == null) {
				return relativesResInfos;
			}
			if (mCursor.getCount() <= 0) {
				return relativesResInfos;
			}
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				RelativesResInfo info = new RelativesResInfo();

				info.setUid(Util.String2Long(mUserID));
				info.setRuid(mCursor.getLong(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_RUSERID)));
				info.setResId(mCursor.getLong(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_RESID)));
				info.setUserName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_USERNAME)));
				info.setResName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_RESNAME)));
				info.setResDesc(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_RUSERID)));
				info.setResType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_TYPE)));
				info.setResSource(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_RESSOURCE)));
				info.setResDesc(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_RESDESC)));
				double lat = mCursor.getDouble(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_LOCATION_LAT));
				double lon = mCursor.getDouble(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_LOCATION_LON));
				Location location = new Location(lon, lat);
				info.setLocation(location);
				info.setCreateTime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.INTIMATE_DEALTIME)));
				relativesResInfos.add(info);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return relativesResInfos;
	}

	private void close() {
		if (m_MyDataBaseAdapter != null) {
			m_MyDataBaseAdapter.close();
			m_MyDataBaseAdapter = null;
		}
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}
	}

	@Override
	public boolean deleteAllData() {
		// TODO Auto-generated method stub

		return false;
	}

	@Override
	public boolean deleteData(String RUid) {
		// TODO Auto-generated method stub
		return false;
	}

}
