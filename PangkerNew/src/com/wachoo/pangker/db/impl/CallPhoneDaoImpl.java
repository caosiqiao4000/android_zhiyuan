package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.wachoo.pangker.db.ICallPhoneDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.util.Util;

public class CallPhoneDaoImpl implements ICallPhoneDao {
	
	private String TAG = "CallPhoneDaoImpl";// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private MyDataBaseAdapter dbAdapter;
	private Context mContext;

	public CallPhoneDaoImpl(Context context) {
		this.mContext = context;
	}

	@Override
	public boolean insertPhone(String uid, String phone) {
		// TODO Auto-generated method stub
		try {
			if(isExist(phone)){
				return false;
			}
			dbAdapter = new MyDataBaseAdapter(this.mContext);
			dbAdapter.open();
			ContentValues phoneValues = new ContentValues();
			phoneValues.put(MyDataBaseAdapter.CALLPHONE_USERID, uid);
			phoneValues.put(MyDataBaseAdapter.CALLPHONE_NUM, phone);
			long _userid = dbAdapter.insertData(MyDataBaseAdapter.TABLE_CALLPHONE, phoneValues);
			return _userid > 0;
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (dbAdapter != null) {
				dbAdapter.close();
				dbAdapter = null;
			}
		}
		return false;
	}

	@Override
	public void clearPhones(String uid) {
		// TODO Auto-generated method stub
		String deleteSql = "delete * from " +  MyDataBaseAdapter.TABLE_CALLPHONE + " where " + MyDataBaseAdapter.CALLPHONE_USERID + " = '"
		+ uid + "'"; 
		
		try {
			dbAdapter = new MyDataBaseAdapter(this.mContext);
			dbAdapter.open();
			dbAdapter.execSql(deleteSql);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (dbAdapter != null) {
				dbAdapter.close();
				dbAdapter = null;
			}
		}
		
	}

	@Override
	public String[] getPhoneNumbers(String userid) {
		// TODO Auto-generated method stub
		List<String> phoneNums = new ArrayList<String>();
		Cursor mCursor = null;
		
		try {
			this.dbAdapter = new MyDataBaseAdapter(mContext);
			dbAdapter.open();
			String selectSQL = "select DISTINCT " + MyDataBaseAdapter.CALLPHONE_NUM  +" from " + MyDataBaseAdapter.TABLE_CALLPHONE + " where " + MyDataBaseAdapter.CALLPHONE_USERID + " = '"
			+ userid + "'"; 
			mCursor  = dbAdapter.rawQuery(selectSQL);
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				String phoneNum = mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.CALLPHONE_NUM));
				if(!Util.isEmpty(phoneNum)){
					phoneNums.add(phoneNum);
				}
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
			if (dbAdapter != null) {
				dbAdapter.close();
			}
		}
		if(phoneNums != null && phoneNums.size() > 0){
			return phoneNums.toArray(new String[phoneNums.size()]);
		}
		else return null;
	}
	
	/**
	 * 判断该当前用户是否存在黑名单表中
	 */
	@Override
	public boolean isExist(String phoneNum) {
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_CALLPHONE + " where "
				+ MyDataBaseAdapter.CALLPHONE_NUM + "='" + phoneNum + "'";
		Cursor mCursor = null;
		
		try {
			dbAdapter = new MyDataBaseAdapter(this.mContext);
			dbAdapter.open();
			mCursor = dbAdapter.rawQuery(selectSQL);
			return mCursor != null && mCursor.getCount() > 0;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
			if (dbAdapter != null) {
				dbAdapter.close();
				dbAdapter = null;
			}
		}
		return false;
	}

	

}
