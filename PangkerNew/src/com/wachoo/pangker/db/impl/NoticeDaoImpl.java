package com.wachoo.pangker.db.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.db.INoticeDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.NoticeInfo;

public class NoticeDaoImpl implements INoticeDao {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Cursor mCursor;
	private Context context;

	private NoticeInfo getNoticeInfo() {
		NoticeInfo noticeInfo = new NoticeInfo();
		noticeInfo.setId(mCursor.getLong(mCursor.getColumnIndex(MyDataBaseAdapter.NOTICE_ID)));
		noticeInfo.setNotice(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.NOTICE_CONTENT)));
		noticeInfo.setOpeUserid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.NOTICE_FROM)));
		noticeInfo
				.setOpeUsername(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.NOTICE_FROMNAME)));
		noticeInfo.setRemark(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.NOTICE_REAMR)));
		noticeInfo.setStatus(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.NOTICE_STATUS)));
		noticeInfo.setType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.NOTICE_TYPE)));
		noticeInfo.setTime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.NOTICE_TIME)));
		return noticeInfo;
	}

	public NoticeDaoImpl(Context context) {
		super();
		this.context = context;
	}

	@Override
	public boolean delNoticeInfo(NoticeInfo noticeInfo) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		try {
			return m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_NOTICEINFO,
					MyDataBaseAdapter.NOTICE_ID, noticeInfo.getId());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return false;
	}

	@Override
	public boolean delNoticeInfos(String userId) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		try {
			m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_NOTICEINFO,
					MyDataBaseAdapter.NOTICE_USERID, userId);
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return false;
	}

	@Override
	public boolean delNoticeInfos(String fromId, int msgtype) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();

		String sql = "Delete From " + MyDataBaseAdapter.TABLE_NOTICEINFO + " Where "
				+ MyDataBaseAdapter.NOTICE_USERID + " ='" + fromId + "' " + " And "
				+ MyDataBaseAdapter.NOTICE_MSG_TYPE + " = " + msgtype;
		try {
			return m_MyDataBaseAdapter.execSql(sql);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return false;
	}

	@Override
	public List<NoticeInfo> getAllNoticeInfos(String userId) {
		List<NoticeInfo> noticeInfos = new ArrayList<NoticeInfo>();
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_NOTICEINFO,
					new String[] { MyDataBaseAdapter.NOTICE_USERID }, new String[] { userId },
					MyDataBaseAdapter.NOTICE_TIME, true);
			if (mCursor == null)
				return noticeInfos;
			if (mCursor.getCount() == 0)
				return noticeInfos;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				NoticeInfo noticeInfo = getNoticeInfo();
				noticeInfos.add(noticeInfo);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return noticeInfos;
	}

	@Override
	public List<NoticeInfo> getNoticesByTime(String userId, Date date) {
		// TODO Auto-generated method stub
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
		String time = format.format(date);
		String sql = "Select * from " + MyDataBaseAdapter.TABLE_NOTICEINFO + " Where "
				+ MyDataBaseAdapter.NOTICE_TIME + " > " + time + " And " + MyDataBaseAdapter.NOTICE_USERID
				+ " = " + userId;

		List<NoticeInfo> noticeInfos = new ArrayList<NoticeInfo>();
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.rawQuery(sql);
			if (mCursor == null)
				return noticeInfos;
			if (mCursor.getCount() == 0)
				return noticeInfos;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				NoticeInfo noticeInfo = getNoticeInfo();
				noticeInfos.add(noticeInfo);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return noticeInfos;
	}

	@Override
	public List<NoticeInfo> getNoticesByType(String userId, int type) {
		List<NoticeInfo> noticeInfos = new ArrayList<NoticeInfo>();
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_NOTICEINFO, new String[] {
					MyDataBaseAdapter.NOTICE_USERID, MyDataBaseAdapter.NOTICE_MSG_TYPE }, new String[] {
					userId, String.valueOf(type) }, MyDataBaseAdapter.NOTICE_TIME, true);
			if (mCursor == null)
				return noticeInfos;
			if (mCursor.getCount() == 0)
				return noticeInfos;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				NoticeInfo noticeInfo = getNoticeInfo();
				noticeInfos.add(noticeInfo);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return noticeInfos;
	}

	@Override
	public long saveNoticeInfo(NoticeInfo noticeInfo) {

		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.NOTICE_USERID, noticeInfo.getUserId());
		initialValues.put(MyDataBaseAdapter.NOTICE_FROM, noticeInfo.getOpeUserid());
		initialValues.put(MyDataBaseAdapter.NOTICE_FROMNAME, noticeInfo.getOpeUsername());
		initialValues.put(MyDataBaseAdapter.NOTICE_CONTENT, noticeInfo.getNotice());
		initialValues.put(MyDataBaseAdapter.NOTICE_STATUS, noticeInfo.getStatus());
		initialValues.put(MyDataBaseAdapter.NOTICE_TYPE, noticeInfo.getType());
		initialValues.put(MyDataBaseAdapter.NOTICE_REAMR, noticeInfo.getRemark());
		initialValues.put(MyDataBaseAdapter.NOTICE_TIME, noticeInfo.getTime());
		initialValues.put(MyDataBaseAdapter.NOTICE_MSG_TYPE, noticeInfo.getMsgType());
		try {
			return m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_NOTICEINFO, initialValues);
		} catch (Exception e) {
			logger.error(TAG, e);
			return -1;
		} finally {
			close();
		}
	}

	@Override
	public boolean updateNoticeInfo(NoticeInfo noticeInfo) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.NOTICE_STATUS, noticeInfo.getStatus());
		initialValues.put(MyDataBaseAdapter.NOTICE_CONTENT, noticeInfo.getNotice());
		initialValues.put(MyDataBaseAdapter.NOTICE_TIME, noticeInfo.getTime());
		initialValues.put(MyDataBaseAdapter.NOTICE_TYPE, noticeInfo.getType());
		initialValues.put(MyDataBaseAdapter.NOTICE_REAMR, noticeInfo.getRemark());
		try {
			return m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_NOTICEINFO, initialValues,
					MyDataBaseAdapter.NOTICE_ID, new String[] { String.valueOf(noticeInfo.getId()) });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close();
		}
	}

	private void close() {
		if (m_MyDataBaseAdapter != null) {
			m_MyDataBaseAdapter.close();
			m_MyDataBaseAdapter = null;
		}
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}
	}

}
