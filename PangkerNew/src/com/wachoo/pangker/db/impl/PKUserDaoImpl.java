package com.wachoo.pangker.db.impl;

import java.sql.Timestamp;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.util.Util;

/**
 * 
 * 旁客联系人本地vcard管理接口
 * 
 * @author wangxin
 * 
 */
public class PKUserDaoImpl implements IPKUserDao {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log TAG

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Context context;

	/**
	 * 
	 * 
	 * 
	 * @param context
	 */
	public PKUserDaoImpl(Context context) {
		this.context = context;
	}

	@Override
	public boolean saveUser(UserInfo user) {

		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
		try {
			m_MyDataBaseAdapter.open();

			ContentValues initialValues = new ContentValues();
			initialValues.put(MyDataBaseAdapter.USER_ID, Util.trimUserIDDomain(user.getUserId()));
			initialValues.put(MyDataBaseAdapter.USER_USERNAME, user.getUserName());// 后台getNickName表示昵称，getUserName是真实名
			initialValues.put(MyDataBaseAdapter.USER_NICKNAME, user.getUserName());
			initialValues.put(MyDataBaseAdapter.USER_REALNAME, user.getRealName());
			initialValues.put(MyDataBaseAdapter.USER_PORTARITTYPE, user.getIconType());

			initialValues.put(MyDataBaseAdapter.USER_SEX, user.getSex());
			initialValues.put(MyDataBaseAdapter.USER_EMAIL, user.getEmail());
			initialValues.put(MyDataBaseAdapter.USER_BIRTHDAY, user.getBirthday());
			initialValues.put(MyDataBaseAdapter.USER_SIGNATURE, user.getSign());
			initialValues.put(MyDataBaseAdapter.USER_IDENTITY, user.getIdentity());
			initialValues.put(MyDataBaseAdapter.USER_TIME, new Timestamp(System.currentTimeMillis()) + "");
			initialValues.put(MyDataBaseAdapter.USER_CALLPHONE, user.getCallphone());

			if (m_MyDataBaseAdapter.checkUserIsExist(Util.trimUserIDDomain(user.getUserId())))
				return m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_USERS, initialValues,
						MyDataBaseAdapter.USER_ID, new String[] { user.getUserId() });
			else {
				long _id = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_USERS, initialValues);
				if (_id > 0) {
					return true;
				}
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	public boolean updateUser(UserInfo user) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
		try {
			m_MyDataBaseAdapter.open();
			ContentValues initialValues = new ContentValues();
			initialValues.put(MyDataBaseAdapter.USER_ID, Util.trimUserIDDomain(user.getUserId()));
			initialValues.put(MyDataBaseAdapter.USER_USERNAME, user.getUserName());
			initialValues.put(MyDataBaseAdapter.USER_NICKNAME, user.getUserName());
			initialValues.put(MyDataBaseAdapter.USER_REALNAME, user.getRealName());
			initialValues.put(MyDataBaseAdapter.USER_PORTARITTYPE, user.getIconType());
			initialValues.put(MyDataBaseAdapter.USER_SEX, user.getSex());
			initialValues.put(MyDataBaseAdapter.USER_EMAIL, user.getEmail());
			initialValues.put(MyDataBaseAdapter.USER_BIRTHDAY, user.getBirthday());
			initialValues.put(MyDataBaseAdapter.USER_SIGNATURE, user.getSign());
			initialValues.put(MyDataBaseAdapter.USER_IDENTITY, user.getIdentity());
			initialValues.put(MyDataBaseAdapter.USER_CALLPHONE, user.getCallphone());
			// initialValues.put(m_MyDataBaseAdapter.USER_TIME, new
			// Timestamp(System.currentTimeMillis()) + "");
			// return
			// m_MyDataBaseAdapter.updateData(m_MyDataBaseAdapter.TABLE_USERS,
			// m_MyDataBaseAdapter.USER_ID, Long.valueOf(user.getUserId()),
			// initialValues);
			return m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_USERS, initialValues,
					MyDataBaseAdapter.USER_ID, new String[] { user.getUserId() });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}

	@Override
	public UserInfo getUserByUserID(String userid) {
		// TODO Auto-generated method stub
		Log.i(TAG, "userid -->" + userid);
		UserInfo user = null;
		Cursor mCursor = null;
		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getUserDataByUserID(MyDataBaseAdapter.USER_ID,
					Util.trimUserIDDomain(userid));
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {

				user = new UserInfo();
				user.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_ID)));
				user.setUserName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
				user.setNickName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
				user.setRealName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_REALNAME)));
				user.setSign(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));
				user.setEmail(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_EMAIL)));
				user.setCallphone(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_CALLPHONE)));
			}

		} catch (Exception e) {
			Log.e(TAG, "error", e);
			return null;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			if (mCursor != null) {
				mCursor.close();
			}
		}
		Log.i(TAG, "get UserInfo Form database over ");
		return user;
	}

	@Override
	public UserItem getUserItemByUserID(String userid) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Log.i(TAG, "userid -->" + userid);
		UserItem user = null;
		Cursor mCursor = null;
		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getUserItemByUserID(MyDataBaseAdapter.USER_ID, Util.trimUserIDDomain(userid));
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToLast();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				user = new UserItem();
				user.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_ID)));
				user.setUserName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
				user.setNickName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
				user.setRealName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_REALNAME)));
				user.setIconType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识
				if (mCursor.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME) != -1) {
					user.setrName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME)));
				}
				user.setSign(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));
			}

		} catch (Exception e) {
			Log.e(TAG, "error", e);
			return null;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		Log.i(TAG, "get UserItem Form database over ");
		return user;
	}

	@Override
	public boolean saveUser(UserItem user) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
		try {
			m_MyDataBaseAdapter.open();

			ContentValues initialValues = new ContentValues();
			initialValues.put(MyDataBaseAdapter.USER_ID, Util.trimUserIDDomain(user.getUserId()));
			initialValues.put(MyDataBaseAdapter.USER_USERNAME, user.getUserName());
			initialValues.put(MyDataBaseAdapter.USER_NICKNAME, user.getUserName());
			initialValues.put(MyDataBaseAdapter.USER_REALNAME, user.getRealName());
			initialValues.put(MyDataBaseAdapter.USER_PORTARITTYPE, user.getIconType());
			initialValues.put(MyDataBaseAdapter.USER_SIGNATURE, user.getSign());
			initialValues.put(MyDataBaseAdapter.USER_TIME, new Timestamp(System.currentTimeMillis()) + "");
			if (m_MyDataBaseAdapter.checkUserIsExist(Util.trimUserIDDomain(user.getUserId())))
				return m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_USERS, initialValues,
						MyDataBaseAdapter.USER_ID, new String[] { user.getUserId() });
			else {
				long _id = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_USERS, initialValues);
				if (_id > 0) {
					return true;
				}
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	public boolean saveUser(List<UserItem> users) {
		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		try {

			m_MyDataBaseAdapter.open();
			for (UserItem user : users) {

				ContentValues initialValues = new ContentValues();
				initialValues.put(MyDataBaseAdapter.USER_ID, Util.trimUserIDDomain(user.getUserId()));
				initialValues.put(MyDataBaseAdapter.USER_USERNAME, user.getUserName());//
				initialValues.put(MyDataBaseAdapter.USER_NICKNAME, user.getUserName());//
				initialValues.put(MyDataBaseAdapter.USER_REALNAME, user.getRealName());//
				initialValues.put(MyDataBaseAdapter.USER_PORTARITTYPE, user.getIconType());
				initialValues.put(MyDataBaseAdapter.USER_SIGNATURE, user.getSign());
				initialValues.put(MyDataBaseAdapter.USER_TIME, new Timestamp(System.currentTimeMillis())
						+ "");
				if (m_MyDataBaseAdapter.checkUserIsExist(Util.trimUserIDDomain(user.getUserId())))
					return m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_USERS, initialValues,
							MyDataBaseAdapter.USER_ID, new String[] { user.getUserId() });
				else {
					long _id = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_USERS, initialValues);
					if (_id > 0) {
						return true;
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	public boolean saveUser(UserItem user, MyDataBaseAdapter myDataBaseAdapter) {
		try {

			ContentValues initialValues = new ContentValues();
			initialValues.put(MyDataBaseAdapter.USER_ID, Util.trimUserIDDomain(user.getUserId()));
			if (user.getUserName() != null) {
				initialValues.put(MyDataBaseAdapter.USER_USERNAME, user.getUserName());
				initialValues.put(MyDataBaseAdapter.USER_NICKNAME, user.getUserName());
			}
			if (user.getRealName() != null) {
				initialValues.put(MyDataBaseAdapter.USER_REALNAME, user.getRealName());
			}
			initialValues.put(MyDataBaseAdapter.USER_PORTARITTYPE, user.getIconType());
			if (user.getSign() != null) {
				initialValues.put(MyDataBaseAdapter.USER_SIGNATURE, user.getSign());
			}
			initialValues.put(MyDataBaseAdapter.USER_TIME, new Timestamp(System.currentTimeMillis()) + "");

			if (myDataBaseAdapter.checkUserIsExist(Util.trimUserIDDomain(user.getUserId()))) {
				return myDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_USERS, initialValues,
						MyDataBaseAdapter.USER_ID, new String[] { user.getUserId() });
			}

			else {
				long _id = myDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_USERS, initialValues);
				if (_id > 0) {
					return true;
				}
			}

		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		}
		return false;
	}

	@Override
	public boolean updateSign(UserInfo userInfo) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
		try {
			m_MyDataBaseAdapter.open();
			String updateSql = "UPDATE " + MyDataBaseAdapter.TABLE_USERS + " SET "
					+ MyDataBaseAdapter.USER_SIGNATURE + "='" + userInfo.getSign() + "'  WHERE "
					+ MyDataBaseAdapter.USER_ID + "=" + userInfo.getUserId();
			return m_MyDataBaseAdapter.updateData(updateSql);
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}
}
