package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.db.IAttentsDao;
import com.wachoo.pangker.db.IBlacklistDao;
import com.wachoo.pangker.db.IContactsGroupDao;
import com.wachoo.pangker.db.IFansDao;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @author wangxin
 * 
 */
public class ContactsGroupDaoIpml implements IContactsGroupDao {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Context context;
	private IFansDao fansDao;
	private IPKUserDao userDao;
	private IFriendsDao friendsDao;
	private IAttentsDao attentsDao;
	private IBlacklistDao blacklistDao;
	private String myUserID;
	private PangkerApplication application;

	/**
	 * 
	 * GroupDaoIpml
	 * 
	 * @param context
	 */
	public ContactsGroupDaoIpml(Context context) {
		this.context = context;
		fansDao = new FansDaoImpl(context);
		userDao = new PKUserDaoImpl(context);
		friendsDao = new FriendsDaoImpl(context);
		attentsDao = new AttentsDaoImpl(context);
		blacklistDao = new BlacklistDaoImpl(context);
		application = (PangkerApplication) context.getApplicationContext();
		myUserID = application.getMyUserID();
	}

	@Override
	/**
	 * 同步时使用
	 */
	public void saveGroups(List<ContactGroup> groups) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			for (ContactGroup group : groups) {
				ContentValues groupValues = new ContentValues();
				groupValues.put(MyDataBaseAdapter.GROUP_GROUPID, group.getGid());
				groupValues.put(MyDataBaseAdapter.GROUP_CREATER, group.getUid());
				groupValues.put(MyDataBaseAdapter.GROUP_GROUPNAME, group.getName());
				groupValues.put(MyDataBaseAdapter.GROUP_MEMBER_COUNT, group.getCount());
				groupValues.put(MyDataBaseAdapter.GROUP_MYUID, myUserID);
				groupValues.put(MyDataBaseAdapter.GROUP_TYPE, group.getType());
				// group
				if (!m_MyDataBaseAdapter.checkContactsGroupIsExist(group.getGid(), myUserID, group.getType())) {
					long _frdgroupsid = m_MyDataBaseAdapter.insertData(
							MyDataBaseAdapter.TABLE_CONTACTS_GROUP, groupValues);
					System.out.println("Sava groupInfo to GroupTable ====>" + _frdgroupsid);
				}
				if (group.getType() == PangkerConstant.GROUP_FRIENDS) {// friend
					for (UserItem user : group.getUserItems()) {
						boolean result = friendsDao.addFriend(user, group.getGid(), m_MyDataBaseAdapter);
						System.out.println("Sava FriendInfo to FriendTable ====>" + result);
						result = userDao.saveUser(user, m_MyDataBaseAdapter);
						System.out.println("Sava FriendInfo to UserTable ====>" + result);
					}
				} else if (group.getType() == PangkerConstant.GROUP_FOLLOWS) {// fans
					for (UserItem user : group.getUserItems()) {
						// check fans exist
						boolean result = fansDao.saveFans(user, m_MyDataBaseAdapter);
						System.out.println("Sava Fans= to FansTable ====>" + result);
						result = userDao.saveUser(user, m_MyDataBaseAdapter);
						System.out.println("Sava FansInfo to UserTable ====>" + result);
					}
				} else if (group.getType() == PangkerConstant.GROUP_ATTENTS) {// attents
					for (UserItem user : group.getUserItems()) {
						boolean result = attentsDao.addAttent(user, group.getGid(), m_MyDataBaseAdapter);
						System.out.println("Sava FriendInfo to AttentsTable ====>" + result);
						result = userDao.saveUser(user, m_MyDataBaseAdapter);
						System.out.println("Sava FriendInfo to UserTable ====>" + result);
					}
				} else if (group.getType() == PangkerConstant.GROUP_BLACKLIST) {// blacklist
					for (UserItem user : group.getUserItems()) {
						boolean result = blacklistDao.insertBlacklist(user, m_MyDataBaseAdapter);
						System.out.println("Sava Blacklist to AttentsTable ====>" + result);
						result = userDao.saveUser(user, m_MyDataBaseAdapter);
						System.out.println("Sava Blacklist to UserTable ====>" + result);
					}
				}
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}

	@Override
	/**
	 * 添加组时使用
	 */
	public boolean addGroup(ContactGroup group, String myUid) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			ContentValues groupValues = new ContentValues();
			groupValues.put(MyDataBaseAdapter.GROUP_GROUPID, group.getGid());
			groupValues.put(MyDataBaseAdapter.GROUP_CREATER, group.getUid());
			groupValues.put(MyDataBaseAdapter.GROUP_GROUPNAME, group.getName());
			groupValues.put(MyDataBaseAdapter.GROUP_MEMBER_COUNT, group.getCount());
			groupValues.put(MyDataBaseAdapter.GROUP_MYUID, myUserID);
			groupValues.put(MyDataBaseAdapter.GROUP_TYPE, group.getType());

			// 组的id由后台接口返回。
			if (m_MyDataBaseAdapter.checkContactsGroupIsExist(group.getGid(), myUid, group.getType()))
				return false;
			long _frdgroupsid = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_CONTACTS_GROUP,
					groupValues);
			if (_frdgroupsid > 0) {
				application.addContactsGroup(group);
				return true;
			}

		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	// 删除本地group组（需要先调用后台接口删除成功，再调用此方法）
	public boolean delGroup(ContactGroup group, String myUid) {
		// TODO Auto-generated method stub
		boolean result = false;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();

			// 组的id由后台接口返回。
			if (!m_MyDataBaseAdapter.checkContactsGroupIsExist(group.getGid(), myUid, group.getType()))
				return result;
			result = m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_CONTACTS_GROUP,
					MyDataBaseAdapter.GROUP_GROUPID, group.getGid());
			if (result) {
				application.removeContactsGroup(group);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return result;
	}

	@Override
	public List<ContactGroup> getGroups() {
		// TODO Auto-generated method stub
		List<ContactGroup> groups = new ArrayList<ContactGroup>();
		Cursor mCursor = null;
		Cursor itemsCursor = null;
		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getContactsGroups(MyDataBaseAdapter.GROUP_MYUID, myUserID);
			if (mCursor == null)
				return groups;
			if (mCursor.getCount() == 0)
				return groups;
			mCursor.moveToFirst();
			groups = new ArrayList<ContactGroup>();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				if (itemsCursor != null && !itemsCursor.isClosed()) {
					itemsCursor.close();
				}
				ContactGroup group = new ContactGroup();
				group.setClientgid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_ID)));

				group.setGid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_GROUPID)));
				group.setName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_GROUPNAME)));
				group.setType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_TYPE)));
				group.setUid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_CREATER)));
				group.setCount(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_MEMBER_COUNT)));
				if (group.getType() == PangkerConstant.GROUP_FRIENDS) {// 好友搜索
					itemsCursor = m_MyDataBaseAdapter.getFriendsByGroupID(group.getGid(), myUserID);
					if (itemsCursor != null && itemsCursor.getCount() > 0) {
						itemsCursor.moveToFirst();
						for (; !itemsCursor.isAfterLast(); itemsCursor.moveToNext()) {
							UserItem item = new UserItem();

							item.setUserId(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_ID)));
							item.setUserName(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
							item.setNickName(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
							item.setIconType(itemsCursor.getInt(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识

							item.setSign(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));
							item.setrName(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.FRIENDS_REMARKNAME)));

							// if
							// (PresenceManager.isFriendOnline(Util.addUserIDDomain(item.getUserId())))
							// {
							// item.setState(PangkerConstant.STATE_TYPE_ONLINE);
							// } else {
							// item.setState(PangkerConstant.STATE_TYPE_OFFLINE);
							// }
							group.addUserItem(item);
						}
					}
				} else if (group.getType() == PangkerConstant.GROUP_FOLLOWS) {// fans
					// search
					itemsCursor = m_MyDataBaseAdapter.getUserFans(MyDataBaseAdapter.FANS_MYUID, myUserID);
					if (itemsCursor != null && itemsCursor.getCount() > 0) {
						itemsCursor.moveToFirst();
						for (; !itemsCursor.isAfterLast(); itemsCursor.moveToNext()) {
							UserItem user = new UserItem();
							user.setUserId(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_ID)));
							user.setUserName(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
							user.setNickName(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
							user.setIconType(itemsCursor.getInt(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识

							user.setSign(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));
							// int status =
							// PresenceManager.IsUserOnLine(user.getUserId());
							// if (status == PangkerConstant.STATE_TYPE_ONLINE)
							// {
							// user.setState(PangkerConstant.STATE_TYPE_ONLINE);
							// } else {
							// user.setState(PangkerConstant.STATE_TYPE_OFFLINE);
							// }
							group.addUserItem(user);
						}
					}
				} else if (group.getType() == PangkerConstant.GROUP_ATTENTS) {
					// search
					itemsCursor = m_MyDataBaseAdapter.getAttentsByGroupID(group.getGid(), myUserID);
					if (itemsCursor != null && itemsCursor.getCount() > 0) {
						itemsCursor.moveToFirst();
						for (; !itemsCursor.isAfterLast(); itemsCursor.moveToNext()) {
							UserItem user = new UserItem();
							user.setUserId(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_ID)));
							user.setUserName(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
							user.setNickName(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
							user.setIconType(itemsCursor.getInt(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识

							user.setSign(itemsCursor.getString(itemsCursor
									.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));

							group.addUserItem(user);
						}
					}
				}
				groups.add(group);
			}

		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			if (mCursor != null) {
				mCursor.close();
			}
			if (itemsCursor != null) {
				itemsCursor.close();
			}

		}
		// Log.i(tag, "get UserInfo Form database over ");
		return groups;
	}

	/***
	 * 
	 * @return
	 */
	public List<UserItem> getUserFans(MyDataBaseAdapter m_MyDataBaseAdapter) {
		// TODO Auto-generated method stub
		List<UserItem> fans = null;
		Cursor fansCursor = null;
		Cursor mCursor = null;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			fansCursor = m_MyDataBaseAdapter.getUserFans(MyDataBaseAdapter.FANS_MYUID, myUserID);
			if (fansCursor == null)
				return fans;
			if (fansCursor.getCount() == 0)
				return fans;
			fansCursor.moveToFirst();
			fans = new ArrayList<UserItem>();
			for (; !fansCursor.isAfterLast(); fansCursor.moveToNext()) {
				if (mCursor != null && !mCursor.isClosed()) {
					mCursor.close();
				}

				UserItem user = new UserItem();
				String fansid = fansCursor
						.getString(fansCursor.getColumnIndex(MyDataBaseAdapter.FANS_FANUID));
				mCursor = m_MyDataBaseAdapter.getUserDataByUserID(MyDataBaseAdapter.USER_ID,
						Util.trimUserIDDomain(fansid));
				if (mCursor == null)
					return null;
				if (mCursor.getCount() == 0)
					return null;
				mCursor.moveToFirst();
				for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
					user = new UserItem();
					user.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_ID)));
					user.setUserName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_USERNAME)));
					user.setNickName(mCursor.getString(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_NICKNAME)));
					user.setIconType(mCursor.getInt(mCursor
							.getColumnIndex(MyDataBaseAdapter.USER_PORTARITTYPE)));// 用户头像上传标识

					user.setSign(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.USER_SIGNATURE)));
					fans.add(user);
				}
			}

		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (fansCursor != null) {
				fansCursor.close();
			}
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		// Log.i(tag, "get UserInfo Form database over ");
		return fans;
	}

	@Override
	public void updateGroup(ContactGroup group, String myUid) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();

			// 组的id由后台接口返回。
			if (!m_MyDataBaseAdapter.checkContactsGroupIsExist(group.getGid(), myUid, group.getType()))
				return;

			ContentValues groupValues = new ContentValues();
			groupValues.put(MyDataBaseAdapter.GROUP_GROUPID, group.getGid());
			groupValues.put(MyDataBaseAdapter.GROUP_CREATER, group.getUid());
			groupValues.put(MyDataBaseAdapter.GROUP_GROUPNAME, group.getName());
			groupValues.put(MyDataBaseAdapter.GROUP_MEMBER_COUNT, group.getCount());
			groupValues.put(MyDataBaseAdapter.GROUP_MYUID, myUserID);
			groupValues.put(MyDataBaseAdapter.GROUP_TYPE, group.getType());
			// 更新使用的是本机保存的自动增长列的id
			boolean result = m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_CONTACTS_GROUP,
					groupValues, MyDataBaseAdapter.GROUP_ID, new String[] { group.getClientgid() });
			if (result) {
				application.updateContactsGroup(group);
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}

	@Override
	public List<ContactGroup> getMyGroupByType(String type) {
		List<ContactGroup> groups = new ArrayList<ContactGroup>();
		Cursor mCursor = null;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			mCursor = m_MyDataBaseAdapter.getMyGroupByType(myUserID, type);
			if (mCursor == null)
				return groups;
			if (mCursor.getCount() == 0)
				return groups;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				ContactGroup group = new ContactGroup();
				group.setClientgid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_ID)));
				String groupId = mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_GROUPID));
				group.setGid(groupId);
				group.setName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_GROUPNAME)));
				group.setType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_TYPE)));
				group.setUid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_CREATER)));
				List<UserItem> mUserItem = new ArrayList<UserItem>();
				if (type.equals(String.valueOf(PangkerConstant.GROUP_FRIENDS))) {
					mUserItem = friendsDao.getFriendsByGroupId(groupId);
				} else if (type.equals(String.valueOf(PangkerConstant.GROUP_ATTENTS))) {
					mUserItem = attentsDao.getAttentsByGroupId(groupId);
				}
				group.setCount(mUserItem != null ? mUserItem.size() : 0);
				if (mUserItem != null) {
					group.setMembers(mUserItem);
				}
				groups.add(group);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}

		}
		// Log.i(tag, "get UserInfo Form database over ");
		return groups;
	}

	@Override
	public String getGroupByNameId(String groupID) {
		// TODO Auto-generated method stub
		String groupName = "";
		Cursor mCursor = null;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			mCursor = m_MyDataBaseAdapter.getGroupByID(myUserID, groupID);
			if (mCursor == null)
				return groupName;
			if (mCursor.getCount() == 0)
				return groupName;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				groupName = mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.GROUP_GROUPNAME));
				return groupName;
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}

		}
		return groupName;
	}
}
