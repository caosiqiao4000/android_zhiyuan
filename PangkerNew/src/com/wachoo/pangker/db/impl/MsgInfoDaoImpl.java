package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.wachoo.pangker.db.IMsgInfoDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.MsgInfo;

public class MsgInfoDaoImpl implements IMsgInfoDao {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private Context context;

	private int[] DEALTYPS = { MsgInfo.MSG_APPLY, MsgInfo.MSG_RECOMMEND, MsgInfo.MSG_INVITION,
			MsgInfo.MSG_NOTICE, MsgInfo.MSG_TIP};

	public MsgInfoDaoImpl(Context context) {
		super();
		this.context = context;
	}

	private MsgInfo getMsgInfo(Cursor mCursor) {
		MsgInfo msgInfo = new MsgInfo();
		msgInfo.setMsgId(mCursor.getLong(mCursor.getColumnIndex(MyDataBaseAdapter.MSG_ID)));
		msgInfo.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MSG_USERID)));
		msgInfo.setFromId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MSG_FROMID)));
		msgInfo.setTheme(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MSG_THEME)));
		msgInfo.setContent(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MSG_CONTENT)));
		msgInfo.setType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MSG_TYPE)));
		msgInfo.setResType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MSG_RES_TYPE)));
		msgInfo.setUncount(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MSG_UNCOUNT)));
		msgInfo.setTime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MSG_TIME)));
		return msgInfo;
	}

	// 首先要删除分表的消息
	@Override
	public boolean delMsgInfo(MsgInfo msgInfo) {
		MyDataBaseAdapter m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			if (msgInfo.getType() == MsgInfo.MSG_NOTICE) {
				m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_NOTICEINFO, new String[] {
						MyDataBaseAdapter.NOTICE_USERID, MyDataBaseAdapter.NOTICE_MSG_TYPE }, new String[] {
						msgInfo.getUserId(), String.valueOf(msgInfo.getType()) });
			} else if (msgInfo.getType() == MsgInfo.MSG_GROUP_CHAT) {
				if (!m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_MEETROOM,
						MyDataBaseAdapter.MEETROOM_ID, msgInfo.getFromId())) {
					return false;
				}
			}
			return m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_MSGINFO, MyDataBaseAdapter.MSG_ID,
					msgInfo.getMsgId());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close(m_MyDataBaseAdapter, null);
		}
		return false;
	}

	@Override
	public List<MsgInfo> getAllMsgInfos(String userId) {
		// TODO Auto-generated method stub
		List<MsgInfo> msgInfos = new ArrayList<MsgInfo>();
		MyDataBaseAdapter m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		Cursor mCursor = null;
		try {
			mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_MSGINFO,
					new String[] { MyDataBaseAdapter.MSG_USERID }, new String[] { userId },
					MyDataBaseAdapter.MSG_TIME, true);
			if (mCursor == null)
				return msgInfos;
			if (mCursor.getCount() == 0)
				return msgInfos;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				MsgInfo msgInfo = getMsgInfo(mCursor);
				msgInfos.add(msgInfo);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close(m_MyDataBaseAdapter, mCursor);
		}
		return msgInfos;
	}

	private boolean ifInType(int type) {
		for (int t : DEALTYPS) {
			if (t == type) {
				return true;
			}
		}
		return false;
	}

	private MsgInfo getBeforeMsgInfo(MsgInfo msgInfo) {
		MsgInfo beforeMsgInfo = new MsgInfo();
		MyDataBaseAdapter m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		Cursor mCursor = null;
		try {
			//根据类型获取
			if (ifInType(msgInfo.getType())) {
				mCursor = m_MyDataBaseAdapter.getTableInfo(MyDataBaseAdapter.TABLE_MSGINFO, new String[] {
						MyDataBaseAdapter.MSG_USERID, MyDataBaseAdapter.MSG_TYPE },
						new String[] {msgInfo.getUserId(), String.valueOf(msgInfo.getType()) });
			} 
			//根据FromId获取
			else {
				mCursor = m_MyDataBaseAdapter.getTableInfo(
						MyDataBaseAdapter.TABLE_MSGINFO,
						new String[] { MyDataBaseAdapter.MSG_USERID, MyDataBaseAdapter.MSG_TYPE,
								MyDataBaseAdapter.MSG_FROMID },
						new String[] { msgInfo.getUserId(), String.valueOf(msgInfo.getType()),
								msgInfo.getFromId() });
			}
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 1) {
				mCursor.moveToFirst();
				beforeMsgInfo = getMsgInfo(mCursor);
			} else {
				return null;
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			close(m_MyDataBaseAdapter, mCursor);
		}
		return beforeMsgInfo;
	}

	@Override
	public synchronized long saveMsgInfo(MsgInfo msgInfo) {
		// TODO Auto-generated method stub
		MsgInfo beforeMsgInfo = getBeforeMsgInfo(msgInfo);
		MyDataBaseAdapter m_MyDataBaseAdapter = null;
		if (beforeMsgInfo == null) {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
			m_MyDataBaseAdapter.open();

			ContentValues initialValues = new ContentValues();
			initialValues.put(MyDataBaseAdapter.MSG_USERID, msgInfo.getUserId());
			initialValues.put(MyDataBaseAdapter.MSG_FROMID, msgInfo.getFromId());
			initialValues.put(MyDataBaseAdapter.MSG_THEME, msgInfo.getTheme());
			initialValues.put(MyDataBaseAdapter.MSG_CONTENT, msgInfo.getContent());
			initialValues.put(MyDataBaseAdapter.MSG_TYPE, msgInfo.getType());
			initialValues.put(MyDataBaseAdapter.MSG_TIME, msgInfo.getTime());
			initialValues.put(MyDataBaseAdapter.MSG_RES_TYPE, msgInfo.getResType());
			initialValues.put(MyDataBaseAdapter.MSG_UNCOUNT, msgInfo.getUncount());
			try {
				return m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_MSGINFO, initialValues);
			} catch (Exception e) {
				logger.error(TAG, e);
				return -1;
			} finally {
				close(m_MyDataBaseAdapter, null);
			}

		} else {
			beforeMsgInfo.setFromId(msgInfo.getFromId());
			beforeMsgInfo.setTheme(msgInfo.getTheme());
			beforeMsgInfo.setContent(msgInfo.getContent());
			beforeMsgInfo.setResType(msgInfo.getResType());
			beforeMsgInfo.setTime(msgInfo.getTime());
			beforeMsgInfo.setUncount(beforeMsgInfo.getUncount() + msgInfo.getUncount());
			if (updateMsgInfo(beforeMsgInfo)) {
				return beforeMsgInfo.getMsgId();
			}
		}
		return -1;
	}

	@Override
	public boolean updateMsgInfo(MsgInfo msgInfo) {
		MyDataBaseAdapter m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.MSG_FROMID, msgInfo.getFromId());
		initialValues.put(MyDataBaseAdapter.MSG_THEME, msgInfo.getTheme());
		initialValues.put(MyDataBaseAdapter.MSG_CONTENT, msgInfo.getContent());
		initialValues.put(MyDataBaseAdapter.MSG_TIME, msgInfo.getTime());
		initialValues.put(MyDataBaseAdapter.MSG_RES_TYPE, msgInfo.getResType());
		initialValues.put(MyDataBaseAdapter.MSG_UNCOUNT, msgInfo.getUncount());
		try {
			return m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_MSGINFO, initialValues,
					MyDataBaseAdapter.MSG_ID, new String[] { String.valueOf(msgInfo.getMsgId()) });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close(m_MyDataBaseAdapter, null);
		}
	}

	private void close(MyDataBaseAdapter m_MyDataBaseAdapter, Cursor mCursor) {
		if (m_MyDataBaseAdapter != null) {
			m_MyDataBaseAdapter.close();
			m_MyDataBaseAdapter = null;
		}
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}
	}

	@Override
	public int getUnreadCount(String userId) {
		// TODO Auto-generated method stub
		String selectSQL = "Select Sum(" + MyDataBaseAdapter.MSG_UNCOUNT + ") From "
				+ MyDataBaseAdapter.TABLE_MSGINFO + " Where " + MyDataBaseAdapter.MSG_USERID + " = '"
				+ userId + "'";
		MyDataBaseAdapter m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		int count = 0;
		Cursor mCursor = null;
		try {
			mCursor = m_MyDataBaseAdapter.rawQuery(selectSQL);
			mCursor.moveToFirst();
			count = mCursor.getInt(0);
		} catch (Exception e) {
			// TODO: handle exception
			return 0;
		} finally {
			close(m_MyDataBaseAdapter, mCursor);
		}
		return count;
	}

	@Override
	public boolean updateMsgInfo(String fromId, int type) {
		MyDataBaseAdapter m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		String updateSQL = "update " + MyDataBaseAdapter.TABLE_MSGINFO + " set "
				+ MyDataBaseAdapter.MSG_UNCOUNT + "=" + 0 + " where " + MyDataBaseAdapter.MSG_FROMID + " = "
				+ fromId + " and " + MyDataBaseAdapter.MSG_TYPE + " = " + type;
		try {
			return m_MyDataBaseAdapter.execSql(updateSQL);
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close(m_MyDataBaseAdapter, null);
		}
	}

	@Override
	public boolean clearMsgInfos(String userId) {
		MyDataBaseAdapter m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
		m_MyDataBaseAdapter.open();
		try {
			boolean isDeleSuccess = m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_MSGINFO,
					MyDataBaseAdapter.MSG_USERID, userId);
			return isDeleSuccess;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			close(m_MyDataBaseAdapter, null);
		}
		return false;
	}

}
