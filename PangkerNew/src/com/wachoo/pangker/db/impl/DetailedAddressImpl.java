package com.wachoo.pangker.db.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.db.IDetailedAddress;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.DetailedAddress;

public class DetailedAddressImpl implements IDetailedAddress {
	private Context context;

	public DetailedAddressImpl(Context context) {
		this.context = context;
	}

	@Override
	public boolean saveDitailedAddress(DetailedAddress detailedAddress) {
		MyDataBaseAdapter mdb = null;
		try {
			mdb = new MyDataBaseAdapter(this.context);
			mdb.open();
			ContentValues cv = new ContentValues();
			cv.put(MyDataBaseAdapter.DETAILEDADDRESS_ID, detailedAddress.getId());
			cv.put(MyDataBaseAdapter.DETAILEDADDRESS_ADDRESSSTREET, detailedAddress.getAddressStreet());
			cv.put(MyDataBaseAdapter.DETAILEDADDRESS_ADDRESSSTREETZIP, detailedAddress.getAddressStreetZip());
			cv.put(MyDataBaseAdapter.DETAILEDADDRESS_AREA, detailedAddress.getArea());
			cv.put(MyDataBaseAdapter.DETAILEDADDRESS_CITY, detailedAddress.getCity());
			cv.put(MyDataBaseAdapter.DETAILEDADDRESS_COUNTRY, detailedAddress.getCountry());
			cv.put(MyDataBaseAdapter.DETAILEDADDRESS_PROVINCE, detailedAddress.getProvince());
			cv.put(MyDataBaseAdapter.DETAILEDADDRESS_STREET, detailedAddress.getStreet());
			long _id = mdb.insertData(MyDataBaseAdapter.TABLE_DETAILEDADDRESS, cv);
			if (_id > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			if (mdb != null) {
				mdb.close();
			}
		}
	}

	@Override
	public DetailedAddress getDetailedAddress(String locationString) {
		MyDataBaseAdapter mdb = null;
		DetailedAddress detailedAddress = null;
		Cursor mCursor = null;
		try {
			mdb = new MyDataBaseAdapter(context);
			mdb.open();
			mCursor = mdb.getAllDatas(MyDataBaseAdapter.TABLE_DETAILEDADDRESS, MyDataBaseAdapter.DETAILEDADDRESS_ID,
					locationString);
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			detailedAddress = new DetailedAddress();
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				detailedAddress.setAddressStreet(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.DETAILEDADDRESS_ADDRESSSTREET)));
				detailedAddress.setAddressStreetZip(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.DETAILEDADDRESS_ADDRESSSTREETZIP)));
				detailedAddress.setCity(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.DETAILEDADDRESS_CITY)));
				detailedAddress.setCountry(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.DETAILEDADDRESS_COUNTRY)));
				detailedAddress.setArea(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.DETAILEDADDRESS_AREA)));
				detailedAddress.setProvince(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.DETAILEDADDRESS_PROVINCE)));
				detailedAddress.setStreet(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.DETAILEDADDRESS_STREET)));
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (mdb != null) {
				mdb.close();
			}
			if (mCursor != null)
				mCursor.close();
		}

		return detailedAddress;
	}

}
