package com.wachoo.pangker.db.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.util.Log;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.db.ILocalContactsDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.server.response.PUser;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

//>>>wangxin create 
public class LocalContactsDaoImpl implements ILocalContactsDao {
	private String TAG = "LocalContactsDaoImpl";// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private Context context;
	private ContentResolver resolver;
	static final Uri phoneuri = Uri
			.parse("content://com.android.contacts/data/phones");
	private static final String[] PHONES_PROJECTION = new String[] {
			Phone.CONTACT_ID, Phone.DISPLAY_NAME, Phone.NUMBER, "sort_key",
			Photo.PHOTO_ID };

	private static final String strSplit = ",";
	private String myUserId;

	public LocalContactsDaoImpl(Context context) {
		this.context = context;
		this.resolver = this.context.getContentResolver();
		myUserId = ((PangkerApplication) this.context.getApplicationContext())
				.getMyUserID();
	}

	long startTime = 0l;

	@Override
	public List<LocalContacts> initLocalAddressBooks() {
		startTime = System.currentTimeMillis();
		Log.d(TAG, ">>loadLocalAdressbook:start" + startTime);
		Cursor phoneCursor = null;
		List<LocalContacts> contacts = null;
		MyDataBaseAdapter adapter = null;
		try {

			contacts = new ArrayList<LocalContacts>();
			adapter = new MyDataBaseAdapter(context);
			adapter.open();
			phoneCursor = resolver.query(phoneuri, PHONES_PROJECTION, null,
					null, " sort_key COLLATE LOCALIZED asc ");

			if (phoneCursor != null) {

				int CONTACT_ID_COL = phoneCursor
						.getColumnIndex(Phone.CONTACT_ID);
				int DISPLAY_NAME_COL = phoneCursor
						.getColumnIndex(Phone.DISPLAY_NAME);
				int PHOTO_ID_COL = phoneCursor.getColumnIndex(Photo.PHOTO_ID);
				int SORT_KEY_COL = phoneCursor.getColumnIndex("sort_key");

				while (phoneCursor.moveToNext()) {
					LocalContacts loca = new LocalContacts();

					// 得到联系人ID
					Long contactid = phoneCursor.getLong(CONTACT_ID_COL);
					// 如果已经存在则不需要再添加
					if (Util.isExistLocatanct(contacts,
							String.valueOf(contactid))) {
						continue;
					} else
						loca.setContactId(String.valueOf(contactid));
					// 得到联系人名称
					String contactName = phoneCursor
							.getString(DISPLAY_NAME_COL);

					// 得到联系人头像ID
					Long photoid = phoneCursor.getLong(PHOTO_ID_COL);
					// 排序key
					String sortkey = phoneCursor.getString(SORT_KEY_COL);

					// 得到联系人头像Bitamp
					Bitmap contactPhoto = null;
					// photoid 大于0 表示联系人有头像 如果没有给此人设置头像则给他一个默认的
					if (photoid > 0) {
						Uri photpuri = ContentUris.withAppendedId(
								ContactsContract.Contacts.CONTENT_URI,
								contactid);
						InputStream input = ContactsContract.Contacts
								.openContactPhotoInputStream(resolver, photpuri);
						contactPhoto = BitmapFactory.decodeStream(input);
						loca.setHasPhoto(true);
						loca.setPhoto(contactPhoto);
					} else {
						loca.setHasPhoto(false);
					}
					// >>>>>>用户名
					loca.setUserName(contactName);
					// >>>>>排序的key
					loca.setSortkey(sortkey);

					// 当前汉语拼音首字母
					String currentStr = Util.getAlpha(sortkey);
					// 上一个汉语拼音首字母，如果不存在为" "
					String previewStr = (contacts.size() - 1) >= 0 ? Util
							.getAlpha(contacts.get(contacts.size() - 1)
									.getSortkey()) : " ";
					if (!previewStr.equals(currentStr)) {
						loca.setFirstLetter(Util.getAlpha(sortkey));
					} else {
						loca.setFirstLetter(LocalContacts.NO_LETTER);
					}

					contacts.add(loca);
				}
				phoneCursor.close();
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (phoneCursor != null)
				phoneCursor.close();
			if (adapter != null)
				adapter.close();

		}
		long endTime = System.currentTimeMillis();
		Log.d(TAG,
				">>loadLocalAdressbook:end" + endTime + ";size="
						+ contacts.size());
		Log.d(TAG,
				">>loadLocalAdressbook=" + String.valueOf(endTime - startTime));
		return contacts;

	}

	// 添加单个用户数据到匹配记录表
	@Override
	public boolean insertData(String ruid, String phoneNum) {
		// TODO Auto-generated method stub
		MyDataBaseAdapter adapter = null;
		try {
			adapter = new MyDataBaseAdapter(this.context);
			adapter.open();
			if (adapter.checkPangkerIsExist(ruid, myUserId)) {
				return false;
			}

			ContentValues friendsValues = new ContentValues();
			friendsValues.put(MyDataBaseAdapter.CONTACTS_PANGKER_MYUID,
					myUserId);
			friendsValues.put(MyDataBaseAdapter.CONTACTS_PANGKER_PHONENUM,
					phoneNum);
			friendsValues.put(MyDataBaseAdapter.CONTACTS_PANGKER_RUID, ruid);

			long _result = adapter.insertData(
					MyDataBaseAdapter.TABLE_CONTACTS_PANGKER, friendsValues);
			if (_result > 0)
				return true;

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (adapter != null) {
				adapter.close();
			}
		}
		return false;
	}

	@Override
	public void deletePUsers(String uid) {
		// TODO Auto-generated method stub
		MyDataBaseAdapter adapter = null;
		try {
			adapter = new MyDataBaseAdapter(this.context);
			adapter.open();
			adapter.deletePUsers();
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (adapter != null) {
				adapter.close();
			}
		}
	}

	// 同步匹配数据到本机
	@Override
	public void notifyPUsers(List<PUser> pUsers) {
		// TODO Auto-generated method stub
		MyDataBaseAdapter adapter = null;
		try {
			adapter = new MyDataBaseAdapter(this.context);
			adapter.open();

			// 先删除用户数据
			adapter.deletePUsers();

			// 再插入用户数据
			for (PUser pUser : pUsers) {
				insertPUser(pUser, adapter);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (adapter != null) {
				adapter.close();
			}
		}
	}

	// 插入数据
	private boolean insertPUser(PUser pUser, MyDataBaseAdapter baseAdapter) {
		if (baseAdapter.checkPangkerIsExist(pUser.getUid(), pUser.getPhone())) {
			return false;
		}

		ContentValues friendsValues = new ContentValues();
		friendsValues.put(MyDataBaseAdapter.CONTACTS_PANGKER_MYUID, myUserId);
		friendsValues.put(MyDataBaseAdapter.CONTACTS_PANGKER_PHONENUM,
				pUser.getPhone());
		friendsValues.put(MyDataBaseAdapter.CONTACTS_PANGKER_RUID,
				pUser.getUid());
		long _result = baseAdapter.insertData(
				MyDataBaseAdapter.TABLE_CONTACTS_PANGKER, friendsValues);
		if (_result > 0)
			return true;
		return false;
	}

	@Override
	public String getAllPhoneNumbers() {
		// TODO Auto-generated method stub
		StringBuffer allPhoneNumbers = new StringBuffer("");
		Cursor phones = null;
		try {
			phones = resolver.query(phoneuri, PHONES_PROJECTION, null, null,
					"sort_key COLLATE LOCALIZED asc");
			if (phones != null) {
				phones.moveToFirst();
				while (phones.moveToNext()) {
					// 得到联系人名称
					String phoneNumber = Util.delSpace(phones.getString(phones
							.getColumnIndex(Phone.NUMBER)));
					if (phoneNumber.contains("+86")) {
						phoneNumber = phoneNumber.substring(3,
								phoneNumber.length());
					}
					allPhoneNumbers.append(phoneNumber);
					allPhoneNumbers.append(strSplit);
				}
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (phones != null)
				phones.close();
		}

		if (allPhoneNumbers.length() > 0)
			return allPhoneNumbers.toString().substring(0,
					allPhoneNumbers.length() - 1);
		else
			return "";
	}

	@Override
	public LocalContacts getLocalContact(String phoneNum) {
		LocalContacts localContacts = null;
		Cursor pCur = null;
		try {
			ContentResolver cr = context.getContentResolver();
			pCur = cr.query(phoneuri, null, Phone.NUMBER + " = ?",
					new String[] { phoneNum }, null);
			if (pCur.moveToFirst()) {
				localContacts = new LocalContacts();
				String contactName = pCur.getString(pCur
						.getColumnIndex(Phone.DISPLAY_NAME));

				localContacts.setUserName(contactName);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (pCur != null)
				pCur.close();
		}
		return localContacts;

	}

	@Override
	public List<LocalContacts> matchRegisterUsers(List<LocalContacts> list) {
		// TODO Auto-generated method stub
		ContentResolver contentResolver;
		SharedPreferencesUtil preferencesUtil;
		MyDataBaseAdapter adapter = null;
		try {
			adapter = new MyDataBaseAdapter(context);
			adapter.open();
			preferencesUtil = new SharedPreferencesUtil(this.context);
			contentResolver = context.getContentResolver();
			final boolean isBandAdress = preferencesUtil.getBoolean(
					PangkerConstant.IS_BIND_KEY, false);

			for (LocalContacts local : list) {
				local.initSelf(contentResolver, myUserId, adapter, isBandAdress);
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (adapter != null)
				adapter.close();
		}

		Log.d(TAG, ">>matchRegisterUsers>>>all ok!");
		return list;
	}
}
