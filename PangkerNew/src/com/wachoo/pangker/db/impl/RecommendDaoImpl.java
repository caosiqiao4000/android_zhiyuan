package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.db.IRecommendDao;
import com.wachoo.pangker.db.IUserStatusDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.server.response.UserStatus;

public class RecommendDaoImpl implements IRecommendDao {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Cursor mCursor;
	private Context context;

	public RecommendDaoImpl(Context context) {
		super();
		this.context = context;
	}

	private RecommendInfo getRecommendInfo() {
		RecommendInfo recommendInfo = new RecommendInfo();
		recommendInfo.setId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_ID)));
		recommendInfo
				.setDatetime(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_TIME)));
		recommendInfo
				.setFromId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_FROMID)));
		recommendInfo.setFromName(mCursor.getString(mCursor
				.getColumnIndex(MyDataBaseAdapter.RECOMMEND_FROMNAME)));
		recommendInfo
				.setReason(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_REASON)));
		recommendInfo
				.setRemark(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_REMARK)));
		recommendInfo.setResId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_SID)));
		recommendInfo
				.setResName(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_SNAME)));
		recommendInfo.setStatus(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_STATUS)));
		recommendInfo.setType(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_TYPE)));
		recommendInfo
				.setUserId(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_USERID)));
		recommendInfo
				.setSinger(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.RECOMMEND_SINGER)));
		recommendInfo.setShareUid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.REMARK5)));
		return recommendInfo;
	}

	@Override
	public boolean deleteRecommendInfo(int id) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_RECOMMEND, MyDataBaseAdapter.RECOMMEND_ID,
					String.valueOf(id));
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return false;
	}

	@Override
	public List<RecommendInfo> getRecommendInfos(String userId, boolean isAccept, int offset) {
		// TODO Auto-generated method stub
		List<RecommendInfo> recommendInfos = new ArrayList<RecommendInfo>();
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			
			if(isAccept){
				mCursor = m_MyDataBaseAdapter.getTableInfoEx(MyDataBaseAdapter.TABLE_RECOMMEND,
						new String[] {MyDataBaseAdapter.RECOMMEND_USERID }, new String[] {userId},
						MyDataBaseAdapter.RECOMMEND_TIME, true, 10, offset);
			} else {
				mCursor = m_MyDataBaseAdapter.getTableInfoEx(MyDataBaseAdapter.TABLE_RECOMMEND,
						new String[] {MyDataBaseAdapter.RECOMMEND_FROMID }, new String[] {userId},
						MyDataBaseAdapter.RECOMMEND_TIME, true, 10, offset);
			}
			
			if (mCursor == null)
				return recommendInfos;
			if (mCursor.getCount() == 0)
				return recommendInfos;
			mCursor.moveToFirst();
			
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				RecommendInfo info = getRecommendInfo();
				
				//如果是自己发出的资源，还需读取被推荐人数据
				if(info.getFromId().equals(userId)){
					IUserStatusDao mIUserStatusDao = new UserStatusDaoImpl(context);
					List<UserStatus> userStatusList =  mIUserStatusDao.getUserStatus(userId, info.getResId(), info.getDatetime());
					if(userStatusList != null){
						info.setUserStatus(userStatusList);
					}
				}
				recommendInfos.add(info);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return recommendInfos;
	}

	@Override
	public long saveRecommendInfo(RecommendInfo recommendInfo) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.RECOMMEND_FROMID, recommendInfo.getFromId());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_FROMNAME, recommendInfo.getFromName());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_USERID, recommendInfo.getUserId());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_TYPE, recommendInfo.getType());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_STATUS, recommendInfo.getStatus());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_SID, recommendInfo.getResId());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_SNAME, recommendInfo.getResName());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_TIME, recommendInfo.getDatetime());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_REASON, recommendInfo.getReason());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_SINGER, recommendInfo.getSinger());
		initialValues.put(MyDataBaseAdapter.RECOMMEND_REMARK, recommendInfo.getRemark());
		initialValues.put(MyDataBaseAdapter.REMARK5, recommendInfo.getShareUid());
		try {
			return m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_RECOMMEND, initialValues);
		} catch (Exception e) {
			logger.error(TAG, e);
			return -1;
		} finally {
			close();
		}
	}

	@Override
	public boolean truncRecommendInfo(String userId) {
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
			m_MyDataBaseAdapter.open();
			String sqlStr = "delete from " + MyDataBaseAdapter.TABLE_RECOMMEND + " where " + MyDataBaseAdapter.RECOMMEND_USERID + "='"+userId+"' or " +MyDataBaseAdapter.RECOMMEND_FROMID + "='"+userId+"'"; 
			m_MyDataBaseAdapter.execSql(sqlStr);
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return false;
	}

	@Override
	public boolean updateRecommendInfo(RecommendInfo recommendInfo) {
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		ContentValues initialValues = new ContentValues();
		initialValues.put(MyDataBaseAdapter.RECOMMEND_STATUS, recommendInfo.getStatus());
		try {
			return m_MyDataBaseAdapter.updateData(MyDataBaseAdapter.TABLE_RECOMMEND, initialValues,
					MyDataBaseAdapter.RECOMMEND_ID, new String[] { String.valueOf(recommendInfo.getId()) });
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			close();
		}
	}

	private void close() {
		if (m_MyDataBaseAdapter != null) {
			m_MyDataBaseAdapter.close();
		}
		if (mCursor != null) {
			mCursor.close();
			mCursor = null;
		}
	}

	@Override
	public int getRecommendInfoCount(String userId, boolean isAccept) {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			if(isAccept){
				mCursor = m_MyDataBaseAdapter.getTableInfoEx(MyDataBaseAdapter.TABLE_RECOMMEND,
						new String[] {MyDataBaseAdapter.RECOMMEND_USERID }, new String[] {userId},
						MyDataBaseAdapter.RECOMMEND_TIME, true);
			} else {
				mCursor = m_MyDataBaseAdapter.getTableInfoEx(MyDataBaseAdapter.TABLE_RECOMMEND,
						new String[] {  MyDataBaseAdapter.RECOMMEND_FROMID }, new String[] {userId},
						MyDataBaseAdapter.RECOMMEND_TIME, true);
			}
			
			if (mCursor == null)
				return 0;
			return mCursor.getCount();
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			close();
		}
		return 0;
	}

	@Override
	public void saveRecommends(String mUserId ,List<RecommendInfo> mRecommendList) {
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
			m_MyDataBaseAdapter.open();
			for (RecommendInfo recommendInfo : mRecommendList) {
				
				//如果是自己推荐的资源，保存被推荐人列表数据
				if(recommendInfo.getFromId().equals(mUserId)){
					IUserStatusDao mIUserStatusDao = new UserStatusDaoImpl(context);
					mIUserStatusDao.insertUserStatus(mUserId, recommendInfo.getResId(), recommendInfo.getDatetime(), recommendInfo.getUserStatus());
				}
				if (!isExist(String.valueOf(recommendInfo.getResId()), m_MyDataBaseAdapter)) {
					
					ContentValues initialValues = new ContentValues();
					initialValues.put(MyDataBaseAdapter.RECOMMEND_FROMID, recommendInfo.getFromId());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_FROMNAME, recommendInfo.getFromName());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_USERID, recommendInfo.getUserId());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_TYPE, recommendInfo.getType());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_STATUS, recommendInfo.getStatus());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_SID, recommendInfo.getResId());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_SNAME, recommendInfo.getResName());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_TIME, recommendInfo.getDatetime());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_REASON, recommendInfo.getReason());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_SINGER, recommendInfo.getSinger());
					initialValues.put(MyDataBaseAdapter.RECOMMEND_REMARK, recommendInfo.getRemark());
					initialValues.put(MyDataBaseAdapter.REMARK5, recommendInfo.getShareUid());
					m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_RECOMMEND, initialValues);
				}
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}
	
	@Override
	public boolean isExist(String resId, MyDataBaseAdapter adapter){
		
		String selectSQL = "select * from " + MyDataBaseAdapter.TABLE_RECOMMEND + " where "
		+ MyDataBaseAdapter.RECOMMEND_SID + "='" + resId + "'";
        Cursor mCursor = null;
        boolean isExist = false;
        try {
	        mCursor = adapter.rawQuery(selectSQL);
	        isExist = mCursor != null && mCursor.getCount() > 0;

        } catch (Exception e) {
	        logger.error(TAG, e);
        } finally {
	        if (mCursor != null) {
		        mCursor.close();
	        }
        }
		return isExist;
	}
}
