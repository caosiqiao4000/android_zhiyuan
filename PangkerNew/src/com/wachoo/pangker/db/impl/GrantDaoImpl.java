package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.db.IGrantDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.entity.Grant;

/**
 * @author wubo
 * @createtime May 7, 2012
 */
public class GrantDaoImpl implements IGrantDao {
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	MyDataBaseAdapter db;
	Context context;

	public GrantDaoImpl(Context context) {
		this.context = context;
	}

	@Override
	public Grant getGrantValue(String uid, String name) {
		try {
			db = new MyDataBaseAdapter(context);
			db.open();
			Grant grant = db.getGrantValue(uid, name);
			return grant;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
			return null;
		} finally {
			if (db != null)
				db.close();
		}
	}

	@Override
	public boolean saveGrants(List<Grant> grants) {
		// TODO Auto-generated method stub
		db = new MyDataBaseAdapter(context);
		db.open();
		try {
			for (int i = 0; i < grants.size(); i++) {
				ContentValues values = new ContentValues();
				values.put(MyDataBaseAdapter.GRANT_UID, grants.get(i).getUid());
				values.put(MyDataBaseAdapter.GRANT_NAME, grants.get(i).getGrantName());
				values.put(MyDataBaseAdapter.GRANT_VALUE, grants.get(i).getGrantValue());
				values.put(MyDataBaseAdapter.GRANT_GID, grants.get(i).getGrantGId());
				values.put(MyDataBaseAdapter.GRANT_UIDS, grants.get(i).getGrantUid());
				db.insertData(MyDataBaseAdapter.TABLE_GRANT, values);
			}
			return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (db != null)
				db.close();
		}
	}

	@Override
	public boolean updateGrant(String uid, String name, int value) {
		// TODO Auto-generated method stub
		try {
			db = new MyDataBaseAdapter(context);
			db.open();
			db.updateGrantValue(uid, name, value);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
			return false;
		} finally {
			if (db != null)
				db.close();
		}
	}

	@Override
	public List<Grant> getGrantValues(String uid) {
		List<Grant> grants = new ArrayList<Grant>();
		Cursor cursor = null;
		try {
			db = new MyDataBaseAdapter(context);
			db.open();
			cursor = db.getGrants(uid);
			if (cursor != null) {
				cursor.moveToFirst();
				while (!cursor.isAfterLast()) {
					Grant grant = new Grant();
					grant.setGrantId(cursor.getInt(cursor.getColumnIndex(MyDataBaseAdapter.GRANT_ID)));
					grant.setGrantName(cursor.getString(cursor.getColumnIndex(MyDataBaseAdapter.GRANT_NAME)));
					grant.setGrantValue(cursor.getInt(cursor.getColumnIndex(MyDataBaseAdapter.GRANT_VALUE)));
					grant.setUid(cursor.getString(cursor.getColumnIndex(MyDataBaseAdapter.GRANT_UID)));
					grant.setGrantGId(cursor.getString(cursor.getColumnIndex(MyDataBaseAdapter.GRANT_GID)));
					grant.setGrantUid(cursor.getString(cursor.getColumnIndex(MyDataBaseAdapter.GRANT_UIDS)));
					grants.add(grant);
					cursor.moveToNext();
				}
			}
			return grants;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
			return grants;
		} finally {
			if (db != null)
				db.close();
			if (cursor != null)
				cursor.close();
		}
	}

	@Override
	public void updateGrantGid(String uid, String name, int value, String gids) {
		// TODO Auto-generated method stub
		try {
			db = new MyDataBaseAdapter(context);
			db.open();
			db.updateGrantGid(uid, name, value, gids);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
		} finally {
			if (db != null)
				db.close();
		}
	}

	@Override
	public void updateGrantUid(String uid, String name, int value, String uids) {
		// TODO Auto-generated method stub
		try {
			db = new MyDataBaseAdapter(context);
			db.open();
			db.updateGrantUid(uid, name, value, uids);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
		} finally {
			if (db != null)
				db.close();
		}
	}

	@Override
	public boolean saveGrant(Grant grant) {
		// TODO Auto-generated method stub
		try {
			db = new MyDataBaseAdapter(context);
			db.open();

			ContentValues values = new ContentValues();
			values.put(MyDataBaseAdapter.GRANT_UID, grant.getUid());
			values.put(MyDataBaseAdapter.GRANT_NAME, grant.getGrantName());
			values.put(MyDataBaseAdapter.GRANT_VALUE, grant.getGrantValue());
			values.put(MyDataBaseAdapter.GRANT_GID, grant.getGrantGId());
			values.put(MyDataBaseAdapter.GRANT_UIDS, grant.getGrantUid());
			long result = db.insertData(MyDataBaseAdapter.TABLE_GRANT, values);
			if (result > 0)
				return true;
			return false;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (db != null)
				db.close();
		}

	}
}
