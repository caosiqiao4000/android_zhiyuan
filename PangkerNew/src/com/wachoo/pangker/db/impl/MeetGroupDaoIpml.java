package com.wachoo.pangker.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.IMeetGroupDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.server.response.MeetGroup;

/**
 * 
 * @author wubo
 * @createtime Apr 20, 2012
 */
public class MeetGroupDaoIpml implements IMeetGroupDao {
	private MyDataBaseAdapter m_MyDataBaseAdapter;
	private Context context;
	private String myUserID;
	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	/**
	 * 
	 * @param context
	 */
	public MeetGroupDaoIpml(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		myUserID = ((PangkerApplication) context.getApplicationContext()).getMySelf().getUserId();
	}

	@Override
	public boolean addGroup(MeetGroup group) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			ContentValues groupValues = new ContentValues();

			groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPID, group.getGroupId());
			groupValues.put(MyDataBaseAdapter.MEETGROUP_UID, group.getUid());
			groupValues.put(MyDataBaseAdapter.MEETGROUP_OWNUID, group.getOwnUid());
			groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPSID, group.getGroupSid());
			groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPNAME, group.getGroupName());
			groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPTYPE, group.getGroupType());
			groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPOWN, group.getGroupOwn());
			long _frdgroupsid = m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_MEETGROUP,
					groupValues);
			if (_frdgroupsid > 0)
				return true;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	public boolean delGroup(MeetGroup group) {
		// TODO Auto-generated method stub
		boolean result = false;
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();

			result = m_MyDataBaseAdapter.deleteData(MyDataBaseAdapter.TABLE_MEETGROUP,
					MyDataBaseAdapter.MEETGROUP_GROUPID, group.getGroupId());
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return result;
	}

	@Override
	public void saveGroups(List<MeetGroup> groups) {
		try {
			m_MyDataBaseAdapter = new MyDataBaseAdapter(this.context);
			m_MyDataBaseAdapter.open();
			for (MeetGroup group : groups) {
				ContentValues groupValues = new ContentValues();
				groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPID, group.getGroupId());
				groupValues.put(MyDataBaseAdapter.MEETGROUP_UID, group.getUid());
				groupValues.put(MyDataBaseAdapter.MEETGROUP_OWNUID, group.getOwnUid());
				groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPSID, group.getGroupSid());
				groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPNAME, group.getGroupName());
				groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPTYPE, group.getGroupType());
				groupValues.put(MyDataBaseAdapter.MEETGROUP_GROUPOWN, group.getGroupOwn());

				m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_USER_GROUP, groupValues);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}

	}

	@Override
	public List<MeetGroup> getMeetGroups() {
		// TODO Auto-generated method stub
		List<MeetGroup> groups = null;
		Cursor mCursor = null;

		this.m_MyDataBaseAdapter = new MyDataBaseAdapter(context);
		m_MyDataBaseAdapter.open();
		try {
			mCursor = m_MyDataBaseAdapter.getMeetGroup(myUserID);
			if (mCursor == null)
				return null;
			if (mCursor.getCount() == 0)
				return null;
			mCursor.moveToFirst();
			groups = new ArrayList<MeetGroup>();
			while (!mCursor.isAfterLast()) {
				MeetGroup group = new MeetGroup();
				group.setId(mCursor.getInt(mCursor.getColumnIndex(MyDataBaseAdapter.MEETGROUP_ID)));
				group.setGroupId(mCursor.getLong(mCursor
						.getColumnIndex(MyDataBaseAdapter.MEETGROUP_GROUPID)));
				group.setUid(mCursor.getString(mCursor.getColumnIndex(MyDataBaseAdapter.MEETGROUP_UID)));
				group.setOwnUid(mCursor.getLong(mCursor.getColumnIndex(MyDataBaseAdapter.MEETGROUP_OWNUID)));
				group.setGroupSid(mCursor.getLong(mCursor
						.getColumnIndex(MyDataBaseAdapter.MEETGROUP_GROUPSID)));
				group.setGroupName(mCursor.getString(mCursor
						.getColumnIndex(MyDataBaseAdapter.MEETGROUP_GROUPNAME)));
				group.setGroupType(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.MEETGROUP_GROUPTYPE)));
				group.setGroupOwn(mCursor.getInt(mCursor
						.getColumnIndex(MyDataBaseAdapter.MEETGROUP_GROUPOWN)));
				groups.add(group);
			}

		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
			if (mCursor != null) {
				mCursor.close();
			}
		}
		// Log.i(tag, "get UserInfo Form database over ");
		return groups;
	}

}
