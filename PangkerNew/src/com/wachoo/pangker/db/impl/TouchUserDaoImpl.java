package com.wachoo.pangker.db.impl;

import android.content.ContentValues;
import android.content.Context;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.db.ITouchUserDao;
import com.wachoo.pangker.db.MyDataBaseAdapter;
import com.wachoo.pangker.util.Util;

public class TouchUserDaoImpl implements ITouchUserDao {

	private Context mContext;
	private String myuserid;
	private MyDataBaseAdapter m_MyDataBaseAdapter;

	public TouchUserDaoImpl(Context mContext) {
		super();
		this.mContext = mContext;
		this.myuserid = ((PangkerApplication) mContext.getApplicationContext()).getMyUserID();

	}

	@Override
	public boolean canTouchUser() {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.mContext);

		try {
			m_MyDataBaseAdapter.open();
			return m_MyDataBaseAdapter.canTouchUser(myuserid, Util.getToday());
		} catch (Exception e) {

		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
		return false;
	}

	@Override
	public void itouchUser() {
		// TODO Auto-generated method stub
		m_MyDataBaseAdapter = new MyDataBaseAdapter(this.mContext);
		try {
			m_MyDataBaseAdapter.open();
			if (m_MyDataBaseAdapter.dayTouchedUser(myuserid, Util.getToday())) {
				String update_touch = "UPDATE  " + MyDataBaseAdapter.TABLE_TOUCH_USER + " SET "
						+ MyDataBaseAdapter.TOUCH_USER_TIMES + " = " + MyDataBaseAdapter.TOUCH_USER_TIMES
						+ "+1 where " + MyDataBaseAdapter.TOUCH_USER_MYUID + " = '" + myuserid + "' AND "
						+ MyDataBaseAdapter.TOUCH_USER_DAY + " = '" + Util.getToday() + "'";

				m_MyDataBaseAdapter.execSql(update_touch);
			} else {
				ContentValues initialValues = new ContentValues();
				initialValues.put(MyDataBaseAdapter.TOUCH_USER_MYUID, myuserid);// 后台getNickName表示昵称，getUserName是真实名
				initialValues.put(MyDataBaseAdapter.TOUCH_USER_TIMES, 1);
				initialValues.put(MyDataBaseAdapter.TOUCH_USER_DAY, Util.getToday());

				m_MyDataBaseAdapter.insertData(MyDataBaseAdapter.TABLE_TOUCH_USER, initialValues);
			}

		} catch (Exception e) {

		} finally {
			if (m_MyDataBaseAdapter != null) {
				m_MyDataBaseAdapter.close();
			}
		}
	}
}
