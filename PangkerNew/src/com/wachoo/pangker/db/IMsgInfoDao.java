package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.MsgInfo;

public interface IMsgInfoDao {
	/**
	 * 记录一条消息
	 * 
	 * @param noticeInfo
	 * @return
	 */
	public long saveMsgInfo(MsgInfo msgInfo);

	/**
	 * 获取所有的消息
	 * 
	 * @return
	 */
	public List<MsgInfo> getAllMsgInfos(String userId);

	/**
	 * 删除一条消息,要删除对应表的所有消息
	 * 
	 * @param noticeInfo
	 * @return
	 */
	public boolean delMsgInfo(MsgInfo msgInfo);

	/**
	 * 修改一条消息状态
	 * 
	 * @param noticeInfo
	 */
	public boolean updateMsgInfo(MsgInfo msgInfo);
	
	/**
	 * 修改一条消息状态
	 *  根据fromId和类型
	 * @param noticeInfo
	 */
	public boolean updateMsgInfo(String fromId, int type);

	/**
	 * 获取所有的消息
	 * 
	 * @return
	 */
	public int getUnreadCount(String userId);
	
	/**
	 * 清空用户所有的会话
	 * 
	 * @return
	 */
	public boolean clearMsgInfos(String userId);
}
