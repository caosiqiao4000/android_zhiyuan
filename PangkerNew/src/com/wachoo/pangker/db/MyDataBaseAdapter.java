package com.wachoo.pangker.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.pangkerArea;
import com.wachoo.pangker.entity.Grant;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.util.Util;

/**
 * sqlite databaseManager
 * 
 * @author wangxin
 * 
 */
public class MyDataBaseAdapter {
	private String TAG = "MyDataBaseAdapter";// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	// 数据库名称
	private static final String DB_NAME = "pangker.db";
	// 数据库版本，每一次的数据库版本都以1增加，
	private static final int DB_VERSION = 5;

	/** 每个表都添加的5个保留字段 **/
	public static final String REMARK1 = "remarkFir";// 保留字段1
	public static final String REMARK2 = "remarkSec";// 保留字段2
	public static final String REMARK3 = "remarkThi";// 保留字段3
	public static final String REMARK4 = "remarkFor";// 保留字段4
	public static final String REMARK5 = "remarkFiv";// 保留字段5
	/*******************/

	// 联系人表 wangxin
	public static final String TABLE_USERS = "UsersTable"; // 表名
	public static final String _ID = "_id";// 自动增长列
	public static final String USER_ID = "userid";// 用户id
	public static final String USER_Contacteid = "contacteId"; // 联系人id
	public static final String USER_SYNID = "synId"; // 同步id
	public static final String USER_USERNAME = "userName"; // 联系人名称
	public static final String USER_NICKNAME = "nickName"; // 联系人昵称
	public static final String USER_REALNAME = "realName"; // 联系人真实名
	public static final String USER_PORTARIT = "portrait"; // 头像
	public static final String USER_SIGNATURE = "signature"; // 个性签名
	public static final String USER_ADDR = "addr"; // 所在地
	public static final String USER_COMPANY = "company"; // 公司
	public static final String USER_ORG = "org"; // 部门
	public static final String USER_EMAIL = "email"; // 邮箱
	public static final String USER_TIME = "updatetime"; // 最后更新时间
	public static final String USER_STATUS = "updateStatus"; // 最后更新状态
	public static final String USER_HEADlETTER = "headLetter"; // 联系人的名称首字母
	public static final String USER_VERSION = "version"; // 联系人版本号
	public static final String USER_FRIEND = "user_friend"; // 是否为好友
	public static final String USER_NET_PORTARIT = "netportrait"; // IM好友图像
	public static final String USER_PORTARITTYPE = "portraittpye"; // 头像类型 add
	public static final String USER_SEX = "sex"; // 性别 >>>> wangxin
	public static final String USER_BIRTHDAY = "birthday"; // 生日 >>> wangxin
	public static final String USER_IDENTITY = "identity"; // 身份类型 >>> wangxin
	public static final String USER_CALLPHONE = "callphone"; // 呼叫转移号码

	// 联系人的电话号码表 wangxin
	public static final String TABLE_PHONE = "PhoneTable"; // 表名
	public static final String PHONE_ID = "_id";
	public static final String PHONE_Contacteid = "contacteId"; // 联系人ID
	public static final String PHONE_SYNID = "synId"; // 同步数据ID
	public static final String PHONE_NUM = "phonenum";
	public static final String PHONE_TYPE = "phonetype"; // 电话类型，归属地判断
	public static final String PHONE_FRIEND = "phone_friend"; // 是否为好友

	// 联系人的版本表 wangxin
	public static final String TABLE_VERSION = "VersionTable"; // 表名
	public static final String VERSION_ID = "_id";
	public static final String VERSION_VERSION = "version"; // 是否为好友
	public static final String VERSION_Contacteid = "contacteId"; // 联系人ID
	public static final String VERSION_STATUS = "contactstatus"; // 同步数据ID

	// 聊天记录表 wangxin
	public static final String TABLE_MESRECORD = "MesRecordTable";// 表名
	public static final String MESSAGE_ID = "_id";
	public static final String MESSAGE_PORTRAIT = "message_portrait"; // 发送者的头像
	public static final String MESSAGE_NAME = "sendername"; // 发送者的名称
	public static final String MESSAGE_SENDER = "sender";
	public static final String MESSAGE_RECEIVER = "receiver";
	public static final String MESSAGE_CONTENT = "content";
	public static final String MESSAGE_TIME = "creatTime";
	public static final String MESSAGE_CHATNUM = "chatNum"; // 所在聊天室编号
	public static final String MESSAGE_STATUS = "status"; // 记录状态，0：送达，1：已读（听），
	public static final String MESSAGE_TYPE = "type"; // 消息类型，0：文字；1：图片；2：语音
	public static final String MESSAGE_FROM = "fromtype";
	// 聊天室 wangxin
	public static final String TABLE_CHATROOM = "ChatRoomTable";
	public static final String CHAT_ID = "_id";
	public static final String CHAT_PORTRAIT = "chat_portrait";
	public static final String CHAT_NAME = "chatname";
	public static final String CHAT_NUM = "chatNum";
	public static final String CHAT_MENBERSNUM = "MembersNum";
	public static final String CHAT_TIME = "cCreatTime";
	public static final String CHAT_TYPE = "TYPE"; // 聊天室类型，0：私聊；1：群聊

	public static final String CHAT_ISRECEIVERSMS = "isReceiveSms";
	public static final String CHAT_OWNER = "owner";
	public static final String CHAT_MAXCOUNT = "maxCount";

	// 私信聊天记录
	public static final String TABLE_USERMSG = "UserMsgTable"; // 表名
	public static final String USERMSG_ID = "_id"; // 私信编号
	public static final String USERMSG_MYUID = "my_uid"; // 登录用户的uid
	public static final String USERMSG_HIMUID = "him_uid"; // 聊天对象的uid
	public static final String USERMSG_DIRECTION = "direction"; // 谁发的信息(0表示发送人,1表示聊天对象)
	public static final String USERMSG_CONTENT = "content"; // 信息的内容
	public static final String USERMSG_TALKTIME = "time"; // 发信息的时间
	public static final String USERMSG_FILEPATH = "filepath"; // 发送人的SD卡文件地址;接收人的服务器文件地址
	public static final String USERMSG_STATUS = "status";
	// 信息的状态, 0:文字信息，不用处理，1:开始发送,2:发送失败,3:发送成功,4:拒绝接收,5:接收失败,6:接收成功,
	public static final String USERMSG_TYPE = "type";

	// >>>>>>>IM服务器是否接收到私聊消息
	public static final String USERMSG_RECV = "msg_recv";

	/**
	 * Add by wubo 会议室聊天记录 edit by wangxin
	 */
	public static final String TABLE_MEETROOM = "MeetRoomTable"; // 表名
	public static final String MEETROOM_ID = "_id";
	public static final String MEETROOM_MYUID = "meetroom_myuid";
	public static final String MEETROOM_GID = "meetroom_gid"; // 房间编号
	public static final String MEETROOM_UID = "meetroom_uid"; // 发信人的uid
	public static final String MEETROOM_NICKNAME = "meetroom_nickname"; // 发信人的用户名
	public static final String MEETROOM_CONTENT = "meetroom_content"; // 内容
	public static final String MEETROOM_TALKTIME = "meetroom_talktime"; // 时间
	public static final String MEETROOM_MSGID = "meetroom_msgid"; // 编号
	public static final String MEETROOM_MSGTYPE = "meetroom_msgtype"; // 类型
	public static final String MEETROOM_STATUS = "meetroom_status"; // 文件下载状态

	// 粉丝联系人表 wangxin
	public static final String TABLE_FANS = "Fanstable"; // 表名（fans）
	public static final String FANS_MYUID = "myuid"; // 我的用户id（当前用户id）
	public static final String FANS_FANUID = "fansuid"; // 粉丝uid
	public static final String FANS_SOURCE = "sourse"; // 来源

	// 好友联系人表 wangxin
	public static final String TABLE_FRIENDS = "Friends"; // 表名
	public static final String FRIENDS_MYUID = "myuid"; // 我的用户id（当前用户id）
	public static final String FRIENDS_FRIENDUID = "frienduid"; // 好友用户id
	public static final String FRIENDS_GROUPID = "groupid"; // 好友所在组id
	public static final String FRIENDS_REMARKNAME = "remarkname";// 好友的备注名称
	public static final String FRIENDS_RELATIONTYPE = "relationtype"; // 关系状态
	// "0":添加等待对方确认
	// "1"：好友关系
	// "2": 亲友关系，也是好友,因此在查询好友的FRIENDS_RELATIONTYPE = 1或者FRIENDS_RELATIONTYPE = 2
	// "99":无关系
	public static final String FRIENDS_SOURCE = "sourse"; // 来源
	// 好友的经纬度信息，以及更新时间>>>>>>>>lb
	public static final String TRACE_LAT = "trace_lat";// 用户最后的维度
	public static final String TRACE_LNG = "trace_lng";// 用户最后的经度
	public static final String TRACE_TIME = "trace_time";// 用户最后的时间

	// 关注人联系人表 wangxin
	public static final String TABLE_ATTENTS = "Attents"; // 表名
	public static final String ATTENTS_MYUID = "myuid"; // 我的用户id（当前用户id）
	public static final String ATTENTS_ATTENTUID = "attentuid"; // 关注人用户id
	public static final String ATTENTS_GROUPID = "groupid"; // 关注人所在组id
	public static final String ATTENTS_SOURCE = "sourse"; // 来源
	public static final String ATTENTS_TYPE = "attenttype"; // 关注类型，0隐式关注，1显式关注。（后加，add
	// by zhengjy）

	// 友踪联系人表 wangxin
	public static final String TABLE_TRACE = "Trace"; // 表名
	public static final String TRACE_MYUID = "myuid"; // 我的用户id（当前用户id）
	public static final String TRACE_RUID = "ruid"; // 关注人用户id
	public static final String TRACE_GROUPID = "groupid"; // 关注人所在组id
	public static final String TRACE_SOURCE = "sourse"; // 来源

	// 联系人分组表 wangxin add
	public static final String TABLE_CONTACTS_GROUP = "ContactsGroups"; // 表名分组表
	public static final String GROUP_ID = "_id"; // 自动增长id
	public static final String GROUP_CREATER = "createruid"; // 组创建者
	public static final String GROUP_MYUID = "myuid"; // 登录用户uid
	public static final String GROUP_GROUPID = "groupid"; // 分组id
	public static final String GROUP_GROUPNAME = "groupname"; // 组名称
	public static final String GROUP_TYPE = "type"; // 组类别标识：用于区分组属于那些联系人类别，如：12：我的好友，14：关注人，15：我的粉丝（粉丝无分组信息）
	public static final String GROUP_MEMBER_COUNT = "membercount"; // 当前分组下记录总数

	// 联系人分组表 wangxin add
	public static final String TABLE_USER_GROUP = "UserGroups"; // 表名分组表
	public static final String USER_GROUP_ID = "_id"; // 自动增长id
	public static final String USER_GROUP_CREATER = "createruid"; // 组创建者
	public static final String USER_GROUP_MYUID = "myuid"; // 登录用户uid
	public static final String USER_GROUP_GROUPID = "groupid"; // 分组id
	public static final String USER_GROUP_GROUPSID = "groupsid"; // 组SID标识
	public static final String USER_GROUP_GROUPNAME = "groupname"; // 组名称
	public static final String USER_GROUP_CREATETIME = "createtime"; // 群组创建时间
	public static final String USER_GROUP_LASTVISITTIME = "lastvisittime"; // 群组访问时间
	public static final String USER_GROUP_TYPE = "type"; // 组类别：10:会议室，12：带位置属性的组，13：友踪组
	public static final String USER_GROUP_LOTYPE = "loType";// 0固定群组,1移动群组,2,没有位置属性
	public static final String USER_GROUP_FLAG = "flag"; // 标识:0:我的群组,1:收藏过的群组,2:参与过的群组
	public static final String USER_GROUP_COVERFLAG = "coverflag"; // 是否上传过图片
	public static final String USER_GROUP_LABLE = "lable";// 群标签
	public static final String USER_GROUP_CATEGORY = "groupcategory";// 群分类，参考R.array.group_category

	// 本地资源信息表 pool add
	public static final String TABLE_RES = "Resources";
	public static final String RES_CID = "_id"; // 自动增长id
	public static final String RES_ID = "res_id"; // 数据库服务器Id
	public static final String RES_USERID = "res_userid"; // 组创建者
	public static final String RES_CREATETIME = "createtime";// 资源的创建时间
	public static final String RES_PATH = "res_path";// 资源的绝对路径
	public static final String RES_NAME = "res_name";// 资源的名称
	public static final String RES_TYPE = "res_type";// 资源类别
	public static final String RES_GROUP = "res_group";// 资源类别
	public static final String RES_LOCALID = "res_localid";// 系统自带的资源Id
	public static final String RES_ROOT = "res_root";// 资源的真正创建人Id
	public static final String RES_LATLNG = "res_latlng";// 资源经纬度
	public static final String RES_PARENT_ID = "res_parentid";// 父资源ID
	public static final String RES_ISFILE = "res_isfile"; // 文件类型，是否是文件夹。
	public static final String RES_DEAL = "res_deal"; // 资源形式：收藏还是分享。
	public static final String RES_FORMAT = "res_format"; // 资源格式
	public static final String RES_APPRAISEMENT = "res_appraisement"; // 资源评价。
	public static final String RES_FILECOUNT = "res_filecount"; // 文件类型，如果是文件夹就是下面文件的个数，如果是文件就是资源的大小。
	public static final String RES_REMARK = "res_remark"; // 阅读进度

	// 地市信息表
	public static final String TABLE_AREA = "area";
	public static final String AREA_AREAID = "areaid";
	public static final String AREA_AREANAME = "areaName";
	public static final String AREA_AREAFATHERID = "fatherid";

	// 黑名单用户表
	public static final String TABLE_BLACKLIST = "Blacklist";
	public static final String BLACK_ID = "_id";// 自动增长id
	public static final String BLACK_MUSERID = "myuid"; // 添加者userid
	public static final String BLACK_RUSERID = "ruid"; // 被添加者id

	// 单位通讯录表
	public static final String TABLE_NET_CONTACT = "address_book";
	public static final String NET_CONTACT_ID = "_id";//
	public static final String NET_CONTACT_AID = "address_bid";// 通讯录ID标识
	public static final String NET_CONTACT_NAME = "address_name"; // 通讯录名称
	public static final String NET_MY_USERID = "my_userid"; // 本机账号用户id
	public static final String NET_CONTACT_CREATER = "address_creater"; // 创建人姓名
	public static final String NET_CONTACT_CID = "address_cid"; // 创建者Uid、
	public static final String NET_CONTACT_MOBILE = "address_mobile"; // 创建人手机号码
	public static final String NET_CONTACT_CREATETIME = "address_createtime"; // 创建时间
	public static final String NET_CONTACT_NUM = "address_num"; // 人员总数
	public static final String NET_CONTACT_VERSION = "address_version"; // 版本信息（保留）
	public static final String NET_CONTACT_FLAG = "address_flag"; // 0:本人创建，1：本人加入。

	// 单位通讯录成员表
	public static final String TABLE_NET_CONTACT_USER = "address_book_user";
	public static final String NET_CONTACT_USER_ID = "_id";//
	public static final String NET_CONTACT_USER_RID = "user_rid";// 关系Id
	public static final String NET_CONTACT_USER_UID = "user_id"; // 成员UID，为空代表非旁客用户
	public static final String NET_CONTACT_USER_AID = "user_address_id"; // 所在单位通讯录Id
	public static final String NET_CONTACT_USER_MOBILE = "user_mobile"; // 联系号码
	public static final String NET_CONTACT_USER_PHONE1 = "user_phone1"; //
	public static final String NET_CONTACT_USER_PHONE2 = "user_phone2"; //
	public static final String NET_CONTACT_USER_PHONE3 = "user_phone3"; //
	public static final String NET_CONTACT_USER_EMAIL = "user_email"; //
	public static final String NET_CONTACT_USER_ADDRESS = "user_address"; //
	public static final String NET_CONTACT_USER_ZIPCODE = "user_zipcode"; // 邮编
	public static final String NET_CONTACT_USER_DEPARTMENT = "user_department"; // 部门名称
	public static final String NET_CONTACT_USER_RNAME = "user_remarkname"; // 备注姓名

	private static final String SQL_CREATE_AREA_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_AREA + "( " + AREA_AREAID
			+ " INTEGER," + AREA_AREANAME + " VARCHAR(20)," + AREA_AREAFATHERID + " INTEGER)";

	// >>>>>wangxin add
	public static final String TABLE_CONTACTS_PANGKER = "ContactsPangker"; // 表名
	public static final String CONTACTS_PANGKER_ID = "_id";
	public static final String CONTACTS_PANGKER_MYUID = "myuid";
	public static final String CONTACTS_PANGKER_RUID = "ruid";
	public static final String CONTACTS_PANGKER_RELATION = "relation";
	public static final String CONTACTS_PANGKER_PHONENUM = "phonenum";

	// >>>>>wangxin add
	public static final String TABLE_TOUCH_USER = "TouchUser"; // 表名
	public static final String TOUCH_ID = "_id";
	public static final String TOUCH_USER_MYUID = "myuid";
	public static final String TOUCH_USER_TIMES = "times";
	public static final String TOUCH_USER_DAY = "day";

	/*
	 * public static final String TABLE_DOWNLOAD_INFO = "DownLoadInfo"; // 表名
	 * public static final String DOWNLOAD_ID = "_id"; public static final
	 * String FILE_URL = "path"; // 文件网络地址 public static final String LOAD_DONE
	 * = "done"; // 下载完成大小 public static final String LOAD_STATE = "state"; //
	 * 下载状态
	 */
	// 下载管理记录表 Add by zhengjy
	public static final String DOWNLOAD_TABLE = "download_table";
	public static final String DOWN_ID = "_id";
	public static final String DOWN_RES_ID = "resid";
	public static final String THREAD_ID = "thread_id";
	public static final String DOWN_USERID = "down_userid";
	public static final String END_POS = "end_pos";
	public static final String DONE_SIZE = "done_size";
	public static final String DOWNLOAD_URL = "url";
	public static final String FILE_NAME = "file_name";
	public static final String SAVE_FILE_NAME = "save_file_name";
	public static final String SAVE_PATH = "save_path";
	public static final String DOWN_TYPE = "down_type";
	public static final String DOWN_TIME = "down_time";
	public static final String DOWN_STATUS = "down_status";
	public static final String DOWN_NET_TYPE = "down_net_type";
	// 群组表 wubo add
	public static final String TABLE_MEETGROUP = "meetGroup"; // 表名分组表
	public static final String MEETGROUP_ID = "_id"; // 自动增长id
	public static final String MEETGROUP_GROUPID = "groupId"; // 组号
	public static final String MEETGROUP_UID = "uid"; // 登录用户uid
	public static final String MEETGROUP_OWNUID = "ownUid"; // 组创建者
	public static final String MEETGROUP_GROUPSID = "groupSid"; // 组SID标识
	public static final String MEETGROUP_GROUPNAME = "groupName"; // 组名称
	public static final String MEETGROUP_GROUPTYPE = "groupType"; // 组类别：10:会议室，11：直播室，12：群组讨论组，13：友踪组
	public static final String MEETGROUP_GROUPOWN = "groupOwn"; // 拥有者:0:我的群组,1:收藏过的群组,2:参与过的群组

	// 资源上传表add libo
	public static final String TABLE_UPLOADINFO = "uploadinfo"; // 表名分组表
	public static final String UPLOAD_ID = "_id"; // 自动增长id
	public static final String UPLOAD_USERID = "user_id"; // 用户id
	public static final String UPLOAD_RESID = "upload_resid"; // 用户上传到服务器成功之后与后台resId对应
	public static final String UPLOAD_RESTYPE = "res_type"; // 资源类型
	public static final String UPLOAD_PATH = "file_path"; // 资源类型
	public static final String UPLOAD_NAME = "file_name"; // 资源名称
	public static final String UPLOAD_MD5 = "upload_md5"; // md5Value
	public static final String UPLOAD_TIME = "upload_time"; // 上传时间
	public static final String UPLOAD_SHARE = "upload_ifshare"; // 是否分享
	public static final String UPLOAD_FORMAT = "upload_format"; // 资源格式
	public static final String UPLOAD_SIZE = "upload_size"; // 资源长度
	public static final String UPLOAD_STYPE = "upload_stype"; // 资源长度
	public static final String UPLOAD_DIRID = "upload_dir"; // 资源目录
	public static final String UPLOAD_AUTHOR = "upload_author"; // 资源长度
	public static final String UPLOAD_MUSICD = "upload_musicid"; // 资源长度
	public static final String UPLOAD_DES = "upload_desc"; // 资源描述
	public static final String UPLOAD_STATUS = "upload_status"; // 上传状态
	public static final String UPLOAD_EXIST = "upload_exist"; // 上传状态
	public static final String UPLOAD_TYPE = "upload_type"; // 上传网络类型,
	public static final String UPLOAD_LAT = "upload_lat"; // 上传时用的经纬度,
	public static final String UPLOAD_LNG = "upload_lng"; // 上传时用的经纬度,

	// *** 通知消息表add libo
	public static final String TABLE_NOTICEINFO = "noticeinfo"; // 表名分组表
	public static final String NOTICE_ID = "_id"; // 自动增长id
	public static final String NOTICE_USERID = "user_id"; // 用户Id
	public static final String NOTICE_FROM = "from_userid"; // 消息发送人的userid
	public static final String NOTICE_FROMNAME = "from_name"; // 消息发送人的名称
	public static final String NOTICE_TYPE = "notice_type"; // 消息类型
	public static final String NOTICE_MSG_TYPE = "msg_type"; // 消息类型
	public static final String NOTICE_CONTENT = "notice_content"; // 当前消息内容，会更新
	public static final String NOTICE_STATUS = "notice_status"; // 消息状态
	public static final String NOTICE_TIME = "notice_time"; // 消息时间
	public static final String NOTICE_REAMR = "notice_remark"; // 消息备注

	// *** 通知消息表add libo
	public static final String TABLE_TIPINFO = "tipinfo"; // 表名分组表
	public static final String TIP_ID = "_id"; // 自动增长id
	public static final String TIP_USERID = "tip_user_id"; // 用户Id
	public static final String TIP_FROM = "tip_from_userid"; // 消息发送人的userid
	public static final String TIP_FROMNAME = "tip_from_name"; // 消息发送人的名称
	public static final String TIP_TYPE = "tip_type"; // 消息类型
	public static final String TIP_RESID = "tip_res_id"; // 资源Id
	public static final String TIP_RESTYPE = "tip_res_type"; // 资源类型
	public static final String TIP_RESNAME = "tip_res_name"; // 资源名称
	public static final String TIP_CONTENT = "tip_content"; // 消息内容
	public static final String TIP_TIME = "tip_time"; // 消息时间

	// 权限表
	public static final String TABLE_GRANT = "grant";// 权限表
	public static final String GRANT_ID = "_id";// id
	public static final String GRANT_UID = "uid";// uid
	public static final String GRANT_NAME = "name";// 权限名
	public static final String GRANT_VALUE = "value";// 权限值
	public static final String GRANT_GID = "gid";// 分组权限
	public static final String GRANT_UIDS = "guid";// 联系人权限

	// 资源推荐表add libo
	public static final String TABLE_RECOMMEND = "recommend";//
	public static final String RECOMMEND_ID = "recommend_id";// d
	public static final String RECOMMEND_USERID = "recommend_userid";//
	public static final String RECOMMEND_FROMID = "recommend_fromid";//
	public static final String RECOMMEND_FROMNAME = "recommend_fromname";//
	public static final String RECOMMEND_STATUS = "recommend_status";//
	public static final String RECOMMEND_TYPE = "recommend_type";//
	public static final String RECOMMEND_SID = "recommend_sid";//
	public static final String RECOMMEND_SNAME = "recommend_sname";//
	public static final String RECOMMEND_TIME = "recommend_time";//
	public static final String RECOMMEND_REASON = "recommend_reason";//
	public static final String RECOMMEND_SINGER = "recommend_singer";//
	public static final String RECOMMEND_REMARK = "recommend_remark";//

	// 消息接收表add libo
	public static final String TABLE_MSGINFO = "MsgInfos";// 信息表
	public static final String MSG_ID = "msg_id";// 自动增长id
	public static final String MSG_USERID = "msg_userid";// 用户Id
	public static final String MSG_FROMID = "msg_fromid";// 发送消息的用户Id
	public static final String MSG_THEME = "msg_theme";// 消息标题
	public static final String MSG_CONTENT = "msg_content";// 消息内容
	public static final String MSG_TIME = "msg_time";// 消息时间
	public static final String MSG_TYPE = "msg_type";// 消息类别，动态可变
	public static final String MSG_RES_TYPE = "msg_res_type";// 消息类别，动态可变
	public static final String MSG_UNCOUNT = "msg_uncount";// 消息未读条数

	// 旁客呼叫号码表（曾经设置过）
	public static final String TABLE_CALLPHONE = "TableCallphone"; // 表名
	public static final String CALLPHONE_ID = "_id";
	public static final String CALLPHONE_NUM = "callphoneNum"; // 设置过的号码
	public static final String CALLPHONE_USERID = "cUserid"; // 设置用户Id

	// 发出推荐资源，被推荐人阅读状态列表
	public static final String TABLE_USERSTATUS = "UserStatus"; // 表名
	public static final String USERSTATUS_ID = "_id";
	public static final String USERSTATUS_USERID = "u_userid"; // 用户ID
	public static final String USERSTATUS_RESID = "u_resid"; // 推荐资源ID
	public static final String USERSTATUS_TIME = "u_time"; // 推荐时间
	public static final String USERSTATUS_BE_USERID = "be_userid"; // 被推荐人用户ID
	public static final String USERSTATUS_STATUS = "u_status"; // 阅读状态
	public static final String USERSTATUS_USERNAME = "u_username"; // 推荐时间接收者

	// >>>>>>>>>>亲友数据保存
	public static final String TABLE_INTIMATE = "Intimate"; // 表名
	// public static final String INTIMATE_ID = "_id";
	public static final String INTIMATE_USERID = "userid"; // 用户ID
	public static final String INTIMATE_RUSERID = "ruserid"; // 用户ID
	public static final String INTIMATE_RESID = "resid"; // 资源ID
	public static final String INTIMATE_USERNAME = "username"; // 资源ID
	public static final String INTIMATE_RESNAME = "res_name"; // 资源名称
	public static final String INTIMATE_RESDESC = "res_desc"; // 资源描述
	public static final String INTIMATE_TYPE = "res_type"; // 资源类型
	public static final String INTIMATE_RESSOURCE = "res_source"; // 资源来源
	public static final String INTIMATE_LOCATION_LAT = "lat"; // 经纬度
	public static final String INTIMATE_LOCATION_LON = "lon"; // 经纬度
	public static final String INTIMATE_DEALTIME = "dealtime"; // 资源处理时间

	/**
	 * 地址位置信息表
	 */

	// >>>>>>>>>>亲友数据保存
	public static final String TABLE_DETAILEDADDRESS = "detailedaddress"; // 表名
	public static final String DETAILEDADDRESS_ID = "_id";
	public static final String DETAILEDADDRESS_ADDRESSSTREETZIP = "addressStreetZip"; //
	public static final String DETAILEDADDRESS_ADDRESSSTREET = "addressStreet"; //
	public static final String DETAILEDADDRESS_STREET = "street"; //
	public static final String DETAILEDADDRESS_AREA = "area"; //
	public static final String DETAILEDADDRESS_CITY = "city"; //
	public static final String DETAILEDADDRESS_COUNTRY = "country"; //
	public static final String DETAILEDADDRESS_PROVINCE = "province";
	/**
	 * >>>>>>亲友数据来源
	 * 
	 */
	public final String SQL_DETAILEDADDRESS = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILEDADDRESS + "("
			+ DETAILEDADDRESS_ID + " VARCHAR(50), "//
			+ DETAILEDADDRESS_ADDRESSSTREETZIP + " VARCHAR(100),"//
			+ DETAILEDADDRESS_ADDRESSSTREET + " VARCHAR(100), "//
			+ DETAILEDADDRESS_STREET + " VARCHAR(20), "//
			+ DETAILEDADDRESS_AREA + " VARCHAR(20), "//
			+ DETAILEDADDRESS_CITY + " VARCHAR(20), "//
			+ DETAILEDADDRESS_COUNTRY + " VARCHAR(20), "//
			+ DETAILEDADDRESS_PROVINCE + " VARCHAR(20), "//
			+ REMARK1 + " INTEGER," //
			+ REMARK2 + " INTEGER," //
			+ REMARK3 + " VARCHAR(50),"//
			+ REMARK4 + " VARCHAR(50)," //
			+ REMARK5 + " VARCHAR(20))";

	/**
	 * >>>>>>亲友数据来源
	 * 
	 */
	public final String SQL_INTIMATE = "CREATE TABLE IF NOT EXISTS " + TABLE_INTIMATE + "(" + INTIMATE_USERID
			+ " VARCHAR(20), " + INTIMATE_RUSERID + " VARCHAR(20)," + INTIMATE_RESID + " BIGINT, " + INTIMATE_RESNAME
			+ " VARCHAR(40), " + INTIMATE_USERNAME + " VARCHAR(40), " + INTIMATE_RESDESC + " TEXT, " + INTIMATE_TYPE
			+ " INTEGER, " + INTIMATE_RESSOURCE + " INTEGER," + INTIMATE_LOCATION_LAT + " REAL,"
			+ INTIMATE_LOCATION_LON + " REAL," + INTIMATE_DEALTIME + " DATETIME, " + REMARK1 + " INTEGER," + REMARK2
			+ " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)"
			+ ",PRIMARY KEY (" + INTIMATE_USERID + "," + INTIMATE_RUSERID + "))";

	/**
	 * ========================================================================
	 * ================================================
	 */
	public final String SQL_DOWNLOAD = "CREATE TABLE IF NOT EXISTS " + DOWNLOAD_TABLE + "(" + DOWN_ID
			+ " integer PRIMARY KEY AUTOINCREMENT," + THREAD_ID + " INTEGER, " + DOWN_USERID + " VARCHAR(20),"
			+ END_POS + " INTEGER, " + DONE_SIZE + " INTEGER, " + DOWNLOAD_URL + " VARCHAR(100), " + SAVE_PATH
			+ " VARCHAR(100), " + FILE_NAME + " VARCHAR(50)," + SAVE_FILE_NAME + " VARCHAR(50)," + DOWN_TYPE
			+ " INTEGER," + DOWN_TIME + " VARCHAR(20)," + DOWN_RES_ID + " INTEGER," + DOWN_NET_TYPE + " INTEGER,"
			+ DOWN_STATUS + " INTEGER," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50),"
			+ REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// sql语句-创建联系人表
	private static String SQL_CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_USERS + " (" + _ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + USER_Contacteid + " INTEGER," + USER_SYNID + " INTEGER,"
			+ USER_USERNAME + " VARCHAR(60)," + USER_REALNAME + " VARCHAR(60)," + USER_ID + " VARCHAR(60),"
			+ USER_NICKNAME + " VARCHAR(60)," + USER_PORTARIT + " BLOB," + USER_NET_PORTARIT + " BLOB,"
			+ USER_PORTARITTYPE + " INTEGER," + USER_SIGNATURE + " TEXT," + USER_ADDR + " VARCHAR(60)," + USER_COMPANY
			+ " VARCHAR(60)," + USER_ORG + " VARCHAR(60)," + USER_EMAIL + " VARCHAR(60)," + USER_STATUS
			+ " VARCHAR(60)," + USER_HEADlETTER + " VARCHAR(60)," + USER_VERSION + " VARCHAR(60)," + USER_FRIEND
			+ " INTEGER," + USER_SEX
			+ " INTEGER," // add wangxin
			+ USER_BIRTHDAY
			+ " VARCHAR(20)," // add wangxin
			+ USER_IDENTITY
			+ " INTEGER," // add wangxin
			+ USER_TIME + " TIMESTAMP," + USER_CALLPHONE + " VARCHAR(15)," + REMARK1 + " INTEGER," + REMARK2
			+ " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// sql语句-创建联系人的版本表

	// + VERSION_Contacteid
	// + " INTEGER REFERENCES " + TABLE_USERS + "("+ USER_Contacteid+ ")"
	// +" NOT NULL "
	private static String SQL_CREATE_VERSION_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_VERSION + " (" + VERSION_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + VERSION_VERSION + " VARCHAR(60)," + VERSION_Contacteid
			+ " INTEGER," + VERSION_STATUS + " VARCHAR(60)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3
			+ " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// sql语句-创建联系人的电话号码表
	private static String SQL_CREATE_PHONE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_PHONE + " (" + PHONE_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + PHONE_Contacteid + " INTEGER," + PHONE_SYNID + " INTEGER,"
			+ PHONE_TYPE + " INTEGER," + PHONE_FRIEND + " INTEGER," + PHONE_NUM + " VARCHAR(60)," + REMARK1
			+ " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5
			+ " VARCHAR(20)" + ")";

	// sql语句-创建聊天记录表
	private static String SQL_CREATE_MESRECORD_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_MESRECORD + " ("
			+ MESSAGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + MESSAGE_PORTRAIT + " BLOB," + MESSAGE_NAME
			+ " VARCHAR(60)," + MESSAGE_SENDER + " VARCHAR(60)," + MESSAGE_RECEIVER + " VARCHAR(60)," + MESSAGE_CONTENT
			+ " TEXT," + MESSAGE_TIME + " TIMESTAMP," + MESSAGE_CHATNUM + " VARCHAR(60)," + MESSAGE_STATUS
			+ " VARCHAR(60)," + MESSAGE_FROM + " INTEGER," + MESSAGE_TYPE + " VARCHAR(60)," + REMARK1 + " INTEGER,"
			+ REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)"
			+ ")";

	// sql语句-创建聊天室表
	private static String SQL_CREATE_CHATROOM_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_CHATROOM + " (" + CHAT_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + CHAT_PORTRAIT + " BLOB," + CHAT_NAME + " VARCHAR(60),"
			+ CHAT_MENBERSNUM + " VARCHAR(60)," + CHAT_NUM + " VARCHAR(60)," + CHAT_ISRECEIVERSMS + " VARCHAR(60),"
			+ CHAT_OWNER + " VARCHAR(60)," + CHAT_MAXCOUNT + " VARCHAR(60)," + CHAT_TIME + " TIMESTAMP," + CHAT_TYPE
			+ " VARCHAR(60)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4
			+ " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// sql语句-创建私信聊天记录
	private static String SQL_CREATE_USERMSG_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_USERMSG + " ( " + USERMSG_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + USERMSG_MYUID + " VARCHAR(60), " + USERMSG_HIMUID
			+ " VARCHAR(60), " + USERMSG_DIRECTION + " VARCHAR(60), " + USERMSG_CONTENT + " VARCHAR(60), "
			+ USERMSG_FILEPATH + " VARCHAR(60), " + USERMSG_STATUS + " INTEGER, " + USERMSG_RECV
			+ " INTEGER DEFAULT 0, " + USERMSG_TYPE + " INTEGER, " + USERMSG_TALKTIME + " TIMESTAMP," + REMARK1
			+ " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5
			+ " VARCHAR(20)" + ")";

	// sql语句-创建粉丝联系人表
	private static String SQL_CREATE_FANS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_FANS + " (" + _ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + FANS_MYUID + " INTEGER," + FANS_FANUID + " INTEGER,"
			+ FANS_SOURCE + " VARCHAR(10)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50),"
			+ REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// sql语句-创建好友联系人表
	private static String SQL_CREATE_FRIENDS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_FRIENDS + " (" + _ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + FRIENDS_MYUID + " INTEGER," + FRIENDS_FRIENDUID + " INTEGER,"
			+ FRIENDS_RELATIONTYPE + " INTEGER," + FRIENDS_GROUPID + " INTEGER," + FANS_SOURCE + " VARCHAR(10),"
			+ FRIENDS_REMARKNAME + " VARCHAR(10)," + TRACE_LAT + " VARCHAR(10)," + TRACE_LNG + " VARCHAR(10),"
			+ TRACE_TIME + " VARCHAR(20)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50),"
			+ REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// sql语句-创建关注人联系人表
	private static String SQL_CREATE_ATTENTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ATTENTS + " (" + _ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + ATTENTS_MYUID + " INTEGER," + ATTENTS_ATTENTUID + " INTEGER,"
			+ ATTENTS_GROUPID + " INTEGER," + ATTENTS_SOURCE + " VARCHAR(10)," + ATTENTS_TYPE + " INTEGER," + REMARK1
			+ " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5
			+ " VARCHAR(20)" + ")";

	// sql语句-创建友踪联系人
	private static String SQL_CREATE_TRACE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TRACE + " (" + _ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + TRACE_MYUID + " INTEGER," + TRACE_RUID + " INTEGER,"
			+ TRACE_GROUPID + " INTEGER," + TRACE_SOURCE + " VARCHAR(10)," + REMARK1 + " INTEGER," + REMARK2
			+ " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// sql语句-创建联系人群组表 by wangxin
	private static String SQL_CREATE_CONTACTS_GROUP_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_CONTACTS_GROUP + " ("
			+ GROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + GROUP_GROUPID + " INTEGER," + GROUP_MYUID
			+ " INTEGER," + GROUP_TYPE + " INTEGER," + GROUP_CREATER + " INTEGER," + GROUP_MEMBER_COUNT + " INTEGER,"
			+ GROUP_GROUPNAME + " VARCHAR(10)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3
			+ " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// sql语句-创建联系人群组表 by wangxin
	private static String SQL_CREATE_USER_GROUP_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_USER_GROUP + " ("
			+ USER_GROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + USER_GROUP_GROUPID + " INTEGER,"
			+ USER_GROUP_GROUPSID + " INTEGER," + USER_GROUP_MYUID + " INTEGER," + USER_GROUP_TYPE + " INTEGER,"
			+ USER_GROUP_LOTYPE + " INTEGER," + USER_GROUP_CREATER + " INTEGER," + USER_GROUP_GROUPNAME
			+ " VARCHAR(10)," + USER_GROUP_CREATETIME + " VARCHAR(10)," + USER_GROUP_FLAG + " INTEGER,"
			+ USER_GROUP_COVERFLAG + " INTEGER," + USER_GROUP_LABLE + " VARCHAR(20)," + USER_GROUP_LASTVISITTIME
			+ " VARCHAR(10)," + USER_GROUP_CATEGORY + " INTEGER," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER,"
			+ REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// sql语句-创建资源表 by pool
	private static String SQL_CREATE_RES_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_RES + " (" + RES_CID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + RES_ID + " VARCHAR(10), " + RES_USERID + " VARCHAR(20), "
			+ RES_TYPE + " INTEGER, " + RES_REMARK + " INTEGER, " + RES_DEAL + " INTEGER, " + RES_LOCALID
			+ " INTEGER, " + RES_ROOT + " INTEGER, " + RES_CREATETIME + " DATETIME, " + RES_PATH + " VARCHAR(100), "
			+ RES_APPRAISEMENT + " VARCHAR(100), " + RES_NAME + " VARCHAR(10), " + RES_GROUP + " VARCHAR(10), "
			+ RES_ISFILE + " INTEGER, " + RES_FORMAT + " VARCHAR(5), " + RES_LATLNG + " VARCHAR(10), " + RES_FILECOUNT
			+ " INTEGER, " + RES_PARENT_ID + " VARCHAR(10)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3
			+ " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	/**
	 * Add by wubo 会议室聊天记录
	 */
	private static String SQL_CREATE_TABLE_MEETROOM = "CREATE TABLE IF NOT EXISTS " + TABLE_MEETROOM + " ( "
			+ MEETROOM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + MEETROOM_GID + " VARCHAR(60), " + MEETROOM_UID
			+ " VARCHAR(60), " + MEETROOM_NICKNAME + " VARCHAR(60), " + MEETROOM_CONTENT + " VARCHAR(60), "
			+ MEETROOM_MSGID + " INTEGER, " + MEETROOM_MSGTYPE + " INTEGER, " + MEETROOM_STATUS + " INTEGER, "
			+ MEETROOM_MYUID + " VARCHAR(60), " + MEETROOM_TALKTIME + " TIMESTAMP," + REMARK1 + " INTEGER," + REMARK2
			+ " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// >>.wangxin
	private static String SQL_CREATE_TABLE_CONTACTS_PANGKER = "CREATE TABLE IF NOT EXISTS " + TABLE_CONTACTS_PANGKER
			+ " ( " + CONTACTS_PANGKER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CONTACTS_PANGKER_MYUID
			+ " VARCHAR(20), " + CONTACTS_PANGKER_RUID + " VARCHAR(20), " + CONTACTS_PANGKER_RELATION + " INTEGER, "
			+ CONTACTS_PANGKER_PHONENUM + " VARCHAR(20)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3
			+ " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	private static final String SQL_CREATE_TABLE_DOWNLOAD_INFO = "CREATE TABLE info(path VARCHAR(1024), "
			+ "thid INTEGER, done INTEGER,fileName VARCHAR(50),state INTEGER, updown INTEGER,"
			+ "remark1 INTEGER, remark2 INTEGER, remark3 VARCHAR(50), remark4 VARCHAR(50), remark5 VARCHAR(20),"
			+ "PRIMARY KEY(path, thid))";

	// sql语句-创建群组表 by wubo
	private static String SQL_CREATE_MEETGROUP_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_MEETGROUP + " ("
			+ MEETGROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + MEETGROUP_GROUPID + " INTEGER," + MEETGROUP_UID
			+ " varchar(20)," + MEETGROUP_OWNUID + " INTEGER," + MEETGROUP_GROUPSID + " INTEGER," + MEETGROUP_GROUPNAME
			+ " varchar(20)," + MEETGROUP_GROUPTYPE + " INTEGER," + MEETGROUP_GROUPOWN + " INTEGER," + REMARK1
			+ " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5
			+ " VARCHAR(20)" + ")";

	// 上传资源信息表SQL --->add libo
	private static String SQL_CREATE_UPLOAD_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_UPLOADINFO + " (" + UPLOAD_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + UPLOAD_USERID + " VARCHAR(20)," + UPLOAD_RESTYPE + " INTEGER,"
			+ UPLOAD_PATH + " VARCHAR(50)," + UPLOAD_RESID + " VARCHAR(12)," + UPLOAD_NAME + " VARCHAR(20),"
			+ UPLOAD_MD5 + " VARCHAR(30)," + UPLOAD_TIME + " VARCHAR(20)," + UPLOAD_SHARE + " VARCHAR(1),"
			+ UPLOAD_SIZE + " INTEGER," + UPLOAD_STYPE + " VARCHAR(2)," + UPLOAD_FORMAT + " VARCHAR(5)," + UPLOAD_DIRID
			+ " VARCHAR(10)," + UPLOAD_AUTHOR + " VARCHAR(20)," + UPLOAD_MUSICD + " INTEGER," + UPLOAD_EXIST
			+ " INTEGER," + UPLOAD_LAT + " VARCHAR(10)," + UPLOAD_LNG + " VARCHAR(10)," + UPLOAD_TYPE + " INTEGER,"
			+ UPLOAD_DES + " VARCHAR(100)," + UPLOAD_STATUS + " INTEGER," + REMARK1 + " INTEGER," + REMARK2
			+ " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// 消息接收表SQL--->add libo
	private static String SQL_CREATE_NOTICE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTICEINFO + " (" + NOTICE_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + NOTICE_USERID + " VARCHAR(20)," + NOTICE_FROM + " VARCHAR(20),"
			+ NOTICE_FROMNAME + " VARCHAR(30)," + NOTICE_CONTENT + " VARCHAR(200)," + NOTICE_STATUS + " INTEGER,"
			+ NOTICE_TYPE + " INTEGER," + NOTICE_MSG_TYPE + " INTEGER," + NOTICE_TIME + " DATETIME," + NOTICE_REAMR
			+ " VARCHAR(400)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4
			+ " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	// 消息提示接收表SQL--->add libo
	private static String SQL_CREATE_TIP_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TIPINFO + " (" + TIP_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + TIP_USERID + " VARCHAR(20)," + TIP_FROM + " VARCHAR(20),"
			+ TIP_FROMNAME + " VARCHAR(30)," + TIP_CONTENT + " VARCHAR(200)," + TIP_TIME + " DATETIME," + TIP_RESID
			+ " VARCHAR(20)," + TIP_RESTYPE + " INTEGER," + TIP_TYPE + " INTEGER," + TIP_RESNAME + " VARCHAR(40),"
			+ REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50),"
			+ REMARK5 + " VARCHAR(20)" + ")";
	/**
	 * 创建权限表
	 */
	private static String SQL_CREATE_GRANT_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_GRANT + " (" + GRANT_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + GRANT_NAME + " VARCHAR(20)," + GRANT_UID + " VARCHAR(20), "
			+ GRANT_GID + " VARCHAR(5000), " + GRANT_UIDS + " VARCHAR(5000), " + GRANT_VALUE + " Integer," + REMARK1
			+ " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5
			+ " VARCHAR(20)" + ")";
	/**
	 * 黑名单用户表
	 */
	private static final String SQL_CREATE_BLACKLIST_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_BLACKLIST + " ("
			+ BLACK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + BLACK_MUSERID + " VARCHAR(60)," + BLACK_RUSERID
			+ " VARCHAR(60)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4
			+ " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	/**
	 * 创建单位通讯录表
	 */
	private static String SQL_CREATE_TABLE_NET_CONTACT = "CREATE TABLE IF NOT EXISTS " + TABLE_NET_CONTACT + " ("
			+ NET_CONTACT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + NET_CONTACT_AID + " INTEGER," + NET_CONTACT_NAME
			+ " VARCHAR(10)," + NET_MY_USERID + " VARCHAR(20)," + NET_CONTACT_CREATER + " VARCHAR(10),"
			+ NET_CONTACT_CID + " INTEGER," + NET_CONTACT_MOBILE + " VARCHAR(10)," + NET_CONTACT_CREATETIME
			+ " VARCHAR(20)," + NET_CONTACT_NUM + " INTEGER," + NET_CONTACT_VERSION + " VARCHAR(10),"
			+ NET_CONTACT_FLAG + " INTEGER," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3
			+ " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	/**
	 * 创建单位通讯录成员表
	 */
	private static String SQL_CREATE_TABLE_NET_CONTACT_USER = "CREATE TABLE IF NOT EXISTS " + TABLE_NET_CONTACT_USER
			+ " (" + NET_CONTACT_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + NET_CONTACT_USER_RID + " INTEGER,"
			+ NET_CONTACT_USER_UID + " INTEGER," + NET_CONTACT_USER_AID + " INTEGER," + NET_CONTACT_USER_MOBILE
			+ " VARCHAR(15)," + NET_CONTACT_USER_PHONE1 + " VARCHAR(15)," + NET_CONTACT_USER_PHONE2 + " VARCHAR(15),"
			+ NET_CONTACT_USER_PHONE3 + " VARCHAR(15)," + NET_CONTACT_USER_EMAIL + " VARCHAR(50),"
			+ NET_CONTACT_USER_ADDRESS + " VARCHAR(50)," + NET_CONTACT_USER_ZIPCODE + " VARCHAR(7),"
			+ NET_CONTACT_USER_DEPARTMENT + " VARCHAR(100)," + NET_CONTACT_USER_RNAME + " VARCHAR(10)," + REMARK1
			+ " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5
			+ " VARCHAR(20)" + ")";

	/**
	 * sql 资源推荐表
	 */
	private static final String SQL_CREATE_TABLE_RECOMMEND = "CREATE TABLE IF NOT EXISTS " + TABLE_RECOMMEND + " ("
			+ RECOMMEND_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + RECOMMEND_USERID + " VARCHAR(20),"
			+ RECOMMEND_FROMID + " VARCHAR(20)," + RECOMMEND_FROMNAME + " VARCHAR(30)," + RECOMMEND_STATUS
			+ " INTEGER," + RECOMMEND_TYPE + " INTEGER," + RECOMMEND_SID + " VARCHAR(10)," + RECOMMEND_SNAME
			+ " VARCHAR(50)," + RECOMMEND_REASON + " VARCHAR(50)," + RECOMMEND_SINGER + " VARCHAR(50),"
			+ RECOMMEND_TIME + " DATETIME," + RECOMMEND_REMARK + " VARCHAR(10)," + REMARK1 + " INTEGER," + REMARK2
			+ " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	/**
	 * sql 信息接收表
	 */
	private static final String SQL_CREATE_TABLE_MSGINFO = "CREATE TABLE IF NOT EXISTS " + TABLE_MSGINFO + " ("
			+ MSG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + MSG_USERID + " VARCHAR(15)," + MSG_FROMID
			+ " VARCHAR(15)," + MSG_THEME + " VARCHAR(40), " + MSG_CONTENT + " VARCHAR(60)," + MSG_TYPE + " INTEGER,"
			+ MSG_RES_TYPE + " INTEGER," + MSG_UNCOUNT + " INTEGER," + MSG_TIME + " TIMESTAMP," + REMARK1 + " INTEGER,"
			+ REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)"
			+ ")";

	private static String SQL_CREATE_TOUCH_USER_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TOUCH_USER + " ("
			+ TOUCH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TOUCH_USER_MYUID + " VARCHAR(20)," + TOUCH_USER_TIMES
			+ " INTEGER," + TOUCH_USER_DAY + " VARCHAR(60)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3
			+ " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	public final String SQL_TABLE_CALLPHONE = "CREATE TABLE IF NOT EXISTS " + TABLE_CALLPHONE + "(" + CALLPHONE_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + CALLPHONE_NUM + " VARCHAR(20)," + CALLPHONE_USERID
			+ " VARCHAR(50)," + REMARK1 + " INTEGER," + REMARK2 + " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4
			+ " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	public final String SQL_TABLE_USERSTATUS = "CREATE TABLE IF NOT EXISTS " + TABLE_USERSTATUS + "(" + USERSTATUS_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + USERSTATUS_USERID + " VARCHAR(20)," + USERSTATUS_RESID
			+ " VARCHAR(50)," + USERSTATUS_TIME + " VARCHAR(30)," + USERSTATUS_BE_USERID + " VARCHAR(30),"
			+ USERSTATUS_STATUS + " INTEGER," + USERSTATUS_USERNAME + " VARCHAR(30)," + REMARK1 + " INTEGER," + REMARK2
			+ " INTEGER," + REMARK3 + " VARCHAR(50)," + REMARK4 + " VARCHAR(50)," + REMARK5 + " VARCHAR(20)" + ")";

	private Context mContext = null;
	public SQLiteDatabase mSQLiteDatabase = null;
	public DatabaseHelper mDatabaseHelper = null;

	public MyDataBaseAdapter(Context context) {
		mContext = context;
	}

	private class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(SQL_CREATE_USERS_TABLE);
			db.execSQL(SQL_CREATE_PHONE_TABLE);
			db.execSQL(SQL_CREATE_MESRECORD_TABLE);
			db.execSQL(SQL_CREATE_CHATROOM_TABLE);
			db.execSQL(SQL_CREATE_VERSION_TABLE);
			db.execSQL(SQL_CREATE_USERMSG_TABLE);
			db.execSQL(SQL_CREATE_FANS_TABLE);

			db.execSQL(SQL_CREATE_CONTACTS_GROUP_TABLE); // ADD BY WANGXIN
			db.execSQL(SQL_CREATE_FRIENDS_TABLE); // ADD BY WANGXIN
			db.execSQL(SQL_CREATE_ATTENTS_TABLE); // ADD BY WANGXIN
			db.execSQL(SQL_CREATE_TRACE_TABLE); // ADD BY WANGXIN
			db.execSQL(SQL_CREATE_RES_TABLE); // ADD BY POOL
			db.execSQL(SQL_CREATE_USER_GROUP_TABLE); // ADD BY WANGXIN

			db.execSQL(SQL_CREATE_AREA_TABLE); // ADD BY WANGXIN
			db.execSQL(SQL_CREATE_TABLE_CONTACTS_PANGKER);// <<<<wangxin
			initAreaData(db);

			db.execSQL(SQL_CREATE_TABLE_MEETROOM);// Add by wubo

			db.execSQL(SQL_CREATE_TABLE_DOWNLOAD_INFO); // Add by zhengjy
			db.execSQL(SQL_DOWNLOAD);
			db.execSQL(SQL_CREATE_MEETGROUP_TABLE);// add by wubo
			db.execSQL(SQL_CREATE_UPLOAD_TABLE);// add libo

			db.execSQL(SQL_CREATE_NOTICE_TABLE);// add libo
			db.execSQL(SQL_CREATE_TIP_TABLE);// add libo
			db.execSQL(SQL_CREATE_GRANT_TABLE);// add wubo
			db.execSQL(SQL_CREATE_BLACKLIST_TABLE);// add by zhengjy
			db.execSQL(SQL_CREATE_TABLE_NET_CONTACT);// add by zhengjy
			db.execSQL(SQL_CREATE_TABLE_RECOMMEND);// add by libo
			db.execSQL(SQL_CREATE_TABLE_NET_CONTACT_USER);// add by zhengjy
			db.execSQL(SQL_CREATE_TABLE_MSGINFO);// add by libo

			db.execSQL(SQL_CREATE_TOUCH_USER_TABLE);// add by

			db.execSQL(SQL_TABLE_CALLPHONE);//
			db.execSQL(SQL_TABLE_USERSTATUS);//
			db.execSQL(SQL_INTIMATE);
			db.execSQL(SQL_DETAILEDADDRESS);

		}

		private void initAreaData(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			for (String areaSql : pangkerArea.areasSql) {
				db.execSQL(areaSql);
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			upgradeDataBase(db, oldVersion, newVersion);
		}
	}

	/**
	 * 数据库的升级，既要修改表结构，又要保持里面的数据，同时要注意版本
	 * 
	 * @param db
	 * @param oldVersion
	 * @param newVersion
	 *            只需要从最低的版本升级到最高，例如最低版本5，最新版本7,两种升级方案：5——7；6——7；
	 */
	public void upgradeDataBase(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d("DB_VERSION", "Version:" + oldVersion + "," + newVersion);
		// 数据库不升级
		if (newVersion <= oldVersion) {
			return;
		}
		// 下面是每一张需要修改表的升级,每一张表的升级如下：需要在一个事务中进行
		// 在对数据库运行ADD COLUMN之后，该数据库将无法由SQLite 3.1.3及更早版本读取，除非运行VACUUM命令
		db.beginTransaction();// 开始事务
		try {
			// 1. 将表名改为临时表[ALTER TABLE Subscription RENAME TO
			// __temp__Subscription;]
			// 2. 创建新表[CREATE TABLE Subscription (OrderId VARCHAR(32) PRIMARY
			// KEY ,UserName VARCHAR(32) NOT NULL ,ProductId VARCHAR(16) NOT
			// NULL);]
			// 3. 导入数据[INSERT INTO Subscription SELECT OrderId, “”, ProductId
			// FROM __temp__Subscription;]　
			// 4. 删除临时表[DROP TABLE __temp__Subscription;]　
			db.setTransactionSuccessful();// 调用此方法会在执行到endTransaction()
											// 时提交当前事务，如果不调用此方法会回滚事务
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			db.endTransaction();// 由事务的标志决定是提交事务，还是回滚事务
			db.close();
		}
	}

	public void clearuser() {
		mSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
		mSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PHONE);
		mSQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_VERSION);
		mSQLiteDatabase.execSQL(SQL_CREATE_USERS_TABLE);
		mSQLiteDatabase.execSQL(SQL_CREATE_PHONE_TABLE);
		mSQLiteDatabase.execSQL(SQL_CREATE_VERSION_TABLE);
	}

	// 打开数据库
	public void open() {
		try {
			mDatabaseHelper = new DatabaseHelper(mContext);
			if (mSQLiteDatabase == null || !mSQLiteDatabase.isOpen()) {
				mSQLiteDatabase = mDatabaseHelper.getWritableDatabase();
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		}
	}

	// 关闭数据库
	public void close() {
		if (mDatabaseHelper != null) {
			mDatabaseHelper.close();
			mDatabaseHelper = null;
		}
		if (mSQLiteDatabase != null) {
			mSQLiteDatabase.close();
			mSQLiteDatabase = null;
		}
	}

	// 插入一条数据
	public long insertData(String table, ContentValues initialValues) {
		return mSQLiteDatabase.insert(table, "_id", initialValues);
	}// 插入一条数据

	public long replaceData(String table, ContentValues initialValues) {
		return mSQLiteDatabase.replace(table, null, initialValues);
	}

	// 删除一条数据
	public boolean deleteData(String table, String key_id, long id) {
		return mSQLiteDatabase.delete(table, key_id + "=" + id, null) > 0;
	}

	public boolean deleteData(String table) {

		return mSQLiteDatabase.delete(table, null, null) > 0;
	}

	public boolean deleteData(String table, String key, String value) {
		if (key != null && value != null) {
			return mSQLiteDatabase.delete(table, key + "='" + value + "'", null) > 0;
		}
		return mSQLiteDatabase.delete(table, null, null) > 0;
	}

	public boolean deleteData(String table, String[] key, String[] value) {
		if (key != null && value != null) {
			StringBuffer valString = new StringBuffer();
			for (int i = 0; i < key.length; i++) {
				if (i == key.length - 1) {
					valString.append(key[i]).append("='").append(value[i]).append("'");
				} else {
					valString.append(key[i]).append("='").append(value[i]).append("' and ");
				}
			}
			return mSQLiteDatabase.delete(table, valString.toString(), null) > 0;
		}
		return mSQLiteDatabase.delete(table, null, null) > 0;
	}

	// 删除一条联系人
	public boolean deleteUser(long id) {
		mSQLiteDatabase.beginTransaction();
		try {
			deleteData(TABLE_PHONE, PHONE_SYNID, id);
			Boolean flag = deleteData(TABLE_USERS, USER_SYNID, id);
			mSQLiteDatabase.setTransactionSuccessful();
			return flag;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			mSQLiteDatabase.endTransaction();
		}
	}

	// 删除一条联系人
	public boolean deleteGroup(String table, String key, String value) {
		mSQLiteDatabase.beginTransaction();
		try {
			Boolean flag = deleteData(table, key, value);
			mSQLiteDatabase.setTransactionSuccessful();
			return flag;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			mSQLiteDatabase.endTransaction();
		}
	}

	// 删除聊天室+记录
	public boolean deleteChat(String chatTable, String chatKey, String messageTable, String messageKey, String charNum) {
		mSQLiteDatabase.beginTransaction();
		try {
			deleteData(messageTable, messageKey, charNum);
			Boolean flag = deleteData(chatTable, chatKey, charNum);
			mSQLiteDatabase.setTransactionSuccessful();
			return flag;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			mSQLiteDatabase.endTransaction();
		}
	}

	// 更新一条数据
	public boolean updateData(String table, String key_id, long rowId, ContentValues args) {
		return mSQLiteDatabase.update(table, args, key_id + "=" + rowId, null) > 0;
	}

	public boolean updateData(String updateSQL) {
		try {
			mSQLiteDatabase.execSQL(updateSQL);
			return true;
		} catch (SQLException e) {
			logger.error(TAG, e);
			return false;
		}
	}

	public boolean updateData(String table, String key, String value, String wherekey, long wherevalue) {
		try {
			String updateSQL = "update " + table + " set " + key + "='" + value + "' where " + wherekey + " = "
					+ wherevalue;
			Log.i("MyDataBaseAdapter-->updateData()", "updateSQL=" + updateSQL);
			mSQLiteDatabase.execSQL(updateSQL);
		} catch (SQLException e) {
			logger.error(TAG, e);
			return false;
		}
		return true;
	}

	public boolean updateData(String table, String key, String value, String wherekey, String wherevalue) {
		try {
			String updateSQL = "update " + table + " set " + key + "='" + value + "' where " + wherekey + " = "
					+ wherevalue;
			Log.i("MyDataBaseAdapter-->updateData()", "updateSQL=" + updateSQL);
			mSQLiteDatabase.execSQL(updateSQL);
		} catch (SQLException e) {
			logger.error(TAG, e);
			return false;
		}
		return true;
	}

	public boolean updateData(String table, String key, String key1, String wherekey, String wherevalue, String key2) {
		try {
			String updateSQL = "update " + table + " set " + key + "='" + key2 + "' where " + wherekey + " = '"
					+ wherevalue + "' and " + key + "='" + key1 + "'";
			Log.i("MyDataBaseAdapter-->updateData()", "updateSQL=" + updateSQL);
			mSQLiteDatabase.execSQL(updateSQL);
		} catch (SQLException e) {
			logger.error(TAG, e);
			System.out.println("---------------数据库报错----------------------" + e);
			return false;
		}
		return true;
	}

	/**
	 * 修改多个数据库表字段
	 * 
	 * @param table
	 * @param keys
	 * @param values
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 */
	public boolean updateData(String table, String[] keys, String[] values, String wherekey, long wherevalue) {
		try {
			String updateSQL = "update " + table + " set " + Util.getUpdateAppend(keys, values) + " where " + wherekey
					+ " = " + wherevalue;
			Log.i("MyDataBaseAdapter-->updateData()", "updateSQL=" + updateSQL);
			mSQLiteDatabase.execSQL(updateSQL);
		} catch (SQLException e) {
			logger.error(TAG, e);
			return false;
		}
		return true;
	}

	// 更新一条数据
	public boolean updateData(String table, String key, int value, String wherekey, String valueList) {
		try {
			String updateSQL = "update " + table + " set " + key + "=" + value + " where " + wherekey + " in ("
					+ valueList + ")";
			mSQLiteDatabase.execSQL(updateSQL);
		} catch (SQLException e) {
			logger.error(TAG, e);
			return false;
		}
		return true;
	}

	public boolean updateData(String table, ContentValues values, String whereClause, String[] whereArgs) {
		return mSQLiteDatabase.update(table, values, whereClause + "=?", whereArgs) > 0;
	}

	// 通过Cursor查询所有指定字段的数据
	public Cursor getAllDatas(String table, String[] keyString) {
		return mSQLiteDatabase.query(table, keyString, null, null, null, null, null);
	}

	// 通过Cursor查询所有指定字段的数据
	public Cursor getDatasbyLimit(String table, String[] keyString, String limitString) {
		return mSQLiteDatabase.query(table, keyString, null, null, null, null, null, limitString);
	}

	// 通过Cursor查询所有指定字段的数据
	public Long getDatasCount(String table) {
		String selectSQL = "select count(*) from " + table;
		Cursor mCursor = null;
		Long count = 0l;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			mCursor.moveToFirst();
			count = mCursor.getLong(0);
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			mCursor.close();
		}
		return count;
	}

	// 按条件，通过Cursor查询指定字段的数据
	public Cursor getAllDatas(String table, String[] keyString, String key, String value) {
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.query(true, table, keyString, key + "='" + value + "'", null, null, null, null,
					null);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 根据sql查询数据
	 * 
	 * @param selectSQL
	 * @return
	 */
	public Cursor getDatas(String selectSQL) {
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 通过Cursor查询所有数据
	public Cursor getAllDatas(String table) {
		String selectSQL = "select * from " + table;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 通过Cursor查询所有数据
	public Cursor getAllDatas1(String table, String order) {
		String selectSQL = "select * from " + table + " Order By " + order;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按条件，通过Cursor查询数据
	public Cursor getAllDatas(String table, String wherekey, String wherevalue) {
		String selectSQL = "select * from " + table + " where " + wherekey + " = '" + wherevalue + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按条件，通过Cursor查询数据
	public Cursor getAllDatas(String table, String wherekey, String wherevalue, String orderby, boolean flag_desc) {
		String selectSQL = "Select * From " + table + " Where " + wherekey + " = '" + wherevalue + "' Order By "
				+ orderby;
		if (flag_desc) {
			selectSQL += " desc";
		}
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按条件，通过Cursor查询数据
	public Cursor getAllDatasexcept(String table, String wherekey, String wherevalue, String order) {
		String selectSQL = "select * from " + table + " where " + wherekey + " != '" + wherevalue + "' Order By "
				+ order;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按条件（是否为好友）查找数据库
	public Cursor getFriendDatas(String table, String wherekey, String wherevalue) {
		String selectSQL = "select " + TABLE_USERS + ".*," + TABLE_PHONE + "." + PHONE_NUM + " from " + table
				+ " left outer join " + TABLE_USERS + " where " + TABLE_USERS + "." + USER_Contacteid + " = "
				+ TABLE_PHONE + "." + PHONE_Contacteid + " and " + wherekey + " = '" + wherevalue + "'";
		// Log.v("SQL",selectSQL);
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按条件（电话号码）查找数据库
	public Cursor getUserDatas(String table, String wherekey, String wherevalue) {

		String selectSQL = "select " + TABLE_USERS + ".*," + TABLE_PHONE + "." + PHONE_FRIEND + " from " + table
				+ " left outer join " + TABLE_USERS + " where " + TABLE_USERS + "." + USER_Contacteid + " = "
				+ TABLE_PHONE + "." + PHONE_Contacteid + " and " + wherekey + " = '" + wherevalue + "'";

		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按条件（电话号码）查找数据库
	public Cursor getUserDatasByPhone() {
		String selectSQL = "select " + TABLE_USERS + ".*," + TABLE_PHONE + ".*" + " from " + TABLE_PHONE
				+ " left outer join " + TABLE_USERS + " where " + TABLE_USERS + "." + USER_Contacteid + " = "
				+ TABLE_PHONE + "." + PHONE_Contacteid;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按条件，通过Cursor查询数据
	public Cursor getAllDatascontain(String table, String wherekey, String wherevalue, String order) {
		String selectSQL = "select * from " + table + " where " + wherekey + " = '" + wherevalue + "' Order By "
				+ order;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getAllDatas(String table, String wherekey, long wherevalue) {
		String selectSQL = "select * from " + table + " where " + wherekey + " = " + wherevalue;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按时间排序，获取聊天室的消息列表
	public Cursor getUserDataByUserID(String wherekey, String wherevalue) {
		String selectSQL = "select * from " + TABLE_USERS + " where " + wherekey + " = '" + wherevalue + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getGroupById(String wherevalue) {
		String selectSQL = "select " + USER_GROUP_GROUPNAME + " from " + TABLE_USER_GROUP + " where " + _ID + " = "
				+ wherevalue;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	//
	public Cursor getUserItemByUserID(String wherekey, String wherevalue) {
		String selectSQL = "select " + TABLE_USERS + ".*," + TABLE_FRIENDS + "." + FRIENDS_REMARKNAME + " from "
				+ TABLE_USERS + "," + TABLE_FRIENDS + " where " + TABLE_USERS + "." + wherekey + " = '" + wherevalue
				+ "' and " + TABLE_FRIENDS + "." + FRIENDS_FRIENDUID + "=" + TABLE_USERS + "." + USER_ID;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor != null && !mCursor.isClosed() && mCursor.getCount() == 0) {
				mCursor = null;
				selectSQL = "select * from " + TABLE_USERS + " where " + TABLE_USERS + "." + wherekey + " = '"
						+ wherevalue + "'";
				mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按时间排序，获取聊天室的消息列表
	public Cursor getAllDatas(String wherekey, String wherevalue) {
		String selectSQL = "select * from MesRecordTable where " + wherekey + " = '" + wherevalue
				+ "' Order By creatTime ASC";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 获取指定条件的记录 by lzm
	public Cursor getDatas(String table, String where, String order) {
		String selectSQL = "select * from " + table + " where " + where + order;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 按时间排序，获取聊天室列表
	public Cursor getChatList(String type, String selectNum) {
		String typeStr = "";
		String selectNumStr = "";
		String selectSQL = "";
		if (type != null) {
			typeStr = " and C." + CHAT_TYPE + "=" + type;
			selectSQL = "Select * from " + TABLE_CHATROOM + " where " + CHAT_TYPE + "=" + type;
		}
		if (selectNum != null) {
			selectNumStr = "limit " + selectNum;
			selectSQL = "Select * from " + TABLE_CHATROOM + " as C left outer join (" + "Select * from "
					+ TABLE_MESRECORD + " where " + MESSAGE_TIME + " IN (" + "Select max(" + MESSAGE_TIME + ") from "
					+ TABLE_MESRECORD + " where " + MESSAGE_FROM + "=0 group by " + MESSAGE_CHATNUM + " ) )as M "
					+ "where C." + CHAT_NUM + "=M." + MESSAGE_CHATNUM + typeStr + " ORDER BY M." + MESSAGE_TIME
					+ " DESC " + selectNumStr;
		}
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 获取所有聊天室信息
	public Cursor getAllChatList(long count) {
		String selectSQL = "Select * from " + TABLE_CHATROOM + " order by " + CHAT_TIME + " desc limit " + count;

		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 查询私信
	public Cursor getUserMsg(String myuid, String himuid) {
		if (Util.isEmpty(myuid) || Util.isEmpty(himuid)) {
			return null;
		}
		String sql = "SELECT * FROM " + TABLE_USERMSG + " WHERE " + USERMSG_MYUID + " =" + myuid + " AND "
				+ USERMSG_HIMUID + " =" + himuid + " ORDER BY " + USERMSG_TALKTIME + " DESC";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * @param myuid
	 * @param himuid
	 * @param startLimit
	 * @param length
	 * @return
	 */
	public Cursor getUserMsgByLimit(String myuid, String himuid, int startLimit, int length) {
		if (Util.isEmpty(myuid) || Util.isEmpty(himuid)) {
			return null;
		}
		String sql = "SELECT * FROM " + TABLE_USERMSG + " WHERE " + USERMSG_MYUID + " =" + myuid + " AND "
				+ USERMSG_HIMUID + " =" + himuid + " ORDER BY " + USERMSG_TALKTIME + " limit " + startLimit + ","
				+ length;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	// 查询全部聊天对象
	public Cursor getUserList(String myuid) {
		String sql = "SELECT * FROM " + TABLE_USERMSG + " WHERE " + USERMSG_MYUID + " =" + myuid + " GROUP BY "
				+ USERMSG_HIMUID + " ORDER BY " + USERMSG_TALKTIME + " DESC";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * 获取当前用户的粉丝列表
	 * 
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 */
	public Cursor getUserFans(String wherekey, String wherevalue) {
		String selectSQL = "select DISTINCT A." + FANS_FANUID + ", B.* from " + TABLE_FANS + " A ," + TABLE_USERS
				+ " B where A." + wherekey + " = '" + wherevalue + "' AND A." + FANS_FANUID + "= B." + USER_ID;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getMyGroupByType(String myuid, String grouptype) {
		String selectSQL = "select * from " + TABLE_CONTACTS_GROUP + " where " + GROUP_MYUID + "='" + myuid + "' and "
				+ GROUP_TYPE + "='" + grouptype + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getGroupByID(String myuid, String groupID) {
		String selectSQL = "select * from " + TABLE_CONTACTS_GROUP + " where " + GROUP_MYUID + "='" + myuid + "' and "
				+ GROUP_ID + "='" + groupID + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * 获取当前用户的好友组列表
	 * 
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 */
	public Cursor getFriendsByGroupID(String groupID, String myuid) {
		String selectSQL = "select B.*,A." + FRIENDS_REMARKNAME + " AS " + FRIENDS_REMARKNAME + " from "
				+ TABLE_FRIENDS + " A ," + TABLE_USERS + " B where A." + FRIENDS_MYUID + " = '" + myuid + "' and A."
				+ FRIENDS_GROUPID + " = '" + groupID + "' AND A." + FRIENDS_FRIENDUID + "= B." + USER_ID;
		// String selectSQL="select * from Friends";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * 获取当前用户的好友组列表
	 * 
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 */
	public Cursor getAttentsByGroupID(String groupID, String myuid) {
		String selectSQL = "select B.* from " + TABLE_ATTENTS + " A ," + TABLE_USERS + " B where A." + ATTENTS_MYUID
				+ " = '" + myuid + "' and A." + ATTENTS_GROUPID + " = '" + groupID + "' AND A." + ATTENTS_ATTENTUID
				+ "= B." + USER_ID;
		// String selectSQL="select * from Friends";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * 获取当前用户的好友组列表
	 * 
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 */
	public Cursor getTracersByGroupID(String groupID, String myuid) {
		String selectSQL = "select B.* from " + TABLE_TRACE + " A ," + TABLE_USERS + " B where A." + TRACE_MYUID
				+ " = '" + myuid + "' and A." + TRACE_GROUPID + " = '" + groupID + "' AND A." + TRACE_RUID + "= B."
				+ USER_ID;
		// String selectSQL="select * from Friends";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 获取资源4个大类信息
	 * 
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 */
	public Cursor getTopRess(String wherekey, String wherevalue) {
		String selectSQL = "select * from " + TABLE_RES + " where " + wherekey + " = '" + wherevalue + "' and "
				+ RES_PARENT_ID + " = '0'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getTableInfo(String table, String[] keys, String[] values) {
		StringBuffer sbSQL = new StringBuffer("select * from " + table);
		if (keys != null && values != null && keys.length == values.length) {
			sbSQL.append(" where ");
			for (int i = 0; i < keys.length; i++) {
				if (i == keys.length - 1) {
					sbSQL.append(keys[i]).append(" = '").append(values[i]).append("'");
				} else {
					sbSQL.append(keys[i]).append(" = '").append(values[i]).append("' and ");
				}
			}
		}

		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sbSQL.toString(), null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getTableInfoEx(String table, String[] keys, String[] values) {
		StringBuffer sbSQL = new StringBuffer("select * from " + table + " where ");
		if (keys != null || values != null) {
			for (int i = 0; i < keys.length; i++) {
				if (i == 0) {
					sbSQL.append(keys[i]).append(" = '").append(values[i]).append("'");
				} else
					sbSQL.append(" and ").append(keys[i]).append(" = '").append(values[i]);
			}
		}
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sbSQL.toString(), null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getTableInfoEx(String table, String[] keys, String[] values, String orderby, boolean flag) {
		StringBuffer sbSQL = new StringBuffer("select * from " + table + " where ");
		if (keys != null || values != null) {
			for (int i = 0; i < keys.length; i++) {
				if (i == 0) {
					sbSQL.append(keys[i]).append("= '").append(values[i]).append("'");
					;
				} else
					sbSQL.append("' and ").append(keys[i]).append(" = '").append(values[i]);
			}
		}

		sbSQL.append(" order by ").append(orderby);
		if (flag) {
			sbSQL.append(" desc");
		}
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sbSQL.toString(), null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getTableInfoEx(String table, String[] keys, String[] values, String orderby, boolean flag,
			int length, int offset) {
		StringBuffer sbSQL = new StringBuffer("select * from " + table + " where ");
		if (keys != null || values != null) {
			for (int i = 0; i < keys.length; i++) {
				if (i == 0) {
					sbSQL.append(keys[i]).append("= '").append(values[i]).append("'");
					;
				} else
					sbSQL.append("' and ").append(keys[i]).append(" = '").append(values[i]);

			}
		}

		sbSQL.append(" order by ").append(orderby);
		if (flag) {
			sbSQL.append(" desc");
		}
		if (length > 0) {
			sbSQL.append(" limit ").append(length).append(" offset ").append(offset);
		}
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sbSQL.toString(), null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor rawQuery(String sql) {
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getTableInfo(String table, String[] keys, String[] values, String orderby, boolean desc) {
		StringBuffer sbSQL = new StringBuffer("select * from " + table + " where ");
		for (int i = 0; i < keys.length; i++) {
			if (i == keys.length - 1) {
				sbSQL.append(keys[i]).append(" = '").append(values[i]).append("' ");
			} else {
				sbSQL.append(keys[i]).append(" = '").append(values[i]).append("' and ");
			}
		}
		sbSQL.append("order by ").append(orderby);
		if (desc) {
			sbSQL.append(" desc");
		}
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sbSQL.toString(), null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getTableInfo(String table, String[] keys, String[] values, String orderby, boolean desc, int length,
			int offset) {
		StringBuffer sbSQL = new StringBuffer("select * from " + table + " where ");
		for (int i = 0; i < keys.length; i++) {
			if (i == keys.length - 1) {
				sbSQL.append(keys[i]).append(" = '").append(values[i]).append("' ");
			} else {
				sbSQL.append(keys[i]).append(" = '").append(values[i]).append("' and ");
			}
		}
		sbSQL.append("order by ").append(orderby);
		if (desc) {
			sbSQL.append(" desc");
		}
		if (length > 0) {
			sbSQL.append(" limit ").append(length).append(" offset ").append(offset);
		}
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sbSQL.toString(), null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * 获取当前用户的好友组列表
	 * 
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 */
	public Cursor getContactsGroups(String wherekey, String wherevalue) {
		String selectSQL = "select * from " + TABLE_CONTACTS_GROUP + " where " + wherekey + " = '" + wherevalue
				+ "' and " + GROUP_ID + "!='" + PangkerConstant.DEFAULT_GROUPID + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 判断用户是否存在
	 * 
	 * @param userid
	 * @return
	 */
	public boolean checkUserIsExist(String userid) {
		String selectSQL = "select * from " + TABLE_USERS + " where " + USER_ID + " = '" + userid + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}

		return true;
	}

	/**
	 * 判断用户是否还能执行碰一下操作
	 * 
	 * @param userid
	 * @return
	 */
	public boolean canTouchUser(String myuserid, String day) {
		String selectSQL = "select * from " + TABLE_TOUCH_USER + " where " + TOUCH_USER_MYUID + " = '" + myuserid
				+ "' AND " + TOUCH_USER_DAY + " = '" + day + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return true;
			if (mCursor.getCount() == 0)
				return true;
			mCursor.moveToFirst();
			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				if (mCursor.getInt(mCursor.getColumnIndex(TOUCH_USER_TIMES)) > 10) {
					return false;
				}
			}

		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return true;
	}

	/**
	 * 判断用户是否还能执行碰一下操作
	 * 
	 * @param userid
	 * @return
	 */
	public boolean dayTouchedUser(String myuserid, String day) {
		String selectSQL = "select * from " + TABLE_TOUCH_USER + " where " + TOUCH_USER_MYUID + " = '" + myuserid
				+ "' AND " + TOUCH_USER_DAY + " = '" + day + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return true;
	}

	/**
	 * 判断当前用户的粉丝是否存在
	 * 
	 * @param fansid
	 * @return
	 */
	public boolean checkFriendIsExist(String frienduid, String groupid, String myuid) {
		String selectSQL = "select * from " + TABLE_FRIENDS + " where " + FRIENDS_MYUID + " = '"
				+ Util.trimUserIDDomain(myuid) + "' and " + FRIENDS_FRIENDUID + " = '" + frienduid + "' and "
				+ FRIENDS_GROUPID + " = '" + groupid + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}

		return true;
	}

	/**
	 * 判断当前用户的粉丝是否存在
	 * 
	 * @param fansid
	 * @return
	 */
	public boolean checkFriendIsExist(String frienduid, String myuid) {
		String selectSQL = "select * from " + TABLE_FRIENDS + " where " + FRIENDS_MYUID + " = '"
				+ Util.trimUserIDDomain(myuid) + "' and " + FRIENDS_FRIENDUID + " = '" + frienduid + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}

		return true;
	}

	/**
	 * 判断当前用户的粉丝是否存在
	 * 
	 * @param fansid
	 * @return
	 */
	public boolean checkFansIsExist(String fansid, String myuid) {
		String selectSQL = "select * from " + TABLE_FANS + " where " + FANS_MYUID + " = '"
				+ Util.trimUserIDDomain(myuid) + "' and " + FANS_FANUID + " = '" + fansid + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return true;
	}

	/**
	 * 
	 * @param fansid
	 * @param myuid
	 * @return
	 * @author wangxin 2012-3-26 下午03:30:13
	 */
	public boolean checkPangkerIsExist(String ruid, String myuid) {
		String selectSQL = "select * from " + TABLE_CONTACTS_PANGKER + " where " + CONTACTS_PANGKER_MYUID + " = '"
				+ Util.trimUserIDDomain(myuid) + "' and " + CONTACTS_PANGKER_RUID + " = '" + ruid + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return true;
	}

	/**
	 * 判断当前用户的粉丝是否存在
	 * 
	 * @param fansid
	 * @return
	 */
	public boolean checkAttentIsExist(String uid, String groupid, String myuid) {
		String selectSQL = "select * from " + TABLE_ATTENTS + " where " + ATTENTS_ATTENTUID + " = '"
				+ Util.trimUserIDDomain(myuid) + "' and " + ATTENTS_ATTENTUID + " = '" + uid + "' and "
				+ ATTENTS_GROUPID + "= '" + groupid + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return true;
	}

	/**
	 * 判断当前用户的粉丝是否存在
	 * 
	 * @param fansid
	 * @return
	 */
	public boolean checkContactsGroupIsExist(String groupid, String myuid, int grouptype) {
		String selectSQL = "select * from " + TABLE_CONTACTS_GROUP + " where " + GROUP_MYUID + " = '"
				+ Util.trimUserIDDomain(myuid) + "' and " + GROUP_GROUPID + " = '" + groupid + "' and " + GROUP_TYPE
				+ " = '" + grouptype + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}

		return true;
	}

	public boolean checkExistById(String table, String id_key, String id_value) {
		String selectSQL = "select * from " + table + " where " + id_key + " = '" + id_value + "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return true;
	}

	/**
	 * 
	 * 获取全部好友列表（排重）
	 * 
	 * @return
	 */
	public Cursor getAllFriends(String myuid) {
		String selectSQL = "select  A." + FRIENDS_FRIENDUID + ",A." + FRIENDS_REMARKNAME + " AS " + FRIENDS_REMARKNAME
				+ ", B.* from " + TABLE_FRIENDS + " A," + TABLE_USERS + " B where A." + FRIENDS_FRIENDUID + "= B."
				+ USER_ID + " AND A." + FRIENDS_MYUID + "= " + myuid + " GROUP BY A." + FRIENDS_FRIENDUID;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * 获取全部好友列表（排重）
	 * 
	 * @return
	 */
	public Cursor getFriendsByGroupId(String groupId, String myuid) {
		String selectSQL = "select  A." + FRIENDS_FRIENDUID + ",A." + FRIENDS_REMARKNAME + " AS " + FRIENDS_REMARKNAME
				+ " , B.* from " + TABLE_FRIENDS + " A," + TABLE_USERS + " B where A." + FRIENDS_FRIENDUID + "= B."
				+ USER_ID + " and A." + FRIENDS_GROUPID + " = '" + groupId + "'" + " and A." + FRIENDS_MYUID + " = '"
				+ myuid + "'" + " GROUP BY A." + FRIENDS_FRIENDUID;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * 获取全部好友列表（排重）
	 * 
	 * @return
	 */
	public Cursor getAllAttents() {
		String selectSQL = "select DISTINCT A." + ATTENTS_ATTENTUID + ",A." + ATTENTS_TYPE + ", B.* from "
				+ TABLE_ATTENTS + " A," + TABLE_USERS + " B where A." + ATTENTS_ATTENTUID + "= B." + USER_ID
				+ " GROUP BY A." + ATTENTS_ATTENTUID;
		;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * 分组关注人
	 * 
	 * @return
	 */
	public Cursor getAttentsByGroupId(String groupId, String myuid) {
		String selectSQL = "select DISTINCT A." + ATTENTS_ATTENTUID + ",A." + ATTENTS_TYPE + ", B.* from "
				+ TABLE_ATTENTS + " A," + TABLE_USERS + " B where A." + ATTENTS_ATTENTUID + "= B." + USER_ID
				+ " and A." + ATTENTS_GROUPID + " = '" + groupId + "'" + " and A." + ATTENTS_MYUID + " = '" + myuid
				+ "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * execSql
	 * 
	 * @param sql
	 * @return
	 */
	public boolean execSql(String sql) {
		try {
			mSQLiteDatabase.execSQL(sql);
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		}
		return true;
	}

	public boolean checkUserGroupIsExist(String groupId, String myUid, String groupType, int flag) {
		String selectSQL = "select * from " + TABLE_USER_GROUP + " where " + USER_GROUP_MYUID + " = '"
				+ Util.trimUserIDDomain(myUid) + "' and " + USER_GROUP_GROUPID + " = '" + groupId + "' and "
				+ USER_GROUP_TYPE + " = '" + groupType + "'" + " and " + USER_GROUP_FLAG + " = " + flag;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return false;
			if (mCursor.getCount() == 0)
				return false;
		} catch (Exception e) {
			logger.error(TAG, e);
			return false;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}

		return true;
	}

	public Cursor getAreaIDByName(String areaName) {
		String sql = "select " + AREA_AREAID + " from " + TABLE_AREA + " where " + AREA_AREANAME + " like '" + areaName
				+ "%'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	public Cursor getAreaNameByID(String areaID) {
		String sql = "select " + AREA_AREANAME + " from " + TABLE_AREA + " where " + AREA_AREAID + " = '" + areaID
				+ "'";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-27 下午01:38:56
	 */

	public Cursor getUserGroups(String wherekey, String wherevalue) {
		String selectSQL = "select * from " + TABLE_USER_GROUP + " where " + wherekey + " = '" + wherevalue + "' and "
				+ USER_GROUP_FLAG + " != 2";

		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 * 
	 * @author wangxin
	 * @date 2012-2-27 下午01:38:56
	 */
	public Cursor getComeUserGroups(String wherekey, String wherevalue) {
		String selectSQL = "select * from " + TABLE_USER_GROUP + " where " + wherekey + " = '" + wherevalue + "' and "
				+ USER_GROUP_FLAG + " = 2 order by " + USER_GROUP_LASTVISITTIME + " desc limit 10";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 会议室聊天记录
	 * 
	 * @author wubo
	 * @createtime 2012-3-19
	 * @param groupId
	 * @return
	 */
	public Cursor getMeetRoomHistory(String groupId, String myuserid) {
		String sql = "SELECT * FROM " + TABLE_MEETROOM + " WHERE " + MEETROOM_GID + " =" + groupId + " AND "
				+ MEETROOM_MYUID + " = " + myuserid + " ORDER BY " + MEETROOM_MSGID + " DESC";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 会议室聊天记录(limit)
	 * 
	 * @author wubo
	 * @createtime 2012-3-19
	 * @param groupId
	 * @return
	 */
	public Cursor getMeetRoomHistoryLimit(String groupId, int start, int lenght, String myuserid) {
		String sql = "SELECT * FROM " + TABLE_MEETROOM + " WHERE " + MEETROOM_GID + " =" + groupId + " AND "
				+ MEETROOM_MYUID + " = " + myuserid + " ORDER BY " + MEETROOM_MSGID + " ASC limit " + start + ","
				+ lenght;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 会议室聊天记录最大的序列号
	 * 
	 * @author wubo
	 * @createtime 2012-3-19
	 * @param groupId
	 * @return
	 */
	public Cursor getMsgMaxNum(String groupId) {
		String sql = "SELECT max(" + MEETROOM_MSGID + ") as " + MEETROOM_MSGID + " FROM " + TABLE_MEETROOM + " WHERE "
				+ MEETROOM_GID + " =" + groupId;
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * >>>>判断是否是旁客用户
	 * 
	 * @param phoneNum
	 * @return
	 * @author wangxin 2012-3-23 上午11:21:40
	 */
	public LocalContacts getContactsRelation(String phoneNum, String myUid) {
		String selectSQL = "select * from " + TABLE_CONTACTS_PANGKER + " where " + CONTACTS_PANGKER_PHONENUM + " = '"
				+ phoneNum + "' and " + CONTACTS_PANGKER_MYUID + " = '" + myUid + "'";
		Cursor mCursor = null;
		LocalContacts user = new LocalContacts();
		int relation = LocalContacts.RELATION_CONTACTS;
		user.setRelation(relation);
		String rUid = "";
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
			if (mCursor == null)
				return user;
			if (mCursor.getCount() == 0)
				return user;
			mCursor.moveToFirst();

			for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
				relation = LocalContacts.RELATION_P_USER;
				user.setRelation(relation);
				rUid = mCursor.getString(mCursor.getColumnIndex(CONTACTS_PANGKER_RUID));
				user.setUserId(rUid);
			}
			if (relation == LocalContacts.RELATION_P_USER) {
				if (checkFriendIsExist(rUid, myUid)) {
					relation = LocalContacts.RELATION_P_FRIEND;
					user.setRelation(relation);
				}
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		Log.d("LocalContacts", "relation:" + relation);
		return user;
	}

	/**
	 * 群组信息
	 * 
	 * @author wubo
	 * @createtime Apr 20, 2012
	 * @param wherekey
	 * @param wherevalue
	 * @return
	 */
	public Cursor getMeetGroup(String uid) {
		String selectSQL = "select * from " + TABLE_MEETGROUP + " where " + MEETGROUP_UID + " = ?";
		Cursor mCursor = null;
		try {
			mCursor = mSQLiteDatabase.rawQuery(selectSQL, new String[] { uid });
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return mCursor;
	}

	/**
	 * 获得权限值
	 * 
	 * @author wubo
	 * @createtime May 8, 2012
	 * @param uid
	 * @param name
	 * @return
	 */
	public Grant getGrantValue(String uid, String name) {
		Grant grant = null;
		String sql = "select * from " + TABLE_GRANT + " where uid= ? and " + GRANT_NAME + " =? ";
		Cursor cursor = null;
		try {
			cursor = mSQLiteDatabase.rawQuery(sql, new String[] { uid, name });
			if (cursor != null && cursor.moveToFirst()) {
				grant = new Grant();
				grant.setGrantId(cursor.getInt(cursor.getColumnIndex(GRANT_ID)));
				grant.setGrantName(cursor.getString(cursor.getColumnIndex(GRANT_NAME)));
				grant.setGrantValue(cursor.getInt(cursor.getColumnIndex(GRANT_VALUE)));
				grant.setUid(cursor.getString(cursor.getColumnIndex(GRANT_UID)));
				grant.setGrantGId(cursor.getString(cursor.getColumnIndex(GRANT_GID)));
				grant.setGrantUid(cursor.getString(cursor.getColumnIndex(GRANT_UIDS)));
				return grant;
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			return null;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (mSQLiteDatabase != null) {
				mSQLiteDatabase.close();
			}
		}
		return null;
	}

	/**
	 * 更新权限值
	 * 
	 * @author wubo
	 * @createtime May 8, 2012
	 * @param uid
	 * @param name
	 * @param value
	 */
	public void updateGrantValue(String uid, String name, int value) {
		String sql = "update " + TABLE_GRANT + " set " + GRANT_VALUE + " = ? where " + GRANT_UID + " =? and "
				+ GRANT_NAME + " =?";
		mSQLiteDatabase.execSQL(sql, new Object[] { value, uid, name });
	}

	/**
	 * 分组权限
	 * 
	 * @author wubo
	 * @createtime 2012-8-15
	 * @param uid
	 * @param name
	 * @param value
	 * @param gid
	 */
	public void updateGrantGid(String uid, String name, int value, String gids) {

		String sql = "update " + TABLE_GRANT + " set " + GRANT_VALUE + " = ? , " + GRANT_GID + " =? where " + GRANT_UID
				+ " =? and " + GRANT_NAME + " =?";

		mSQLiteDatabase.execSQL(sql, new Object[] { value, gids, uid, name });
	}

	/**
	 * 联系人权限
	 * 
	 * @author wubo
	 * @createtime 2012-8-15
	 * @param uid
	 * @param name
	 * @param value
	 * @param gid
	 */
	public void updateGrantUid(String uid, String name, int value, String uids) {
		String sql = "update " + TABLE_GRANT + " set " + GRANT_VALUE + " = ? , " + GRANT_UIDS + " =? where "
				+ GRANT_UID + " =? and " + GRANT_NAME + " =?";
		mSQLiteDatabase.execSQL(sql, new Object[] { value, uids, uid, name });
	}

	/**
	 * 获取全部grant
	 * 
	 * @author wubo
	 * @createtime May 8, 2012
	 * @param uid
	 * @return
	 */
	public Cursor getGrants(String uid) {
		String sql = "select * from " + TABLE_GRANT + " where uid= ? ";
		Cursor cursor = null;
		try {
			cursor = mSQLiteDatabase.rawQuery(sql, new String[] { uid });
		} catch (Exception e) {
			logger.error(TAG, e);
		}
		return cursor;
	}

	/**
	 * 更新群组信息
	 * 
	 * @author wubo
	 * @createtime 2012-5-18
	 * @param userGroup
	 */
	public void updateGroup(UserGroup userGroup) {

		String sql = "update " + TABLE_USER_GROUP + " set " + USER_GROUP_GROUPNAME + " =? , " + USER_GROUP_LABLE
				+ " =? , " + USER_GROUP_TYPE + " =? , " + USER_GROUP_LOTYPE + " =? where " + USER_GROUP_GROUPID
				+ " =? ";
		mSQLiteDatabase.execSQL(
				sql,
				new Object[] { userGroup.getGroupName(), userGroup.getGroupLabel(), userGroup.getGroupType(),
						userGroup.getLotype(), userGroup.getGroupId() });

	}

	/**
	 * 更新群组基本信息
	 * 
	 * @author wubo
	 * @createtime 2012-5-18
	 * @param userGroup
	 */
	public void updateGroupBase(UserGroup userGroup) {
		String sql = "update " + TABLE_USER_GROUP + " set " + USER_GROUP_GROUPNAME + " =? , " + USER_GROUP_LABLE
				+ " =? where " + USER_GROUP_GROUPID + " =? ";
		mSQLiteDatabase.execSQL(sql,
				new Object[] { userGroup.getGroupName(), userGroup.getGroupLabel(), userGroup.getGroupId() });
	}

	/**
	 * 更新群组进入时间
	 * 
	 * @author wubo
	 * @createtime 2012-5-18
	 * @param userGroup
	 */
	public void updateGroupIntoTime(UserGroup userGroup) {
		String sql = "update " + TABLE_USER_GROUP + " set " + USER_GROUP_LASTVISITTIME + " =? where "
				+ USER_GROUP_GROUPID + " =? ";
		mSQLiteDatabase.execSQL(sql, new Object[] { userGroup.getLastVisitTime(), userGroup.getGroupId() });
	}

	public void updateGroupCover(String uid, String gid) {
		String sql = "update " + TABLE_USER_GROUP + " set " + USER_GROUP_COVERFLAG + " =1 where " + USER_GROUP_GROUPID
				+ " =? and " + USER_GROUP_MYUID + " =?";
		mSQLiteDatabase.execSQL(sql, new Object[] { gid, uid });
	}

	/**
	 * 删除群组记录
	 * 
	 * @author wubo
	 * @createtime 2012-5-18
	 * @param userGroup
	 */
	public void deleteUserGroup(String table, String where, String value) {
		String sql = "delete from " + table + " where " + where + " = ? ";
		mSQLiteDatabase.execSQL(sql, new Object[] { value });
	}

	public void deletePUsers() {
		mSQLiteDatabase.execSQL("delete from " + TABLE_CONTACTS_PANGKER);
	}
}
