package com.wachoo.pangker.db;



public interface ICallPhoneDao {


	public boolean insertPhone(String uid, String phone);

	public void clearPhones(String uid);

	public String[] getPhoneNumbers(String userid);
	
	public boolean isExist(String phoneNum);

}
