package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.server.response.AddressBooks;

public interface INetAddressBookDao {

	/**
	 * 添加单位通讯录
	 * 
	 * @param book
	 */
	public void insertAddressBook(String mUserId ,AddressBooks book);

	/**
	 * 删除单位通讯录
	 * 
	 * @param bookId
	 */
	public void deleAddressBook(String mUserId ,String bookId);

	/**
	 * 修改单位通讯录
	 * 
	 * @param book
	 */
	public void updateAddressBook(String mUserId ,AddressBooks book);

	public boolean isExist(String mUserId ,String bookId, MyDataBaseAdapter adapter);

	/**
	 * 保存单位通讯录列表
	 * 
	 * @param addressBookLists
	 */
	public void saveAddressBooks(String mUserId ,List<AddressBooks> addressBookLists);

	/**
	 * 获取所有单位通讯录（自己创建的和加入的）
	 * 
	 * @return
	 */
	public List<AddressBooks> getAddressBookList(String mUserId);

	public boolean clearAddressBook(String mUserId);
}
