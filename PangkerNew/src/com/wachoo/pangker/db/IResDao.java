package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.DirInfo;

public interface IResDao {
	/**
	 * 保存一个资源信息,需要父目录
	 * @param resinfo
	 * @return
	 */
	public long saveResInfo(DirInfo resinfo);
	/**
	 * 删除本地资源，连带他的子资源也要删除，但是parentid为0的不能删除
	 * @param resinfo
	 * @return
	 */
	public boolean deleteResInfo(DirInfo resinfo);
	/**
	 * 更新一个资源信息，如文件名称
	 * @param resinfo
	 * @return
	 */
	public boolean updateResInfo(DirInfo resinfo);
	/**
	 * 根据Id获取资源信息
	 * @param resinfo
	 * @return
	 */
	public DirInfo getResById(String id); 
	/**
	 * 根据类型获取资源信息,仅仅是头部信息，便于扩展parentid=0
	 * @param resinfo
	 * @return
	 */
	public List<DirInfo> getResByIndex(String userId); 
	/**
	 * 根据类型分享，收藏，转播的类型获取资源
	 * @param resinfo
	 * @return
	 */
	public List<DirInfo> getDirByType(String userId, int dealkey, int type);
	/**
	 * 用户是选择或者下载了该资源,以资源路径为准
	 * @param resinfo
	 * @return
	 */
	public boolean isSelected(String userId, String path, int type);
	/**
	 * 保存一个资源信息,不需要父目录
	 * @param resinfo
	 * @return
	 */
	public boolean saveSelectResInfo(DirInfo resinfo);
	
	/**
	 * 删除一个资源信息,不需要父目录
	 * @param resinfo
	 * @return
	 */
	public boolean deleteSelectResInfo(DirInfo resinfo);
	
	/**
	 * 根据Id获取资源信息
	 * @param resinfo
	 * @return
	 */
	public DirInfo getResByPath(String path, String userId); 
	 
}
