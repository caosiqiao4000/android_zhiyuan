package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.MessageTip;

public interface ITipsDao {
	/**
	 * 记录一条消息
	 * 
	 * @param noticeInfo
	 * @return
	 */
	public long saveMessageTip(MessageTip messageTip);

	/**
	 * 获取所有的消息
	 * 
	 * @return
	 */
	public List<MessageTip> getMessageTips(String userId);

	/**
	 * 删除一条消息,要删除对应表的所有消息
	 * 
	 * @param noticeInfo
	 * @return
	 */
	public boolean delMessageTip(MessageTip messageTip);

	/**
	 * 修改一条消息状态
	 * 
	 * @param noticeInfo
	 */
	public boolean updateMessageTip(MessageTip messageTip);
	
	/**
	 * 清空用户所有的会话
	 * @return
	 */
	public boolean clearMessageTips(String userId);
	
}
