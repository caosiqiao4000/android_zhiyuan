package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.UserItem;

/**
 * 
 * users friends db manager
 * 
 * @author wangxin
 * 
 */
public interface IFriendsDao {

	/**
	 * 
	 * save friend by groupid
	 * 
	 * @param user
	 * @param groupid
	 */
	public boolean addFriend(UserItem user, String groupid);

	/**
	 * 
	 * @param user
	 * @param groupid
	 * @param myDataBaseAdapter
	 */
	public boolean addFriend(UserItem user, String groupid, MyDataBaseAdapter myDataBaseAdapter);

	/**
	 * 
	 * get all friends list(distinct)
	 * 
	 * 获取全部好友列表（排重）
	 * 
	 * @return
	 */
	public List<UserItem> getAllFriendsByUID(String myUid);

	/**
	 * delFriend
	 * 
	 * @param userid
	 * @param groupid
	 * @return
	 *
	 * @author wangxin 2012-2-17 下午05:59:24
	 */
	public boolean delFriend(String userid, String groupid);

	/**
	 * 更新好友关系
	 * 
	 * @param rUserid
	 * @param relationtype
	 * @return
	 *
	 * @author wangxin 2012-2-16 下午04:32:15
	 */
	public boolean updateRelation(String rUserid, String relationtype);
	
	/**
	 * 更新好友信息
	 * 
	 * @param user
	 * @return boolean
	 */
	public boolean updateFriend(UserItem user);
	/**
	 * 更新亲友位置信息
	 * @param user
	 * @return boolean
	 */
	public boolean updateFriendLocation(UserItem user) ;
	/**
	 * 同步亲友
	 * @param user
	 * @return boolean
	 */
	public boolean pharseTracelist(List<UserItem> users) ;
	/**
	 * 更新亲友位置信息
	 * @param user
	 * @return boolean
	 */
	public boolean pharseTracelist(UserItem user) ;
	/**
	 * 根据用户分组Id获取用户列表
	 * @param groupId
	 * @return
	 */
	public List<UserItem> getFriendsByGroupId(String groupId);
	/**
	 * @param list
	 * @return
	 */
	public boolean refreshFriends(List<ContactGroup> list);
	
	/**
	 * 获取亲友列表
	 * @param myUid
	 * @return
	 */
	public List<UserItem> getTracelist(String myUid);
	/**
	 * 将 所有的亲友都变成好友
	 * @param myUid
	 */
	public boolean clearTraceUsers(String myUid);
}
