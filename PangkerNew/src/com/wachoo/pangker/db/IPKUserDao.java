package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;

/**
 * 
 * 旁客联系人本地vcard管理接口
 * 
 * @author wangxin
 * 
 */
public interface IPKUserDao {

	/**
	 * save pangker user vcard information
	 * 
	 * @param vcard
	 */
	public boolean saveUser(UserInfo user);

	/**
	 * update pangker user vcard information
	 * 
	 * @param vcard
	 * @return
	 */
	public boolean updateUser(UserInfo user);

	/**
	 * 
	 * get user by user id
	 * 
	 * @param userid
	 * @return
	 */
	public UserInfo getUserByUserID(String userid);

	/**
	 * 
	 * getUserItemByUserID
	 * 
	 * @param userid
	 * @return
	 */
	public UserItem getUserItemByUserID(String userid);
	
	
	/**
	 * 
	 * getUserItemByUserID
	 * 
	 * @param userid
	 * @return
	 */
	public boolean saveUser(UserItem user);
	
	/**
	 * 
	 * saveUser
	 * 
	 * @param user
	 * @param myDataBaseAdapter
	 * @return
	 */
	public boolean saveUser(UserItem user,MyDataBaseAdapter myDataBaseAdapter);
	
	
	/**
	 * 批量保存用户
	 * @author wangxin
	 * @createtime 2012-1-13 上午11:15:54
	 * @param users
	 * @return
	 */
	public boolean saveUser(List<UserItem> users);
	
	/**
	 * 修改签名
	 * @param userInfo
	 * @return
	 */
	public boolean updateSign(UserInfo userInfo);
	

}
