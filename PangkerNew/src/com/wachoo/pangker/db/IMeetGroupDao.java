package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.server.response.MeetGroup;

/**
 * 
 * @author wangxin
 * 
 */
public interface IMeetGroupDao {
	/**
	 * 
	 * @param group
	 */
	public boolean addGroup(MeetGroup group);

	/**
	 * 
	 * @param group
	 */
	public boolean delGroup(MeetGroup group);

	/**
	 * 
	 * saveGroups
	 * 
	 * @param groups
	 */
	public void saveGroups(List<MeetGroup> groups);

	/**
	 * getGroups
	 */
	public List<MeetGroup> getMeetGroups();

}
