package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.UploadJob;

public interface UploadDao {
	/**
	 * 保存一条上传任务
	 */
	public long saveUploadInfo(UploadJob uploadInfo);
	/**
	 * 获取用户的上传任务
	 */
	public List<UploadJob> getUploadInofs(String userId);
	/**
	 * 删除所有的上传任务
	 */
	public boolean updateUploadInfo(UploadJob uploadInfo);
	/**
	 * 根据状态删除上传任务
	 * @param status
	 */
	public void deleteUploadInfos(int status);
	/**
	 * 删除所有的上传任务
	 */
	public void deleteUploadInfos(String userId);
	/**
	 * 删除一条上传任务
	 */
	public void deleteUploadInfo(UploadJob uploadInfo);
	/**
	 * 根据md5,状态值获取当前文件是否在上传，
	 * @param md5
	 * @return
	 */
	public int ifUploading(String userId, String md5, int status);
	
	/**
	 * 根据本地的地址获取用户的上传任务，防止重复上传
	 */
	public UploadJob getUploadInof(String userId, String filepath);
	
	/**
	 * 根据ResId获取本地的文件地址
	 */
	public String getResPath(String resId);
}
