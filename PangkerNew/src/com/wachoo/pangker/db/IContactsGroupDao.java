package com.wachoo.pangker.db;

import java.util.List;

import com.wachoo.pangker.entity.ContactGroup;

/**
 * 
 * @author wangxin
 * 
 */
public interface IContactsGroupDao {

	/**
	 * getMyGroupByType
	 * 
	 * @param type
	 * @return
	 * 
	 * @author wangxin 2012-2-14 上午11:41:36
	 */
	public List<ContactGroup> getMyGroupByType(String type);

	/**
	 * 
	 * saveGroups
	 * 
	 * @param groups
	 */
	public void saveGroups(List<ContactGroup> groups);

	/**
	 * getGroups
	 */
	public List<ContactGroup> getGroups();

	/**
	 * 
	 * @param group
	 */
	public boolean addGroup(ContactGroup group, String myUid);

	/**
	 * 
	 * @param group
	 */
	public boolean delGroup(ContactGroup group, String myUid);

	/**
	 * 
	 * @param group
	 */
	public void updateGroup(ContactGroup group, String myUid);

	public String getGroupByNameId(String groupID);
}