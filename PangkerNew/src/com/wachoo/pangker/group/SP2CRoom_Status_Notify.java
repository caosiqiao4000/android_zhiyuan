package com.wachoo.pangker.group;

public class SP2CRoom_Status_Notify {

	private long OwnerUID;
	private long OwnerSID;
	private short MembersOperate;
	private short OnlineNum;
	private short CRoomOp;
	private short MembersNum;
	private P2PUserInfo[] MembersInfos;

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public short getMembersOperate() {
		return MembersOperate;
	}

	public void setMembersOperate(short membersOperate) {
		MembersOperate = membersOperate;
	}

	public short getOnlineNum() {
		return OnlineNum;
	}

	public void setOnlineNum(short onlineNum) {
		OnlineNum = onlineNum;
	}

	public short getMembersNum() {
		return MembersNum;
	}

	public void setMembersNum(short membersNum) {
		MembersNum = membersNum;
	}

	public P2PUserInfo[] getMembersInfos() {
		return MembersInfos;
	}

	public void setMembersInfos(P2PUserInfo[] membersInfos) {
		MembersInfos = membersInfos;
	}

	public short getCRoomOp() {
		return CRoomOp;
	}

	public void setCRoomOp(short cRoomOp) {
		CRoomOp = cRoomOp;
	}

}
