package com.wachoo.pangker.group;

/**
 *@author wubo
 *@createtime Jun 14, 2012
 */
public class CRoom2SP_LoginOut_Req {
	
	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	public long getSender_UID() {
		return Sender_UID;
	}
	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}
	public long getReceiver_UID() {
		return Receiver_UID;
	}
	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}
	public long getOwnerUID() {
		return OwnerUID;
	}
	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}
	public long getOwnerSID() {
		return OwnerSID;
	}
	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}
	public CRoom2SP_LoginOut_Req(long senderUID, long receiverUID,
			long ownerUID, long ownerSID) {
		super();
		Sender_UID = senderUID;
		Receiver_UID = receiverUID;
		OwnerUID = ownerUID;
		OwnerSID = ownerSID;
	}
	public CRoom2SP_LoginOut_Req() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
