package com.wachoo.pangker.group;

public class CRoom2SP_Status_Report {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private short RKey;
	private short CRoomOperate;
	private short MembersOperate; //0x11，新增，0x12删除，0x13更新，0x14更新，0x15更新，0x16开启接受语音，0x17关闭接受语音
	private short AddType; //
	private P2PUserInfo MembersInfo;
	
	public long getOwnerUID() {
		return OwnerUID;
	}
	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}
	public long getOwnerSID() {
		return OwnerSID;
	}
	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}
	public short getRKey() {
		return RKey;
	}
	public void setRKey(short rKey) {
		RKey = rKey;
	}
	public short getCRoomOperate() {
		return CRoomOperate;
	}
	public void setCRoomOperate(short cRoomOperate) {
		CRoomOperate = cRoomOperate;
	}
	public short getMembersOperate() {
		return MembersOperate;
	}
	public void setMembersOperate(short membersOperate) {
		MembersOperate = membersOperate;
	}
	public short getAddType() {
		return AddType;
	}
	public void setAddType(short addType) {
		AddType = addType;
	}
	public P2PUserInfo getMembersInfo() {
		return MembersInfo;
	}
	public void setMembersInfo(P2PUserInfo membersInfo) {
		MembersInfo = membersInfo;
	}
	public long getSender_UID() {
		return Sender_UID;
	}
	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}
	public long getReceiver_UID() {
		return Receiver_UID;
	}
	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}
	public CRoom2SP_Status_Report(long senderUID, long receiverUID,
			long ownerUID, long ownerSID, short rKey, short cRoomOperate,
			short membersOperate, short addType, P2PUserInfo membersInfo) {
		super();
		Sender_UID = senderUID;
		Receiver_UID = receiverUID;
		OwnerUID = ownerUID;
		OwnerSID = ownerSID;
		RKey = rKey;
		CRoomOperate = cRoomOperate;
		MembersOperate = membersOperate;
		AddType = addType;
		MembersInfo = membersInfo;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Sender_UID:" + Sender_UID + ",Receiver_UID:" + Receiver_UID + ",OwnerUID:" + OwnerUID + ",OwnerSID:" + OwnerSID
				+ ",RKey:" + RKey + ",CRoomOperate:" + CRoomOperate + ",AddType:" + AddType + ",MembersInfo:{" + MembersInfo.toString() + "}";
	}
	
};
