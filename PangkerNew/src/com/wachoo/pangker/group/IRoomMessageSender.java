package com.wachoo.pangker.group;


public interface IRoomMessageSender {
	/**
	 * 发包
	 * @author wubo
	 * @createtime Jun 4, 2012 
	 * @param caseKey
	 */
	public void sendPack(int caseKey);
	
	
	/**
	 * 开始发言
	 * @author wubo
	 * @createtime Jun 4, 2012
	 */
	public void startAudioRecode();
	
	/**
	 * 停止发言
	 * @author wubo
	 * @createtime Jun 4, 2012
	 */
	public void stopAudioRecode();
	
	/**
	 * 要发送的音频
	 * @author wubo
	 * @createtime Jun 5, 2012
	 */
	public void setSendAmr(byte[] sendAmr);
	
	/**
	 * 停止发言
	 * @author wubo
	 * @createtime Jun 4, 2012
	 */
	public void showRecordTime(int second);
}
