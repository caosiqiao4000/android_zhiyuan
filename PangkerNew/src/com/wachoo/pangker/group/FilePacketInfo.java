package com.wachoo.pangker.group;

import java.io.Serializable;

/**
 * com.wachoo.pangker.group.FilePacketInfo
 * 
 * @author wubo
 * @date 2012-11-8
 * 
 */
@SuppressWarnings("serial")
public class FilePacketInfo implements Serializable {

	private String FileName;
	private String Pad;
	private int FileTotal;
	private byte[] FileContext;

	public String getFileName() {
		return FileName;
	}

	public void setFileName(String fileName) {
		FileName = fileName;
	}

	public String getPad() {
		return Pad;
	}

	public void setPad(String pad) {
		Pad = pad;
	}

	public int getFileTotal() {
		return FileTotal;
	}

	public void setFileTotal(int fileTotal) {
		FileTotal = fileTotal;
	}

	public byte[] getFileContext() {
		return FileContext;
	}

	public void setFileContext(byte[] fileContext) {
		FileContext = fileContext;
	}

	public FilePacketInfo(String fileName, String pad, int fileTotal,
			byte[] fileContext) {
		FileName = fileName;
		Pad = pad;
		FileTotal = fileTotal;
		FileContext = fileContext;
	}

	public FilePacketInfo() {
	}

}
