package com.wachoo.pangker.group;

public class SP2CRoom_Status_Resp {

	private long OwnerUID;
	private long OwnerSID;
	private short Error_Code;
	private long ServerTime; //服务器时间  毫秒，从1970/1/1 00：00：00：00开始
	private short MembersOp;//提交类型；增删改
	
	public long getOwnerUID() {
		return OwnerUID;
	}
	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}
	public long getOwnerSID() {
		return OwnerSID;
	}
	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}
	public short getError_Code() {
		return Error_Code;
	}
	public void setError_Code(short errorCode) {
		Error_Code = errorCode;
	}
	public long getServerTime() {
		return ServerTime;
	}
	public void setServerTime(long serverTime) {
		ServerTime = serverTime;
	}
	public short getMembersOp() {
		return MembersOp;
	}
	public void setMembersOp(short membersOp) {
		MembersOp = membersOp;
	}
	
}
