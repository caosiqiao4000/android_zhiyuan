package com.wachoo.pangker.group;

public class SP2Client_NotRead_Resp {

	private long OwnerUID;
	private long OwnerSID;
	private String Pad;
	private short ErrCode;
	private ChatRecordInfo[] PayLoad;
	private long NoReadEndTime; // 未读消息最后一条时间
	private int TotalNoReadSize; // 总的未读条数
	private short EveryNoReadSize; // 每次返回的未读消息条数

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public String getPad() {
		return Pad;
	}

	public void setPad(String pad) {
		Pad = pad;
	}

	public short getErrCode() {
		return ErrCode;
	}

	public void setErrCode(short errCode) {
		ErrCode = errCode;
	}

	public ChatRecordInfo[] getPayLoad() {
		return PayLoad;
	}

	public void setPayLoad(ChatRecordInfo[] payLoad) {
		PayLoad = payLoad;
	}

	public int getTotalNoReadSize() {
		return TotalNoReadSize;
	}

	public void setTotalNoReadSize(int totalNoReadSize) {
		TotalNoReadSize = totalNoReadSize;
	}

	public short getEveryNoReadSize() {
		return EveryNoReadSize;
	}

	public long getNoReadEndTime() {
		return NoReadEndTime;
	}

	public void setNoReadEndTime(long noReadEndTime) {
		NoReadEndTime = noReadEndTime;
	}

	public void setEveryNoReadSize(short everyNoReadSize) {
		EveryNoReadSize = everyNoReadSize;
	}

}
