package com.wachoo.pangker.group;

/**
 * com.wachoo.pangker.group.SP2File_Upload_Resp
 * 
 * @author wubo
 * @date 2012-11-7
 * 
 */
public class SP2File_Upload_Resp {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private long FileStartSize;
	private int MsgId;
	private short Error_Code;

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public long getFileStartSize() {
		return FileStartSize;
	}

	public void setFileStartSize(long fileStartSize) {
		FileStartSize = fileStartSize;
	}

	public short getError_Code() {
		return Error_Code;
	}

	public void setError_Code(short errorCode) {
		Error_Code = errorCode;
	}

	public int getMsgId() {
		return MsgId;
	}

	public void setMsgId(int msgId) {
		MsgId = msgId;
	}

}
