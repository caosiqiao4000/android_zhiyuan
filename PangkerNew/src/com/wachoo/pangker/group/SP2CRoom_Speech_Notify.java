package com.wachoo.pangker.group;

public class SP2CRoom_Speech_Notify {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private long SpeekerUID;
	private short SpeechType;

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public long getSpeekerUID() {
		return SpeekerUID;
	}

	public void setSpeekerUID(long speekerUID) {
		SpeekerUID = speekerUID;
	}

	public short getSpeechType() {
		return SpeechType;
	}

	public void setSpeechType(short speechType) {
		SpeechType = speechType;
	}

	@Override
	public String toString() {
		return "SP2CRoom_Speech_Notify [Sender_UID=" + Sender_UID + ", Receiver_UID=" + Receiver_UID
				+ ", OwnerUID=" + OwnerUID + ", OwnerSID=" + OwnerSID + ", SpeekerUID=" + SpeekerUID
				+ ", SpeechType=" + SpeechType + "]";
	}

	public boolean isSpeecher(long UID) {
		return SpeekerUID == UID;
	}

};
