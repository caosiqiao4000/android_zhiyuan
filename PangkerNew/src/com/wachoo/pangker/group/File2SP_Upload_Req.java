package com.wachoo.pangker.group;

/**
 * com.wachoo.pangker.group.File2SP_Upload_Req
 * 
 * @author wubo
 * @date 2012-11-7
 * 
 */
public class File2SP_Upload_Req {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private short Wkey;
	private String FlieName;
	private long FileSize;
	private String MD5;

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public short getWkey() {
		return Wkey;
	}

	public void setWkey(short wkey) {
		Wkey = wkey;
	}

	public String getFlieName() {
		return FlieName;
	}

	public void setFlieName(String flieName) {
		FlieName = flieName;
	}

	public long getFileSize() {
		return FileSize;
	}

	public void setFileSize(long fileSize) {
		FileSize = fileSize;
	}

	public String getMD5() {
		return MD5;
	}

	public void setMD5(String mD5) {
		MD5 = mD5;
	}

	public File2SP_Upload_Req(long senderUID, long receiverUID, long ownerUID,
			long ownerSID, short wkey, String flieName, long fileSize,
			String mD5) {
		Sender_UID = senderUID;
		Receiver_UID = receiverUID;
		OwnerUID = ownerUID;
		OwnerSID = ownerSID;
		Wkey = wkey;
		FlieName = flieName;
		FileSize = fileSize;
		MD5 = mD5;
	}

}
