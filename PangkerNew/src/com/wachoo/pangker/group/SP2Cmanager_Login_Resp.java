package com.wachoo.pangker.group;


public class SP2Cmanager_Login_Resp {
	public SP2Cmanager_Login_Resp(){};
	private short Error_Code;
	private long  ServerTime;//

	public short getError_Code() {
		return Error_Code;
	}

	public void setError_Code(short errorCode) {
		Error_Code = errorCode;
	}

	public long getServerTime() {
		return ServerTime;
	}

	public void setServerTime(long serverTime) {
		ServerTime = serverTime;
	}

	public SP2Cmanager_Login_Resp(short error_Code, long serverTime) {
		super();
		Error_Code = error_Code;
		ServerTime = serverTime;
	}
};

