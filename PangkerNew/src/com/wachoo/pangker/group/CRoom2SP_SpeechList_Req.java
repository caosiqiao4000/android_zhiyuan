package com.wachoo.pangker.group;

public class CRoom2SP_SpeechList_Req {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private short RKEY;
	public long getSender_UID() {
		return Sender_UID;
	}
	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}
	public long getReceiver_UID() {
		return Receiver_UID;
	}
	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}
	public long getOwnerUID() {
		return OwnerUID;
	}
	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}
	public long getOwnerSID() {
		return OwnerSID;
	}
	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}
	public short getRKEY() {
		return RKEY;
	}
	public void setRKEY(short rKEY) {
		RKEY = rKEY;
	}
	public CRoom2SP_SpeechList_Req(long senderUID, long receiverUID,
			long ownerUID, long ownerSID, short rKEY) {
		super();
		Sender_UID = senderUID;
		Receiver_UID = receiverUID;
		OwnerUID = ownerUID;
		OwnerSID = ownerSID;
		RKEY = rKEY;
	}
	
};
