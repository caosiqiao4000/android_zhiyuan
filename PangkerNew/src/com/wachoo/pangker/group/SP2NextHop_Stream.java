package com.wachoo.pangker.group;

public class SP2NextHop_Stream {

	private long Sender_UID;
	private long Receiver_UID;
	private long OwnerUID;
	private long OwnerSID;
	private long CP_UID;
	private short S_Type;
	private short PayLoadLen;
	private String TTL;
	private int MsgId;
	private String Pad;
	private long ServerTime; // 服务器时间
	private byte[] PayLoad;

	public long getSender_UID() {
		return Sender_UID;
	}

	public void setSender_UID(long senderUID) {
		Sender_UID = senderUID;
	}

	public long getReceiver_UID() {
		return Receiver_UID;
	}

	public void setReceiver_UID(long receiverUID) {
		Receiver_UID = receiverUID;
	}

	public long getOwnerUID() {
		return OwnerUID;
	}

	public void setOwnerUID(long ownerUID) {
		OwnerUID = ownerUID;
	}

	public long getOwnerSID() {
		return OwnerSID;
	}

	public void setOwnerSID(long ownerSID) {
		OwnerSID = ownerSID;
	}

	public long getCP_UID() {
		return CP_UID;
	}

	public void setCP_UID(long cPUID) {
		CP_UID = cPUID;
	}

	public short getS_Type() {
		return S_Type;
	}

	public void setS_Type(short sType) {
		S_Type = sType;
	}

	public short getPayLoadLen() {
		return PayLoadLen;
	}

	public void setPayLoadLen(short payLoadLen) {
		PayLoadLen = payLoadLen;
	}

	public String getTTL() {
		return TTL;
	}

	public void setTTL(String tTL) {
		TTL = tTL;
	}

	public int getMsgId() {
		return MsgId;
	}

	public void setMsgId(int msgId) {
		MsgId = msgId;
	}

	public String getPad() {
		return Pad;
	}

	public void setPad(String pad) {
		Pad = pad;
	}

	public long getServerTime() {
		return ServerTime;
	}

	public void setServerTime(long serverTime) {
		ServerTime = serverTime;
	}

	public byte[] getPayLoad() {
		return PayLoad;
	}

	public void setPayLoad(byte[] payLoad) {
		PayLoad = payLoad;
	}

}
