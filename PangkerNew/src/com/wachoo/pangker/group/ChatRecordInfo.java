package com.wachoo.pangker.group;

public class ChatRecordInfo {

	private int MsgID;
	private short S_Type;
	private String Pad;
	private long SpeakerUID;
	private String SpeakerName;
	private long SpeakerTime;
	private String SpeakerContext;

	public int getMsgID() {
		return MsgID;
	}

	public void setMsgID(int msgID) {
		MsgID = msgID;
	}

	public String getPad() {
		return Pad;
	}

	public void setPad(String pad) {
		Pad = pad;
	}

	public long getSpeakerUID() {
		return SpeakerUID;
	}

	public void setSpeakerUID(long speakerUID) {
		SpeakerUID = speakerUID;
	}

	public String getSpeakerName() {
		return SpeakerName;
	}

	public void setSpeakerName(String speakerName) {
		SpeakerName = speakerName;
	}

	public long getSpeakerTime() {
		return SpeakerTime;
	}

	public void setSpeakerTime(long speakerTime) {
		SpeakerTime = speakerTime;
	}

	public String getSpeakerContext() {
		return SpeakerContext;
	}

	public void setSpeakerContext(String speakerContext) {
		SpeakerContext = speakerContext;
	}

	public short getS_Type() {
		return S_Type;
	}

	public void setS_Type(short sType) {
		S_Type = sType;
	}

}
