package com.wachoo.pangker.group;

public interface P2PInterface {
	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:54
	 * @param obj
	 */
	public void setSP2Cmanager_Login_Resp(SP2Cmanager_Login_Resp obj);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:57
	 * @param obj
	 */
	public void setSP2CRoom_Status_Resp(SP2CRoom_Status_Resp obj);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-13 02:05:44
	 * @param obj
	 */
	public void setSP2CRoom_Status_Notify(SP2CRoom_Status_Notify obj, Object Sock);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-13 02:07:17
	 * @param obj
	 */
	public void setSP2NextHop_Stream(SP2NextHop_Stream obj, Object Sock);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:49
	 * @param obj
	 */
	public void setSP2CRoom_Speech_Notify(SP2CRoom_Speech_Notify obj);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:49
	 * @param obj
	 */
	public void setSP2CRoom_Speech_Resp(SP2CRoom_Speech_Resp obj);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:45
	 * @param obj
	 */
	public void setSP2CRoom_SpeechList_Rsep(SP2CRoom_SpeechList_Rsep obj, Object Sock);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-3-8 11:34:49
	 * @param obj
	 */
	public void setSP2CRoom_SpeechListStatus_Notify(SP2CRoom_SpeechListStatus_Notify obj);

	public void setSP2Croom_UserList_Resp(SP2Croom_UserList_Resp obj, Object Sock);

	/**
	 * 
	 * @author wubo
	 * @param obj
	 * @param Sock
	 */
	public void setSP2Client_NotRead_Resp(SP2Client_NotRead_Resp obj, Object Sock);

	/**
	 * 
	 * @author wubo
	 * @createtime 2012-8-23
	 * @param flag
	 */
	public void connDown();

	public void setSP2File_Upload_Resp(SP2File_Upload_Resp obj);

	/**
	 * 
	 * @author wubo
	 * @time 2012-12-12
	 * @param obj
	 * 
	 */
	public void setSP2Client_Hello_ACK(SP2Client_Hello_ACK obj);
	/**
	 * 
	 * @param obj
	 */
	public void setSP2CRoom_Conf_Resp(SP2CRoom_Conf_Resp obj);
}
