package com.wachoo.pangker.music;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.util.Log;

/**
 * 
 * @author wangxin
 * 
 */
public class MusicPlayerManager {

	protected Map<String, MusicPlayerListener> listenerMap = new HashMap<String, MusicPlayerListener>();

	/**
	 * 添加一个Listener监听器
	 */
	public synchronized void addMusicPlayerListenter(MusicPlayerListener listener) {
		listenerMap.put(listener.getClass().getName(), listener);
	}
	
	public void exitManager() {
		// TODO Auto-generated method stub
        if(listenerMap != null){
        	listenerMap.clear();
        }
	}
   
	/**
	 * 移除一个已注册的Listener监听器. 如果监听器列表中已有相同的监听器listener1、listener2,
	 * 并且listener1==listener2, 那么只移除最近注册的一个监听器。
	 */
	public synchronized void removeMusicPlayerListenter(MusicPlayerListener listener) {
		listenerMap.remove(listener);
	}

	public void playerMusicAction(MusicPlayerEvent playerEvent) {
		MusicPlayerListener listener;

		// >>>>>>>>>通知栏

		Iterator<?> iter = listenerMap.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			listener = (MusicPlayerListener) entry.getValue();
			// 根据下载文件的地址
			listener.play(playerEvent.getInfo());

		}
	}

	public void playerErrorAction(MusicPlayerEvent playerEvent) {
		MusicPlayerListener listener;
		Iterator<?> iter = listenerMap.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			listener = (MusicPlayerListener) entry.getValue();
			// 根据下载文件的地址
			listener.error();
		}
	}

	public void playerPauseAction(MusicPlayerEvent playerEvent) {
		MusicPlayerListener listener;
		Iterator<?> iter = listenerMap.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			listener = (MusicPlayerListener) entry.getValue();
			// 根据下载文件的地址
			listener.pause(playerEvent.getInfo());
		}
	}

	public void playerOnBufferStatus(int status) {
		Log.i("MusicPlayActivity", ">>>>>playerOnBufferStatus");
		MusicPlayerListener listener;
		Iterator<?> iter = listenerMap.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			listener = (MusicPlayerListener) entry.getValue();
			Log.i("MusicPlayActivity", ">>>>>playerOnBufferStatus:Notice = " + listener);
			// 根据下载文件的地址
			listener.onBufferStatus(status);
		}
	}

	public void playeronBufferChanged(int percent) {
		MusicPlayerListener listener;
		Iterator<?> iter = listenerMap.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			listener = (MusicPlayerListener) entry.getValue();
			// 根据下载文件的地址
			listener.onBufferChanged(percent);
		}
	}

	public void playOver() {
		MusicPlayerListener listener;
		Iterator<?> iter = listenerMap.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			listener = (MusicPlayerListener) entry.getValue();
			// 根据下载文件的地址
			listener.playOver();
		}
	}

}
