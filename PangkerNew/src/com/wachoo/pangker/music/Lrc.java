package com.wachoo.pangker.music;

public class Lrc {

	private String lrcid;
	private String name;
	private String singer;
	
	
	public String getLrcid() {
		return lrcid;
	}
	public void setLrcid(String lrcid) {
		this.lrcid = lrcid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSinger() {
		return singer;
	}
	public void setSinger(String singer) {
		this.singer = singer;
	}
	
	
}
