package com.wachoo.pangker.audio;

import android.util.Log;

import com.wachoo.pangker.util.Util;

public class HelloThread extends Thread {

	private final String TAG = "HelloThread";
	private final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private final int CONNECT_MAX_RETRY_TIMES = 3;// >>>>>>>最多进行三次尝试连接
	private final int HELLO_SLEEP_TIME = 10000; // >>>>>>>>hello包发送时间间隔

	private int connectReTry = 0;// >>>>>>>当前尝试连接次数

	private boolean isHelloing = true; // >>>>>>>>是否允许标记
	private boolean isHasHelloResp = false; // >>>>>>>是否有hello的响应
	private boolean isConnected = false;//0:未连接， 1：连接中，2：已连接
	private OnHelloListener mOnHelloListener;

	// >>>>>>>>设置hello线程开启
	public HelloThread(OnHelloListener mOnHelloListener) {
		this.mOnHelloListener = mOnHelloListener;
	}

	/**
	 * hello包同时今天groupSP登陆
	 * @param result
	 */
	private void sendHelloPacket() {
		// >>>>>>hello尝试次数
		connectReTry++;
		// >>>判断心跳包是否有响应
		if (!isConnected || !isHasHelloResp) {
			Log.d("Hello", "sendHelloPacket>>>>Login-->" + isConnected +  "," + isHasHelloResp);
			if (mOnHelloListener != null) {
				mOnHelloListener.onLoginoutListener(true);
			}
			return;
		}
		// >>>正常发送心跳包
		Log.i("Hello", "JavaSock_Send>>>>>>>>,connectReTry:" + connectReTry);
		if (mOnHelloListener != null) {
			int num = mOnHelloListener.onHelloSendListener();
			if (!judgeSendBySockID(num, 0)) {
				Log.e("Hello", "JavaSock_Send>>>>>>>>" + Util.getSysNowTime() + ",Fail!!!");
				mOnHelloListener.onLoginoutListener(false);
			}
		}
		// >>>>>>>设置hello包,未接收
		setHasHelloResp(false);
	}

	@Override
	public void run() {
		try {
			while (isHelloing) {
				// >>>>>>>>>>如果心跳尝试三次失败。则通知用户群组断开！！
				if (connectReTry == CONNECT_MAX_RETRY_TIMES) {
					// >>>>此处发送离开群组的包已经没有任何意义了，连接已经断开
					if (mOnHelloListener != null) {
						mOnHelloListener.onLoginoutListener(false);
					}
					return;
				}
				sendHelloPacket();
				// 睡眠
				Thread.sleep(HELLO_SLEEP_TIME);
			}
		} catch (Exception e) {
			isConnected = false;
			logger.error(TAG, e);
		}
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public boolean isHelloing() {
		return isHelloing;
	}

	public void stopHelloing() {
		this.isHelloing = false;
		this.isConnected = false;
	}

	public boolean isHasHelloResp() {
		return isHasHelloResp;
	}

	public void setHasHelloResp(boolean HasHelloResp) {
		this.isHasHelloResp = HasHelloResp;
		// >>>>>>>>>如果服务器有响应则，重置尝试次数
		if (isHasHelloResp){
			connectReTry = 0;
		}
	}

	private boolean judgeSendBySockID(int num, int sendFlag) {
		// >>>>>>>如果NUM = -1 则连接中断，为失效的！！！
		if (num < 0) {
			Log.e(TAG, "发包失败  ..." + num + " |||sendFlag = " + sendFlag);
			isConnected = false;
			setHasHelloResp(false);
			// TODO 提示用户 与服务器的连接中断 对发包失败的事件做出回应 ,改变一些状态
			return false;
		}
		return true;
	}

	public interface OnHelloListener {

		public int onHelloSendListener();

		public void onLoginoutListener(boolean isLogin);
	}
}
