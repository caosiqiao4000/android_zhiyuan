package com.wachoo.pangker.audio;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import com.bambuser.broadcaster.AmrEncoder;
import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.group.CP2SP_Stream;
import com.wachoo.pangker.group.IRoomMessageSender;
import com.wachoo.pangker.group.P2PFarther;
import com.wachoo.pangker.util.ZlibUtil;

/**
 * @author wubo
 * @createtime Apr 1, 2012
 */
public class AudioRecordThread extends Thread {

	private String TAG = "AudioRecordThread";// log tag
	private final Logger logger = LoggerFactory.getLogger();
	public static final boolean flag_compress = false;//是否进行压缩
	private final int SampleRateInHz = 8000;//采样率
	private final int MAX_LENGHT = 2560;// 录音写进的数据长度
	private final int UNIT_LENGHT = 32; //每次发送的语音包字节数长度
	private final int READ_LENGHT = 320;//每次读取的长度
	private final int MAXCOUNT = 20;//最大的采集次数限制和MAX_LENGHT，UNIT_LENGHT有关
	private P2PFarther farther = new P2PFarther();
	private AmrEncoder mAmrEncoder = new AmrEncoder();
	
	private AudioRecord mAudioRecorder;// 录音
	private boolean ifsendArm = false;//是否发送录音数据byte[]
	private CP2SP_Stream cs;//
	private IRoomMessageSender context;//发送接口

	private ByteArrayOutputStream bos = new ByteArrayOutputStream();
	private ByteArrayInputStream ais;
	private byte[] result;//发送转换之后的byte数据
	private boolean isRecoding = true;// >>>>>>>是否录音

	private int maxTime;//发言的最大时长
	private TimingTask timingTask = null;//发言的时长计时器
	
	public boolean isRecoding() {
		return isRecoding;
	}

	public void stopRecoding() {
		this.isRecoding = false;
		context.showRecordTime(maxTime);
	}

	public AudioRecordThread(CP2SP_Stream cs, IRoomMessageSender cont, int maxTime) {
		this.context = cont;
		this.cs = cs;
		this.maxTime = maxTime;
		cs.setS_Type(MeetRoom.s_type_amr);
		ifsendArm = false;
		isRecoding = true;
	}

	public void run() {
		try {
			int count = 0;//采集数据的次数
			int messId = 0;//对应的单个音乐流的MessageId
			/**
			 * 每次read最少的字节数
			 */
			int min = AudioRecord.getMinBufferSize(SampleRateInHz, AudioFormat.CHANNEL_IN_MONO,
					AudioFormat.ENCODING_PCM_16BIT);
			int frameSize = Math.max(MAX_LENGHT, min) * 4;
			if (mAudioRecorder != null) {
				mAudioRecorder.release();
			}
			// 录音
			mAudioRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SampleRateInHz,
					AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, frameSize);
			int read = 0;
			byte[] arrayOfByte1 = new byte[MAX_LENGHT];// 录音写进的数据
			byte[] arrayOfByte2 = new byte[UNIT_LENGHT];//每次发送的语音包字节数 
			mAudioRecorder.startRecording();

			while (isRecoding) {
				read = mAudioRecorder.read(arrayOfByte1, 0, READ_LENGHT);
				if (read < 0){
					continue;
				}
				count++;
				mAmrEncoder.encode(arrayOfByte1, 0, read, arrayOfByte2, AmrEncoder.MR475);
				bos.write(arrayOfByte2);
				if (count >= MAXCOUNT) {
					// 发送数据包
					if(flag_compress){
						byte[] sendBytes = ZlibUtil.compress(bos.toByteArray());
						bos.reset();
						count = 0;
						cs.setPayLoadLen(sendBytes.length);
						cs.setPayLoad(sendBytes);
						result = farther.getCP2SP_Stream(cs, new byte[2048]);
						
						//获取信息头长度，默认16
						int head_length = farther.getMsgHead_Size();
						byte[] compress = null;
						byte[] head = new byte[head_length];
						//获取消息体和语音数据流，待压缩的源内容
						byte[] bMsg_struct_payload = new byte[result.length - head_length];
						ais = new ByteArrayInputStream(result);
						//将流读取消息头
						ais.read(head);
						////获取消息体和语音数据
						ais.read(bMsg_struct_payload);
						//压缩获取消息体和语音数据
						compress = ZlibUtil.compress(bMsg_struct_payload);
						bos.write(head);
						bos.write(compress);
						result = bos.toByteArray();
						result = farther.setMsg_Length(result, (short) compress.length);
						bos.reset();
						if (result != null) {
							context.setSendAmr(result);
						}
					} else {
						byte[] sendBytes = bos.toByteArray();
						bos.reset();
						count = 0;
						cs.setPayLoadLen(sendBytes.length);
						cs.setPayLoad(sendBytes);
						cs.setMsgId(messId);
						result = farther.getCP2SP_Stream(cs, new byte[2048]);
						
						short SendTotal = (short) (result.length - 16);
						result = farther.setMsg_Length(result,  SendTotal);
						if (result != null && ifsendArm) {
							messId ++;
							Log.d("sendVoice", "=====>setSendAmr messId->" + cs.getMsgId());
							context.setSendAmr(result);
						}
					}
				}
			}
			mAudioRecorder.stop();// 停止录音
			mAudioRecorder.release();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
		}
	}
	
	public void startSendArm(boolean isAdmin) {
		// TODO Auto-generated method stub
		ifsendArm = true;
		//如果不是群主，便开启时间限制的线程
		if(!isAdmin){
			timingTask = new TimingTask();
			timingTask.start();
		}
	}
	
	public boolean isIfsendArm() {
		return ifsendArm;
	}

	class TimingTask extends Thread{
		@Override
		public void run() {
			while (isRecoding && maxTime >= 0) {
				context.showRecordTime(maxTime);
				maxTime--;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					context.stopAudioRecode();
					logger.error(TAG, e);
					return;
				}
				if (maxTime == 0) {
					context.stopAudioRecode();
				}
			}
		}
	}

}
