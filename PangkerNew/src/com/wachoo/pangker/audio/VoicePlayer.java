package com.wachoo.pangker.audio;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;

import com.wachoo.pangker.util.Util;

public class VoicePlayer {

	private MediaPlayer mPlayer;
	private static VoicePlayer voicePlayer;
	
	private VoicePlayer() {
		// TODO Auto-generated constructor stub
	}
	
	
	public static VoicePlayer getVoicePlayer() {
		if(voicePlayer == null){
			voicePlayer = new VoicePlayer();
		}
		return voicePlayer;
	}

	final MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
		@Override
		public void onCompletion(MediaPlayer mp) {
			// TODO Auto-generated method stub
			stop();
		}
	};
	
	private OnVoiceListener onVoiceListener;

	public void play(String source, OnVoiceListener onVoiceListener) {
		if(this.onVoiceListener != null){
			this.onVoiceListener.onVoiceStatusListener(0);
			this.onVoiceListener = null;
		}
		this.onVoiceListener =  onVoiceListener;
		if(!Util.isExitFileSD(source)){
			this.onVoiceListener.onVoiceStatusListener(-1);
			return;
		}
		try {
			MediaPlayer player = new MediaPlayer();
			player.setOnCompletionListener(onCompletionListener);
			player.setDataSource(source);
			player.prepare();
			player.start();
			if (mPlayer != null) {
				mPlayer.stop();
				mPlayer.release();
			}
			mPlayer = player;
			new VoiceChageTask().execute();
		} catch (Exception e) {
			Log.w("VoicePlayer", "error loading sound for " + e);
		}
	}

	public void stop() {
		if(mPlayer != null){
			mPlayer.stop();
	        mPlayer.release();
	        mPlayer = null;
		}
        if(this.onVoiceListener != null){
			this.onVoiceListener.onVoiceStatusListener(0);
			this.onVoiceListener = null;
		}
	}
	
	class VoiceChageTask extends AsyncTask<Void, Integer, Void>{
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			int i = 1;
			while (mPlayer != null && onVoiceListener != null) {
				try {
					if(i > 3){
						i = 1;
					}
					publishProgress(i);
					Thread.sleep(200);
					i++;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			if(onVoiceListener != null){
				onVoiceListener.onVoiceStatusListener(values[0]);
			}
		}
		
	} 
	
	public interface OnVoiceListener{
		public void onVoiceStatusListener(int state) ;
	}
}
