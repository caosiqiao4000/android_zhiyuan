package com.wachoo.pangker.audio;

import java.io.File;

import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.util.Log;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.util.Util;

public class VoiceRecorder {

	private final int AUDIO_TIMIE_MIN = 1;// 最小语音时长（单位:秒）
	private final int AUDIO_TIMIE_MAX = 300;// 最大语音时长（单位:秒）
	static final private double EMA_FILTER = 0.6;
	private MediaRecorder mRecorder = null;
	private double mEMA = 0.0;
	private File audioFile;
	private OnAudioStateListener onAudioStateListener;
	private boolean isRecording = false;

	public VoiceRecorder(OnAudioStateListener onAudioStateListener) {
		super();
		this.onAudioStateListener = onAudioStateListener;
	}

	private boolean isStart = false;

	public boolean isStart() {
		return isStart;
	}

	public void setStart(boolean isStart) {
		this.isStart = isStart;
	}

	private long start_audio_time;

	/**
	 * 要判断Sd卡不可用，是不能使用该功能的!
	 */
	public void startRecord() {
		// TODO Auto-generated method stub
		if (!Util.isExistSdkard()) {
			if (onAudioStateListener != null) {
				onAudioStateListener.onAudioStateListener(-1, null, 0);
			}
			return;
		}
		isStart = true;
		if (mRecorder == null) {
			mRecorder = new MediaRecorder();
			mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			mRecorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
			mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			mRecorder.setAudioChannels(1);
			// 接下来我们需要指定录制后文件的存储路径
			File fpath = new File(PangkerConstant.DIR_SENDVOICE);
			if (!fpath.exists()) {
				fpath.mkdirs();
			}
			try {
				audioFile = File.createTempFile(Util.getNowTime(), ".amr", fpath);
				mRecorder.setOutputFile(audioFile.getAbsolutePath());
				mRecorder.prepare();
				mRecorder.start();
				mEMA = 0.0;

				isRecording = true;
				start_audio_time = System.currentTimeMillis();
				new RecordTask().execute();
				if (onAudioStateListener != null) {
					onAudioStateListener.onAudioStateListener(0, null, 0);
				}
			} catch (Exception e) {
				if (mRecorder != null) {
					isRecording = false;
					mRecorder.stop();
					mRecorder.release();
					mRecorder = null;
				}
				if (onAudioStateListener != null) {
					onAudioStateListener.onAudioStateListener(3, null, 0);
				}
			}
		}
	}

	class RecordTask extends AsyncTask<Void, Integer, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			while (isRecording) {
				publishProgress(getAmplitude());
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					stopRecord();
				}
				if (System.currentTimeMillis() - start_audio_time >= 1000 * AUDIO_TIMIE_MAX) {
					stopRecord();
				}
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			if (onAudioStateListener != null) {
				onAudioStateListener.onAudioRecordListener(values[0]);
			}
		}
	}

	public boolean isRecording() {
		return isRecording;
	}

	public void stopRecord() {
		// TODO Auto-generated method stub
		if (!isStart)
			return;
		if (mRecorder != null) {
			isRecording = false;
			mRecorder.stop();
			mRecorder.release();
			mRecorder = null;
		}
		int audio_time = (int) ((System.currentTimeMillis() - start_audio_time) / 1000);
		Log.d("AudioMeter", "stopRecord:" + audio_time);
		if (audio_time < AUDIO_TIMIE_MIN) {
			if (audioFile.exists()) {
				audioFile.delete();
			}
			if (onAudioStateListener != null) {
				onAudioStateListener.onAudioStateListener(1, null, 0);
			}
		} else {
			if (onAudioStateListener != null) {
				onAudioStateListener.onAudioStateListener(2, audioFile.getAbsolutePath(), audio_time);
			}
		}
	}

	private int getAmplitude() {
		if (mRecorder != null)
			return (int) (mRecorder.getMaxAmplitude() / 2700.0);
		else
			return 0;

	}

	public double getAmplitudeEMA() {
		double amp = getAmplitude();
		mEMA = EMA_FILTER * amp + (1.0 - EMA_FILTER) * mEMA;
		return mEMA;
	}

	public interface OnAudioStateListener {
		/**
		 * @param status
		 *            -1:Sd卡不存在，0:开始录音；1：时间太短，2：录音成功，3：异常失败;
		 * @param file
		 *            文件地址
		 * @param record
		 *            录音长度,
		 */
		public void onAudioStateListener(int status, String file, int record);

		/**
		 * 
		 * @param volume
		 */
		public void onAudioRecordListener(int volume);
	}
}
