package com.wachoo.pangker.image;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
/**
 * 下载并保存到本地相册的Task
 * @author zxx
 */
public class ImgsaveTask extends AsyncTask<String, Void, Boolean>{

	private String TAG = "LrcLoader";// log tag
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private IResDao iResDao;
	private Context ctx;
	private File imgFile;
	
	public ImgsaveTask(Context ctx) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
		iResDao = new ResDaoImpl(ctx);
		File imgDir = new File(PangkerConstant.DIR_PICTURE);
		if (!imgDir.exists()) {
			imgDir.mkdirs();
		}
		imgFile = new File(PangkerConstant.DIR_PICTURE + "/" + System.currentTimeMillis() + ".jpg");
	}
	
	private boolean loaderImage(String imgurl) {
		InputStream inputStream = null;
		FileOutputStream fos = null;
		BufferedReader br = null;
		try {
			URL indexUrl = new URL(imgurl);
			HttpURLConnection httpUrlConnection = (HttpURLConnection) indexUrl.openConnection();
			httpUrlConnection.setDoOutput(true);
			httpUrlConnection.setConnectTimeout(5000);
			httpUrlConnection.setReadTimeout(5000);
			httpUrlConnection.connect();
			inputStream = httpUrlConnection.getInputStream();

			// 如果Sd卡存在，先保存在解析，否则直接解析
			if (!imgFile.exists()) {
				imgFile.createNewFile();
			}
			fos = new FileOutputStream(imgFile);
			byte[] buf = new byte[1204];
			int len = 0;
			while ((len = inputStream.read(buf)) != -1) {
				fos.write(buf, 0, len);
			}
			fos.flush();
			// 保存之后对文件进行解析
			return true;

		} catch (Exception e) {
			logger.error(TAG, e);
			try {
				if (inputStream != null) {
					inputStream.close();
					inputStream = null;
				}
				if (fos != null) {
					fos.flush();
					fos.close();
					fos = null;
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				logger.error(TAG, e1);
			}
			return false;
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
					inputStream = null;
				}
				if (br != null) {
					br.close();
					br = null;
				}
				if (fos != null) {
					fos.flush();
					fos.close();
					fos = null;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(TAG, e);
			}
		}
	}
	
	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub
		return loaderImage(params[0]);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(result && saveResAsLoacl()){
			insertImage();
			Toast.makeText(ctx, "图片保存成功!", Toast.LENGTH_SHORT).show();
		}
	}
	
	private void insertImage() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		Uri uri = Uri.fromFile(imgFile);
		intent.setData(uri);
		ctx.sendBroadcast(intent);
	}
	
	private boolean saveResAsLoacl() {
		// TODO Auto-generated method stub
		DirInfo dirInfo = new DirInfo();
		dirInfo.setPath(imgFile.getAbsolutePath());
		dirInfo.setDeal(DirInfo.DEAL_SELECT);
		PangkerApplication app = (PangkerApplication) ctx.getApplicationContext();
		String userId = app.getMyUserID();
		dirInfo.setUid(userId);
		dirInfo.setIsfile(DirInfo.ISFILE);
		dirInfo.setFileCount((int) imgFile.length());
		dirInfo.setResType(PangkerConstant.RES_PICTURE);
		dirInfo.setRemark(0);
		if (!iResDao.isSelected(userId, dirInfo.getPath(), dirInfo.getResType())) {
			return iResDao.saveSelectResInfo(dirInfo);
		}
		return false;
	}

}
