package com.wachoo.pangker.image;

import android.app.Activity;
import android.graphics.Bitmap;

import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class MusicoverLoader extends ImageLoader<Integer> {

	private static MusicoverLoader musicoverLoader;

	public static MusicoverLoader getMusicoverLoader() {
		if (musicoverLoader == null) {
			musicoverLoader = new MusicoverLoader();
		}
		return musicoverLoader;
	}

	private MusicoverLoader() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Bitmap getBitmap(Integer musicId, Activity activity) {
		// TODO Auto-generated method stub
		String filePath = Util.getAlbumArt(activity, musicId);
		if (filePath == null) {
			return null;
		}

		return ImageUtil.decodeFile(filePath, PKIconLoader.iconSideLength, PKIconLoader.iconSideLength
				* PKIconLoader.iconSideLength);
	}
}
