package com.wachoo.pangker.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.wachoo.pangker.util.ImageUtil;

public class ImageLocalFetcher extends ImageWorker {

	protected int mImageWidth;
	protected int mImageHeight;

	/**
	 * Set the target image width and height.
	 * 
	 * @param width
	 * @param height
	 */
	public void setImageSize(int width, int height) {
		mImageWidth = width;
		mImageHeight = height;
	}

	public ImageLocalFetcher(Context context, int sidelength) {
		super(context);
		setImageSize(sidelength, sidelength);
	}

	@Override
	protected Bitmap processBitmap(Object data, Object key) {
		// TODO Auto-generated method stub
		String imagelocalPath = String.valueOf(data);
		Log.d("ImageCache", "====>>>" + imagelocalPath);
		return ImageUtil.decodeFile(imagelocalPath, mImageWidth, mImageWidth * mImageWidth);
	}

}
