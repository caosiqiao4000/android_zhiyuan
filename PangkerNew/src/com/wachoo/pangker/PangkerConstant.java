package com.wachoo.pangker;

import java.io.File;

import android.os.Environment;

/**
 * pangker 常量
 * 
 * @author wangxin
 * 
 */
public class PangkerConstant {

	public static final String CONTENT_CHARSET = "UTF-8";
	public static final String CERTIFICATE_FILE_NAME = "certificate.conf";

	// 旁客的domian
	public static final String PANGKER_DOMAIN = "@pangker";
	public static final String PANGKER_RESOURCE = "Pangker";
	public static final String PANGKER_PACKAGE_NAME = "com.wachoo.pangker.activity";

	// 异步加载ListView使用常量
	public static final int UPDATE_LISTVIEW = 1025;
	public static final int ERROR = 1026;

	// 登录结果
	public static final String OF_LOGIN_SUCESS = "OF_LOGIN_SUCESS";// 成功
	public static final String OF_LOGIN_FAIL = "OF_LOGIN_FAIL";// 失败
	public static final String OF_LOGIN_NONETWORK = "OF_LOGIN_NONETWORK";// 失败
	public static final String OF_LOGIN_LOGDING = "OF_LOGIN_LOGDING";// 失败
	public static final String LOGIN_RESULT = "LOGIN_RESULT";// 登录结果
	public static final String ACTTION_LOGIN_RESULT = "ACTTION_LOGIN_RESULT";// 登录结果

	public static final String ACTTION_ONLINE_STATE = "ACTTION_ONLINE_STATE";// 当前在线状态广播
	public static final String NO_ONLINE = "NO_ONLINE";// 当前不在线广播
	public static final String ONLINE = "ONLINE";// 当前不在线广播
	public static final String ONLINE_STATE = "ONLINE_STATE";// 当前在线状态广播
	public static final String ACTTION_LOCATE_STATE = "ACTTION_LOCATE_STATE";// 定位的广播状态
	public static final String ACTTION_ICON = "ACTTION_ICON";// 定位的广播状态
	// Dialog及Message id
	public static final int SHOW_HELP = 8;
	public static final int SHOWDIALOG_LOGIN = 9;// 显示登录提示框
	public static final int SHOWDIALOG_REGISTER = 10;// 显示注册提示框
	public static final int SHOWDIALOG_LOGINNOTICE = 11;// 显示注册注意事项提示框
	public static final int MESSAGE_lOGIN = 12;// 登录
	public static final int MESSAGE_REGISTER_RETRY = 13;// 注册重试
	public static final int MESSAGE_REGISTER = 14;// 注册
	public static final int MESSAGE_START = 15;// 开启服务器
	public static final int MESSAGE_UPDATEUI = 16;// 更新界面信息
	public static final int MESSAGE_LOGINNOTICE = 17;
	public static final int MESSAGE_INITDATE = 18;
	public static final int MESSAGE_ONLINE = 19;
	public static final int MESSAGE_OFFLINE = 20;

	// 记录在共享数据中的字段
	public static final String ALREADY_LOGIN_KEY = "already_login";
	public static final String ACCOUNT_KEY = "login_account";
	public static final String PASSWORD_KEY = "login_password";
	public static final String CHATROOM_KEY = "isCourrentChatRoom";
	public static final String USERNAME_KEY = "user_nickname";// 昵称
	public static final String QIANMING_MSG = "qian_ming";// 个性签名
	public static final String CONTACTPHOTO_MSG = "contact_photo";// 头像
	public static final String REQURESTADDFRIENDINFO = "add_friendinfo";// 请求添加好友信息

	// 注册相关
	public static final int REG_RETRY_COUNT = 3; // 通过http获取用户名密码的总重试次数，包括发注册短信前的次数
	public static final int REG_RETRY_COUNT_BEFORE_SMS = 1; // 发注册短信前，通过http获取用户名密码的重试次数
	public static final int REG_RETRY_INTERVAL = 5000; // 注册重试的时间间隔,1000=1秒
	public static String SERVICE_NUM = "18620021696"; // 接收注册短信的短信网关接入号、或手机号
	public static String SERVICE_NUM_EMULATOR = "15555215554"; // 接收注册短信的短信网关接入号、或手机号，模拟器专用
	public static final int REG_SLEEP_TIME = 1000;// 验证是否注册成功睡眠时间

	// 用户在线状态变更相关 ------> 广播
	public static final String ACTTION_UPDATE_STATE = "action_update_state";
	public static final String ACTTION_UPDATE_ATTENT_STATE = "ACTTION_UPDATE_ATTENT_STATE";
	public static final String STATE_WHO_KEY = "state_who_key";
	public static final String STATE_WHO_VALUE_ME = "state_who_value_me";
	public static final String STATE_TYPE_KEY = "state_type_key";

	// 用户私信通知相关
	public static final String ACTTION_CHAT_HISTORY = "ACTTION_CHAT_HISTORY";
	public static final String CHAT_CHATHISTROY_ITEM = "CHAT_CHATHISTROY_ITEM";

	// 联系人变更后广播
	public static final String ACTTION_CONTACTS_CHANGED = "ACTTION_CONTACTS_CHANGED";

	public static final String TYPE_FRIENDS_UI_REFRESH = "01";// 好友界面刷新
	public static final String TYPE_ATTENTS_UI_REFRESH = "02";// 关注人界面刷新
	public static final String TYPE_FANS_UI_REFRESH = "03";// 粉丝界面刷新

	public static final String TYPE_CONTACTS_SUBSCRIBED = "01";// 联系人添加
	public static final String TYPE_CONTACTS_UNSUBSCRIBED = "02";// 联系人删除

	public static final int CONTACTS_OPE_ADD = 1;// 增加联系人
	public static final int CONTACTS_OPE_DEL = 2;// 删除联系人
	public static final String CONTACTS_OPE_TYPE = "CONTACTS_OPE_TYPE";

	// 消息通知广播
	public static final String ACTION_ADDRESSBOOK_EXIT = "ACTION_ADDRESSBOOK_EXIT";

	// 广播友踪组相关
	// public static final String ACTION_TRACEINVITE_NOTICE =
	// "ACTION_TRACEINVITE_NOTICE";
	// 漂移通知
	public static final int DRIFT_NOTICE = 0x2390;

	// 资源推荐的广播
	// public static final String ACTION_SENDFILE = "ACTION_SENDFILE";
	// 群组运行中
	public static final String ACTION_GROUPLOGIN = "ACTION_GROUPLOGIN";
	public static final String ACTION_GROUPLOGOUT = "ACTION_GROUPLOGOUT";

	// 用户消息广播标识----->广播
	public static final String CHATMESSAGE_VALUE_KEY = "CHATMESSAGE_VALUE_KEY";

	// 旁客用户在线状态，目前只包括上线和下线两种
	public static final int STATE_CHANGE_REFRESHUI = 10;// 上下线状态变更刷新ui
	public static final int USERINFO_CHANGE_REFRESHUI = 11;// 上下线状态变更刷新ui
	public static final int STATE_TYPE_NOTHING = 0;// NOTHING
	public static final int STATE_TYPE_CALLME = 1;// callme
	public static final int STATE_TYPE_ONLINE = 2;// online
	public static final int STATE_TYPE_BUSY = 3;// busy
	public static final int STATE_TYPE_LEAVE = 4;// leave
	public static final int STATE_TYPE_NODUSTURB = 5;// 请勿打扰
	public static final int STATE_TYPE_OFFLINE = 6;// offline
	public static final int STATE_TYPE_NODEFINE = 7;// 无定义状态，add by zhengjy
	public static final String STATE_KEY = "state_key";// 在线状态标志key名，用于activity间传递state值
	public static final int REFRESHUI_ADD_FRIEND = 21;// 好友增加
	public static final int REFRESHUI_DEL_FRIEND = 23;// 好友删除
	// ，add by zhengjy

	// 搜索后台接口key
	public static final int SEARCH_FRIENDS = 1;// SEARCH FRIENDS
	public static final int SEARCH_FOLLOWS = 2;// SEARCH FOLLOWS
	public static final int SEARCH_ATTENTS = 3;// SEARCH ATTENTS

	// 群组类别 // 12：我的好友，14：关注人，15：我的粉丝(粉丝无分组)
	public static final int GROUP_FRIENDS = 12;// FRIENDS
	public static final int GROUP_FOLLOWS = 15;// FOLLOWS
	public static final int GROUP_ATTENTS = 14;// ATTENTS
	public static final int GROUP_BLACKLIST = 99;// 黑名单
	// ：10:会议室，11：直播室，12：群组讨论组，13：友踪组
	public static final String GROUP_MEETING = "10";// 会议室

	public static final String GROUP_TRACES = "13";// TRACES

	// 群组操作 01：添加分组,02：删除分组,03:修改分组名称
	public static final String GROUP_CREATE = "01";// 01：添加分组
	public static final String GROUP_UPDATE = "03";// 03:修改分组名称
	public static final String GROUP_DELETE = "02";// 02：删除分组

	public static final String DEFAULT_GROUPID = "0";// 群组默认id

	// 本机数据保存标识
	public static final String MYSELF_DATA = "MYSELFDATA"; // 本机数据保存标识
	public static final String MYSELF_DATA_SAVE = "MYSELF_DATA_SAVE"; // 保存过
	public static final String MYSELF_DATA_NOSAVE = "MYSELF_DATA_NOSAVE";// 没保存过

	// 消息栏通知标识
	public static final int FLAG_ONGOING_EVENT = -1;// 群组默认id

	// 友踪组的个数
	public static final int TRACEGROUP_MAX = 10;
	// Date Format
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_PULL_FORMAT = "yyyy-MM-dd HH:mm";
	public static final String DATE_ACCURATE_FORMAT = "MM-dd HH:MM:mm.sss";
	public static final String NEAR_USER_ALL = "";// ALL USERS
	public static final String NEAR_USER_BOYS = "0";// BOYS
	public static final String NEAR_USER_GIRLS = "1";// GIRLS
	public static final String NEAR_USER_FIRENDS = "2";// FIRENDS
	// 男女标识，整型
	public static final int BOYS_USER = 0;// 男
	public static final int GIRLS_USER = 1; // 女
	public static final int OPPOSITE_SEX = 2; // 异性

	public static final String MY_TRACE_GROUPS_KEY = "10";
	public static final String JOIN_TRACE_GROUPS_KEY = "11";
	public static final String[] TRACE_GROUPS_NAME = { "我的友踪组", "我参加过的友踪组" };

	public static final String POP_MSGTALK_TYPE = "POP_MSGTALK_TYPE";// 周边行人
	public static final String POP_MSGTALK_TYPE_NEARUSER = "10";// 周边行人

	// 关系状态 "0":添加等待对方确认 "1"：好友关系 "99":无关系
	public static final String FRIEDN_RELATION_TYPE_ADDFRIEND = "0";
	public static final String FRIEDN_RELATION_TYPE_CONFIMFRIEND = "1";// 周边行人

	// 资源类型
	public static final String RES_NAME_SHARE = "我的分享";// 资源——分享
	public static final String RES_NAME_COLLECT = "我的收藏";// 资源——收藏

	// 资源类型
	public static final int RES_VOICE = 8;// 资源——语音
	public static final int RES_TOUCH = 9;// 资源——碰一下
	public static final int RES_SPEAK = 10;// 资源——文字
	public static final int RES_APPLICATION = 14;// 资源——应用
	public static final int RES_DOCUMENT = 11;// 资源——文档
	public static final int RES_MUSIC = 13; // 资源——音乐
	public static final int RES_PICTURE = 12;// 资源——图片
	public static final int RES_VIDEO = 16;// 资源——视频
	public static final int RES_BROADCAST = 15;// 资源——直播
	public static final int RES_HOME = 17;// 资源库
	public static final int RES_LOCATION = 18;// 资源库--位置信息，带地图
	public static final int RES_SPEAK_SMS = 19;// 通过短信发送私信
	public static final int RES_CALL = 20;// 呼叫消息
	public static final int RES_SIGN = 21;// 签名

	// public static final int RES_GROUP = 20;// 群组
	public static final int[] RES_SUM = { RES_DOCUMENT, RES_PICTURE, RES_MUSIC, RES_APPLICATION, RES_HOME };

	// 音乐列表
	public static String MUSIC_PLAYER_EXTRAL_PLAYLSTITEM = "playlstitem";
	public static String MUSIC_PLAYER_ADD = "from"; // false，表示来源于本地，true，表示来源于网络

	public static String MUSIC_PLAYER_PRE = "pre";
	public static String MUSIC_PLAYER_PLAY = "play";
	public static String MUSIC_PLAYER_NEXT = "next";
	public static String MUSIC_PLAYER_STOP = "stop";
	public static String MUSIC_PLAYER_POS = "pos";
	public static String MUSIC_PLAYER_SEEK = "seek";
	public static String MUSIC_PLAYER_MODE = "model";

	public static final int MPM_SINGLE_LOOP_PLAY = 0; // 单曲循环
	public static final int MPM_ORDER_PLAY = 1; // 顺序播放
	public static final int MPM_LIST_LOOP_PLAY = 2; // 列表循环
	public static final int MPM_RANDOM_PLAY = 3; // 随即播放

	// 对应SD卡文件
	static File SD_DIR = Environment.getExternalStorageDirectory();// 获取跟目录
	public static final String DIR_ROOT = SD_DIR + "/100pangker";
	public static final String DIR_DOWNLOAD = DIR_ROOT + "/download";// >>>>>>>下载保存目录
	public static final String DIR_UPLOAD = DIR_ROOT + "/upload";// >>>>>>>上传目录

	public static final String DIR_RECIVE = DIR_ROOT + "/receive";// >>>>>>>接收目录
	public static final String DIR_SEND = DIR_ROOT + "/send";// >>>>>>>发送目录

	// >>>>>>>>资源下载保存目录
	public static final String DIR_PICTURE = DIR_DOWNLOAD + "/photo";
	public static final String DIR_DOCUMENT = DIR_DOWNLOAD + "/document";
	public static final String DIR_MUSIC = DIR_DOWNLOAD + "/music";
	public static final String DIR_APP = DIR_DOWNLOAD + "/app";

	// >>>>>>>系统使用目录
	public static final String DIR_SYSTEM = DIR_ROOT + "/.system";
	// >>>>>>版本资源
	public static final String DIR_VERSION = DIR_SYSTEM + "/.version/";// >>>>>>>上传目录

	// >>>>>>>>>>>>用户头像目录
	public static final String DIR_ICON = DIR_ROOT + "/.icons/";

	// >>>>>>>>>发送图片（经过压缩的图片）私信等case使用
	public static final String DIR_SENDPIC = DIR_SEND + "/.photo/";

	public static final String DIR_SENDVOICE = DIR_SEND + "/.voice/";

	// >>>>>>>>上传图片，经过压缩的图片，如果没有压缩不保存
	public static final String DIR_UPLOADPIC = DIR_UPLOAD + "/photo/";

	public static final String DIR_REC_PIC = DIR_RECIVE + "/.photo"; // 接收文件的地址
	public static final String DIR_REC_APP = DIR_RECIVE + "/app"; // 接收文件的地址
	public static final String DIR_REC_MUSIC = DIR_RECIVE + "/music"; // 接收文件的地址
	public static final String DIR_REC_DOC = DIR_RECIVE + "/document"; // 接收文件的地址
	public static final String DIR_REC_VOICE = DIR_RECIVE + "/.voice"; // 接收文件的地址

	public static final String DIR_GROUPMSGPIC = DIR_RECIVE + "/groupmsg/.pic";// 群组聊天图片目录

	public static final int DIR_LIMIT = 9;// 目录限制，= DIR_LIMIT - 1；为8个
	// Intent传递参数
	public static final String INTENT_UESRITEM = "home_userinfo"; // 传递其界面操作的用户Id
	public static final String INTENT_RES_TYPE = "res_type"; // 传递其界面操作的用户Id
	public static final String INTENT_APPITEM = "home_appitem"; // 传递其界面操作的用户Id

	// 友踪组 滑动屏幕标记文字
	public static final String[] TRACEGROUP_VIEW_TITLE = { "地图", "群聊", "群信息" };

	public static final String BUSINESS_MESSAGE = "BUSINESS_MESSAGE"; // 联系人改变消息类型0：问答，1：资源，2：群组
	public static final String NEAR_FRIENDS_NOTICE_MESSAGE = "NEAR_FRIENDS_NOTICE_MESSAGE"; //
	public static final String CONTACTS_REGISTER_PHONNUM = "CONTACTS_CHANGED"; //
	public static final int OPTYPE_ASK = 0;
	public static final int OPTYPE_RES = 1;
	public static final int OPTYPE_GROUP = 2;

	// 12星座
	public static final String[] CONSTELLATIONS = { "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座",
			"摩羯座", "水瓶座", "双鱼座" };
	public static final int[] CONSTELLATION_KEY = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

	// 12生肖
	public static final String[] ZODIACS = { "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪" };
	public static final int[] ZODIAC_KEY = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

	public static final int NO_MAKEFRIEND_USER = 0;// 不是交友用户
	public static final int IS_MAKEFRIEND_USER = 1; // 交友用户

	public static final int MAKEFRIEND_TYPE_BOYS = 10;// 男孩
	public static final int MAKEFRIEND_TYPE_GIRLS = 11; // 女孩
	public static final int MAKEFRIEND_TYPE_OTHERS = 12;// 普通身份(异性)

	public static final String GROUP_TYPE_ALL = "2";// 所有人群
	public static final String GROUP_TYPE_FIXED = "0";// 固定群组
	public static final String GROUP_TYPE_MOVED = "1";// 移动群组
	public static final String SEACH_GROUPS_BYKEYS = "3";// 根据关键字搜索所有群组

	public static final String LOCATE_KEY = "LOCATE_KEY";
	public static final int LOCATE_CLIENT = 0;// 终端定位
	public static final int LOCATE_DRIFT = 1;// 不要定位。漂移模式

	public static final String NEAR_FRIEND_NOTICE_KEY = "NEAR_FRIEND_NOTICE_KEY";

	// public static final int START_DRIF_HANDLER = 0x001;
	// public static final int CANCEL_DRIF_HANDLER = 0x002;
	public static final String SERVICE_INTENT = "SERVICE_INTENT";// service
																	// Action
																	// key
	public static final String LOCATE_INTENT = "LOCATE_INTENT";// 定位意图
	public static final String AUTOLOCATE_INTENT = "AUTO_LOCATE_INTENT";// 自动定位意图
	public static final String LOGIN_IM_SERVER_THREAD_CHECK = "LOGIN_IM_SERVER_THREAD_CHECK";// 判断登陆线程是否运行

	// public static final String CONTACTS_BIND_INTENT =
	// "CONTACTS_BIND_INTENT";// 绑定通讯录意图
	// public static final String CONTACTS_BIND_KEY = "CONTACTS_BIND_KEY";//
	// 绑定通讯录意图
	// public static final String CONTACTS_BIND = "CONTACTS_BIND";// 绑定通讯录意图
	// public static final String CONTACTS_UNBIND = "CONTACTS_UNBIND";// 解绑通讯录意图
	public static final String INTENT_DRIFT = "INTENT_DRIFT";// 漂移意图
	public static final String INTENT_DRIFT_START = "DRIFT_START";// 启动漂移
	public static final String INTENT_UPLOAD = "INTENT_UPLOAD";// 上传意图
	public static final String INTENT_DOWNLOAD = "INTENT_DOWNLOAD";// 下载意图
	public static final String INTENT_VERSION = "INTENT_VERSION";// 版本检测
	public static final String INTENT_IMALIVE = "INTENT_IMALIVE";// IM线程停止 add
	public static final String INTENT_RES_INIT = "INTENT_RES_INIT";// 初始化顶层目录
	public static final String INTENT_PLAY_SOUND = "INTENT_PLAY_SOUND";// 播放声音的意图

	public static final String TIMING_LOCATE = "TIMING_LOCATE";// 定时定位意图
	// public static final String TIMING_LOCATE_OPEN = "TIMING_LOCATE_OPEN";//
	// 开启定时定位
	// public static final String TIMING_LOCATE_CLOSE = "TIMING_LOCATE_CLOSE";//
	// 关闭定时定位
	public static final String AUTOWIFI_LACATE_INTENT = "AUTOWIFI_LACATE_INTENT";// 自动Wifi定位意图
	public static final String AUTOWIFI_LACATE_CLOSE_INTENT = "AUTOWIFI_LACATE_CLOSE_INTENT";// 关闭自动Wifi定位意图
	public static final String WIFI_LACATE_INTENT = "WIFI_LACATE_INTENT";// Wifi手动定位意图

	public static final String INTENT_SENDFILE_OPERATE = "INTENT_SENDFILE_OPERATE";// 是否接收文件意图
	public static final String INTENT_SENDFILE_REC = "INTENT_SENDFILE_REC";// 接收
	public static final String INTENT_SENDFILE_REJ = "INTENT_SENDFILE_REJ";// 拒绝

	public static final String[] PIC_FORMAT = { "jpeg", "jpg", "png" };
	public static final String[] MUSIC_FORMAT = { "ogg", "mp3", "wav", "3gp", "aac" };// wma的不支持
	public static final String[] DOC_FORMAT = { "txt", "doc", "pdf" };

	public static final int SORT_BY_CREATETIME_DESC = 0;// 根据上传时间倒序排列
	public static final int SORT_BY_SCORE_DESC = 1;// 得分排序，高分在前，低分在后
	public static final int SORT_BY_COMMENTTIME_DESC = 2;// 评论次数 多在前，少在后
	public static final int SORT_BY_DOWNLOADTIMES_DESC = 3;// 下载次数

	public static final int TRACEIN_LIMIT = 35;// 友踪人数限制
	public static final String IS_BIND_KEY = "IS_BIND_KEY";

	/**
	 * 权限类型 10：允许任何人加入群组，11：需要身份验证加入群组, 12:不允许任何人加入 13: 允许好友加入 14:密码访问 15:区域访问
	 * 16:通过邀请方可加入 17：允许部分联系人加入群组
	 */
	public final static int Role_PUBLIC = 10;// >>>>>>允许任何用户访问
	public final static int Role_IDENTITYCHECK = 11;// >>>>>>>>身份验证访问 （暂时不使用）
	public final static int Role_PRIVATE = 12;// >>>>>只允许自己访问
	public final static int Role_FRIEND = 13;// >>>允许所有好友
	public final static int Role_PASSWORD = 14;// >>>>输入密码访问
	public final static int Role_AREA = 15;// >>>区域性访问 （暂时不使用）
	public final static int Role_INVITE = 16;// >>>>>邀请访问 （暂时不适用）
	public final static int Role_CONTACT_GROUP = 17;// >>>>>指定联系人分组
	public final static int Role_RELATIVES = 18;// >>>>>允许亲友访问
	public final static int Role_GROUPSANDPWD = 19;// >>>>>>>>授权组+密码访问
	public final static int Role_MEMBERS = 20;// >>>>>>>>指定成员进行访问

	public static final int[] RES_VISIBLE_LOCK = { Role_PASSWORD };

	public static final int VISIBLELAYOUT = 1001;

	public static final String SEARCH_RANGE_DATA = "0";// 搜索附近的资源
	public static final String SEARCH_ALL_DATA = "1"; // 搜索全网资源

	public static final String AUTO_LOCATE_TIME_KEY = "AUTO_LOCATE_TIME_KEY";
	public static final String AUTO_LOCATE_WHICH_KEY = "AUTO_LOCATE_WHICH_KEY";
	public static final String WIFI_AUTO_LOCATE_TIME_KEY = "WIFI_AUTO_LOCATE_TIME_KEY";
	// wifi自动定位
	public static final String WIFI_LOCATE_KEY = "WIFI_LOCATE_KEY";
	// 运营商判断
	public static final int CHINA_MOBILE_TYPE = 0;
	public static final int CHINA_UNICOM_TYPE = 1;
	public static final int CHINA_TELECOM_TYPE = 2;

	public static final String CHINA_MOBILE_PORT = "106575557603028";
	public static final String CHINA_UNICOM_PORT = "106550200133028";// 联通上行端口
	public static final String CHINA_TELECOM_PORT = "1065902001055028";

	// 主界面imgSystem的区别标示
	public static final int SYS_NOTHING = 0;// 什么都不干
	public static final int SYS_MUSIC_PLAYING = 1;// 播放音乐
	public static final int SYS_GROUPING = 2;// 群组内，开启语音功能
	public static final int SYS_GROUP_MUSICPLAY = 3;// 群组内，暂停音乐功能

	public static final String APPID = "10000";

	public static final String PANGKER_REGEX = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";

	public static final int MAX_USERNAME_LENGTH = 18; // >>>>>>>>>>>用户名（昵称）最大长度限制

	// 设置本地数据库保存与userId相关Begin
	public static final String SP_SMS_SET_SEFI_OFFLINE = "SMS_SET_SEFI_OFFLINE";
	public static final String SP_SMS_SET_OTHER_OFFLINE = "SMS_SET_OTHER_OFFLINE";

	public static final String SP_SOUND_SET = "SOUND_SET";// 信息声音提示
	public static final String SP_SHOCK_SET = "SHOCK_SET";// 信息震动提示

	public static final String SP_DOWNLOAD_SET_SOUND = "dwonload_set_sound";// 下载声音提示
	public static final String SP_DOWNLOAD_SET_INSTALL = "dwonload_set_install";// 下载安装
	public static final String SP_DOWNLOAD_SET_NTE = "dwonload_set_net";// 下载设置
	public static final String SP_UPLOAD_SET_NTE = "upload_set_net";// 上传设置
	// public static final String SP_UPLOAD_SET_AUTO_TE =
	// "upload_set_auto_net";// 上传设置

	public static final String SP_MUSIC_SET_PLAY = "music_set_play";// 音乐播放
	public static final String SP_FRIEND_ADD = "sp_friend_add";// 处理好友申请

	public static final String SP_DRIFT_SET = "sp_drift_set";// 漂移提示
	public static final String SP_DRIFT_CAHANGE = "sp_drift_change";// 漂移改变提示
	public static final String IS_AGREEDMENT_CHECKED_KEY = "IS_AGREEDMENT_CHECKED_KEY";// 上传规则提示
	public static final String SP_WEB_SET = "sp_web_set";// 网站设置提示
	// 设置本地数据库保存与userId相关END
	public static final int TYPE_TRACE_relatives = 0x01; // 亲友选择
	public static final int TYPE_RES_recommend = 0x02; // 资源推荐
	public static final String INTENT_SELECT = "INTENT_SELECT";

	public static final String SP_NEARUSER_KEY = "user_key";
	public static final String SP_NEARGROUP_KEY = "group_key";
	public static final String SP_APP_KEY = "app_key";
	public static final String SP_PIC_KEY = "pic_key";
	public static final String SP_MUSIC_KEY = "music_key";
	public static final String SP_DOC_KEY = "doc_key";
	// >>>>>>>>时间分隔两分钟为时间分隔点
	public static final long SPLIT_TIME = 1000 * 60;

	// >>>>>>>>>防止被销毁的key
	public static final String PANGKER_LOCATE_TYPE_KEY = "PANGKER_LOCATE_TYPE_KEY";
	public static final String PANGKER_RESLIST_KEY = "PANGKER_RESLIST_KEY";
	public static final String PANGKER_MUSICPLAY_INEXE_KEY = "PANGKER_MUSICPLAY_INEXE_KEY";
	public static final String PANGKER_MUSICLIST_KEY = "PANGKER_MUSICLIST_KEY";
	public static final String PANGKER_ISBIND_PHONE_KEY = "PANGKER_ISBIND_PHONE_KEY";

	public static final String PANGKER_VIEW_RESINDEX_KEY = "PANGKER_VIEW_RESINDEX_KEY";

	// >>>>>>>保存上传访问数据key
	public static final String LAST_SHOW_DATA_KEY = "LAST_SHOW_DATA_KEY";

	public static final String SHOW_MORE_TEXT = "显示更多";
	public static final String SHOW_UP_TEXT = "收起";

	public static final int showLength = 200;

	// >>>>>>>>>>服务器时间差
	public static final String CLIENT_SERVER_TIMEDIFF = "CLIENT_SERVER_TIMEDIFF";
	public static final String CLIENT_GSP_TIMEDIFF = "CLIENT_GSP_TIMEDIFF";

	// >>>>>>>>>>>>上传头像的图片尺寸 用户头像 群组封面头像
	public static final int UPLOAD_IONC_SIDELENGTH = 320;
	// 联系人好友相关信息提示
	public static final String MSG_FRIEND_ADDAPPLY_SUECCESS = "已经申请添加对方为好友，请等待对方确认！";// 已经申请添加对方为好友，请等待对方确认
	public static final String MSG_FRIEND_ADDAPPLY_FAIL = "申请添加好友失败！";// 已经申请添加对方为好友，请等待对方确认
	public static final String MSG_FRIEND_ISEXIST = "已经存在好友关系！";// 已经存在好友关系

	public static final String MSG_FRIEND_GROUP_ISEXIST = "该用户已经在此好友组中！";// 该用户已经在此好友组中application.getFriendGroup()

	public static final String MSG_GROUP_NOHAVE = "您还没有好友分组";// 该用户已经在此好友组中application.getFriendGroup()

	public static final String MSG_NO_ONLINE = "您当前不在线，无法进行此操作";// 该用户已经在此好友组中application.getFriendGroup()

	// >>>>>>>>>>>>图片显示

	public static final int PANGKER_ICON_SIDELENGTH = 150;
	public static final int PANGKER_PHOTO_SIDELENGTH_MIN = 150;
	public static final int PANGKER_PHOTO_SIDELENGTH_MIDDLE = 250;
	public static final int PANGKER_PHOTO_SIDELENGTH_MAX = 360;
	public static final int PANGKER_PHOTO_SIDELENGTH_HD = 600;
}
