package com.wachoo.pangker.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.util.ImageUtil;

public class ContactLetterListView extends View {

	final String[] letters = { "#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
			"O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

	private OnTouchingLetterChangedListener onTouchingLetterChangedListener;

	int choose = -1;
	Paint paint = new Paint();
	boolean showBkg = false;
	Context context;
	PangkerApplication application;
	int size = 0;

	public ContactLetterListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;

		init();
	}

	public ContactLetterListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		init();
	}

	public ContactLetterListView(Context context) {
		super(context);
		this.context = context;

		init();
	}

	private void init() {
		paint.setColor(0xffffffff);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setFakeBoldText(true);
		paint.setAntiAlias(true);
		application = (PangkerApplication) context.getApplicationContext();

		// >>>>>>>>计算文字大小
		float density = ImageUtil.getDensity((Activity)getContext());
		if (density > 0.75f && density <= 1.0f) {
			size = 2;
		} else if (density > 1.0f && density <= 1.5f) {
			size = 2;
		} else if (density > 1.5f) {
			size = 1;
		}
	}

	private int getSize(float height, int width) {
		int textsieze = (int) (height < width ? height - size : width - size);
		Log.d("textsieze", "textsieze:" + textsieze + "," + height + "," + width + "," + size);
		return textsieze;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (showBkg) {
			canvas.drawColor(Color.parseColor("#40000000"));
		}
		int width = getWidth() - getPaddingLeft() - getPaddingRight();
		int height = getHeight() - getPaddingTop() - getPaddingBottom();
		float seperateHeight = (float) height / letters.length - .3f;
		int textsieze = getSize(seperateHeight, width);
		for (int i = 0; i < letters.length; i++) {
			paint.setColor(Color.GRAY);
			paint.setTypeface(Typeface.MONOSPACE);
			paint.setAntiAlias(true);
			if (i == choose) {
				paint.setColor(Color.parseColor("#3399ff"));
				paint.setTextAlign(Align.LEFT);
				paint.setFakeBoldText(true);
			}
			float xPos = width / 2 - paint.measureText(letters[i]) / 2;
			float yPos = seperateHeight * i + seperateHeight;
			paint.setTextSize(textsieze);
			canvas.drawText(letters[i], xPos, yPos, paint);
			paint.reset();
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		int action = event.getAction();
		float y = event.getY();
		int oldChoose = choose;
		int c = (int) (y / getHeight() * letters.length);

		switch (action) {
		case MotionEvent.ACTION_DOWN:
			showBkg = true;
			if (oldChoose != c && onTouchingLetterChangedListener != null) {
				if (c >= 0 && c < letters.length) {
					onTouchingLetterChangedListener.onTouchingLetterChanged(letters[c]);
					choose = c;
					invalidate();
				}
			}
			break;
		case MotionEvent.ACTION_MOVE:
			if (oldChoose != c && onTouchingLetterChangedListener != null) {
				if (c >= 0 && c < letters.length) {
					onTouchingLetterChangedListener.onTouchingLetterChanged(letters[c]);
					choose = c;
					invalidate();
				}
			}
			break;
		case MotionEvent.ACTION_UP:
			showBkg = false;
			choose = -1;
			invalidate();
			break;
		}
		return true;
	}

	public void setOnTouchingLetterChangedListener(
			OnTouchingLetterChangedListener onTouchingLetterChangedListener) {
		this.onTouchingLetterChangedListener = onTouchingLetterChangedListener;
	}

	public interface OnTouchingLetterChangedListener {
		public void onTouchingLetterChanged(String s);
	}

}
