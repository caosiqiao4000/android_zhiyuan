package com.wachoo.pangker.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import android.content.Context;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;

public class EmoView extends LinearLayout implements OnClickListener {

	// public static final int[] gridArray = { R.drawable.p001, R.drawable.p002,
	// R.drawable.p003, R.drawable.p004, R.drawable.p005, R.drawable.p006,
	// R.drawable.p007, R.drawable.p008, R.drawable.p009, R.drawable.p010,
	// R.drawable.p011, R.drawable.p012, R.drawable.p013, R.drawable.p014,
	// R.drawable.p015, R.drawable.p016, R.drawable.p017, R.drawable.p018,
	// R.drawable.p019, R.drawable.p020 ,R.drawable.p001, R.drawable.p002,
	// R.drawable.p003, R.drawable.p004, R.drawable.p005, R.drawable.p006,
	// R.drawable.p007, R.drawable.p008, R.drawable.p009, R.drawable.p010,
	// R.drawable.p011, R.drawable.p012, R.drawable.p013, R.drawable.p014,
	// R.drawable.p015, R.drawable.p016, R.drawable.p017, R.drawable.p018,
	// R.drawable.p019, R.drawable.p020 };
	public static final int[] gridArray = { R.drawable.smiley_0, R.drawable.smiley_1, R.drawable.smiley_2,
			R.drawable.smiley_3, R.drawable.smiley_4, R.drawable.smiley_5, R.drawable.smiley_6, R.drawable.smiley_7,
			R.drawable.smiley_8, R.drawable.smiley_9, R.drawable.smiley_10, R.drawable.smiley_11, R.drawable.smiley_12,
			R.drawable.smiley_13, R.drawable.smiley_14, R.drawable.smiley_15, R.drawable.smiley_16,
			R.drawable.smiley_17, R.drawable.smiley_18, R.drawable.smiley_19, R.drawable.smiley_20,
			R.drawable.smiley_21, R.drawable.smiley_22, R.drawable.smiley_23, R.drawable.smiley_24,
			R.drawable.smiley_25, R.drawable.smiley_26, R.drawable.smiley_27, R.drawable.smiley_28,
			R.drawable.smiley_29, R.drawable.smiley_30, R.drawable.smiley_31, R.drawable.smiley_32,
			R.drawable.smiley_33, R.drawable.smiley_34, R.drawable.smiley_35, R.drawable.smiley_36,
			R.drawable.smiley_37, R.drawable.smiley_38, R.drawable.smiley_39, R.drawable.smiley_40,
			R.drawable.smiley_41, R.drawable.smiley_42, R.drawable.smiley_43, R.drawable.smiley_44,
			R.drawable.smiley_45, R.drawable.smiley_46, R.drawable.smiley_47, R.drawable.smiley_48,
			R.drawable.smiley_49, R.drawable.smiley_50, R.drawable.smiley_51, R.drawable.smiley_52,
			R.drawable.smiley_53, R.drawable.smiley_54, R.drawable.smiley_55, R.drawable.smiley_56,
			R.drawable.smiley_57, R.drawable.smiley_58, R.drawable.smiley_59, R.drawable.smiley_60,
			R.drawable.smiley_61, R.drawable.smiley_62, R.drawable.smiley_63, R.drawable.smiley_64,
			R.drawable.smiley_65, R.drawable.smiley_66, R.drawable.smiley_67, R.drawable.smiley_68,
			R.drawable.smiley_69, R.drawable.smiley_70, R.drawable.smiley_71, R.drawable.smiley_72,
			R.drawable.smiley_73, R.drawable.smiley_74, R.drawable.smiley_75, R.drawable.smiley_76,
			R.drawable.smiley_77, R.drawable.smiley_78, R.drawable.smiley_79, R.drawable.smiley_80,
			R.drawable.smiley_81, R.drawable.smiley_82, R.drawable.smiley_83, R.drawable.smiley_84,
			R.drawable.smiley_85, R.drawable.smiley_86, R.drawable.smiley_87, R.drawable.smiley_88,
			R.drawable.smiley_89 };
	private ViewPager viewPager;
	private LinePageIndicator linePageIndicator;
	private GridViewAdapter pagerAdapter;
	private OnEmoClickListener onEmoClickListener;

	public EmoView(Context context) {
		this(context, null);
	}

	public EmoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		View view = LayoutInflater.from(context).inflate(R.layout.emotion_layout, null);
		LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
		addView(view, localLayoutParams);
		viewPager = (ViewPager) view.findViewById(R.id.pager);

		// emoPopupWindow = new EmoPopupWindow(getContext());
		linePageIndicator = (LinePageIndicator) view.findViewById(R.id.indicator);
		initPagerAdapter();
	}

	public void setOnEmoClickListener(OnEmoClickListener onEmoClickListener) {
		this.onEmoClickListener = onEmoClickListener;
	}

	public void showEmoView() {
		// TODO Auto-generated method stub
		setVisibility(View.VISIBLE);
		viewPager.setCurrentItem(0);
	}

	private void initPagerAdapter() {
		// TODO Auto-generated method stub
		List<View> mListViews = new ArrayList<View>();
		int count = (gridArray.length + 20) / 21;
		for (int i = 0; i < count; i++) {
			GridView gdView = new GridView(getContext());
			gdView.setPadding(3, 3, 0, 0);
			gdView.setGravity(Gravity.CENTER);
			gdView.setHorizontalSpacing(2);
			gdView.setVerticalSpacing(2);
			gdView.setColumnWidth(40);
			gdView.setNumColumns(7);
			gdView.setStretchMode(2);
			int[] dgArray = ArrayUtils.subarray(gridArray, i * 21, i == (count - 1) ? gridArray.length : (i + 1) * 21);
			EomotionAdapter eomotionAdapter = new EomotionAdapter(i, dgArray);
			gdView.setAdapter(eomotionAdapter);
			// 添加view
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.FILL_PARENT);
			gdView.setLayoutParams(lp);
			mListViews.add(gdView);
		}
		pagerAdapter = new GridViewAdapter(mListViews);
		viewPager.setAdapter(pagerAdapter);
		linePageIndicator.setViewPager(viewPager);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (onEmoClickListener != null) {
			onEmoClickListener.onEmoClickListener(v, (Integer) v.getTag());
		}
	}

	class EmoPopupWindow extends PopupWindows {

		private int rootWidth = 0;
		private LinearLayout scorLayout;
		private ImageView ivEmo;
		private TextView txtEmo;
		private View mRootView;
		private ImageView mArrowUp;
		private ImageView mArrowDown;

		// private LayoutInflater mInflater;

		public EmoPopupWindow(Context context) {
			super(context);
		}

		/**
		 * Show arrow
		 * 
		 * @param whichArrow
		 *            arrow type resource id
		 * @param requestedX
		 *            distance from left screen
		 * @return
		 */
		private int showArrow(int whichArrow, int requestedX) {
			final View showArrow = (whichArrow == R.id.arrow_up) ? mArrowUp : mArrowDown;
			final View hideArrow = (whichArrow == R.id.arrow_up) ? mArrowDown : mArrowUp;

			final int arrowWidth = mArrowUp.getMeasuredWidth();

			showArrow.setVisibility(View.VISIBLE);

			ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) showArrow.getLayoutParams();

			param.leftMargin = requestedX - arrowWidth / 2;

			hideArrow.setVisibility(View.INVISIBLE);
			return arrowWidth / 2;
		}

		/**
		 * Show quickaction popup. Popup is automatically positioned, on top or
		 * bottom of anchor view.
		 * 
		 */
		public void show(View anchor, int emoId) {
			preShow();
			txtEmo.setText("/表情");
			ivEmo.setImageResource(emoId);
			int xPos, yPos, arrowPos;

			int[] location = new int[2];

			anchor.getLocationOnScreen(location);

			Rect anchorRect = new Rect(location[0], location[1], location[0] + anchor.getWidth(), location[1]
					+ anchor.getHeight());

			// mRootView.setLayoutParams(new
			// LayoutParams(LayoutParams.WRAP_CONTENT,
			// LayoutParams.WRAP_CONTENT));

			mRootView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

			int rootHeight = mRootView.getMeasuredHeight();

			if (rootWidth == 0) {
				rootWidth = mRootView.getMeasuredWidth();
			}

			int screenWidth = mWindowManager.getDefaultDisplay().getWidth();
			int screenHeight = mWindowManager.getDefaultDisplay().getHeight();

			// automatically get X coord of popup (top left)
			if ((anchorRect.left + rootWidth) > screenWidth) {
				xPos = anchorRect.left - (rootWidth - anchor.getWidth());
				xPos = (xPos < 0) ? 0 : xPos;

				arrowPos = anchorRect.centerX() - xPos;

			} else {
				if (anchor.getWidth() > rootWidth) {
					xPos = anchorRect.centerX() - (rootWidth / 2);
					arrowPos = rootWidth / 2;
				} else {
					xPos = anchorRect.left;
					arrowPos = anchorRect.centerX() - xPos;
				}
			}

			int dyTop = anchorRect.top;
			int dyBottom = screenHeight - anchorRect.bottom;

			boolean onTop = (dyTop > dyBottom) ? true : false;

			if (onTop) {
				if (rootHeight > dyTop) {
					yPos = 15;
					android.view.ViewGroup.LayoutParams l = scorLayout.getLayoutParams();
					l.height = dyTop - anchor.getHeight();
				} else {
					yPos = anchorRect.top - rootHeight;
				}
			} else {
				yPos = anchorRect.bottom;

				if (rootHeight > dyBottom) {
					android.view.ViewGroup.LayoutParams l = scorLayout.getLayoutParams();
					l.height = dyBottom;
				}
			}

			showArrow(((onTop) ? R.id.arrow_down : R.id.arrow_up), arrowPos);

			mWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, xPos, yPos);
		}
	}

	// View .OnLongClickListener onLongClickListener = new
	// View.OnLongClickListener() {
	// @Override
	// public boolean onLongClick(View v) {
	// // TODO Auto-generated method stub
	// emoPopupWindow.show(v, gridArray[(Integer)v.getTag()]);
	// return true;
	// }
	// };

	class EomotionAdapter extends BaseAdapter {

		private int[] emotionArray;
		private int order;

		public EomotionAdapter(int order, int[] emotionArray) {
			super();
			this.emotionArray = emotionArray;
			this.order = order;
			Log.d("order", "order:" + order);
			for (int i = 0; i < emotionArray.length; i++) {
				Log.d("order", "=>" + emotionArray[i]);
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return emotionArray.length;
		}

		@Override
		public Integer getItem(int arg0) {
			// TODO Auto-generated method stub
			return emotionArray[arg0];
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ImageView ivExpress;
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.popup_express_item, null);
				ivExpress = (ImageView) convertView.findViewById(R.id.iv_item);
				convertView.setTag(ivExpress);
			} else {
				ivExpress = (ImageView) convertView.getTag();
			}
			ivExpress.setTag(order * 21 + position);
			ivExpress.setOnClickListener(EmoView.this);
			ivExpress.setImageResource(getItem(position));
			return convertView;
		}

	}

	class GridViewAdapter extends PagerAdapter {

		private List<View> mListViews = new ArrayList<View>();

		public GridViewAdapter(List<View> mListViews) {
			super();
			this.mListViews = mListViews;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mListViews.size();
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mListViews.get(position);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public Object instantiateItem(View view, int position) {
			((ViewPager) view).addView(mListViews.get(position), 0);
			return mListViews.get(position);
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {

		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {

		}
	}

	public interface OnEmoClickListener {
		public void onEmoClickListener(View arg1, int position);
	}

}
