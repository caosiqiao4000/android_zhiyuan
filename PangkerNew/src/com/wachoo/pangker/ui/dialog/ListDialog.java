package com.wachoo.pangker.ui.dialog;

import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.DirsAdapter;
import com.wachoo.pangker.entity.DirInfo;

public class ListDialog extends Dialog {

	private ListView dirListView;
	private TextView txtTitle;
	private DirsAdapter dirAdapter;

	public ListDialog(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
	}

	public ListDialog(Context context, int attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		Window window = getWindow();
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.type = WindowManager.LayoutParams.TYPE_APPLICATION_ATTACHED_DIALOG;
		window.setAttributes(lp);
		// window.addFlags(WindowManager.LayoutParams.FLAGS_CHANGED);
		window.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		setCanceledOnTouchOutside(true);
		setContentView(R.layout.list_dialog);
		dirListView = (ListView) findViewById(R.id.listView);
		txtTitle = (TextView) findViewById(R.id.txtPopTitle);
	}

	public void setAdapter(BaseAdapter adapter) {
		if (dirListView != null) {
			dirListView.setAdapter(adapter);
		}
	}

	public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
		// TODO Auto-generated method stub
		if (dirListView != null) {
			dirListView.setOnItemClickListener(onItemClickListener);
		}
	}

	public void showDir(List<DirInfo> dirinfos) {
		// TODO Auto-generated method stub
		if (dirAdapter == null) {
			dirAdapter = new DirsAdapter(getContext(), dirinfos);
			dirListView.setAdapter(dirAdapter);
		}
		dirAdapter.setDirinfos(dirinfos);
	}

	public void setTitle(String title) {
		txtTitle.setText(title);
	}
}
