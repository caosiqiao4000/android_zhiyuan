package com.wachoo.pangker.ui.dialog;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;

/**
 * 
 * @TODO消息提示对话框
 * 
 * @author zhengjy
 * 
 *         2012-7-12
 * 
 */
public class MessageTipDialog extends ConfimNoticeDialog {

	public static final int TYPE_DRIFT = 0x2001;//漂移提示
	public static final int TYPE_WEBSET = 0x2002;//网站名称提示
	
	private DialogMsgCallback resultCallback;

	public MessageTipDialog(DialogMsgCallback resultCallback, Context context) {
		super(context);
		this.resultCallback = resultCallback;
		application = (PangkerApplication) context.getApplicationContext();
	}

	private PangkerApplication application;
	// >>>>>>>>刷新显示内容
	private Handler handler = new Handler();

	// >>>>>>更新title显示内容
	public void refreshTitle(final String title) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setTitle(title);
			}
		});
	}

	// >>>>>>更新title显示内容
	public void refreshContent(final String content) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setContent(content);
			}
		});
	}
	
	public void showDialog(String title, String text) {
		showDialog(title, text, false);
	}

	public void showDialog(String text, boolean isShowbox) {
		showDialog(null, text, isShowbox);
	}

	/**
	 * TODO 显示消息提示对话框
	 * 
	 * @param title
	 *            Dialog消息提示题目
	 * @param text
	 *            Dialog消息提示内容
	 * @param isShowCancel
	 *            是否显示取消按钮
	 */
	public void showDialog(String title, String text, boolean isShowbox) {
		setDialogTitle("温馨提示");
		setTitle(title);
		setContent(text);

		setConfimOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
				resultCallback.buttonResult(true);
			}
		});
		setCanceledOnTouchOutside(false);

		setCancelOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
				resultCallback.buttonResult(false);
			}
		});
		if (isShowbox) {
			RememberSelected();
		}
		show();
	}

	public void showDialog(int title, int text) {
		showDialog(title, text, false);
	}

	public void showDialog(int title, int text, boolean isShowbox) {
		setDialogTitle("温馨提示");
		setTitle(title);
		setContent(text);

		setConfimOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
				resultCallback.buttonResult(true);
			}
		});
		setCanceledOnTouchOutside(false);

		setCancelOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
				resultCallback.buttonResult(false);
			}
		});
		if (isShowbox) {
			RememberSelected();
		}
		show();
	}

	private void dealType(int dealType) {
		// TODO Auto-generated method stub
		Log.i("TYPE", "dealType("+dealType+")");
		if (isRememberSelected()) {
			switch (dealType) {
			case TYPE_DRIFT:
				saveSPBoolean(PangkerConstant.SP_DRIFT_SET + application.getMyUserID(), isSelected());
				Log.i("TYPE", TYPE_DRIFT+"");
				break;
				
			case TYPE_WEBSET:
				saveSPBoolean(PangkerConstant.SP_WEB_SET + application.getMyUserID(), isSelected());
				Log.i("TYPE", TYPE_WEBSET+"");
				break;
			}
		}
	}

	public void showDialog(final int dealType, String title, String text) {
		showDialog(dealType, title, text, false);
	}

	public void showDialog(final int dealType, String title, String text, boolean isShowbox) {
		setDialogTitle("温馨提示");
		setTitle(title);
		setContent(text);

		setConfimOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i("TYPE", "ConfimOnClickListener");
				dealType(dealType);
				dismiss();
				resultCallback.buttonResult(true);
			}
		});
		setCanceledOnTouchOutside(false);

		setCancelOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i("TYPE", "CancelOnClickListener");
				dealType(dealType);
				dismiss();
				resultCallback.buttonResult(false);
			}
		});
		if (isShowbox) {
			RememberSelected();
		}
		show();
	}

	/**
	 * @TODO 对话框回调接口
	 * 
	 * @author zhengjy
	 * 
	 *         2012-7-12
	 * 
	 */
	public interface DialogMsgCallback {
		public void buttonResult(boolean isSubmit);
	}
}
