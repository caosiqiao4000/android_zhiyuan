package com.wachoo.pangker.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.AsynImageLoader;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.util.ImageUtil;

/**
 * @author zxx 用来显示详细图片的一个Dialog，区分本地和网络的 也可以考虑用Dialog形式的Activity来弄，
 */
public class ImageShowDialog extends Dialog {

	private Context context;
	private ImageView iv_show;
	private PKIconResizer mImageResizer;
	private Bitmap mBitmap;

	public ImageShowDialog(Context context) {
		super(context);
		this.context = context;
	}

	public ImageShowDialog(Context context, int attrs) {
		super(context, attrs);
		this.context = context;
	}

	protected ImageShowDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		this.context = context;
	}

	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		if (mBitmap != null && !mBitmap.isRecycled()) {
			mBitmap.recycle();
			mBitmap = null;
		}
		super.dismiss();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.imgshow_dialog);
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		iv_show = (ImageView) findViewById(R.id.ImageView_pic);
		iv_show.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});
	}

	public void setImgurl(String imgUrl, String userID) {
		if (imgUrl.contains("http")) {
			mImageResizer.setLoadingImage(R.drawable.nav_head);
			mImageResizer.loadImageFromServer(userID, iv_show);
		} else {
			mBitmap = ImageUtil.decodeFile(imgUrl, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE,
					PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE * PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
			iv_show.setImageBitmap(mBitmap);

		}
	}

	AsynImageLoader.OnLoaderListener loaderListener = new AsynImageLoader.OnLoaderListener() {

		@Override
		public void onImgLoader(ImageView iImageView, Bitmap bitmap) {
			// TODO Auto-generated method stub
			if (bitmap != null) {
				iv_show.setImageBitmap(bitmap);
			}
		}

		@Override
		public void onInitLenght(int lenght) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onProgress(int done) {
			// TODO Auto-generated method stub
			Log.d("done", "" + done);
		}

		@Override
		public void onSpeed(String speed) {
			// TODO Auto-generated method stub
		}
	};

}
