package com.wachoo.pangker.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.util.Util;

public class PasswordDialog extends Dialog implements TextWatcher {

	private TextView txtTitle;
	private EditText edPassword;
	private TextView txtHint;
	private Button btnConfrim;
	private Button btnCancel;
	private CheckBox checkBox;
	private LinearLayout hintLayout;
	private LinearLayout checkLayout;
	private DialogMsgCallback resultCallback;

	public PasswordDialog(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		init();
	}

	public PasswordDialog(Context context, int attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init();
	}

	private void init() {
		// TODO Auto-generated method stub
		setContentView(R.layout.dialog_password);
		txtTitle = (TextView) findViewById(R.id.txtTitle);
		txtHint = (TextView) findViewById(R.id.txtHint);
		edPassword = (EditText) findViewById(R.id.edContent);
		// imgHint = (ImageView) findViewById(R.id.imageHint);
		btnConfrim = (Button) findViewById(R.id.btn_dialog_confirm);
		btnCancel = (Button) findViewById(R.id.btn_dialog_cancel);
		checkBox = (CheckBox) findViewById(R.id.checkPass);
		hintLayout = (LinearLayout) findViewById(R.id.hintLayout);
		checkLayout = (LinearLayout) findViewById(R.id.checkLayout);

		btnCancel.setOnClickListener(cancelClickListener);
		btnConfrim.setOnClickListener(cancelClickListener);
		checkBox.setOnCheckedChangeListener(onCheckedChangeListener);
		edPassword.addTextChangedListener(this);
	}

	public void setResultCallback(DialogMsgCallback resultCallback) {
		this.resultCallback = resultCallback;
	}

	public void onBackPressed() {
		if (btnCancel.getVisibility() == View.VISIBLE) {
			super.onBackPressed();
		}
	};

	View.OnClickListener cancelClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (resultCallback == null) {
				return;
			}
			if (v == btnCancel) {
				resultCallback.buttonResult(false);
			} else if (v == btnConfrim) {
				resultCallback.buttonResult(true);
			}
			dismiss();
		}
	};

	OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			if (isChecked) {
				edPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
			} else {
				edPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
			}
		}
	};

	// public void showHint(String hint){
	// hintLayout.setVisibility(View.VISIBLE);
	// checkLayout.setVisibility(View.GONE);
	// txtHint.setText(hint);
	// }

	public void hodeBtnCancel() {
		btnCancel.setVisibility(View.GONE);
	}

	public void setTitle(String title) {
		txtTitle.setText(title);
	}

	public boolean checkPass() {
		String password = edPassword.getText().toString();
		if (Util.isEmpty(password)) {
			return false;
		}
		if (password.length() < 6 || password.length() > 10) {
			return false;
		}
		return true;
	}

	public String getPassword() {
		return edPassword.getText().toString();
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		hintLayout.setVisibility(View.GONE);
		checkLayout.setVisibility(View.VISIBLE);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	public void showDialog() {
		// TODO Auto-generated method stub
		edPassword.setText("");
		hintLayout.setVisibility(View.GONE);
		checkLayout.setVisibility(View.VISIBLE);
		show();
	}
}
