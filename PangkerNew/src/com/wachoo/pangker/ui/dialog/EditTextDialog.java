package com.wachoo.pangker.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.InputFilter;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.listener.EdInputFilter;

public class EditTextDialog extends Dialog{
	
	private EditText mEditText;
	private TextView mTitle;// >>>>>子标题
	private Button mBtnConfim;
	private Button mBtnCancel;
	
	private DialogResultCallback resultCallback;
	
	public EditTextDialog(Activity mActivity) {
		super(mActivity, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_editview);
		// 系统中关机对话框就是这个属性
		Window window = getWindow();
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.type = WindowManager.LayoutParams.TYPE_APPLICATION_ATTACHED_DIALOG;
		window.setAttributes(lp);
		// window.addFlags(WindowManager.LayoutParams.FLAGS_CHANGED);
		window.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		mTitle = (TextView) findViewById(R.id.tv_dialog_title);
		mEditText = (EditText)findViewById(R.id.et_dialog);
		mBtnConfim = (Button) findViewById(R.id.btn_dialog_confirm);
		mBtnConfim.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(resultCallback != null){
					String etDialogStr = mEditText.getText().toString();
					resultCallback.buttonResult(etDialogStr, true);
				}
				hideSoftInput(mEditText);
				dismiss();
			}
		});
		mBtnCancel = (Button) findViewById(R.id.btn_dialog_cancel);
		mBtnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				hideSoftInput(mEditText);
				dismiss();
			}
		});
	}

	public void setLine(int lines){
		mEditText.setLines(lines);
	}
	
	public void setTitle(int title){
		mTitle.setText(title);
	}
	
	public void setTitle(String title){
		mTitle.setText(title);
	}
	
	public void showDialog(String text, String etHint, int limit, DialogResultCallback resultCallback) {
		this.resultCallback = resultCallback;
		
		mEditText.setHint(etHint);
		mEditText.setText(text);
		mEditText.setFilters(new InputFilter[] { new EdInputFilter(limit)});
		show(); 
	}
	
	public void showDialog(String text, int etHint, int limit, DialogResultCallback resultCallback) {
		this.resultCallback = resultCallback;
		
		mEditText.setHint(etHint);
		mEditText.setText(text);
		mEditText.setFilters(new InputFilter[] { new EdInputFilter(limit)});
		show(); 
	}
	
	public void showDialog(String title, String text, String etHint, int limit, DialogResultCallback resultCallback) {
		this.resultCallback = resultCallback;
		
		mEditText.setHint(etHint);
		mEditText.setText(text);
		mTitle.setText(title);
		mEditText.setFilters(new InputFilter[] { new EdInputFilter(limit)});
		show(); 
	}
	
	public void showDialog(int title, String text, String etHint, int limit, DialogResultCallback resultCallback) {
		this.resultCallback = resultCallback;
		
		mEditText.setHint(etHint);
		mTitle.setText(title);
		mEditText.setText(text);
		mEditText.setFilters(new InputFilter[] { new EdInputFilter(limit)});
		show(); 
	}
	
	public void showDialog(int title, String text, int etHint, int limit, DialogResultCallback resultCallback) {
		this.resultCallback = resultCallback;
		
		mEditText.setHint(etHint);
		mTitle.setText(title);
		mEditText.setText(text);
		mEditText.setFilters(new InputFilter[] { new EdInputFilter(limit)});
		show(); 
	}
	
	public interface DialogResultCallback{
		public void buttonResult(String result,boolean isSubmit);
	}

	protected void hideSoftInput(View v) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}
	
}
