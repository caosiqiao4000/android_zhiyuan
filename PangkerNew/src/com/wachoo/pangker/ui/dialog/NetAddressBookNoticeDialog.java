package com.wachoo.pangker.ui.dialog;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.msg.NoticeInfoActivity;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.db.INetAddressBookDao;
import com.wachoo.pangker.db.INetAddressBookUserDao;
import com.wachoo.pangker.db.impl.NetAddressBookDaoImpl;
import com.wachoo.pangker.db.impl.NetAddressBookUserDaoImpl;
import com.wachoo.pangker.entity.NoticeInfo;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBookResult;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.AddressMembers;
import com.wachoo.pangker.server.response.BaseResult;

/**
 * 单位通讯录操作通知Dialog
 * 
 * @author zhengjy
 * 
 *         2012-06-19
 * 
 */
public class NetAddressBookNoticeDialog extends ConfimNoticeDialog implements
		IUICallBackInterface {

	private final int ADD_ADDRESSBOOK = 0x10;// 申请加入的Key

	private Context mContext;
	private NoticeInfo info;
	private INetAddressBookDao iNetAddressBookDao;
	private INetAddressBookUserDao iNetAddressBookUserDao;
	private PangkerApplication application;
	private AddressBooks books;
	private String userId;

	public NetAddressBookNoticeDialog(Context mContext, NoticeInfo info) {
		super(mContext);
		this.mContext = mContext;
		this.info = info;
		books = JSONUtil.fromJson(info.getRemark(), AddressBooks.class);
		iNetAddressBookDao = new NetAddressBookDaoImpl(mContext);
		iNetAddressBookUserDao = new NetAddressBookUserDaoImpl(mContext);
		application = (PangkerApplication) mContext.getApplicationContext();
		userId = application.getMyUserID();
	}

	/**
	 * 显示对话框
	 * 
	 * @param title
	 * @param text
	 * @param isShowCancel
	 */
	public void showDialog() {
		setDialogTitle("单位通讯录通知");
		setTitleVisibility(View.GONE);
		setContent(info.getNotice());

		setConfimOnClickListener(onConfimClickListener);
		setCanceledOnTouchOutside(false);
		setCancelOnClickListener("拒绝", onCancelClickListener);

		show();
	}

	private View.OnClickListener onConfimClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final OpenfireManager openfireManager = application
					.getOpenfireManager();
			if (openfireManager.isLoggedIn()) {
				// 申请加入当前用户所创建的单位通讯录接收到的通知
				if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_RESPONSE) {
					AddressBookInOut(userId, books.getId().toString(), "0");
				}
				// 被其他用户邀请加入某个单位通讯录接收到的通知
				else if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_INVITE_RESPONSE) {
					AddressBookInOut(userId, books.getId().toString(), "0");
				}
				// 申请退出当前用户所创建的单位通讯录
				else if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_APPLY_EXIT) {
					AddressBookInOut(info.getOpeUserid(),
							String.valueOf(books.getId()), "1");
				}
			} else {
				mShowToast(PangkerConstant.MSG_NO_ONLINE);
			}
			dismiss();
		}
	};

	private View.OnClickListener onCancelClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final OpenfireManager openfireManager = application
					.getOpenfireManager();
			if (openfireManager.isLoggedIn()) {
				if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_RESPONSE) {
					openfireManager.sendNetAddressBookApplyResp(userId,
							info.getOpeUserid(), "0", books, "refuse");
					info.setRemark("已经拒绝!");
				} else if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_INVITE_RESPONSE) {
					openfireManager.sendNetAddressBookInviteResp(userId,
							info.getOpeUserid(), new AddressMembers(), "0",
							"ok");
					info.setRemark("已经取消!");
				}
				// 申请退出当前用户所创建的单位通讯录
				else if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_APPLY_EXIT) {
					openfireManager.sendNetAddressBook(userId,
							info.getOpeUserid(), "0", "applyout", books,
							BusinessType.applyExitNetAddressBookAgree);
					info.setRemark("已经取消!");
				}
				resultNotice();
			} else {
				mShowToast(PangkerConstant.MSG_NO_ONLINE);
			}
			dismiss();
		}
	};

	private void resultNotice() {
		if (mContext instanceof NoticeInfoActivity) {
			((NoticeInfoActivity) mContext).resultNoticeInfo(info);
		}
	}

	/**
	 * 加入、退出通讯录接口 接口：AddressBookInOut.json
	 */
	private void AddressBookInOut(String uid, String bookid, String type) {
		ServerSupportManager serverMana = new ServerSupportManager(mContext,
				this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", uid)); // 加入、退出的UID
		paras.add(new Parameter("bookid", bookid)); // 加入、退出的通讯录id
		paras.add(new Parameter("type", type)); // 操作类型(必填)：0：加入，1：退出
		serverMana.supportRequest(Configuration.getAddressBookInOut(), paras,
				true, "正在处理,请稍后...", ADD_ADDRESSBOOK);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (caseKey == ADD_ADDRESSBOOK) {
			AddressBookResult baseResult = JSONUtil.fromJson(
					response.toString(), AddressBookResult.class);
			final OpenfireManager openfireManager = application
					.getOpenfireManager();
			if (null == baseResult) {
				mShowToast(R.string.to_server_fail);
				return;
			} else if (baseResult.getErrorCode() == BaseResult.SUCCESS) {
				if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_RESPONSE) {
					books.setNum(books.getNum() + 1);
					iNetAddressBookDao.updateAddressBook(userId, books);
					// 通知申请者，同意申请者加入单位通讯录
					openfireManager.sendNetAddressBookApplyResp(userId,
							info.getOpeUserid(), "1", books, "ok");
					info.setRemark("对方已经同意加入!");
					// iNetAddressBookDao.insertAddressBook(book)
				} else if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_INVITE_RESPONSE) {
					AddressMembers member = new AddressMembers();
					member.setId(baseResult.getMembers().get(0).getId());
					member.setUid(Long.parseLong(userId));
					member.setBookId(books.getId());
					member.setMobile(application.getMySelf().getPhone());
					member.setRemarkname(application.getMySelf().getUserName());
					iNetAddressBookDao.insertAddressBook(userId, books);
					iNetAddressBookUserDao.deleAddressMemberByBookid(books
							.getId().toString());
					openfireManager.sendNetAddressBookInviteResp(userId,
							info.getOpeUserid(), member, "1", "ok");
					info.setRemark("已经成功加入!");
				} else if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_APPLY_EXIT) {
					iNetAddressBookUserDao.deleAddressMemberByUserid(info
							.getOpeUserid());
					if (books.getNum() > 0) {
						books.setNum(books.getNum() - 1);
						iNetAddressBookDao.updateAddressBook(userId, books);
					}
					openfireManager.sendNetAddressBook(userId,
							info.getOpeUserid(), "1", "ok", books,
							BusinessType.applyExitNetAddressBookAgree);
					info.setRemark("已经退出!");
				}
			} else {
				info.setRemark("处理失败!");
				if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_RESPONSE) {
					openfireManager.sendNetAddressBookApplyResp(userId,
							info.getOpeUserid(), "0", books, "");
				} else if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_INVITE_RESPONSE) {
					openfireManager.sendNetAddressBookInviteResp(userId,
							info.getOpeUserid(), new AddressMembers(), "0", "");
				} else if (info.getType() == NoticeInfo.NOTICE_NET_ADDRESS_APPLY_EXIT) {
					openfireManager.sendNetAddressBook(userId,
							info.getOpeUserid(), "0", "", books,
							BusinessType.applyExitNetAddressBookAgree);
				}
				mShowToast(baseResult.getErrorMessage());
			}
			resultNotice();
		}
	}

	private void mShowToast(String mess) {
		Toast.makeText(mContext, mess, Toast.LENGTH_SHORT).show();
	}

	private void mShowToast(int resId) {
		Toast.makeText(mContext, resId, Toast.LENGTH_SHORT).show();
	}
}
