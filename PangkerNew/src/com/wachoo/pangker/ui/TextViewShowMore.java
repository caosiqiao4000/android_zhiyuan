package com.wachoo.pangker.ui;

import com.wachoo.pangker.PangkerConstant;

import android.content.Context;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewShowMore extends TextView {
	private TextView mTextView;// >>>>控制的文字控件
	private String mContent;// >>>>显示的文字内容
	private int mShowType;// >>>显示类型 0：显示一部分(提供显示全部功能) 1：全部显示（提供收起功能）

	// >>>>>>>设置显示长度
	private InputFilter[] filters = { new InputFilter.LengthFilter(PangkerConstant.showLength) };
	private InputFilter[] morefilters = {};

	public TextViewShowMore(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public TextViewShowMore(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public TextViewShowMore(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

}
