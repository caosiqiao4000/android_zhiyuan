package com.wachoo.pangker.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.wachoo.pangker.activity.R;

/**
 * 
 * 圆角图片效果
 * 
 * @author wangxin
 * 
 */
public class RoundedCornerImageView extends ImageView {

	public RoundedCornerImageView(Context context) {
		super(context);
		setBackgroundResource(R.drawable.btn_textview_selector);
	}

	public RoundedCornerImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setBackgroundResource(R.drawable.btn_textview_selector);
	}

	public RoundedCornerImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setBackgroundResource(R.drawable.btn_textview_selector);
	}

	static float radius = 6.0f;

	float padding = radius / 2;

	protected void onDraw(Canvas canvas) {
		Path clipPath = new Path();

		int w = this.getWidth();
		int h = this.getHeight();

		clipPath.addRoundRect(new RectF(padding, padding, w - padding, h - padding), radius, radius, Path.Direction.CW);
		canvas.clipPath(clipPath);
		super.onDraw(canvas);
	}

	private Paint mBitmapPaint = new Paint() {
		{
			setAntiAlias(true);
			setFilterBitmap(true);
		}
	};
	// >>>>>>>>是否点击
	boolean drawGlow = false;

	// >>>>>>>>>>>用来做点击效果
	Paint paint = new Paint();
	{
		paint.setAntiAlias(true);
		// >>>>>>>设置颜色
		paint.setColor(getContext().getResources().getColor(R.color.milky));
		// >>>>>>>设置透明度
		paint.setAlpha(80);
	};

	//
	// @Override
	// public void draw(Canvas canvas) {
	// super.draw(canvas);
	// if (drawGlow)
	// canvas.drawRect(new RectF(0.0f, 0.0f, this.getWidth(), this.getHeight()),
	// paint);
	// }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// if (event.getAction() == MotionEvent.ACTION_DOWN) {
		// drawGlow = true;
		// } else if (event.getAction() == MotionEvent.ACTION_UP)
		// drawGlow = false;
		//
		// this.invalidate();
		return super.onTouchEvent(event);
	}
}
