package com.wachoo.pangker.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.Overlay;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.image.PKIconResizer;

public class OverUserItem extends Overlay {

	private String userID;
	private GeoPoint mGeoPoint;

	private View mUserView;
	private ImageView mUserIcon;
	private ImageView mFollow;
	private Bitmap mBitmap;
	private int locateType;

	private PKIconResizer mImageResizer;

	public OverUserItem(Context context, GeoPoint mGeoPoint, String userID, Bitmap mBitmap, int locateType) {
		this.userID = userID;
		this.mGeoPoint = mGeoPoint;
		this.mBitmap = mBitmap;
		this.locateType = locateType;
		mImageResizer = PangkerManager.getUserIconResizer(context.getApplicationContext());
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mUserView = inflater.inflate(R.layout.pop_location_userinfo, null);
		mUserIcon = (ImageView) mUserView.findViewById(R.id.iv_map_usericon);
		mFollow = (ImageView) mUserView.findViewById(R.id.iv_follow);
		showFollow(this.locateType);
		mImageResizer.loadImage(userID, mUserIcon);
	}

	public int getLocateType() {
		return locateType;
	}

	public void setLocateType(int locateType) {
		this.locateType = locateType;
		showFollow(this.locateType);
	}

	public void showFollow(int locateType) {
		if (locateType == 1) {
			mFollow.setVisibility(View.VISIBLE);
		} else {
			mFollow.setVisibility(View.GONE);
		}
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public GeoPoint getmGeoPoint() {
		return mGeoPoint;
	}

	public void setmGeoPoint(GeoPoint mGeoPoint) {
		this.mGeoPoint = mGeoPoint;
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		Point p = new Point();
		mapView.getProjection().toPixels(mGeoPoint, p);
		canvas.drawBitmap(mBitmap, p.x - mBitmap.getWidth() / 2, p.y - mBitmap.getHeight() / 2, null);
	}

	public void addToMap(MapView mapview) {
		// TODO Auto-generated method stub
		MapView.LayoutParams lp;
		lp = new MapView.LayoutParams(MapView.LayoutParams.WRAP_CONTENT, MapView.LayoutParams.WRAP_CONTENT,
				mGeoPoint, 0, 0, MapView.LayoutParams.BOTTOM_CENTER);
		mapview.addView(mUserView, lp);
	}

	public void setmOnClickListener(final OnClickListener mOnClickListener) {
		if(mOnClickListener != null){
			mUserIcon.setOnClickListener(mOnClickListener);
		}
	}
    
}
