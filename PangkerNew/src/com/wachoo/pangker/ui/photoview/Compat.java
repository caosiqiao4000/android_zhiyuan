package com.wachoo.pangker.ui.photoview;

import android.view.View;

public class Compat {

	private static final int SIXTY_FPS_INTERVAL = 300 / 60;

	public static void postOnAnimation(View view, Runnable runnable) {
		view.postDelayed(runnable, SIXTY_FPS_INTERVAL);
	}

}
