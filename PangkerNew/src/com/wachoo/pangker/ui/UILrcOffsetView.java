package com.wachoo.pangker.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;

import com.wachoo.pangker.activity.R;

public class UILrcOffsetView {
	private Button btnLrcUp;
	private Button btnLrcDown;
	private Button btnLrcNow;

	// >>>>>>>歌词偏量功能栏
	private PopupWindow mLyricOffsetWindow;
	private ViewGroup mLyricOffsetViewGroup;

	private Context mContext;

	public UILrcOffsetView(Context mContext) {
		this.mContext = mContext;
		Init();
	}

	private void Init() {
		LayoutInflater mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// >>>>>>>>>>歌词偏量功能栏
		mLyricOffsetViewGroup = (ViewGroup) mLayoutInflater.inflate(R.layout.layout_lyric_offset, null, true);
		mLyricOffsetWindow = new PopupWindow(mLyricOffsetViewGroup, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		mLyricOffsetWindow.update();

		btnLrcUp = (Button) mLyricOffsetViewGroup.findViewById(R.id.btn_lrcUp);
		btnLrcDown = (Button) mLyricOffsetViewGroup.findViewById(R.id.btn_lrcDown);
		btnLrcNow = (Button) mLyricOffsetViewGroup.findViewById(R.id.btn_lrcNew);
	}

	public void showAtLocation(View parent, int gravity, int x, int y) {
		this.mLyricOffsetWindow.showAtLocation(parent, gravity, x, y);
	}

	public void dismiss() {
		this.mLyricOffsetWindow.dismiss();
	}

	public void setLrcUPOnClickListener(OnClickListener clickListener) {
		btnLrcUp.setOnClickListener(clickListener);
	}

	public void setLrcDOWNOnClickListener(OnClickListener clickListener) {
		btnLrcDown.setOnClickListener(clickListener);
	}

	public void setLrcResetOnClickListener(OnClickListener clickListener) {
		btnLrcNow.setOnClickListener(clickListener);
	}

	public Button getBtnLrcUp() {
		return btnLrcUp;
	}

	public void setBtnLrcUp(Button btnLrcUp) {
		this.btnLrcUp = btnLrcUp;
	}

	public Button getBtnLrcDown() {
		return btnLrcDown;
	}

	public void setBtnLrcDown(Button btnLrcDown) {
		this.btnLrcDown = btnLrcDown;
	}

	public Button getBtnLrcNow() {
		return btnLrcNow;
	}

	public void setBtnLrcNow(Button btnLrcNow) {
		this.btnLrcNow = btnLrcNow;
	}

}
