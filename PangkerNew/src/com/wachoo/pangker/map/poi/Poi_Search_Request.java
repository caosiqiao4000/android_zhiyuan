package com.wachoo.pangker.map.poi;

import com.wachoo.pangker.entity.Location;

/**
 * 使用驴博士查询poi的request
 * 
 * @author wangxin
 * 
 */
public class Poi_Search_Request extends LBS_Request {
	public static final String CATEGORY_ALL = "全部";

	public static final String CATEGORY_BOARD_LODGING = "酒店";

	public static final String CATEGORY_SHOPPING = "购物";
	// Entertainment
	public static final String CATEGORY_ENTERTAINMENT = "娱乐";
	// Travel
	public static final String CATEGORY_TRAVEL = "交通";
	// 其他
	public static final String CATEGORY_OHTERS = "其他";

	/**
	 * 对应Tab的搜索的关键字
	 */
	public static final String[] CATEGORY_KEY = {Poi_Search_Request.CATEGORY_ALL, Poi_Search_Request.CATEGORY_BOARD_LODGING, Poi_Search_Request.CATEGORY_SHOPPING,
			Poi_Search_Request.CATEGORY_ENTERTAINMENT, Poi_Search_Request.CATEGORY_TRAVEL, Poi_Search_Request.CATEGORY_OHTERS };
	private static final String[] POI_TAB_LABEL = {Poi_Search_Request.CATEGORY_ALL, Poi_Search_Request.CATEGORY_BOARD_LODGING,
			Poi_Search_Request.CATEGORY_SHOPPING, Poi_Search_Request.CATEGORY_ENTERTAINMENT, Poi_Search_Request.CATEGORY_TRAVEL,
			Poi_Search_Request.CATEGORY_OHTERS };
	
	public static final int REANG_100 = 100;
	public static final int REANG_200 = 200;
	public static final int REANG_300 = 300;
	public static final int REANG_400 = 400;
	public static final int REANG_500 = 500;

	private String query;
	private int range;
	private int page;
	private int count;

	/**
	 * 构造函数
	 * 
	 * @param ver
	 * @param key
	 * @param query
	 * @param lon
	 * @param lat
	 * @param range
	 * @param page
	 * @param count
	 */
	public Poi_Search_Request(String query, double lon, double lat, int range, int page, int count) {

		this.lon = lon;
		this.lat = lat;
		this.query = query;
		this.range = range;
		this.page = page;
		this.count = count;
	}
	
	public Poi_Search_Request(String query, Location location, int range, int page, int count) {
		// TODO Auto-generated constructor stub
		this.lon = location.getLongitude();
		this.lat = location.getLatitude();
		this.query = query;
		this.range = range;
		this.page = page;
		this.count = count;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * 
	 * "<poi_search key='f6b5d78c0a175ec2c0c9dfb99be5a53f'>" +
	 * "<query>餐馆</query>" + "<lon>113.360167</lon>" + "<lat>23.135309</lat>" +
	 * "<range>200</range>" + "<page>1</page>" + "<count>10</count>" +
	 * "</poi_search>";
	 * 
	 * @return
	 */
	@Override
	public String getLbsSearchReques() {
		// TODO Auto-generated method stub
		StringBuilder requesBuilder = new StringBuilder("<poi_search ");
		requesBuilder.append("ver='" + getVer() + "' ");
		requesBuilder.append("key='" + getKey() + "'>");
		requesBuilder.append("<query>" + getQuery() + "</query>");
		requesBuilder.append("<lon>" + getLon() + "</lon>");
		requesBuilder.append("<lat>" + getLat() + "</lat>");
		requesBuilder.append("<range>" + getRange() + "</range>");
		requesBuilder.append("<page>" + getPage() + "</page>");
		requesBuilder.append("<count>" + getCount() + "</count></poi_search>");
		return requesBuilder.toString();
	}

	public static String getPoiKeyByIndex(int point) {
		// TODO Auto-generated method stub
		return CATEGORY_KEY[point];
	}

	public static String[] getPoiTab() {
		// TODO Auto-generated method stub
		return POI_TAB_LABEL;
	}
	
}
