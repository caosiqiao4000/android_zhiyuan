package com.wachoo.pangker.map.poi;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * dom 解析xml
 * 
 * @author wangxin
 * 
 */
public class DomParser {
	DocumentBuilderFactory domfac = DocumentBuilderFactory.newInstance();
	
	public static Poi_Search_Response FromXML(String xml) {
		Poi_Search_Response pResponse = new Poi_Search_Response();
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			Document doc = docBuilderFactory.newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
			Element root = doc.getDocumentElement();
			if(root.hasAttribute("err")){
				String err = root.getAttributeNode("err").getNodeValue();
				if("1".equals(err)){
					pResponse.setError(true);
					return pResponse;
				}
			}
			NodeList pois = root.getFirstChild().getChildNodes();
			pResponse.setPois(FromNodeList(pois));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pResponse;
	}

	/**
	 * List<Poi_concise>
	 * 
	 * @param pois
	 * @return
	 */
	private static List<Poi_concise> FromNodeList(NodeList pois) {
		List<Poi_concise> list = new ArrayList<Poi_concise>();
		for (int i = 0; i < pois.getLength(); i++) {
			Node book = pois.item(i);
			Poi_concise Poiconcise = new Poi_concise();
			
			NodeList datalist = book.getChildNodes();
			for (int j = 0; j < datalist.getLength(); j++) {
				Node node = datalist.item(j);
				String nValue = "";
				if (node.hasChildNodes()) {
					if (node != null) {
						nValue = node.getFirstChild().getNodeValue();
					}
					if (node.getNodeName().equals("id")) {
						Poiconcise.setId(nValue);
					}
					if (node.getNodeName().equals("category_id")) {
						Poiconcise.setCategory_id(nValue);
					}
					if (node.getNodeName().equals("name")) {
						Poiconcise.setName(nValue);
					}
					if (node.getNodeName().equals("address")) {
						Poiconcise.setAddress(nValue);
					}
					if (node.getNodeName().equals("lat")) {
						Poiconcise.setLat(nValue);
					}
					if (node.getNodeName().equals("lon")) {
						Poiconcise.setLon(nValue);
					}
					if (node.getNodeName().equals("phone")) {
						Poiconcise.setPhone(nValue);
					}
					if (node.getNodeName().equals("city")) {
						Poiconcise.setCity(nValue);
					}
					if (node.getNodeName().equals("country")) {
						Poiconcise.setCountry(nValue);
					}
					if (node.getNodeName().equals("province")) {
						Poiconcise.setProvince(nValue);
					}
					if (node.getNodeName().equals("area")) {
						Poiconcise.setProvince(nValue);
					}
				 }
			}
			list.add(Poiconcise);
		}
		return list;
	}
	/**
	 * 返回一个Geo信息
	 * @param xml
	 * @return
	 */
	public static Geo_Search_Response GeoFromXML(String xml) {
		Geo_Search_Response gResponse = new Geo_Search_Response();
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		Geo_info geoinfo = new Geo_info();
		try {
			Document doc = docBuilderFactory.newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
			NodeList nodes = doc.getElementsByTagName("locationinfo");
			NodeList geoInfo = nodes.item(0).getChildNodes();
			for (int i = 0; i < geoInfo.getLength(); i++) {
				Node node = geoInfo.item(i);
				String nValue = "";
				if (node.hasChildNodes()) {
					if (node != null) {
						nValue = node.getFirstChild().getNodeValue();
					}
					if("citycode".equals(node.getNodeName())){
						geoinfo.setCitycode(nValue);
					}
					if("city".equals(node.getNodeName())){
						geoinfo.setCity(nValue);
					}
					if("road1".equals(node.getNodeName())){
						geoinfo.setRoad1(nValue);
					}
					if("road2".equals(node.getNodeName())){
						geoinfo.setRoad2(nValue);
					}
					if("address".equals(node.getNodeName())){
						geoinfo.setAddress(nValue);
					}
				}
				gResponse.setGeoinfo(geoinfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return gResponse;
	}
}
