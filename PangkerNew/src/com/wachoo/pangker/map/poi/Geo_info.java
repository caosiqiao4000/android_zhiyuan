package com.wachoo.pangker.map.poi;

import com.wachoo.pangker.util.Util;


public class Geo_info {
	private final String limitString = "市";
	private String lat;
	private String lon;
	private String citycode;
	private String city;
	private String road1;
	private String road2;
	private String address;
	
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getCitycode() {
		return citycode;
	}
	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRoad1() {
		return road1;
	}
	public void setRoad1(String road1) {
		this.road1 = road1;
	}
	public String getRoad2() {
		return road2;
	}
	public void setRoad2(String road2) {
		this.road2 = road2;
	}
	public String getAddress() {
		
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		if(!Util.isEmpty(address)){
			int index = address.indexOf(limitString);
			if(index > 0){
				return address.substring(index, address.length());
			}
			return address;
		}
		if(!Util.isEmpty(road2) && !Util.isEmpty(road1)){
			return road1 + "与" + road2 + "附近";
		}
		if(!Util.isEmpty(road2)){
			return road2;
		}
		if(!Util.isEmpty(road1)){
			return road1;
		}
		return super.toString();
	}
	
}
