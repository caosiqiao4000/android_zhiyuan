package com.wachoo.pangker.map.poi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;

import com.amap.mapapi.core.GeoPoint;
import com.wachoo.pangker.util.MapUtil;

public class RouteLoader extends AsyncTask<Void, Void, String>{

	final String roueturl = "http://maps.google.com/maps/api/directions/xml";
	
	private GeoPoint startGeoPoint;
	private GeoPoint endGeoPoint;
	private String mode;
	private OnRouteLoaderback onRouteLoaderback;
	
	public RouteLoader(GeoPoint startGeoPoint, GeoPoint endGeoPoint) {
		this(startGeoPoint, endGeoPoint, "walking");
	}
	
	public RouteLoader(GeoPoint startGeoPoint, GeoPoint endGeoPoint, String mode) {
		super();
		this.startGeoPoint = startGeoPoint;
		this.endGeoPoint = endGeoPoint;
		this.mode = mode;
	}
	
	public RouteLoader setOnRouteLoaderback(OnRouteLoaderback onRouteLoaderback) {
		this.onRouteLoaderback = onRouteLoaderback;
		return this;
	}

	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		Map<String, String> paramss = new HashMap<String, String>();
		paramss.put("origin", MapUtil.toString(startGeoPoint));
		paramss.put("destination", MapUtil.toString(endGeoPoint));
		paramss.put("sensor", "false");
		paramss.put("mode", mode);
		return httpGet(roueturl, paramss);
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		Log.d("RouteLoader", result);
		if (-1 == result.indexOf("<status>OK</status>") && onRouteLoaderback != null){
			onRouteLoaderback.onRouteLoaderback(null);
			return;
		}
		int pos = result.indexOf("<overview_polyline>");
		pos = result.indexOf("<points>", pos + 1);
		int pos2 = result.indexOf("</points>", pos);
		result = result.substring(pos + 8, pos2);
		List<GeoPoint> points = decodePoly(result);
		if(onRouteLoaderback != null){
			onRouteLoaderback.onRouteLoaderback(points);
		}
		super.onPostExecute(result);
	}
	
	 /**
	 * 解析返回xml中overview_polyline的路线编码
	 * @param encoded
	 * @return
	 */
	private List<GeoPoint> decodePoly(String encoded) {
	    List<GeoPoint> poly = new ArrayList<GeoPoint>();
	    int index = 0, len = encoded.length();
	    int lat = 0, lng = 0;

	    while (index < len) {
	        int b, shift = 0, result = 0;
	        do {
	            b = encoded.charAt(index++) - 63;
	            result |= (b & 0x1f) << shift;
	            shift += 5;
	        } while (b >= 0x20);
	        int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	        lat += dlat;

	        shift = 0;
	        result = 0;
	        do {
	            b = encoded.charAt(index++) - 63;
	            result |= (b & 0x1f) << shift;
	            shift += 5;
	        } while (b >= 0x20);
	        int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	        lng += dlng;

	        GeoPoint p = new GeoPoint((int) (((double) lat / 1E5) * 1E6),
	             (int) (((double) lng / 1E5) * 1E6));
	        poly.add(p);
	    }
	    return poly;
	}
	
	public String httpGet(String url, Map<String, String> params) {
		if(!url.endsWith("?")){
			url += "?";
		}
		StringBuffer sburl = new StringBuffer();
		sburl.append(url);
		if (params != null) {
			for (Entry<String, String> e : params.entrySet()) {
				sburl.append(e.getKey());
				sburl.append("=");
				sburl.append(e.getValue());
				sburl.append("&");
			}
			sburl.substring(0, sburl.length() - 1);
		}
		
		HttpGet get = new HttpGet(sburl.toString());
		String strResult = "";
		try {
			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
			HttpClient httpClient = new DefaultHttpClient(httpParameters); 
			
			HttpResponse httpResponse = null;
			httpResponse = httpClient.execute(get);
			
			if (httpResponse.getStatusLine().getStatusCode() == 200){
				strResult = EntityUtils.toString(httpResponse.getEntity());
			}
		} catch (Exception e) {
			return null;
		}
		return strResult;
	}
	
	public String httpGet(String url) {
		HttpGet get = new HttpGet(url.toString());
		String strResult = "";
		try {
			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
			HttpClient httpClient = new DefaultHttpClient(httpParameters); 
			
			HttpResponse httpResponse = null;
			httpResponse = httpClient.execute(get);
			
			if (httpResponse.getStatusLine().getStatusCode() == 200){
				strResult = EntityUtils.toString(httpResponse.getEntity());
			}
		} catch (Exception e) {
			return null;
		}
		return strResult;
	}
	
	public interface OnRouteLoaderback{
		public void onRouteLoaderback(List<GeoPoint> routePoints);
	}
	
}
