package com.wachoo.pangker.map.poi;

import com.wachoo.pangker.Configuration;

public abstract class LBS_Request {
	
	public double lat;
	public double lon;
	public double getLat() {
		return lat;
	}
	
	public double getLon() {
		return lon;
	}
	public String getKey() {
		return Configuration.getPoiSearchKey();
	}

	public String getVer() {
		// TODO Auto-generated method stub
		return Configuration.getPoiSearchVer();
	}
	
	public abstract String getLbsSearchReques();
}
