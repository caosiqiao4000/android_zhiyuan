package com.wachoo.pangker.map.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;

import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapView;
import com.wachoo.pangker.activity.R;

public class MapLocation {

	public static final int TYPE_START = 0;
	public static final int TYPE_END = 1;
	private Bitmap drawIcon, shadowIcon;
	private boolean isOwner = false;
	private GeoPoint geoPoint;
	
	public MapLocation(Context context, GeoPoint geoPoint) {
		// TODO Auto-generated constructor stub
		this.geoPoint = geoPoint;
		drawIcon = BitmapFactory.decodeResource(context.getResources(),R.drawable.current_position_red);
		shadowIcon = BitmapFactory.decodeResource(context.getResources(),R.drawable.current_position_blue);
	}
	
	public GeoPoint getGeoPoint() {
		return geoPoint;
	}

	public void setGeoPoint(GeoPoint geoPoint) {
		this.geoPoint = geoPoint;
	}
	
	public boolean isOwner() {
		return isOwner;
	}

	public void setOwner(boolean isOwner) {
		this.isOwner = isOwner;
	}
	
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		Point p = new Point();
		mapView.getProjection().toPixels(geoPoint, p);
    	if(isOwner){
    		canvas.drawBitmap(drawIcon, p.x - drawIcon.getWidth() / 2, p.y - drawIcon.getHeight() / 2, null);
    	} else {
    		canvas.drawBitmap(shadowIcon, p.x - shadowIcon.getWidth() / 2, p.y - shadowIcon.getHeight() / 2 ,null);
    	}
	}
	
}
