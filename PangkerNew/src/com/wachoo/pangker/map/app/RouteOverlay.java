package com.wachoo.pangker.map.app;

import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapView;
import com.amap.mapapi.map.Overlay;
import com.amap.mapapi.map.Projection;

public class RouteOverlay extends Overlay{

	private List<GeoPoint> points;
	private Paint paint;
	
	/**
	 * 构造函数，使用GeoPoint List构造Polyline
	 * 
	 * @param points GeoPoint点List
	 */
	public RouteOverlay(List<GeoPoint> points) {
		this.points = points;
		paint = new Paint();
		paint.setColor(Color.BLUE);
		paint.setAlpha(150);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setStrokeWidth(4);
	}
	
	/**  
	 * 真正将线绘制出来 只需将线绘制到canvas上即可，主要是要转换经纬度到屏幕坐标  
	 */
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		if (!shadow) {// 不是绘制shadow层   
			Projection projection = mapView.getProjection();
			if (points != null && points.size() >= 2) {// 画线
				Point start = new Point();
				projection.toPixels(points.get(0), start);// 需要转换坐标
				for (int i = 1; i < points.size(); i++) {
					Point end = new Point();
					projection.toPixels(points.get(i), end);
					canvas.drawLine(start.x, start.y, end.x, end.y, paint);// 绘制到canvas上即可
					start = end;
				}
			}
		}
	}
}
