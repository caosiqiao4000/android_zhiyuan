package com.wachoo.pangker.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.about.PangkerWizardActivity;
import com.wachoo.pangker.activity.camera.CameraCommActivity;
import com.wachoo.pangker.activity.contact.ContactAttentionActivity;
import com.wachoo.pangker.activity.contact.ContactFollowActivity;
import com.wachoo.pangker.activity.group.ChatRoomListActivity;
import com.wachoo.pangker.activity.pangker.MapLocationActivity;
import com.wachoo.pangker.activity.res.UpDownLoadManagerActivity;
import com.wachoo.pangker.activity.root.PangkerAccountActivity;
import com.wachoo.pangker.activity.root.PangkerInfluenceActivity;
import com.wachoo.pangker.activity.root.PangkerResManagerActivity;
import com.wachoo.pangker.activity.root.PangkerSetActivity;
import com.wachoo.pangker.api.IResWarnListener;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.chat.MessageChangedManager;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.impl.PKUserDaoImpl;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconLoader;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CountQueryResult;
import com.wachoo.pangker.server.response.HeadUploadResult;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.ui.dialog.EditTextDialog.DialogResultCallback;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.util.ContactsUtil;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class HomeActivity extends CameraCommActivity implements IUICallBackInterface, IResWarnListener,
		ITabSendMsgListener {

	private final int SIGIN_LENGTH = 100;
	private ImageView iv_usericon;
	private TextView tvNickName;
	private LinearLayout ly_drift;
	private TextView textDrift;
	private TextView tvSign;
	private Button btnUserInfo;
	private Button btn_location;
	private TextView tvAttentNum;
	private TextView tvFanNum;
	private RatingBar rating_bar_show;

	private LinearLayout infoLayout;
	private LinearLayout attentLayout;
	private LinearLayout fansLayout;

	private LinearLayout mResManagerLayout;

	private LinearLayout influenceLayout;// >>>>>>>>我的影响力
	private LinearLayout resGroupLayout;
	private LinearLayout accountLayout;
	private LinearLayout wizarLayout;
	private ImageView ivAccount;
	private TextView txtAccount;
	private LinearLayout setLayout;

	private LinearLayout uploadmanaLayout;

	private String SIGN_ITEM = "";// 个性签名
	private final int UPDATESIGN_CODE = 0x110;// 更新签名
	private final int UPLOADICON_CODE = 0x111;// 上传头像
	private final int CANCELDRIFT = 0x112;
	private final int COUNT_USER_CASEKEY = 0x113;
	private String myUserID;
	private UserInfo userInfo;
	private IPKUserDao iPKUserDao;
	private PangkerApplication application;// application 旁客应用

	private OnlineReceiver onlineReceiver;
	private MessageChangedManager msgChangedManager;

	private ContactsUtil contactsUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		inits();
		initView();
		initListener();
		initData();
		// // 注册广播
		registerBoradcastReceiver();
		CountQuery();
	}

	private void inits() {
		// TODO Auto-generated method stub
		msgChangedManager = PangkerManager.getMessageChangedManager();
		PangkerManager.getTabBroadcastManager().addMsgListener(this);
		application = (PangkerApplication) getApplication();
		iPKUserDao = new PKUserDaoImpl(this);
		userInfo = application.getMySelf();
		myUserID = userInfo.getUserId();
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		contactsUtil = new ContactsUtil(this, myUserID);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// 首次登录旁客设置向导
		// addGuideImage(HomeActivity.class.getName(), R.drawable.home_01);
	}

	@Override
	protected void onDestroy() {
		unregisterReceiver(onlineReceiver);
		super.onDestroy();
	}

	private void registerBoradcastReceiver() {
		// TODO Auto-generated method stub
		onlineReceiver = new OnlineReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(PangkerConstant.ACTTION_LOGIN_RESULT);// 登录结果
		filter.addAction(PangkerConstant.ACTTION_ICON);// 用户图像修改
		registerReceiver(onlineReceiver, filter);
	}

	private class OnlineReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (PangkerConstant.ACTTION_LOGIN_RESULT.equals(intent.getAction())) {
				// loginResult(intent);
			} else if (PangkerConstant.ACTTION_ICON.equals(intent.getAction())) {
				setUserIcon(REFRESH_USERICON);
			}
		}
	}

	/**
	 * 监听
	 * 
	 * @author wubo
	 * @createtime 2012-2-1 下午04:26:15
	 */
	OnClickListener myOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Intent intent = new Intent();
			if (view == tvNickName || view == infoLayout || view == btnUserInfo) {
				intent.setClass(HomeActivity.this, UserInfoEditActivity.class);
				startActivityForResult(intent, UPDATESIGN_CODE);
			} else if (view == tvSign) {
				EditTextDialog dialog = new EditTextDialog(getParent());
				dialog.setLine(4);
				dialog.showDialog(R.string.user_sign_num, userInfo.getSign(), R.string.user_sign_hint, SIGIN_LENGTH,
						new DialogResultCallback() {
							@Override
							public void buttonResult(String result, boolean isSubmit) {
								// TODO Auto-generated method stub
								if (isSubmit && result != null) {
									if (!result.equals(userInfo.getSign())) {
										SIGN_ITEM = result;
										tvSign.setText(Util.getStringByLength(SIGN_ITEM, SIGIN_LENGTH));
										updateSign();
									}
								}
							}
						});
			} else if (view == iv_usericon) {
				showImageDialog("头像上传", actionItems1, true);
			} else if (view == btn_location) {
				intent.setClass(HomeActivity.this, MapLocationActivity.class);
				HomeActivity.this.startActivity(intent);
			} else if (view == attentLayout) {
				intent.setClass(HomeActivity.this, ContactAttentionActivity.class);
				HomeActivity.this.startActivity(intent);
			} else if (view == fansLayout) {
				intent.setClass(HomeActivity.this, ContactFollowActivity.class);
				HomeActivity.this.startActivity(intent);
			} else if (view == accountLayout) {
				intent.setClass(HomeActivity.this, PangkerAccountActivity.class);
				HomeActivity.this.startActivity(intent);
			} else if (view == resGroupLayout) {
				intent.setClass(HomeActivity.this, ChatRoomListActivity.class);
				intent.putExtra("ifShow", true);
				startActivity(intent);

			} else if (view == uploadmanaLayout) {

				intent.setClass(HomeActivity.this, UpDownLoadManagerActivity.class);
				HomeActivity.this.startActivity(intent);
			} else if (view == setLayout) {
				intent.setClass(HomeActivity.this, PangkerSetActivity.class);
				HomeActivity.this.startActivity(intent);
			} else if (view == wizarLayout) {
				intent.setClass(HomeActivity.this, PangkerWizardActivity.class);
				startActivity(intent);
			} else if (view == mResManagerLayout) {
				intent.setClass(HomeActivity.this, PangkerResManagerActivity.class);
				startActivity(intent);
			}
			// >>>>>>>我的影响力
			else if (view == influenceLayout) {
				intent.setClass(HomeActivity.this, PangkerInfluenceActivity.class);
				HomeActivity.this.startActivity(intent);
			}

			// >>>>>>>>跳转到漂移目标的个人网站
			else if (view == textDrift) {
				// TODO Auto-generated method stub
				intent.setClass(HomeActivity.this, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, application.getDriftUser().getUserId());
				intent.putExtra(UserItem.USERNAME, application.getDriftUser().getUserName());
				intent.putExtra(UserItem.USERSIGN, application.getDriftUser().getSign());
				intent.putExtra(PangkerConstant.STATE_KEY, application.getDriftUser().getState());
				startActivity(intent);
			}

		}
	};

	/**
	 * 更新个性签名
	 * 
	 * @author wubo
	 * @createtime 2012-2-1 下午04:26:03
	 */
	public void updateSign() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", myUserID));
		paras.add(new Parameter("sign", SIGN_ITEM));
		paras.add(new Parameter("optype", "1"));
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getUserinfoModify(), paras, true, mess, UPDATESIGN_CODE);
	}

	/**
	 * 上传用户头像
	 * 
	 * @param stream
	 *            void
	 * @throws FileNotFoundException
	 */
	private void uploadHead(String imageLocalPath) {
		File headIocn = new File(imageLocalPath);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", myUserID));
		MyFilePart uploader = null;
		try {
			uploader = new MyFilePart(headIocn.getName(), headIocn, null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			uploader = null;
			return;
		}
		if (uploader != null) {
			String mess = getResources().getString(R.string.uploading);
			ServerSupportManager serverMana = new ServerSupportManager(getParent(), this);
			serverMana.supportRequest(Configuration.getUploadHead(), paras, uploader, imageLocalPath, true, mess,
					UPLOADICON_CODE);
		}
	}

	/**
	 * 初始化UI
	 */
	private void initView() {
		tvNickName = (TextView) findViewById(R.id.nickname);
		tvSign = (TextView) findViewById(R.id.sign);
		ly_drift = (LinearLayout) findViewById(R.id.ly_drift);
		textDrift = (TextView) findViewById(R.id.txt_drift);
		iv_usericon = (ImageView) findViewById(R.id.home_head);
		infoLayout = (LinearLayout) findViewById(R.id.info_layout);
		btnUserInfo = (Button) findViewById(R.id.btn_userinfo);

		tvAttentNum = (TextView) findViewById(R.id.attent_num);
		tvFanNum = (TextView) findViewById(R.id.fan_num);

		attentLayout = (LinearLayout) findViewById(R.id.ly_attent);
		btn_location = (Button) findViewById(R.id.btn_location);
		fansLayout = (LinearLayout) findViewById(R.id.ly_fans);
		rating_bar_show = (RatingBar) findViewById(R.id.rating_bar_show);
		rating_bar_show.setNumStars(1);

		// >>>>>>>>资源管理
		mResManagerLayout = (LinearLayout) findViewById(R.id.ly_res_manager);

		resGroupLayout = (LinearLayout) findViewById(R.id.ly_group);
		accountLayout = (LinearLayout) findViewById(R.id.ly_mysite);

		txtAccount = (TextView) findViewById(R.id.txt_account);
		ivAccount = (ImageView) findViewById(R.id.iv_account);

		setLayout = (LinearLayout) findViewById(R.id.ly_pangker_setting);
		uploadmanaLayout = (LinearLayout) findViewById(R.id.ly_up_down);

		wizarLayout = (LinearLayout) findViewById(R.id.ly_pangker);

		influenceLayout = (LinearLayout) findViewById(R.id.ly_influence);// >>>>>>>>我的影响力
	}

	private void showDriftInfo() {
		UserItem userInfo = application.getDriftUser();
		if (userInfo != null) {
			ly_drift.setVisibility(View.VISIBLE);

			textDrift.setText(userInfo.getUserName());
			textDrift.setOnClickListener(myOnClickListener);

		} else {
			ly_drift.setVisibility(View.INVISIBLE);
		}

	}

	private void resumeData() {
		// TODO Auto-generated method stub
		userInfo = ((PangkerApplication) getApplication()).getMySelf();
		// 交友用户
		tvNickName.setText(userInfo.getUserName());
		tvSign.setText(Util.getStringByLength(userInfo.getSign(), SIGIN_LENGTH));
		// 关注人粉丝数
		tvAttentNum.setText("(" + contactsUtil.getAttentCount() + ")");
		tvFanNum.setText("(" + contactsUtil.getFanCount() + ")");
	}

	/**
	 * 初始化数据h
	 * 
	 * @author wubo
	 * @createtime 2012-2-1 下午04:24:48
	 */
	public void initData() {
		tvNickName.setText(userInfo.getUserName() != null ? userInfo.getUserName() : "");
		SIGN_ITEM = userInfo.getSign() != null ? userInfo.getSign() : "";
		tvSign.setText(Util.getStringByLength(SIGN_ITEM, SIGIN_LENGTH));
		setUserIcon(SET_USERICON);
		showDriftInfo();
		showAccount();
	}

	private void showAccount() {
		if (!Util.isEmpty(application.getImAccount())) {
			txtAccount.setVisibility(View.VISIBLE);
			txtAccount.setText(application.getImAccount());
			ivAccount.setImageResource(R.drawable.icon46_inf);
		} else {
			txtAccount.setVisibility(View.GONE);
			ivAccount.setImageResource(R.drawable.icon46_inf_7);
		}
	}

	private PKIconResizer mImageResizer;
	static final int SET_USERICON = 0x101;// 设置头像
	static final int REFRESH_USERICON = 0x102;// 重新获取头像

	private void setUserIcon(int type) {
		if (type == SET_USERICON) {
			mImageResizer.loadImage(myUserID, iv_usericon);
		} else if (type == REFRESH_USERICON) {
			mImageResizer.loadImageFromServer(myUserID, iv_usericon);
		}
	}

	/**
	 * 初始化监听
	 * 
	 * @author wubo
	 * @createtime 2012-2-1 下午04:25:09
	 */
	public void initListener() {
		// 设置按钮监听
		iv_usericon.setOnClickListener(myOnClickListener);
		infoLayout.setOnClickListener(myOnClickListener);
		tvSign.setOnClickListener(myOnClickListener);
		tvNickName.setOnClickListener(myOnClickListener);
		btnUserInfo.setOnClickListener(myOnClickListener);
		attentLayout.setOnClickListener(myOnClickListener);
		btn_location.setOnClickListener(myOnClickListener);
		fansLayout.setOnClickListener(myOnClickListener);

		resGroupLayout.setOnClickListener(myOnClickListener);
		accountLayout.setOnClickListener(myOnClickListener);
		setLayout.setOnClickListener(myOnClickListener);

		uploadmanaLayout.setOnClickListener(myOnClickListener);
		mResManagerLayout.setOnClickListener(myOnClickListener);
		wizarLayout.setOnClickListener(myOnClickListener);
		influenceLayout.setOnClickListener(myOnClickListener);
	}

	/**
	 * 查询关注人 ，粉丝个数接口 接口：CountQuery.json
	 */
	public void CountQuery() {
		List<Parameter> paras = new ArrayList<Parameter>();
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		paras.add(new Parameter("uid", application.getMyUserID()));
		serverMana.supportRequest(Configuration.getCountQuery(), paras, COUNT_USER_CASEKEY);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		if (!HttpResponseStatus(response))
			return;

		if (caseKey == UPDATESIGN_CODE) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				userInfo.setSign(SIGN_ITEM);
				application.setMySelf(userInfo);
				iPKUserDao.updateSign(userInfo);
				userInfo.setIconBytes(null);
				Util.writeHomeUser(this, userInfo);
				showToast(result.getErrorMessage());
			} else {
				showToast(R.string.to_server_fail);
			}
		} else if (caseKey == UPLOADICON_CODE) {
			HeadUploadResult result = JSONUtil.fromJson(response.toString(), HeadUploadResult.class);
			dealHeadIcon(result);
		} else if (caseKey == CANCELDRIFT) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result == null) {
				return;
			}
			if (result.getErrorCode() == BaseResult.SUCCESS) {
				application.setLocateType(PangkerConstant.LOCATE_CLIENT);// //取消漂移状态,重新开始定位
				application.setDriftUser(null);
				showToast("取消漂移成功!");
				Intent intent = new Intent(HomeActivity.this, PangkerService.class);
				intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.LOCATE_INTENT);
				startService(intent);
			} else if (result.getErrorCode() == 0) {
				showToast(result.getErrorMessage());
			} else {
				showToast(R.string.to_server_fail);
			}
		} else if (caseKey == COUNT_USER_CASEKEY) {
			CountQueryResult result = JSONUtil.fromJson(response.toString(), CountQueryResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				int attentCount = result.getAttentCount() == null ? 0 : result.getAttentCount();
				int fansCount = result.getFansCount() == null ? 0 : result.getFansCount();
				if (result.getVipLevel() != null) {
					if (result.getVipLevel() == 0) {
						rating_bar_show.setNumStars(1);
					} else if (result.getVipLevel() == 1) {
						rating_bar_show.setNumStars(2);
					} else if (result.getVipLevel() == 2) {
						rating_bar_show.setNumStars(3);
					}
				}
				tvAttentNum.setText("(" + attentCount + ")");
				tvFanNum.setText("(" + fansCount + ")");
				contactsUtil.setAttentCount(attentCount);
				contactsUtil.setFanCount(fansCount);
			}
		}
	}

	private void dealHeadIcon(HeadUploadResult result) {
		// TODO Auto-generated method stub
		if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
			userInfo.setIconType(1);
			Intent intent = new Intent(PangkerConstant.ACTTION_ICON);
			sendBroadcast(intent);
			showToast(result.getErrorMessage());
		} else {
			showToast("上传头像失败!");
			mImageResizer.loadImage(myUserID, iv_usericon);
		}
		// setUserIcon(REFRESH_USERICON);
	}

	@Override
	public void resWarnNotice(int count) {
		// TODO Auto-generated method stub
		/*
		 * if(count > 0){ imgUnread.setVisibility(View.VISIBLE); } if (count <=
		 * 10) { resWarhCount.setText(count == 0 ? "" : String.valueOf(count));
		 * 
		 * } else resWarhCount.setText("10+");
		 */

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == UPDATESIGN_CODE && resultCode == RESULT_OK) {
			resumeData();
			((MainActivity) getParent()).initTitleDate();
		}
	}

	@Override
	public void doImageBitmap(final String imageLocalPath) {
		// TODO Auto-generated method stub
		// >>>>>>>>>因为是头像上传，图片很小
		Bitmap bitmap = ImageUtil.decodeFile(imageLocalPath, PKIconLoader.iconSideLength, 
				PKIconLoader.iconSideLength * PKIconLoader.iconSideLength);
		if (bitmap != null) {
			iv_usericon.setImageBitmap(bitmap);
			MessageTipDialog tipDialog = new MessageTipDialog(new DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-generated method stub
					if (isSubmit) {
						uploadHead(imageLocalPath);
					} else {
						setUserIcon(SET_USERICON);
					}
				}
			}, this);
			tipDialog.showDialog(null, "是否上传头像?");
		}
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		if (msg.what == PangkerConstant.DRIFT_NOTICE) {
			showDriftInfo();
		}
		if (msg.what == -1) {
			showAccount();
			((MainActivity) getParent()).checkImAccount();
		}
	}

}
