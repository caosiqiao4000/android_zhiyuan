package com.wachoo.pangker.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.R.array;
import com.wachoo.pangker.activity.R.drawable;
import com.wachoo.pangker.activity.R.id;
import com.wachoo.pangker.activity.R.layout;
import com.wachoo.pangker.activity.R.string;
import com.wachoo.pangker.activity.root.CitySelectionActivity;
import com.wachoo.pangker.activity.root.FingerprintActivity;
import com.wachoo.pangker.db.IAreaDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.impl.AreaDaoImpl;
import com.wachoo.pangker.db.impl.PKUserDaoImpl;
import com.wachoo.pangker.entity.Area;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.HeadUploadResult;
import com.wachoo.pangker.server.response.UserinfoQueryResult;
import com.wachoo.pangker.service.MeetRoomService;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.ui.dialog.EditTextDialog.DialogResultCallback;
import com.wachoo.pangker.util.Util;

/**
 * 用户详细信息设置，交友申请，以及其他用户的详细信息显示
 * 
 * @author zhengjy
 * 
 */
public class UserInfoEditActivity extends CommonPopActivity implements IUICallBackInterface {

	private String TAG = "EditUserInfoActivity";// log tag
	private static final Logger logger = LoggerFactory.getLogger();

	private Button btn_submit;

	private TextView tvAccount;
	private TextView tvSign;
	private TextView tvUsername;
	// private TextView tvRealname;
	private TextView tvSex;
	private TextView tvAge;
	private TextView tvHeight;
	private TextView tvWeight;
	private TextView tvDegree;
	private TextView tvBirthday;
	private TextView tvZodiac;
	private TextView tvStar;
	private TextView tvBlood;
	private TextView tvCity;
	private TextView tvInterest;
	private TextView tvFriendtype;
	private TextView tvPurpose;

	// 用户名、性别、年龄、身高、体重、学历
	// 生日、生肖、星座、血型、城市、爱好
	// 点击LinearLayout弹出修改相应属性界面
	private LinearLayout lyAccount;
	private LinearLayout lySign;
	private LinearLayout lyUsername;
	// private LinearLayout lyRealname;
	private LinearLayout lySex;
	private LinearLayout lyAge;
	private LinearLayout lyHeight;
	private LinearLayout lyWeight;
	private LinearLayout lyDegree;
	private LinearLayout lyBirthday;
	private LinearLayout lyZodiac;
	private LinearLayout lyStar;
	private LinearLayout lyBlood;
	private LinearLayout lyCity;
	private LinearLayout lyInterest;
	private LinearLayout mfLayout;
	private LinearLayout mfLyFriendtype;
	private LinearLayout mfLyPurpose;
	private ImageView ivTipRight;
	private ImageView ivTipRight0;
	private ImageView ivTipRight1;
	private ImageView ivTipRight2;
	private ImageView ivTipRight3;
	private ImageView ivTipRight4;
	private ImageView ivTipRight5;
	private ImageView ivTipRight6;
	private ImageView ivTipRight7;
	private ImageView ivTipRight8;
	private ImageView ivTipRight9;
	private ImageView ivTipRight10;
	private ImageView ivTipRight11;
	private ImageView ivTipRight12;
	private ImageView imageview;

	private View dialogView;
	private EditText et_dialog;

	private int currentDialogId = -1;
	private int cityRequestCode = 100;
	private final int DIALOG_ID_SEX = 0x100;
	// private final int DIALOG_ID_SIGN = 0x101;
	private final int DIALOG_ID_IMAGE = 0x104;
	private final int DIALOG_ID_AGE = 0x105;
	private final int DIALOG_ID_HEIGHT = 0x106;
	private final int DIALOG_ID_INTEREST = 0x107;
	private final int DIALOG_ID_EDUCATION = 0x108;
	private final int DIALOG_ID_STAR = 0x109;
	private final int DIALOG_ID_BLOODTYPE = 0x10A;
	private final int DIALOG_ID_WEIGHT = 0x10B;
	private final int DIALOG_UPLOADICON = 0x10C;
	private final int DIALOG_ZODIAC = 0x10D;
	private final int SETCODE = 0x10E;
	private final int LOADCODE = 0x10F;
	private final int DIALOG_ID_FRIENDTYPE = 0x110;
	private final int DIALOG_ID_PURPOSE = 0x111;
	private final int RESIGST_CASEKEY = 0x112;
	private final int FRINGER_CODE = 0x113;// 更新旁克号码

	private String[] SEX_ITEM;// 性别
	private String[] SEXCODE_ITEM;// 性别代码
	private String[] EDUCATION_ITEM;// 学历
	private String[] EDUCATIONCODE_ITEM;// 学历代码
	private String[] BLOODTYPE_ITEM;// 血型
	private String[] ZODIAC_ITEM;// 生肖
	private String[] ZODIACCODE_ITEM;// 生肖代码
	private String[] STAR_ITEM;// 星座
	private String[] STARCODE_ITEM;// 星座代码
	private String[] AGE_ITEM;// 年龄
	private String[] HEIGHT_ITEM;// 身高
	private String[] WEIGHT_ITEM; // 体重
	private String[] HOBBY_ITEM;// 爱好
	private boolean[] HOBBY_ISCHOICE;// 被选择的爱好
	private String[] FRIENDTYPE_ITEM;// 交友类型
	private String[] FRIENDTYPECODE_ITEM;// 交友类型代码

	private String realname_item = "";// 真实名
	private String username_item = "";// 用户名
	private String birthday_item = "";// 生日
	private String sexCode = "0";// 性别代码
	private String educationCode = "";// 当前学历代码
	private String starCode = ""; // 当前星座代码
	private String zodiacCode = ""; // 当前生肖代码
	private String sign_item = "";// 个性签名
	private int FRIENDTYPE_ITEMID = 0;// 交友类型
	private String friendtypeCode = "";// 交友类型代码
	private PKIconResizer mImageResizer;
	static final int SET_USERICON = 0x101;// 设置头像
	static final int REFRESH_USERICON = 0x102;// 重新获取头像

	private int sex_itemid = -1;// 性别
	private int age_itemid = -1;// 年龄
	private int height_itemid = 40;// 身高
	private int weight_itemid = 8; // 体重
	private int education_itemid = -1;// 学历
	private int zodiac_itemid = -1;// 生肖
	private int star_itemid = -1;// 星座
	private int bloodtype_itemid = -1;// 血型

	private int YEAR_ITEM = 1980;
	private int MONTH_ITEM = 0;
	private int DAY_ITEM = 1;

	private int isMakeFriendSet = 0;// 是否来自交友用户, 1:是；0：否
	private String otherUserId;
	private String mUserId;
	private IAreaDao areaDao;
	private UserInfo mUserInfo;
	private IPKUserDao pkUserDao;
	private PangkerApplication application;
	private OnlineReceiver onlineReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_userinfo);
		// 注册广播
		// registerBoradcastReceiver();
		otherUserId = this.getIntent().getStringExtra(UserInfo.USERID);
		isMakeFriendSet = this.getIntent().getIntExtra("MakeFriendSet", 0);
		application = (PangkerApplication) getApplication();
		mUserInfo = application.getMySelf();
		mUserId = mUserInfo.getUserId();
		areaDao = new AreaDaoImpl(this);
		pkUserDao = new PKUserDaoImpl(this);
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		initView();
		initData();
		if (otherUserId == null) {
			initUserData(mUserInfo);
			initListener();
			loadUserInfo(mUserId);

		} else {
			loadUserInfo(otherUserId);
			tvSign.setHint("");
		}
		registerBoradcastReceiver();
	}

	private void initView() {
		imageview = (ImageView) findViewById(R.id.iv_portrait);
		tvSign = (TextView) findViewById(R.id.user_tv_sign);
		tvUsername = (TextView) findViewById(R.id.user_tv_username);

		// tvRealname = (TextView) findViewById(R.id.user_tv_realname);
		tvAccount = (TextView) findViewById(R.id.user_tv_account);
		tvSex = (TextView) findViewById(R.id.user_tv_sex);
		tvAge = (TextView) findViewById(R.id.user_tv_age);
		tvHeight = (TextView) findViewById(R.id.user_tv_height);
		tvWeight = (TextView) findViewById(R.id.user_tv_weight);
		tvDegree = (TextView) findViewById(R.id.user_tv_degree);
		tvBirthday = (TextView) findViewById(R.id.user_tv_birthday);
		tvZodiac = (TextView) findViewById(R.id.user_tv_zodiac);
		tvStar = (TextView) findViewById(R.id.user_tv_star);
		tvBlood = (TextView) findViewById(R.id.user_tv_blood);
		tvCity = (TextView) findViewById(R.id.user_tv_city);
		tvInterest = (TextView) findViewById(R.id.user_tv_interest);

		lyAccount = (LinearLayout) findViewById(R.id.user_ly_account);
		lyUsername = (LinearLayout) findViewById(R.id.user_ly_username);
		// lyRealname = (LinearLayout) findViewById(R.id.user_ly_realname);
		lySex = (LinearLayout) findViewById(R.id.user_ly_sex);
		lyAge = (LinearLayout) findViewById(R.id.user_ly_age);
		lyHeight = (LinearLayout) findViewById(R.id.user_ly_height);
		lyWeight = (LinearLayout) findViewById(R.id.user_ly_weight);
		lyDegree = (LinearLayout) findViewById(R.id.user_ly_degree);
		lyBirthday = (LinearLayout) findViewById(R.id.user_ly_birthday);
		lyZodiac = (LinearLayout) findViewById(R.id.user_ly_zodiac);
		lyStar = (LinearLayout) findViewById(R.id.user_ly_star);
		lyBlood = (LinearLayout) findViewById(R.id.user_ly_blood);
		lyCity = (LinearLayout) findViewById(R.id.user_ly_city);
		lyInterest = (LinearLayout) findViewById(R.id.user_ly_interest);
		mfLayout = (LinearLayout) findViewById(R.id.mf_ly_bg);
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UserInfoEditActivity.this.finish();
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText(R.string.setting);
		btn_submit = (Button) findViewById(R.id.iv_more);
		btn_submit.setText(R.string.save);
		btn_submit.setBackgroundResource(R.drawable.btn_default_selector);

		if (otherUserId == null) {
			tvFriendtype = (TextView) findViewById(R.id.mf_tv_friendtype);
			tvPurpose = (TextView) findViewById(R.id.user_tv_purpose);
			lySign = (LinearLayout) findViewById(R.id.user_ly_sign);
			mfLyFriendtype = (LinearLayout) findViewById(R.id.mf_ly_friendtype);
			mfLyPurpose = (LinearLayout) findViewById(R.id.mf_ly_purpose);
			//
			if (isMakeFriendSet == 1 || mUserInfo.getFriendType() == PangkerConstant.IS_MAKEFRIEND_USER) {
				mfLayout.setVisibility(View.VISIBLE);
			}
		} else {
			btn_submit.setVisibility(View.GONE);
			mfLayout.setVisibility(View.GONE);
			ivTipRight = (ImageView) findViewById(R.id.iv_right);
			ivTipRight0 = (ImageView) findViewById(R.id.iv_right0);
			ivTipRight1 = (ImageView) findViewById(R.id.iv_right1);
			ivTipRight2 = (ImageView) findViewById(R.id.iv_right2);
			ivTipRight3 = (ImageView) findViewById(R.id.iv_right3);
			ivTipRight4 = (ImageView) findViewById(R.id.iv_right4);
			ivTipRight5 = (ImageView) findViewById(R.id.iv_right5);
			ivTipRight6 = (ImageView) findViewById(R.id.iv_right6);
			ivTipRight7 = (ImageView) findViewById(R.id.iv_right7);
			ivTipRight8 = (ImageView) findViewById(R.id.iv_right8);
			ivTipRight9 = (ImageView) findViewById(R.id.iv_right9);
			ivTipRight10 = (ImageView) findViewById(R.id.iv_right10);
			ivTipRight11 = (ImageView) findViewById(R.id.iv_right11);
			ivTipRight12 = (ImageView) findViewById(R.id.iv_right12);
			ivTipRight.setVisibility(View.GONE);
			ivTipRight0.setVisibility(View.GONE);
			ivTipRight1.setVisibility(View.GONE);
			ivTipRight2.setVisibility(View.GONE);
			ivTipRight3.setVisibility(View.GONE);
			ivTipRight4.setVisibility(View.GONE);
			ivTipRight5.setVisibility(View.GONE);
			ivTipRight6.setVisibility(View.GONE);
			ivTipRight7.setVisibility(View.GONE);
			ivTipRight8.setVisibility(View.GONE);
			ivTipRight9.setVisibility(View.GONE);
			ivTipRight10.setVisibility(View.GONE);
			ivTipRight11.setVisibility(View.GONE);
			ivTipRight12.setVisibility(View.GONE);

			lyAccount.setEnabled(false);
			lyUsername.setEnabled(false);
			lySex.setEnabled(false);
			lyAge.setEnabled(false);
			lyHeight.setEnabled(false);
			lyWeight.setEnabled(false);
			lyDegree.setEnabled(false);
			lyBirthday.setEnabled(false);
			lyZodiac.setEnabled(false);
			lyStar.setEnabled(false);
			lyBlood.setEnabled(false);
			lyCity.setEnabled(false);
			lyInterest.setEnabled(false);
		}

	}

	/**
	 * 初始化监听器
	 */
	private void initListener() {
		btn_submit.setOnClickListener(mClickListener);
		lyAccount.setOnClickListener(mClickListener);
		lySign.setOnClickListener(mClickListener);
		lyUsername.setOnClickListener(mClickListener);
		lySex.setOnClickListener(mClickListener);
		lyAge.setOnClickListener(mClickListener);
		lyHeight.setOnClickListener(mClickListener);
		lyWeight.setOnClickListener(mClickListener);
		lyDegree.setOnClickListener(mClickListener);
		lyBirthday.setOnClickListener(mClickListener);
		lyZodiac.setOnClickListener(mClickListener);
		lyStar.setOnClickListener(mClickListener);
		lyBlood.setOnClickListener(mClickListener);
		lyCity.setOnClickListener(mClickListener);
		lyInterest.setOnClickListener(mClickListener);

		mfLyFriendtype.setOnClickListener(mClickListener);
		mfLyPurpose.setOnClickListener(mClickListener);

	}

	/**
	 * 获取数据数组
	 */
	private void initData() {
		SEX_ITEM = getResources().getStringArray(R.array.mf_sex_item);
		SEXCODE_ITEM = getResources().getStringArray(R.array.mf_sexcode_item);
		EDUCATION_ITEM = getResources().getStringArray(R.array.mf_education_item);
		EDUCATIONCODE_ITEM = getResources().getStringArray(R.array.mf_educationcode_item);
		BLOODTYPE_ITEM = getResources().getStringArray(R.array.mf_bloodtype_item);
		ZODIAC_ITEM = getResources().getStringArray(R.array.zodiac_item);
		ZODIACCODE_ITEM = getResources().getStringArray(R.array.zodiaccode_item);
		STAR_ITEM = getResources().getStringArray(R.array.mf_constellation_item);
		STARCODE_ITEM = getResources().getStringArray(R.array.mf_constellationcode_item);
		AGE_ITEM = getResources().getStringArray(R.array.mf_age_item);
		HEIGHT_ITEM = getResources().getStringArray(R.array.mf_height_item);
		WEIGHT_ITEM = getResources().getStringArray(R.array.weight_item);
		SEX_ITEM = getResources().getStringArray(R.array.mf_sex_item);
		HOBBY_ITEM = getResources().getStringArray(R.array.mf_label_item);
		HOBBY_ISCHOICE = new boolean[HOBBY_ITEM.length];
		FRIENDTYPE_ITEM = getResources().getStringArray(R.array.mf_friendtype_item);
		FRIENDTYPECODE_ITEM = getResources().getStringArray(R.array.mf_friendtypecode_item);
	}

	private void registerBoradcastReceiver() {
		// TODO Auto-generated method stub
		onlineReceiver = new OnlineReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(PangkerConstant.ACTTION_ICON);// 用户图像修改
		registerReceiver(onlineReceiver, filter);

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(onlineReceiver);
	}

	/**
	 * 初始化用户信息
	 */
	private void initUserData(UserInfo userInfo) {
		sign_item = userInfo.getSign();
		if (Util.isEmpty(sign_item)) {
			if (!mUserId.equals(userInfo.getUserId())) {
				tvSign.setHint(R.string.userinfo_haveno_sign);
			}
		} else {
			tvSign.setText(sign_item);
		}

		realname_item = userInfo.getRealName();
		username_item = userInfo.getUserName();
		// tvRealname.setText(realname_item != null ? realname_item : "");
		tvUsername.setText(username_item != null ? username_item : "");

		if (otherUserId == null) {
			if (!Util.isEmpty(application.getImAccount())) {
				tvAccount.setText(application.getImAccount());
			} else {
				tvAccount.setText("");
				tvAccount.setHint("未设置");
			}

		} else {
			if (!Util.isEmpty(userInfo.getAccount())) {
				tvAccount.setText(userInfo.getAccount());
			} else {
				tvAccount.setText("");
				tvAccount.setHint("未设置");
			}
		}

		sexCode = String.valueOf(userInfo.getSex());// 性别代码
		tvSex.setText(ConvertCodeToValue(sexCode, SEXCODE_ITEM, SEX_ITEM, "SEX_ITEMID"));
		tvAge.setText(String.valueOf(userInfo.getAge()));
		tvHeight.setText(userInfo.getHeight() != null ? userInfo.getHeight() + "" : "");
		tvWeight.setText(userInfo.getWeight() != null ? userInfo.getWeight() + "" : "");
		educationCode = String.valueOf(userInfo.getDegree());// 当前学历代码
		tvDegree.setText(ConvertCodeToValue(educationCode, EDUCATIONCODE_ITEM, EDUCATION_ITEM,
				"EDUCATION_ITEMID"));

		birthday_item = userInfo.getBirthday();
		tvBirthday.setText(birthday_item != null ? birthday_item : "");

		if (birthday_item != null && birthday_item.length() > 0) {
			String[] date = userInfo.getBirthday().split("-");
			if (date.length == 3) {
				YEAR_ITEM = Integer.parseInt(date[0]);
				MONTH_ITEM = Integer.parseInt(date[1]) - 1;
				DAY_ITEM = Integer.parseInt(date[2]);
			}
		}
		String Zodiac = userInfo.getZodiac() != null ? userInfo.getZodiac().toString() : "";
		// 当前生肖代码
		tvZodiac.setText(ConvertCodeToValue(Zodiac, ZODIACCODE_ITEM, ZODIAC_ITEM, "ZODIAC_ITEMID"));
		// 当前星座代码constellation
		String Constellation = userInfo.getConstellation() != null ? userInfo.getConstellation().toString() : "";
		tvStar.setText(ConvertCodeToValue(Constellation, STARCODE_ITEM, STAR_ITEM, "STAR_ITEMID"));
		tvBlood.setText(userInfo.getBloodType());
		String proviceName = areaDao.getAreaNameByID(userInfo.getProvinceId());
		String cityName = areaDao.getAreaNameByID(userInfo.getCity());
		String areaName = areaDao.getAreaNameByID(userInfo.getArea());
		String address = proviceName + "-" + cityName + "-" + areaName;
		if (proviceName.equals(cityName)) {
			address = cityName + "-" + areaName;
		}
		tvCity.setText(address);
		area = new Area();
		area.setProvinceID(userInfo.getProvinceId() != null ? userInfo.getProvinceId() : "");
		area.setCityID(userInfo.getCity() != null ? userInfo.getCity() : "");
		area.setAreaID(userInfo.getArea() != null ? userInfo.getArea() : "");
		area.setAreaAddress(address);
		tvInterest.setText(userInfo.getHobby() != null ? userInfo.getHobby() : "");
		setHobby(userInfo.getHobby() != null ? userInfo.getHobby() : "");

		if (otherUserId == null) {
			if (mUserInfo.getFriendType() == PangkerConstant.IS_MAKEFRIEND_USER) {
				tvFriendtype.setText(ConvertCodeToValue(String.valueOf(mUserInfo.getMfriendtype()),
						FRIENDTYPECODE_ITEM, FRIENDTYPE_ITEM, "FRIENDTYPE_ITEMID"));
				tvPurpose.setText(mUserInfo.getIntention());
			}
			setUserIcon(mUserId);
		} else {
			setUserIcon(otherUserId);
		}

		if (userInfo.getBloodType() != null) {
			for (int i = 0, size = BLOODTYPE_ITEM.length; i < size; i++) {
				if (BLOODTYPE_ITEM[i].equals(userInfo.getBloodType())) {
					bloodtype_itemid = i;
				}
			}
		}
		for (int i = 0, size = AGE_ITEM.length; i < size; i++) {
			if (AGE_ITEM[i].trim().equals(userInfo.getAge()+"")) {
				age_itemid = i;
			}
		}
		if (userInfo.getHeight() != null) {
			for (int i = 0, size = HEIGHT_ITEM.length; i < size; i++) {
				if (HEIGHT_ITEM[i].trim().equals(userInfo.getHeight()+"")) {
					height_itemid = i;
				}
			}
		}
		if (userInfo.getWeight() != null) {
			for (int i = 0, size = WEIGHT_ITEM.length; i < size; i++) {
				if (WEIGHT_ITEM[i].trim().equals(userInfo.getWeight()+"") ) {
					weight_itemid = i;
				}
			}
		}
	}

	/**
	 * View单击监听器
	 */
	private OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == btn_submit) {// 确定
				// 来自缘分交友申请
				if (isMakeFriendSet == 1) {
					resigstMakeFriend();// 申请交友身份
				} else {
					update();// 修改用户信息
				}
			}
			// else if (v == imageview) {
			// mShowDialog(DIALOG_ID_IMAGE);
			// }
			else if (v == lySign) {
				EditTextDialog dialog = new EditTextDialog(UserInfoEditActivity.this);
				dialog.setLine(4);
				dialog.showDialog(R.string.user_sign_num, sign_item, "", 100, new DialogResultCallback() {
					@Override
					public void buttonResult(String result, boolean isSubmit) {
						// TODO Auto-generated method stub
						if (isSubmit && result != null) {
							sign_item = result;
							tvSign.setText(sign_item);
						}
					}
				});
			} else if (v == lyUsername) {
				EditTextDialog dialog = new EditTextDialog(UserInfoEditActivity.this);
				dialog.setLine(1);
				dialog.showDialog(R.string.user_dialog_title, tvUsername.getText().toString(), "", 18,
						new DialogResultCallback() {
							@Override
							public void buttonResult(String result, boolean isSubmit) {
								// TODO Auto-generated method stub
								if (isSubmit && result != null) {
									username_item = result;
									tvUsername.setText(result);
								}
							}
						});
			}
			else if (v == lySex) {
				mShowDialog(DIALOG_ID_SEX);
			} else if (v == lyAge) {
				mShowDialog(DIALOG_ID_AGE);
			} else if (v == lyHeight) {
				mShowDialog(DIALOG_ID_HEIGHT);
			} else if (v == lyWeight) {
				mShowDialog(DIALOG_ID_WEIGHT);
			} else if (v == lyDegree) {
				mShowDialog(DIALOG_ID_EDUCATION);
			} else if (v == lyBirthday) {
				new DatePickerDialog(UserInfoEditActivity.this, listener, YEAR_ITEM, MONTH_ITEM, DAY_ITEM)
						.show();
			} else if (v == lyZodiac) {
				mShowDialog(DIALOG_ZODIAC);
			} else if (v == lyStar) {
				mShowDialog(DIALOG_ID_STAR);
			} else if (v == lyBlood) {
				mShowDialog(DIALOG_ID_BLOODTYPE);
			} else if (v == lyCity) {// 选择城市
				Intent intent = new Intent();
				intent.setClass(UserInfoEditActivity.this, CitySelectionActivity.class);
				startActivityForResult(intent, cityRequestCode);
			} else if (v == lyInterest) {
				mShowDialog(DIALOG_ID_INTEREST);
			} else if (v == mfLyFriendtype) {
				mShowDialog(DIALOG_ID_FRIENDTYPE);
			} else if (v == mfLyPurpose) {
				mShowDialog(DIALOG_ID_PURPOSE);
			} else if (v == lyAccount) {
				Intent intent = new Intent();
				intent.setClass(UserInfoEditActivity.this, FingerprintActivity.class);
				startActivityForResult(intent, FRINGER_CODE);
			}
		}
	};

	/**
	 * 先清除之前的dialog再显示新的dialog
	 * @param id
	 */
	private void mShowDialog(int id) {
		if (currentDialogId != -1) {
			removeDialog(currentDialogId);
		}
		currentDialogId = id;
		showDialog(id);
	}

	/**
	 * dialog单选以及确定、取消按钮监听
	 */
	private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			if (dialog != null) {
				dialog.cancel();
			}
			if (currentDialogId == DIALOG_ID_INTEREST) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					setLabelText();
				}
			} else if (currentDialogId == DIALOG_ID_PURPOSE) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					tvPurpose.setText(et_dialog.getText());
				}
			} else if (currentDialogId == DIALOG_ID_IMAGE) {
//				doSelection(which, true);
			} else if (currentDialogId == DIALOG_UPLOADICON) {
//				try {
//					uploadHead();
//				} catch (IOException e) {
//					logger.error(TAG, e);
//				}
			} else
				setTextView(which);
		}
	};

	/**
	 * 创建新的Dialog
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(UserInfoEditActivity.this);
		switch (id) {
		case DIALOG_ID_SEX:
			builder.setTitle(R.string.mf_set_sex);
			builder.setSingleChoiceItems(SEX_ITEM, sex_itemid, mDialogClickListener);
			break;
		case DIALOG_ID_IMAGE:
			final String[] carmera_tag = { getResourcesMessage(R.string.open_camera),
					getResourcesMessage(R.string.local_photo) };
			builder.setTitle(R.string.chooice);
			builder.setItems(carmera_tag, mDialogClickListener);
			break;
		case DIALOG_ID_AGE:
			builder.setTitle(R.string.mf_set_age_between);
			builder.setSingleChoiceItems(AGE_ITEM, age_itemid, mDialogClickListener);
			break;
		case DIALOG_ID_HEIGHT:
			builder.setTitle(R.string.mf_set_height);
			builder.setSingleChoiceItems(HEIGHT_ITEM, height_itemid, mDialogClickListener);
			break;
		case DIALOG_ID_WEIGHT:
			builder.setTitle(R.string.mf_set_weight);
			builder.setSingleChoiceItems(WEIGHT_ITEM, weight_itemid, mDialogClickListener);
			break;

		case DIALOG_ID_EDUCATION:
			builder.setTitle(R.string.mf_set_education);
			builder.setSingleChoiceItems(EDUCATION_ITEM, education_itemid, mDialogClickListener);
			break;
		case DIALOG_ZODIAC:
			builder.setTitle(R.string.user_set_zodiac);
			builder.setSingleChoiceItems(ZODIAC_ITEM, zodiac_itemid, mDialogClickListener);
			break;
		case DIALOG_ID_STAR:
			builder.setTitle(R.string.mf_set_constellation);
			builder.setSingleChoiceItems(STAR_ITEM, star_itemid, mDialogClickListener);
			break;

		case DIALOG_ID_BLOODTYPE:
			builder.setTitle(R.string.mf_set_bloodtype);
			builder.setSingleChoiceItems(BLOODTYPE_ITEM, bloodtype_itemid, mDialogClickListener);
			break;
		case DIALOG_UPLOADICON:
			builder.setTitle("是否上传头像?").setPositiveButton("是", mDialogClickListener)
					.setNegativeButton("否", null);
			break;
		case DIALOG_ID_INTEREST:
			builder.setTitle(R.string.mf_set_label);
			builder.setMultiChoiceItems(HOBBY_ITEM, HOBBY_ISCHOICE, mOnMultiChoiceListener);
			builder.setPositiveButton(R.string.submit, mDialogClickListener);
			builder.setNegativeButton(R.string.cancel, null);
			break;
		case DIALOG_ID_FRIENDTYPE:
			builder.setTitle(R.string.mf_title_friendtype);
			builder.setSingleChoiceItems(FRIENDTYPE_ITEM, FRIENDTYPE_ITEMID, mDialogClickListener);
			break;
		case DIALOG_ID_PURPOSE:
			builder.setTitle(R.string.user_set_interest_title);
			dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_editview, null);
			et_dialog = (EditText) dialogView.findViewById(R.id.et_dialog);

			et_dialog.setFilters(new InputFilter[] { new InputFilter.LengthFilter(50) });
			et_dialog.setText(tvPurpose.getText());
			et_dialog.setLines(3);
			et_dialog.setHint(R.string.please_input);
			builder.setView(dialogView);
			builder.setPositiveButton(R.string.submit, mDialogClickListener);
			builder.setNegativeButton(R.string.cancel, null);
			break;
		}
		return builder.create();
	}

	/**
	 * 在弹出标签选择框之前把LABEL_ISCHOICE布尔数组全部设置为false
	 */
	private void setLabelFalse() {
		for (int i = 0; i < HOBBY_ISCHOICE.length; i++) {
			HOBBY_ISCHOICE[i] = false;
		}
	}

	private void setHobby(String hobby) {
		setLabelFalse();
		String[] hobbys = hobby.split(",");
		for (int i = 0; i < HOBBY_ITEM.length; i++) {
			for (int j = 0; j < hobbys.length; j++) {
				if (HOBBY_ITEM[i].equals(hobbys[j])) {
					HOBBY_ISCHOICE[i] = true;
				}
			}
		}
	}

	/**
	 * dialog多选监听
	 */
	private OnMultiChoiceClickListener mOnMultiChoiceListener = new OnMultiChoiceClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which, boolean isChecked) {
			if (which >= 0) {
				HOBBY_ISCHOICE[which] = isChecked;
			}
		}
	};

	/**
	 * 多选标签的dialog确定返回后设置标签Text
	 */
	protected void setLabelText() {
		StringBuffer labelStr = new StringBuffer();
		for (int i = 0; i < HOBBY_ITEM.length; i++) {
			if (labelStr.toString().length() < 1 && HOBBY_ISCHOICE[i]) {
				labelStr.append(HOBBY_ITEM[i]);
			} else if (HOBBY_ISCHOICE[i]) {
				labelStr.append(",");
				labelStr.append(HOBBY_ITEM[i]);
			}
		}
		tvInterest.setText(labelStr.toString());
	}

	/**
	 * 日期监听
	 */
	private DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, monthOfYear);
			cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			if (Calendar.getInstance().before(cal)) {
				showToast(R.string.personall_info_birthday);
				return;
			}
			SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
			birthday_item = simple.format(cal.getTime());
			tvBirthday.setText(birthday_item);
			YEAR_ITEM = year;
			MONTH_ITEM = monthOfYear;
			DAY_ITEM = dayOfMonth;
			try {
				int age = Util.getAge(cal.getTime());
				tvAge.setText(String.valueOf(age));
				for (int i = 0, size = AGE_ITEM.length; i < size; i++) {
					if (Integer.parseInt(AGE_ITEM[i]) == age) {
						age_itemid = i;
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error(TAG, e);
			}
			zodiac_itemid = Util.date2Zodica(cal);
			tvZodiac.setText(ZODIAC_ITEM[zodiac_itemid]);// 生肖
			zodiacCode = ZODIACCODE_ITEM[zodiac_itemid];

			star_itemid = Util.date2Constellation(cal);
			tvStar.setText(STAR_ITEM[star_itemid]);
			starCode = STARCODE_ITEM[star_itemid];
		}
	};

//	/**
//	 * 上传用户头像
//	 * 
//	 * @param stream
//	 *            void
//	 * @throws IOException
//	 */
//	private void uploadHead() throws IOException {
//		ServerSupportManager serverMana = new ServerSupportManager(this, this);
//		String filePath = getTempPhotoPath();
//		File file = new File(filePath);
//		if (null == file || !file.isFile()) {
//			file = new File(filePath);
//		}
//		List<Parameter> paras = new ArrayList<Parameter>();
//		paras.add(new Parameter("uid", mUserId));
//		MyFilePart uploader = new MyFilePart(filePath, file, null);
//		String mess = getResources().getString(R.string.uploading);
//		serverMana.supportRequest(Configuration.getUploadHead(), paras, uploader, filePath, true, mess,
//				DIALOG_UPLOADICON);
//	}

	/**
	 * 更新
	 * 
	 * @author wubo
	 * @createtime 2012-1-4 下午03:43:37
	 */
	public void update() {
		String s_username = tvUsername.getText().toString();
		// String s_realname = tvRealname.getText().toString();
		String s_sign = tvSign.getText().toString();
		String s_age = tvAge.getText().toString();
		String s_height = tvHeight.getText().toString();
		String s_weight = tvWeight.getText().toString();
		String s_birthday = tvBirthday.getText().toString();
		String s_bloodtype = tvBlood.getText().toString();
		String s_interest = tvInterest.getText().toString();

		if (Util.isEmpty(s_username)) {
			showToast(R.string.s_username);
			return;
		}
		// 如果是交友用户，以下信息不能为空
		if (mUserInfo.getFriendType() == PangkerConstant.IS_MAKEFRIEND_USER) {
			if (Util.isEmpty(sexCode)) {
				showToast(R.string.s_sex);
				return;
			}
			if (Util.isEmpty(s_age)) {
				showToast(R.string.s_age);
				return;
			}
			if (Util.isEmpty(s_height)) {
				showToast(R.string.s_height);
				return;
			}
			if (Util.isEmpty(s_weight)) {
				showToast(R.string.s_weight);
				return;
			}
			if (Util.isEmpty(s_interest)) {
				showToast(R.string.s_interest);
				return;
			}
			if (area == null || area.getProvinceID() == null || area.getCityID() == null
					|| area.getAreaID() == null) {
				showToast(R.string.s_city);
				return;
			}
		}

		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", mUserId));
		paras.add(new Parameter("username", s_username));
		paras.add(new Parameter("realname", ""));
		paras.add(new Parameter("sign", s_sign));
		paras.add(new Parameter("birthday", s_birthday));

		String s_sex = tvSex.getText().toString();
		if (!Util.isEmpty(s_sex)) {
			paras.add(new Parameter("sex", sexCode != null ? sexCode : ""));
		}

		paras.add(new Parameter("country", area.getCountyID() != null ? area.getCountyID() : ""));
		paras.add(new Parameter("provinceid", area.getProvinceID()));
		paras.add(new Parameter("city", area.getCityID()));
		paras.add(new Parameter("area", area.getAreaID()));
		String s_constellation = tvStar.getText().toString();
		if (!Util.isEmpty(s_constellation)) {
			paras.add(new Parameter("constellation", starCode != null ? starCode : ""));
		}
		String s_zodiac = tvZodiac.getText().toString();
		if (!Util.isEmpty(s_zodiac)) {
			paras.add(new Parameter("zodiac", zodiacCode != null ? zodiacCode : ""));
		}
		String s_degree = tvDegree.getText().toString();
		if (!Util.isEmpty(s_degree)) {
			paras.add(new Parameter("degree", educationCode != null ? educationCode : ""));
		}

		paras.add(new Parameter("age", s_age));
		paras.add(new Parameter("weight", s_weight));
		paras.add(new Parameter("height", s_height));
		paras.add(new Parameter("hobby", s_interest));
		paras.add(new Parameter("bloodtype", s_bloodtype));
		if (mUserInfo.getFriendType() == PangkerConstant.IS_MAKEFRIEND_USER) {
			if (tvPurpose == null || tvPurpose.getText() == null
					|| Util.isEmpty(tvPurpose.getText().toString())) {
				showToast(R.string.s_purpose);
				return;
			}
			if (tvFriendtype == null || tvFriendtype.getText() == null
					|| Util.isEmpty(tvFriendtype.getText().toString())) {
				showToast(R.string.s_friendtype);
				return;
			}
			paras.add(new Parameter("mfriendtype", friendtypeCode));
			paras.add(new Parameter("intentiotn", tvPurpose.getText().toString()));
		}
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getUserinfoRefresh(), paras, true, mess, SETCODE);
	}

	/**
	 * 申请交友身份
	 * 
	 */
	public void resigstMakeFriend() {
		String s_bloodtype = tvBlood.getText().toString();

		String s_username = tvUsername.getText().toString();
		if (Util.isEmpty(s_username)) {
			showToast(R.string.s_username);
			return;
		}
		if (Util.isEmpty(sexCode)) {
			showToast(R.string.s_sex);
			return;
		}
		String s_age = tvAge.getText().toString();
		if (Util.isEmpty(s_age)) {
			showToast(R.string.s_age);
			return;
		}
		String s_height = tvHeight.getText().toString();
		if (Util.isEmpty(s_height)) {
			showToast(R.string.s_height);
			return;
		}
		String s_weight = tvWeight.getText().toString();
		if (Util.isEmpty(s_weight)) {
			showToast(R.string.s_weight);
			return;
		}
		if (area == null || area.getProvinceID() == null || area.getCityID() == null
				|| area.getAreaID() == null) {
			showToast(R.string.s_city);
			return;
		}
		String s_interest = tvInterest.getText().toString();
		if (Util.isEmpty(s_interest)) {
			showToast(R.string.s_purpose);
			return;
		}
		if (tvPurpose == null || tvPurpose.getText() == null || Util.isEmpty(tvPurpose.getText().toString())) {
			showToast(R.string.s_purpose);
			return;
		}
		if (tvFriendtype == null || tvFriendtype.getText() == null
				|| Util.isEmpty(tvFriendtype.getText().toString())) {
			showToast(R.string.s_friendtype);
			return;
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", mUserId));
		paras.add(new Parameter("operatetype", "0"));// 0：申请交友身份 1：取消交友身份（暂无）
		paras.add(new Parameter("height", s_height));// 身高(必填)
		paras.add(new Parameter("bodyweight", s_weight));// 体重(必填)
		paras.add(new Parameter("age", s_age));
		paras.add(new Parameter("sex", sexCode != null ? sexCode : ""));// 性别(必填)
		paras.add(new Parameter("country", area.getCountyID() != null ? area.getCountyID() : ""));
		paras.add(new Parameter("province", area.getProvinceID()));// 省份代码(必填)
		paras.add(new Parameter("city", area.getCityID()));// 城市码(必填)
		paras.add(new Parameter("area", area.getAreaID()));// 地区(必填)
		paras.add(new Parameter("friendtype", friendtypeCode != null ? friendtypeCode : ""));// 交友类型细分类(必填)
		paras.add(new Parameter("intention", tvPurpose.getText().toString()));// 交友意向(必填)
		paras.add(new Parameter("friendnickname", s_username));
		paras.add(new Parameter("constellation", starCode != null ? starCode : ""));// 星座（非必填）
		paras.add(new Parameter("bloodtype", s_bloodtype));// 血型代码（非必填）
		paras.add(new Parameter("hobby", s_interest));
		paras.add(new Parameter("education", educationCode != null ? educationCode : ""));// 学历（非必填）
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getFriendStatusManager(), paras, true, mess, RESIGST_CASEKEY);
	}

	/**
	 * 加载用户信息
	 * 
	 * @author wubo
	 * 
	 */
	public void loadUserInfo(String userId) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", mUserId));
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("optype", "1"));
		String message = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getUserinfoQuery(), paras, true, message, LOADCODE);
	}

	/**
	 * Dialog显示后被点击返回值设置
	 * 
	 * @param index
	 */
	private void setTextView(int index) {
		switch (currentDialogId) {
		case DIALOG_ID_SEX:
			sex_itemid = index;
			tvSex.setText(SEX_ITEM[index]);
			sexCode = SEXCODE_ITEM[index];
			break;
		case DIALOG_ID_AGE:
			age_itemid = index;
			tvAge.setText(AGE_ITEM[index]);
			break;
		case DIALOG_ID_HEIGHT:
			height_itemid = index;
			tvHeight.setText(HEIGHT_ITEM[index]);
			break;
		case DIALOG_ID_WEIGHT:
			weight_itemid = index;
			tvWeight.setText(WEIGHT_ITEM[index]);
			break;
		case DIALOG_ZODIAC:
			zodiac_itemid = index;
			zodiacCode = ZODIACCODE_ITEM[index];
			tvZodiac.setText(ZODIAC_ITEM[index]);
			break;
		case DIALOG_ID_EDUCATION:
			education_itemid = index;
			educationCode = EDUCATIONCODE_ITEM[index];
			tvDegree.setText(EDUCATION_ITEM[index]);
			break;
		case DIALOG_ID_STAR:
			star_itemid = index;
			starCode = STARCODE_ITEM[index];
			tvStar.setText(STAR_ITEM[index]);
			break;
		case DIALOG_ID_BLOODTYPE:
			// bloodCode = BLOODTYPECODE_ITEM[index];
			bloodtype_itemid = index;
			tvBlood.setText(BLOODTYPE_ITEM[index]);
			break;
		case DIALOG_ID_FRIENDTYPE:
			tvFriendtype.setText(FRIENDTYPE_ITEM[index]);
			friendtypeCode = FRIENDTYPECODE_ITEM[index];
			break;
		}
	}

	private com.wachoo.pangker.entity.Area area;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 选择城市后返回值
		if (requestCode == cityRequestCode && resultCode == RESULT_OK && data != null) {
			area = (com.wachoo.pangker.entity.Area) data.getExtras().getSerializable(Area.AREA_TAG);
			if (area != null) {
				tvCity.setText(area.getAreaAddress());
			}
		} else if (requestCode == FRINGER_CODE && resultCode == RESULT_OK) {
			checkFinger();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void checkFinger() {
		tvAccount.setText(!Util.isEmpty(application.getImAccount()) ? application.getImAccount() : "未设置");
		Message msg = new Message();
		msg.what = -1;
		PangkerManager.getTabBroadcastManager().sendResultMessage(HomeActivity.class.getName(), msg);
	}

	// 验证非空
	public boolean checkEmpty(String check, final String toastText) {
		if (check == null || check.equals("")) {
			showToast(toastText);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 根据得到的code匹配对应的内容
	 * 
	 * @author wubo
	 * @createtime 2012-1-4 下午05:08:45
	 * @param code
	 * @param ValuesCode
	 * @param Values
	 * @return
	 */
	public String ConvertCodeToValue(String code, String[] ValueCodes, String[] Values, String checkItem) {
		for (int i = 0; i < ValueCodes.length; i++) {
			if (code.equals(ValueCodes[i])) {
				if (checkItem.equals("SEX_ITEMID")) {
					sex_itemid = i;
				}
				if (checkItem.equals("EDUCATION_ITEMID")) {
					education_itemid = i;
				}
				if (checkItem.equals("BLOODTYPE_ITEMID")) {
					bloodtype_itemid = i;
				}
				if (checkItem.equals("STAR_ITEMID")) {
					star_itemid = i;
				}
				if (checkItem.equals("AGE_ITEMID")) {
					age_itemid = i;
				}
				if (checkItem.equals("HEIGHT_ITEMID")) {
					height_itemid = i;
				}
				if (checkItem.equals("WEIGHT_ITEMID")) {
					weight_itemid = i;
				}
				if (checkItem.equals("ZODIAC_ITEMID")) {
					zodiac_itemid = i;
				}
				if (checkItem.equals("FRIENDTYPE_ITEMID")) {
					FRIENDTYPE_ITEMID = i;
				}
				return Values[i];
			}
		}
		return "";
	}

	private void setUserIcon(String userId) {
		mImageResizer.loadImage(userId, imageview);
	}

	private class OnlineReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (PangkerConstant.ACTTION_ICON.equals(intent.getAction())) {
				mImageResizer.loadImageFromServer(mUserId, imageview);
			}
		}
	}

	private void dealHeadIcon(HeadUploadResult result) {
		// TODO Auto-generated method stub
		if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
			Intent intent = new Intent(PangkerConstant.ACTTION_ICON);
			sendBroadcast(intent);
			showToast(result.getErrorMessage());
		} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
			showToast(result.getErrorMessage());
			setUserIcon(mUserId);
		} else {
			setUserIcon(mUserId);
			showToast(R.string.to_server_fail);
		}
	}

	private void notifyAppUserInfo() {
		pkUserDao.updateUser(mUserInfo);
		mUserInfo.setIconBytes(null);
		Util.writeHomeUser(this, mUserInfo);
		application.setMySelf(mUserInfo);
		mUserInfo.setIconBytes(null);
		// 更新在群组内的信息
		if (application.getCurrunGroup() != null) {
			Intent meetService = new Intent(this, MeetRoomService.class);
			meetService.putExtra(MeetRoomService.MEETING_ROOM_INTENT,
					Configuration.CROOM2SP_STATUS_REPORT_MEMBER_UPD_FLAG);
			startService(meetService);
		}
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		if (caseKey == SETCODE) {
			BaseResult result = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() != 999) {
				showToast(result.getErrorMessage());
				if (result.getErrorCode() == BaseResult.SUCCESS) {
					// application.setMySelf(mUserInfo);
					if (!Util.isEmpty(username_item)) {
						mUserInfo.setUserName(username_item);
					}
					if (!Util.isEmpty(birthday_item)) {
						mUserInfo.setBirthday(birthday_item);
					}
					if (area != null) {
						mUserInfo.setArea(area.getAreaID());
						mUserInfo.setCity(area.getCityID());
						mUserInfo.setProvinceId(area.getProvinceID());
					}
					if (!Util.isEmpty(friendtypeCode)) {
						mUserInfo.setMfriendtype(Integer.parseInt(friendtypeCode));
					}
					if (!Util.isEmpty(starCode)) {
						mUserInfo.setConstellation(Integer.parseInt(starCode));
					}
					if (!Util.isEmpty(educationCode)) {
						mUserInfo.setDegree(Integer.parseInt(educationCode));
					}
					if (bloodtype_itemid >= 0) {
						mUserInfo.setBloodType(BLOODTYPE_ITEM[bloodtype_itemid]);
					}
					if (!Util.isEmpty(zodiacCode)) {
						mUserInfo.setZodiac(Integer.parseInt(zodiacCode));
					}
					if (weight_itemid >= 0) {
						mUserInfo.setWeight(Integer.parseInt(WEIGHT_ITEM[weight_itemid]));
					}
					if (height_itemid >= 0) {
						mUserInfo.setHeight(Integer.parseInt(HEIGHT_ITEM[height_itemid]));
					}
					if (age_itemid >= 0) {
						mUserInfo.setAge(Integer.parseInt(AGE_ITEM[age_itemid]));
					}
					mUserInfo.setSign(sign_item);

					mUserInfo.setSex(Integer.parseInt(sexCode));
					if (!Util.isEmpty(tvInterest.getText().toString())) {
						mUserInfo.setHobby(tvInterest.getText().toString());
					}
					if (!Util.isEmpty(tvPurpose.getText().toString())) {
						mUserInfo.setIntention(tvPurpose.getText().toString());
					}
					mUserInfo.setRealName(realname_item);
					notifyAppUserInfo();
					setResult(RESULT_OK);
					UserInfoEditActivity.this.finish();
				}
			} else {
				showToast(R.string.to_server_fail);
			}
		} else if (caseKey == LOADCODE) {
			UserinfoQueryResult result = JSONUtil.fromJson(supportResponse.toString(),
					UserinfoQueryResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				if (result.getUserInfo() != null) {
					pkUserDao.saveUser(result.getUserInfo());
					initUserData(result.getUserInfo());
				} else
					showToast(R.string.load_userinfo_failed);
			} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
			} else
				showToast(R.string.load_userinfo_failed);
		} else if (caseKey == DIALOG_UPLOADICON) {
			HeadUploadResult result = JSONUtil.fromJson(supportResponse.toString(), HeadUploadResult.class);
			dealHeadIcon(result);
		} else if (caseKey == RESIGST_CASEKEY) {
			BaseResult result = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				showToast(result.getErrorMessage());
				mUserInfo.setFriendType(PangkerConstant.IS_MAKEFRIEND_USER);// 设置为交友用户
				notifyAppUserInfo();
				UserInfoEditActivity.this.finish();
			} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
			} else {
				showToast(R.string.to_server_fail);
			}
		}
	}

}
