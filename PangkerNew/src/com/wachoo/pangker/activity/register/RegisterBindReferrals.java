package com.wachoo.pangker.activity.register;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;

public class RegisterBindReferrals extends CommonPopActivity implements IUICallBackInterface {
	private EditText mReferralsPKNO;
	private Button mBtnSkip;
	private Button mBtnSubmit;
	private PangkerApplication mApplication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.register_bind_referrals);

		mApplication = ((PangkerApplication) getApplication());
		// >>>>>>>
		mReferralsPKNO = (EditText) findViewById(R.id.et_referrals_pkno);
		mBtnSkip = (Button) findViewById(R.id.skip);
		mBtnSubmit = (Button) findViewById(R.id.submit_next);

		mBtnSkip.setOnClickListener(onClickListener);
		mBtnSubmit.setOnClickListener(onClickListener);
	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// >>>>跳过
			if (v == mBtnSkip) {
				Intent intent = new Intent(RegisterBindReferrals.this, RegisterPangkerNoSetting.class);
				startActivity(intent);
				RegisterBindReferrals.this.finish();
			}
			// >>>>提交保存
			else if (v == mBtnSubmit) {
				String referralsPKNO = null;
				String mUserID = null;
				mUserID = mApplication.getMyUserID();
				referralsPKNO = mReferralsPKNO.getText().toString();
				// >>>>>>>>判断是否为空
				if (!Util.isEmpty(referralsPKNO) && !Util.isEmpty(mUserID))
					bindReferrals(referralsPKNO, mUserID);
				else
					showToast("未填写推荐人的旁客号码");
			}
		}
	};

	private void bindReferrals(String referralsPKNO, String mUserID) {

		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", mUserID));
		paras.add(new Parameter("pkNo", referralsPKNO));

		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		String mess = getString(R.string.please_wait);
		serverMana.supportRequest(Configuration.getSetInvitation(), paras, true, mess);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;
		BaseResult submitResult = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
		if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
			showToast(submitResult.errorMessage);
			Intent intent = new Intent(RegisterBindReferrals.this, RegisterPangkerNoSetting.class);
			startActivity(intent);
			finish();
		} else if (submitResult != null && submitResult.errorCode == 0) {
			showToast(submitResult.errorMessage);
		} else {
			showToast(R.string.res_comment_failed);
		}
	}

	public void onAttachedToWindow() {
		this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
		super.onAttachedToWindow();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			Log.i(TAG, "KEYCODE_BACK");
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
