package com.wachoo.pangker.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.wachoo.pangker.ActivityStackManager;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.server.HttpReqCode;
import com.wachoo.pangker.util.Util;

/**
 * CommonActivity
 * 
 * @author wangxin
 * 
 */
public abstract class CommonActivity extends Activity {

	private ActivityStackManager stackManager;

	/**
	 * 
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		stackManager = PangkerManager.getActivityStackManager();
		stackManager.pushActivity(this);
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC); // 让音量键固定为媒体音量控制

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		stackManager.popActivity(this);
	}

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public void launch(Context From, Class<Activity> To, int StartFlag) {
		Util.launch(From, To, StartFlag);
	}

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public void launch(Intent intent, Context From) {
		Util.launch(intent, From);
	}

	/**
	 * 直接启动activity
	 * 
	 * @param From
	 * @param To
	 */
	public void launch(Context From, Class<Activity> To) {
		Util.launch(From, To);
	}

	/**
	 * 根据字符串显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final String toastText) {
		Toast.makeText(CommonActivity.this, toastText, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 根据Id显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final int toastText) {
		Toast.makeText(CommonActivity.this, toastText, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 取得资源文件的message
	 * 
	 * @param id
	 * @return
	 */
	public String getResourcesMessage(int id) {
		return getResources().getString(id);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public String getRawText(int id) {
		return Util.getRawText(CommonActivity.this, id);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		Log.i("popPangker", "main--onKeyDown");
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(Intent.ACTION_MAIN);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // android123提示如果是服务里调用，必须加入new
														// task标识
			i.addCategory(Intent.CATEGORY_HOME);
			startActivity(i);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 
	 * @param context
	 * @return
	 * 
	 * @author wangxin 2012-2-10 上午11:33:37
	 */
	protected boolean isActivated(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName task_info = manager.getRunningTasks(2).get(0).topActivity;
		if (task_info.getClassName().equals(context.getClass().getName())) {
			return true;
		} else if (task_info.getPackageName().equals(PangkerConstant.PANGKER_PACKAGE_NAME)) {
			return true;
		}
		return false;
	}

	public boolean HttpResponseStatus(Object supportResponse) {
		if (supportResponse == null) {
			showToast(R.string.to_server_fail);
			return false;
		}
		if (supportResponse instanceof HttpReqCode) {
			HttpReqCode httpReqCode = (HttpReqCode) supportResponse;
			if (httpReqCode.equals(HttpReqCode.no_network)) {
				showToast("请检查您的网络！");
			} else if (httpReqCode.equals(HttpReqCode.error)) {
				showToast(R.string.to_server_fail);
			}
			return false;
		} else {
			return true;
		}
	}

	protected void hideSoftInput(View v) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}
	
	protected void hideSoftInput() {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
	}
}
