package com.wachoo.pangker.activity.res;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.DocReaderActivity;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.res.ResLocalActivity.ResLocalModel;
import com.wachoo.pangker.adapter.DocAdapter;
import com.wachoo.pangker.adapter.DocAdapter.ViewHolder;
import com.wachoo.pangker.adapter.LocalApkAdapter;
import com.wachoo.pangker.adapter.LocalApkAdapter.ViewHolderLocal;
import com.wachoo.pangker.adapter.MusicPlayAdapter;
import com.wachoo.pangker.adapter.MusicPlayAdapter.PlayerStatus;
import com.wachoo.pangker.adapter.MyAlbumAdapter;
import com.wachoo.pangker.adapter.MyAlbumAdapter.ViewHolderAlbum;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.SqliteMusicQuery;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.music.MusicPlayerListener;
import com.wachoo.pangker.receiver.StorageEventFilter;
import com.wachoo.pangker.receiver.StorageEventReceiver;
import com.wachoo.pangker.receiver.StorageEventReceiver.StorageEventListener;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

public class ResLocalManagerActivity extends CommonPopActivity implements MusicPlayerListener {

	private final String RES_INDEX = "RES_LOCAL_INDEX";
	public final static int RequestCode = 0x100;
	public final static int RequestCode_Reader = 0x101;
	public final static int RequestCode_FILE = 0x102;

	private PangkerApplication application;
	private int index_type;
	private SharedPreferencesUtil spu;

	private ResLocalModel Model;// 全局控制，主要是进入这个界面之后的操作
	private String userId;
	private String groupId;
	private String groupName;
	protected DirInfo dirInfo;// 本地资源信息
	protected IResDao iResDao;

	private Button btnBack;
	private Button ivMore;
	private TextView tvWarnings;
	private RadioGroup radioGroup;

	private QuickAction quickAction;
	private List<ActionItem> docActionItems;
	private List<ActionItem> picActionItems;
	private List<ActionItem> musicActionItems;
	private List<ActionItem> appActionItems;

	private PopMenuDialog menuDialog;
	/****************************************** DOC **********************************************************/
	private LinearLayout docLayout;
	private ListView docListView;
	private EmptyView docEmptyView;
	private DocAdapter docAdapter;
	private List<DirInfo> docList;
	protected ResLocalModel docLocalStatu = ResLocalModel.seeStatu;// 状态只有编辑和查看
	private UiDocEdit uiDocEdit;
	/****************************************** MUSIC *********************************************************/
	private LinearLayout musicLayout;
	private ListView musicListView;
	private EmptyView musicEmptyView;
	private MusicPlayAdapter musicAdapter;
	private MusicInfo musicInfo;
	protected ResLocalModel musicLocalStatu = ResLocalModel.seeStatu;// 状态只有编辑和查看
	private SqliteMusicQuery sqliteMusicQuery;
	private boolean ifBind;
	private UIPlayerMini uiPlayerMini;
	private MediaPlayer mediaPlayer;
	private int music_index;// 这个表示仅仅是长按的操作；
	/****************************************** PCITURE **********************************************************/
	private LinearLayout picLayout;
	private GridView picGridView;
	private EmptyView picEmptyView;
	private MyAlbumAdapter imageAdapter;
	private List<DirInfo> picLists;
	private int pic_index;// 这个表示仅仅是长按的操作；

	// >>>>>>>>>获取本地图片
	private ImageLocalFetcher localFetcher;

	protected ResLocalModel picLocalStatu = ResLocalModel.seeStatu;// 状态只有编辑和查看
	private UiPicEdit uiPicEdit;
	/****************************************** APP **********************************************************/
	private LinearLayout appLayout;
	private ListView appListView;
	private EmptyView appEmptyView;
	private LocalApkAdapter appAdapter;
	private List<DirInfo> appList;
	protected ResLocalModel appLocalStatu = ResLocalModel.seeStatu;// 状态只有编辑和查看
	private UiAppEdit uiAppEdit;
	private ImageApkIconFetcher imageApkIconFetcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_local_show);
		registerReceiver();
		init();
		initView();
		checkSD();
		showScrean();
	}

	private void registerReceiver() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		storageEventReceiver = new StorageEventReceiver(mStorageEventListener);
		registerReceiver(storageEventReceiver, new StorageEventFilter());
	}

	StorageEventReceiver storageEventReceiver;

	StorageEventListener mStorageEventListener = new StorageEventListener() {
		@Override
		public void onStorageStateChanged(boolean isAvailable, boolean writeAble) {
			// TODO Auto-generated method stub
			if (!isAvailable) {
				finish();
			}
		}
	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		// >>>>>>>清空缓存
		Log.d(TAG, "onDestroy");
		unregisterReceiver(storageEventReceiver);
		if (Util.isEmpty(groupId) && Model != ResLocalModel.upLoad) {
			PangkerManager.getMusicPlayerManager().removeMusicPlayerListenter(this);
			if (ifBind) {
				application.unbindService(musicConn);
			}
		}
		super.onDestroy();
	}

	private void init() {
		// TODO Auto-generated method stub
		userId = application.getMyUserID();
		iResDao = new ResDaoImpl(this);
		groupId = getIntent().getStringExtra(UserGroup.GROUPID_KEY);
		if (getIntent().getSerializableExtra("ResMiddleManager_Flag") == null) {
			Model = ResLocalModel.seeStatu;
		} else {
			Model = (ResLocalModel) getIntent().getSerializableExtra("ResMiddleManager_Flag");
		}
		groupName = getIntent().getStringExtra(UserGroup.GROUP_NAME_KEY);

		spu = new SharedPreferencesUtil(this);
		index_type = spu.getInt(RES_INDEX + userId, PangkerConstant.RES_PICTURE);

		mediaPlayer = PangkerManager.getMediaPlayer();
		initMenu();

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = false;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>>>加载
		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		localFetcher.setmSaveDiskCache(false);
		localFetcher.setImageCache(mImageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);

		imageApkIconFetcher = new ImageApkIconFetcher(this);
		imageApkIconFetcher.setmSaveDiskCache(false);
		imageApkIconFetcher.setImageCache(mImageCache);
		imageApkIconFetcher.setLoadingImage(R.drawable.icon46_apk);
	}

	private void initMenu() {
		// TODO Auto-generated method stub
		docActionItems = new ArrayList<ActionItem>();
		if (Model == ResLocalModel.seeStatu) {
			docActionItems.add(new ActionItem(1, "编辑我的文档", ActionItem.buildDrawable(this,
					R.drawable.comp_list_menubut_change)));
		}
		docActionItems.add(new ActionItem(2, getString(R.string.res_local_auto), ActionItem.buildDrawable(this,
				R.drawable.icon64_file)));
		docActionItems.add(new ActionItem(3, getString(R.string.res_local_directory_lookup), ActionItem.buildDrawable(
				this, R.drawable.icon64_file)));

		picActionItems = new ArrayList<ActionItem>();
		if (Model == ResLocalModel.seeStatu) {
			picActionItems.add(new ActionItem(1, "编辑我的图片", ActionItem.buildDrawable(this,
					R.drawable.comp_list_menubut_change)));
		}
		picActionItems.add(new ActionItem(2, getString(R.string.res_local_auto), ActionItem.buildDrawable(this,
				R.drawable.icon64_file)));
		picActionItems.add(new ActionItem(3, getString(R.string.res_local_directory_lookup), ActionItem.buildDrawable(
				this, R.drawable.icon64_file)));

		musicActionItems = new ArrayList<ActionItem>();
		if (Model == ResLocalModel.seeStatu) {
			musicActionItems.add(new ActionItem(1, "编辑我的音乐", ActionItem.buildDrawable(this,
					R.drawable.comp_list_menubut_change), false));
		}
		musicActionItems.add(new ActionItem(3, getString(R.string.res_local_directory_lookup), ActionItem
				.buildDrawable(this, R.drawable.icon64_file)));

		appActionItems = new ArrayList<ActionItem>();
		if (Model == ResLocalModel.seeStatu) {
			appActionItems.add(new ActionItem(1, "编辑我的应用", ActionItem.buildDrawable(this,
					R.drawable.comp_list_menubut_change)));
		}
		appActionItems.add(new ActionItem(2, getString(R.string.res_local_auto), ActionItem.buildDrawable(this,
				R.drawable.icon64_file)));
		appActionItems.add(new ActionItem(3, getString(R.string.res_local_directory_lookup), ActionItem.buildDrawable(
				this, R.drawable.icon64_file)));
	}

	private void initView() {
		// TODO Auto-generated method stub
		((TextView) findViewById(R.id.mmtitle)).setText("手机本地资源");
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		ivMore = (Button) findViewById(R.id.iv_more);
		ivMore.setBackgroundResource(R.drawable.btn_menu_bg);
		ivMore.setOnClickListener(onClickListener);
		ivMore.setVisibility(index_type != PangkerConstant.RES_MUSIC ? View.VISIBLE: View.GONE);

		tvWarnings = (TextView) findViewById(R.id.tv_warnings);
		radioGroup = (RadioGroup) findViewById(R.id.res_rg);
		radioGroup.setOnCheckedChangeListener(onCheckedChangeListener);

		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);

		docLayout = (LinearLayout) findViewById(R.id.doc_layout);// 阅读
		picLayout = (LinearLayout) findViewById(R.id.pic_layout);// 图片
		musicLayout = (LinearLayout) findViewById(R.id.music_layout);// 音乐
		appLayout = (LinearLayout) findViewById(R.id.app_layout);// 应用
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				onBackPressed();
			}
			if (v == ivMore) {
				quickAction.show(ivMore);
			}
		}
	};

	OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			switch (checkedId) {
			case R.id.res_read:
				index_type = PangkerConstant.RES_DOCUMENT;
				break;
			case R.id.res_pic:
				index_type = PangkerConstant.RES_PICTURE;
				break;
			case R.id.res_music:
				index_type = PangkerConstant.RES_MUSIC;
				break;
			case R.id.res_app:
				index_type = PangkerConstant.RES_APPLICATION;
				break;
			}
			ivMore.setVisibility(index_type != PangkerConstant.RES_MUSIC ? View.VISIBLE: View.GONE);
			spu.saveInt(RES_INDEX + userId, index_type);
			showScrean();
		}
	};

	private void sendQuickAction(int actionId) {
		// TODO Auto-generated method stub
		switch (index_type) {
		case PangkerConstant.RES_DOCUMENT:
			if (actionId == 1) {
				editDoc();
			} else if (actionId == 2) {
				toLocalDocRes();
			} else if (actionId == 3) {
				toDocFileRes();
			}
			break;
		case PangkerConstant.RES_PICTURE:
			if (actionId == 1) {
				editPic();
			} else if (actionId == 2) {
				toLocalPicRes();
			} else if (actionId == 3) {
				toPicFileRes();
			}
			break;
		case PangkerConstant.RES_MUSIC:
			if (actionId == 1) {
				// editMusic();
			} else if (actionId == 2) {

			} else if (actionId == 3) {
				toMusicFileRes();
			}
			break;
		case PangkerConstant.RES_APPLICATION:
			if (actionId == 1) {
				editApp();
			} else if (actionId == 2) {
				toLocalAppRes();
			} else if (actionId == 3) {
				toAppFileRes();
			}
			break;
		}
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			sendQuickAction(actionId);
		}
	};

	private void showScrean() {
		// TODO Auto-generated method stub
		switch (index_type) {
		case PangkerConstant.RES_DOCUMENT:
			radioGroup.check(R.id.res_read);
			quickAction.setActionItems(docActionItems);
			docLayout.setVisibility(View.VISIBLE);
			musicLayout.setVisibility(View.GONE);
			picLayout.setVisibility(View.GONE);
			appLayout.setVisibility(View.GONE);
			initDocView();
			break;
		case PangkerConstant.RES_PICTURE:
			radioGroup.check(R.id.res_pic);
			quickAction.setActionItems(picActionItems);

			docLayout.setVisibility(View.GONE);
			musicLayout.setVisibility(View.GONE);
			picLayout.setVisibility(View.VISIBLE);
			appLayout.setVisibility(View.GONE);
			initPicView();
			break;
		case PangkerConstant.RES_MUSIC:
			radioGroup.check(R.id.res_music);
			quickAction.setActionItems(musicActionItems);

			docLayout.setVisibility(View.GONE);
			musicLayout.setVisibility(View.VISIBLE);
			picLayout.setVisibility(View.GONE);
			appLayout.setVisibility(View.GONE);
			initMusicView();
			break;
		case PangkerConstant.RES_APPLICATION:
			radioGroup.check(R.id.res_app);
			quickAction.setActionItems(appActionItems);

			docLayout.setVisibility(View.GONE);
			musicLayout.setVisibility(View.GONE);
			picLayout.setVisibility(View.GONE);
			appLayout.setVisibility(View.VISIBLE);
			initAppView();
			break;
		}
	}

	private void checkSD() {
		if (!application.ismExternalStorageAvailable()) {
			tvWarnings.setVisibility(View.VISIBLE);
			tvWarnings.setText("提示：SD卡不存在,本地资源无法查看和上传!");
		} else {
			tvWarnings.setVisibility(View.GONE);
		}
	}

	private void showMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this);
			menuDialog.setListener(mClickListener);
		}
		switch (index_type) {
		case PangkerConstant.RES_DOCUMENT:
			ActionItem[] docMenu = new ActionItem[] { new ActionItem(2, getString(R.string.localDoc_open)),
					new ActionItem(1, getString(R.string.local_res_upload)),
					new ActionItem(3, getString(R.string.localDoc_pangker_move)) };
			menuDialog.setMenus(docMenu);
			break;
		case PangkerConstant.RES_PICTURE:
			ActionItem[] picMenu = new ActionItem[] { new ActionItem(2, getString(R.string.local_pic_play)),
					new ActionItem(1, getString(R.string.local_res_upload)),
					new ActionItem(3, getString(R.string.local_pic_delete)) };
			menuDialog.setMenus(picMenu);
			break;
		case PangkerConstant.RES_MUSIC:
			ActionItem[] musicMenu = new ActionItem[] { new ActionItem(2, getString(R.string.local_music_play)),
					new ActionItem(1, getString(R.string.local_res_upload)),
					new ActionItem(3, getString(R.string.local_music_delete)) };
			menuDialog.setMenus(musicMenu);
			break;
		case PangkerConstant.RES_APPLICATION:
			ActionItem[] appMenu = new ActionItem[] { new ActionItem(2, getString(R.string.localDoc_open)),
					new ActionItem(1, getString(R.string.local_res_upload)),
					new ActionItem(3, getString(R.string.localDoc_pangker_move)) };
			menuDialog.setMenus(appMenu);
			break;
		}
		if (index_type == PangkerConstant.RES_PICTURE) {
			menuDialog.setTitle("图片操作");
		} else if (index_type == PangkerConstant.RES_MUSIC) {
			menuDialog.setTitle(musicInfo.getMusicName());
		} else {
			menuDialog.setTitle(dirInfo.getDirName());
		}
		menuDialog.show();
	}

	/* 处理长按选项 */
	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			// 资源上传
			if (actionId == 1) {
				switch (index_type) {
				case PangkerConstant.RES_DOCUMENT:
					uploadDoc(dirInfo, false);
					break;
				case PangkerConstant.RES_PICTURE:
					uploadPic(dirInfo, false);
					break;
				case PangkerConstant.RES_MUSIC:
					uploadMusic(musicInfo, false);
					break;
				case PangkerConstant.RES_APPLICATION:
					uploadAppInfo(dirInfo, false);
					break;
				}
			}
			// 资源打开
			else if (actionId == 2) {
				switch (index_type) {
				case PangkerConstant.RES_DOCUMENT:
					openDoc(dirInfo);
					break;
				case PangkerConstant.RES_PICTURE:
					palyPhoto();
					break;
				case PangkerConstant.RES_MUSIC:
					playMusic(music_index);
					break;
				case PangkerConstant.RES_APPLICATION:
					dealAppFile(dirInfo);
					break;
				}
			}
			// 资源删除
			else if (actionId == 3) {
				switch (index_type) {
				case PangkerConstant.RES_DOCUMENT:
					if (iResDao.deleteSelectResInfo(dirInfo)) {
						docAdapter.remove(dirInfo);
					}
					break;
				case PangkerConstant.RES_PICTURE:
					if (iResDao.deleteSelectResInfo(dirInfo)) {
						imageAdapter.remove(dirInfo);
					}
					break;
				case PangkerConstant.RES_MUSIC:
					if (sqliteMusicQuery.delMusic(musicInfo.getMusicId())) {
						musicAdapter.delMusic(musicInfo);
					}
					break;
				case PangkerConstant.RES_APPLICATION:
					if (iResDao.deleteSelectResInfo(dirInfo)) {
						appAdapter.remove(dirInfo);
					}
					break;
				}
			}
			menuDialog.dismiss();
		}
	};

	/****************************************** DOC **********************************************************/
	private void initDocView() {
		// TODO Auto-generated method stub
		if (docListView == null) {
			docAdapter = new DocAdapter(this, new ArrayList<DirInfo>());
			docListView = (ListView) findViewById(R.id.res_docListView);
			docListView.setAdapter(docAdapter);
			docListView.setOnItemClickListener(docOnItemClickListener);
			if (Util.isEmpty(groupId) && Model == ResLocalModel.seeStatu) {
				docListView.setOnItemLongClickListener(docOnItemLongClickListener);
			}
			docEmptyView = new EmptyView(this);
			docEmptyView.addToListView(docListView);

			uiDocEdit = new UiDocEdit();
			initDocData();
		}
	}

	private void initDocData() {
		// TODO Auto-generated method stub
		docList = application.getResDirInfos(PangkerConstant.RES_DOCUMENT);
		docAdapter.setDirInfoLists(docList);
		showDocEmptyView();
	}

	private void showDocEmptyView() {
		// TODO Auto-generated method stub
		if (application.ismExternalStorageAvailable()) {
			docEmptyView.showView(R.drawable.emptylist_icon, "提示：您还没有文档,可以通过右上角的菜单进行操作将文档添加进来!");
			docEmptyView.setBtnOne(getString(R.string.res_local_auto), docOneClickListener);
			docEmptyView.setBtnTwo(getString(R.string.res_local_directory_lookup), docTwoClickListener);
		} else {
			docEmptyView.showView(R.drawable.emptylist_icon, "提示：SD卡不存在!");
		}
	}

	AdapterView.OnItemClickListener docOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (docLocalStatu == ResLocalModel.Edit) {
				ViewHolder vHolder = (ViewHolder) view.getTag();
				vHolder.selected.toggle();
				docAdapter.selectByIndex(position, vHolder.selected.isChecked());
				int temp = docAdapter.getSeletedMembers().size();
				if (temp == 0) {
					uiDocEdit.btnDelete.setEnabled(false);
				} else {
					uiDocEdit.btnDelete.setEnabled(true);
				}
				if (temp == docAdapter.getCount()) {
					uiDocEdit.isSelectedAll = true;
					uiDocEdit.btnAll.setText("反选");
				} else {
					uiDocEdit.isSelectedAll = false;
					uiDocEdit.btnAll.setText("全选");
				}
				uiDocEdit.btnDelete.setText("删除(" + temp + ")");
			} else {
				dirInfo = (DirInfo) parent.getAdapter().getItem(position);
				dealDocFile(dirInfo);
			}
		}
	};

	private void editDoc() {
		// TODO Auto-generated method stub
		if (docLocalStatu != ResLocalModel.Edit && docAdapter.getCount() > 0) {
			docAdapter.init();
			uiDocEdit.showEdit(true);
		} else {
			uiDocEdit.showEdit(false);
		}
	}

	private void uploadDoc(DirInfo aoFile, boolean isClose) {
		if (!application.ismExternalStorageAvailable()) {
			showToast("Sd卡文件找不到,无法上传!");
			return;
		}
		Intent intent = new Intent();
		intent.putExtra(DocInfo.FILE_PATH, aoFile.getPath());
		if (!Util.isEmpty(groupId)) {
			intent.putExtra(UserGroup.GROUPID_KEY, groupId);
			intent.putExtra(UserGroup.GROUP_NAME_KEY, groupName);
		}
		intent.setClass(this, UploadDocActivity.class);
		startActivity(intent);
		if (isClose) {
			finish();
		}
	}

	private void dealDocFile(DirInfo dirInfo) {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(groupId) || Model == ResLocalModel.upLoad) {
			uploadDoc(dirInfo, true);
		} else {
			openDoc(dirInfo);
		}
	}

	/**
	 * 检查文件扩展名
	 * 
	 * @param checkItsEnd
	 * @param fileEndings
	 * @return
	 */
	private boolean checkEnds(String checkItsEnd, String[] fileEndings) {
		for (String aEnd : fileEndings) {
			if (checkItsEnd.toLowerCase().endsWith(aEnd))
				return true;
		}
		return false;
	}

	/**
	 * 打开文档
	 * 
	 * @param aoFile
	 */
	private void openDoc(DirInfo aoFile) {
		// TODO Auto-generated method stub
		if (!application.ismExternalStorageAvailable()) {
			showToast("SD卡不存在，无法查看!");
			return;
		}
		// 如果是文本格式文件
		if (checkEnds(aoFile.getDirName(), getResources().getStringArray(R.array.textEnds))) {
			Intent intent = new Intent();
			intent.setClass(this, DocReaderActivity.class);
			intent.putExtra("DirInfo", aoFile);
			startActivityForResult(intent, RequestCode_Reader);
		} else if (checkEnds(aoFile.getDirName(), getResources().getStringArray(R.array.docEnds))) {
			Intent intent = new Intent();
			intent.setClass(this, DocReaderActivity.class);
			intent.putExtra("DirInfo", aoFile);
			startActivityForResult(intent, RequestCode_Reader);
		} else {
			showToast("其他格式文件还在开发中...");
		}
	}

	private OnItemLongClickListener docOnItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			if (docLocalStatu == ResLocalModel.seeStatu) {
				dirInfo = (DirInfo) parent.getAdapter().getItem(position);
				showMenu();
			}
			return true;
		}
	};

	View.OnClickListener docOneClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toLocalDocRes();
		}
	};

	View.OnClickListener docTwoClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toDocFileRes();
		}
	};

	private void toLocalDocRes() {
		Intent intent = getIntent();
		intent.putExtra("res_type", PangkerConstant.RES_DOCUMENT);
		intent.setClass(this, SdcardResSearchActivity.class);
		startActivityForResult(intent, RequestCode);
	}

	private void toDocFileRes() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		intent.setClass(this, FileManagerActivity.class);
		intent.putExtra("file_type", PangkerConstant.RES_DOCUMENT);
		startActivityForResult(intent, RequestCode_FILE);
	}

	class UiDocEdit implements View.OnClickListener {

		private Button btnAll;
		private Button btnCancel;
		private Button btnDelete;
		private boolean isSelectedAll = false;
		private View view;

		public UiDocEdit() {
			view = findViewById(R.id.doc_eidt);
			btnAll = (Button) view.findViewById(R.id.local_all);
			btnAll.setOnClickListener(this);
			btnCancel = (Button) view.findViewById(R.id.local_cancel);
			btnCancel.setOnClickListener(this);
			btnDelete = (Button) view.findViewById(R.id.local_conf);
			btnDelete.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnAll) {
				selectAll();
			}
			if (v == btnCancel) {
				showEdit(false);
			}
			if (v == btnDelete) {
				deleteDoc();
			}
		}

		private void selectAll() {
			isSelectedAll = !isSelectedAll;
			int count = docAdapter.getCount();
			if (isSelectedAll) {
				btnAll.setText("反选");
				btnDelete.setEnabled(true);
				docAdapter.setSelectedAll(true);
				btnDelete.setText("删除(" + count + ")");
			} else {
				btnAll.setText("全选");
				btnDelete.setEnabled(false);
				docAdapter.setSelectedAll(false);
				btnDelete.setText("删除( 0 )");
			}
			if (count == 0) {
				btnDelete.setEnabled(false);
			}
		}

		private void deleteDoc() {
			List<DirInfo> lAppInfos = docAdapter.getSeletedMembers();
			for (DirInfo dirInfo : lAppInfos) {
				if (iResDao.deleteSelectResInfo(dirInfo)) {
					docList.remove(dirInfo);
				}
			}
			docAdapter.init();
			btnDelete.setEnabled(false);
			showEdit(false);
			btnDelete.setText("删除(0)");
			docAdapter.notifyDataSetChanged();
			showDocEmptyView();
		}

		public void showEdit(boolean flag) {
			if (flag) {
				docLocalStatu = ResLocalModel.Edit;
				view.setVisibility(View.VISIBLE);
				btnDelete.setEnabled(false);
				btnDelete.setText("删除(0)");
				btnAll.setText("全选");
			} else {
				docLocalStatu = ResLocalModel.seeStatu;
				view.setVisibility(View.GONE);
			}
			isSelectedAll = false;
			docAdapter.setFlagShow(flag);
		}

	}

	/****************************************** MUSIC *********************************************************/
	private ServiceConnection musicConn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			musicInfo = ((MusicService.MusicPlayerBinder) binder).getMusicInfo();
			initMusicUI();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
		}
	};

	private void initMusicView() {
		// TODO Auto-generated method stub
		if (musicListView == null) {
			musicAdapter = new MusicPlayAdapter(this);
			musicListView = (ListView) findViewById(R.id.res_musicListView);
			musicListView.setAdapter(musicAdapter);
			musicListView.setOnItemClickListener(musicOnItemClickListener);
			if (Util.isEmpty(groupId) && Model == ResLocalModel.seeStatu) {
				musicListView.setOnItemLongClickListener(musicOnItemLongClickListener);
			}

			musicEmptyView = new EmptyView(this);
			musicEmptyView.addToListView(musicListView);
			uiPlayerMini = new UIPlayerMini();

			sqliteMusicQuery = new SqliteMusicQuery(this);
			List<MusicInfo> musicInfos = sqliteMusicQuery.getMusicList();
			musicAdapter.setMusicList(musicInfos);
			showMusicEmptyView();

			if (Util.isEmpty(groupId) && Model == ResLocalModel.seeStatu) {
				ifBind = application.bindService(new Intent(this, MusicService.class), musicConn, BIND_AUTO_CREATE);
				PangkerManager.getMusicPlayerManager().addMusicPlayerListenter(this);
				Log.d("MusicInfo", "isBind11111:" + ifBind);
			}
		}
	}

	private void initMusicUI() {
		// TODO Auto-generated method stub
		Log.d("MusicInfo", "isBind:" + ifBind);
		if (musicInfo == null) {
			uiPlayerMini.txtFlag.setText("还没有音乐播放");
			return;
		}
		uiPlayerMini.playerMini.setVisibility(View.VISIBLE);
		PlayerStatus playerStatus;
		if (mediaPlayer.isPlaying()) {
			playerStatus = PlayerStatus.paly;
			uiPlayerMini.btnPlay.setBackgroundResource(R.drawable.music_pause_btn);
		} else {
			playerStatus = PlayerStatus.pause;
			uiPlayerMini.btnPlay.setBackgroundResource(R.drawable.music_play_btn);
		}
		uiPlayerMini.playerTitle.setText(musicInfo.getMusicName());
		if (musicInfo.isNetMusic()) {
			uiPlayerMini.txtFlag.setText("播放网络音乐");
		} else {
			uiPlayerMini.txtFlag.setText("播放本地音乐");
			musicAdapter.setPlayingMusicIndex(application.getPlayMusicIndex());
			musicAdapter.setPlayerStatus(playerStatus);
			musicAdapter.notifyDataSetChanged();
		}
	}

	// >>>>> wangxin 上传音乐到后台type:0被选择上传 1：选择上传
	private void uploadMusic(MusicInfo musicInfo, boolean isclose) {
		// TODO Auto-generated method stub
		if (!application.ismExternalStorageAvailable()) {
			showToast("Sd卡文件找不到,无法上传!");
			return;
		}
		Intent intent = new Intent();
		intent.putExtra("intent_music", musicInfo);
		intent.putExtra(DocInfo.FILE_PATH, musicInfo.getMusicPath());
		if (!Util.isEmpty(groupId)) {
			intent.putExtra(UserGroup.GROUPID_KEY, groupId);
			intent.putExtra(UserGroup.GROUP_NAME_KEY, groupName);
		}
		if (musicLocalStatu == ResLocalModel.change) {
			setResult(1, intent);
		} else {
			intent.setClass(this, UploadMusicActivity.class);
			startActivity(intent);
		}
		if (isclose) {
			this.finish();
		}
	}

	// >>>>>>>>>>>> edit by wangxin 播放操作 上一首 、下一首、暂停、播放。。。操作
	private void playControl(PlayControl playControl) {
		Intent intent = new Intent(this, MusicService.class);
		intent.putExtra("control", playControl);
		startService(intent);
	}

	private void playMusic(int position) {
		// TODO Auto-generated method stub
		if (!application.ismExternalStorageAvailable()) {
			showToast("Sd卡文件找不到,无法播放该音乐!");
			return;
		}
		application.setMusiList(musicAdapter.getMusicList());

		// 直接跳转到播放器页面
		// if (!application.isPlayingMusic(position)) {
		application.setPlayMusicIndex(position);
		playMusic(PlayControl.MUSIC_START_PLAY);// >>>> wangxin add
		// 启动音乐服务
		// }
		toMusicPlayerActivity();// >>>>>> wangxin add 跳转到音乐播放界面
	}

	// >>>>> wangxin 播放音乐
	private void playMusic(PlayControl playcontrol) {
		// TODO Auto-generated method stub
		Intent intentSerivce = new Intent(this, MusicService.class);
		intentSerivce.putExtra("control", playcontrol);
		startService(intentSerivce);
	}

	// >>>>>> wangxin 跳转到音乐播放界面
	private void toMusicPlayerActivity() {
		Intent intent = new Intent(this, MusicPlayActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void deaMusiclFile(int position) {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(groupId) || Model == ResLocalModel.upLoad) {
			uploadMusic(musicInfo, true);
		} else {
			playMusic(position);
		}
	}

	private void showMusicEmptyView() {
		// TODO Auto-generated method stub
		if (application.ismExternalStorageAvailable()) {
			musicEmptyView.showView(R.drawable.emptylist_icon, "提示：您还没有音乐,可以通过右上角的菜单进行操作将音乐添加进来!");
			musicEmptyView.setBtnTwo(getString(R.string.res_local_directory_lookup), musicTwoClickListener);
		} else {
			musicEmptyView.showView(R.drawable.emptylist_icon, "提示：SD卡不存在!");
		}
	}

	private void toMusicFileRes() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		intent.setClass(this, FileManagerActivity.class);
		intent.putExtra("file_type", PangkerConstant.RES_MUSIC);
		startActivityForResult(intent, RequestCode_FILE);
	}

	View.OnClickListener musicTwoClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toMusicFileRes();
		}
	};

	AdapterView.OnItemClickListener musicOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			musicInfo = (MusicInfo) parent.getItemAtPosition(position);
			// 判断是否是上传音乐
			deaMusiclFile(position);
		}
	};

	AdapterView.OnItemLongClickListener musicOnItemLongClickListener = new AdapterView.OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			if (musicLocalStatu == ResLocalModel.seeStatu) {
				musicInfo = (MusicInfo) parent.getItemAtPosition(position);
				music_index = position;
				showMenu();
			}
			return true;
		}
	};

	@Override
	public void play(MusicInfo info) {
		// TODO Auto-generated method stub
		uiPlayerMini.playerMini.setVisibility(View.VISIBLE);
		Log.d("MusicInfo", "Play");
		if (!info.isNetMusic()) {
			uiPlayerMini.txtFlag.setText("本地音乐");
			String url = Util.getAlbumArt(this, info.getMusicId());
			if (url != null) {
				uiPlayerMini.playerAlbum.setImageURI(Uri.parse(url));
			} else {
				uiPlayerMini.playerAlbum.setImageResource(R.drawable.icon46_music);
			}
			musicAdapter.setPlayerStatus(PlayerStatus.paly);
			musicAdapter.setPlayingMusicIndex(application.getPlayMusicIndex());
		} else {
			uiPlayerMini.txtFlag.setText("网络音乐");
			musicAdapter.setPlayingMusicIndex(-1);
		}
		uiPlayerMini.btnPlay.setBackgroundResource(R.drawable.music_pause_btn);
		musicAdapter.notifyDataSetChanged();
		uiPlayerMini.playerTitle.setText(info.getMusicName());
	}

	@Override
	public void pause(MusicInfo info) {
		// TODO Auto-generated method stub
		if (!info.isNetMusic()) {
			uiPlayerMini.txtFlag.setText("本地音乐");
			musicAdapter.setPlayerStatus(PlayerStatus.pause);
			musicAdapter.setPlayingMusicIndex(application.getPlayMusicIndex());
		} else {
			uiPlayerMini.txtFlag.setText("网络音乐");
			musicAdapter.setPlayingMusicIndex(-1);
		}
		uiPlayerMini.btnPlay.setBackgroundResource(R.drawable.music_play_btn);
		musicAdapter.notifyDataSetChanged();
		uiPlayerMini.playerTitle.setText(info.getMusicName());
	}

	@Override
	public void error() {
		// TODO Auto-generated method stub
		uiPlayerMini.btnPlay.setBackgroundResource(R.drawable.music_play_btn);
		musicAdapter.setPlayingMusicIndex(-1);
		musicAdapter.notifyDataSetChanged();
		uiPlayerMini.txtFlag.setText("音乐播放出错！");
	}

	@Override
	public void playOver() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBufferChanged(int percent) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBufferStatus(int status) {
		// TODO Auto-generated method stub

	}

	class UIPlayerMini implements View.OnClickListener {

		private LinearLayout playerMini;
		private TextView txtFlag;
		private TextView playerTitle;
		private ImageView playerAlbum;
		private ImageButton btnPlay;
		private ImageButton btnNext;
		private ImageButton btnPre;

		public UIPlayerMini() {
			// TODO Auto-generated constructor stub
			playerMini = (LinearLayout) findViewById(R.id.player_mini_layout);
			playerMini.setOnClickListener(this);
			playerTitle = (TextView) findViewById(R.id.player_title_text);
			txtFlag = (TextView) findViewById(R.id.txt_netflag);
			btnPlay = (ImageButton) findViewById(R.id.playpause_song);
			btnPlay.setOnClickListener(this);

			btnNext = (ImageButton) findViewById(R.id.next_song);
			btnNext.setOnClickListener(this);

			btnPre = (ImageButton) findViewById(R.id.previous_song);
			btnPre.setOnClickListener(this);

			playerAlbum = (ImageView) findViewById(R.id.player_mini_album);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnPlay) {
				if (mediaPlayer.isPlaying()) {
					playControl(PlayControl.MUSIC_PLAYER_PAUSE);// 暂停播放
				} else {
					playControl(PlayControl.MUSIC_PLAYER_PLAY);// 播放音乐
				}
			}
			if (v == btnPre) {
				playControl(PlayControl.MUSIC_PLAYER_PRE);// 上一首
			}
			if (v == btnNext) {
				playControl(PlayControl.MUSIC_PLAYER_NEXT);// 下一首
			}
			if (v == playerMini) {
				if (application.getMusiList() == null) {
					application.setMusiList(musicAdapter.getMusicList());
					application.setPlayMusicIndex(0);
				}
				toMusicPlayerActivity();
			}
		}
	}

	/****************************************** PCITURE **********************************************************/
	private void initPicView() {
		// TODO Auto-generated method stub
		if (picGridView == null) {
			picGridView = (GridView) findViewById(R.id.PicGridView);
			imageAdapter = new MyAlbumAdapter(this, localFetcher);
			picGridView.setAdapter(imageAdapter);
			picGridView.setOnItemClickListener(picOnItemClickListener);
			if (Util.isEmpty(groupId) && Model == ResLocalModel.seeStatu) {
				picGridView.setOnItemLongClickListener(picOnItemLongClickListener);
			}
			picEmptyView = new EmptyView(this);
			picEmptyView.addToGridView(picGridView);
			uiPicEdit = new UiPicEdit();
			initPicData();
		}
	}

	private void initPicData() {
		// TODO Auto-generated method stub
		picLists = application.getResDirInfos(PangkerConstant.RES_PICTURE);
		if (picLists == null) {
			picLists = new ArrayList<DirInfo>();
		}
		imageAdapter.setLocalDirInfos(picLists);
		showPicEmptyView();
	}

	private void editPic() {
		// TODO Auto-generated method stub
		if (picLocalStatu != ResLocalModel.Edit && imageAdapter.getCount() > 0) {
			imageAdapter.init();
			uiPicEdit.showEdit(true);
		} else {
			uiPicEdit.showEdit(false);
		}
	}

	private void dealPicFile(DirInfo picInfo, int position) {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(groupId) || Model == ResLocalModel.upLoad) {
			uploadPic(picInfo, true);
		} else {
			pic_index = position;
			palyPhoto();
		}
	}

	private void uploadPic(DirInfo item, boolean isclose) {
		if (!application.ismExternalStorageAvailable()) {
			showToast("Sd卡文件找不到,无法上传!");
			return;
		}
		Intent intent = getIntent();
		intent.putExtra(DocInfo.FILE_PATH, item.getPath());
		if (!Util.isEmpty(groupId)) {
			intent.putExtra(UserGroup.GROUPID_KEY, groupId);
			intent.putExtra(UserGroup.GROUP_NAME_KEY, groupName);
		}
		intent.setClass(this, UploadPicActivity.class);
		startActivity(intent);
		if (isclose) {
			this.finish();
		}
	}

	private void toLocalPicRes() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");
		startActivityForResult(intent, 1);
	}

	private void toPicFileRes() {
		// TODO Auto-generated method stub
		Intent intent2 = getIntent();
		intent2.setClass(this, FileManagerActivity.class);
		intent2.putExtra("file_type", PangkerConstant.RES_PICTURE);
		startActivityForResult(intent2, RequestCode_FILE);
	}

	// 查看图片，如果SD卡不存在无法查看图片
	private void palyPhoto() {
		// TODO Auto-generated method stub
		if (!application.ismExternalStorageAvailable()) {
			showToast("Sd卡文件找不到,无法查看 !");
			return;
		}
		ArrayList<PictureViewInfo> imagePathes = new ArrayList<PictureViewInfo>();
		for (DirInfo dirInfo : picLists) {
			PictureViewInfo viewInfo = new PictureViewInfo();
			viewInfo.setPicPath(dirInfo.getPath());
			viewInfo.setPicName(dirInfo.getDirName());
			imagePathes.add(viewInfo);
		}
		Intent it = new Intent(this, PictureBrowseActivity.class);
		it.putExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY, imagePathes);
		it.putExtra("index", pic_index);
		startActivity(it);
	}

	View.OnClickListener picOneClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toLocalPicRes();
		}
	};

	View.OnClickListener picTwoClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toPicFileRes();
		}
	};

	AdapterView.OnItemClickListener picOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (picLocalStatu == ResLocalModel.Edit) {
				ViewHolderAlbum vHollder = (ViewHolderAlbum) view.getTag();
				vHollder.selected.toggle();
				imageAdapter.selectByIndex(position, vHollder.selected.isChecked());
				int temp = imageAdapter.getSeletedCount();
				if (temp <= 0) {
					uiPicEdit.btnDelete.setEnabled(false);
				} else {
					uiPicEdit.btnDelete.setEnabled(true);
				}
				if (temp == imageAdapter.getCount()) {
					uiPicEdit.isSelectedAll = true;
					uiPicEdit.btnAll.setText("反选");
				} else {
					uiPicEdit.isSelectedAll = false;
					uiPicEdit.btnAll.setText("全选");
				}
				uiPicEdit.btnDelete.setText("删除(" + temp + ")");
			} else {
				DirInfo item = (DirInfo) parent.getItemAtPosition(position);
				dealPicFile(item, position);
			}
		}
	};

	// 长按事件
	AdapterView.OnItemLongClickListener picOnItemLongClickListener = new AdapterView.OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			if (picLocalStatu == ResLocalModel.seeStatu) {
				pic_index = position;
				dirInfo = (DirInfo) parent.getItemAtPosition(position);
				showMenu();
			}
			return true;
		}
	};

	private void showPicEmptyView() {
		// TODO Auto-generated method stub
		if (application.ismExternalStorageAvailable()) {
			picEmptyView.showView(R.drawable.emptylist_icon, "提示：您还没有图片,可以通过右上角的菜单进行操作将图片添加进来!");
			picEmptyView.setBtnOne(getString(R.string.res_local_auto), picOneClickListener);
			picEmptyView.setBtnTwo(getString(R.string.res_local_directory_lookup), picTwoClickListener);
		} else {
			picEmptyView.showView(R.drawable.emptylist_icon, "提示：SD卡不存在!");
		}
	}

	class UiPicEdit implements View.OnClickListener {

		private Button btnAll;
		private Button btnCancel;
		private Button btnDelete;
		private boolean isSelectedAll = false;
		private View view;

		public UiPicEdit() {
			// TODO Auto-generated constructor stub
			view = findViewById(R.id.pic_eidt);
			btnAll = (Button) view.findViewById(R.id.local_all);
			btnAll.setOnClickListener(this);
			btnCancel = (Button) view.findViewById(R.id.local_cancel);
			btnCancel.setOnClickListener(this);
			btnDelete = (Button) view.findViewById(R.id.local_conf);
			btnDelete.setOnClickListener(this);
		}

		private void selectAll() {
			isSelectedAll = !isSelectedAll;
			int count = imageAdapter.getCount();
			if (isSelectedAll) {
				btnAll.setText("反选");
				btnDelete.setEnabled(true);
				imageAdapter.setSelectedAll(true);
				btnDelete.setText("删除(" + count + ")");
			} else {
				btnAll.setText("全选");
				btnDelete.setEnabled(false);
				imageAdapter.setSelectedAll(false);
				btnDelete.setText("删除( 0 )");
			}
			if (count == 0) {
				btnDelete.setEnabled(false);
			}
		}

		private void deletePic() {
			List<DirInfo> lAppInfos = imageAdapter.getSeletedMembers();
			if (lAppInfos.size() <= 0) {
				return;
			}
			List<DirInfo> pangkerPicList = application.getLocalDirs().get(PangkerConstant.RES_PICTURE);
			for (DirInfo dirInfo : lAppInfos) {
				if (iResDao.deleteSelectResInfo(dirInfo)) {
					picLists.remove(dirInfo);
					pangkerPicList.remove(dirInfo);
				}
			}
			uiPicEdit.btnDelete.setEnabled(false);
			uiPicEdit.btnDelete.setText("删除(0)");
			showEdit(false);
			imageAdapter.setLocalDirInfos(picLists);
			showPicEmptyView();
		}

		public void showEdit(boolean flag) {
			if (flag) {
				picLocalStatu = ResLocalModel.Edit;
				view.setVisibility(View.VISIBLE);
				btnDelete.setEnabled(false);
				btnDelete.setText("删除(0)");
				btnAll.setText("全选");
			} else {
				picLocalStatu = ResLocalModel.seeStatu;
				view.setVisibility(View.GONE);
			}
			isSelectedAll = false;
			imageAdapter.setIfShow(flag);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnAll) {
				selectAll();
			}
			if (v == btnCancel) {
				showEdit(false);
			}
			if (v == btnDelete) {
				deletePic();
			}
		}
	}

	/****************************************** APP **********************************************************/
	private void initAppView() {
		// TODO Auto-generated method stub
		if (appListView == null) {
			appAdapter = new LocalApkAdapter(this, new ArrayList<DirInfo>(), imageApkIconFetcher);
			appListView = (ListView) findViewById(R.id.res_appListView);
			appListView.setOnItemClickListener(appOnItemClickListener);
			if (Util.isEmpty(groupId) && Model == ResLocalModel.seeStatu) {
				appListView.setOnItemLongClickListener(appOnItemLongClickListener);
			}
			appEmptyView = new EmptyView(this);
			appEmptyView.addToListView(appListView);
			appListView.setAdapter(appAdapter);
			uiAppEdit = new UiAppEdit();
			initAppData();
		}
	}

	private void initAppData() {
		// TODO Auto-generated method stub
		appList = application.getResDirInfos(PangkerConstant.RES_APPLICATION);
		if (appList == null) {
			appList = new ArrayList<DirInfo>();
		}
		appAdapter.setDirInfoLists(appList);
		showAppEmptyView();
	}

	private void showAppEmptyView() {
		// TODO Auto-generated method stub
		if (application.ismExternalStorageAvailable()) {
			appEmptyView.showView(R.drawable.emptylist_icon, "提示：您还没有应用,可以通过右上角的菜单进行操作将应用添加进来!");
			appEmptyView.setBtnOne(getString(R.string.res_local_auto), appOneClickListener);
			appEmptyView.setBtnTwo(getString(R.string.res_local_directory_lookup), appTwoClickListener);
		} else {
			appEmptyView.showView(R.drawable.emptylist_icon, "提示：SD卡不存在!");
		}
	}

	AdapterView.OnItemClickListener appOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (appLocalStatu == ResLocalModel.Edit) {
				ViewHolderLocal vHolder = (ViewHolderLocal) view.getTag();
				vHolder.selected.toggle();
				appAdapter.selectByIndex(position, vHolder.selected.isChecked());
				int temp = appAdapter.getSelectCount();
				if (temp == 0) {
					uiAppEdit.btnDelete.setEnabled(false);
				} else {
					uiAppEdit.btnDelete.setEnabled(true);
				}
				if (temp == appAdapter.getCount()) {
					uiAppEdit.isSelectedAll = true;
					uiAppEdit.btnAll.setText("反选");
				} else {
					uiAppEdit.isSelectedAll = false;
					uiAppEdit.btnAll.setText("全选");
				}
				uiAppEdit.btnDelete.setText("删除(" + temp + ")");
			} else {
				DirInfo appInfo = (DirInfo) parent.getItemAtPosition(position);
				dealAppFile(appInfo);
			}
		}
	};

	/* 上传本地程序 */
	private void uploadAppInfo(DirInfo appInfo, boolean ifclose) {
		// TODO Auto-generated method stub
		if (!application.ismExternalStorageAvailable()) {
			showToast("SD卡文件不存在，无法上传!");
			return;
		}
		Intent intent = new Intent();
		intent.putExtra(DocInfo.FILE_PATH, appInfo.getPath());
		intent.putExtra(DocInfo.FILE_NAME, appInfo.getDirName() + ".apk");
		if (!Util.isEmpty(groupId)) {
			intent.putExtra(UserGroup.GROUPID_KEY, groupId);
			intent.putExtra(UserGroup.GROUP_NAME_KEY, groupName);
		}
		intent.setClass(this, UploadAppActivity.class);
		startActivity(intent);
		if (ifclose) {
			this.finish();
		}
	}

	private void dealAppFile(DirInfo appInfo) {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(groupId) || Model == ResLocalModel.upLoad) {
			uploadAppInfo(appInfo, true);
		} else {
			String packageName = Util.getApkFileInfo(appInfo.getPath());
			if (Util.checkAppIsExist(this, packageName)) {
				openApplication(packageName, appInfo.getPath());
			} else {
				Util.instalApplication(this, appInfo.getPath());
			}
		}
	}

	MessageTipDialog mTipDialog;

	private void openApplication(final String pakegname, final String path) {
		// TODO Auto-generated method stub
		mTipDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
			@Override
			public void buttonResult(boolean isSubmit) {
				// TODO Auto-generated method stub
				if (isSubmit) {
					PackageManager packageManager = getPackageManager();
					Intent intent = new Intent();
					intent = packageManager.getLaunchIntentForPackage(pakegname);
					if (intent != null) {
						startActivity(intent);
					}
				} else {
					Util.instalApplication(ResLocalManagerActivity.this, path);
				}
			}
		}, this);
		mTipDialog.setButtonName(R.string.apk_exe, R.string.apk_inistal);
		mTipDialog.showDialog("", "旁客发现您已安装了本软件，您可以‘直接运行’或‘重新安装’两个选项");
	}

	private void editApp() {
		// TODO Auto-generated method stub
		if (appLocalStatu != ResLocalModel.Edit && appAdapter.getCount() > 0) {
			appAdapter.init();
			uiAppEdit.showEdit(true);
		} else {
			uiAppEdit.showEdit(false);
		}
	}

	// listView长按事件
	AdapterView.OnItemLongClickListener appOnItemLongClickListener = new AdapterView.OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			if (appLocalStatu == ResLocalModel.seeStatu) {
				dirInfo = (DirInfo) parent.getAdapter().getItem(position);
				showMenu();
			}
			return true;
		}
	};

	private void toLocalAppRes() {
		Intent intent = getIntent();
		intent.putExtra("res_type", PangkerConstant.RES_APPLICATION);
		intent.setClass(this, SdcardResSearchActivity.class);
		startActivityForResult(intent, RequestCode);
	}

	private void toAppFileRes() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		intent.setClass(this, FileManagerActivity.class);
		intent.putExtra("file_type", PangkerConstant.RES_APPLICATION);
		startActivityForResult(intent, RequestCode_FILE);
	}

	View.OnClickListener appOneClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toLocalAppRes();
		}
	};

	View.OnClickListener appTwoClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toAppFileRes();
		}
	};

	class UiAppEdit implements View.OnClickListener {

		private Button btnAll;
		private Button btnCancel;
		private Button btnDelete;
		private boolean isSelectedAll = false;// 表示没有全部选择
		private View view;

		public UiAppEdit() {
			// TODO Auto-generated constructor stub
			view = findViewById(R.id.app_eidt);
			btnAll = (Button) view.findViewById(R.id.local_all);
			btnAll.setOnClickListener(this);
			btnCancel = (Button) view.findViewById(R.id.local_cancel);
			btnCancel.setOnClickListener(this);
			btnDelete = (Button) view.findViewById(R.id.local_conf);
			btnDelete.setOnClickListener(this);
		}

		private void selectAll() {
			isSelectedAll = !isSelectedAll;
			int count = appAdapter.getCount();
			if (isSelectedAll) {
				btnAll.setText("反选");
				btnDelete.setEnabled(true);
				appAdapter.setSelectedAll(true);
				btnDelete.setText("删除(" + count + ")");
			} else {
				btnAll.setText("全选");
				btnDelete.setEnabled(false);
				appAdapter.setSelectedAll(false);
				btnDelete.setText("删除( 0 )");
			}
			if (count == 0) {
				btnDelete.setEnabled(false);
			}
		}

		private void deleteApp() {
			List<DirInfo> lAppInfos = appAdapter.getSeletedMembers();
			for (DirInfo dirInfo : lAppInfos) {
				if (iResDao.deleteSelectResInfo(dirInfo)) {
					appList.remove(dirInfo);
				}
			}
			appAdapter.init();
			btnDelete.setEnabled(false);
			btnDelete.setText("删除(0)");
			appAdapter.notifyDataSetChanged();
			showEdit(false);
			showAppEmptyView();
		}

		public void showEdit(boolean flag) {
			if (flag) {
				appLocalStatu = ResLocalModel.Edit;
				view.setVisibility(View.VISIBLE);
				btnDelete.setEnabled(false);
				btnDelete.setText("删除(0)");
				btnAll.setText("全选");
			} else {
				appLocalStatu = ResLocalModel.seeStatu;
				view.setVisibility(View.GONE);
			}
			isSelectedAll = false;
			appAdapter.setFlagShow(flag);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnAll) {
				selectAll();
			}
			if (v == btnCancel) {
				showEdit(false);
			}
			if (v == btnDelete) {
				deleteApp();
			}
		}
	}

	/****************************************** ActivityResult *************************************************/
	/**
	 * 获取图片在sd卡中的路径
	 * @param uri
	 * @return String
	 */
	public String getPhotoPath(Uri uri) {
		String[] proj = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID };
		Cursor cursor = managedQuery(uri, proj, null, null, null);
		if (cursor == null) {
			return null;
		}
		int colum_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		String path = cursor.getString(colum_index);
		return path;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RequestCode_Reader) {
			int remark = data.getIntExtra("remark", 0);
			dirInfo.setRemark(remark);
			iResDao.updateResInfo(dirInfo);
			docAdapter.notifyDataSetChanged();
		}
		if (requestCode == 1 && resultCode == RESULT_OK) {
			Uri photoUri = data.getData();
			if (photoUri != null) {
				String filePath = getPhotoPath(photoUri);
				DirInfo dirInfo = new DirInfo();
				if (filePath != null) {
					dirInfo.setPath(filePath);
				}
				if (!iResDao.isSelected(userId, dirInfo.getPath(), PangkerConstant.RES_PICTURE)) {
					dirInfo.setDeal(DirInfo.DEAL_SELECT);
					dirInfo.setUid(userId);
					dirInfo.setIsfile(DirInfo.ISFILE);
					dirInfo.setResType(PangkerConstant.RES_PICTURE);
					iResDao.saveSelectResInfo(dirInfo);
					imageAdapter.addPidInfo(dirInfo);

				}
			}
		}
		if (requestCode == RequestCode && resultCode == RequestCode) {
			switch (index_type) {
			case PangkerConstant.RES_DOCUMENT:
				initDocData();
				break;
			case PangkerConstant.RES_PICTURE:
				initPicData();
				break;
			case PangkerConstant.RES_MUSIC:
				break;
			case PangkerConstant.RES_APPLICATION:
				initAppData();
				break;
			}
		}
		if (requestCode == RequestCode_FILE && resultCode == RESULT_OK) {
			switch (index_type) {
			case PangkerConstant.RES_DOCUMENT:
				String docpath = data.getExtras().getString(DocInfo.FILE_PATH);
				File docFile = new File(docpath);
				if (!docFile.exists()) {
					return;
				}
				if (!iResDao.isSelected(userId, docpath, PangkerConstant.RES_DOCUMENT)) {
					DirInfo dirInfo = new DirInfo();
					dirInfo.setPath(docpath);
					dirInfo.setDeal(DirInfo.DEAL_SELECT);
					dirInfo.setUid(userId);
					dirInfo.setIsfile(DirInfo.ISFILE);
					dirInfo.setResType(PangkerConstant.RES_DOCUMENT);
					dirInfo.setDirName(docFile.getName());
					dirInfo.setFileCount((int) docFile.length());

					iResDao.saveSelectResInfo(dirInfo);
					docAdapter.addDirInfo(dirInfo);
				}
				break;
			case PangkerConstant.RES_PICTURE:
				String picpath = data.getExtras().getString(DocInfo.FILE_PATH);
				File picFile = new File(picpath);
				if (!picFile.exists()) {
					return;
				}
				if (!iResDao.isSelected(userId, picpath, PangkerConstant.RES_PICTURE)) {
					DirInfo dirInfo = new DirInfo();
					dirInfo.setPath(picpath);
					dirInfo.setDeal(DirInfo.DEAL_SELECT);
					dirInfo.setUid(userId);
					dirInfo.setIsfile(DirInfo.ISFILE);
					dirInfo.setResType(PangkerConstant.RES_PICTURE);
					dirInfo.setDirName(picFile.getName());
					dirInfo.setFileCount((int) picFile.length());

					iResDao.saveSelectResInfo(dirInfo);
					imageAdapter.addPidInfo(dirInfo);
				}
				break;
			case PangkerConstant.RES_MUSIC:
				String filepath = data.getExtras().getString(DocInfo.FILE_PATH);
				File file = new File(filepath);
				if (!file.exists()) {
					return;
				}
				if (!sqliteMusicQuery.existMusic(filepath)) {
					MusicInfo musicInfo = new MusicInfo();
					musicInfo.setMusicPath(filepath);
					musicInfo.setMusicName(file.getName());
					musicInfo.setSinger("暂无歌手信息");
					musicAdapter.addMusic(musicInfo);
				}
				break;
			case PangkerConstant.RES_APPLICATION:
				String apkpath = data.getExtras().getString(DocInfo.FILE_PATH);
				File apkFile = new File(apkpath);
				if (!apkFile.exists()) {
					return;
				}
				if (!iResDao.isSelected(userId, apkpath, PangkerConstant.RES_APPLICATION)) {
					DirInfo dirInfo = new DirInfo();
					dirInfo.setPath(apkpath);
					dirInfo.setDeal(DirInfo.DEAL_SELECT);
					dirInfo.setUid(userId);
					dirInfo.setIsfile(DirInfo.ISFILE);
					dirInfo.setResType(PangkerConstant.RES_APPLICATION);
					dirInfo.setDirName(apkFile.getName());
					dirInfo.setFileCount((int) apkFile.length());

					iResDao.saveSelectResInfo(dirInfo);
					appAdapter.addDirInfo(dirInfo);
				}
				break;
			}
		}
	}

}
