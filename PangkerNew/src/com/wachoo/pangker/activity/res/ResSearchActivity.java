package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.ActivityStackManager;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.server.HttpReqCode;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.MovingRes;
import com.wachoo.pangker.server.response.ResDirQueryResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.QuickActionBar;
import com.wachoo.pangker.ui.dialog.ListDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

//>>>>> wangxin 
public class ResSearchActivity extends CommonPopActivity implements IUICallBackInterface {

	protected final int[] MENU_INDEXS = { 0x100, 0x101, 0x102, 0x103 };
	protected final String[] MENU_TITLE = { " 按最新 ", " 按得分", " 按好评 ", " 下载量 " };
	protected QuickAction popopQuickAction;
	
	protected final static int RES_DOC_QUERY_TYPE = 0x11;
	protected final static int RES_MUS_QUERY_TYPE = 0x12;
	protected final static int RES_PIC_QUERY_TYPE = 0x13;
	protected final static int RES_APP_QUERY_TYPE = 0x14;

	protected static final int CASEKEY_RES = 0;// 资源 
	protected static final int CASEKEY_NEARGROUP = 1;// 群组

	protected final static int SHARE_TO_ME = 0x102;
	protected final static int DIR_SEARCH = 0x10;
	protected final static int DIR_TO_ME = 0x15;

	protected View loadmoreView;
	protected LinearLayout lyLoadPressBar;
	protected LinearLayout lyLoadText;
	protected TextView loadText;

	protected int startLimit = 0;
	protected int endLimit = 0;
	public static final int incremental = 12;
	private PangkerApplication pangkerApp;
	private int distanceRang = 10000;
	protected boolean isLoadAll = false;
	private String orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
	String lon = "";
	String lat = "";
	String userId = "";

	protected ServerSupportManager serverMana;

	protected ListDialog dirDialog;
	private String rootDirId;
	protected MovingRes selectItem;
	protected int type;
	protected String searchtype;

	private ActivityStackManager stackManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		pangkerApp = (PangkerApplication) getApplication();
		userId = pangkerApp.getMyUserID();
		serverMana = new ServerSupportManager(getParent(), this);
		stackManager = PangkerManager.getActivityStackManager();
		stackManager.pushActivity(this);
	}

	// >>>>>> getSearchLimit每次加载incremental个,后台接口专用
	protected Parameter getSearchLimit() {
		endLimit = startLimit + incremental;
		return new Parameter("limit", startLimit + "," + incremental);
	}
	// >>>>>> getSearchLimit每次加载incremental个 ，搜索引擎接口专用
	protected Parameter getSearchLbsLimit() {
		endLimit = startLimit + incremental;
		return new Parameter("limit", startLimit + "," + endLimit);
	}

	// >>>>>>>> getDistanceRang
	protected Parameter getDistanceRang() {
		return new Parameter("distancerange", String.valueOf(distanceRang));
	}

	// >>>>>>>> getRange 行人附近好友使用
	protected Parameter getRange() {
		return new Parameter("range", String.valueOf(distanceRang));
	}

	// >>>>>>>>>>> getLongitude
	protected Parameter getLongitude() {
		if(pangkerApp.getLocateType() != PangkerConstant.LOCATE_DRIFT){
			lon = String.valueOf(pangkerApp.getCurrentLocation().getLongitude());	
		} else {
			lon = String.valueOf(pangkerApp.getDirftLocation().getLongitude());
		}
		return new Parameter("longitude", lon);
	}

	// >>>>>>>>>>> getLatitude
	protected Parameter getLatitude() {
		if(pangkerApp.getLocateType() != PangkerConstant.LOCATE_DRIFT){
			lat = String.valueOf(pangkerApp.getCurrentLocation().getLatitude());	
		} else {
			lat = String.valueOf(pangkerApp.getDirftLocation().getLatitude());
		}
		return new Parameter("latitude", lat);
	}

	public String getLon() {
		if(pangkerApp.getLocateType() != PangkerConstant.LOCATE_DRIFT){
			lon = String.valueOf(pangkerApp.getCurrentLocation().getLongitude());	
		} else {
			lon = String.valueOf(pangkerApp.getDirftLocation().getLongitude());
		}
		return lon;
	}

	public String getLat() {
		if(pangkerApp.getLocateType() != PangkerConstant.LOCATE_DRIFT){
			lat = String.valueOf(pangkerApp.getCurrentLocation().getLatitude());	
		} else {
			lat = String.valueOf(pangkerApp.getDirftLocation().getLatitude());
		}
		return lat;
	}

	// >>>>>>>>>>> getUserID
	protected Parameter getUserID() {
		return new Parameter("userid", userId);
	}

	// >>>>>>>>>> setStartLimit
	public void setStartLimit(int startLimit) {
		this.startLimit = startLimit;
	}

	// >>>>>>>>> setStartLimit
	public void resetStartLimit() {
		startLimit = endLimit;
	}

	// 刷新，重新查询
	public void reset() {
		startLimit = 0;
		isLoadAll = false;
	}

	/**
	 * 后台数据是否已经加载完成
	 * 
	 * @return
	 */
	public boolean isLoadAll() {
		return isLoadAll;
	}

	public void setLoadAll(boolean isLoadAll) {
		this.isLoadAll = isLoadAll;
	}

	public int getStartLimit() {
		return startLimit;
	}

	public String getOrderkey() {
		return orderkey;
	}

	public void setOrderkey(String orderkey) {
		this.orderkey = orderkey;
	}

	protected void initPressBar(ListView mListView) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		loadmoreView = inflater.inflate(R.layout.loadmore_footerview, null);
		mListView.addFooterView(loadmoreView);
		lyLoadPressBar = (LinearLayout) loadmoreView.findViewById(R.id.ly_loadmore_probar);
		lyLoadText = (LinearLayout) loadmoreView.findViewById(R.id.ly_loadmore_text);
		loadText = (TextView) loadmoreView.findViewById(R.id.tv_loadmore);
	}

	protected void setPresdBarVisiable(boolean isShow) {
		if (isShow) {
			lyLoadPressBar.setVisibility(View.VISIBLE);
			lyLoadText.setVisibility(View.GONE);
		} else {
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.VISIBLE);
		}

	}

	/**
	 * 弹出搜索引擎功能菜单（上传，排序；排序包括：最新，最热，热评...）
	 * 
	 * @param view
	 */
	protected void showFunctionButton(View view, int casekey) {

		QuickActionBar qaBar = new QuickActionBar(view, 0);
		qaBar.setEnableActionsLayoutAnim(true);
		// 资源搜索页面
		if (CASEKEY_RES == casekey) {
			ActionItem actTime = new ActionItem(
					getResources().getDrawable(R.drawable.btn_menu_news_selector), getResources().getString(
							R.string.search_orderkey_time), mSearchClickListener);
			ActionItem actComment = new ActionItem(getResources().getDrawable(
					R.drawable.btn_menu_comment_selector), getResources().getString(
					R.string.search_orderkey_comment), mSearchClickListener);
			qaBar.addActionItem(actTime);
			ActionItem actScore = new ActionItem(
					getResources().getDrawable(R.drawable.btn_menu_hot_selector), getResources().getString(
							R.string.search_orderkey_score), mSearchClickListener);
			qaBar.addActionItem(actScore);
			qaBar.addActionItem(actComment);
			ActionItem actDownload = new ActionItem(getResources().getDrawable(
					R.drawable.btn_menu_download_selector), getResources().getString(
					R.string.search_orderkey_download), mSearchClickListener);
			qaBar.addActionItem(actDownload);
		}
		// 周边群组页面
		if (CASEKEY_NEARGROUP == casekey) {
			ActionItem actGroupFixed = new ActionItem(getResources().getDrawable(R.drawable.icon32_search),
					getResources().getString(R.string.search_grouptype_fixed), mSearchClickListener);
			qaBar.addActionItem(actGroupFixed);
			ActionItem actGroupMove = new ActionItem(getResources().getDrawable(R.drawable.icon32_search),
					getResources().getString(R.string.search_grouptype_move), mSearchClickListener);
			qaBar.addActionItem(actGroupMove);
			ActionItem actGroupAll = new ActionItem(getResources().getDrawable(R.drawable.icon32_search),
					getResources().getString(R.string.search_grouptype_all), mSearchClickListener);
			qaBar.addActionItem(actGroupAll);
			/*
			 * ActionItem actGroupByKey= new
			 * ActionItem(getResources().getDrawable( R.drawable.search_off),
			 * getResources().getString(R.string.search_group_bykey),
			 * mSearchClickListener); qaBar.addActionItem(actGroupByKey);
			 */
		}
		qaBar.show();
	}

	protected final OnClickListener mSearchClickListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			// TODO Auto-generated method stub
			if (view instanceof LinearLayout) {
				LinearLayout actionsLayout = (LinearLayout) view;
				QuickActionBar bar = (QuickActionBar) actionsLayout.getTag();
				bar.dismissQuickActionBar();
				TextView txtView = (TextView) actionsLayout.findViewById(R.id.tv_title);
				String actionName = txtView.getText().toString();
				/*
				 * if
				 * (actionName.equals(getResources().getString(R.string.upload_photo
				 * ))) { upload(); } else
				 */
				if (actionName.equals(getResources().getString(R.string.search_orderkey_time))) {
					orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
					searchtype = PangkerConstant.SEARCH_RANGE_DATA;
					search();
				} else if (actionName.equals(getResources().getString(R.string.search_orderkey_score))) {
					orderkey = String.valueOf(PangkerConstant.SORT_BY_SCORE_DESC);
					searchtype = PangkerConstant.SEARCH_RANGE_DATA;
					search();
				} else if (actionName.equals(getResources().getString(R.string.search_orderkey_comment))) {
					orderkey = String.valueOf(PangkerConstant.SORT_BY_COMMENTTIME_DESC);
					searchtype = PangkerConstant.SEARCH_RANGE_DATA;
					search();
				} else if (actionName.equals(getResources().getString(R.string.search_orderkey_download))) {
					orderkey = String.valueOf(PangkerConstant.SORT_BY_DOWNLOADTIMES_DESC);
					searchtype = PangkerConstant.SEARCH_RANGE_DATA;
					search();
				} else if (actionName.equals(getResources().getString(R.string.search_grouptype_fixed))) {
					searchtype = PangkerConstant.GROUP_TYPE_FIXED;
					search();
				} else if (actionName.equals(getResources().getString(R.string.search_grouptype_move))) {
					searchtype = PangkerConstant.GROUP_TYPE_MOVED;
					search();
				} else if (actionName.equals(getResources().getString(R.string.search_grouptype_all))) {
					searchtype = PangkerConstant.GROUP_TYPE_ALL;
					search();
				}
				/*
				 * elseif(actionName.equals(getResources().getString(R.string.
				 * search_group_bykey))) { searchtype =
				 * PangkerConstant.SEACH_GROUPS_BYKEYS; search(); }
				 */
			}
		}
	};

	protected void search() {

	}

	/**
	 * 弹出菜单上传按钮事件
	 */
	protected void upload() {

	}

	private DirInfo getRootIdByType(int type) {
		List<DirInfo> rootDirs = pangkerApp.getRootDirs();
		if (rootDirs == null) {
			return null;
		}
		for (DirInfo dirInfo : rootDirs) {
			if (dirInfo.getResType() == type) {
				return dirInfo;
			}
		}
		return null;
	}

	protected void showDir(List<DirInfo> dirinfos) {
		// TODO Auto-generated method stub
		if (dirinfos == null) {
			dirinfos = new ArrayList<DirInfo>();
		}
		if (getParent() != null) {
			dirDialog = new ListDialog(getParent(), R.style.MyDialog);
		} else {
			dirDialog = new ListDialog(this, R.style.MyDialog);
		}
		dirDialog.show();
		dirDialog.setOnItemClickListener(onDItemClickListener);
		dirDialog.setTitle("目录选择");
		dirinfos.add(0, getRootIdByType(type));
		dirDialog.showDir(dirinfos);

	}

	OnItemClickListener onDItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			DirInfo dirInfo = (DirInfo) parent.getItemAtPosition(position);
			dirDialog.dismiss();
			saveForMe(dirInfo.getDirId());
		}
	};

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			return;
		}
		if (caseKey == DIR_SEARCH) {
			ResDirQueryResult result = JSONUtil.fromJson(response.toString(), ResDirQueryResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showDir(result.getDirinfos());
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == DIR_TO_ME) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showToast("收藏成功!");
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
	}

	/**
	 * 搜索第二层目录
	 * @param type
	 */
	protected void searchDirInfo(int type) {
		this.type = type;
		DirInfo dirInfo = getRootIdByType(type);
		if (dirInfo != null) {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appuid", userId));
			paras.add(new Parameter("uid", userId));
			paras.add(new Parameter("dirid", dirInfo.getDirId()));
			paras.add(new Parameter("limit", "0,10"));
			serverMana.supportRequest(Configuration.getResDirQueryByDir(), paras, true, "正在发送...", DIR_SEARCH);
		} else {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appuid", userId));
			paras.add(new Parameter("uid", userId));
			paras.add(new Parameter("type", String.valueOf(type)));
			paras.add(new Parameter("limit", "0,10"));
			serverMana.supportRequest(Configuration.getDirQueryByType(), paras, true, "正在发送...", DIR_SEARCH);
		}
	}

	/**
	 * 资源收藏为已有，将他人资源收藏为本人资源
	 */
	protected void saveForMe(String dirid) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", String.valueOf(selectItem.getSid())));
		paras.add(new Parameter("type", "0"));
		paras.add(new Parameter("dirid", dirid));
		paras.add(getLongitude());
		paras.add(getLatitude());
		serverMana.supportRequest(Configuration.getGarnerRes(), paras, DIR_TO_ME);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		stackManager.popActivity(this);
	}

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public void launch(Context From, Class To, int StartFlag) {
		Util.launch(From, To, StartFlag);
	}

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public void launch(Intent intent, Context From) {
		Util.launch(intent, From);
	}

	/**
	 * 直接启动activity
	 * 
	 * @param From
	 * @param To
	 */
	public void launch(Context From, Class To) {
		Util.launch(From, To);
	}

	/**
	 * 根据字符串显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final String toastText) {
		Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 根据Id显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final int toastText) {
		Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 取得资源文件的message
	 * 
	 * @param id
	 * @return
	 */
	public String getResourcesMessage(int id) {
		return getResources().getString(id);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public String getRawText(int id) {
		return Util.getRawText(this, id);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Log.i("popPangker", "OnStart--ComActivity:" + this.getLocalClassName());
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		Log.i("popPangker", "main--onKeyDown");
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(Intent.ACTION_MAIN);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // android123提示如果是服务里调用，必须加入new
			// task标识
			i.addCategory(Intent.CATEGORY_HOME);
			startActivity(i);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 
	 * @param context
	 * @return
	 * 
	 * @author wangxin 2012-2-10 上午11:33:37
	 */
	private boolean isActivated(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
		ComponentName task_info = manager.getRunningTasks(2).get(0).topActivity;
		if (task_info.getClassName().equals(context.getClass().getName())) {
			return true;
		} else if (task_info.getPackageName().equals(PangkerConstant.PANGKER_PACKAGE_NAME)) {
			return true;
		}
		return false;
	}

	public boolean HttpResponseStatus(Object supportResponse) {
		if (supportResponse == null) {
			showToast(R.string.to_server_fail);
			return false;
		}
		if (supportResponse instanceof HttpReqCode) {
			HttpReqCode httpReqCode = (HttpReqCode) supportResponse;
			if (httpReqCode.equals(HttpReqCode.no_network)) {
				showToast("请检查您的网络！");
			} else if (httpReqCode.equals(HttpReqCode.error)) {
				showToast(R.string.to_server_fail);
			}
			return false;
		} else {
			return true;
		}
	}
	
	public void addGuideImage(final String className,int resDrawableId) {
		View view = getParent().findViewById(R.id.my_content_view);
        //View view = getWindow().getDecorView().findViewById(resounceView);//查找通过setContentView上的根布局
        if(view==null)return;
        if(SharedPreferencesUtil.activityIsGuided(this, className)){
            //引导过了
            return;
        }
        ViewParent viewParent = view.getParent();
        if(viewParent instanceof FrameLayout){
            final FrameLayout frameLayout = (FrameLayout)viewParent;
            if(resDrawableId!=0){//设置了引导图片
                final ImageView guideImage = new ImageView(this);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
                guideImage.setLayoutParams(params);
                guideImage.setScaleType(ScaleType.FIT_XY);
                guideImage.setImageResource(resDrawableId);
                guideImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        frameLayout.removeView(guideImage);
                        SharedPreferencesUtil.setIsGuided(getApplicationContext(), className);//设为已引导
                    } 
                });
                frameLayout.addView(guideImage);//添加引导图片
            }
        }
    }

}
