package com.wachoo.pangker.activity.res;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.about.AgreementActivity;
import com.wachoo.pangker.entity.LocalAppInfo;
import com.wachoo.pangker.listener.CheckBoxChangedListener;
import com.wachoo.pangker.listener.TextLimitWatcher;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.dialog.WaitingDialog;
import com.wachoo.pangker.util.Util;

/**
 * 应用上传 zhengjy 2012-2-30
 */
public class UploadAppActivity extends UploadActivity {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag
	private static final com.google.code.microlog4android.Logger logger = LoggerFactory.getLogger();

	private Button btnClear;
	private TextView tvTitle;
	private EditText tvAppName;
	private TextView txtNameCount;
	private TextView tvSize;
	private EditText etApkDetail;
	private TextView txtDetailCount; //
	private ImageView appIcon;
	private CheckBox cbIfShare;
	private LinearLayout typeLayout;// 类别
	private Button btnType;
	private TextView txtType;
	private String[] fileTypeCodes;
	private String[] fileTypeKeys;

	private LocalAppInfo mAppInfo;
	private LinearLayout selectLayout;
	private LinearLayout uploadinfo;
	private LinearLayout ly_dirinfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.res_upload_apk);

		inits();
		initView();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(this);
		if (id == 1) {
			builder.setSingleChoiceItems(fileTypeKeys, 0, dlistener);
			return builder.create();
		}
		return super.onCreateDialog(id);
	}

	DialogInterface.OnClickListener dlistener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			txtType.setText(fileTypeKeys[which]);
			uploadType = fileTypeCodes[which];
			dialog.dismiss();
		}
	};

	private void inits() {
		// TODO Auto-generated method stub
		userId = ((PangkerApplication) getApplication()).getMySelf().getUserId();
		filePath = getIntent().getStringExtra(DocInfo.FILE_PATH);
		fileTypeKeys = getResources().getStringArray(R.array.res_app_type_key);
		fileTypeCodes = getResources().getStringArray(R.array.res_app_type_value);
		uploadType = fileTypeCodes[0];
	}

	private void initView() {
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UploadAppActivity.this.onBackPressed();
			}
		});
		tvTitle = (TextView) findViewById(R.id.mmtitle);
		if (!Util.isEmpty(groupId)) {
			tvTitle.setText(R.string.res_group_upload_apk_title);
		} else {
			tvTitle.setText(R.string.res_upload_apk_title);
		}
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		btnUpload = (Button) findViewById(R.id.btn_upload);
		btnUpload.setOnClickListener(mClickListener);

		tvAppName = (EditText) findViewById(R.id.et_file_name);
		txtNameCount = (TextView) findViewById(R.id.txt_name_count);
		tvAppName.addTextChangedListener(new TextLimitWatcher(MaxNameLenght, txtNameCount, tvAppName));
		btnClear = (Button) findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(mClickListener);
		tvSize = (TextView) findViewById(R.id.tv_app_size);

		appIcon = (ImageView) findViewById(R.id.iv_apkicon);
		appIcon.setOnClickListener(mClickListener);
		cbIfShare = (CheckBox) findViewById(R.id.cb_ifshare);

		TextView txtLabel = (TextView) findViewById(R.id.tv_label);
		txtLabel.setText("应用类别:");
		typeLayout = (LinearLayout) findViewById(R.id.type_layout);
		typeLayout.setOnClickListener(mClickListener);
		txtType = (TextView) findViewById(R.id.tv_type);
		txtType.setHint(fileTypeKeys[0]);

		txtDirName = (TextView) findViewById(R.id.txtDirname);

		txtUpload = (TextView) findViewById(R.id.txt_upload);
		txtUpload.setText("点击选择本地应用文件");
		etApkDetail = (EditText) findViewById(R.id.et_apk_detail);
		txtDetailCount = (TextView) findViewById(R.id.txt_detail_count);

		selectLayout = (LinearLayout) findViewById(R.id.selectLayout);
		uploadinfo = (LinearLayout) findViewById(R.id.uploadinfo);//
		ly_dirinfo = (LinearLayout) findViewById(R.id.ly_dirinfo);

		selectLayout.setOnClickListener(mClickListener);
		uploadinfo.setOnClickListener(mClickListener);
		ly_dirinfo.setOnClickListener(mClickListener);
		btnType = (Button) findViewById(R.id.btn_type_select);
		btnType.setOnClickListener(mClickListener);
		upoladLayout = (LinearLayout) findViewById(R.id.uploadLayout);
		upoladLayout.setOnClickListener(mClickListener);

		groupLayout = (LinearLayout) findViewById(R.id.groupLayout);
		txtGroupName = (TextView) findViewById(R.id.txt_groupname);
		if (!Util.isEmpty(groupId)) {
			groupLayout.setVisibility(View.VISIBLE);
			txtGroupName.setText(groupName);
		}
		cbTerms = (CheckBox) findViewById(R.id.checkbox_item);
		cbTerms.setOnCheckedChangeListener(new CheckBoxChangedListener(btnUpload, promptDialog));
		initUploadButton();
		tvAgreement = (TextView) findViewById(R.id.tv_agreement);
		tvAgreement.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(UploadAppActivity.this, AgreementActivity.class);
				intent.putExtra(AgreementActivity.START_FLAG, AgreementActivity.UPLOAD_RES);
				UploadAppActivity.this.startActivity(intent);
			}
		});
		if (!Util.isEmpty(filePath)) {
			new DownloadTask().execute(filePath);
		}
		initDir();
	}

	private void initData(LocalAppInfo appInfo) {
		if (appInfo != null) {
			findViewById(R.id.size_layout).setVisibility(View.VISIBLE);
			tvAppName.setText(appInfo.appName);
			String strCount = tvAppName.getEditableText().toString().trim();
			if (strCount.length() > MaxNameLenght) {
				this.tvAppName.setText(strCount.substring(0, MaxNameLenght));
			}
			txtUpload.setText(tvAppName.getText());
			String appSize = Formatter.formatFileSize(this, appInfo.size);
			tvSize.setText(appSize);
			appIcon.setImageDrawable(appInfo.appIcon);
		} else
			showToast(R.string.res_get_file_failed);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		hideInputMethod(tvAppName);
		super.onBackPressed();
	}

	private void initDir() {
		// TODO Auto-generated method stub
		sDirInfo = application.getRootDir(PangkerConstant.RES_APPLICATION);
		if (sDirInfo != null) {
			selectDirId = sDirInfo.getDirId();
		} else {
			PangkerManager.getTabBroadcastManager().addMsgListener(this);
			initDirService();
		}
		String dirName = sDirInfo != null ? sDirInfo.getDirNames() : "获取目录";
		txtDirName.setText(dirName);
	}

	/**
	 * 上传资源接口
	 * 
	 * @param filepath
	 *            文件路径
	 * @param isFileExist
	 *            文件是否已经存在服务器 接口：ResUpload.json
	 */
	@Override
	protected void uploadFile(String filename, int job_type) {
		try {
			String resName = tvAppName.getText().toString();
			if (Util.isEmpty(resName)) {
				resName = mAppInfo.appName;
			}

			if (resName == null || resName.equals("")) {
				resName = filePath;
			}
			String detatil = etApkDetail.getText().toString();
			String ifShare = cbIfShare.isChecked() ? "1" : "0";
			uploaderFile(PangkerConstant.RES_APPLICATION, detatil, ifShare, resName, "apk", null, 0, job_type);
			onBackPressed();
		} catch (Exception e) {
			logger.error(TAG, e);
			showToast("异常!");
		}
	}

	private void uploadApp() {
		if (Util.isEmpty(tvAppName.getEditableText().toString())) {
			showToast("请输入应用名称!");
			return;
		}
		if (Util.isEmpty(selectDirId)) {
			showToast("请选择一个目录!");
			return;
		}
		uploadCheck(PangkerConstant.RES_APPLICATION);
	}

	private final OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			if (view == btnUpload) {
				if (!uploadCheck()) {
					return;
				}
				if (mAppInfo != null) {
					uploadApp();
				} else {
					showToast("请选择本地应用文件!");
				}
			} else if (view == selectLayout || view == ly_dirinfo) {
				selectDirInfos(PangkerConstant.RES_APPLICATION);
			} else if (view == upoladLayout || view == uploadinfo) {
				selectApk();
			} else if (view == btnClear) {
				tvAppName.setText("");
			} else if (view == typeLayout || view == btnType) {
				showDialog(1);
			}
		}
	};

	private void selectApk() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.change);
		intent.setClass(this, ResLocalAppActivity.class);
		startActivityForResult(intent, CODE_RESELECT);
	}

	class DownloadTask extends AsyncTask<String, Integer, LocalAppInfo> {

		private WaitingDialog popupDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			popupDialog = new WaitingDialog(UploadAppActivity.this);
			popupDialog.setWaitMessage(R.string.waiting);
			popupDialog.show();
		}

		@Override
		protected LocalAppInfo doInBackground(String... params) {
			mAppInfo = Util.getApkFileInfo(UploadAppActivity.this, filePath);
			return mAppInfo;
		}

		@Override
		protected void onPostExecute(LocalAppInfo appInfo) {
			popupDialog.dismiss();
			initData(appInfo);
			super.onPostExecute(appInfo);
		}
	}

	@Override
	protected void onLoaderGeo(String address) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == CODE_RESELECT && resultCode == RESULT_OK) {
			filePath = intent.getStringExtra(DocInfo.FILE_PATH);
			btnUpload.setEnabled(true);
			new DownloadTask().execute(filePath);
		}
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				initDir();
			}
		};
		handler.sendMessage(msg);
	}

}
