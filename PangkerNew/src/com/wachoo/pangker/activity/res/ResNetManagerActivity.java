﻿package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.ResAdapter;
import com.wachoo.pangker.adapter.UserStorePhotoAdapter;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.ResDirManagerResult;
import com.wachoo.pangker.server.response.ResInfo;
import com.wachoo.pangker.server.response.ResPropertySettingResult;
import com.wachoo.pangker.server.response.ResShareResult;
import com.wachoo.pangker.server.response.ResSiteQueryResult;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshGridView;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.ui.dialog.ListDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

/**
 * @author Administrator
 */
public class ResNetManagerActivity extends CommonPopActivity implements IUICallBackInterface, ITabSendMsgListener {

	public static final int DIR_SET = 0x51;
	private int pageLength = 12;

	private final int DIR_ADD = 10;// 目录增加
	private final int DIR_UPD = 11;// 目录修改
	private final int DIR_DEL = 12;// 目录删除
	private final int RES_MOVE = 0x140;// 资源移动
	private final int RES_COPY = 0x150;// 资源复制
	private final int RES_DEL = 0x160;// 资源删除
	private final int RES_QUERY = 0x170;// 资源查询
	private final int RES_SHARE = 0x21;// 资源分享
	private final int RES_USHARE = 0x22;// 资源取消分享

	private ServerSupportManager serverMana;// 后台交互
	private PangkerApplication application;
	private IResDao iResDao;// 仅仅用于修改顶层目录
	private String userId;
	private DirInfo nowDirInfo;// 当前当前操作的资源，包含目录
	private DirInfo navDirInfo;// 当前界面所指向的目录

	// 保存缓存目录数据
	private Map<String, List<DirInfo>> netResDatas = new HashMap<String, List<DirInfo>>();// >>>>wangxin
	// 保存用户自建的目录数据,根据类型
	private Map<Integer, List<DirInfo>> ownDirs = new HashMap<Integer, List<DirInfo>>();// >>>>Lab
	// 保存目录Sum数据，为的是是否显示加载分页
	private Map<String, Integer> sumDatas = new HashMap<String, Integer>();// >>>>libo
	String mess = "正在向服务器提交...";// 提交提示信息
	// 资源操作
	private PopMenuDialog menuDialog;

	ActionItem[] dirMenu1 = new ActionItem[] { new ActionItem(0, "访问权限设置"), new ActionItem(1, "删除目录"),
			new ActionItem(2, "修改目录") };
	ActionItem[] dirMenu2 = new ActionItem[] { new ActionItem(0, "访问权限设置") };
	ActionItem[] dirMenu3 = new ActionItem[] { new ActionItem(0, "移动到指定目录"), new ActionItem(1, "复制到指定目录"),
			new ActionItem(2, "删除该资源"), new ActionItem(3, "分享该资源"), new ActionItem(4, "编辑资源") };
	ActionItem[] dirMenu4 = new ActionItem[] { new ActionItem(0, "移动到指定目录"), new ActionItem(1, "复制到指定目录"),
			new ActionItem(2, "删除该资源"), new ActionItem(3, "取消分享"), new ActionItem(4, "编辑资源") };

	ActionItem[] dirMenu5 = new ActionItem[] { new ActionItem(0, "移动到指定目录"), new ActionItem(1, "复制到指定目录"),
			new ActionItem(2, "删除该资源") };

	private int deal_key;

	private TextView txtTitle;
	private Button btnBack;
	private Button ivMore;
	private QuickAction quickAction;

	private PullToRefreshListView pullToRefreshListView;
	private ListView resListView;
	private EmptyView lsitEmptyView;
	private FooterView footerView;
	private ResAdapter resAdapter;

	private PullToRefreshGridView pullToRefreshGridView;
	private GridView resGridView;
	private EmptyView gridEmptyView;
	private UserStorePhotoAdapter picAdapter;

	// 目录选择，复制
	private ListDialog dirDialog;
	private String newDirId;// 要复制的目录,和dirID并不重复,
	private String dirID;// 当前查询的目录id和展示用户的当前目录不一致
	private EditTextDialog updateDialog;
	private String txtContent;
	private boolean ifsending = true;
	private RadioGroup radioGroup;
	private int index_type = PangkerConstant.RES_PICTURE;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_net_show);
		init();
		initView();
		initDialog();
		if (application.getRootDir(index_type) != null) {
			initFristTag();
		} else {
			initResInfo();
		}
	}

	private void initFristTag() {
		// TODO Auto-generated method stub
		radioGroup.setTag(R.id.res_read, application.getRootDir(PangkerConstant.RES_DOCUMENT));
		radioGroup.setTag(R.id.res_pic, application.getRootDir(PangkerConstant.RES_PICTURE));
		radioGroup.setTag(R.id.res_music, application.getRootDir(PangkerConstant.RES_MUSIC));
		radioGroup.setTag(R.id.res_app, application.getRootDir(PangkerConstant.RES_APPLICATION));

		navDirInfo = application.getRootDir(index_type);
		enterIntoDirinfo();
	}

	private void enterIntoDirinfo() {
		if (navDirInfo == null) {
			showToast("抱歉，还没有加载到您的资源信息，请点击菜单!");
			quickAction.clearActionItem();
			quickAction.addActionItem(new ActionItem(1, "重新加载资源库"), true);
			return;
		}
		navDirInfo.setParentDirId("0");
		if (index_type == PangkerConstant.RES_PICTURE) {
			pullToRefreshGridView.setVisibility(View.VISIBLE);
			pullToRefreshListView.setVisibility(View.GONE);
		} else {
			pullToRefreshListView.setVisibility(View.VISIBLE);
			pullToRefreshGridView.setVisibility(View.GONE);
		}
		goinDirInfo(navDirInfo, false, "1");
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			refreshDirinfo();
		}
	};

	PullToRefreshBase.OnRefreshListener<GridView> onRefreshListener2 = new PullToRefreshBase.OnRefreshListener<GridView>() {
		@Override
		public void onRefresh(PullToRefreshBase<GridView> refreshView) {
			// TODO Auto-generated method stub
			refreshDirinfo();
		}
	};

	private void initView() {
		// 列表展示
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		resListView = pullToRefreshListView.getRefreshableView();
		resListView.setOnItemClickListener(mOnItemClickListener);
		resListView.setOnItemLongClickListener(mOnItemLongClickListener);
		lsitEmptyView = new EmptyView(this);
		resListView.setEmptyView(lsitEmptyView.getView());
		footerView = new FooterView(this);
		footerView.addToListView(resListView);
		footerView.setOnLoadMoreListener(onLoadMoreListener);
		resAdapter = new ResAdapter(this, new ArrayList<DirInfo>(), mImageWorker);
		resListView.setAdapter(resAdapter);
		// 矩阵显示
		pullToRefreshGridView = (PullToRefreshGridView) findViewById(R.id.pull_refresh_grid);
		// >>>>>>刷新功能
		pullToRefreshGridView.setOnRefreshListener(onRefreshListener2);
		// >>>>>>>>>图片的view显示
		resGridView = pullToRefreshGridView.getRefreshableView();
		resGridView.setNumColumns(3);
		gridEmptyView = new EmptyView(this);
		gridEmptyView.addToGridView(resGridView);

		picAdapter = new UserStorePhotoAdapter(this, new ArrayList<DirInfo>(), mImageWorker);
		resGridView.setAdapter(picAdapter);
		picAdapter.setOnClickListener(picClickListener);
		picAdapter.setOnLongClickListener(picLongClickListener);
		picAdapter.setOnLoadMoreListener(onLoadMoreListener);

		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(clickListener);
		txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("我的网盘资源");
		ivMore = (Button) findViewById(R.id.iv_more);
		ivMore.setVisibility(View.GONE);
		ivMore.setOnClickListener(clickListener);
		ivMore.setBackgroundResource(R.drawable.btn_menu_bg);

		radioGroup = (RadioGroup) findViewById(R.id.res_rg);
		radioGroup.setOnCheckedChangeListener(onCheckedChangeListener);
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		iResDao = new ResDaoImpl(this);
		List<DirInfo> rootDirs = application.getRootDirs();
		netResDatas.put("0", rootDirs);
		sumDatas.put("0", rootDirs.size());

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);

		serverMana = new ServerSupportManager(this, this);
		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);
		PangkerManager.getTabBroadcastManager().addMsgListener(this);
	}

	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			// TODO Auto-generated method stub
			if (!ifsending) {
				if (actionId == 1) {
					initResInfo();
				} else if (actionId == 2) {
					deal_key = DIR_ADD;
					showDialog();
				}
			} else {
				showToast("正在加载,请稍后...");
			}
		}
	};

	OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			switch (checkedId) {
			case R.id.res_read:
				index_type = PangkerConstant.RES_DOCUMENT;
				break;
			case R.id.res_pic:
				index_type = PangkerConstant.RES_PICTURE;
				break;
			case R.id.res_music:
				index_type = PangkerConstant.RES_MUSIC;
				break;
			case R.id.res_app:
				index_type = PangkerConstant.RES_APPLICATION;
				break;
			}
			navDirInfo = (DirInfo) group.getTag(checkedId);
			if (navDirInfo != null) {
				navDirInfo.setParentDirId("0");
				pullToRefreshListView.onRefreshComplete();
				pullToRefreshGridView.onRefreshComplete();
				enterIntoDirinfo();
			}
		}
	};

	/**
	 * 进入当前目录，nowDirInfo flagMust :进入下一个目录
	 * 
	 * @param nowDirInfo
	 */
	private void goinDirInfo(DirInfo nowDirInfo, boolean flagMust, String flag_fir) {
		// TODO Auto-generated method stub
		this.navDirInfo = nowDirInfo;
		if (!flagMust) {
			resAdapter.setResInfos(new ArrayList<DirInfo>());
			picAdapter.setResinfos(new ArrayList<DirInfo>());
		}
		getResInfo(nowDirInfo.getDirId(), flagMust, flag_fir);
	}

	private void refreshBtn() {
		if ("0".equals(navDirInfo.getParentDirId())) {
			if (ownDirs.get(index_type).size() < PangkerConstant.DIR_LIMIT) {
				ivMore.setVisibility(View.VISIBLE);
				quickAction.clearActionItem();
				quickAction.addActionItem(new ActionItem(2, "新建资源目录"), true);
			} else {
				ivMore.setVisibility(View.GONE);
			}
		} else {
			ivMore.setVisibility(View.GONE);
		}
	}

	private void showMenu() {
		// TODO Auto-generated method stub
		if (nowDirInfo.getIsfile() == 1) {

			// >>>>>>>>>如果是收藏来的资源:收藏来的资源不能分享，也不能编辑
			if (nowDirInfo.getSource() == 1) {
				menuDialog.setMenus(dirMenu5);
			}

			// >>>>>>>原创资源（用户自己上传的资源）
			else {
				if (nowDirInfo.getDeal() == 1) {
					menuDialog.setMenus(dirMenu4);
				}
				if (nowDirInfo.getDeal() == 0) {
					menuDialog.setMenus(dirMenu3);
				}
			}
		} else {
			if ("0".equals(nowDirInfo.getParentDirId())) {
				menuDialog.setMenus(dirMenu2);
			} else {
				menuDialog.setMenus(dirMenu1);
			}
		}
		if (nowDirInfo.getIsfile() == 1) {
			menuDialog.setTitle("资源操作");
		} else {
			menuDialog.setTitle("目录操作");
		}
		menuDialog.show();
	}

	private MessageTipDialog mMessageTipDialog;

	private void showTipDialog() {
		// TODO Auto-generated method stub
		if (mMessageTipDialog == null) {
			mMessageTipDialog = new MessageTipDialog(mResultCallback, this);
		}
		switch (deal_key) {
		case RES_DEL:
			mMessageTipDialog.showDialog("", "确认要删除该资源？");
			break;
		case DIR_DEL:
			mMessageTipDialog.showDialog("", "确认要删除该目录");
			break;
		}
	}

	MessageTipDialog.DialogMsgCallback mResultCallback = new MessageTipDialog.DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if (isSubmit) {
				if (deal_key == RES_DEL) {
					removeRes(nowDirInfo);
				} else if (deal_key == DIR_DEL) {
					sendServer(DIR_DEL, nowDirInfo);
				}
			}
		}
	};

	UITableView.ClickListener menuClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int which) {
			if (nowDirInfo.getIsfile() == 1) {
				// 资源删除
				if (which == 2) {
					deal_key = RES_DEL;
					showTipDialog();
				}
				// 设置分享
				else if (which == 3) {
					ushareResInfo();
				}
				// 进行编辑
				else if (which == 4) {
					editRes(nowDirInfo);
					// goinMovingRes(nowDirInfo, 0, true);
				} else {
					// 文件移动操作
					List<DirInfo> popInfos = ownDirs.get(nowDirInfo.getResType());
					if (popInfos == null) {
						showToast("当前资源没有目录!");
						return;
					}
					dirDialog.show();
					dirDialog.showDir(popInfos);
					if (which == 0) {
						deal_key = RES_MOVE;
						dirDialog.setTitle("文件移动");
					}
					// 文件复制操作
					if (which == 1) {
						dirDialog.setTitle("文件复制");
						deal_key = RES_COPY;
					}
				}
			} else {
				// 目录权限
				if (which == 0) {
					setDirInfo();
				}
				// 目录操作
				if (which == 2) {
					deal_key = DIR_UPD;
					showDialog();
				}
				// 目录删除
				if (which == 1) {
					deal_key = DIR_DEL;
					showTipDialog();
				}
			}
			menuDialog.dismiss();
		}
	};

	//
	private void initDialog() {
		dirDialog = new ListDialog(this, R.style.MyDialog);
		dirDialog.setOnItemClickListener(dOnItemClickListener);

		menuDialog = new PopMenuDialog(this, R.style.MyDialog);
		menuDialog.setListener(menuClickListener);
		// 初始化ownDirs
		for (int i = 0; i < PangkerConstant.RES_SUM.length; i++) {
			List<DirInfo> ownTypeDirs = new ArrayList<DirInfo>();
			ownTypeDirs.add(application.getRootDir(PangkerConstant.RES_SUM[i]));
			ownDirs.put(PangkerConstant.RES_SUM[i], ownTypeDirs);
		}
	}

	EditTextDialog.DialogResultCallback resultCallback = new EditTextDialog.DialogResultCallback() {
		@Override
		public void buttonResult(String result, boolean isSubmit) {
			// TODO Auto-generated method stub
			if (isSubmit) {
				txtContent = result;
				if (deal_key == DIR_ADD) {
					initAddResInfo();
				}
				if (deal_key == DIR_UPD) {
					updDirInfo();
				}
			}
		}
	};

	private void showDialog() {
		if (updateDialog == null) {
			updateDialog = new EditTextDialog(this);
		}
		txtContent = null;
		String dirText = "";
		if (deal_key == DIR_ADD) {
			updateDialog.setTitle("创建资源目录");
		} else {
			updateDialog.setTitle("修改资源目录");
			dirText = nowDirInfo.getDirName();
		}
		updateDialog.showDialog(dirText, "", 20, resultCallback);
	}

	private ResShowEntity toResShowEntity(DirInfo resinfo) {
		// TODO Auto-generated method stub
		ResShowEntity entity = new ResShowEntity();
		entity.setCommentTimes(resinfo.getCommentTimes());
		entity.setResType(resinfo.getResType());
		entity.setDownloadTimes(resinfo.getDownloadTimes());
		entity.setFavorTimes(resinfo.getFavorTimes());
		entity.setResID(Long.valueOf(resinfo.getDirId()));
		entity.setDirid(resinfo.getParentDirId());
		entity.setShareUid(userId);
		entity.setScore(Integer.parseInt(String.valueOf(resinfo.getScore())));
		entity.setResName(resinfo.getDirName());
		entity.setSource(resinfo.getSource());
		entity.setKeep(resinfo.getSource() == 1);
		entity.setIfshare(resinfo.getDeal() == 1);
		return entity;
	}

	// 进入资源详细信息界面
	private void goinMovingRes(DirInfo dirInfo, int position, boolean ifEdit) {
		// TODO Auto-generated method stub
		List<ResShowEntity> showList = new ArrayList<ResShowEntity>();
		Intent intent = new Intent();

		if (ifEdit) {
			showList.add(toResShowEntity(dirInfo));
			intent.putExtra("SERACH_TYPE", 4);
		} else {
			intent.putExtra("SERACH_TYPE", 2);
			List<DirInfo> nowList = netResDatas.get(navDirInfo.getDirId());
			for (DirInfo resinfo : nowList) {
				if (resinfo.getIsfile() != 1) {
					position--;
					continue;
				}
				showList.add(toResShowEntity(resinfo));
			}
		}
		application.setResLists(showList);
		application.setViewIndex(position);
		if (dirInfo.getResType() == PangkerConstant.RES_PICTURE) {
			intent.setClass(this, PictureInfoActivity.class);
		} else {
			intent.setClass(this, ResInfoActivity.class);
		}
		startActivity(intent);
	}

	// 进入资源详细信息界面
	private void editRes(DirInfo dirInfo) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ResInfoEditActivity.class);
		intent.putExtra("resShowEntity", toResShowEntity(dirInfo));
		startActivity(intent);
	}

	private void showLoading() {
		if (index_type == PangkerConstant.RES_PICTURE) {
			pullToRefreshGridView.setRefreshing(true);
			lsitEmptyView.reset();
			footerView.hideFoot();
		} else {
			pullToRefreshListView.setRefreshing(true);
			gridEmptyView.reset();
		}
	}

	private DirInfo getNavParentDirinfo() {
		return application.getRootDir(index_type);
	}

	private void backDirinfo() {
		// TODO Auto-generated method stub
		navDirInfo = getNavParentDirinfo();
		String dirids = navDirInfo.getDirId();
		if (netResDatas.get(dirids) != null) {
			if (index_type == PangkerConstant.RES_PICTURE) {
				int sum = sumDatas.get(dirids);
				int count = netResDatas.get(dirids).size();
				picAdapter.setResinfos(netResDatas.get(dirids), navDirInfo, isLoadMore(count, sum));
				count = picAdapter.getSize();
			} else {
				resAdapter.setResInfos(netResDatas.get(dirids), navDirInfo);
				int count = resAdapter.getCount();
				if (sumDatas.containsKey(dirids)) {
					int sum = sumDatas.get(dirids);
					footerView.onLoadComplete(count, sum);
				}
			}
		}
		Log.d("remark", "Back===>" + dirids + "dirID===>" + dirID);
		refreshBtn();
	}

	/**
	 * 刷新当前的目录
	 */
	private void refreshDirinfo() {
		if (navDirInfo == null) {
			showToast("请检查网络!");
			return;
		}
		String dirid = navDirInfo.getDirId();
		netResDatas.put(dirid, null);
		sumDatas.remove(dirid);

		List<DirInfo> ownTypeDirs = new ArrayList<DirInfo>();
		ownTypeDirs.add(application.getRootDir(index_type));
		ownDirs.put(index_type, ownTypeDirs);
		String flag = "0"; // 0 search dir
		if ("0".equals(navDirInfo.getParentDirId())) {
			flag = "1";
		}
		getResInfo(dirid, true, flag);
	}

	/**
	 * 根据目录id加载资源
	 * 
	 * @param dirid
	 *            目录id
	 * @param flagMust
	 *            是否到后台加载数据，主要是为加载更多
	 * @param flag_dir
	 *            是否加载资源目录 1：是，0：不加载
	 */
	private void getResInfo(String dirid, boolean flagMust, String flag_dir) {
		this.dirID = dirid;
		if (!flagMust && netResDatas.get(dirID) != null) {
			if (index_type == PangkerConstant.RES_PICTURE) {
				int sum = sumDatas.get(dirID);
				int count = netResDatas.get(dirID).size();
				picAdapter.setResinfos(netResDatas.get(dirID), navDirInfo, isLoadMore(count, sum));
				count = picAdapter.getSize();
			} else {
				resAdapter.setResInfos(netResDatas.get(dirID), navDirInfo);
				int count = resAdapter.getCount();
				if (sumDatas.containsKey(dirID)) {
					footerView.onLoadComplete(count, sumDatas.get(dirID));
				} else {
					footerView.hideFoot();
				}
			}
			refreshBtn();
			return;
		}
		int startIndex = 0;
		if (netResDatas.get(dirID) == null) {
			showLoading();
		} else {
			startIndex = getStartIndex();
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appuid", userId));
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("flag", flag_dir));
		paras.add(new Parameter("dirid", dirID));
		paras.add(new Parameter("resType", String.valueOf(index_type)));
		Log.d("remark", "search====>" + dirID);
		ifsending = true;
		Log.d("remark", startIndex + "," + pageLength);
		paras.add(new Parameter("limit", startIndex + "," + pageLength));
		serverMana.supportRequest(Configuration.getResSiteQuery(), paras, RES_QUERY);
	}

	private int getStartIndex() {
		if (index_type == PangkerConstant.RES_PICTURE) {
			return picAdapter.getResCount();
		}
		return resAdapter.getResCount();
	}

	// 目录权限
	private void setDirInfo() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ResSetDirinfoActivity.class);
		intent.putExtra("DirInfo", nowDirInfo);
		startActivityForResult(intent, DIR_SET);
	}

	/**
	 * 点击事情
	 */
	View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				onBackPressed();
			}
			if (v == ivMore) {
				quickAction.show(ivMore);
			}
		}
	};

	protected void onDestroy() {
		PangkerManager.getTabBroadcastManager().removeUserListenter(ResNetManagerActivity.this);
		super.onDestroy();
	};

	// 到服务器加载资源信息
	private void initResInfo() {
		Intent intent = new Intent(this, PangkerService.class);
		intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_RES_INIT);
		startService(intent);
	}

	/**
	 * 列表项短按监听器
	 */
	AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (index_type != PangkerConstant.RES_PICTURE) {
				nowDirInfo = (DirInfo) parent.getItemAtPosition(position);
				if (nowDirInfo.getIsfile() == 0) {
					goinDirInfo(nowDirInfo, false, "0");
				} else if (nowDirInfo.getIsfile() == 1) {
					goinMovingRes(nowDirInfo, position - 1, false);
				} else {
					backDirinfo();
				}
			}
		}
	};

	private AdapterView.OnItemClickListener dOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			DirInfo resinfo = (DirInfo) parent.getItemAtPosition(position);
			moveInDir(resinfo, deal_key);
		}
	};

	private OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View arg1, int position, long arg3) {
			if (index_type != PangkerConstant.RES_PICTURE) {
				nowDirInfo = (DirInfo) parent.getItemAtPosition(position);
				showMenu();
			}
			return true;
		}
	};

	private FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			goinDirInfo(navDirInfo, true, "0");
		}
	};

	private View.OnClickListener picClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Integer position = (Integer) v.getTag(R.id.tag_index);
			nowDirInfo = (DirInfo) v.getTag(R.id.tag_shareinfo);
			if (nowDirInfo.getIsfile() == 0) {
				goinDirInfo(nowDirInfo, false, "0");
			} else if (nowDirInfo.getIsfile() == 1) {
				goinMovingRes(nowDirInfo, position, false);
			} else {
				backDirinfo();
			}
		}
	};

	private View.OnLongClickListener picLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			nowDirInfo = (DirInfo) v.getTag(R.id.tag_shareinfo);
			showMenu();
			return true;
		}
	};

	/*
	 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * +++++++++++++++++++++++++++++++
	 */
	// 分享设置
	private void ushareResInfo() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("optype", "1"));
		paras.add(new Parameter("resid", nowDirInfo.getDirId()));
		// 取消分享
		if (nowDirInfo.getDeal() == 1) {
			serverMana.supportRequest(Configuration.getCancelShare(), paras, true, mess, RES_USHARE);
		}
		// 分享
		if (nowDirInfo.getDeal() == 0) {
			paras.add(new Parameter("type", "1"));
			serverMana.supportRequest(Configuration.getResShare(), paras, true, mess, RES_SHARE);
		}
	}

	private boolean isNameExit(String string) {
		// TODO Auto-generated method stub
		for (DirInfo resInfo : resAdapter.getResInfos()) {
			if (resInfo.getIsfile() == DirInfo.ISFILE) {
				continue;
			}
			if (string.equals(resInfo.getDirName())) {
				return true;
			}
		}
		return false;
	}

	protected void moveInDir(DirInfo resinfo, int key) {
		// TODO Auto-generated method stub
		dirDialog.dismiss();
		newDirId = resinfo.getDirId();

		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", nowDirInfo.getDirId()));
		paras.add(new Parameter("sourcedirid", navDirInfo.getDirId()));
		paras.add(new Parameter("targetdirid", newDirId));
		paras.add(new Parameter("optype", key == RES_MOVE ? "1" : "2"));
		serverMana.supportRequest(Configuration.getMoveResToDir(), paras, true, mess, key);
	}

	// 初始化文件添加
	private void initAddResInfo() {
		// TODO Auto-generated method stub
		if (Util.isEmpty(txtContent)) {
			showToast("请输入文件名称!");
		} else if (isNameExit(txtContent)) {
			showToast("文件名称已经存在了!");
		} else {
			String pid = navDirInfo.getDirId();
			nowDirInfo = new DirInfo();
			nowDirInfo.setDirName(txtContent);
			nowDirInfo.setParentDirId(pid);
			nowDirInfo.setIsfile(0);
			nowDirInfo.setFileCount(0);
			nowDirInfo.setUid(userId);
			nowDirInfo.setResType(index_type);
			sendServer(DIR_ADD, nowDirInfo);
		}
	}

	// 修改资源目录
	private void updDirInfo() {
		// TODO Auto-generated method stub
		if (Util.isEmpty(txtContent)) {
			showToast("请输入文件名称!");
		} else if (isNameExit(txtContent)) {
			showToast("文件名称已经存在了!");
		} else {
			sendServer(DIR_UPD, nowDirInfo);
		}
	}

	// 资源删除
	private void removeRes(DirInfo nowResinfo) {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", nowResinfo.getDirId()));
		paras.add(new Parameter("dirid", navDirInfo.getDirId()));
		serverMana.supportRequest(Configuration.getDeleteRes(), paras, true, mess, RES_DEL);
	}

	private void sendServer(int type, DirInfo dirInfo) {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("optype", String.valueOf(type)));
		if (type == DIR_ADD) {
			paras.add(new Parameter("dirname", dirInfo.getDirName()));
		} else {
			paras.add(new Parameter("dirid", String.valueOf(dirInfo.getDirId())));
			paras.add(new Parameter("dirname", txtContent));
		}
		paras.add(new Parameter("restype", "" + dirInfo.getResType()));
		paras.add(new Parameter("dirparentid", dirInfo.getParentDirId()));
		serverMana.supportRequest(Configuration.getResDirManager(), paras, true, mess, type);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		ifsending = false;
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(response, caseKey != RES_QUERY)) {
			if (caseKey == RES_QUERY) {
				footerView.onLoadCompleteErr();
				lsitEmptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
				gridEmptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
			}
			return;
		}

		if (caseKey == RES_QUERY) {// 资源查询
			pullToRefreshListView.onRefreshComplete();
			pullToRefreshGridView.onRefreshComplete();
			lsitEmptyView.showView(R.drawable.emptylist_icon, R.string.no_data);
			gridEmptyView.showView(R.drawable.emptylist_icon, R.string.no_data);
			ResSiteQueryResult result = JSONUtil.fromJson(response.toString(), ResSiteQueryResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				handleQueryResult(result);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == DIR_ADD) {// 添加目录
			ResDirManagerResult result = JSONUtil.fromJson(response.toString(), ResDirManagerResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				addDirInfo(result.getDirId());
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == DIR_UPD) {// 修改目录
			ResDirManagerResult result = JSONUtil.fromJson(response.toString(), ResDirManagerResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				updResInfo();
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == RES_DEL || caseKey == DIR_DEL) {// 删除资源、目录
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				delResInfo(caseKey);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == RES_MOVE) {// 资源移动
			ResPropertySettingResult result = JSONUtil.fromJson(response.toString(), ResPropertySettingResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				moveResInfo();
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == RES_COPY) {// 资源复制
			ResPropertySettingResult result = JSONUtil.fromJson(response.toString(), ResPropertySettingResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				copyResInfo();
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == RES_SHARE) {// 分享
			ResShareResult result = JSONUtil.fromJson(response.toString(), ResShareResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				setShareRes(1);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == RES_USHARE) {// 取消分享
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				setShareRes(0);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
	}

	// 设置资源分享
	private void setShareRes(int ifahse) {
		// TODO Auto-generated method stub
		nowDirInfo.setDeal(ifahse);
		if (index_type == PangkerConstant.RES_PICTURE) {
			picAdapter.notifyDataSetChanged();
		} else {
			resAdapter.notifyDataSetChanged();
		}
		// 搜索所有复制的资源，并修改其状态,首先根据类型获取资源的顶层目录
		String typeRootDirId = application.getRootDir(nowDirInfo.getResType()).getDirId();
		updateShareResCopy(typeRootDirId, ifahse);
	}

	/**
	 * 设置所有复制资源的分享修改
	 */
	private void updateShareResCopy(String dirid, int ifahse) {
		List<DirInfo> listdDirInfos = netResDatas.get(dirid);
		if (listdDirInfos != null) {
			for (DirInfo dirInfo : listdDirInfos) {
				if (dirInfo.getIsfile() == 0) {
					updateShareResCopy(dirInfo.getDirId(), ifahse);
				} else {
					if (nowDirInfo.getDirId().equals(dirInfo.getDirId())) {
						dirInfo.setDeal(ifahse);
					}
				}
			}
		}
	}

	// 移动资源一个目录,目的目录就两个，一个是顶层目录，另外就是第二次目录
	private void moveResInfo() {
		// TODO Auto-generated method stub
		showToast("移动成功!");
		// 旧目录的文件数量-1，新文件的数量+1
		if (index_type == PangkerConstant.RES_PICTURE) {
			picAdapter.delDirInfo(nowDirInfo);
		} else {
			resAdapter.delDirInfo(nowDirInfo);
		}
		navDirInfo.setFileCount(navDirInfo.getFileCount() - 1);
		netResDatas.get(navDirInfo.getDirId()).remove(nowDirInfo);

		DirInfo rootDirInfo = application.getRootDir(index_type);
		if (rootDirInfo.getDirId().equals(newDirId)) {
			netResDatas.get(rootDirInfo.getDirId()).add(nowDirInfo);
		} else {
			List<DirInfo> dirss = netResDatas.get(rootDirInfo.getDirId());
			for (DirInfo dirs : dirss) {
				if (dirs.getDirId().equals(newDirId)) {
					dirs.setFileCount(dirs.getFileCount() + 1);
					if (netResDatas.get(dirs.getDirId()) != null) {
						netResDatas.get(dirs.getDirId()).add(nowDirInfo);
					}
				}
			}
		}
		// 同时要修改目录文件的长度,当前的目录文件长度减一，如果目标目录已经访问了，便加一
		sumDatas.put(navDirInfo.getDirId(), sumDatas.get(navDirInfo.getDirId()) - 1);
		if (sumDatas.get(newDirId) != null) {
			sumDatas.put(newDirId, sumDatas.get(newDirId) + 1);
		}
	}

	// 复制资源一个目录
	private void copyResInfo() {
		// TODO Auto-generated method stub
		showToast("复制成功!");
		DirInfo rootDirInfo = application.getRootDir(nowDirInfo.getResType());
		if (rootDirInfo.getDirId().equals(newDirId)) {
			netResDatas.get(rootDirInfo.getDirId()).add(nowDirInfo);
		} else {
			List<DirInfo> dirss = netResDatas.get(rootDirInfo.getDirId());
			for (DirInfo dirs : dirss) {
				if (dirs.getDirId().equals(newDirId)) {
					dirs.setFileCount(dirs.getFileCount() + 1);
					if (netResDatas.get(dirs.getDirId()) != null) {
						netResDatas.get(dirs.getDirId()).add(nowDirInfo);
					}
				}
			}
		}
		if (index_type == PangkerConstant.RES_PICTURE) {
			picAdapter.notifyDataSetChanged();
		} else {
			resAdapter.notifyDataSetChanged();
		}
		// 如果目标目录已经访问了，便加一
		if (sumDatas.get(newDirId) != null) {
			sumDatas.put(newDirId, sumDatas.get(newDirId) + 1);
		}
	}

	private void delResInfo(int caseKey) {
		// TODO Auto-generated method stub
		if (caseKey == RES_DEL) {
			showToast("资源删除成功!");
		}
		if (caseKey == DIR_DEL) {
			showToast("目录删除成功!");
		}
		if (index_type == PangkerConstant.RES_PICTURE) {
			picAdapter.delDirInfo(nowDirInfo);
		} else {
			resAdapter.delDirInfo(nowDirInfo);
		}
		// 同时要修改目录文件的长度
		sumDatas.put(nowDirInfo.getParentDirId(), sumDatas.get(nowDirInfo.getParentDirId()) - 1);
		ownDirs.get(index_type).remove(nowDirInfo);
		netResDatas.get(nowDirInfo.getParentDirId()).remove(nowDirInfo);
		navDirInfo.setFileCount(navDirInfo.getFileCount() - 1);
		refreshBtn();
	}

	private void updResInfo() {
		// TODO Auto-generated method stub
		nowDirInfo.setDirName(txtContent);
		showToast("目录修改成功!");
		if (index_type == PangkerConstant.RES_PICTURE) {
			picAdapter.notifyDataSetChanged();
		} else {
			resAdapter.notifyDataSetChanged();
		}

		// 如果是顶层目录还是要修改数据库
		if ("0".equals(nowDirInfo.getParentDirId())) {
			iResDao.updateResInfo(nowDirInfo);
		}
	}

	private void addDirInfo(String dirId) {
		// TODO Auto-generated method stub
		nowDirInfo.setDirId(dirId);
		netResDatas.get(nowDirInfo.getParentDirId()).add(0, nowDirInfo);
		if (index_type == PangkerConstant.RES_PICTURE) {
			picAdapter.setResinfos(netResDatas.get(nowDirInfo.getParentDirId()));
		} else {
			resAdapter.setResInfos(netResDatas.get(nowDirInfo.getParentDirId()));
		}
		// 同时要修改目录文件的长度
		sumDatas.put(nowDirInfo.getParentDirId(), sumDatas.get(nowDirInfo.getParentDirId()) + 1);
		ownDirs.get(nowDirInfo.getResType()).add(nowDirInfo);
		refreshBtn();
	}

	// 判断该数据是否是已经查过了的，避免重复
	private boolean isCallbacked(DirInfo result, String resule_dirId) {
		if (netResDatas.get(resule_dirId) == null) {
			return false;
		}
		Iterator<DirInfo> iterator = netResDatas.get(resule_dirId).iterator();
		while (iterator.hasNext()) {
			DirInfo item = iterator.next();
			if (result.getDirId().equals(item.getDirId())) {
				return true;
			}
		}
		return ifsending;
	}

	private void saveToCache(List<DirInfo> data, String id, int count) {
		// TODO Auto-generated method stub
		if (netResDatas.get(id) == null) {
			netResDatas.put(id, data);
		} else {
			netResDatas.get(id).addAll(data);
		}
		if (!sumDatas.containsKey(id)) {
			sumDatas.put(id, count);
		}
	}

	private void handleQueryResult(ResSiteQueryResult result) {
		// TODO Auto-generated method stub
		String resule_dirId = result.getDirid();
		List<DirInfo> allResultDatas = new ArrayList<DirInfo>();

		int dirCount = 0;
		if (result.getDirinfos() != null && result.getDirinfos().size() > 0) {
			allResultDatas.addAll(result.getDirinfos());
			Integer type = result.getDirinfos().get(0).getResType();
			ownDirs.get(type).addAll(result.getDirinfos());
			dirCount = result.getDirinfos().size();
		}
		List<DirInfo> resInfos = changRes(result.getResinfos());
		allResultDatas.addAll(resInfos);
		if (allResultDatas.size() > 0) {
			DirInfo first = allResultDatas.get(0);
			if (!isCallbacked(first, resule_dirId)) {
				saveToCache(allResultDatas, resule_dirId, result.getTotalcount() + dirCount);
			}
		} else {
			saveToCache(allResultDatas, resule_dirId, 0);
		}

		if (resule_dirId.equals(navDirInfo.getDirId())) {
			if (index_type == PangkerConstant.RES_PICTURE) {
				int sum = sumDatas.get(resule_dirId);
				int count = netResDatas.get(resule_dirId).size();
				picAdapter.setResinfos(netResDatas.get(resule_dirId), navDirInfo, isLoadMore(count, sum));
			} else {
				resAdapter.setResInfos(netResDatas.get(resule_dirId), navDirInfo);
				footerView.onLoadComplete(resAdapter.getSize(), sumDatas.get(resule_dirId));
			}
			refreshBtn();
		}
	}

	/**
	 * 正对图片的加载更多
	 * 
	 * @param count
	 * @param sum
	 * @return
	 */
	private boolean isLoadMore(int count, int sum) {
		if (count < pageLength) {
			return false;
		}
		if (count >= sum) {
			return false;
		}
		return true;
	}

	private DirInfo transMovingRes(ResInfo resInfo) {
		DirInfo dirinfo = new DirInfo();
		dirinfo.setDirName(resInfo.resName);
		dirinfo.setDirId(String.valueOf(resInfo.getSid()));
		dirinfo.setParentDirId(navDirInfo.getDirId());
		dirinfo.setDeal(resInfo.getIfShare());
		dirinfo.setResType(Integer.parseInt(resInfo.getType()));
		String path = Configuration.getCoverDownload() + resInfo.getSid();
		dirinfo.setPath(path);
		double szie = resInfo.getFileSize();
		dirinfo.setFileCount((int) szie);
		dirinfo.setIsfile(1);
		dirinfo.setAppraisement(Util.TimeIntervalBtwNow(resInfo.getIssuetime()));
		dirinfo.setShareTimes(resInfo.getShareTimes());
		dirinfo.setCommentTimes(resInfo.getCommentTimes());
		dirinfo.setDownloadTimes(resInfo.getDownloadTimes());
		dirinfo.setFavorTimes(resInfo.getFavorTimes());
		dirinfo.setScore(Integer.parseInt(String.valueOf(resInfo.getScore())));
		dirinfo.setSource(resInfo.getResSource());
		return dirinfo;
	}

	private List<DirInfo> changRes(List<ResInfo> resinfos) {
		List<DirInfo> resInfos = new ArrayList<DirInfo>();
		if (resinfos != null) {
			for (ResInfo resinfo : resinfos) {
				resInfos.add(transMovingRes(resinfo));
			}
		}
		return resInfos;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == DIR_SET && resultCode == DIR_SET) {
			int vtype = data.getIntExtra("DirInfo", 0);
			nowDirInfo.setVisitType(vtype);
		}
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			if (msg.what == 9) {
				initFristTag();
			}
			if (msg.what == 1) {
				refreshDirinfo();
			}
		}
	};

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		handler.sendMessage(msg);
	};

}
