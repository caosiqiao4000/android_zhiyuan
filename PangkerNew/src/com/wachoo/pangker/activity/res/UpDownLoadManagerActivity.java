package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.DocReaderActivity;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.PictureBrowseActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.root.DownloadSetActivity;
import com.wachoo.pangker.adapter.DownloadAdapter;
import com.wachoo.pangker.adapter.UploadJobAdapter;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.downupload.DownLoadManager;
import com.wachoo.pangker.downupload.DownloadAsyncTask;
import com.wachoo.pangker.downupload.DownloadListenter;
import com.wachoo.pangker.downupload.UpLoadManager;
import com.wachoo.pangker.downupload.UpLoadlistenter;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.PictureViewInfo;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.image.ImageApkIconFetcher;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.HorizontalPager;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

public class UpDownLoadManagerActivity extends CommonPopActivity implements UpLoadlistenter, DownloadListenter {
	// >>>>>>>>>>>title工具栏
	private Button mBtnBack;
	private TextView mTitle;
	private Button mBtnMore; // 菜单键

	// >>>>>>>>>>滑动界面
	private RadioGroup mRadioGroup;
	private HorizontalPager mHorizontalPager;

	// >>>>>>>>>>>>>上传列表
	private ListView mUploadListView;
	private UploadJobAdapter mUploadJobAdapter;
	private List<UploadJob> mUploadJobs;

	// >>>>>>>>>>>>下载列表
	private ListView mDownloadListView;
	private DownloadAdapter mDownloadAdapter;
	private List<DownloadJob> mDownloadJobs;

	// >>>>>>>>>>上传管理类
	private UpLoadManager mUpLoadManager;

	// >>>>>>>>>>下载管理类
	private DownLoadManager mDownLoadManager;

	// >>>>>>>当前用户用户UID
	private PangkerApplication mApplication;

	// >>>>>>>>>刷新UI
	private Handler mHandler;

	// >>>>>>>功能按钮
	private QuickAction mQuickAction;

	private static final int ID_CLEAR_ALL_COMPLETE_DOWNLOAD_JOBS = 1; // 清空已经完成的下载任务
	private static final int ID_CLEAR_ALL_COMPLETE_UPLOAD_JOBS = 2; // 清空已经完成的上传任务
	private static final int ID_DOWN_SETTING = 3; // 下载设置

	private DownloadJob downloadJob;
	private UploadJob uploadJob;// 当前操作的UploadJob
	private PopMenuDialog menuDialog;
	private ImageApkIconFetcher imageApkIconFetcher;
	private ImageLocalFetcher localFetcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_download);
		init(); // >>>>>初始化工具类
		initView(); // >>>>>>初始化界面
	}

	private void init() {
		// TODO Auto-generated method stub
		// >>>>>>>获取application
		mApplication = (PangkerApplication) getApplication();

		// >>>>>>>获取上传管理类
		mUpLoadManager = mApplication.getUpLoadManager();
		mDownLoadManager = mApplication.getDownLoadManager();

		// >>>>>>>获取所有的上传任务
		mUploadJobs = mUpLoadManager.getmUploadJobs();
		mDownloadJobs = mDownLoadManager.getmDownloadJobs();
		// >>>>>>>>刷新界面UI
		mHandler = new Handler();

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);

		imageApkIconFetcher = new ImageApkIconFetcher(this);
		imageApkIconFetcher.setmSaveDiskCache(false);
		imageApkIconFetcher.setImageCache(mImageCache);
		imageApkIconFetcher.setLoadingImage(R.drawable.icon46_apk);

		// >>>>>>>>>加载
		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		mImageCache = new ImageCache(this, ImageFetcher.HTTP_CACHE_DIR);
		localFetcher.setmSaveDiskCache(false);
		localFetcher.setImageCache(mImageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);

	}

	// >>>>>>>>>加载界面
	private void initView() {
		// TODO Auto-generated method stub
		// >>>titlebar
		initTitleMenuBarView();

		// >>>>>>>>主题界面切换
		mRadioGroup = (RadioGroup) findViewById(R.id.searchRadioGroup);
		mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
				case R.id.downloadsys:
					mHorizontalPager.setCurrentScreen(0, true);
					break;
				case R.id.uploadsys:
					mHorizontalPager.setCurrentScreen(1, true);
					break;
				}
			}
		});
		mHorizontalPager = (HorizontalPager) findViewById(R.id.horizontal_pager);
		mHorizontalPager.setOnScreenSwitchListener(new HorizontalPager.OnScreenSwitchListener() {
			@Override
			public void onScreenSwitched(int screen) {
				// TODO Auto-generated method stub
				switch (screen) {
				case 0:
					mRadioGroup.check(R.id.downloadsys);
					break;
				case 1:
					mRadioGroup.check(R.id.uploadsys);
					break;
				}
			}
		});

		// >>>>>>>下载管理界面
		initDownloadManagerView();
		// >>>>>>>>上传管理界面
		initUploadManagerView();

	}

	// >>>>>>>>>>>>>>title 菜单键功能
	private void initTitleMenuBarView() {
		// >>>>>>>>>title 工具栏 start
		mTitle = (TextView) findViewById(R.id.mmtitle);
		mTitle.setText(R.string.updownload_title);

		mBtnBack = (Button) findViewById(R.id.btn_back);
		mBtnBack.setOnClickListener(clickListener);

		mBtnMore = (Button) findViewById(R.id.iv_more);
		mBtnMore.setBackgroundResource(R.drawable.btn_menu_bg);
		mBtnMore.setOnClickListener(clickListener);
		// >>>>>>>>>title 工具栏 end
		// >>>>>>>菜单功能按钮
		mQuickAction = new QuickAction(this, QuickAction.VERTICAL);
		// 清空已经完成下载任务按钮
		mQuickAction.addActionItem(new ActionItem(ID_CLEAR_ALL_COMPLETE_DOWNLOAD_JOBS, getResources().getString(
				R.string.updownload_clear_all_complete_download_jobs)));
		// 清空已经完成上传任务按钮
		mQuickAction.addActionItem(new ActionItem(ID_CLEAR_ALL_COMPLETE_UPLOAD_JOBS, getResources().getString(
				R.string.updownload_clear_all_complete_upload_jobs)));
		// 下载设置按钮
		mQuickAction.addActionItem(
				new ActionItem(ID_DOWN_SETTING, getResources().getString(R.string.download_setting)), true);
		mQuickAction.setOnActionItemClickListener(onActionItemClickListener);
	}

	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			// TODO Auto-generated method stub
			// 清空已经完成down任务
			if (actionId == ID_CLEAR_ALL_COMPLETE_DOWNLOAD_JOBS) {
				int count = mDownLoadManager.getDownloadOverCount();
				mDownLoadManager.removeAllCompleteJobs();
				mHandler.post(mUpdateDownloadTimeTask);
				if (count > 0) {
					showToast("清理完成!");
				}
			}
			// 清空已经完成up任务
			else if (actionId == ID_CLEAR_ALL_COMPLETE_UPLOAD_JOBS) {
				// >>>删除数据
				int count = mUpLoadManager.getUploadOverCount();
				mUpLoadManager.removeAllCompleteJobs();
				// >>>>>刷新界面
				mHandler.post(mUpdateUploadTimeTask);
				if (count > 0) {
					showToast("清理完成!");
				}
			}
			// 下载设置
			else if (actionId == ID_DOWN_SETTING) {
				// >>>>>>>界面跳转
				launch(DownloadSetActivity.class);
			}
		}
	};
	// >>>>>长按菜单，下载1——9；上传10——90；
	ActionItem[] msgMenu10 = new ActionItem[] { new ActionItem(10, "从列表中移除") };// >>上传成功
	ActionItem[] msgMenu20 = new ActionItem[] { new ActionItem(10, "从列表中移除"), new ActionItem(20, "重新上传") };// >>上传失败
	ActionItem[] msgMenu30 = new ActionItem[] { new ActionItem(30, "取消上传") };// >>正在上传
	ActionItem[] msgMenu40 = new ActionItem[] { new ActionItem(30, "取消上传"), new ActionItem(40, "开始上传") };// >>上传失败

	ActionItem[] msgMenu1 = new ActionItem[] { new ActionItem(1, "从列表中移除") };// >>下载成功
	ActionItem[] msgMenu2 = new ActionItem[] { new ActionItem(1, "从列表中移除"), new ActionItem(2, "重新下载") };// >>下载失败

	/**
	 * @param menu_case
	 *            以菜单的最大Id为准
	 */
	private void showMenu(ActionItem[] msgMenu, int menu_case) {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
		}
		if (menu_case == 1) {
			menuDialog.setTitle(downloadJob.getName());
		} else if (menu_case == 2) {
			menuDialog.setTitle(uploadJob.getResName());
		}
		menuDialog.setMenus(msgMenu);
		menuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			if (actionId == 1) {// 从下载列表中移除
				mDownLoadManager.removeCompleteJob(downloadJob);
			} else if (actionId == 10) {// 从上传列表中移除
				mUpLoadManager.removeCompleteJob(uploadJob);
			} else if (actionId == 20) {// 重新上传
				Intent intent = new Intent(UpDownLoadManagerActivity.this, PangkerService.class);
				intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_UPLOAD);
				// >>>>>>>>>>传递上传jobID到service
				intent.putExtra(UploadJob.UPLOAD_JOB_ID, uploadJob.getId());
				startService(intent);
			} else if (actionId == 40) {// 开始上传
				Intent intent = new Intent(UpDownLoadManagerActivity.this, PangkerService.class);
				intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_UPLOAD);
				// >>>>>>>>>>传递上传jobID到service
				intent.putExtra(UploadJob.UPLOAD_JOB_ID, uploadJob.getId());
				intent.putExtra(UploadJob.UPLOADJOB_TYPE, UploadJob.UPLOAD_WAIT);
				startService(intent);
			} else if (actionId == 30) {// 取消上传
				mUpLoadManager.cancelUploadingJob(uploadJob);
			}
			menuDialog.dismiss();
			// >>>>>刷新界面
			mHandler.post(mUpdateUploadTimeTask);
		}
	};

	private void initDownloadManagerView() {
		// TODO Auto-generated method stub
		// >>>>>>列表显示
		mDownloadListView = (ListView) findViewById(R.id.listview_download);
		mDownloadAdapter = new DownloadAdapter(this, mImageWorker, imageApkIconFetcher);
		mDownloadAdapter.setmDownloadJobs(mDownloadJobs);
		mDownloadListView.setAdapter(mDownloadAdapter);
		// >>>>>>>>设置空view
		EmptyView emptyView = new EmptyView(this);
		emptyView.addToListView(mDownloadListView);
		emptyView.showView(R.drawable.emptylist_icon, "您还没有下载过任何资源");

		// >>>>>>>点击监听
		mDownloadListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> patent, View view, int i, long l) {
				// TODO Auto-generated method stub
				// >>>>>>>打开文件
				DownloadJob job = (DownloadJob) patent.getItemAtPosition(i);
				if (job.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
					openDownloadJob(job);
				}
			}
		});
		// >>>>>>长按监听
		mDownloadListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
				// TODO Auto-generated method stub
				// >>>>>>>>>按钮长按处理
				downloadJob = (DownloadJob) adapterView.getItemAtPosition(i);
				// 下载异常
				if (downloadJob.getStatus() == DownloadJob.DOWNLOADING && downloadJob.getmDownloadAsyncTask() == null) {
					showMenu(msgMenu2, 1);
				} else if (downloadJob.getStatus() == DownloadJob.DOWNLOAD_FAILE) {
					showMenu(msgMenu2, 1);
				} else if (downloadJob.getStatus() == DownloadJob.DOWNLOAD_SUCCESS) {
					showMenu(msgMenu1, 1);
				}
				return true;
			}
		});
	}

	// >>>>上传资源管理列表初始化
	private void initUploadManagerView() {
		// TODO Auto-generated method stub
		// >>>>上传资源管理列表
		mUploadListView = (ListView) findViewById(R.id.listview_upload);
		mUploadJobAdapter = new UploadJobAdapter(this, localFetcher, imageApkIconFetcher);
		mUploadJobAdapter.setUploadJobs(mUploadJobs);
		mUploadListView.setAdapter(mUploadJobAdapter);
		// >>>>>>>>设置空view
		// >>>>>>>>>>>>无数据显示效果
		// >>>>>>>>设置空view
		EmptyView emptyView = new EmptyView(this);
		emptyView.addToListView(mUploadListView);
		emptyView.showView(R.drawable.emptylist_icon, "您还没有上传过任何资源");

		// >>>>>>>点击监听
		mUploadListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> patent, View view, int i, long l) {
				// TODO Auto-generated method stub
				// >>>>>>>打开文件
				UploadJob job = (UploadJob) patent.getItemAtPosition(i);
				openUploadJob(job);
			}
		});

		// >>>>>>长按监听
		mUploadListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
				// >>>>>>>>>按钮长按处理
				uploadJob = (UploadJob) adapterView.getItemAtPosition(i);
				// 正在上传的时候，可能出现异常 先处理等待上传的
				if (uploadJob.getUploadType() == UploadJob.UPLOAD_WAIT) {
					showMenu(msgMenu40, 2);
				} else {
					if (uploadJob.getStatus() == UploadJob.UPLOAD_UPLOAGINDG) {
						if (uploadJob.getFilePart() != null) {
							showMenu(msgMenu30, 2);
						} else {
							showMenu(msgMenu20, 2);
						}
					} else {
						if (uploadJob.getStatus() == UploadJob.UPLOAD_FAILED
								|| uploadJob.getStatus() == UploadJob.UPLOAD_CANCEL) {
							showMenu(msgMenu20, 2);
						} else if (uploadJob.getStatus() == UploadJob.UPLOAD_SUCCESS) {
							showMenu(msgMenu10, 2);
						}
					}
				}
				return true;
			}
		});
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// >>>>>>>返回按钮
			if (v == mBtnBack) {
				UpDownLoadManagerActivity.this.finish();
			}
			// >>>>菜单键
			else if (v == mBtnMore) {
				mQuickAction.show(mBtnMore);
			}
		}
	};

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>打开阅读>>>>>>>>>>>>>>>>>>>>>>
	private DirInfo getDirInfoByTypeAndPath(int type, String path) {
		// TODO Auto-generated method stub
		List<DirInfo> docLists = mApplication.getResDirInfos(type);
		for (DirInfo dir : docLists) {
			if (path.equals(dir.getPath())) {
				return dir;
			}
		}
		return null;
	}

	private void openReader(String path) {
		if (!Util.isExitFileSD(path)) {
			showToast("抱歉，文件不存在!");
			return;
		}
		IResDao resDao = new ResDaoImpl(this);
		DirInfo dirInfo = resDao.getResByPath(path, mApplication.getMyUserID());
		Intent intent = new Intent(this, DocReaderActivity.class);
		if (dirInfo != null) {
			intent.putExtra("DirInfo", dirInfo);
		} else {
			intent.putExtra("sdPath", path);
		}
		startActivity(intent);
	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>打开图片>>>>>>>>>>>>>>>>>>>>>>
	private void openPhoto(String path) {
		ArrayList<PictureViewInfo> imagePathes = new ArrayList<PictureViewInfo>();
		PictureViewInfo pictureViewInfo = new PictureViewInfo();
		pictureViewInfo.setPicPath(path);
		imagePathes.add(pictureViewInfo);
		Intent it = new Intent(this, PictureBrowseActivity.class);
		it.putExtra(PictureViewInfo.PICTURE_VIEW_INTENT_KEY, imagePathes);
		it.putExtra("index", 0);
		startActivity(it);
	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>打开音乐>>>>>>>>>>>>>>>>>>>>>>

	private void openMusic(String name, String path) {
		// TODO Auto-generated method stub
		List<MusicInfo> musicInfos = new ArrayList<MusicInfo>();
		MusicInfo music = new MusicInfo();
		music.setMusicName(name);
		music.setMusicPath(path);
		musicInfos.add(music);
		mApplication.setMusiList(musicInfos);
		mApplication.setPlayMusicIndex(0);
		Intent intentSerivce = new Intent(this, MusicService.class);
		intentSerivce.putExtra("control", PlayControl.MUSIC_START_PLAY);
		startService(intentSerivce);
		Intent intent = new Intent(this, MusicPlayActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void openDownloadJob(DownloadJob job) {
		if (job.getType() == PangkerConstant.RES_APPLICATION) {
			Util.instalApplication(this, job.getAbsolutePath());
		} else if (job.getType() == PangkerConstant.RES_DOCUMENT) {
			openReader(job.getAbsolutePath());
		} else if (job.getType() == PangkerConstant.RES_PICTURE) {
			openPhoto(job.getAbsolutePath());
		} else if (job.getType() == PangkerConstant.RES_MUSIC) {
			openMusic(job.getName(), job.getAbsolutePath());
		}
	}

	// modify by lb
	private void openUploadJob(UploadJob job) {
		// TODO Auto-generated method stub
		if (job.getType() == PangkerConstant.RES_APPLICATION) {
			Util.instalApplication(this, job.getFilePath());
		} else if (job.getType() == PangkerConstant.RES_DOCUMENT) {
			openReader(job.getFilePath());
		} else if (job.getType() == PangkerConstant.RES_PICTURE) {
			openPhoto(job.getFilePath());
		} else if (job.getType() == PangkerConstant.RES_MUSIC) {
			openMusic(job.getResName(), job.getFilePath());
		}
	}

	@Override
	public long getUploadID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mHandler.removeCallbacks(mUpdateUploadTimeTask);
		mHandler.removeCallbacks(mUpdateDownloadTimeTask);
		// >>>>>>>>>添加监听，待更新使用
		mUpLoadManager.removeUpLoadListenter(this);
		mDownLoadManager.removeDownloadListenter(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// >>>>>>>>>添加监听，待更新使用
		mUpLoadManager.addUpLoadListenter(this);
		mDownLoadManager.addDownloadListenter(this);
	}

	@Override
	public void refreshProgressUI(Message message) {
		// TODO Auto-generated method stub
		// >>>>>>>>>>刷新listview来更新下载进度
		Log.i("uploadfile", "UpDownLoadManagerActivity--refreshProgressUI");
		mHandler.post(mUpdateUploadTimeTask);
	}

	private Runnable mUpdateUploadTimeTask = new Runnable() {
		public void run() {
			mUploadJobs = mUpLoadManager.getmUploadJobs();
			mUploadJobAdapter.setUploadJobs(mUploadJobs);
		}
	};

	private Runnable mUpdateDownloadTimeTask = new Runnable() {
		public void run() {
			mDownloadJobs = mDownLoadManager.getmDownloadJobs();
			mDownloadAdapter.setmDownloadJobs(mDownloadJobs);
		}
	};

	@Override
	public long getDownloaderID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void refreshProgressUI(DownloadAsyncTask mDownloadAsyncTask) {
		// TODO Auto-generated method stub
		mHandler.post(mUpdateDownloadTimeTask);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}
}
