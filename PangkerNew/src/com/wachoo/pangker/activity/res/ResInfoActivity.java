package com.wachoo.pangker.activity.res;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mobile.json.JSONUtil;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.DocReaderActivity;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.DownloadJob;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.gestures.GestureCommandRegister;
import com.wachoo.pangker.gestures.GestureListener;
import com.wachoo.pangker.gestures.NextCommend;
import com.wachoo.pangker.gestures.PrevCommend;
import com.wachoo.pangker.listener.ShowhideOnClickListener;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CommentResult;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.ResInfo;
import com.wachoo.pangker.server.response.ResShareResult;
import com.wachoo.pangker.server.response.SearchCommentsResult;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.KeyboardLayout;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.util.ConnectivityHelper;
import com.wachoo.pangker.util.Util;

public class ResInfoActivity extends ResShowActivity implements GestureListener, ITabSendMsgListener {

	private final int CODE_PLAY = 0x23;
	// 手势滑动的监听
	private LinearLayout mainLayout;
	private Button btn_back;// 返回
	private TextView txt_title;

	private ImageView iv_cover;// res iocn
	private EditText tv_resname;// res name
	private TextView tv_ressize;// res file size
	private RatingBar rb_res_score; // res score
	private TextView tv_forwardtimes;// 资源分享次数
	private TextView tv_favoritetimes;// favorite times
	private TextView tv_downloadtimes;// download times
	private TextView tv_description;// Detail comments
	private TextView tvResonShow;
	private TextView tv_lable;// 资源分享次数

	private UIUserInfo uiUserInfo;
	private UIOperate uiOperate;

	private String localPath = null;

	private TextView tv_res_author_title;// 文档：作者 ；音乐:歌手
	private TextView tv_res_author;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// 防止输入法在启动Activity时自动弹出
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.net_res_info);
		PangkerManager.getTabBroadcastManager().addMsgListener(this);

		// TODO Auto-generated method stub
		if (resShowEntity.getResType() == PangkerConstant.RES_MUSIC) {
			initMusicApplication();
		}
		initView();
		if (application.getResLists() == null || application.getResLists().size() <= 0) {
			showToast("Err!");
			// finish();
			return;
		}

		resShowEntity = application.getResLists().get(application.getViewIndex());
		if (resShowEntity == null) {
			showToast("Err!");
			// finish();
			return;
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(onClickListener);
		txt_title = (TextView) findViewById(R.id.mmtitle);
		txt_title.setText(R.string.net_resinfomation);
		mainLayout = (LinearLayout) findViewById(R.id.main_layout);
		mainLayout.setLongClickable(true);

		// 资源拥有用户头像
		uiUserInfo = new UIUserInfo();

		iv_cover = (ImageView) findViewById(R.id.iv_resicon);
		tv_resname = (EditText) findViewById(R.id.tv_resname);
		tv_resname.setEnabled(false);
		tv_ressize = (TextView) findViewById(R.id.tv_ressize);
		rb_res_score = (RatingBar) findViewById(R.id.rb_res_score);
		tv_description = (TextView) findViewById(R.id.tv_description);
		tvResonShow = (TextView) findViewById(R.id.tv_reson_show);
		tv_description.setOnLongClickListener(descLongClickListener);
		tv_favoritetimes = (TextView) findViewById(R.id.tv_favoritetimes);
		tv_downloadtimes = (TextView) findViewById(R.id.tv_downloadtimes);
		tv_forwardtimes = (TextView) findViewById(R.id.tv_forwardtimes);
		tv_lable = (TextView) findViewById(R.id.tv_title_lable);

		// >>>>>>作者
		tv_res_author_title = (TextView) findViewById(R.id.tv_res_author_title);
		tv_res_author = (TextView) findViewById(R.id.tv_res_author);

		uiOperate = new UIOperate();
		uiComment = new UIComment();
		uiSubmit = new UICommentSubmit();

		// >>>>>>>>>阅读文档
		if (resShowEntity.getResType() == PangkerConstant.RES_DOCUMENT) {
			uiOperate.btn_open.setBackgroundResource(R.drawable.btn_default_selector);
			uiOperate.btn_open.setText(R.string.net_resinfo_doc_read);
			// >>>>>>>>>作者
			tv_res_author_title.setVisibility(View.VISIBLE);
			tv_res_author_title.setText(R.string.net_resinfo_doc_author);
			tv_res_author.setVisibility(View.VISIBLE);
			tv_lable.setText("文档说明");
			// tv_res_author.setText(text)
		}
		// >>>>>>>>>>安装应用
		else if (resShowEntity.getResType() == PangkerConstant.RES_APPLICATION) {
			tv_res_author_title.setVisibility(View.GONE);
			tv_res_author.setVisibility(View.GONE);
			mImageWorker.setLoadingImage(R.drawable.icon46_apk);
			uiOperate.btn_open.setBackgroundResource(R.drawable.btn_default_selector);
			uiOperate.btn_open.setText(R.string.net_resinfo_app_install);
			tv_lable.setText("应用介绍");
		}
		// >>>>>>>>>>收听歌曲
		else {
			mImageWorker.setLoadingImage(R.drawable.icon46_music);
			uiOperate.btn_open.setBackgroundResource(R.drawable.btn_default_selector);
			uiOperate.btn_open.setText(R.string.net_resinfo_music_listen);
			tv_res_author_title.setVisibility(View.VISIBLE);
			tv_res_author_title.setText(R.string.net_resinfo_music_singer);
			tv_res_author.setVisibility(View.VISIBLE);
			tv_lable.setText("音乐介绍");
		}

		mViewFlipper = (ViewFlipper) findViewById(R.id.DownloadViewFlipper);
		mGestureOverlayView = (GestureOverlayView) findViewById(R.id.gestures);

		mKeyboardLayout = (KeyboardLayout) findViewById(R.id.kl_res_info);
		initData(true);
	}

	private void initMusicApplication() {
		// TODO Auto-generated method stub
		List<MusicInfo> musiList = new ArrayList<MusicInfo>();
		for (ResShowEntity entity : application.getResLists()) {
			MusicInfo musicInfo = new MusicInfo();
			musicInfo.setMusicName(entity.getResName());
			musicInfo.setSid(entity.getResID());
			musicInfo.setDuration(entity.getDuration());
			musicInfo.setFileSize(entity.getFileSize());
			musicInfo.setFileFormat(entity.getFromat());
			musicInfo.setSinger(entity.getSinger());
			musicInfo.setShareUid(entity.getShareUid());// >>>>>>分享资源用户的UID
			musiList.add(musicInfo);

		}
		application.setMusiList(musiList);
	}
	
	private void checkNet() {
		if (ConnectivityHelper.getAPNType(this) != 1) {
			MessageTipDialog mTipDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-generated method stub
					if(isSubmit){
						playMusic();
					}
				}}, this);
			mTipDialog.showDialog(0, R.string.net_prompt_music);
		} else {
			playMusic();
		}
	}

	private void downAndOpen() {
		// TODO Auto-generated method stub
		int apnType = ConnectivityHelper.getAPNType(ResInfoActivity.this);
		if (apnType == -1) {
			showToast("无法打开，请检查您的网络！");
			return;
		}
		MessageTipDialog mTipDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
			@Override
			public void buttonResult(boolean isSubmit) {
				// TODO Auto-generated method stub
				if(isSubmit){
					download(false, true);
				}
			}}, this);
		String mssg = "确认下载之后打开?";
		if (apnType != 1) {
			mssg = "温馨提示,您现在是运营商网络接入， 确认下载之后打开?";
			if (resInfo.isMyres == 1) {// 本人资源
				mssg = "温馨提示,这个资源是有您本人上传的，可以通过手机本地资源找到这个文件，当然，您也可以进行下载，您现在是运营商网络接入，确认下载之后打开?";
			}
		} else {
			if (resInfo.isMyres == 1) {// 本人资源
				mssg = "温馨提示,这个资源是有您本人上传的，可以通过手机本地资源找到这个文件，当然，您也可以进行下载，确认下载之后打开?";
			}
		}
		mTipDialog.showDialog("资源下载", mssg);
	}

	/**
	 * 打开网络资源的说明，三种资源要分开，如果是下载或者上传的资源，当做本地资源打开。如果是本人的，做一定的提示
	 * 1：音乐，如果找不到Sd地址，不需要下载，如果在群组内，还有确认是否屏蔽群组的语音功能(这个判断的优先级最高)。
	 * 2：阅读和应用，如果找不到Sd地址，需要下载之后打开，
	 */
	private void playResinfo() {
		if (!application.ismExternalStorageAvailable()) {
			showToast("SD卡不存在,无法进行该操作!");
			return;
		}
		// 如果是应用并且本地已安装则直接运行
		if (resInfo.getResType() == PangkerConstant.RES_APPLICATION && !Util.isEmpty(resInfo.getPackageinfo())
				&& Util.checkAppIsExist(this, resInfo.getPackageinfo())) {
			PackageManager packageManager = this.getPackageManager();
			Intent intent = new Intent();
			intent = packageManager.getLaunchIntentForPackage(resInfo.getPackageinfo());
			if (intent != null) {
				startActivity(intent);
			} else
				showToast("该应用已在系统中禁止启动!");
		}
		// 本地文件不存在，音乐不需要下载 ,播放网络流，需要检测网络
		else if (!Util.isExitFileSD(localPath)) {
			if (resShowEntity.getResType() == PangkerConstant.RES_MUSIC) {
				checkNet();
			}
			// 如果是应用和文档便进行下载打开
			else {
				downAndOpen();
			}
		}
		// 本地文件存在， 当做本地文件处理
		else {
			if (resShowEntity.getResType() == PangkerConstant.RES_APPLICATION) {
				Util.instalApplication(this, localPath);
			} else if (resShowEntity.getResType() == PangkerConstant.RES_DOCUMENT) {
				// open document
				openReader(localPath);
			} else if (resShowEntity.getResType() == PangkerConstant.RES_MUSIC) {

				MusicInfo musicInfo = application.getMusiList().get(application.getViewIndex());
				musicInfo.setMusicPath(localPath);
				playMusic();
			}
		}
	}

	// 下载完成之后的判断
	public void downloadOver(String path, String fileurl) {
		if (resInfo != null && fileurl.equals(Configuration.getResDownload() + resInfo.getSid())) {
			isDownloaded = true;
			localPath = path;
			uiOperate.btn_download.setBackgroundResource(R.drawable.res_btn_downed);
		}
	}

	private void initGesture() {
		// TODO Auto-generated method stub
		if (application.getResLists().size() > 1) {
			register.registerCommand("next", new NextCommend(this));
			register.registerCommand("prev", new PrevCommend(this));
			application.getGestureHandler().setRegister(register);
			mGestureOverlayView.addOnGesturePerformedListener(application.getGestureHandler());
		}
	}

	private void playMusic() {
		// TODO Auto-generated method stub
		// >>>>>>>>判断是否播放当前音乐，同时判断音乐播放器的状态
		// if (!application.isPlayingMusic(application.getViewIndex())
		// || !PangkerManager.getMediaPlayer().isPlaying()) {
		application.setPlayMusicIndex(application.getViewIndex());
		Intent intentSerivce = new Intent(this, MusicService.class);
		intentSerivce.putExtra("control", PlayControl.MUSIC_START_PLAY);
		startService(intentSerivce);
		// }
		Intent intent = new Intent(this, MusicPlayActivity.class);
		intent.putExtra(MusicPlayActivity.START_FLAG, MusicPlayActivity.NET_RESINFO);
		startActivityForResult(intent, 1);
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.btn_back) {
				PangkerManager.getTabBroadcastManager().removeUserListenter(ResInfoActivity.this);
				finish();
			}
		}
	};

	private void initData(boolean ifLoad) {
		uiUserInfo.hideBroader();
		tv_resname.setText(resShowEntity.getResName());
		tv_forwardtimes.setText(timesText(String.valueOf(resShowEntity.getShareTimes())));
		tv_favoritetimes.setText(timesText(String.valueOf(resShowEntity.getFavorTimes())));
		tv_downloadtimes.setText(timesText(String.valueOf(resShowEntity.getDownloadTimes())));
		uiUserInfo.iv_share.setVisibility(View.INVISIBLE);
		isDownloaded = false;
		uiOperate.btn_download.setBackgroundResource(R.drawable.res_btn_down);

		initCover();
		showComment();
		if (ifLoad) {
			getResInfo();
			uiOperate.enableOperateAll(false);
			uiComment.showSerach();
			searchComment();
		}
	}

	private void initCover() {
		// 如果是应用、音乐
		if (resShowEntity.getResType() == PangkerConstant.RES_APPLICATION) {
			mImageWorker.setLoadingImage(R.drawable.icon46_apk);
		}
		if (resShowEntity.getResType() == PangkerConstant.RES_MUSIC) {
			mImageWorker.setLoadingImage(R.drawable.icon46_music);
		}
		if (resShowEntity.getResType() == PangkerConstant.RES_APPLICATION
				|| resShowEntity.getResType() == PangkerConstant.RES_MUSIC) {
			String iconUrl = Configuration.getCoverDownload() + resShowEntity.getResID();
			mImageWorker.loadImage(iconUrl, iv_cover, String.valueOf(resShowEntity.getResID()));
		} else {
			iv_cover.setImageResource(R.drawable.attachment_doc);
		}
	}

	@Override
	public void callbackErr(int caseKey) {
		if (caseKey == SEARCH_COMMENT) {
			uiComment.serachCommentFail();
		}
		if (caseKey == KEY_EDITRES) {
			checkEdited();
		}
	}

	public void dealBaseResult(int key) {
		// TODO Auto-generated method stub
		if (key == KEY_COLLECT) {
			showToast("收藏成功!");
			tv_favoritetimes.setText(timesText(String.valueOf(resInfo.getFavorTimes())));
			uiOperate.btn_favorite.setEnabled(false);
			uiOperate.btn_favorite.setBackgroundResource(R.drawable.form_vipinf_but_keep_c);
		}
		if (key == KEY_RATINGS) {
			showScore();
			showToast("评分成功!");
			uiOperate.enableSocre(false);
		}
	}

	@Override
	public void download(boolean blag, boolean isopen) {
		// TODO Auto-generated method stub
		if (!Util.isExistSdkard()) {
			showToast("SD卡找不到，不能下载!");
			return;
		}

		// >>>>>>>>>>如果是重新下载的case，删除老文件，重新下载
		if (blag) {
			File oldFile = new File(localPath);
			if (oldFile.exists())
				oldFile.delete();
		}
		resInfo.setDownloadTimes(resInfo.downloadTimes + 1);
		tv_downloadtimes.setText(timesText(String.valueOf(resInfo.downloadTimes)));

		String fileurl = Configuration.getResDownload() + resInfo.getSid();
		String filename = null;
		if (resInfo.getFileFormat().equals("doc")) {
			resInfo.setFileFormat("txt");
		}
		if (resInfo.getResName().contains(resInfo.getFileFormat())) {
			filename = resInfo.getResName();
		} else {
			filename = resInfo.getResName() + "." + resInfo.getFileFormat();
		}

		String savePath = getSavePath();

		// >>>>>>>>>>初始化下载任务
		DownloadJob downloadJob = new DownloadJob();
		downloadJob.setmResID(Integer.valueOf(resInfo.getSid()));
		double endPos = resInfo.getFileSize();
		downloadJob.setEndPos((int) endPos);
		downloadJob.setDoneSize(0);
		downloadJob.setUrl(fileurl);
		downloadJob.setFileName(filename);
		downloadJob.setSaveFileName(resInfo.getResName() + DownloadJob.SUFFIX_RESID + resInfo.getSid() + "."
				+ resInfo.getFileFormat());
		downloadJob.setSavePath(savePath);
		downloadJob.setType(resInfo.getResType());
		downloadJob.setDwonTime(Util.getSysNowTime());
		downloadJob.setStatus(DownloadJob.DOWNLOAD_INIT);
		downloadJob.setIsopen(isopen);
		// >>>>>>设置下载任务的id
		long JobID = downloadDao.saveDownloadInfo(downloadJob);
		if (JobID > 0) {
			showToast(resInfo.getResName() + "已经加入到下载列表中。");
			downloadJob.setId((int) JobID);
			// >>>>>>>添加下载任务
			downloadManager.addDownloadJob(downloadJob);
			Intent intent = new Intent(this, PangkerService.class);
			intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DOWNLOAD);
			// >>>>>>>>>>传递上传jobID到service
			intent.putExtra(DownloadJob.DOWNLOAD_JOB_ID, downloadJob.getId());
			startService(intent);
		}
	}

	private void showScore() {
		float score = 0f;
		float sum = (float) resInfo.getScore() / 2;
		if (resInfo.appraiseTimes != null && resInfo.appraiseTimes >= 0) {
			score = sum / resInfo.appraiseTimes;
		}
		rb_res_score.setRating(score);
	}

	private void checkEdited() {
		// TODO Auto-generated method stub
		checkMenuEdit();
		toggleComment();
	}

	private void checkMenuEdit() {
		// TODO Auto-generated method stub
		if (pViewFlipper.getDisplayedChild() == 1) {
			quickAction.removeActionItem(editActionItem);
			editActionItem.setTitle("进行编辑");
			quickAction.addActionItem(editActionItem, true);
			tv_resname.setEnabled(false);
			tv_description.setEnabled(false);
			tv_description.setClickable(false);
		} else {
			quickAction.removeActionItem(editActionItem);
			editActionItem.setTitle("取消编辑");
			quickAction.addActionItem(editActionItem, true);
			tv_resname.setEnabled(true);
			tv_description.setEnabled(true);
			tv_description.setClickable(true);
		}
	}

	private void initEditMenu() {
		// TODO Auto-generated method stub
		editActionItem.setTitle("进行编辑");
		tv_resname.setEnabled(false);
		tv_description.setEnabled(false);
		tv_description.setClickable(false);
		quickAction.addActionItem(editActionItem, true);
	}

	// 对SERACH_TYPE参数进行判断
	private void checkShare() {
		// TODO Auto-generated method stub
		// 如果是编辑模式，就没有菜单
		if (SERACH_TYPE == 4) {
			uiOperate.btn_refresh.setVisibility(View.GONE);
			uiUserInfo.iv_share.setVisibility(View.GONE);
			pViewFlipper.setDisplayedChild(1);
			tv_resname.setEnabled(true);
			tv_description.setEnabled(true);
			tv_description.setClickable(true);
		} else if (SERACH_TYPE != 4 && SERACH_TYPE != 0) {
			quickAction.clearActionItem();
			uiUserInfo.iv_share.setVisibility(View.VISIBLE);
			if (resInfo.ifShare == 0) {
				uiUserInfo.iv_share.setImageResource(R.drawable.icon32_unshare);
				shareActionItem.setTitle("分享资源");
				if (SERACH_TYPE == 2) {
					quickAction.addActionItem(shareActionItem);
					quickAction.addActionItem(new ActionItem(2, "删除资源"));
					initEditMenu();
				}
				if (SERACH_TYPE == 3) {
					if (resShowEntity.isIfshare()) {
						quickAction.addActionItem(new ActionItem(4, "取消转播"));
					} else {
						quickAction.addActionItem(shareActionItem);
						quickAction.addActionItem(new ActionItem(2, "删除资源"), true);
					}
				}
			} else {
				uiUserInfo.iv_share.setImageResource(R.drawable.icon32_share);
				shareActionItem.setTitle("取消分享");
				if (SERACH_TYPE == 2) {
					quickAction.addActionItem(shareActionItem);
					quickAction.addActionItem(new ActionItem(2, "删除资源"));
					initEditMenu();
				}
				if (SERACH_TYPE == 3) {
					if (resShowEntity.isIfshare()) {
						quickAction.addActionItem(new ActionItem(4, "取消转播"));
					} else {
						quickAction.addActionItem(shareActionItem);
						quickAction.addActionItem(new ActionItem(2, "删除资源"), true);
					}
				}
			}
		}
	}

	// >>>>>>>>加载资源详情
	@Override
	public void showResInfo(ResInfo resinfo) {
		// TODO Auto-generated method stub
		if (resinfo == null) {
			showToast("资源不存在!");
			PangkerManager.getTabBroadcastManager().removeUserListenter(this);
			finish();
			return;
		}

		if (resinfo.ifShare == 0 && !resinfo.getUid().equals(userId)) {
			showToast("该用户已经取消分享,暂时不能查看!");
			PangkerManager.getTabBroadcastManager().removeUserListenter(this);
			finish();
			return;
		}
		this.resInfo = resinfo;
		// 界面检查,以及参数判断
		checkShare();

		uiOperate.enableOperateAll(true);
		uiOperate.checkDownload();

		tv_resname.setText(resinfo.getResName());
		String fileSize = Formatter.formatFileSize(this, resinfo.getFileSize().longValue());
		tv_ressize.setText("大小:" + fileSize);
		String temp;
		if (resShowEntity.getResType() == PangkerConstant.RES_DOCUMENT) {
			temp = resinfo.getDocAuthor();
			if (temp != null && !temp.equals("")) {
				tv_res_author.setText(resinfo.getDocAuthor());
			} else {
				tv_res_author.setText("未知");
			}
		} else if (resShowEntity.getResType() == PangkerConstant.RES_MUSIC) {
			temp = resinfo.getSinger();
			if (temp != null && !temp.equals("")) {
				tv_res_author.setText(resinfo.getSinger());
			} else {
				tv_res_author.setText("未知");
			}
		}

		showScore();
		tv_favoritetimes.setText(timesText(String.valueOf(resinfo.getFavorTimes())));
		tv_downloadtimes.setText(timesText((String.valueOf(resinfo.getDownloadTimes()))));
		tv_forwardtimes.setText(timesText(String.valueOf(resinfo.getShareTimes())));

		uiUserInfo.showData();

		if (resInfo.isMyres == 1) {// 本人资源
			uiOperate.btn_favorite.setBackgroundResource(R.drawable.form_vipinf_but_keep_c);
			uiOperate.btn_favorite.setEnabled(false);
			uiOperate.btn_forward.setBackgroundResource(R.drawable.form_vipinf_but_relay_c);
			uiOperate.btn_forward.setEnabled(false);
			uiOperate.enableSocre(false);// 不能评分，
		}

		if (resInfo.isScore != null && resInfo.isScore == 1) {
			uiOperate.enableSocre(false);
		}
		if (resInfo.isGarner != null && resInfo.isGarner == 1) {
			uiOperate.btn_favorite.setBackgroundResource(R.drawable.form_vipinf_but_keep_c);
			uiOperate.btn_favorite.setEnabled(false);
		}
		if (resInfo.isRebroadcast != null && resInfo.isRebroadcast == 1) {
			uiOperate.btn_forward.setBackgroundResource(R.drawable.form_vipinf_but_relay_c);
			uiOperate.btn_forward.setEnabled(false);
		}

		// 文档封面，根据格式
		if (resShowEntity.getResType() == PangkerConstant.RES_DOCUMENT) {
			if (resInfo.getFileFormat() != null && resInfo.getFileFormat().endsWith("txt")) {
				iv_cover.setImageResource(R.drawable.attachment_doc);
			} else {
				iv_cover.setImageResource(R.drawable.attachment_doc);
			}
		}
		// >>>>>>>>>>>>>是否是音乐资源
		if (resInfo.getResType() != null && resInfo.getResType() == PangkerConstant.RES_MUSIC) {
			resShowEntity.setSinger(resInfo.getSinger());
			resShowEntity.setDuration(resInfo.getDuration() != null ? resInfo.getDuration() : 0);
		}
		// 是否应用资源
		if (resInfo.getResType() != null && resInfo.getResType() == PangkerConstant.RES_APPLICATION) {
			if (!Util.isEmpty(resInfo.getPackageinfo()) && Util.checkAppIsExist(this, resInfo.getPackageinfo())) {
				uiOperate.btn_open.setText("运行");
			}
		}

		// 显示资源描述
		String desc = getResinfoDesc();
		if (!Util.isEmpty(desc)) {
			tv_description.setText(desc);
			tv_description.setFilters(ShowhideOnClickListener.filters);
			if (desc.length() > PangkerConstant.showLength) {
				tvResonShow.setText("显示更多");
				tvResonShow.setVisibility(View.VISIBLE);
			} else
				tvResonShow.setVisibility(View.GONE);
			tvResonShow.setOnClickListener(new ShowhideOnClickListener(tv_description, desc));
			tv_description.setText(desc);
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		super.uiCallBack(response, caseKey);
		if (!HttpResponseStatus(response))
			return;
		// 评分,收藏
		if (caseKey == KEY_RATINGS || caseKey == KEY_COLLECT) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				dealBaseResult(caseKey);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == KEY_TRANS) {// 转播
			ResShareResult result = JSONUtil.fromJson(response.toString(), ResShareResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showToast("转播成功!");
				tv_forwardtimes.setText(timesText(String.valueOf(resInfo.getShareTimes())));
				uiOperate.btn_forward.setBackgroundResource(R.drawable.form_vipinf_but_relay_c);
				uiOperate.btn_forward.setEnabled(false);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == SUBMIT_COMMENT) {// 发送评论
			CommentResult submitResult = JSONUtil.fromJson(response.toString(), CommentResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				showToast(submitResult.errorMessage);

				// 重新获取资源评论
				uiSubmit.etComment.setText("");
				uiSubmit.getNewCommentinfo().setCommentId(String.valueOf(submitResult.getCommentId()));
				// >>>>>不需要再次获取
				uiComment.addComment(uiSubmit.getNewCommentinfo());
				// >>>>>>>通知对方您的评论
				sendMessageTip();
			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
		}
		if (caseKey == SEARCH_COMMENT) {
			SearchCommentsResult commentResult = JSONUtil.fromJson(response.toString(), SearchCommentsResult.class);
			uiComment.showComments(commentResult);
		}
		if (caseKey == KEY_EDITRES) {
			if (SERACH_TYPE != 4) {
				checkEdited();
			}
			BaseResult resEditResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (resEditResult != null && resEditResult.getErrorCode() == BaseResult.SUCCESS) {
				showToast(resEditResult.getErrorMessage());
				if (SERACH_TYPE == 4) {
					PangkerManager.getTabBroadcastManager().removeUserListenter(this);
					finish();
				}
			} else if (resEditResult != null && resEditResult.getErrorCode() == BaseResult.FAILED) {
				showToast(resEditResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}
		if (caseKey == DEL_COMMENT) {
			BaseResult submitResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				// >>>>>>直接删除
				uiComment.deleteComment();
				showToast(submitResult.errorMessage);
			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
		}
		if (caseKey == KEY_DELETE || caseKey == KEY_FORWARD) {// 资源删除，显示下一张，没有就关闭
			BaseResult submitResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				showToast(submitResult.errorMessage);
				if (caseKey == KEY_DELETE) {
					Message msg = new Message();
					msg.what = 1;
					PangkerManager.getTabBroadcastManager().sendResultMessage(ResNetManagerActivity.class.getName(),
							msg);
				}
				application.getResLists().remove(application.getViewIndex());
				if (application.getResLists().size() > 0) {
					if (application.getViewIndex() < application.getResLists().size()) {
						resShowEntity = application.getResLists().get(application.getViewIndex());
						initData(true);
					} else if (application.viewIndexMinus() >= 0) {
						resShowEntity = application.getResLists().get(application.getViewIndex());
						initData(true);
					}
				} else {
					setResult(RESULT_OK);
					PangkerManager.getTabBroadcastManager().removeUserListenter(this);
					finish();
				}
			} else if (submitResult != null) {
				showToast(submitResult.errorMessage);
			}
		}
		if (caseKey == KEY_SHARE) {// 分享
			ResShareResult result = JSONUtil.fromJson(response.toString(), ResShareResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				resInfo.ifShare = resInfo.ifShare == 1 ? 0 : 1;
				checkShare();
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
	}

	public void editResinfo() {
		if (tv_resname.isEnabled()) {
			if (Util.isEmpty(tv_resname.getText().toString())) {
				showToast("资源名称不能为空!");
				return;
			}
			resEditSave(tv_resname.getText().toString(), tv_description.getText().toString());
		} else {
			tv_resname.setEnabled(true);
			tv_description.setEnabled(true);
		}
	}

	class UIOperate implements View.OnClickListener {
		/**
		 * 资源的操作分为3种情况
		 * --1：资源进行加载的时候不能进行操作;2：资源是自己的，不能进行相关的操作，以及提示;3：已经进行过的操作，不能进行重复操作
		 */
		private Button btn_praise; // 赞
		private Button btn_tread; // 踩
		private Button btn_score; // 评分

		private Button btn_download;// 下载
		private Button btn_favorite; // 收藏
		private Button btn_forward;// 转播
		private Button btn_recommend;// 推荐
		// private Button btn_comment;// 评论
		private Button btn_open;

		private Button btn_refresh;// 刷新

		public UIOperate() {
			// 赞
			btn_praise = (Button) findViewById(R.id.btn_praise);
			btn_praise.setOnClickListener(this);
			// 踩
			btn_tread = (Button) findViewById(R.id.btn_tread);
			btn_tread.setOnClickListener(this);
			// 评分
			btn_score = (Button) findViewById(R.id.btn_score);
			btn_score.setOnClickListener(this);
			// 评论
			btn_download = (Button) findViewById(R.id.btn_download);
			btn_download.setOnClickListener(this);
			// 收藏
			btn_favorite = (Button) findViewById(R.id.btn_favorite);
			btn_favorite.setOnClickListener(this);
			// 转播
			btn_forward = (Button) findViewById(R.id.btn_forward);
			btn_forward.setOnClickListener(this);
			// 推荐
			btn_recommend = (Button) findViewById(R.id.btn_recommend);
			btn_recommend.setOnClickListener(this);
			// 评论
			// btn_comment = (Button) findViewById(R.id.btn_comment);
			// btn_comment.setOnClickListener(this);

			btn_open = (Button) findViewById(R.id.btn_open);
			btn_open.setOnClickListener(this);
			//
			btn_refresh = (Button) findViewById(R.id.iv_more);
			btn_refresh.setBackgroundResource(R.drawable.btn_menu_bg);
			btn_refresh.setOnClickListener(this);
			if (SERACH_TYPE == 0) {
				btn_refresh.setVisibility(View.GONE);
			} else {
				btn_refresh.setVisibility(View.VISIBLE);
			}

		}

		// 本来打算遍历旁克目录的，太耗时
		private String exitFileSDPath(String filename) {
			// TODO Auto-generated method stub
			// >>>>>>>>>应用下载
			if (resInfo.getResType() == PangkerConstant.RES_APPLICATION) {
				if (Util.isExitFileSD(PangkerConstant.DIR_APP + "/" + filename)) {
					return PangkerConstant.DIR_APP + "/" + filename;
				}
			}
			// >>>>>>>>文档
			if (resInfo.getResType() == PangkerConstant.RES_DOCUMENT) {
				if (Util.isExitFileSD(PangkerConstant.DIR_DOCUMENT + "/" + filename)) {
					return PangkerConstant.DIR_DOCUMENT + "/" + filename;
				}
			}
			// >>>>>>>>>音乐
			if (resInfo.getResType() == PangkerConstant.RES_MUSIC) {
				if (Util.isExitFileSD(PangkerConstant.DIR_MUSIC + "/" + filename)) {
					return PangkerConstant.DIR_MUSIC + "/" + filename;
				}
			}
			// >>>>>>>>>图片下载
			if (resInfo.getResType() == PangkerConstant.RES_PICTURE) {
				if (Util.isExitFileSD(PangkerConstant.DIR_PICTURE + "/" + filename)) {
					return PangkerConstant.DIR_PICTURE + "/" + filename;
				}
			}

			return null;
		}

		/**
		 * 判断在sd卡是否存在该文件， 1：下载数据库中是否存在 2：本地文件目录是否存在该文件（指定地址是否存在）
		 * 下载判断的流程，先判断是否下载过，然后判断是否上传过，最后判断本地Sd卡是否有这个文件
		 */
		public void checkDownload() {
			// >>>>>>>>完整的文件名称 : 文件名称 + “-” + 资源ID + 后缀（文件格式）
			String saveFileName = resInfo.getResName() + DownloadJob.SUFFIX_RESID + resInfo.getSid() + "."
					+ resInfo.getFileFormat();
			// >>>>>>>>>获取本地路径
			localPath = exitFileSDPath(saveFileName);
			if (!Util.isEmpty(localPath)) {
				isDownloaded = true;
				btn_download.setBackgroundResource(R.drawable.res_btn_downed);
			}
		}

		public void enableOperateAll(boolean flag) {
			btn_favorite.setEnabled(flag);
			btn_forward.setEnabled(flag);
			btn_download.setEnabled(flag);
			btn_praise.setEnabled(flag);
			btn_tread.setEnabled(flag);
			btn_recommend.setEnabled(flag);
			btn_score.setEnabled(flag);

			if (flag) {
				btn_favorite.setBackgroundResource(R.drawable.res_btn_collect);
				btn_forward.setBackgroundResource(R.drawable.res_btn_broadcast);
				btn_download.setBackgroundResource(R.drawable.res_btn_down);
				btn_praise.setBackgroundResource(R.drawable.btn_praise_bg);
				btn_tread.setBackgroundResource(R.drawable.btn_hate_bg);
				btn_score.setBackgroundResource(R.drawable.res_btn_score);
			}
		}

		public void enableSocre(boolean b) {
			if (!b) {
				btn_praise.setBackgroundResource(R.drawable.form_vipinf_but_good_p);
				btn_tread.setBackgroundResource(R.drawable.form_vipinf_but_stepup_p);
				btn_score.setBackgroundResource(R.drawable.form_vipinf_but_poin_p);
				btn_praise.setEnabled(false);
				btn_tread.setEnabled(false);
				btn_score.setEnabled(false);
			}
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btn_recommend) {
				Intent intent = new Intent(ResInfoActivity.this, ContactsSelectActivity.class);
				intent.putExtra(ContactsSelectActivity.INTENT_SELECT, ContactsSelectActivity.TYPE_RES_RECOMMEND);
				if (resShowEntity.getResName() == null) {
					resShowEntity.setResName(resInfo.getResName() != null ? resInfo.getResName() : "未知");
				}
				intent.putExtra(ResShowEntity.RES_ENTITY, resShowEntity);
				startActivity(intent);

				return;
			}
			if (resInfo == null) {
				showToast("正在加载,还不能进行该操作,请稍等!");
				return;
			}
			if (v == btn_refresh) {
				quickAction.show(v);
			}
			if (v == btn_open) {
				playResinfo();
			}

			if (v == btn_forward) {
				transRes();
			}
			if (v == btn_favorite) {
				searchDirInfo(resShowEntity.getResType());
			}
			if (v == btn_praise) {
				ratingServer("0", 10);
			}
			if (v == btn_tread) {
				ratingServer("1", 0);
			}
			if (v == btn_score) {
				ratingPopup.show();
			}
			// 下载事件
			if (v == btn_download) {
				adjustDownload();
			}
		}
	}

	private void adjustDownload() {
		// 判断文件是否正在下载
		String fileurl = Configuration.getResDownload() + resInfo.getSid();
		// >>>>>>>>>判断该文件是否正在下载(根据网络地址来判断)
		if (downloadManager.isHttpFileDowning(fileurl)) {
			Intent intent = new Intent(ResInfoActivity.this, UpDownLoadManagerActivity.class);
			startActivity(intent);
			return;
		}
		// 是否下载过
		if (isDownloaded == true) {
			showDownloadDialog();
			return;
		}

		download(isDownloaded, false);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initGesture();
		Log.i("ResInfoActivity", "onResume");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		Log.i("ResInfoActivity", "onActivityResult");
		if (requestCode == CODE_PLAY) {
			if (application.getViewIndex() != resultCode) {
				application.setViewIndex(resultCode);
				resShowEntity = application.getResLists().get(application.getViewIndex());
				initData(true);
			}
		}
		if (requestCode == NET_SET) {
			adjustDownload();
		}
		if (requestCode == 1) {
			ResShowEntity mNewResShowEntity = application.getResLists().get(application.getViewIndex());
			if (mNewResShowEntity != null && resShowEntity != null
					&& resShowEntity.getResID() != mNewResShowEntity.getResID()) {
				this.resShowEntity = mNewResShowEntity;

				// >>>>>>重新设置
				initData(true);
			}
		}

	}

	GestureCommandRegister register = new GestureCommandRegister();

	@Override
	public void gestureNext() {
		// TODO Auto-generated method stub
		if (isLoading) {
			return;
		}
		if (application.getViewIndex() > 0) {
			resShowEntity = application.getResLists().get(application.viewIndexMinus());
			initData(true);
		} else {
			showToast("已经是第一个了!");
		}
	}

	@Override
	public void gesturePrev() {
		// TODO Auto-generated method stub
		if (isLoading) {
			return;
		}
		if (application.getViewIndex() < application.getResLists().size() - 1) {

			resShowEntity = application.getResLists().get(application.viewIndexAdd());
			initData(true);
		} else {
			showToast("已经是最后一个!");
		}
	}

	// 进入资源详细信息界面
	private void editRes() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ResInfoEditActivity.class);
		intent.putExtra("resShowEntity", resShowEntity);
		startActivity(intent);
	}

	private MessageTipDialog mMessageTipDialog;

	private void showTipDialog() {
		// TODO Auto-generated method stub
		if (mMessageTipDialog == null) {
			mMessageTipDialog = new MessageTipDialog(mResultCallback, this);
		}
		mMessageTipDialog.showDialog("", "确认要删除该资源？");
	}

	MessageTipDialog.DialogMsgCallback mResultCallback = new MessageTipDialog.DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if (isSubmit) {
				deleteRes();
			}
		}
	};

	@Override
	public void onItemClick(QuickAction source, int pos, int actionId) {
		// TODO Auto-generated method stub
		if (actionId == 1) {
			ushareResInfo(null);
		}
		if (actionId == 2) {
			showTipDialog();
		}
		if (actionId == 3) {
			editRes();
			// checkMenuEdit();
			// toggleComment();
		}
		if (actionId == 4) {
			ushareResInfo(resShowEntity.getShareId());
		}
	}

	Handler mHandler = new Handler();

	@Override
	public void onMessageListener(final Message msg) {
		// TODO Auto-generated method stub
		if (msg.what == 1) {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					DownloadJob job = (DownloadJob) msg.obj;
					downloadOver(job.getAbsolutePath(), job.getUrl());
					if (job.isIsopen()) {
						if (job.getType() == PangkerConstant.RES_APPLICATION) {
							Util.instalApplication(ResInfoActivity.this, job.getAbsolutePath());
						} else if (job.getType() == PangkerConstant.RES_DOCUMENT) {
							openReader(job.getAbsolutePath());
						}
					}
				}
			});

		}
	}

	private void openReader(String path) {
		if (!Util.isExitFileSD(path)) {
			showToast("抱歉，文件不存在!");
			return;
		}

		DirInfo dirInfo = new DirInfo();
		dirInfo.setPath(path);
		dirInfo.setDirName(resInfo.getResName());
		dirInfo.setRemark(0);
		Intent intent = new Intent(this, DocReaderActivity.class);

		intent.putExtra("DirInfo", dirInfo);

		startActivity(intent);
	}
}
