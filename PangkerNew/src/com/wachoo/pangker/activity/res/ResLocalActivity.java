package com.wachoo.pangker.activity.res;

import android.os.Bundle;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.ui.QuickAction;

public abstract class ResLocalActivity extends CommonPopActivity{
	
	public final static int RequestCode = 0x100;
	public final static int RequestCode_Reader = 0x101;
	public final static int RequestCode_FILE = 0x102;
	protected DirInfo dirInfo;//本地资源信息
	protected IResDao iResDao;
	protected String userId;
	protected ResLocalModel resLocalStatu;
	protected PangkerApplication application;
	protected QuickAction quickAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		iResDao = new ResDaoImpl(this);
		if(getIntent().getSerializableExtra("ResMiddleManager_Flag") == null){
			resLocalStatu = ResLocalModel.seeStatu;
		} else {
			resLocalStatu = (ResLocalModel) getIntent().getSerializableExtra("ResMiddleManager_Flag");
		}
	}
	
	//删除一个选择的资源
	protected boolean deleteDirInfo(){
		return false;
	}
	
	public enum ResLocalModel{
		Edit,     
		upLoad,  // 上传
		seeStatu, //查看
		change//更换
	}
	
}
