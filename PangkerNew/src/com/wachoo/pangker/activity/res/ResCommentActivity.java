package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.ResCommentAdapter;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.listener.TextCountLimitWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.Commentinfo;
import com.wachoo.pangker.server.response.SearchCommentsResult;
import com.wachoo.pangker.ui.PushListView;
import com.wachoo.pangker.ui.PushListView.OnLoadMoreListener;
import com.wachoo.pangker.util.Util;

/**
 * 资源（应用、音乐、阅读）评论界面 发送一条评论就会刷新一次界面 zhengjy
 * 
 * 2012-3-13
 */
public class ResCommentActivity extends CommonPopActivity implements IUICallBackInterface {

	private int pageIndex = 0;
	private final int pageLength = 10;

	private List<Commentinfo> commentList = new ArrayList<Commentinfo>();
	private ImageButton btnReply;
	private String userId;
	private UserInfo userInfo;
	private String type;
	private String resId;
	private Commentinfo itemCommentinfo;

	private AnimationDrawable rocketAnimation;
	private TextView txtSum;
	private EditText etContent;
	private EditText et_dialog;
	private PushListView commentListView;
	private ResCommentAdapter commentAdapter;
	private ServerSupportManager serverMana;

	private final int SEARCH_REPLY = 0x100;
	private final int SUBMIT_COMMENT = 0x101;
	private final int SUBMIT_REPLY = 0x102;
	private String content;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// 防止输入法在启动Activity时自动弹出
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		// setContentView(R.layout.res_comment_layout);

		Intent intent = this.getIntent();
		type = intent.getStringExtra("type");
		resId = intent.getStringExtra("resId");
		PangkerApplication application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		userInfo = application.getMySelf();
		serverMana = new ServerSupportManager(this, this);
		commentAdapter = new ResCommentAdapter(this, commentList);
		commentAdapter.setOnClickListener(rOnClickListener);
		initView();
		commentListView.setAdapter(commentAdapter);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		if (id == 1) {
			Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("回复评论");
			View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_editview, null);
			et_dialog = (EditText) dialogView.findViewById(R.id.et_dialog);
			et_dialog.addTextChangedListener(new TextCountLimitWatcher(20, et_dialog));
			et_dialog.setText("");
			et_dialog.setLines(3);
			et_dialog.setHint("评论回复");
			builder.setView(dialogView);
			builder.setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					String reply = et_dialog.getEditableText().toString();
					if (!Util.isEmpty(reply)) {
						submitReply(reply, itemCommentinfo.getCommentId());
						dialog.dismiss();
					} else {
						showToast(R.string.res_input_comment);
					}
				}
			});
			builder.setNegativeButton(R.string.cancel, null);
			return builder.create();
		}
		return super.onCreateDialog(id);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (findViewById(R.id.main_layout).getVisibility() == View.GONE) {
			searchComment();
		}
	}

	protected void stopLoading() {
		if (rocketAnimation != null) {
			rocketAnimation.stop();
		}
		findViewById(R.id.loading).setVisibility(View.GONE);
		findViewById(R.id.main_layout).setVisibility(View.VISIBLE);
	}

	private void initView() {

		// txtSum =(TextView) findViewById(R.id.txtSum);
		// commentListView = (PushListView) findViewById(R.id.comment_list);
		commentListView.setOnLoadMoreListener(onLoadMoreListener);

		btnReply = (ImageButton) findViewById(R.id.btn_reply);
		btnReply.setOnClickListener(onClickListener);
	}

	/**
	 * 查询评论接口
	 * 
	 * @param count
	 *            请求数目, askId 问题Id
	 * 
	 *            接口：SearchComments.json
	 */
	private void searchComment() {
		rocketAnimation.start();
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("resid", resId));
		paras.add(new Parameter("type", type));
		paras.add(new Parameter("limit", (pageIndex * pageLength) + "," + pageLength));
		serverMana.supportRequest(Configuration.getSearchComments(), paras, SEARCH_REPLY);
	}

	/**
	 * 资源评论接口
	 * 
	 * @param content
	 *            评论内容
	 * 
	 *            请求数目 接口：AddComment.json
	 */
	private void submitReply(String content, String commentId) {
		this.content = content;
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("resid", resId));// 问题id
		paras.add(new Parameter("type", type));// 操作id
		paras.add(new Parameter("content", content));// 回答内容
		if (commentId != null) {
			paras.add(new Parameter("commentid", commentId));// 回答内容
			serverMana.supportRequest(Configuration.getAddComment(), paras, true, "正在提交...", SUBMIT_REPLY);
		} else {
			btnReply.setEnabled(false);
			serverMana.supportRequest(Configuration.getAddComment(), paras, true, "正在提交...", SUBMIT_COMMENT);
		}
	}

	private void submitReply() {
		// TODO Auto-generated method stub
		if (!etContent.getText().toString().trim().equals("")) {
			submitReply(etContent.getText().toString(), null);
		} else
			showToast(R.string.res_input_comment);
	}

	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == btnReply) {
				submitReply();
			}
		}
	};

	OnClickListener rOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			itemCommentinfo = (Commentinfo) v.getTag();
			if (userId.equals(itemCommentinfo.getUserid())) {
				showToast("不能自己的评论进行回复!");
			} else {
				showDialog(1);
			}
		}
	};

	/**
	 * Pull to refresh,if getCount() < pageIdex * pageLength can not to service,
	 * sametime adjust dirId;
	 */
	private OnLoadMoreListener onLoadMoreListener = new OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			if (pageIndex * pageLength < commentListView.getCount()) {
				searchComment();
			}
		}
	};

	private void showComments(SearchCommentsResult commentResult) {
		// TODO Auto-generated method stub
		if (commentResult.getCommentinfo() != null && commentResult.getCommentinfo().size() > 0) {
			commentList = commentResult.getCommentinfo();
			if (pageIndex == 0) {
				commentAdapter.setCommentinfos(commentList);
			} else {
				commentAdapter.addCommentinfos(commentList);
			}
			txtSum.setText("总共有" + commentResult.getSumCount() + "条评论");
			if (commentResult.getCount() == pageLength) {
				pageIndex++;
			}
		} else if (commentResult.errorCode == BaseResult.SUCCESS) {
			txtSum.setText("总共有0条评论");
		} else
			showToast(R.string.res_get_comment_failed);
		commentListView.onLoadComplete(commentAdapter.getCount(), commentResult.getSumCount());
		commentListView.scrollTo(0, 0);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		stopLoading();
		btnReply.setEnabled(true);

		if (!HttpResponseStatus(response))
			return;

		switch (caseKey) {
		case SEARCH_REPLY:
			SearchCommentsResult commentResult = JSONUtil.fromJson(response.toString(), SearchCommentsResult.class);
			if (commentResult != null && commentResult.errorCode == BaseResult.SUCCESS) {
				showComments(commentResult);
			} else
				showToast(R.string.res_get_comment_failed);
			break;

		case SUBMIT_COMMENT:
			BaseResult submitResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
				showToast(submitResult.errorMessage);
				addComment();
			} else if (submitResult != null && submitResult.errorCode == 0) {
				showToast(submitResult.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
			break;
		case SUBMIT_REPLY:
			et_dialog.setText("");
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && result.errorCode == BaseResult.SUCCESS) {
				showToast("回复成功!");
				addCommentReply();
			} else if (result != null && result.errorCode == 0) {
				showToast(result.errorMessage);
			} else
				showToast(R.string.res_comment_failed);
			break;
		}
	}

	private void addComment() {
		// TODO Auto-generated method stub
		etContent.setText("");
		pageIndex = 0;
		searchComment();
	}

	private void addCommentReply() {
		// TODO Auto-generated method stub
		Commentinfo reply = new Commentinfo();
		reply.setContent(content);
		reply.setUserid(userId);
		reply.setUsername(userInfo.getUserName());
		reply.setCommenttime(Util.getSysNowTime());
		commentAdapter.addCommentReply(itemCommentinfo.getCommentId(), reply);
	}

}
