package com.wachoo.pangker.activity.res;

import java.io.File;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.about.AgreementActivity;
import com.wachoo.pangker.listener.CheckBoxChangedListener;
import com.wachoo.pangker.listener.TextLimitWatcher;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @ClassName: ResMusicUploadActivity
 * @Description: 音乐上传
 * @author longxianwen
 * @date Mar 9, 2012 2:16:40 PM
 * 
 */
public class UploadMusicActivity extends UploadActivity {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private ImageView ivAlbum; // 音乐专辑图片
	private EditText txtMusicName; // 音乐名称
	private TextView txtNameCount;
	private Button btnClear;
	private TextView txtMusicSize; // 音乐大小
	private CheckBox cbIfshare; // 是否分享音乐
	private EditText etMusicDetail; // 音乐描述
	private TextView txtDetailCount; //
	private EditText tvAuthor;// 演唱者
	// private Spinner mpinner;// 类别
	private LinearLayout typeLayout;// 类别
	private Button btnType;
	private TextView txtType;

	private String[] MusicTypes;
	private String[] queTypes;

	File file;// 上传的文件
	private MusicInfo musicInfo;

	private LinearLayout selectLayout;
	private LinearLayout uploadinfo;
	private LinearLayout ly_dirinfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.res_upload_apk);

		init();
		initView();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(this);
		if (id == 1) {
			builder.setSingleChoiceItems(queTypes, 0, dlistener);
			return builder.create();
		}
		return super.onCreateDialog(id);
	}

	DialogInterface.OnClickListener dlistener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			txtType.setText(queTypes[which]);
			uploadType = MusicTypes[which];
			dialog.dismiss();
		}
	};

	private void init() {
		// TODO Auto-generated method stub
		musicInfo = (MusicInfo) getIntent().getSerializableExtra("intent_music");
		userId = application.getMySelf().getUserId();

		queTypes = getResources().getStringArray(R.array.res_music_type);
		MusicTypes = getResources().getStringArray(R.array.res_music_value);
		uploadType = MusicTypes[0];
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		hideInputMethod(txtMusicName);
		super.onBackPressed();
	}

	private void initView() {
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
						UploadMusicActivity.this.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
				UploadMusicActivity.this.onBackPressed();
			}
		});
		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText(R.string.res_upload_music_title);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		btnUpload = (Button) findViewById(R.id.btn_upload);
		TextView author = (TextView) findViewById(R.id.res_author);
		author.setText("演唱者：");
		author.setVisibility(View.VISIBLE);
		tvAuthor = (EditText) findViewById(R.id.tv_author_name);
		tvAuthor.setVisibility(View.VISIBLE);
		// btnUpload.setTextColor(R.color.white);
		btnUpload.setOnClickListener(mClickListener);

		ivAlbum = (ImageView) findViewById(R.id.iv_apkicon);
		ivAlbum.setImageResource(R.drawable.icon46_music);
		txtMusicName = (EditText) findViewById(R.id.et_file_name);
		txtNameCount = (TextView) findViewById(R.id.txt_name_count);
		txtMusicName.addTextChangedListener(new TextLimitWatcher(MaxNameLenght, txtNameCount, txtMusicName));
		btnClear = (Button) findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(mClickListener);

		txtUpload = (TextView) findViewById(R.id.txt_upload);
		txtUpload.setText("点击选择本地音乐文件");
		txtMusicSize = (TextView) findViewById(R.id.tv_app_size); // 音乐大小
		cbIfshare = (CheckBox) findViewById(R.id.cb_ifshare); // 是否分享音乐
		boolean ifShare = getIntent().getBooleanExtra("ifShare", true);
		cbIfshare.setChecked(ifShare);
		etMusicDetail = (EditText) findViewById(R.id.et_apk_detail); // 音乐描述
		etMusicDetail.setText(R.string.res_upload_music_detail);
		txtDetailCount = (TextView) findViewById(R.id.txt_detail_count);

		txtDirName = (TextView) findViewById(R.id.txtDirname);
		TextView txtLabel = (TextView) findViewById(R.id.tv_label);
		txtLabel.setText("音乐类型:");
		typeLayout = (LinearLayout) findViewById(R.id.type_layout);
		typeLayout.setOnClickListener(mClickListener);
		txtType = (TextView) findViewById(R.id.tv_type);
		txtType.setHint(queTypes[0]);
		btnType = (Button) findViewById(R.id.btn_type_select);
		btnType.setOnClickListener(mClickListener);

		selectLayout = (LinearLayout) findViewById(R.id.selectLayout);
		uploadinfo = (LinearLayout) findViewById(R.id.uploadinfo);//
		ly_dirinfo = (LinearLayout) findViewById(R.id.ly_dirinfo);

		selectLayout.setOnClickListener(mClickListener);
		uploadinfo.setOnClickListener(mClickListener);
		ly_dirinfo.setOnClickListener(mClickListener);
		upoladLayout = (LinearLayout) findViewById(R.id.uploadLayout);
		upoladLayout.setOnClickListener(mClickListener);

		groupLayout = (LinearLayout) findViewById(R.id.groupLayout);
		txtGroupName = (TextView) findViewById(R.id.txt_groupname);
		if (!Util.isEmpty(groupId)) {
			groupLayout.setVisibility(View.VISIBLE);
			txtGroupName.setText(groupName);
		}

		cbTerms = (CheckBox) findViewById(R.id.checkbox_item);
		cbTerms.setOnCheckedChangeListener(new CheckBoxChangedListener(btnUpload, promptDialog));
		initUploadButton();
		tvAgreement = (TextView) findViewById(R.id.tv_agreement);
		tvAgreement.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(UploadMusicActivity.this, AgreementActivity.class);
				intent.putExtra(AgreementActivity.START_FLAG, AgreementActivity.UPLOAD_RES);
				UploadMusicActivity.this.startActivity(intent);
			}
		});

		if (musicInfo != null) {
			initData();
		}
		initDir();

	}

	private void initData() {
		filePath = musicInfo.getMusicPath();
		file = new File(filePath);
		if (file == null || !file.exists()) {
			showToast(R.string.res_get_file_failed);
			return;
		}
		findViewById(R.id.size_layout).setVisibility(View.VISIBLE);
		txtMusicName.setText(musicInfo.getMusicName());
		String strCount = txtMusicName.getEditableText().toString().trim();
		if (strCount.length() > MaxNameLenght) {
			this.txtMusicName.setText(strCount.substring(0, MaxNameLenght));
		}
		txtUpload.setText(txtMusicName.getText().toString());
		String musicSize = Formatter.formatFileSize(this, new Double(musicInfo.getMusicSize()).longValue());
		txtMusicSize.setText(musicSize);

		String url = Util.getAlbumArt(this, musicInfo.getMusicId());
		if (url != null && Uri.parse(url) != null) {
			ivAlbum.setImageURI(Uri.parse(url));
		} else {
			ivAlbum.setImageResource(R.drawable.icon46_music);
		}

		String singer = musicInfo.getSinger();
		if ("<unknown>".equalsIgnoreCase(singer)) {
			singer = "未知";
		}
		tvAuthor.setText(singer);
	}

	private void initDir() {
		// TODO Auto-generated method stub
		sDirInfo = application.getRootDir(PangkerConstant.RES_MUSIC);
		if (sDirInfo != null) {
			selectDirId = sDirInfo.getDirId();
		} else {
			PangkerManager.getTabBroadcastManager().addMsgListener(this);
			initDirService();
		}
		String dirName = sDirInfo != null ? sDirInfo.getDirNames() : "请获取目录";
		txtDirName.setText(dirName);
	}

	/**
	 * 上传资源接口
	 * 
	 * @param filepath
	 *            文件路径
	 * @param isFileExist
	 *            文件是否已经存在服务器
	 * 
	 *            接口：ResUpload.json
	 */
	protected void uploadFile(String filepath, int job_type) {
		try {
			String resName = txtMusicName.getText().toString();
			if (Util.isEmpty(resName)) {
				resName = musicInfo.getMusicName(); // 缩写的名字
			}
			String desc = etMusicDetail.getText().toString();
			String ifshare = cbIfshare.isChecked() ? "1" : "0";
			String format = Util.getFileformat(filepath);

			uploaderFile(PangkerConstant.RES_MUSIC, desc, ifshare, resName, format, musicInfo.getSinger(),
					musicInfo.getMusicId(), job_type);
			onBackPressed();
		} catch (Exception e) {
			logger.error(TAG, e);
		}
	}

	private void selectMusic() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.change);
		intent.setClass(this, ResLocalMusicActivity.class);
		startActivityForResult(intent, CODE_RESELECT);
	}

	private void uplodMusic() {
		if (Util.isEmpty(txtMusicName.getEditableText().toString())) {
			showToast("请输入歌曲名称!");
			return;
		}
		if (Util.isEmpty(selectDirId)) {
			showToast("请选择一个目录!");
			return;
		}
		uploadCheck(PangkerConstant.RES_MUSIC);
	}

	View.OnClickListener mClickListener = new View.OnClickListener() {
//		private LinearLayout uploadinfo;
//		private LinearLayout ly_dirinfo;
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.btn_upload:
				if (!uploadCheck()) {
					return;
				}
				if (filePath != null) {
					uplodMusic();
				} else {
					showToast("请选择本地音乐文件!");
				}
				break;
			case R.id.selectLayout:
			case R.id.ly_dirinfo:
				selectDirInfos(PangkerConstant.RES_MUSIC);
				break;
			case R.id.uploadLayout:
			case R.id.uploadinfo:
				selectMusic();
				break;
			case R.id.btn_clear:
				txtMusicName.setText("");
				break;
			case R.id.type_layout:
				showDialog(1);
				break;
			case R.id.btn_type_select:
				showDialog(1);
				break;
			}
		}
	};
	
	@Override
	protected void onLoaderGeo(String address) {
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == CODE_RESELECT && resultCode == RESULT_OK) {
			musicInfo = (MusicInfo) intent.getSerializableExtra("intent_music");
			initData();
		}
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				initDir();
			}
		};
		handler.sendMessage(msg);
	}

}
