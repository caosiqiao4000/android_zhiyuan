package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.MusicPlayActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.ResEventAdapter;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.EventResult;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.ResEventQueryResult;
import com.wachoo.pangker.service.MusicService;
import com.wachoo.pangker.service.MusicService.PlayControl;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * 资源动态列表信息
 * 
 * zhengjy
 * 
 * 2012-06-06
 */
public class ResNewsActivity extends ResEventSearchActivity  implements IUICallBackInterface {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	private final int SEARCH_EVENT = 0x100;
	private final int SUBMIT_REPLY = 0x101;

	private List<EventResult> eventList = new ArrayList<EventResult>();
    private ListView mListView;
	private ResEventAdapter resAdapter;
	private SharedPreferencesUtil SP_Util;
	private EventResult currEvent;
	

	private String mUserid;
	private PangkerApplication application;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.local_listview);
		application = (PangkerApplication) getApplication();
		mUserid = application.getMyUserID();
		SP_Util = new SharedPreferencesUtil(this);
		long defaultEndtime = System.currentTimeMillis() - 7200000;//两个小时以前
		endtime = SP_Util.getString("endtime", Util.getTimeOfFormat(defaultEndtime,PangkerConstant.DATE_FORMAT));
		
		initView();

		//searchEventlist();
		
		searchLastEventlist();
		
		
	}

	private void initView() {
		findViewById(R.id.progress_ly).setVisibility(View.GONE);
		mListView = (ListView) findViewById(R.id.musicListView);

		mListView.setOnItemClickListener(onItemClickListener);
		initPressBar(mListView);
		loadmoreView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(lyLoadPressBar.getVisibility() == View.GONE && lyLoadText.getVisibility() == View.VISIBLE){
					if(isLoadAll()){
						loadText.setText(R.string.nomore_data);
					}
					else searchEventlist();
				}
			}
		});
		resAdapter = new ResEventAdapter(this, eventList);
		mListView.setAdapter(resAdapter);
		
		Button button = (Button)findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ResNewsActivity.this.finish();
			}
		});
		
	}

	OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			currEvent = (EventResult)arg0.getAdapter().getItem(arg2);
			lookEventInfo(currEvent);
		}
	};
	
	private void lookEventInfo(EventResult currEvent) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		if(currEvent.getType() == PangkerConstant.RES_MUSIC){
			List<MusicInfo> musicInfos = new ArrayList<MusicInfo>();//播放列表
			MusicInfo musicInfo = new MusicInfo();
		    musicInfo.setMusicName(currEvent.getName());
		    musicInfos.add(musicInfo);
		    application.setMusiList(musicInfos);
		    application.setPlayMusicIndex(0);
		        
		    Intent intentSerivce = new Intent(this, MusicService.class);
		    intentSerivce.putExtra("control", PlayControl.MUSIC_START_PLAY);
			startService(intentSerivce);
			intent.setClass(this, MusicPlayActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		} else {
			ResShowEntity entity = new ResShowEntity();
			entity.setResType(currEvent.getType());
			entity.setResID(currEvent.getId());
			entity.setResName(currEvent.getName());	
			intent.putExtra("resShowEntity", entity);
			if(currEvent.getType() == PangkerConstant.RES_PICTURE){
				intent.setClass(ResNewsActivity.this, PictureInfoActivity.class);
			} else {
				intent.setClass(ResNewsActivity.this, ResInfoActivity.class);
			}
		}
		startActivity(intent); 
	}
	/**
	 * 查询动态资源接口
	 * 接口：ResEventInfoQuery.json
	 */
	private void searchEventlist() {
		//setPresdBarVisiable(true);
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "0"));
		paras.add(new Parameter("uid", mUserid));
		paras.add(getSearchLimit());
		paras.add(new Parameter("endtime", endtime));
		long nowTime = System.currentTimeMillis();
		String nowTimeStr = Util.getTimeOfFormat(nowTime,PangkerConstant.DATE_FORMAT);
		serverMana.supportRequest(Configuration.getResEventInfoQuery(), paras, SEARCH_EVENT);
		SP_Util.saveString("endtime", nowTimeStr);
	}
	
	/**
	 * 查询动态资源接口
	 * 接口：ResEventInfoQuery.json
	 */
	private void searchLastEventlist() {
		setPresdBarVisiable(true);
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "0"));
		paras.add(new Parameter("uid", mUserid));
		paras.add(new Parameter("limit", "0,100"));
		long nowTime = System.currentTimeMillis();
		String nowTimeStr = Util.getTimeOfFormat(nowTime,PangkerConstant.DATE_FORMAT);
		serverMana.supportRequest(Configuration.getResEventInfoLastQuery(), paras,true,"请稍候...", SEARCH_EVENT);
		SP_Util.saveString("endtime", nowTimeStr);
	}
	
	/**
	 * 相同资源去重
	 * 
	 * @param list
	 * @return
	 */
	private List<EventResult> deleteDuplicate(List<EventResult> list) {
		Set<Long> set = new HashSet<Long>();
		List<EventResult> newList = new ArrayList<EventResult>();
		for (Iterator<EventResult> iter = list.iterator(); iter.hasNext();) {
			EventResult element = (EventResult) iter.next();
			if (set.add(element.getId()))
				newList.add(element);
		}
		return newList;
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {

		if (!HttpResponseStatus(response)){
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			return;
		}
			
		switch (caseKey) {
		case SEARCH_EVENT:
			setPresdBarVisiable(false);
			ResEventQueryResult result = JSONUtil.fromJson(response.toString(),
					ResEventQueryResult.class);
			if (result != null && BaseResult.SUCCESS == result.getErrorCode()) {
				List<EventResult> newResult = result.getResult();
				if (newResult != null && newResult.size() > 0) {
					eventList = newResult;
					//newResult = Util.deleteDuplicate(newResult);//资源排重
					if(newResult.size() < incremental){
						setLoadAll(true);
						lyLoadPressBar.setVisibility(View.GONE);
						lyLoadText.setVisibility(View.GONE);
					}
					else {
						setPresdBarVisiable(false);
					}
					//如果不是从0开始加载
					if(getStartLimit() != 0){
						eventList.addAll(newResult);
					}
					//首次加载
					else {
						if(!isLoadAll()){
							setPresdBarVisiable(false);
						}
						eventList = newResult;
					}
					resetStartLimit();
					eventList = deleteDuplicate(eventList);//资源排重
					resAdapter.setEventList(eventList);
				}
				else if(newResult == null || newResult.size() == 0){
					showToast(R.string.no_res_data);
				}
				
			}
			break;

		}
	}

}
