package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.MyAlbumAdapter;
import com.wachoo.pangker.adapter.MyAlbumAdapter.ViewHolderAlbum;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.util.ImageUtil;

/**
 * 本地相册
 * 
 * @author Administrator
 * 
 */
public class AlbumActivity extends CommonPopActivity {

	// >>>>>>>>>获取本地图片
	private ImageLocalFetcher localFetcher;
	private ImageCache imageCache;

	private GridView gridView;
	private MyAlbumAdapter albumAdapter;
	private Button btnAll;
	private Button btnSure;
	private boolean isSelectedAll = false;

	protected IResDao iResDao;
	protected String userId;
	PangkerApplication pangkerApp;
	private ProgressDialog pd;
	private ImageCache mImageCache;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.album);
		init();
		gridView = (GridView) findViewById(R.id.PicGridView);
		gridView.setOnItemClickListener(onItemClickListener);
		View view = LayoutInflater.from(this).inflate(R.layout.empty_view, null, false);
		ImageView iv_empty_icon = (ImageView) view.findViewById(R.id.iv_empty_icon);
		TextView tv_empty_prompt = (TextView) view.findViewById(R.id.textViewEmpty);
		iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
		tv_empty_prompt.setText("没有发现本地应用!");
		LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		((ViewGroup) gridView.getParent()).addView(view, lp);
		gridView.setEmptyView(view);

		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("本地图片");
		gridView.setAdapter(albumAdapter);
		btnAll = (Button) findViewById(R.id.iv_more);
		btnAll.setText("全选");
		btnAll.setBackgroundResource(R.drawable.btn_default_selector);
		btnSure = (Button) findViewById(R.id.btn_back);

		btnAll.setOnClickListener(onClickListener);
		btnSure.setOnClickListener(onClickListener);
		setProgressBarIndeterminateVisibility(true);

		new LoadImagesFromSDCard(this).execute();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stu
		imageCache.removeMemoryCaches();
		super.onDestroy();
	}

	private void init() {
		// TODO Auto-generated method stub
		iResDao = new ResDaoImpl(this);
		pangkerApp = (PangkerApplication) getApplication();
		userId = ((PangkerApplication) getApplicationContext()).getMySelf().getUserId();

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		mImageCache = new ImageCache(this, imageCacheParams);

		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		localFetcher.setmSaveDiskCache(false);
		localFetcher.setImageCache(imageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);

		albumAdapter = new MyAlbumAdapter(this, localFetcher);
		albumAdapter.setIfShow(true);
	}

	class LoadDBTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(AlbumActivity.this);
			pd.setMessage("正在加载……");
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			List<DirInfo> lAppInfos = albumAdapter.getSeletedMembers();
			for (DirInfo dirInfo : lAppInfos) {
				if (!iResDao.isSelected(userId, dirInfo.getPath(), PangkerConstant.RES_PICTURE)) {
					dirInfo.setDeal(DirInfo.DEAL_SELECT);
					dirInfo.setUid(userId);
					dirInfo.setIsfile(DirInfo.ISFILE);
					dirInfo.setResType(PangkerConstant.RES_PICTURE);
					iResDao.saveSelectResInfo(dirInfo);
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			setResult(ResLocalPicActivity.RequestCode);
			pd.dismiss();
			AlbumActivity.this.finish();
		}
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == btnAll) {
				selectAll();
			}
			if (v == btnSure) {
				btnSure.setEnabled(false);
				if (albumAdapter.getSeletedCount() > 0) {
					new LoadDBTask().execute();
				} else {
					finish();
				}
			}
		}
	};

	private void selectAll() {
		// TODO Auto-generated method stub
		isSelectedAll = !isSelectedAll;
		if (isSelectedAll) {
			btnAll.setText("反选");
		} else {
			btnAll.setText("全选");
		}
		albumAdapter.setSelectedAll(isSelectedAll);
	}

	AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			ViewHolderAlbum holder = (ViewHolderAlbum) view.getTag();
			holder.selected.toggle();
			albumAdapter.selectByIndex(position, holder.selected.isChecked());
		}
	};

	private String getImagePath(int imageId) {
		String imagePath = null;
		String[] projection = { Media._ID, Media.DATA };
		Cursor cursor = getContentResolver().query(Media.EXTERNAL_CONTENT_URI, projection, Media._ID + "=" + imageId,
				null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			imagePath = cursor.getString(cursor.getColumnIndex(Media.DATA));
		}
		return imagePath;
	}

	class LoadImagesFromSDCard extends AsyncTask<Object, DirInfo, Object> {
		// Set up an array of the Thumbnail Image ID column we want
		String[] projection = { MediaStore.Images.Thumbnails.IMAGE_ID, MediaStore.Images.Thumbnails.DATA };
		private ProgressDialog dialog;

		public LoadImagesFromSDCard(Context context) {
			dialog = new ProgressDialog(context);
			dialog.setMessage("请稍后...");
			dialog.show();
		}

		@Override
		protected Object doInBackground(Object... params) {
			// TODO Auto-generated method stub
			Cursor cursor = null;
			List<DirInfo> dirInfos = new ArrayList<DirInfo>();
			try {
				cursor = managedQuery(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, projection, null, null, null);
				if (cursor != null) {
					cursor.moveToFirst();
					int image_idColumn = cursor.getColumnIndex(MediaStore.Images.Thumbnails.IMAGE_ID);
					int dataColumn = cursor.getColumnIndex(MediaStore.Images.Thumbnails.DATA);
					int temp = cursor.getCount();
					for (int i = 0; i < temp; i++) {
						DirInfo dirInfo = new DirInfo();
						dirInfo.setLocalid(cursor.getInt(image_idColumn));
						dirInfo.setPath(cursor.getString(dataColumn));
						dirInfos.add(dirInfo);
						// publishProgress(dirInfo);
						loadImage(dirInfo);

						cursor.moveToNext();
					}
				}
				cursor.close();
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				if (cursor != null)
					cursor.close();
			}
			return dirInfos;
		}

		private void loadImage(DirInfo dirInfo) {
			// TODO Auto-generated method stub
			mImageCache.addBitmapToCache(dirInfo.getPath(), ImageUtil.decodeFile(dirInfo.getPath(),
					PangkerConstant.PANGKER_ICON_SIDELENGTH, PangkerConstant.PANGKER_ICON_SIDELENGTH
							* PangkerConstant.PANGKER_ICON_SIDELENGTH), true);
		}

		@Override
		protected void onPostExecute(Object result) {
			dialog.dismiss();
			albumAdapter.setLocalDirInfos((List<DirInfo>) result);
		}
	}
}
