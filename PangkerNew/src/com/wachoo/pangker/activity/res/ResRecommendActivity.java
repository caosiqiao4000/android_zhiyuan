package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.msg.MsgInfoActivity;
import com.wachoo.pangker.adapter.RecommendInfoAdapter;
import com.wachoo.pangker.db.IRecommendDao;
import com.wachoo.pangker.db.IUserStatusDao;
import com.wachoo.pangker.db.impl.RecommendDaoImpl;
import com.wachoo.pangker.db.impl.UserStatusDaoImpl;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.QueryBoxResResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.HorizontalPager;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * @author zxx 接受，发送的推荐资源，实现分页，每页10条
 */
public class ResRecommendActivity extends CommonPopActivity {

	public static final String KEY_RECOMMEND_OPEN = "KEY_RECOMMEND_OPEN";

	// >>>>>>推荐打开方式设置
	private static final int ID_RECOMMEND_SETTING = 1;
	private String userId;
	private PangkerApplication application;
	private IRecommendDao recommendDao;
	// >>>>>>>>>>>title工具栏
	private Button btnBack;
	private Button btnMenu; // 菜单键
	private QuickAction mQuickAction;

	// >>>>>>>>>>滑动界面
	private RadioGroup mRadioGroup;
	private HorizontalPager mHorizontalPager;
	// >>>>>>>>>>接受界面
	private int acceptCount;
	private PullToRefreshListView acceptRefreshListView;
	private ListView recipientListView;
	private EmptyView acceptEmptyView;
	private FooterView acceptFooterView;
	private RecommendInfoAdapter acceptAdapter;
	// >>>>>>>>>>推荐界面
	private int sendCount;
	private PullToRefreshListView sendRefreshListView;
	private ListView sendListView;
	private EmptyView sendEmptyView;
	private FooterView sendFooterView;
	private RecommendInfoAdapter sendAdapter;

	private RecommendInfo recommendInfo;

	private PopMenuDialog menuDialog;
	private int index = 0;// // 0>>>>>>>>>>接受界面 1>>>>>>>>>>推荐界面

	private SharedPreferencesUtil mPreferencesUtil;
	private int seting;
	private ActionItem[] setMenu;
	private int[] recommendIds = null;
	private String[] recommendNames = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_recommend);
		mPreferencesUtil = new SharedPreferencesUtil(this);
		init();
		initView();
	}

	private void init() {
		// TODO Auto-generated method stub
		recommendDao = new RecommendDaoImpl(this);
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		String fromId = getIntent().getStringExtra("FROM_ID");
		if (!Util.isEmpty(fromId)) {
			refreshMsgInfoActivityById(fromId);
		}

		recommendIds = getResources().getIntArray(R.array.res_recommend_ids);
		recommendNames = getResources().getStringArray(R.array.res_recommend_names);

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
	}

	private void refreshMsgInfoActivityById(String fromId) {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(fromId)) {
			Message msg = new Message();
			msg.what = 2;
			msg.arg1 = MsgInfo.MSG_RECOMMEND;
			msg.obj = fromId;
			PangkerManager.getTabBroadcastManager().sendResultMessage(MsgInfoActivity.class.getName(), msg);
		}
	}

	private void initView() {
		// TODO Auto-generated method stub

		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("推荐资源");
		btnMenu = (Button) findViewById(R.id.iv_more);
		btnMenu.setBackgroundResource(R.drawable.btn_menu_bg);
		btnMenu.setOnClickListener(onClickListener);
		// >>>>>>>菜单功能按钮
		mQuickAction = new QuickAction(this, QuickAction.VERTICAL);
		mQuickAction.addActionItem(
				new ActionItem(ID_RECOMMEND_SETTING, getResources().getString(R.string.res_recommend_setting)), true);

		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {
				// TODO Auto-generated method stub
				// 清空已经完成down任务
				if (actionId == ID_RECOMMEND_SETTING) {
					showSetMenu();
				}
			}
		});
		// >>>>>>>>主题界面切换
		RadioButton rb1 = (RadioButton) findViewById(R.id.downloadsys);
		rb1.setText("收到的推荐");
		RadioButton rb2 = (RadioButton) findViewById(R.id.uploadsys);
		rb2.setText("发出的推荐");

		mRadioGroup = (RadioGroup) findViewById(R.id.searchRadioGroup);
		mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
				case R.id.downloadsys:
					mHorizontalPager.setCurrentScreen(0, true);
					break;
				case R.id.uploadsys:
					mHorizontalPager.setCurrentScreen(1, true);
					break;
				}
			}
		});
		mHorizontalPager = (HorizontalPager) findViewById(R.id.horizontal_pager);
		mHorizontalPager.setOnScreenSwitchListener(new HorizontalPager.OnScreenSwitchListener() {
			@Override
			public void onScreenSwitched(int screen) {
				// TODO Auto-generated method stub
				switch (screen) {
				case 0:
					mRadioGroup.check(R.id.downloadsys);
					break;
				case 1:
					mRadioGroup.check(R.id.uploadsys);
					break;
				}
			}
		});
		acceptRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_acceptlistview);
		sendRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_sendlistview);
		initAcceptView();
		initSendView();
	}

	private void initAcceptView() {
		// TODO Auto-generated method stub
		recipientListView = acceptRefreshListView.getRefreshableView();

		//
		recipientListView.setBackgroundDrawable(null);
		recipientListView.setCacheColorHint(R.color.clarity);

		acceptEmptyView = new EmptyView(this);
		acceptEmptyView.addToListView(recipientListView);

		acceptFooterView = new FooterView(this);
		acceptFooterView.addToListView(recipientListView);
		acceptFooterView.setOnLoadMoreListener(onLoadMoreListener2);

		acceptCount = recommendDao.getRecommendInfoCount(userId, true);

		acceptAdapter = new RecommendInfoAdapter(this, new ArrayList<RecommendInfo>(), mImageWorker);
		acceptAdapter.setmImageResizer(initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH));
		acceptAdapter.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				recommendInfo = (RecommendInfo) v.getTag();
				showMenu(0);
				return true;
			}
		});
		recipientListView.setAdapter(acceptAdapter);
		recipientListView.setFocusable(true);

		acceptEmptyView.showView(R.drawable.emptylist_icon, R.string.no_recommend_res);

		acceptRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				QueryBoxRes();
			}
		});
		initAcceptData();
	}

	private void initAcceptData() {
		// TODO Auto-generated method stub
		acceptAdapter.addRecommendInfos(recommendDao.getRecommendInfos(userId, true, acceptAdapter.getCount()));
		acceptFooterView.onLoadComplete(acceptAdapter.getCount(), acceptCount);
	}

	private FooterView.OnLoadMoreListener onLoadMoreListener2 = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			initAcceptData();
		}
	};

	private void initSendView() {
		// TODO Auto-generated method stub
		sendListView = sendRefreshListView.getRefreshableView();
		sendEmptyView = new EmptyView(this);
		sendEmptyView.addToListView(sendListView);

		sendFooterView = new FooterView(this);
		sendFooterView.addToListView(sendListView);
		sendFooterView.setOnLoadMoreListener(onLoadMoreListener);

		sendCount = recommendDao.getRecommendInfoCount(userId, false);

		sendAdapter = new RecommendInfoAdapter(this, new ArrayList<RecommendInfo>(), mImageWorker);
		sendAdapter.setmImageResizer(initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH));
		sendAdapter.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				recommendInfo = (RecommendInfo) v.getTag();
				showMenu(1);
				return true;
			}
		});
		sendListView.setAdapter(sendAdapter);
		// sendListView.setFocusable(false);
		sendEmptyView.showView(R.drawable.emptylist_icon, R.string.no_send_res);

		sendRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				QueryBoxRes();
			}
		});
		initSendData();
	}

	private void initSendData() {
		// TODO Auto-generated method stub
		sendAdapter.addRecommendInfos(recommendDao.getRecommendInfos(userId, false, sendAdapter.getCount()));
		sendFooterView.onLoadComplete(sendAdapter.getCount(), sendCount);
	}

	private FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			initSendData();
		}
	};

	ActionItem[] msgMenu = new ActionItem[] { new ActionItem(10, "删除") };// >>上传成功

	private void showMenu(int index) {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
		}
		menuDialog.setMenus(msgMenu);
		menuDialog.setTitle(recommendInfo.getResName());
		this.index = index;
		menuDialog.show();
	}

	private void showSetMenu() {
		seting = mPreferencesUtil.getInt(ResRecommendActivity.KEY_RECOMMEND_OPEN + application.getMyUserID(),
				recommendIds[0]);

		setMenu = new ActionItem[recommendIds.length];
		for (int i = 0; i < recommendIds.length; i++) {
			ActionItem item = new ActionItem(recommendIds[i], recommendNames[i], seting == recommendIds[i]);
			setMenu[i] = item;
		}
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mSetingClickListener);
		}
		menuDialog.setMenuChoose(setMenu, true);
		menuDialog.setTitle("资源接收设置");
		menuDialog.show();
	}

	UITableView.ClickListener mSetingClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			mPreferencesUtil.saveInt(ResRecommendActivity.KEY_RECOMMEND_OPEN + application.getMyUserID(),
					recommendIds[actionId]);
			menuDialog.dismiss();
		}
	};
	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			if (actionId == 10) {
				if (index == 0) {
					deleteRes(recommendInfo.getResId(), "0");
				}
				if (index == 1) {
					deleteRes(recommendInfo.getResId(), "1");
				}
			}
			menuDialog.dismiss();
		}
	};

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				// acceptAdapter.clearImageMemoryData();
				// sendAdapter.clearImageMemoryData();
				finish();
			}
			// >>>菜单按钮
			else if (v == btnMenu) {
				mQuickAction.show(btnMenu);
			}
		}
	};

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		// acceptAdapter.clearImageMemoryData();
		// sendAdapter.clearImageMemoryData();
	}

	/**
	 * 查询推荐资源
	 */
	public void QueryBoxRes() {
		recommendDao.truncRecommendInfo(userId);
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object supportResponse, int caseKey) {
				// TODO Auto-generated method stub
				sendRefreshListView.onRefreshComplete();
				acceptRefreshListView.onRefreshComplete();
				if (!HttpResponseStatus(supportResponse))
					return;
				QueryBoxResResult resResult = JSONUtil.fromJson(supportResponse.toString(), QueryBoxResResult.class);
				if (resResult != null && resResult.getErrorCode() == BaseResult.SUCCESS
						&& resResult.getBoxRes() != null) {
					IUserStatusDao mIUserStatusDao = new UserStatusDaoImpl(ResRecommendActivity.this);
					mIUserStatusDao.clearUser(userId);
					List<RecommendInfo> acceptLists = resResult.getBoxRes();
					List<RecommendInfo> sendLists = resResult.getOutBoxRes();
					recommendDao.saveRecommends(userId, acceptLists);
					recommendDao.saveRecommends(userId, sendLists);
					if (acceptLists != null && acceptLists.size() > 0) {
						acceptAdapter.setRecommendInfos(acceptLists);
						acceptFooterView.onLoadComplete(acceptLists.size(), 0);
					}
					if (sendLists.size() > 0) {
						sendAdapter.setRecommendInfos(sendLists);
						sendFooterView.onLoadComplete(sendLists.size(), 0);
					}
				}
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userId", userId));
		serverMana.supportRequest(Configuration.getQueryBoxRes(), paras);
	}

	/**
	 * 删除资源或者标识资源为已读
	 */
	public void deleteRes(String resId, final String optype) {
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {

			@Override
			public void uiCallBack(Object supportResponse, int caseKey) {
				// TODO Auto-generated method stub
				if (!HttpResponseStatus(supportResponse)) {
					return;
				}

				BaseResult resResult = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
				if (resResult != null) {
					if (resResult.getErrorCode() == resResult.SUCCESS) {
						if (!optype.equals("2")) {
							if (index == 0) {
								if (recommendDao.deleteRecommendInfo(recommendInfo.getId())) {
									acceptAdapter.delRecommendInfo(recommendInfo);
								}
								acceptCount--;
							}
							if (index == 1) {
								if (recommendDao.deleteRecommendInfo(recommendInfo.getId())) {
									sendAdapter.delRecommendInfo(recommendInfo);
								}
								sendCount--;
							}
						}
					} else if (resResult.getErrorCode() == resResult.FAILED) {
						showToast(resResult.getErrorMessage());
					} else
						showToast(R.string.to_server_fail);
				} else
					showToast(R.string.to_server_fail);
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("resid", resId));
		paras.add(new Parameter("optype", optype));
		serverMana.supportRequest(Configuration.getDeleteBoxRes(), paras, true,
				getResourcesMessage(R.string.please_wait));
	}

}
