package com.wachoo.pangker.activity.res;

import java.io.File;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.about.AgreementActivity;
import com.wachoo.pangker.listener.CheckBoxChangedListener;
import com.wachoo.pangker.listener.TextLimitWatcher;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.util.Util;

/**
 * @ClassName: ResFileUploadActivity
 * @Description: 阅读资源上传
 */
public class UploadDocActivity extends UploadActivity {

	private String TAG = com.wachoo.pangker.util.Util.getClassName();// log tag
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();

	private File file;

	private TextView tvTitle;
	private EditText tvAuthor;
	private EditText etFileName;
	private TextView txtNameCount;
	private Button btnClear;
	private TextView txtFileSize; // 文件大小
	private EditText etDetail; // 文件描述
	private TextView txtDetailCount; //

	private ImageView iv_fileicon; // 文件图标
	private CheckBox cbIfShare;
	private LinearLayout typeLayout;// 类别
	private Button btnType;
	private TextView txtType;
	private String[] fileTypeKeys;
	private String[] fileTypeCodes;

	private LinearLayout selectLayout;
	private LinearLayout uploadinfo;
	private LinearLayout ly_dirinfo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.res_upload_apk);

		init();
		initView();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(this);
		if (id == 1) {
			builder.setSingleChoiceItems(fileTypeKeys, 0, dlistener);
			return builder.create();
		}
		return super.onCreateDialog(id);
	}

	DialogInterface.OnClickListener dlistener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			txtType.setText(fileTypeKeys[which]);
			uploadType = fileTypeCodes[which];
			dialog.dismiss();
		}
	};

	private void init() {
		userId = application.getMyUserID();
		filePath = getIntent().getStringExtra(DocInfo.FILE_PATH);
		fileTypeKeys = getResources().getStringArray(R.array.res_doc_type_key);
		fileTypeCodes = getResources().getStringArray(R.array.res_doc_type_value);
		uploadType = fileTypeCodes[0];
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		hideInputMethod(etFileName);
		super.onBackPressed();
	}

	private void initView() {
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
						UploadDocActivity.this.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
				UploadDocActivity.this.onBackPressed();
			}
		});
		TextView author = (TextView) findViewById(R.id.res_author);
		author.setVisibility(View.VISIBLE);
		author.setText("作者：");
		tvAuthor = (EditText) findViewById(R.id.tv_author_name);
		tvAuthor.setVisibility(View.VISIBLE);
		tvTitle = (TextView) findViewById(R.id.mmtitle);
		if (!Util.isEmpty(groupId)) {
			tvTitle.setText("群组-上传阅读资源");
		} else {
			tvTitle.setText("上传阅读资源");
		}
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		btnUpload = (Button) findViewById(R.id.btn_upload);
		// btnUpload.setTextColor(R.color.white);
		btnUpload.setOnClickListener(mClickListener);

		txtFileSize = (TextView) findViewById(R.id.tv_app_size);
		etDetail = (EditText) findViewById(R.id.et_apk_detail);
		etDetail.setText(R.string.res_upload_file_detail);
		txtDetailCount = (TextView) findViewById(R.id.txt_detail_count);

		btnClear = (Button) findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(mClickListener);

		iv_fileicon = (ImageView) findViewById(R.id.iv_apkicon);
		iv_fileicon.setImageResource(R.drawable.attachment_doc);
		cbIfShare = (CheckBox) findViewById(R.id.cb_ifshare);
		cbIfShare.setChecked(true);

		TextView txtLabel = (TextView) findViewById(R.id.tv_label);
		txtLabel.setText("文档类别:");
		typeLayout = (LinearLayout) findViewById(R.id.type_layout);
		typeLayout.setOnClickListener(mClickListener);
		txtType = (TextView) findViewById(R.id.tv_type);
		txtType.setHint(fileTypeKeys[0]);
		btnType = (Button) findViewById(R.id.btn_type_select);
		btnType.setOnClickListener(mClickListener);

		selectLayout = (LinearLayout) findViewById(R.id.selectLayout);
		uploadinfo = (LinearLayout) findViewById(R.id.uploadinfo);//
		ly_dirinfo = (LinearLayout) findViewById(R.id.ly_dirinfo);

		selectLayout.setOnClickListener(mClickListener);
		uploadinfo.setOnClickListener(mClickListener);
		ly_dirinfo.setOnClickListener(mClickListener);
		upoladLayout = (LinearLayout) findViewById(R.id.uploadLayout);
		upoladLayout.setOnClickListener(mClickListener);

		txtUpload = (TextView) findViewById(R.id.txt_upload);
		txtUpload.setText("点击选择本地阅读文件");
		txtDirName = (TextView) findViewById(R.id.txtDirname);
		txtNameCount = (TextView) findViewById(R.id.txt_name_count);
		etFileName = (EditText) findViewById(R.id.et_file_name);
		etFileName.addTextChangedListener(new TextLimitWatcher(MaxNameLenght, txtNameCount, etFileName));

		groupLayout = (LinearLayout) findViewById(R.id.groupLayout);
		txtGroupName = (TextView) findViewById(R.id.txt_groupname);
		if (!Util.isEmpty(groupId)) {
			groupLayout.setVisibility(View.VISIBLE);
			txtGroupName.setText(groupName);
		}
		cbTerms = (CheckBox) findViewById(R.id.checkbox_item);
		cbTerms.setOnCheckedChangeListener(new CheckBoxChangedListener(btnUpload, promptDialog));
		initUploadButton();
		tvAgreement = (TextView) findViewById(R.id.tv_agreement);
		tvAgreement.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(UploadDocActivity.this, AgreementActivity.class);
				intent.putExtra(AgreementActivity.START_FLAG, AgreementActivity.UPLOAD_RES);
				UploadDocActivity.this.startActivity(intent);
			}
		});
		if (!Util.isEmpty(filePath)) {
			initData();
		}
		initDir();

	}

	private void initData() {

		file = new File(filePath);
		if (file == null || !file.exists()) {
			showToast(R.string.res_get_file_failed);
			return;
		}
		// TODO Auto-generated method stub
		findViewById(R.id.size_layout).setVisibility(View.VISIBLE);
		etFileName.setText(file.getName());
		String strCount = etFileName.getEditableText().toString().trim();
		if (strCount.length() > MaxNameLenght) {
			this.etFileName.setText(strCount.substring(0, MaxNameLenght));
		}
		txtUpload.setText(etFileName.getText());

		String fileSize = Formatter.formatFileSize(this, file.length());
		txtFileSize.setText(fileSize);

		if (file.getName().contains(".txt")) {
			iv_fileicon.setImageResource(R.drawable.attachment_doc);
		} else {
			iv_fileicon.setImageResource(R.drawable.attachment_doc);
		}

	}

	private void initDir() {
		// TODO Auto-generated method stub
		sDirInfo = application.getRootDir(PangkerConstant.RES_DOCUMENT);
		if (sDirInfo != null) {
			selectDirId = sDirInfo.getDirId();
		} else {
			PangkerManager.getTabBroadcastManager().addMsgListener(this);
			initDirService();
		}
		String dirName = sDirInfo != null ? sDirInfo.getDirNames() : "请获取目录";
		txtDirName.setText(dirName);
	}

	/**
	 * 上传资源接口
	 * 
	 * @param filepath
	 *            文件路径
	 * @param isFileExist
	 *            文件是否已经存在服务器 接口：ResUpload.json
	 */
	protected void uploadFile(String filepath, int job_type) {
		try {
			File file = new File(filepath);
			String fileName = file.getName();//
			String resName = etFileName.getText().toString();
			if (Util.isEmpty(resName)) {
				resName = toFile(fileName); // 缩写的名字
			}
			String author = tvAuthor.getText().toString().trim();// 文档作者
			String desc = etDetail.getText().toString();
			String ifshare = cbIfShare.isChecked() ? "1" : "0";
			uploaderFile(PangkerConstant.RES_DOCUMENT, desc, ifshare, resName, "txt", author, 0, job_type);
			onBackPressed();
		} catch (Exception e) {
			logger.error(TAG, e);
			showToast(R.string.to_server_fail);
		}
	}

	private void upoadDoc() {
		if (Util.isEmpty(etFileName.getEditableText().toString())) {
			showToast("请输入文档名称!");
			return;
		}
		if (Util.isEmpty(selectDirId)) {
			showToast("请选择一个目录!");
			return;
		}
		uploadCheck(PangkerConstant.RES_DOCUMENT);
	}

	View.OnClickListener mClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_upload:
				if (!uploadCheck()) {
					return;
				}
				if (filePath != null) {
					upoadDoc();
				} else {
					showToast("请选择本地文本文件!");
				}
				break;
			case R.id.btn_cancel:
				UploadDocActivity.this.finish();
				break;
			case R.id.selectLayout:
			case R.id.ly_dirinfo:
				selectDirInfos(PangkerConstant.RES_DOCUMENT);
				break;
			case R.id.uploadLayout:
			case R.id.uploadinfo:
				Intent intent = new Intent();
				intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.change);
				intent.setClass(UploadDocActivity.this, ResLocalDocActivity.class);
				startActivityForResult(intent, CODE_RESELECT);
				break;
			case R.id.btn_clear:
				etFileName.setText("");
				break;
			case R.id.type_layout:
				showDialog(1);
				break;
			case R.id.btn_type_select:
				showDialog(1);
				break;
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == CODE_RESELECT && resultCode == RESULT_OK) {
			filePath = intent.getStringExtra(DocInfo.FILE_PATH);
			initData();
		}
	}

	public static String toFile(String name) {
		int search = name.length();
		if (name.toLowerCase().endsWith(".doc")) {
			search = name.indexOf(".doc");
		} else if (name.toLowerCase().endsWith(".docx")) {
			search = name.indexOf(".docx");
		} else if (name.toLowerCase().endsWith(".txt")) {
			search = name.indexOf(".txt");
		} else if (name.toLowerCase().endsWith(".xls")) {
			search = name.indexOf(".xls");
		} else if (name.toLowerCase().endsWith(".xlsx")) {
			search = name.indexOf(".xlsx");
		} else if (name.toLowerCase().endsWith(".ppt")) {
			search = name.indexOf(".ppt");
		} else if (name.toLowerCase().endsWith(".pptx")) {
			search = name.indexOf(".pptx");
		} else if (name.toLowerCase().endsWith(".pdf")) {
			search = name.indexOf(".pdf");
		} else if (name.toLowerCase().endsWith(".chm")) {
			search = name.indexOf(".chm");
		} else if (name.toLowerCase().endsWith(".umd")) {
			search = name.indexOf(".umd");
		}

		String newName = name.substring(0, search);
		return newName;
	}
	
	@Override
	protected void onLoaderGeo(String address) {
		
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				initDir();
			}
		};
		handler.sendMessage(msg);
	}

}
