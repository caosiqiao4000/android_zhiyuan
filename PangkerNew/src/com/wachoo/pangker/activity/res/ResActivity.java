package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.MusicListAdapter;
import com.wachoo.pangker.adapter.ResAppAdapter;
import com.wachoo.pangker.adapter.ResNetPicAdapter;
import com.wachoo.pangker.adapter.ResReadListAdapter;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.response.AppInfo;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.server.response.MovingRes;
import com.wachoo.pangker.server.response.MusicInfo;
import com.wachoo.pangker.server.response.PicInfo;
import com.wachoo.pangker.server.response.ResShareResult;
import com.wachoo.pangker.server.response.SearchLbsAppResult;
import com.wachoo.pangker.server.response.SearchLbsDocResult;
import com.wachoo.pangker.server.response.SearchLbsMusicResult;
import com.wachoo.pangker.server.response.SearchLbsPictureResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.Mode;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.QuickAction.OnActionItemClickListener;
import com.wachoo.pangker.ui.QuickAction.OnDismissListener;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

public class ResActivity extends ResSearchActivity implements IUICallBackInterface {

	private static final String TAG = "ResActivity";// log tag
	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	public RadioGroup rg_res;

	private PullToRefreshListView relv_doc;// 图书容器
	private PullToRefreshListView relv_music;// 音乐容器
	private PullToRefreshListView relv_pic;// 图片容器
	private PullToRefreshListView relv_app;// 应用容器

	/** 搜索区间 **/
	private static final int incremental = 15;
	// private static final int pic_incremental = 9;
	private int doc_startLimit, doc_endLimit = 0;
	private int music_startLimit, music_endLimit = 0;
	private int pic_startLimit, pic_endLimit = 0;
	private int app_startLimit, app_endLimit = 0;
	/** 搜索区间 **/

	/** 搜索工具栏 **/
	private View music_search_bar;
	private View pic_search_bar;
	private View app_search_bar;
	private View doc_search_bar;

	private final int[] RES_MENU_INDEXS = { 0x100, 0x101, 0x102};
	private final String[] RES_MENU_TITLE = { "行人分享", "好友分享", "关注人分享"};
	private LinearLayout doclayoutTitle;
	private ImageView dociconDrow;
	private RelativeLayout docTitleTopLayout;
	private TextView doc_tvTitle;

	private LinearLayout doclayoutTitle_nodata;
	private ImageView dociconDrow_nodata;
	private RelativeLayout docTitleTopLayout_nodata;
	private TextView doc_tvTitle_nodata;

	private LinearLayout musiclayoutTitle;
	private ImageView musiciconDrow;
	private RelativeLayout musicTitleTopLayout;
	private TextView music_tvTitle;

	private LinearLayout musiclayoutTitle_nodata;
	private ImageView musiciconDrow_nodata;
	private RelativeLayout musicTitleTopLayout_nodata;
	private TextView music_tvTitle_nodata;

	private LinearLayout piclayoutTitle;
	private ImageView piciconDrow;
	private RelativeLayout picTitleTopLayout;
	private TextView pic_tvTitle;

	private LinearLayout piclayoutTitle_nodata;
	private ImageView piciconDrow_nodata;
	private RelativeLayout picTitleTopLayout_nodata;
	private TextView pic_tvTitle_nodata;

	private LinearLayout applayoutTitle;
	private ImageView appiconDrow;
	private RelativeLayout appTitleTopLayout;
	private TextView app_tvTitle;

	private LinearLayout applayoutTitle_nodata;
	private ImageView appiconDrow_nodata;
	private RelativeLayout appTitleTopLayout_nodata;
	private TextView app_tvTitle_nodata;

	private QuickAction docpopQuickAction;

	private Button btn_app_order;
	private Button btn_app_order_nodata;
	private TextView tv_app_sorttype;
	private String str_app_search; // 搜索框

	private Button btn_doc_order;
	private TextView tv_doc_sorttype;
	private Button btn_doc_order_nodata;
	private String str_doc_search; // 搜索框

	private Button btn_music_order;
	private Button btn_music_order_nodata;
	private TextView tv_music_sorttype;
	private String str_music_search; // 搜索框

	private Button btn_pic_order;
	private Button btn_pic_order_nodata;
	private TextView tv_pic_sorttype;
	private String str_pic_search; // 搜索框
	/** 搜索工具栏 **/

	/** 加载更多按钮 **/
	private View doc_loadmore;
	private View pic_loadmore;
	private View app_loadmore;
	private View music_loadmore;

	private View empty_view_pic;
	private View empty_view_doc;
	private View empty_view_app;
	private View empty_view_music;

	private ImageView iv_empty_icon_doc;
	private ImageView iv_empty_icon_music;
	private ImageView iv_empty_icon_pic;
	private ImageView iv_empty_icon_app;

	private TextView doc_TextViewEmpty;
	private TextView music_TextViewEmpty;
	private TextView pic_TextViewEmpty;
	private TextView app_TextViewEmpty;

	private LinearLayout ly_docLoadPressBar;
	private LinearLayout ly_docLoadText;
	private LinearLayout ly_appLoadPressBar;
	private LinearLayout ly_appLoadText;
	private LinearLayout ly_musicLoadPressBar;
	private LinearLayout ly_musicLoadText;
	private LinearLayout ly_picLoadPressBar;
	private LinearLayout ly_picLoadText;

	private TextView tv_docLoadText;
	private TextView tv_musicLoadText;
	private TextView tv_picLoadText;
	private TextView tv_appLoadText;

	private Button btnDocSearch;
	private Button btnMusSearch;
	private Button btnPicSearch;
	private Button btnAppSearch;

	private Button btnDocSearchEmpty;
	private Button btnMusSearchEmpty;
	private Button btnPicSearchEmpty;
	private Button btnAppSearchEmpty;
	/** 加载更多按钮 **/

	/** 是否还有更多数据 **/
	private boolean doc_NoHaveMoreData = false;
	private boolean music_NoHaveMoreData = false;
	private boolean pic_NoHaveMoreData = false;
	private boolean app_NoHaveMoreData = false;

	/** 是否还有更多数据 **/

	private ListView lv_doc; // 阅读资源列表
	private ListView lv_music; // 音乐资源列表
	private ListView lv_pic;// 图片容器
	private ListView lv_app;// 应用容器

	private ResReadListAdapter docAdapter; // 阅读资源数据源
	private MusicListAdapter musicListAdapter; // 音乐资源
	private ResNetPicAdapter picAdapter;// 图片资源
	private ResAppAdapter appAdapter; // 应用资源

	// >>>>>>获取图片
	private ImageResizer mImageWorker;
	private ImageCache mImageCache;

	private static final String IMAGE_CACHE_DIR = "thumbs";
	// 网络加载图片使用 end

	private List<DocInfo> allListFiles = new ArrayList<DocInfo>();
	private List<MusicInfo> allMusicList = new ArrayList<MusicInfo>();
	private List<PicInfo> photoList = new ArrayList<PicInfo>();
	private List<AppInfo> appList = new ArrayList<AppInfo>();

	private static final int RES_DOC_QUERY = 0;
	private static final int RES_PIC_QUERY = 1;
	private static final int RES_MUSIC_QUERY = 2;
	private static final int RES_APP_QUERY = 3;
	private static final int FAVORITE = 0x102;
	private static final int SAVE_TO_ME = 0x101;

	private int view_type;// 当前显示的数据源
	// 排序用的key
	private String doc_orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
	private String music_orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
	private String pic_orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
	private String app_orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
	// 排序用的key
	private String doc_searchtype = PangkerConstant.SEARCH_RANGE_DATA;
	private String music_searchtype = PangkerConstant.SEARCH_RANGE_DATA;
	private String app_searchtype = PangkerConstant.SEARCH_RANGE_DATA;
	private String pic_searchtype = PangkerConstant.SEARCH_RANGE_DATA;

	private String userID;
	private PangkerApplication myApplication;

	private LayoutInflater inflater;
	// private Dialog searchDialog;
	private PopMenuDialog menuDialog;

	// add by lb
	private int doc_title_index = RES_MENU_INDEXS[0];
	private int pic_title_index = RES_MENU_INDEXS[0];
	private int music_title_index = RES_MENU_INDEXS[0];
	private int app_title_index = RES_MENU_INDEXS[0];
	private SharedPreferencesUtil sp_Util;
	private int pos;
	private long currentTime = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sp_Util = new SharedPreferencesUtil(this);
		init();
		initView();
		// initDialog();

		int resIndex = sp_Util.getInt(TAG, 0);
		switch (resIndex) {
		case 1:
			rg_res.check(R.id.res_music);
			initMusic();
			break;
		case 2:
			rg_res.check(R.id.res_read);
			initDocument();
			break;
		case 3:
			rg_res.check(R.id.res_app);
			initApp();
			break;
		default:
			rg_res.check(R.id.res_pic);
			initPic();
			pos = sp_Util.getInt(PangkerConstant.SP_PIC_KEY, 0);
			loadResData();
			break;
		}
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC); // 让音量键固定为媒体音量控制
	}

	private void loadResData() {
		showTitie(pos);
		if (RES_MENU_INDEXS[pos] == RES_MENU_INDEXS[0]) {// 行人
			cleanSearchString();
			searchAllData(String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC), PangkerConstant.SEARCH_RANGE_DATA);
		} else if (RES_MENU_INDEXS[pos] == RES_MENU_INDEXS[1]) {// 好友
			searchContactsData("0");
		} else if (RES_MENU_INDEXS[pos] == RES_MENU_INDEXS[2]) {// 关注人
			searchContactsData("1");
		} else if (RES_MENU_INDEXS[pos] == RES_MENU_INDEXS[3]) {// 全网最新
			searchResData();
		} else if (RES_MENU_INDEXS[pos] == RES_MENU_INDEXS[4]) {// 全网最热
			searchResData();
		}
	}

	// @Override
	// protected void onStart() {
	// // TODO Auto-generated method stub
	// super.onStart();
	//
	// addGuideImage(ResActivity.class.getName(), R.drawable.inf_01);
	//
	// }

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// >>>>>>>>返回的时候设置按钮
	}

	private void init() {
		// TODO Auto-generated method stub
		inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		view_type = R.id.res_pic;
		myApplication = (PangkerApplication) getApplication();
		userID = myApplication.getMyUserID();

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);

		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);

		popopQuickAction = new QuickAction(this, QuickAction.ANIM_GROW_FROM_LEFT);
		popopQuickAction.setOnActionItemClickListener(onActionItemClickListener);
		for (int i = 0; i < MENU_INDEXS.length; i++) {
			if (i == MENU_INDEXS.length - 1) {
				popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[i], MENU_TITLE[i]), true);
			} else {
				popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[i], MENU_TITLE[i]));
			}
		}

		docpopQuickAction = new QuickAction(this, QuickAction.ANIM_GROW_FROM_CENTER);
		docpopQuickAction.setOnActionItemClickListener(onActionItemClickListener2);
		docpopQuickAction.setOnDismissListener(onDismissListener);
		for (int i = 0; i < RES_MENU_INDEXS.length; i++) {
			if (i == RES_MENU_INDEXS.length - 1) {
				docpopQuickAction.addActionItem(new ActionItem(RES_MENU_INDEXS[i], RES_MENU_TITLE[i]), true);
			} else {
				docpopQuickAction.addActionItem(new ActionItem(RES_MENU_INDEXS[i], RES_MENU_TITLE[i]));
			}
		}
	}

	private void initView() {
		setContentView(R.layout.res);

		// 文档资源
		relv_doc = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_doc);
		relv_doc.setMode(Mode.PULL_DOWN_TO_REFRESH);
		relv_doc.setOnRefreshListener(onRefreshListener);
		relv_doc.setTag("pk_doc");

		// 音乐资源
		relv_music = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_music);
		relv_music.setMode(Mode.PULL_DOWN_TO_REFRESH);
		relv_music.setOnRefreshListener(onRefreshListener);
		relv_music.setTag("pk_music");

		// 图片资源
		relv_pic = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_pic);
		relv_pic.setMode(Mode.PULL_DOWN_TO_REFRESH);
		relv_pic.setOnRefreshListener(onRefreshListener);
		relv_pic.setTag("pk_pic");

		// 应用资源
		relv_app = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_app);
		relv_app.setMode(Mode.PULL_DOWN_TO_REFRESH);
		relv_app.setTag("pk_app");
		relv_app.setOnRefreshListener(onRefreshListener);

		rg_res = (RadioGroup) findViewById(R.id.res_rg);

		rg_res.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				currentTime = System.currentTimeMillis() - currentTime;
				if (currentTime > 0 && currentTime < 500) {
					return;
				}
				view_type = checkedId;
				if (checkedId == R.id.res_read) {
					initDocument();
					// queryLbsDoc(str_doc_search);
					if (docAdapter == null || docAdapter.isEmpty()) {
						pos = sp_Util.getInt(PangkerConstant.SP_DOC_KEY, 0);
						loadResData();
					}
				} else if (checkedId == R.id.res_pic) {
					initPic();
					// queryLbsPic(str_pic_search);
					if (picAdapter == null || picAdapter.isEmpty()) {
						pos = sp_Util.getInt(PangkerConstant.SP_PIC_KEY, 0);
						loadResData();
					}

				} else if (checkedId == R.id.res_app) {
					initApp();
					// queryLbsApp(str_app_search);
					if (appAdapter == null || appAdapter.isEmpty()) {
						pos = sp_Util.getInt(PangkerConstant.SP_APP_KEY, 0);
						loadResData();
					}

				} else if (checkedId == R.id.res_music) {
					initMusic();
					// queryLbsMusic(str_music_search);
					if (musicListAdapter == null || musicListAdapter.isEmpty()) {
						pos = sp_Util.getInt(PangkerConstant.SP_MUSIC_KEY, 0);
						loadResData();
					}

				}
			}
		});

	}

	private void showTitie(int pos) {
		// TODO Auto-generated method stub
		switch (view_type) {
		case R.id.res_read:
			doc_title_index = RES_MENU_INDEXS[pos];
			doc_tvTitle.setText(RES_MENU_TITLE[pos]);
			doc_tvTitle_nodata.setText(RES_MENU_TITLE[pos]);
			btn_doc_order.setEnabled(true);
			dociconDrow.setImageResource(R.drawable.icon32_arrow);
			sp_Util.saveInt(PangkerConstant.SP_DOC_KEY, pos);
			break;
		case R.id.res_music:
			music_title_index = RES_MENU_INDEXS[pos];
			music_tvTitle.setText(RES_MENU_TITLE[pos]);
			music_tvTitle_nodata.setText(RES_MENU_TITLE[pos]);
			btn_music_order.setEnabled(true);
			musiciconDrow.setImageResource(R.drawable.icon32_arrow);
			sp_Util.saveInt(PangkerConstant.SP_MUSIC_KEY, pos);
			break;
		case R.id.res_pic:
			pic_title_index = RES_MENU_INDEXS[pos];
			pic_tvTitle.setText(RES_MENU_TITLE[pos]);
			pic_tvTitle_nodata.setText(RES_MENU_TITLE[pos]);
			btn_pic_order.setEnabled(true);
			piciconDrow.setImageResource(R.drawable.icon32_arrow);
			sp_Util.saveInt(PangkerConstant.SP_PIC_KEY, pos);
			break;
		case R.id.res_app:
			app_title_index = RES_MENU_INDEXS[pos];
			app_tvTitle.setText(RES_MENU_TITLE[pos]);
			app_tvTitle_nodata.setText(RES_MENU_TITLE[pos]);
			btn_app_order.setEnabled(true);
			appiconDrow.setImageResource(R.drawable.icon32_arrow);
			sp_Util.saveInt(PangkerConstant.SP_APP_KEY, pos);
			break;
		}
		// 更新排序，使之成为默认的排序
		doc_orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
		music_orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
		pic_orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
		app_orderkey = String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC);
	}

	OnDismissListener onDismissListener = new OnDismissListener() {
		@Override
		public void onDismiss() {
			// TODO Auto-generated method stub
			switch (view_type) {
			case R.id.res_read:
				dociconDrow.setImageResource(R.drawable.icon32_arrow);
				break;
			case R.id.res_music:
				musiciconDrow.setImageResource(R.drawable.icon32_arrow);
				break;
			case R.id.res_pic:
				piciconDrow.setImageResource(R.drawable.icon32_arrow);
				break;
			case R.id.res_app:
				appiconDrow.setImageResource(R.drawable.icon32_arrow);
				break;
			}
		}
	};

	private void cleanSearchString() {
		switch (view_type) {
		case R.id.res_read:
			str_doc_search = "";
			break;
		case R.id.res_music:
			str_music_search = "";
			break;
		case R.id.res_pic:
			str_pic_search = "";
			break;
		case R.id.res_app:
			str_app_search = "";
			break;
		}
	}

	/**
	 * 搜索类型监听
	 */
	OnActionItemClickListener onActionItemClickListener2 = new OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			// TODO Auto-generated method stub
			showTitie(pos);
			if (actionId == RES_MENU_INDEXS[0]) {// 行人
				cleanSearchString();
				searchAllData(String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC),
						PangkerConstant.SEARCH_RANGE_DATA);
			} else if (actionId == RES_MENU_INDEXS[1]) {// 好友
				searchContactsData("0");
			} else if (actionId == RES_MENU_INDEXS[2]) {// 关注人
				searchContactsData("1");
			}
		}
	};

	// private void initDialog() {
	// // TODO Auto-generated method stub
	// AlertDialog.Builder builder = new AlertDialog.Builder(this);
	// et_searchKey = new EditText(getParent());
	// et_searchKey.addTextChangedListener(new TextCountLimitWatcher(20,
	// et_searchKey));
	// builder.setView(et_searchKey);
	// builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// switch (view_type) {
	// case R.id.res_read:
	// str_doc_search = et_searchKey.getText().toString().trim();
	// break;
	// case R.id.res_music:
	// str_music_search = et_searchKey.getText().toString().trim();
	// break;
	// case R.id.res_pic:
	// str_pic_search = et_searchKey.getText().toString().trim();
	// break;
	// case R.id.res_app:
	// str_app_search = et_searchKey.getText().toString().trim();
	// break;
	// default:
	// break;
	// }
	// searchAllData(String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC),
	// PangkerConstant.SEARCH_ALL_DATA);
	// dialog.dismiss();
	// }
	// });
	// builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// dialog.dismiss();
	// }
	// });
	// searchDialog = builder.create();
	// }

	/**
	 * 搜索全部数据
	 */
	private void searchAllData(String orderType, String searchType) {
		switch (view_type) {
		case R.id.res_read:
			if (searchType != null)
				doc_searchtype = searchType;
			doc_orderkey = orderType;
			setResOrderText(tv_doc_sorttype, Integer.parseInt(orderType));
			break;
		case R.id.res_music:
			if (searchType != null)
				music_searchtype = searchType;
			music_orderkey = orderType;
			setResOrderText(tv_music_sorttype, Integer.parseInt(orderType));
			break;
		case R.id.res_pic:
			if (searchType != null)
				pic_searchtype = searchType;
			pic_orderkey = orderType;
			setResOrderText(tv_pic_sorttype, Integer.parseInt(orderType));
			break;
		case R.id.res_app:
			if (searchType != null)
				app_searchtype = searchType;
			app_orderkey = orderType;
			setResOrderText(tv_app_sorttype, Integer.parseInt(orderType));
			break;
		default:
			break;
		}
		searchResData();
	}

	private void setResOrderText(TextView textView, int orderType) {
		// public static final int SORT_BY_CREATETIME_DESC = 0;// 根据上传时间倒序排列
		// public static final int SORT_BY_SCORE_DESC = 1;// 得分排序，高分在前，低分在后
		// public static final int SORT_BY_COMMENTTIME_DESC = 2;// 评论次数 多在前，少在后
		// public static final int SORT_BY_DOWNLOADTIMES_DESC = 3;// 下载次数
		switch (orderType) {
		case PangkerConstant.SORT_BY_CREATETIME_DESC:
			textView.setText(R.string.res_sort_time);
			break;
		case PangkerConstant.SORT_BY_SCORE_DESC:
			textView.setText(R.string.res_sort_hot);
			break;
		case PangkerConstant.SORT_BY_COMMENTTIME_DESC:
			textView.setText(R.string.res_sort_praise);
			break;
		case PangkerConstant.SORT_BY_DOWNLOADTIMES_DESC:
			textView.setText(R.string.res_sort_downloadtimes);
			break;
		default:
			break;
		}

	}

	/**
	 * 搜索联系人数据
	 */
	private void searchContactsData(String contactType) {
		// TODO Auto-generated method stub
		int search_type = 0;
		String sorderkey = "";
		switch (view_type) {
		case R.id.res_read:
			search_type = RES_DOC_QUERY;
			sorderkey = doc_orderkey;
			doc_startLimit = 0;
			doc_NoHaveMoreData = false;
			tv_docLoadText.setText(R.string.loadmore);
			break;
		case R.id.res_music:
			sorderkey = music_orderkey;
			music_startLimit = 0;
			search_type = RES_MUSIC_QUERY;
			music_NoHaveMoreData = false;
			tv_musicLoadText.setText(R.string.loadmore);
			break;
		case R.id.res_pic:
			sorderkey = pic_orderkey;
			pic_startLimit = 0;
			search_type = RES_PIC_QUERY;
			pic_NoHaveMoreData = false;
			tv_picLoadText.setText(R.string.loadmore);
			break;
		case R.id.res_app:
			app_NoHaveMoreData = false;
			app_startLimit = 0;
			search_type = RES_APP_QUERY;
			sorderkey = app_orderkey;
			tv_appLoadText.setText(R.string.loadmore);
			break;
		}

		queryContactsRes(search_type, contactType, sorderkey);
	}

	/**
	 * 加载网络文档数据
	 */
	private void initDocument() {
		// TODO Auto-generated method stub
		sp_Util.saveInt(TAG, 2);
		if (lv_doc == null) {
			lv_doc = relv_doc.getRefreshableView();
			lv_doc.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					selectItem = (DocInfo) parent.getItemAtPosition(position);
					intoDocInfo(position - 2);
				}
			});
			lv_doc.setOnItemLongClickListener(new OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					selectItem = (MovingRes) parent.getItemAtPosition(position);
					showPopMenu();
					return true;
				}
			});

			/**
			 * 文档搜索栏
			 */
			doc_search_bar = inflater.inflate(R.layout.restitle_bar, null);
			btnDocSearch = (Button) doc_search_bar.findViewById(R.id.btn_search);
			btnDocSearch.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// showDialogs();
					Intent mIntent = new Intent(ResActivity.this, NetResSearchActivity.class);
					mIntent.putExtra("resType", RES_DOC_QUERY_TYPE);
					startActivity(mIntent);

				}
			});
			btn_doc_order = (Button) doc_search_bar.findViewById(R.id.btn_left);
			tv_doc_sorttype = (TextView) doc_search_bar.findViewById(R.id.tv_res_sorttype);
			doclayoutTitle = (LinearLayout) doc_search_bar.findViewById(R.id.layout_title);
			doclayoutTitle.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					docpopQuickAction.show(docTitleTopLayout);
					dociconDrow.setImageResource(R.drawable.icon32_arrow_p);
				}
			});

			dociconDrow = (ImageView) doc_search_bar.findViewById(R.id.icon_drow);
			dociconDrow.setImageResource(R.drawable.icon32_arrow);
			doc_tvTitle = (TextView) doc_search_bar.findViewById(R.id.tv_title);
			doc_tvTitle.getPaint().setFakeBoldText(true);
			docTitleTopLayout = (RelativeLayout) doc_search_bar.findViewById(R.id.title_top_ly);

			btn_doc_order.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showPopopQuickAction(btn_doc_order);
				}
			});

			lv_doc.addHeaderView(doc_search_bar);
			initDocumentBar();
			docAdapter = new ResReadListAdapter(ResActivity.this, allListFiles);
			lv_doc.setAdapter(docAdapter);

		}

		relv_doc.setVisibility(View.VISIBLE);
		relv_music.setVisibility(View.GONE);
		relv_pic.setVisibility(View.GONE);
		relv_app.setVisibility(View.GONE);
	}

	/**
	 * 加载title
	 */
	private void initDocumentBar() {
		// 无数据的title
		empty_view_doc = LayoutInflater.from(this).inflate(R.layout.res_empty_view, null, false);
		View title_bar_divider = (View) empty_view_doc.findViewById(R.id.title_bar_divider);
		title_bar_divider.setVisibility(View.VISIBLE);
		btn_doc_order_nodata = (Button) empty_view_doc.findViewById(R.id.btn_left);
		btnDocSearchEmpty = (Button) empty_view_doc.findViewById(R.id.btn_search);
		btnDocSearchEmpty.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showDialogs();
				Intent mIntent = new Intent(ResActivity.this, NetResSearchActivity.class);
				mIntent.putExtra("resType", RES_DOC_QUERY_TYPE);
				startActivity(mIntent);
			}
		});

		doclayoutTitle_nodata = (LinearLayout) empty_view_doc.findViewById(R.id.layout_title);
		doclayoutTitle_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				docpopQuickAction.show(docTitleTopLayout_nodata);
				dociconDrow.setImageResource(R.drawable.icon32_arrow_p);
			}
		});
		dociconDrow_nodata = (ImageView) empty_view_doc.findViewById(R.id.icon_drow);
		dociconDrow_nodata.setImageResource(R.drawable.icon32_arrow);
		doc_tvTitle_nodata = (TextView) empty_view_doc.findViewById(R.id.tv_title);
		doc_tvTitle_nodata.getPaint().setFakeBoldText(true);
		docTitleTopLayout_nodata = (RelativeLayout) empty_view_doc.findViewById(R.id.title_top_ly);

		btn_doc_order_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopopQuickAction(btn_doc_order_nodata);
			}
		});

		iv_empty_icon_doc = (ImageView) empty_view_doc.findViewById(R.id.iv_empty_icon);
		iv_empty_icon_doc.setImageDrawable(null);
		doc_TextViewEmpty = (TextView) empty_view_doc.findViewById(R.id.textViewEmpty);
		doc_TextViewEmpty.setText("");
		lv_doc.setEmptyView(empty_view_doc);

		/**
		 * 加载更多功能栏
		 */
		doc_loadmore = inflater.inflate(R.layout.loadmore_footerview, null);
		doc_loadmore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (ly_docLoadPressBar.getVisibility() == View.GONE && ly_docLoadText.getVisibility() == View.VISIBLE) {
					if (doc_NoHaveMoreData) {
						tv_docLoadText.setText(R.string.nomore_data);
					} else {
						if (doc_title_index == RES_MENU_INDEXS[0]) {
							queryLbsDoc(str_doc_search);
						} else if (doc_title_index == RES_MENU_INDEXS[1]) {
							queryContactsRes(RES_DOC_QUERY, "0", app_orderkey);
						} else if (doc_title_index == RES_MENU_INDEXS[2]) {
							queryContactsRes(RES_DOC_QUERY, "1", app_orderkey);
						}
					}
				}
			}
		});
		lv_doc.addFooterView(doc_loadmore);
		ly_docLoadPressBar = (LinearLayout) doc_loadmore.findViewById(R.id.ly_loadmore_probar);
		ly_docLoadText = (LinearLayout) doc_loadmore.findViewById(R.id.ly_loadmore_text);
		tv_docLoadText = (TextView) doc_loadmore.findViewById(R.id.tv_loadmore);
	}

	private void initMusic() {
		sp_Util.saveInt(TAG, 1);
		if (lv_music == null) {
			lv_music = relv_music.getRefreshableView();
			lv_music.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					selectItem = (MusicInfo) parent.getItemAtPosition(position);
					intoMusicInfo((MusicInfo) selectItem, position - 2);
				}
			});
			lv_music.setOnItemLongClickListener(new OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					selectItem = (MovingRes) parent.getItemAtPosition(position);
					showPopMenu();
					return true;
				}
			});

			initMusicBar();

		}

		relv_doc.setVisibility(View.GONE);
		relv_music.setVisibility(View.VISIBLE);
		relv_pic.setVisibility(View.GONE);
		relv_app.setVisibility(View.GONE);
	}

	private void initMusicBar() {
		// TODO Auto-generated method stub
		/**
		 * 搜索栏
		 */
		music_search_bar = inflater.inflate(R.layout.restitle_bar, null);
		btn_music_order = (Button) music_search_bar.findViewById(R.id.btn_left);
		btnMusSearch = (Button) music_search_bar.findViewById(R.id.btn_search);
		btnMusSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showDialogs();
				Intent mIntent = new Intent(ResActivity.this, NetResSearchActivity.class);
				mIntent.putExtra("resType", RES_MUS_QUERY_TYPE);
				startActivity(mIntent);
			}
		});

		musiclayoutTitle = (LinearLayout) music_search_bar.findViewById(R.id.layout_title);
		musiclayoutTitle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				docpopQuickAction.show(musicTitleTopLayout);
				musiciconDrow.setImageResource(R.drawable.icon32_arrow_p);
			}
		});
		tv_music_sorttype = (TextView) music_search_bar.findViewById(R.id.tv_res_sorttype);
		musiciconDrow = (ImageView) music_search_bar.findViewById(R.id.icon_drow);
		musiciconDrow.setImageResource(R.drawable.icon32_arrow);
		music_tvTitle = (TextView) music_search_bar.findViewById(R.id.tv_title);
		music_tvTitle.getPaint().setFakeBoldText(true);
		musicTitleTopLayout = (RelativeLayout) music_search_bar.findViewById(R.id.title_top_ly);

		btn_music_order.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopopQuickAction(btn_doc_order);
			}
		});

		btn_music_order.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopopQuickAction(btn_music_order);
			}
		});
		lv_music.addHeaderView(music_search_bar);

		empty_view_music = LayoutInflater.from(this).inflate(R.layout.res_empty_view, null, false);
		View title_bar_divider = (View) empty_view_music.findViewById(R.id.title_bar_divider);
		title_bar_divider.setVisibility(View.VISIBLE);
		btn_music_order_nodata = (Button) empty_view_music.findViewById(R.id.btn_left);
		btnMusSearchEmpty = (Button) empty_view_music.findViewById(R.id.btn_search);
		btnMusSearchEmpty.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showDialogs();
				Intent mIntent = new Intent(ResActivity.this, NetResSearchActivity.class);
				mIntent.putExtra("resType", RES_MUS_QUERY_TYPE);
				startActivity(mIntent);
			}
		});
		musiclayoutTitle_nodata = (LinearLayout) empty_view_music.findViewById(R.id.layout_title);
		musiclayoutTitle_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				docpopQuickAction.show(musicTitleTopLayout_nodata);
				musiciconDrow.setImageResource(R.drawable.icon32_arrow_p);
			}
		});
		musiciconDrow_nodata = (ImageView) empty_view_music.findViewById(R.id.icon_drow);
		musiciconDrow_nodata.setImageResource(R.drawable.icon32_arrow);
		music_tvTitle_nodata = (TextView) empty_view_music.findViewById(R.id.tv_title);
		music_tvTitle_nodata.getPaint().setFakeBoldText(true);
		musicTitleTopLayout_nodata = (RelativeLayout) empty_view_music.findViewById(R.id.title_top_ly);

		btn_music_order_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopopQuickAction(btn_music_order_nodata);
			}
		});

		music_TextViewEmpty = (TextView) empty_view_music.findViewById(R.id.textViewEmpty);
		iv_empty_icon_music = (ImageView) empty_view_music.findViewById(R.id.iv_empty_icon);
		iv_empty_icon_music.setImageDrawable(null);
		music_TextViewEmpty.setText("");
		lv_music.setEmptyView(empty_view_music);

		mImageWorker.setLoadingImage(R.drawable.photolist_head);
		// mImageWorker.setImageCache(new ImageCache(this, cacheParams));

		// 绑定数据
		musicListAdapter = new MusicListAdapter(this, allMusicList, mImageWorker);
		lv_music.setAdapter(musicListAdapter);

		music_loadmore = inflater.inflate(R.layout.loadmore_footerview, null);
		music_loadmore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (ly_musicLoadPressBar.getVisibility() == View.GONE
						&& ly_musicLoadText.getVisibility() == View.VISIBLE) {
					if (music_NoHaveMoreData) {
						tv_musicLoadText.setText(R.string.nomore_data);
					} else {
						if (music_title_index == RES_MENU_INDEXS[0]) {
							queryLbsMusic(str_music_search);
						} else if (music_title_index == RES_MENU_INDEXS[1]) {
							queryContactsRes(RES_MUSIC_QUERY, "0", app_orderkey);
						} else if (music_title_index == RES_MENU_INDEXS[2]) {
							queryContactsRes(RES_MUSIC_QUERY, "1", app_orderkey);
						}
					}
				}
			}
		});
		lv_music.addFooterView(music_loadmore);
		ly_musicLoadPressBar = (LinearLayout) music_loadmore.findViewById(R.id.ly_loadmore_probar);
		ly_musicLoadText = (LinearLayout) music_loadmore.findViewById(R.id.ly_loadmore_text);
		tv_musicLoadText = (TextView) music_loadmore.findViewById(R.id.tv_loadmore);
	}

	private void initPic() {
		sp_Util.saveInt(TAG, 0);
		if (lv_pic == null) {
			lv_pic = relv_pic.getRefreshableView();
			lv_pic.setDividerHeight(0);

			initPhotoBar();
			// queryLbsPic(str_pic_search);
		}

		relv_doc.setVisibility(View.GONE);
		relv_music.setVisibility(View.GONE);
		relv_pic.setVisibility(View.VISIBLE);
		relv_app.setVisibility(View.GONE);

		if (!pic_NoHaveMoreData) {
			pic_loadmore.setVisibility(View.VISIBLE);
		}
	}

	View.OnClickListener picOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int location = (Integer) v.getTag();
			intoPic(location);
		}
	};

	View.OnLongClickListener picOnLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			int location = (Integer) v.getTag();
			selectItem = (MovingRes) photoList.get(location);
			showPopMenu();
			return true;
		}
	};

	private void initPhotoBar() {
		// TODO Auto-generated method stub
		/**
		 * 搜索栏
		 */
		pic_search_bar = inflater.inflate(R.layout.restitle_bar, null);
		btnPicSearch = (Button) pic_search_bar.findViewById(R.id.btn_search);
		btnPicSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showDialogs();
				Intent mIntent = new Intent(ResActivity.this, NetResSearchActivity.class);
				mIntent.putExtra("resType", RES_PIC_QUERY_TYPE);
				startActivity(mIntent);
			}
		});

		piclayoutTitle = (LinearLayout) pic_search_bar.findViewById(R.id.layout_title);
		piclayoutTitle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				docpopQuickAction.show(picTitleTopLayout);
				piciconDrow.setImageResource(R.drawable.icon32_arrow_p);
			}
		});

		tv_pic_sorttype = (TextView) pic_search_bar.findViewById(R.id.tv_res_sorttype);

		piciconDrow = (ImageView) pic_search_bar.findViewById(R.id.icon_drow);
		piciconDrow.setImageResource(R.drawable.icon32_arrow);
		pic_tvTitle = (TextView) pic_search_bar.findViewById(R.id.tv_title);
		pic_tvTitle.getPaint().setFakeBoldText(true);
		picTitleTopLayout = (RelativeLayout) pic_search_bar.findViewById(R.id.title_top_ly);

		View title_bar_divider = (View) pic_search_bar.findViewById(R.id.title_bar_divider);
		title_bar_divider.setVisibility(View.VISIBLE);
		btn_pic_order = (Button) pic_search_bar.findViewById(R.id.btn_left);
		btn_pic_order.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopopQuickAction(btn_pic_order);
			}
		});

		lv_pic.addHeaderView(pic_search_bar);

		empty_view_pic = LayoutInflater.from(this).inflate(R.layout.res_empty_view, null, false);
		View title_bar_divider_nodata = (View) empty_view_pic.findViewById(R.id.title_bar_divider);
		title_bar_divider_nodata.setVisibility(View.VISIBLE);
		btn_pic_order_nodata = (Button) empty_view_pic.findViewById(R.id.btn_left);
		btnPicSearchEmpty = (Button) empty_view_pic.findViewById(R.id.btn_search);
		btnPicSearchEmpty.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(ResActivity.this, NetResSearchActivity.class);
				mIntent.putExtra("resType", RES_PIC_QUERY_TYPE);
				startActivity(mIntent);
			}
		});
		piclayoutTitle_nodata = (LinearLayout) empty_view_pic.findViewById(R.id.layout_title);
		piclayoutTitle_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				docpopQuickAction.show(picTitleTopLayout_nodata);
				piciconDrow.setImageResource(R.drawable.icon32_arrow_p);
			}
		});
		piciconDrow_nodata = (ImageView) empty_view_pic.findViewById(R.id.icon_drow);
		piciconDrow_nodata.setImageResource(R.drawable.icon32_arrow);
		pic_tvTitle_nodata = (TextView) empty_view_pic.findViewById(R.id.tv_title);
		pic_tvTitle_nodata.getPaint().setFakeBoldText(true);
		picTitleTopLayout_nodata = (RelativeLayout) empty_view_pic.findViewById(R.id.title_top_ly);

		btn_pic_order_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopopQuickAction(btn_pic_order_nodata);
			}
		});

		pic_TextViewEmpty = (TextView) empty_view_pic.findViewById(R.id.textViewEmpty);
		iv_empty_icon_pic = (ImageView) empty_view_pic.findViewById(R.id.iv_empty_icon);
		iv_empty_icon_pic.setImageDrawable(null);
		pic_TextViewEmpty.setText("");
		lv_pic.setEmptyView(empty_view_pic);

		// 绑定数据
		picAdapter = new ResNetPicAdapter(this, photoList, mImageWorker);
		picAdapter.setPicOnClickListener(picOnClickListener);
		picAdapter.setPicOnLongClickListener(picOnLongClickListener);
		lv_pic.setAdapter(picAdapter);

		pic_loadmore = inflater.inflate(R.layout.loadmore_footerview, null);
		pic_loadmore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (ly_picLoadPressBar.getVisibility() == View.GONE && ly_picLoadText.getVisibility() == View.VISIBLE) {
					if (pic_NoHaveMoreData) {
						tv_picLoadText.setText(R.string.nomore_data);
					} else {
						if (pic_title_index == RES_MENU_INDEXS[0]) {
							queryLbsPic(str_pic_search);
						} else if (pic_title_index == RES_MENU_INDEXS[1]) {
							queryContactsRes(RES_PIC_QUERY, "0", pic_orderkey);
						} else if (pic_title_index == RES_MENU_INDEXS[2]) {
							queryContactsRes(RES_PIC_QUERY, "1", pic_orderkey);
						}
					}
				}
			}
		});
		lv_pic.addFooterView(pic_loadmore);
		ly_picLoadPressBar = (LinearLayout) pic_loadmore.findViewById(R.id.ly_loadmore_probar);
		ly_picLoadText = (LinearLayout) pic_loadmore.findViewById(R.id.ly_loadmore_text);
		tv_picLoadText = (TextView) pic_loadmore.findViewById(R.id.tv_loadmore);
	}

	private void initApp() {
		sp_Util.saveInt(TAG, 3);
		if (lv_app == null) {
			lv_app = relv_app.getRefreshableView();
			lv_app.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					// AppInfo resInfo = resList.get(position);
					selectItem = (AppInfo) arg0.getAdapter().getItem(position);
					intoApp(position - 2);
				}

			});
			lv_app.setOnItemLongClickListener(new OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					selectItem = (MovingRes) arg0.getAdapter().getItem(position);
					showPopMenu();
					return true;
				}
			});

			initAppBar();

			// 绑定数据
			appAdapter = new ResAppAdapter(this, appList, mImageWorker);
			lv_app.setAdapter(appAdapter);
			app_loadmore = inflater.inflate(R.layout.loadmore_footerview, null);
			ly_appLoadPressBar = (LinearLayout) app_loadmore.findViewById(R.id.ly_loadmore_probar);
			ly_appLoadText = (LinearLayout) app_loadmore.findViewById(R.id.ly_loadmore_text);
			tv_appLoadText = (TextView) app_loadmore.findViewById(R.id.tv_loadmore);
			app_loadmore.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					if (ly_appLoadPressBar.getVisibility() == View.GONE
							&& ly_appLoadText.getVisibility() == View.VISIBLE) {
						if (app_NoHaveMoreData) {
							tv_appLoadText.setText(R.string.nomore_data);
						} else {
							if (app_title_index == RES_MENU_INDEXS[0]) {
								queryLbsApp(str_app_search);
							} else if (app_title_index == RES_MENU_INDEXS[1]) {
								queryContactsRes(RES_APP_QUERY, "0", app_orderkey);
							} else if (app_title_index == RES_MENU_INDEXS[2]) {
								queryContactsRes(RES_APP_QUERY, "1", app_orderkey);
							}
						}
					}
				}
			});

			lv_app.addFooterView(app_loadmore);

		}
		relv_doc.setVisibility(View.GONE);
		relv_music.setVisibility(View.GONE);
		relv_pic.setVisibility(View.GONE);
		relv_app.setVisibility(View.VISIBLE);
	}

	private void initAppBar() {
		// TODO Auto-generated method stub
		/**
		 * 搜索栏
		 */
		app_search_bar = inflater.inflate(R.layout.restitle_bar, null);
		btnAppSearch = (Button) app_search_bar.findViewById(R.id.btn_search);
		btnAppSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showDialogs();
				Intent mIntent = new Intent(ResActivity.this, NetResSearchActivity.class);
				mIntent.putExtra("resType", RES_APP_QUERY_TYPE);
				startActivity(mIntent);
			}
		});
		btn_app_order = (Button) app_search_bar.findViewById(R.id.btn_left);
		btn_app_order.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopopQuickAction(btn_app_order);
			}
		});

		applayoutTitle = (LinearLayout) app_search_bar.findViewById(R.id.layout_title);
		applayoutTitle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				docpopQuickAction.show(appTitleTopLayout);
				appiconDrow.setImageResource(R.drawable.icon32_arrow_p);
			}
		});

		tv_app_sorttype = (TextView) app_search_bar.findViewById(R.id.tv_res_sorttype);

		appiconDrow = (ImageView) app_search_bar.findViewById(R.id.icon_drow);
		appiconDrow.setImageResource(R.drawable.icon32_arrow);
		app_tvTitle = (TextView) app_search_bar.findViewById(R.id.tv_title);
		app_tvTitle.getPaint().setFakeBoldText(true);
		appTitleTopLayout = (RelativeLayout) app_search_bar.findViewById(R.id.title_top_ly);

		lv_app.addHeaderView(app_search_bar);

		empty_view_app = LayoutInflater.from(this).inflate(R.layout.res_empty_view, null, false);
		View title_bar_divider = (View) empty_view_app.findViewById(R.id.title_bar_divider);
		title_bar_divider.setVisibility(View.VISIBLE);
		btn_app_order_nodata = (Button) empty_view_app.findViewById(R.id.btn_left);

		btnAppSearchEmpty = (Button) empty_view_app.findViewById(R.id.btn_search);
		btnAppSearchEmpty.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(ResActivity.this, NetResSearchActivity.class);
				mIntent.putExtra("resType", RES_APP_QUERY_TYPE);
				startActivity(mIntent);
			}
		});

		applayoutTitle_nodata = (LinearLayout) empty_view_app.findViewById(R.id.layout_title);
		applayoutTitle_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				docpopQuickAction.show(appTitleTopLayout_nodata);
				appiconDrow.setImageResource(R.drawable.icon32_arrow_p);
			}
		});
		appiconDrow_nodata = (ImageView) empty_view_app.findViewById(R.id.icon_drow);
		appiconDrow_nodata.setImageResource(R.drawable.icon32_arrow);
		app_tvTitle_nodata = (TextView) empty_view_app.findViewById(R.id.tv_title);
		app_tvTitle_nodata.getPaint().setFakeBoldText(true);
		appTitleTopLayout_nodata = (RelativeLayout) empty_view_app.findViewById(R.id.title_top_ly);

		btn_app_order_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopopQuickAction(btn_app_order_nodata);
			}
		});

		app_TextViewEmpty = (TextView) empty_view_app.findViewById(R.id.textViewEmpty);
		iv_empty_icon_app = (ImageView) empty_view_app.findViewById(R.id.iv_empty_icon);
		iv_empty_icon_app.setImageDrawable(null);
		app_TextViewEmpty.setText("");
		lv_app.setEmptyView(empty_view_app);
	}

	private void searchResData() {
		switch (view_type) {
		case R.id.res_read:
			doc_startLimit = 0;
			doc_NoHaveMoreData = false;
			tv_docLoadText.setText(R.string.loadmore);
			if (doc_title_index == RES_MENU_INDEXS[0]) {
				queryLbsDoc(str_doc_search);
			} else if (doc_title_index == RES_MENU_INDEXS[1]) {
				queryContactsRes(RES_DOC_QUERY, "0", doc_orderkey);
			} else if (doc_title_index == RES_MENU_INDEXS[2]) {
				queryContactsRes(RES_DOC_QUERY, "1", doc_orderkey);
			}
			break;
		case R.id.res_music:
			music_startLimit = 0;
			music_NoHaveMoreData = false;
			tv_musicLoadText.setText(R.string.loadmore);
			if (music_title_index == RES_MENU_INDEXS[0]) {
				queryLbsMusic(str_music_search);
			} else if (music_title_index == RES_MENU_INDEXS[1]) {
				queryContactsRes(RES_MUSIC_QUERY, "0", music_orderkey);
			} else if (music_title_index == RES_MENU_INDEXS[2]) {
				queryContactsRes(RES_MUSIC_QUERY, "1", music_orderkey);
			}
			break;
		case R.id.res_pic:
			pic_startLimit = 0;
			pic_NoHaveMoreData = false;
			tv_picLoadText.setText(R.string.loadmore);
			// queryLbsPic(et_pic_search.getText().toString().trim());
			if (pic_title_index == RES_MENU_INDEXS[0]) {
				queryLbsPic(str_pic_search);
			} else if (pic_title_index == RES_MENU_INDEXS[1]) {
				queryContactsRes(RES_PIC_QUERY, "0", pic_orderkey);
			} else if (pic_title_index == RES_MENU_INDEXS[2]) {
				queryContactsRes(RES_PIC_QUERY, "1", pic_orderkey);
			}
			break;
		case R.id.res_app:
			app_NoHaveMoreData = false;
			app_startLimit = 0;
			tv_appLoadText.setText(R.string.loadmore);
			// queryLbsApp(et_app_search.getText().toString().trim());
			if (app_title_index == RES_MENU_INDEXS[0]) {
				queryLbsApp(str_app_search);
			} else if (app_title_index == RES_MENU_INDEXS[1]) {
				queryContactsRes(RES_APP_QUERY, "0", app_orderkey);
			} else if (app_title_index == RES_MENU_INDEXS[2]) {
				queryContactsRes(RES_APP_QUERY, "1", app_orderkey);
			}
			break;
		}
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			case FAVORITE:
				broadcastApp();
				break;
			case SAVE_TO_ME:
				switch (view_type) {
				case R.id.res_read:
					searchDirInfo(PangkerConstant.RES_DOCUMENT);
					break;
				case R.id.res_pic:
					searchDirInfo(PangkerConstant.RES_PICTURE);
					break;
				case R.id.res_music:
					searchDirInfo(PangkerConstant.RES_MUSIC);
					break;
				case R.id.res_app:
					searchDirInfo(PangkerConstant.RES_APPLICATION);
					break;
				}
				break;
			}
			menuDialog.dismiss();
		}
	};

	private void showPopMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
			ActionItem[] msgMenu = new ActionItem[] {
					new ActionItem(FAVORITE, getString(R.string.res_question_retransmit)),
					new ActionItem(SAVE_TO_ME, getString(R.string.res_app_collect_btn)) };
			menuDialog.setMenus(msgMenu);
		}
		switch (view_type) {
		case R.id.res_read:
			menuDialog.setTitle(((DocInfo) selectItem).getDocname());
			break;
		case R.id.res_pic:
			menuDialog.setTitle(((PicInfo) selectItem).getResName());
			break;
		case R.id.res_music:
			menuDialog.setTitle(((MusicInfo) selectItem).getMusicName());
			break;
		case R.id.res_app:
			menuDialog.setTitle(((AppInfo) selectItem).getAppname());
			break;
		}
		menuDialog.show();
	}

	/**
	 * 查看阅读资源详细 这里都是自己上传的资源，而没有转播，一次shareUid都是用户Id
	 * 
	 * @param docInfo
	 */
	private void intoDocInfo(int position) {
		Intent intent = new Intent();

		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		for (DocInfo docInfo : allListFiles) {
			ResShowEntity entity = new ResShowEntity();
			entity.setCommentTimes(docInfo.getCommentTimes());
			entity.setResType(PangkerConstant.RES_DOCUMENT);
			entity.setDownloadTimes(docInfo.getDownloadTimes());
			entity.setFavorTimes(docInfo.getFavorTimes());
			entity.setResID(docInfo.getSid());
			entity.setScore(Integer.parseInt(String.valueOf(docInfo.getScore())));
			entity.setResName(docInfo.getResName());
			entity.setShareUid(docInfo.getShareUid());
			entity.setIfshare(docInfo.getIsShare());
			reList.add(entity);
		}
		myApplication.setResLists(reList);
		myApplication.setViewIndex(position);
		intent.setClass(this, ResInfoActivity.class);
		startActivity(intent);
	}

	private void intoApp(int position) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		for (AppInfo appInfo : appList) {
			ResShowEntity entity = new ResShowEntity();
			entity.setCommentTimes(appInfo.getCommentTimes());
			entity.setResType(PangkerConstant.RES_APPLICATION);
			entity.setDownloadTimes(appInfo.getDownloadTimes());
			entity.setFavorTimes(appInfo.getFavorTimes());
			entity.setResID(appInfo.getSid());
			entity.setScore(Integer.parseInt(String.valueOf(appInfo.getScore())));
			entity.setResName(appInfo.getResName());
			entity.setShareUid(appInfo.getShareUid());
			entity.setIfshare(appInfo.getIsShare());
			reList.add(entity);
		}
		myApplication.setResLists(reList);
		myApplication.setViewIndex(position);
		intent.setClass(ResActivity.this, ResInfoActivity.class);
		ResActivity.this.startActivity(intent);
	}

	/**
	 * 查看图片资源详细
	 * 
	 * @param musicInfo
	 */
	private void intoPic(int position) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		for (PicInfo picInfo : photoList) {
			ResShowEntity entity = new ResShowEntity();
			entity.setIfshare(picInfo.getIsShare());
			entity.setCommentTimes(picInfo.getCommentTimes());
			entity.setResType(PangkerConstant.RES_PICTURE);
			entity.setDownloadTimes(picInfo.getDownloadTimes());
			entity.setFavorTimes(picInfo.getFavorTimes());
			entity.setResID(picInfo.getSid());
			entity.setScore(Integer.parseInt(String.valueOf(picInfo.getScore())));
			entity.setResName(picInfo.getResName());
			entity.setShareUid(picInfo.getShareUid());
			reList.add(entity);
		}
		myApplication.setResLists(reList);
		myApplication.setViewIndex(position);
		intent.setClass(this, PictureInfoActivity.class);
		startActivity(intent);
	}

	/**
	 * 查看音乐资源详细
	 * 
	 * @param musicInfo
	 */
	private void intoMusicInfo(MusicInfo musicInfo, int position) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		List<ResShowEntity> reList = new ArrayList<ResShowEntity>();
		for (MusicInfo info : allMusicList) {
			ResShowEntity entity = new ResShowEntity();
			entity.setCommentTimes(info.getCommentTimes());
			entity.setResType(PangkerConstant.RES_MUSIC);
			entity.setDownloadTimes(info.getDownloadTimes());
			entity.setFavorTimes(info.getFavorTimes());
			entity.setResID(info.getSid());
			entity.setScore(Integer.parseInt(String.valueOf(info.getScore())));
			entity.setResName(info.getResName());
			entity.setShareUid(info.getShareUid());
			entity.setIfshare(info.getIsShare());
			entity.setDuration(info.getDuration());
			entity.setFileSize(info.getFileSize());
			entity.setFromat(info.getFileFormat());
			entity.setSinger(info.getSinger());
			reList.add(entity);
		}
		myApplication.setResLists(reList);
		myApplication.setViewIndex(position);
		intent.setClass(this, ResInfoActivity.class);
		startActivity(intent);
	}

	/**
	 * @Title: queryLbsDoc
	 * @Description: 查询阅读,调用搜索引擎http://192.168.3.22/lbsindex/test/docsearch.html
	 * @param searchKey
	 *            根据指定关键字查询，为空则查询全部
	 * @return void 返回类型
	 */
	private void queryLbsDoc(String searchKey) {
		if (docAdapter.getCount() >= incremental) {
			ly_docLoadPressBar.setVisibility(View.VISIBLE);
			ly_docLoadText.setVisibility(View.GONE);
		}
		try {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("userid", userID));
			paras.add(getLongitude());
			paras.add(getLatitude());
			paras.add(new Parameter("searchkey", searchKey));
			paras.add(getDocSearchLimit());//
			paras.add(getDistanceRang());
			paras.add(new Parameter("orderkey", doc_orderkey));
			paras.add(new Parameter("searchtype", doc_searchtype));
			serverMana.supportRequest(Configuration.getSearchDoc(), paras, RES_DOC_QUERY);
			if (doc_startLimit == 0) {
				relv_doc.setRefreshing(true);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			ly_docLoadPressBar.setVisibility(View.GONE);
			ly_docLoadText.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 查询音乐 从搜索引擎 接口：http://192.168.3.22/lbsindex/test/musicsearch.html
	 */
	private void queryLbsMusic(String searchKey) {
		if (musicListAdapter.getCount() >= incremental) {
			ly_musicLoadPressBar.setVisibility(View.VISIBLE);
			ly_musicLoadText.setVisibility(View.GONE);
		}
		try {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("userid", userID));
			paras.add(getLongitude());
			paras.add(getLatitude());
			paras.add(getDistanceRang());
			paras.add(getMusicSearchLimit());//
			paras.add(new Parameter("searchkey", searchKey));
			paras.add(new Parameter("orderkey", music_orderkey));
			paras.add(new Parameter("searchtype", music_searchtype));
			serverMana.supportRequest(Configuration.getSearchMusic(), paras, RES_MUSIC_QUERY);
			if (music_startLimit == 0) {
				relv_music.setRefreshing(true);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			ly_musicLoadPressBar.setVisibility(View.GONE);
			ly_musicLoadText.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 查询相片接口
	 * 
	 * @param count请求数目
	 *            , search 搜索关键字 访问接口：http://IP/lbspicindex/picsearch
	 */
	private void queryLbsPic(String search) {
		if (picAdapter.getCount() >= incremental) {
			ly_picLoadPressBar.setVisibility(View.VISIBLE);
			ly_picLoadText.setVisibility(View.GONE);
		}

		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userID));
		paras.add(getPicSearchLimit()); //
		paras.add(getDistanceRang());// 默认搜索一千米
		paras.add(getLatitude()); // 23.0599112
		paras.add(getLongitude()); // 113.4018965
		paras.add(new Parameter("searchkey", search));//
		paras.add(new Parameter("orderkey", pic_orderkey));
		paras.add(new Parameter("searchtype", pic_searchtype));
		if (pic_startLimit == 0) {
			relv_pic.setRefreshing(true);
		}
		serverMana.supportRequest(Configuration.getSearchPicture(), paras, RES_PIC_QUERY);
	}

	private void queryLbsApp(String searchKey) {

		try {
			if (appAdapter.getCount() >= incremental) {
				ly_appLoadPressBar.setVisibility(View.VISIBLE);
				ly_appLoadText.setVisibility(View.GONE);
			}

			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(getUserID());
			paras.add(getLongitude());
			paras.add(getLatitude());
			paras.add(getDistanceRang());
			paras.add(getAppSearchLimit());
			paras.add(new Parameter("searchkey", searchKey));
			paras.add(new Parameter("searchtype", app_searchtype));
			paras.add(new Parameter("orderkey", app_orderkey));
			serverMana.supportRequest(Configuration.getSearchLbsApp(), paras, RES_APP_QUERY);
			if (app_startLimit == 0) {
				relv_app.setRefreshing(true);
			}
		} catch (Exception e) {
			logger.error(TAG, e);
			ly_appLoadPressBar.setVisibility(View.GONE);
			ly_appLoadText.setVisibility(View.VISIBLE);
		}
	}

	private void judgeContactAdapter() {
		switch (view_type) {
		case R.id.res_read:
			if (docAdapter.getCount() >= incremental) {
				ly_docLoadPressBar.setVisibility(View.VISIBLE);
				ly_docLoadText.setVisibility(View.GONE);
			}
			break;
		case R.id.res_pic:
			if (picAdapter.getCount() >= incremental) {
				ly_picLoadPressBar.setVisibility(View.VISIBLE);
				ly_picLoadText.setVisibility(View.GONE);
			}
			break;
		case R.id.res_music:
			if (musicListAdapter.getCount() >= incremental) {
				ly_musicLoadPressBar.setVisibility(View.VISIBLE);
				ly_musicLoadText.setVisibility(View.GONE);
			}
			break;
		case R.id.res_app:
			if (appAdapter.getCount() >= incremental) {
				ly_appLoadPressBar.setVisibility(View.VISIBLE);
				ly_appLoadText.setVisibility(View.GONE);
			}
			break;
		}
	}

	private void queryContactsRes(int search_type, String contactType, String orderkey) {
		try {
			judgeContactAdapter();

			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", userId));
			paras.add(new Parameter("uid", userId));
			paras.add(new Parameter("type", String.valueOf(search_type)));
			paras.add(new Parameter("contactType", contactType));
			paras.add(new Parameter("orderkey", orderkey));
			if (search_type == RES_DOC_QUERY) {
				paras.add(getContactDocSearchLimit());
				if (doc_startLimit == 0) {
					relv_doc.setRefreshing(true);
				}
			} else if (search_type == RES_PIC_QUERY) {
				paras.add(getContactPicSearchLimit());
				if (pic_startLimit == 0) {
					relv_pic.setRefreshing(true);
				}
			} else if (search_type == RES_MUSIC_QUERY) {
				paras.add(getContactMusicSearchLimit());
				if (music_startLimit == 0) {
					relv_music.setRefreshing(true);
				}
			} else if (search_type == RES_APP_QUERY) {
				paras.add(getContactAppSearchLimit());
				if (app_startLimit == 0) {
					relv_app.setRefreshing(true);
				}
			}
			paras.add(new Parameter("endtime", Util.getSysNowTime()));
			Log.d("Parame", "search_type:" + search_type + ", contactType :" + contactType + ", orderkey:" + orderkey
					+ ", limit:" + doc_startLimit);
			serverMana.supportRequest(Configuration.getContactsResQuery(), paras, search_type);
		} catch (Exception e) {
			logger.error(TAG, e);
			dealSearchException();
		}
	}

	private void dealSearchException() {
		switch (view_type) {
		case R.id.res_read:
			ly_docLoadPressBar.setVisibility(View.GONE);
			ly_docLoadText.setVisibility(View.VISIBLE);
			break;
		case R.id.res_pic:
			ly_picLoadPressBar.setVisibility(View.GONE);
			ly_picLoadText.setVisibility(View.VISIBLE);
			break;
		case R.id.res_music:
			ly_musicLoadPressBar.setVisibility(View.GONE);
			ly_musicLoadText.setVisibility(View.VISIBLE);
			break;
		case R.id.res_app:
			ly_appLoadPressBar.setVisibility(View.GONE);
			ly_appLoadText.setVisibility(View.VISIBLE);
			break;
		}
	}

	private void broadcastApp() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userID));
		paras.add(new Parameter("resid", String.valueOf(selectItem.getSid())));
		paras.add(new Parameter("shareid", ""));// 分享ID（type=2时使用,shareid为空,则转播资源广播时的shareid）
		paras.add(new Parameter("optype", String.valueOf(PangkerConstant.OPTYPE_RES)));// 操作类型（0：问答，1：资源，2：群组）资源广播时必填.
		paras.add(new Parameter("type", "2"));// 1: 广播 2:转播
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getResShare(), paras, true, mess, FAVORITE);
	}

	// pull
	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {

		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			searchResData();
		}
	};

	protected void showPopopQuickAction(Button button) {
		popopQuickAction.show(button);
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == MENU_INDEXS[0]) {
				searchAllData(String.valueOf(PangkerConstant.SORT_BY_CREATETIME_DESC), null);
			} else if (actionId == MENU_INDEXS[1]) {
				searchAllData(String.valueOf(PangkerConstant.SORT_BY_SCORE_DESC), null);
			} else if (actionId == MENU_INDEXS[2]) {
				searchAllData(String.valueOf(PangkerConstant.SORT_BY_COMMENTTIME_DESC), null);
			} else if (actionId == MENU_INDEXS[3]) {
				searchAllData(String.valueOf(PangkerConstant.SORT_BY_DOWNLOADTIMES_DESC), null);
			}

		}
	};

	@Override
	public void uiCallBack(Object response, int caseKey) {
		super.uiCallBack(response, caseKey);
		Log.d("uiCallBack", response.toString());
		if (!HttpResponseStatus(response)) {
			if (caseKey == RES_DOC_QUERY) {
				relv_doc.onRefreshComplete();
				iv_empty_icon_doc.setImageResource(R.drawable.emptylist_icon);
				doc_TextViewEmpty.setText(R.string.no_network);
				ly_docLoadPressBar.setVisibility(View.GONE);
				ly_docLoadText.setVisibility(View.GONE);

			} else if (caseKey == RES_MUSIC_QUERY) {
				iv_empty_icon_music.setImageResource(R.drawable.emptylist_icon);
				music_TextViewEmpty.setText(R.string.no_network);
				ly_musicLoadPressBar.setVisibility(View.GONE);
				ly_musicLoadText.setVisibility(View.GONE);
				relv_music.onRefreshComplete();
			} else if (caseKey == RES_PIC_QUERY) {
				iv_empty_icon_pic.setImageResource(R.drawable.emptylist_icon);
				pic_TextViewEmpty.setText(R.string.no_network);
				ly_picLoadPressBar.setVisibility(View.GONE);
				ly_picLoadText.setVisibility(View.GONE);
				relv_pic.onRefreshComplete();
			} else if (caseKey == RES_APP_QUERY) {
				iv_empty_icon_app.setImageResource(R.drawable.emptylist_icon);
				app_TextViewEmpty.setText(R.string.no_network);
				ly_appLoadPressBar.setVisibility(View.GONE);
				ly_appLoadText.setVisibility(View.GONE);
				relv_app.onRefreshComplete();
			}
			return;
		}

		switch (caseKey) {
		case RES_DOC_QUERY:

			relv_doc.onRefreshComplete();
			SearchLbsDocResult resLbsResult = JSONUtil.fromJson(response.toString(), SearchLbsDocResult.class);
			if (resLbsResult != null && resLbsResult.isSuccess()) {
				List<DocInfo> listData = resLbsResult.getData().getData();
				if (listData != null && listData.size() > 0) {

					// 若获取到的数据少于搜索区间，则表示已经数据已经取完
					if (listData.size() < incremental) {
						this.doc_NoHaveMoreData = true;
						ly_docLoadPressBar.setVisibility(View.GONE);
						ly_docLoadText.setVisibility(View.GONE);
					} else {
						ly_docLoadPressBar.setVisibility(View.GONE);
						ly_docLoadText.setVisibility(View.VISIBLE);
					}

					// 如果不是从0开始加载
					if (doc_startLimit != 0) {
						allListFiles.addAll(listData);
					}
					// 首次加载
					else {
						if (!doc_NoHaveMoreData) {
							ly_docLoadPressBar.setVisibility(View.GONE);
							ly_docLoadText.setVisibility(View.VISIBLE);
						}
						allListFiles = listData;
					}
					resetDocStartLimit();
					docAdapter.setmDocs(allListFiles);
				} else if (listData == null || listData.size() == 0) {
					if (doc_startLimit == 0) {
						docAdapter.setmDocs(new ArrayList<DocInfo>());
						iv_empty_icon_doc.setImageResource(R.drawable.emptylist_icon);
						doc_TextViewEmpty.setText(R.string.no_near_res_prompt);

					} else
						showToast(R.string.nomore_data);
					doc_NoHaveMoreData = true;
					ly_docLoadPressBar.setVisibility(View.GONE);
					ly_docLoadText.setVisibility(View.GONE);
				} else {
					tv_docLoadText.setText(R.string.load_data_fail);
					ly_docLoadPressBar.setVisibility(View.GONE);
					ly_docLoadText.setVisibility(View.VISIBLE);
				}

			} else {
				if (doc_startLimit == 0) {
					iv_empty_icon_doc.setImageResource(R.drawable.emptylist_icon);
					doc_TextViewEmpty.setText(R.string.to_server_fail);
				} else {
					showToast(R.string.to_server_fail);
				}
				ly_docLoadPressBar.setVisibility(View.GONE);
				ly_docLoadText.setVisibility(View.GONE);
			}
			break;
		case RES_MUSIC_QUERY:

			relv_music.onRefreshComplete();
			relv_music.setBackgroundDrawable(null);
			SearchLbsMusicResult lbsMusicResult = JSONUtil.fromJson(response.toString(), SearchLbsMusicResult.class);
			if (lbsMusicResult != null && lbsMusicResult.isSuccess()) {
				if (lbsMusicResult.getData().getData() != null && lbsMusicResult.getData().getData().size() > 0) {
					List<MusicInfo> newMusicList = lbsMusicResult.getData().getData();

					if (newMusicList != null && newMusicList.size() > 0) {
						if (newMusicList.size() < incremental) {
							music_NoHaveMoreData = true;
							ly_musicLoadPressBar.setVisibility(View.GONE);
							ly_musicLoadText.setVisibility(View.GONE);
						} else {
							ly_musicLoadPressBar.setVisibility(View.GONE);
							ly_musicLoadText.setVisibility(View.VISIBLE);
						}
						// 如果不是从0开始加载
						if (music_startLimit != 0) {
							allMusicList.addAll(newMusicList);
						}
						// 首次加载
						else {
							if (!music_NoHaveMoreData) {
								ly_musicLoadPressBar.setVisibility(View.GONE);
								ly_musicLoadText.setVisibility(View.VISIBLE);
							}
							allMusicList = newMusicList;
						}
						resetMusicStartLimit();
						// 更新列表数据
						musicListAdapter.setRowContent(allMusicList);
					} else if (newMusicList == null || newMusicList.size() == 0) {
						showToast(R.string.nomore_data);
						music_NoHaveMoreData = true;
						ly_musicLoadPressBar.setVisibility(View.GONE);
						ly_musicLoadText.setVisibility(View.GONE);
					} else {
						tv_musicLoadText.setText(R.string.load_data_fail);
						ly_musicLoadPressBar.setVisibility(View.GONE);
						ly_musicLoadText.setVisibility(View.VISIBLE);
					}
				} else if (lbsMusicResult.getData().getData() != null && lbsMusicResult.getData().getData().size() == 0) {
					if (music_startLimit == 0) {
						musicListAdapter.setRowContent(new ArrayList<MusicInfo>());
						iv_empty_icon_music.setImageResource(R.drawable.emptylist_icon);
						music_TextViewEmpty.setText(R.string.no_near_res_prompt);
					} else {
						showToast(R.string.nomore_data);
					}

					music_NoHaveMoreData = true;
					ly_musicLoadPressBar.setVisibility(View.GONE);
					ly_musicLoadText.setVisibility(View.GONE);
				} else {
					showToast(R.string.to_server_fail);
					ly_musicLoadPressBar.setVisibility(View.GONE);
					ly_musicLoadText.setVisibility(View.GONE);
				}
			} else {
				if (music_startLimit == 0) {
					iv_empty_icon_music.setImageResource(R.drawable.emptylist_icon);
					music_TextViewEmpty.setText(R.string.no_near_res_prompt);
				} else {
					showToast(R.string.to_server_fail);
				}
				ly_musicLoadPressBar.setVisibility(View.GONE);
				ly_musicLoadText.setVisibility(View.GONE);
			}
			break;

		case RES_PIC_QUERY:
			relv_pic.onRefreshComplete();
			relv_pic.setBackgroundDrawable(null);
			if (response.toString() != null) {

				SearchLbsPictureResult pictureResult = JSONUtil.fromJson(response.toString(),
						SearchLbsPictureResult.class);
				if (pictureResult != null && pictureResult.isSuccess()) {
					List<PicInfo> picList = pictureResult.getData().getData();

					if (picList != null && picList.size() > 0) {
						// 若获取到的数据少于搜索区间，则表示已经数据已经取完
						if (picList.size() < incremental) {
							ly_picLoadPressBar.setVisibility(View.GONE);
							ly_picLoadText.setVisibility(View.GONE);
							pic_NoHaveMoreData = true;
						} else {
							ly_picLoadPressBar.setVisibility(View.GONE);
							ly_picLoadText.setVisibility(View.VISIBLE);
						}

						// 如果不是从0开始加载
						if (pic_startLimit != 0) {
							List<PicInfo> addList = new ArrayList<PicInfo>(picList);
							photoList.addAll(addList);
						}
						// 首次加载
						else {
							if (!pic_NoHaveMoreData) {
								ly_picLoadPressBar.setVisibility(View.GONE);
								ly_picLoadText.setVisibility(View.VISIBLE);
							}
							photoList = picList;
						}
						resetPicStartLimit();
						picAdapter.setPhotoResult(photoList);

					} else if (picList == null || picList.size() == 0) {
						if (pic_startLimit == 0) {
							picAdapter.setPhotoResult(new ArrayList<PicInfo>());
							pic_TextViewEmpty.setText(R.string.no_near_res_prompt);
							iv_empty_icon_pic.setImageResource(R.drawable.emptylist_icon);
						} else {
							showToast(R.string.nomore_data);
						}

						pic_NoHaveMoreData = true;
						ly_picLoadPressBar.setVisibility(View.GONE);
						ly_picLoadText.setVisibility(View.GONE);
					} else {
						tv_picLoadText.setText(R.string.load_data_fail);
						ly_picLoadPressBar.setVisibility(View.GONE);
						ly_picLoadText.setVisibility(View.VISIBLE);
					}
				} else {
					if (pic_startLimit == 0) {
						pic_TextViewEmpty.setText(R.string.no_near_res_prompt);
						iv_empty_icon_pic.setImageResource(R.drawable.emptylist_icon);
					} else {
						showToast(R.string.to_server_fail);
					}
					ly_picLoadPressBar.setVisibility(View.GONE);
					ly_picLoadText.setVisibility(View.GONE);
				}
			}
			break;
		case RES_APP_QUERY:
			relv_app.onRefreshComplete();
			lv_app.setBackgroundDrawable(null);
			SearchLbsAppResult resappResult = JSONUtil.fromJson(response.toString(), SearchLbsAppResult.class);
			if (resappResult != null && resappResult.isSuccess()) {
				List<AppInfo> newResList = resappResult.getData().getData();
				if (newResList != null && newResList.size() > 0) {
					if (newResList.size() < incremental) {
						app_NoHaveMoreData = true;
						ly_appLoadPressBar.setVisibility(View.GONE);
						ly_appLoadText.setVisibility(View.GONE);
					} else {
						ly_appLoadPressBar.setVisibility(View.GONE);
						ly_appLoadText.setVisibility(View.VISIBLE);
					}

					if (app_startLimit != 0) {
						appList.addAll(newResList);
					}

					else {
						if (!app_NoHaveMoreData) {
							ly_appLoadPressBar.setVisibility(View.GONE);
							ly_appLoadText.setVisibility(View.VISIBLE);
						}
						appList = newResList;
					}
					resetAppStartLimit();
					appAdapter.setmApps(appList);
				} else if (newResList == null || newResList.size() == 0) {
					if (app_startLimit == 0) {

						appAdapter.setmApps(new ArrayList<AppInfo>());
						app_TextViewEmpty.setText(R.string.no_near_res_prompt);
						iv_empty_icon_app.setImageResource(R.drawable.emptylist_icon);
						// showToast(R.string.no_data);
					} else
						showToast(R.string.nomore_data);
					app_NoHaveMoreData = true;
					ly_appLoadPressBar.setVisibility(View.GONE);
					ly_appLoadText.setVisibility(View.GONE);
				} else {
					tv_appLoadText.setText(R.string.load_data_fail);
					ly_appLoadPressBar.setVisibility(View.GONE);
					ly_appLoadText.setVisibility(View.VISIBLE);
				}
			} else {
				if (app_startLimit == 0) {
					app_TextViewEmpty.setText(R.string.no_near_res_prompt);
					iv_empty_icon_app.setImageResource(R.drawable.emptylist_icon);
				} else {
					showToast(R.string.to_server_fail);
				}

				ly_appLoadPressBar.setVisibility(View.GONE);
				ly_appLoadText.setVisibility(View.GONE);
			}

			break;
		case SAVE_TO_ME:
			BaseResult collectResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (collectResult.errorCode == BaseResult.SUCCESS) {
				selectItem.setFavorTimes(selectItem.getFavorTimes() + 1);
				docAdapter.notifyDataSetChanged();
			}
			break;
		case FAVORITE:
			retransmitResult(response.toString());
			break;
		}
	}

	private void retransmitResult(String response) {
		// TODO Auto-generated method stub
		try {
			ResShareResult retransmitAsk = JSONUtil.fromJson(response.toString(), ResShareResult.class);
			if (retransmitAsk != null && retransmitAsk.errorCode == BaseResult.SUCCESS) {
				showToast(retransmitAsk.errorMessage);
			} else if (retransmitAsk != null && retransmitAsk.errorCode == 0) {
				showToast(retransmitAsk.errorMessage);
			} else {
				showToast(R.string.res_retransmit_failed);
			}
		} catch (Exception e) {
			showToast(R.string.res_retransmit_failed);
			logger.error(TAG, e);
		}
	}

	// >>>>>> getSearchLimit每次加载incremental个, 如果是直接访问接口的，limit：0,10、10,10……
	protected Parameter getDocSearchLimit() {
		doc_endLimit = doc_startLimit + incremental;
		Log.i(TAG, "doclimit:start=" + doc_startLimit + ";end=" + doc_endLimit);
		return new Parameter("limit", doc_startLimit + "," + doc_endLimit);
	}

	protected Parameter getContactDocSearchLimit() {
		doc_endLimit = doc_startLimit + incremental;
		return new Parameter("limit", doc_startLimit + "," + incremental);
	}

	protected Parameter getMusicSearchLimit() {
		music_endLimit = music_startLimit + incremental;
		Log.i(TAG, "musiclimit:start=" + music_startLimit + ";end=" + music_endLimit);
		return new Parameter("limit", music_startLimit + "," + music_endLimit);
	}

	protected Parameter getContactMusicSearchLimit() {
		music_endLimit = music_startLimit + incremental;
		return new Parameter("limit", music_startLimit + "," + incremental);
	}

	protected Parameter getPicSearchLimit() {
		pic_endLimit = pic_startLimit + incremental;
		Log.i(TAG, "piclimit:start=" + pic_startLimit + ";end=" + pic_endLimit);
		return new Parameter("limit", pic_startLimit + "," + pic_endLimit);
	}

	protected Parameter getContactPicSearchLimit() {
		pic_endLimit = pic_startLimit + incremental;
		return new Parameter("limit", pic_startLimit + "," + incremental);
	}

	protected Parameter getAppSearchLimit() {
		app_endLimit = app_startLimit + incremental;
		Log.i(TAG, "applimit:start=" + app_startLimit + ";end=" + app_endLimit);
		return new Parameter("limit", app_startLimit + "," + app_endLimit);
	}

	protected Parameter getContactAppSearchLimit() {
		app_endLimit = app_startLimit + incremental;
		return new Parameter("limit", app_startLimit + "," + incremental);
	}

	// >>>>>>>>> setStartLimit
	public void resetDocStartLimit() {
		doc_startLimit = doc_endLimit;
	}

	// >>>>>>>>> setStartLimit
	public void resetMusicStartLimit() {
		music_startLimit = music_endLimit;
	}

	// >>>>>>>>> setStartLimit
	public void resetPicStartLimit() {
		pic_startLimit = pic_endLimit;
	}

	// >>>>>>>>> setStartLimit
	public void resetAppStartLimit() {
		app_startLimit = app_endLimit;
	}

	protected void onDestroy() {
		mImageCache.removeMemoryCaches();
		super.onDestroy();
	};
}
