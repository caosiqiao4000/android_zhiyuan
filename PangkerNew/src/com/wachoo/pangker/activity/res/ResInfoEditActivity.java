package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.msg.MsgLoactionActivity;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.ResInfo;
import com.wachoo.pangker.server.response.ResInfoQueryByIDResult;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

/**
 * @TODO
 * @author zhengjy 2013-3-18
 */
public class ResInfoEditActivity extends CommonPopActivity implements IUICallBackInterface {

	private final int RESINFO_QUERY = 0x10;
	private final int RESINFO_EDIT = 0x11;
	private ImageView ivResIcon; // 资源图标
	private EditText txtName; // 资源名称
	private Button btnClear;
	private CheckBox cbIfshare; // 是否分享
	private EditText etDetail; // 描述
	private EditText tvAuthor;// 演唱者
	private LinearLayout typeLayout;// 类别
	private Button btnType;
	private TextView txtType;
	private TextView txtSize;
	private Button btnUpload; // 确定修改
	private String[] queTypesKey;
	private String[] resTypesVaule;
	private int resType = 0;

	private LinearLayout locationLayout;
	private TextView tv_pic_location;
	private double ctrlon = 0;
	private double ctrlat = 0;

	private ResShowEntity mResEntity;
	private String mUserid;

	private ServerSupportManager serverMana;// 后台交互
	private PangkerApplication mApplication;
	private PoiaddressLoader poiaddressLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.resinfo_edit_layout);
		mApplication = (PangkerApplication) this.getApplication();
		mUserid = mApplication.getMyUserID();
		mResEntity = (ResShowEntity) this.getIntent().getSerializableExtra("resShowEntity");
		serverMana = new ServerSupportManager(this, this);
		poiaddressLoader = new PoiaddressLoader(getApplicationContext());
		initView();

		init();

		getResInfo();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(this);
		if (id == 1) {
			builder.setSingleChoiceItems(queTypesKey, resType, dlistener);
			return builder.create();
		}
		return super.onCreateDialog(id);
	}

	DialogInterface.OnClickListener dlistener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			txtType.setText(queTypesKey[which]);
			resType = which;
			dialog.dismiss();
		}
	};

	private void init() {
		// TODO Auto-generated method stub
		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);

		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);

		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		TextView txtLabel = (TextView) findViewById(R.id.tv_label);
		TextView author = (TextView) findViewById(R.id.res_author);
		// 文档封面，根据格式
		if (mResEntity.getResType() == PangkerConstant.RES_DOCUMENT) {
			queTypesKey = getResources().getStringArray(R.array.res_doc_type_key);
			resTypesVaule = getResources().getStringArray(R.array.res_doc_type_value);
			tvTitle.setText(R.string.res_title_editdoc);
			txtLabel.setHint("文档类型:");
			author.setText("作者：");
			author.setVisibility(View.VISIBLE);
			tvAuthor.setVisibility(View.VISIBLE);
			tvAuthor.setEnabled(true);
			ivResIcon.setImageResource(R.drawable.attachment_doc);
			locationLayout.setVisibility(View.GONE);
		}
		// >>>>>>>>>>>>>音乐资源
		else if (mResEntity.getResType() == PangkerConstant.RES_MUSIC) {
			queTypesKey = getResources().getStringArray(R.array.res_music_type);
			resTypesVaule = getResources().getStringArray(R.array.res_music_type_value);
			tvTitle.setText(R.string.res_title_editmusic);
			txtLabel.setHint("音乐类型:");
			author.setText("演唱者：");
			author.setVisibility(View.VISIBLE);
			tvAuthor.setVisibility(View.VISIBLE);
			tvAuthor.setEnabled(true);
			String iconUrl = Configuration.getCoverDownload() + mResEntity.getResID();
			mImageWorker.loadImage(iconUrl, ivResIcon, String.valueOf(mResEntity.getResID()));
			locationLayout.setVisibility(View.GONE);
		}
		// 应用资源
		else if (mResEntity.getResType() == PangkerConstant.RES_APPLICATION) {
			queTypesKey = getResources().getStringArray(R.array.res_app_type_key);
			resTypesVaule = getResources().getStringArray(R.array.res_app_type_value);
			tvTitle.setText(R.string.res_title_editapp);
			txtLabel.setHint("应用类型:");
			tvAuthor.setVisibility(View.GONE);
			tvAuthor.setEnabled(false);
			author.setVisibility(View.GONE);
			String iconUrl = Configuration.getCoverDownload() + mResEntity.getResID();
			mImageWorker.loadImage(iconUrl, ivResIcon, String.valueOf(mResEntity.getResID()));
			locationLayout.setVisibility(View.GONE);
		}
		// 图片资源
		else if (mResEntity.getResType() == PangkerConstant.RES_PICTURE) {
			queTypesKey = getResources().getStringArray(R.array.res_pic_type);
			resTypesVaule = getResources().getStringArray(R.array.res_pic_value);
			tvTitle.setText(R.string.res_title_editpic);
			txtLabel.setHint("图片类型:");
			tvAuthor.setVisibility(View.GONE);
			tvAuthor.setEnabled(false);
			author.setVisibility(View.GONE);
			int sideLength = ImageUtil.getPicPreviewResolution(this);
			String IconUrl = ImageUtil.getWebImageURL(mResEntity.getResID(), sideLength);
			mImageWorker.loadImage(IconUrl, ivResIcon, String.valueOf(mResEntity.getResID()) + "_" + sideLength);
			locationLayout.setVisibility(View.VISIBLE);
		}

		txtType.setText(queTypesKey[0]);
		txtName.setText(mResEntity.getResName());
	}

	// 根据资源Id获取资源信息
	private void getResInfo() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", mUserid));
		paras.add(new Parameter("resid", String.valueOf(mResEntity.getResID())));
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getResInfoQueryByID(), paras, true, mess, RESINFO_QUERY);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	private void initView() {
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideSoftInput(v);
				ResInfoEditActivity.this.onBackPressed();
			}
		});

		btnUpload = (Button) findViewById(R.id.iv_more);
		btnUpload.setText("修改");
		tvAuthor = (EditText) findViewById(R.id.tv_author_name);
		tvAuthor.setEnabled(false);
		tvAuthor.setVisibility(View.VISIBLE);

		txtSize = (TextView) findViewById(R.id.tv_app_size);
		// btnUpload.setTextColor(R.color.white);
		btnUpload.setBackgroundResource(R.drawable.btn_default_selector);
		btnUpload.setOnClickListener(mClickListener);

		ivResIcon = (ImageView) findViewById(R.id.iv_resicon);
		ivResIcon.setImageResource(R.drawable.icon46_music);
		txtName = (EditText) findViewById(R.id.et_file_name);
		btnClear = (Button) findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(mClickListener);

		cbIfshare = (CheckBox) findViewById(R.id.cb_ifshare); // 是否分享音乐
		cbIfshare.setChecked(mResEntity.isIfshare());

		etDetail = (EditText) findViewById(R.id.et_apk_detail); // 音乐描述
		etDetail.setText(R.string.res_upload_music_detail);

		typeLayout = (LinearLayout) findViewById(R.id.type_layout);
		typeLayout.setOnClickListener(mClickListener);
		txtType = (TextView) findViewById(R.id.tv_type);
		btnType = (Button) findViewById(R.id.btn_type_select);
		btnType.setOnClickListener(mClickListener);

		tv_pic_location = (TextView) findViewById(R.id.tv_pic_location);
		locationLayout = (LinearLayout) findViewById(R.id.location_layout);
		locationLayout.setOnClickListener(mClickListener);
	}

	View.OnClickListener mClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.iv_more:
				// 修改
				resEditSave();
				break;
			case R.id.uploadLayout:
				break;
			case R.id.btn_clear:
				txtName.setText("");
				break;
			case R.id.type_layout:
				showDialog(1);
				break;
			case R.id.btn_type_select:
				showDialog(1);
				break;
			case R.id.location_layout:
				// 这里跳转到选择地理位置的界面
				Intent intent = new Intent();
				intent.setClass(ResInfoEditActivity.this, MsgLoactionActivity.class);
				intent.putExtra("msg_type", 1);
				startActivityForResult(intent, 0x11);
				break;
			}
		}
	};

	private int getTypeIndex(String restype) {
		// TODO Auto-generated method stub
		if (Util.isEmpty(restype)) {
			return 0;
		}
		for (int i = 0; i < resTypesVaule.length; i++) {
			if (restype.equals(resTypesVaule[i])) {
				return i;
			}
		}
		return 0;
	}

	private void showDesc(ResInfo resinfo) {
		// TODO Auto-generated method stub
		txtName.setText(resinfo.getResName());
		String fileSize = Formatter.formatFileSize(this, resinfo.getFileSize());
		txtSize.setText(fileSize);

		switch (mResEntity.getResType()) {
		case PangkerConstant.RES_DOCUMENT:
			etDetail.setText(resinfo.getDocDesc());
			if (!Util.isEmpty(resinfo.docDesc)) {
				etDetail.setText(resinfo.docDesc);
			}
			tvAuthor.setText(resinfo.getDocAuthor() != null ? resinfo.getDocAuthor() : "未知");
			resType = getTypeIndex(resinfo.getDocType().toString());
			txtType.setText(queTypesKey[resType]);
			break;
		case PangkerConstant.RES_MUSIC:
			etDetail.setText(resinfo.getMusicdesc());
			tvAuthor.setText(resinfo.getSinger() != null ? resinfo.getSinger() : "未知");
			if (!Util.isEmpty(resinfo.musicdesc)) {
				etDetail.setText(resinfo.musicdesc);
			}
			resType = getTypeIndex(resinfo.getMusicType().toString());
			txtType.setText(queTypesKey[resType]);
			break;
		case PangkerConstant.RES_APPLICATION:
			etDetail.setText(resinfo.getAppdesc());
			if (!Util.isEmpty(resinfo.appdesc)) {
				etDetail.setText(resinfo.appdesc);
			}
			resType = getTypeIndex(resinfo.getApptype().toString());
			txtType.setText(queTypesKey[resType]);
			break;
		case PangkerConstant.RES_PICTURE:
			if (!Util.isEmpty(resinfo.getPicDesc())) {
				etDetail.setText(resinfo.getPicDesc());
			}
			resType = getTypeIndex(resinfo.getPicType().toString());
			txtType.setText(queTypesKey[resType]);
			poiaddressLoader.onPoiLoader(resinfo.getLocation(), this, tv_pic_location);
			break;
		}
	}

	private void resEditSave() {
		// TODO Auto-generated method stub
		String resName = txtName.getText().toString();
		if (Util.isEmpty(resName)) {
			showToast("资源名称不能为空!");
			return;
		}
		String resDetail = etDetail.getText().toString();
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", mUserid));
		paras.add(new Parameter("resid", String.valueOf(mResEntity.getResID())));
		paras.add(new Parameter("resName", resName));
		if (mResEntity.getResType() == PangkerConstant.RES_DOCUMENT
				|| mResEntity.getResType() == PangkerConstant.RES_MUSIC) {
			paras.add(new Parameter("author", tvAuthor.getText().toString().trim()));
		}
		paras.add(new Parameter("resDesc", resDetail));
		paras.add(new Parameter("type", resTypesVaule[resType]));
		// 如果是收藏的资源，ifshare = 0；modify by Lb,依据张宏光对搜索引擎同步进行修改的
		if (mResEntity.getSource() == 1) {
			paras.add(new Parameter("ifshare", "0"));
		} else {
			paras.add(new Parameter("ifshare", cbIfshare.isChecked() ? "1" : "0"));
		}
		if (ctrlat > 0) {
			paras.add(new Parameter("ctrlat", String.valueOf(ctrlat))); // 23.11747
		}
		if (ctrlon > 0) {
			paras.add(new Parameter("ctrlon", String.valueOf(ctrlon))); // 23.11747
		}

		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getResInfoUpdate(), paras, true, mess, RESINFO_EDIT);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0x11 && resultCode == RESULT_OK) {
			Location location = (Location) data.getSerializableExtra("Location");
			ctrlon = location.getLongitude();
			ctrlat = location.getLatitude();
			tv_pic_location.setText(location.getAddress());
		}
	};

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse)) {
			return;
		}
		if (caseKey == RESINFO_QUERY) {// 获取资源信息
			ResInfoQueryByIDResult result = JSONUtil.fromJson(supportResponse.toString(), ResInfoQueryByIDResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				showDesc(result.getResinfo());
			} else {
				showToast(R.string.to_server_fail);
				this.finish();
			}
		} else if (caseKey == RESINFO_EDIT) {
			BaseResult resEditResult = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (resEditResult != null && resEditResult.getErrorCode() == BaseResult.SUCCESS) {
				showToast("修改成功!");
				this.finish();
			} else if (resEditResult != null && resEditResult.getErrorCode() == BaseResult.FAILED) {
				showToast(resEditResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}
	}

	protected void onDestroy() {
		mImageCache.removeMemoryCaches();
		super.onDestroy();
	};
}
