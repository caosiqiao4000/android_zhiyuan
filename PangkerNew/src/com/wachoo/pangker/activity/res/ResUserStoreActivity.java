package com.wachoo.pangker.activity.res;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.group.ResGroupShareActivity;
import com.wachoo.pangker.adapter.ResAdapter;
import com.wachoo.pangker.adapter.UserStorePhotoAdapter;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.ResInfo;
import com.wachoo.pangker.server.response.ResSiteQueryResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshGridView;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PasswordDialog;
import com.wachoo.pangker.util.Util;

/**
 * @author zxx 说明 1：在前台只有两层资源，第一层以类型区分，包含目录和资源;第二层以目录id区分，不包含资源. 2:
 *         返回上一层目录时，当前的目录dirId就是"0",以类别进行显示，
 * 
 *         修改2013年2月28日10:32:07@zxx， 添加一个本地文件浏览的，不做任何操作,当然Id必须是本人
 */
public class ResUserStoreActivity extends CommonPopActivity implements IUICallBackInterface {

	// >>>>>>>判断是从那个界面传递
	private int startTag = 0;
	public final int OTHERUSER_RES_STORE = 0;
	public final int SHARE_OWN_RES = 1;
	public static final String START_TYPE = "START_TYPE";

	private final int KEY_SEARCH = 0x132;
	private PangkerApplication application;
	private String myUserID;
	private UserItem uItem;// 查询的用户
	/***********************************************************************************************/
	private DirInfo navDirInfo;// 展示当前所的目录的资源
	private String search_dirId = "0";// 当前查询的目录id，不一定就是用户展示的目录id,与navDirinfo没关系
	private ServerSupportManager serverMana;// 后台交互
	// 第二层资源，现在只能以类型进行区分>>>>lb
	private Map<Integer, List<DirInfo>> netFristDatas = new HashMap<Integer, List<DirInfo>>();
	private Map<Integer, Integer> fristPageIndexs = new HashMap<Integer, Integer>();// >>>>lb
	private Map<Integer, Integer> fristSumDatas = new HashMap<Integer, Integer>();// >>>>lb
	// 第三层资源，不会有目录，以当前目录id为准>>>>wangxin
	private Map<String, List<DirInfo>> netSecondDatas = new HashMap<String, List<DirInfo>>();
	private Map<String, Integer> secondPageIndexs = new HashMap<String, Integer>();// >>>>lb
	private Map<String, Integer> secondSumDatas = new HashMap<String, Integer>();// >>>>lb
	private int pageLength = 12;// >>>>lb

	private TextView txtTitle;
	private Button btnBack;
	private Button btnLocal;
	private RadioGroup radioGroup;
	private int index_type = PangkerConstant.RES_PICTURE;
	private PasswordDialog passwordDialog;

	/***********************************************************************************************/
	private PullToRefreshListView pullToRefreshListView;
	private ListView resListView;
	private FooterView footerView;
	private ResAdapter resAdapter;
	private PullToRefreshGridView pullToRefreshGridView;
	private GridView resGridView;
	private UserStorePhotoAdapter userStorePhotoAdapter;

	private String groupId;
	private String groupName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_net_show);

		groupId = this.getIntent().getStringExtra(UserGroup.GROUPID_KEY);
		groupName = this.getIntent().getStringExtra(UserGroup.GROUP_NAME_KEY);
		init();
		// >>>>>>>>判断启动模式
		// >>>>>>>>>>> 0:访问其他用户资源模式 ；1：选择资源分享模式
		startTag = getIntent().getIntExtra(START_TYPE, OTHERUSER_RES_STORE);
		getIntent().getStringExtra(START_TYPE);
		// 加载页面
		initview();
		// 加载数据
		navDirInfo = (DirInfo) radioGroup.getTag(R.id.res_pic);
		goinDirinfo(navDirInfo, null, "1");
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		myUserID = application.getMyUserID();
		serverMana = new ServerSupportManager(this, this);
		uItem = (UserItem) getIntent().getSerializableExtra(PangkerConstant.INTENT_UESRITEM);
		navDirInfo = new DirInfo("0");
		navDirInfo.setResType(PangkerConstant.RES_PICTURE);

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIN);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
	}

	/**
	 * @param dirid
	 *            当前的目录id
	 * @param password
	 *            是否输入密码
	 * @param flag_dir
	 *            是否加载目录 1==>加载，0==>不加载
	 */
	private void searchResInfo(String dirid, String password, String flag_dir) {
		this.search_dirId = dirid;
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appuid", myUserID));
		paras.add(new Parameter("uid", uItem.getUserId()));
		paras.add(new Parameter("dirid", search_dirId));
		paras.add(new Parameter("flag", flag_dir));
		paras.add(new Parameter("resType", String.valueOf(index_type)));
		if (password != null) {
			paras.add(new Parameter("password", password));
		}

		int pageIndex = 0;
		if ("0".equals(navDirInfo.getDirId())) {
			if (netFristDatas.get(index_type) == null) {
				if (index_type == PangkerConstant.RES_PICTURE) {
					pullToRefreshGridView.setRefreshing(true);
				} else {
					pullToRefreshListView.setRefreshing(true);
				}
			} else {
				pageIndex = fristPageIndexs.get(index_type);
			}
		} else {
			if (netSecondDatas.get(search_dirId) == null) {
				if (index_type == PangkerConstant.RES_PICTURE) {
					pullToRefreshGridView.setRefreshing(true);
				} else {
					pullToRefreshListView.setRefreshing(true);
				}
			} else {
				pageIndex = secondPageIndexs.get(search_dirId);
			}
		}
		Log.d("remark", "limit:" + (pageIndex * pageLength) + "," + pageLength + "||dirid:" + dirid + "||type:"
				+ index_type);
		paras.add(new Parameter("limit", (pageIndex * pageLength) + "," + pageLength));
		serverMana.supportRequest(Configuration.getResSiteQuery(), paras, KEY_SEARCH);
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			refreshDirinfo();
		}
	};

	private void initview() {
		// 加载顶部视图
		initHeadView();
		// 加载网盘视图
		initNetView();
	}

	private void initHeadView() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(clickListener);
		// >>>>>>>title名称设置
		txtTitle = (TextView) findViewById(R.id.mmtitle);
		if (!myUserID.equals(uItem.getUserId()))
			txtTitle.setText("TA的网盘资源");
		else
			txtTitle.setText("我的网盘资源");
		findViewById(R.id.iv_more).setVisibility(View.GONE);

		radioGroup = (RadioGroup) findViewById(R.id.res_rg);
		radioGroup.setOnCheckedChangeListener(onCheckedChangeListener);
		radioGroup.setTag(R.id.res_read, new DirInfo("0", PangkerConstant.RES_DOCUMENT));
		radioGroup.setTag(R.id.res_pic, new DirInfo("0", PangkerConstant.RES_PICTURE));
		radioGroup.setTag(R.id.res_music, new DirInfo("0", PangkerConstant.RES_MUSIC));
		radioGroup.setTag(R.id.res_app, new DirInfo("0", PangkerConstant.RES_APPLICATION));
	}

	private void initNetView() {
		// TODO Auto-generated method stub
		// 列表显示
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
		// 矩阵显示
		pullToRefreshGridView = (PullToRefreshGridView) findViewById(R.id.pull_refresh_grid);
		// >>>>>>刷新功能
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		// >>>>>>刷新功能
		pullToRefreshGridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<GridView>() {
			@Override
			public void onRefresh(PullToRefreshBase<GridView> refreshView) {
				// TODO Auto-generated method stub
				refreshDirinfo();
			}
		});
		// >>>>>>>获取列表
		resListView = pullToRefreshListView.getRefreshableView();
		// >>>>>>>>>图片的view显示
		resGridView = pullToRefreshGridView.getRefreshableView();
		resGridView.setNumColumns(3);
		resGridView.setGravity(Gravity.CENTER);
		EmptyView gEmptyView = new EmptyView(this);
		gEmptyView.showView(R.drawable.emptylist_icon, "没有图片");
		gEmptyView.addToGridView(resGridView);

		userStorePhotoAdapter = new UserStorePhotoAdapter(this, new ArrayList<DirInfo>(), mImageWorker);
		resGridView.setAdapter(userStorePhotoAdapter);
		userStorePhotoAdapter.setOnClickListener(picOnClickListener);
		userStorePhotoAdapter.setOnLoadMoreListener(onLoadMoreListener);

		footerView = new FooterView(this);
		footerView.addToListView(resListView);
		footerView.setOnLoadMoreListener(onLoadMoreListener);
		EmptyView emptyView = new EmptyView(this);
		emptyView.showView(R.drawable.emptylist_icon, "没有数据");
		emptyView.addToListView(resListView);

		resAdapter = new ResAdapter(this, new ArrayList<DirInfo>(), mImageWorker);
		resAdapter.setIfLock(true);
		resListView.setAdapter(resAdapter);
		resListView.setOnItemClickListener(onItemClickListener);
	}

	OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			switch (checkedId) {
			case R.id.res_read:
				index_type = PangkerConstant.RES_DOCUMENT;
				break;
			case R.id.res_pic:
				index_type = PangkerConstant.RES_PICTURE;
				break;
			case R.id.res_music:
				index_type = PangkerConstant.RES_MUSIC;
				break;
			case R.id.res_app:
				index_type = PangkerConstant.RES_APPLICATION;
				break;
			}
			pullToRefreshListView.onRefreshComplete();
			navDirInfo = (DirInfo) group.getTag(checkedId);
			goinDirinfo(navDirInfo, null, "1");
		}
	};

	private void showPasswordDialog() {
		// TODO Auto-generated method stub
		if (passwordDialog == null) {
			passwordDialog = new PasswordDialog(this, R.style.MyDialog);
			passwordDialog.setResultCallback(rMsgCallback);
		}
		passwordDialog.setTitle("输入密码");
		passwordDialog.showDialog();
	}

	MessageTipDialog.DialogMsgCallback rMsgCallback = new MessageTipDialog.DialogMsgCallback() {

		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if (passwordDialog.checkPass() && isSubmit) {
				navDirInfo.setPassword(passwordDialog.getPassword());
				goinDirinfo(navDirInfo, passwordDialog.getPassword(), "0");
			}
		}
	};

	View.OnClickListener onClickListener2 = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (passwordDialog.checkPass()) {
				navDirInfo.setPassword(passwordDialog.getPassword());
				goinDirinfo(navDirInfo, passwordDialog.getPassword(), "0");
				passwordDialog.dismiss();
			}
		}
	};
	/**
	 * Pull to refresh,if getCount() < pageIdex * pageLength can not to service,
	 * sametime adjust dirId;
	 */
	private FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			if (navDirInfo != null) {
				searchResInfo(navDirInfo.getDirId(), navDirInfo.getPassword(), "0");
			} else {
				footerView.hideFoot();
			}
		}
	};

	// >>>>>>>>>>>>>其他用户资源单击事件
	private void enterDir(DirInfo dirInfo, int position) {
		// TODO Auto-generated method stub
		if (dirInfo.getIsfile() == 1) {
			// >>>>>>>判断启动模式 0:查看其他用户资源库
			if (startTag == OTHERUSER_RES_STORE) {
				lookDirInfo(dirInfo, position);
			}
			// >>>>>>>>>1：选择自己要分享的资源
			else {
				shareRes(dirInfo);
			}

		} else if (dirInfo.getIsfile() == 0) {
			navDirInfo = dirInfo;
			goinDirinfo(dirInfo, dirInfo.getPassword(), "0");
		} else {
			backDirinfo();
		}
	}

	private void toLocalResManager() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, ResLocalManagerActivity.class);
		intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.upLoad);

		if (groupId != null && groupName != null) {
			intent.putExtra(UserGroup.GROUPID_KEY, groupId);
			intent.putExtra(UserGroup.GROUP_NAME_KEY, groupName);
		}
		startActivity(intent);
		finish();
	}

	// >>>>>>>>>>>分享资源
	private void shareRes(DirInfo dirInfo) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra(ResShowEntity.RES_ENTITY, toResShowEntity(dirInfo));
		if (groupId != null && groupName != null) {
			intent.putExtra(UserGroup.GROUPID_KEY, groupId);
			intent.putExtra(UserGroup.GROUP_NAME_KEY, groupName);
		}
		intent.setClass(this, ResGroupShareActivity.class);
		startActivity(intent);
		finish();
	}

	/**
	 * 图片的点击事情，以及长按事件
	 */
	private View.OnClickListener picOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Integer index = (Integer) v.getTag(R.id.tag_index);
			DirInfo dirinfo = (DirInfo) v.getTag(R.id.tag_shareinfo);
			enterDir(dirinfo, index);
		}
	};

	/**
	 * 列表项短按监听器
	 */
	AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (index_type != PangkerConstant.RES_PICTURE) {
				DirInfo dirinfo = (DirInfo) parent.getItemAtPosition(position);
				enterDir(dirinfo, position - 1);
			}
		}
	};

	View.OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				finish();
			}
			// 打开本地资源
			if (v == btnLocal) {
				toLocalResManager();
			}
		}
	};

	private void refreshDirinfo() {
		// TODO Auto-generated method stub
		if ("0".equals(navDirInfo.getDirId())) {
			netFristDatas.put(index_type, null);
			fristPageIndexs.put(index_type, 0);
			fristSumDatas.remove(index_type);
		} else {
			netSecondDatas.put(navDirInfo.getDirId(), null);
			secondPageIndexs.put(navDirInfo.getDirId(), 0);
			secondSumDatas.remove(navDirInfo.getDirId());
		}
		String flag = "0"; // 0 search dir
		if ("0".equals(navDirInfo.getDirId())) {
			flag = "1";
		}
		searchResInfo(navDirInfo.getDirId(), navDirInfo.getPassword(), flag);
	}

	/**
	 * 正对图片的加载更多
	 * 
	 * @param count
	 * @param sum
	 * @return
	 */
	private boolean isLoadMore(int count, int sum) {
		if (count < pageLength) {
			return false;
		}
		if (count >= sum) {
			return false;
		}
		return true;
	}

	// 进入当前目录下，当前目录,首先 对目录的权限进行判断
	/**
	 * 
	 */
	private void goinDirinfo(DirInfo tag, String password, String flag) {
		// TODO Auto-generated method stub
		if (password == null && (tag.getVisitType() == PangkerConstant.Role_PASSWORD)) {
			showPasswordDialog();
			return;
		}

		if (index_type == PangkerConstant.RES_PICTURE) {
			// picAdapter.setResinfos(new ArrayList<DirInfo>());
			pullToRefreshListView.setVisibility(View.GONE);
			pullToRefreshGridView.setVisibility(View.VISIBLE);
		} else {
			pullToRefreshListView.setVisibility(View.VISIBLE);
			pullToRefreshGridView.setVisibility(View.GONE);

			resListView.setDividerHeight(1);
		}

		if ("0".equals(tag.getDirId())) {
			if (fristPageIndexs.get(tag.getResType()) == null) {
				fristPageIndexs.put(tag.getResType(), 0);
			}
			if (netFristDatas.get(index_type) != null) {
				if (index_type == PangkerConstant.RES_PICTURE) {
					int sum = fristSumDatas.get(index_type);
					int count = netFristDatas.get(index_type).size();
					userStorePhotoAdapter.setUserResinfos(netFristDatas.get(index_type), navDirInfo,
							isLoadMore(count, sum));
				} else {
					resAdapter.setUserResInfos(netFristDatas.get(index_type), navDirInfo);
					int sum = fristSumDatas.get(index_type);
					footerView.onLoadComplete(resAdapter.getSize(), sum);
				}
				return;
			}
		} else {
			if (secondPageIndexs.get(tag.getDirId()) == null) {
				secondPageIndexs.put(tag.getDirId(), 0);
			}
			if (netSecondDatas.get(tag.getDirId()) != null) {
				if (index_type == PangkerConstant.RES_PICTURE) {
					int sum = secondSumDatas.get(tag.getDirId());
					int count = netSecondDatas.get(tag.getDirId()).size();
					userStorePhotoAdapter.setUserResinfos(netSecondDatas.get(tag.getDirId()), navDirInfo,
							isLoadMore(count, sum));
				} else {
					resAdapter.setUserResInfos(netSecondDatas.get(tag.getDirId()), navDirInfo);
					int sum = secondSumDatas.get(tag.getDirId());
					footerView.onLoadComplete(resAdapter.getSize(), sum);
				}
				return;
			}
		}
		searchResInfo(tag.getDirId(), password, flag);
	}

	// 返回上一层目录，只按类型加载.
	private void backDirinfo() {
		// TODO Auto-generated method stub
		if (index_type == PangkerConstant.RES_PICTURE) {
			int sum = fristSumDatas.get(index_type);
			int count = netFristDatas.get(index_type).size();
			userStorePhotoAdapter.setUserResinfos(netFristDatas.get(index_type), null, isLoadMore(count, sum));
			footerView.onLoadComplete(userStorePhotoAdapter.getSize(), fristSumDatas.get(index_type));
		} else {
			resAdapter.setUserResInfos(netFristDatas.get(index_type), null);
			footerView.onLoadComplete(resAdapter.getSize(), fristSumDatas.get(index_type));
		}
		switch (index_type) {
		case PangkerConstant.RES_PICTURE:
			navDirInfo = (DirInfo) radioGroup.getTag(R.id.res_pic);
			break;

		case PangkerConstant.RES_DOCUMENT:
			navDirInfo = (DirInfo) radioGroup.getTag(R.id.res_read);
			break;
		case PangkerConstant.RES_APPLICATION:
			navDirInfo = (DirInfo) radioGroup.getTag(R.id.res_app);
			break;
		case PangkerConstant.RES_MUSIC:
			navDirInfo = (DirInfo) radioGroup.getTag(R.id.res_music);
			break;
		}

	}

	private ResShowEntity toResShowEntity(DirInfo resinfo) {
		// TODO Auto-generated method stub
		ResShowEntity entity = new ResShowEntity();
		entity.setCommentTimes(resinfo.getCommentTimes());
		entity.setShareUid(uItem.getUserId());
		entity.setResType(resinfo.getResType());
		entity.setDownloadTimes(resinfo.getDownloadTimes());
		entity.setFavorTimes(resinfo.getFavorTimes());
		entity.setResID(Long.valueOf(resinfo.getDirId()));
		entity.setScore(Integer.parseInt(String.valueOf(resinfo.getScore())));
		entity.setResName(resinfo.getDirName());
		return entity;
	}

	// 进入资源的详细信息界面 ,要过滤用户没法查看的资源
	private void lookDirInfo(DirInfo tag, int index) {
		// TODO Auto-generated method stub
		// Log.d("resShowEntity", "index===:" + index);
		if (tag.getDeal() != 1) {
			showToast("用户没分享,无法查看!");
			return;
		}
		List<ResShowEntity> showList = new ArrayList<ResShowEntity>();
		List<DirInfo> navAllDirInfos = null;
		if ("0".equals(navDirInfo.getDirId())) {
			navAllDirInfos = netFristDatas.get(index_type);
		} else {
			navAllDirInfos = netSecondDatas.get(navDirInfo.getDirId());
		}
		int uncount = 0;
		for (int i = 0; i < navAllDirInfos.size(); i++) {
			DirInfo resinfo = navAllDirInfos.get(i);
			if (resinfo.getIsfile() != 1 || resinfo.getDeal() != 1) {
				if (i < index) {
					uncount++;
				}
				continue;
			}
			showList.add(toResShowEntity(resinfo));
		}
		index -= uncount;
		application.setResLists(showList);
		Intent intent = new Intent();
		application.setViewIndex(index);

		if (tag.getResType() == PangkerConstant.RES_PICTURE) {
			intent.setClass(this, PictureInfoActivity.class);
		} else {
			intent.setClass(this, ResInfoActivity.class);
		}
		startActivity(intent);

	}

	/**
	 * 判断是否在相应Dirid获取过数据了。
	 * 
	 * @param result
	 * @return
	 */
	private boolean isCallbacked(DirInfo result, String resule_dirId) {
		int type = result.getResType();
		Iterator<DirInfo> iterator = null;
		if ("0".equals(resule_dirId)) {
			if (netFristDatas.get(type) == null) {
				return false;
			}
			iterator = netFristDatas.get(type).iterator();
		} else {
			if (netSecondDatas.get(resule_dirId) == null) {
				return false;
			}
			iterator = netSecondDatas.get(resule_dirId).iterator();
		}
		while (iterator.hasNext()) {
			DirInfo item = iterator.next();
			if (result.getDirId().equals(item.getDirId())) {
				return true;
			}
		}
		return false;
	}

	private void saveToCache(List<DirInfo> data, String id, int type, int count) {
		// TODO Auto-generated method stub
		// 将结果保存在缓存中,同时更改其他属性
		if ("0".equals(id)) {
			int index = fristPageIndexs.get(type) + 1;
			fristPageIndexs.put(type, index);
			if (netFristDatas.get(type) == null) {
				netFristDatas.put(type, data);
			} else {
				netFristDatas.get(type).addAll(data);
			}
			if (!fristSumDatas.containsKey(type)) {
				fristSumDatas.put(type, count);
			}
		} else {
			int index = secondPageIndexs.get(id) + 1;
			secondPageIndexs.put(id, index);
			if (netSecondDatas.get(id) == null) {
				netSecondDatas.put(id, data);
			} else {
				netSecondDatas.get(id).addAll(data);
			}
			if (!secondSumDatas.containsKey(id)) {
				secondSumDatas.put(id, count);
			}
		}
	}

	/**
	 * 预先处理返回结果，1：看返回的Id和查询的Id，以及类型是否一致;
	 * 2：查看缓存中的是否已经存在该数据了，如果有就属于重复查询，没有，保存在缓存中.
	 * 
	 * @param result
	 */
	private void handleResult(ResSiteQueryResult result) {
		// TODO Auto-generated method stub
		int result_type = result.getResType();
		String result_dirId = result.getDirid();
		int dirCount = 0;
		List<DirInfo> allResultDirInfos = new ArrayList<DirInfo>();
		if (result.getDirinfos() != null && result.getDirinfos().size() > 0) {
			allResultDirInfos.addAll(result.getDirinfos());
			dirCount = result.getDirinfos().size();
		}
		List<DirInfo> resInfos = changRes(result.getResinfos());
		allResultDirInfos.addAll(resInfos);
		if (allResultDirInfos.size() > 0) {
			DirInfo first = allResultDirInfos.get(0);
			if (!isCallbacked(first, result_dirId)) {
				saveToCache(allResultDirInfos, result_dirId, result_type, result.getTotalcount() + dirCount);
			}
		} else {
			saveToCache(allResultDirInfos, result_dirId, index_type, 0);
		}
		// 对数据进行显示，对此要做判断
		if (isCanshow(result_dirId, result_type)) {
			// 如果有密码的，输入密码成功之后，将其修改成公共的
			if (navDirInfo.getVisitType() == PangkerConstant.Role_PASSWORD) {
				navDirInfo.setVisitType(PangkerConstant.Role_PUBLIC);
			}
			if ("0".equals(search_dirId)) {
				if (index_type == PangkerConstant.RES_PICTURE) {
					int sum = fristSumDatas.get(index_type);
					int count = netFristDatas.get(index_type).size();

					userStorePhotoAdapter.setUserResinfos(netFristDatas.get(index_type), navDirInfo,
							isLoadMore(count, sum));
				} else {
					resAdapter.setUserResInfos(netFristDatas.get(index_type), navDirInfo);
					footerView.onLoadComplete(resAdapter.getSize(), fristSumDatas.get(index_type));
				}
			} else {
				if (index_type == PangkerConstant.RES_PICTURE) {
					int sum = secondSumDatas.get(search_dirId);
					int count = netSecondDatas.get(search_dirId).size();
					userStorePhotoAdapter.setUserResinfos(netSecondDatas.get(search_dirId), navDirInfo,
							isLoadMore(count, sum));
				} else {
					resAdapter.setUserResInfos(netSecondDatas.get(search_dirId), navDirInfo);
					footerView.onLoadComplete(resAdapter.getSize(), secondSumDatas.get(search_dirId));
				}
			}
		}
	}

	boolean isCanshow(String dirId, int type) {
		if ("0".equals(dirId)) {
			return type == index_type;
		} else {
			if (type == 0) {
				return this.search_dirId.equals(dirId);
			}
			return (this.search_dirId.equals(dirId) && type == index_type);
		}
	}

	private List<DirInfo> changRes(List<ResInfo> resinfos) {
		List<DirInfo> resInfos = new ArrayList<DirInfo>();
		if (resinfos != null) {
			for (ResInfo resinfo : resinfos) {
				resInfos.add(transMovingRes(resinfo));
			}
		}
		return resInfos;
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		pullToRefreshListView.onRefreshComplete();
		pullToRefreshGridView.onRefreshComplete();
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			footerView.onLoadCompleteErr();
			return;
		}

		if (caseKey == KEY_SEARCH) {// 如果失败，那么要返回上一层的资源信息
			ResSiteQueryResult result = JSONUtil.fromJson(response.toString(), ResSiteQueryResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				handleResult(result);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
				navDirInfo.setPassword(null);
			}
		}
	}

	private DirInfo transMovingRes(ResInfo resInfo) {
		DirInfo dirinfo = new DirInfo();
		dirinfo.setDirName(resInfo.resName);
		dirinfo.setDirId(String.valueOf(resInfo.getSid()));
		dirinfo.setParentDirId(navDirInfo.getDirId());
		dirinfo.setDeal(resInfo.getIfShare());
		dirinfo.setResType(Integer.parseInt(resInfo.getType()));
		String path = Configuration.getCoverDownload() + resInfo.getSid();
		dirinfo.setPath(path);
		double szie = resInfo.getFileSize();
		dirinfo.setFileCount((int) szie);
		dirinfo.setIsfile(1);
		dirinfo.setAppraisement(Util.TimeIntervalBtwNow(resInfo.getIssuetime()));
		dirinfo.setShareTimes(resInfo.getShareTimes());
		dirinfo.setCommentTimes(resInfo.getCommentTimes());
		dirinfo.setDownloadTimes(resInfo.getDownloadTimes());
		dirinfo.setFavorTimes(resInfo.getFavorTimes());
		dirinfo.setScore(Integer.parseInt(String.valueOf(resInfo.getScore())));
		dirinfo.setSource(resInfo.getResSource());
		return dirinfo;
	}

}
