package com.wachoo.pangker.activity;

import java.io.File;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.ui.dialog.WaitingDialog;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class PicturePreviewActivity extends CommonPopActivity {
	private static final String TAG = "PhotopreviewActivity";

	private static final int quality_message = 80; // 清晰度
	private static final int quality_upload = 80; // 清晰度
	private static final int MAX_PHOTO_SIZE = 20 * 1024; // 文件最大限制

	private static final int MAX_PHOTO_SIDE_UPLOAD = 1100;
	private static final int MIN_PHOTO_SIDE_UPLOAD = 1100;

	private static final int MAX_PHOTO_SIDE_MESSAGE = 700;
	private static final int MIN_PHOTO_SIDE_MESSAGE = 700;

	private int quality = 80;
	private int MAX_PHOTO_SIDE = 1000;
	private int MIN_PHOTO_SIDE = 1000;

	// 图片处理
	private Button btn_original;// 原始
	private Button btn_blur;// 柔化
	private Button btn_emboss; // 浮雕
	private Button btn_sunshine;// 光照
	private Button btn_reminiscence; // 怀旧
	private Button btn_grays;// 黑白效果

	private Button btnConfirm;
	private Button btnBack;

	private Button btnCompression;// >>>压缩按钮.只有在上传图片的时候显示。私信聊天的时候不显示

	private TextView txtTitle;
	private TextView txtFileSize;
	private ImageView img_preview;

	private Bitmap bitmap_processed; // 显示位图
	private Bitmap bitmap_original;// 原始位图

	private int intentFrom = 0; // 判断是从哪里来的意图

	private String photoPath;// 原始图片地址
	private String photoName;
	private String photoPathTemp;// 处理过的图片地址（包括缩小分辨率、效果处理）
	private String usePhotoPath;// 用户选择使用的图片地址

	private File photoFile;

	private boolean isProcessed = false;// 是否处理过图片
	private boolean isScaleed = false;// 是否缩小过图片的分辨率
	private boolean isCompression = false; // 是否压缩过图片

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.photo_preview);

		photoPath = getIntent().getStringExtra("photo_path");

		intentFrom = getIntent().getIntExtra("photo_type", 0);

		if (intentFrom == 0) {
			quality = quality_message;
			MAX_PHOTO_SIDE = MAX_PHOTO_SIDE_MESSAGE;
			MIN_PHOTO_SIDE = MIN_PHOTO_SIDE_MESSAGE;
			photoPathTemp = PangkerConstant.DIR_SENDPIC;
			isCompression = true;

		} else {
			quality = quality_upload;
			MAX_PHOTO_SIDE = MAX_PHOTO_SIDE_UPLOAD;
			MIN_PHOTO_SIDE = MIN_PHOTO_SIDE_UPLOAD;
			photoPathTemp = PangkerConstant.DIR_UPLOADPIC + "/";

		}

		photoName = Util.getNowTime() + ".jpg";

		initView();
		compressPhotoFile();
	}

	// public Bitmap decodeFile(String path) {// you can provide file path here
	// int orientation;
	// try {
	// if (path == null) {
	// return null;
	// }
	// // decode image size
	// BitmapFactory.Options o = new BitmapFactory.Options();
	// o.inJustDecodeBounds = true;
	// // Find the correct scale value. It should be the power of 2.
	// final int REQUIRED_SIZE = 70;
	// int width_tmp = o.outWidth, height_tmp = o.outHeight;
	// int scale = 0;
	// while (true) {
	// if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
	// break;
	// width_tmp /= 2;
	// height_tmp /= 2;
	// scale++;
	// }
	// // decode with inSampleSize
	// BitmapFactory.Options o2 = new BitmapFactory.Options();
	// o2.inSampleSize = scale;
	// Bitmap bm = BitmapFactory.decodeFile(path, o2);
	// Bitmap bitmap = bm;
	//
	// ExifInterface exif = new ExifInterface(path);
	//
	// orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
	//
	// Log.e("ExifInteface .........", "rotation =" + orientation);
	//
	// // exif.setAttribute(ExifInterface.ORIENTATION_ROTATE_90, 90);
	//
	// Log.e("orientation", "" + orientation);
	// Matrix m = new Matrix();
	//
	// if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
	// m.postRotate(180);
	// // m.postScale((float) bm.getWidth(), (float) bm.getHeight());
	// // if(m.preRotate(90)){
	// Log.e("in orientation", "" + orientation);
	// bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m,
	// true);
	// return bitmap;
	// } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
	// m.postRotate(90);
	// Log.e("in orientation", "" + orientation);
	// bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m,
	// true);
	// return bitmap;
	// } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
	// m.postRotate(270);
	// Log.e("in orientation", "" + orientation);
	// bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m,
	// true);
	// return bitmap;
	// }
	// return bitmap;
	// } catch (Exception e) {
	// return null;
	// }
	// }

	private void initView() {
		// TODO Auto-generated method stub
		try {
			// >>>>>>>>>>上传图片case
			if (intentFrom != 0) {
				// >>压缩按钮
				btnCompression = (Button) findViewById(R.id.btn_compression);
				btnCompression.setVisibility(View.VISIBLE);
				btnCompression.setText("压缩");
				isCompression = false;// 默认不压缩图片
				btnCompression.setOnClickListener(clickListener);
				txtFileSize = (TextView) findViewById(R.id.tv_file_size);
				txtFileSize.setVisibility(View.VISIBLE);
				String fileSize = Formatter.formatFileSize(this, Util.getFileSize(new File(photoPath)));
				Log.i(TAG, ">>>photoSize=" + fileSize);
				txtFileSize.setText("文件大小：" + fileSize);
			}
			btnBack = (Button) findViewById(R.id.btn_back);
			btn_blur = (Button) findViewById(R.id.btn_blur);
			btn_grays = (Button) findViewById(R.id.btn_grays);
			btn_emboss = (Button) findViewById(R.id.btn_emboss);
			btn_original = (Button) findViewById(R.id.btn_original);
			btn_sunshine = (Button) findViewById(R.id.btn_sunshine);
			btn_reminiscence = (Button) findViewById(R.id.btn_reminiscence);

			btnBack.setOnClickListener(clickListener);
			btn_blur.setOnClickListener(clickListener);
			btn_grays.setOnClickListener(clickListener);
			btn_emboss.setOnClickListener(clickListener);
			btn_original.setOnClickListener(clickListener);
			btn_sunshine.setOnClickListener(clickListener);
			btn_reminiscence.setOnClickListener(clickListener);

			txtTitle = (TextView) findViewById(R.id.mmtitle);
			txtTitle.setText(R.string.photo_preview_process);

			btnConfirm = (Button) findViewById(R.id.iv_more);
			btnConfirm.setOnClickListener(clickListener);
			btnConfirm.setBackgroundResource(R.drawable.btn_default_selector);
			btnConfirm.setText(R.string.button_confim);

			img_preview = (ImageView) findViewById(R.id.img_preview);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			try {
				if (v == btn_original) {
					isProcessed = false; // 设置未处理
					img_preview.setImageBitmap(bitmap_original);
					isCompression = false;
					btnCompression.setText("压缩");
					String fileSize = Formatter.formatFileSize(PicturePreviewActivity.this,
							Util.getFileSize(new File(photoPath)));
					txtFileSize.setText("文件大小：" + fileSize);
				}
				// >>>>>>美化效果按钮
				else if (v == btn_blur || v == btn_emboss || v == btn_sunshine || v == btn_reminiscence
						|| v == btn_grays) {
					isProcessed = true;// 设置处理过
					new PhotoProcessAsyncTask().execute(v.getId());
				}
				// >>>>>> back button ;finish
				else if (v == btnBack) {
					finish();
				}
				// >>>>>>>confirm button
				else if (v == btnConfirm) {
					img_preview.setImageBitmap(null);
					saveProcessFile();
					sendProcessFile();
					// >>>>>>>>释放资源
					if (bitmap_processed != null && !bitmap_processed.isRecycled()) {
						bitmap_processed.recycle();
					}
					if (bitmap_original != null && !bitmap_original.isRecycled()) {
						bitmap_original.recycle();
					}
				}
				// >>>>>>compression button :compression the photo
				else if (v == btnCompression) {

					if (isProcessed)
						return;
					if (isCompression) {
						isCompression = false;
						btnCompression.setText("压缩");
						String fileSize = Formatter.formatFileSize(PicturePreviewActivity.this,
								Util.getFileSize(new File(photoPath)));
						txtFileSize.setText("文件大小：" + fileSize);
						Log.i(TAG, ">>>photoSize=" + fileSize);
					} else {
						isCompression = true;
						btnCompression.setText("取消压缩");
						usePhotoPath = photoPathTemp + photoName;
						setPhotoSize(bitmap_original);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	};

	// >>>>>>>>>查看图片大小
	private void setPhotoSize(Bitmap bitmap) {
		String fileSize = Formatter.formatFileSize(PicturePreviewActivity.this,
				(long) (ImageUtil.getBitmapSize(bitmap, quality)));
		txtFileSize.setText("文件大小：" + fileSize);
		Log.i(TAG, ">>>photoSize=" + fileSize);
	}

	// >>>>>>>>保存图片
	private void saveProcessFile() {

		// >>>>如果美化过图片
		if (isProcessed) {
			ImageUtil.saveFileAndRecycle(photoPathTemp, photoName, bitmap_processed, quality);
			usePhotoPath = photoPathTemp + photoName;
		}
		// >>>>如果没有压缩，也没有美化过图片
		else if (!isCompression) {
			usePhotoPath = photoPath;
		}
		// >>>>上传压缩图片
		else if (isScaleed) {
			ImageUtil.saveFileAndRecycle(photoPathTemp, photoName, bitmap_original, quality);
			usePhotoPath = photoPathTemp + photoName;
		}
		// >>>>>图片较小，不需要做任何处理的case
		else {
			usePhotoPath = photoPath;
		}
	}

	private void compressPhotoFile() {
		// TODO Auto-generated method stub
		try {
			photoFile = new File(photoPath);
			// 判断文件是否存在
			if (photoFile.exists()) {
				// 判断文件大小是否在范围之内
				if (photoFile.length() > MAX_PHOTO_SIZE) {
					isScaleed = true; // 缩小图片分辨率
					// 获取SD卡图片
					bitmap_original = ImageUtil.decodeFile(photoPath, MIN_PHOTO_SIDE, MAX_PHOTO_SIDE * MAX_PHOTO_SIDE);
				} else {
					bitmap_original = ImageUtil.decodeFile(photoPath, MIN_PHOTO_SIDE, MAX_PHOTO_SIDE * MAX_PHOTO_SIDE);
				}
				img_preview.setImageBitmap(bitmap_original);
			}
		} catch (Exception e) {
			return;
		}
	}

	// 删除另外一个不用的文件
	private void sendProcessFile() {
		Intent intent = new Intent();
		Log.i(TAG, "photopath=" + usePhotoPath);
		intent.putExtra(DocInfo.FILE_PATH, usePhotoPath);
		setResult(RESULT_OK, intent);
		this.finish();
	}

	/**
	 * 图片效果处理异步类
	 * 
	 * @author wangxin
	 * 
	 */
	private class PhotoProcessAsyncTask extends AsyncTask<Object, Integer, Bitmap> {
		// ProgressDialog
		private WaitingDialog mWaitingDialog;
		private int process;

		@Override
		protected Bitmap doInBackground(Object... params) {
			// TODO Auto-generated method stub
			process = Integer.parseInt(params[0].toString());
			switch (process) {

			case R.id.btn_blur:// 柔化效果
				bitmap_processed = ImageUtil.blurImageAmeliorate(bitmap_original);
				break;
			case R.id.btn_emboss:// 浮雕效果
				bitmap_processed = ImageUtil.emboss(bitmap_original);
				break;
			case R.id.btn_sunshine:// 光照效果
				bitmap_processed = ImageUtil.sunshine(bitmap_original, bitmap_original.getWidth() / 2,
						bitmap_original.getHeight() / 2);
				break;
			case R.id.btn_reminiscence:// 老相片想过
				bitmap_processed = ImageUtil.oldRemeber(bitmap_original);
				break;
			case R.id.btn_grays:// 黑白效果
				bitmap_processed = ImageUtil.toGrayscale(bitmap_original);

			default:
				break;
			}
			return bitmap_processed;
		}

		@Override
		protected void onPreExecute() {

			mWaitingDialog = new WaitingDialog(PicturePreviewActivity.this);

			mWaitingDialog.setWaitMessage(R.string.loading);

			mWaitingDialog.show();

		}

		@Override
		protected void onPostExecute(Bitmap mBitmap) {
			// TODO Auto-generated method stub
			mWaitingDialog.dismiss();
			super.onPostExecute(mBitmap);
			img_preview.setImageBitmap(mBitmap);
			if (intentFrom != 0) {
				setPhotoSize(mBitmap);
				btnCompression.setText("取消压缩");
			}
		}
	}
}
