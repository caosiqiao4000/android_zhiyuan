package com.wachoo.pangker.activity.group;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @TODO 分享文字信息到群组
 * 
 *       2013-01-14
 * 
 */
public class TextGroupShareActivity extends CommonPopActivity implements IUICallBackInterface {

	private String TAG = "TextGroupShareActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	private final int GROUP_SHARE_KEY = 0x10;
	private PangkerApplication application;
	private EditText etDetail;
	private String mUserId;
	private TextView tvGroupName;

	private String groupId;
	private String groupName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.text_group_share);

		application = (PangkerApplication) getApplication();
		mUserId = application.getMyUserID();
		groupId = this.getIntent().getStringExtra(UserGroup.GROUPID_KEY);
		groupName = this.getIntent().getStringExtra(UserGroup.GROUP_NAME_KEY);

		initView();

		//tvGroupName.setText(groupName);

	}

	private void initView() {
		Button btnRight = (Button) findViewById(R.id.iv_more);
		btnRight.setVisibility(View.GONE);

		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("分享文字到群组");

		etDetail = (EditText) findViewById(R.id.et_detail);

		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
						TextGroupShareActivity.this.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
				TextGroupShareActivity.this.onBackPressed();
			}
		});
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		Button btnSubmit = (Button) findViewById(R.id.btn_share);
		btnSubmit.setVisibility(View.VISIBLE);
		btnSubmit.setText("分享");
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 提交
				AddResGroupShare();
			}
		});
	}

	/**
	 * 文字信息分享
	 * 
	 * AddResGroupShare.json
	 */
	private void AddResGroupShare() {
		String reson = etDetail.getText().toString();
		if(Util.isEmpty(reson)){
			showToast("请输入分享文字!");
			return;
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("groupId", groupId));// 群组id
		paras.add(new Parameter("shareUid", mUserId));// 分享者Uid
		paras.add(new Parameter("shareReason", reson));// 分享理由
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getAddResGroupShare(), paras, true, mess, GROUP_SHARE_KEY);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		if (caseKey == GROUP_SHARE_KEY) {
			BaseResult result = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == result.SUCCESS) {
				showToast("分享成功！");
				finish();
			} else if (result != null && result.getErrorCode() == result.FAILED) {
				showToast(result.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}

	}
	

}
