package com.wachoo.pangker.activity.group;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.camera.CameraCompActivity;
import com.wachoo.pangker.activity.contact.FriendGroupActivity;
import com.wachoo.pangker.db.IUserGroupDao;
import com.wachoo.pangker.db.impl.UserGroupDaoIpml;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.image.PKIconLoader;
import com.wachoo.pangker.listener.TextCountLimitWatcher;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.map.poi.PoiaddressLoader.GeoCallback;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.PangkerGroupAddResult;
import com.wachoo.pangker.server.response.PangkerGroupInfo;
import com.wachoo.pangker.server.response.QueryGroupsByIDResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.ui.dialog.EditTextDialog.DialogResultCallback;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PasswordDialog;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class ChatRoomCreateActivity extends CameraCompActivity implements IUICallBackInterface {
	private static final int UPLOADICON_CODE = 0x10;
	private TextView tv_title;
	private Button btnLabel;// 群标签

	private LinearLayout lyGroupType;
	private LinearLayout lyCheck;
	private LinearLayout lyGroupCategory;
	private LinearLayout ly_group_channel;
	private LinearLayout lyMaxTime;

	private TextView txtGroupName;
	private TextView tv_type;
	private EditText tv_introduce;
	private TextView tv_check;
	private TextView tv_grouptype;
	private TextView tv_category;
	private TextView tv_channel;
	private TextView tv_maxTime;
	private CheckBox cb_ifshare;
	private ImageView iv_group_icon;

	private Button submit;

	private final int dalog_type = 11;
	private final int dalog_introduce = 12;
	private final int dalog_check = 13;
	private final int dalog_isMove = 14;
	private final int dalog_custom = 16;
	private final int dalog_grouptype = 17;
	private final int dalog_defined_label = 18;
	private final int dialog_group_category = 19;
	private final int dialog_label_userdefined = 20;
	private final int dialog_channle = 21;
	private final int dialog_max_time = 22;
	private int nowDalogId = -1;

	private View layout;
	private EditText edit;
	PangkerGroupInfo info;

	String[] chatroom_type_item;// 类型
	String[] chatroom_type_id;

	// ********************************
	String[] lotype_item;// 群组位置属性(类型)
	String[] lotype_id; //
	String[] chatroom_grouptype_item;// 群组类型
	String[] chatroom_grouptype_id;
	String[] chatroom_channel = new String[] { "1", "2" };
	String[] chatroom_channel_show = new String[] { "单话筒模式", "双话筒模式" };
	String[] chatroom_time_type;// 时长限制

	String[] group_category_items;// 群组分类
	String[] group_category_ids; // 群组分类ID

	int type_id = 0;
	// >>>>>>>>群组权限设置
	int mPerissionChecked = 0;
	int move_id = 0;
	int speech_limit_id = 0;
	int grouptype_id = 0;
	int groupCategoryId = 0;
	int groupChannel = 0;
	int groupMaxTime = 0;

	boolean[] group_label_ischeck;

	String flag = "0";// 0创建,1：群组内修改2：群组外修改
	String groupId = "nogroupid";
	private String mAddress = "";

	int CREATE_FLAG = 10;// 注册
	int UPDATE_FLAG = 11;// 更新
	int LOAD_FLAG = 12;// 加载会议室信息

	private String uid;
	private String password = "";

	private IUserGroupDao userGroupDao;
	PangkerApplication application;

	// >>>>>>>>获取返回参数
	private static final int GROUP_JION_PEMISSION = 0;
	// >>>>>>>>>运行进入群组的联系人组（目前只支持好友组）
	private String groupJionAllowGroups;

	// >>>>>>>>>当前选择的好友分组id
	private String mSelectedGroupID = "";

	private PoiaddressLoader poiaddressLoader;
	// >>>>>>获取图片
	private ImageResizer mImageWorker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.chat_room_info);
		flag = getIntent().getExtras().getString("flag");
		groupId = getIntent().getExtras().getString("groupId");
		application = (PangkerApplication) this.getApplicationContext();
		uid = application.getMySelf().getUserId();
		mImageWorker = PangkerManager.getGroupIconResizer(getApplicationContext());
		poiaddressLoader = new PoiaddressLoader(application);
		initView();
		initData();
		initImgBitmap();
		initListen();
	}

	private void initImgBitmap() {
		// TODO Auto-generated method stub
		if (flag.equals("1")) {
			final String groupUrl = Configuration.getGroupCover() + groupId;
			mImageWorker.loadImage(groupUrl, iv_group_icon, groupId);
		}
	}

	private void initView() {

		tv_title = (TextView) findViewById(R.id.mmtitle);
		tv_title.setText(R.string.create_group);
		btnLabel = (Button) findViewById(R.id.btn_addlabel);
		lyGroupType = (LinearLayout) findViewById(R.id.ly_group_type);
		lyCheck = (LinearLayout) findViewById(R.id.ly_check);
		lyGroupCategory = (LinearLayout) findViewById(R.id.ly_group_category);
		cb_ifshare = (CheckBox) findViewById(R.id.cb_ifshare);
		iv_group_icon = (ImageView) findViewById(R.id.iv_group_icon);

		txtGroupName = (TextView) findViewById(R.id.tv_name);
		txtGroupName.setOnClickListener(listener);
		tv_type = (TextView) findViewById(R.id.tv_type);
		tv_introduce = (EditText) findViewById(R.id.tv_introduce);

		tv_introduce.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				tv_introduce.setFocusable(true);
				tv_introduce.requestFocus();
				tv_introduce.setFocusableInTouchMode(true);
				tv_introduce.setSelection(tv_introduce.getEditableText().toString().length());
				return false;
			}
		});
		tv_introduce.setFilters(new InputFilter[] { new InputFilter.LengthFilter(100) });
		tv_check = (TextView) findViewById(R.id.tv_check);
		tv_grouptype = (TextView) findViewById(R.id.tv_grouptye);
		tv_category = (TextView) findViewById(R.id.tv_category);
		submit = (Button) findViewById(R.id.iv_more);
		submit.setText("提交");
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideSoftInput(v);
				ChatRoomCreateActivity.this.finish();
			}
		});
		if (!flag.equals("0")) {
			iv_group_icon.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					showImageDialog("图标上传", actionItems1, true);
				}
			});

		}

		ly_group_channel = (LinearLayout) findViewById(R.id.ly_group_channel);
		tv_channel = (TextView) findViewById(R.id.tv_channel);

		lyMaxTime = (LinearLayout) findViewById(R.id.ly_max_time);
		tv_maxTime = (TextView) findViewById(R.id.tv_max_time);
	}

	private void initData() {
		userGroupDao = new UserGroupDaoIpml(this);

		if (flag.equals("1")) {
			tv_title.setText("修改群组信息");
			submit.setText("确认修改");
			loadGroup();
		}
		chatroom_type_item = getResources().getStringArray(R.array.chatroom_type_item);
		chatroom_type_id = getResources().getStringArray(R.array.chatroom_type_id);

		chatroom_grouptype_item = getResources().getStringArray(R.array.group_type_item);
		chatroom_grouptype_id = getResources().getStringArray(R.array.chatroom_grouptype_id);
		lotype_item = getResources().getStringArray(R.array.lotype_item);
		lotype_id = getResources().getStringArray(R.array.lotype_id);
		chatroom_time_type = getResources().getStringArray(R.array.chatroom_time_type);

		group_category_items = getResources().getStringArray(R.array.group_category_item);
		group_category_ids = getResources().getStringArray(R.array.group_category_ids_item);

		// >>>>>>>>>显示设置的权限
		tv_check.setText(UserGroup.PERMISSION_ITEM_NAME[0]);
		tv_grouptype.setText(chatroom_grouptype_item[0]);
		tv_category.setText(group_category_items[0]);
		group_label_ischeck = new boolean[chatroom_type_item.length];
		poiaddressLoader.onPoiLoader(((PangkerApplication) getApplication()).getCurrentLocation(), this, geoCallback);
	}

	/**
	 * 在弹出标签选择框之前把group_label_ischeck布尔数组重置为false
	 */
	private void setLabelFalse() {
		for (int i = 0; i < chatroom_type_item.length; i++) {
			group_label_ischeck[i] = false;
		}
	}

	private void setLabel(String label) {
		setLabelFalse();
		String[] hobbys = label.split(",");
		boolean[] labels = new boolean[hobbys.length];
		for (int i = 0; i < labels.length; i++) {
			labels[i] = true;
		}
		for (int i = 0; i < chatroom_type_item.length; i++) {
			for (int j = 0; j < hobbys.length; j++) {
				if (chatroom_type_item[i].equals(hobbys[j])) {
					group_label_ischeck[i] = true;
					labels[j] = false;
				}
			}
		}
		// 自定义标签截取
		StringBuffer sbLabel = new StringBuffer();
		for (int i = 0; i < hobbys.length; i++) {
			if (labels[i]) {
				sbLabel.append(hobbys[i]).append(",");
			}
		}
	}

	private void initListen() {
		btnLabel.setOnClickListener(listener);
		lyCheck.setOnClickListener(listener);
		lyGroupType.setOnClickListener(listener);
		submit.setOnClickListener(listener);
		lyGroupCategory.setOnClickListener(listener);
		lyMaxTime.setOnClickListener(listener);
		ly_group_channel.setOnClickListener(listener);
	}

	private void loadGroup() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", uid));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("shareUid", uid));
		serverMana.supportRequest(Configuration.getQueryGroupsByID(), paras, true,
				getResources().getString(R.string.room_loading), LOAD_FLAG);
	}

	/**
	 * 创建群组
	 */
	private void createGroup() {
		// TODO Auto-generated method stub
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", uid));
		paras.add(new Parameter("optype", flag));
		paras.add(new Parameter("grouplabel", tv_type.getText().toString()));
		paras.add(new Parameter("groupname", txtGroupName.getText().toString()));
		paras.add(new Parameter("grouptype", "12"));
		double ctrlon = ((PangkerApplication) getApplication()).getCurrentLocation().getLongitude();
		double ctrlat = ((PangkerApplication) getApplication()).getCurrentLocation().getLatitude();
		paras.add(new Parameter("ctrlon", String.valueOf(ctrlon)));
		paras.add(new Parameter("ctrlat", String.valueOf(ctrlat)));
		paras.add(new Parameter("lotype", "1"));
		paras.add(new Parameter("address", mAddress));
		paras.add(new Parameter("desc", tv_introduce.getText().toString()));
		// >>>>>>>>>>>权限设置字段
		paras.add(new Parameter("accesstype", String.valueOf(UserGroup.PERMISSION_ITEM_ID[mPerissionChecked])));
		// >>>>>>>>>如果是指定分组需要追加此项目
		if (UserGroup.PERMISSION_ITEM_ID[mPerissionChecked] == PangkerConstant.Role_CONTACT_GROUP) {
			paras.add(new Parameter("allowGroups", groupJionAllowGroups));
		}
		// 设置密码访问
		else if (UserGroup.PERMISSION_ITEM_ID[mPerissionChecked] == PangkerConstant.Role_PASSWORD) {
			paras.add(new Parameter("password", password));
		}
		paras.add(new Parameter("category", group_category_ids[groupCategoryId]));
		paras.add(new Parameter("ifshare", cb_ifshare.isChecked() ? "1" : "0"));
		paras.add(new Parameter("voiceChannelType", String.valueOf(groupChannel + 1)));
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getPangkerGroupManager(), paras, true, mess, CREATE_FLAG);
	}

	private boolean isVoiceChannelChange() {
		return info.getVoiceChannelType() != Integer.parseInt(chatroom_channel[groupChannel]);
	}

	private boolean isMaxTimeChange() {
		return info.getMaxSpeechTime() != Integer.parseInt(chatroom_time_type[groupMaxTime]);
	}

	/**
	 * 更新群组信息
	 */
	private void updateGroup() {
		// TODO Auto-generated method stub
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", uid));
		paras.add(new Parameter("optype", flag));
		paras.add(new Parameter("groupname", txtGroupName.getText().toString()));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("grouplabel", tv_type.getText().toString()));
		paras.add(new Parameter("desc", tv_introduce.getText().toString()));
		// >>>>>>>>>>群组权限设置字段
		paras.add(new Parameter("accesstype", String.valueOf(UserGroup.PERMISSION_ITEM_ID[mPerissionChecked])));

		// >>>>>>>>>如果是指定分组需要追加此项目
		if (UserGroup.PERMISSION_ITEM_ID[mPerissionChecked] == PangkerConstant.Role_CONTACT_GROUP) {
			paras.add(new Parameter("allowGroups", groupJionAllowGroups));
		}
		// 设置密码访问
		else if (UserGroup.PERMISSION_ITEM_ID[mPerissionChecked] == PangkerConstant.Role_PASSWORD) {
			paras.add(new Parameter("password", password));
		}

		paras.add(new Parameter("category", group_category_ids[groupCategoryId]));
		paras.add(new Parameter("ifshare", cb_ifshare.isChecked() ? "1" : "0"));
		// 判断通道是否修改
		if (isVoiceChannelChange()) {
			paras.add(new Parameter("voiceChannelType", chatroom_channel[groupChannel]));
		}
		// 判断发言时间是否修改chatroom_time_type[groupMaxTime]
		if (isMaxTimeChange()) {
			paras.add(new Parameter("maxSpeechTime", chatroom_time_type[groupMaxTime]));
		}

		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getPangkerGroupManager(), paras, true, mess, UPDATE_FLAG);
	}

	/**
	 * 创建群组的检测
	 * 
	 * @return
	 */
	private boolean checkCreateGroup() {
		// TODO Auto-generated method stub
		String groupName = txtGroupName.getText().toString().trim();
		String groupLabel = tv_type.getText().toString();
		if (Util.isEmpty(groupName)) {
			showToast(R.string.create_name_null);
			return false;
		} else if (groupName.length() < 3 || groupName.length() > 10) {
			showToast(R.string.create_name_length);
			return false;
		} else if (groupLabel == null || groupLabel.length() < 1) {
			showToast(R.string.create_label_null);
			return false;
		} else if (groupLabel.split(",") == null || groupLabel.split(",").length < 1
				|| groupLabel.split(",").length > 5
				|| groupLabel.equals(getResources().getString(R.string.room_setsign))) {
			showToast(R.string.create_label_length);
			return false;
		}
		return true;
	}

	private int getChannleIndex() {
		// TODO Auto-generated method stub
		for (int i = 0; i < chatroom_channel.length; i++) {
			if (chatroom_channel[i].equals(String.valueOf(info.getVoiceChannelType()))) {
				return i;
			}
		}
		return 0;
	}

	private int getMaxTimeIndex() {
		// TODO Auto-generated method stub
		for (int i = 0; i < chatroom_time_type.length; i++) {
			if (chatroom_time_type[i].equals(String.valueOf(info.getMaxSpeechTime()))) {
				return i;
			}
		}
		return 0;
	}

	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnLabel) {
				EditTextDialog editTextDialog = new EditTextDialog(ChatRoomCreateActivity.this);
				editTextDialog.setLine(2);
				editTextDialog.showDialog(R.string.chatroom_glabel_defined, "", R.string.chatroom_glabel_defined_hint,
						50, new DialogResultCallback() {
							@Override
							public void buttonResult(String result, boolean isSubmit) {
								// TODO Auto-generated method stub
								if (isSubmit && result != null) {
									tv_type.setText(result);
								}
							}
						});

			} else if (v == lyCheck) {

				createDialog(dalog_check);
			} else if (v == lyGroupType) {
				if (flag.equals("0")) {
					createDialog(dalog_grouptype);
				}
			}

			else if (v == submit) {
				if (checkCreateGroup()) {
					if (flag.equals("0")) {// 创建
						createGroup();
					} else {// 更新
						updateGroup();
					}
				}
			} else if (v == lyGroupCategory) {
				createDialog(dialog_group_category);
			} else if (v == ly_group_channel) {
				createDialog(dialog_channle);
			} else if (v == lyMaxTime) {
				createDialog(dialog_max_time);
			} else if (v == txtGroupName) {
				EditTextDialog dialog = new EditTextDialog(ChatRoomCreateActivity.this);
				dialog.setLine(4);
				dialog.showDialog("请输入群组名称", "", txtGroupName.getText().toString(), 20, new DialogResultCallback() {
					@Override
					public void buttonResult(String result, boolean isSubmit) {
						// TODO Auto-generated method stub
						if (isSubmit && result != null) {
							txtGroupName.setText(result);
						}
					}
				});

			}
		}
	};

	GeoCallback geoCallback = new GeoCallback() {
		@Override
		public void onGeoLoader(String address, int err) {
			// TODO Auto-generated method stub
			if (!Util.isEmpty(address)) {
				// tv_address.setText(address);
				mAddress = address;
			}
		}
	};

	public void createDialog(int id) {
		if (nowDalogId != -1) {
			removeDialog(nowDalogId);
		}
		nowDalogId = id;
		showDialog(nowDalogId);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1 && resultCode == RESULT_OK) {
			final String groupUrl = Configuration.getGroupCover() + groupId;
			mImageWorker.clearData(groupUrl);
			mImageWorker.loadImage(groupUrl, iv_group_icon, groupId);
			Message msg = new Message();
			msg.what = 1;
			PangkerManager.getTabBroadcastManager().sendResultMessage(ChatRoomListActivity.class.getName(), msg);
		}
		// >>>>>>>>>群组设置权限联系人分组
		if (requestCode == GROUP_JION_PEMISSION && resultCode == RESULT_OK) {
			// >>>>>>>>设置选择分组

			mSelectedGroupID = data.getStringExtra(FriendGroupActivity.SELECT_CONTACT_GROUP_ID);
			String allowGroupName = application.getFriendGroupNameByID(mSelectedGroupID);
			// >>>>>>>>获取指定加入分组
			groupJionAllowGroups = data.getStringExtra(FriendGroupActivity.GROUP_JION_PEMISSION_KEY);
			// >>>>>>>>>>选中好友分组
			mPerissionChecked = 3;
			tv_check.setText("(好友组)" + allowGroupName);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(ChatRoomCreateActivity.this);
		if (id == dalog_type) {
			builder.setPositiveButton(R.string.submit, dlistener).setNegativeButton(R.string.cancel, null)
					.setMultiChoiceItems(chatroom_type_item, group_label_ischeck, new OnMultiChoiceClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which, boolean isChecked) {
							// TODO Auto-generated method stub
							group_label_ischeck[which] = isChecked;
						}
					}).setNeutralButton(R.string.btn_my_definition, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							// 自定义标签
							createDialog(dialog_label_userdefined);
						}
					});

		} else if (id == dalog_defined_label) {
			layout = LayoutInflater.from(ChatRoomCreateActivity.this).inflate(R.layout.dialog_editview, null);
			edit = (EditText) layout.findViewById(R.id.et_dialog);
			edit.setHint(R.string.chatroom_glabel_hint);
			builder.setTitle(R.string.chatroom_glabel_title).setPositiveButton(R.string.submit, dlistener)
					.setNegativeButton(R.string.cancel, null).setView(layout);
		} else if (id == dalog_introduce) {
			layout = LayoutInflater.from(ChatRoomCreateActivity.this).inflate(R.layout.dialog_editview, null);
			edit = (EditText) layout.findViewById(R.id.et_dialog);
			edit.addTextChangedListener(new TextCountLimitWatcher(100, edit));
			edit.setHint(R.string.chatroom_gname_hint_noneed);
			edit.setText(tv_introduce.getText());
			edit.setLines(3);
			builder.setTitle(R.string.chatroom_gintroduce_title).setView(layout)
					.setPositiveButton(R.string.submit, dlistener).setNegativeButton(R.string.cancel, null);
		}
		// >>>>>>>>群组设置权限
		else if (id == dalog_check) {

			// >>>>>>>>>>显示
			builder.setSingleChoiceItems(UserGroup.PERMISSION_ITEM_NAME, mPerissionChecked, dlistener);

		} else if (id == dalog_isMove) {
			builder.setSingleChoiceItems(lotype_item, move_id, dlistener);
		} else if (id == dalog_grouptype) {
			builder.setSingleChoiceItems(chatroom_grouptype_item, grouptype_id, dlistener);
		} else if (id == dalog_custom) {
			builder.setView(layout).setPositiveButton(R.string.submit, dlistener)
					.setNegativeButton(R.string.cancel, null);
		} else if (id == dialog_group_category) {
			builder.setSingleChoiceItems(group_category_items, groupCategoryId, dlistener);
		} else if (id == dialog_channle) {
			builder.setSingleChoiceItems(chatroom_channel_show, groupChannel, dlistener);
		} else if (id == dialog_max_time) {
			builder.setSingleChoiceItems(chatroom_time_type, groupMaxTime, dlistener);
		}
		return builder.create();
	}

	DialogInterface.OnClickListener dlistener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			if (nowDalogId == dalog_type) {
				// 确定按钮
				if (which == DialogInterface.BUTTON_POSITIVE) {
					StringBuffer sb = new StringBuffer();
					for (int i = 0, size = chatroom_type_item.length; i < size; i++) {
						if (group_label_ischeck[i]) {
							if (sb.toString().trim().length() > 0) {
								sb.append(",");
							}
							sb.append(chatroom_type_item[i]);
						}
					}
					if (sb.toString().trim().length() > 0) {
						tv_type.setText(sb.toString());
					}
				}
			} else if (nowDalogId == dalog_defined_label) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					String strType = tv_type.getText().toString();
					if (strType.equals("")) {
						tv_type.setText(edit.getText().toString());
					} else {
						tv_type.setText(tv_type.getText().toString() + "," + edit.getText().toString());
					}
				}
			} else if (nowDalogId == dalog_introduce) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					tv_introduce.setText(edit.getText());
				}
			}
			// >>>>>>>>>群组权限设置
			else if (nowDalogId == dalog_check) {

				// >>>>>>>>>指定好友组可以访问
				if (which == 3) {
					Intent intent = new Intent();
					intent.putExtra("fromType", FriendGroupActivity.GROUP_JION_PERMISSION);
					intent.putExtra(FriendGroupActivity.SELECT_CONTACT_GROUP_ID, mSelectedGroupID);
					intent.setClass(ChatRoomCreateActivity.this, FriendGroupActivity.class);
					startActivityForResult(intent, GROUP_JION_PEMISSION);
				}
				// >>>>>>>>密码访问
				else if (which == 4) {
					showPasswordDialog();
				} else {
					// >>>>>>>>设置当前选中项目
					mPerissionChecked = which;
					tv_check.setText(UserGroup.PERMISSION_ITEM_NAME[which]);
				}

			} else if (nowDalogId == dalog_grouptype) {
				grouptype_id = which;
				tv_grouptype.setText(chatroom_grouptype_item[which]);
				if (grouptype_id == 0 || grouptype_id == 1) {
					tv_check.setText(UserGroup.PERMISSION_ITEM_NAME[0]);
				}
			} else if (nowDalogId == dalog_custom) {
				tv_type.setText(edit.getText());
			} else if (nowDalogId == dialog_group_category) {
				groupCategoryId = which;
				tv_category.setText(group_category_items[which]);
			} else if (nowDalogId == dialog_label_userdefined) {
				tv_type.setText(edit.getText());
			} else if (nowDalogId == dialog_channle) {
				groupChannel = which;
				tv_channel.setText(chatroom_channel_show[groupChannel]);
			} else if (nowDalogId == dialog_max_time) {
				groupMaxTime = which;
				tv_maxTime.setText(chatroom_time_type[groupMaxTime] + "S");
			}
		}
	};

	PasswordDialog mPasswordDialog;

	private void showPasswordDialog() {
		// TODO Auto-generated method stub
		if (mPasswordDialog == null) {

			mPasswordDialog = new PasswordDialog(this);
			mPasswordDialog.setTitle("请输入密码");
			mPasswordDialog.setResultCallback(callback);
		}
		mPasswordDialog.showDialog();
	}

	MessageTipDialog.DialogMsgCallback callback = new MessageTipDialog.DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if (isSubmit && mPasswordDialog.checkPass()) {

				password = mPasswordDialog.getPassword();
				if (!Util.isEmpty(password)) {
					mPerissionChecked = 4;
					tv_check.setText(UserGroup.PERMISSION_ITEM_NAME[mPerissionChecked]);
				}
				// >>>>>>>>设置当前选中项目
				tv_check.setText(UserGroup.PERMISSION_ITEM_NAME[mPerissionChecked]);
			}
			tv_check.setText(UserGroup.PERMISSION_ITEM_NAME[mPerissionChecked]);
		}
	};

	private void initGroupInfoData() {
		// TODO Auto-generated method stub
		txtGroupName.setText(info.getGroupName());
		tv_type.setText(info.getGroupLabel());
		tv_introduce.setText(info.getGroupDesc());

		if (info.getIfShare() == 1) {
			cb_ifshare.setChecked(true);
		} else
			cb_ifshare.setChecked(false);

		if (info.getGroupLabel() != null) {
			setLabel(info.getGroupLabel());
		}
		// 设置单双通道
		groupChannel = getChannleIndex();
		tv_channel.setText(chatroom_channel_show[groupChannel]);
		groupMaxTime = getMaxTimeIndex();
		tv_maxTime.setText(info.getMaxSpeechTime() + "S");

		// >>>>>>>获取当前设置的访问权限
		for (int i = 0; i < UserGroup.PERMISSION_ITEM_ID.length; i++) {
			if (info.getAccessType() == UserGroup.PERMISSION_ITEM_ID[i]) {
				tv_check.setText(UserGroup.PERMISSION_ITEM_NAME[i]);

				// >>>>>>>>>设置当前选中的项目
				mPerissionChecked = i;
				// >>>>如果是选中好友分组的话，需要
				if (info.getAccessType() == PangkerConstant.Role_CONTACT_GROUP) {
					this.groupJionAllowGroups = info.getAuthorizedGroup();
					int start = info.getAuthorizedGroup().indexOf("[") + 1;
					int end = info.getAuthorizedGroup().indexOf("]");
					// >>>>>>>设置当前选中的群组的名称
					mSelectedGroupID = info.getAuthorizedGroup().substring(start, end);
					tv_check.setText(UserGroup.PERMISSION_ITEM_NAME[i] + ":"
							+ application.getFriendGroupNameByID(mSelectedGroupID));
				}

				// >>>>密码权限
				if (info.getAccessType() == PangkerConstant.Role_PASSWORD) {
					this.password = info.getVisitPassword();
				}
				break;
			}
		}
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse)) {
			if (caseKey == UPLOADICON_CODE) {
				initImgBitmap();
			}
			return;
		}

		if (caseKey == LOAD_FLAG) {
			QueryGroupsByIDResult result = JSONUtil.fromJson(supportResponse.toString(), QueryGroupsByIDResult.class);
			if (result != null && result.getErrorCode() != 999) {
				if (result.getErrorCode() == BaseResult.SUCCESS) {
					info = result.getGroupinfo();
					initGroupInfoData();
				} else if (result.getErrorCode() == 0) {
					showToast(result.getErrorMessage());
					finish();
				}
			} else {
				showToast(R.string.return_value_999);
				finish();
			}

		} else if (caseKey == CREATE_FLAG || caseKey == UPDATE_FLAG) {
			PangkerGroupAddResult result = JSONUtil.fromJson(supportResponse.toString(), PangkerGroupAddResult.class);
			if (result != null && result.getErrorCode() != 999) {
				if (result.getErrorCode() == BaseResult.SUCCESS) {
					if (caseKey == CREATE_FLAG && result.getGroupInfo() != null) {
						UserGroup userGroup = result.getGroupInfo();
						userGroup.setGroupFlag(0);
						userGroupDao.addGroup(userGroup);
						Intent toCover = new Intent(this, ChatRoomCoverActivity.class);
						toCover.putExtra("userGroup", userGroup);
						startActivity(toCover);

					} else {
						showToast(result.getErrorMessage());
						UserGroup userGroup = new UserGroup();
						userGroup.setUid(uid);
						userGroup.setGroupId(groupId);
						userGroup.setGroupName(txtGroupName.getText().toString());
						userGroup.setGroupLabel(tv_type.getText().toString());
						userGroupDao.updateGroupBase(userGroup);
					}
					// >>>>通知ChatRoomList修改成功
					if (flag.equals("1")) {
						Intent data = new Intent();
						if (isVoiceChannelChange()) {
							data.putExtra("voiceChannelType", Integer.parseInt(chatroom_channel[groupChannel]));
						}
						if (isMaxTimeChange()) {
							data.putExtra("maxSpeechTime", Integer.parseInt(chatroom_time_type[groupMaxTime]));
						}
						setResult(RESULT_OK, data);
					}
					if ("0".equals(flag)) {
						setResult(RESULT_OK);
					}
					finish();
				} else if (result.getErrorCode() == 0) {
					showToast(result.getErrorMessage());
				} else {
					showToast(R.string.return_value_999);
				}

			} else {
				showToast(R.string.return_value_999);
			}
		} else if (caseKey == UPLOADICON_CODE) {
			BaseResult result = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (result.getErrorCode() != 999) {
				showToast(result.getErrorMessage());
				if (result.getErrorCode() == 1) {
					setResult(RESULT_OK);
				}
			} else {
				initImgBitmap();
				showToast(R.string.return_value_999);
			}
		}
	}

	/**
	 * 
	 * String TODO过滤群组名称字符输入
	 * 
	 * @param str
	 * @return
	 * @throws PatternSyntaxException
	 */
	public String stringFilter(String str) throws PatternSyntaxException {
		// 只允许中英文和数字
		String regex = "[^a-zA-Z0-9\u4e00-\u9fa5]";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(str);
		return m.replaceAll("").trim();
	}

	@Override
	public void doImageBitmap(final String imageLocalPath) {
		// TODO Auto-generated method stub
		Bitmap bitmap = ImageUtil.decodeFile(imageLocalPath, PKIconLoader.iconSideLength, PKIconLoader.iconSideLength
				* PKIconLoader.iconSideLength);
		if (bitmap != null) {
			iv_group_icon.setImageBitmap(bitmap);
		}
		MessageTipDialog messageTipDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {

			@Override
			public void buttonResult(boolean isSubmit) {
				// TODO Auto-generated method stub
				if (isSubmit)
					UploadGroupCover(imageLocalPath);
			}
		}, this);

		messageTipDialog.showDialog("", "是否上传群组头像?");

	}

	/**
	 * 
	 * @author wubo
	 * @createtime Jun 7, 2012
	 * @throws FileNotFoundException
	 */
	private void UploadGroupCover(String imageLocalPath) {
		try {
			File file = new File(imageLocalPath);

			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", uid));
			paras.add(new Parameter("gid", groupId));
			MyFilePart uploader = new MyFilePart(file.getName(), file, null);
			String mess = getResources().getString(R.string.uploading);
			ServerSupportManager serverMana = new ServerSupportManager(this, this);
			serverMana.supportRequest(Configuration.getUploadGroupCover(), paras, uploader, imageLocalPath, true, mess,
					UPLOADICON_CODE);
		} catch (Exception e) {
			// TODO Auto-generated catch block

			showToast("获取图片失败!");
			e.printStackTrace();
		}
	}

}
