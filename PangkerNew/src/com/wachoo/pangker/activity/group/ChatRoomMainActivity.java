package com.wachoo.pangker.activity.group;

import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.text.ClipboardManager;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.PicturePreviewActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.camera.CameraCompActivity;
import com.wachoo.pangker.activity.msg.MsgInfoActivity;
import com.wachoo.pangker.activity.msg.MsgLoactionActivity;
import com.wachoo.pangker.activity.res.ResLocalActivity;
import com.wachoo.pangker.activity.res.ResUserStoreActivity;
import com.wachoo.pangker.activity.res.UploadPicActivity;
import com.wachoo.pangker.adapter.ChatRoomListAdapter;
import com.wachoo.pangker.adapter.ChatRoomUserAdapter;
import com.wachoo.pangker.adapter.ChatRoomUserGVAdapter;
import com.wachoo.pangker.adapter.GroupShareAdapter;
import com.wachoo.pangker.adapter.MicListAdapter;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.chat.MessageBroadcast;
import com.wachoo.pangker.db.IMeetRoomDao;
import com.wachoo.pangker.db.impl.MeetRoomDaoImpl;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.UploadJob;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.group.P2PUserInfo;
import com.wachoo.pangker.group.SP2CRoom_Status_Resp;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.image.PKIconFetcher;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.listener.BtnOnClickNumOrTimeListener;
import com.wachoo.pangker.listener.EdInputFilter;
import com.wachoo.pangker.listener.TextButtonWatcher;
import com.wachoo.pangker.receiver.HeadsetPlugReceiver;
import com.wachoo.pangker.receiver.HeadsetPlugReceiver.OnHeadsetListener;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.DocInfo;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.QueryResGroupShareResult;
import com.wachoo.pangker.server.response.ResGroupshare;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.service.MeetRoomService;
import com.wachoo.pangker.service.MeetRoomService.MeetRoomServiceBinder;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmoView;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.QuickAction.OnActionItemClickListener;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.ConfrimDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.ui.dialog.WaitingDialog;
import com.wachoo.pangker.util.MeetRoomUtil;
import com.wachoo.pangker.util.MeetRoomUtil.SpeechAllow;
import com.wachoo.pangker.util.SmileyParser;
import com.wachoo.pangker.util.Util;

public class ChatRoomMainActivity extends CameraCompActivity implements ITabSendMsgListener {

	private String TAG = "ChatRoomMainActivity";// log tag
	private final int MAX_MSG_LIMIT = 300;

	private static final int GROUP_VISITOR = 1;// 群组来访（未做 ）
	private static final int GROUP_HISTORY = 2;
	private static final int GROUP_SHARERES = 4;// 聊天记录
	private static final int GROUP_LOGOUT = 5;
	private static final int GROUP_INVITE = 6; // 邀请好友
	private static final int GROUP_INFO = 7;// 聊天记录
	private static final int GROUP_INFO_EDIT = 9;// 聊天记录
	private static final int GROUP_SHARE_TXT = 10;// 分享文字信息
	private final int DEFAULT_SHOW_MESSAGE_COUNT = 15; // 默认显示消息条数
	private final int GROUP_MAP = 8; // 默认显示消息条数

	// >>>>>要加入的群组
	public static final String JION_TO_GROUP = "JION_TO_GROUP";
	public static final String NOREAD_EndTIME_KEY = "noReadEndTime_key";
	private static final int LOCATION_SEND = 0x102; // 发送位置信息使用 >>>wangxin
	public final int TEXT_FLAG = 15;// 发送文字
	/***************************************************************************/
	private static final int REQUEST_TO_ChatRoomUserListActivity = 0x11;//
	private IMeetRoomDao mMeetRoomDao;
	private SmileyParser parser; // 表情转化辅助类
	private ChatRoomHandler handler;
	private UserGroup jionin_groupInfo;// 要进入的群组
	private String mMyUserID;
	/************************ 整体架构界面 *************************************/
	public boolean Play_flag = true;// 语音播放状态
	private String micMode = "";

	private WaitingDialog bar;// 等待进度条
	private Intent meetService;

	private TextView mGroupName;// 房间名称
	private RadioGroup mRadioGroup;
	private Button mBtnBack;
	private Button mBtnMenu;// 右上角功能按钮
	// >>>>>>>>>>右上角功能按钮
	private QuickAction menuQuickAction;

	private PKIconResizer mImageResizer;
	private PangkerApplication application;
	private PopMenuDialog msgDialog;
	private MeetRoom msgItem;
	private MessageBroadcast msgInfoBroadcast;
	private HeadsetPlugReceiver headsetPlugReceiver;

	private ConfrimDialog mConfrimDialog;
	/************************ 直播界面 *************************************/
	private Button btn_micMode;// 麦模式
	private TextView show_mic;// 显示或隐藏麦列表
	private Button btn_upordown_mic;// 抢麦或下麦
	private Button btn_speech_mic;// 发言
	private ImageButton btn_voice;

	// >>>当前发言用户信息
	private LinearLayout ly_speaker_info;
	private ImageView iv_speaker_icon;
	private TextView tv_speaker_name;

	// >>>管理员发言信息
	private LinearLayout ly_admin_info;
	private ImageView iv_admin_icon;
	private ImageView iv_admin_mark;
	private TextView tv_admin_name;

	private ListView micListView;// 麦序
	private ListView msgListView;// 消息
	private ChatRoomListAdapter mChatMessageAdapter; // >>>>>>>私信会话adapter
	private EditText et_message;// 消息
	private Button btn_sendMessage;// 发送
	private MicListAdapter mMicAdapter;// 适配器

	// add start wangxin
	private LinearLayout chat_layout; // 文字信息界面
	private LinearLayout speak_layout; // 发言信息界面
	private Button btn_type;// 功能切换按钮
	private Button btn_actions; // 功能按钮// 功能按钮 >>> by wangxin
	private Button btnLocationSend;// 发送位置信息 >>> wangxin
	private Button btnImgSend; // 发送图片信息 >>> wangxin
	private Button btnExpression;
	private LinearLayout chat_actions_tools; // 功能按钮工具栏 >>>wangxin

	private EmoView emoView;// 表情View>>>lb
	// >>>wangxin 显示聊天记录
	private View more_chat_message;
	private Button btn_more_message;
	// >>>>>>群组聊天界面
	private LinearLayout mGroupChatView;
	/************************* 群组内成员信息列表 *************************************/
	private final int[] MENU_INDEXS = { 0x100, 0x101, 0x102 };
	private final String[] MENU_TITLE = { "男生", "女生", "所有用户" };

	private ImageView iconDrow;
	private TextView tvTitle;// >>>>.显示当前过滤的key
	private TextView txtCount;// >>>>显示当前用户总人数

	private LinearLayout layoutTitle;// >>>>>过滤显示title
	private RelativeLayout mTitleTopLayout;
	private QuickAction popopQuickAction;

	private static final int LIST_TYPE = 0;
	private static final int GRID_TYPE = 1;
	private int currListType = LIST_TYPE;// 当前显示列表类型标识，0为ListView，1为GridView

	private LinearLayout mGroupMembersView;
	private ListView mGroupMembersListView;
	private ChatRoomUserAdapter mGroupUsersAdapter;
	private ChatRoomUserGVAdapter mGroupUsersGVAdapter;
	private Button mBtnShowType;// >>>>>>>>>显示方式

	/************************* 群组资源分享列表界面 *************************************/
	private LinearLayout mGroupShareView;
	private ListView mGroupShareListView;
	private EmptyView mGroupShareEmptyView;

	private GroupShareAdapter mGroupShareAdapter;// 群分享adapter
	private PullToRefreshListView pullToRefreshListView;
	private FooterView groupShareFooterView;
	private Button btnPhtopShare;
	private Button etShareText;
	private ResGroupshare mGroupshare;// 当前被选中资源
	private boolean isFirst = true;
	/************************* 群组资源分享列表界面 *************************************/
	// >>>>>>>>>双通道的case判断耳机是否插上
	private AudioManager audiomanage;
	private boolean isBind = false;

	private Button mHideSpeaker;// >>>>>>>>显示隐藏开关
	private LinearLayout mSpeakerLayout;// >>>>>>>发言相关显示信息
	ImageLocalFetcher localFetcher;
	ImageCache mImageCache;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if (onSaveGroup(savedInstanceState)) {
			finish();
			return;
		}
		// 防止输入法在启动Activity时自动弹出
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.chat_group_mian);

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = false;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>>>加载
		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		localFetcher.setmSaveDiskCache(false);
		localFetcher.setImageCache(mImageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);

		// >>加载群组信息
		initGroup();
		// >>>加载界面
		initTitleView();
		initMemberView();
		initChatView();

		initGroupShareView();
		mGroupMembersView.setVisibility(View.GONE);
		mGroupChatView.setVisibility(View.VISIBLE);
		mGroupShareView.setVisibility(View.GONE);
		// >>>绑定服务
		isBind = bindService(new Intent(this, MeetRoomService.class), conn, BIND_AUTO_CREATE);

		// 通知会话消息，群组消息已读
		noticeMsgInfoActivity();
	}

	private boolean onSaveGroup(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (savedInstanceState != null) {
			Boolean isSaveIn = savedInstanceState.getBoolean("isJoin");
			if (isSaveIn != null && isSaveIn) {
				showToast("您已经断开连接!");
				Log.d("onSaveGroup", "onSaveGroup==>Logout.");
				return true;
			}
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		// >>>>>>>判断表情是否POP，如果显示状态，取消表情显示
		if (emoView.getVisibility() == View.VISIBLE) {
			emoView.setVisibility(View.GONE);
		} else {
			PangkerManager.getTabBroadcastManager().removeUserListenter(ChatRoomMainActivity.this);
			super.onBackPressed();
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (isBind) {
			Log.d("MeetRoomService", "onDestroy and unbindService!");
			unbindService(conn);
		}
		super.onDestroy();
	}

	OnHeadsetListener onHeadsetListener = new OnHeadsetListener() {
		@Override
		public void onHeadsetConnectListener(boolean connect) {
			// >>>>>>>如果所在群组不是双向麦的群组不需要检测监听
			if (application.getCurrunGroup() == null || application.getCurrunGroup().isSingleVoiceChannel())
				return;
			// >>>>>>>如果耳机拔出
			if (!connect) {
				// 如果正在发言的过程中，发现耳机拔出来的，则立马停止发言
				// >>>>>>>>>>>>下麦申请
				if (application.getCurrunGroup().getSpeechStatus() == SpeechAllow.SPEECHING) {
					showToast("您手机的耳机已经拔出，不能继续发言!");
					sendPack(Configuration.STOP_AUDIO_FLAG);
				} else {
					// 如果正在麦序中，则要下麦
					if (application.getCurrunGroup().isAtGroupMics(Long.parseLong(mMyUserID))) {
						showToast("您手机的耳机已经拔出，已经下麦!");
						speechMicDOWN();
					}
				}
			}
		}
	};

	// 通知会话消息，私信已读
	private void noticeMsgInfoActivity() {
		// TODO Auto-generated method stub
		if (application.getCurrunGroup() != null) {
			android.os.Message msg = new android.os.Message();
			msg.what = 2;
			msg.obj = application.getCurrunGroup().getGroupId();
			msg.arg1 = MsgInfo.MSG_GROUP_CHAT;
			PangkerManager.getTabBroadcastManager().sendResultMessage(MsgInfoActivity.class.getName(), msg);
		}
	}

	// >>>>>>>>>>>加载成员列表
	private void initMemberView() {
		// TODO Auto-generated method stub
		PKIconFetcher pkIconFetcher = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);

		findViewById(R.id.btn_search).setVisibility(View.GONE);
		findViewById(R.id.layout_count).setVisibility(View.VISIBLE);
		txtCount = (TextView) findViewById(R.id.txt_members_count);
		mGroupMembersListView = (ListView) findViewById(R.id.lv_group_members_list);
		mGroupUsersAdapter = new ChatRoomUserAdapter(this, pkIconFetcher);
		mGroupMembersListView.setAdapter(mGroupUsersAdapter);
		txtCount.setText(String.valueOf(mGroupUsersAdapter.getCount()));
		mGroupUsersGVAdapter = new ChatRoomUserGVAdapter(this, pkIconFetcher);
		// >>>切换显示类型
		mBtnShowType = (Button) findViewById(R.id.btn_right);
		// >>>>>>>>title过滤器
		iconDrow = (ImageView) findViewById(R.id.icon_drow);
		iconDrow.setImageResource(R.drawable.icon32_arrow);

		mTitleTopLayout = (RelativeLayout) findViewById(R.id.title_top_ly);
		tvTitle = (TextView) findViewById(R.id.tv_title);
		tvTitle.setText(MENU_TITLE[2]);
		layoutTitle = (LinearLayout) findViewById(R.id.layout_title);

		layoutTitle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				popopQuickAction.show(mTitleTopLayout);
				iconDrow.setImageResource(R.drawable.icon32_arrow_p);
			}
		});

		popopQuickAction = new QuickAction(this, QuickAction.VERTICAL);
		for (int i = 0; i < MENU_INDEXS.length; i++) {
			if (i == MENU_INDEXS.length - 1) {
				popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[i], MENU_TITLE[i]), true);
			} else {
				popopQuickAction.addActionItem(new ActionItem(MENU_INDEXS[i], MENU_TITLE[i]));
			}
		}

		popopQuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				iconDrow.setImageResource(R.drawable.icon32_arrow);
			}
		});

		popopQuickAction.setOnActionItemClickListener(new OnActionItemClickListener() {

			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {
				// TODO Auto-generated method stub
				iconDrow.setImageResource(R.drawable.icon32_arrow);
				tvTitle.setText(MENU_TITLE[pos]);
				mGroupUsersAdapter.setmUserList(application.getCurrunGroup().getConlist());
				mGroupUsersGVAdapter.setmUserList(application.getCurrunGroup().getConlist());
				// >>>>>>>>>>男生
				if (actionId == MENU_INDEXS[0]) {

					mGroupUsersAdapter.showBoys();
					mGroupUsersGVAdapter.showBoys();

				}
				// >>>>>>>>女生
				else if (actionId == MENU_INDEXS[1]) {
					mGroupUsersAdapter.showGrils();
					mGroupUsersGVAdapter.showGrils();
				}
				// >>>>>>>>所有用户
				else if (actionId == MENU_INDEXS[2]) {
					mGroupUsersAdapter.showAllUsers();
					mGroupUsersGVAdapter.showAllUsers();
				}
			}
		});
	}

	// >>>>>>>>>>>加载头部按钮
	private void initTitleView() {
		// >>>>>>>>>>>>四个界面切换view初始化
		mGroupChatView = (LinearLayout) findViewById(R.id.group_chat);
		mGroupMembersView = (LinearLayout) findViewById(R.id.group_members);
		mGroupShareView = (LinearLayout) findViewById(R.id.chat_group_share);

		mBtnBack = (Button) findViewById(R.id.btn_back);
		mBtnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (ChatRoomMainActivity.this.getCurrentFocus() != null) {
					hideInputMethod(v);
				}
				backAndInto();
			}
		});

		mGroupName = (TextView) findViewById(R.id.mmtitle);
		mRadioGroup = (RadioGroup) findViewById(R.id.rg_group);
		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				hideSoftInput(group);
				switch (checkedId) {
				case R.id.rb_group_member:
					mGroupMembersView.setVisibility(View.VISIBLE);
					mGroupChatView.setVisibility(View.GONE);
					mGroupShareView.setVisibility(View.GONE);
					break;
				case R.id.rb_group_chat:
					mGroupMembersView.setVisibility(View.GONE);
					mGroupChatView.setVisibility(View.VISIBLE);
					mGroupShareView.setVisibility(View.GONE);
					break;
				case R.id.rb_group_history:
					mGroupMembersView.setVisibility(View.GONE);
					mGroupChatView.setVisibility(View.GONE);
					mGroupShareView.setVisibility(View.GONE);
					break;

				case R.id.rb_group_share:
					mGroupMembersView.setVisibility(View.GONE);
					mGroupChatView.setVisibility(View.GONE);
					mGroupShareView.setVisibility(View.VISIBLE);
					if (isFirst) {
						// 加载群组分享信息
						QueryResGroupShare(mBinder.getCurrenGroup().getGroupId(), isFirst);
						isFirst = false;
					}
					break;
				}
			}
		});
		menuQuickAction = new QuickAction(this, QuickAction.VERTICAL);
	}

	private void initGroupShareView() {
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_groupshare);
		pullToRefreshListView.setTag("pk_group_share");
		pullToRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				reset();
				QueryResGroupShare(mBinder.getCurrenGroup().getGroupId(), false);
			}
		});

		mGroupShareListView = pullToRefreshListView.getRefreshableView();
		// (ListView)findViewById(R.id.resshare_list);
		mGroupShareAdapter = new GroupShareAdapter(this, new ArrayList<ResGroupshare>(), mImageWorker);

		mGroupShareListView.setAdapter(mGroupShareAdapter);

		mGroupShareAdapter.setOnDeleteLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				mGroupshare = (ResGroupshare) v.getTag();
				if (mGroupshare.getShareUid().toString().equals(mMyUserID)
						|| mBinder.getCurrenGroup().getUid().equals(mMyUserID)) {
					showMenu();// showMsgMenu
				}
				return true;
			}
		});
		mGroupShareAdapter.setOnCoppyLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				mGroupshare = (ResGroupshare) v.getTag();

				showMsgMenu(mGroupshare);// showMsgMenu
				// if(mGroupshare.getShareUid().toString().equals(mMyUserID)
				// ||
				// mBinder.getCurrenGroup().getUid().equals(mMyUserID)){
				//
				// }
				return true;
			}
		});

		mGroupShareListView.setClickable(false);
		mGroupShareListView.setFocusable(false);
		mGroupShareEmptyView = new EmptyView(this);
		mGroupShareEmptyView.addToListView(mGroupShareListView);
		mGroupShareEmptyView.showView(R.drawable.emptylist_icon, R.string.no_recommend_groupshare);
		groupShareFooterView = new FooterView(this);
		groupShareFooterView.addToListView(mGroupShareListView);
		groupShareFooterView.setOnLoadMoreListener(onLoadMoreListener);
		mGroupShareEmptyView.setBtnOne("分享资源", new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				shareRes2Group();
			}
		});
		btnPhtopShare = (Button) findViewById(R.id.btn_left);
		btnPhtopShare.setBackgroundResource(R.drawable.btn_camera_selector);

		btnPhtopShare.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sharePhoto2Group();

			}
		});
		etShareText = (Button) findViewById(R.id.et_search);

		etShareText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TextGroupShare();
			}
		});
		etShareText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TextGroupShare();
			}
		});

	}

	protected void sharePhoto2Group() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(ChatRoomMainActivity.this, UploadPicActivity.class);
		intent.putExtra(UserGroup.GROUPID_KEY, mBinder.getCurrenGroup().getGroupId());
		intent.putExtra("ResMiddleManager_Flag", ResLocalActivity.ResLocalModel.upLoad);
		intent.putExtra(UploadPicActivity.ONLY_CAMERA_KEY, true);
		startActivity(intent);
	}

	private FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			QueryResGroupShare(mBinder.getCurrenGroup().getGroupId(), false);
		}
	};

	private void initGroup() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplicationContext();
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		// >>>>>>>>初始化耳机监听类
		audiomanage = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		headsetPlugReceiver = new HeadsetPlugReceiver();
		// >>>>>>>>服务service
		if (meetService == null) {
			meetService = new Intent(this, MeetRoomService.class);
		}

		parser = new SmileyParser(this);// 表情转化辅助工具
		// >>>获取当前用户的userid
		mMyUserID = application.getMyUserID();
		// >>>获取要加入的群组
		jionin_groupInfo = (UserGroup) getIntent().getSerializableExtra(JION_TO_GROUP);
		// >>>服务意图初始化
		// >>>广播消息
		msgInfoBroadcast = application.getMsgInfoBroadcast();
		// >>>显示mainActivity title栏群组图标
		sendBroadcast(new Intent(PangkerConstant.ACTION_GROUPLOGIN));

		// >>>初始化mMeetRoomDao
		mMeetRoomDao = new MeetRoomDaoImpl(ChatRoomMainActivity.this);
		// >>>添加监听
		PangkerManager.getTabBroadcastManager().addMsgListener(this);

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = true;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>设置图片浏览边长
		mImageWorker = new ImageFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		mImageWorker.setmSaveDiskCache(true);
		mImageWorker.setImageCache(mImageCache);
		mImageWorker.setLoadingImage(R.drawable.photolist_head);
	}

	ActionItem[] msgDeleteMenu = new ActionItem[] { new ActionItem(2, "删除") };
	private PopMenuDialog menuDialog;

	private void showMenu() {
		// TODO Auto-generated method stub
		menuDialog = new PopMenuDialog(this, R.style.MyDialog);
		menuDialog.setListener(mShareClickListener);
		menuDialog.setMenus(msgDeleteMenu);
		menuDialog.setTitle(mGroupshare.getResName());
		menuDialog.show();
	}

	private PopMenuDialog msgCoppyDialog;
	ActionItem[] msgCoppyMenu = new ActionItem[] { new ActionItem(1, "复制") };

	private void showMsgMenu(final ResGroupshare item) {
		// TODO Auto-generated method stub
		if (msgCoppyDialog == null) {
			msgCoppyDialog = new PopMenuDialog(this, R.style.MyDialog);
			msgCoppyDialog.setListener(mShareClickListener);
		}
		msgCoppyDialog.setMenus(msgCoppyMenu);
		msgCoppyDialog.setTitle("文字信息");
		msgCoppyDialog.show();

	}

	@Override
	protected void onResume() {
		// >>>>>>>>注册监听服务
		headsetPlugReceiver.registerHeadsetPlugReceiver(this, onHeadsetListener);
		super.onResume();
	}

	protected void onPause() {
		unregisterReceiver(headsetPlugReceiver);
		super.onPause();
	}

	private MeetRoomServiceBinder mBinder;
	private ServiceConnection conn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			// TODO Auto-generated method stub
			mBinder = (MeetRoomServiceBinder) binder;
			// 判断是返回群组还是进入一个新的群组
			if (mBinder.getCurrenGroup() == null && jionin_groupInfo != null) {
				// >>>没有进入任何群组
				// >>>1、设置即将要进入的群组
				application.setCurrunGroup(jionin_groupInfo);
				// >>>2、发送进入群组消息包
				sendPack(Configuration.CMANAGER2SP_LOGIN_REQ_FLAG);
				initData(jionin_groupInfo, false);
			}
			// >>>>>>>要加入的群组不是当前所在的群组
			else if (jionin_groupInfo != null && mBinder.getCurrenGroup() != null
					&& !jionin_groupInfo.getGroupId().equals(mBinder.getCurrenGroup().getGroupId())) {
				// >>>如果不是同一个群组,
				// >>>1、发送退出群组消息包
				sendPack(Configuration.CROOM2SP_STATUS_REPORT_MEMBER_DEL_FLAG);
				// >>>2、设置即将要进入的群组
				application.setCurrunGroup(jionin_groupInfo);
				// >>>3、发送进入群组消息包
				sendPack(Configuration.CROOM2SP_STATUS_REPORT_MEMBER_ADD_FLAG);
				// 初始化
				initData(jionin_groupInfo, false);
			}
			// >>>>>>>直接进入群组（当前所在群组）
			else if (jionin_groupInfo == null && mBinder.getCurrenGroup() != null) {
				// 初始化
				initData(mBinder.getCurrenGroup(), true);
			}
			// >>>>>>>>加入的群组就是当前所在的群组
			else if (jionin_groupInfo != null && mBinder.getCurrenGroup() != null
					&& jionin_groupInfo.getGroupId().equals(mBinder.getCurrenGroup().getGroupId())) {
				// 初始化
				initData(mBinder.getCurrenGroup(), true);
				// 点击返回时,关闭了群组房间
				// 获取15条未读,已经进入房间
				// sendPack(Configuration.CLIENT2SP_NOTREAD_REQ);
			}

			initListenner();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			showToast(getResources().getString(R.string.deal_fail));
			sendPack(Configuration.CMANAGER2SP_LOGINOUT_REQ_FLAG);
		}
	};

	private void showWaitingBar(int mess) {
		// TODO Auto-generated method stub
		if (bar == null) {
			bar = new WaitingDialog(this);
			bar.setOnDismissListener(onDismissListener);
		}
		bar.setWaitMessage(mess);
		bar.show();
	}

	DialogInterface.OnDismissListener onDismissListener = new DialogInterface.OnDismissListener() {
		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub

		}
	};

	// >>>>>>>>关闭等待进度条
	private void closeWaitingBar() {
		// TODO Auto-generated method stub
		if (bar != null && bar.isShowing()) {
			bar.dismiss();
		}
	}

	ActionItem[] msgMenu = new ActionItem[] { new ActionItem(1, "复制") };

	private void showMsgMenu() {
		// TODO Auto-generated method stub
		if (msgDialog == null) {
			msgDialog = new PopMenuDialog(this);
			msgDialog.setListener(mClickListener);
			msgDialog.setMenus(msgMenu);
		}
		msgDialog.setTitle("群组信息");
		msgDialog.show();
	}

	View.OnLongClickListener msgOnLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			msgItem = (MeetRoom) v.getTag();
			if (msgItem.getMsgType() == MeetRoom.s_type_text) {
				showMsgMenu();
			}
			return true;
		}
	};

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			// TODO Auto-generated method stub
			if (actionId == 1) {// 复制
				ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clip.setText(msgItem.getContent());
				msgDialog.dismiss();
			} else if (actionId == 2) {// 删除资源
				menuDialog.dismiss();
				deleteResGroupShare(mGroupshare);

			}

		}
	};

	UITableView.ClickListener mShareClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			// TODO Auto-generated method stub
			if (actionId == 1) {// 复制
				msgCoppyDialog.dismiss();
				ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clip.setText(mGroupshare.getShareReason());
			} else if (actionId == 2) {// 删除资源
				menuDialog.dismiss();
				deleteResGroupShare(mGroupshare);
			}

		}
	};

	/**
	 * TODO 删除群内分享资源 QueryResGroupShare.json
	 */
	private void deleteResGroupShare(final ResGroupshare groupShare) {
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				if (response != null) {
					BaseResult deleteResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
					if (deleteResult == null) {
						showToast(R.string.to_server_fail);
						return;
					} else if (deleteResult.getErrorCode() == BaseResult.SUCCESS) {
						mGroupShareAdapter.removeResGroupshares(groupShare);
					} else {
						showToast(deleteResult.getErrorMessage());
					}
				}
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", mMyUserID));
		paras.add(new Parameter("id", groupShare.getId().toString()));
		serverMana.supportRequest(Configuration.getDeleteResGroupShare(), paras, true, getString(R.string.please_wait));
	}

	/**
	 * 初始化控件
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:30
	 */
	private void initChatView() {
		// 显示加载信息
		Log.d("dialog", "showWaitingBar=>840");
		showWaitingBar(R.string.waiting);
		// 加载titleBar
		mBtnMenu = (Button) findViewById(R.id.iv_more);
		mBtnMenu.setBackgroundResource(R.drawable.btn_menu_bg);
		// >>>当前发言用户信息
		ly_speaker_info = (LinearLayout) findViewById(R.id.ly_speaker_info);
		iv_speaker_icon = (ImageView) findViewById(R.id.iv_speaker_icon);
		tv_speaker_name = (TextView) findViewById(R.id.tv_speaker_name);
		// >>>管理员信息
		ly_admin_info = (LinearLayout) findViewById(R.id.ly_admin_info);
		iv_admin_icon = (ImageView) findViewById(R.id.iv_admin_icon);
		iv_admin_mark = (ImageView) findViewById(R.id.iv_admin_mark);
		tv_admin_name = (TextView) findViewById(R.id.tv_admin_name);

		btn_micMode = (Button) findViewById(R.id.mic_mode);
		show_mic = (TextView) findViewById(R.id.show_mic);
		btn_upordown_mic = (Button) findViewById(R.id.btn_upordown_mic);
		btn_speech_mic = (Button) findViewById(R.id.btn_speech_mic);
		btn_speech_mic.setVisibility(View.GONE);// 初始化时不能发言,必须先抢麦
		micListView = (ListView) findViewById(R.id.miclist);
		msgListView = (ListView) findViewById(R.id.msglist);
		msgListView.setDivider(null);
		msgListView.setDividerHeight(0);
		
		btn_voice = (ImageButton) findViewById(R.id.btn_voice_sound);

		chat_layout = (LinearLayout) findViewById(R.id.chat_layout);
		speak_layout = (LinearLayout) findViewById(R.id.group_speak_layout);
		btn_type = (Button) findViewById(R.id.btn_chat_type);
		et_message = (EditText) findViewById(R.id.et_send_message);
		btnImgSend = (Button) findViewById(R.id.btn_send_phono);
		btn_sendMessage = (Button) findViewById(R.id.btn_send_message);
		btn_sendMessage.setEnabled(false);
		// >>>>>>>wangxin start
		chat_actions_tools = (LinearLayout) findViewById(R.id.chat_actions_tools);
		btn_actions = (Button) findViewById(R.id.btn_actions);
		btnLocationSend = (Button) findViewById(R.id.btn_send_location);
		btnExpression = (Button) findViewById(R.id.btn_expression);
		// 显示聊天记录 >>>wangxin
		more_chat_message = LayoutInflater.from(this).inflate(R.layout.more_chat_message, null);
		btn_more_message = (Button) (Button) more_chat_message.findViewById(R.id.btn_more_message);
		btn_more_message.setText("获取离线消息");
		msgListView.addHeaderView(more_chat_message);
		// 表情界面 >>>lb
		emoView = (EmoView) findViewById(R.id.emoview);
		mSpeakerLayout = (LinearLayout) findViewById(R.id.ly_speak_layout);
		mHideSpeaker = (Button) findViewById(R.id.btn_hidespeaker);

		initLitener();
	}

	private void initLitener() {
		// TODO Auto-generated method stub
		msgListView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				hideSoftInput(v);
				return false;
			}
		});
		emoView.setOnEmoClickListener(new EmoView.OnEmoClickListener() {
			@Override
			public void onEmoClickListener(View arg1, int position) {
				// TODO Auto-generated method stub
				String expressStr = getResources().getStringArray(R.array.express_item_texts)[position];
				String oldStr = et_message.getText().toString();
				et_message.setText(parser.addSmileySpans(oldStr + expressStr));
			}
		});

		mHideSpeaker.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mSpeakerLayout.getVisibility() == View.GONE) {
					mSpeakerLayout.setVisibility(View.VISIBLE);
					mHideSpeaker.setBackgroundResource(R.drawable.chatroom_speaker_collapse);
				} else {
					mSpeakerLayout.setVisibility(View.GONE);
					mHideSpeaker.setBackgroundResource(R.drawable.chatroom_speaker_unfold);
				}
			}
		});
	}

	/**
	 * 初始化群组显示数据
	 * 
	 * @param userGroup
	 *            群组
	 * @param isFirstTime
	 *            是否已经在群组中
	 */
	private void initData(UserGroup userGroup, boolean isJion) {
		// >>>>>>>进行耳机检测,如果是双通道便检测耳机，单通道就默认是有耳机的
		// >>>>>>>获取群组消息数据
		mChatMessageAdapter = new ChatRoomListAdapter(ChatRoomMainActivity.this, localFetcher);
		msgListView.setAdapter(mChatMessageAdapter);
		// >>>>>麦序适配器
		mMicAdapter = new MicListAdapter(ChatRoomMainActivity.this, userGroup.getGroupMics());
		micListView.setAdapter(mMicAdapter);
		micListView.setVisibility(View.GONE);
		mChatMessageAdapter.setMsgOnLongClickListener(msgOnLongClickListener);

		application.setChatRoomMainActivity(this);
		Play_flag = true;

		handler = new ChatRoomHandler();
		mGroupName.setText(userGroup.getGroupName());
		mChatMessageAdapter.setData(userGroup.getMessages());

		// >>>>>>..判断当前用户是否是管理员
		if (userGroup.isAdmin(mMyUserID)) {
			micMode = getResources().getString(R.string.room_micstart);
		} else {
			micMode = getResources().getString(R.string.mic_up);
		}
		// UP_DOWN = 0x41;// 0x00：无操作；0x41：抢麦，申请发言；0x42：下麦，退出申请；====>这个要通话，
		// >>>>>>>>>初始化
		if (application.getCurrunGroup().getSpeechStatus() == SpeechAllow.INITIALIZATION) {
			// >>>>>>>>>>判断是否是群组的创建者，如果是：占麦 ； 不是：抢麦
			btn_upordown_mic.setText(micMode);
		}
		// >>>>>>>麦序中
		else if (application.getCurrunGroup().getSpeechStatus() == SpeechAllow.IN_MIC_SEQUENCE) {
			btn_upordown_mic.setText(R.string.mic_down);
		}

		// >>>正在发言
		else if (application.getCurrunGroup().getSpeechStatus() == SpeechAllow.SPEECHING) {
			btn_upordown_mic.setText(R.string.room_micstop);
		}
		initPopmenu(userGroup.isAdmin(mMyUserID));
		setGroupVoice();
		menuQuickAction.setOnActionItemClickListener(actionItemClickListener);

		// >>>如果已经在群组当中
		if (isJion) {
			// >>>>>>>>>>>取消等待对话框
			closeWaitingBar();
			// >>>>>>>显示麦序列表
			// setMicCount(userGroup.getGroupMics().size());
			mMicAdapter.setMicList(userGroup.getGroupMics());

			// >>>>>>麦序列表个数显示
			setMicCount(userGroup.getGroupMics().size());
			// >>>>>>>>显示群组聊天记录信息
			mChatMessageAdapter.setData(userGroup.getMessages());
			msgListView.setSelection(mChatMessageAdapter.getCount());

			// >>>>>>>群组成员信息
			mGroupUsersAdapter.setmUserList(userGroup.getConlist());
			txtCount.setText(String.valueOf(mGroupUsersAdapter.getCount()));
			// >>>>>>>>>>>>绘制友踪地图模式
			// showMemberMap(userGroup.getConlist());
			// >>>>>>>>>显示发言人信息，目前方式，显示麦序列表第一个用户
			// 如果是单通道：管理员不在发言，就言显示麦序列表第一个用户；否则只显示管理员发言
			if (userGroup.isSingleVoiceChannel()) {
				if (userGroup.isAdminSpeaking()) {
					showAdminView(Integer.parseInt(userGroup.getUid()), true);
				} else {
					if (userGroup.getGroupMics().size() > 0) {
						showSpeakerView(mBinder.getCurrenGroup().getGroupMics().get(0), true);
					}
				}
			}
			// 如果是双通道：都进行显示，只有是在发言的===》Modify By Lb
			else {
				if (userGroup.isAdminSpeaking()) {
					showAdminView(Integer.parseInt(userGroup.getUid()), true);
				}
				if (userGroup.getGroupMics().size() > 0) {
					showSpeakerView(mBinder.getCurrenGroup().getGroupMics().get(0), true);
				}
			}
		}
	}

	// 判断当前用户是否是群组的创建者
	private void initPopmenu(boolean isAdmin) {
		// "聊天记录", "群设置", "定位设置", "分享资源", "退出群组"
		menuQuickAction.addActionItem(new ActionItem(GROUP_INVITE, "邀请好友"));
		menuQuickAction.addActionItem(new ActionItem(GROUP_SHARERES, "分享资源"));
		menuQuickAction.addActionItem(new ActionItem(GROUP_MAP, "群组地图"));
//		menuQuickAction.addActionItem(new ActionItem(GROUP_INFO, "群组首页"));
//		if (isAdmin) {
//			menuQuickAction.addActionItem(new ActionItem(GROUP_INFO_EDIT, "群组设置"));
//		}
		menuQuickAction.addActionItem(new ActionItem(GROUP_LOGOUT, "退出群组"), true);
	}
	
	private void setGroupVoice() {
		// TODO Auto-generated method stub
		if (application.getCurrunGroup().isAcceptVoice()) {
			btn_voice.setImageResource(R.drawable.group_voice_sound);
		} else {
			btn_voice.setImageResource(R.drawable.group_voice_mute);
		}
	}

	private P2PUserInfo getP2PUserInfoById(int userId) {
		if (mBinder.getCurrenGroup().getGroupMics() != null) {

		}
		for (P2PUserInfo p2pUserInfo : mBinder.getCurrenGroup().getConlist()) {
			if (p2pUserInfo.getUID() == userId) {
				return p2pUserInfo;
			}
		}
		return null;
	}

	// >>>>>>显示当前发言用户信息
	private void showSpeakerView(P2PUserInfo p2pUserInfo, boolean isShow) {
		// >>>>>>>>显示当前发言用户信息
		if (p2pUserInfo != null) {
			Log.i("infos", "当前发言用户信息 nitSpeakerView = " + p2pUserInfo.toString() + "  isShow" + isShow);
		}
		if (isShow) {
			ly_speaker_info.setVisibility(View.VISIBLE);
			mImageResizer.loadImage(String.valueOf(p2pUserInfo.getUID()), iv_speaker_icon);
			tv_speaker_name.setText(p2pUserInfo.getUserName());
		} else {
			ly_speaker_info.setVisibility(View.GONE);
		}
	}

	// >>>>>>显示管理员是否发言信息
	private void showAdminView(int uid, boolean isShow) {
		// TODO Auto-generated method stub
		if (application.getCurrunGroup().isAdmin(mMyUserID)) {
			application.getCurrunGroup().setAdminSpeaking(isShow);
		}
		if (isShow) {
			P2PUserInfo adminInfo = getP2PUserInfoById(uid);
			ly_admin_info.setVisibility(View.VISIBLE);
			if (adminInfo != null) {
				mImageResizer.loadImage(String.valueOf(adminInfo.getUID()), iv_admin_icon);
				tv_admin_name.setText(adminInfo.getUserName());
				iv_admin_mark.setVisibility(View.VISIBLE);
			}
		} else {
			ly_admin_info.setVisibility(View.GONE);
		}
	}

	/**
	 * 初始化监听
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:52
	 */
	private void initListenner() {
		// >>>>>>>wangxin start
		btn_actions.setOnClickListener(onClickListener);
		btnLocationSend.setOnClickListener(onClickListener);
		// >>>>>>>>wangxin end
		btn_more_message.setOnClickListener(onClickListener);
		btn_micMode.setOnClickListener(onClickListener);
		btn_voice.setOnClickListener(onClickListener);
		mBtnMenu.setOnClickListener(onClickListener);
		btn_speech_mic.setOnClickListener(onClickListener);
		btn_sendMessage.setOnClickListener(onClickListener);
		btn_type.setOnClickListener(onClickListener);
		et_message.addTextChangedListener(new TextButtonWatcher(btn_sendMessage));
		et_message.setFilters(new InputFilter[] { new EdInputFilter(MAX_MSG_LIMIT) });
		et_message.setOnTouchListener(onTouchListener);
		btnImgSend.setOnClickListener(onClickListener);
		btnExpression.setOnClickListener(onClickListener);
		mBtnShowType.setOnClickListener(onClickListener);

		new BtnOnClickListener(btn_upordown_mic, handler);
	}

	View.OnTouchListener onTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if (v == et_message) {
				emoView.setVisibility(View.GONE);
				chat_actions_tools.setVisibility(View.GONE);
			}
			return false;
		}
	};

	private void sendTxtMessage() {
		// TODO Auto-generated method stub
		String txtMsg = et_message.getText().toString();
		if (txtMsg.length() > 0) {
			try {
				if (Util.getHalfStringLength(txtMsg) > 70) {
					showToast("亲，发送消息内容有些长哦！");
					return;
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			meetService.putExtra(MeetRoomUtil.MEETROOM_CHAT_TXT_MESSAGE_CONTENT, txtMsg);
			sendPack(Configuration.CP2SP_STREAM_TEXT_FLAG);
			et_message.setText("");
		}
	}

	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

			if (v == btn_micMode) {// 隐藏麦序
				if (micListView.getVisibility() == View.VISIBLE) {
					micListView.setVisibility(View.GONE);
				} else {
					micListView.setVisibility(View.VISIBLE);
				}
			} else if (v == btn_sendMessage) {// 发送文字
				sendTxtMessage();
			} else if (v == btnImgSend) { // 发送图片按钮
				chat_actions_tools.setVisibility(View.GONE);
				showImageDialog("发送图片发送图片", actionItems2, false);
			}

			else if (v == btn_speech_mic) {// 发言
				if (application.getCurrunGroup().getSpeechStatus() != SpeechAllow.SPEECHING) {
					startSpeak();
				} else {
					sendPack(Configuration.STOP_AUDIO_FLAG);
				}

			} else if (v == btn_type) {// 功能切换
				if (chat_layout.getVisibility() == View.VISIBLE) {
					btn_type.setBackgroundResource(R.drawable.btn_keyboard_selector);
					chat_layout.setVisibility(View.GONE);
					speak_layout.setVisibility(View.VISIBLE);
					hideInputMethod(v);
				} else {
					btn_type.setBackgroundResource(R.drawable.btn_voice_selector);
					chat_layout.setVisibility(View.VISIBLE);
					speak_layout.setVisibility(View.GONE);
				}
				// >>>>>>>工具栏隐藏
				chat_actions_tools.setVisibility(View.GONE);
				// >>>>>>>>表情隐藏
				emoView.setVisibility(View.GONE);
			} else if (v == mBtnMenu) {
				menuQuickAction.show(mBtnMenu);
			} else if (v == btn_actions) { // 更多功能操作按钮 >>>>>>>>>>>wangxin add
				// start 功能按钮实现
				if (emoView.getVisibility() == View.VISIBLE) {
					emoView.setVisibility(View.GONE);
				}
				chat_layout.setVisibility(View.VISIBLE);
				speak_layout.setVisibility(View.GONE);
				hideInputMethod(v);
				if (chat_actions_tools.getVisibility() == View.VISIBLE) {
					chat_actions_tools.setVisibility(View.GONE);
				} else {
					chat_actions_tools.setVisibility(View.VISIBLE);
				}
			} else if (v == btnLocationSend) { // 发送位置信息按钮>>>>>>>wangxin
				chat_actions_tools.setVisibility(View.GONE);
				Intent intent = new Intent(ChatRoomMainActivity.this, MsgLoactionActivity.class);
				startActivityForResult(intent, LOCATION_SEND);

			} else if (v == btn_more_message) { // 查看群组的聊天记录>>>>>>>wangxin
				sendPack(Configuration.CLIENT2SP_NOTREAD_REQ);
				// btn_more_message.setVisibility(View.GONE);
			}
			// >>>>>>>切换显示方式按钮
			else if (v == mBtnShowType) {
				if (currListType == LIST_TYPE) {
					mBtnShowType.setBackgroundResource(R.drawable.btn_list_mode_selector);
					mGroupMembersListView.setAdapter(mGroupUsersGVAdapter);
					mGroupUsersGVAdapter.setmUserList(application.getCurrunGroup().getConlist());
					mGroupMembersListView.setDividerHeight(0);
					currListType = GRID_TYPE;
				} else if (currListType == GRID_TYPE) {
					mBtnShowType.setBackgroundResource(R.drawable.btn_grid_mode_selector);
					mGroupMembersListView.setAdapter(mGroupUsersAdapter);
					mGroupUsersAdapter.setmUserList(application.getCurrunGroup().getConlist());

					mGroupMembersListView.setDividerHeight(1);
					currListType = LIST_TYPE;
				}
			}
			if (v == btnExpression) {
				chat_actions_tools.setVisibility(View.GONE);
				emoView.setVisibility(View.VISIBLE);
				hideInputMethod(v);
			}
			if(v == btn_voice){
				if (application.getCurrunGroup().isAcceptVoice()) {
					sendPack(Configuration.CROOM2SP_STATUS_RADIO_ACCEPT_STOP_FLAG);
				} else {
					sendPack(Configuration.CROOM2SP_STATUS_RADIO_ACCEPT_PLAY_FLAG);
				}
			}
		}
	};

	private void hideInputMethod(View v) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	class BtnOnClickListener extends BtnOnClickNumOrTimeListener {

		public BtnOnClickListener(Button button, Handler handler) {
			super(button, handler);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void doClick(View v, int resultCode) {
			// TODO Auto-generated method stub
			if (resultCode == 1) {
				showToast("请求太频繁,请5秒之后再点击!");
				return;
			}
			if (v == btn_upordown_mic) {// 抢麦 or 下麦
				btn_speech_mic.setVisibility(View.GONE);
				// >>>>>>..初始化状态，可以抢麦
				if (application.getCurrunGroup().getSpeechStatus() == SpeechAllow.INITIALIZATION) {
					speechMicUP();
				}
				// >>>>>>>>>>>>在麦序中
				else if (application.getCurrunGroup().getSpeechStatus() == SpeechAllow.IN_MIC_SEQUENCE) {
					// >>>>>>>下麦包
					speechMicDOWN();
				}
				// >>>>>>>>>正在发言
				else if (application.getCurrunGroup().getSpeechStatus() == SpeechAllow.SPEECHING) {
					// 停止发言的包
					sendPack(Configuration.STOP_AUDIO_FLAG);
				}
			}
		}

	}

	/**
	 * 开始发言
	 * 
	 * @author wubo
	 * @createtime Apr 6, 2012
	 */
	public void startSpeak() {
		Log.d("setSendAmr", "OnCkick==>");
		if (!application.getCurrunGroup().isSingleVoiceChannel() && !audiomanage.isWiredHeadsetOn()) {
			showToast("如果要发言,请先插上耳机!");
			return;
		}
		if (application.getCurrunGroup().isAdmin(mMyUserID)) {
			showAdminView(Integer.parseInt(mMyUserID), true);
			// 如果是单通道，停止显示成员发言
			if (application.getCurrunGroup().isSingleVoiceChannel()) {
				showSpeakerView(null, false);
			}
		}

		// >>>>>>>>.设置状态为正在发言
		application.getCurrunGroup().setSpeechStatus(SpeechAllow.SPEECHING);
		btn_upordown_mic.setVisibility(View.GONE);
		btn_speech_mic.setVisibility(View.VISIBLE);
		btn_speech_mic.setText(getResources().getString(R.string.room_micstop));
		sendPack(Configuration.START_AUDIO_FLAG);
	}

	/**
	 * 取消发言 || 停止发言
	 * 
	 * @author wubo
	 * @createtime Apr 6, 2012
	 */
	public void stopSpeakView() {
		if (application.getCurrunGroup().isAdmin(mMyUserID)) {
			showAdminView(-1, false);
			setMicAndshowAdapterOrView();
		}
		btn_speech_mic.setVisibility(View.GONE);
		btn_upordown_mic.setVisibility(View.VISIBLE);

		// >>>>>>>>>停止发言：设置为初始化
		application.getCurrunGroup().setSpeechStatus(SpeechAllow.INITIALIZATION);

		// 如果是群组管理员，显示发言，否则显示抢麦
		if (application.getCurrunGroup().isAdmin(mMyUserID)) {
			btn_upordown_mic.setText(R.string.room_micstart);
		} else {
			btn_upordown_mic.setText(R.string.mic_up);
		}
		btn_upordown_mic.setEnabled(true);
		setMicCount(mBinder.getCurrenGroup().getGroupMics().size());
	}

	/**
	 * 设置当前麦列表的人数
	 * 
	 * @author wubo
	 * @createtime 2012-3-1 上午10:06:02
	 */
	private void setMicCount(int count) {
		show_mic.setText(getResources().getString(R.string.mic_mode) + ": " + count
				+ getResources().getString(R.string.mic_count));
	}

	// >>>>>>>>>退出群组
	private void Logout() {
		sendPack(Configuration.CMANAGER2SP_LOGINOUT_REQ_FLAG);
	}

	private void logoutGroup() {
		// TODO Auto-generated method stub
		Log.d("MeetRoomService", "logoutGroup===>");
		closeWaitingBar();
		PangkerManager.getTabBroadcastManager().removeUserListenter(this);
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// 当在群组信息界面点击退出群组时
		if (requestCode == REQUEST_TO_ChatRoomUserListActivity && resultCode == RESULT_OK) {
			// >>>>>>退出群组
			Logout();
		} else if (resultCode == RESULT_OK && requestCode == PangkerConstant.RES_PICTURE) {
			String filepath = data.getStringExtra(DocInfo.FILE_PATH);
			sendFile(filepath, requestCode);
		}
		// >>>>>>>>>>>>>wangxin 显示用户当前在哪
		else if (requestCode == LOCATION_SEND && resultCode == RESULT_OK) {
			Location location = (Location) data.getSerializableExtra("Location");
			meetService.putExtra(MeetRoomUtil.MEETROOM_CHAT_LOCATION_CONTENT, location);
			sendPack(Configuration.CP2SP_STREAM_LOCATION_FLAG);
		}
		// >>>>>>>>>>>>>wangxin 显示用户当前在哪
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 点击返回后要保存 麦的状态
	 */
	private void backAndInto() {
		PangkerManager.getTabBroadcastManager().removeUserListenter(this);
		finish();
	}

	public class ChatRoomHandler extends Handler {
		public ChatRoomHandler() {

		}

		public ChatRoomHandler(Looper looper) {
			super(looper);
		}

		/**
		 * >>>>>>>>>lb, msg.arg1已经语音在双通道中被设置成speakid,如果要添加参数，最后不要使用该参数
		 */
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			if (msg.what == MeetRoomUtil.handler_SP2CRoom_Disconnect) {
				// >>>>>>>>>>>>退出该界面,同时需要停止群组的服务MeetRoomService
				String message = (String) msg.obj;
				if (!Util.isEmpty(message))
					showToast(message);
				logoutGroup();
			}
			// >>>>>>>如果当前不在任何群组当中，
			if (application.getCurrunGroup() == null) {
				return;
			}
			switch (msg.what) {
			case MeetRoomUtil.handler_SpeechMicUP_Notify:// >>>>>>>>占麦
				speechMicUP();
				break;
			case MeetRoomUtil.handler_SpeechMicDOWN_Notify:// >>>>>下麦
				speechMicDOWN();
				break;
			case MeetRoomUtil.handler_SP2CRoom_Login_Resp:// 登陆成功提示
				initialiseGroup();
				showToast("进入群组成功!");
				break;
			case MeetRoomUtil.handler_SetTime_Flag:// 时间
				setMicCount(mBinder.getCurrenGroup().getGroupMics().size());
				show_mic.setText(show_mic.getText().toString() + " 剩" + msg.arg1 + "s");
				if (msg.arg1 <= 0) {
					// >>>>>>..如果是管理员，去掉管理员发言显示
					if (application.getCurrunGroup().isAdmin(mMyUserID)) {
						showAdminView(-1, false);
					}
					btn_upordown_mic.setText(getResources().getString(R.string.mic_up));
					btn_upordown_mic.setEnabled(true);
				}
				break;
			case MeetRoomUtil.handler_SP2CRoom_Status_Resp:// 进入群组结果反馈
				SP2CRoom_Status_Resp ssr = (SP2CRoom_Status_Resp) msg.obj;
				if (ssr.getMembersOp() == 0x11) {
					btn_micMode.setEnabled(true);
					// >>>>>>>>关闭等待进度
					closeWaitingBar();
					int ssrcode = 1;
					if (ssr.getError_Code() == 0x0000 || ssr.getError_Code() == 1376) {
						ssrcode = 0;
					} else {
						ssrcode = 1;
					}
					// >>>>无法加入群组
					if (ssrcode != 0) {
						showToast(getResources().getString(R.string.login_sp_fail));
						backAndInto();
					}
				} else if (ssr.getMembersOp() == 0x17 || ssr.getMembersOp() == 0x16) {
					showToast("设置成功!");
					setGroupVoice();
				}
				break;
			case MeetRoomUtil.handler_SP2CRoom_Status_Notify:// 房间参与者
				Log.d("SpeechListStatus", "handler_SP2CRoom_Status_Notify==>" + msg.arg1);
				// >>>>>>>>>>>>群组内人员变更通知
				mGroupUsersAdapter.setmUserList(application.getCurrunGroup().getConlist());
				txtCount.setText(String.valueOf(mGroupUsersAdapter.getCount()));
				// showMemberMap(application.getCurrunGroup().getConlist());
				break;
			case MeetRoomUtil.handler_SP2CRoom_Speech_Notify:// 发言通知(在10秒内需要点击发言)
				Log.d("SpeechListStatus", "handler_SP2CRoom_Speech_Notify==>" + msg.arg1);
				// >>>>>>>>增加管理员发言通知 需要判断是不是自己发言 speekerUID == myUserID
				// ;true：按照原来流程 false：界面多显示一个用户发言的view
				if (mMyUserID.equals(String.valueOf(msg.arg1))) {
					// >>>>>>>当前状态为抢麦（判断当前是否在麦序列表中）
					if (application.getCurrunGroup().getSpeechStatus() == SpeechAllow.IN_MIC_SEQUENCE) {
						// >>>>>>>>设置成可以发言（给予用户10S时间点击发言按钮，如果不点击取消发言资格！！！）
						application.getCurrunGroup().setSpeechStatus(SpeechAllow.CAN_SPEAK);
						btn_speech_mic.setText(R.string.room_micstart);
						popSpeechDialog();
						// >>>>>>>>>>>显示第一个发言成员信息
						setMicAndshowAdapterOrView();
					}
				}
				// >>显示管理员发言
				else {
					showAdminView(msg.arg1, true);
					if (!application.getCurrunGroup().isAdmin(mMyUserID)
							&& application.getCurrunGroup().isSingleVoiceChannel()) {
						showSpeakerView(null, false);
					}
				}
				break;
			case MeetRoomUtil.handler_SP2NextHop_Text_Stream:// 文字聊天
				MeetRoom mr = (MeetRoom) msg.obj;
				if (mMeetRoomDao.saveRoomHistory(mr)) {
					application.getCurrunGroup().addMessage(mr);
					if (PangkerManager.getActivityStackManager().isActivityShow(ChatRoomMainActivity.this)) {
						msgInfoBroadcast.sendMeetroom(mr, 0);
					} else {
						msgInfoBroadcast.sendMeetroom(mr, 1);
					}
					mChatMessageAdapter.setData(application.getCurrunGroup().getMessages());
					msgListView.setSelection(mChatMessageAdapter.getCount());
				}
				break;
			case MeetRoomUtil.handler_SP2CRoom_Speech_Resp:// 抢麦或下麦
				Log.i(TAG, ">>>>>>>>>抢麦或下麦");
				if (msg.arg1 == 0x0000) {
					// >>>>>>>>>0x41抢麦
					if (application.getCurrunGroup().getSpeechStatus() == SpeechAllow.INITIALIZATION) {
						btn_upordown_mic.setText(R.string.mic_down);
						// >>>>>>>>设置成在麦序中等待
						application.getCurrunGroup().setSpeechStatus(SpeechAllow.IN_MIC_SEQUENCE);

						// >>>>>>>>>>如果是管理员用户直接发言
						if (mMyUserID.equals(mBinder.getCurrenGroup().getUid())) {
							btn_speech_mic.setVisibility(View.VISIBLE);
							showToast("您可以开始发言了！");
							startSpeak();
						}
					}
					// >>>>>>>>>0x42下麦
					else {
						// >>>>>>设置成初始化状态
						application.getCurrunGroup().setSpeechStatus(MeetRoomUtil.SpeechAllow.INITIALIZATION);
						btn_upordown_mic.setText(micMode);
						btn_speech_mic.setVisibility(View.GONE);
					}
					btn_upordown_mic.setEnabled(true);
				} else if (msg.arg1 == 0x050B) {
					showToast("麦序列表超出限制,请稍后再进行申请!");
					btn_upordown_mic.setEnabled(true);
				}
				break;
			case MeetRoomUtil.handler_SP2CRoom_SpeechList_Rsep:
				Log.d("SpeechListStatus", "MeetRoomUtil.handler_SP2CRoom_SpeechList_Rsep");
				setMicAndshowAdapterOrView();
				// />>>>>>>显示管理员是否在发言Modify By Lb
				if (application.getCurrunGroup().isAdminSpeaking()) {
					Log.d("SpeechListStatus", "======>isAdminSpeaking");
					showAdminView(msg.arg1, true);
				}
				break;
			// >>>>麦序通知,如果是双通道，则不显示抢麦信息
			case MeetRoomUtil.handler_SP2CRoom_SpeechListStatus_Notify:
				Log.d("SpeechListStatus", "MeetRoomUtil.handler_SP2CRoom_SpeechListStatus_Notify");
				setMicAndshowAdapterOrView();//
				break;
			case MeetRoomUtil.handler_SP2CRoom_StreamStop:
				Log.d("SpeechListStatus", "MeetRoomUtil.handler_SP2CRoom_StreamStop");
				stopSpeakView();
				break;
			case MeetRoomUtil.handler_SP2CRoom_MicUp:// >>>>>>>>>.麦序变动
				btn_upordown_mic.setText(R.string.mic_down);
				btn_upordown_mic.setEnabled(true);

				break;
			case MeetRoomUtil.handler_SP2CRoom_MicCatch:
				String speakId2 = String.valueOf(msg.arg1);
				Log.d("SpeechListStatus", "handler_SP2CRoom_MicCatch==>" + speakId2);
				if (mMyUserID.equals(speakId2)) {

					// >>>>>>..等待用户点击发言/取消发言处理的时间，管理员抢麦
					if (messageTipDialog != null && messageTipDialog.isShowing()
							&& application.getCurrunGroup().getSpeechStatus() == SpeechAllow.CAN_SPEAK) {
						cancelSpeechDialog();
					} else {
						stopSpeakView();
					}

					application.getCurrunGroup().setSpeechStatus(MeetRoomUtil.SpeechAllow.IN_MIC_SEQUENCE);

					// >>>>>>震动提示当前用户，管理员抢麦发言，迫使当前用户停止发言
					Util.Vibrate(ChatRoomMainActivity.this, 500);
					showToast(getResources().getString(R.string.room_adminwait));
					btn_upordown_mic.setText(R.string.mic_down);

					btn_upordown_mic.setVisibility(View.VISIBLE);
					btn_upordown_mic.setEnabled(true);
					btn_speech_mic.setVisibility(View.GONE);
				} else {
					// >>隐藏管理员抢麦
					showAdminView(msg.arg1, false);
					setMicAndshowAdapterOrView();
				}
				break;
			case MeetRoomUtil.handler_SP2Login_Fail:// 登陆失败通知
				showToast(getResources().getString(R.string.login_sp_fail));
				backAndInto();
				break;
			case MeetRoomUtil.handler_SP2Client_NotRead_Resp:// 离线消息,登陆之后获取群组的离线消息
				ArrayList<MeetRoom> mrs = (ArrayList<MeetRoom>) msg.obj;
				if (mrs != null && mrs.size() != 0) {
					mMeetRoomDao.saveMsgs(mrs); // >>>>>显示未读消息
					application.getCurrunGroup().addMessages(mrs, true);
					if (msg.arg1 > 0) {
						msgListView.setSelection(1);
						btn_more_message.setVisibility(View.VISIBLE);
						btn_more_message.setText("还有 " + msg.arg1 + " 条离线消息");
					} else {
						btn_more_message.setVisibility(View.GONE);
						showToast("在当前群组中,没有离线消息记录...");
					}
					mChatMessageAdapter.setData(application.getCurrunGroup().getMessages());
				} else {
					btn_more_message.setVisibility(View.GONE);
					showToast("在当前群组中,没有离线消息记录...");
				}
				break;
			case MeetRoomUtil.handler_SP2NextHop_PIC_Stream: // 图片消息记录
				// >>>>>>有两种case 1:图片接收中 ；2：图片接收完成
				mChatMessageAdapter.notifyDataSetChanged();
				break;
			case MeetRoomUtil.handler_SP2Status_Notify:// 文字聊天
				mr = (MeetRoom) msg.obj;
				if (mMeetRoomDao.saveRoomHistory(mr)) {
					application.getCurrunGroup().addMessage(mr);
					mChatMessageAdapter.setData(application.getCurrunGroup().getMessages());
				}
				break;
			case MeetRoomUtil.handler_SP2CRoom_ReLogin:
				showWaitingBar(R.string.waiting);
				initialiseGroup();
				break;
			}
		}
	};

	/**
	 * 在进行重新登录的时候，将用户的状态初始化到默认
	 */
	private void initialiseGroup() {
		// TODO Auto-generated method stub
		application.getCurrunGroup().setSpeechStatus(SpeechAllow.INITIALIZATION);
		showSpeakerView(null, false);
		showAdminView(0, false);
		btn_micMode.setText(R.string.room_micstart);
		btn_micMode.setEnabled(false);
		// >>>>>>..判断当前用户是否是管理员
		if (application.getCurrunGroup().isAdmin(mMyUserID)) {
			micMode = getResources().getString(R.string.room_micstart);
		} else {
			micMode = getResources().getString(R.string.mic_up);
		}
		// >>>>>>>>>>判断是否是群组的创建者，如果是：占麦 ； 不是：抢麦
		btn_upordown_mic.setVisibility(View.GONE);
		btn_upordown_mic.setEnabled(true);
		btn_upordown_mic.setText(micMode);
	}

	/**
	 * 如果是单通道，管理员在发言，那么不显示成员发言，否则显示； 如果是双通道，就显示成员发言
	 * 
	 * @return
	 */
	private boolean checkShowSpeakView() {
		if (!application.getCurrunGroup().isSingleVoiceChannel()) {
			return true;
		} else {
			return !application.getCurrunGroup().isAdminSpeaking();
		}
	}

	/**
	 * 设置和显示麦序相关控件
	 */
	private synchronized void setMicAndshowAdapterOrView() {
		setMicCount(mBinder.getCurrenGroup().getGroupMics().size());
		mMicAdapter.setMicList(mBinder.getCurrenGroup().getGroupMics());
		// >>>>>>>>>显示发言人信息，目前方式，显示麦序列表第一个用户
		if (mBinder.getCurrenGroup().getGroupMics().size() > 0) {
			// >>>麦序列表不为空显示发言用户信息,如果是单通道，只有开始发言才显示
			if (checkShowSpeakView()) {
				showSpeakerView(mBinder.getCurrenGroup().getGroupMics().get(0), true);
			}
		} else {
			// >>>麦序列表为空不显示发言用户信息
			showSpeakerView(null, false);
		}
	};

	/**
	 * 取聊天历史记录
	 */
	private void getChatHistroy() {
		final List<MeetRoom> list = mMeetRoomDao.getLimitHistory(mBinder.getCurrenGroup().getGroupId(), 0,
				DEFAULT_SHOW_MESSAGE_COUNT);
		if (list == null) {
			return;
		}
		application.getCurrunGroup().addMessages(list);
		// Collections.sort(mMessageListData, comparator);
		mChatMessageAdapter.setData(application.getCurrunGroup().getMessages());
	}

	// >>>>>>>等待点击发言时间
	private static final int speechWaitTime = 10000;
	private int speechNoticeTime = 11;
	private Timer speechWaitTimer;
	private MessageTipDialog messageTipDialog;

	// >>>>>>>等待发言dialog
	private void popSpeechDialog() {
		// TODO Auto-generated method stub
		btn_speech_mic.setText(getResources().getString(R.string.room_micstart));

		messageTipDialog = new MessageTipDialog(new DialogMsgCallback() {
			@Override
			public void buttonResult(boolean isSubmit) {
				speechWaitTimer.cancel();
				// TODO Auto-genaerated method stub
				// >>>>点击发言按钮
				if (isSubmit) {
					// >>>>开始发言
					if (application.getCurrunGroup().getSpeechStatus() != SpeechAllow.SPEECHING) {
						startSpeak();
					}
					// >>>>>>>下麦
					else {
						// >>>>>>>>下麦
						handler.handleMessage(handler.obtainMessage(MeetRoomUtil.handler_SpeechMicDOWN_Notify));
					}
				}
				// >>>>点击取消发言按钮
				else {
					// >>>>>>>>下麦
					handler.handleMessage(handler.obtainMessage(MeetRoomUtil.handler_SpeechMicDOWN_Notify));
				}
			}
		}, ChatRoomMainActivity.this);
		// >>>设置点击不能取消
		messageTipDialog.setCancelable(false);
		// 显示dialog
		messageTipDialog.showDialog("发言通知:", "点击发言按钮开始发言,10S后取消本次发言资格!", false);

		// >>>>>>启用定时器
		speechWaitTimer = new Timer();
		speechWaitTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (messageTipDialog != null && messageTipDialog.isShowing()) {
					messageTipDialog.dismiss();
					// >>>>>>>>下麦
					handler.post(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							handler.handleMessage(handler.obtainMessage(MeetRoomUtil.handler_SpeechMicDOWN_Notify));
						}
					});
				}
				speechWaitTimer.cancel();
			}
		}, speechWaitTime);

		// >>>>>>设置通知秒数
		speechNoticeTime = 11;
		speechWaitTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				speechNoticeTime--;
				if (messageTipDialog != null && messageTipDialog.isShowing()) {
					messageTipDialog.refreshContent("发言通知:" + String.valueOf(speechNoticeTime) + "S后取消本次发言资格!");
				}
			}
		}, 0, 1000);
	}

	private void cancelSpeechDialog() {
		// TODO Auto-generated method stub
		speechNoticeTime = 11;
		if (speechWaitTimer != null) {
			speechWaitTimer.cancel();
		}
		messageTipDialog.dismiss();
		messageTipDialog = null;
	}

	// 已经将文件发送到http，得到响应成功后，将Key通过Sp分发出去。
	public void sendFileResult(MeetRoom msg, String fileName) {
		// TODO Auto-generated method stub
		meetService.putExtra(MeetRoomUtil.MEETROOM_CHAT_TXT_MESSAGE_CONTENT, fileName);
		sendPack(Configuration.CP2SP_STREAM_PIC_FLAG);
		Log.d("MeetRoom", fileName);
	}

	// 发送文件md by lb, 首先发送到http服务器，待key值返回之后再发给sp服务器进行分发，由于定义的不一致，先写死成图片
	private void sendFile(String filepath, int msgType) {
		// File file = new
		// File("/mnt/sdcard/DCIM/Camera/2012-12-20 17.13.27.jpg");
		File file = new File(filepath);
		if (!file.exists()) {
			showToast("文件不存在，请检查后在发送!");
			return;
		}

		MeetRoom rm = new MeetRoom(mMyUserID);
		rm.setContent(filepath);
		rm.setMsgType(MeetRoom.s_type_pic);
		rm.setDirection(MeetRoom.MSG_FROM_ME);
		rm.setUserId(mMyUserID);
		rm.setGid(application.getCurrunGroup().getGroupId());
		rm.setTalkTime(application.getGroupSPServerTime());
		rm.setGroupname(application.getCurrunGroup().getGroupName());
		rm.setStatus(UploadJob.UPLOAD_INITOK);
		if (mMeetRoomDao.saveRoomHistory(rm)) {
			application.getCurrunGroup().addMessage(rm);
			mChatMessageAdapter.setData(application.getCurrunGroup().getMessages());
		}
	}

	// >>>>>>>>>>>>抢麦
	private void speechMicUP() {
		// 检测耳机是否插上
		if (!application.getCurrunGroup().isSingleVoiceChannel() && !audiomanage.isWiredHeadsetOn()) {
			showToast("如果要发言,请先插上耳机!");
			return;
		}

		sendPack(Configuration.CROOM2SP_SPEECH_REQ_FLAG_UP);
		btn_upordown_mic.setEnabled(false);
	}

	// >>>>>>>>>>>>发送下麦通知
	private void speechMicDOWN() {
		sendPack(Configuration.CROOM2SP_SPEECH_REQ_FLAG_DOWN);
		btn_upordown_mic.setEnabled(true);
	}

	public void sendPack(int flag) {
		if (application.getCurrunGroup() == null) {
			Log.d(TAG, "application.getCurrunGroup() = null");
			return;
		}
		// >>>>>>>>>下麦判断是否是管理员 ：管理员目前GroupSP没有通知停止发言的包
		if (application.getCurrunGroup().isAdmin(mMyUserID) && flag == Configuration.STOP_AUDIO_FLAG) {
			// >>>>>>>>>
			showAdminView(-1, false);
		}
		meetService.putExtra(MeetRoomService.MEETING_ROOM_INTENT, flag);
		startService(meetService);
	}

	private OnActionItemClickListener actionItemClickListener = new OnActionItemClickListener() {

		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			// TODO Auto-generated method stub
			// >>>>>>>>>成员过滤菜单
			switch (actionId) {
			case GROUP_INFO_EDIT:
				// TODO Auto-generated method stub
				Intent intenteidt = new Intent();
				intenteidt.putExtra("flag", "1");
				intenteidt.putExtra("groupId", application.getCurrunGroup().getGroupId());
				intenteidt.setClass(ChatRoomMainActivity.this, ChatRoomCreateActivity.class);
				startActivityForResult(intenteidt, 10);
				break;
			case GROUP_LOGOUT:// >>>>退出群组
				Logout();
				break;
			case GROUP_HISTORY:// >>>>查看直播记录>>>>>>跳转到群组直播记录界面
				Intent intent = getIntent();
				intent.putExtra("UserGroup", jionin_groupInfo);
				intent.setClass(ChatRoomMainActivity.this, GroupMessageHistoryActivity.class);
				startActivity(intent);
				break;

			case GROUP_VISITOR:// >>>>群组设置
				// >>>>>>跳转到群基本信息修改界面
				Intent mUpdateIntent = new Intent();
				mUpdateIntent.putExtra("flag", "1");
				mUpdateIntent.putExtra("groupId", mBinder.getCurrenGroup().getGroupId());
				mUpdateIntent.setClass(ChatRoomMainActivity.this, ChatRoomCreateActivity.class);
				startActivity(mUpdateIntent);
				break;
			case GROUP_INVITE:
				Intent mInviteIntent = new Intent();
				mInviteIntent.putExtra("groupInfo", mBinder.getCurrenGroup());
				mInviteIntent.putExtra(ContactsSelectActivity.INTENT_SELECT, ContactsSelectActivity.TYPE_GROUP_INVITE);
				mInviteIntent.setClass(ChatRoomMainActivity.this, ContactsSelectActivity.class);
				startActivity(mInviteIntent);
				break;
			case GROUP_SHARERES:// >>>>群组分享
				// >>>>>>跳转到分享资源到群组的界面
				shareRes2Group();
				break;
			case GROUP_SHARE_TXT:
				TextGroupShare();
				break;
			case GROUP_INFO:
				lookChatRoomInfo(mBinder.getCurrenGroup());
				break;
			case GROUP_MAP:
				showConfrimGroupMap();
				break;
			}
		}
	};

	/**
	 * TODO 查询群内分享资源 QueryResGroupShare.json
	 */
	public void QueryResGroupShare(String groupId, boolean isFrist) {
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				pullToRefreshListView.onRefreshComplete();
				if (!HttpResponseStatus(response)) {
					groupShareFooterView.onLoadCompleteErr();
					return;
				}
				QueryResGroupShareResult shareResult = JSONUtil.fromJson(response.toString(),
						QueryResGroupShareResult.class);
				if (shareResult != null && shareResult.getErrorCode() == BaseResult.SUCCESS) {
					groupShareFooterView.onLoadComplete(shareResult.getCount(), shareResult.getSumCount());
					if (startLimit == 0) {
						mGroupShareAdapter.setResGroupshares(shareResult.getResGroupShare());
					} else
						mGroupShareAdapter.addResGroupshares(shareResult.getResGroupShare());
				} else if (shareResult != null && shareResult.getErrorCode() == BaseResult.FAILED) {
					showToast(shareResult.getErrorMessage());
				} else {
					groupShareFooterView.onLoadCompleteErr();
					showToast(R.string.to_server_fail);
				}
				resetStartLimit();
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("groupId", groupId));
		paras.add(getSearchLimit());
		if (isFrist) {
			pullToRefreshListView.setRefreshing(true);
		}
		serverMana.supportRequest(Configuration.getQueryResGroupShare(), paras);
	}

	private void lookChatRoomInfo(UserGroup userGroup) {
		Intent intent = new Intent();
		List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
		GroupLbsInfo lbsInfo = new GroupLbsInfo();
		lbsInfo.setgName(userGroup.getGroupName());
		lbsInfo.setSid(Long.parseLong(userGroup.getGroupId()));
		lbsInfo.setUid(Long.parseLong(userGroup.getUid()));
		lbsInfo.setShareUid(userGroup.getUid());
		groupList.add(lbsInfo);
		intent.putExtra("GroupInfo", (Serializable) groupList);
		intent.putExtra("group_index", 0);
		intent.putExtra("group_type", 1);
		intent.setClass(this, ChatRoomInfoActivity.class);
		this.startActivity(intent);
	}

	private void TextGroupShare() {
		if (mBinder != null && mBinder.getCurrenGroup() != null) {
			Intent intent = new Intent(this, TextGroupShareActivity.class);
			intent.putExtra(UserGroup.GROUPID_KEY, mBinder.getCurrenGroup().getGroupId());
			intent.putExtra(UserGroup.GROUP_NAME_KEY, mBinder.getCurrenGroup().getGroupName());
			startActivity(intent);
		}
	}

	private void showConfrimGroupMap() {
		if (application.getCurrunGroup() == null) {
			showToast("请先进入群组!");
			return;
		}
		if (application.getCurrunGroup().isInMapMember(Long.parseLong(mMyUserID))) {
			toGroupMap();
			return;
		}
		if (mConfrimDialog == null) {
			mConfrimDialog = new ConfrimDialog(this);
		}
		mConfrimDialog.showConfrimDialog("温馨提示", "切换到地图模式之后，群内其他用户将会看到您的位置，是否确定？",
				new ConfrimDialog.OnDialogClickListener() {
					@Override
					public void onClick(Dialog dialog, boolean positive) {
						// TODO Auto-generated method stub
						if (positive) {
							// 先进行定位，然后
							toGroupMap();
						}
					}
				});
	}

	private void toGroupMap() {
		// TODO Auto-generated method stub
		// 先进行定位，然后
		Intent mapIntent = new Intent();
		mapIntent.setClass(ChatRoomMainActivity.this, GroupMapActivity.class);
		startActivity(mapIntent);
	}

	private void groupShareRes() {

		if (mBinder != null && mBinder.getCurrenGroup() != null) {
			Intent intent = new Intent(ChatRoomMainActivity.this, ResUserStoreActivity.class);
			intent.putExtra(UserGroup.GROUPID_KEY, mBinder.getCurrenGroup().getGroupId());
			intent.putExtra(UserGroup.GROUP_NAME_KEY, mBinder.getCurrenGroup().getGroupName());
			intent.putExtra(ResUserStoreActivity.START_TYPE, 1);
			UserItem userItem = new UserItem();
			userItem.setUserId(application.getMyUserID());
			userItem.setUserName(application.getMyUserName());
			intent.putExtra(PangkerConstant.INTENT_UESRITEM, userItem);
			startActivity(intent);
		}
	}

	private PopMenuDialog popMenuDialog;

	ActionItem[] shareResMenu = new ActionItem[] { new ActionItem(1, "拍照上传"), new ActionItem(2, "分享文字"),
			new ActionItem(3, "从网盘资源获取") };

	private void shareRes2Group() {

		// TODO Auto-generated method stub
		if (popMenuDialog == null) {
			popMenuDialog = new PopMenuDialog(this);
			popMenuDialog.setListener(shareRes2GroupListener);
		}
		popMenuDialog.setMenus(shareResMenu);
		popMenuDialog.setTitle("资源上传");
		popMenuDialog.show();

	}

	private UITableView.ClickListener shareRes2GroupListener = new UITableView.ClickListener() {

		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			switch (actionid) {
			case 1:
				sharePhoto2Group();
				break;
			case 2:
				TextGroupShare();
				break;
			case 3:
				groupShareRes();
				break;

			}
			popMenuDialog.dismiss();
		}
	};

	// >>>>加载更多区间....
	private int startLimit = 0;
	private int endLimit = 0;
	private static final int incremental = 10;

	// >>>>>> getSearchLimit每次加载incremental个
	private Parameter getSearchLimit() {
		endLimit = startLimit + incremental;
		return new Parameter("limit", startLimit + "," + endLimit);
	}

	// >>>>>>>>> setStartLimit
	private void resetStartLimit() {
		startLimit = endLimit;
	}

	// 刷新，重新查询
	private void reset() {
		startLimit = 0;
	}

	@Override
	public void doImageBitmap(String imageLocalPath) {
		// TODO Auto-generated method stub

		if (Util.isExitFileSD(imageLocalPath)) {
			Intent intent = new Intent(this, PicturePreviewActivity.class);
			intent.putExtra("photo_path", imageLocalPath);
			startActivityForResult(intent, PangkerConstant.RES_PICTURE);
		}
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		if (handler != null) {
			handler.sendMessage(msg);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if (application.getCurrunGroup().isJoin()) {
			outState.putBoolean("isJoin", true);
		}
	}
}