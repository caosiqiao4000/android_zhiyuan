package com.wachoo.pangker.activity.group;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.camera.CameraCompActivity;
import com.wachoo.pangker.db.IUserGroupDao;
import com.wachoo.pangker.db.impl.UserGroupDaoIpml;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.image.PKIconLoader;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

public class ChatRoomCoverActivity extends CameraCompActivity implements IUICallBackInterface {

	private String TAG = "ChatRoomCoverActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	private PangkerApplication application;// application 旁客应用
	private static final int UPLOADICON_CODE = 0x10;
	UserGroup group;
	IUserGroupDao dao;
	private ImageView imageview;
	String uid;
	String gid;
	// Bitmap bitmap;

	Button submit;
	// >>>>>>获取图片
	private ImageResizer mImageWorker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.chatroomcover);
		application = (PangkerApplication) getApplication();
		mImageWorker = PangkerManager.getGroupIconResizer(getApplicationContext());
		dao = new UserGroupDaoIpml(this);
		initView();
		initData();
		initListener();

		final String groupUrl = Configuration.getGroupCover() + gid;
		mImageWorker.loadImage(groupUrl, imageview, gid);
	}

	/**
	 * 初始化UI
	 */
	private void initView() {
		imageview = (ImageView) findViewById(R.id.iv_cover);
		submit = (Button) findViewById(R.id.submit);
		TextView tv_title = (TextView) findViewById(R.id.mmtitle);
		tv_title.setText(R.string.room_setlogo);
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ChatRoomCoverActivity.this.finish();
			}
		});
		Button iv_more = (Button) findViewById(R.id.iv_more);
		iv_more.setVisibility(View.GONE);
	}

	/**
	 * 初始化数据
	 * 
	 * @author wubo
	 * @createtime 2012-2-1 下午04:24:48
	 */
	public void initData() {
		application = (PangkerApplication) getApplication();
		uid = application.getMyUserID();
		group = (UserGroup) getIntent().getSerializableExtra("userGroup");
		gid = group.getGroupId();

	}

	public void initListener() {
		submit.setOnClickListener(clickListener);
		imageview.setOnClickListener(clickListener);
	}

	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == submit) {
				if (!Util.isEmpty(imageLocalPath))
					UploadGroupCover(imageLocalPath);
				else
					showToast("图标获取失败!");
			}
			if (v == imageview) {
				showImageDialog("图标上传", actionItems1, true);
			}
		}
	};

	private String imageLocalPath;

	@Override
	public void doImageBitmap(String imageLocalPath) {
		this.imageLocalPath = imageLocalPath;
		Bitmap bitmap = ImageUtil.decodeFile(this.imageLocalPath, PKIconLoader.iconSideLength,
				PKIconLoader.iconSideLength * PKIconLoader.iconSideLength);
		if (bitmap != null) {
			imageview.setImageBitmap(bitmap);
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		if (!HttpResponseStatus(response)) {
			showToast(R.string.to_server_fail);
			return;
		}
		BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
		if (result == null) {
			showToast(R.string.to_server_fail);
			return;
		}
		if (result.getErrorCode() != 999) {
			showToast(result.getErrorMessage());
			if (result.getErrorCode() == 1) {
				setResult(RESULT_OK);
				finish();
			}
		} else {
			showToast(R.string.return_value_999);
		}
	}

	/**
	 * 
	 * @author wubo
	 * @createtime Jun 7, 2012
	 * @throws FileNotFoundException
	 */
	private void UploadGroupCover(String imageLocalPath) {
		try {
			File file = new File(imageLocalPath);

			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", uid));
			paras.add(new Parameter("gid", gid));
			MyFilePart uploader = new MyFilePart(file.getName(), file, null);
			String mess = getResources().getString(R.string.uploading);
			ServerSupportManager serverMana = new ServerSupportManager(this, this);
			serverMana.supportRequest(Configuration.getUploadGroupCover(), paras, uploader, imageLocalPath, true, mess,
					UPLOADICON_CODE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
			showToast("获取图片失败!");
			e.printStackTrace();
		}
	}

}
