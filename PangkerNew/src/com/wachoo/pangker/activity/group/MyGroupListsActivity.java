package com.wachoo.pangker.activity.group;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.pangker.RelativesManagerActivity;
import com.wachoo.pangker.adapter.MeetGroupAdapter;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupsNotifyResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;

public class MyGroupListsActivity extends CommonPopActivity implements IUICallBackInterface {

	private PangkerApplication pangkerApplication;
	private String myUserId;
	private PullToRefreshListView pullToRefreshListView;
	private ListView groupListView;// 群组
	private MeetGroupAdapter groupAdaper;
	ServerSupportManager serverMana = new ServerSupportManager(this, this);
	private List<UserGroup> myselfGroups = new ArrayList<UserGroup>();// 我创建的群组
	private String fromType;

	public static String FROM_TYPE = "fromType";
	public static String INVITE_2_TRACEROME = "inviteToTraceRoom";
	public static String INVITE_2_UTITSROME = "inviteUtitsRoom";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_room_home);
		pangkerApplication = (PangkerApplication) getApplication();
		myUserId = pangkerApplication.getMyUserID();

		fromType = this.getIntent().getStringExtra(FROM_TYPE);
		initView();
		reFreshGroup(true);
	}

	private void initView() {
		// TODO Auto-generated method stub
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("选择一个群组");
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		findViewById(R.id.rg_group).setVisibility(View.GONE);

		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		groupListView = pullToRefreshListView.getRefreshableView();
		groupListView.setOnItemClickListener(onItemClickListener);
		groupAdaper = new MeetGroupAdapter(new ArrayList<UserGroup>(), this);
		groupListView.setAdapter(groupAdaper);
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			reFreshGroup(false);
		}
	};

	private OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
			// groupInfo = userGroups.get(position);
			UserGroup groupInfo = (UserGroup) parent.getItemAtPosition(position);
			// >>>>>>>邀请亲友
			if (fromType != null && fromType.equals(INVITE_2_TRACEROME)) {
				invite2groupchat(groupInfo);
			}
			// >>>>>>>>>>邀请单位通讯录用户
			else if (fromType != null && fromType.equals(INVITE_2_UTITSROME)) {
				Intent intent = new Intent();
				intent.putExtra("UserGroup", groupInfo);
				setResult(RESULT_OK, intent);
				finish();
			}
			// >>>>>>>>邀请好友
			else {
				Intent mInviteIntent = new Intent();
				mInviteIntent.putExtra("groupInfo", groupInfo);
				mInviteIntent.putExtra(ContactsSelectActivity.INTENT_SELECT, ContactsSelectActivity.TYPE_GROUP_INVITE_ENTER);
				mInviteIntent.setClass(MyGroupListsActivity.this, ContactsSelectActivity.class);
				startActivity(mInviteIntent);
				finish();
			}

		}
	};

	/**
	 * 邀请亲友到群聊 void TODO
	 * 
	 * @param usergroup
	 */
	private void invite2groupchat(UserGroup usergroup) {
		Intent mIntent = new Intent(this, RelativesManagerActivity.class);
		mIntent.putExtra("UserGroup", usergroup);
		mIntent.putExtra("optionType", "inviteToTraceRoom");
		startActivity(mIntent);
		this.finish();
	}

	private void reFreshGroup(boolean isShowDialog) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", myUserId));
		paras.add(new Parameter("uid", myUserId));
		paras.add(new Parameter("userId", myUserId));
		serverMana.supportRequest(Configuration.getGroupInfoQuery(), paras, 0);
		if (isShowDialog) {
			pullToRefreshListView.setRefreshing(true);
		}
	}

	private void showGroups(List<UserGroup> uGroups) {
		myselfGroups.clear();
		if (uGroups.size() > 0) {
			for (UserGroup uGroup : uGroups) {
				if (uGroup.getGroupFlag() == 0) {
					myselfGroups.add(uGroup);
				}
			}
		}
		groupAdaper.setUserGroups(myselfGroups);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse)) {
			if (caseKey == 0) {
				pullToRefreshListView.onRefreshComplete();
			}
			return;
		}
		if (caseKey == 0) {
			pullToRefreshListView.onRefreshComplete();
			GroupsNotifyResult groupResult = JSONUtil.fromJson(supportResponse.toString(),
					GroupsNotifyResult.class);
			if (groupResult == null) {
				showToast(R.string.to_server_fail);
				return;
			}
			if (groupResult.getErrorCode() == BaseResult.SUCCESS) {
				List<UserGroup> usergroups = groupResult.getGroups();
				if (usergroups != null) {
					showGroups(usergroups);
				}
			} else if (groupResult.getErrorCode() == BaseResult.FAILED) {
				showToast(groupResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);

		}
	}
}
