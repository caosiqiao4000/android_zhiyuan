package com.wachoo.pangker.activity.group;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.pangker.RelativesManagerActivity;
import com.wachoo.pangker.adapter.MeetGroupAdapter;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupEnterCheckResult;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.GroupsNotifyResult;
import com.wachoo.pangker.server.response.PangkerGroupManagerResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.PasswordDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

public class ChatRoomListActivity extends CommonPopActivity implements IUICallBackInterface, ITabSendMsgListener{

	private PullToRefreshListView pullToRefreshListView;
	private ListView groupListView;// 群组
	private EmptyView mEmptyView;
	private EditText etSearch;
	private Button btnAddGroup;
	private QuickAction quickAction;
	private Button btnEdit;
	private MeetGroupAdapter groupAdaper;
	private List<UserGroup> userGroups;// 与用户有关的所有群组
	private List<UserGroup> myselfGroups;// 我创建的群组
	private List<UserGroup> favoriteGroups;// 收藏的群组
	private List<UserGroup> braodcastGroups;// 转播的群组
	private List<UserGroup> historyGroups;// 最近访问的群组
	private RadioGroup mRadioGroup;

	private int selectedPosition = 0;
	private int status_flag = 0; // 显示界面:0:0我的群组,1:收藏过的群组,2:参与过的群组，3转播的群组
	private UserGroup groupInfo;
	private String myUserId;

	ServerSupportManager serverMana = new ServerSupportManager(this, this);
	private int DELETE_FLAG = 12;
	private int FAVORITE_FLAG = 13;
	public static int GROUP_ENTER = 14;
	private int REFRESH = 15;
	private int BROADCAST_FLAG = 16;
	private int SHARE_FLAG = 17;
	private int UNSHARE_FLAG = 18;

	EditText et_dialog;
	PangkerApplication pangkerApplication;
	Builder builder;
	boolean ifShow = false;
	private PopMenuDialog menuDialog;
	private String fromType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_room_home);
		fromType = this.getIntent().getStringExtra("fromType");
		PangkerManager.getTabBroadcastManager().addMsgListener(this);
		initView();
		initData();
		initListen();
		reFreshGroup(true);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// 首次登录旁客设置向导
		// addGuideImage(ChatRoomListActivity.class.getName(),R.drawable.talk_02);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		PangkerManager.getTabBroadcastManager().removeUserListenter(this);
		super.onDestroy();
	}

	private void initView() {
		boolean ifShow = getIntent().getBooleanExtra("ifShow", false);
		if (!ifShow) {
			findViewById(R.id.title_layout).setVisibility(View.GONE);
		} else {
			findViewById(R.id.title_layout).setVisibility(View.VISIBLE);
		}

		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("我的群组会议室");
		btnEdit = (Button) findViewById(R.id.iv_more);
		btnEdit.setBackgroundResource(R.drawable.btn_menu_bg);

		// 如果是邀请群聊，则不显示更多菜单按钮
		if (fromType != null && fromType.equals("inviteToTraceRoom")) {
			btnEdit.setVisibility(View.GONE);
		} else
			btnEdit.setOnClickListener(onClickListener);
		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.addActionItem(new ActionItem(1, "添加群组"), true);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);

		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		groupListView = pullToRefreshListView.getRefreshableView();
		if (!ifShow) {
			View view1 = LayoutInflater.from(this).inflate(
					R.layout.contact_search, null);
			groupListView.addHeaderView(view1);
			etSearch = (EditText) view1.findViewById(R.id.et_search);
			etSearch.addTextChangedListener(editTextChangeListener);
			btnAddGroup = (Button) view1.findViewById(R.id.btn_left);
			btnAddGroup.setVisibility(View.VISIBLE);
			btnAddGroup
					.setBackgroundResource(R.drawable.btn_common_add_selector);
			ImageView btnClear = (ImageView) view1.findViewById(R.id.btn_clear);
			btnClear.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					etSearch.setText("");
				}
			});
		}
		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(groupListView);
		mRadioGroup = (RadioGroup) findViewById(R.id.rg_group);

		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
				case R.id.rb_group_my:
					groupAdaper.setUserGroups(userGroups);
					status_flag = 0;
					break;
				case R.id.rb_group_history:
					groupAdaper.setUserGroups(historyGroups);
					status_flag = 1;
					break;
				}
			}
		});

	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			quickAction.show(btnEdit);
		}
	};

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			reFreshGroup(false);
		}
	};

	private void reFreshGroup(boolean isShowDialog) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", myUserId));
		paras.add(new Parameter("uid", myUserId));
		paras.add(new Parameter("userId", myUserId));
		serverMana.supportRequest(Configuration.getGroupInfoQuery(), paras, REFRESH);
		if (isShowDialog) {
			pullToRefreshListView.setRefreshing(true);
		}
	}

	private void initData() {
		pangkerApplication = (PangkerApplication) getApplication();
		// userGroupDao = new UserGroupDaoIpml(this);
		pangkerApplication.setChatRoomListActivity(this);
		// userGroups = userGroupDao.getUserGroups();
		if (userGroups == null) {
			userGroups = new ArrayList<UserGroup>();
		}
		myselfGroups = new ArrayList<UserGroup>();
		favoriteGroups = new ArrayList<UserGroup>();
		historyGroups = new ArrayList<UserGroup>();
		braodcastGroups = new ArrayList<UserGroup>();

		groupAdaper = new MeetGroupAdapter(new ArrayList<UserGroup>(), this);
		groupListView.setAdapter(groupAdaper);
		myUserId = ((PangkerApplication) this.getApplication()).getMyUserID();
	}

	private void initListen() {
		groupListView.setOnItemClickListener(onItemClickListener);
		if (btnAddGroup != null) {
			btnAddGroup.setOnClickListener(listener);
		}
		if (fromType == null || !fromType.equals("inviteToTraceRoom")) {
			groupListView.setOnItemLongClickListener(onItemLongClickListener);
		}
	}

	OnItemLongClickListener onItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			groupInfo = (UserGroup) parent.getItemAtPosition(position);
			selectedPosition = position - 1;
			showPopMenus();
			return true;
		}
	};

	/**
	 * 删除群组
	 * 
	 * @author wubo
	 * @createtime 2012-5-18
	 * @param groupId
	 */
	private void deleteGroup(String groupId, String groupType, String loType) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", myUserId));
		paras.add(new Parameter("grouptype", groupType));
		paras.add(new Parameter("optype", "2"));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("lotype", loType));
		serverMana.supportRequest(Configuration.getPangkerGroupManager(),
				paras, DELETE_FLAG);
	}

	/**
	 * 取消群组收藏
	 * 
	 * @author wubo
	 * @createtime 2012-5-18
	 */
	public void cancelFav(String groupId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", myUserId));
		paras.add(new Parameter("resid", groupId));
		paras.add(new Parameter("optype", "1"));// 0收藏 1取消收藏
		paras.add(new Parameter("type", "2"));// 0:问答,1:资源,2:群组
		serverMana.supportRequest(Configuration.getCollectResManager(), paras,
				FAVORITE_FLAG);
	}

	/**
	 * 进行广播的接口
	 */
	private void braodcastGroup(String groupId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", myUserId));
		paras.add(new Parameter("resid", groupId));
		paras.add(new Parameter("optype", "2"));// 0:问答,1:资源,2:群组
		paras.add(new Parameter("type", "1"));// 0:问答,1:资源,2:群组
		serverMana.supportRequest(Configuration.getResShare(), paras, true,
				getResourcesMessage(R.string.please_wait), SHARE_FLAG);
	}

	/**
	 * 取消转播，广播的接口
	 * 
	 * @param groupId
	 */
	private void cancelBraodcast(String groupId, String shareId) {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", myUserId));
		int flag = UNSHARE_FLAG;
		if (!Util.isEmpty(shareId)) {
			flag = BROADCAST_FLAG;
			paras.add(new Parameter("shareid", shareId));
		} else {
			paras.add(new Parameter("resid", groupId));
		}
		paras.add(new Parameter("optype", "2"));// 0:问答,1:资源,2:群组
		serverMana.supportRequest(Configuration.getCancelShare(), paras, true,
				getResourcesMessage(R.string.please_wait), flag);
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			if (actionId == 1) {
				addGroup();
			} else if (actionId == 2) {

			} else if (actionId == 3) {

			}
		}
	};

	private void addGroup() {
		Intent intent = new Intent();
		intent.setClass(ChatRoomListActivity.this, ChatRoomCreateActivity.class);
		intent.putExtra("flag", "0");
		intent.putExtra("groupId", "nogroupid");
		startActivityForResult(intent, 1);
	}

	/**
	 * 设置成功之后要对现在的UserGroup的时间以及单双通道进行修改.
	 */
	private void updateGroup() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra("flag", "1");
		intent.putExtra("groupId", groupInfo.getGroupId());
		intent.setClass(ChatRoomListActivity.this, ChatRoomCreateActivity.class);
		startActivityForResult(intent, 10);
	}

	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnAddGroup) {
				addGroup();
			}
		}
	};

	OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
			// groupInfo = userGroups.get(position);
			groupInfo = (UserGroup) parent.getItemAtPosition(position);
			if (fromType != null && fromType.equals("inviteToTraceRoom")) {
				invite2groupchat(groupInfo);
			} else {
				if (pangkerApplication.getCurrunGroup() != null
						&& pangkerApplication.getCurrunGroup().getUid() != null
						&& !pangkerApplication.getCurrunGroup().getGroupId()
								.equals(groupInfo.getGroupId())) {

					MessageTipDialog tipDialog = new MessageTipDialog(
							new DialogMsgCallback() {
								@Override
								public void buttonResult(boolean isSubmit) {
									// TODO Auto-generated method stub
									if (isSubmit) {
										PangkerManager.getActivityStackManager().popOneActivity(ChatRoomMainActivity.class);
										checkGroup(null);
									}
								}
							}, ChatRoomListActivity.this);
					tipDialog.showDialog("", "您已经在一个群组里面,进入此群组会退出当前所在的群组,是否确认进入?", false);
				} else {
					checkGroup(null);
				}
			}
		}
	};
	
	private void showPassword() {
		// TODO Auto-generated method stub
		final PasswordDialog passwordDialog = new PasswordDialog(this);
        passwordDialog.setTitle("群组会议室");
        passwordDialog.setResultCallback(new DialogMsgCallback() {
			@Override
			public void buttonResult(boolean isSubmit) {
				// TODO Auto-generated method stub
				if(isSubmit && passwordDialog.checkPass()){
					checkGroup(passwordDialog.getPassword());
				}
			}
		});
        passwordDialog.showDialog();
	}

	private void lookChatRoomInfo(int position) {
		Intent intent = new Intent();
		List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
		List<UserGroup> groups = null;
		if(status_flag == 0){
			groups = userGroups;
		} else {
			groups = historyGroups;
		}
		if(groups == null){
			return;
		}
		for (UserGroup u : groups) {
			GroupLbsInfo lbsInfo = new GroupLbsInfo();
			lbsInfo.setgName(u.getGroupName());
			lbsInfo.setSid(Long.parseLong(u.getGroupId()));
			lbsInfo.setUid(Long.parseLong(u.getUid()));
			lbsInfo.setShareUid(pangkerApplication.getMyUserID());
			groupList.add(lbsInfo);
		}
		intent.putExtra("GroupInfo", (Serializable) groupList);
		intent.putExtra("group_index", position);
		Log.d(TAG, "==>" + position + "," + groupList.size());
		intent.setClass(this, ChatRoomInfoActivity.class);
		startActivity(intent);
	}

	/**
	 * 搜索框字符串改变监听器
	 */
	private TextWatcher editTextChangeListener = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			etSearch.setHint(R.string.hint_search);
			searchUserList(s.toString().trim());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}
	};

	/**
	 * @param s
	 *            void
	 */
	protected void searchUserList(CharSequence s) {
		List<UserGroup> searchList = new ArrayList<UserGroup>();
		if (s == null || s.length() < 1) {
			searchList = userGroups;
		}
		if (s != null && s.length() > 0) {
			for (UserGroup contactUser : userGroups) {
				if (contactUser.getGroupName().contains(s)
						|| (contactUser.getGroupLabel() != null && contactUser
								.getGroupLabel().contains(s))) {
					searchList.add(contactUser);
				}
			}
		}
		refreshGroup(searchList);
	}

	private void checkGroup(String password) {
		groupInfo.setLastVisitTime(Util.getNowTime());
		groupInfo.setGroupFlag(2);
		if (groupInfo != null && groupInfo.getGroupId() != null) {
			groupEnterCheck(groupInfo.getGroupId(), password);
		}
	}

	// 进入群组
	private void intoGroup() {
		// TODO Auto-generated method stub
		groupInfo.setLastVisitTime(Util.date2Str(new Date()));
		Intent intent = new Intent();
		// >>>>>wangxin add 进入群组
		intent.putExtra(ChatRoomMainActivity.JION_TO_GROUP, groupInfo);
		intent.setClass(this, ChatRoomMainActivity.class);
		startActivity(intent);
		// 将进入群组的消息纪律到MsgInfo中
		pangkerApplication.getMsgInfoBroadcast().sendMeetroom(groupInfo);
	}

	private List<ActionItem> getActionItems() {
		// TODO Auto-generated method stub
		List<ActionItem> actionItems = new ArrayList<ActionItem>();
		if(groupInfo.getGroupFlag() == 0){
			actionItems.add(new ActionItem(30, getString(R.string.chatroom_seach)));
			// 判断该群组是不是转播的，如果是转播的便取消转播
			if (!groupInfo.getGroupType().equals(UserGroup.GroupType_Trace)) {
				actionItems.add(new ActionItem(35, getString(R.string.chatroom_delete)));
			}
			// 0不分享 1：分享
			if (groupInfo.getIfShare() != null && groupInfo.getIfShare() == 1) {
				actionItems.add(new ActionItem(32, getString(R.string.chatroom_unshare)));
			} else {
				actionItems.add(new ActionItem(32, getString(R.string.chatroom_share)));
			}
			actionItems.add(new ActionItem(34, getString(R.string.chatroom_update)));
		} else if(groupInfo.getGroupFlag() == 1){
			actionItems.add(new ActionItem(30, getString(R.string.chatroom_seach)));
			actionItems.add(new ActionItem(33, getString(R.string.room_cancelfav)));
		} else if(groupInfo.getGroupFlag() == 3){
			actionItems.add(new ActionItem(30, getString(R.string.chatroom_seach)));
			actionItems.add(new ActionItem(31, getString(R.string.chatroom_cancel_braodcast)));
		} else {
			actionItems.add(new ActionItem(30, getString(R.string.chatroom_seach)));
		}
		return actionItems;
	}

	private void showPopMenus() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
		}
		menuDialog.setTitle("群组信息");
		menuDialog.setMenus(getActionItems());
		menuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			case 30:// 查看群组
				lookChatRoomInfo(selectedPosition);
				break;
			case 31:// 群星群组的转播
				MessageTipDialog tipDialog = new MessageTipDialog(
						new DialogMsgCallback() {
							@Override
							public void buttonResult(boolean isSubmit) {
								// TODO Auto-generated method stub
								if (isSubmit) {
									cancelBraodcast(groupInfo.getGroupId(), groupInfo.getShareId());
								}
							}
						}, ChatRoomListActivity.this);
				tipDialog.showDialog(groupInfo.getGroupName(), "确认取消转播该群组？",
						false);
				break;
			case 32:// 群组广播的操作0不分享 1：分享
				if (groupInfo.getIfShare() != null && groupInfo.getIfShare() == 1) {
					cancelBraodcast(groupInfo.getGroupId(), null);
				} else {
					braodcastGroup(groupInfo.getGroupId());
				}
				break;
			case 35:// 群组删除
				if (pangkerApplication.getCurrunGroup() != null
						&& pangkerApplication.getCurrunGroup().isJoin()
						&& pangkerApplication.getCurrunGroup().getGroupId().equals(groupInfo.getGroupId())) {
					showToast("您正在当前的群组中,请先退出该群组!");
				} else {
					MessageTipDialog tipDialog2 = new MessageTipDialog(
							new DialogMsgCallback() {
								@Override
								public void buttonResult(boolean isSubmit) {
									// TODO Auto-generated method stub
									if (isSubmit) {
										deleteGroup(groupInfo.getGroupId(),
												groupInfo.getGroupType(),
												groupInfo.getLotype());
									}
								}
							}, ChatRoomListActivity.this);
					tipDialog2.showDialog(groupInfo.getGroupName(), "确认删除该群组？",
							false);
				}
				break;
			case 33: // 取消收藏
				MessageTipDialog tipDialog3 = new MessageTipDialog(
						new DialogMsgCallback() {
							@Override
							public void buttonResult(boolean isSubmit) {
								// TODO Auto-generated method stub
								if (isSubmit) {
									cancelFav(groupInfo.getGroupId());
								}
							}
						}, ChatRoomListActivity.this);
				tipDialog3.showDialog(groupInfo.getGroupName(), "确认取消收藏该群组？",
						false);
				break;
			case 34: // 修改群组
				updateGroup();
				break;
			}
			menuDialog.dismiss();
		}
	};

	@Override
	public void onContextMenuClosed(Menu menu) {
		// TODO Auto-generated method stub
		super.onContextMenuClosed(menu);
	}

	/**
	 * 进群记录
	 */
	public void groupEnterCheck(String groupId, String password) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", myUserId));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("optype", "0"));//
		// 群组权限0：需要权限验证进入 不等于0：无需权限
		if(password != null){
			paras.add(new Parameter("password", password));// 群组密码，需要密码校验时使用
		}
		serverMana.supportRequest(Configuration.getGroupEnterCheck(), paras, true, getResourcesMessage(R.string.jion_group_waiting), GROUP_ENTER);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response, caseKey != REFRESH)) {
			if (caseKey == REFRESH) {
				pullToRefreshListView.onRefreshComplete();
				mEmptyView.showView(R.drawable.emptylist_icon, "没有数据");
			}
			return;
		}
		if (caseKey == DELETE_FLAG) {
			PangkerGroupManagerResult result = JSONUtil.fromJson(response.toString(), PangkerGroupManagerResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				if (result.getErrorCode() == 1) {
					showToast(result.getErrorMessage());
					// userGroupDao.delGroup(groupInfo);
					userGroups.remove(groupInfo);
					refreshGroup(userGroups);
				} else if (result != null && result.getErrorCode() != BaseResult.SUCCESS) {
					showToast(result.getErrorMessage());
				}
			} else {
				showToast(R.string.return_value_999);
			}
		} else if (caseKey == GROUP_ENTER) {
			GroupEnterCheckResult result = JSONUtil.fromJson(response.toString(), GroupEnterCheckResult.class);
			if(result == null){
				showToast(R.string.return_value_999);
				return;
			}
			if (result.getErrorCode() == BaseResult.SUCCESS) {
				groupInfo.setVoiceChannelType(result.getUserGroup().getVoiceChannelType());
				groupInfo.setMaxSpeechTime(result.getUserGroup().getMaxSpeechTime());
				intoGroup();
			} else if (result.getErrorCode() == BaseResult.OTHER) {
				showPassword();
			} else {
				showToast(result.getErrorMessage());
			}
		}
		if (caseKey == FAVORITE_FLAG) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				if (result.getErrorCode() == 1) {
					showToast(result.getErrorMessage());
					// userGroupDao.delGroup(groupInfo);
					userGroups.remove(groupInfo);
					refreshGroup(userGroups);
				} else if (result != null && result.getErrorCode() != BaseResult.SUCCESS) {
					showToast(result.getErrorMessage());
				}
			} else {
				showToast(R.string.return_value_999);
			}
		}
		if (caseKey == REFRESH) {
			pullToRefreshListView.onRefreshComplete();
			GroupsNotifyResult groupResult = JSONUtil.fromJson(response.toString(), GroupsNotifyResult.class);
			if (groupResult == null) {
				showToast(R.string.to_server_fail);
				return;
			}
			if (groupResult.getErrorCode() == BaseResult.SUCCESS) {
				List<UserGroup> usergroups = groupResult.getGroups();
				if (usergroups == null) {
					groupAdaper.setUserGroups(new ArrayList<UserGroup>());
					return;
				}
				userGroups.clear();
				refreshGroup(usergroups);
			} else if (groupResult.getErrorCode() == BaseResult.FAILED) {
				showToast(groupResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
			mEmptyView.showView(R.drawable.emptylist_icon, "没有数据");
		}
		// 取消转播
		if (caseKey == BROADCAST_FLAG) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				userGroups.remove(groupInfo);
				refreshGroup(userGroups);
			} else if (result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}
		if (caseKey == SHARE_FLAG) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				setShareGroup(1);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}
		if (caseKey == UNSHARE_FLAG) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				setShareGroup(0);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				}
			}
		}

	}

	/**
	 * 邀请亲友到群聊 void TODO
	 * 
	 * @param usergroup
	 */
	private void invite2groupchat(UserGroup usergroup) {
		Intent mIntent = new Intent(this, RelativesManagerActivity.class);
		mIntent.putExtra("UserGroup", usergroup);
		mIntent.putExtra("optionType", "inviteToTraceRoom");
		startActivity(mIntent);
		this.finish();
	}

	private void setShareGroup(int ifShare) {
		// TODO Auto-generated method stub
		groupInfo.setIfShare(ifShare);
		groupAdaper.notifyDataSetChanged();
	}

	private void refreshGroup(List<UserGroup> uGroups) {
		myselfGroups.clear();
		favoriteGroups.clear();
		historyGroups.clear();
		braodcastGroups.clear();
		for (UserGroup uGroup : uGroups) {
			if (uGroup.getGroupFlag() == 0) {
				myselfGroups.add(uGroup);
			} else if (uGroup.getGroupFlag() == 1) {
				if(favoriteGroups.size() == 0){
					uGroup.setLabel("收藏的群组");
				}
				favoriteGroups.add(uGroup);
			} else if (uGroup.getGroupFlag() == 2) {
				historyGroups.add(uGroup);
			} else if (uGroup.getGroupFlag() == 3) {
				if(braodcastGroups.size() == 0){
					uGroup.setLabel("转播的群组");
				}
				braodcastGroups.add(uGroup);
			}
		}
		
		userGroups.clear();
		userGroups.addAll(myselfGroups);
		userGroups.addAll(favoriteGroups);
		userGroups.addAll(braodcastGroups);
		if (status_flag == 0) {
			Log.d("userGroups", "==>" + userGroups.size());
			groupAdaper.setUserGroups(userGroups);
		} else if (status_flag == 1) {
			groupAdaper.setUserGroups(historyGroups);
		}  
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 10 && resultCode == RESULT_OK) {
			if(data != null){
				int voiceChannelType = data.getIntExtra("voiceChannelType", 0);
				if (voiceChannelType > 0) {
					groupInfo.setVoiceChannelType(voiceChannelType);
				}
				int maxSpeechTime = data.getIntExtra("maxSpeechTime", 0);
				if (maxSpeechTime > 0) {
					groupInfo.setMaxSpeechTime(maxSpeechTime);
				}
				reFreshGroup(true);
			}
		}
		if (requestCode == 1 && resultCode == RESULT_OK) {
			reFreshGroup(true);
		}
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		if(groupAdaper != null){
			groupAdaper.notifyDataSetChanged();
		}
	}

}
