package com.wachoo.pangker.activity.group;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.image.ImageResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.util.ImageUtil;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @TODO 分享资源到群组
 * 
 *       2013-01-14
 * 
 */
public class ResGroupShareActivity extends CommonPopActivity implements IUICallBackInterface {

	private String TAG = "ChatRoomInfoActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	private final int GROUP_SHARE_KEY = 0x10;
	PangkerApplication application;
	private EditText etDetail;
	private String mUserId;
	private ImageView iconGroup;
	private ImageView iconRes;
	private TextView tvGroupName;
	private TextView tvresName;
	private CheckBox cbIfshareres;

	private String groupId;
	private String groupName;
	private ResShowEntity resShowEntity;

	private ImageResizer mImageWorker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.res_group_share);

		application = (PangkerApplication) getApplication();
		mUserId = application.getMyUserID();
		groupId = this.getIntent().getStringExtra(UserGroup.GROUPID_KEY);
		groupName = this.getIntent().getStringExtra(UserGroup.GROUP_NAME_KEY);

		resShowEntity = (ResShowEntity) getIntent().getSerializableExtra(ResShowEntity.RES_ENTITY);

		initView();

		mImageWorker = PangkerManager.getGroupIconResizer(getApplicationContext());

		tvGroupName.setText(groupName);
		tvresName.setText(resShowEntity.getResName());
		if (resShowEntity.getResType() == PangkerConstant.RES_APPLICATION) {
			String iconUrl = Configuration.getCoverDownload() + resShowEntity.getResID();
			mImageWorker.loadImage(iconUrl, iconRes, String.valueOf(resShowEntity.getResID()));
		} else if (resShowEntity.getResType() == PangkerConstant.RES_PICTURE) {
			int sideLength = ImageUtil.getPicPreviewResolution(this);
			String IconUrl = ImageUtil.getWebImageURL(resShowEntity.getResID(), sideLength);
			mImageWorker.loadImage(IconUrl, iconRes, String.valueOf(resShowEntity.getResID()) + "_" + sideLength);
		} else if (resShowEntity.getResType() == PangkerConstant.RES_DOCUMENT) {
			iconRes.setImageResource(R.drawable.attachment_doc);
		} else if (resShowEntity.getResType() == PangkerConstant.RES_MUSIC) {
			iconRes.setImageResource(R.drawable.icon64_music);
		}

		final String groupUrl = Configuration.getGroupCover() + groupId;
		mImageWorker.loadImage(groupUrl, iconGroup, groupId);
	}

	private void initView() {
		Button btnRight = (Button) findViewById(R.id.iv_more);
		btnRight.setVisibility(View.GONE);

		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("分享资源到群组");

		iconGroup = (ImageView) findViewById(R.id.iv_groupicon);
		iconRes = (ImageView) findViewById(R.id.iv_resshare);
		tvGroupName = (TextView) findViewById(R.id.tv_groupname);
		tvresName = (TextView) findViewById(R.id.tv_res_name);

		etDetail = (EditText) findViewById(R.id.et_detail);
		cbIfshareres = (CheckBox) findViewById(R.id.cb_ifshareres);

		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ResGroupShareActivity.this.finish();
			}
		});
		Button btnSubmit = (Button) findViewById(R.id.btn_submit);
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 提交
				AddResGroupShare();
			}
		});
	}

	/**
	 * 14.4 添加群组资源分享接口
	 * 
	 * AddResGroupShare.json
	 */
	private void AddResGroupShare() {
		String reson = etDetail.getText().toString();
		if (cbIfshareres.isChecked() && Util.isEmpty(reson)) {
			showToast("只分享文字信息时需要填写分享理由！");
			return;
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("groupId", groupId));// 群组id
		// 是否只分享文字信息
		if (!cbIfshareres.isChecked()) {
			paras.add(new Parameter("resIds", String.valueOf(resShowEntity.getResID())));// 资源的Id列表
		}
		paras.add(new Parameter("shareUid", mUserId));// 分享者Uid
		paras.add(new Parameter("shareReason", reson));// 分享理由，70字内
		String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getAddResGroupShare(), paras, true, mess, GROUP_SHARE_KEY);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		if (caseKey == GROUP_SHARE_KEY) {
			BaseResult result = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == result.SUCCESS) {
				showToast("分享成功！");
				finish();
			} else if (result != null && result.getErrorCode() == result.FAILED) {
				showToast(result.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}

	}

}
