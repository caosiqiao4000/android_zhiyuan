package com.wachoo.pangker.activity.group;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.ChatRoomListAdapter;
import com.wachoo.pangker.db.IMeetRoomDao;
import com.wachoo.pangker.db.impl.MeetRoomDaoImpl;
import com.wachoo.pangker.entity.MeetRoom;
import com.wachoo.pangker.image.ImageCache;
import com.wachoo.pangker.image.ImageCache.ImageCacheParams;
import com.wachoo.pangker.image.ImageFetcher;
import com.wachoo.pangker.image.ImageLocalFetcher;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;

public class ChatRoomHistoryActivity extends CommonPopActivity {

	private ListView history;
	private Button last;
	private Button next;
	private TextView count;
	private List<MeetRoom> list;
	ChatRoomListAdapter mrAdapter;
	private List<MeetRoom> list_item;
	int start = 0;
	int length = 8;
	int total = 0;
	int totalPage = 0;
	int page = 1;
	private IMeetRoomDao mrdao;
	private String groupId;

	String uid;
	ImageLocalFetcher localFetcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.msghistory);
		groupId = this.getIntent().getStringExtra("groupId");

		// >>>>>>>>>>图片缓存区
		ImageCacheParams imageCacheParams = new ImageCacheParams(ImageFetcher.HTTP_CACHE_DIR);
		imageCacheParams.imageSideLength = PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE;
		imageCacheParams.diskCacheEnabled = false;
		mImageCache = new ImageCache(this, imageCacheParams);
		// >>>>>>>>>加载
		localFetcher = new ImageLocalFetcher(this, PangkerConstant.PANGKER_PHOTO_SIDELENGTH_MIDDLE);
		localFetcher.setmSaveDiskCache(false);
		localFetcher.setImageCache(mImageCache);
		localFetcher.setLoadingImage(R.drawable.listmod_bighead_photo_default);
		initView();
		initData();
		initListenner();
	}

	private void initView() {
		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText(R.string.history_show);
		Button btnRight = (Button) findViewById(R.id.iv_more);
		btnRight.setVisibility(View.GONE);
		history = (ListView) findViewById(R.id.history_list);
		last = (Button) findViewById(R.id.last);
		next = (Button) findViewById(R.id.next);
		count = (TextView) findViewById(R.id.count);
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ChatRoomHistoryActivity.this.finish();
			}
		});
		Button btnClear = (Button) findViewById(R.id.clear_history);
		btnClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MessageTipDialog diaolog = new MessageTipDialog(new DialogMsgCallback() {

					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-generated method stub
						if (isSubmit && mrdao.clearRoomHistory(groupId)) {
							showToast("清空成功!");
							page = 1;
							totalPage = 0;
							count.setText("1/1");
							list_item = new ArrayList<MeetRoom>();
							list = new ArrayList<MeetRoom>();
							mrAdapter.setData(list_item);
						}
					}
				}, ChatRoomHistoryActivity.this);
				diaolog.showDialog("提示!", "确认清空该群组所有聊天记录？", false);
			}
		});

	}

	private void initData() {
		uid = ((PangkerApplication) this.getApplicationContext()).getMySelf().getUserId();
		mrdao = new MeetRoomDaoImpl(this);
		list = mrdao.getRoomHistory(groupId);
		total = list.size();
		if (total % length == 0) {
			totalPage = total / length;
		} else {
			totalPage = (total / length) + 1;
		}
		if (totalPage == 0) {
			totalPage = 1;
		}
		count.setText(page + "/" + totalPage);
		list_item = new ArrayList<MeetRoom>();
		msgLimit();
		mrAdapter = new ChatRoomListAdapter(this, localFetcher);
		history.setAdapter(mrAdapter);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		mImageCache.removeMemoryCaches();
		super.onDestroy();
	}

	private void initListenner() {
		last.setOnClickListener(listener);
		next.setOnClickListener(listener);
	}

	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == next) {// 下一页
				if ((page + 1) <= totalPage) {
					start += length;
					page += 1;
					msgLimit();
					mrAdapter.notifyDataSetChanged();
					count.setText(page + "/" + totalPage);
				} else {
					Toast.makeText(ChatRoomHistoryActivity.this, R.string.toonext, Toast.LENGTH_SHORT).show();
				}
			} else if (v == last) {
				if (page - 1 > 0) {// 上一页
					start -= length;
					page -= 1;
					msgLimit();
					mrAdapter.notifyDataSetChanged();
					count.setText(page + "/" + totalPage);
				} else {
					Toast.makeText(ChatRoomHistoryActivity.this, R.string.toolast, Toast.LENGTH_SHORT).show();
				}
			}
		}
	};

	/**
	 * 分页
	 * 
	 * @author wubo
	 * @createtime Apr 13, 2012
	 */
	private void msgLimit() {
		list_item.clear();
		if ((start + length) >= total) {
			for (int i = start; i < total; i++) {
				list_item.add(list.get(i));
			}
		} else {
			for (int i = start; i < start + length; i++) {
				list_item.add(list.get(i));
			}
		}
	}
}
