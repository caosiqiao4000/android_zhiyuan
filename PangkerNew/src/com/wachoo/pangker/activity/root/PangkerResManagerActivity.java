package com.wachoo.pangker.activity.root;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.res.ResLocalManagerActivity;
import com.wachoo.pangker.activity.res.ResNetManagerActivity;
import com.wachoo.pangker.activity.res.ResRecommendActivity;
import com.wachoo.pangker.activity.res.ResShareActivity;
import com.wachoo.pangker.activity.res.ResSpeakActivity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.util.Util;

/**
 * 旁客资源管理 1）分享中的资源 2）网盘个人资源 3）旁客推荐资源 4）手机本地资源
 * 
 * @author wangxin
 * 
 */
public class PangkerResManagerActivity extends CommonPopActivity {

	private Button mBtnBack;// >>>>>>返回按钮

	private TextView txtTitle;// >>>>>>>>>标题

	private LinearLayout resShareLayout;// >>>>>分享中的资源
	private LinearLayout resStoreLayout;// >>>>>>>网盘资源
	private LinearLayout resSpeakLayout;// >>>>>>>我的说说
	private LinearLayout resLocalLayout;// >>>>>>>本地资源
	private LinearLayout resRecommentLayout;// >>>>>>>推荐资源

	private PangkerApplication mApplication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.panker_res_manager);

		mBtnBack = (Button) findViewById(R.id.btn_back);// >>>>>>>>>返回按钮
		findViewById(R.id.iv_more).setVisibility(View.GONE);// >>>>>>>>>隐藏功能按钮

		txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("我的资源");

		resShareLayout = (LinearLayout) findViewById(R.id.ly_res_share);// 分享中的资源
		resStoreLayout = (LinearLayout) findViewById(R.id.ly_res_web);// 网盘个人资源
		resSpeakLayout = (LinearLayout) findViewById(R.id.ly_res_speak);// 我的说说
		resLocalLayout = (LinearLayout) findViewById(R.id.ly_res_local);// 手机本地资源
		resRecommentLayout = (LinearLayout) findViewById(R.id.ly_res_recommend);// 旁客推荐资源

		mBtnBack.setOnClickListener(myOnClickListener);

		resShareLayout.setOnClickListener(myOnClickListener);
		resStoreLayout.setOnClickListener(myOnClickListener);
		resSpeakLayout.setOnClickListener(myOnClickListener);
		resLocalLayout.setOnClickListener(myOnClickListener);
		resRecommentLayout.setOnClickListener(myOnClickListener);

		mApplication = (PangkerApplication) getApplication();
	}

	OnClickListener myOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Intent intent = new Intent();
			// >>>>>>>>>>>网盘资源
			if (view == resStoreLayout) {
				UserItem item = Util
						.toUserItem(((PangkerApplication) getApplication())
								.getMySelf());
				intent.putExtra(PangkerConstant.INTENT_UESRITEM, item);
				intent.setClass(PangkerResManagerActivity.this,
						ResNetManagerActivity.class);
				PangkerResManagerActivity.this.startActivity(intent);
			}
			// >>>>>>>分享资源
			else if (view == resShareLayout) {
				intent.setClass(PangkerResManagerActivity.this,
						ResShareActivity.class);
				PangkerResManagerActivity.this.startActivity(intent);
			}
			// >>>>>>>>推荐资源
			else if (view == resRecommentLayout) {
				intent.setClass(PangkerResManagerActivity.this,
						ResRecommendActivity.class);
				startActivity(intent);
			}
			// >>>>>>>>本地资源
			else if (view == resLocalLayout) {
				if (mApplication.ismExternalStorageAvailable()) {
					intent.setClass(PangkerResManagerActivity.this,
							ResLocalManagerActivity.class);
					PangkerResManagerActivity.this.startActivity(intent);
				} else {
					showToast("SD卡不存在，无法打开本地资源!");
				}
			}
			// >>>>>>>>说说资源
			else if (view == resSpeakLayout) {
				intent.setClass(PangkerResManagerActivity.this,
						ResSpeakActivity.class);
				startActivity(intent);
			}

			// >>>>>>>>返回按钮
			else if (view == mBtnBack) {
				PangkerResManagerActivity.this.finish();
			}

		}
	};
}
