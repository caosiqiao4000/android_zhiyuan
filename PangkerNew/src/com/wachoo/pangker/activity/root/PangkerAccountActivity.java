package com.wachoo.pangker.activity.root;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.HomeActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.contact.BlackManagerActivity;
import com.wachoo.pangker.activity.contact.LocalContactsSelectActivity;
import com.wachoo.pangker.activity.res.ResRecommendActivity;
import com.wachoo.pangker.db.IGrantDao;
import com.wachoo.pangker.db.impl.GrantDaoImpl;
import com.wachoo.pangker.entity.Grant;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CallMeSetResult;
import com.wachoo.pangker.server.response.GrantNotifyResult;
import com.wachoo.pangker.server.response.PangkerCallgrant;
import com.wachoo.pangker.server.response.PangkerLocationgrant;
import com.wachoo.pangker.server.response.UserinfoQueryResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.ui.dialog.EditTextDialog.DialogResultCallback;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

public class PangkerAccountActivity extends CommonPopActivity implements IUICallBackInterface {

	private final int FRINGER_CODE = 0x14;// 更新旁克号码

	private final int QUESTER_CODE_CALLNO = 0x104;
	private final int BINDPHONE_CASE = 0x105;// >>>>>>>绑定电话
	private final int CALL_ROOT_SETING = 0x106;// >>>>>>电话呼叫权限设置
	private final int LOCATION_ROOT_SETING = 0x107;// >>>>>>位置查看权限设置
	private final int UPDATE_WEBSITENAME = 0X88;// >>>>>>更新网站名称
	private final int LOAD_USERINFO_CODE = 0X108;// 加载自己的用户信息

	private final int VIP_LEVEL = 0X21;// VIP用户
	private final String lowVipLevelStr = "   Sorry,亲，您目前还不是VIP用户哦~！只有我们的VIP用户才可设定自己的网站名称!至少邀请10位好友注册成功之后您就可以自动升级为VIP用户啦。点击“确定”,现在就去邀请好友吧~！";
	// **********定位设置**********
	private LinearLayout lyLocateSetting;// >>>>>>>定位设置
	private TextView txtLocateSeting;// >>>>>>>定位设置详情

	// *********旁客号码***********
	private LinearLayout fingerLayout;
	private TextView tv_finger;
	private ImageView iv_finger;
	// ***********旁客网站*********
	private LinearLayout lyWebSiteName;
	// private ImageView iv_webSite;
	private TextView txtWebSiteName;
	private String websiteName = "";
	private UserInfo userInfo;

	// ********消息通知设置*******
	private LinearLayout lySoundSetLayout;
	private TextView txtSoundSeting;
	// *********通讯录匹配***********
	private LinearLayout addressLayout;// >>>>>>>通讯录匹配
	private TextView txtAddressMatchSeting;
	// **********语音呼叫 &&　　位置权限 移植过来的代码**********
	private LinearLayout lyCallRootSeting;//
	private TextView txtCallRootSeting;
	private LinearLayout lyLocationRootSeting;//
	private TextView txtLocationRootSeting;
	private String userId;

	private IGrantDao mGrantDao; // >>>>>>>权限

	private String[] calls = null;// 拨号权限表
	private String[] call_ids = null;// 拨号权限表id
	private String[] locations = null;// 漂移权限表
	private String[] location_ids = null;// 漂移权限表id

	// >>>>>>>>>>>>选中的项目值
	private int selectRootVale = 0;

	// >>>>>>>>>选择时使用
	private PopMenuDialog mMenuDialog;
	// >>>>>>>电话
	private ActionItem[] callItems = null;
	private ActionItem[] locationItems = null;
	private int CLICK_CODE = 0;// 0呼叫1漂移2友踪3查看我的位置信息>>>>>>>lb -1:资源查看权限
	private int callSelected = 1;
	private int locationSelected = 1;

	// *********上传下载设置***********
	private LinearLayout updownSetingLayout;// >>>>>上传下载设置 ly_updaownload_seting

	// ***************资源推荐设置**********************
	private LinearLayout resRecommendSetingLayout;// >>>>>资源推荐设置
	private PopMenuDialog menuDialog;
	private TextView tvRecommendSeting;//

	private int seting;
	private ActionItem[] setMenu;
	private int[] recommendIds = null;
	private String[] recommendNames = null;

	// ***************资源推荐设置**********************

	// ***********黑名单***************
	private LinearLayout blackManaLayout;// >>>>>>>黑名单
	// ***********黑名单***************
	private PangkerApplication application;
	private SharedPreferencesUtil mPreferencesUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.panker_account);
		application = (PangkerApplication) getApplication();
		mPreferencesUtil = new SharedPreferencesUtil(this);
		userInfo = application.getMySelf();
		userId = application.getMyUserID();
		boolean isEmpty = Util.isEmpty(userInfo.getWebsiteName());
		websiteName = !isEmpty ? userInfo.getWebsiteName() : "您还没有设置";
		mGrantDao = new GrantDaoImpl(this);
		initView();
		initData();

	}

	private void initData() {

		if (!Util.isEmpty(application.getImAccount())) {
			tv_finger.setVisibility(View.VISIBLE);
			tv_finger.setText(application.getImAccount());
		} else {
			tv_finger.setVisibility(View.GONE);
		}
		recommendIds = getResources().getIntArray(R.array.res_recommend_ids);
		recommendNames = getResources().getStringArray(R.array.res_recommend_names);

		seting = mPreferencesUtil.getInt(ResRecommendActivity.KEY_RECOMMEND_OPEN + application.getMyUserID(),
				recommendIds[0]);
		tvRecommendSeting.setText(recommendNames[seting]);

		// >>>>>>>>>
		initCallLocation();
		initViewData();

	}

	private void showSetMenu() {
		// TODO Auto-generated method stub
		// >>>>>>>获取推荐设置类型
		seting = mPreferencesUtil.getInt(ResRecommendActivity.KEY_RECOMMEND_OPEN + application.getMyUserID(),
				recommendIds[0]);

		setMenu = new ActionItem[recommendIds.length];
		for (int i = 0; i < recommendIds.length; i++) {
			ActionItem item = new ActionItem(recommendIds[i], recommendNames[i], seting == recommendIds[i]);
			setMenu[i] = item;
		}
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
		}
		menuDialog.setMenuChoose(setMenu, true);
		menuDialog.setTitle("歌曲接收设置");
		menuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			mPreferencesUtil.saveInt(ResRecommendActivity.KEY_RECOMMEND_OPEN + application.getMyUserID(),
					recommendIds[actionId]);
			menuDialog.clearChoosed();
			tvRecommendSeting.setText(recommendNames[actionId]);
			menuDialog.setChoosed(actionId);
			menuDialog.dismiss();
		}
	};

	private void initView() {
		// TODO Auto-generated method stub
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("旁客设置");
		findViewById(R.id.iv_more).setVisibility(View.GONE);

		tvRecommendSeting = (TextView) findViewById(R.id.tv_recommend_seting);
		tv_finger = (TextView) findViewById(R.id.tv_finger);
		iv_finger = (ImageView) findViewById(R.id.iv_finger);
		fingerLayout = (LinearLayout) findViewById(R.id.ly_fingerprint);

		// >>>>个人网站
		lyWebSiteName = (LinearLayout) findViewById(R.id.ly_webside_name);
		// iv_webSite = (ImageView) findViewById(R.id.iv_website);
		txtWebSiteName = (TextView) findViewById(R.id.tv_webside_name);
		txtWebSiteName.setText(websiteName);

		// >>>>>>定位设置
		lyLocateSetting = (LinearLayout) findViewById(R.id.ly_locate_setting);
		txtLocateSeting = (TextView) findViewById(R.id.tv_locate_seting);
		// >>>>>>>>通讯录匹配功能
		addressLayout = (LinearLayout) findViewById(R.id.ly_address);
		txtAddressMatchSeting = (TextView) findViewById(R.id.tv_address_match_seting);
		// >>>>>>位置权限
		lyLocationRootSeting = (LinearLayout) findViewById(R.id.ly_location_seting);
		txtLocationRootSeting = (TextView) findViewById(R.id.tv_location_seting);

		// >>>>>>>>>语音呼叫
		lyCallRootSeting = (LinearLayout) findViewById(R.id.ly_call_seting);
		txtCallRootSeting = (TextView) findViewById(R.id.tv_call_seting);

		// >>>>>>>声音设置
		lySoundSetLayout = (LinearLayout) findViewById(R.id.ly_sound_setting);
		txtSoundSeting = (TextView) findViewById(R.id.tv_sound_seting);

		// >>>>>>>>上传下载设置
		updownSetingLayout = (LinearLayout) findViewById(R.id.ly_updaownload_seting);
		resRecommendSetingLayout = (LinearLayout) findViewById(R.id.ly_res_recommend_seting);// >>>>>资源推荐设置
		blackManaLayout = (LinearLayout) findViewById(R.id.ly_blackmana);// >>>>>>>黑名单

		fingerLayout.setOnClickListener(listener);
		lyWebSiteName.setOnClickListener(listener);
		addressLayout.setOnClickListener(listener);
		lyLocateSetting.setOnClickListener(listener);
		updownSetingLayout.setOnClickListener(listener);
		resRecommendSetingLayout.setOnClickListener(listener);
		blackManaLayout.setOnClickListener(listener);
		lySoundSetLayout.setOnClickListener(listener);
		checkFinger();
		checkWebsite();
	}

	private void showInviteDialog() {
		// TODO Auto-generated method stub
		MessageTipDialog messageTipDialog = new MessageTipDialog(new DialogMsgCallback() {
			@Override
			public void buttonResult(boolean isSubmit) {
				// TODO Auto-generated method stub
				if (!isSubmit)
					return;
				launch(LocalContactsSelectActivity.class);
			}
		}, this);

		messageTipDialog.showDialog("权限不足", lowVipLevelStr, false);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initViewData();
	}

	private void initViewData() {
		// >>>>>>>>通讯录是否开启状态
		boolean isBind = mPreferencesUtil.getBoolean(PangkerConstant.IS_BIND_KEY, false);
		if (isBind)
			txtAddressMatchSeting.setText("已开启");
		else
			txtAddressMatchSeting.setText("未开启");
		// >>>>>>>>获取上次设置的方式
		boolean isAutoLocate = mPreferencesUtil.getBoolean(LOCATION_SERVICE + userId, true);
		boolean isWifiLocate = mPreferencesUtil.getBoolean(PangkerConstant.WIFI_LOCATE_KEY + userId, true);

		StringBuffer locateSeting = new StringBuffer();
		locateSeting.append(isAutoLocate ? "自动" : "手动");
		locateSeting.append(" , WIFI邻居");
		locateSeting.append(isWifiLocate ? "(开)" : "(关)");
		txtLocateSeting.setText(locateSeting);

		// >>>>>>>>消息通知设置
		boolean isSound = mPreferencesUtil.getBoolean(PangkerConstant.SP_SOUND_SET + userId, true);
		boolean isShock = mPreferencesUtil.getBoolean(PangkerConstant.SP_SHOCK_SET + userId, false);
		StringBuffer noticeSeting = new StringBuffer();
		noticeSeting.append("声音");
		noticeSeting.append(isSound ? "(开)" : "(关)");
		noticeSeting.append(" , 震动");
		noticeSeting.append(isShock ? "(开)" : "(关)");
		txtSoundSeting.setText(noticeSeting);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == FRINGER_CODE && resultCode == RESULT_OK) {
			checkFinger();
		}
		if (requestCode == QUESTER_CODE_CALLNO && resultCode == RESULT_OK) {
			initData();
		}
	}

	private void checkFinger() {
		if (Util.isEmpty(application.getImAccount())) {
			iv_finger.setImageResource(R.drawable.icon46_inf_7);
		} else {
			iv_finger.setImageResource(R.drawable.icon46_inf);
			tv_finger.setText(application.getImAccount());
		}

		Message msg = new Message();
		msg.what = -1;
		PangkerManager.getTabBroadcastManager().sendResultMessage(HomeActivity.class.getName(), msg);
	}

	private void checkWebsite() {
		// if (Util.isEmpty(userInfo.getWebsiteName())) {
		// iv_webSite.setImageResource(R.drawable.icon46_inf_7);
		// } else {
		// iv_webSite.setImageResource(R.drawable.icon46_inf);
		// }
	}

	/**
	 * 监听
	 */
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == fingerLayout) {
				Intent intent = new Intent();
				intent.setClass(PangkerAccountActivity.this, FingerprintActivity.class);
				startActivityForResult(intent, FRINGER_CODE);
			} else if (v == lyWebSiteName) {
				if (Util.isEmpty(userInfo.getWebsiteName())) {
					loadMyselfUserInfo(userId);
				} else {
					showToast("您已经设定好了！");
				}
			} else if (v == lyLocateSetting) {
				Intent intent = new Intent();
				intent.setClass(PangkerAccountActivity.this, LocateSettingActivity.class);
				startActivity(intent);
			} else if (v == addressLayout) {
				launch(AddressMatchActivity.class);
			} else if (v == resRecommendSetingLayout) {
				showSetMenu();
			} else if (v == updownSetingLayout) {
				launch(DownloadSetActivity.class);
			} else if (v == blackManaLayout) {
				launch(BlackManagerActivity.class);
			} else if (v == lySoundSetLayout) {
				launch(SoundSetActivity.class);
			}
		}
	};

	private void setWebsiteNameDialog() {
		// TODO Auto-generated method stub
		EditTextDialog dialog = new EditTextDialog(PangkerAccountActivity.this);
		dialog.setLine(4);
		dialog.showDialog(R.string.dialog_setWebsideName, "", "恭喜您已经是我们的VIP用户啦！现在就请输入您自己的网站名称吧。", 20,
				new DialogResultCallback() {
					@Override
					public void buttonResult(String result, boolean isSubmit) {
						// TODO Auto-generated method stub
						if (isSubmit && result != null) {
							websiteName = result.trim();
							int len;
							try {
								len = Util.getHalfStringLength(websiteName);
								if (len >= 12 && len <= 20) {
									updateWebsiteName();
								} else if (len < 12) {
									showToast("网站名称至少6个汉字或者12个字母、数字、下划线");
								}
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});
	}

	/**
	 * 加载用户信息
	 * 
	 * @author wubo
	 * 
	 */
	public void loadMyselfUserInfo(String userId) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", userId));
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("optype", "1"));
		String message = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getUserinfoQuery(), paras, true, message, LOAD_USERINFO_CODE);
	}

	/**
	 * 更新网站名称
	 * 
	 * @JarlinHot
	 */
	private void updateWebsiteName() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("webSiteName", websiteName));
		serverMana.supportRequest(Configuration.getWebsiteNameModify(), paras, UPDATE_WEBSITENAME);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse)) {
			return;
		}
		if (caseKey == BINDPHONE_CASE) {
			UserinfoQueryResult result = JSONUtil.fromJson(supportResponse.toString(), UserinfoQueryResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {

				UserInfo info = result.getUserInfo();
				if (info != null && !Util.isEmpty(info.getPhone())) {
					Util.delFileData(PangkerAccountActivity.this, PangkerConstant.CERTIFICATE_FILE_NAME);
					application.setMySelf(info);
					Util.writeHomeUser(this, info);
				} else
					showToast("同步失败!");
			} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
			} else
				showToast("同步失败!");
		}
		// >>>>>>位置电话权限设置结果返回
		if (caseKey == CALL_ROOT_SETING || caseKey == LOCATION_ROOT_SETING) {
			CallMeSetResult result = JSONUtil.fromJson(supportResponse.toString(), CallMeSetResult.class);
			if (result != null && result.errorCode != 999) {
				showToast(result.getErrorMessage());
				if (result.getErrorCode() == 1) {
					mGrantDao.updateGrant(userId, String.valueOf(CLICK_CODE), selectRootVale);
				}
			} else {
				showToast(R.string.return_value_999);
			}
		}

		if (caseKey == LOAD_USERINFO_CODE) {
			UserinfoQueryResult result = JSONUtil.fromJson(supportResponse.toString(), UserinfoQueryResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {

				UserInfo info = result.getUserInfo();
				if (info != null && info.getVipLevel() != userInfo.getVipLevel()) {
					Util.delFileData(PangkerAccountActivity.this, PangkerConstant.CERTIFICATE_FILE_NAME);
					application.setMySelf(info);
					Util.writeHomeUser(this, info);
					userInfo = application.getMySelf();
				}
				if (userInfo.getVipLevel() > 1000) {
					setWebsiteNameDialog();
				} else {
					showInviteDialog();
				}
			} else if (result != null && result.getErrorCode() == BaseResult.FAILED) {
				showToast(result.getErrorMessage());
			} else {
				showToast("获取权限失败!");
			}

		}

		if (caseKey == UPDATE_WEBSITENAME) {
			BaseResult baseResult = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (baseResult != null && baseResult.errorCode == BaseResult.SUCCESS) {
				txtWebSiteName.setText(websiteName);
				showToast(R.string.set_success);
				application.getMySelf().setWebsiteName(websiteName);
				Util.writeHomeUser(this, application.getMySelf());
			} else if (baseResult != null && baseResult.errorCode == BaseResult.FAILED) {
				showToast(baseResult.getErrorMessage());
			} else {
				showToast(R.string.to_server_fail);
			}
		}
	}

	// >>>>>>>>>>>>>>>电话 位置权限代码
	private void initCallLocation() {
		// >>>>>>>>>获取呼叫权限
		calls = getResources().getStringArray(R.array.root_call);
		call_ids = getResources().getStringArray(R.array.root_call_id);

		// >>>>>>>>>>>获取漂移权限
		locations = getResources().getStringArray(R.array.root_move);
		location_ids = getResources().getStringArray(R.array.root_move_id);

		// >>>>>>获取呼叫权限
		Grant mCallGrant = mGrantDao.getGrantValue(userId, Grant.GRANT_TYPE_CALL);
		// >>>>>>>获取位置权限
		Grant mLocationGrant = mGrantDao.getGrantValue(userId, Grant.GRANT_TYPE_LOCATION);

		// >>>>>>判断是否需要同步
		if (mCallGrant == null || mLocationGrant == null) {
			notifyGrant();
		} else {
			callSelected = mCallGrant.getGrantValue() - 1;
			locationSelected = mLocationGrant.getGrantValue() - 1;
			txtCallRootSeting.setText(calls[callSelected]);
			txtLocationRootSeting.setText(locations[locationSelected]);
		}

		callItems = new ActionItem[call_ids.length];
		for (int i = 0; i < call_ids.length; i++) {
			ActionItem actionItem = new ActionItem(Util.String2Integer(call_ids[i]), calls[i], getResources()
					.getDrawable(R.drawable.icon32_radio));
			callItems[i] = actionItem;
		}
		callItems[callSelected].setIcon(getResources().getDrawable(R.drawable.icon32_radio_p));

		locationItems = new ActionItem[location_ids.length];
		for (int i = 0; i < call_ids.length; i++) {
			ActionItem actionItem = new ActionItem(Util.String2Integer(location_ids[i]), locations[i], getResources()
					.getDrawable(R.drawable.icon32_radio));
			locationItems[i] = actionItem;
		}
		locationItems[locationSelected].setIcon(getResources().getDrawable(R.drawable.icon32_radio_p));
		lyCallRootSeting.setOnClickListener(onClickListener);
		lyLocationRootSeting.setOnClickListener(onClickListener);

	}

	private void notifyGrant() {
		// TODO Auto-generated method stub
		try {

			SyncHttpClient http = new SyncHttpClient();
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", "1"));//
			paras.add(new Parameter("uid", userId)); //
			String grantNotity = http.httpPost(Configuration.getGrantNotify(), paras, application.getServerTime());
			if (grantNotity != null) {
				GrantNotifyResult grantResult = JSONUtil.fromJson(grantNotity, GrantNotifyResult.class);
				if (grantResult != null && grantResult.getErrorCode() == BaseResult.SUCCESS) {

					// >>>>>>>>同步位置权限
					PangkerLocationgrant locationGrant = grantResult.getLocationgrant();
					if (locationGrant != null) {
						Grant mGrant = new Grant(userId, Grant.GRANT_TYPE_LOCATION, locationGrant.getGrantType(),
								locationGrant.getGrantInfo(), locationGrant.getMembers());
						mGrantDao.saveGrant(mGrant);
						callSelected = mGrant.getGrantValue() - 1;
						txtLocationRootSeting.setText(locations[mGrant.getGrantValue() - 1]);

					}
					// >>>>>>>>同步呼叫权限
					PangkerCallgrant callGrant = grantResult.getCallgrant();
					if (callGrant != null) {
						Grant mGrant = new Grant(userId, Grant.GRANT_TYPE_LOCATION, callGrant.getGrantType(),
								callGrant.getGrantInfo(), callGrant.getMembers());
						mGrantDao.saveGrant(mGrant);
						locationSelected = mGrant.getGrantValue() - 1;
						txtCallRootSeting.setText(calls[mGrant.getGrantValue() - 1]);
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			showToast("获取权限失败");
		}
	}

	private void initMenuDialog() {
		if (mMenuDialog == null) {
			mMenuDialog = new PopMenuDialog(this, R.style.MyDialog);
			mMenuDialog.setListener(clickListener);
		}
	}

	UITableView.ClickListener clickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			if (CLICK_CODE == 0) {
				// >>>>>>>>设置当前选中项目
				txtCallRootSeting.setText(calls[actionId - 1]);

				// >>>>>>>>设置图标
				for (ActionItem item : callItems) {
					item.setIcon(getResources().getDrawable(R.drawable.icon32_radio));
				}
				callItems[actionId - 1].setIcon(getResources().getDrawable(R.drawable.icon32_radio_p));

				// >>>>>>>>保存结果
				setAllowOrPass(actionId - 1);
			} else if (CLICK_CODE == 3) {

				// >>>>>>>>设置当前选中项目
				txtLocationRootSeting.setText(locations[actionId - 1]);
				// >>>>>>>>设置图标
				for (ActionItem item : locationItems) {
					item.setIcon(getResources().getDrawable(R.drawable.icon32_radio));
				}
				locationItems[actionId - 1].setIcon(getResources().getDrawable(R.drawable.icon32_radio_p));

				// >>>>>>>>保存结果
				setAllowOrPass(actionId - 1);
			}
			mMenuDialog.dismiss();
		}
	};

	public void setAllowOrPass(int type) {
		ServerSupportManager serverMana = new ServerSupportManager(this, PangkerAccountActivity.this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		if (CLICK_CODE == 0) {
			paras.add(new Parameter("type", call_ids[type]));
			selectRootVale = Util.String2Integer(call_ids[type]);
			serverMana.supportRequest(Configuration.getCallMeSet(), paras, CALL_ROOT_SETING);
		} else if (CLICK_CODE == 3) {
			paras.add(new Parameter("type", location_ids[type]));
			selectRootVale = Util.String2Integer(location_ids[type]);
			serverMana.supportRequest(Configuration.getLocationQuerySet(), paras, LOCATION_ROOT_SETING);
		}
	}

	/**
	 * 监听
	 */
	OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			initMenuDialog();
			if (v == lyCallRootSeting) {// 呼叫权限
				CLICK_CODE = 0;
				mMenuDialog.setTitle(R.string.root_call_set);
				mMenuDialog.setMenus(callItems);
			}

			else if (v == lyLocationRootSeting) {
				CLICK_CODE = 3;
				mMenuDialog.setTitle(R.string.root_location_set);
				mMenuDialog.setMenus(locationItems);
			}
			mMenuDialog.show();
		}
	};

}
