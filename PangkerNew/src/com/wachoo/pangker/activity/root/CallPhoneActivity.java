package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.ICallPhoneDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.impl.CallPhoneDaoImpl;
import com.wachoo.pangker.db.impl.PKUserDaoImpl;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;
/**
 * 呼叫转移
 * @author zhengjy
 *  2012-011-06
 */
public class CallPhoneActivity extends CommonPopActivity {
	
	private String match = "^1[3|4|5|8][0-9]\\d{4,8}$";
	private PangkerApplication application;
	private LinearLayout lyCallOld;
	private LinearLayout lyEverPhone;
	private TextView tv_phone;
	private LinearLayout ly_phone;
	private Button btnEverPhone;
	private TextView txtAccount;
	private EditText etCallAccount;
	private EditText etCheck;
	private Button btnSubmit;
	private Button btnRestore;
	private String mUserid;
	private IPKUserDao pkUserDao;
	
	private String callAccount = "";
	private int nowDalogId = -1;
	private String[] everCallPhones;
    private ICallPhoneDao mCallDao;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.callphone_layout);
		application = (PangkerApplication) getApplication();
		mUserid = application.getMyUserID();
		pkUserDao = new PKUserDaoImpl(this);
		mCallDao = new CallPhoneDaoImpl(this); 
		
		everCallPhones = mCallDao.getPhoneNumbers(mUserid);
		initView();
		//CheckSecret();
	}

	private void initView() {
		
		txtAccount= (TextView)findViewById(R.id.txt_account);
		lyEverPhone = (LinearLayout)findViewById(R.id.ly_ever_phone);
		ly_phone = (LinearLayout)findViewById(R.id.ly_phone);
		tv_phone = (TextView)findViewById(R.id.tv_phone);
		btnEverPhone = (Button)findViewById(R.id.btn_phone);
		lyCallOld =  (LinearLayout)findViewById(R.id.ly_call_old);
		etCallAccount = (EditText)findViewById(R.id.et_call_account);
		etCheck = (EditText)findViewById(R.id.et_check);
		btnSubmit =  (Button)findViewById(R.id.btn_submit);
		btnRestore =  (Button)findViewById(R.id.btn_restore);
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(checkInput()){
					callAccount = etCallAccount.getText().toString().trim();
					passManager(callAccount);
				}
			}
		});
		
		Button button = (Button)findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//隐藏输入法
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
						CallPhoneActivity.this.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
				finish();
			}
		});
		btnRestore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callAccount = "";
				passManager("");
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("旁客呼叫号码");
        findViewById(R.id.iv_more).setVisibility(View.GONE);
        
        if(!Util.isEmpty(application.getMySelf().getCallphone())){
        	lyCallOld.setVisibility(View.VISIBLE);
        	btnRestore.setVisibility(View.VISIBLE);
        	txtAccount.setText(application.getMySelf().getCallphone());
        }
        else {
        	lyCallOld.setVisibility(View.GONE);
            btnRestore.setVisibility(View.GONE);
        }
        btnEverPhone.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				createDialog(1);
			}
		});
        ly_phone.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				createDialog(1);
			}
		});
        if(everCallPhones != null && everCallPhones.length > 0){
        	lyEverPhone.setVisibility(View.VISIBLE);
        }
        else lyEverPhone.setVisibility(View.GONE);
	}
	private boolean checkInput(){
		String callAccount = etCallAccount.getText().toString().trim();
		String check = etCheck.getText().toString().trim();
		if(Util.isEmpty(callAccount)){
			showToast("旁客呼叫号码不能为空!");
			return false;
		}
		if(!callAccount.matches(match)){
			showToast("请输入正确的手机号!");
			return false;
		}
		if(Util.isEmpty(check)){
			showToast("请验证旁客呼叫号码!");
			return false;
		}
		if(!callAccount.equals(check)){
			showToast("验证输入呼叫号码不匹配!");
			return false;
		}
		return true;
		
	}
	
	public void createDialog(int id) {
		if (nowDalogId != -1) {
			removeDialog(nowDalogId);
		}
		nowDalogId = id;
		showDialog(nowDalogId);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		
		if(id == 1){
			Builder builder = new AlertDialog.Builder(CallPhoneActivity.this);
			builder.setSingleChoiceItems(everCallPhones, 0, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					if(which >= 0){
						etCallAccount.setText(everCallPhones[which]);
						etCheck.setText(everCallPhones[which]);
					}
				}
			});
			return builder.create();
		}
		return null;
	}
		
	/**
	 * 密码设置
	 * 
	 * 
	 * 接口：Setsecret.json
	 */
	private void passManager(String phone) {
		
		ServerSupportManager serverMana = new ServerSupportManager(this,new IUICallBackInterface() {
			
			@Override
			public void uiCallBack(Object supportResponse, int caseKey) {
				// TODO Auto-generated method stub
				if (!HttpResponseStatus(supportResponse))
					return;
				BaseResult baseResult = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
				if(baseResult != null && baseResult.errorCode == BaseResult.SUCCESS){
					showToast(baseResult.getErrorMessage());
					application.getMySelf().setCallphone(callAccount);
					mCallDao.insertPhone(mUserid, callAccount);
					pkUserDao.updateUser(application.getMySelf());
					Util.writeHomeUser(CallPhoneActivity.this, application.getMySelf());
					setResult(RESULT_OK);
					finish();
				}
				else if(baseResult != null && baseResult.errorCode == BaseResult.FAILED){
					showToast(baseResult.getErrorMessage());
				}
				else showToast(R.string.to_server_fail);
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1000"));
		paras.add(new Parameter("uid", mUserid));	
		paras.add(new Parameter("phone", phone));
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getSetCallRedirect(), paras, true, mess);
	}

}
