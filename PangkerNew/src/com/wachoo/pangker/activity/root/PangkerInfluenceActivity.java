package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.contact.ContactFollowActivity;
import com.wachoo.pangker.activity.contact.UnderUserActivity;
import com.wachoo.pangker.activity.pangker.DriftUserActivity;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.QueryRelationCountResult;

public class PangkerInfluenceActivity extends CommonPopActivity implements IUICallBackInterface {
	private Button mBtnBack;// >>>>>>返回按钮

	private TextView txtDrift;// >>>>>>>漂移情况
	private TextView txtUnders;// >>>>>>>>我邀请的用户情况
	private TextView txtVisitor;// >>>>>>>访问人次
	private TextView txtFansCount;// >>>>>>>粉丝数量
	private TextView txtTitle;// >>>>>>>>>标题

	private LinearLayout driftLayout;// >>>>>>>谁在漂移我
	private LinearLayout visitorLayout;// >>>>>>>>>谁访问过我
	private LinearLayout fansLayout;// >>>>>>>>我的粉丝
	private LinearLayout undersLayout;// >>>>>>>我邀请的用户（我的下家）

	private PangkerApplication mApplication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// >>>>>>>>>>加载view
		setContentView(R.layout.panker_influence);
		mBtnBack = (Button) findViewById(R.id.btn_back);// >>>>>>>>>返回按钮
		findViewById(R.id.iv_more).setVisibility(View.GONE);// >>>>>>>>>隐藏功能按钮

		txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("我的影响力");
		txtDrift = (TextView) findViewById(R.id.txt_dirfts);
		txtVisitor = (TextView) findViewById(R.id.txt_visitors);
		txtFansCount = (TextView) findViewById(R.id.txt_fan_num);
		txtUnders = (TextView) findViewById(R.id.txt_underusers);

		fansLayout = (LinearLayout) findViewById(R.id.ly_fans);
		driftLayout = (LinearLayout) findViewById(R.id.dirfts_layout);

		undersLayout = (LinearLayout) findViewById(R.id.underusers_layout);
		visitorLayout = (LinearLayout) findViewById(R.id.visitor_layout);

		// >>>>>>>>加载监听
		mBtnBack.setOnClickListener(myOnClickListener);
		fansLayout.setOnClickListener(myOnClickListener);
		driftLayout.setOnClickListener(myOnClickListener);
		visitorLayout.setOnClickListener(myOnClickListener);
		undersLayout.setOnClickListener(myOnClickListener);

		mApplication = (PangkerApplication) getApplication();

		// >>>>>>>>加载数据
		QueryRelationCount();
	}

	OnClickListener myOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// >>>>>>>>返回按钮
			if (v == mBtnBack) {
				PangkerInfluenceActivity.this.finish();
			} else if (v == driftLayout) {
				launch(DriftUserActivity.class);
			} else if (v == visitorLayout) {
				launch(VisitorActivity.class);
			} else if (v == undersLayout) {
				launch(UnderUserActivity.class);
			} else if (v == fansLayout) {
				launch(ContactFollowActivity.class);
			}
		}
	};

	/**
	 * 1.26 QueryRelationCount
	 */
	public void QueryRelationCount() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", mApplication.getMyUserID()));
		paras.add(new Parameter("appid", mApplication.getMyUserID()));
		serverMana.supportRequest(Configuration.getQueryRelationCount(), paras);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse)) {
			return;
		}

		QueryRelationCountResult result = JSONUtil.fromJson(supportResponse.toString(),
				QueryRelationCountResult.class);
		if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
			txtDrift.setText("(" + result.getDriftCount() + "位用户在漂移我)");
			txtVisitor.setText("(累计" + result.getVisitorCount() + "人次访问主页)");
			txtUnders.setText("(已成功邀请" + result.getFollowCount() + "位用户)");
			txtFansCount.setText("您有" + result.getFansCount() + "个粉丝");
		}

	}
}
