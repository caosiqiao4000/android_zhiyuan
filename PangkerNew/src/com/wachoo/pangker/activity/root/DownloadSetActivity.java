package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.SwitchView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;

/**
 * set the download
 * 
 * @author zxx
 * 
 */
public class DownloadSetActivity extends CommonPopActivity {

	private LinearLayout soundLayout;
	private SwitchView soundSwitchView;
	private LinearLayout installLayout;
	private SwitchView installSwitchView;

	private LinearLayout netDownloadLayout;
	private TextView txtnetDownload;
	private LinearLayout netUploadLayout;
	private TextView txtnetUpload;

	private String[] DOWNLOAD_TYPE_NTE;
	private String[] UPLOAD_TYPE_NTE;

	private PopMenuDialog menuDialog;
	private SharedPreferencesUtil apu;
	private String mUserid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.download_set);
		mUserid = ((PangkerApplication) this.getApplicationContext()).getMyUserID();
		DOWNLOAD_TYPE_NTE = getResources().getStringArray(R.array.download_set_type);
		UPLOAD_TYPE_NTE = getResources().getStringArray(R.array.upload_set_type);
		inintView();
	}

	private void inintView() {
		Button btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(onClickListener);
		TextView txt_title = (TextView) findViewById(R.id.mmtitle);
		txt_title.setText("下载设置");
		findViewById(R.id.iv_more).setVisibility(View.GONE);

		apu = new SharedPreferencesUtil(this);
		soundLayout = (LinearLayout) findViewById(R.id.soundLayout);
		soundLayout.setOnClickListener(onClickListener);
		soundSwitchView = (SwitchView) findViewById(R.id.soundSwitchView);
		soundSwitchView.setChecked(apu.getBoolean(PangkerConstant.SP_DOWNLOAD_SET_SOUND + mUserid, false));
		soundSwitchView.setOnCheckedChangeListener(soundChangeListener);

		installLayout = (LinearLayout) findViewById(R.id.installLayout);
		installLayout.setOnClickListener(onClickListener);
		installSwitchView = (SwitchView) findViewById(R.id.installSwitchView);
		installSwitchView
				.setChecked(apu.getBoolean(PangkerConstant.SP_DOWNLOAD_SET_INSTALL + mUserid, false));
		installSwitchView.setOnCheckedChangeListener(installChangeListener);

		netDownloadLayout = (LinearLayout) findViewById(R.id.netDownloadLayout);
		netDownloadLayout.setOnClickListener(onClickListener);
		txtnetDownload = (TextView) findViewById(R.id.tv_net_download);
		txtnetDownload
				.setText(DOWNLOAD_TYPE_NTE[apu.getInt(PangkerConstant.SP_DOWNLOAD_SET_NTE + mUserid, 0)]);

		netUploadLayout = (LinearLayout) findViewById(R.id.netUploadLayout);
		netUploadLayout.setOnClickListener(onClickListener);
		txtnetUpload = (TextView) findViewById(R.id.tv_net_upload);
		txtnetUpload.setText(UPLOAD_TYPE_NTE[apu.getInt(PangkerConstant.SP_UPLOAD_SET_NTE + mUserid, 0)]);

	}

	private View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.soundLayout) {
				soundSwitchView.setChecked(!soundSwitchView.isChecked());
			}
			if (v.getId() == R.id.installLayout) {
				installSwitchView.setChecked(!installSwitchView.isChecked());
			}
			if (v.getId() == R.id.btn_back) {
				finish();
			}
			if (v.getId() == R.id.netDownloadLayout) {
				showMenu(
						"下载资源网络设置",
						getActionItemsByIndex(1),
						apu.getInt(PangkerConstant.SP_DOWNLOAD_SET_NTE + mUserid,
								apu.getInt(PangkerConstant.SP_DOWNLOAD_SET_NTE + mUserid, 0)));
			}
			if (v.getId() == R.id.netUploadLayout) {
				showMenu(
						"上传资源网络设置",
						getActionItemsByIndex(2),
						apu.getInt(PangkerConstant.SP_UPLOAD_SET_NTE + mUserid,
								apu.getInt(PangkerConstant.SP_UPLOAD_SET_NTE + mUserid, 0)));
			}
		}
	};

	private List<ActionItem> getActionItemsByIndex(int index) {
		List<ActionItem> actionItems = new ArrayList<ActionItem>();
		if (index == 1) {
			for (int i = 0; i < DOWNLOAD_TYPE_NTE.length; i++) {
				actionItems.add(new ActionItem(1 + i, DOWNLOAD_TYPE_NTE[i], false));
			}
		} else if (index == 2) {
			for (int i = 0; i < UPLOAD_TYPE_NTE.length; i++) {
				actionItems.add(new ActionItem(11 + i, UPLOAD_TYPE_NTE[i], false));
			}
		}
		return actionItems;
	}

	private void showMenu(String title, List<ActionItem> msgMenu, int choose) {
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
		}
		menuDialog.setTitle(title);
		menuDialog.setMenuChoose(msgMenu, choose);
		menuDialog.isShowCheckbox(true);
		menuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			switch (actionid) {
			case 1:
				txtnetDownload.setText(DOWNLOAD_TYPE_NTE[0]);
				apu.saveInt(PangkerConstant.SP_DOWNLOAD_SET_NTE + mUserid, 0);
				break;
			case 2:
				txtnetDownload.setText(DOWNLOAD_TYPE_NTE[1]);
				apu.saveInt(PangkerConstant.SP_DOWNLOAD_SET_NTE + mUserid, 1);
				break;
			case 3:
				txtnetDownload.setText(DOWNLOAD_TYPE_NTE[2]);
				apu.saveInt(PangkerConstant.SP_DOWNLOAD_SET_NTE + mUserid, 2);
				break;
			case 11:
				txtnetUpload.setText(UPLOAD_TYPE_NTE[0]);
				apu.saveInt(PangkerConstant.SP_UPLOAD_SET_NTE + mUserid, 0);
				break;
			case 12:
				txtnetUpload.setText(UPLOAD_TYPE_NTE[1]);
				apu.saveInt(PangkerConstant.SP_UPLOAD_SET_NTE + mUserid, 1);
				break;
			case 13:
				txtnetUpload.setText(UPLOAD_TYPE_NTE[2]);
				apu.saveInt(PangkerConstant.SP_UPLOAD_SET_NTE + mUserid, 2);
				break;
			}
			menuDialog.dismiss();
		}
	};

	SwitchView.OnCheckedChangeListener soundChangeListener = new SwitchView.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(boolean isChecked) {
			// TODO Auto-generated method stub
			apu.saveBoolean(PangkerConstant.SP_DOWNLOAD_SET_SOUND + mUserid, isChecked);
		}
	};

	SwitchView.OnCheckedChangeListener installChangeListener = new SwitchView.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(boolean isChecked) {
			// TODO Auto-generated method stub
			apu.saveBoolean(PangkerConstant.SP_DOWNLOAD_SET_INSTALL + mUserid, isChecked);
		}
	};

	// SwitchView.OnCheckedChangeListener netChangeListener = new
	// SwitchView.OnCheckedChangeListener() {
	// @Override
	// public void onCheckedChanged(boolean isChecked) {
	// // TODO Auto-generated method stub
	// apu.saveBoolean(PangkerConstant.SP_UPLOAD_SET_AUTO_TE + mUserid,
	// isChecked);
	// }
	// };

}
