package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONArray;
import mobile.json.JSONException;
import mobile.json.JSONObject;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.IGrantDao;
import com.wachoo.pangker.db.impl.GrantDaoImpl;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.Grant;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CallMeSetResult;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PasswordDialog;

public class RootGroupActivity extends CommonPopActivity implements IUICallBackInterface {
	private String TAG = "RootGroupActivity";// log tag

	private TextView title;

	RadioGroup friendGroup;// 好友
	RadioButton friendAll;
	RadioButton friendAny;
	RadioButton friendNo;

	RadioGroup attGroup;// 关注人
	RadioButton attAll;
	RadioButton attAny;
	RadioButton attNo;

	private CheckBox cb_fans;

	private Button submit;
	private Button cancel;

	private int CLICK_CODE = 0;// -1 表示资源权限设置

	private PangkerApplication application;// application 旁客应用
	private List<ContactGroup> friend;
	private List<ContactGroup> attention;

	private String[] friend_items;
	private boolean[] friend_flag;
	String fgroup = "\"12\":[],";
	int fcount = 0;

	private String[] attention_items;
	private boolean[] attention_flag;
	String agroup = "\"14\":[],";
	int acount = 0;

	private String password;

	AlertDialog.Builder builder;
	private PasswordDialog passwordDialog;
	String userId;

	IGrantDao grantDao;
	Grant grant;
	int value;
	String grant_gids = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rootgroup);
		init();
		initView();
		initData();
		initListenner();
	}

	private void init() {
		// TODO Auto-generated method stub
		CLICK_CODE = getIntent().getExtras().getInt("clickcode");
		value = getIntent().getExtras().getInt("value");
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		grantDao = new GrantDaoImpl(this);
	}

	/**
	 * 初始化控件
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:30
	 */
	private void initView() {
		title = (TextView) findViewById(R.id.mmtitle);

		friendGroup = (RadioGroup) findViewById(R.id.friendgroup);
		friendAll = (RadioButton) findViewById(R.id.friendall);
		friendAny = (RadioButton) findViewById(R.id.friendany);
		friendNo = (RadioButton) findViewById(R.id.friendno);

		attGroup = (RadioGroup) findViewById(R.id.attgroup);
		attAll = (RadioButton) findViewById(R.id.attall);
		attAny = (RadioButton) findViewById(R.id.attany);
		attNo = (RadioButton) findViewById(R.id.attno);

		cb_fans = (CheckBox) findViewById(R.id.rootgroup_fans);

		submit = (Button) findViewById(R.id.iv_more);
		submit.setBackgroundResource(R.drawable.btn_default_selector);
		submit.setText(R.string.confrim);
		cancel = (Button) findViewById(R.id.btn_back);
	}

	private void showPasswordDialog() {
		// TODO Auto-generated method stub
		passwordDialog = new PasswordDialog(this, R.style.MyDialog);
		passwordDialog.setResultCallback(rMsgCallback);
		passwordDialog.showDialog();
		passwordDialog.hodeBtnCancel();
	}
	
	MessageTipDialog.DialogMsgCallback rMsgCallback = new MessageTipDialog.DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if (passwordDialog.checkPass() && isSubmit) {
				password = passwordDialog.getPassword();
			}
		}
	};

	/**
	 * 初始化数据
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:41
	 */
	private void initData() {
		grant = grantDao.getGrantValue(application.getMyUserID(), String.valueOf(CLICK_CODE));
		if (CLICK_CODE == 0) {
			title.setText(getResources().getString(R.string.root_call_set));
		} else if (CLICK_CODE == 1) {
			title.setText(getResources().getString(R.string.root_move_set));
		} else if (CLICK_CODE == 3) {
			title.setText(getResources().getString(R.string.root_location_set));
		} else if (CLICK_CODE == -1) {
			title.setText(getResources().getString(R.string.root_dir_set));
		}
		friend = application.getFriendGroup();
		friend_items = new String[friend.size()];
		friend_flag = new boolean[friend.size()];

		attention = application.getAttentGroup();
		attention_items = new String[attention.size()];
		attention_flag = new boolean[attention.size()];

		for (int i = 0; i < friend.size(); i++) {
			friend_items[i] = friend.get(i).getName();
		}
		for (int i = 0; i < attention.size(); i++) {
			attention_items[i] = attention.get(i).getName();
		}
		friendNo.setChecked(true);
		attNo.setChecked(true);
		cb_fans.setChecked(false);
		
		initGrant();
	}
	
	private void initGrant() {
		// TODO Auto-generated method stub
		
		JSONObject pjson;
		try {
			if (grant==null) {
				List<Grant> grants = new ArrayList<Grant>();
				Grant g_call = new Grant(userId, String.valueOf(CLICK_CODE), 1);
				grants.add(g_call);
				grantDao.saveGrants(grants);
				logger.warn(TAG + "grantDao.getGrantValue....return null");
			}else if (grant.getGrantGId() != null) {
				pjson = new JSONObject(grant.getGrantGId());
				for (Iterator iter = pjson.keys(); iter.hasNext();) {
					// 获取key值
					String key = iter.next().toString();
					// 我的好友、我的关注人类型特殊处理
					if (Integer.valueOf(key) == 12) {
						JSONArray parr = pjson.optJSONArray(key);
						if (parr == null || parr.length() == 0) {
							friendAll.setChecked(true);
						} else {
							if (friend.size() > 0) {
								friendAny.setChecked(true);
								for (int i = 0; i < friend.size(); i++) {
									for (int k = 0; k < parr.length(); k++) {
										if (friend.get(i).getGid().equals(parr.get(k).toString())) {
											friend_flag[i] = true;
										}
									}
								}
							} else {
								friendNo.setChecked(true);
							}
						}
					} else if (Integer.valueOf(key) == 14) {
						JSONArray parr = pjson.optJSONArray(key);
						if (parr == null || parr.length() == 0) {
							attAll.setChecked(true);
						} else {
							if (attention.size() > 0) {
								attAny.setChecked(true);
								for (int i = 0; i < attention.size(); i++) {
									for (int k = 0; k < parr.length(); k++) {
										if (attention.get(i).getGid().equals(parr.get(k).toString())) {
											attention_flag[i] = true;
										}
									}
								}
							} else {
								attNo.setChecked(true);
							}
						}
					} else if (Integer.valueOf(key) == 15) {
						cb_fans.setChecked(true);
					}
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 初始化监听
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:52
	 */
	private void initListenner() {
		submit.setOnClickListener(listener);
		cancel.setOnClickListener(listener);

		friendAny.setOnClickListener(listener);
		attAny.setOnClickListener(listener);
	}

	public void buildFriendGroup() {
		// TODO Auto-generated method stub
		if (friend.size() < 1) {
//			friendAll.setChecked(true);
			showToast("没有好友分组!");
		} else {
			builder = new AlertDialog.Builder(RootGroupActivity.this);
			builder.setTitle("我的好友组");
			builder.setMultiChoiceItems(friend_items, friend_flag,
					new DialogInterface.OnMultiChoiceClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which,
								boolean isChecked) {
							// TODO Auto-generated method
							// stub
							friend_flag[which] = isChecked;
						}
					});
			builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
				@Override
				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_BACK) {
						friendAll.setChecked(true);
					}
					return false;
				}
			});
			builder.setPositiveButton(R.string.rootgroup_ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							fcount = 0;
							fgroup = "\"12\":[";
							for (int i = 0; i < friend_flag.length; i++) {
								if (friend_flag[i]) {
									fcount++;
									fgroup += (friend.get(i).getGid() + ",");
								}
							}
							if (fcount == 0) {
								fgroup = "\"12\":[],";
//								friendNo.setChecked(true);
//							} else if (fcount == friend.size()) {
//								fgroup = "\"12\":[],";
							} else {
								fgroup = fgroup.substring(0,
										fgroup.length() - 1);
								fgroup += ("],");
							}
						};
					});
			builder.create();
			builder.show();
		}
	}

	public void buildAttGroup() {
		// TODO Auto-generated method stub
		if (attention.size() < 1) {
			attAll.setChecked(true);
			showToast("没有关注人分组!");
		} else {
			builder = new AlertDialog.Builder(RootGroupActivity.this);
			builder.setTitle("我的关注人组");
			builder.setMultiChoiceItems(attention_items, attention_flag,
					new DialogInterface.OnMultiChoiceClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which,
								boolean isChecked) {
							// TODO Auto-generated method
							// stub
							attention_flag[which] = isChecked;
						}
					});
			builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
				@Override
				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_BACK) {
						attAll.setChecked(true);
					}
					return false;
				}
			});
			builder.setPositiveButton(R.string.rootgroup_ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							acount = 0;
							agroup = "\"14\":[";
							for (int i = 0; i < attention_flag.length; i++) {
								if (attention_flag[i]) {
									acount++;
									agroup += (attention.get(i).getGid() + ",");
								}
							}
							if (acount == 0) {
								agroup = "\"14\":[],";
//								attNo.setChecked(true);
//							} else if (acount == attention.size()) {
//								agroup = "\"14\":[],";
							} else {
								agroup = agroup.substring(0,
										agroup.length() - 1);
								agroup += ("],");
							}
						};
					});
			builder.create();
			builder.show();
		}
	}

	public String getGrant() {
		String grantinfo = "{";
		// 好友
		if (friendAll.isChecked()) {
			grantinfo += "\"12\":[],";
		} else if (friendAny.isChecked()) {
			grantinfo += fgroup;
		}
		// 关注人
		if (attAll.isChecked()) {
			grantinfo += "\"14\":[],";
		} else if (attAny.isChecked()) {
			grantinfo += agroup;
		}
		// 粉丝
		if (cb_fans.isChecked()) {
			grantinfo += ("\"15\":[]");
		}
		if (grantinfo.charAt(grantinfo.length() - 1) == ',') {
			grantinfo = grantinfo.substring(0, grantinfo.length() - 1);
		}
		grantinfo += "}";
		return grantinfo;
	}

	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == submit) {
				rootGroup();
			} else if (v == cancel) {
				backIntent(11);
			} else if (v == attAny) {
				buildAttGroup();
			} else if (v == friendAny) {
				buildFriendGroup();
			}
		}
	};

	private void rootGroup() {
		// TODO Auto-generated method stub
		String grantinfo = getGrant();
		if (grantinfo.length() > 3) {
			grant_gids = grantinfo;
			if (CLICK_CODE == -1 || CLICK_CODE == 4) {
				rootDirGroup(grantinfo);
			} else if (CLICK_CODE == 0) {
				rootCallMeGroup(grantinfo);
			} else if (CLICK_CODE == 1) {
				rootDriftGroup(grantinfo);
			} else if (CLICK_CODE == 3) {
				rootLocationGroup(grantinfo);
			}
		} else {
			showToast(R.string.rootgroup_nochoose);
		}
	}

	/**
	 * 返回RootActivity
	 * 
	 * @author wubo
	 * @createtime 2012-2-17 上午09:46:27
	 */
	private void backIntent(int backCode) {
		setResult(backCode);
		finish();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		backIntent(11);
		super.onBackPressed();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			backIntent(11);
		}
		return super.onKeyDown(keyCode, event);
	}

	private void rootLocationGroup(String grantinfo) {
		// TODO Auto-generated method stub
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("type", "3"));
		paras.add(new Parameter("grantinfo", grantinfo));
		serverMana.supportRequest(Configuration.getLocationQuerySet(), paras,
				true, "正在处理...", CLICK_CODE);
	}

	private void rootDriftGroup(String grantinfo) {
		// TODO Auto-generated method stub
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("type", "3"));
		paras.add(new Parameter("grantinfo", grantinfo));
		serverMana.supportRequest(Configuration.getDriftSet(), paras, true,
				"正在处理...", CLICK_CODE);
	}

	private void rootCallMeGroup(String grantinfo) {
		// TODO Auto-generated method stub
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		paras.add(new Parameter("type", "3"));
		paras.add(new Parameter("grantinfo", grantinfo));
		serverMana.supportRequest(Configuration.getCallMeSet(), paras, true,
				"正在处理...", CLICK_CODE);
	}

	private void rootDirGroup(String grantinfo) {
		// TODO Auto-generated method stub
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		String dirid = getIntent().getStringExtra("dirid");
		paras.add(new Parameter("dirid", dirid));
		paras.add(new Parameter("grantinfo", grantinfo));
		if (CLICK_CODE == 4) {
			paras.add(new Parameter("password", password));
			paras.add(new Parameter("type", "4"));
		} else {
			paras.add(new Parameter("type", "1"));
		}
		serverMana.supportRequest(Configuration.getResInfoQuerySet(), paras, true, "正在处理...", -1);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			return;
		}

		if (caseKey == 0 || caseKey == -1 || caseKey == 3) {
//			if (caseKey == 0 || caseKey == 1 || caseKey == 3) {
			CallMeSetResult result = JSONUtil.fromJson(response.toString(),
					CallMeSetResult.class);
			if (result != null && result.errorCode == BaseResult.SUCCESS) {
				showToast(result.getErrorMessage());
				grantDao.updateGrantGid(application.getMyUserID(), String
						.valueOf(CLICK_CODE), value, grant_gids);
				backIntent(10);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				} else {
					showToast("处理失败!");
				}
				backIntent(11);
			}
		}

		if (caseKey == 1) {
//			if (caseKey == -1) {
			BaseResult result = JSONUtil.fromJson(response.toString(),
					BaseResult.class);
			if (result != null && result.errorCode == BaseResult.SUCCESS) {
				showToast(result.getErrorMessage());
				grantDao.updateGrantGid(application.getMyUserID(), String
						.valueOf(CLICK_CODE), value, grant_gids);
				backIntent(10);
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				} else {
					showToast("处理失败!");
				}
				backIntent(11);
			}
		}
	}
}
