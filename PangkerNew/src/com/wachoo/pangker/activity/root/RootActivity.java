package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.IGrantDao;
import com.wachoo.pangker.db.impl.GrantDaoImpl;
import com.wachoo.pangker.entity.Grant;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.CallMeSetResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @TODO 设置界面
 * 
 * @author wubo
 * 
 */
public class RootActivity extends CommonPopActivity implements IUICallBackInterface {

	private LinearLayout ly_call;
	private LinearLayout ly_location;

	private TextView tv_call;
	private TextView tv_location;

	private int ROOTGROUP_CODE = 10;
	private int ROOTWHO_CODE = 11;

	private int value = 0;

	AlertDialog.Builder builder;
	private String userId;

	IGrantDao grantDao;
	List<Grant> grants;

	String[] calls = null;// 拨号权限表
	String[] call_ids = null;// 拨号权限表id
	String[] locations = null;// 漂移权限表
	String[] location_ids = null;// 漂移权限表id

	String[] enters = null;// 群组进入权限表
	String[] enter_ids = null;// 群组进入权限表id
	private PangkerApplication application;

	// >>>>>>>>>选择时使用
	private PopMenuDialog menuDialog;

	// >>>>>>>电话
	ActionItem[] callItems = null;
	ActionItem[] locationItems = null;

	private int CLICK_CODE = 0;// 0呼叫1漂移2友踪3查看我的位置信息>>>>>>>lb -1:资源查看权限

	private int callSelected = 1;
	private int locationSelected = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.root);
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		initView();
		initData();
	}

	/**
	 * 初始化控件
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:30
	 */
	private void initView() {
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RootActivity.this.finish();
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("安全隐私设置");
		findViewById(R.id.iv_more).setVisibility(View.GONE);

		ly_call = (LinearLayout) findViewById(R.id.root_ly_call);
		tv_call = (TextView) findViewById(R.id.root_tv_call);

		ly_location = (LinearLayout) findViewById(R.id.root_ly_location);
		tv_location = (TextView) findViewById(R.id.root_tv_location);

	}

	/**
	 * 初始化数据
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:41
	 */
	private void initData() {

		// >>>>>>>>>获取呼叫权限
		calls = getResources().getStringArray(R.array.root_call);
		call_ids = getResources().getStringArray(R.array.root_call_id);

		// >>>>>>>>>>>获取漂移权限
		locations = getResources().getStringArray(R.array.root_move);
		location_ids = getResources().getStringArray(R.array.root_move_id);

		enters = getResources().getStringArray(R.array.root_enter);
		enter_ids = getResources().getStringArray(R.array.root_enter_id);

		grantDao = new GrantDaoImpl(this);
		grants = new ArrayList<Grant>();
		grants = grantDao.getGrantValues(userId);
		if (grants != null && grants.size() > 0) {
			for (int i = 0; i < grants.size(); i++) {
				if (grants.get(i).getGrantName().equals("0")) {
					callSelected = grants.get(i).getGrantValue();
					tv_call.setText(calls[callSelected]);
				} else if (grants.get(i).getGrantName().equals("3")) {
					locationSelected = grants.get(i).getGrantValue();
					tv_location.setText(locations[locationSelected]);
				}
			}
		} else {
			grants = new ArrayList<Grant>();
			Grant g_call = new Grant(userId, "0", 1);
			Grant g_move = new Grant(userId, "1", 1);
			Grant g_location = new Grant(userId, "3", 1);
			grants.add(g_call);
			grants.add(g_move);

			grants.add(g_location);
			grantDao.saveGrants(grants);
			tv_call.setText(calls[1]);
			tv_location.setText(locations[1]);
		}

		callItems = new ActionItem[call_ids.length];
		for (int i = 0; i < call_ids.length; i++) {
			ActionItem actionItem = new ActionItem(Util.String2Integer(call_ids[i]), calls[i], getResources()
					.getDrawable(R.drawable.icon32_radio));
			callItems[i] = actionItem;
		}
		callItems[callSelected].setIcon(getResources().getDrawable(R.drawable.icon32_radio_p));

		locationItems = new ActionItem[location_ids.length];
		for (int i = 0; i < call_ids.length; i++) {
			ActionItem actionItem = new ActionItem(Util.String2Integer(location_ids[i]), locations[i],
					getResources().getDrawable(R.drawable.icon32_radio));
			locationItems[i] = actionItem;
		}
		locationItems[locationSelected].setIcon(getResources().getDrawable(R.drawable.icon32_radio_p));
		ly_call.setOnClickListener(listener);
		ly_location.setOnClickListener(listener);

	}

	private void initMenuDialog() {
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
		}
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			if (CLICK_CODE == 0) {
				// >>>>>>>>设置当前选中项目
				tv_call.setText(calls[actionId - 1]);

				// >>>>>>>>设置图标
				for (ActionItem item : callItems) {
					item.setIcon(getResources().getDrawable(R.drawable.icon32_radio));
				}
				callItems[actionId - 1].setIcon(getResources().getDrawable(R.drawable.icon32_radio_p));

				// >>>>>>>>保存结果
				setAllowOrPass(actionId - 1);
			} else if (CLICK_CODE == 3) {

				// >>>>>>>>设置当前选中项目
				tv_location.setText(locations[actionId - 1]);
				// >>>>>>>>设置图标
				for (ActionItem item : locationItems) {
					item.setIcon(getResources().getDrawable(R.drawable.icon32_radio));
				}
				locationItems[actionId - 1].setIcon(getResources().getDrawable(R.drawable.icon32_radio_p));

				// >>>>>>>>保存结果
				setAllowOrPass(actionId - 1);
			}
			menuDialog.dismiss();
		}
	};

	/**
	 * 监听
	 */
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			initMenuDialog();
			if (v == ly_call) {// 呼叫权限
				CLICK_CODE = 0;
				menuDialog.setTitle(R.string.root_call_set);
				menuDialog.setMenus(callItems);
			}

			else if (v == ly_location) {
				CLICK_CODE = 3;
				menuDialog.setTitle(R.string.root_location_set);
				menuDialog.setMenus(locationItems);
			}
			menuDialog.show();
		}
	};

	public void setAllowOrPass(int type) {
		ServerSupportManager serverMana = new ServerSupportManager(this, RootActivity.this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		if (CLICK_CODE == 0) {
			paras.add(new Parameter("type", call_ids[type]));
			serverMana.supportRequest(Configuration.getCallMeSet(), paras);
		} else if (CLICK_CODE == 3) {
			paras.add(new Parameter("type", location_ids[type]));
			serverMana.supportRequest(Configuration.getLocationQuerySet(), paras);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ROOTGROUP_CODE) {
			if (resultCode == 10) {// 10表示设置成功,11表示未设置
				if (CLICK_CODE == 0) {
					tv_call.setText(calls[2]);
				} else if (CLICK_CODE == 3) {
					tv_location.setText(locations[2]);
				}
			}
		} else if (requestCode == ROOTWHO_CODE) {
			if (resultCode == 10) {// 0表示设置成功,1表示未设置
				// grantDao.updateGrant(userId, String.valueOf(CLICK_CODE),
				// value);
				if (CLICK_CODE == 0) {
					tv_call.setText(calls[3]);
				} else if (CLICK_CODE == 3) {
					tv_location.setText(locations[3]);
				}
			}
		}
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		if (caseKey == 999) {
			CallMeSetResult result = JSONUtil.fromJson(supportResponse.toString(), CallMeSetResult.class);
			if (result != null && result.errorCode != 999) {
				showToast(result.getErrorMessage());
				if (result.getErrorCode() == 1) {
					grantDao.updateGrant(userId, String.valueOf(CLICK_CODE), value);
				}
			} else {
				showToast(R.string.return_value_999);
			}
		}
	}
}
