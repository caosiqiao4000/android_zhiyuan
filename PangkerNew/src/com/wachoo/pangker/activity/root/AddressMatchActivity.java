package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.ILocalContactsDao;
import com.wachoo.pangker.db.impl.LocalContactsDaoImpl;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.PhoneBookResult;
import com.wachoo.pangker.util.SharedPreferencesUtil;

// create by wangxin  通讯录匹配功能
public class AddressMatchActivity extends CommonPopActivity implements IUICallBackInterface {

	private Button btn_AddressBind;

	private String userid;
	private String phones;
	private boolean isBind = false;// 绑定标记 false ： unbind ； true ：bind
	private static final int ADDRESS_BIND = 1;
	private static final int ADDRESS_UNBIND = 2;

	private ILocalContactsDao contactsDao;
	private PangkerApplication application;
	private ServerSupportManager supportManager;
	private SharedPreferencesUtil preferencesUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.address_match);

		userid = ((PangkerApplication) getApplication()).getMyUserID();
		contactsDao = new LocalContactsDaoImpl(this);
		application = (PangkerApplication) getApplication();
		application.getMySelf();
		supportManager = new ServerSupportManager(this, this);
		preferencesUtil = new SharedPreferencesUtil(this);
		isBind = preferencesUtil.getBoolean(PangkerConstant.IS_BIND_KEY, false);

		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AddressMatchActivity.this.finish();
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText(R.string.title_address_match);
		findViewById(R.id.iv_more).setVisibility(View.GONE);

		btn_AddressBind = (Button) findViewById(R.id.btn_address_bind);
		if (isBind)
			btn_AddressBind.setText(R.string.btn_unbind_text);
		else
			btn_AddressBind.setText(R.string.btn_bind_text);
		btn_AddressBind.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				List<Parameter> paras = new ArrayList<Parameter>();
				if (!isBind) {
					paras.add(new Parameter("appid", "1"));
					paras.add(new Parameter("uid", userid));
					phones = contactsDao.getAllPhoneNumbers();
					paras.add(new Parameter("phones", phones.toString()));
					if (phones == null || phones.length() == 0) {
						showToast(R.string.phonenumber_unbind_noneed_message);
						return;
					}
					supportManager.supportRequest(Configuration.getAddressBookBinding(), paras, true,
							getResources().getString(R.string.phonenumber_bind_message), ADDRESS_BIND);
				} else {
					paras.add(new Parameter("appid", "1"));
					paras.add(new Parameter("uid", userid));
					supportManager.supportRequest(Configuration.getAddressBookUNBinding(), paras, true,
							getResources().getString(R.string.phonenumber_unbind_message), ADDRESS_UNBIND);
				}
			}
		});

	}

	// 解绑操作
	private void unBindResult(PhoneBookResult bookResult) {
		// TODO Auto-generated method stub
		preferencesUtil.saveBoolean(PangkerConstant.IS_BIND_KEY, false);
		contactsDao.deletePUsers("");
		this.isBind = false;

		showToast(R.string.phonenumber_unbind_sucess_message);
		btn_AddressBind.setText(R.string.btn_bind_text);
		application.setLocalContacts(contactsDao.initLocalAddressBooks());
		if (application.getAddressBookBind() != null)
			application.getAddressBookBind().refreshAddressBook();
	}

	// 绑定操作
	private void bindResult(PhoneBookResult bookResult) {
		// TODO Auto-generated method stub
		contactsDao.notifyPUsers(bookResult.getPks());
		preferencesUtil.saveBoolean(PangkerConstant.IS_BIND_KEY, true);
		this.isBind = true;

		showToast(R.string.phonenumber_bind_sucess_message);
		btn_AddressBind.setText(R.string.btn_unbind_text);
		application.setLocalContacts(contactsDao.initLocalAddressBooks());
		if (application.getAddressBookBind() != null)
			application.getAddressBookBind().refreshAddressBook();
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;
		PhoneBookResult bookResult = JSONUtil.fromJson(supportResponse.toString(), PhoneBookResult.class);
		if (bookResult == null || bookResult.getErrorCode() != BaseResult.SUCCESS) {
			showToast(R.string.operate_fail);
			return;
		}
		switch (caseKey) {
		case ADDRESS_BIND:
			bindResult(bookResult);
			break;
		case ADDRESS_UNBIND:
			unBindResult(bookResult);
			break;
		default:
			break;
		}
	}
}
