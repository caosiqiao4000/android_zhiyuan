package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import mobile.encrypt.EncryptUtils;
import mobile.http.Parameter;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.util.Util;

public class PangkerNoSetting extends CommonPopActivity {
	protected EditText mPangkerNo;
	protected EditText mPassword;
	protected EditText mPasswordConfim;
	protected Button btn_submit;
	protected String mUserID;
	protected PangkerApplication mApplication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.mApplication = (PangkerApplication) getApplication();
		this.mUserID = this.mApplication.getMyUserID();
	}

	protected boolean checkThree(String str) {
		char a = str.charAt(0);
		for (int i = 1; i < str.length(); i++) {
			if (a != str.charAt(i)) {
				return false;
			}
		}
		return true;
	}

	protected boolean checkAccount() {
		String account = mPangkerNo.getEditableText().toString();
		if (Util.isEmpty(account)) {
			showToast("请输入旁客号码!");
			return false;
		}
		if (account.length() < 8) {
			showToast("确认旁客号码不低于8位!");
			return false;
		}
		for (int i = 0; i < account.length() - 2; i++) {
			if (checkThree(account.substring(i, i + 3))) {
				showToast("为了确保号码安全，请不要连续输入3位相同的数字!");
				return false;
			}
		}
		return true;
	}

	protected boolean checkPwd() {
		String pwd1 = mPassword.getEditableText().toString();
		String pwd2 = mPasswordConfim.getEditableText().toString();
		if (!pwd1.equals(pwd2)) {
			showToast("请确认两次输入的密码一致!");
			return false;
		}
		if (!pwd1.matches(PangkerConstant.PANGKER_REGEX)) {
			showToast("请输入6-20位密码,同时包含数字和字母!");
			return false;
		}
		return true;
	}

	protected void setFingerAccount(IUICallBackInterface iuiCallBackInterface) {
		// TODO Auto-generated method stub
		if (checkAccount() && checkPwd()) {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("userid", this.mUserID));
			ServerSupportManager serverMana = new ServerSupportManager(this, iuiCallBackInterface);

			String account = mPangkerNo.getEditableText().toString().trim();
			String pwd = mPassword.getEditableText().toString().trim();

			try {
				paras.add(new Parameter("imAccount", account));
				paras.add(new Parameter("imPassword", EncryptUtils.encrypt(pwd)));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.debug(e);
			}
			String mess = getResourcesMessage(R.string.please_wait);
			serverMana.supportRequest(Configuration.getSetIMAccount(), paras, true, mess);
		}
	}

}
