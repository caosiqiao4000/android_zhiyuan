package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import mobile.encrypt.EncryptUtils;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;

/**
 * 安全密码设置
 * 
 * @author zhengjy
 * 
 *         2012-08-30
 * 
 */
public class PassSafetySetActivity extends CommonPopActivity {

	private PangkerApplication application;
	private TextView txtAccount;
	private EditText etPass0;
	private EditText etPass1;
	private EditText etPass2;
	private Button btnSubmit;
	private Button btnGetpassword;
	private String mUserid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.pass_safety_set);
		application = (PangkerApplication) getApplication();
		mUserid = application.getMyUserID();

		initView();
		// CheckSecret();
	}

	private void initView() {
		txtAccount = (TextView) findViewById(R.id.txt_account);
		txtAccount.setText(application.getImAccount());

		etPass0 = (EditText) findViewById(R.id.et_pass0);
		etPass1 = (EditText) findViewById(R.id.et_pass1);
		etPass2 = (EditText) findViewById(R.id.et_pass2);
		btnSubmit = (Button) findViewById(R.id.btn_submit);
		btnGetpassword = (Button) findViewById(R.id.btn_getpassword);
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				passManager();
			}
		});

		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 隐藏输入法
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
						PassSafetySetActivity.this.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
				finish();
			}
		});
		btnGetpassword.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PassSafetySetActivity.this, SecurityCenterActivity.class);
				intent.putExtra("imsi", Util.getImsiNum(PassSafetySetActivity.this));
				intent.putExtra("userid", application.getMyUserID());
				startActivity(intent);
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("旁客号码密码设置");
		findViewById(R.id.iv_more).setVisibility(View.GONE);
	}

	/**
	 * 密码设置
	 * 
	 * 
	 * 接口：Setsecret.json
	 */
	private void passManager() {
		String pass0 = etPass0.getText().toString().trim();
		if (Util.isEmpty(pass0)) {
			showToast("请输入原始密码!");
			return;
		}
		String pass1 = etPass1.getText().toString().trim();
		String pass2 = etPass2.getText().toString().trim();
		if (!pass1.matches(PangkerConstant.PANGKER_REGEX)) {
			showToast("请输入6-20位的数字和字母(同时包含)!");
			return;
		}
		if (Util.isEmpty(pass1)) {
			showToast("设置密码不能为空!");
			return;
		}
		if (Util.isEmpty(pass2)) {
			showToast("请确认密码!");
			return;
		}
		if (!pass1.equals(pass2)) {
			showToast("两次输入密码不匹配!");
			return;
		}

		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {

			@Override
			public void uiCallBack(Object supportResponse, int caseKey) {
				// TODO Auto-generated method stub
				if (!HttpResponseStatus(supportResponse))
					return;
				BaseResult baseResult = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
				if (baseResult != null && baseResult.errorCode == BaseResult.SUCCESS) {
					showToast(baseResult.getErrorMessage());
					finish();
				} else if (baseResult != null && baseResult.errorCode == BaseResult.FAILED) {
					showToast(baseResult.getErrorMessage());
				} else
					showToast(R.string.to_server_fail);
			}
		});
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", mUserid));
		String account = application.getImAccount();
		String oldPwd = etPass0.getEditableText().toString().trim();
		String newPwd = etPass1.getEditableText().toString().trim();
		try {
			paras.add(new Parameter("imAccount", account));
			paras.add(new Parameter("oldimPassword", EncryptUtils.encrypt(oldPwd)));
			paras.add(new Parameter("newimPassword", EncryptUtils.encrypt(newPwd)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getUpdateIMAccountPassword(), paras, true, mess);
	}

}
