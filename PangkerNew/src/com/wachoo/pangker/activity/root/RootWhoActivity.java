package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.RootWhoAdapter;
import com.wachoo.pangker.adapter.RootWhoAdapter.Holder;
import com.wachoo.pangker.db.IGrantDao;
import com.wachoo.pangker.db.impl.GrantDaoImpl;
import com.wachoo.pangker.entity.Grant;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;

public class RootWhoActivity extends CommonPopActivity implements
		IUICallBackInterface {

	private String TAG = "RootWhoActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();

	private TextView title;
	private ListView lv_list;
	private Button cancel;
	private Button submit;
	private TextView tv_count;
	private RootWhoAdapter adapter;
	private List<UserItem> cgList;
	private List<String> ids;
	int count = 0;
	String members = "";
	private Bundle bundle;
	private int CLICK_CODE = 0;
	PangkerApplication application;

	String uid;
	int backCode = 10;// 返回值,设置显示信息10成功,11失败
	private String dirid;

	IGrantDao grantDao;
	Grant grant;
	int value;
	String grant_uids = "";
	String[] uids = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.rootwho);
		bundle = getIntent().getExtras();
		CLICK_CODE = bundle.getInt("clickcode");
		value = bundle.getInt("value");
		grantDao = new GrantDaoImpl(this);
		if (CLICK_CODE == -1) {
			dirid = bundle.getString("dirid");
		}
		application = (PangkerApplication) getApplication();
		initView();
		initData();
		initListenner();
	}

	/**
	 * 初始化控件
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:30
	 */
	private void initView() {
		title = (TextView) findViewById(R.id.mf_title);

		lv_list = (ListView) findViewById(R.id.rootwho_list);

		tv_count = (TextView) findViewById(R.id.count);

		submit = (Button) findViewById(R.id.submit);
		cancel = (Button) findViewById(R.id.cancel);
	}

	/**
	 * 初始化数据
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:41
	 */
	private void initData() {
		uid = ((PangkerApplication) this.getApplicationContext()).getMySelf()
				.getUserId();
		grant = grantDao.getGrantValue(application.getMyUserID(), String
				.valueOf(CLICK_CODE));
		if (CLICK_CODE == 0) {
			title.setText(getResources().getString(R.string.root_call_set));
		} else if (CLICK_CODE == 3) {
			title.setText(getResources().getString(R.string.root_location_set));
		} else if (CLICK_CODE == 2) {
			title
					.setText(getResources().getString(
							R.string.root_checkinfo_set));
		} else if (CLICK_CODE == -1) {
			title.setText(getResources().getString(R.string.root_dir_set));
		}
		ids = new ArrayList<String>();
		cgList = new ArrayList<UserItem>();
		List<UserItem> cgFriend = application.getFriendList();
		if (cgFriend != null && cgFriend.size() > 0) {
			cgList.addAll(cgFriend);
		}
		int cou = 0;
		if (cgList != null && cgList.size() > 0) {
			for (int i = cgList.size() - 1; i >= 0;) {
				cou = 0;
				String cgUid = cgList.get(i).getUserId();
				for (int j = i - 1; j >= 0; j--) {
					if (cgUid.equals(cgList.get(j).getUserId())) {
						cgList.remove(j);
						cou++;
					}
				}
				i = i - cou - 1;
			}
		}
		tv_count.setText("0/200");
		lv_list.setItemsCanFocus(false);
		lv_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		
		
		if(grant != null){
			grant_uids = grant.getGrantUid();
			if (grant_uids != null && grant_uids.length() > 2) {
				grant_uids = grant_uids.substring(1, grant_uids.length() - 1);
				uids = grant_uids.split(",");
				count = uids.length;
			}
		}else {
			logger.warn(TAG+ "未找到相关权限数据...");
			uids = new String[128];
		}
		adapter = new RootWhoAdapter(this, cgList, uids);
		lv_list.setAdapter(adapter);
	}

	/**
	 * 初始化监听
	 * 
	 * @author wubo
	 * @createtime 2012-2-9 上午11:07:52
	 */
	private void initListenner() {

		lv_list.setOnItemClickListener(itemClickListener);
		submit.setOnClickListener(listener);
		cancel.setOnClickListener(listener);
	}

	/**
	 * 按钮点击事件
	 */
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == cancel) {
				backCode = 11;
				backIntent();
			} else if (v == submit) {
				for(int i=0;i<cgList.size();i++){
					if(adapter.isSelected.get(i)){
						ids.add(cgList.get(i).getUserId());
					}
				}
				if (ids.size() < 1) {
					showToast(R.string.rootwho_null);
				} else if (ids.size() > 200) {
					showToast(R.string.rootwho_spill);
				} else {
					members = "[";
					for (int i = 0; i < ids.size(); i++) {
						members += ids.get(i) + ",";
					}
					members = members.substring(0, members.length() - 1);
					members += "]";
					ServerSupportManager serverMana = new ServerSupportManager(
							RootWhoActivity.this, RootWhoActivity.this);
					List<Parameter> paras = new ArrayList<Parameter>();
					paras.add(new Parameter("uid", uid));
					if (CLICK_CODE == -1) {
						paras.add(new Parameter("type", "5"));
						paras.add(new Parameter("dirid", dirid));
					} else {
						paras.add(new Parameter("type", "4"));
					}
					paras.add(new Parameter("members", members));
					if (CLICK_CODE == 0) {
						serverMana.supportRequest(Configuration.getCallMeSet(),
								paras);
					} else if (CLICK_CODE == 1) {
						serverMana.supportRequest(Configuration.getDriftSet(),
								paras);
					} else if (CLICK_CODE == 3) {
						serverMana.supportRequest(Configuration
								.getLocationQuerySet(), paras);
					} else if (CLICK_CODE == -1) {
						serverMana.supportRequest(Configuration
								.getResInfoQuerySet(), paras, -1);
					}
				}
			}
		}
	};

	/**
	 * ListView点击事件
	 */
	OnItemClickListener itemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			Holder holder = (Holder) view.getTag();
			holder.check.toggle();
			adapter.isSelected.put(position, holder.check.isChecked());
			if (holder.check.isChecked()) {
				count++;
			} else {
				count--;
			}
			tv_count.setText(count + "/200");
		}
	};

	/**
	 * 返回RootActivity
	 * 
	 * @author wubo
	 * @createtime 2012-2-17 上午09:42:45
	 */
	private void backIntent() {
		Intent intent = new Intent();
		intent.setClass(RootWhoActivity.this, RootActivity.class);
		setResult(backCode, intent);
		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			backCode = 11;
			backIntent();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		if (caseKey == 999 || caseKey == -1) {
			if (supportResponse != null) {
				BaseResult result = JSONUtil.fromJson(supportResponse
						.toString(), BaseResult.class);
				if (result != null && result.errorCode != 999) {
					showToast(result.getErrorMessage());
					if (result.getErrorCode() == BaseResult.SUCCESS) {
						grantDao.updateGrantUid(uid,
								String.valueOf(CLICK_CODE), value, members);
						backCode = 10;
						backIntent();
					} else {
						backCode = 11;
						backIntent();
					}
				} else {
					backCode = 11;
					backIntent();
				}
			} else {
				backCode = 11;
				backIntent();
			}
		}
	}

}
