package com.wachoo.pangker.activity.root;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.SwitchView;
import com.wachoo.pangker.util.SharedPreferencesUtil;

public class LocateSettingActivity extends CommonPopActivity {
	/*
	 * 位置提交方式设置（手动；自动）
	 */
	private TextView tvAutoLocation;
	private LinearLayout lyAutoLocation;
	public static final String[] LOACTE_TYPES = new String[] { "自动定位", "手动定位" };
	private static final String[] WIFI_LOCATE_TYPES = new String[] { "自动提交WIFI" };

	private String mMyUserID;
	private AlertDialog.Builder builder;
	private SharedPreferencesUtil spUtil;

	/**
	 * WIFI邻居查询
	 */
	private LinearLayout lyWifiSwitch;// ly_wifi_switch
	private SwitchView mWifiSwitchView;
	private LinearLayout lyWifiSetting;
	private TextView mWifiTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_location_setting);
		spUtil = new SharedPreferencesUtil(this);
		mMyUserID = ((PangkerApplication) getApplication()).getMyUserID();

		/*
		 * 手机终端定位。定位方式设置
		 */
		// >>>>>>>>>位置提交方式设置
		lyAutoLocation = (LinearLayout) findViewById(R.id.ly_auto_location);
		lyAutoLocation.setOnClickListener(clickListener);

		// >>>>>>>>获取上次设置的方式
		boolean isAutoLocatie = spUtil.getBoolean(LOCATION_SERVICE + mMyUserID, true);
		tvAutoLocation = (TextView) findViewById(R.id.tv_location);
		int timeCount = spUtil.getInt(PangkerConstant.AUTO_LOCATE_TIME_KEY + mMyUserID, 10);
		tvAutoLocation.setText(isAutoLocatie ? LOACTE_TYPES[0] + ";每隔" + timeCount + "分钟" : LOACTE_TYPES[1]);

		/*
		 * WIFI相对定位定位相关设置
		 */
		lyWifiSwitch = (LinearLayout) findViewById(R.id.ly_wifi_switch);
		lyWifiSetting = (LinearLayout) findViewById(R.id.ly_wifi_setting);
		lyWifiSetting.setOnClickListener(clickListener);
		mWifiSwitchView = (SwitchView) findViewById(R.id.sv_wifi_switch);
		mWifiSwitchView.setChecked(spUtil.getBoolean(PangkerConstant.WIFI_LOCATE_KEY + mMyUserID, true));
		mWifiSwitchView.setOnCheckedChangeListener(new SwitchView.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(boolean isChecked) {
				// TODO Auto-generated method stub
				spUtil.saveBoolean(PangkerConstant.WIFI_LOCATE_KEY + mMyUserID, isChecked);
				if (isChecked) {
					// 默认10分钟自动定位
					spUtil.saveInt(PangkerConstant.WIFI_AUTO_LOCATE_TIME_KEY + mMyUserID, 10);
					// >>>>>>>通知默认10分钟自动定位设置
					Intent intent = new Intent(LocateSettingActivity.this, PangkerService.class);
					intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.AUTOWIFI_LACATE_INTENT);
					startService(intent);
				} else {
					// 关闭自动定位
					Intent intent = new Intent(LocateSettingActivity.this, PangkerService.class);
					intent.putExtra(PangkerConstant.SERVICE_INTENT,
							PangkerConstant.AUTOWIFI_LACATE_CLOSE_INTENT);
					startService(intent);
				}
			}
		});
		mWifiTextView = (TextView) findViewById(R.id.tv_wifi_setting);

		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("定位设置");
		findViewById(R.id.iv_more).setVisibility(View.GONE);
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// >>>>>>>终端定位
			if (v == lyAutoLocation) {
				builder = new AlertDialog.Builder(LocateSettingActivity.this);
				boolean isAuto = spUtil.getBoolean(LOCATION_SERVICE + mMyUserID, true);
				builder.setSingleChoiceItems(LOACTE_TYPES, isAuto ? 0 : 1,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								if (dialog != null) {
									dialog.dismiss();
								}
								if (which == 0) {
									setAutoLocationTime(0);
								} else if (which == 1) {
									tvAutoLocation.setText(LOACTE_TYPES[1]);
									spUtil.saveBoolean(LOCATION_SERVICE + mMyUserID, false);
									showToast("设置成功!");
								}

							}
						});
				builder.create();
				builder.show();
			}
			// >>>>>>>>>>wifi定位
			else if (v == lyWifiSetting) {
				builder = new AlertDialog.Builder(LocateSettingActivity.this);
				boolean isAuto = spUtil.getBoolean(PangkerConstant.WIFI_LOCATE_KEY + mMyUserID, false);
				builder.setSingleChoiceItems(WIFI_LOCATE_TYPES, 0, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (dialog != null) {
							dialog.dismiss();
						}
						if (which == 0) {
							setAutoLocationTime(1);
						}
						// else if (which == 1) {
						// mWifiTextView.setText(WIFI_LOCATE_TYPES[1]);
						// spUtil.saveBoolean(PangkerConstant.WIFI_LOCATE_KEY +
						// mMyUserID, true);
						// // >>>>>>>通知定位手动设置，需要追加代码
						// Intent intent = new
						// Intent(LocateSettingActivity.this,
						// PangkerService.class);
						// intent.putExtra(PangkerConstant.SERVICE_INTENT,
						// PangkerConstant.WIFI_LACATE_INTENT);
						// startService(intent);
						// showToast("设置成功!");
						// }

					}
				});
				builder.create();
				builder.show();
			}
		}
	};

	/**
	 * void TODO 设置自动定位时间间隔
	 */
	private void setAutoLocationTime(final int flag) {
		// 默认选择
		int which = -1;
		if (flag == 0) {
			// 如果是定位设置，默认10分钟自动定位
			which = spUtil.getInt(PangkerConstant.AUTO_LOCATE_WHICH_KEY + mMyUserID, 4);
		}
		String[] time_array = getResources().getStringArray(R.array.auto_location_time);
		builder = new AlertDialog.Builder(LocateSettingActivity.this).setTitle("请选择定位时间间隔");
		builder.setSingleChoiceItems(time_array, which, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (dialog != null) {
					dialog.dismiss();
				}

				int saveTime = 0;
				// 每隔一分钟（单位是分钟）
				if (which == 0) {
					saveTime = 1;

				}
				// 每隔两分钟
				else if (which == 1) {
					saveTime = 2;
				}
				if (which == 2) {
					saveTime = 3;
				} else if (which == 3) {
					saveTime = 5;
				}
				if (which == 4) {
					saveTime = 10;
				} else if (which == 5) {
					saveTime = 15;
				}
				if (which == 6) {
					saveTime = 20;
				} else if (which == 7) {
					saveTime = 30;
				}
				if (which == 8) {
					saveTime = 60;
				} else if (which == 9) {
					saveTime = 120;
				}
				// >>>>>>间隔时间
				String show = ";每隔" + saveTime + "分钟";
				// >>>>>>>>终端定位间隔时间
				if (which >= 0 && flag == 0) {
					spUtil.saveInt(PangkerConstant.AUTO_LOCATE_TIME_KEY + mMyUserID, saveTime);
					spUtil.saveInt(PangkerConstant.AUTO_LOCATE_WHICH_KEY + mMyUserID, which);
					tvAutoLocation.setText(LOACTE_TYPES[0] + show);
					spUtil.saveBoolean(LOCATION_SERVICE + mMyUserID, true);
					Intent intent = new Intent(LocateSettingActivity.this, PangkerService.class);
					intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.AUTOLOCATE_INTENT);
					startService(intent);
					showToast("设置成功!");
				}
				// >>>>>>>wifi提交位置信息
				else if (which >= 0 && flag == 1) {
					spUtil.saveInt(PangkerConstant.WIFI_AUTO_LOCATE_TIME_KEY + mMyUserID, saveTime);
					mWifiTextView.setText(WIFI_LOCATE_TYPES[0] + show);
					spUtil.saveBoolean(PangkerConstant.WIFI_LOCATE_KEY + mMyUserID, true);
					// >>>>>>>通知定位周期设置，需要追加代码
					Intent intent = new Intent(LocateSettingActivity.this, PangkerService.class);
					intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.AUTOWIFI_LACATE_INTENT);
					startService(intent);
				}

			}
		});
		builder.setNegativeButton(R.string.cancel, null);
		builder.create();
		builder.show();
	}
}
