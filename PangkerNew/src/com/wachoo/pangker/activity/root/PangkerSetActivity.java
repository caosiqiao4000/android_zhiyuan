package com.wachoo.pangker.activity.root;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.SwitchView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

public class PangkerSetActivity extends CommonPopActivity {

	private final int CALL_CODE = 0x13;// 更新旁克号码
	private final int FRINGER_CODE = 0x14;// 更新旁克号码

	private LinearLayout msgSetLayout1;
	private TextView txt_Smsset1;
	private LinearLayout msgSetLayout2;
	private TextView txt_Smsset2;

	private LinearLayout rootSetLayout;
	private LinearLayout downloadSetLayout;
	private LinearLayout macthSetLayout;
	private LinearLayout callSetLayout;
	private TextView tv_callphone;
	private LinearLayout resrecommendLayout;

	private SwitchView soundSwitchView;
	private SwitchView shockSwitchView;

	private PangkerApplication application;
	private String userId;
	private SharedPreferencesUtil mPreferencesUtil;
	private PopMenuDialog menuDialog;
	private String[] SMS_SET_SEFI_OFFLINE_TYPES;
	private String[] MSG_SET_SEFI_OFFLINE_TYPES;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pangkerset);
		init();

		initView();
		initData();
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		mPreferencesUtil = new SharedPreferencesUtil(this);
		SMS_SET_SEFI_OFFLINE_TYPES = getResources().getStringArray(R.array.sms_set_type);
		MSG_SET_SEFI_OFFLINE_TYPES = getResources().getStringArray(R.array.msg_set_type);
	}

	private void initView() {
		// TODO Auto-generated method stub
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PangkerSetActivity.this.finish();
			}
		});
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("设置");
		findViewById(R.id.iv_more).setVisibility(View.GONE);

		msgSetLayout1 = (LinearLayout) findViewById(R.id.ly_msg_offline);
		txt_Smsset1 = (TextView) findViewById(R.id.tv_msg_offline);
		txt_Smsset1.setText(SMS_SET_SEFI_OFFLINE_TYPES[mPreferencesUtil.getInt(
				PangkerConstant.SP_SMS_SET_OTHER_OFFLINE + userId, 0)]);
		msgSetLayout1.setOnClickListener(onClickListener);
		msgSetLayout2 = (LinearLayout) findViewById(R.id.ly_msg_set);
		txt_Smsset2 = (TextView) findViewById(R.id.tv_msg_set);
		txt_Smsset2.setText(MSG_SET_SEFI_OFFLINE_TYPES[mPreferencesUtil.getInt(
				PangkerConstant.SP_SMS_SET_SEFI_OFFLINE + userId, 0)]);
		msgSetLayout2.setOnClickListener(onClickListener);

		downloadSetLayout = (LinearLayout) findViewById(R.id.ly_download);
		downloadSetLayout.setOnClickListener(onClickListener);

		rootSetLayout = (LinearLayout) findViewById(R.id.ly_rootset);
		rootSetLayout.setOnClickListener(onClickListener);

		resrecommendLayout = (LinearLayout) findViewById(R.id.ly_resrecommend_set);
		resrecommendLayout.setOnClickListener(onClickListener);

		macthSetLayout = (LinearLayout) findViewById(R.id.ly_address);
		macthSetLayout.setOnClickListener(onClickListener);

		callSetLayout = (LinearLayout) findViewById(R.id.ly_callno);
		tv_callphone = (TextView) findViewById(R.id.tv_callphone);
		callSetLayout.setOnClickListener(onClickListener);

		soundSwitchView = (SwitchView) findViewById(R.id.soundSwitchView);
		soundSwitchView.setChecked(mPreferencesUtil.getBoolean(PangkerConstant.SP_SOUND_SET + userId, false));
		soundSwitchView.setOnCheckedChangeListener(new SwitchView.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(boolean isChecked) {
				// TODO Auto-generated method stub
				mPreferencesUtil.saveBoolean(PangkerConstant.SP_SOUND_SET + userId, isChecked);
			}
		});
		shockSwitchView = (SwitchView) findViewById(R.id.shockSwitchView);
		shockSwitchView.setChecked(mPreferencesUtil.getBoolean(PangkerConstant.SP_SHOCK_SET + userId, false));
		shockSwitchView.setOnCheckedChangeListener(new SwitchView.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(boolean isChecked) {
				// TODO Auto-generated method stub
				mPreferencesUtil.saveBoolean(PangkerConstant.SP_SHOCK_SET + userId, isChecked);
			}
		});
	}

	private void initData() {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(application.getMySelf().getCallphone())) {
			tv_callphone.setText(application.getMySelf().getCallphone());
		} else {
			tv_callphone.setText(application.getMySelf().getPhone());
		}
	}

	private List<ActionItem> getActionItemsByIndex(int index) {
		List<ActionItem> actionItems = new ArrayList<ActionItem>();
		if (index == 1) {
			for (int i = 0; i < SMS_SET_SEFI_OFFLINE_TYPES.length; i++) {
				actionItems.add(new ActionItem(1 + i, SMS_SET_SEFI_OFFLINE_TYPES[i]));
			}
		} else if (index == 2) {
			for (int i = 0; i < MSG_SET_SEFI_OFFLINE_TYPES.length; i++) {
				actionItems.add(new ActionItem(11 + i, MSG_SET_SEFI_OFFLINE_TYPES[i]));
			}
		}

		return actionItems;
	}

	private void showMenu(String title, List<ActionItem> msgMenu) {
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
		}
		menuDialog.setTitle(title);
		menuDialog.setMenus(msgMenu);
		menuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			switch (actionid) {
			case 1:
				txt_Smsset1.setText(SMS_SET_SEFI_OFFLINE_TYPES[0]);
				mPreferencesUtil.saveInt(PangkerConstant.SP_SMS_SET_OTHER_OFFLINE + userId, 0);
				break;
			case 2:
				txt_Smsset1.setText(SMS_SET_SEFI_OFFLINE_TYPES[1]);
				mPreferencesUtil.saveInt(PangkerConstant.SP_SMS_SET_OTHER_OFFLINE + userId, 1);
				break;
			case 3:
				txt_Smsset1.setText(SMS_SET_SEFI_OFFLINE_TYPES[2]);
				mPreferencesUtil.saveInt(PangkerConstant.SP_SMS_SET_OTHER_OFFLINE + userId, 2);
				break;
			case 11:
				txt_Smsset2.setText(SMS_SET_SEFI_OFFLINE_TYPES[0]);
				mPreferencesUtil.saveInt(PangkerConstant.SP_SMS_SET_SEFI_OFFLINE + userId, 0);
				break;
			case 12:
				txt_Smsset2.setText(SMS_SET_SEFI_OFFLINE_TYPES[1]);
				mPreferencesUtil.saveInt(PangkerConstant.SP_SMS_SET_SEFI_OFFLINE + userId, 1);
				break;
			}
			menuDialog.dismiss();
		}
	};

	/**
	 * 监听
	 */
	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// TODO Auto-generated method stub
			if (view == msgSetLayout1) {
				showMenu("私信发送设置", getActionItemsByIndex(1));
			}
			if (view == msgSetLayout2) {
				showMenu("私信发送设置", getActionItemsByIndex(2));
			}
			if (view == downloadSetLayout) {
				launch(DownloadSetActivity.class);
			}
			// if(view == resrecommendLayout){
			// launch(RecommendSettingActivity.class);
			// }
			if (view == rootSetLayout) {
				launch(RootActivity.class);
			}
			if (view == macthSetLayout) {
				launch(AddressMatchActivity.class);
			}
			if (view == callSetLayout) {
				launch(CallPhoneActivity.class, CALL_CODE);
			}
		}
	};

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CALL_CODE && resultCode == RESULT_OK) {
			initData();
		}
	};

}
