package com.wachoo.pangker.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.google.code.microlog4android.config.PropertyConfigurator;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.db.IBlacklistDao;
import com.wachoo.pangker.db.IContactsGroupDao;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.IGrantDao;
import com.wachoo.pangker.db.IMsgInfoDao;
import com.wachoo.pangker.db.INetAddressBookDao;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.IUserGroupDao;
import com.wachoo.pangker.db.impl.BlacklistDaoImpl;
import com.wachoo.pangker.db.impl.ContactsGroupDaoIpml;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.db.impl.GrantDaoImpl;
import com.wachoo.pangker.db.impl.MsgInfoDaoImpl;
import com.wachoo.pangker.db.impl.NetAddressBookDaoImpl;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.db.impl.UserGroupDaoIpml;
import com.wachoo.pangker.entity.ContactGroup;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.entity.Grant;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.response.AddressBookResult;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.BlackManagerResult;
import com.wachoo.pangker.server.response.Client2UMCResult;
import com.wachoo.pangker.server.response.GrantNotifyResult;
import com.wachoo.pangker.server.response.GroupsNotifyResult;
import com.wachoo.pangker.server.response.LocationQueryResult;
import com.wachoo.pangker.server.response.LoginValidateResult;
import com.wachoo.pangker.server.response.PangkerCallgrant;
import com.wachoo.pangker.server.response.PangkerLocationgrant;
import com.wachoo.pangker.server.response.UserinfoQueryResult;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.LoadingView;
import com.wachoo.pangker.ui.dialog.ConfrimDialog;
import com.wachoo.pangker.util.ConnectivityHelper;
import com.wachoo.pangker.util.MapUtil;
import com.wachoo.pangker.util.MeetRoomUtil;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * 初始化activity
 * 
 * @author wangxin
 * 
 */
public class StartActivity extends CommonActivity {

	private static final String TAG = "StartActivity";
	final int nWelcomeScreenDisplay = 2000;
	private PangkerApplication myApplication;// application
	private UserInfo myself;// 用户信息
	private String myuserid;// 用户id

	private LoadingView loadingView;

	// 本机数据库操作类
	private IContactsGroupDao contactsGroupDao;// group manager
	private IUserGroupDao userGroupDao;
	private IFriendsDao friendsDao;

	private IResDao iResDao;
	private IBlacklistDao iBlacklistDao;
	private IMsgInfoDao msgInfoDao;
	private INetAddressBookDao iNetAddressBookDao;
	private IGrantDao iGrantDao;

	private List<ContactGroup> friendGroup = new ArrayList<ContactGroup>(); // >>>好友分组数据
	private List<UserItem> friendList = new ArrayList<UserItem>();// 好友的用户数据
	private List<UserItem> blackList = new ArrayList<UserItem>();// 黑名单的用户数据
	private List<AddressBooks> mNetAddressBooks = new ArrayList<AddressBooks>();// 单位通讯录数据

	private List<DirInfo> rootDirs = new ArrayList<DirInfo>();

	private SharedPreferencesUtil mUtil;// 数据保存工具类
	private String mySelfDataSaveKey;// 保存标识key
	private String myIMSI;
	private boolean loadOver = false;// 是否加载完毕

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.e("PangkerApplication", ">>>>>>start==onCreate");
		super.onCreate(savedInstanceState);
		PropertyConfigurator.getConfigurator(this).configure();
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);// 设置全屏
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉titile
		setContentView(R.layout.startscreen);// 获取layout
		// 启动图标
		myApplication = (PangkerApplication) getApplication();
		loadingView = (LoadingView) findViewById(R.id.main_imageview);
		loadingView.setImageIds(imageIds);
		loadingView.startAnim(); // >>>>> 初始化等待界面

		// >>>>>>>>>>获取本机手机卡
		myIMSI = Util.getImsiNum(this);
		if (Util.isEmpty(myIMSI)) {
			showToast(R.string.no_phone_card);
			this.finish();
			return;
		}
		// 等待画面
		new LoginThread(handler).start();
	}

	class LoginThread extends Thread {
		private Handler handler;

		public LoginThread(Handler handler) {
			this.handler = handler;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			mUtil = new SharedPreferencesUtil(StartActivity.this);
			myself = Util.readHomeUser(StartActivity.this);// >>>>>>>>>>获取用户信息
			if (myself != null && !myIMSI.equals(myself.getImsi())) {
				Util.delFileData(StartActivity.this, PangkerConstant.CERTIFICATE_FILE_NAME);
			}

			// 判断网络是否可用
			if (!ConnectivityHelper.isConnectivityAvailable(StartActivity.this)) {
				// >>>>>>>>>如果本机数据可用
				if (loadHomeUser()) {
					loginToMain();
				} else {
					handler.sendEmptyMessage(LoadingView.NO_NETWORK);// 网络不可用,本地数据不可用
				}
				return;
			}
			// 网络可用，根据IMSI去后台判断该账户的状态,正常态并且加载用户信息成功，则登录
			if (LoginValidate()) {
				if (loadHomeUser()) {
					// >>>>>>>>初始化数据
					bindMobile();
					loginToMain();
				}
			} else if (loadingView.getLoading() != LoadingView.LOAD_USER_NOUSER) {
				handler.sendEmptyMessage(LoadingView.LOAD_USER_FAIL);// 网络不可用,本地数据不可用
			}
		}
	}

	private void bindMobile() {
		// TODO Auto-generated method stub
		if (!myApplication.isBindPhone()) {
			ConnectivityHelper.sendBindSMSMessage(myself.getUserId(), myIMSI);
		}
	}

	/**
	 * TODO 登录过程，数据初始化
	 */
	private void loginToMain() {

		myApplication.setMySelf(myself);
		myuserid = myself.getUserId();
		// 本机数据库帮助类
		iResDao = new ResDaoImpl(StartActivity.this);
		friendsDao = new FriendsDaoImpl(StartActivity.this);

		userGroupDao = new UserGroupDaoIpml(StartActivity.this);
		msgInfoDao = new MsgInfoDaoImpl(StartActivity.this);
		iBlacklistDao = new BlacklistDaoImpl(StartActivity.this);
		contactsGroupDao = new ContactsGroupDaoIpml(StartActivity.this);// group
		iNetAddressBookDao = new NetAddressBookDaoImpl(StartActivity.this);
		iGrantDao = new GrantDaoImpl(StartActivity.this);
		myApplication.setCurrentLocation(MapUtil.getLastLocation(StartActivity.this));

		// >>>>end by LB
		// 初始化
		PangkerManager.getActivityStackManager();
		PangkerManager.getMessageChangedManager();

		mySelfDataSaveKey = PangkerConstant.MYSELF_DATA + myuserid;
		//
		// // 初始化群组服务器配置
		// MeetRoomUtil.setP2PServiceIp(mUtil.getString(MeetRoomUtil.P2P_SERVER_IP,
		// "119.147.1.211"));
		// MeetRoomUtil.setSpUid(Long.valueOf(mUtil.getString(MeetRoomUtil.P2P_SP_UID,
		// "4294967301")));
		// MeetRoomUtil.setGroupSpPort(mUtil.getInt(MeetRoomUtil.P2P_GROUP_PORT,
		// 1230));

		// 初始化群组服务器配置
		InitMySelfData();// 加载用户信息（当前用户）
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (loadOver == true) {
				return;
			}
			ConfrimDialog dialog = new ConfrimDialog(StartActivity.this);
			if (msg.what == LoadingView.NO_NETWORK) {
				dialog.showLoginDialog("网络连接失败，无法登录!", clickListener);
			} else if (msg.what == LoadingView.LOAD_USER_FAIL) {
				dialog.showLoginDialog("获取用户信息失败，请重新登陆!", clickListener);
			} else if (msg.what == LoadingView.LOAD_CANCEL_USER) {
				dialog.showLoginDialog("你的账号已注销，不能登录!", clickListener);
			} else if (msg.what == LoadingView.NETWORK_STABLE) {
				if (myself != null && myIMSI.equals(myself.getImsi())) {
					loginToMain();// 网络状态不稳定
				} else {
					dialog.showLoginDialog("网络连接异常，无法登录!", clickListener);
				}
			}
		};
	};

	private ConfrimDialog.OnDialogClickListener clickListener = new ConfrimDialog.OnDialogClickListener() {
		@Override
		public void onClick(Dialog dialog, boolean positive) {
			// TODO Auto-generated method stub
			StartActivity.this.finish();
		}
	};

	protected void onDestroy() {
		Log.i(TAG, "StartAcitvity>>onDestroy");
		super.onDestroy();
	};

	/**
	 * 初始化用户，同时判断是否是本机登录
	 * 
	 * @return
	 * @author wangxin 2012-3-21 下午01:28:44
	 */
	private boolean loadHomeUser() {
		if (myself == null) {// 本地获取失败判断
			myself = loadHomeUserFromServer();// 到服务端查询 根据手机的IMSI号码查询
		}
		if (myself == null)// 网络获取失败判断
			return false;// 没有注册过
		if (!myIMSI.equals(myself.getImsi())) {
			Util.delFileData(this, PangkerConstant.CERTIFICATE_FILE_NAME);
			return false;
		}
		Log.i("LoadingView", "initUserSelf ==== ok ====");
		return true;
	}

	/**
	 * 到服务器
	 * 
	 * @author wangxin 2012-3-21 下午06:37:05
	 */
	private UserInfo loadHomeUserFromServer() {
		Intent intent = null;
		if (!ConnectivityHelper.isConnectivityAvailable(this)) {
			intent = new Intent("loadUserFromServerResult");
			sendBroadcast(intent);
			return null;
		}
		UserInfo info = null;
		try {
			// 调用后台接口，根据IMSI号码返回用户信息的接口 ,返回UserInfo对象
			SyncHttpClient client = new SyncHttpClient();
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", "1"));
			paras.add(new Parameter("imsi", myIMSI));
			String supportResponse = client.httpPost(Configuration.getUserinfoQueryByIMSI(), paras,
					myApplication.getServerTime());
			UserinfoQueryResult result = JSONUtil.fromJson(supportResponse.toString(), UserinfoQueryResult.class);
			if (result == null) {
				return info;
			}
			if (result.getErrorCode() == BaseResult.SUCCESS) {
				info = result.getUserInfo();
				Util.writeHomeUser(this, info);
			}
		} catch (Exception e) {
			// TODO: handle exception
			intent = new Intent("loadUserFromServerResult");
			sendBroadcast(intent);
			return null;
		}
		return info;
	}

	private final int[] imageIds = { R.drawable.loader_frame_1, R.drawable.loader_frame_2, R.drawable.loader_frame_3,
			R.drawable.loader_frame_4, R.drawable.loader_frame_5, R.drawable.loader_frame_6 };

	private void notifyGroups(String supportResponse) {
		mUtil.saveString(mySelfDataSaveKey, PangkerConstant.MYSELF_DATA_SAVE);
		String json = supportResponse.toString();
		Log.d(TAG, json);
		GroupsNotifyResult result = JSONUtil.fromJson(json, GroupsNotifyResult.class);
		if (result != null) {
			// 同步联系人组以及组成员
			contactsGroupDao.saveGroups(result.getCgroups());
			// 同步群组
			userGroupDao.saveGroups(result.getGroups());
			// 同步亲友
			friendsDao.pharseTracelist(result.getRelatives());
		}
	}

	private void setGroupsData() {
		// 加载好友组
		myApplication.setFriendGroup(friendGroup);
		// 加载好友
		myApplication.setFriendList(friendList);
		myApplication.setBlackList(blackList);
		myApplication.setNetAddressBooks(mNetAddressBooks);
		// 加载本地友踪组
		myApplication.setTraceList(friendsDao.getTracelist(myuserid));
		loadingView.loadingResult(LoadingView.LOAD_USER_SUCESS);
		// wangxin
		myApplication.setRootDirs(rootDirs);
	}

	protected SyncHttpClient http = new SyncHttpClient();

	/**
	 * 加载用户信息（当前用户）
	 */
	private void InitMySelfData() {
		Log.i("LoadingView", "LoadingView.loading = LoadingView.INITUSER_SUCESS");
		if (!PangkerConstant.MYSELF_DATA_SAVE.equals(mUtil.getString(mySelfDataSaveKey,
				PangkerConstant.MYSELF_DATA_NOSAVE)) && ConnectivityHelper.isConnectivityAvailable(StartActivity.this)) {
			notifyUserGroups();
			// 同步资源类型（文件夹第一层）
			initResInfo();
			// 黑名单用户同步
			AddressBookQuery();
			// 同步单位通讯录数据（只需要同步单位通讯录，不需要同步通讯录用户 ） 带追加***************************
			GrantNotify();
			mUtil.saveString(mySelfDataSaveKey, PangkerConstant.MYSELF_DATA_SAVE);
		}
		// 获取群组sp服务器地址,以便后台服务器进行修改，以及根据经纬度获取服务器
		GetGroupSPServerIP();
		// 初始化联系人数据
		friendGroup = contactsGroupDao.getMyGroupByType(String.valueOf(PangkerConstant.GROUP_FRIENDS));
		friendList = friendsDao.getAllFriendsByUID(myuserid);// 全部好友
		blackList = iBlacklistDao.getBlacklist();
		// 获取本地消息数据总数
		myApplication.setUnReadMessageCount(msgInfoDao.getUnreadCount(myuserid));
		mNetAddressBooks = iNetAddressBookDao.getAddressBookList(myuserid);

		rootDirs = iResDao.getResByIndex(myuserid);
		initLocalDirInfo();
		setGroupsData();

		seachDrift();
	}

	private void GetGroupSPServerIP() {
		// TODO Auto-generated method stub
		try {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", myuserid));
			String supportResponse = http.httpPost(Configuration.getPangkerGroupSpIpInfo(), paras,
					myApplication.getServerTime());
			Client2UMCResult spResult = JSONUtil.fromJson(supportResponse, Client2UMCResult.class);
			if (spResult == null || spResult.getSp().getIpAddress() == null || spResult.getSp().getTcpPort() == null
					|| spResult.getSp().getUid() == null) {
				showToast(R.string.to_server_fail);
				return;
			}
			if (spResult.getErrorCode() == BaseResult.SUCCESS && spResult.getSp() != null) {
				mUtil.saveString(MeetRoomUtil.P2P_SERVER_IP, spResult.getSp().getIpAddress());
				mUtil.saveString(MeetRoomUtil.P2P_SP_UID, spResult.getSp().getUid());
				mUtil.saveInt(MeetRoomUtil.P2P_GROUP_TUP_PORT, spResult.getSp().getTcpPort());

				MeetRoomUtil.setSpUid(Long.valueOf(spResult.getSp().getUid()));
			} else if (spResult.getErrorCode() == BaseResult.FAILED) {
				showToast(spResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// 同步好友组，以及好友组成员，用户的群组会议室，亲友组
	private void notifyUserGroups() {
		try {
			// TODO Auto-generated method stub
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", myuserid));
			paras.add(new Parameter("uid", myuserid));
			paras.add(new Parameter("optime", Util.getSysNowTime()));
			String supportResponse = http.httpPost(Configuration.getGroupsNotify(), paras,
					myApplication.getServerTime());
			notifyGroups(supportResponse);
		} catch (Exception e) {
			// Log.e(TAG, "notify contacts  ---> " + e.getMessage());
		}
	}

	/**
	 * 用户登录鉴权判断
	 * 
	 * 接口LoginValidate.json
	 */
	private boolean LoginValidate() {
		try {
			// >>>>>设置为未绑定
			myApplication.setBindPhone(false);
			// TODO Auto-generated method stub
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", "0"));
			paras.add(new Parameter("imsi", myIMSI));
			paras.add(new Parameter("password", ""));
			String supportResponse = http.httpPost(Configuration.getLoginValidate(), paras,
					myApplication.getServerTime());
			if (!HttpResponseStatus(supportResponse)) {
				handler.sendEmptyMessage(LoadingView.NETWORK_STABLE);
				return false;
			}
			LoginValidateResult result = JSONUtil.fromJson(supportResponse, LoginValidateResult.class);
			// >>>>>>>同步系统时间
			long serverTime = Util.String2Long(result.getSysTime());
			myApplication.setPangkerTimeDiff(serverTime - java.lang.System.currentTimeMillis());
			// 鉴权失败，退出登录
			if (result == null || result.getErrorCode() == LoginValidateResult.FAILED) {
				handler.sendEmptyMessage(LoadingView.LOAD_USER_FAIL);
				return false;
			}
			// 没注册
			else if (result.getErrorCode() == 2) {
				loadingView.loadingResult(LoadingView.LOAD_USER_NOUSER);
				loadOver = true;
				return false;
			}
			// 账号已注销，无法登录提示用户无法登录
			else if (result.getErrorCode() == 3) {
				handler.sendEmptyMessage(LoadingView.LOAD_CANCEL_USER);
				return false;
			} else {
				// 电话号码是否上传标记
				if (result.getPhoneBind() == LoginValidateResult.PHONE_BIND)
					myApplication.setBindPhone(true);

				// 是否绑定本地通讯录
				myApplication.setAddressBooksMacth(result.getIsBookBind() == 1 ? true : false);
				//
				return result.getErrorCode() == LoginValidateResult.SUCCESS;
			}

		} catch (Exception e) {
			handler.sendEmptyMessage(LoadingView.NETWORK_STABLE);
			return false;
		}
	}

	// 到服务器加载资源信息
	private void initResInfo() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, PangkerService.class);
		intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_RES_INIT);
		startService(intent);
	}

	/**
	 * 黑名单用户
	 * 
	 * @param type
	 *            操作类型
	 */
	public void BlacklistManager() {
		try {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", myuserid));
			paras.add(new Parameter("ruid", ""));
			paras.add(new Parameter("type", "1")); // 操作类型：0：添加，1：查询，2：删除。
			String blacklistResult = http.httpPost(Configuration.getBlackManager(), paras,
					myApplication.getServerTime());
			BlackManagerResult blackResult = JSONUtil.fromJson(blacklistResult, BlackManagerResult.class);
			if (blackResult != null && blackResult.getErrorCode() == BaseResult.SUCCESS) {
				if (blackResult.getUsers() != null) {
					iBlacklistDao.saveBlacklist(blackList);
				}
			}
		} catch (Exception e) {
		}
	}

	/**
	 * 获取单位通讯录信息
	 * 
	 * 
	 * 接口AdressBookQuery.json
	 */
	private void AddressBookQuery() {
		try {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", "1"));//
			paras.add(new Parameter("uid", myuserid)); //
			String addressBook = http.httpPost(Configuration.getAddressBookQuery(), paras,
					myApplication.getServerTime());
			if (addressBook != null) {
				AddressBookResult addressResult = JSONUtil.fromJson(addressBook, AddressBookResult.class);
				if (addressResult != null) {
					List<AddressBooks> mContactList = addressResult.getBooks();
					if (mContactList != null) {
						iNetAddressBookDao.saveAddressBooks(myuserid, mContactList);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 15.2 权限信息同步接口 接口GrantNotify.json
	 */
	private void GrantNotify() {
		try {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", "1"));//
			paras.add(new Parameter("uid", myuserid)); //
			String grantNotity = http.httpPost(Configuration.getGrantNotify(), paras, myApplication.getServerTime());
			if (grantNotity != null) {
				GrantNotifyResult grantResult = JSONUtil.fromJson(grantNotity, GrantNotifyResult.class);
				if (grantResult != null && grantResult.getErrorCode() == BaseResult.SUCCESS) {

					// >>>>>>>>同步位置权限
					PangkerLocationgrant locationGrant = grantResult.getLocationgrant();
					if (locationGrant != null) {
						Grant mGrant = new Grant(myuserid, Grant.GRANT_TYPE_LOCATION, locationGrant.getGrantType(),
								locationGrant.getGrantInfo(), locationGrant.getMembers());
						iGrantDao.saveGrant(mGrant);
					}
					// >>>>>>>>同步呼叫权限
					PangkerCallgrant callGrant = grantResult.getCallgrant();
					if (callGrant != null) {
						Grant mGrant = new Grant(myuserid, Grant.GRANT_TYPE_CALL, callGrant.getGrantType(),
								callGrant.getGrantInfo(), callGrant.getMembers());
						iGrantDao.saveGrant(mGrant);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void initLocalDirInfo() {
		for (int i = 0; i < PangkerConstant.RES_SUM.length; i++) {
			List<DirInfo> resInfos = iResDao.getDirByType(myuserid, DirInfo.DEAL_SELECT, PangkerConstant.RES_SUM[i]);
			myApplication.putListDirs(PangkerConstant.RES_SUM[i], resInfos);
		}
	}

	/**
	 * void TODO同步用户是否在漂移状态
	 */
	private void seachDrift() {
		try {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", myApplication.getMyUserID()));
			String driftResult = http.httpPost(Configuration.getDriftLocationQuery(), paras,
					myApplication.getServerTime());
			LocationQueryResult result = JSONUtil.fromJson(driftResult.toString(), LocationQueryResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				Log.d("LocateType", driftResult.toString());
				if (result.getLat() != null && !result.getLat().equals("") && result.getLon() != null
						&& !result.getLon().equals("")) {
					if (result.getStatus() == LocationQueryResult.STATUS_DRIFT) {
						myApplication.setLocateType(PangkerConstant.LOCATE_DRIFT);// 变更漂移状态
						myApplication.setDirftLocation(new Location(Double.parseDouble(result.getLon()), Double
								.parseDouble(result.getLat())));
						uploadLocation(myApplication.getCurrentLocation());
						UserItem driftItem = new UserItem();
						driftItem.setUserId(result.getTargetUid());
						driftItem.setUserName(result.getTargetUserName());
						myApplication.setDriftUser(driftItem);
					} else
						myApplication.setLocateType(PangkerConstant.LOCATE_CLIENT);//
				} else {
					cancelDrift();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 上传用户位置信息
	 * 
	 */
	public void uploadLocation(com.wachoo.pangker.entity.Location location) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", myApplication.getMyUserID()));
		paras.add(new Parameter("lon", String.valueOf(location.getLongitude())));
		paras.add(new Parameter("lat", String.valueOf(location.getLatitude())));
		try {
			String uploadLocationResult = http.httpPost(Configuration.getUpLocation(), paras,
					myApplication.getServerTime());
			Log.i("location", "upload location - " + uploadLocationResult);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// /**
	// * 取消漂移
	// */
	private void cancelDrift() {
		try {
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("uid", myApplication.getMyUserID()));
			String cancelDriftResult = http.httpPost(Configuration.getCancelDrift(), paras,
					myApplication.getServerTime());
			BaseResult result = JSONUtil.fromJson(cancelDriftResult.toString(), BaseResult.class);
			if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
				myApplication.setLocateType(PangkerConstant.LOCATE_CLIENT);//
				myApplication.setDriftUser(null);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
