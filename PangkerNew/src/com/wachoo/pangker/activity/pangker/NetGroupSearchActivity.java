package com.wachoo.pangker.activity.pangker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.group.ChatRoomInfoActivity;
import com.wachoo.pangker.activity.res.ResSearchActivity;
import com.wachoo.pangker.adapter.MeetGroupAdapter;
import com.wachoo.pangker.adapter.NearGroupAdapter;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.GroupsNotifyResult;
import com.wachoo.pangker.server.response.SearchLbsGroupResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.util.Util;

/**
 * 搜索全网人群
 * 
 * @author zhegnjy
 *
 */
public class NetGroupSearchActivity extends ResSearchActivity implements IUICallBackInterface {

	private String TAG = "ContactFriendInviteActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	private final int SEARCH_GROUP = 0x10;
	private final int SEARCH_GROUP_BY_TYPE = 0x11;
	private EditText etEditName;
	private Button btnMenuShow;
	private LinearLayout ly_title;
	private String uid;
	private String searchKey;
	private PangkerApplication application;
	
	private ServerSupportManager serverMana;

	private ListView mResListview;
	private EmptyView emptyView;
	private List<GroupLbsInfo> groupList;
	private List<UserGroup> userGroups; 
	private NearGroupAdapter groupAdapter;//搜索引擎数据
	private MeetGroupAdapter adapter;//后台搜索数据
	private int fromtype;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.net_res_search);
		application = (PangkerApplication) getApplication();
		uid = application.getMyUserID();
		fromtype = this.getIntent().getIntExtra("fromtype", 0);
		initView();
		
		if(fromtype == 0){
			groupList = new ArrayList<GroupLbsInfo>();
			groupAdapter = new NearGroupAdapter(this,groupList);
			mResListview.setAdapter(groupAdapter);
		}
		else {
			userGroups = new ArrayList<UserGroup>();
			adapter = new MeetGroupAdapter(userGroups,this);
			mResListview.setAdapter(adapter);
		}
		
		initListener();
	}
	
	public void initView() {
		Button btn_Right = (Button) findViewById(R.id.iv_more);
		btn_Right.setVisibility(View.GONE);
		TextView tv_title = (TextView) findViewById(R.id.mmtitle);
		tv_title.setText("群组搜索");
		findViewById(R.id.lv_mPullToRefresh).setVisibility(View.GONE);
		findViewById(R.id.title_icon).setVisibility(View.GONE);
		mResListview = (ListView) findViewById(R.id.res_list);
	
		
		initPressBar(mResListview);
		emptyView = new EmptyView(this);
		emptyView.addToListView(mResListview); 

		btnMenuShow = (Button) findViewById(R.id.btn_reply);
		etEditName = (EditText) findViewById(R.id.et_query_name);
		
		if(fromtype == 0){
			etEditName.setHint("通过群组名称搜索");
		}
		else if(fromtype == 1){
			etEditName.setInputType(InputType.TYPE_CLASS_NUMBER);
			etEditName.setHint("通过旁客号码搜索");
		}
		else if(fromtype == 2){
			etEditName.setInputType(InputType.TYPE_CLASS_PHONE);
			etEditName.setHint("通过用户手机号搜索");
		}
		
		btnMenuShow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				searchKey = etEditName.getText().toString();
				if (Util.isEmpty(searchKey)) {
					showToast("搜索条件不能为空!");
					return;
				}
				if(fromtype == 0){
					searchtype = PangkerConstant.SEACH_GROUPS_BYKEYS;
					search();
				}
				else {
					searchGroupByType(searchKey);
				}
			}
		});

		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
						.hideSoftInputFromWindow(NetGroupSearchActivity.this
								.getCurrentFocus().getWindowToken(),
								InputMethodManager.HIDE_NOT_ALWAYS);
				NetGroupSearchActivity.this.finish();
			}
		});

	}
	
	private void initListener(){
		mResListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				
				if(fromtype == 0){
					Intent intent = new Intent();
					intent.putExtra("GroupInfo", (Serializable)groupAdapter.getGroupList());
					intent.putExtra("group_index", position);
					intent.setClass(NetGroupSearchActivity.this, ChatRoomInfoActivity.class);
					startActivity(intent);
				}
				else {
					UserGroup usergroup = (UserGroup)arg0.getItemAtPosition(position);
					lookChatRoomInfo(usergroup);
				}
			}
		});
		loadmoreView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (lyLoadPressBar.getVisibility() == View.GONE && lyLoadText.getVisibility() == View.VISIBLE) {
					if (isLoadAll()) {
						loadText.setText(R.string.nomore_data);
					} else {
						String label = "";
						if (etEditName != null && etEditName.getText() != null
								&& etEditName.getText().toString() != null) {
							label = etEditName.getText().toString();
						}
						searchGroupFromLbs(label,false);
					}
				}
			}
		});
	}
	
	private void lookChatRoomInfo(UserGroup userGroup) {
		Intent intent = new Intent();
		List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
		GroupLbsInfo lbsInfo = new GroupLbsInfo();
		lbsInfo.setgName(userGroup.getGroupName());
		lbsInfo.setSid(Long.parseLong(userGroup.getGroupId()));
		lbsInfo.setUid(Long.parseLong(userGroup.getUid()));
		lbsInfo.setShareUid(userGroup.getUid());
		groupList.add(lbsInfo);
		intent.putExtra("GroupInfo", (Serializable) groupList);
		intent.putExtra("group_index", 0);
		intent.setClass(this, ChatRoomInfoActivity.class);
		this.startActivity(intent);
	}
	
	/**
	 * 搜索数据
	 */
	@Override
	protected void search() {
		reset();
		String label = etEditName.getText().toString();
		loadText.setText(R.string.loadmore);
		searchGroupFromLbs(label.trim(),true);
	}
	/**
	 * 搜索周边群聊
	 * 
	 * @param gName群组名
	 * @param loType
	 * 
	 *            return void
	 */
	public void searchGroupFromLbs(String label,boolean isShowMess) {
		if (groupAdapter.getCount() >= incremental) {
			setPresdBarVisiable(true);
		}
		if(serverMana == null){
			serverMana = new ServerSupportManager(this, this);
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", uid));
		paras.add(getLatitude());
		paras.add(getLongitude());
		paras.add(getDistanceRang());
		paras.add(getSearchLimit());
		paras.add(new Parameter("searchkey", label)); // 群组标签，$ 符号分隔
		paras.add(new Parameter("searchtype", searchtype)); // 群组类别，0固定群组；1
		// 移动群组。2.全部，3关键字
		paras.add(new Parameter("groupcategory", "2"));
		paras.add(new Parameter("orderkey", "4"));
		serverMana.supportRequest(Configuration.getGroupFromLbs(), paras, isShowMess,getString(R.string.please_wait),SEARCH_GROUP);
		emptyView.reset();
	}
	
	/**
	 * 搜索用户群组
	 * 
	 */
	public void searchGroupByType(String typeStr) {
		if(serverMana == null){
			serverMana = new ServerSupportManager(this, this);
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		if(fromtype == 1){
			paras.add(new Parameter("account", typeStr)); // optye=2时必须旁客账号
			paras.add(new Parameter("optype", "2")); // 操作类型1： 通过手机号码查询2：通过旁客账号
		}
		else if(fromtype == 2){
			paras.add(new Parameter("mobile", typeStr));//optype=1时必须手机号码
			paras.add(new Parameter("optype", "1")); // 操作类型1： 通过手机号码查询2：通过旁客账号
		}
		
		serverMana.supportRequest(Configuration.getGroupSearch(), paras,true,getString(R.string.please_wait),SEARCH_GROUP_BY_TYPE);
		emptyView.reset();
	}
	

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			emptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			return;
		}

		if(caseKey == SEARCH_GROUP){
			SearchLbsGroupResult groupResult = JSONUtil.fromJson(response.toString(),
					SearchLbsGroupResult.class);
			if (groupResult != null && groupResult.isSuccess() && groupResult.getData() != null) {
				List<GroupLbsInfo> gList = groupResult.getData().getData();

				if (gList != null && gList.size() > 0) {
					if (gList.size() < incremental) {
						lyLoadPressBar.setVisibility(View.GONE);
						lyLoadText.setVisibility(View.GONE);
						setLoadAll(true);
					} else
						setPresdBarVisiable(false);

					// 如果不是从0开始加载
					if (getStartLimit() != 0) {
						List<GroupLbsInfo> addList = new ArrayList<GroupLbsInfo>(gList);
						groupList.addAll(addList);
					}
					// 首次加载
					else {
						if (!isLoadAll()) {
							setPresdBarVisiable(false);
						}
						groupList = gList;
					}
					resetStartLimit();

					int index = mResListview.getFirstVisiblePosition();
					groupAdapter.setGroupList(groupList);
					mResListview.setSelection(index);

				} else if (gList == null || gList.size() == 0) {
					if (getStartLimit() == 0) {
						groupAdapter.setGroupList(new ArrayList<GroupLbsInfo>());
						emptyView.showView(R.drawable.emptylist_icon, R.string.no_search_group);
					} else
						showToast(R.string.nomore_data);
					setLoadAll(true);
					lyLoadPressBar.setVisibility(View.GONE);
					lyLoadText.setVisibility(View.GONE);
				} else {
					loadText.setText(R.string.load_data_fail);
					lyLoadPressBar.setVisibility(View.GONE);
					lyLoadText.setVisibility(View.VISIBLE);
				}
			} else {
				if (startLimit == 0) {
					emptyView.showView(R.drawable.emptylist_icon, R.string.to_server_fail);
				} else {
					showToast(R.string.to_server_fail);
				}
				lyLoadPressBar.setVisibility(View.GONE);
				lyLoadText.setVisibility(View.GONE);
			}
		}
		else if(caseKey == SEARCH_GROUP_BY_TYPE){
			GroupsNotifyResult groupResult = JSONUtil.fromJson(response.toString(),
					GroupsNotifyResult.class);
			if(groupResult != null && groupResult.getErrorCode() == groupResult.SUCCESS){
				List<UserGroup> userGroupList = groupResult.getGroups();
				if(userGroupList != null && userGroupList.size() > 0){
					adapter.setUserGroups(userGroupList);
				}
				else{
					adapter.setUserGroups(new ArrayList<UserGroup>());
					emptyView.showView(R.drawable.emptylist_icon, R.string.no_search_group);
				} 
			}
			else if(groupResult != null && groupResult.getErrorCode() == groupResult.FAILED){
				emptyView.showView(R.drawable.emptylist_icon, R.string.no_search_group);
				adapter.setUserGroups(new ArrayList<UserGroup>());
			}
			else {
				adapter.setUserGroups(new ArrayList<UserGroup>());
				emptyView.showView(R.drawable.emptylist_icon,R.string.to_server_fail);
			}
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
					.hideSoftInputFromWindow(NetGroupSearchActivity.this
							.getCurrentFocus().getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
			NetGroupSearchActivity.this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onBackPressed() {
		((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
				.hideSoftInputFromWindow(NetGroupSearchActivity.this
						.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
		NetGroupSearchActivity.this.finish();
	}
	
}
