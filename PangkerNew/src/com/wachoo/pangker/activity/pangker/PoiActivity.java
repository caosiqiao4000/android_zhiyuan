package com.wachoo.pangker.activity.pangker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.PoiScanAdapter;
import com.wachoo.pangker.api.PangkerException;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.map.poi.LBS_Request;
import com.wachoo.pangker.map.poi.Poi_Search_Request;
import com.wachoo.pangker.map.poi.Poi_Search_Response;
import com.wachoo.pangker.map.poi.Poi_concise;
import com.wachoo.pangker.map.poi.Search_Response;
import com.wachoo.pangker.server.ILbsOrgSupport;
import com.wachoo.pangker.server.LbsOrgSupport;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.util.Util;

/**
 * PoiActivity
 * 
 * @author libo eidt by : wangxin
 */
public class PoiActivity extends CommonActivity {

	final int defaultDistant = 1000;
	final int defaultCount = 30;

	private QuickAction quickAction;
	private PullToRefreshListView pullToRefreshListView;
	private ListView poiListView;
	private FooterView footerView;
	private EmptyView emptyView;
	private PoiScanAdapter poiAdapter;
	private EditText txtInput;
	// 搜索Cache
	private Map<String, List<Poi_concise>> PoiCache = new HashMap<String, List<Poi_concise>>();
	private Map<String, Boolean> footCache = new HashMap<String, Boolean>();// 表示是否可以进行加载更多
	private Map<String, Integer> PageCache = new HashMap<String, Integer>();
	// 手动定位的Dialog
	private LocateReceiver loacteReceiver;
	private PangkerApplication application;

	String poi_key = Poi_Search_Request.CATEGORY_ALL;
	private ILbsOrgSupport lbsOrgSupport;
	protected Button btnMenu;
	private Button btnSearch;

	/**
	 * onCreate
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_friends_listview);
		// MainService.allActivity.add(this);
		lbsOrgSupport = new LbsOrgSupport();
		application = (PangkerApplication) getApplication();
		// 初始化activity
		initView();
		// 用户选择的Dialog
		// initDialog();
		dealPoiKey(0);
		registerReceiver();
		// 首次登录旁客设置向导
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// 首次登录旁客设置向导
	}

	private void registerReceiver() {
		// TODO Auto-generated method stub
		loacteReceiver = new LocateReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(PangkerConstant.ACTTION_LOCATE_STATE);// 友踪邀请定位请求
		registerReceiver(loacteReceiver, filter);
	}

	private class LocateReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (PangkerConstant.ACTTION_LOCATE_STATE.equals(action)) {
				PoiCache.clear();
				PageCache.put(poi_key, 1);
				SearchPoiDataByKey(poi_key, defaultDistant, PageCache.get(poi_key), getCurrentLocation());
			}
		}
	}

	class PoiLoader extends AsyncTask<LBS_Request, Void, Search_Response> {
		private Poi_Search_Request request;

		public PoiLoader(Location location, String searchkey, int page) {
			// TODO Auto-generated constructor stub
			if (page <= 1) {
				pullToRefreshListView.setRefreshing(true);
			}
			request = new Poi_Search_Request(searchkey, location, defaultDistant, page, defaultCount);
		}

		@Override
		protected Search_Response doInBackground(LBS_Request... params) {
			// TODO Auto-generated method stub
			try {
				return lbsOrgSupport.PoiSearch(request);
			} catch (PangkerException e) {

			}
			return null;
		}

		@Override
		protected void onPostExecute(Search_Response result) {
			// TODO Auto-generated method stub
			pullToRefreshListView.onRefreshComplete();
			refresh(result);
		}

	}

	/**
	 * 根据条件查询poi
	 * 
	 * @param searchKey
	 */
	private void SearchPoiDataByKey(String searchKey, int range, int page, Location location) {
		if (location == null) {
			showToast("还没有获取到您的位置信息，请稍等！");
			return;
		}
		if (PoiCache.containsKey(searchKey)) {
			showPois(PoiCache.get(searchKey));
			if (footCache.get(searchKey) == null || !footCache.get(searchKey)) {
				footerView.hideFoot();
			} else {
				footerView.onLoadComplete();
			}
		} else {
			new PoiLoader(location, searchKey, page).execute();
		}
	}

	/***
	 * 
	 * @param key
	 */
	private void removePoiCacheByKey(String key) {
		PoiCache.remove(PoiCache.get(key));
	}

	private void showPois(List<Poi_concise> pois) {
		if (PoiCache.get(poi_key) == null) {
			PoiCache.put(poi_key, pois);
		} else {
			PoiCache.get(poi_key).addAll(pois);
		}
		poiAdapter.setList(PoiCache.get(poi_key));
	}

	private void condtionSearchPoi(String searchkey) {
		// TODO Auto-generated method stub
		emptyView.getTxtInput().setText(searchkey);
		if (!Util.isEmpty(searchkey)) {
			removePoiCacheByKey(searchkey);
			poi_key = searchkey;
			if (PageCache.get(poi_key) == null) {
				PageCache.put(poi_key, 1);
			}
			SearchPoiDataByKey(poi_key, defaultDistant, PageCache.get(poi_key), getCurrentLocation());
		}
	}

	TextWatcher textWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// TODO Auto-generated method stub
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if (s != null && !s.toString().trim().equals("")) {
				btnSearch.setVisibility(View.VISIBLE);
			} else {
				btnSearch.setVisibility(View.GONE);
			}
		}
	};
	
	View.OnTouchListener listviewOnTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			hideSoftInput(v);
			return false;
		}
	};

	/**
	 * 初始化activity
	 */
	private void initView() {
		// 显示Poi信息的ListView
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_contacts);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		pullToRefreshListView.setTag("pk_near_pois");
		poiListView = pullToRefreshListView.getRefreshableView();
		poiAdapter = new PoiScanAdapter(this, new ArrayList<Poi_concise>());
		poiListView.setOnItemClickListener(onItemClickListener);
		poiListView.setOnTouchListener(listviewOnTouchListener);
		footerView = new FooterView(this);
		footerView.setOnLoadMoreListener(onLoadMoreListener);
		footerView.addToListView(poiListView);

		emptyView = new EmptyView(this, true);
		emptyView.getBtnMenu().setOnClickListener(onClickListener);
		emptyView.setOnSearchListener(onSearchListener);
		poiListView.setEmptyView(emptyView.getView());

		View listBar = LayoutInflater.from(this).inflate(R.layout.contact_search, null);
		txtInput = (EditText) listBar.findViewById(R.id.et_search);
		// 监听搜索框是否有输入
		txtInput.addTextChangedListener(textWatcher);
		ImageView btnClear = (ImageView) listBar.findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(onClickListener);
		btnMenu = (Button) listBar.findViewById(R.id.btn_left);
		btnMenu.setOnClickListener(onClickListener);
		btnSearch = (Button) listBar.findViewById(R.id.btn_right);
		btnSearch.setBackgroundResource(R.drawable.btn_search_selector);
		btnSearch.setOnClickListener(onClickListener);
		poiListView.addHeaderView(listBar);

		// 初始化poi类别
		quickAction = new QuickAction(this, QuickAction.VERTICAL);
		quickAction.setOnActionItemClickListener(onActionItemClickListener);
		for (int i = 0; i < Poi_Search_Request.getPoiTab().length; i++) {
			if (i == Poi_Search_Request.getPoiTab().length - 1) {
				quickAction.addActionItem(new ActionItem(i, Poi_Search_Request.getPoiKeyByIndex(i)), true);
			} else {
				quickAction.addActionItem(new ActionItem(i, Poi_Search_Request.getPoiKeyByIndex(i)));
			}
		}
		poiListView.setAdapter(poiAdapter);
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			if (Util.isEmpty(emptyView.getSearch())) {
				poi_key = "";
			}
			if (PoiCache.containsKey(poi_key)) {
				PoiCache.remove(poi_key);
				PageCache.put(poi_key, 1);
			}
			SearchPoiDataByKey(poi_key, defaultDistant, PageCache.get(poi_key), getCurrentLocation());
		}
	};

	EmptyView.OnSearchListener onSearchListener = new EmptyView.OnSearchListener() {
		@Override
		public void onSearch(String search) {
			// TODO Auto-generated method stub
			condtionSearchPoi(search);
			txtInput.setText("");
			txtInput.setHint(search);
		}
	};

	FooterView.OnLoadMoreListener onLoadMoreListener = new FooterView.OnLoadMoreListener() {
		@Override
		public void onLoadMoreListener() {
			// TODO Auto-generated method stub
			new PoiLoader(getCurrentLocation(), poi_key, PageCache.get(poi_key)).execute();
		}
	};

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btn_clear) {
				txtInput.setText("");
			}
			if (v == btnMenu || v == emptyView.getBtnMenu()) {
				quickAction.show(v);
			}
			if (v == btnSearch) {
				condtionSearchPoi(txtInput.getText().toString().trim());
			}
		}
	};

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			dealPoiKey(actionId);
		}
	};

	OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View arg1, int point, long arg3) {
			// TODO Auto-generated method stub
			Poi_concise poiInfo = (Poi_concise) adapterView.getItemAtPosition(point);
			goPoiInfoActivity(poiInfo);
		}
	};

	/**
	 * 根据选择的Poi进入信息信息
	 * 
	 * @param point
	 */
	private void goPoiInfoActivity(Poi_concise poiInfo) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setClass(getParent(), PoiInfoActivity.class);
		intent.putExtra(getResourcesMessage(R.string.intent_poi), poiInfo);
		startActivity(intent);
	}

	private Location getCurrentLocation() {
		// TODO Auto-generated method stub
		if (application.getLocateType() == PangkerConstant.LOCATE_CLIENT) {
			return application.getCurrentLocation();
		}
		return application.getDirftLocation();
	}

	/**
	 * 根据Tab的index进行操作
	 * 
	 * @param point
	 */
	private void dealPoiKey(int point) {
		// TODO Auto-generated method stub
		poi_key = Poi_Search_Request.getPoiKeyByIndex(point);
		txtInput.setText("");
		if (point == 0) {
			poi_key = "";
			txtInput.setHint(Poi_Search_Request.CATEGORY_ALL);
		} else {
			txtInput.setHint(poi_key);
		}
		if (getCurrentLocation() != null) {
			if (PageCache.get(poi_key) == null) {
				PageCache.put(poi_key, 1);
			}
			SearchPoiDataByKey(poi_key, defaultDistant, PageCache.get(poi_key), getCurrentLocation());
		}
	}

	public void refresh(Search_Response result) {
		// TODO Auto-generated method stub
		txtInput.setText("");
		if (Util.isEmpty(poi_key)) {
			txtInput.setHint(Poi_Search_Request.CATEGORY_ALL);
		} else {
			txtInput.setHint(poi_key);
		}

		Poi_Search_Response response = (Poi_Search_Response) result;
		List<Poi_concise> pois = response.getPois();
		if (!response.isError() && pois != null) {
			if (pois.size() >= defaultCount) {
				footCache.put(poi_key, true);
				footerView.onLoadComplete();
				PageCache.put(poi_key, PageCache.get(poi_key) + 1);
			} else {
				footCache.put(poi_key, false);
				footerView.hideFoot();
			}
			Collections.sort(pois);
			showPois(pois);
		} else {
			if (poiAdapter.getCount() <= 0) {
				emptyView.showView(R.drawable.emptylist_icon, R.string.no_data);
			}
			footerView.hideFoot();
		}
	}

}
