package com.wachoo.pangker.activity.pangker;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.InvitedUserAdapter;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.SearchUnderUsersResult;
import com.wachoo.pangker.server.response.SimpleUserInfo;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;

/**
 * @author wubo
 * @createtime 2012-5-23
 */
public class DriftUserActivity extends CommonPopActivity implements IUICallBackInterface {

	private PullToRefreshListView pullToRefreshListView;//
	private PKIconResizer mImageResizer;
	private ListView driftlistView;
	private EmptyView mEmptyView;
	InvitedUserAdapter adapter;
	PangkerApplication application;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blacklist_manager);
		application = (PangkerApplication) getApplication();
		mImageResizer = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);
		initView();
		getDriftUser();
	}

	private void initView() {
		// TODO Auto-generated method stub
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("漂移我的用户");
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DriftUserActivity.this.finish();
			}
		});

		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		driftlistView = pullToRefreshListView.getRefreshableView();
		
		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(driftlistView);

		adapter = new InvitedUserAdapter(this, InvitedUserAdapter.DRIFT_USERS, mImageResizer);
		driftlistView.setAdapter(adapter);

		driftlistView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				SimpleUserInfo userItem = (SimpleUserInfo) arg0.getItemAtPosition(arg2);
				Intent intent = new Intent(DriftUserActivity.this, UserWebSideActivity.class);
				intent.putExtra(UserItem.USERID, userItem.getUserId());
				intent.putExtra(UserItem.USERNAME, userItem.getUserName());
				intent.putExtra(UserItem.USERSIGN, userItem.getSign());
				startActivity(intent);

			}
		});
	}

	OnRefreshListener<ListView> onRefreshListener = new OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			getDriftUser();
		}
	};

	public void getDriftUser() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", application.getMyUserID()));
		serverMana.supportRequest(Configuration.getDriftUserQuery(), paras);
		pullToRefreshListView.setRefreshing(true);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(supportResponse)) {
			mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_network);
			return;
		}
		SearchUnderUsersResult result = JSONUtil.fromJson(supportResponse.toString(),
				SearchUnderUsersResult.class);
		if (result == null || result.getErrorCode() == 999) {
			mEmptyView.showView(R.drawable.emptylist_icon, R.string.return_value_999);
			return;
		}
		if (result.getErrorCode() == 0) {
			mEmptyView.showView(R.drawable.emptylist_icon, result.getErrorMessage());
			return;
		}
		if (result.getErrorCode() == 1) {
			adapter.setmInvitedUsers(result.getUsers());
			mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_driftusers_prompt);
		}
	}
}
