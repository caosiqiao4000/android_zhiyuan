package com.wachoo.pangker.activity.pangker;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.amap.mapapi.core.GeoPoint;
import com.amap.mapapi.map.MapController;
import com.amap.mapapi.map.MapView;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonMapActivity;
import com.wachoo.pangker.activity.HomeActivity;
import com.wachoo.pangker.activity.MainActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.chat.VCardManager;
import com.wachoo.pangker.entity.Location;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.map.app.LocateManager;
import com.wachoo.pangker.map.poi.PoiaddressLoader;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.CallMeSetResult;
import com.wachoo.pangker.server.response.LocationQueryCheckResult;
import com.wachoo.pangker.service.PangkerService;
import com.wachoo.pangker.ui.OverUserItem;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.util.MapUtil;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

/**
 * @author Pool 在地图显示用户的位置信息，只需要传递(userid),和username，sign
 */
public class MapLocationActivity extends CommonMapActivity implements IUICallBackInterface {

	private final int CANCEL_DRIFT = 0x10A;
	private final int LOCATION_QUERY = 0x10B;
	private final int CASE_DRIFT = 0x10C;
	private final int CALL_CHECK_CASEKEY = 0x10D;

	private Button btnlocate;
	private Button btnBack;
	private TextView txtTitle;
	private MapView mapview;

	private MapController mapcontroller;
	private Bitmap drawIcon;
	private PKIconResizer mImageResizer;
	private OverUserItem oItem;

	UIUserInfo uiUserInfo;
	private String userId;
	private UserItem userItem;

	private PangkerApplication application;
	private LocateReceiver locateReceiver;
	private ServerSupportManager serverMana;
	private Location poiLocation;

	private ZoomControls mZoomControls;

	private PoiaddressLoader poiaddressLoader;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		this.setMapMode(MAP_MODE_VECTOR);// 设置地图为矢量模式
		super.onCreate(arg0);
		setContentView(R.layout.maplocation);
		poiaddressLoader = new PoiaddressLoader(getApplicationContext());
		init();
		initView();
		initdata();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if (id == 1) {
			builder.setMessage("您现在处于漂移模式,只有结束漂移才能定位,确认结束漂移?").setCancelable(false)
					.setPositiveButton("确定", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							cancelDrift();
						}
					}).setNegativeButton("取消", null);
			AlertDialog alert = builder.create();
			return alert;
		}
		return super.onCreateDialog(id);
	}

	private void init() {
		// TODO Auto-generated method stub
		serverMana = new ServerSupportManager(this, this);
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		userItem = (UserItem) getIntent().getSerializableExtra("userinfo");
		if (userItem == null) {
			userItem = Util.toUserItem(application.getMySelf());
		}
		if (userId.equals(userItem.getUserId())) {
			locateReceiver = new LocateReceiver();
			IntentFilter filter = new IntentFilter();
			filter.addAction(PangkerConstant.ACTTION_LOCATE_STATE);// 定位状态
			registerReceiver(locateReceiver, filter);
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText("用户地图");
		btnlocate = (Button) findViewById(R.id.iv_more);
		btnlocate.setOnClickListener(onClickListener);

		mapview = (MapView) findViewById(R.id.atmapsView);
		mapview.setBuiltInZoomControls(false);

		mZoomControls = (ZoomControls) findViewById(R.id.zc_textsize_zoom);
		mZoomControls.setOnZoomInClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mapcontroller.zoomIn();
			}
		});

		mZoomControls.setOnZoomOutClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mapcontroller.zoomOut();
			}
		});
		uiUserInfo = new UIUserInfo();
		mapcontroller = mapview.getController();
		mapcontroller.setZoom(15);
		drawIcon = BitmapFactory.decodeResource(getResources(), R.drawable.map_placemark);
	}

	private String getRName(String userId) {
		VCardManager cardManager = new VCardManager(this);
		UserItem item = cardManager.getUserItem(userId);
		if (item != null) {
			return item.getrName();
		}
		return null;
	}

	private void initdata() {
		// TODO Auto-generated method stub
		if (userId.equals(userItem.getUserId())) {
			txtTitle.setText("我的位置");
			btnlocate.setBackgroundResource(R.drawable.locate_btn);
			poiLocation = (Location) getIntent().getSerializableExtra("location");
			if (poiLocation == null) {
				if (application.getLocateType() == PangkerConstant.LOCATE_CLIENT) {
					poiLocation = application.getCurrentLocation();
				} else {
					poiLocation = application.getDirftLocation();
				}
			}
			setcenter(poiLocation, application.getLocateType());
			poiaddressLoader.onPoiLoader(poiLocation, this, uiUserInfo.textViewLocation);
		} else {
			btnlocate.setBackgroundResource(R.drawable.btn_default_selector);
			btnlocate.setText("漂移");
			double lat = userItem.getLatitude();
			double lon = userItem.getLongitude();
			poiLocation = new Location();
			if (lat > 20 && lon > 10) {
				poiLocation.setLatitude(lat);
				poiLocation.setLongitude(lon);
				setcenter(poiLocation, userItem.getLocateType());
			} else {
				poiLocation.setLatitude(LocateManager.LOCATE_LAT);
				poiLocation.setLongitude(LocateManager.LOCATE_LON);
				setcenter(poiLocation, userItem.getIconType());
				// searchLocByUserId(userItem.getUserId());
			}
		}
		uiUserInfo.initData(userItem);
		uiUserInfo.showDrift();
	}

	/**
	 * 漂移
	 * 
	 * @author wubo
	 * @createtime 2012-5-23
	 * @param item
	 */
	private void driftToUserItem(final UserItem item) {
		if (application.getLocateType() == PangkerConstant.LOCATE_DRIFT) {
			UserItem userInfo = application.getDriftUser();
			if (userItem.getUserId().equals(userInfo.getUserId())) {
				toast("您在漂移在Ta身边!");
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item.getUserId());
						}
					}
				}, this);
				dialog.showDialog("您目前漂移在用户" + userInfo.getUserName() + "身边，确定要漂移到用户" + userItem.getUserName() + "身边？",
						false);
			}
		} else {
			SharedPreferencesUtil spu = new SharedPreferencesUtil(this);
			if (spu.getBoolean(PangkerConstant.SP_DRIFT_SET + application.getMyUserID(), false)) {
				locationCheck(item.getUserId());
			} else {
				MessageTipDialog dialog = new MessageTipDialog(new DialogMsgCallback() {
					@Override
					public void buttonResult(boolean isSubmit) {
						// TODO Auto-genaerated method stub
						if (isSubmit) {
							locationCheck(item.getUserId());
						}
					}
				}, this);
				dialog.setTextSize(17, 13);
				String data = getResources().getString(R.string.drift_show);
				data = String.format(data, item.getUserName());
				dialog.showDialog(MessageTipDialog.TYPE_DRIFT, data, getResourcesMessage(R.string.drift_tip), true);
			}
		}
	}

	/**
	 * 位置漂移鉴权
	 * 
	 * @author wubo
	 * @createtime May 15, 2012
	 * @param friendId
	 */
	public void locationCheck(String friendId) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", friendId));
		paras.add(new Parameter("visituid", userId));
		paras.add(new Parameter("optype", "2"));
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getLocationQueryCheck(), paras, true, mess, CASE_DRIFT);
	}

	/**
	 * 呼叫校验 void
	 */
	public void callCheck(String friendId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", friendId));
		paras.add(new Parameter("visituid", userId));
		paras.add(new Parameter("password", ""));
		String mess = "权限校验中...";
		serverMana.supportRequest(Configuration.getCallCheck(), paras, true, mess, CALL_CHECK_CASEKEY);
	}

	/**
	 * @author wangxin 2012-3-19 下午01:46:51
	 */
	public void showInfo(UserItem user) {
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, user.getUserId());
		intent.putExtra(UserItem.USERNAME, user.getUserName());
		intent.putExtra(UserItem.USERSIGN, user.getSign());
		intent.putExtra(PangkerConstant.STATE_KEY, user.getState());
		startActivity(intent);
	}

	public class UIUserInfo implements View.OnClickListener {

		private ImageView imgUserIcon;
		private TextView textViewLocation;
		private TextView txtName;

		public UIUserInfo() {
			// TODO Auto-generated constructor stub
			textViewLocation = (TextView) findViewById(R.id.textViewLocation);
			imgUserIcon = (ImageView) findViewById(R.id.iv_usericon);
			txtName = (TextView) findViewById(R.id.trace_username);
			imgUserIcon.setOnClickListener(this);
		}

		public void initData(UserItem userItem) {
			// TODO Auto-generated method stub
			userItem.setrName(getRName(userItem.getUserId()));
			txtName.setText(userItem.getAvailableName());
			mImageResizer.loadImage(userItem.getUserId(), imgUserIcon);

			if (!Util.isEmpty(userItem.getPoi())) {
				textViewLocation.setText(userItem.getPoi());
			} else {
				if (poiLocation != null) {
					poiaddressLoader.onPoiLoader(poiLocation, MapLocationActivity.this, uiUserInfo.textViewLocation);
				}
			}
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (poiLocation != null)
				if (userId.equals(userItem.getUserId())) {
					setcenter(poiLocation, application.getLocateType());
				} else {
					setcenter(poiLocation, userItem.getLocateType());
				}

		}

		public void showDrift() {
			// TODO Auto-generated method stub
			if (userId.equals(userItem.getUserId())) {
				if (application.getLocateType() == PangkerConstant.LOCATE_DRIFT && application.getDriftUser() != null) {
					String htmlstring = userItem.getUserName() + "(漂移在"
							+ Util.getHtmlString(application.getDriftUser().getUserName()) + "身边)";
					txtName.setText(Html.fromHtml(htmlstring));
					findViewById(R.id.iv_drift).setVisibility(View.VISIBLE);
				} else {
					txtName.setText(userItem.getUserName());
					findViewById(R.id.iv_drift).setVisibility(View.GONE);
				}
			} else {
				findViewById(R.id.iv_drift).setVisibility(View.GONE);
			}
		}
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				finish();
			}
			if (v == btnlocate) {
				if (userId.equals(userItem.getUserId())) {
					if (application.getLocateType() == PangkerConstant.LOCATE_DRIFT) {
						showDialog(1);
					} else {
						// 防止连续点击卡住
						btnlocate.setEnabled(false);
						locate();
					}
				} else {
					driftToUserItem(userItem);
				}
			}
		}
	};

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		// mapview.closeMapview();
		this.finish();
	}

	private void updateLoaction(Location location, int type) {
		// TODO Auto-generated method stub
		if (type == PangkerConstant.LOCATE_DRIFT) {
			application.setDriftUser(userItem);
		} else {
			application.setDriftUser(null);
		}
		application.setLocateType(type);// 变更漂移状态
		application.setDirftLocation(location);
		Message msg = new Message();
		msg.what = PangkerConstant.DRIFT_NOTICE;
		PangkerManager.getTabBroadcastManager().sendResultMessage(MainActivity.class.getName(), msg);
		PangkerManager.getTabBroadcastManager().sendResultMessage(HomeActivity.class.getName(), msg);
		if (type == PangkerConstant.LOCATE_CLIENT) {
			uploadLocation(application.getCurrentLocation());
		}
		Intent intent = new Intent(MapLocationActivity.this, PangkerService.class);
		intent.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.INTENT_DRIFT_START);
		startService(intent);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (userId.equals(userItem.getUserId())) {
			unregisterReceiver(locateReceiver);
		}
		super.onDestroy();
	}

	private void dealLocation(Location location, int status) {
		// TODO Auto-generated method stub
		String msgString = "";
		if (status == LocateManager.LOCATE_FAILED) {
			msgString = "定位失败! 获取的预先制定的位置信息(" + location.getLongitude() + "," + location.getLatitude() + ")";
		}
		if (status == LocateManager.LOCATE_LAST) {
			msgString = "定位失败! 获取上一次的位置信息(" + location.getLongitude() + "," + location.getLatitude() + ")";
		}
		if (status == LocateManager.LOCATE_SUCCESS) {
			msgString = "定位成功! (" + location.getLongitude() + "," + location.getLatitude() + ")";
		}
		toast(msgString);
		if (!isFinishing()) {
			setcenter(location, application.getLocateType());
			poiaddressLoader.onPoiLoader(location, this, uiUserInfo.textViewLocation);
		}
	}

	private void locate() {
		// TODO Auto-generated method stub
		Intent intent2 = new Intent(this, PangkerService.class);
		intent2.putExtra(PangkerConstant.SERVICE_INTENT, PangkerConstant.LOCATE_INTENT);
		startService(intent2);

		toast("定位中...");
	}

	private void dealLocation(Intent intent) {
		// TODO Auto-generated method stub
		int status = intent.getIntExtra(PangkerConstant.ACTTION_LOCATE_STATE, LocateManager.LOCATE_FAILED);
		Location location = (Location) intent.getSerializableExtra("Location");

		updateLoaction(location, PangkerConstant.LOCATE_CLIENT);

		dealLocation(location, status);
	}

	private class LocateReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (PangkerConstant.ACTTION_LOCATE_STATE.equals(intent.getAction())) {
				btnlocate.setEnabled(true);
				dealLocation(intent);
			}
		}
	}

	/**
	 * 取消漂移
	 * 
	 * @author wubo
	 * @createtime 2012-5-23
	 */
	private void cancelDrift() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", userId));
		serverMana.supportRequest(Configuration.getCancelDrift(), paras, true, "正在取消漂移...", CANCEL_DRIFT);
	}

	private void setcenter(Location location, int locateType) {
		GeoPoint nowPoint = MapUtil.toPoint(location);
		if (nowPoint != null) {
			oItem = new OverUserItem(this, nowPoint, userItem.getUserId(), drawIcon, locateType);
			// mapview.getOverlays().add(oItem);
			mapcontroller.animateTo(nowPoint);
			mapview.removeAllViews();
			oItem.addToMap(mapview);
		} else {
			Toast.makeText(this, "还无法获取您的位置信息!", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response))
			return;

		if (caseKey == CANCEL_DRIFT) {
			BaseResult cancelresult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (cancelresult != null && cancelresult.getErrorCode() == 1) {
				application.setLocateType(PangkerConstant.LOCATE_CLIENT);// 变更漂移状态
				application.setDriftUser(null);
				uiUserInfo.showDrift();
				Message msg = new Message();
				msg.what = PangkerConstant.DRIFT_NOTICE;
				PangkerManager.getTabBroadcastManager().sendResultMessage(MainActivity.class.getName(), msg);
				PangkerManager.getTabBroadcastManager().sendResultMessage(HomeActivity.class.getName(), msg);
				locate();
			} else {
				toast("取消失败!");
			}
		}
		if (caseKey == CASE_DRIFT) {
			btnlocate.setEnabled(true);
			LocationQueryCheckResult result = JSONUtil.fromJson(response.toString(), LocationQueryCheckResult.class);
			if (result != null && result.getErrorCode() == 1) {
				// 变更漂移状态
				String data = getResources().getString(R.string.drift_show);
				data = String.format(data, userItem.getUserName());
				showToast(data);
				updateLoaction(new Location(Double.valueOf(result.getLon()), Double.valueOf(result.getLat())),
						PangkerConstant.LOCATE_DRIFT);
				uiUserInfo.showDrift();
			} else if (result != null) {
				toast(result.errorMessage);
			} else {
				toast("漂移失败!");
			}
		}
		if (caseKey == CALL_CHECK_CASEKEY) {
			CallMeSetResult resultCall = JSONUtil.fromJson(response.toString(), CallMeSetResult.class);
			if (resultCall != null && resultCall.getErrorCode() == BaseResult.SUCCESS) {
				application.getMsgInfoBroadcast().sendCallMsg(userItem.getUserId(), userItem.getAvailableName());
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel://" + resultCall.getMobile()));
				startActivity(intent);
			} else if (resultCall != null
					&& (resultCall.getErrorCode() == BaseResult.FAILED || resultCall.getErrorCode() == 2)) {
				showToast(resultCall.getErrorMessage());
			} else
				showToast(R.string.return_value_999);
		}
	}

	/**
	 * 上传用户位置信息
	 * 
	 */
	public void uploadLocation(com.wachoo.pangker.entity.Location location) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("lon", String.valueOf(location.getLongitude())));
		paras.add(new Parameter("lat", String.valueOf(location.getLatitude())));
		serverMana.supportRequest(Configuration.getUpLocation(), paras);
	}

	private void toast(String content) {
		Toast.makeText(this, content, Toast.LENGTH_SHORT).show();
	}

}
