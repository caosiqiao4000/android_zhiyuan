package com.wachoo.pangker.activity.pangker;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserInfoEditActivity;
import com.wachoo.pangker.activity.contact.ContactAttentionActivity;
import com.wachoo.pangker.activity.contact.FriendAddActivity;
import com.wachoo.pangker.activity.res.ResSearchActivity;
import com.wachoo.pangker.adapter.NearUserGVAdapter;
import com.wachoo.pangker.adapter.NearUserListAdapter;
import com.wachoo.pangker.api.ContactUserListenerManager;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.db.IAttentsDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.impl.AttentsDaoImpl;
import com.wachoo.pangker.db.impl.PKUserDaoImpl;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.entity.index.IndexResponse;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.SearchNearFriendsResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.QuickAction;
import com.wachoo.pangker.util.ContactsUtil;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wifi.locate.manager.WifiLocateManager;

/**
 * 行人
 * 
 * @author wangxin
 */
public class NearUserActivity extends ResSearchActivity implements
		IUICallBackInterface {
	// private static final String TAG = "NearUserActivity";
	private final int[] MENU_INDEXS = { 0x100, 0x101, 0x102, 0x103 };
	private final String[] MENU_TITLE = { "女生", "男生", "附近好友", "附近所有人" };

	private List<UserItem> mPedestrianList = new ArrayList<UserItem>();

	private Button btnChangeList;
	private Button btnChangeList_nodata;
	private LinearLayout layoutTitle;
	private LinearLayout layoutTitle_nodata;
	private ImageView iconDrow;
	private ImageView iconDrow_nodata;

	private ImageView iv_empty_icon;

	private PullToRefreshListView pullToRefreshListView;
	private ListView mPedestrianListView;

	private TextView tvTitle;
	private TextView tvTitle_nodata;
	private TextView mEmptyTextView;
	private Button btnSearch1;
	private Button btnSearch2;

	private NearUserListAdapter mPedestriansAdapter;
	private NearUserGVAdapter nearUserGVAdapter;

	private IAttentsDao iAttentsDao;
	private IPKUserDao pkUserDao;
	private PangkerApplication application;
	private RelativeLayout mTitleTopLayout;
	private RelativeLayout mTitleTopLayout_nodata;
	private QuickAction popopQuickAction;

	private ServerSupportManager serverMana;

	// 是否加载完周边用户，如果加载完可以插入wifi定位用户，如果没加载完，则等待，0：没加载完，1：加载完
	private int isLoadFinish = 0;
	private List<UserItem> mWifiListUser;

	private UserItem currUserItem;
	private static final int LIST_TYPE = 0;
	private static final int GRID_TYPE = 1;
	private int currListType = LIST_TYPE;// 当前显示列表类型标识，0为ListView，1为GridView
	// 搜索附近的人的条件
	private String searchCondition;
	private static int LOADCODE = 0x100;
	private final int LOOK_FAN = 0x101;
	private final int LOAD_NEAR_FRIEND = 0x102;
	private final int ADD_TO_FRIEND = 0x103;
	private final int ADD_TO_ATTENTION = 0x104;
	private final int ADD_TO_ATTENTION_IMPLICIT = 0x105;

	private int nUserStartlimit = 0;
	private int nUserEndlimit = 0;
	private final int nUserincremental = 15;

	private String userId;
	private ContactUserListenerManager userListenerManager;
	private SharedPreferencesUtil sp_Util;

	private ContactsUtil contactsUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_friends_listview);
		sp_Util = new SharedPreferencesUtil(this);
		searchCondition = sp_Util.getString(PangkerConstant.SP_NEARUSER_KEY,
				PangkerConstant.NEAR_USER_ALL);
		init();
		initView();// 加载画面控件

		initListener();// 添加控件监听事件
		setAdapter();// 加载adapter

		// 附近好友
		if (searchCondition.equals(PangkerConstant.NEAR_USER_FIRENDS)) {

			// >>>>>>>>>>加载上次数据
			// String supportResponse = sp_Util.getString(TAG + "friend");
			// if (!Util.isEmpty(supportResponse)) {
			// SearchNearFriendsResult nearFriend =
			// JSONUtil.fromJson(supportResponse,
			// SearchNearFriendsResult.class);
			// mPedestriansAdapter.setmUserList(nearFriend.getUsers());
			// }
			reset();
			loadNearFriend();
		} else {
			// >>>>>>>>>>加载上次数据
			// String supportResponse = sp_Util.getString(TAG + "lbs");
			// if (!Util.isEmpty(supportResponse)) {
			// IndexResponse result = JSONUtil.fromJson(supportResponse,
			// IndexResponse.class);
			// mPedestriansAdapter.setmUserList(result.getData().getData());
			// mPedestriansAdapter.notifyDataSetChanged();
			// }
			reset();
			load(searchCondition);
		}

		initWifiAutoLocate();

	}

	private void init() {
		// TODO Auto-generated method stub
		userId = ((PangkerApplication) getApplication()).getMyUserID();
		serverMana = new ServerSupportManager(getParent(), this);
		iAttentsDao = new AttentsDaoImpl(this);
		pkUserDao = new PKUserDaoImpl(this);
		application = (PangkerApplication) getApplication();
		userListenerManager = PangkerManager.getContactUserListenerManager();
		contactsUtil = new ContactsUtil(this, userId);
	}

	/**
	 * 初始化控件
	 * 
	 * @author wubo
	 * @createtime 2012-1-9 上午11:41:44
	 */
	private void initView() {
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_contacts);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		pullToRefreshListView.setTag("pk_near_users");
		mPedestrianListView = pullToRefreshListView.getRefreshableView();

		initPressBar(mPedestrianListView);
		loadmoreView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (lyLoadPressBar.getVisibility() == View.GONE
						&& lyLoadText.getVisibility() == View.VISIBLE) {
					if (isLoadAll()) {
						loadText.setText(R.string.nomore_data);
					} else {
						if (searchCondition != null
								&& searchCondition
										.equals(PangkerConstant.NEAR_USER_FIRENDS)) {
							lyLoadPressBar.setVisibility(View.GONE);
							lyLoadText.setVisibility(View.GONE);
							loadNearFriend();
						} else
							load(searchCondition);
					}
				}
			}
		});
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View nearuser_bar = inflater.inflate(R.layout.nearuser_bar, null);

		tvTitle = (TextView) nearuser_bar.findViewById(R.id.tv_title);
		iconDrow = (ImageView) nearuser_bar.findViewById(R.id.icon_drow);
		iconDrow.setImageResource(R.drawable.icon32_arrow);
		layoutTitle = (LinearLayout) nearuser_bar
				.findViewById(R.id.layout_title);
		btnChangeList = (Button) nearuser_bar.findViewById(R.id.btn_right);
		mTitleTopLayout = (RelativeLayout) nearuser_bar
				.findViewById(R.id.title_top_ly);
		btnSearch1 = (Button) nearuser_bar.findViewById(R.id.btn_search);

		mPedestrianListView.addHeaderView(nearuser_bar);

		View nearuser_emptyview = LayoutInflater.from(this).inflate(
				R.layout.nearuser_empty_view, null, false);

		iv_empty_icon = (ImageView) nearuser_emptyview
				.findViewById(R.id.iv_empty_icon);
		iv_empty_icon.setImageBitmap(null);
		mEmptyTextView = (TextView) nearuser_emptyview
				.findViewById(R.id.textViewEmpty);
		mEmptyTextView.setText("");

		tvTitle_nodata = (TextView) nearuser_emptyview
				.findViewById(R.id.tv_title);
		btnChangeList_nodata = (Button) nearuser_emptyview
				.findViewById(R.id.btn_right);
		mTitleTopLayout_nodata = (RelativeLayout) nearuser_emptyview
				.findViewById(R.id.title_top_ly);
		layoutTitle_nodata = (LinearLayout) nearuser_emptyview
				.findViewById(R.id.layout_title);
		iconDrow_nodata = (ImageView) nearuser_emptyview
				.findViewById(R.id.icon_drow);
		iconDrow_nodata.setImageResource(R.drawable.icon32_arrow);
		btnSearch2 = (Button) nearuser_emptyview.findViewById(R.id.btn_search);

		mPedestrianListView.setEmptyView(nearuser_emptyview);

		popopQuickAction = new QuickAction(this,
				QuickAction.ANIM_GROW_FROM_CENTER);
		popopQuickAction
				.setOnActionItemClickListener(onActionItemClickListener);
		List<ActionItem> menuActionItems = new ArrayList<ActionItem>();
		for (int i = 0; i < MENU_INDEXS.length; i++) {
			menuActionItems.add(new ActionItem(MENU_INDEXS[i], MENU_TITLE[i]));
		}
		popopQuickAction.setActionItems(menuActionItems);

		popopQuickAction
				.setOnDismissListener(new QuickAction.OnDismissListener() {
					@Override
					public void onDismiss() {
						// TODO Auto-generated method stub
						iconDrow.setImageResource(R.drawable.icon32_arrow);
						iconDrow_nodata
								.setImageResource(R.drawable.icon32_arrow);
					}
				});

		if (searchCondition.equals(PangkerConstant.NEAR_USER_GIRLS)) {
			tvTitle.setText(MENU_TITLE[0]);
			tvTitle_nodata.setText(MENU_TITLE[0]);
		} else if (searchCondition.equals(PangkerConstant.NEAR_USER_BOYS)) {
			tvTitle.setText(MENU_TITLE[1]);
			tvTitle_nodata.setText(MENU_TITLE[1]);
		} else if (searchCondition.equals(PangkerConstant.NEAR_USER_FIRENDS)) {
			tvTitle.setText(MENU_TITLE[2]);
			tvTitle_nodata.setText(MENU_TITLE[2]);
		} else if (searchCondition.equals(PangkerConstant.NEAR_USER_ALL)) {
			tvTitle.setText(MENU_TITLE[3]);
			tvTitle_nodata.setText(MENU_TITLE[3]);
		}
	}

	/* 弹出窗口选项 */
	QuickAction.OnActionItemClickListener onActionItemClickListener = new QuickAction.OnActionItemClickListener() {
		@Override
		public void onItemClick(QuickAction source, int pos, int actionId) {
			iconDrow.setImageResource(R.drawable.icon32_arrow);
			iconDrow_nodata.setImageResource(R.drawable.icon32_arrow);
			tvTitle.setText(MENU_TITLE[pos]);
			tvTitle_nodata.setText(MENU_TITLE[pos]);
			// 女生
			if (actionId == MENU_INDEXS[0]) {
				searchCondition = PangkerConstant.NEAR_USER_GIRLS;
				sp_Util.saveString(PangkerConstant.SP_NEARUSER_KEY,
						PangkerConstant.NEAR_USER_GIRLS);
			}
			// 男生
			else if (actionId == MENU_INDEXS[1]) {
				searchCondition = PangkerConstant.NEAR_USER_BOYS;
				sp_Util.saveString(PangkerConstant.SP_NEARUSER_KEY,
						PangkerConstant.NEAR_USER_BOYS);
			}
			// 附近好友
			else if (actionId == MENU_INDEXS[2]) {
				searchCondition = PangkerConstant.NEAR_USER_FIRENDS;
				sp_Util.saveString(PangkerConstant.SP_NEARUSER_KEY,
						PangkerConstant.NEAR_USER_FIRENDS);
				reset();
				// loadmoreView.setVisibility(View.GONE);
				loadNearFriend();

			}
			// 附近啊所有人
			else if (actionId == MENU_INDEXS[3]) {
				searchCondition = PangkerConstant.NEAR_USER_ALL;
				sp_Util.saveString(PangkerConstant.SP_NEARUSER_KEY,
						PangkerConstant.NEAR_USER_ALL);
			}
			isLoadFinish = 0;
			if (actionId != MENU_INDEXS[2]) {
				loadmoreView.setVisibility(View.GONE);
				reset();
				load(searchCondition);
			}
			if (actionId == MENU_INDEXS[3]) {
				initWifiAutoLocate();
			}
		}
	};

	/**
	 * void TODO 查找wifi定位用户
	 */
	private void initWifiAutoLocate() {
		boolean isAutoWifiLocate = sp_Util.getBoolean(
				PangkerConstant.WIFI_LOCATE_KEY + userId, true);
		if (isAutoWifiLocate) {
			WifiLocateManager mWifiLocateManager = new WifiLocateManager(this,
					this);
			mWifiLocateManager.searchNearUsers(userId, null, 0);
			Log.i("AutoWifiLocationTask", "searchNearUsers!");

		}
	}

	// 刷新，重新查询
	public void reset() {
		nUserStartlimit = 0;
		this.isLoadAll = false;
	}

	/**
	 * 搜索数据
	 */
	protected void search() {
		reset();
		load(searchCondition);
	}

	private OnClickListener btnChangeListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnSearch2 || v == btnSearch1) {
				Intent intent = new Intent(NearUserActivity.this,
						FriendAddActivity.class);
				intent.putExtra("fromactivity", "NearUserActivity");
				startActivity(intent);
			} else {
				if (currListType == LIST_TYPE) {
					btnChangeList
							.setBackgroundResource(R.drawable.btn_list_mode_selector);
					btnChangeList_nodata
							.setBackgroundResource(R.drawable.btn_list_mode_selector);
					nearUserGVAdapter = new NearUserGVAdapter(
							NearUserActivity.this,
							mPedestriansAdapter.getmUserList());
					mPedestrianListView.setAdapter(nearUserGVAdapter);
					mPedestrianListView.setDividerHeight(0);
					nearUserGVAdapter.notifyDataSetChanged();
					currListType = GRID_TYPE;
				} else if (currListType == GRID_TYPE) {
					btnChangeList
							.setBackgroundResource(R.drawable.btn_grid_mode_selector);
					btnChangeList_nodata
							.setBackgroundResource(R.drawable.btn_grid_mode_selector);
					mPedestriansAdapter.setmUserList(nearUserGVAdapter
							.getmUserList());
					mPedestrianListView.setAdapter(mPedestriansAdapter);
					mPedestriansAdapter.notifyDataSetChanged();
					mPedestrianListView.setDividerHeight(1);
					currListType = LIST_TYPE;
				}
			}
		}
	};

	/**
	 * 初始化监听器
	 */
	private void initListener() {

		btnChangeList.setOnClickListener(btnChangeListener);
		btnChangeList_nodata.setOnClickListener(btnChangeListener);
		layoutTitle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				popopQuickAction.show(mTitleTopLayout);
				iconDrow.setImageResource(R.drawable.icon32_arrow_p);

			}
		});
		layoutTitle_nodata.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				popopQuickAction.show(mTitleTopLayout_nodata);
				iconDrow_nodata.setImageResource(R.drawable.icon32_arrow_p);
			}
		});
		btnSearch1.setOnClickListener(btnChangeListener);
		btnSearch2.setOnClickListener(btnChangeListener);
	}

	/**
	 * 搜索周边过客
	 * 
	 * @author wubo
	 * @createtime 2012-1-9 下午05:35:09
	 */
	public void load(String sex) {
		if (mPedestriansAdapter.getCount() >= nUserincremental) {
			setPresdBarVisiable(true);
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("location", getLon() + "," + getLat()));
		paras.add(getDistanceRang());// 搜索范围
		paras.add(new Parameter("sex", sex != null ? sex : ""));
		paras.add(new Parameter("agelimit", "0,100"));
		paras.add(new Parameter("online", "1"));
		paras.add(getSearchLimit());
		paras.add(new Parameter("usertype", "0")); // 0 旁客，
		if (nUserStartlimit == 0) {
			pullToRefreshListView.setRefreshing(true);
		}
		serverMana.supportRequest(Configuration.getUserSearch(), paras,
				LOADCODE);
	}

	// >>>>>> getSearchLimit每次加载nUserincremental个
	protected Parameter getSearchLimit() {
		nUserEndlimit = nUserStartlimit + nUserincremental;
		return new Parameter("limit", nUserStartlimit + "," + nUserincremental);
	}

	/**
	 * 搜索附近好友
	 * 
	 * @author zhengjy
	 * 
	 */
	public void loadNearFriend() {
		pullToRefreshListView.setRefreshing(true);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("lon", getLon()));
		paras.add(new Parameter("lat", getLat()));
		paras.add(getRange());// 搜索范围
		serverMana.supportRequest(Configuration.getSearchNearFriends(), paras,
				LOAD_NEAR_FRIEND);
	}

	/**
	 * 刷新列表数据
	 * 
	 * @param userList
	 */
	private void setAdapter() {
		mPedestriansAdapter = new NearUserListAdapter(this, mPedestrianList);
		mPedestrianListView.setAdapter(mPedestriansAdapter);

		mPedestriansAdapter.notifyDataSetChanged();

	}

	// 长按的上下文菜单选项点击事件处理
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// 用户子列表长按
		if (item.getGroupId() == 0) {
			switch (item.getItemId()) {
			// 查看资料
			case LOOK_FAN:
				goToUserInfoMinute();
				break;
			// 添加为好友
			case ADD_TO_FRIEND:
				if (!currUserItem.getUserId().trim().equals(userId)) {
					if (application.IsFriend(currUserItem.getUserId())) {
						showToast(PangkerConstant.MSG_FRIEND_ISEXIST);
					} else {
						if (application.IsBlacklistExist(currUserItem
								.getUserId())) {// 黑名单用户
							showToast(R.string.toast_blacklist_exist);
						} else {
							final OpenfireManager openfireManager = application
									.getOpenfireManager();
							if (openfireManager.isLoggedIn()) {
								if (openfireManager.createEntry(
										currUserItem.getUserId(),
										currUserItem.getUserName(), null)) {
									showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_SUECCESS);
								} else {
									showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_FAIL);
								}
							} else {
								showToast(R.string.not_logged_cannot_operating);
							}
						}

					}
				} else
					showToast(R.string.can_not_addfriend_self);
				break;
			// 添加为关注，隐式关注
			case ADD_TO_ATTENTION:
			case ADD_TO_ATTENTION_IMPLICIT:

				if (!currUserItem.getUserId().trim().equals(userId)) {
					if (application.IsBlacklistExist(currUserItem.getUserId())) {// 黑名单用户
						showToast(R.string.toast_blacklist_exist);
					} else {
						if (item.getItemId() == ADD_TO_ATTENTION) {
							attentionManager(currUserItem.getUserId(), "1",
									ADD_TO_ATTENTION);
						} else if (item.getItemId() == ADD_TO_ATTENTION_IMPLICIT) {
							attentionManager(currUserItem.getUserId(), "0",
									ADD_TO_ATTENTION_IMPLICIT);
						}

					}

				} else
					showToast(R.string.can_not_addattend_self);

				break;

			}
		}
		return super.onContextItemSelected(item);
	}

	/**
	 * 详细信息
	 * 
	 * @author wubo
	 * @createtime 2012-1-13 下午04:53:04
	 */
	public void goToUserInfoMinute() {
		Intent intent = new Intent();
		intent.putExtra("userid", currUserItem.getUserId());
		intent.putExtra("userName", currUserItem.getUserName());
		intent.putExtra("userSign", currUserItem.getUserId());
		intent.setClass(this, UserInfoEditActivity.class);
		this.startActivity(intent);
		/*
		 * intent.putExtra("userid", currUserItem.getUserId());
		 * intent.setClass(this, UserInfoMinuteActivity.class); launch(intent,
		 * this);
		 */

	}

	/**
	 * 关注管理、添加或者删除
	 * 
	 * @param groupid
	 *            被操作组Id
	 * @param fanId
	 *            被添加或删除对象 接口：AttentManager.json
	 */
	private void attentionManager(String attentionId, String attentType,
			int caseKey) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("appuserid", userId));
		paras.add(new Parameter("relationuserid", attentionId));
		paras.add(new Parameter("groupid", ""));
		paras.add(new Parameter("type", "1"));
		paras.add(new Parameter("attentType", attentType));// 关注类型 0 : 隐式关注 //
		// 1：显式关注
		serverMana.supportRequest(Configuration.getAttentManager(), paras,
				caseKey);
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub

			if (searchCondition.equals(PangkerConstant.NEAR_USER_FIRENDS)) {
				reset();
				loadNearFriend();
			} else {
				isLoadFinish = 0;
				search();
				initWifiAutoLocate();
			}

		}
	};

	// >>>>>>>>> setStartLimit
	public void resetStartLimit() {
		nUserStartlimit = nUserEndlimit;
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(supportResponse)) {
			if (caseKey == LOADCODE) {
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				mEmptyTextView.setText(R.string.no_network);
			}
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			return;
		}

		if (caseKey == 2) {
			Log.i("AutoWifiLocationTask", "searchResult=" + supportResponse);
			if (supportResponse != null) {
				IndexResponse result = JSONUtil.fromJson(
						supportResponse.toString(), IndexResponse.class);
				if (result != null && result.isSuccess()) {
					if (result.getData().getTotal() > 0) {
						List<UserItem> listUser = result.getData().getData();
						if (listUser != null && listUser.size() > 0) {
							// 1：表示已加载完周边用户
							if (isLoadFinish == 1) {
								if (currListType == LIST_TYPE) {
									mPedestriansAdapter
											.addWifiUserList(listUser);
								} else if (currListType == GRID_TYPE) {
									nearUserGVAdapter.addWifiUserList(listUser);
								}
								isLoadFinish = 0;
								mWifiListUser = null;
							} else {
								isLoadFinish = 1;
								mWifiListUser = listUser;
							}

						}
					}
				}
			}
		}

		// >>>>>>>>>>查询附近用户
		if (caseKey == LOADCODE) {

			IndexResponse result = JSONUtil.fromJson(
					supportResponse.toString(), IndexResponse.class);
			if (result != null && result.isSuccess()) {
				if (result.getData().getTotal() > 0) {
					List<UserItem> listUser = result.getData().getData();
					if (listUser != null && listUser.size() > 0) {

						// >>>>>>>>>>>从返回用户中删除自己
						for (UserItem item : listUser) {
							if (userId.equals(item.getUserId())) {
								listUser.remove(item);
								break;
							}
						}

						if (result.getData().getTotal() < nUserincremental) {
							setLoadAll(true);
							lyLoadPressBar.setVisibility(View.GONE);
							lyLoadText.setVisibility(View.GONE);
						} else {
							setPresdBarVisiable(false);
						}
						// 如果不是从0开始加载
						if (nUserStartlimit != 0) {
							mPedestrianList.addAll(listUser);

						}
						// 首次加载
						else {
							if (!isLoadAll()) {
								setPresdBarVisiable(false);
							}
							mPedestrianList = listUser; // >>>>>>>>>>本地缓存数据
							// sp_Util.saveString(TAG + "lbs",
							// supportResponse.toString());
						}
						resetStartLimit();
					} else if (listUser != null && listUser.size() == 0) {
						if (nUserStartlimit == 0) {
							mPedestrianList = new ArrayList<UserItem>();
						}
						setLoadAll(true);
						lyLoadPressBar.setVisibility(View.GONE);
						lyLoadText.setVisibility(View.GONE);
					} else {
						loadText.setText(R.string.load_data_fail);
						lyLoadPressBar.setVisibility(View.GONE);
						lyLoadText.setVisibility(View.VISIBLE);
					}

				} else {
					if (nUserStartlimit == 0) {
						mPedestrianList = new ArrayList<UserItem>();
						iv_empty_icon
								.setImageResource(R.drawable.emptylist_icon);
						if (searchCondition == PangkerConstant.NEAR_USER_ALL) {
							mEmptyTextView
									.setText(R.string.no_near_user_prompt);
						} else if (searchCondition == PangkerConstant.NEAR_USER_BOYS) {
							mEmptyTextView
									.setText(R.string.no_near_boys_prompt);
						} else if (searchCondition == PangkerConstant.NEAR_USER_GIRLS) {
							mEmptyTextView
									.setText(R.string.no_near_girls_prompt);
						}
					}
					lyLoadPressBar.setVisibility(View.GONE);
					lyLoadText.setVisibility(View.GONE);
				}

			} else {
				lyLoadPressBar.setVisibility(View.GONE);
				lyLoadText.setVisibility(View.GONE);
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				mEmptyTextView.setText(R.string.to_server_fail);
			}

			if (currListType == LIST_TYPE) {
				mPedestriansAdapter.setmUserList(mPedestrianList);
			} else if (currListType == GRID_TYPE) {
				nearUserGVAdapter.setmUserList(mPedestrianList);
			}
			if (isLoadFinish == 1) {
				if (mWifiListUser != null) {
					if (currListType == LIST_TYPE) {
						mPedestriansAdapter.addWifiUserList(mWifiListUser);
					} else if (currListType == GRID_TYPE) {
						nearUserGVAdapter.addWifiUserList(mWifiListUser);
					}
					isLoadFinish = 0;
					mWifiListUser = null;
				}

			} else
				isLoadFinish = 1;

		} else if (caseKey == ADD_TO_ATTENTION
				|| caseKey == ADD_TO_ATTENTION_IMPLICIT) {
			BaseResult addAttent = JSONUtil.fromJson(
					supportResponse.toString(), BaseResult.class);
			if (addAttent != null
					&& addAttent.getErrorCode() == BaseResult.SUCCESS) {
				showToast(addAttent.getErrorMessage());
				if (caseKey == ADD_TO_ATTENTION) {
					currUserItem.setAttentType(UserItem.ATTENT_TYPE_COMMON);
					application.getOpenfireManager().sendContactsChangedPacket(
							userId, currUserItem.getUserId(),
							BusinessType.addattent);
				} else
					currUserItem.setAttentType(UserItem.ATTENT_TYPE_IMPLICIT);
				pkUserDao.saveUser(currUserItem);
				contactsUtil.addAttentCount(1);
				iAttentsDao.addAttent(currUserItem,
						String.valueOf(PangkerConstant.DEFAULT_GROUPID));
				userListenerManager.refreshUI(
						ContactAttentionActivity.class.getName(),
						PangkerConstant.STATE_CHANGE_REFRESHUI);
			} else if (addAttent != null
					&& addAttent.getErrorCode() == BaseResult.FAILED) {
				showToast(addAttent.getErrorMessage());
			} else
				showToast(R.string.add_attent_fail);

		}
		// >>>>>>>>>查询附近好友
		else if (caseKey == LOAD_NEAR_FRIEND) {

			SearchNearFriendsResult neatFriend = JSONUtil.fromJson(
					supportResponse.toString(), SearchNearFriendsResult.class);
			if (neatFriend == null) {
				showToast(R.string.to_server_fail);
				return;
			} else if (neatFriend.getErrorCode() == BaseResult.SUCCESS) {
				List<UserItem> userList = neatFriend.getUsers();
				if (userList != null) {

					mPedestrianList = userList;
					if (userList.size() == 0) {
						iv_empty_icon
								.setImageResource(R.drawable.emptylist_icon);
						mEmptyTextView.setText(R.string.no_near_friends_prompt);
						// >>>>>>>>>>本地缓存数据
						// sp_Util.saveString(TAG + "friend",
						// supportResponse.toString());
					}
				} else {
					iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
					mEmptyTextView.setText(R.string.no_near_friends_prompt);
					mPedestrianList = new ArrayList<UserItem>();
				}

				if (currListType == LIST_TYPE) {

					mPedestriansAdapter.setmUserList(mPedestrianList);
					// mPedestrianListView.setAdapter(mPedestriansAdapter);
					mPedestriansAdapter.notifyDataSetChanged();

				} else if (currListType == GRID_TYPE) {
					nearUserGVAdapter.setmUserList(mPedestrianList);
					// mPedestrianListView.setAdapter(nearUserGVAdapter);
					nearUserGVAdapter.notifyDataSetChanged();
				}

			} else if (neatFriend.getErrorCode() == BaseResult.FAILED) {
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				mEmptyTextView.setText(R.string.no_near_friends_prompt);
			}

		}
	}
}
