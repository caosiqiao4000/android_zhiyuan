package com.wachoo.pangker.activity.pangker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.activity.group.OtherGroupsActivity;
import com.wachoo.pangker.adapter.ContactsSelectedAdapter;
import com.wachoo.pangker.adapter.TraceMemberAdapter;
import com.wachoo.pangker.adapter.TraceMemberAdapter.ViewHolder;
import com.wachoo.pangker.api.TabBroadcastManager;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupEnterCheckResult;
import com.wachoo.pangker.server.response.SearchRelativeResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.HorizontalListView;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.util.Util;

public class RelativesManagerActivity extends CommonPopActivity implements IUICallBackInterface {

	private final int PHASE_TRACE = 0x104;
	private final int DEL_TRACE = 0x103;

	private IFriendsDao iFriendsDao;
	private PangkerApplication application;
	private TabBroadcastManager tabBroadcastManager;

	private PullToRefreshListView pullToRefreshListView;
	private ListView traceMemberList;
	private EmptyView emptyView;
	private FooterView mFooterView;
	private TraceMemberAdapter mFriendsAdapter;

	private HorizontalListView mHorizontalListView;// 下方选择头像区域
	private ContactsSelectedAdapter selectedAdapter; // 选中的人员

	private Button btnConfrim;
	private Button btnCancel;
	private Button btnAll;

	private String optionType;
	private UserGroup mUserGroup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trace_addmember);
		application = (PangkerApplication) getApplication();
		iFriendsDao = new FriendsDaoImpl(this);
		tabBroadcastManager = PangkerManager.getTabBroadcastManager();
		optionType = this.getIntent().getStringExtra("optionType");
		if (optionType != null && optionType.equals("inviteToTraceRoom")) {
			mUserGroup = (UserGroup) getIntent().getSerializableExtra("UserGroup");
		}
		initView();
		showToast("如果没有正确显示您的亲友列表，可尝试下拉刷新");
		initData();
	}

	private void initView() {
		// TODO Auto-generated method stub
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_contacts);
		pullToRefreshListView.setOnRefreshListener(onRefreshListener);
		traceMemberList = pullToRefreshListView.getRefreshableView();
		mFooterView = new FooterView(this);
		mFooterView.addToListView(traceMemberList);
		
		mFriendsAdapter = new TraceMemberAdapter(this, new ArrayList<UserItem>());
		traceMemberList.setAdapter(mFriendsAdapter);
		traceMemberList.setOnItemClickListener(vOnItemClickListener);
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		btnCancel = (Button) findViewById(R.id.btn_back);
		if (optionType != null && optionType.equals("inviteToTraceRoom")) {
			findViewById(R.id.btn_layout).setVisibility(View.GONE);
			findViewById(R.id.invite_layout).setVisibility(View.VISIBLE);

			btnConfrim = (Button) findViewById(R.id.btn_select_over);
			txtTitle.setText("邀请群聊");

			mHorizontalListView = (HorizontalListView) findViewById(R.id.lv_selected_members);
			selectedAdapter = new ContactsSelectedAdapter(this);
			mHorizontalListView.setAdapter(selectedAdapter);
		} else {
			findViewById(R.id.btn_layout).setVisibility(View.VISIBLE);
			findViewById(R.id.invite_layout).setVisibility(View.GONE);

			btnConfrim = (Button) findViewById(R.id.traceIn_confrim);
			txtTitle.setText("亲友列表");
		}

		btnConfrim.setOnClickListener(mClickListener);
		btnCancel.setOnClickListener(mClickListener);

		btnAll = (Button) findViewById(R.id.iv_more);
		btnAll.setBackgroundResource(R.drawable.btn_default_selector);
		btnAll.setText("全选");
		btnAll.setOnClickListener(mClickListener);

		emptyView = new EmptyView(this, false);
		emptyView.addToListView(traceMemberList);
		emptyView.setBtnOne("添加亲友", new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				addTraceMembers();
			}
		});
		emptyView.showView(R.drawable.emptylist_icon, "您还没有亲友，快去添加一些亲友吧！");
	}

	private void initData() {
		// TODO Auto-generated method stub
		mFriendsAdapter.setMembers(application.getTraceList());
		if (application.getTraceList().size() > 0) {
			findViewById(R.id.btn_layout).setVisibility(View.VISIBLE);
			btnConfrim.setEnabled(true);
			btnAll.setEnabled(true);
		} else {
			findViewById(R.id.btn_layout).setVisibility(View.GONE);
			btnConfrim.setEnabled(false);
			btnAll.setEnabled(false);
		}
	}

	PullToRefreshBase.OnRefreshListener<ListView> onRefreshListener = new PullToRefreshBase.OnRefreshListener<ListView>() {
		@Override
		public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			// TODO Auto-generated method stub
			phraseRelations();
		}
	};

	private OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			if (view == btnConfrim) {
				if (optionType != null && optionType.equals("inviteToTraceRoom")) {
					if (mFriendsAdapter.getSeletedMembers().size() > 0) {
						checkGroupIn(mUserGroup.getGroupId());
					} else
						showToast("请选择亲友!");
				} else
					submitMembers();
			}
			if (view == btnCancel) {
				RelativesManagerActivity.this.finish();
			}
			if (view == btnAll) {
				if (mFriendsAdapter.isSelectedAll()) {
					mFriendsAdapter.setSelectedAll(false);
					btnAll.setText("全选");
				} else {
					btnAll.setText("反选");
					mFriendsAdapter.setSelectedAll(true);
				}
				changeCount();
			}
		}
	};

	private void phraseRelations() {
		// TODO Auto-generated method stub
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "pangker"));
		paras.add(new Parameter("uid", application.getMyUserID()));
		serverMana.supportRequest(Configuration.getSearchRelative(), paras, PHASE_TRACE);
		pullToRefreshListView.setRefreshing(true);
	}

	private void checkGroupIn(final String groupId) {
		if (application.getCurrunGroup() != null // 3：就在当前的群组中
				&& application.getCurrunGroup().getUid() != null
				&& application.getCurrunGroup().getGroupId().equals(groupId)) {
			gotoRoom(null);
		} else if (application.getCurrunGroup() != null // 2：在一个群组中，但不在点击的群组
				&& application.getCurrunGroup().getUid() != null
				&& !application.getCurrunGroup().getGroupId().equals(groupId)) {
			MessageTipDialog mDialog = new MessageTipDialog(new DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-genaerated method stub
					if (isSubmit) {
						// 首先要关闭之前的群组界面
						PangkerManager.getActivityStackManager().popOneActivity(ChatRoomMainActivity.class);
						groupEnterCheck(groupId);
					}
				}
			}, this);
			mDialog.showDialog("", "您已经在一个群组里面,进入此群组会退出当前所在的群组,是否确认进入?", false);
		} else {// 1:自己不在任何一个群组中
			groupEnterCheck(groupId);
		}
	}

	/**
	 * 进群记录
	 */
	private void groupEnterCheck(String groupId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", application.getMyUserID()));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("optype", "2"));//
		// 1：申请进入群组（必填）；2:被邀请加入；3：直接进入（自己群组，收藏的群组等等 ）
		paras.add(new Parameter("password", ""));// 群组密码，需要密码校验时使用
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				GroupEnterCheckResult result = JSONUtil.fromJson(response.toString(),
						GroupEnterCheckResult.class);
				if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
					UserGroup enterGroup = result.getUserGroup();
					if (enterGroup != null) {
						enterGroup.setLastVisitTime(Util.date2Str(new Date()));
						gotoRoom(enterGroup);
					} else {
						showToast("查询失败,还无法登录该群组!");
					}
				} else {
					showToast(R.string.return_value_999);
				}
			}
		});
		serverMana.supportRequest(Configuration.getGroupEnterCheck(), paras, true, "正在进入群组...");
	}

	private void gotoRoom(UserGroup userGroup) {
		Intent intent = new Intent();
		// >>>>>wangxin add 进入群组
		if (userGroup != null) {
			intent.putExtra(ChatRoomMainActivity.JION_TO_GROUP, userGroup);
			// 将进入群组的消息纪律到MsgInfo中
			application.getMsgInfoBroadcast().sendMeetroom(userGroup);
		} else {
			application.getMsgInfoBroadcast().sendMeetroom(application.getCurrunGroup());
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		}
		intent.setClass(this, ChatRoomMainActivity.class);
		startActivity(intent);
		// 进入之前进行邀请动作
		inviteGroup();
	}

	/**
	 * 邀请亲友到群聊 void TODO
	 * 
	 * @param usergroup
	 */
	private void inviteGroup() {
		OpenfireManager manager = application.getOpenfireManager();
		if (!application.getOpenfireManager().isLoggedIn()) {
			showToast(R.string.not_logged_cannot_operating);
			return;
		}
		// 邀请操作msg===>Id#groupname
		String msg = mUserGroup.getGroupId() + "#" + mUserGroup.getGroupName();
		for (UserItem user : mFriendsAdapter.getSeletedMembers()) {
			manager.sendGroupInvite(application.getMyUserID(), user.getUserId(), msg);
		}
		showToast("已成功发送邀请消息!");
		finish();
	}

	private void addTraceMembers() {
		// TODO Auto-generated method stub
		if (application.getTraceList().size() >= PangkerConstant.TRACEIN_LIMIT) {
			showToast("亲友人数已经超出限制!");
			return;
		}
		Intent intent = new Intent(this, ContactsSelectActivity.class);
		intent.putExtra(ContactsSelectActivity.INTENT_SELECT, ContactsSelectActivity.TYPE_TRACE_RELATIVES);
		startActivity(intent);
	}

	AdapterView.OnItemClickListener vOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			ViewHolder vHollder = (ViewHolder) view.getTag();
			vHollder.selected.toggle();
			mFriendsAdapter.selectByIndex(position - 1, vHollder.selected.isChecked());
			if (optionType != null && optionType.equals("inviteToTraceRoom")) {
				selectedAdapter.dealUserItem(application.getTraceList().get(position - 1),
						vHollder.selected.isChecked());
			}
			changeCount();
		}
	};

	public class MyHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			if (msg.what == 1) {
				Intent intent = new Intent(RelativesManagerActivity.this, OtherGroupsActivity.class);
				intent.putExtra("fromType", RelativesManagerActivity.class.getName());
				intent.putExtra(UserItem.USERID, application.getMyUserID());
				RelativesManagerActivity.this.startActivity(intent);
			}
			super.handleMessage(msg);
		}

	}

	private void changeCount() {
		// TODO Auto-generated method stub
		if (optionType != null && optionType.equals("inviteToTraceRoom")) {
			String countString = "邀请(" + (mFriendsAdapter.getSelectCount()) + ")";
			btnConfrim.setText(countString);
		} else {
			String countString = "删除(" + (mFriendsAdapter.getSelectCount()) + ")";
			btnConfrim.setText(countString);
		}
		if (mFriendsAdapter.getSelectCount() == mFriendsAdapter.getCount()) {
			btnAll.setText("反选");
		} else {
			btnAll.setText("全选");
		}
	}

	private String getRuids() {
		String ruid = "";
		for (UserItem userItem : mFriendsAdapter.getSeletedMembers()) {
			ruid += userItem.getUserId() + ",";
		}
		return ruid.substring(0, ruid.length() - 1);
	}

	private void submitMembers() {
		// TODO Auto-generated method stub
		if (mFriendsAdapter.getSelectCount() > 0) {
			ServerSupportManager serverMana = new ServerSupportManager(this, this);
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", "pangker"));
			paras.add(new Parameter("uid", application.getMyUserID()));
			paras.add(new Parameter("ruid", getRuids()));
			paras.add(new Parameter("type", "1"));
			serverMana.supportRequest(Configuration.getRelativeManager(), paras, true, "正在提交，请稍后...",
					DEL_TRACE);
		} else {
			showToast("请选择一个用户!");
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		pullToRefreshListView.onRefreshComplete();
		if (!HttpResponseStatus(response)) {
			return;
		}
		if (caseKey == DEL_TRACE) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				for (UserItem userItem : mFriendsAdapter.getSeletedMembers()) {
					iFriendsDao.updateRelation(userItem.getUserId(), "1");
					application.removeTraceklist(userItem.getUserId());
				}
				showToast("操作成功!");
				Message msg = new Message();
				msg.what = 1;
				// msg.obj = uid;
				tabBroadcastManager.sendResultMessage(RelativesDynamicActivity.class.getName(), msg);
				finish();
			} else {
				dealFailResult(result);
			}
		}
		// 亲友同步
		else if (caseKey == PHASE_TRACE) {
			SearchRelativeResult result = JSONUtil.fromJson(response.toString(), SearchRelativeResult.class);
			if (result != null && result.errorCode == BaseResult.SUCCESS) {
				phaseTraces(result);
			} else {
				emptyView.showView(R.drawable.emptylist_icon, R.string.no_relatives_prompt);
				if (result != null) {
					emptyView.showView(R.drawable.emptylist_icon, R.string.no_relatives_prompt);
				} else {
					emptyView.showView(R.drawable.emptylist_icon, R.string.return_value_999);
				}
			}
		}
	}

	/**
	 * 根据后台的返回进行同步,如果后台总数和
	 * 
	 * @param result
	 */
	private void phaseTraces(SearchRelativeResult result) {
		// TODO Auto-generated method stub
		if (result.getUsers() == null || result.getUsers().size() <= 0) {
			emptyView.showView(R.drawable.emptylist_icon, R.string.no_relatives_prompt);
			return;
		}
		if (isPhaseTrace(result.getUsers())) {
			phaseApplicationTraceList(result.getUsers(), false);
		} else {
			phaseApplicationTraceList(result.getUsers(), true);
		}
	}

	/**
	 * 判断服务器和本地是否同步
	 * 
	 * @param users
	 * @return
	 */
	private boolean isPhaseTrace(List<UserItem> users) {
		if (application.getTraceList().size() != users.size()) {
			return false;
		}
		for (UserItem user : users) {
			if (!application.IsTracelistExist(user.getUserId())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 对数据库和application的亲友进行同步，application的同步在数据库中已经做了
	 * isClear:如果前后台的数据不一致，便先清理本地亲友数据，否则不更新关系!
	 * 
	 * @param users
	 */
	private void phaseApplicationTraceList(List<UserItem> users, boolean isClear) {
		if (isClear) {
			if (iFriendsDao.clearTraceUsers(application.getMyUserID())) {
				for (UserItem userInfo : users) {
					iFriendsDao.updateRelation(userInfo.getUserId(), "2");
				}
			}
		}
		application.setTraceList(iFriendsDao.getTracelist(application.getMyUserID()));
		initData();
	}
}
