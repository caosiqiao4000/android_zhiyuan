package com.wachoo.pangker.activity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.contact.FriendAddActivity;
import com.wachoo.pangker.activity.contact.FriendGroupActivity;
import com.wachoo.pangker.activity.contact.LocalContactsSelectActivity;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.activity.msg.UserRecommendActivity;
import com.wachoo.pangker.activity.pangker.RelativesDynamicActivity;
import com.wachoo.pangker.adapter.ContactsSelectAdapter;
import com.wachoo.pangker.adapter.ContactsSelectedAdapter;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.db.IFriendsDao;
import com.wachoo.pangker.db.impl.FriendsDaoImpl;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupEnterCheckResult;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.HorizontalListView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.util.Util;

/**
 * 邀请好友的界面
 * @author zxx
 * 
 */
public class ContactsSelectActivity extends CommonPopActivity implements IUICallBackInterface {

	public static final String INTENT_SELECT = "INTENT_SELECT";
	public static final int TYPE_TRACE_RELATIVES = 0x01; // 亲友选择
	public static final int TYPE_RES_RECOMMEND = 0x02; // 资源推荐
	public static final int TYPE_ADDFRIEND_GROUP = 0x03; // 添加好友到分组
	public static final int TYPE_GROUP_INVITE = 0x04; // 邀请好友进入群组(进入之后)
	public static final int TYPE_GROUP_INVITE_ENTER = 0x05; // 邀请好友进入群组(进入之前)
	public static final int TYPE_UNIT_INVITE = 0x06; // 邀请好友到单位通讯录
	public static final int TYPE_ADDATTENT_GROUP = 0x07; // 添加关注人到分组
	public static final int TYPE_FRIEND_SELECT = 0x08; // 选择一个联系人(好友)

	public static final int TYPE_GROUP_UTITS_INVITE_ENTER = 0x08; // >>>>>>邀请单位通讯录成员加入群组
	public static final String INVITE_BUNDLE = "INVITE_BUNDLE"; // >>>>>>邀请单位通讯录成员加入群组
	public static final String INVITE_MEMBSER = "INVITE_MEMBSER"; // >>>>>>邀请单位通讯录成员加入群组

	// >>>>>>>邀请好友加入群组case，即将加入的群组
	private UserGroup jion_groupInfo;

	private PangkerApplication application;
	private String userId;
	private TextView tv_member_type;
	private TextView nodata_text;

	private Button btn_back; // back
	private Button btn_select_over; // btn_select_over
	private ImageView iv_arrow;
	private Button btn_local_contact; // 通讯录
	private EditText etSearch;

	private List<UserItem> memberList = new ArrayList<UserItem>();

	private ListView lv_members;
	private HorizontalListView lv_selected_members;// 下方选择头像区域

	private ContactsSelectedAdapter selectedAdapter; // 选中的人员
	private ContactsSelectAdapter memberAdapter;// 选择人员的列表

	private int dealtype;// 处理类型
	private int intentType;
	private ResShowEntity resShowEntity;
	private AddressBooks addressBooks;// 邀请好友到单位通讯录

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.select_members);

		dealtype = getIntent().getIntExtra(INTENT_SELECT, 0);
		// >>>>>>如果是邀请加入群组，获取群组信息
		if (dealtype == TYPE_GROUP_INVITE || dealtype == TYPE_GROUP_INVITE_ENTER
				|| dealtype == TYPE_GROUP_UTITS_INVITE_ENTER) {
			jion_groupInfo = (UserGroup) getIntent().getExtras().getSerializable("groupInfo");
		} else if (dealtype == TYPE_UNIT_INVITE) {
			addressBooks = (AddressBooks) this.getIntent().getSerializableExtra(AddressBooks.BOOK_KEY);
		}

		intentType = getIntent().getIntExtra("intentType", 0);
		resShowEntity = (ResShowEntity) getIntent().getSerializableExtra(ResShowEntity.RES_ENTITY);

		initView();
		initData();
	}

	/**
	 * 初始化数据，如果是亲友选择，要排除自己已经存在的亲友
	 * 
	 * @return
	 */
	private void initData() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		// 资源推荐
		if (dealtype == TYPE_RES_RECOMMEND || dealtype == TYPE_ADDFRIEND_GROUP
				|| dealtype == TYPE_GROUP_INVITE || dealtype == TYPE_GROUP_INVITE_ENTER
				|| dealtype == TYPE_UNIT_INVITE || dealtype == TYPE_FRIEND_SELECT) {
			memberList = application.getFriendList();
			tv_member_type.setText("我的好友");
		} else if (dealtype == TYPE_ADDATTENT_GROUP) {
			memberList = application.getAttentList();
			tv_member_type.setText("我的关注人");
		} else if (dealtype == TYPE_GROUP_UTITS_INVITE_ENTER) {
			memberList = (ArrayList<UserItem>) getIntent().getBundleExtra(INVITE_BUNDLE).getSerializable(INVITE_MEMBSER);
			tv_member_type.setText("单位成员");
		}
		// 亲友添加
		else if (dealtype == TYPE_TRACE_RELATIVES) {
			tv_member_type.setText("我的好友");
			nodata_text.setText(R.string.no_friend_addto_trace);
			for (UserItem userItem : application.getFriendList()) {
				if (!application.IsTracelistExist(userItem.getUserId())) {
					memberList.add(userItem);
				}
			}
			if (application.getFriendList().size() > 0 && memberList.size() == 0) {
				nodata_text.setText(R.string.no_friend_moreaddto_trace);
			} else
				nodata_text.setText(R.string.no_friend_addto_trace);
		}
		if(memberList.size() == 0){
			btn_select_over.setEnabled(false);
		}
		// 全部好友列表
		memberAdapter = new ContactsSelectAdapter(this, memberList);
		lv_members.setAdapter(memberAdapter);
		selectedAdapter = new ContactsSelectedAdapter(this);
		lv_selected_members.setAdapter(selectedAdapter);
	}

	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btn_back) {
				finish();
			}
			if (v == btn_select_over) {
				// 选人结束跳转到下一个界面
				if (selectedAdapter.getUserItems() == null || selectedAdapter.getUserItems().size() <= 0) {
					showToast("请选择联系人!");
				} else {
					dealByType();
				}
			}
			if (v == btn_local_contact) {
				if (dealtype == TYPE_TRACE_RELATIVES) {
					Intent mIntent = new Intent(ContactsSelectActivity.this, FriendAddActivity.class);
					ContactsSelectActivity.this.startActivity(mIntent);
				} else {
					Intent mIntent = new Intent(ContactsSelectActivity.this, LocalContactsSelectActivity.class);
					mIntent.putExtra("fromType", 1);
					mIntent.putExtra(ResShowEntity.RES_ENTITY, resShowEntity);
					ContactsSelectActivity.this.startActivity(mIntent);
				}
			}
		}
	};

	OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
			// TODO Auto-generated method stub
			if (dealtype == TYPE_TRACE_RELATIVES && isBeyond()) {
				showToast("超出人数限制!");
				return;
			}

			selectedAdapter.dealUserItem(memberList.get(position), memberAdapter.onCheckBoxClick(position));
			if (selectedAdapter.getCount() > 0) {
				if (dealtype == TYPE_TRACE_RELATIVES || dealtype == TYPE_ADDFRIEND_GROUP
						|| dealtype == TYPE_ADDATTENT_GROUP) {
					btn_select_over.setText("添加(" + (selectedAdapter.getCount() - 1) + ")");
				} else if (dealtype == TYPE_RES_RECOMMEND) {
					btn_select_over.setText("下一步(" + (selectedAdapter.getCount() - 1) + ")");
				} else if (dealtype == TYPE_GROUP_INVITE || dealtype == TYPE_GROUP_INVITE_ENTER
						|| dealtype == TYPE_GROUP_UTITS_INVITE_ENTER || dealtype == TYPE_UNIT_INVITE) {
					btn_select_over.setText("邀请(" + (selectedAdapter.getCount() - 1) + ")");
				} else {
					btn_select_over.setText("下一步(" + (selectedAdapter.getCount() - 1) + ")");
				}

			}
		}
	};

	OnItemClickListener donItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
			// TODO Auto-generated method stub
			String userId = selectedAdapter.removeUser(position);
			if (!Util.isEmpty(userId)) {
				memberAdapter.onUnCheckedClick(userId);
				if (selectedAdapter.getCount() > 0) {

					if (dealtype == TYPE_TRACE_RELATIVES || dealtype == TYPE_ADDFRIEND_GROUP
							|| dealtype == TYPE_ADDATTENT_GROUP) {
						btn_select_over.setText("添加(" + (selectedAdapter.getCount() - 1) + ")");
					} else if (dealtype == TYPE_RES_RECOMMEND) {
						btn_select_over.setText("下一步(" + (selectedAdapter.getCount() - 1) + ")");
					} else if (dealtype == TYPE_GROUP_INVITE || dealtype == TYPE_GROUP_INVITE_ENTER
							|| dealtype == TYPE_GROUP_UTITS_INVITE_ENTER || dealtype == TYPE_UNIT_INVITE) {
						btn_select_over.setText("邀请(" + (selectedAdapter.getCount() - 1) + ")");
					} else {
						btn_select_over.setText("下一步(" + (selectedAdapter.getCount() - 1) + ")");
					}
				}
			}

		}
	};

	private void initView() {
		// TODO Auto-generated method stub
		btn_back = (Button) findViewById(R.id.btn_back);
		iv_arrow = (ImageView) findViewById(R.id.icon_drow);
		btn_select_over = (Button) findViewById(R.id.btn_select_over);
		btn_local_contact = (Button) findViewById(R.id.btn_local_contact);
		btn_local_contact.setVisibility(View.GONE);

		btn_back.setOnClickListener(onClickListener);
		btn_select_over.setOnClickListener(onClickListener);
		btn_local_contact.setOnClickListener(onClickListener);

		lv_members = (ListView) findViewById(R.id.lv_members);
		lv_members.setOnItemClickListener(onItemClickListener);
		tv_member_type = (TextView) findViewById(R.id.tv_member_type);

		tv_member_type.setText("我的好友");
		View view = LayoutInflater.from(this).inflate(R.layout.empty_view, null, false);
		LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		((ViewGroup) lv_members.getParent()).addView(view, lp);
		nodata_text = (TextView) findViewById(R.id.textViewEmpty);
		lv_members.setEmptyView(view);
		// 选择的人的头像列表
		lv_selected_members = (HorizontalListView) findViewById(R.id.lv_selected_members);
		lv_selected_members.setOnItemClickListener(donItemClickListener);
		etSearch = (EditText) findViewById(R.id.et_file_name);
		etSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				searchUserList(s.toString().trim());
			}
		});
		initViewByType();
	}

	/**
	 * @param s
	 *            void
	 */
	protected void searchUserList(CharSequence s) {
		List<UserItem> searchList = new ArrayList<UserItem>();
		if (s == null || s.length() < 1) {
			searchList = memberList;
		}
		if (s != null && s.length() > 0) {
			for (UserItem contactUser : memberList) {
				if (contactUser.getUserName().contains(s)
						|| (contactUser.getrName() != null && contactUser.getrName().contains(s))) {
					searchList.add(contactUser);
				}
			}
		}
		memberAdapter.setMembers(searchList);
	}

	private String getRuids() {
		String ruid = "";
		for (UserItem userItem : selectedAdapter.getUserItems()) {
			ruid += userItem.getUserId() + ",";
		}
		return ruid.substring(0, ruid.length() - 1);
	}

	private boolean isBeyond() {
		// TODO Auto-generated method stub
		int traceIned = application.getTraceList().size();
		int selected = selectedAdapter.getCount() - 1;
		if ((traceIned + selected) > PangkerConstant.TRACEIN_LIMIT) {
			return true;
		}
		return false;
	}

	private void dealByType() {
		// TODO Auto-generated method stub
		if (dealtype == TYPE_TRACE_RELATIVES) {
			ServerSupportManager serverMana = new ServerSupportManager(this, this);
			List<Parameter> paras = new ArrayList<Parameter>();
			paras.add(new Parameter("appid", "pangker"));
			paras.add(new Parameter("uid", userId));
			paras.add(new Parameter("ruid", getRuids()));
			paras.add(new Parameter("type", "0"));
			serverMana.supportRequest(Configuration.getRelativeManager(), paras, true, "正在提交...", dealtype);
		}
		//资源推荐
		else if (dealtype == TYPE_RES_RECOMMEND) {
			Intent intent = new Intent(ContactsSelectActivity.this, UserRecommendActivity.class);
			intent.putExtra("selectedUser", (Serializable) selectedAdapter.getUserItems());
			intent.putExtra(ResShowEntity.RES_ENTITY, resShowEntity);
			if (intentType == 1) {
				setResult(RESULT_OK, intent);
			} else {
				ContactsSelectActivity.this.startActivity(intent);
			}
			finish();
		} else if (dealtype == TYPE_ADDFRIEND_GROUP || dealtype == TYPE_ADDATTENT_GROUP) {
			Intent intent = new Intent(ContactsSelectActivity.this, FriendGroupActivity.class);
			intent.putExtra("selectedUser", (Serializable) selectedAdapter.getUserItems());
			setResult(RESULT_OK, intent);
			finish();
		}
		// >>>>>>>>邀请用户加入群组
		else if (dealtype == TYPE_GROUP_INVITE) {
			if (selectedAdapter.getUserItems().size() < 1) {
				showToast(R.string.rootwho_null);
			} else if (selectedAdapter.getUserItems().size() > 200) {
				showToast(R.string.rootwho_spill);
			} else {
				inviteGroup();
			}
		} else if (dealtype == TYPE_GROUP_INVITE_ENTER || dealtype == TYPE_GROUP_UTITS_INVITE_ENTER) {
			checkGroupIn(jion_groupInfo.getGroupId());
		} else if (dealtype == TYPE_UNIT_INVITE) {
			inviteMembers();
		}  
	}

	private void inviteGroup() {
		// TODO Auto-generated method stub
		if (selectedAdapter.getUserItems().size() < 1) {
			showToast(R.string.rootwho_null);
		} else if (selectedAdapter.getUserItems().size() > 200) {
			showToast(R.string.rootwho_spill);
		} else {
			if (!application.getOpenfireManager().isLoggedIn()) {
				showToast(R.string.not_logged_cannot_operating);
				return;
			}
			// 邀请操作msg===>Id#groupname
			String msg = jion_groupInfo.getGroupId() + "#" + jion_groupInfo.getGroupName();
			// >>>>>>>>发送邀请通知
			for (int i = 0; i < selectedAdapter.getUserItems().size(); i++) {
				application.getOpenfireManager().sendGroupInvite(application.getMyUserID(),
						selectedAdapter.getUserItems().get(i).getUserId(), msg);
			}
			showToast("已成功发送邀请消息!");
		}
		finish();
	}

	public void gotoRoom(UserGroup userGroup) {
		Intent intent = new Intent();
		// >>>>>wangxin add 进入群组
		if (userGroup != null) {
			intent.putExtra(ChatRoomMainActivity.JION_TO_GROUP, userGroup);
			// 将进入群组的消息纪律到MsgInfo中
			application.getMsgInfoBroadcast().sendMeetroom(userGroup);
		} else {
			application.getMsgInfoBroadcast().sendMeetroom(application.getCurrunGroup());
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		}
		intent.setClass(this, ChatRoomMainActivity.class);
		startActivity(intent);
		// 进入之前进行邀请动作
		inviteGroup();
	}

	private void checkGroupIn(final String groupId) {
		if (application.getCurrunGroup() != null // 3：就在当前的群组中
				&& application.getCurrunGroup().getUid() != null
				&& application.getCurrunGroup().getGroupId().equals(groupId)) {
			gotoRoom(null);
		} else if (application.getCurrunGroup() != null // 2：在一个群组中，但不在点击的群组
				&& application.getCurrunGroup().getUid() != null
				&& !application.getCurrunGroup().getGroupId().equals(groupId)) {

			MessageTipDialog mDialog = new MessageTipDialog(new DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-genaerated method stub
					if (isSubmit) {
						// 首先要关闭之前的群组界面
						PangkerManager.getActivityStackManager().popOneActivity(ChatRoomMainActivity.class);
						groupEnterCheck(groupId);
					}
				}
			}, this);
			mDialog.showDialog("您已经在一个群组里面,进入此群组会退出当前所在的群组,是否确认进入?", false);
		} else {// 1:自己不在任何一个群组中
			groupEnterCheck(groupId);
		}
	}

	/**
	 * 进群记录
	 */
	private void groupEnterCheck(String groupId) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", application.getMyUserID()));
		paras.add(new Parameter("groupid", groupId));
		paras.add(new Parameter("optype", "2"));//
		// 1：申请进入群组（必填）；2:被邀请加入；3：直接进入（自己群组，收藏的群组等等 ）
		paras.add(new Parameter("password", ""));// 群组密码，需要密码校验时使用
		ServerSupportManager serverMana = new ServerSupportManager(this, new IUICallBackInterface() {
			@Override
			public void uiCallBack(Object response, int caseKey) {
				// TODO Auto-generated method stub
				GroupEnterCheckResult result = JSONUtil.fromJson(response.toString(),
						GroupEnterCheckResult.class);
				if (result != null && result.getErrorCode() == BaseResult.SUCCESS) {
					UserGroup enterGroup = result.getUserGroup();
					if (enterGroup != null) {
						enterGroup.setLastVisitTime(Util.date2Str(new Date()));
						gotoRoom(enterGroup);
					} else {
						showToast("查询失败,还无法登录该群组!");
					}
				} else {
					showToast(R.string.return_value_999);
				}
			}
		});
		serverMana.supportRequest(Configuration.getGroupEnterCheck(), paras, true, "正在进入群组...", 0);
	}

	private void initViewByType() {
		// TODO Auto-generated method stub

		// public static final int TYPE_TRACE_relatives = 0x01; // 亲友选择
		// public static final int TYPE_RES_recommend = 0x02; // 资源推荐
		// public static final int TYPE_ADDFRIEND_GROUP = 0x03; // 添加好友到分组
		// public static final int TYPE_GROUP_INVITE = 0x04; // 邀请好友进入群组(进入之后)
		// public static final int TYPE_GROUP_INVITE_ENTER = 0x05; //
		// 邀请好友进入群组(进入之前)
		// public static final int TYPE_UNIT_INVITE = 0x06; // 邀请好友到单位通讯录

		if (dealtype == TYPE_TRACE_RELATIVES) {
			btn_local_contact.setVisibility(View.GONE);
			btn_local_contact.setText("加好友");
			btn_select_over.setText("添加");
			iv_arrow.setVisibility(View.GONE);
		} else if (dealtype == TYPE_ADDFRIEND_GROUP) {
			btn_local_contact.setVisibility(View.GONE);
			btn_select_over.setText("添加");
		} else if (dealtype == TYPE_ADDATTENT_GROUP) {
			btn_local_contact.setVisibility(View.GONE);
			tv_member_type.setText("我的关注人");
			btn_select_over.setText("添加");
		} else if (dealtype == TYPE_RES_RECOMMEND) {
			btn_select_over.setText("下一步");
		} else if (dealtype == TYPE_GROUP_INVITE || dealtype == TYPE_GROUP_INVITE_ENTER
				|| dealtype == TYPE_GROUP_UTITS_INVITE_ENTER || dealtype == TYPE_UNIT_INVITE) {
			btn_select_over.setText("邀请");
		} else {
			btn_select_over.setText("下一步");
		}
	}

	private void inviteMembers() {
		// TODO Auto-generated method stub
		String uid = application.getMyUserID();
		final OpenfireManager openfireManager = application.getOpenfireManager();
		if (openfireManager.isLoggedIn()) {
			if (selectedAdapter.getCount() > 0) {
				for (UserItem userItem : selectedAdapter.getUserItems()) {
					openfireManager.sendNetAddressBookInviteReq(uid, userItem.getUserId(), addressBooks,
							"邀请加入Ta的单位通讯录");
				}
				showToast(R.string.net_addressbook_apply_mess);
				finish();
			}

		} else {
			showToast(R.string.not_logged_cannot_operating);
		}

	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			return;
		}
		if (caseKey == TYPE_TRACE_RELATIVES) {
			BaseResult result = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (result != null && BaseResult.SUCCESS == result.errorCode) {
				IFriendsDao iFriendsDao = new FriendsDaoImpl(this);
				for (UserItem userItem : selectedAdapter.getUserItems()) {
					iFriendsDao.updateRelation(userItem.getUserId(), "2");
					application.addTracelist(userItem);
				}
				showToast(result.getErrorMessage());
				Message msg = new Message();
				msg.what = 1;
				PangkerManager.getTabBroadcastManager().sendResultMessage(RelativesDynamicActivity.class.getName(), msg);
				finish();
			} else {
				if (result != null) {
					showToast(result.getErrorMessage());
				} else {
					showToast(R.string.res_getphoto_fail);
				}
			}
		}
	}

}
