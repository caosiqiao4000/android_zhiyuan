package com.wachoo.pangker.activity;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.activity.res.ResLocalActivity;
import com.wachoo.pangker.db.IResDao;
import com.wachoo.pangker.db.impl.ResDaoImpl;
import com.wachoo.pangker.entity.DirInfo;
import com.wachoo.pangker.reader.BookPageFactory;
import com.wachoo.pangker.reader.PageWidget;
import com.wachoo.pangker.util.ImageUtil;

public class DocReaderActivity extends CommonPopActivity {
	/** Called when the activity is first created. */
	private PageWidget mPageWidget;
	private Bitmap mCurPageBitmap, mNextPageBitmap;
	private Canvas mCurPageCanvas, mNextPageCanvas;
	private BookPageFactory pagefactory;
	private String filePath;
	private String mFileName;
	private int remark = 0;
	private int screenW;
	private int screenH;

	private float x_left;// >>>>>>>> wangxin 区间左
	private float x_right;// >>>>>>>> wangxin 区间右

	private PopupWindow topTools;
	private PopupWindow bottomTools;

	private ViewGroup topView;// 上部title
	private ViewGroup bottomView;// 下部view

	private String TAG = "DocReaderActivity";// log tag

	private ZoomControls mZoomControls;// >>>>>>放大缩小字体
	private EditText mCurrentPage; // >>>>>>>>当前页码
	private TextView mSumPage;// >>>>总页数
	private Button mJumpButton;// >>>跳转按钮
	private Bitmap rbg;
	private static final Logger logger = LoggerFactory.getLogger();

	private IResDao resDao;
	private DirInfo dirInfo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		mPageWidget = new PageWidget(this);
		setContentView(mPageWidget);

		resDao = new ResDaoImpl(this);
		// >>>>>>>>>> add by wangxin
		screenW = ImageUtil.getWidthPixels(this);
		screenH = ImageUtil.getHeightPixels(this);
		x_left = screenW - screenW / 5 * 3;
		x_right = x_left + screenW / 5;
		mCurPageBitmap = Bitmap.createBitmap(screenW, screenH, Bitmap.Config.ARGB_8888);
		mNextPageBitmap = Bitmap.createBitmap(screenW, screenH, Bitmap.Config.ARGB_8888);
		// >>>>>>>>>> add by wangxin

		mCurPageCanvas = new Canvas(mCurPageBitmap);
		mNextPageCanvas = new Canvas(mNextPageBitmap);

		rbg = ImageUtil.getBitmapScale(BitmapFactory.decodeResource(this.getResources(), R.drawable.reader_bg),
				screenW, screenH);

		dirInfo = (DirInfo) getIntent().getSerializableExtra("DirInfo");

		filePath = dirInfo.getPath();
		mFileName = dirInfo.getDirName();
		remark = dirInfo.getRemark();
		openBook(remark);
		initPopView();
	}

	private int m_fontSize = 25; // 默认字体大小
	private int m_zoomSize = 2;

	private void openBook(int position) {
		try {
			if (pagefactory != null)
				pagefactory.clear();
			pagefactory = new BookPageFactory(screenW, screenH);
			pagefactory.setBgBitmap(rbg);
			pagefactory.setM_fontSize(m_fontSize);
			pagefactory.init();
			pagefactory.openbook(filePath);
			pagefactory.setmPosition(position);
			handler.handleMessage(new Message());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(TAG, e);
			Toast.makeText(this, "文件不存在，请您确认！", Toast.LENGTH_SHORT).show();
		}

		mPageWidget.setBitmaps(mCurPageBitmap, mCurPageBitmap);
		mPageWidget.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {

				float x = e.getX();
				Log.i("DocOnTouch", "aciton = " + e.getAction() + " ;x = " + x + "; x_left = " + x_left
						+ "; x_right = " + x_right);
				if (e.getAction() == 0 && x_left <= x && x <= x_right) {
					if (topTools.isShowing()) {
						topTools.dismiss();
						bottomTools.dismiss();
					} else {
						showTopToolsPopwin();
					}
					return false;
				}

				boolean ret = false;
				if (v == mPageWidget) {
					if (e.getAction() == MotionEvent.ACTION_DOWN) {
						mPageWidget.abortAnimation();
						mPageWidget.calcCornerXY(e.getX(), e.getY());

						pagefactory.onDraw(mCurPageCanvas);

						Log.i(TAG, "pagefactory.onDraw(mCurPageCanvas)");
						if (mPageWidget.DragToRight()) {
							try {
								pagefactory.prePage();

								Log.i(TAG, "pagefactory.prePage()");
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								logger.error(TAG, e1);
							}
							if (pagefactory.isfirstPage())
								return false;

							pagefactory.onDraw(mNextPageCanvas);
							Log.i(TAG, "pagefactory.onDraw(mNextPageCanvas)");
						} else {
							try {
								pagefactory.nextPage();
							} catch (IOException e2) {
								// TODO Auto-generated catch block
								logger.error(TAG, e2);
							}
							if (pagefactory.islastPage())
								return false;
							pagefactory.onDraw(mNextPageCanvas);
						}
						mPageWidget.setBitmaps(mCurPageBitmap, mNextPageBitmap);
					}

					ret = mPageWidget.doTouchEvent(e);
					return ret;
				}
				return false;
			}
		});
	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			Log.i(TAG, "hander-message!");

			pagefactory.onDraw(mCurPageCanvas);
			mPageWidget.postInvalidate();

		};
	};

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// showTopToolsPopwin();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (topTools != null && topTools.isShowing())
			topTools.dismiss();
		int remark = pagefactory.getM_mbBufBegin();
		Log.d("remark", "===>" + remark);
		dirInfo.setRemark(remark);
		resDao.updateResInfo(dirInfo);
		Intent dintent = new Intent();
		dintent.putExtra("remark", remark);
		setResult(ResLocalActivity.RequestCode_Reader, dintent);
		super.onBackPressed();
	}

	private void initPopView() {
		// wangxin add start
		LayoutInflater mLayoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		topView = (ViewGroup) mLayoutInflater.inflate(R.layout.reader_title, null, true);
		Button btn_back = (Button) topView.findViewById(R.id.btn_back);
		TextView tv_title = (TextView) topView.findViewById(R.id.mmtitle);
		tv_title.setText(mFileName);

		btn_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				timerClosePopupWindow.cancel();
				onBackPressed();
				DocReaderActivity.this.finish();
			}
		});

		topTools = new PopupWindow(topView, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		topTools.update();

		bottomView = (ViewGroup) mLayoutInflater.inflate(R.layout.text_reader_toolbar, null, true);

		// >>>zoom text size
		mZoomControls = (ZoomControls) bottomView.findViewById(R.id.zc_textsize_zoom);
		// 缩小
		mZoomControls.setOnZoomInClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				m_fontSize = m_fontSize + m_zoomSize;
				openBook(pagefactory.getmPosition());
			}
		});

		// 放大
		mZoomControls.setOnZoomOutClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				m_fontSize = m_fontSize - m_zoomSize;
				openBook(pagefactory.getmPosition());
			}
		});
		// >>>>>>mCurrentPage
		mCurrentPage = (EditText) bottomView.findViewById(R.id.et_currentpage);
		// >>>>>>总页数
		mSumPage = (TextView) bottomView.findViewById(R.id.tv_pagecount);
		// >>>>跳转按钮
		mJumpButton = (Button) bottomView.findViewById(R.id.btn_jumpto);
		// 跳转到页面
		mJumpButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		bottomTools = new PopupWindow(bottomView, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		bottomTools.setBackgroundDrawable(new BitmapDrawable());
		bottomTools.update();

	}

	private Timer timerClosePopupWindow = new Timer();

	public void showTopToolsPopwin() {
		topTools.showAtLocation(mPageWidget, Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
		bottomTools.showAtLocation(mPageWidget, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				mHandler.sendEmptyMessage(1);
			}
		};
		timerClosePopupWindow.schedule(task, 3000);

	}

	Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			if (topTools != null && topTools.isShowing()) {
				topTools.dismiss();
			}

			if (bottomTools != null && bottomTools.isShowing()) {
				bottomTools.dismiss();
			}
		}

	};

	class MenuAdapter extends BaseAdapter {
		private int[] menu_name_array = { R.drawable.date_icon_lv, R.drawable.date_icon_lv, R.drawable.date_icon_lv,
				R.drawable.date_icon_lv };

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return menu_name_array.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View statusView = LayoutInflater.from(DocReaderActivity.this).inflate(R.layout.item_menu, null);
			ImageButton item_button = (ImageButton) statusView.findViewById(R.id.item_button);

			// 设置按钮文本

			item_button.setBackgroundResource(menu_name_array[position]);
			return statusView;
		}
	}

}
