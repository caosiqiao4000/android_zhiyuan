package com.wachoo.pangker.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.amap.mapapi.map.MapActivity;
import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.ActivityStackManager;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.server.HttpReqCode;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.SharedPreferencesUtil;
import com.wachoo.pangker.util.Util;

public class CommonMapActivity extends MapActivity {
	private ActivityStackManager stackManager;

	protected String TAG = "CommonPopActivity";// log tag

	protected static final Logger logger = LoggerFactory.getLogger();

	/**
	 * 
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		stackManager = PangkerManager.getActivityStackManager();
		stackManager.pushActivity(this);
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC); // 让音量键固定为媒体音量控制
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		stackManager.popActivity(this);
	}

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public void launch(Context From, Class<Activity> To, int StartFlag) {
		Util.launch(From, To, StartFlag);
	}

	/**
	 * 根据启动条件来启动activity
	 * 
	 * @param From
	 * @param To
	 * @param StartFlag
	 */
	public void launch(Intent intent, Context From) {
		Util.launch(intent, From);
	}

	/**
	 * 直接启动activity
	 * 
	 * @param From
	 * @param To
	 */
	public void launch(Context From, Class<Activity> To) {
		Util.launch(From, To);
	}

	/**
	 * 根据字符串显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final String toastText) {
		Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
	}

	protected void launch(Class<?> toClass) {
		Intent intent = new Intent(this, toClass);
		startActivity(intent);
	}

	/**
	 * 根据Id显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final int toastText) {
		Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 取得资源文件的message
	 * 
	 * @param id
	 * @return
	 */
	public String getResourcesMessage(int id) {
		return getResources().getString(id);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public String getRawText(int id) {
		return Util.getRawText(this, id);
	}

	public String getStringByID(int id) {
		return getResources().getString(id);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	public void dealFailResult(BaseResult result) {
		if (result != null) {
			showToast(result.getErrorMessage());
		} else {
			showToast(R.string.res_getphoto_fail);
		}
	}

	public boolean HttpResponseStatus(Object supportResponse) {
		if (supportResponse == null) {
			showToast(R.string.to_server_fail);
			return false;
		}
		if (supportResponse instanceof HttpReqCode) {
			HttpReqCode httpReqCode = (HttpReqCode) supportResponse;
			if (httpReqCode.equals(HttpReqCode.no_network)) {
				showToast("请检查您的网络！");
			} else if (httpReqCode.equals(HttpReqCode.error)) {
				showToast(R.string.to_server_fail);
			}
			return false;
		} else {
			return true;
		}
	}

	/**
	 * void TODO 设置新注册用户引导
	 * 
	 * @param className
	 * @param resDrawableId
	 */
	public void addGuideImage(final String className, int resDrawableId) {
		View view = getParent().findViewById(R.id.my_content_view);
		// View view =
		// getWindow().getDecorView().findViewById(resounceView);//查找通过setContentView上的根布局
		if (view == null)
			return;
		if (SharedPreferencesUtil.activityIsGuided(this, className)) {
			// 引导过了
			return;
		}
		ViewParent viewParent = view.getParent();
		if (viewParent instanceof FrameLayout) {
			final FrameLayout frameLayout = (FrameLayout) viewParent;
			if (resDrawableId != 0) {// 设置了引导图片
				final ImageView guideImage = new ImageView(this);
				FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.FILL_PARENT);
				guideImage.setLayoutParams(params);
				guideImage.setScaleType(ScaleType.FIT_XY);
				guideImage.setImageResource(resDrawableId);
				guideImage.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						frameLayout.removeView(guideImage);
						SharedPreferencesUtil.setIsGuided(getApplicationContext(), className);// 设为已引导
					}
				});
				frameLayout.addView(guideImage);// 添加引导图片
			}
		}
	}

	protected void hideSoftInput(View v) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}
}
