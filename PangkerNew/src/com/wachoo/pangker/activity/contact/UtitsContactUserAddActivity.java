package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.listener.TextCountLimitWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBookResult;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.AddressMemberCheckResult;
import com.wachoo.pangker.util.Util;

/**
 * 添加单位通讯录成员(非旁客用户)
 * @author zhengjy
 *         2012-6-15
 * 
 */
public class UtitsContactUserAddActivity extends CommonPopActivity implements IUICallBackInterface {

	private final static int CONTACT_USER_ADD = 0x10;
	private final static int CONTACT_USER_CHECK = 0x11;
	private final static int SELECT_REQUESTCODE = 0x11;

	private PangkerApplication application;
	private Button btnSubmit;
	private EditText etName;
	private EditText etMobile;
	private EditText etEmail;
	private EditText etAddress;
	private EditText etZipcode;
	private EditText etDepartment;

	private TextView tvTitle;
	private LinearLayout mLayoutMobile1;
	private LinearLayout mLayoutMobile2;
	private LinearLayout mLayoutMobile3;
	private Set<LinearLayout> mobileLists = new HashSet<LinearLayout>();
	private Button btnAddMobile;
	private Button btnAddContact;

	private String mUserid;
	private AddressBooks addressbooks;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.units_contacts_user_add);
		application = (PangkerApplication) getApplication();
		mUserid = application.getMyUserID();
		addressbooks = (AddressBooks) this.getIntent().getSerializableExtra(AddressBooks.BOOK_KEY);

		initView();
	}

	private void initView() {

		btnSubmit = (Button) findViewById(R.id.btn_submit);
		tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText(R.string.units_title_contact_useradd);
		etName = (EditText) findViewById(R.id.et_name);
		etMobile = (EditText) findViewById(R.id.et_mobile);
		etEmail = (EditText) findViewById(R.id.et_email);
		etAddress = (EditText) findViewById(R.id.et_address);
		etZipcode = (EditText) findViewById(R.id.et_zipcode);
		etDepartment = (EditText) findViewById(R.id.et_department);
		btnAddContact = (Button) findViewById(R.id.iv_more);
		btnAddContact.setBackgroundResource(R.drawable.btn_add_content_selector);
		mLayoutMobile1 = (LinearLayout) findViewById(R.id.layout_mobile1);
		mLayoutMobile2 = (LinearLayout) findViewById(R.id.layout_mobile2);
		mLayoutMobile3 = (LinearLayout) findViewById(R.id.layout_mobile3);
		btnAddMobile = (Button) findViewById(R.id.btn_add_more_info);

		etName.addTextChangedListener(new TextCountLimitWatcher(18, etName));
		etMobile.addTextChangedListener(new TextCountLimitWatcher(11, etMobile));
		etEmail.setFilters(new InputFilter[] { new InputFilter.LengthFilter(100) });
		etAddress.setFilters(new InputFilter[] { new InputFilter.LengthFilter(100) });
		etZipcode.addTextChangedListener(new TextCountLimitWatcher(10, etZipcode));
		etDepartment.addTextChangedListener(new TextCountLimitWatcher(100, etDepartment));

		btnSubmit.setOnClickListener(mClickListener);
		btnAddMobile.setOnClickListener(mClickListener);
		setClearClickListener(mLayoutMobile1);
		setClearClickListener(mLayoutMobile2);
		setClearClickListener(mLayoutMobile3);
		btnAddContact.setOnClickListener(mClickListener);
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UtitsContactUserAddActivity.this.finish();
			}
		});

	}

	private final OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// TODO Auto-generated method stub
			if (view == btnAddMobile) {
				if (mobileLists.size() >= 3) {
					showToast(R.string.contact_add_number_nomore);
					return;
				}
				if (mobileLists.add(mLayoutMobile1)) {
					mLayoutMobile1.setVisibility(View.VISIBLE);
				} else {
					if (mobileLists.add(mLayoutMobile2)) {
						mLayoutMobile2.setVisibility(View.VISIBLE);
					} else if (mobileLists.add(mLayoutMobile3)) {
						mLayoutMobile3.setVisibility(View.VISIBLE);
					}
				}
			} else if (view == btnSubmit) {
				AddressAddMemberCheck();
			} else if (view == btnAddContact) {
				Intent intent = new Intent(UtitsContactUserAddActivity.this,
						LocalContactsSelectActivity.class);
				intent.putExtra("fromType", 2);
				UtitsContactUserAddActivity.this.startActivityForResult(intent, SELECT_REQUESTCODE);
			}
		}
	};

	private void setClearClickListener(final LinearLayout view) {
		ImageView btnClear = (ImageView) view.findViewById(R.id.iv_item_clear);
		final EditText etMobileAdd = (EditText) view.findViewById(R.id.et_mobile_add);
		etMobileAdd.addTextChangedListener(new TextCountLimitWatcher(11, etMobileAdd));
		btnClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				view.setVisibility(View.GONE);
				etMobileAdd.setText("");
				mobileLists.remove(view);
			}
		});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (SELECT_REQUESTCODE == requestCode && RESULT_OK == resultCode) {
			LocalContacts contact = (LocalContacts) data
					.getSerializableExtra(LocalContacts.LOCALCONTACTS_KEY);
			if (contact != null) {
				etName.setText(contact.getUserName());
				etMobile.setText(contact.getPhoneNum());
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 添加单位通讯录成员（非旁客用户）
	 * 
	 * 
	 * 接口：AddressBookAddMember.json
	 */
	private void AddressBookAddMember() {
		String name = etName.getText().toString();
		if (Util.isEmpty(name)) {
			showToast(R.string.units_name_cannotnull);
			return;
		}
		String mobile = etMobile.getText().toString();
		if (Util.isEmpty(mobile)) {
			showToast(R.string.units_mobile_cannotnull);
			return;
		}
		String phone1 = "";
		String phone2 = "";
		String phone3 = "";
		if (mobileLists.contains(mLayoutMobile1) || mobileLists.contains(mLayoutMobile2)
				|| mobileLists.contains(mLayoutMobile3)) {
			final Iterator<LinearLayout> iter = mobileLists.iterator();
			int i = 0;
			while (iter.hasNext()) {
				LinearLayout item = (LinearLayout) iter.next();
				EditText etPhone = (EditText) item.findViewById(R.id.et_mobile_add);
				if (i == 0) {
					phone1 = etPhone.getText().toString();
				}
				if (i == 1) {
					phone2 = etPhone.getText().toString();
				}
				if (i == 2) {
					phone3 = etPhone.getText().toString();
				}
				i++;
			}
		}
		String email = etEmail.getText().toString();
		String address = etAddress.getText().toString();
		String zipcode = etZipcode.getText().toString();
		String department = etDepartment.getText().toString();
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", mUserid));// 操作者Uid.
		paras.add(new Parameter("remarkname", name));// 用户备注名称
		paras.add(new Parameter("bookid", String.valueOf(addressbooks.getId())));// 所属单位通讯录。
		paras.add(new Parameter("mobile", mobile));// 手机号码（主号码，关联使用）
		paras.add(new Parameter("phone1", phone1));//
		paras.add(new Parameter("phone2", phone2));
		paras.add(new Parameter("phone3", phone3));//
		paras.add(new Parameter("email", email));//
		paras.add(new Parameter("address", address));
		paras.add(new Parameter("zipcode", zipcode));// 邮编
		paras.add(new Parameter("department", department));// 邮编
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getAddressBookAddMember(), paras, true, mess,
				CONTACT_USER_ADD);
	}

	/**
	 * 校验是否旁课用户
	 * 
	 * 
	 * 接口：AddressAddMemberCheck.json
	 */
	private void AddressAddMemberCheck() {
		String name = etName.getText().toString();
		if (Util.isEmpty(name)) {
			showToast(R.string.units_name_cannotnull);
			return;
		}
		String mobile = etMobile.getText().toString();
		if (Util.isEmpty(mobile)) {
			showToast(R.string.units_mobile_cannotnull);
			return;
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", mUserid));// 操作者Uid.
		paras.add(new Parameter("bookid", String.valueOf(addressbooks.getId())));// 所属单位通讯录。
		paras.add(new Parameter("mobile", mobile));// 手机号码（主号码，关联使用）
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getAddressAddMemberCheck(), paras, true, mess, CONTACT_USER_CHECK);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		if (!HttpResponseStatus(supportResponse))
			return;
		if (caseKey == CONTACT_USER_ADD) {

			AddressBookResult baseResult = JSONUtil.fromJson(supportResponse.toString(),
					AddressBookResult.class);
			if (null == baseResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (baseResult.getErrorCode() == baseResult.SUCCESS) {
				String mobile = etMobile.getText().toString();
				sendMsg(mobile);
				showToast(R.string.add_success);
				this.finish();
			} else if (baseResult.getErrorCode() == baseResult.FAILED) {
				showToast(baseResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		} else if (caseKey == CONTACT_USER_CHECK) {
			AddressMemberCheckResult baseResult = JSONUtil.fromJson(supportResponse.toString(),
					AddressMemberCheckResult.class);
			if (null == baseResult) {
				showToast(R.string.to_server_fail);
				return;
			}
			// 处理成功，该号码已经存在通讯录中。
			else if (baseResult.getErrorCode() == baseResult.SUCCESS) {
				showToast(baseResult.getErrorMessage());
			}
			// 2：处理成功，为旁客用户，走邀请流程
			else if (baseResult.getErrorCode() == 2) {
				final OpenfireManager openfireManager = application.getOpenfireManager();
				if (openfireManager.isLoggedIn()) {
					showToast(R.string.net_addressbook_invited);
					openfireManager.sendNetAddressBookInviteReq(mUserid, String.valueOf(baseResult.getUid()),
							addressbooks, "ok");
					this.finish();
				} else {
					showToast(R.string.not_logged_cannot_operating);
				}
			}
			// 3：非旁客用户，且未存在通讯录中。
			else if (baseResult.getErrorCode() == 3) {
				AddressBookAddMember();
			} else if (baseResult.getErrorCode() == baseResult.FAILED) {
				showToast(baseResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}

	}

	/**
	 * 发送私信
	 * 
	 * @param contacts
	 *            void
	 */
	private void sendMsg(String phoneNum) {
		if (phoneNum != null && phoneNum.length() > 0) {
			String sms = "我已加您到旁客软件的单位通讯录：‘" + addressbooks.getName()
					+ "’中，建议您也去安装一个旁客软件，可以看到我们单位中的其他成员通信方式。";
			SmsManager smsMana = SmsManager.getDefault();
			if (sms.length() > 70) {
				ArrayList<String> msgs = smsMana.divideMessage(sms);
				smsMana.sendMultipartTextMessage(phoneNum, null, msgs, null, null);
			} else
				smsMana.sendTextMessage(phoneNum, null, sms, null, null);
		}

	}

}
