package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.UserInfoAdapter;
import com.wachoo.pangker.api.ContactUserListenerManager;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.db.IAttentsDao;
import com.wachoo.pangker.db.IBlacklistDao;
import com.wachoo.pangker.db.impl.AttentsDaoImpl;
import com.wachoo.pangker.db.impl.BlacklistDaoImpl;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.BlackManagerResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.FooterView;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.ContactsUtil;
import com.wachoo.pangker.util.Util;

/**
 * 黑名单用户管理
 * 
 * @author zhengjy
 */
public class BlackManagerActivity extends CommonPopActivity implements IUICallBackInterface {

	private PullToRefreshListView pullToRefreshListView;//

	private ListView mBlacklistView;
	private FooterView mFooterView;
	private EmptyView mEmptyView;
	private List<UserItem> users = new ArrayList<UserItem>();
	private UserItem currChooseUser;
	private String mUserId;
	private UserInfoAdapter adapter;
	private IBlacklistDao iBlacklistDao;
	private final int CASE_BLACKLIST = 0x10;
	private final int CASE_CANCEL = 0x11;
	private final int CASE_ADDFRIEND = 0x12;
	private final int CASE_ATTENTION = 0x13;
	private final int CASE_USERINFO = 0x14;
	private final int CASE_ATTENTION_IMPLICIT = 0x15;

	private int currentDialogId = -1;

	private PangkerApplication mApplication;// application
	private ContactsUtil contactsUtil;
	private PKIconResizer mImageResizer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blacklist_manager);
		mApplication = (PangkerApplication) getApplication();
		mUserId = mApplication.getMyUserID();
		iBlacklistDao = new BlacklistDaoImpl(this);
		contactsUtil = new ContactsUtil(this, mUserId);
		mImageResizer = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);
		initView();
		adapter = new UserInfoAdapter(this, new ArrayList<UserItem>(), mImageResizer);
		mBlacklistView.setAdapter(adapter);
	}

	@Override
	protected void onResume() {
		users = mApplication.getBlackList();
		if (users == null) {
			users = new ArrayList<UserItem>();
		}
		adapter.setItems(users);
		if (users.size() == 0) {
			mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_blacks_prompt);
		}
		super.onResume();
	}

	private void initView() {
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		mBlacklistView = pullToRefreshListView.getRefreshableView();
		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(mBlacklistView);

		mBlacklistView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				currChooseUser = (UserItem) arg0.getAdapter().getItem(arg2);
				mBlacklistView.showContextMenu();
			}
		});
		mBlacklistView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				currChooseUser = (UserItem) arg0.getAdapter().getItem(arg2);
				showMenuDialog();
				return false;// mBlacklistView.showContextMenu();
			}
		});

		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				BlackManagerActivity.this.finish();
			}
		});

		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText(R.string.title_blacklist);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		
		mFooterView = new FooterView(this);
		mFooterView.addToListView(mBlacklistView);
	}
	
	private PopMenuDialog menuDialog;
	
	private void showMenuDialog() {
		// TODO Auto-generated method stub
        if(menuDialog == null){
        	menuDialog = new PopMenuDialog(this);
        	menuDialog.setListener(mClickListener);
        	menuDialog.setMenus(getActionItems());
        }
        menuDialog.setTitle(currChooseUser.getUserName() != null ? currChooseUser.getUserName() : "");
        menuDialog.show();
	}
	
	private ActionItem[] getActionItems() {
		// TODO Auto-generated method stub
		ActionItem[] msgMenu = new ActionItem[] { new ActionItem(CASE_USERINFO, getString(R.string.title_look_friend)),
				new ActionItem(CASE_ATTENTION, getString(R.string.add_to_attent)),
				new ActionItem(CASE_ATTENTION_IMPLICIT, getString(R.string.add_to_attent_implicit)),
				new ActionItem(CASE_ADDFRIEND, getString(R.string.add_to_friend)),
				new ActionItem(CASE_CANCEL, getString(R.string.black_delete_blacklist))};
		return msgMenu;
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionid) {
			// TODO Auto-generated method stub
			menuDialog.dismiss();
			if (CASE_USERINFO == actionid) {
				showInfo(currChooseUser);
			} else
				mShowDialog(actionid);
		}
	};

	/**
	 * 
	 */
	public void showInfo(UserItem user) {
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra("userid", user.getUserId());
		intent.putExtra("username", user.getUserName());
		intent.putExtra("usersign", user.getSign());
		startActivity(intent);
	}

	private MessageTipDialog mTipDialog;
	/**
	 * 显示dialog前的设置
	 * @param id
	 */
	private void mShowDialog(int id) {
		if(mTipDialog == null){
			mTipDialog = new MessageTipDialog(resultCallback, this);
		}
		currentDialogId = id;
		if (CASE_CANCEL == id) {
			mTipDialog.showDialog(currChooseUser.getUserName(), getString(R.string.if_moveout_blacklist));
		} else if (CASE_ADDFRIEND == id) {
			mTipDialog.showDialog(currChooseUser.getUserName(), getString(R.string.blacklist_addfriend));
		} else if (CASE_ATTENTION == id) {
			mTipDialog.showDialog(currChooseUser.getUserName(), getString(R.string.blacklist_addattention));
		} else if (CASE_ATTENTION_IMPLICIT == id) {
			mTipDialog.showDialog(currChooseUser.getUserName(), getString(R.string.blacklist_addattention_implicit));
		}
	}
	
	MessageTipDialog.DialogMsgCallback resultCallback = new MessageTipDialog.DialogMsgCallback() {
		@Override
		public void buttonResult(boolean isSubmit) {
			// TODO Auto-generated method stub
			if(isSubmit){
				if (CASE_CANCEL == currentDialogId) {
					BlacklistManager("2", CASE_CANCEL);
				} else if (CASE_ADDFRIEND == currentDialogId) {
					BlacklistManager("2", CASE_ADDFRIEND);
					// addFriend();
				} else if (CASE_ATTENTION == currentDialogId) {
					BlackToAttent("1", CASE_ATTENTION);
				} else if (CASE_ATTENTION_IMPLICIT == currentDialogId) {
					BlackToAttent("0", CASE_ATTENTION_IMPLICIT);
				}
			}
		}
	};


	/**
	 * 黑名单用户管理接口
	 * @param type 操作类型
	 */
	public void BlacklistManager(String type, int caseKey) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", mUserId));
		paras.add(new Parameter("ruid", currChooseUser.getUserId()));
		paras.add(new Parameter("type", type)); // 操作类型：0：添加，1：查询，2：删除。
		paras.add(new Parameter("sysdate", Util.getNowTime()));
		String mess = getResourcesMessage(R.string.requesting);
		serverMana.supportRequest(Configuration.getBlackManager(), paras, true, mess, caseKey);
	}

	/**
	 * 添加好友
	 */
	private void addFriend() {
		final OpenfireManager openfireManager = mApplication.getOpenfireManager();
		if (openfireManager.isLoggedIn()) {
			if (openfireManager.createEntry(currChooseUser.getUserId(), currChooseUser.getUserName(), null)) {
				showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_SUECCESS);
			} else {
				showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_FAIL);
			}
		} else {
			showToast(R.string.not_logged_cannot_operating);
		}
	}

	/**
	 * 对加入黑名单的用户添加关注，自动解除黑名单。
	 * 
	 */
	public void BlackToAttent(String attentType, int caseKey) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", mUserId));
		paras.add(new Parameter("uid", mUserId));
		paras.add(new Parameter("ruid", currChooseUser.getUserId()));
		paras.add(new Parameter("attentType", attentType));
		String mess = getResourcesMessage(R.string.requesting);
		serverMana.supportRequest(Configuration.getBlackToAttent(), paras, true, mess, caseKey);
	}
	
	private void removeUser() {
		// TODO Auto-generated method stub
		iBlacklistDao.removeUser(currChooseUser.getUserId());
		adapter.getItems().remove(currChooseUser);
		adapter.notifyDataSetChanged();
		if(adapter.getCount() == 0){
			mEmptyView.showView(R.drawable.emptylist_icon, R.string.no_blacks_prompt);
		}
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		if (!HttpResponseStatus(response))
			return;
		if (CASE_BLACKLIST == caseKey || CASE_CANCEL == caseKey || CASE_ADDFRIEND == caseKey) {
			BlackManagerResult blackResult = JSONUtil.fromJson(response.toString(), BlackManagerResult.class);
			if (null == blackResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (blackResult.getErrorCode() == BaseResult.SUCCESS) {
				if (CASE_CANCEL == caseKey) {
					showToast(blackResult.getErrorMessage());
					removeUser();
				} else if (CASE_BLACKLIST == caseKey) {
					if (blackResult.getUsers() != null) {
						users = blackResult.getUsers();
					} else
						users = new ArrayList<UserItem>();
					adapter.setUserList(users);
				} else if (CASE_ADDFRIEND == caseKey) {
					removeUser();
					addFriend();
				}
			} else if (blackResult.getErrorCode() == BaseResult.FAILED && CASE_CANCEL == caseKey) {
				showToast(blackResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		} else if (CASE_ATTENTION == caseKey || CASE_ATTENTION_IMPLICIT == caseKey) {
			BaseResult blackResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
			if (null == blackResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (blackResult.getErrorCode() == BaseResult.SUCCESS) {
				removeUser();
				if (CASE_ATTENTION == caseKey) {
					currChooseUser.setAttentType(UserItem.ATTENT_TYPE_COMMON);
					mApplication.getOpenfireManager().sendContactsChangedPacket(mUserId, currChooseUser.getUserId(), BusinessType.addattent);
				} else
					currChooseUser.setAttentType(UserItem.ATTENT_TYPE_IMPLICIT);
				IAttentsDao iAttentsDao = new AttentsDaoImpl(this);
				iAttentsDao.addAttent(currChooseUser, String.valueOf(PangkerConstant.DEFAULT_GROUPID));
				contactsUtil.addAttentCount(1);
				ContactUserListenerManager userListenerManager = PangkerManager.getContactUserListenerManager();
				userListenerManager.refreshUI(ContactAttentionActivity.class.getName(), PangkerConstant.STATE_CHANGE_REFRESHUI);
			} else if (blackResult.getErrorCode() == BaseResult.FAILED) {
				showToast(blackResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}
	}

}
