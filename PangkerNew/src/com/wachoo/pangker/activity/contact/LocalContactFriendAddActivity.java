package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.LocalContactUserAdapter;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.InviteRegisterResult;
import com.wachoo.pangker.server.response.InviteResult;
import com.wachoo.pangker.util.SMSUtil;

/**
 * 
 * @TODO 邀请用户注册旁客
 * 
 * @author zhengjy
 * 
 * 
 */
public class LocalContactFriendAddActivity extends CommonPopActivity implements IUICallBackInterface,
		OnFocusChangeListener {

	private String TAG = "ChatRoomInfoActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	private final int mRequestCode = 0x10;
	private final int INVITE_TO_PANGKER = 0x101;

	PangkerApplication application;
	private com.wachoo.pangker.ui.MyGridView gvUuseritem;
	private LocalContactUserAdapter userAdapter;
	private String mUserId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.localcontact_invite);

		application = (PangkerApplication) getApplication();
		mUserId = application.getMyUserID();

		initView();

		List<LocalContacts> selectedUser = (List<LocalContacts>) this.getIntent().getSerializableExtra("selectedUser");
		if (selectedUser != null && selectedUser.size() > 0) {
			userAdapter.setUserList(selectedUser);
		}

	}

	private void initView() {
		Button btnRight = (Button) findViewById(R.id.iv_more);
		btnRight.setVisibility(View.GONE);

		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("准备发送");

		gvUuseritem = (com.wachoo.pangker.ui.MyGridView) findViewById(R.id.gv_useritem);
		userAdapter = new LocalContactUserAdapter(this, new ArrayList<LocalContacts>());
		// 添加成员监听
		userAdapter.setmAddClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				userAdapter.setDeleteState(false);
				Intent intent = new Intent(LocalContactFriendAddActivity.this, LocalContactsSelectActivity.class);
				intent.putExtra("fromActivity", "LocalContactFriendAddActivity");
				LocalContactFriendAddActivity.this.startActivityForResult(intent, mRequestCode);
			}
		});
		userAdapter.setCreater(true);
		gvUuseritem.setAdapter(userAdapter);

		gvUuseritem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				if (position < userAdapter.getCount() - 2) {
					UserItem userItem = (UserItem) arg0.getAdapter().getItem(position);
					// 移除成员
					if (userAdapter.isDeleteState()) {
						userAdapter.getUserList().remove(userItem);
						userAdapter.notifyDataSetChanged();
					}

				}

			}
		});

		LinearLayout lyGridView = (LinearLayout) findViewById(R.id.ly_gridview);
		lyGridView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				userAdapter.setDeleteState(false);
				return false;
			}
		});
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LocalContactFriendAddActivity.this.finish();
			}
		});
		Button btnSubmit = (Button) findViewById(R.id.btn_submit);
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 提交
				recommend();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == mRequestCode && resultCode == RESULT_OK && data != null) {
			List<LocalContacts> selectedUser = (List<LocalContacts>) data.getSerializableExtra("selectedUser");
			if (selectedUser != null && selectedUser.size() > 0) {
				userAdapter.addUserList(selectedUser);
			}
		}
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;
		if (caseKey == INVITE_TO_PANGKER) {
			InviteRegisterResult inviteResult = JSONUtil.fromJson(supportResponse.toString(),
					InviteRegisterResult.class);
			if (inviteResult != null && inviteResult.errorCode == BaseResult.SUCCESS) {
				List<InviteResult> inviteResultList = inviteResult.getInviteResult();
				StringBuffer mobilePhones = new StringBuffer();
				for (InviteResult inviteResultItem : inviteResultList) {
					if (inviteResultItem.getCode() == InviteResult.SUCCESS || inviteResultItem.getCode() == 2) {
						if (mobilePhones.length() == 0) {
							mobilePhones.append(inviteResultItem.getMobile());
						} else
							mobilePhones.append(";").append(inviteResultItem.getMobile());

					}
				}
				if (mobilePhones.length() > 0) {
					sendSms(mobilePhones.toString());
				}
			} else
				showToast(R.string.invite_failed);
		}

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v != gvUuseritem && hasFocus) {
			userAdapter.setDeleteState(false);
		}
	}

	private void recommend() {
		// TODO Auto-generated method stub
		if (userAdapter.getUserList() == null || userAdapter.getUserList().size() < 1) {
			showToast("请选择联系人!");
			return;
		}
		invite2Panker(userAdapter.getUserList());

	}

	/**
	 * 邀请通讯录好友注册旁客
	 * 
	 * @param phoneNum
	 *            被邀请人手机号 接口：InviteRegister.json
	 */
	private void invite2Panker(List<LocalContacts> contacts) {
		String phoneNums = "";
		for (LocalContacts localContacts : contacts) {
			if (phoneNums.length() > 0) {
				phoneNums = phoneNums + "," + localContacts.getPhoneNum();
			} else
				phoneNums = phoneNums + localContacts.getPhoneNum();
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		// showToast(R.string.invite_is_sended);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("inviteuid", mUserId));
		paras.add(new Parameter("invitemobile", application.getMySelf().getPhone() != null ? application.getMySelf()
				.getPhone() : ""));
		paras.add(new Parameter("beinvitemobiles", phoneNums));
		serverMana.supportRequest(Configuration.getInviteRegister(), paras, INVITE_TO_PANGKER);
	}

	/**
	 * TODO系统发送短信界面
	 * 
	 * @param mobileNum
	 *            接受人（支持多人，分号“；”隔离）
	 * @param smsStr
	 *            短信内容
	 */
	private void sendSms(String mobileNum) {
		if (mobileNum != null && mobileNum.length() > 0) {
			SMSUtil.sendInviteSMS(mobileNum, application.getImAccount(), this);
		}
	}

}
