package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.InviteRegisterResult;
import com.wachoo.pangker.server.response.InviteResult;
import com.wachoo.pangker.server.response.UserinfoQueryResult;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.util.SMSUtil;
import com.wachoo.pangker.util.Util;

/**
 * 根据手机号查找用户
 * 
 * @author wubo
 * @createtime 2012-5-29
 */
public class ContactFriendInviteActivity extends CommonPopActivity implements IUICallBackInterface {

	private final int SELECT_REQUESTCODE = 0x100;
	private final int INVITE_TO_PANGKER = 0x101;
	private final int KEY_SAERCH_USER = 0x102;
	
	private final String[] choiceItems = new String[] { "请输入手机号码", "请输入旁客账号", "请输入网站名称"};
	TextView et_search;
	ImageView usericon;
	TextView userName;
	TextView sign;
	EditText et_dialog;
	LinearLayout ly_info;
	//ImageView status;
	private EditText etEditName;
	private Button btnMenuShow;
	private Button btnAddContact;
	private String userId;
	String phoneNum;
	UserItem item;
	PangkerApplication application;
	private PKIconResizer mImageResizer;
	private int searchType = 0;
	ServerSupportManager serverMana = new ServerSupportManager(this, this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friendinvite);
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		searchType = this.getIntent().getIntExtra("type", 0);
		
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		initView();
		
		if(searchType == 0){
			etEditName.setInputType(InputType.TYPE_CLASS_PHONE);
			etEditName.setHint(choiceItems[0]);
		}
		else if(searchType == 1){
			etEditName.setInputType(InputType.TYPE_CLASS_TEXT);
			btnAddContact.setVisibility(View.GONE);
			etEditName.setHint(choiceItems[1]);
		}
		else if(searchType == 2){
			etEditName.setInputType(InputType.TYPE_CLASS_TEXT);
			btnAddContact.setVisibility(View.GONE);
			etEditName.setHint(choiceItems[2]);
		}
	}

	public void initView() {
		TextView tv_title = (TextView) findViewById(R.id.mmtitle);
		tv_title.setText("用户搜索");
		
		et_search = (TextView) findViewById(R.id.et_follow_search);
		ImageView btnClear = (ImageView) findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				et_search.setText("");
			}
		});
		usericon = (ImageView) findViewById(R.id.iv_usericon);
		userName = (TextView) findViewById(R.id.tv_name);
		sign = (TextView) findViewById(R.id.tv_sign);
		btnMenuShow = (Button) findViewById(R.id.btn_reply);
		etEditName = (EditText) findViewById(R.id.et_query_name);
		btnAddContact = (Button) findViewById(R.id.iv_more);
		btnAddContact.setBackgroundResource(R.drawable.btn_add_content_selector);
		ly_info = (LinearLayout) findViewById(R.id.ly_info);
		ly_info.setVisibility(View.GONE);
		ly_info.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(ContactFriendInviteActivity.this, UserWebSideActivity.class);
				intent.putExtra("userid", item.getUserId());
				intent.putExtra("username", item.getUserName());
				intent.putExtra("usersign", item.getSign());
				startActivity(intent);
			}
		});
		//status = (ImageView) findViewById(R.id.iv_status);
		btnMenuShow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				phoneNum = etEditName.getText().toString().trim();
				if (Util.isEmpty(phoneNum)) {
					showToast("查询条件不能为空!");
					return;
				}
				hideSoftInput(v);
				searchUsersByType();
			}
		});
		btnAddContact.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ContactFriendInviteActivity.this, LocalContactsSelectActivity.class);
				intent.putExtra("fromType", 2);
				ContactFriendInviteActivity.this.startActivityForResult(intent, SELECT_REQUESTCODE);
			}
		});

		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideSoftInput(v);
				ContactFriendInviteActivity.this.finish();
			}
		});
		
	}
	
	private void searchUsersByType() {
		// TODO Auto-generated method stub
		if(searchType == 2){
			getWebUserInfo();
        } else {
        	getUserInfo();	
        }
	}
	
	private void getWebUserInfo() {
		// TODO Auto-generated method stub
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("webSiteName", phoneNum));
		String mess = getString(R.string.pop_searching);
		serverMana.supportRequest(Configuration.getSearchUserByWebSite(), paras, true, mess, KEY_SAERCH_USER);
	}

	/**
	 * 获取用户信息
	 * @author wubo
	 * @createtime 2012-5-31
	 */
	public void getUserInfo() {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", userId));
		paras.add(new Parameter("mobile", phoneNum));
		paras.add(new Parameter("type", searchType + ""));
		paras.add(new Parameter("account", phoneNum));
		String mess = getString(R.string.pop_searching);
		serverMana.supportRequest(Configuration.getUserinfoByMobile(), paras, true, mess, KEY_SAERCH_USER);
	}

	/**
	 * 设置用户信息
	 * 
	 * @author wubo
	 * @createtime 2012-5-31
	 * @param info
	 */
	public void setUserInfo(UserInfo info) {
		ly_info.setVisibility(View.VISIBLE);
		item = new UserItem(info);
		userName.setText(item.getUserName());
		sign.setText(item.getSign());

//		new UserOnlineTask(item.getUserId(), new OnlineInterface() {
//			@Override
//			public void isOnline(int status) {
//				if (status == PangkerConstant.STATE_TYPE_ONLINE) {
//					ContactFriendInviteActivity.this.status.setImageResource(R.drawable.icon_online);
//				} else
//					ContactFriendInviteActivity.this.status.setImageResource(R.drawable.icon_offline);
//			}
//		}).execute(null);

		mImageResizer.loadImage(item.getUserId(), usericon);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (SELECT_REQUESTCODE == requestCode && RESULT_OK == resultCode) {
			LocalContacts contact = (LocalContacts) data
					.getSerializableExtra(LocalContacts.LOCALCONTACTS_KEY);
			if (contact != null) {
				etEditName.setText(contact.getPhoneNum());
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)) {
			showToast(R.string.to_server_fail);
			return;
		}
        if(caseKey == INVITE_TO_PANGKER){
        	InviteRegisterResult inviteResult = JSONUtil.fromJson(response.toString(), InviteRegisterResult.class);
			if (inviteResult != null && inviteResult.errorCode == BaseResult.SUCCESS) {
				List<InviteResult> inviteResultList = inviteResult.getInviteResult();
				StringBuffer mobilePhones = new StringBuffer();
				for (InviteResult inviteResultItem : inviteResultList) {
					if (inviteResultItem.getCode() == InviteResult.SUCCESS || inviteResultItem.getCode() == 2) {
						if (mobilePhones.length() == 0) {
							mobilePhones.append(inviteResultItem.getMobile());
						} else
							mobilePhones.append(";").append(inviteResultItem.getMobile());
					}
				}
				if (mobilePhones.length() > 0) {
					sendSms(mobilePhones.toString());
				}

			} else
				showToast(R.string.invite_failed);
        } 
        else if (caseKey == KEY_SAERCH_USER){
        	UserinfoQueryResult queryResult = JSONUtil.fromJson(response.toString(), UserinfoQueryResult.class);
    		if (queryResult != null && queryResult.getErrorCode() == BaseResult.SUCCESS) {
    			UserInfo info = queryResult.getUserInfo();
    			if (info != null) {
    				setUserInfo(info);
    			} 
    		}
    		else if (queryResult != null && queryResult.getErrorCode() == BaseResult.FAILED) {
    			if(searchType == 0 &&  SMSUtil.isMobileNO(phoneNum)){
    				showInviteRegistDialog();
    			}
    			ly_info.setVisibility(View.GONE);
    		} else {
    			showToast(R.string.to_server_fail);
    		}
        }
	}
	
	MessageTipDialog mTipDialog;
	private void showInviteRegistDialog() {
		// TODO Auto-generated method stub
        if(mTipDialog == null){
        	mTipDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-generated method stub
					if(isSubmit){
						invite2Panker(phoneNum);
					}
				}
			}, this);
        }
        mTipDialog.showDialog(null, "该手机号码还未注册，是否邀请TA注册？(邀请会发送一条短信)");
	}
	
	/**
	 * 邀请通讯录好友注册旁客
	 * 
	 * @param phoneNum
	 *            被邀请人手机号 接口：InviteRegister.json
	 */
	private void invite2Panker(String phoneNum) {
		showToast(R.string.invite_is_sended);
		String mPhoneNum = application.getMySelf().getPhone();
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("inviteuid", userId));
		paras.add(new Parameter("invitemobile", mPhoneNum != null ? mPhoneNum : ""));
		paras.add(new Parameter("beinvitemobiles", phoneNum));
		serverMana.supportRequest(Configuration.getInviteRegister(), paras, INVITE_TO_PANGKER);
	}

	/**
	 * TODO系统发送短信界面
	 * @param mobileNum 接受人（支持多人，分号“；”隔离）
	 * @param smsStr  短信内容
	 */
	private void sendSms(String mobileNum) {
		if (mobileNum != null && mobileNum.length() > 0) {
			SMSUtil.sendInviteSMS(mobileNum, application.getImAccount(), this);
		}
	}
}
