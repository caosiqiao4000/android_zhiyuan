package com.wachoo.pangker.activity.contact;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.ContentUris;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.msg.MsgChatActivity;
import com.wachoo.pangker.db.INetAddressBookUserDao;
import com.wachoo.pangker.db.impl.NetAddressBookUserDaoImpl;
import com.wachoo.pangker.entity.LocalContacts;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBookResult;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.AddressMembers;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.InviteRegisterResult;
import com.wachoo.pangker.server.response.InviteResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.EditTextDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.SMSUtil;
import com.wachoo.pangker.util.Util;

/**
 * 单位通讯录成员名片
 * 
 * @author zhengjy 2012-6-21
 * 
 */
public class UtitsContactUserDetailActivity extends CommonPopActivity implements IUICallBackInterface {

	// private final static int CONTACT_USER_ADD = 0x10;
	private final int INVITE_TO_PANGKER = 0x11;
	private final int REMARK_USER = 0x12;

	private PangkerApplication application;
	private TextView etName;
	private TextView etMobile;
	private TextView etEmail;
	private TextView etAddress;
	private TextView etZipcode;
	private TextView tvDepartment;
	private ImageView ivPortrait;
	private ImageView ivIdentity;
	private ImageView ivInto;
	private LinearLayout btnCall;
	private Button btnMessage;
	private LinearLayout btnEmail;
	private LinearLayout mLayoutMobile1;
	private LinearLayout mLayoutMobile2;
	private LinearLayout mLayoutMobile3;
	private LinearLayout ly_department;
	private INetAddressBookUserDao iNetAddressBookUserDao;

	private TextView user_title;
	private Button btnMess;
	private Button btnInvite;
	private Button btnUserinfo;

	private PKIconResizer mImageResizer;
	private AddressBooks currBooks;
	private AddressMembers addressMember;

	private PopMenuDialog menuDialog;
	private String etRname;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.units_contacts_user_detail);
		application = (PangkerApplication) getApplication();
		mImageResizer = PangkerManager.getUserIconResizer(this.getApplicationContext());
		iNetAddressBookUserDao = new NetAddressBookUserDaoImpl(this);

		currBooks = (AddressBooks) this.getIntent().getSerializableExtra(AddressBooks.BOOK_KEY);
		initView();

		LocalContacts contact = (LocalContacts) this.getIntent().getSerializableExtra(LocalContacts.LOCALCONTACTS_KEY);
		addressMember = (AddressMembers) getIntent().getSerializableExtra(AddressMembers.ADDRESSMEMBER_KEY);
		if (contact != null) {
			initLocalContacts(contact);
		} else if (addressMember != null) {
			initAddressMembeerData();
		}
	}

	private void initView() {
		user_title = (TextView) findViewById(R.id.mmtitle);
		user_title.setText(R.string.units_title_contact_detail);
		etName = (TextView) findViewById(R.id.et_name);
		etMobile = (TextView) findViewById(R.id.et_mobile);
		etEmail = (TextView) findViewById(R.id.et_email);
		etAddress = (TextView) findViewById(R.id.et_address);
		etZipcode = (TextView) findViewById(R.id.et_zipcode);
		tvDepartment = (TextView) findViewById(R.id.et_department);

		ivPortrait = (ImageView) findViewById(R.id.iv_portrait);
		ivIdentity = (ImageView) findViewById(R.id.iv_identity);
		ivInto = (ImageView) findViewById(R.id.iv_into);
		btnCall = (LinearLayout) findViewById(R.id.btn_call);
		btnEmail = (LinearLayout) findViewById(R.id.btn_email);

		btnMess = (Button) findViewById(R.id.iv_more);
		btnMess.setVisibility(View.GONE);
		btnMess.setBackgroundResource(R.drawable.btn_chat_selector);
		btnMess.setBackgroundResource(R.drawable.btn_default_selector);
		btnInvite = (Button) findViewById(R.id.btn_invite);
		btnUserinfo = (Button) findViewById(R.id.btn_userinfo);
		btnMessage = (Button) findViewById(R.id.btn_sms_mobile);

		mLayoutMobile1 = (LinearLayout) findViewById(R.id.ly_mobile1);
		mLayoutMobile2 = (LinearLayout) findViewById(R.id.ly_mobile2);
		mLayoutMobile3 = (LinearLayout) findViewById(R.id.ly_mobile3);

		ly_department = (LinearLayout) findViewById(R.id.ly_department);

		//
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (!Util.isEmpty(etRname)) {
			Intent intent = getIntent();
			intent.putExtra(AddressMembers.ADDRESSMEMBER_KEY, addressMember);
			setResult(RESULT_OK, intent);
		}
		super.onBackPressed();
	}

	/**
	 * void TODO 单位通讯录成员名片入口
	 * 
	 * @param contact
	 */
	private void initAddressMembeerData() {
		etName.setText(addressMember.getRemarkname());
		etMobile.setText(addressMember.getMobile());
		etEmail.setText(addressMember.getEmail());
		etAddress.setText(addressMember.getAddress());
		etZipcode.setText(addressMember.getZipcode());
		tvDepartment.setText(addressMember.getDepartment());
		if (!Util.isEmpty(addressMember.getPhone1())) {
			setInitCallListener(mLayoutMobile1, addressMember.getPhone1());
		}
		if (!Util.isEmpty(addressMember.getPhone2())) {
			setInitCallListener(mLayoutMobile2, addressMember.getPhone2());
		}
		if (!Util.isEmpty(addressMember.getPhone3())) {
			setInitCallListener(mLayoutMobile3, addressMember.getPhone3());
		}
		// >>>>>>创建者或者自己则可以修改备注
		if (currBooks.getUid().toString().equals(application.getMyUserID()) || addressMember.getUid() != null
				&& addressMember.getUid().toString().equals(application.getMyUserID())) {
			Button btnUpdate = (Button) findViewById(R.id.btn_update);
			btnUpdate.setVisibility(View.VISIBLE);
			btnUpdate.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showEditTextDialog();
				}
			});
		} else {
			// >>>>>>不是创建者或者不是自己则不能修改他人备注
			findViewById(R.id.btn_update).setVisibility(View.GONE);
		}

		if (addressMember.getUid() != null && addressMember.getUid() != 0) {
			mImageResizer.loadImage(String.valueOf(addressMember.getUid()), ivPortrait);
		}

		// 如果是旁客用户，显示旁客图标并加载头像
		if (addressMember.getUid() != null && addressMember.getUid() != 0
				&& !addressMember.getUid().toString().equals(application.getMyUserID())) {
			btnMess.setVisibility(View.VISIBLE);
			btnMess.setText("私信");
			btnMess.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(UtitsContactUserDetailActivity.this, MsgChatActivity.class);
					intent.putExtra(UserItem.USERID, addressMember.getUid().toString());
					intent.putExtra(UserItem.USERNAME, addressMember.getRemarkname());
					intent.putExtra(UserItem.USERSIGN, "");
					UtitsContactUserDetailActivity.this.startActivity(intent);
				}
			});

			ivPortrait.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					lookUserInfo(addressMember);
				}
			});

			ivIdentity.setVisibility(View.VISIBLE);
			if (application.IsFriend(addressMember.getUid().toString())) {
				ivIdentity.setImageResource(R.drawable.pangkefriends_icon);
			} else {
				ivIdentity.setImageResource(R.drawable.pangker);
			}

			// >>>>>>>>
			btnInvite.setVisibility(View.GONE);
			ivInto.setVisibility(View.VISIBLE);
			btnUserinfo.setVisibility(View.VISIBLE);
			btnUserinfo.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					lookUserInfo(addressMember);
				}
			});

		}
		// 非旁客用户
		else {
			ivInto.setVisibility(View.GONE);
			btnUserinfo.setVisibility(View.GONE);
			if (!(addressMember.getUid() != null && addressMember.getUid().toString().equals(application.getMyUserID()))) {

				btnInvite.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						invite2Panker(addressMember.getMobile());
					}
				});
			} else {
				btnInvite.setVisibility(View.GONE);
			}
		}

		btnCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callUser(addressMember.getMobile());
			}
		});
		btnMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sendSms(addressMember.getMobile(), "");
			}
		});

		btnEmail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Util.isEmpty(etEmail.getText().toString())) {
					showToast(R.string.contact_user_detail_email_notnull);
					return;
				}
				Intent email = new Intent(android.content.Intent.ACTION_SEND);
				String[] tos = { addressMember.getEmail() };
				email.putExtra(Intent.EXTRA_EMAIL, tos);
				email.setType("plain/text");
				startActivity(email);
			}
		});
	}

	private void lookUserInfo(LocalContacts contact) {
		Intent intent = new Intent(UtitsContactUserDetailActivity.this, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, contact.getUserId());
		intent.putExtra(UserItem.USERNAME, contact.getUserName());
		intent.putExtra(UserItem.USERSIGN, "");
		UtitsContactUserDetailActivity.this.startActivity(intent);
	}

	private void lookUserInfo(AddressMembers addressMember) {
		Intent intent = new Intent(UtitsContactUserDetailActivity.this, UserWebSideActivity.class);
		intent.putExtra(UserItem.USERID, addressMember.getUid().toString());
		intent.putExtra(UserItem.USERNAME, addressMember.getRemarkname());
		intent.putExtra(UserItem.USERSIGN, "");
		UtitsContactUserDetailActivity.this.startActivity(intent);
	}

	private void loadPhoto(Long pid) {
		if (pid > 0) {
			Uri photpuri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, pid);
			InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(), photpuri);
			Bitmap bp = BitmapFactory.decodeStream(input);
			ivPortrait.setImageBitmap(bp);
		}
	}

	/**
	 * void TODO 本地通讯录联系人信息入口
	 * 
	 * @param contact
	 */
	private void initLocalContacts(final LocalContacts contact) {
		ly_department.setVisibility(View.GONE);
		etName.setText(contact.getUserName());
		// 通讯录联系人入口不能修改备注
		findViewById(R.id.btn_update).setVisibility(View.GONE);
		String mPhoneNum = contact.getPhoneNum();
		if (!Util.isEmpty(mPhoneNum)) {
			etMobile.setText(mPhoneNum);
		}
		if (contact.getPhoneNums() != null && contact.getPhoneNums().size() > 0) {
			for (int i = 0, size = contact.getPhoneNums().size(); i < size; i++) {
				String phoneTemp = contact.getPhoneNums().get(i);
				if (i == 0 && !Util.isEmpty(phoneTemp)) {
					if (Util.isEmpty(mPhoneNum)) {
						setInitCallListener(mLayoutMobile1, phoneTemp);
					} else if (!mPhoneNum.equals(phoneTemp)) {
						setInitCallListener(mLayoutMobile1, phoneTemp);
					}
				} else if (i == 1 && !Util.isEmpty(phoneTemp)) {
					if (Util.isEmpty(mPhoneNum)) {
						setInitCallListener(mLayoutMobile2, phoneTemp);
					} else if (!mPhoneNum.equals(phoneTemp)) {
						setInitCallListener(mLayoutMobile2, phoneTemp);
					}
				} else if (i == 2 && !Util.isEmpty(phoneTemp)) {
					if (Util.isEmpty(mPhoneNum)) {
						setInitCallListener(mLayoutMobile3, phoneTemp);
					} else if (!mPhoneNum.equals(phoneTemp)) {
						setInitCallListener(mLayoutMobile3, phoneTemp);
					}
				}
			}
		}

		if (contact.getRelation() == LocalContacts.RELATION_P_USER
				|| contact.getRelation() == LocalContacts.RELATION_P_FRIEND) {
			ivInto.setVisibility(View.VISIBLE);
			btnMess.setVisibility(View.VISIBLE);
			btnMess.setText("私信");
			btnMess.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(UtitsContactUserDetailActivity.this, MsgChatActivity.class);
					intent.putExtra(UserItem.USERID, contact.getUserId());
					intent.putExtra(UserItem.USERNAME, contact.getUserName());
					intent.putExtra(UserItem.USERSIGN, "");
					UtitsContactUserDetailActivity.this.startActivity(intent);
				}
			});

			ivIdentity.setVisibility(View.VISIBLE);
			ivIdentity.setImageResource(R.drawable.pangker);
			mImageResizer.loadImage(contact.getUserId(), ivPortrait);
			ivPortrait.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					lookUserInfo(contact);
				}
			});
			btnInvite.setVisibility(View.GONE);
			ivInto.setVisibility(View.VISIBLE);
			btnUserinfo.setVisibility(View.VISIBLE);
			btnUserinfo.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					lookUserInfo(contact);
				}
			});
		} else {
			ivInto.setVisibility(View.GONE);
			btnUserinfo.setVisibility(View.GONE);
			if (contact.isHasPhoto()) {
				loadPhoto(Long.parseLong(contact.getContactId()));
			}
			btnInvite.setVisibility(View.VISIBLE);
			btnInvite.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					// >>>>>>同时判断三个号码是否全部为空
					if (contact.getPhoneNums() != null && contact.getPhoneNums().size() > 0) {
						// >>>>>>判断电话号码个数
						if (contact.getPhoneNums().size() == 1)
							invite2Panker(contact.getPhoneNums().get(0));
						else {
							menuDialog = new PopMenuDialog(UtitsContactUserDetailActivity.this);

							menuDialog.setListener(new UITableView.ClickListener() {

								@Override
								public void onClick(int actionid) {
									// TODO Auto-generated method stub
									invite2Panker(contact.getPhoneNums().get(actionid));
									menuDialog.dismiss();
								}
							});
							// >>>>>>获取电话号码
							ActionItem[] actionItems = new ActionItem[contact.getPhoneNums().size()];

							for (int i = 0; i < contact.getPhoneNums().size(); i++) {
								actionItems[i] = new ActionItem(i, contact.getPhoneNums().get(i));
							}
							menuDialog.setMenus(actionItems);
							menuDialog.setTitle("请选择电话号码");
							menuDialog.show();
						}
					} else
						showToast("该用户没有电话号码！");
				}
			});
		}
		btnCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!Util.isEmpty(contact.getPhoneNum())) {
					callUser(contact.getPhoneNum());
				} else
					showToast("号码为空！");
			}
		});
		btnMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Util.isEmpty(contact.getPhoneNum())) {
					showToast("号码为空！");
					return;
				} else if (!Util.isMobile(contact.getPhoneNum())) {
					showToast("非手机号码，无法发送短信！");
				} else
					sendSms(contact.getPhoneNum(), "");
			}
		});

	}

	EditTextDialog mEditTextDialog;

	private void showEditTextDialog() {
		// TODO Auto-generated method stub
		if (mEditTextDialog == null) {
			mEditTextDialog = new EditTextDialog(this);
		}
		mEditTextDialog.setTitle(R.string.user_dialog_remark);
		mEditTextDialog.showDialog(addressMember.getRemarkname(), "", 18, resultCallback);
	}

	EditTextDialog.DialogResultCallback resultCallback = new EditTextDialog.DialogResultCallback() {
		@Override
		public void buttonResult(String result, boolean isSubmit) {
			// TODO Auto-generated method stub
			if (isSubmit) {
				etRname = result;
				if (Util.isEmpty(result)) {
					showToast(R.string.units_remark_cannot_null);
					return;
				} else
					AddressBookModify(result);
			} else {
				etRname = null;
			}
		}
	};

	/**
	 * 多个手机号时的号码设置
	 * 
	 * @param view
	 * @param phoneNum
	 */
	private void setInitCallListener(final LinearLayout view, final String phoneNum) {
		view.setVisibility(View.VISIBLE);
		final TextView tvMobile = (TextView) view.findViewById(R.id.tv_mobile);
		tvMobile.setText(phoneNum);

		LinearLayout btnCall = (LinearLayout) view.findViewById(R.id.btn_call);
		btnCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callUser(phoneNum);
			}
		});
		Button btnSms = (Button) view.findViewById(R.id.btn_mess_mobile);
		btnSms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sendSms(phoneNum, "");
			}
		});

	}

	/**
	 * 单位通讯录成员信息修改
	 * 
	 * 
	 * 接口：AddressBookModify.json
	 */
	private void AddressBookModify(String remarkName) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", application.getMyUserID())); // 操作者UID。
		paras.add(new Parameter("rid", String.valueOf(addressMember.getId())));// 关系ID标识。
		paras.add(new Parameter("mobile", addressMember.getMobile())); //
		paras.add(new Parameter("remarkname", remarkName));//
		paras.add(new Parameter("type", "1")); // 0:修改手机号码，1：修改备注。默认修改备注。
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getAddressBookModify(), paras, true, mess, REMARK_USER);
	}

	/**
	 * 拨打电话
	 * 
	 * @param phoneNum
	 */
	private void callUser(String phoneNum) {
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setData(Uri.parse("tel://" + phoneNum));
		startActivity(intent);
	}

	/**
	 * TODO系统发送短信界面
	 * 
	 * @param mobileNum
	 *            接受人
	 * @param smsStr
	 *            短信内容
	 */
	private void sendSms(String mobileNum, String smsStr) {
		Uri smsToUri = Uri.parse("smsto:" + mobileNum);
		Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
		if (!Util.isEmpty(smsStr)) {
			mIntent.putExtra("sms_body", smsStr);
		}
		startActivity(mIntent);
	}

	/**
	 * 邀请通讯录好友注册旁客
	 * 
	 * @param phoneNum
	 *            被邀请人手机号 接口：InviteRegister.json
	 */
	private void invite2Panker(String phoneNum) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		String mPhoneNum = ((PangkerApplication) getApplication()).getMySelf().getPhone();

		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("inviteuid", application.getMyUserID()));
		paras.add(new Parameter("invitemobile", mPhoneNum != null ? mPhoneNum : ""));
		paras.add(new Parameter("beinvitemobiles", phoneNum));
		serverMana.supportRequest(Configuration.getInviteRegister(), paras, INVITE_TO_PANGKER);
	}

	@Override
	public void uiCallBack(Object response, int key) {
		if (!HttpResponseStatus(response))
			return;

		if (key == INVITE_TO_PANGKER) {
			InviteRegisterResult inviteResult = JSONUtil.fromJson(response.toString(), InviteRegisterResult.class);
			if (inviteResult != null && inviteResult.errorCode == BaseResult.SUCCESS) {
				List<InviteResult> inviteResultList = inviteResult.getInviteResult();
				for (InviteResult inviteResultItem : inviteResultList) {
					if (inviteResultItem.getCode() == InviteResult.SUCCESS || inviteResultItem.getCode() == 2) {
						sendMsg(inviteResultItem.getMobile(), inviteResultItem.getMessage());
					}
				}
			} else if (inviteResult != null && inviteResult.errorCode == BaseResult.FAILED) {
				showToast(inviteResult.getErrorMessage());
			} else
				showToast(R.string.invite_failed);
		} else if (key == REMARK_USER) {
			AddressBookResult baseResult = JSONUtil.fromJson(response.toString(), AddressBookResult.class);
			if (null == baseResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (baseResult.getErrorCode() == BaseResult.SUCCESS) {
				showToast(baseResult.getErrorMessage());
				etName.setText(etRname);
				addressMember.setRemarkname(etRname);
				iNetAddressBookUserDao.updateAddressMember(addressMember);
			} else if (baseResult.getErrorCode() == BaseResult.FAILED) {
				showToast(baseResult.getErrorMessage());
			} else
				showToast(R.string.to_server_fail);
		}

	}

	/**
	 * 发送私信
	 * 
	 * @param contacts
	 *            void
	 */
	private void sendMsg(String phoneNum, String smsUrl) {
		if (phoneNum != null && phoneNum.length() > 0) {
			SMSUtil.sendInviteSMS(phoneNum, application.getImAccount(), this);
		}
	}

}
