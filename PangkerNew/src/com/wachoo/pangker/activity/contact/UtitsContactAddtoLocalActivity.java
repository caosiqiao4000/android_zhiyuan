package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentProviderOperation;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.RawContacts.Data;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.NetContactMemberAdapter;
import com.wachoo.pangker.adapter.NetContactMemberSelectAdapter;
import com.wachoo.pangker.server.response.AddressMembers;
import com.wachoo.pangker.ui.HorizontalListView;

/**
 * 单位通讯录成员导入本地通讯录
 * @author zhengjy
 */
public class UtitsContactAddtoLocalActivity extends CommonPopActivity {

	private String TAG = "UtitsContactAddtoLocalActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	
	private List<AddressMembers> inviteLits;
	private ListView inviteListView;
	private NetContactMemberAdapter utitsInviteAdapter;
	private HorizontalListView horizontalListView;
	private NetContactMemberSelectAdapter traceInAdpater;
	private Button btnConfrim;
	private Button btnCancel;
	private TextView txtCount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.units_invite);
		inviteLits  = (List<AddressMembers>) this.getIntent().getSerializableExtra("contactList");
		initView();
		
		if (inviteLits != null && inviteLits.size() > 0) {
			utitsInviteAdapter = new NetContactMemberAdapter(this, inviteLits);
			inviteListView.setAdapter(utitsInviteAdapter);
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		TextView tvTitle = (TextView) findViewById(R.id.traceIn_title);
		tvTitle.setText(R.string.contact_addto_local_title);
		inviteListView = (ListView) findViewById(R.id.traceMemberList);
		inviteListView.setOnItemClickListener(vOnItemClickListener);

		horizontalListView = (HorizontalListView) findViewById(R.id.traceHorizontalListView);
		traceInAdpater = new NetContactMemberSelectAdapter(this);
		horizontalListView.setAdapter(traceInAdpater);

		btnConfrim = (Button) findViewById(R.id.traceIn_confrim);
		btnCancel = (Button) findViewById(R.id.traceIn_cancel);
		btnConfrim.setOnClickListener(mClickListener);
		btnCancel.setOnClickListener(mClickListener);
		txtCount = (TextView) findViewById(R.id.traceIn_count);
	}

	/**
	 * 判断号码是否存在通讯录中
	 * @param mNumber
	 * @return
	 */
	private boolean isExist(String mNumber){
		 String[] projection = new String[] { Phone.NUMBER };
		 Cursor mCursor = null;
			 try {
				 mCursor = this.getContentResolver().query(
			                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, 
			                projection, //返回字段
			                ContactsContract.CommonDataKinds.Phone.NUMBER + " = '" + mNumber + "'", // 
			                null, // WHERE clause value substitution
			                null); // Sort order.

		            return mCursor != null && mCursor.getCount() > 0;

		        } catch (Exception e) {
			        Log.e(TAG, e.getMessage());
		        } finally {
			    if (mCursor != null) {
				    mCursor.close();
			    }
		        }
		    return false;
		}

	AdapterView.OnItemClickListener vOnItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			NetContactMemberAdapter.ViewHolder vHollder = (NetContactMemberAdapter.ViewHolder) view.getTag();
			vHollder.selected.toggle();
			utitsInviteAdapter.selectByIndex(position, vHollder.selected.isChecked());
			traceInAdpater.dealMember(inviteLits.get(position), vHollder.selected.isChecked());
			changeCount();
		}
	};

	private OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			if (view == btnConfrim) {
				addMembersToLocal();
			}
			if (view == btnCancel) {
				UtitsContactAddtoLocalActivity.this.finish();
			}
		}
	};

	private void changeCount() {
		// TODO Auto-generated method stub
		String countString = "(" + (traceInAdpater.getCount() - 1) + ")";
		txtCount.setText(countString);
	}
    /**
     * 批量导入本地通讯录
     */
	private void addMembersToLocal() {
		// TODO Auto-generated method stub
		if (traceInAdpater.getCount() > 1) {

			for (AddressMembers member : traceInAdpater.getAddressMembers()) {
				if(isExist(member.getMobile()))
					continue;
				ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
				int rawContactInsertIndex = ops.size();
				ops.add(ContentProviderOperation
						.newInsert(RawContacts.CONTENT_URI)
						.withValue(RawContacts.ACCOUNT_TYPE, null)
						.withValue(RawContacts.ACCOUNT_NAME, null).build());

				ops.add(ContentProviderOperation
						.newInsert(
								android.provider.ContactsContract.Data.CONTENT_URI)
						.withValueBackReference(Data.RAW_CONTACT_ID,
								rawContactInsertIndex)
						.withValue(Data.MIMETYPE,
								StructuredName.CONTENT_ITEM_TYPE)
						.withValue(StructuredName.GIVEN_NAME,
								member.getRemarkname()).build());
				ops.add(ContentProviderOperation
						.newInsert(
								android.provider.ContactsContract.Data.CONTENT_URI)
						.withValueBackReference(Data.RAW_CONTACT_ID,
								rawContactInsertIndex)
						.withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
						.withValue(Phone.NUMBER, member.getMobile())
						.withValue(Phone.TYPE, Phone.TYPE_MOBILE)
						.withValue(Phone.LABEL, "手机号").build());

				try {
					getContentResolver().applyBatch(
							ContactsContract.AUTHORITY, ops);
				} catch (RemoteException e) {
					logger.error(TAG, e);
					e.printStackTrace();
				} catch (OperationApplicationException e) {
					logger.error(TAG, e);
					e.printStackTrace();
				}

			}
			showToast(R.string.contact_addto_local_success);
			this.finish();

		}
		else showToast(R.string.contact_addto_local_select);
	}
	
}
