package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.ContactsAdapter;
import com.wachoo.pangker.api.ContactUserListenerManager;
import com.wachoo.pangker.api.IContactUserChangedListener;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.chat.OpenfireManager.BusinessType;
import com.wachoo.pangker.db.IAttentsDao;
import com.wachoo.pangker.db.IPKUserDao;
import com.wachoo.pangker.db.impl.AttentsDaoImpl;
import com.wachoo.pangker.db.impl.PKUserDaoImpl;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.SearchAttents;
import com.wachoo.pangker.server.response.SearchFansResult;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.PullToRefreshBase;
import com.wachoo.pangker.ui.PullToRefreshBase.OnRefreshListener;
import com.wachoo.pangker.ui.PullToRefreshListView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.ContactsUtil;

/**
 * 
 * ContactFollowActivity
 * 
 * @author wangxin
 * 
 */
public class ContactFollowActivity extends UserSearchActivity implements
		IUICallBackInterface, IContactUserChangedListener {

	private Button btnBack;
	private TextView txtTitle;
	private List<UserItem> followList = new ArrayList<UserItem>();
	private ContactsAdapter mFollowsAdapter;
	private PullToRefreshListView pullToRefreshListView;
	private ImageView iv_empty_icon;
	private TextView tv_empty_prompt;
	private ListView mFollowListView;
	private ServerSupportManager serverMana;
	private IAttentsDao iAttentsDao;
	private IPKUserDao pkUserDao;
	private PangkerApplication application;// application 旁客应用

	private final int LOOK_FAN = 0x101;
	private final int ADD_TO_FRIEND = 0x103;
	private final int ADD_TO_ATTENTION = 0x104;
	private final int ADD_TO_ATTENTION_IMPLICIT = 0x105;
	private final int queryAttent = 0x106;
	private final int REFRESH = 0x111;

	private String userId;
	private UserItem currUserItem;
	private String queryUserId = null;
	private int userType;

	private ContactUserListenerManager userListenerManager;
	private PopMenuDialog menuDialog;
	private ContactsUtil contactsUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blacklist_manager);
		init();
		initView();
		queryUserId = this.getIntent().getStringExtra(UserItem.USERID);
		userType = this.getIntent().getIntExtra("userType", 0);
		if (userType == 1 && queryUserId != null) {
			txtTitle.setText("Ta的关注人");
			queryAttent(queryUserId);
		} else {
			if (queryUserId != null) {
				txtTitle.setText("Ta的粉丝");
				reFresh(queryUserId);
			} else {
				txtTitle.setText("我的粉丝");
				reFresh(userId);
			}
		}

	}

	private void init() {
		// TODO Auto-generated method stub
		userId = ((PangkerApplication) getApplication()).getMyUserID();
		contactsUtil = new ContactsUtil(this, userId);
		serverMana = new ServerSupportManager(this, this);
		iAttentsDao = new AttentsDaoImpl(this);
		pkUserDao = new PKUserDaoImpl(this);
		application = (PangkerApplication) getApplication();
		userListenerManager = PangkerManager.getContactUserListenerManager();
		userListenerManager.addUserListenter(this);
		mImageResizer = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);
	}

	private void initView() {
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		txtTitle = (TextView) findViewById(R.id.mmtitle);

		findViewById(R.id.iv_more).setVisibility(View.GONE);
		pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list_members);
		pullToRefreshListView.setTag("pk_contacts_follows");
		pullToRefreshListView.setRefreshing(true);
		pullToRefreshListView
				.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(
							PullToRefreshBase<ListView> refreshView) {
						// TODO Auto-generated method stub
						reset();
						if (userType == 1 && queryUserId != null) {
							txtTitle.setText("Ta的关注人");
							queryAttent(queryUserId);
						} else {
							if (queryUserId != null) {
								txtTitle.setText("Ta的粉丝");
								reFresh(queryUserId);
							} else {
								txtTitle.setText("我的粉丝");
								reFresh(userId);
							}
						}
					}
				});
		mFollowListView = pullToRefreshListView.getRefreshableView();

		View view = LayoutInflater.from(this).inflate(R.layout.empty_view,
				null, false);
		iv_empty_icon = (ImageView) view.findViewById(R.id.iv_empty_icon);
		iv_empty_icon.setImageDrawable(null);
		tv_empty_prompt = (TextView) view.findViewById(R.id.textViewEmpty);
		tv_empty_prompt.setText("");
		mFollowListView.setEmptyView(view);

		mFollowListView.setOnItemClickListener(mOnItemClickListener);
		mFollowListView.setOnItemLongClickListener(mOnItemLongClickListener);
		mFollowsAdapter = new ContactsAdapter(this, new ArrayList<UserItem>(), mImageResizer);
		mFollowListView.setAdapter(mFollowsAdapter);
		mFollowsAdapter.notifyDataSetChanged();

		initPressBar(mFollowListView);

		loadmoreView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (lyLoadPressBar.getVisibility() == View.GONE
						&& lyLoadText.getVisibility() == View.VISIBLE) {
					if (isLoadAll()) {
						loadText.setText(R.string.nomore_data);
					} else {
						if (userType == 1 && queryUserId != null) {
							txtTitle.setText("Ta的关注人");
							queryAttent(queryUserId);
						} else {
							if (queryUserId != null) {
								txtTitle.setText("Ta的粉丝");
								reFresh(queryUserId);
							} else {
								txtTitle.setText("我的粉丝");
								reFresh(userId);
							}
						}
					}
				}
			}
		});

	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btnBack) {
				finish();
			}
		}
	};

	/**
	 * 关注管理、添加或者删除
	 * 
	 * @param groupid
	 *            被操作组Id
	 * @param fanId
	 *            被添加或删除对象 接口：AttentManager.json
	 */
	private void attentionManager(String attentionId, String attentType,
			int caseKey) {
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(new Parameter("appuserid", userId));
		paras.add(new Parameter("relationuserid", attentionId));
		paras.add(new Parameter("groupid", ""));
		paras.add(new Parameter("type", "1"));
		paras.add(new Parameter("attentType", attentType));// 关注类型 0 : 隐式关注
		// 1：显式关注
		// String mess = getResourcesMessage(R.string.doing_request_server);
		serverMana.supportRequest(Configuration.getAttentManager(), paras,
				false, null, caseKey);
	}

	/**
	 * 查询他人的关注人
	 */
	public void queryAttent(String otherUserId) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", otherUserId));
		paras.add(getSearchLimit());
		// String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getSearchAttents(), paras,
				false, null, queryAttent);
	}

	/**
	 * 列表项单击监听器
	 */
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			currUserItem = (UserItem) arg0.getAdapter().getItem(arg2);
			toUserInfo(currUserItem);
		}
	};

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			// 查看资料
			case LOOK_FAN:
				toUserInfo(currUserItem);
				break;
			// 添加为好友
			case ADD_TO_FRIEND:
				if (!currUserItem.getUserId().trim().equals(userId)) {
					if (application.IsFriend(currUserItem.getUserId())) {
						showToast(PangkerConstant.MSG_FRIEND_ISEXIST);
					} else {
						OpenfireManager openfireManager = application
								.getOpenfireManager();
						if (openfireManager.isLoggedIn()) {
							if (openfireManager.createEntry(
									currUserItem.getUserId(),
									currUserItem.getUserName(), null)) {
								showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_SUECCESS);
							} else {
								showToast(PangkerConstant.MSG_FRIEND_ADDAPPLY_FAIL);
							}
						} else {
							showToast(R.string.not_logged_cannot_operating);
						}
					}
				} else
					showToast(R.string.can_not_addfriend_self);
				break;
			// 添加为关注人
			case ADD_TO_ATTENTION:

				attentionManager(currUserItem.getUserId(), "1",
						ADD_TO_ATTENTION);
				break;
			case ADD_TO_ATTENTION_IMPLICIT:
				// if (!PangkerManager.getOpenfireManager().isLoggedIn()) {
				// showToast(R.string.not_logged_cannot_operating);
				// break;
				// }
				attentionManager(currUserItem.getUserId(), "0",
						ADD_TO_ATTENTION_IMPLICIT);
				break;
			}
			menuDialog.dismiss();
		}
	};

	private void showPopMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this, R.style.MyDialog);
			menuDialog.setListener(mClickListener);
			ActionItem[] msgMenu = new ActionItem[] {
					new ActionItem(LOOK_FAN,
							getString(R.string.title_look_friend)),
					new ActionItem(ADD_TO_ATTENTION,
							getString(R.string.add_to_attent)),
					new ActionItem(ADD_TO_ATTENTION_IMPLICIT,
							getString(R.string.add_to_attent_implicit)),
					new ActionItem(ADD_TO_FRIEND,
							getString(R.string.add_to_friend)) };
			menuDialog.setMenus(msgMenu);
		}
		menuDialog.setTitle(currUserItem.getUserName());
		menuDialog.show();
	}

	/**
	 * 列表项长按监听器
	 */
	private OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) {
			currUserItem = (UserItem) arg0.getAdapter().getItem(position);
			showPopMenu();
			return true;
		}
	};

	/**
	 * 查看基本信息
	 * 
	 * @param user
	 */
	private void toUserInfo(UserItem user) {
		Intent intent = new Intent(this, UserWebSideActivity.class);
		intent.putExtra("userid", user.getUserId());
		intent.putExtra("username", user.getUserName());
		intent.putExtra("usersign", user.getSign());
		startActivity(intent);
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// mSearchLayout.setVisibility(View.VISIBLE);
		if (!HttpResponseStatus(response)) {
			pullToRefreshListView.onRefreshComplete();
			if (caseKey == REFRESH) {
				iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
				tv_empty_prompt.setText(R.string.no_network);
			}
			lyLoadPressBar.setVisibility(View.GONE);
			lyLoadText.setVisibility(View.GONE);
			return;
		}

		if (caseKey == ADD_TO_ATTENTION || caseKey == ADD_TO_ATTENTION_IMPLICIT) {
			BaseResult addAttent = JSONUtil.fromJson(response.toString(),
					BaseResult.class);
			if (addAttent != null
					&& addAttent.getErrorCode() == BaseResult.SUCCESS) {
				if (caseKey == ADD_TO_ATTENTION) {
					currUserItem.setAttentType(UserItem.ATTENT_TYPE_COMMON);
					application.getOpenfireManager().sendContactsChangedPacket(
							userId, currUserItem.getUserId(),
							BusinessType.addattent);
				} else
					currUserItem.setAttentType(UserItem.ATTENT_TYPE_IMPLICIT);
				pkUserDao.saveUser(currUserItem);
				iAttentsDao.addAttent(currUserItem,
						String.valueOf(PangkerConstant.DEFAULT_GROUPID));
				contactsUtil.addAttentCount(1);
				userListenerManager.refreshUI(
						ContactAttentionActivity.class.getName(),
						PangkerConstant.STATE_CHANGE_REFRESHUI);
				showToast(addAttent.getErrorMessage());
			} else if (addAttent.getErrorCode() == BaseResult.FAILED) {
				showToast(addAttent.getErrorMessage());
			} else
				showToast(R.string.add_attent_fail);
		} else if (caseKey == REFRESH) {
			pullToRefreshListView.onRefreshComplete();
			mFollowListView.setBackgroundDrawable(null);
			setPresdBarVisiable(false);
			SearchFansResult fansResult = JSONUtil.fromJson(
					response.toString(), SearchFansResult.class);
			if (fansResult != null && fansResult.getUsers() != null
					&& fansResult.getUsers().size() > 0) {
				if (fansResult.getUsers().size() < incremental) {
					setLoadAll(true);
					lyLoadPressBar.setVisibility(View.GONE);
					lyLoadText.setVisibility(View.GONE);
				} else {
					lyLoadPressBar.setVisibility(View.GONE);
					lyLoadText.setVisibility(View.VISIBLE);
				}
				// 如果不是从0开始加载
				if (startLimit != 0) {
					followList.addAll(fansResult.getUsers());
				}
				// 首次加载
				else {
					if (!isLoadAll) {
						lyLoadPressBar.setVisibility(View.GONE);
						lyLoadText.setVisibility(View.VISIBLE);
					}
					followList = fansResult.getUsers();
					if (followList.size() == 0) {
						iv_empty_icon
								.setImageResource(R.drawable.emptylist_icon);
						if (queryUserId != null) {
							tv_empty_prompt
									.setText(R.string.him_no_fans_prompt);
						} else {
							tv_empty_prompt.setText(R.string.no_fans_prompt);
						}
					}
				}
				resetStartLimit();
				// 更新列表数据
				mFollowsAdapter.setmUserList(followList);
			} else if (fansResult != null && fansResult.getUsers() == null
					|| fansResult.getUsers().size() == 0) {
				if (startLimit == 0) {
					iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
					if (queryUserId != null) {
						tv_empty_prompt.setText(R.string.him_no_fans_prompt);
					} else {
						tv_empty_prompt.setText(R.string.no_fans_prompt);
					}

				} else {
					showToast(R.string.nomore_data);
				}

				setLoadAll(true);
				lyLoadPressBar.setVisibility(View.GONE);
				lyLoadText.setVisibility(View.GONE);
			} else {
				if (startLimit == 0) {
					iv_empty_icon.setImageResource(R.drawable.emptylist_icon);
					tv_empty_prompt.setText(R.string.load_data_fail);
				} else {
					loadText.setText(R.string.load_data_fail);
				}

				lyLoadPressBar.setVisibility(View.GONE);
				lyLoadText.setVisibility(View.VISIBLE);
			}

		} else if (caseKey == queryAttent) {
			pullToRefreshListView.onRefreshComplete();
			SearchAttents friendsResult = JSONUtil.fromJson(
					response.toString(), SearchAttents.class);
			if (friendsResult != null
					&& friendsResult.getErrorCode() == BaseResult.SUCCESS) {

				if (friendsResult.getAttentgroup() != null
						&& friendsResult.getAttentgroup().size() > 0) {
					mFollowsAdapter.setmUserList(friendsResult.getAttentgroup()
							.get(0).getMembers());
					if (mFollowsAdapter.getmUserList().size() == 0) {
						iv_empty_icon
								.setImageResource(R.drawable.emptylist_icon);
						tv_empty_prompt.setText(R.string.him_no_attents_prompt);
					}
				} else {
					if (startLimit == 0) {
						iv_empty_icon
								.setImageResource(R.drawable.emptylist_icon);
						tv_empty_prompt.setText(R.string.load_data_fail);
					} else {
						loadText.setText(R.string.load_data_fail);
					}
				}
			} else {
				showToast(getResources().getString(R.string.return_value_999));
			}
		}

	}

	/**
	 * 销毁广播
	 */
	protected void onDestroy() {
		userListenerManager.removeUserListenter(this);
		super.onDestroy();
	}

	@Override
	public boolean isHaveUser(UserItem item) {
		if (followList != null) {
			for (int i = 0; i < followList.size(); i++) {
				if (followList.get(i).getUserId().equals(item.getUserId())) {
					followList.set(i, item);
					Message message = new Message();
					message.what = PangkerConstant.STATE_CHANGE_REFRESHUI;
					mHandler.sendMessage(message);
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void refreshUser(int what) {
		// TODO Auto-generated method stub
		Message message = new Message();
		message.what = what;
		mHandler.sendMessage(message);
	}

	protected Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case PangkerConstant.STATE_CHANGE_REFRESHUI:
				mFollowsAdapter.notifyDataSetChanged();
				break;
			default:
				break;
			}
		}
	};

	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		menu.add(0, 0, 0, R.string.refresh).setIcon(
				R.drawable.actionbar_but_refresh);
		return super.onCreateOptionsMenu(menu);
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case 0:
			reset();
			if (userType == 1 && queryUserId != null) {
				txtTitle.setText("Ta的关注人");
				queryAttent(queryUserId);
			} else {
				if (queryUserId != null) {
					txtTitle.setText("Ta的粉丝");
					reFresh(queryUserId);
				} else {
					txtTitle.setText("我的粉丝");
					reFresh(userId);
				}
			}
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 刷新信息
	 */
	public void reFresh(String userId) {

		if (mFollowsAdapter.getCount() >= incremental) {
			lyLoadPressBar.setVisibility(View.VISIBLE);
			lyLoadText.setVisibility(View.GONE);
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userid", userId));
		paras.add(getSearchLimit());
		// paras.add(new Parameter("optype", "1"));//
		// 0:邀请某人加入群组，1：申请进入群组（必填）

		serverMana.supportRequest(Configuration.getSearchFans(), paras, false,
				null, REFRESH);

	}

}
