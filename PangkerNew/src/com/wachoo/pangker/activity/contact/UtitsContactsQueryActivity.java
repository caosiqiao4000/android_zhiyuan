package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.adapter.UnitsContactsAdapter;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBookResult;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;

/**
 * 单位通讯录查询页面
 * 
 * @author zhengjy
 * 
 *         2012-6-14
 * 
 */
public class UtitsContactsQueryActivity extends CommonPopActivity implements IUICallBackInterface {

	private String TAG = "UtitsContactsQueryActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();
	private final int QUERY_CONTACT = 0x10;
	private final int ADD_TO_CONTACT = 0x11;
	private final int SELECT_QUERY_TYPE = 0x12;
	private int currDialogId = -1;

	private PangkerApplication application;
	private ListView mListView;
	private UnitsContactsAdapter mContactAdapter;
	private EditText etEditName;
	private ImageButton btnMenuShow;
	private RelativeLayout mSelectTypeLayout;
	private TextView tvSelectType;

	private String currSelectType = "0";
	private String[] SELECT_TYPE_ITEMS;
	private String mUserid;
	private AddressBooks currABook;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.units_contacts_query);
		application = (PangkerApplication) getApplication();
		mUserid = application.getMyUserID();

		initView();

		// AddressBookQuery();
	}

	private void initView() {
		mListView = (ListView) findViewById(R.id.lv_units_contacts);
		btnMenuShow = (ImageButton) findViewById(R.id.btn_reply);
		etEditName = (EditText) findViewById(R.id.et_query_name);
		mSelectTypeLayout = (RelativeLayout) findViewById(R.id.selection_type);
		tvSelectType = (TextView) findViewById(R.id.tv_select_type);

		SELECT_TYPE_ITEMS = getResources().getStringArray(R.array.net_contact_query_type);
		currSelectType = "0";
		tvSelectType.setText(SELECT_TYPE_ITEMS[0]);

		mContactAdapter = new UnitsContactsAdapter(this, new ArrayList<AddressBooks>());
		mListView.setAdapter(mContactAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				currABook = (AddressBooks) arg0.getAdapter().getItem(arg2);
				// 此处需要判断是否已经加入过该单位通讯录
				if (application.IsNetAddressBookExist(currABook.getId())) {
					// showToast("你已经加入该单位通讯录!");
					// 如果已经是查询的通讯录成员，直接点击进入单位通讯录成员列表
					currABook = application.getNetAddressBook(currABook);// 同步本地版本号
					Intent intent = new Intent(UtitsContactsQueryActivity.this,
							UtitsContactUserActivity.class);
					intent.putExtra(AddressBooks.BOOK_KEY, currABook);
					UtitsContactsQueryActivity.this.startActivity(intent);
				} else
					mShowDialog(ADD_TO_CONTACT);
			}
		});

		mListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				currABook = (AddressBooks) arg0.getAdapter().getItem(arg2);
				return mListView.showContextMenu();
			}
		});
		mListView.setOnCreateContextMenuListener(new OnCreateContextMenuListener() {
			@Override
			public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
				menu.setHeaderTitle(currABook.getName());

			}
		});

		btnMenuShow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String account = etEditName.getText().toString();
				if (Util.isEmpty(account)) {
					showToast(R.string.contact_query_nonull);
					return;
				}
				AddressBookQueryByAccount(account, currSelectType);
			}
		});
		mSelectTypeLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mShowDialog(SELECT_QUERY_TYPE);
			}
		});
		Button button = (Button) findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UtitsContactsQueryActivity.this.finish();
			}
		});

	}

	/**
	 * 添加和编辑分组Dialog监听器
	 */
	private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if (dialog != null) {
				dialog.dismiss();
			}
			if (currDialogId == ADD_TO_CONTACT) {
				if (which == dialog.BUTTON_POSITIVE) {
					addToAddressBook();
				}
			} else if (currDialogId == SELECT_QUERY_TYPE) {
				currSelectType = String.valueOf(which);
				tvSelectType.setText(SELECT_TYPE_ITEMS[which]);
			}

		}
	};

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (item.getGroupId() == 0) {
			switch (item.getItemId()) {

			}
		}
		return super.onContextItemSelected(item);
	}

	/**
	 * 显示dialog前的设置
	 * 
	 * @param id
	 */
	private void mShowDialog(int id) {
		if (currDialogId != -1) {
			removeDialog(currDialogId);
		}
		currDialogId = id;
		showDialog(currDialogId);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Builder builder = new AlertDialog.Builder(this);
		if (id == ADD_TO_CONTACT) {
			builder.setTitle(currABook.getName()).setMessage(R.string.units_add_into_ifornot)
					.setNegativeButton(R.string.cancel, null)
					.setPositiveButton(R.string.submit, mDialogClickListener);
		} else if (id == SELECT_QUERY_TYPE) {
			builder.setTitle(R.string.units_query_type);
			builder.setSingleChoiceItems(SELECT_TYPE_ITEMS, Integer.parseInt(currSelectType),
					mDialogClickListener);
		}

		return builder.create();
	}

	/**
	 * 申请加入单位通讯录
	 */
	private void addToAddressBook() {
		OpenfireManager openfireManager = application.getOpenfireManager();
		if (openfireManager.isLoggedIn()) {
			showToast(R.string.net_addressbook_apply_mess);
			openfireManager.sendNetAddressBookApplyReq(mUserid, String.valueOf(currABook.getUid()),
					currABook, "jion");
		} else {
			showToast(R.string.not_logged_cannot_operating);
		}
	}

	/**
	 * 单位通讯录查询
	 * 
	 * 
	 * 接口：AddressBookQueryByAccount.json
	 */
	private void AddressBookQueryByAccount(String account, String accoutType) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", mUserid)); // 当前操作的UID。
		paras.add(new Parameter("account", account));// 账号信息
		paras.add(new Parameter("type", accoutType)); // 0:手机号码，1：用户Uid。默认为手机号码。
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getAddressBookQueryByAccount(), paras, true, mess,
				QUERY_CONTACT);
	}

	/**
	 * 加入、退出通讯录接口
	 * 
	 * 
	 * 接口：AddressBookInOut.json
	 */
	private void AddressBookInOut(String bookid) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));// 应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", mUserid)); // 加入、退出的UID
		paras.add(new Parameter("bookid", bookid)); // 加入、退出的通讯录id
		paras.add(new Parameter("type", "0")); // 操作类型(必填)：0：加入，1：退出
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getAddressBookInOut(), paras, true, mess, ADD_TO_CONTACT);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;
		if (caseKey == QUERY_CONTACT) {
			AddressBookResult addressResult = JSONUtil.fromJson(supportResponse.toString(),
					AddressBookResult.class);
			if (null == addressResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (addressResult.getErrorCode() == addressResult.SUCCESS
					&& addressResult.getBooks() != null) {
				if (addressResult.getBooks().size() == 0) {
					showToast(R.string.units_no_data);
				}
				mContactAdapter.setmContactList(addressResult.getBooks());
			} else if (addressResult.getErrorCode() == addressResult.FAILED) {
				showToast(addressResult.getErrorMessage());
			} else
				showToast(R.string.contact_query_failed);
		}

		else if (caseKey == ADD_TO_CONTACT) {
			BaseResult baseResult = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
			if (null == baseResult) {
				showToast(R.string.to_server_fail);
				return;
			} else if (baseResult.getErrorCode() == baseResult.SUCCESS) {
				showToast(baseResult.getErrorMessage());
			} else if (baseResult.getErrorCode() == baseResult.FAILED) {
				showToast(baseResult.getErrorMessage());
			}
		}

	}

}
