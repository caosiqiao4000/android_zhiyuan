package com.wachoo.pangker.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.db.INetAddressBookDao;
import com.wachoo.pangker.db.impl.NetAddressBookDaoImpl;
import com.wachoo.pangker.entity.UserInfo;
import com.wachoo.pangker.listener.TextCountLimitWatcher;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.AddressBookManagerResult;
import com.wachoo.pangker.server.response.AddressBooks;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;
/**
 * 创建单位通讯录
 * @author zhengjy
 *  2012-6-12
 *
 */
public class UtitsContactCreateActivity extends CommonPopActivity implements IUICallBackInterface {
	
	private final static int CONTACT_MANAGER =  0x10;

	private PangkerApplication application;
	private EditText etContactName;
	private Button btnSubmit;
	
	private String mUserid;
	private String contactId;//编辑单位通讯录的Id
	private String contactName; //编辑单位通讯录的名称
	private Integer optype;  //0：创建通讯录，1更改通讯录
	private UserInfo mUserInfo;
	private INetAddressBookDao iNetAddressBookDao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.units_contacts_create);
		iNetAddressBookDao = new NetAddressBookDaoImpl(this);
		application = (PangkerApplication) getApplication();
		mUserInfo = application.getMySelf();
		mUserid = application.getMyUserID();
		contactId = getIntent().getStringExtra("contactId");
		contactName = getIntent().getStringExtra("contactName");
		optype = getIntent().getIntExtra("optype", 0);
		
		initView();
	}

	private void initView() {
		etContactName = (EditText)findViewById(R.id.et_name);
		btnSubmit =  (Button)findViewById(R.id.btn_submit);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		etContactName.addTextChangedListener(new TextCountLimitWatcher(20,etContactName));
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(optype == 1){
					contactManager(contactId,String.valueOf(optype));
				}
				else contactManager("",String.valueOf(optype));
			}
		});
		TextView mmtitle = (TextView) findViewById(R.id.mmtitle);
		mmtitle.setText("通讯录创建");
		
		if(optype == 1){
			etContactName.setText(contactName);
		}
		Button button = (Button)findViewById(R.id.btn_back);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
						UtitsContactCreateActivity.this.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
				UtitsContactCreateActivity.this.finish();
			}
		});

	}
	
	/**
	 * 单位通讯录管理
	 * 
	 * 
	 * 接口：AddressBookManager.json
	 */
	private void contactManager(String contactId,String optype) {
		String name = etContactName.getText().toString();
		if(!optype.equals("2") && Util.isEmpty(name)){
			showToast(R.string.units_contact_name_nonull);
			return;
		}
		ServerSupportManager serverMana = new ServerSupportManager(this,this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("appid", "1"));//应用系统appid，默认填写“1”
		paras.add(new Parameter("uid", mUserid));
		paras.add(new Parameter("type", optype));//操作类型(必填)：0：创建通讯录，1更改通讯录，2：删除通讯录。
		paras.add(new Parameter("name", name));//通讯录名称，optype=0，1时必填
		paras.add(new Parameter("id", contactId));
		String mess = getResourcesMessage(R.string.please_wait);
		serverMana.supportRequest(Configuration.getAddressBookManager(),paras,true,mess, CONTACT_MANAGER);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		if (!HttpResponseStatus(supportResponse))
			return;
		if(caseKey == CONTACT_MANAGER){
			AddressBookManagerResult baseResult = JSONUtil.fromJson(supportResponse.toString(), AddressBookManagerResult.class);
			if(baseResult.getErrorCode() == BaseResult.SUCCESS){
				showToast(baseResult.getErrorMessage());
				AddressBooks mAddressBook = new AddressBooks();
				mAddressBook.setId(Long.parseLong(baseResult.getId()));
				mAddressBook.setFlag(AddressBooks.FLAG_MYSELF);
				mAddressBook.setMobile(mUserInfo.getPhone());
				mAddressBook.setNum(0);
				mAddressBook.setUid(Long.parseLong(mUserid));
				mAddressBook.setCreatetime(Util.getSysNowTime());
				mAddressBook.setName(etContactName.getText().toString());
				mAddressBook.setUsername(mUserInfo.getUserName());
				mAddressBook.setVersion(Long.parseLong("-1"));
				iNetAddressBookDao.insertAddressBook(mUserid,mAddressBook);
				setResult(RESULT_OK);
				this.finish();
			} else if(baseResult != null){
				showToast(baseResult.getErrorMessage());
			}
		}
		
	}

}
