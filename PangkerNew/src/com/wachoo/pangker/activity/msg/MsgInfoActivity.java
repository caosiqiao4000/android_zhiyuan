package com.wachoo.pangker.activity.msg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerManager;
import com.wachoo.pangker.activity.CommonActivity;
import com.wachoo.pangker.activity.MainActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.activity.group.ChatRoomInfoActivity;
import com.wachoo.pangker.activity.group.ChatRoomListActivity;
import com.wachoo.pangker.activity.group.ChatRoomMainActivity;
import com.wachoo.pangker.activity.res.ResRecommendActivity;
import com.wachoo.pangker.adapter.MsgInfoAdapter;
import com.wachoo.pangker.api.ITabSendMsgListener;
import com.wachoo.pangker.api.TabBroadcastManager;
import com.wachoo.pangker.chat.IMessageListener;
import com.wachoo.pangker.chat.MessageChangedManager;
import com.wachoo.pangker.db.IMsgInfoDao;
import com.wachoo.pangker.db.impl.MsgInfoDaoImpl;
import com.wachoo.pangker.entity.MsgInfo;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.entity.ChatMessage;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.GroupEnterCheckResult;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.server.response.UserGroup;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.ConfrimDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.MessageTipDialog.DialogMsgCallback;
import com.wachoo.pangker.ui.dialog.PasswordDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;
import com.wachoo.pangker.util.Util;

/**
 * 消息展示界面，以msgId和type为准,按时间排序
 * 
 * @author zxx 2013年1月7日15:04:04： 涉及到群组的消息，Theme是查询不到的，为此而修改信息的数据库
 */
public class MsgInfoActivity extends CommonActivity implements IMessageListener, IUICallBackInterface,
		ITabSendMsgListener {

	private String userId;
	private IMsgInfoDao iMsgInfoDao;
	private ListView msgListView;
	private MsgInfoAdapter msgInfoAdapter;
	private TabBroadcastManager tabBroadcastManager;
	private MessageChangedManager messageChangedManagera;
	private MsgInfo msgInfo;
	private PangkerApplication application;
	private PopMenuDialog menuDialog;
	private ConfrimDialog confimDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview);
		init();
		initView();

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// 首次登录旁客设置向导
		// addGuideImage(MsgInfoActivity.class.getName(),R.drawable.talk_01);
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			msgInfoAdapter.updateMsgInfo((MsgInfo) msg.obj);
		};
	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		tabBroadcastManager.removeUserListenter(this);
		super.onDestroy();
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		userId = application.getMyUserID();
		iMsgInfoDao = new MsgInfoDaoImpl(this);
		tabBroadcastManager = PangkerManager.getTabBroadcastManager();
		tabBroadcastManager.addMsgListener(this);
		messageChangedManagera = PangkerManager.getMessageChangedManager();
		messageChangedManagera.addListenter(this);

	}

	private void initView() {
		// TODO Auto-generated method stub
		msgListView = (ListView) findViewById(R.id.lv_contact_follow);
		msgInfoAdapter = new MsgInfoAdapter(this, iMsgInfoDao.getAllMsgInfos(userId));
		EmptyView emptyView = new EmptyView(this);
		emptyView.showView(R.drawable.emptylist_icon, getString(R.string.message_history_nohistory));
		emptyView.addToListView(msgListView);
		msgListView.setAdapter(msgInfoAdapter);
		msgListView.setOnItemClickListener(onItemClickListener);
		msgListView.setOnItemLongClickListener(onItemLongClickListener);
	}

	public void lookUserInfo(MsgInfo item) {
		// TODO Auto-generated method stub
		if (item.getType() == MsgInfo.MSG_PERSONAL_CHAT ) {
			Intent intent = new Intent(this, UserWebSideActivity.class);
			intent.putExtra(UserItem.USERID, item.getFromId());
			startActivity(intent);
		}
		// 显示群组信息
		if (item.getType() == MsgInfo.MSG_GROUP_CHAT) {
			lookChatRoomInfo(item);
		}
	}

	private void lookChatRoomInfo(MsgInfo item) {
		Intent intent = new Intent();
		List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
		GroupLbsInfo lbsInfo = new GroupLbsInfo();
		lbsInfo.setgName(item.getTheme());
		lbsInfo.setSid(Long.parseLong(item.getFromId()));
		lbsInfo.setShareUid(userId);
		groupList.add(lbsInfo);
		intent.putExtra("GroupInfo", (Serializable) groupList);
		intent.putExtra("group_index", 0);
		intent.setClass(this, ChatRoomInfoActivity.class);
		this.startActivity(intent);
	}

	// 在其他地方接受到消息之后，对消息显示进行处理
	private void chageMsgInfo(int type, String fromId) {
		for (MsgInfo mInfo : msgInfoAdapter.getMsgInfos()) {
			if (mInfo.getType() == type && fromId.equals(mInfo.getFromId())) {
				if (mInfo.getUncount() > 0) {
					((MainActivity) getParent()).removeUnReadMessgae(mInfo.getUncount());
				}
				mInfo.setUncount(0);
				iMsgInfoDao.updateMsgInfo(mInfo);
			}
		}
		msgInfoAdapter.notifyDataSetChanged();

	}

	public void lookMsgInfo() {
		// >>>>>>>通知减少未读消息
		if (msgInfo.getUncount() > 0) {
			((MainActivity) getParent()).removeUnReadMessgae(msgInfo.getUncount());
		}
		// >>>>>>>设置所有消息已读
		msgInfo.setUncount(0);
		msgInfoAdapter.notifyDataSetChanged();
		iMsgInfoDao.updateMsgInfo(msgInfo);
		if (msgInfo.getType() == MsgInfo.MSG_RECOMMEND) {
			Intent intent = new Intent(this, ResRecommendActivity.class);
			startActivity(intent);
		}
		if (msgInfo.getType() == MsgInfo.MSG_NOTICE || msgInfo.getType() == MsgInfo.MSG_APPLY
				|| msgInfo.getType() == MsgInfo.MSG_INVITION) {
			Intent intent = new Intent(this, NoticeInfoActivity.class);
			intent.putExtra("NOTICE_TYPE", msgInfo.getType());
			startActivity(intent);
		}
		if (msgInfo.getType() == MsgInfo.MSG_PERSONAL_CHAT) {
			Intent intent = new Intent();
			intent.putExtra(ChatMessage.USERID, msgInfo.getFromId());
			intent.putExtra(ChatMessage.USERNAME, msgInfo.getTheme());
			intent.setClass(this, MsgChatActivity.class);
			startActivity(intent);
		}
		if (msgInfo.getType() == MsgInfo.MSG_GROUP_CHAT) {
			backtoGroup();
		}
		if (msgInfo.getType() == MsgInfo.MSG_TIP) {
			Intent intent = new Intent();
			intent.setClass(this, MsgTipsActivity.class);
			startActivity(intent);
		}
		if (msgInfoAdapter.ifReadAll()) {
			((MainActivity) getParent()).reSetMsgcount();
		}
		application.getMsgInfoBroadcast().canceNotify();
	}

	private void deleteMsgInfo() {
		// TODO Auto-generated method stub
		if (iMsgInfoDao.delMsgInfo(msgInfo)) {
			msgInfoAdapter.delMsgInfo(msgInfo);
			if (msgInfoAdapter.getCount() == 0) {
				((MainActivity) getParent()).reSetMsgcount();
			}
		}
	}

	private void clearMsgInfo() {
		// TODO Auto-generated method stub
		if (confimDialog == null) {
			confimDialog = new ConfrimDialog(this);
		}
		confimDialog.showConfrimDialog("清空会话列表", "确认要清空所有会话吗？(相关聊天记录仍会保留)", onDialogClickListener);
	}

	ConfrimDialog.OnDialogClickListener onDialogClickListener = new ConfrimDialog.OnDialogClickListener() {
		@Override
		public void onClick(Dialog dialog, boolean positive) {
			// TODO Auto-generated method stub
			if (positive) {
				if (iMsgInfoDao.clearMsgInfos(userId)) {
					msgInfoAdapter.clearMsgIns();
				}
			}
		}
	};

	// 1:自己不在任何一个群组中，2：在一个群组中，但不在点击的群组，3：就在当前的群组中
	private void backtoGroup() {
		// TODO Auto-generated method stub
		if (application.getCurrunGroup() != null // 3：就在当前的群组中
				&& application.getCurrunGroup().getUid() != null
				&& application.getCurrunGroup().getGroupId().equals(msgInfo.getFromId())) {
			Intent intent = new Intent();
			intent.setClass(this, ChatRoomMainActivity.class);
			startActivity(intent);

		} else if (application.getCurrunGroup() != null // 2：在一个群组中，但不在点击的群组
				&& application.getCurrunGroup().getUid() != null
				&& !application.getCurrunGroup().getGroupId().equals(msgInfo.getFromId())) {
			MessageTipDialog mDialog = new MessageTipDialog(new DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-genaerated method stub
					if (isSubmit) {
						// 首先要关闭之前的群组界面
						PangkerManager.getActivityStackManager().popOneActivity(ChatRoomMainActivity.class);
						intoGroup(null);
					}
				}
			}, this);
			mDialog.showDialog("", "您已经在一个群组里面,进入此群组会退出当前所在的群组,是否确认进入?", false);
		} else {// 1:自己不在任何一个群组中
			intoGroup(null);
		}
	}

	private void intoGroup(String password) {
		ServerSupportManager serverMana = new ServerSupportManager(getParent(), this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("opuid", userId));
		paras.add(new Parameter("groupid", msgInfo.getFromId()));
		paras.add(new Parameter("optype", "0"));//
		// 1：申请进入群组（必填）；2:被邀请加入；3：直接进入（自己的群组等等 ）
		if (password != null) {
			paras.add(new Parameter("password", password));// 群组密码，需要密码校验时使用
		}
		serverMana.supportRequest(Configuration.getGroupEnterCheck(), paras, true, "正在进入群组...",
				ChatRoomListActivity.GROUP_ENTER);
	}

	OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			msgInfo = (MsgInfo) parent.getItemAtPosition(position);
			lookMsgInfo();
		}
	};

	OnItemLongClickListener onItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			msgInfo = (MsgInfo) parent.getItemAtPosition(position);
			showPopMenu();
			return true;
		}
	};

	private void showPopMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this);
			menuDialog.setListener(mClickListener);
			ActionItem[] msgMenu = new ActionItem[] { new ActionItem(1, getString(R.string.notice_del)),
					new ActionItem(2, getString(R.string.notice_empty)) };
			menuDialog.setMenus(msgMenu);
		}
		menuDialog.setTitle(R.string.notice_title);
		menuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			case 1:
				deleteMsgInfo();
				break;
			case 2:
				clearMsgInfo();
				break;
			}
			menuDialog.dismiss();
		}
	};

	@Override
	public void messageChanged(MsgInfo msgInfo) {
		// TODO Auto-generated method stub
		Message msg = new Message();
		msg.obj = msgInfo;
		handler.sendMessage(msg);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (HttpResponseStatus(supportResponse)) {
			if (caseKey == ChatRoomListActivity.GROUP_ENTER) {
				GroupEnterCheckResult result = JSONUtil.fromJson(supportResponse.toString(),
						GroupEnterCheckResult.class);
				if (result == null) {
					showToast("查询失败,还无法登录该群组!");
					return;
				}
				if (result.getErrorCode() == BaseResult.SUCCESS) {
					UserGroup enterGroup = result.getUserGroup();
					if (enterGroup != null) {
						enterGroup.setLastVisitTime(Util.date2Str(new Date()));
						Intent intent = new Intent();
						// >>>>>wangxin add 进入群组
						intent.putExtra(ChatRoomMainActivity.JION_TO_GROUP, enterGroup);
						intent.setClass(this, ChatRoomMainActivity.class);
						startActivity(intent);
					} else {
						showToast("查询失败,还无法登录该群组!");
					}
				} else if (result.getErrorCode() == BaseResult.OTHER) {
					showPassword();
				}
			}
		}
	}

	private void showPassword() {
		// TODO Auto-generated method stub
		final PasswordDialog passwordDialog = new PasswordDialog(this);
		passwordDialog.setTitle("群组会议室");
		passwordDialog.setResultCallback(new DialogMsgCallback() {
			@Override
			public void buttonResult(boolean isSubmit) {
				// TODO Auto-generated method stub
				if (isSubmit && passwordDialog.checkPass()) {
					intoGroup(passwordDialog.getPassword());
				}
			}
		});
		passwordDialog.showDialog();
	}

	@Override
	public void onMessageListener(Message msg) {
		// TODO Auto-generated method stub
		// >>>>>>>>>>>去掉消息项目!!!例如：系统消息，进入系统消息界面，清除所有系统消息之后，通知会话界面不显示该项目
		if (msg.what == 1) {
			deleteMsgInfo();
		}
		// 通知消息已读
		if (msg.what == 2) {
			// >>>>>>>>处理会话显示
			chageMsgInfo(msg.arg1, msg.obj.toString());
			// >>>>>>>通知主界面消息已读
			if (msgInfoAdapter.ifReadAll()) {
				((MainActivity) getParent()).reSetMsgcount();
			}
		}
	}

}
