package com.wachoo.pangker.activity.msg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.group.ChatRoomInfoActivity;
import com.wachoo.pangker.activity.res.PictureInfoActivity;
import com.wachoo.pangker.activity.res.ResInfoActivity;
import com.wachoo.pangker.activity.res.ResSpeaktInfoActivity;
import com.wachoo.pangker.adapter.MessageTipsAdapter;
import com.wachoo.pangker.db.ITipsDao;
import com.wachoo.pangker.db.impl.TipDaoImpl;
import com.wachoo.pangker.entity.MessageTip;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.server.response.GroupLbsInfo;
import com.wachoo.pangker.ui.ActionItem;
import com.wachoo.pangker.ui.EmptyView;
import com.wachoo.pangker.ui.UITableView;
import com.wachoo.pangker.ui.dialog.MessageTipDialog;
import com.wachoo.pangker.ui.dialog.PopMenuDialog;

public class MsgTipsActivity extends CommonPopActivity {

	PangkerApplication application;
	private ITipsDao tipsDao;
	private ListView tipListView;
	private MessageTipsAdapter tipsAdapter;
	private EmptyView mEmptyView;
	private MessageTip messageTip;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.local_listview);
		init();
		initView();
		initData();
	}

	private void init() {
		// TODO Auto-generated method stub
		application = (PangkerApplication) getApplication();
		tipsDao = new TipDaoImpl(this);
	}

	private void initView() {
		// TODO Auto-generated method stub
		TextView mmtitle = (TextView) findViewById(R.id.mmtitle);
		mmtitle.setText("提示消息");
		findViewById(R.id.progress_ly).setVisibility(View.GONE);
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		Button iv_more = (Button) findViewById(R.id.iv_more);
		iv_more.setText(R.string.clear);
		iv_more.setOnClickListener(onClickListener);

		tipListView = (ListView) findViewById(R.id.musicListView);
		tipListView.setOnItemClickListener(onItemClickListener);
		tipListView.setOnItemLongClickListener(onItemLongClickListener);
		mEmptyView = new EmptyView(this);
		mEmptyView.addToListView(tipListView);
		mEmptyView.showView(R.drawable.emptylist_icon, getString(R.string.message_history_nohistory));
	}

	private void initData() {
		// TODO Auto-generated method stub
		List<MessageTip> tips = tipsDao.getMessageTips(application.getMyUserID());
		tipsAdapter = new MessageTipsAdapter(this, tips, initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH));
		tipListView.setAdapter(tipsAdapter);
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() == R.id.btn_back) {
				onBackPressed();
			}
			if (v.getId() == R.id.iv_more) {
				showTipDialog();
			}
		}
	};

	OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			messageTip = (MessageTip) parent.getItemAtPosition(position);
			lookMessageTip();
		}
	};

	OnItemLongClickListener onItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			messageTip = (MessageTip) parent.getItemAtPosition(position);
			showPopMenu();
			return true;
		}
	};

	private PopMenuDialog menuDialog;

	private void showPopMenu() {
		// TODO Auto-generated method stub
		if (menuDialog == null) {
			menuDialog = new PopMenuDialog(this);
			menuDialog.setListener(mClickListener);
			ActionItem[] msgMenu = new ActionItem[] { new ActionItem(1, getString(R.string.notice_del)),
					new ActionItem(2, getString(R.string.notice_empty)) };
			menuDialog.setMenus(msgMenu);
		}
		menuDialog.setTitle(R.string.notice_title);
		menuDialog.show();
	}

	UITableView.ClickListener mClickListener = new UITableView.ClickListener() {
		@Override
		public void onClick(int actionId) {
			switch (actionId) {
			case 1:
				deleteMessageTip();
				break;
			case 2:
				showTipDialog();
				break;
			}
			menuDialog.dismiss();
		}
	};

	private ResShowEntity toResShowEntity(MessageTip tip) {
		// TODO Auto-generated method stub
		ResShowEntity entity = new ResShowEntity();
		entity.setCommentTimes(0);
		entity.setResType(tip.getResType());
		entity.setDownloadTimes(0);
		entity.setFavorTimes(0);
		entity.setResID(Long.valueOf(tip.getResId()));
		// >>>>>>>如果是我转播的资源
		if (tip.getTipType() == MessageTip.TIP_COMMENT_BROADCAST_RES 
				|| messageTip.getTipType() == MessageTip.TIP_COMMENT_RES) {
			entity.setShareUid(application.getMyUserID());
		}
		entity.setScore(0);
		entity.setResName(tip.getResName());
		entity.setSource(0);
		return entity;
	}

	// >>>>>查看信息提示
	private void lookMessageTip() {
		// 群组详细界面
		if (messageTip.getResType() == PangkerConstant.RES_BROADCAST) {
			lookGroupInfo();
		}
		//图片信息界面
		else if (messageTip.getResType() == PangkerConstant.RES_PICTURE) {
			lookPicInfo();
		} 
		//说说详细界面
		else if (messageTip.getResType() == PangkerConstant.RES_SPEAK) {
			lookSpeakfo();
		}
		//文档、音乐， 应用详细界面
		else if (messageTip.getResType() == PangkerConstant.RES_MUSIC
				|| messageTip.getResType() == PangkerConstant.RES_APPLICATION
				|| messageTip.getResType() == PangkerConstant.RES_DOCUMENT){
			lookResInfo();
		}
		this.finish();
	}

	private void lookSpeakfo() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra("speakId", messageTip.getResId());
		intent.setClass(this, ResSpeaktInfoActivity.class);
		startActivity(intent);
	}

	private void lookGroupInfo() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		List<GroupLbsInfo> groupList = new ArrayList<GroupLbsInfo>();
		GroupLbsInfo lbsInfo = new GroupLbsInfo();
		lbsInfo.setgName(messageTip.getResName());
		lbsInfo.setSid(Long.parseLong(messageTip.getResId()));
		// >>>>>>>如果是我转播的资源
		if (messageTip.getTipType() == MessageTip.TIP_COMMENT_BROADCAST_RES
				|| messageTip.getTipType() == MessageTip.TIP_COMMENT_RES) {
			lbsInfo.setShareUid(application.getMyUserID());
		}
		groupList.add(lbsInfo);
		intent.putExtra("GroupInfo", (Serializable) groupList);
		intent.putExtra("group_index", 0);
		intent.setClass(this, ChatRoomInfoActivity.class);
		startActivity(intent);

	}

	private void lookPicInfo() {
		// TODO Auto-generated method stub
		List<ResShowEntity> showList = new ArrayList<ResShowEntity>();
		Intent intent = new Intent();
		intent.setClass(this, PictureInfoActivity.class);
		showList.add(toResShowEntity(messageTip));
		application.setViewIndex(0);
		application.setResLists(showList);
		startActivity(intent);
	}

	private void lookResInfo() {
		// TODO Auto-generated method stub
		List<ResShowEntity> showList = new ArrayList<ResShowEntity>();
		Intent intent = new Intent();
		intent.setClass(this, ResInfoActivity.class);
		showList.add(toResShowEntity(messageTip));
		application.setResLists(showList);
		application.setViewIndex(0);
		startActivity(intent);
	}

	// >>>>>清空信息提示
	private void clearMessageTips() {
		// TODO Auto-generated method stub
		if (tipsDao.clearMessageTips(application.getMyUserID())) {
			tipsAdapter.clearMessageTip();
		}
	}

	private MessageTipDialog tipDialog;

	private void showTipDialog() {
		// TODO Auto-generated method stub
		if (tipsAdapter.getCount() == 0) {
			return;
		}
		if (tipDialog == null) {
			tipDialog = new MessageTipDialog(new MessageTipDialog.DialogMsgCallback() {
				@Override
				public void buttonResult(boolean isSubmit) {
					// TODO Auto-generated method stub
					if (isSubmit) {
						clearMessageTips();
					}
				}
			}, this);
		}
		tipDialog.showDialog(null, "确认要清空所有提示消息?");
	}

	// >>>>>删除信息提示
	private void deleteMessageTip() {
		// TODO Auto-generated method stub
		if (tipsDao.delMessageTip(messageTip)) {
			tipsAdapter.delMessageTip(messageTip);
		}
	}
}
