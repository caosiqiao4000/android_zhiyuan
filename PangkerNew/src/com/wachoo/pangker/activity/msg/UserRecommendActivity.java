package com.wachoo.pangker.activity.msg;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.PangkerConstant;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.ContactsSelectActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.UserWebSideActivity;
import com.wachoo.pangker.adapter.GroupInfoUserAdapter;
import com.wachoo.pangker.chat.OpenfireManager;
import com.wachoo.pangker.db.IRecommendDao;
import com.wachoo.pangker.db.IUserStatusDao;
import com.wachoo.pangker.db.impl.RecommendDaoImpl;
import com.wachoo.pangker.db.impl.UserStatusDaoImpl;
import com.wachoo.pangker.entity.RecommendInfo;
import com.wachoo.pangker.entity.ResShowEntity;
import com.wachoo.pangker.entity.UserItem;
import com.wachoo.pangker.image.PKIconResizer;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.server.response.UserStatus;
import com.wachoo.pangker.util.Util;

/**
 * 
 * @TODO 资源推荐给旁客好友
 * 
 * @author zhengjy
 * 
 *         2012-9-6
 * 
 */
public class UserRecommendActivity extends CommonPopActivity implements IUICallBackInterface,
		OnFocusChangeListener {

	private final int mRequestCode = 0x10;

	PangkerApplication application;
	private com.wachoo.pangker.ui.MyGridView gvUuseritem;
	private GroupInfoUserAdapter userAdapter;
	private EditText etDetail;
	private ResShowEntity resShowEntity;
	private String mUserId;
	private IRecommendDao iRecommendDao;
	private RecommendInfo recommendInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_recommend);

		application = (PangkerApplication) getApplication();
		resShowEntity = (ResShowEntity) getIntent().getSerializableExtra(ResShowEntity.RES_ENTITY);
		mUserId = application.getMyUserID();
		iRecommendDao = new RecommendDaoImpl(this);

		initView();

		List<UserItem> selectedUser = (List<UserItem>) this.getIntent().getSerializableExtra("selectedUser");
		if (selectedUser != null && selectedUser.size() > 0) {
			userAdapter.setUserList(selectedUser);
		}

	}

	private void initView() {
		Button btnRight = (Button) findViewById(R.id.iv_more);
		btnRight.setVisibility(View.GONE);

		TextView tvTitle = (TextView) findViewById(R.id.mmtitle);
		tvTitle.setText("准备发送");

		gvUuseritem = (com.wachoo.pangker.ui.MyGridView) findViewById(R.id.gv_useritem);
		etDetail = (EditText) findViewById(R.id.et_detail);
		etDetail.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				userAdapter.setDeleteState(false);
				return false;
			}
		});
		PKIconResizer pkIconLoader = initPKIconFetcher(PangkerConstant.PANGKER_ICON_SIDELENGTH);
		userAdapter = new GroupInfoUserAdapter(this, new ArrayList<UserItem>(), pkIconLoader);
		// 添加成员监听
		userAdapter.setmAddClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				userAdapter.setDeleteState(false);
				Intent intent = new Intent(UserRecommendActivity.this, ContactsSelectActivity.class);
				intent.putExtra("intentType", 1);
				intent.putExtra(ContactsSelectActivity.INTENT_SELECT,
						ContactsSelectActivity.TYPE_RES_RECOMMEND);
				UserRecommendActivity.this.startActivityForResult(intent, mRequestCode);
			}
		});
		userAdapter.setCreater(true);
		gvUuseritem.setAdapter(userAdapter);

		gvUuseritem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				if (position < userAdapter.getCount() - 2) {
					UserItem userItem = (UserItem) arg0.getAdapter().getItem(position);
					// 移除成员
					if (userAdapter.isDeleteState()) {
						userAdapter.getUserList().remove(userItem);
						userAdapter.notifyDataSetChanged();
					}
					// 查看成员信息
					else {
						Intent intent = new Intent(UserRecommendActivity.this, UserWebSideActivity.class);
						intent.putExtra(UserItem.USERID, userItem.getUserId());
						intent.putExtra(UserItem.USERNAME, userItem.getUserName());
						intent.putExtra(UserItem.USERSIGN, userItem.getSign());
						UserRecommendActivity.this.startActivity(intent);
					}
				}

			}
			
		});

		LinearLayout lyGridView = (LinearLayout) findViewById(R.id.ly_gridview);
		lyGridView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				userAdapter.setDeleteState(false);
				return false;
			}
		});
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UserRecommendActivity.this.finish();
			}
		});
		Button btnSubmit = (Button) findViewById(R.id.btn_submit);
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 提交
				recommend();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == mRequestCode && resultCode == RESULT_OK) {
			List<UserItem> selectedUser = (List<UserItem>) data.getSerializableExtra("selectedUser");
			if (selectedUser != null && selectedUser.size() > 0) {
				userAdapter.addUserList(selectedUser);
			}
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v != gvUuseritem && hasFocus) {
			userAdapter.setDeleteState(false);
		}
	}

	private String getToRecommendersName() {
		StringBuffer sb = new StringBuffer();
		for (UserItem user : userAdapter.getUserList()) {
			sb.append(user.getUserName()).append(",");
		}
		if (sb.length() > 0) {
			return sb.subSequence(0, sb.length() - 1).toString();
		}
		return sb.toString();
	}
	
	private String getRecommendersIds() {
		StringBuffer sb = new StringBuffer();
		for (UserItem user : userAdapter.getUserList()) {
			sb.append(user.getUserId()).append(",");
		}
		if (sb.length() > 0) {
			return sb.subSequence(0, sb.length() - 1).toString();
		}
		return sb.toString();
	}

	/**
	 * 21.7	推荐资源保存接口
	 */
	public void AddBoxRes(RecommendInfo recommendInfo) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userIds", getRecommendersIds()));//被推荐uid（多个uid，格式用“，”分开） 
		paras.add(new Parameter("resId", recommendInfo.getResId()));
		paras.add(new Parameter("fromId", recommendInfo.getFromId()));//推荐人的ID
		paras.add(new Parameter("shareUid", recommendInfo.getShareUid()));//资源分享者ID
		paras.add(new Parameter("type", String.valueOf(recommendInfo.getType())));//资源类型（11：文档，12：图片，13：音乐 14：应用）
		paras.add(new Parameter("resName", recommendInfo.getResName()!=null?recommendInfo.getResName():""));// 
		paras.add(new Parameter("fromName", recommendInfo.getFromName()!=null?recommendInfo.getFromName():""));   //分享者姓名
		paras.add(new Parameter("datetime", recommendInfo.getDatetime()));   //分享时间
		paras.add(new Parameter("status", String.valueOf(recommendInfo.getStatus())));//分享状态，预留字段（已读、未读）
		paras.add(new Parameter("singer", recommendInfo.getSinger()!=null?recommendInfo.getSinger():""));  //作者
		paras.add(new Parameter("reason", recommendInfo.getReason()!=null?recommendInfo.getReason():""));// 分享理由(不超过50字)
		paras.add(new Parameter("remark", recommendInfo.getRemark()!=null?recommendInfo.getRemark():""));// 备注
		serverMana.supportRequest(Configuration.getAddBoxRes(), paras, true, "请稍候...");
	}
	

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse)){
			showToast("请选择联系人!");
			return;
		}
		BaseResult result = JSONUtil.fromJson(supportResponse.toString(),BaseResult.class);
		if(result != null && result.getErrorCode() == BaseResult.SUCCESS){
			showToast("推荐已发出，刷新个人主页中‘发出的推荐’，可了解对方是否已读！");
			iRecommendDao.saveRecommendInfo(recommendInfo);
			IUserStatusDao mIUserStatusDao = new UserStatusDaoImpl(this);
			OpenfireManager openfireManager = application.getOpenfireManager();
			String msg = etDetail.getText().toString();
			String resId = String.valueOf(resShowEntity.getResID());
            List<UserStatus> userStatusList = new ArrayList<UserStatus>();
			for (UserItem user : userAdapter.getUserList()) {
				UserStatus mUserStatus = new UserStatus();
				mUserStatus.setNickname(user.getUserName());
				mUserStatus.setUid(Long.parseLong(user.getUserId()));
				mUserStatus.setStatus(0);
				userStatusList.add(mUserStatus);
				openfireManager.sendResRecommend(mUserId, user.getUserId(), resShowEntity.getResType(),
						resId, resShowEntity.getResName(), resShowEntity.getSinger(), msg, resShowEntity.getShareUid());
			}
			//本地保存被推荐人列表
			mIUserStatusDao.insertUserStatus(mUserId, resId, recommendInfo.getDatetime(), userStatusList);
			finish();
		}
		else if(result != null && result.getErrorCode() == BaseResult.FAILED){
			showToast(result.getErrorMessage());
		}
		else showToast("推荐失败!");

	}

	
	private void recommend() {
		// TODO Auto-generated method stub
		if (userAdapter.getUserList() == null || userAdapter.getUserList().size() < 1) {
			showToast("请选择联系人!");
			return;
		}
		recommendInfo = new RecommendInfo();
		recommendInfo.setFromId(mUserId);
		recommendInfo.setDatetime(Util.getSysNowTime());
		recommendInfo.setStatus(0);
		recommendInfo.setFromName(application.getMySelf().getUserName());
		recommendInfo.setType(resShowEntity.getResType());
		recommendInfo.setResId(String.valueOf(resShowEntity.getResID()));
		recommendInfo.setResName(resShowEntity.getResName());
		recommendInfo.setUserId(getRecommendersIds());
		recommendInfo.setReason(etDetail.getText().toString());
		recommendInfo.setRemark(getToRecommendersName());
		recommendInfo.setShareUid(resShowEntity.getShareUid());
		AddBoxRes(recommendInfo);
		
	}

}
