package com.wachoo.pangker.activity.about;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wachoo.pangker.Configuration;
import com.wachoo.pangker.PangkerApplication;
import com.wachoo.pangker.activity.CommonPopActivity;
import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.server.IUICallBackInterface;
import com.wachoo.pangker.server.ServerSupportManager;
import com.wachoo.pangker.server.response.BaseResult;
import com.wachoo.pangker.util.Util;

public class FeedbackActivity extends CommonPopActivity implements IUICallBackInterface {

	private EditText mEditText;
	private PangkerApplication application;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback);
        application = (PangkerApplication) getApplication();
		Button btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(onClickListener);
		findViewById(R.id.iv_more).setVisibility(View.GONE);
		Button btnSend = (Button) findViewById(R.id.btn_send);
		btnSend.setText(R.string.btn_msgsend);
		btnSend.setOnClickListener(onClickListener);
		TextView txtTitle = (TextView) findViewById(R.id.mmtitle);
		txtTitle.setText(R.string.pangker_feedback);
		
		mEditText = (EditText) findViewById(R.id.et_detail);
	}
	
	View.OnClickListener onClickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v.getId() == R.id.btn_back){
				finish();
			}
			if(v.getId() == R.id.btn_send){
				sendFeedback();
			}
		}
	};
	

	private void sendFeedback() {
		// TODO Auto-generated method stub
		if(Util.isEmpty(mEditText.getEditableText().toString())){
			showToast("请填写反馈信息!");
			return;
		}
		List<Parameter> paras = new ArrayList<Parameter>();
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		paras.add(new Parameter("appid", application.getMyUserID()));
		paras.add(new Parameter("opUid", application.getMyUserID()));
		paras.add(new Parameter("desc", mEditText.getEditableText().toString()));
		serverMana.supportRequest(Configuration.getSysReport(), paras, true, "正在提交...");
	}

	@Override
	public void uiCallBack(Object response, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(response)){
			return;
		}
		BaseResult submitResult = JSONUtil.fromJson(response.toString(), BaseResult.class);
		if (submitResult != null && submitResult.errorCode == BaseResult.SUCCESS) {
			showToast(submitResult.errorMessage);
			finish();
		} else if (submitResult != null && submitResult.errorCode == 0) {
			showToast(submitResult.errorMessage);
		} else {
			showToast(R.string.res_comment_failed);	
		}
		
	}
}
