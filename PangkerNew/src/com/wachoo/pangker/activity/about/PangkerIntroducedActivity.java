package com.wachoo.pangker.activity.about;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.wachoo.pangker.activity.R;
import com.wachoo.pangker.activity.StartActivity;

public class PangkerIntroducedActivity extends Activity {

	private ViewPager pager;
	// background png
	private final int[] mBackground = { R.drawable.skip01, R.drawable.skip02, R.drawable.skip03, R.drawable.skip04,
			R.drawable.skip05, R.drawable.skip06, R.drawable.skip07 };
	boolean isInit = false;
	private ImageView[] indexImageViews = new ImageView[7];
	private int[] ImgIds = { R.id.imageView1, R.id.imageView2, R.id.imageView3, R.id.imageView4, R.id.imageView5,
			R.id.imageView6, R.id.imageView7 };
	private int start_flag = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		start_flag = getIntent().getIntExtra("start_flag", 0);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.introduce);

		for (int i = 0; i < ImgIds.length; i++) {
			indexImageViews[i] = (ImageView) findViewById(ImgIds[i]);
		}

		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(new IntroduceAdapter(this));
		pager.setOnPageChangeListener(onPageChangeListener);
		pager.setCurrentItem(0);
	}

	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(PangkerIntroducedActivity.this, StartActivity.class);
			startActivity(intent);
			PangkerIntroducedActivity.this.finish();
		}
	};

	ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageSelected(int position) {
			// TODO Auto-generated method stub
			for (int i = 0; i < ImgIds.length; i++) {
				indexImageViews[i].setImageResource(R.drawable.radio_unselect);
			}
			indexImageViews[position].setImageResource(R.drawable.radio_select);
		}

	};

	class IntroduceAdapter extends PagerAdapter {

		private Context context;

		public IntroduceAdapter(Context context) {
			super();
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mBackground.length;
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mBackground[position];
		}

		@Override
		public Object instantiateItem(View view, int position) {
			LayoutInflater inflate = LayoutInflater.from(context);
			View convertView = inflate.inflate(R.layout.pangker_introduced, null);
			ImageView iView = (ImageView) convertView.findViewById(R.id.imageView1);
			iView.setImageResource(mBackground[position]);
			if (position == mBackground.length - 1) {
				Button mButton = (Button) convertView.findViewById(R.id.enter_to_pangker);
				if (start_flag == 0) {
					mButton.setVisibility(View.VISIBLE);
					mButton.setOnClickListener(onClickListener);
				}
			}
			((ViewPager) view).addView(convertView, 0);
			return convertView;
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {

		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {

		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == (arg1);
		}
	}

}
