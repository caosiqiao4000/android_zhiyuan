package com.wachoo.pangker.util;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import com.wachoo.pangker.activity.R;

public class SmileyParser {

	private final Activity activity;
	private final String[] mSmileyTexts;
	private final Pattern mPattern;
	private final HashMap<String, Integer> mSmileyToRes;

	public SmileyParser(Activity context) {
		activity = context;
		mSmileyTexts = activity.getResources().getStringArray(DEFAULT_SMILEY_TEXTS);
		mSmileyToRes = buildSmileyToRes();
		mPattern = buildPattern();
	}

	public static final int[] DEFAULT_SMILEY_RES_IDS = { R.drawable.smiley_0, R.drawable.smiley_1, R.drawable.smiley_2,
			R.drawable.smiley_3, R.drawable.smiley_4, R.drawable.smiley_5, R.drawable.smiley_6, R.drawable.smiley_7,
			R.drawable.smiley_8, R.drawable.smiley_9, R.drawable.smiley_10, R.drawable.smiley_11, R.drawable.smiley_12,
			R.drawable.smiley_13, R.drawable.smiley_14, R.drawable.smiley_15, R.drawable.smiley_16,
			R.drawable.smiley_17, R.drawable.smiley_18, R.drawable.smiley_19, R.drawable.smiley_20,
			R.drawable.smiley_21, R.drawable.smiley_22, R.drawable.smiley_23, R.drawable.smiley_24,
			R.drawable.smiley_25, R.drawable.smiley_26, R.drawable.smiley_27, R.drawable.smiley_28,
			R.drawable.smiley_29, R.drawable.smiley_30, R.drawable.smiley_31, R.drawable.smiley_32,
			R.drawable.smiley_33, R.drawable.smiley_34, R.drawable.smiley_35, R.drawable.smiley_36,
			R.drawable.smiley_37, R.drawable.smiley_38, R.drawable.smiley_39, R.drawable.smiley_40,
			R.drawable.smiley_41, R.drawable.smiley_42, R.drawable.smiley_43, R.drawable.smiley_44,
			R.drawable.smiley_45, R.drawable.smiley_46, R.drawable.smiley_47, R.drawable.smiley_48,
			R.drawable.smiley_49, R.drawable.smiley_50, R.drawable.smiley_51, R.drawable.smiley_52,
			R.drawable.smiley_53, R.drawable.smiley_54, R.drawable.smiley_55, R.drawable.smiley_56,
			R.drawable.smiley_57, R.drawable.smiley_58, R.drawable.smiley_59, R.drawable.smiley_60,
			R.drawable.smiley_61, R.drawable.smiley_62, R.drawable.smiley_63, R.drawable.smiley_64,
			R.drawable.smiley_65, R.drawable.smiley_66, R.drawable.smiley_67, R.drawable.smiley_68,
			R.drawable.smiley_69, R.drawable.smiley_70, R.drawable.smiley_71, R.drawable.smiley_72,
			R.drawable.smiley_73, R.drawable.smiley_74, R.drawable.smiley_75, R.drawable.smiley_76,
			R.drawable.smiley_77, R.drawable.smiley_78, R.drawable.smiley_79, R.drawable.smiley_80,
			R.drawable.smiley_81, R.drawable.smiley_82, R.drawable.smiley_83, R.drawable.smiley_84,
			R.drawable.smiley_85, R.drawable.smiley_86, R.drawable.smiley_87, R.drawable.smiley_88,
			R.drawable.smiley_89 };

	public static final int DEFAULT_SMILEY_TEXTS = R.array.express_item_texts;

	private HashMap<String, Integer> buildSmileyToRes() {
		if (DEFAULT_SMILEY_RES_IDS.length != mSmileyTexts.length) {
			throw new IllegalStateException("Smiley resource ID/text mismatch");
		}

		HashMap<String, Integer> smileyToRes = new HashMap<String, Integer>(mSmileyTexts.length);
		for (int i = 0; i < mSmileyTexts.length; i++) {
			smileyToRes.put(mSmileyTexts[i], DEFAULT_SMILEY_RES_IDS[i]);
		}
		return smileyToRes;
	}

	// 构建正则表达式
	private Pattern buildPattern() {
		StringBuilder patternString = new StringBuilder(mSmileyTexts.length * 3);
		patternString.append('(');
		for (String s : mSmileyTexts) {
			patternString.append(Pattern.quote(s));
			patternString.append('|');
		}
		patternString.replace(patternString.length() - 1, patternString.length(), ")");

		return Pattern.compile(patternString.toString());
	}

	// 根据文本替换成图片
	public CharSequence addSmileySpans(CharSequence text) {
		SpannableString builder = new SpannableString(text);
		Matcher matcher = mPattern.matcher(text);
		while (matcher.find()) {
			int resId = mSmileyToRes.get(matcher.group());
			builder.setSpan(new ImageSpan(activity, resId), matcher.start(), matcher.end(),
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		return builder;
	}

	// 根据文本替换成图片
	public CharSequence setSmileySpans(CharSequence text) {
		SpannableString builder = new SpannableString(text);
		Matcher matcher = mPattern.matcher(text);
		while (matcher.find()) {
			int resId = mSmileyToRes.get(matcher.group());
			ImageSpan imageSpan = getImageSpan(resId);
			builder.setSpan(imageSpan, matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		return builder;
	}

	private ImageSpan getImageSpan(int resId) {
		// TODO Auto-generated method stub
		Drawable drawable = activity.getResources().getDrawable(resId);
		if (ImageUtil.getDensity(activity) == 1.0) {
			drawable.setBounds(0, 0, 12, 12);
		} else if (ImageUtil.getDensity(activity) == 1.5f) {
			drawable.setBounds(0, 0, 19, 19);
		} else if (ImageUtil.getDensity(activity) == 2.0f) {
			drawable.setBounds(0, 0, 26, 26);
		}
		ImageSpan span = new ImageSpan(drawable, ImageSpan.ALIGN_BASELINE);
		return span;
	}

}
