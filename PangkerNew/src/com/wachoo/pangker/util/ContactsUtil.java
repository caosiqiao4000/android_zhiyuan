package com.wachoo.pangker.util;

import android.content.Context;

/**
 * 关注人总数
 * 
 * @author wangxin
 * 
 */
public class ContactsUtil {

	public String ATTENT_COUNT_KEY = "ATTENT_COUNT_KEY";
	public String FANS_COUNT_KEY = "FANS_COUNT_KEY";
	private SharedPreferencesUtil preferencesUtil;
	private String mUserID;

	public ContactsUtil(Context context, String mUserID) {
		super();
		this.preferencesUtil = new SharedPreferencesUtil(context);
		this.mUserID = mUserID;
	}

	/**
	 * 增加关注人数量
	 * 
	 * @param addCount
	 * @param context
	 */
	public void setAttentCount(int count) {

		preferencesUtil.saveInt(ATTENT_COUNT_KEY + mUserID, count);

	}

	/**
	 * 增加关注人数量
	 * 
	 * @param addCount
	 * @param context
	 */
	public void addAttentCount(int addCount) {
		int newCount = getAttentCount() + addCount;
		setAttentCount(newCount);

	}

	/**
	 * 获取关注人数量
	 * 
	 * @param
	 * @return
	 */
	public int getAttentCount() {
		return preferencesUtil.getInt(ATTENT_COUNT_KEY + mUserID, 0);
	}

	public void setFanCount(int count) {
		preferencesUtil.saveInt(FANS_COUNT_KEY + mUserID, count);
	}

	public int getFanCount() {

		return preferencesUtil.getInt(FANS_COUNT_KEY + mUserID, 0);
	}

	/**
	 * void TODO 分数加 1
	 * 
	 * @param addCount
	 */
	public void addFanCount(int addCount) {
		int newCount = getFanCount() + addCount;
		setFanCount(newCount);
	}

	/**
	 * void TODO 分数减 1
	 * 
	 * @param subCount
	 */
	public void subFanCount(int subCount) {

		int newCount = getFanCount() - subCount;
		if (newCount < 0)
			newCount = 0;
		setFanCount(newCount);
	}

	/**
	 * void TODO 关注人数目减 1
	 * 
	 * @param subCount
	 */
	public void subAttentCount(int subCount) {
		int newCount = getAttentCount() - subCount;
		if (newCount < 0)
			newCount = 0;
		setAttentCount(newCount);
	}
}
