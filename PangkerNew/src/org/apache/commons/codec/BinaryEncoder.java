package org.apache.commons.codec;

public abstract interface BinaryEncoder extends Encoder {
	public abstract byte[] encode(byte[] paramArrayOfByte) throws EncoderException;
}

/*
 * Location: C:\Users\wangxin\Desktop\commons-codec-1.6.jar Qualified Name:
 * org.apache.commons.codec.BinaryEncoder JD-Core Version: 0.5.3
 */