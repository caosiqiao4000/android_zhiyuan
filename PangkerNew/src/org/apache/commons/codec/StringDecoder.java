package org.apache.commons.codec;

public abstract interface StringDecoder extends Decoder
{
  public abstract String decode(String paramString)
    throws DecoderException;
}

/* Location:           C:\Users\wangxin\Desktop\commons-codec-1.6.jar
 * Qualified Name:     org.apache.commons.codec.StringDecoder
 * JD-Core Version:    0.5.3
 */