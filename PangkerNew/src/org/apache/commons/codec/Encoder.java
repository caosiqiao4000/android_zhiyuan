package org.apache.commons.codec;

public abstract interface Encoder
{
  public abstract Object encode(Object paramObject)
    throws EncoderException;
}

/* Location:           C:\Users\wangxin\Desktop\commons-codec-1.6.jar
 * Qualified Name:     org.apache.commons.codec.Encoder
 * JD-Core Version:    0.5.3
 */