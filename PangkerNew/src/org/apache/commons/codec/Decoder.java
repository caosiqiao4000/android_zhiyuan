package org.apache.commons.codec;

public abstract interface Decoder
{
  public abstract Object decode(Object paramObject)
    throws DecoderException;
}

/* Location:           C:\Users\wangxin\Desktop\commons-codec-1.6.jar
 * Qualified Name:     org.apache.commons.codec.Decoder
 * JD-Core Version:    0.5.3
 */