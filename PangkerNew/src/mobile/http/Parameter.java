package mobile.http;

/**
 * @author feijinbo
 * @creatTime 2011-7-5下午02:54:51
 * @desc http请求参数封装.
 */
@SuppressWarnings("serial")
public class Parameter implements java.io.Serializable, Comparable<Object> {

	public String mName;

	public String mValue;

	public Parameter(String name, String value) {
		this.mName = name;
		this.mValue = value;
	}

	public boolean equals(Object arg0) {
		if (null == arg0) {
			return false;
		}
		if (this == arg0) {
			return true;
		}
		if (arg0 instanceof Parameter) {
			Parameter param = (Parameter) arg0;
			return this.mName.equals(param.mName)
					&& this.mValue.equals(param.mValue);
		}
		return false;
	}

	public int compareTo(Object o) {
		int compared;
		Parameter param = (Parameter) o;
		compared = mName.compareTo(param.mName);
		if (0 == compared) {
			compared = mValue.compareTo(param.mValue);
		}
		return compared;
	}
}
