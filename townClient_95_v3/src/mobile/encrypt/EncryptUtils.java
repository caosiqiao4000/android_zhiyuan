package mobile.encrypt;

import java.util.Arrays;
import java.util.List;

import mobile.http.Parameter;

/**
 * @author fitch
 * @createtime 2012-6-13 下午3:02:05
 * @version 1.0
 * @desc 客户生加密处理类
 */
public class EncryptUtils {

    // 参数分隔符
    private final static String Separator = "$";
    // 密钥
    private static String strKey = "6DC7BC0758C8E310015E922C0DA87F9DA76B8040A8AB67F8";
    // 密钥向量
    private static byte[] defaultIV = { 1, 2, 3, 4, 5, 6, 7, 8 };

    /**
     * paras 包括所有请求参数，Authenticator除外。
     * 
     * @param paras
     * @return
     * @throws Exception
     */
    public static String getAuthenticator(List<Parameter> paras)
            throws Exception {
        try {
            if (paras == null || paras.size() == 0) {
                return "";
            }
            Parameter[] ps = paras.toArray(new Parameter[paras.size()]);
            Arrays.sort(ps);
            StringBuffer br = new StringBuffer();
            for (int i = 0; i < ps.length; i++) {
                if (i != 0) {
                    br.append(Separator);
                }
                br.append(ps[i].mValue);
            }
            String digest = Des3.GenerateDigest(br.toString());
            br.append(Separator).append(digest);
            return Des3.Encrypt(br.toString(), strKey, defaultIV);
        } catch (Exception e) {
            return "";
        }

    }

    /**
     * 后台 提供的加密
     * 
     * @author caosq 2013-4-19 下午5:26:11
     * @param paras
     * @return
     * @throws Exception
     */
    public static String getAuthenticatorByJaveMD5(List<Parameter> paras)
            throws Exception {
        try {
            Parameter[] ps = paras.toArray(new Parameter[paras.size()]);
            // 参数排序处理
            Arrays.sort(ps);
            // 获取请求参数
            StringBuffer bf = new StringBuffer();
            for (int j = 0; j < ps.length; j++) {
                if (j != 0) {
                    bf.append(Separator);
                }
                bf.append(ps[j].mValue);
            }
            bf.append(Separator).append(strKey);
            return MD5.getMD5(bf.toString());
        } catch (Exception e) {
            return "";
        }
    }

}
