package mobile.encrypt;

import java.security.Key;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

/**
 * 
 * @author feijinbo
 * @createtime 2012-4-8 下午11:56:38
 * @version 1.0
 * @desc 3DES加密算法
 */
public class Des3 {

	private static String CodingType = "UTF-8";

	private static String DigestAlgorithm = "SHA1";

	private static String CryptAlgorithm = "DESede/CBC/PKCS5Padding";

	private static String KeyAlgorithm = "DESede";

	private static byte[] defaultIV = { 1, 2, 3, 4, 5, 6, 7, 8 };

	/**
	 * Function Description 根据16进制字符串生成IV
	 * 
	 * @param strIV
	 *            16进制字符串
	 * @return 字符数组
	 * @throws Exception
	 */
	public static byte[] IVGenerator(String strIV) throws Exception {
		return Hex.decodeHex(strIV.toCharArray());
	}

	/**
	 * Function Description SHA-1数据摘要
	 * 
	 * @param strTobeDigest
	 *            要数字摘要的字符串
	 * @return 返回的数字摘要
	 * @throws Exception
	 */
	public static String GenerateDigest(String strTobeDigest) throws Exception {
		byte[] input = strTobeDigest.getBytes(CodingType);
		byte[] output = null;
		MessageDigest DigestGenerator = MessageDigest
				.getInstance(DigestAlgorithm);
		DigestGenerator.update(input);
		output = DigestGenerator.digest();
		return new String(Base64.encodeBase64(output));
	}

	/**
	 * Function Description 3DES加密
	 * 
	 * @param strTobeEnCrypted
	 *            要加密的字符串
	 * @param strKey
	 *            密钥
	 * @param byteIV
	 *            加密向量
	 * @return 加密后的字符串
	 * @throws Exception
	 */
	public static String Encrypt(String strTobeEnCrypted, String strKey,
			byte[] byteIV) throws Exception {
		byte[] input = strTobeEnCrypted.getBytes(CodingType);
		Key k = KeyGenerator(strKey);
		IvParameterSpec IVSpec = (byteIV.length == 0) ? IvGenerator(defaultIV)
				: IvGenerator(byteIV);
		Cipher c = Cipher.getInstance(CryptAlgorithm);
		c.init(Cipher.ENCRYPT_MODE, k, IVSpec);
		byte[] output = c.doFinal(input);
		return new String(Base64.encodeBase64(output), CodingType);
	}

	/**
	 * Function Description 3DES解密
	 * 
	 * @param strTobeDeCrypted
	 *            要解密的字符串
	 * @param strKey
	 *            密钥
	 * @param byteIV
	 *            解密向量
	 * @return 解密后的字符串
	 * @throws Exception
	 */
	public static String Decrypt(String strTobeDeCrypted, String strKey,
			byte[] byteIV) throws Exception {
		byte[] input = Base64.decodeBase64(strTobeDeCrypted);
				
		Key k = KeyGenerator(strKey);
		IvParameterSpec IVSpec = (byteIV.length == 0) ? IvGenerator(defaultIV)
				: IvGenerator(byteIV);
		Cipher c = Cipher.getInstance(CryptAlgorithm);
		c.init(Cipher.DECRYPT_MODE, k, IVSpec);
		byte[] output = c.doFinal(input);
		return new String(output, CodingType);
	}

	
	/**
	 * 生成加解密向量
	 * 
	 * @param b
	 * @return
	 * @throws Exception
	 */
	private static IvParameterSpec IvGenerator(byte[] b) throws Exception {
		IvParameterSpec IV = new IvParameterSpec(b);
		return IV;
	}

	/**
	 * 根据16进制字符串生成密钥
	 * 
	 * @param keyStr
	 * @return
	 * @throws Exception
	 */
	private static Key KeyGenerator(String keyStr) throws Exception {
		byte[] input = Hex.decodeHex(keyStr.toCharArray());
		DESedeKeySpec KeySpec = new DESedeKeySpec(input);
		SecretKeyFactory KeyFactory = SecretKeyFactory
				.getInstance(KeyAlgorithm);
		return KeyFactory.generateSecret(KeySpec);
	}
}
