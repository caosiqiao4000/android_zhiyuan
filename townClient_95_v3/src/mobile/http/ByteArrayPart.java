package mobile.http;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.httpclient.methods.multipart.PartBase;

/**
 * @author: feijinbo
 * @createTime: 2012-3-1 下午5:16:38
 * @version: 1.0
 * @desc :HttpClient自定义字节流传输方法
 */
public class ByteArrayPart extends PartBase {

	private byte[] mData;
	private String mName;

	public ByteArrayPart(byte[] data, String name, String type)
			throws IOException {
		super(name, type, "UTF-8", "binary");
		mName = name;
		mData = data;
	}

	protected void sendData(OutputStream out) throws IOException {
		out.write(mData);
	}

	protected long lengthOfData() throws IOException {
		return mData.length;
	}

	protected void sendDispositionHeader(OutputStream out) throws IOException {
		super.sendDispositionHeader(out);
		StringBuilder buf = new StringBuilder();
		buf.append("; filename=\"").append(mName).append("\"");
		out.write(buf.toString().getBytes());
	}

}
