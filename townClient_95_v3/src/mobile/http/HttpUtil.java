package mobile.http;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.MimetypesFileTypeMap;

import android.util.Log;

import com.gsta.v2.util.Util;

/**
 * 
 * @author feijinbo
 * @createTime 2011-10-24 下午05:33:26
 * @desc 参数处理工具类
 */
public class HttpUtil {

	private static String TAG = Util.getClassName();// log tag
	private static final String CodingType = "UTF-8";

	/**
	 * Return the MIME type based on the specified file name.
	 * 
	 * @param fileName
	 *            path of input file.
	 * @return MIME type.
	 */
	public static String getContentType(String fileName) {
		return new MimetypesFileTypeMap().getContentType(fileName);
	}

	/**
	 * Return the MIME type based on the specified file name.
	 * 
	 * @param file
	 *            File
	 * @return MIME type.
	 */
	public static String getContentType(File file) {
		return new MimetypesFileTypeMap().getContentType(file);
	}

	/**
	 * 对parameters对象进行URL转换，并返回queryString形式。
	 * 
	 * @param parameters
	 * @return
	 */
	public static String encodeParameters(List parameters) {
		if (null == parameters) {
			return "";
		}
		StringBuffer buf = new StringBuffer();
		for (int j = 0; j < parameters.size(); j++) {
			if (j != 0) {
				buf.append("&");
			}
			Parameter p = (Parameter) parameters.get(j);
			try {
				buf.append(URLEncoder.encode(p.mName, CodingType)).append("=")
						.append(URLEncoder.encode(p.mValue, CodingType));
			} catch (java.io.UnsupportedEncodingException ex) {
				Log.i(TAG, "Exception:" + ex.getMessage());
			}
		}
		return buf.toString();

	}

	/**
	 * Return the list of query parameters based on the specified query string.
	 * 
	 * @param queryString
	 * @return the list of query parameters.
	 */
	public static List<Parameter> getQueryParameters(String queryString) {
		if (queryString.startsWith("?")) {
			queryString = queryString.substring(1);
		}
		List<Parameter> result = new ArrayList<Parameter>();
		if (queryString != null && !queryString.equals("")) {
			String[] p = queryString.split("&");
			for (String s : p) {
				if (s != null && !s.equals("")) {
					if (s.indexOf('=') > -1) {
						String[] temp = s.split("=");
						try {
							result.add(new Parameter(URLDecoder.decode(temp[0],
									CodingType), URLDecoder.decode(temp[1],
									CodingType)));
						} catch (UnsupportedEncodingException ex) {
							// TODO Auto-generated catch block
							Log.i(TAG,"Exception:" + ex.getMessage());
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * Return the map of query parameters based on the specified query string.
	 * 
	 * @param queryString
	 * @return
	 */
	public static Map getQueryParametersWithMap(String queryString) {
		if (queryString.startsWith("?")) {
			queryString = queryString.substring(1);
		}
		Map<String, String> result = new HashMap<String, String>();
		if (queryString != null && !queryString.equals("")) {
			String[] p = queryString.split("&");
			for (String s : p) {
				if (s != null && !s.equals("")) {
					if (s.indexOf('=') > -1) {
						String[] temp = s.split("=");
						try {
							result.put(URLDecoder.decode(temp[0], CodingType),
									URLDecoder.decode(temp[1], CodingType));
						} catch (UnsupportedEncodingException ex) {
							// TODO Auto-generated catch block
							Log.i(TAG,"Exception:" + ex.getMessage());
						}
					}
				}
			}
		}
		return result;
	}
}
