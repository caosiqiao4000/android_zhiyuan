package mobile.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.FilePartSource;
import org.apache.commons.httpclient.methods.multipart.PartBase;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author: feijinbo
 * @createTime: 2012-2-7 下午1:47:38
 * @version: 1.0
 * @desc :文件上传功能类，解决FileItem中文乱码问题.
 */
public class MyFilePart extends PartBase {

	public static final String DEFAULT_CONTENT_TYPE = "application/octet-stream";
	public static final String DEFAULT_CHARSET = "ISO-8859-1";
	public static final String DEFAULT_TRANSFER_ENCODING = "binary";
	private static final Log LOG = LogFactory.getLog(FilePart.class);
	protected static final String FILE_NAME = "; filename=";
	private static final byte[] FILE_NAME_BYTES = EncodingUtil
			.getAsciiBytes("; filename=");
	private PartSource source;

	public MyFilePart(String name, PartSource partSource, String contentType,
			String charset) {
		super(name, contentType == null ? "application/octet-stream"
				: contentType, charset == null ? "ISO-8859-1" : charset,
				"binary");

		if (partSource == null) {
			throw new IllegalArgumentException("Source may not be null");
		}
		this.source = partSource;
	}

	public MyFilePart(String name, PartSource partSource) {
		this(name, partSource, null, null);
	}

	public MyFilePart(String name, File file) throws FileNotFoundException {
		this(name, new FilePartSource(file), null, null);
	}

	public MyFilePart(String name, File file, String contentType, String charset)
			throws FileNotFoundException {
		this(name, new FilePartSource(file), contentType, charset);
	}

	public MyFilePart(String name, String fileName, File file)
			throws FileNotFoundException {
		this(name, new FilePartSource(fileName, file), null, null);
	}

	public MyFilePart(String name, String fileName, File file,
			String contentType, String charset) throws FileNotFoundException {
		this(name, new FilePartSource(fileName, file), contentType, charset);
	}

	protected void sendDispositionHeader(OutputStream out) throws IOException {
		LOG.trace("enter sendDispositionHeader(OutputStream out)");
		super.sendDispositionHeader(out);
		String filename = this.source.getFileName();
		if (filename != null) {
			out.write(FILE_NAME_BYTES);
			out.write(QUOTE_BYTES);
			out.write(EncodingUtil.getBytes(filename, getCharSet()));
			out.write(QUOTE_BYTES);
		}
	}

	protected void sendData(OutputStream out) throws IOException {
		LOG.trace("enter sendData(OutputStream out)");
		if (lengthOfData() == 0L) {
			LOG.debug("No data to send.");
			return;
		}
		byte[] tmp = new byte[4096];
		InputStream instream = this.source.createInputStream();
		try {
			int len;
			while ((len = instream.read(tmp)) >= 0)
				out.write(tmp, 0, len);
		} finally {
			instream.close();
		}
	}

	protected PartSource getSource() {
		LOG.trace("enter getSource()");
		return this.source;
	}

	protected long lengthOfData() throws IOException {
		LOG.trace("enter lengthOfData()");
		return this.source.getLength();
	}

}
