package mobile.http;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mobile.encrypt.EncryptUtils;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;

import android.util.Log;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.util.Util;

/**
 * 
 * @author: feijinbo
 * @createTime: 2012-1-12 下午2:24:16
 * @version: 1.0
 * @desc :HttpClient 公共方法
 */
public class SyncHttpClient {

    private static final Logger logger = LoggerFactory.getLogger(SyncHttpClient.class);
    private final String TAG = Util.getClassName();
    // 超时等待
    private static final int CONNECTION_TIMEOUT = 10000;
    // 字符集
    public static final String CONTENT_CHARSET = "UTF-8";

    /**
     * Using POST method.
     * 
     * @param url
     *            The remote URL.
     * @param queryString
     *            The query string containing parameters
     * @return Response string.
     * @throws Exception
     */
    public String httpPost(String url, String queryString) throws Exception {
        String responseData = null;
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        // 设置超时
        httpPost.getParams().setParameter("http.socket.timeout",
                Integer.valueOf(CONNECTION_TIMEOUT));
        // 设置字符集
        httpPost.addRequestHeader("Content-Type", "text/html; charset=utf-8");
        if (queryString != null && !queryString.equals("")) {
            httpPost.setRequestEntity(new ByteArrayRequestEntity(queryString
                    .getBytes()));
        }
        try {
            int statusCode = httpClient.executeMethod(httpPost);
            if (statusCode != HttpStatus.SC_OK) {
                Log.e(TAG,
                        "HttpPost Method failed: " + httpPost.getStatusLine());
            }
            // Read the response body.
            responseData = httpPost.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            httpPost.releaseConnection();
            httpClient = null;
        }

        return responseData;
    }

    /**
     * post by pairs
     * 
     * @param url
     * @param pairs
     * @return
     * @throws Exception
     */
    public String httpPost(String url, List parameters) throws Exception {
        String responseData = null;
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        // 设置超时
        httpPost.getParams().setParameter("http.socket.timeout",
                Integer.valueOf(CONNECTION_TIMEOUT));
        // 设置字符集
        httpPost.getParams().setParameter(
                HttpMethodParams.HTTP_CONTENT_CHARSET, CONTENT_CHARSET);
        String systime = Util.getSysNowTime();
        parameters.add(new Parameter("systime", systime));
        String authenticator = EncryptUtils.getAuthenticator(parameters);
        parameters.add(new Parameter("authenticator", authenticator));
        String urllog = url + "?";
        // 添加参数
        for (Iterator iter = parameters.iterator(); iter.hasNext();) {
            Parameter para = (Parameter) iter.next();

            httpPost.addParameter(para.mName == null ? "" : para.mName,
                    para.mValue == null ? "" : para.mValue);
            urllog += para.mName + "=" + para.mValue + "&";
        }
        logger.info(TAG + " url= " + urllog);
        try {
            int statusCode = httpClient.executeMethod(httpPost);
            if (statusCode != HttpStatus.SC_OK) {
                Log.e(TAG,
                        "HttpPost Method failed: " + httpPost.getStatusLine());
            }
            // Read the response body.
            responseData = httpPost.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            httpPost.releaseConnection();
            httpClient = null;
        }

        return responseData;
    }

    /**
     * Using POST method with multiParts.
     * 
     * @param url
     *            the request url.
     * @param params
     * @param fileName
     *            must provide
     * @param file
     * @return
     * @throws Exception
     */
    public String httpPostWithFile(String url, List<Parameter> params,
            String fileParamName, MyFilePart filePart) throws Exception {
        String responseData = null;
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        try {
            String systime = Util.getSysNowTime();
            params.add(new Parameter("systime", systime));
            String authenticator = EncryptUtils.getAuthenticator(params);
            params.add(new Parameter("authenticator", authenticator));
            int length = params.size() + (filePart != null ? 1 : 0);
            Part[] parts = new Part[length];
            int i = 0;
            for (Parameter entry : params) {
                parts[i++] = new StringPart(entry.mName, entry.mValue,
                        CONTENT_CHARSET);
            }
            if (filePart != null) {
                filePart.setTransferEncoding("binary");
                parts[i++] = filePart;
            }
            // 设置重试次数
            httpPost.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                    new DefaultHttpMethodRetryHandler(3, false));
            httpPost.setRequestEntity(new MultipartRequestEntity(parts,
                    httpPost.getParams()));
            int statusCode = httpClient.executeMethod(httpPost);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("HttpPost Method failed: "
                        + httpPost.getStatusLine());
            }
            responseData = httpPost.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            httpPost.releaseConnection();
            httpClient = null;
        }
        return responseData;
    }

    public String httpPostWithFile(String url, List<Parameter> params,
            MyFilePart filePart1, MyFilePart filePart2) throws Exception {
        String responseData = null;
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        try {
            String systime = Util.getSysNowTime();
            params.add(new Parameter("appId", "10000"));
            params.add(new Parameter("systime", systime));
            String authenticator = EncryptUtils.getAuthenticator(params);
            params.add(new Parameter("authenticator", authenticator));
            int length = params.size() + (filePart1 != null ? 1 : 0);
            length += (filePart2 != null ? 1 : 0);
            Part[] parts = new Part[length];
            for (int i = 0; i < params.size(); i++) {
                parts[i] = new StringPart(params.get(i).mName,
                        params.get(i).mValue, CONTENT_CHARSET);
            }
            if (filePart1 != null) {
                filePart1.setTransferEncoding("binary");
                parts[params.size()] = filePart1;
            }
            if (filePart2 != null) {
                filePart2.setTransferEncoding("binary");
                parts[params.size() + 1] = filePart2;
            }
            // 设置重试次数
            httpPost.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                    new DefaultHttpMethodRetryHandler(3, false));
            httpPost.setRequestEntity(new MultipartRequestEntity(parts,
                    httpPost.getParams()));
            int statusCode = httpClient.executeMethod(httpPost);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("HttpPost Method failed: "
                        + httpPost.getStatusLine());
            }
            responseData = httpPost.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            httpPost.releaseConnection();
            httpClient = null;
        }
        return responseData;
    }

    /**
     * 上传文件,不限个数
     * 
     * @author wubo
     * @time 2013-3-22
     * @param url
     * @param params
     * @param filePart
     * @return
     * @throws Exception
     * 
     */
    public String httpPostWithFile(String url, List<Parameter> params,
            ArrayList<MyFilePart> uploaders) throws Exception {
        String responseData = null;
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        try {
            String systime = Util.getSysNowTime();
            params.add(new Parameter("appId", "10000"));
            params.add(new Parameter("systime", systime));

            String authenticator = EncryptUtils.getAuthenticator(params);
            params.add(new Parameter("authenticator", authenticator));
            int length = params.size()
                    + (uploaders == null ? 0 : uploaders.size());
            Part[] parts = new Part[length];
            for (int i = 0; i < params.size(); i++) {
                parts[i] = new StringPart(params.get(i).mName,
                        params.get(i).mValue, CONTENT_CHARSET);
            }
            /**
             * 添加文件
             * 
             * @add by wubo
             */
            if (uploaders != null && uploaders.size() > 0) {
                for (int i = 0; i < uploaders.size(); i++) {
                    MyFilePart fp = uploaders.get(i);
                    fp.setTransferEncoding("binary");
                    parts[params.size() + i] = fp;
                }
            }
            // 设置重试次数
            httpPost.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                    new DefaultHttpMethodRetryHandler(3, false));
            httpPost.setRequestEntity(new MultipartRequestEntity(parts,
                    httpPost.getParams()));
            int statusCode = httpClient.executeMethod(httpPost);
            if (statusCode != HttpStatus.SC_OK) {
                Log.e(TAG, "HttpPost Failed: " + httpPost.getStatusLine());
            }
            responseData = httpPost.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            httpPost.releaseConnection();
            httpClient = null;
        }
        return responseData;
    }

    /**
     * Using POST method with byte file
     * 
     * @param url
     * @param queryString
     * @param byData
     * @return
     * @throws Exception
     */

    public String httpPostByteFile(String url, String queryString, byte[] byData)
            throws Exception {
        String responseData = null;
        url += '?' + queryString;
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        try {
            httpPost.setRequestEntity(new ByteArrayRequestEntity(byData));
            int statusCode = httpClient.executeMethod(httpPost);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("HttpPost Method failed: "
                        + httpPost.getStatusLine());
            }
            // Read the response body.
            responseData = httpPost.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            httpPost.releaseConnection();
            httpClient = null;
        }
        return responseData;
    }

    /**
     * 上传资源文件，资源带封面
     * 
     * @param url
     * @param params
     * @param fileParamName
     * @param file
     * @param cover
     * @return
     * @throws Exception
     */
    public String httpPostWithFile(String url, List<Parameter> params,
            MyFilePart filePart, byte[] cover) throws Exception {
        String responseData = null;
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        try {
            String systime = Util.getSysNowTime();
            params.add(new Parameter("systime", systime));
            params.add(new Parameter("appId", "10000"));
            String authenticator = EncryptUtils.getAuthenticator(params);
            params.add(new Parameter("authenticator", authenticator));
            int length = params.size() + (filePart != null ? 1 : 0)
                    + (cover != null && cover.length > 0 ? 1 : 0);
            Part[] parts = new Part[length];
            int i = 0;
            for (Parameter entry : params) {
                parts[i++] = new StringPart(entry.mName, entry.mValue,
                        CONTENT_CHARSET);
            }
            if (filePart != null) {
                filePart.setTransferEncoding("binary");
                parts[i++] = filePart;
            }
            // 添加封面
            if (cover != null && cover.length > 0) {
                parts[i++] = new ByteArrayPart(cover, "pk_cover", null);
            }
            httpPost.setRequestEntity(new MultipartRequestEntity(parts,
                    httpPost.getParams()));
            int statusCode = httpClient.executeMethod(httpPost);
            if (statusCode != HttpStatus.SC_OK) {
                Log.e(TAG, "HttpPost: " + httpPost.getStatusLine());
            }
            responseData = httpPost.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            httpPost.releaseConnection();
            httpClient = null;
        }
        return responseData;
    }

    public String httpGet(String url, String queryString) throws Exception {
        String responseData = null;
        if (queryString != null && !queryString.equals("")) {
            url += "?" + queryString;
        }
        HttpClient httpClient = new HttpClient();
        GetMethod httpGet = new GetMethod(url);
        httpGet.getParams().setParameter("http.socket.timeout",
                Integer.valueOf(CONNECTION_TIMEOUT));
        try {
            int statusCode = httpClient.executeMethod(httpGet);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("HttpGet Method failed: "
                        + httpGet.getStatusLine());
            }
            // Read the response body.
            responseData = httpGet.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            httpGet.releaseConnection();
            httpClient = null;
        }
        return responseData;
    }

    public String httpPostOpenUrl(String url, List parameters) throws Exception {
        String responseData = null;
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        // 设置超时
        httpPost.getParams().setParameter("http.socket.timeout",
                Integer.valueOf(CONNECTION_TIMEOUT));
        // 设置字符集
        httpPost.getParams().setParameter(
                HttpMethodParams.HTTP_CONTENT_CHARSET, CONTENT_CHARSET);
        String systime = Util.getSysNowTime();
        parameters.add(new Parameter("systime", systime));
        parameters.add(new Parameter("appId", "10000"));
        String authenticator = EncryptUtils.getAuthenticator(parameters);
        parameters.add(new Parameter("authenticator", authenticator));
        // 添加参数
        for (Iterator iter = parameters.iterator(); iter.hasNext();) {
            Parameter para = (Parameter) iter.next();

            httpPost.addParameter(para.mName == null ? "" : para.mName,
                    para.mValue == null ? "" : para.mValue);
        }
        try {
            int statusCode = httpClient.executeMethod(httpPost);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("HttpPost Method failed: "
                        + httpPost.getStatusLine());
            }
            // Read the response body.
            responseData = httpPost.getResponseBodyAsString();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            httpPost.releaseConnection();
            httpClient = null;
        }

        return responseData;
    }
}
