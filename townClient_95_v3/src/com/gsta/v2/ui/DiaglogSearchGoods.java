package com.gsta.v2.ui;

import java.util.List;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.CitySpinnerAdapter;
import com.gsta.v2.activity.adapter.ProductOrderAdapter;
import com.gsta.v2.db.ICityDao;
import com.gsta.v2.db.impl.CityDaoImpl;
import com.gsta.v2.entity.City;
import com.gsta.v2.ui.CustomDialog.Builder;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.EditTextKeyListener;
import com.gsta.v2.util.Util;

/**
 * 搜索商品的对话框 目前为固定view样式的对话框
 * 
 * @author caosq 2013-5-21
 */
public class DiaglogSearchGoods {
    private CustomDialog.Builder builder;
    private EditText view_edit; // 商品关键字
    private EditText et_min; // 最小价格
    private EditText et_max; // 最大价格

    private String provinceId = "33";// 省份 默认为空
    private String cityId = "20574";// 地市 默认为空

    private List<City> first;// 一级
    private List<City> second;// 二级

    private Spinner s_first; // 省
    private CitySpinnerAdapter sa1;
    private int cho1 = 0; // 省份的下标
    private Spinner s_second; // 市
    private CitySpinnerAdapter sa2;
    private int cho2 = 0; // 市级的下标
    private Spinner s_order; // 排序条件
    private ProductOrderAdapter orderAdapter;
    private int cho4 = 0;

    private ICityDao cityDao;
    private Context context;
    private AgentApplication application;
    private DialogSearchConfig config;

    public DiaglogSearchGoods(Context context) {
        config = new DialogSearchConfig();
        cityDao = new CityDaoImpl(context);

        application = (AgentApplication) context.getApplicationContext();

        this.context = context;
        changeCityCode();
    }

    /**
     * @author caosq 2013-5-22 上午9:32:43
     */
    private void changeCityCode() {
        if (application.getAgentInfo() != null) {
            provinceId = application.getAgentInfo().getProvinceId();
            cityId = application.getAgentInfo().getCityId();
        }
        if (provinceId == null) {
            provinceId = "33";// 浙江
            cityId = "20574";// 宁波
        }
        if (cityId == null) {
            cityId = "20574";
        }
        // 中国大陆
        first = cityDao.getChildCitybyId("2");
        // second = cityDao.getChildCitybyId(provinceId);

        for (int i = 0; i < first.size(); i++) {
            if (first.get(i).getId().equals(provinceId)) {
                cho1 = i;
                break;
            }
        }
        second = cityDao.getChildCitybyId(provinceId);
        for (int i = 0; i < second.size(); i++) {
            if (second.get(i).getId().equals(cityId)) {
                cho2 = i;
                break;
            }
        }
        sa1 = new CitySpinnerAdapter(context, first);
        sa2 = new CitySpinnerAdapter(context, second);
        orderAdapter = new ProductOrderAdapter(context);
    }

    public void showSeachDialog(String titleNmae, DialogSearchConfig dialogConfig, OnClickListener dialogClickListener) {
        initDialog();
        builder.setTitle(titleNmae);

        s_first.setSelection(cho1);
        s_second.setSelection(cho2);
        final String keyS = dialogConfig.keywords;
        if (null != keyS && keyS.trim().length() > 0) {
            config.setKeywords(keyS);
            view_edit.setText(keyS);
            Util.setEditCursorToTextEnd(view_edit);
        }
        if (null != dialogConfig.cho4) {
            config.setCho4(dialogConfig.cho4);
            s_order.setSelection(dialogConfig.cho4);
        }
        if (null != dialogConfig.minPrice && Integer.valueOf(dialogConfig.minPrice) > 0) {
            config.setMinPrice(dialogConfig.minPrice);
            et_min.setText(dialogConfig.minPrice);
            Util.setEditCursorToTextEnd(et_min);
        }
        if (null != dialogConfig.maxPrice && Integer.valueOf(dialogConfig.maxPrice) > 0) {
            config.setMaxPrice(dialogConfig.maxPrice);
            et_max.setText(dialogConfig.maxPrice);
            Util.setEditCursorToTextEnd(et_max);
        }
        view_edit.addTextChangedListener(editContactTextChangeListener);
        et_max.addTextChangedListener(editContactTextChangeListener);
        et_min.addTextChangedListener(editContactTextChangeListener);
        builder.setNegativeButton("取消", null);
        //
        builder.setPositiveButton("搜索", dialogClickListener);

        builder.create().show();
    }

    private void initDialog() {
        builder = new CustomDialog.Builder(context);
        View inflater = LayoutInflater.from(context)
                .inflate(R.layout.search_product, null);
        view_edit = (EditText) inflater.findViewById(R.id.view_edit);
        view_edit.setHint("请输入搜索条件的关键字");
        et_min = (EditText) inflater.findViewById(R.id.et_min);
        et_min.setKeyListener(new EditTextKeyListener(
                EditTextKeyListener.NUMBER_TYPE));
        et_max = (EditText) inflater.findViewById(R.id.et_max);
        et_max.setKeyListener(new EditTextKeyListener(
                EditTextKeyListener.NUMBER_TYPE));
        builder.setContentView(inflater);

        // 选择城市和条件
        s_first = (Spinner) inflater.findViewById(R.id.s1);
        s_first.setAdapter(sa1);
        s_first.setOnItemSelectedListener(itemSelectedListener1);
        s_second = (Spinner) inflater.findViewById(R.id.s2);
        s_second.setAdapter(sa2);
        s_second.setOnItemSelectedListener(itemSelectedListener2);
        // 选择排序条件
        s_order = (Spinner) inflater.findViewById(R.id.s4);
        s_order.setAdapter(orderAdapter);
        s_order.setOnItemSelectedListener(itemSelectedListener4);
    }

    /**
     * 搜索框字符串改变监听器
     * 
     * 通讯录
     */
    private final TextWatcher editContactTextChangeListener = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (view_edit.isFocused()) {
                config.setKeywords(s.toString());
            } else if (et_max.isFocused()) {
                config.setMaxPrice(s.toString());
            } else if (et_min.isFocused()) {
                config.setMinPrice(s.toString());
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
    };

    // 省市选择
    OnItemSelectedListener itemSelectedListener1 = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View v, int position,
                long id) {
            if (cho1 != position) {
                cho1 = position;
                second = cityDao.getChildCitybyId(first.get(position).getId());
                sa2.setCitys(second);
                s_second.setSelection(0);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    OnItemSelectedListener itemSelectedListener2 = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View v, int position,
                long id) {
            if (cho2 != position) {
                cho2 = position;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    OnItemSelectedListener itemSelectedListener4 = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View v, int position,
                long id) {
            cho4 = position;
            config.setCho4(cho4);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    public Builder getBuilder() {
        return builder;
    }

    public String getProvinceId() {
        return provinceId = first.get(cho1).getId();
    }

    public String getCityId() {
        return cityId = second.get(cho2).getId();
    }

    public DialogSearchConfig getConfig() {
        return config;
    }

    public void setConfig(DialogSearchConfig config) {
        this.config = config;
    }

    /**
     * dialog 中的参数配置类
     * 
     * @author caosq 2013-5-22
     */
    public class DialogSearchConfig {
        private String minPrice = "0";// 最小价格 默认为0
        private String maxPrice = "0";// 最大价格 默认为0
        private String orderBy = "00";// 排序字段，默认为00
        private String keywords = "";// 商品关键词 默认为空
        private Integer cho4 = 0; // 查询条件选择

        public Integer getCho4() {
            return cho4;
        }

        public void setCho4(Integer cho4) {
            if (null != orderAdapter) {
                orderBy = orderAdapter.getItem(cho4).toString();
            }
            this.cho4 = cho4;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }

        public String getMinPrice() {
            return minPrice;
        }

        public void setMinPrice(String minPrice) {
            this.minPrice = minPrice;
        }

        public String getMaxPrice() {
            return maxPrice;
        }

        public void setMaxPrice(String maxPrice) {
            this.maxPrice = maxPrice;
        }

        public String getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(String orderBy) {
            this.orderBy = orderBy;
        }
    }
}
