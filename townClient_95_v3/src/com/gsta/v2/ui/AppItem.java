package com.gsta.v2.ui;


public class AppItem implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	public int id;
	public int img;
	public String label;
	public String content;
	
	public AppItem() {
		// TODO Auto-generated constructor stub
	}
	
	public AppItem(int id, int img, String label) {
		super();
		this.id = id;
		this.img = img;
		this.label = label;
	}
}
