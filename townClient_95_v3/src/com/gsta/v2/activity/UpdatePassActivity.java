package com.gsta.v2.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gsta.v2.response.BaseResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.EditTextKeyListener;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 更改密码
 * @author wubo
 * @createtime 2012-9-17
 */
public class UpdatePassActivity extends BaseActivity implements
        IUICallBackInterface {

    private EditText oldname;
    private EditText newname;
    private EditText renewname;
    private TextView titleTvName;
    private Button btn_submit, btn_cancel, btn_title_right, btn_ib_myshop;
    private final int updatepass_code = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.updatepass);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        oldname = (EditText) findViewById(R.id.oldname);
        newname = (EditText) findViewById(R.id.newname);
        renewname = (EditText) findViewById(R.id.renewname);
        btn_submit = (Button) findViewById(R.id.submit);
        btn_cancel = (Button) findViewById(R.id.cancel);

        btn_ib_myshop = (Button) findViewById(R.id.mytitile_btn_left);
        titleTvName = (TextView) findViewById(R.id.mytitle_textView);
        btn_title_right = (Button) findViewById(R.id.mytitile_btn_right);
        btn_ib_myshop.setVisibility(0);
        btn_title_right.setVisibility(View.GONE);
    }

    private void initData() {
        titleTvName.setText("密码修改");
    }

    private void initListener() {
        oldname.setKeyListener(new EditTextKeyListener());
        newname.setKeyListener(new EditTextKeyListener());
        renewname.setKeyListener(new EditTextKeyListener());
        btn_title_right.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // showDialog(10);
            }
        });
        btn_ib_myshop.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                UpdatePassActivity.this.finish();
            }
        });
        btn_submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String p1 = oldname.getText().toString();
                String p2 = newname.getText().toString();
                String p3 = renewname.getText().toString();
                if (p1.equals(null) || p1.equals("")) {
                    showToast("原始密码不能为空!");
                    return;
                }
                if (p2.equals(null) || p2.equals("")) {
                    showToast("新密码不能为空!");
                    return;
                }
                if (p3.equals(null) || p3.equals("")) {
                    showToast("重复新密码不能为空!");
                    return;
                }
                if (!p2.equals(p3)) {
                    showToast("密码不一致!");
                    return;
                }
                updatePass(p1, p2);
            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdatePassActivity.this.finish();
            }
        });
    }

    /**
     * 修改密码
     * 
     * @author wubo
     * @createtime 2012-8-31
     */
    private void updatePass(String old, String news) {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));// 登录账号
        paras.add(new Parameter("oldPassword", old));// 登录账号
        paras.add(new Parameter("password", news));// 登录密码
        serverMana.supportRequest(AgentConfig.PasswordReset(), paras, true,
                "提交中,请稍等 ...", updatepass_code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        // TODO Auto-generated method stub
        if (!HttpResponseStatus(supportResponse))
            return;

        switch (caseKey) {
        case updatepass_code:
            BaseResult baseResult = JSONUtil.fromJson(supportResponse
                    .toString(), BaseResult.class);
            if (baseResult != null && baseResult.getErrorCode() != 999) {
                showToast(baseResult.getErrorMessage());
                if (baseResult.getErrorCode() == BaseResult.SUCCESS) {
                    finish();
                }
            } else {
                showToast(R.string.to_server_fail);
            }
            break;
        }
    }

}
