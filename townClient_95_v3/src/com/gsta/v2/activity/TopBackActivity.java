package com.gsta.v2.activity;

import com.gsta.v2.util.FinalUtil;

import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;

/**
 * 此类专供 在主界面使用VIEW插入ACTIVITY的类使用继承 解决返回问题
 * 
 * @author caosq
 * 
 */
public abstract class TopBackActivity extends BaseActivity {

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.i("TopBackActivity", "" + keyCode);
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goToNotificationIntent();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MENU) {
            // application.leaveTown(TopBackActivity.this);
            sendBroadcast(new Intent(FinalUtil.RECEIVER_USER_PROGRAM_OUT));
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
