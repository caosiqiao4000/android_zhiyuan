package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.MessageInfo;
import com.gsta.v2.util.SmileyParser;

public class BuyerMsgAdapter extends BaseAdapter {
	Context context;
	public List<MessageInfo> msgs = null;
	private SmileyParser parser;

	public BuyerMsgAdapter(Context context, List<MessageInfo> msgs) {
		this.context = context;
		this.msgs = msgs;
		parser = SmileyParser.getInstance(context);
	}

	class MessageSingleViewHolder {
		TextView mycontent;
		TextView time;
		ImageView warning;
	}

	public int getCount() {
		return msgs.size();
	}

	public Object getItem(int position) {
		return msgs.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		MessageSingleViewHolder holder = new MessageSingleViewHolder();
		// 说话方向
		MessageInfo msg = msgs.get(position);
		if (msg.getDir() == null || msg.getDir().equals("")
				|| msg.getDir().equals("1")) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.message_talk_view_to, null);
			holder.mycontent = (TextView) convertView
					.findViewById(R.id.tv_msg_left);
			holder.time = (TextView) convertView.findViewById(R.id.timefrom);
		} else if (msg.getDir().equals("0")) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.message_talk_view_from, null);
			holder.mycontent = (TextView) convertView
					.findViewById(R.id.contentto);
			holder.time = (TextView) convertView.findViewById(R.id.timeto);
			holder.warning = (ImageView) convertView.findViewById(R.id.warning);
			holder.warning.setVisibility(View.GONE);
		}
		holder.time.setText(msg.getSentTime());
		holder.mycontent.setText(parser.addSmileySpans(msg.getContent()));
		return convertView;
	}

	public List<MessageInfo> getMsgs() {
		return msgs;
	}

	public void setData(List<MessageInfo> msgs) {
		this.msgs = msgs;
		this.notifyDataSetChanged();
	}
}
