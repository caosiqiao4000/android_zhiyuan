package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.Bitmap;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.BuyerInfo;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 
 * @author wubo
 * @createtime 2012-9-5
 */
public class BuyerAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<BuyerInfo> mUserList;
    @SuppressWarnings("unused")
	private Context context;
    private OnClickListener onClickListener;
    AgentApplication application;
    
    // 商品图片使用
    @SuppressWarnings("unused")
	private DisplayImageOptions optionsByNonePic;
    // 头像类使用
    private DisplayImageOptions optionsByNoPhoto;

    /**
     * @return the mUserList
     */
    public List<BuyerInfo> getmUserList() {
        return mUserList;
    }

    public void addContact(List<BuyerInfo> BuyerInfo) {
        if (this.mUserList != null) {
            this.mUserList.addAll(BuyerInfo);
        } else
            this.mUserList = BuyerInfo;

        this.notifyDataSetChanged();
    }

    /**
     * @param mUserList
     *            the mUserList to set
     */
    public void setmUserList(List<BuyerInfo> mUserList) {
        this.mUserList = mUserList;
        this.notifyDataSetChanged();
    }

    public BuyerAdapter(Context context, List<BuyerInfo> users,
            OnClickListener clickListener) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.onClickListener = clickListener;
        this.mUserList = users;
        this.application = (AgentApplication) context.getApplicationContext();
        
        optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
        optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
    }

    public int getCount() {
        return mUserList.size();
    }

    public Object getItem(int position) {
        return mUserList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.buyer_item, null);
        }
        final BuyerInfo item = mUserList.get(position);
        ImageView user_icon = (ImageView) convertView
                .findViewById(R.id.iv_usericon);
        TextView tv_userName = (TextView) convertView
                .findViewById(R.id.tv_username);
        TextView tv_phone_num = (TextView) convertView
                .findViewById(R.id.tv_usersign);
        TextView count = (TextView) convertView.findViewById(R.id.tv_count);
        ImageView iv_into = (ImageView) convertView.findViewById(R.id.iv_into);
        iv_into.setOnClickListener(onClickListener);
        
        
//        ImageUtil.getNetPic(context, user_icon, item.getPhoto(), R.drawable.nav_head);
        
        ImageUtil.getNetPicByUniversalImageLoad( null, user_icon, item.getPhoto(), optionsByNoPhoto);
        
        if (item.getRemark() != null && item.getRemark().length() > 0) {
            String str = "";
            if (item.getNickName() != null) {
                str = item.getRemark() + "(" + item.getNickName() + ")";
            } else {
                str = item.getRemark();
            }
            tv_userName.setText(str);
        } else {
            tv_userName.setText(item.getNickName());
        }
        if (application.getAgentInfo() != null
                && application.getAgentInfo().getAgentIdentity() != null
                && application.getAgentInfo().getAgentIdentity() == 11) {
            tv_phone_num.setText(item.getPhone());
        } else {
            tv_phone_num.setText(Util.hidePhoneNum(item.getPhone()));
        }
        iv_into.setTag(position);
        if (item.getMsgCount() != null && item.getMsgCount() > 0) {
            count.setText("(" + item.getMsgCount() + "条消息未读)");
        } else {
            count.setText("");
        }

        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

}
