package com.gsta.v2.activity.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.WareSalesObject;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class HotSaleGoodsListAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private Context context;
	private List<WareSalesObject> mList = new ArrayList<WareSalesObject>();
	int type;
	// 商品图片使用
	private DisplayImageOptions optionsByNonePic;
	// 头像类使用
	@SuppressWarnings("unused")
	private DisplayImageOptions optionsByNoPhoto;

	public HotSaleGoodsListAdapter(Context context,
			List<WareSalesObject> mList, int type) {
		
		 optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
	     optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
		
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.type = type;
		for (int i = 0; i < mList.size(); i++) {
			if (mList.get(i).getWareFlag() == type) {
				this.mList.add(mList.get(i));
			}
		}
	}

	public int getCount() {
		return mList.size();
	}

	public WareSalesObject getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.list_item, null);
		}
		WareSalesObject item = getItem(position);
		TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
		TextView tvDetail = (TextView) convertView.findViewById(R.id.tv_detail);
		TextView tvPrice = (TextView) convertView
				.findViewById(R.id.tv_bottom_left);
		ImageView iv_icon = (ImageView) convertView.findViewById(R.id.iv_icon);
		tvTitle.setText(item.getProdName());
		tvDetail.setText(item.getSpeciDesc());
		tvPrice.setText("价格：" + item.getMinPrice());
		iv_icon.setImageResource(R.drawable.icon_loading);
		
/*		ImageUtil.getNetPic(context, iv_icon, item.getMediaPath(),
				R.drawable.none_pic);*/
		
        ImageUtil.getNetPicByUniversalImageLoad( null, iv_icon, Util.converPicPath(item.getMediaPath(),FinalUtil.PIC_MIDDLE), optionsByNonePic);
		
		
		return convertView;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public List<WareSalesObject> getmList() {
		return mList;
	}

	public void setmList(List<WareSalesObject> mList) {
		this.mList.clear();
		for (int i = 0; i < mList.size(); i++) {
			if (mList.get(i).getWareFlag() == type) {
				this.mList.add(mList.get(i));
			}
		}
		this.notifyDataSetChanged();
	}

}