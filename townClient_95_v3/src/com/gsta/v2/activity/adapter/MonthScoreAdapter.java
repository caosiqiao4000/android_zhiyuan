package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.StatInfo;

/**
 * 
 * @author wubo
 * @createtime 2012-9-5
 */
public class MonthScoreAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private List<StatInfo> items;

	/**
	 * @return the mUserList
	 */
	public List<StatInfo> getItems() {
		return items;
	}

	public void addContact(List<StatInfo> StatInfo) {
		if (this.items != null) {
			this.items.addAll(StatInfo);
		} else
			this.items = StatInfo;

		this.notifyDataSetChanged();
	}

	/**
	 * @param Items
	 *            the Items to set
	 */
	public void setItems(List<StatInfo> Items) {
		this.items = Items;
		this.notifyDataSetChanged();
	}

	public MonthScoreAdapter(Context context, List<StatInfo> users) {
		layoutInflater = LayoutInflater.from(context);
		this.items = users;
	}

	public int getCount() {
		return items.size();
	}

	public Object getItem(int position) {
		return items.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final StatInfo item = items.get(position);
		if (null == convertView) {
		    convertView = layoutInflater.inflate(R.layout.monthscore_item, null);
        }
		TextView time = (TextView) convertView.findViewById(R.id.time);
		TextView trafficDirect = (TextView) convertView
				.findViewById(R.id.trafficDirect);
		TextView brokerageDirect = (TextView) convertView
				.findViewById(R.id.brokerageDirect);
		TextView trafficIndirect = (TextView) convertView
				.findViewById(R.id.trafficIndirect);
		TextView brokerageIndirect = (TextView) convertView
				.findViewById(R.id.brokerageIndirect);
		TextView totalFee = (TextView) convertView.findViewById(R.id.totalFee);

		time.setText(item.getBillcycle());
		trafficDirect.setText("直接分成:\t" + item.getTrafficDirect() + "元");
		brokerageDirect.setText("直接佣金:\t" + item.getBrokerageDirect() + "元");
		trafficIndirect.setText("间接分成:\t" + item.getTrafficIndirect() + "元");
		brokerageIndirect.setText("间接佣金:\t" + item.getBrokerageIndirect()
				+ "元");
		totalFee.setText("总额:\t" + item.getTotalFee() + "元");
		return convertView;
	}

}
