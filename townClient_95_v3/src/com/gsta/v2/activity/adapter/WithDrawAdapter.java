package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.WithDrawInfo;
import com.gsta.v2.util.Util;

/**
 * @author longxianwen
 * @createTime Apr 10, 2013 3:28:11 PM
 * @version: 1.0
 * @desc:收支/提现明细适配器
 */
public class WithDrawAdapter extends BaseAdapter {

    private Context context;
    private List<WithDrawInfo> wdInfos;

    public WithDrawAdapter(Context context, List<WithDrawInfo> wdInfos) {
        super();
        this.context = context;
        this.wdInfos = wdInfos;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (wdInfos != null) {
            return wdInfos.size();
        }
        return 0;
    }

    @Override
    public WithDrawInfo getItem(int position) {
        // TODO Auto-generated method stub
        if (this.wdInfos != null) {
            return this.wdInfos.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {

        if (rowView == null) {
            rowView = LayoutInflater.from(context).inflate(
                    R.layout.withdrawlist_item, null);
        }
        TextView tv_type = (TextView) rowView.findViewById(R.id.tv_type); // 交易类型,提取类别 0:充值，1:提现，3:分成
        TextView tv_operate1 = (TextView) rowView.findViewById(R.id.tv_operate1); // 收入/支出金额,操作
        TextView tv_account = (TextView) rowView.findViewById(R.id.tv_account); // 提取账号
        TextView tv_money = (TextView) rowView.findViewById(R.id.tv_money); // 提取金额
        TextView tv_time = (TextView) rowView.findViewById(R.id.tv_time); // 时间,提取时间
        TextView tv_operate2 = (TextView) rowView.findViewById(R.id.tv_operate2); // 余额，状态
        LinearLayout ll_withdraw = (LinearLayout) rowView.findViewById(R.id.ll_withdraw);

        WithDrawInfo wdData = getItem(position);

        if (wdData.getType() != null && !wdData.getType().equals("")) {
            int type = Integer.parseInt(wdData.getType());
            getTypeString(tv_type, type);
        } else if (wdData.getTransactionType() != null && !wdData.getTransactionType().equals("")) {
            int type = Integer.parseInt(wdData.getTransactionType());
            getTypeString(tv_type, type);
        } else {
            tv_type.setText("");
        }

        if (wdData.getIncomeSum() != null) {
            tv_operate1.setText(
                    Html.fromHtml(
                            "<font color=#FF0000 >" + wdData.getIncomeSum() + "元" + "</font> "));
        } else if (wdData.getPaySum() != null) {
            tv_operate1.setText(
                    Html.fromHtml(
                            "<font color=#00A600 >" + "-" + wdData.getPaySum() + "元" + "</font> "));
            // } else if(wdData.getTransactionStatus() != null) { //操作,待定
            // int status = Integer.parseInt(wdData.getTransactionStatus());
            // getTransactionStatusString(tv_operate1,status);
        } else {
            tv_operate1.setText("");
        }

        if (wdData.getPaymentAccount() != null && !wdData.getPaymentAccount().equals("")) {
            ll_withdraw.setVisibility(View.VISIBLE);
            tv_account.setText(wdData.getPaymentAccount());
        } else {
            tv_account.setText("");
            ll_withdraw.setVisibility(View.GONE);
        }

        if (wdData.getTransactionAmount() != null) {
            ll_withdraw.setVisibility(View.VISIBLE);
            tv_money.setText(
                    Html.fromHtml(
                            "<font color=#FF0000 >" + wdData.getTransactionAmount() + "元" + "</font> "));
        } else {
            tv_money.setText("");
            ll_withdraw.setVisibility(View.GONE);
        }

        if (wdData.getTime() != null && !wdData.getTime().equals("")) {
            tv_time.setText(wdData.getTime());
        } else if (wdData.getTransactionDate() != null && !wdData.getTransactionDate().equals("")) {
            tv_time.setText(wdData.getTransactionDate());
        } else {
            tv_time.setText(Util.getNowTime());
        }

        if (wdData.getBalance() != null && !wdData.getBalance().equals("")) {
            tv_operate2.setText("余额: " + wdData.getBalance() + "元");
        } else if (wdData.getTransactionStatus() != null && !wdData.getTransactionStatus().equals("")) {
            int status = Integer.parseInt(wdData.getTransactionStatus());
            getTransactionStatusString(tv_operate2, status);
        } else {
            tv_operate2.setText("");
        }
        return rowView;
    }

    private void getTypeString(TextView tv_type, int type) {
        if (type == 0) {
            tv_type.setText("充值");
        } else if (type == 1) {
            tv_type.setText("提现");
        } else {
            tv_type.setText("分成");

        }
    }

    private void getTransactionStatusString(TextView tv_type, int status) {
        if (status == 0) {
            tv_type.setText("待处理");
        } else if (status == 1) {
            tv_type.setText("已转账");
        } else if (status == 2) {
            tv_type.setText("申请驳回");
        } else {
            tv_type.setText("");
        }
    }

    public void setWdInfos(List<WithDrawInfo> wdInfos) {
        this.wdInfos = wdInfos;
        this.notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
