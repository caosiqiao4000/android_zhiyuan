package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.AgentMemoinfo;

public class RemarkAdapter extends BaseAdapter {
    private Context context;
    private List<AgentMemoinfo> messages;

    public RemarkAdapter(Context context, List<AgentMemoinfo> messages) {
        this.context = context;
        this.messages = messages;
    }

    public int getCount() {
        if (messages != null) {
            return messages.size();
        }
        return 0;
    }

    public AgentMemoinfo getItem(int position) {
        return this.messages.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View rowView, ViewGroup parent) {
        final AgentMemoinfo item = getItem(position);
        if (rowView == null) {
            rowView = LayoutInflater.from(context).inflate(
                    R.layout.remark_list_item, null);
        }
        TextView goodsname = (TextView) rowView
                .findViewById(R.id.tv_goodsname);
        TextView remark_info = (TextView) rowView.findViewById(R.id.tv_goodsinfo);// 笔记简略描述
        TextView remark = (TextView) rowView.findViewById(R.id.tv_remark);
        // ImageView del = (ImageView) rowView.findViewById(R.id.iv_del);
        // AgentMemoinfo msg = messages.get(position);

        if (position < 9) {
            goodsname.setText("记录" + "0" + (position + 1));
        } else {
            goodsname.setText("记录" + (position + 1));
        }
        remark_info.setText(item.getProductName());
        remark.setText(item.getDealtime());
        // remark.setText(Util.getFormatDate(msg.getDealtime()))
        // del.setOnClickListener(clickListener);
        // del.setTag(item.getId());
        return rowView;
    }

    public List<AgentMemoinfo> getMessages() {
        return messages;
    }

    public void setMessages(List<AgentMemoinfo> messages) {
        this.messages = messages;
        this.notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
