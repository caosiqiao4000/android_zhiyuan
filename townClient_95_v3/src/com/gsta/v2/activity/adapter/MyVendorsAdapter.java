package com.gsta.v2.activity.adapter;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.SellerInfo;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.Util;
import com.gsta.v2.util.VendorsInfoDesComparator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 
 * @author longxianwen
 * @createTime 2013-3-26 上午11:57:58
 * @version: 1.0
 * @desc:我的卖家数据源
 */
public class MyVendorsAdapter extends BaseAdapter {

    Context context;
    List<SellerInfo> siInfos;
    private double localLat;
    private double localLng;
    
	// 商品图片使用
	@SuppressWarnings("unused")
	private DisplayImageOptions optionsByNonePic;
	// 头像类使用
	private DisplayImageOptions optionsByNoPhoto;

    private Map<Integer, View> viewMap = new HashMap<Integer, View>();

    public MyVendorsAdapter(Context context, List<SellerInfo> rsInfos,
            double localLat, double localLng) {
        this.context = context;
        this.siInfos = rsInfos;
        this.localLat = localLat;
        this.localLng = localLng;
        
        optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
	     optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
    }

    @Override
    public int getCount() {
        if (siInfos != null) {
            return siInfos.size();
        }
        return 0;
    }

    @Override
    public SellerInfo getItem(int position) {
        return this.siInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {

        final SellerInfo item = getItem(position);
        rowView = this.viewMap.get(position);

        if (rowView == null) {
            rowView = LayoutInflater.from(context).inflate(
                    R.layout.rslist_item2, null);

            ImageView iv_icon = (ImageView) rowView.findViewById(R.id.iv_icon); // 店铺icon
            TextView tv_shopname = (TextView) rowView
                    .findViewById(R.id.tv_shopname); // 店铺名称
            ImageView iv_level = (ImageView) rowView
                    .findViewById(R.id.iv_level); // 店铺等级
            TextView tv_distance = (TextView) rowView
                    .findViewById(R.id.tv_distance); // 店铺距离
            TextView tv_notice1 = (TextView) rowView
                    .findViewById(R.id.tv_notice1); // 店铺公告
            ProgressBar iv_pro_bar = (ProgressBar) rowView
                    .findViewById(R.id.iv_pro_bar);

            SellerInfo shopData = siInfos.get(position);

            // icon
            if (shopData.getPhoto() != null && shopData.getPhoto().length() > 1) {
            	
            	
//                ImageUtil.getNetPic(context, iv_pro_bar, iv_icon,
//                        shopData.getPhoto(),R.drawable.nav_head);
                ImageUtil.getNetPicByUniversalImageLoad(iv_pro_bar, iv_icon, shopData.getPhoto(), optionsByNoPhoto);
                
            } else {
                iv_icon.setImageResource(R.drawable.nav_head);
            }

            tv_shopname.setText(shopData.getShopName());

            // 用户等级
            iv_level.setImageResource(Util.getLevelCover(shopData.getLevelCode()));

            if (localLat == 0 || localLng == 0) {
                tv_distance.setVisibility(View.GONE);
            } else {
                System.out.println("定位位置 ：" + localLat + ", " + localLng
                        + "得到位置: " + shopData.getLatitude()
                        + ", " + shopData.getLongitude());

                // 一公里以下：正常显示
                // 一公里以上：换算,1公里=1千米
                System.out.println("两点间的距离是: " + shopData.getDistance());
                if (shopData.getDistance() == null || shopData.getDistance() == Long.MAX_VALUE) {
//                    tv_distance.setText("店铺无定位数据");
                    tv_distance.setText("");
                } else {
                    if (shopData.getDistance() >= 1000) {
                        tv_distance.setText("距离" + shopData.getDistance() / 1000 + "公里以内");
                    } else {
                        tv_distance.setText("距离" + shopData.getDistance() + "米以内");
                    }
                }
            }

            if (shopData.getMemo() == null || shopData.getMemo().equals("")) {
                tv_notice1
                        .setText("无公告,敬请期待...");
            } else {
                tv_notice1.setText(shopData.getMemo());
            }

            viewMap.put(position, rowView);
        }

        return rowView;
    }

    public List<SellerInfo> getRsInfos() {
        return siInfos;
    }

    public void setRsInfos(List<SellerInfo> rsInfos) {
        this.siInfos = rsInfos;
        this.notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        // TODO Auto-generated method stub
        // this.viewMap = new HashMap<Integer, View>();
        viewMap.clear();
        if (localLat != 0 && localLng != 0) {
            sort();
        }
        super.notifyDataSetChanged();
    }

    public void notifyData(double localLat, double localLng) {
        this.localLat = localLat;
        this.localLng = localLng;
        notifyDataSetChanged();
    }

    /**
     * 排序
     * 
     * @author longxianwen
     * @Title: sort
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param 设定文件
     * @return void 返回类型
     * @throws
     */
    public void sort() {
        for (int i = 0; i < siInfos.size(); i++) {
        	SellerInfo si = siInfos.get(i);
            
            if(si.getLatitude() != null && si.getLongitude() != null && si.getLongitude() != 0 && si.getLatitude() != 0) {
            	long distance = Util.DistanceOfTwoPoints(localLat, localLng, si
            			.getLatitude(), si.getLongitude());
            	si.setDistance(distance);
            }else {
                si.setDistance(Long.MAX_VALUE);
            }
            
        }
        VendorsInfoDesComparator sid = new VendorsInfoDesComparator();
        Collections.sort(siInfos, sid);
    }
}
