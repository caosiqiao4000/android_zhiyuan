package com.gsta.v2.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;

/**
 * @author wubo
 * @createtime 2012-9-1
 */
public class ProductOrderAdapter extends BaseAdapter {

    Context context;
    String[] order_item;
    String[] order_id;

    public ProductOrderAdapter(Context context) {
        this.context = context;
        order_item = context.getResources().getStringArray(
                R.array.product_order_item);
        order_id = context.getResources().getStringArray(
                R.array.product_order_id);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return order_item.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return order_id[position];
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.city_item,
                    null);
        }
        TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
        tv_name.setText(order_item[position]);
        return convertView;
    }

}
