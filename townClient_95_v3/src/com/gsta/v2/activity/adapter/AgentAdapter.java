package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.AgentInfo;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.ImageUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 
 * @author wubo
 * @createtime 2012-9-5
 */
public class AgentAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private OnClickListener mClickListener;
	private List<AgentInfo> mUserList;
	private Context context;

	// 商品图片使用
	@SuppressWarnings("unused")
	private DisplayImageOptions optionsByNonePic;
	// 头像类使用
	private DisplayImageOptions optionsByNoPhoto;

	/**
	 * @return the mUserList
	 */
	public List<AgentInfo> getmUserList() {
		return mUserList;
	}

	public void addContact(List<AgentInfo> AgentInfo) {
		if (this.mUserList != null) {
			this.mUserList.addAll(AgentInfo);
		} else
			this.mUserList = AgentInfo;

		this.notifyDataSetChanged();
	}

	/**
	 * @param mUserList
	 *            the mUserList to set
	 */
	public void setmUserList(List<AgentInfo> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	public AgentAdapter(Context context, List<AgentInfo> users,
			OnClickListener mClickListener) {
		this.context = context;
		layoutInflater = LayoutInflater.from(context);
		this.mClickListener = mClickListener;
		this.mUserList = users;

		optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(
				R.drawable.none_pic, Bitmap.Config.ARGB_8888);
		optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(
				R.drawable.nav_head, Bitmap.Config.ARGB_8888);
	}

	public int getCount() {
		return mUserList.size();
	}

	public Object getItem(int position) {
		return mUserList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final AgentInfo item = mUserList.get(position);
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.users_item, null);
		}
		ImageView user_icon = (ImageView) convertView
				.findViewById(R.id.iv_usericon);
		TextView tv_userName = (TextView) convertView
				.findViewById(R.id.tv_username);
		TextView tv_phone_num = (TextView) convertView
				.findViewById(R.id.tv_usersign);
		ImageView iv_into = (ImageView) convertView.findViewById(R.id.iv_into);
		if (null != item.getPhoto()
				&& !TextUtils.isEmpty(item.getPhoto().trim())) {
			((AgentApplication) context.getApplicationContext())
					.setPhotoPathByUid(item.getUid(), item.getPhoto());
			ImageUtil.getNetPicByUniversalImageLoad(null, user_icon,
					item.getPhoto(), optionsByNoPhoto);

		} else {
			user_icon.setImageResource(R.drawable.nav_head);
		}
		tv_userName.setText(item.getName());
		tv_phone_num.setText(item.getMobile());
		iv_into.setOnClickListener(mClickListener);
		iv_into.setTag(position);

		return convertView;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

}
