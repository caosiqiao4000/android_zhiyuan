package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.SmileyParser;

public class MsgHistoryAdapter extends BaseAdapter {

    private Context context;
    private List<IMBean> list;
    private SmileyParser parser;
    String name;
    String himname;

    public MsgHistoryAdapter(Context context, List<IMBean> list, String name,
            String himname) {
        this.context = context;
        this.name = name;
        this.himname = himname;
        this.list = list;
        parser = SmileyParser.getInstance(context);
    }

    public void setData(List<IMBean> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.msghistory_item,
                    null);
        }
        IMBean msg = list.get(position);
        TextView nickname = (TextView) view.findViewById(R.id.nickname);
        TextView time = (TextView) view.findViewById(R.id.time);
        TextView content = (TextView) view.findViewById(R.id.content);
        if (msg.getDirection().equals(FinalUtil.IM_TO_ME)) {
            nickname.setText(himname);
        } else {
            nickname.setText(name);
        }
        time.setText(msg.getTime());
        content.setText(parser.addSmileySpans(msg.getContent()));
        return view;
    }
}
