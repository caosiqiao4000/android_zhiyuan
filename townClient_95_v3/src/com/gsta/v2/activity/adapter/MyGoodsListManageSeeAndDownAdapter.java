package com.gsta.v2.activity.adapter;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.myshop.GoodsDetailActivity;
import com.gsta.v2.activity.myshop.MyGoodsActivity;
import com.gsta.v2.entity.AgentGoodsInfo;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 查看和下架我的商品
 * 
 * @author caosq 2013-5-20
 */
public class MyGoodsListManageSeeAndDownAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private Context context;
    private List<AgentGoodsInfo> mList;
    private AgentGoodsHolder holder;
    private MyDialog activateVoucherDialog;
    public static final int GOODS_NORMAL_FLAG = 0x000; // 新品 正常态
    public static final int GOODS_UP_FLAG = 0x001; // 已经上架
    public static final int GOODS_DOWN_FLAG = 0x002; // 已经下架
    
	// 商品图片使用
	private DisplayImageOptions optionsByNonePic;
	// 头像类使用
	@SuppressWarnings("unused")
	private DisplayImageOptions optionsByNoPhoto;

    public MyGoodsListManageSeeAndDownAdapter(Context context, List<AgentGoodsInfo> mList) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mList = mList;
        
		 optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
	     optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public AgentGoodsInfo getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AgentGoodsInfo item = getItem(position);
        if (null == convertView) {
            holder = new AgentGoodsHolder();
            convertView = layoutInflater.inflate(R.layout.list_item_updown_goods, null);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tvDetail = (TextView) convertView.findViewById(R.id.tv_detail);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.tv_bottom_left);
            // holder.tv_newGoods_des = (TextView) convertView.findViewById(R.id.tv_newGoods_des);
            holder.btn_goods_up_down = (Button) convertView.findViewById(R.id.btn_goods_up_down);
            holder.pro_bar = (ProgressBar) convertView.findViewById(R.id.iv_pro_bar);
            holder.iv_icon = (ImageView) convertView.findViewById(R.id.iv_icon);
            holder.ll_text_midd = (RelativeLayout) convertView.findViewById(R.id.ll_text_midd);
            //
            holder.btn_goods_up_down.setOnClickListener(btn_down_click);
            holder.ll_text_midd.setOnClickListener(btn_down_click);
            holder.iv_icon.setOnClickListener(btn_down_click);
            convertView.setTag(holder);
        } else {
            holder = (AgentGoodsHolder) convertView.getTag();
        }

//        ImageUtil.getNetPic(context, holder.pro_bar, holder.iv_icon, item.getMediaPath(), R.drawable.none_pic);
        
        ImageUtil.getNetPicByUniversalImageLoad( holder.pro_bar, holder.iv_icon, Util.converPicPath(item.getMediaPath(),FinalUtil.PIC_MIDDLE), optionsByNonePic);
        
        if (GOODS_DOWN_FLAG == item.getFlag()) {
            holder.btn_goods_up_down.setText("上  架");
        } else if (GOODS_UP_FLAG == item.getFlag()) {
            holder.btn_goods_up_down.setText("下  架");
        } else if (GOODS_NORMAL_FLAG == item.getFlag()) {
            // holder.tv_newGoods_des.setVisibility(0);
            holder.btn_goods_up_down.setText("上  架");
        }
        //
        holder.btn_goods_up_down.setTag(position);
        holder.ll_text_midd.setTag(position);
        holder.iv_icon.setTag(position);
        holder.tvTitle.setText(item.getProdName());
        holder.tvDetail.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tvDetail.setText("原价:￥" + item.getMinRetailPrice());
        holder.tvPrice.setText("售价:￥" + item.getMinSalePrice());
        return convertView;
    }

    OnClickListener btn_down_click = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final int position = (Integer) v.getTag();
            final AgentGoodsInfo item = getItem(position);
            if (v.getId() == holder.btn_goods_up_down.getId()) {
                String title = item.getProdName();
                String contentText = null;
                if (GOODS_DOWN_FLAG == item.getFlag() || GOODS_NORMAL_FLAG == item.getFlag()) {
                    contentText = "确定要将该商品 上架 吗?";
                } else {
                    contentText = "确定要将该商品 下架 吗?";
                }
                activateVoucherDialog = MyDialog.showDialog(context, title, contentText, R.string.submit,
                        R.string.cancel, false,
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // 发送下架 请求
                                MyGoodsActivity activity = (MyGoodsActivity) context;
                                AgentApplication application = (AgentApplication) activity.getApplication();
                                ServerSupportManager serverMana = new ServerSupportManager(context, activity);
                                List<Parameter> paras = new ArrayList<Parameter>();
                                paras.add(new Parameter("uid", application.getUid()));
                                paras.add(new Parameter("productId", item.getSpeciesId()));
                                int query_code = 100;
                                // 0:上架，1:下架
                                if (GOODS_DOWN_FLAG == item.getFlag() || GOODS_NORMAL_FLAG == item.getFlag()) {
                                    paras.add(new Parameter("opType", "0"));
                                    query_code = MyGoodsActivity.upGoods_code;
                                } else if (GOODS_UP_FLAG == item.getFlag()) {
                                    paras.add(new Parameter("opType", "1"));
                                    query_code = MyGoodsActivity.downGoods_code;
                                }
                                serverMana.supportRequest(AgentConfig.AddToMyMall(), paras, true,
                                        "加载中,请稍等 ...", query_code);
                                activity.sendAdapterViewPosition(position);
                                cancelDialog();
                            }
                        });
            } else if (v.getId() == holder.iv_icon.getId() || v.getId() == holder.ll_text_midd.getId()) {
                // 查看详情
                Intent intent = new Intent(context,
                        GoodsDetailActivity.class);
                intent.putExtra(FinalUtil.GOODSINFO, item);
                context.startActivity(intent);
            }
        }
    };

    private void cancelDialog() {
        if (null != activateVoucherDialog) {
            MyDialog.dismiss(activateVoucherDialog);
            activateVoucherDialog = null;
        }
    }

    final class AgentGoodsHolder {
        TextView tvDetail;
        TextView tvPrice;
        TextView tvTitle;
        RelativeLayout ll_text_midd;
        // TextView tv_newGoods_des;
        ImageView iv_icon;
        ProgressBar pro_bar;
        Button btn_goods_up_down;
    }
}