package com.gsta.v2.activity.adapter;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.AgentWebViewActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.mytown.MyUserIndentQueryActivity;
import com.gsta.v2.entity_v2.OrderInfo;
import com.gsta.v2.entity_v2.OrderWareInfo;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 代理商的订单列表
 * 
 * @author caosq 2013-4-10
 */
public class IndentAgentListQueryAdapter extends BaseAdapter {
	private static final Logger logger = LoggerFactory
			.getLogger(IndentAgentListQueryAdapter.class);
	private LayoutInflater layoutInflater;
	private List<OrderInfo> items;
	private Context context;
	private ViewIndentHolder holder;
	// 商品图片使用
	private DisplayImageOptions optionsByNonePic;
	// 头像类使用
	@SuppressWarnings("unused")
	private DisplayImageOptions optionsByNoPhoto;

	/**
	 * @return the mUserList
	 */
	public List<OrderInfo> getItems() {
		return items;
	}

	/**
	 * @param Items
	 *            the Items to set
	 */
	public void setItems(List<OrderInfo> Items) {
		this.items = Items;
		this.notifyDataSetChanged();
	}

	public IndentAgentListQueryAdapter(Context context, List<OrderInfo> users) {
		this.context = context;
		layoutInflater = LayoutInflater.from(context);
		this.items = users;
		optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(
				R.drawable.none_pic, Bitmap.Config.ARGB_8888);
		optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(
				R.drawable.nav_head, Bitmap.Config.ARGB_8888);
	}

	public int getCount() {
		return items.size();
	}

	public Object getItem(int position) {
		return items.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final OrderInfo item = items.get(position);
		if (convertView == null) {
			holder = new ViewIndentHolder();
			convertView = layoutInflater.inflate(
					R.layout.indent_list_query_item, null);
			holder.imageView2 = (ImageView) convertView
					.findViewById(R.id.imageView2);
			holder.v_line = convertView.findViewById(R.id.v_line);
			holder.iv_pro_bar2 = (ProgressBar) convertView
					.findViewById(R.id.iv_pro_bar2);
			holder.tv_name_two = (TextView) convertView
					.findViewById(R.id.tv_name_two);
			holder.tv_money = (TextView) convertView
					.findViewById(R.id.tv_money);
			holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
			holder.btn_left = (Button) convertView.findViewById(R.id.btn_left);
			holder.btn_right = (Button) convertView
					.findViewById(R.id.btn_right);
			holder.ll_title = (LinearLayout) convertView
					.findViewById(R.id.ll_title);
			holder.ll_detail = (LinearLayout) convertView
					.findViewById(R.id.ll_detail);
			convertView.setTag(holder);
		} else {
			holder = (ViewIndentHolder) convertView.getTag();
		}
		if (null != item.getOrderWareInfos()) {
			final OrderWareInfo info = item.getOrderWareInfos().get(0);
			holder.imageView2.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					AgentApplication application = (AgentApplication) context
							.getApplicationContext();
					// 跳转到商品详情
					List<Parameter> parameters = new ArrayList<Parameter>();
					if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
						parameters.add(new Parameter("account", application
								.getAccount()));
						parameters.add(new Parameter("loginUid", application
								.getUid()));
					} else {
						parameters.add(new Parameter("account", ""));
						parameters.add(new Parameter("loginUid", ""));
					}
					parameters.add(new Parameter(FinalUtil.AGENT_UID, ""));
					parameters.add(new Parameter("specificationId", info
							.getSpecificationId()));
					parameters.add(new Parameter("speciesId", info
							.getSpeciesId()));
					Util.toWebViewActivity(context, AgentWebViewActivity.class,
							AgentConfig.getShopAdd(), parameters);
				}
			});
			// 商品图片
			ImageUtil.getNetPicByUniversalImageLoad(holder.iv_pro_bar2,
					holder.imageView2, Util.converPicPath(info.getMediaPath(),
							FinalUtil.PIC_MIDDLE), optionsByNonePic);

			holder.tv_name_two.setText(Html.fromHtml("<b>" + info.getWareName()
					+ "</b> " + "<font color=#FF0000 >" + info.getDescription()
					+ "</font> "));
		} else {
			logger.error(" 商品详细信息 OrderWareInfo is null");
		}
		holder.ll_detail.setTag(position);
		holder.ll_title.setVisibility(View.GONE);
		holder.v_line.setVisibility(View.GONE);
		holder.tv_money.setText("￥" + item.getOrderPrice());
		holder.tv_time.setText(item.getOrderDate());
		holder.ll_detail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final int a = Integer.valueOf(v.getTag().toString());
				final OrderInfo info = (OrderInfo) getItem(a);
				// OrderWareInfo ware = info.getOrderWareInfos().get(0);
				List<Parameter> parameters = new ArrayList<Parameter>();
				parameters.add(new Parameter("orderId", info.getOrderId()));
				parameters.add(new Parameter("account",
						((AgentApplication) context.getApplicationContext())
								.getAccount()));
				// parameters.add(new Parameter("systime", String.valueOf(new
				// Date().getTime())));
				parameters.add(new Parameter("loginUid",
						((AgentApplication) context.getApplicationContext())
								.getUid()));
				Util.toWebViewActivity(context, AgentWebViewActivity.class,
						AgentConfig.getOrderDetail(), parameters);
			}
		});
		int query_code = item.getOrderStatus();
		/**
		 * <p>
		 * 4.待确认收货,1.待付款,2.待发货,5.已成功,6.已取消, opType =1时必填
		 */
		if (query_code == MyUserIndentQueryActivity.QUERY_UNCONFIRMED) {
			setBtnText("查看物流", "确认收货", position);
		} else {
			convertView.findViewById(R.id.tv_line).setVisibility(View.GONE);
			convertView.findViewById(R.id.ll_btn).setVisibility(View.GONE);
		}
		// else if (query_code == MyIndentQueryActivity.QUERY_UNPAYMENT) {
		// setBtnText("取消订单", "立即支付", position);
		// } else if (query_code == MyIndentQueryActivity.QUERY_UNDELIVERY) {
		// holder.btn_left.setText("发货提醒");
		// holder.btn_right.setVisibility(View.GONE);
		// holder.btn_left.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// // 发货提醒
		//
		// }
		// });
		// } else if (query_code == MyIndentQueryActivity.QUERY_SUCCESS ||
		// query_code == MyIndentQueryActivity.QUERY_CANCEL) {
		// convertView.findViewById(R.id.tv_line).setVisibility(View.GONE);
		// convertView.findViewById(R.id.ll_btn).setVisibility(View.GONE);
		// }
		return convertView;
	}

	private void setBtnText(String string, String string2, final int position) {
		holder.btn_left.setText(string);
		holder.btn_right.setVisibility(View.GONE);
		// holder.btn_right.setText(string2);
		holder.btn_left.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int query_code = ((OrderInfo) getItem(position))
						.getOrderStatus();
				if (query_code == MyUserIndentQueryActivity.QUERY_UNCONFIRMED) {
					// setBtnText("查看物流", "确认收货");
					if (v == holder.btn_left) {
						// Intent intent = new Intent();
						// intent.setClass(context,
						// OrderLogisticsInfoActivity.class);//
						// ArrayList<OrderLogisticsInfo> infos =
						// (ArrayList<OrderLogisticsInfo>) items.get(
						// Integer.valueOf(String.valueOf(holder.btn_left.getTag()))).getOrderLogisticsInfos();
						// intent.putExtra(LOGISTICS_INFO, infos);
						// context.startActivity(intent);
					}
				}
			}
		});
	}

	/**
	 * 每个应用显示的内容，包括图标和名称
	 * 
	 * @author Yao.GUET
	 * 
	 */
	final class ViewIndentHolder {
		View v_line;
		ImageView imageView2;
		ProgressBar iv_pro_bar2;
		TextView tv_name_two;// 这里需要HTML
		TextView tv_money, tv_time;
		Button btn_left, btn_right;
		LinearLayout ll_title, ll_detail;
	}
}
