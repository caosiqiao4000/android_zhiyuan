package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.DirectDeductDetail;

/**
 * 直接收益
 * 
 * @author wubo
 * @createtime 2012-9-5
 */
public class ComissionDirectAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<DirectDeductDetail> items;
    private ViewHolder viewHolder;

    /**
     * @return the mUserList
     */
    public List<DirectDeductDetail> getItems() {
        return items;
    }

    /**
     * @param Items
     *            the Items to set
     */
    public void setItems(List<DirectDeductDetail> Items) {
        this.items = Items;
        this.notifyDataSetChanged();
    }

    public ComissionDirectAdapter(Context context,
            List<DirectDeductDetail> users) {
        layoutInflater = LayoutInflater.from(context);
        this.items = users;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final DirectDeductDetail item = items.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.realsell_item, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.tv_1 = (TextView) convertView.findViewById(R.id.tv_1);
            viewHolder.tv_2 = (TextView) convertView.findViewById(R.id.tv_2);
            // viewHolder.tv_3 = (TextView) convertView.findViewById(R.id.tv_3);
            viewHolder.tv_4 = (TextView) convertView.findViewById(R.id.tv_4);
            viewHolder.tv_5 = (TextView) convertView.findViewById(R.id.tv_5);
            viewHolder.tv_6 = (TextView) convertView.findViewById(R.id.tv_6);
            viewHolder.tv_7 = (TextView) convertView.findViewById(R.id.tv_7);
            viewHolder.tv_8 = (TextView) convertView.findViewById(R.id.tv_8);
            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        convertView.findViewById(R.id.ll_three).setVisibility(View.GONE);
        viewHolder.name.setText((item.getProductName() == null ? "" : item
                .getProductName()));
        viewHolder.tv_1.setText(item.getOrderTime() == null ? "" : item
                .getOrderTime());
        viewHolder.tv_2.setText(item.getOrderId() == null ? "" : item
                .getOrderId());
        // viewHolder.tv_3.setText(item.getMobile() == null ? "" : item
        // .getMobile());
        viewHolder.tv_4.setText(item.getOrderCount() == null ? "" : ""
                + item.getOrderCount());
        viewHolder.tv_5.setText(item.getAmount() == null ? "" : item
                .getAmount() + "");
        viewHolder.tv_6.setText(item.getBrokerageDirect() == null ? "" : ""
                + item.getBrokerageDirect());
        viewHolder.tv_7.setText(item.getDeducts() == null ? "" : "" + item.getDeducts());
        viewHolder.tv_8.setText(item.getIfStat() == null ? "" : item
                .getIfStat());
        return convertView;
    }

    /**
     * 每个应用显示的内容，包括图标和名称
     * 
     * @author Yao.GUET
     * 
     */
    final class ViewHolder {
        TextView name;
        TextView tv_1;
        TextView tv_2;
        // TextView tv_3;
        TextView tv_4;
        TextView tv_5;
        TextView tv_6;
        TextView tv_7;
        TextView tv_8;
    }

}
