package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.Contacts;

/**
 * 
 * @author zhengjy
 * 
 */
public class ContactsAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private OnClickListener mClickListener;
	private List<Contacts> mUserList;

	/**
	 * @return the mUserList
	 */
	public List<Contacts> getmUserList() {
		return mUserList;
	}

	public void addContact(List<Contacts> contacts) {
		if (this.mUserList != null) {
			this.mUserList.addAll(contacts);
		} else
			this.mUserList = contacts;

		this.notifyDataSetChanged();
	}

	/**
	 * @param mUserList
	 *            the mUserList to set
	 */
	public void setmUserList(List<Contacts> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	public ContactsAdapter(Context context, List<Contacts> users,
			OnClickListener mClickListener) {
		layoutInflater = LayoutInflater.from(context);
		this.mClickListener = mClickListener;
		this.mUserList = users;
	}

	public int getCount() {
		return mUserList.size();
	}

	public Object getItem(int position) {
		return mUserList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
	    final Contacts item = mUserList.get(position);
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.contact_item, null);
		}
		TextView tv_firstCharHint = (TextView) convertView
		        .findViewById(R.id.tv_first_char_hint);
		TextView tv_userName = (TextView) convertView
		        .findViewById(R.id.tv_username);
		TextView tv_phone_num = (TextView) convertView
		        .findViewById(R.id.tv_usersign);
		ImageView iv_into = (ImageView) convertView
		        .findViewById(R.id.iv_into);
		
		tv_userName.setText(item.getUserName());
		tv_phone_num.setText(item.getPhoneNum());
		iv_into.setOnClickListener(mClickListener);
		iv_into.setTag(position);
		
		if (!item.getFirstLetter().equals(Contacts.NO_LETTER)) {
		    tv_firstCharHint.setVisibility(View.VISIBLE);
		    tv_firstCharHint.setText(item.getFirstLetter());
		} else {
		    tv_firstCharHint.setVisibility(View.GONE);
		}
		return convertView;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}
}
