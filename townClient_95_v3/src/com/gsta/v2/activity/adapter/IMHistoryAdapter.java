package com.gsta.v2.activity.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.SmileyParser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class IMHistoryAdapter extends BaseAdapter {
    private Context context;
    private List<IMBean> messages;
    private SmileyParser parser;
    private Map<String, String> photoPath;
    private MsgListViewHolder holder;

    // 头像类使用
    private DisplayImageOptions optionsByNoPhoto;

    public IMHistoryAdapter(Context context, List<IMBean> messages) {
        this.context = context;
        this.messages = messages;
        this.photoPath = ((AgentApplication) context.getApplicationContext()).getPhotoPathMap();
        parser = SmileyParser.getInstance(context);
        optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
    }

    public int getCount() {
        if (messages != null) {
            return messages.size();
        }
        return 0;
    }

    public IMBean getItem(int position) {
        return this.messages.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View rowView, ViewGroup parent) {
        final IMBean item = getItem(position);
        if (rowView == null) {
            holder = new MsgListViewHolder();
            rowView = LayoutInflater.from(context).inflate(
                    R.layout.common_list_item, null);
            holder.username = (TextView) rowView.findViewById(R.id.tv_name);
            holder.count = (TextView) rowView.findViewById(R.id.tv_count);
            holder.time = (TextView) rowView.findViewById(R.id.tv_right_top);
            holder.lastmsg = (TextView) rowView
                    .findViewById(R.id.tv_left_bottom);
            holder.usericon = (ImageView) rowView
                    .findViewById(R.id.iv_usericon);
            rowView.setTag(holder);
        } else {
            holder = (MsgListViewHolder) rowView.getTag();
        }
        String photouri = photoPath.get(item.getHisUserId());
        if (photouri == null || photouri.length() == 0) {
            if (null == item.getHisPhotoPath() || item.getHisPhotoPath().trim().length() == 0) {
                holder.usericon.setImageResource(R.drawable.nav_head);
            } else {
                ImageUtil.getNetPicByUniversalImageLoad(null, holder.usericon, item.getHisPhotoPath(), optionsByNoPhoto);
            }
        } else {
            // ImageUtil.getNetPic(context, holder.usericon, photouri,R.drawable.nav_head);
            ImageUtil.getNetPicByUniversalImageLoad(null, holder.usericon, photouri, optionsByNoPhoto);
        }
        // IMBean msg = messages.get(position);
        if (item.getSended() == null
                || item.getSended().equals(FinalUtil.IM_SENDED)) {
            holder.lastmsg.setText(parser.addSmileySpans(item.getContent()));
        } else {
            holder.lastmsg.setText(parser.addSmileySpans(parser.s_warning
                    + item.getContent()));
        }
        holder.time.setText(item.getTime());
        holder.username.setText(item.getHisName() != null ? item.getHisName()
                : "其他好友");
        if (item.getFlag().equals("0")) {
            holder.count.setText("");
        } else {
            holder.count.setText(item.getFlag());
        }
        return rowView;
    }

    public List<IMBean> getMessages() {
        return messages;
    }

    public void setMessages(List<IMBean> messages) {
        this.messages = messages;
        this.notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        this.photoPath = ((AgentApplication) context.getApplicationContext()).getPhotoPathMap();
        super.notifyDataSetChanged();
    }

    class MsgListViewHolder {
        TextView username;
        TextView count;
        TextView time;
        TextView lastmsg;
        ImageView usericon;
    }

}
