package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.MyUserInfo;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.Util;

/**
 * 
 * @author wubo
 * @createtime 2012-9-5
 */
public class UserAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private OnClickListener noIMClickListener;
	private OnClickListener imClickListener;
	private List<MyUserInfo> mUserList;
	AgentApplication application;

	/**
	 * @return the mUserList
	 */
	public List<MyUserInfo> getmUserList() {
		return mUserList;
	}

	public void addContact(List<MyUserInfo> MyUserInfo) {
		if (this.mUserList != null) {
			this.mUserList.addAll(MyUserInfo);
		} else
			this.mUserList = MyUserInfo;

		this.notifyDataSetChanged();
	}

	/**
	 * @param mUserList
	 *            the mUserList to set
	 */
	public void setmUserList(List<MyUserInfo> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	public UserAdapter(Context context, List<MyUserInfo> users,
			OnClickListener noIMClickListener, OnClickListener imClickListener) {
		layoutInflater = LayoutInflater.from(context);
		this.noIMClickListener = noIMClickListener;
		this.imClickListener = imClickListener;
		this.mUserList = users;
		this.application = (AgentApplication) context.getApplicationContext();
	}

	public int getCount() {
		return mUserList.size();
	}

	public Object getItem(int position) {
		return mUserList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final MyUserInfo item = mUserList.get(position);

		if (null == convertView) {
		    convertView = layoutInflater.inflate(R.layout.users_item, null);
        }
		TextView tv_userName = (TextView) convertView
				.findViewById(R.id.tv_username);
		TextView tv_phone_num = (TextView) convertView
				.findViewById(R.id.tv_usersign);
		ImageView iv_into = (ImageView) convertView.findViewById(R.id.iv_into);

		if (application.getAgentInfo() != null
				&& application.getAgentInfo().getAgentIdentity() != null
				&& application.getAgentInfo().getAgentIdentity() == 11) {
			if (item.getRemark() != null && item.getRemark().length() > 0) {
				if (item.getUserName() == null) {
					tv_userName.setText(item.getRemark());
				} else {
					tv_userName.setText(item.getRemark() + "("
							+ item.getUserName() + ")");
				}
			} else {
				if (item.getUserName() == null) {
					tv_userName.setText("");
				} else {
					tv_userName.setText(item.getUserName());
				}
			}
		} else {
			if (item.getRemark() != null && item.getRemark().length() > 0) {
				if (item.getUserName() == null) {
					tv_userName.setText(item.getRemark());
				} else {
					tv_userName.setText(item.getRemark() + "("
							+ Util.hideName(item.getUserName()) + ")");
				}
			} else {
				if (item.getUserName() == null) {
					tv_userName.setText("");
				} else {
					tv_userName.setText(Util.hideName(item.getUserName()));
				}
			}
		}
		// 10：合约机11：智能机12：手机卡（移动卡)13：光宽带
		if (item.getType().equals("0")) {
			tv_phone_num.setText("合约机用户");
			iv_into.setOnClickListener(imClickListener);
		} else if (item.getType().equals("1")) {
			tv_phone_num.setText("裸机用户");
			iv_into.setOnClickListener(noIMClickListener);
		} else if (item.getType().equals("2")) {
			tv_phone_num.setText("手机卡用户");
			iv_into.setOnClickListener(noIMClickListener);
		} else if (item.getType().equals("3")) {
			tv_phone_num.setText("光宽带用户");
			iv_into.setOnClickListener(noIMClickListener);
		}
		iv_into.setTag(position);

		return convertView;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

}
