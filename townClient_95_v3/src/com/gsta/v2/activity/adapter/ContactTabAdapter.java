package com.gsta.v2.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;

public class ContactTabAdapter extends BaseAdapter {

    private Context context;
    private String[] tab;
    private int select = 1;
    View selectView;

    public ContactTabAdapter(Context context, String[] tab) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.tab = tab;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return tab.length;
    }

    public int getDefaultSelection() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return tab[arg0];
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public int getSelect() {
        return select;
    }

    public void setSelect(int select) {
        this.select = select;
        notifyDataSetChanged();

        if (selectView != null) {
            // TranslateAnimation animation = new TranslateAnimation(0, 200, 0, 0);
            // animation.setDuration(350);
            // animation.setFillAfter(true);
            // selectView.setAnimation(animation);
            // animation.startNow();
        }
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if (null == v) {
            v = LayoutInflater.from(context).inflate(R.layout.tab_view, null);
        }
        TextView tv_tap = (TextView) v.findViewById(R.id.tv_tap);
        tv_tap.setText(tab[position]);
        v.setBackgroundResource(R.drawable.check_false);
        if (position == select) {
            selectView = v;
            v.setBackgroundResource(R.drawable.check_true);
        }
        return v;
    }
}
