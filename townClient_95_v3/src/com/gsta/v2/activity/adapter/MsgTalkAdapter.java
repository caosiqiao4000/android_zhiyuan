package com.gsta.v2.activity.adapter;

import java.util.Date;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.SmileyParser;
import com.gsta.v2.util.Util;

public class MsgTalkAdapter extends BaseAdapter {
    public Context context;
    public List<IMBean> msgs = null;
    private SmileyParser parser;
    private MessageSingleViewHolder holder;
    // >>>>>>>>最后一次对话时间(如果时间超过2分钟，显示时间，如果不超过，不需要显示)
    private long lastSayTime = 0l;

    public MsgTalkAdapter(Context context, List<IMBean> msgs) {
        this.context = context;
        this.msgs = msgs;
        parser = SmileyParser.getInstance(context);
    }

    public int getCount() {
        return msgs.size();
    }

    public Object getItem(int position) {
        return msgs.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        IMBean msg = msgs.get(position);
        // >>>>>>>计算上次说话时间
        if (position == 0) {
            lastSayTime = 0l;
        } else {
            lastSayTime = Util.strToDate(msgs.get(position - 1).getTime()).getTime();
        }
        if (null == convertView) {
            holder = new MessageSingleViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.message_talk_view_to, null);
            holder.mycontent = (TextView) convertView
                    .findViewById(R.id.tv_msg_left);
            holder.time = (TextView) convertView.findViewById(R.id.timefrom);
            holder.warning = (ImageView) convertView.findViewById(R.id.warning);
            holder.ll_right = (LinearLayout) convertView.findViewById(R.id.ll_right);
            holder.tv_msg_right = (TextView) convertView.findViewById(R.id.tv_msg_right);
            // else if (msg.getDirection().equals("0")) {
            // convertView = LayoutInflater.from(context).inflate(
            // R.layout.message_talk_view_from, null);
            // holder.mycontent = (TextView) convertView
            // .findViewById(R.id.tv_msg_right);
            // holder.time = (TextView) convertView.findViewById(R.id.timeto);
            // holder.warning = (ImageView) convertView.findViewById(R.id.warning);
            // }
            convertView.setTag(holder);
        } else {
            holder = (MessageSingleViewHolder) convertView.getTag();
        }

        // 说话方向 0表示登陆者,1表示聊天对象
        if (msg.getDirection().equals(FinalUtil.SENDIM_DIRECTION_OTHER)) {// 聊天对象
            holder.ll_right.setVisibility(View.GONE);
            holder.mycontent.setVisibility(0);
            holder.mycontent.setText(parser.addSmileySpans(msg.getContent()));
        } else {
            holder.ll_right.setVisibility(0);
            holder.mycontent.setVisibility(View.GONE);
            holder.tv_msg_right.setText(parser.addSmileySpans(msg.getContent()));
            if (msg.getSended().equals(FinalUtil.IM_UNSEND)) {
                holder.warning.setVisibility(View.VISIBLE);
            } else {
                holder.warning.setVisibility(View.GONE);
            }
        }
        long a = Util.strToDate(msg.getTime()).getTime();
        // >>>>>>>>显示对话时间
        if (position == getCount() - 1) {
            Log.i("MsgTalkAdapter", a - lastSayTime + "");
        }
        showSpeakTime(a, holder.time);
        // holder.time.setText(msg.getTime());
        return convertView;
    }

    private void showSpeakTime(long speakTime, TextView mTimeTextView) {
        Date speakDate = new Date(speakTime);
        // >>>>>>>>时间分隔两分钟为时间分隔点
        if (speakTime - lastSayTime >= 1000 * 60) {
            mTimeTextView.setVisibility(View.VISIBLE);
            mTimeTextView.setText(Util.getDateDiff(speakDate));
        } else {
            mTimeTextView.setVisibility(View.GONE);
        }
        lastSayTime = speakTime;
    }

    public List<IMBean> getMsgs() {
        return msgs;
    }

    class MessageSingleViewHolder {
        TextView mycontent;
        TextView tv_msg_right;
        LinearLayout ll_right;
        TextView time;
        ImageView warning;
    }

    public void setMsgs(List<IMBean> msgs) {
        this.msgs = msgs;
        this.notifyDataSetChanged();
    }
}
