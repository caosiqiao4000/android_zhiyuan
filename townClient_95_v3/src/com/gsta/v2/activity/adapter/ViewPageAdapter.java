package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.gsta.v2.activity.R;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 
 * @author wubo
 * @createtime 2012-9-5
 */
public class ViewPageAdapter extends PagerAdapter {
    private List<String> uris; // 图片地址
    private LayoutInflater layoutInflater;
    private OnClickListener imageClick;
    private DisplayImageOptions options;

    public ViewPageAdapter(Context context, List<String> uris, OnClickListener imageClick) {
        this.uris = uris;
        layoutInflater = LayoutInflater.from(context);
        this.imageClick = imageClick;

        options = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
    }

    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }

    @Override
    public void finishUpdate(View arg0) {
    }

    @Override
    public int getCount() {
        return uris.size();
    }

    @Override
    public Object instantiateItem(View arg0, int arg1) {
        String item = uris.get(arg1);
        View imageLayout = layoutInflater.inflate(R.layout.item_pager_image, (ViewGroup) arg0, false);
        ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
        final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);

        if (imageClick != null) {
            imageView.setOnClickListener(imageClick);
        } else {
            imageView.setOnClickListener(null);
        }
        //
        // ImageUtil.getNetPic(context, spinner, imageView, Util.converPicPath(item, FinalUtil.PIC_MIDDLE), R.drawable.none_pic);
        ImageUtil.getNetPicByUniversalImageLoad( spinner, imageView, Util.converPicPath(item, FinalUtil.PIC_MIDDLE), options);

        ((ViewGroup) arg0).addView(imageLayout);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0.equals(arg1);
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void startUpdate(View arg0) {
    }

}
