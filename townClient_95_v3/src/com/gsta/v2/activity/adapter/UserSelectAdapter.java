package com.gsta.v2.activity.adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.MyUserInfo;

/**
 * 
 * @author zhengjy
 * 
 */
public class UserSelectAdapter extends BaseAdapter {
	private Context context;
	private List<MyUserInfo> allMyUserInfo;
	private List<Boolean> mchecked;

	public UserSelectAdapter(Context context, List<MyUserInfo> allMyUserInfo) {
		super();
		this.context = context;
		this.allMyUserInfo = allMyUserInfo;
		mchecked = new ArrayList<Boolean>();
		for (int i = 0; i < allMyUserInfo.size(); i++) {
			mchecked.add(false);
		}
	}

	public void setUserInfo(List<MyUserInfo> allMyUserInfo) {
		this.allMyUserInfo = allMyUserInfo;
		mchecked = new ArrayList<Boolean>();
		for (int i = 0; i < allMyUserInfo.size(); i++) {
			mchecked.add(false);
		}
		this.notifyDataSetChanged();
	}

	public void addUserInfo(List<MyUserInfo> allMyUserInfo) {
		if (this.allMyUserInfo == null) {
			allMyUserInfo = new ArrayList<MyUserInfo>();
		}
		this.allMyUserInfo.addAll(allMyUserInfo);
		for (int i = 0; i < allMyUserInfo.size(); i++) {
			mchecked.add(false);
		}
		this.notifyDataSetChanged();
	}

	public void selectByIndex(int index, boolean flag) {
		mchecked.set(index, flag);
	}

	public int getSelectCount() {
		int count = 0;
		Iterator<Boolean> iter = mchecked.iterator();
		while (iter.hasNext()) {
			if (iter.next()) {
				count++;
			}
		}
		return count;
	}

	public List<MyUserInfo> getSelectMember() {
		List<MyUserInfo> MyUserInfo = new ArrayList<MyUserInfo>();
		for (int i = 0; i < allMyUserInfo.size(); i++) {
			if (mchecked.get(i)) {
				MyUserInfo.add(allMyUserInfo.get(i));
			}
		}
		return MyUserInfo;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return allMyUserInfo.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return allMyUserInfo.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MyUserInfo item = (MyUserInfo) getItem(position);
		ViewHolder holder = new ViewHolder();
		if (null == convertView) {
		    convertView = LayoutInflater.from(context).inflate(
		            R.layout.contact_select_item, null);
        }
		holder.imgIdentity = (ImageView) convertView
				.findViewById(R.id.iv_identity);
		holder.selected = (CheckBox) convertView
				.findViewById(R.id.mtrace_checkbox);
		// holder.txtHint = (TextView)
		// convertView.findViewById(R.id.tv_first_char_hint);
		holder.txtIdentity = (TextView) convertView
				.findViewById(R.id.tv_identity);
		holder.txtName = (TextView) convertView.findViewById(R.id.tv_username);
		holder.txtPhone = (TextView) convertView.findViewById(R.id.tv_usersign);
		holder.userIcon = (ImageView) convertView
				.findViewById(R.id.iv_usericon);
		convertView.setTag(holder);

		if (mchecked.get(position)) {
			holder.selected.setChecked(true);
		} else {
			holder.selected.setChecked(false);
		}
		// 10：合约机11：智能机12：手机卡（移动卡)13：光宽带
		if (item.getType().equals("10")) {
			holder.txtPhone.setText("合约机用户");
		} else if (item.getType().equals("11")) {
			holder.txtPhone.setText("智能机用户");
		} else if (item.getType().equals("12")) {
			holder.txtPhone.setText("移动卡用户");
		} else if (item.getType().equals("13")) {
			holder.txtPhone.setText("光宽带用户");
		}
		if (item.getRemark() != null && item.getRemark().length() > 0) {
			if (item.getUserName() == null) {
				holder.txtName.setText(item.getRemark());
			} else {
				holder.txtName.setText(item.getRemark() + "("
						+ item.getUserName() + ")");
			}
		} else {
			if (item.getUserName() == null) {
				holder.txtName.setText("");
			} else {
				holder.txtName.setText(item.getUserName());
			}
		}
		/*
		 * if (!item.getFirstLetter().equals(MyUserInfo.NO_LETTER)) {
		 * holder.txtHint.setVisibility(View.VISIBLE);
		 * holder.txtHint.setText(item.getFirstLetter()); } else { //
		 * 此段代码不可缺：实例化一个CurrentView后，会被多次赋值并且只有最后一次赋值的position是正确
		 * holder.txtHint.setVisibility(View.GONE); }
		 */

		return convertView;
	}

	public class ViewHolder {
		public CheckBox selected; // 是否选择一组
		public TextView txtName; // 好友名称
		public TextView txtPhone; // 通讯录电话
		public TextView txtHint;// 
		public TextView txtIdentity;// 
		public ImageView userIcon; // 用户头像
		public ImageView imgIdentity; //  
	}
}
