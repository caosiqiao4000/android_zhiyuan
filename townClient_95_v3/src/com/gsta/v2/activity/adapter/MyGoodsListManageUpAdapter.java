package com.gsta.v2.activity.adapter;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.myshop.GoodsDetailActivity;
import com.gsta.v2.activity.myshop.MyGoodsActivity;
import com.gsta.v2.activity.myshop.MyGoodsManageActivity;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class MyGoodsListManageUpAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private Context context;
    private List<GoodsInfo> mList;
    private AgentGoodsHolder holder;
    private MyDialog activateVoucherDialog;
    
	// 商品图片使用
	private DisplayImageOptions optionsByNonePic;
	// 头像类使用
	@SuppressWarnings("unused")
	private DisplayImageOptions optionsByNoPhoto;

    public MyGoodsListManageUpAdapter(Context context, List<GoodsInfo> mList) {
        layoutInflater = LayoutInflater.from(context);
        
		 optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
	     optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
        
        this.context = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public GoodsInfo getItem(int position) {
        return mList.get(position);
    }

    @Override
    public boolean areAllItemsEnabled() {
        return super.areAllItemsEnabled();
    }

    @Override
    public boolean isEnabled(int position) {
        return super.isEnabled(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GoodsInfo item = getItem(position);
        if (null == convertView) {
            holder = new AgentGoodsHolder();
            convertView = layoutInflater.inflate(R.layout.list_item_updown_goods, null);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tvDetail = (TextView) convertView.findViewById(R.id.tv_detail);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.tv_bottom_left);
            // holder.tv_newGoods_des = (TextView) convertView.findViewById(R.id.tv_newGoods_des);
            holder.btn_goods_up_down = (Button) convertView.findViewById(R.id.btn_goods_up_down);
            holder.pro_bar = (ProgressBar) convertView.findViewById(R.id.iv_pro_bar);
            holder.iv_icon = (ImageView) convertView.findViewById(R.id.iv_icon);
            holder.ll_text_midd = (RelativeLayout) convertView.findViewById(R.id.ll_text_midd);
            //
            holder.btn_goods_up_down.setOnClickListener(btn_down_click);
            holder.ll_text_midd.setOnClickListener(btn_down_click);
            holder.iv_icon.setOnClickListener(btn_down_click);
            convertView.setTag(holder);
        } else {
            holder = (AgentGoodsHolder) convertView.getTag();
        }

//        ImageUtil.getNetPic(context, holder.pro_bar, holder.iv_icon, item.getMediaPath(), R.drawable.none_pic);
        
        ImageUtil.getNetPicByUniversalImageLoad( holder.pro_bar, holder.iv_icon, Util.converPicPath(item.getMediaPath(),FinalUtil.PIC_MIDDLE), optionsByNonePic);
        
        holder.btn_goods_up_down.setText("上 架");
        //
        holder.btn_goods_up_down.setTag(position);
        holder.ll_text_midd.setTag(position);
        holder.iv_icon.setTag(position);
        holder.tvTitle.setText(item.getProdName());
        holder.tvDetail.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tvDetail.setText("原价:￥" + item.getMinRetailPrice());
        holder.tvPrice.setText("售价:￥" + item.getMinSalePrice());
        return convertView;
    }

    OnClickListener btn_down_click = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final int position = (Integer) v.getTag();
            final GoodsInfo item = getItem(position);
            if (v.getId() == holder.btn_goods_up_down.getId()) {
                String title = item.getProdName();
                String contentText = "确定要将该商品 上架 吗?";
                activateVoucherDialog = MyDialog.showDialog(context, title, contentText, R.string.submit,
                        R.string.cancel, false,
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // 发送下架 请求
                                MyGoodsManageActivity activity = (MyGoodsManageActivity) context;
                                AgentApplication application = (AgentApplication) activity.getApplication();
                                ServerSupportManager serverMana = new ServerSupportManager(context, activity);
                                List<Parameter> paras = new ArrayList<Parameter>();
                                paras.add(new Parameter("uid", application.getUid()));
                                paras.add(new Parameter("productId", item.getSpeciesId()));
                                // 0:上架，1:下架
                                paras.add(new Parameter("opType", "0"));
                                serverMana.supportRequest(AgentConfig.AddToMyMall(), paras, true,
                                        "加载中,请稍等 ...", MyGoodsActivity.upGoods_code);
                                activity.sendAdapterViewPosition(position);
                                cancelDialog();
                            }
                        });
            } else if (v.getId() == holder.iv_icon.getId() || v.getId() == holder.ll_text_midd.getId()) {
                // 查看详情
                Intent intent = new Intent(context,
                        GoodsDetailActivity.class);
                intent.putExtra(FinalUtil.GOODSINFO, item);
                context.startActivity(intent);
            }
        }
    };

    private void cancelDialog() {
        if (null != activateVoucherDialog) {
            MyDialog.dismiss(activateVoucherDialog);
            activateVoucherDialog = null;
        }
    }

    final class AgentGoodsHolder {
        TextView tvDetail;
        TextView tvPrice;
        TextView tvTitle;
        RelativeLayout ll_text_midd;
        // TextView tv_newGoods_des;
        ImageView iv_icon;
        ProgressBar pro_bar;
        Button btn_goods_up_down;
    }
}
