
package com.gsta.v2.activity.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;


public class TongjiAdapterforimage extends BaseAdapter {
	private Context context;
	private ArrayList<HashMap<String, Object>> item;

	public TongjiAdapterforimage(Context context,
			ArrayList<HashMap<String, Object>> item) {
		this.context = context;
		this.item = item;
	}

	@Override
	public int getCount() {
		return item.size();
	}

	@Override
	public Object getItem(int position) {
		return item.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Datalist data = new Datalist();
		convertView = LayoutInflater.from(context).inflate(R.layout.tongji_item,
				null);
		data.mimage = (ImageView) convertView.findViewById(R.id.tongji_showitum_image);
		data.mnametextv = (TextView) convertView
				.findViewById(R.id.tongji_showitem_appname);
		data.mtotaltextv = (TextView) convertView
				.findViewById(R.id.tongji_showitem_totaldata);

		data.mimage.setImageDrawable((Drawable)item.get(position).get("appsimage"));
		data.mnametextv.setText(item.get(position).get("appsname").toString());
		data.mtotaltextv.setText(item.get(position).get("totaldata").toString());

		return convertView;

	}

	private class Datalist {
		public ImageView mimage;
		public TextView mnametextv, mtotaltextv;

	}
}
