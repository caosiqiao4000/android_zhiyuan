package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 
 * @author longxianwen
 * @createTime Apr 10, 2013 2:54:12 PM
 * @version: 1.0
 * @desc:商品收藏数据适配器
 */
public class GoodsEnshrineAdapter extends BaseAdapter {

    private List<GoodsInfo> gdInfos;
    private LayoutInflater flater;
    private AgentGoodsHolder holder;
    private DisplayImageOptions options;

    public GoodsEnshrineAdapter(Context context, List<GoodsInfo> gdInfos) {
        flater = LayoutInflater.from(context);
        this.gdInfos = gdInfos;
        options = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
    }

    @Override
    public int getCount() {
        return gdInfos.size();
    }

    @Override
    public GoodsInfo getItem(int position) {
        return gdInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        GoodsInfo goodsData = gdInfos.get(position);
        if (rowView == null) {
            holder = new AgentGoodsHolder();

            rowView = flater.inflate(
                    R.layout.gslist_item, null);
            holder.iv_icon = (ImageView) rowView.findViewById(R.id.iv_icon); // 店铺icon
            holder.tv_goodsdesc = (TextView) rowView.findViewById(R.id.tv_goodsdesc); // 介绍
            holder.tv_goodsprice = (TextView) rowView.findViewById(R.id.tv_goodsprice); // 价格
            holder.tv_collectnum = (TextView) rowView.findViewById(R.id.tv_collectnum); // 收藏人气
            holder.iv_pro_bar = (ProgressBar) rowView.findViewById(R.id.iv_pro_bar);
            rowView.setTag(holder);
        } else {
            holder = (AgentGoodsHolder) rowView.getTag();
        }
        if (goodsData.getMediaPath() != null && goodsData.getMediaPath().length() > 1) {
            // ImageUtil.getNetPic(context, holder.iv_pro_bar, holder.iv_icon,
            // Util.converPicPath(goodsData.getMediaPath(), FinalUtil.PIC_MIDDLE), R.drawable.none_pic);
            ImageUtil.getNetPicByUniversalImageLoad(holder.iv_pro_bar, holder.iv_icon,
                    Util.converPicPath(goodsData.getMediaPath(), FinalUtil.PIC_MIDDLE), options);
        } else {
            holder.iv_icon.setImageResource(R.drawable.none_pic);
        }

        // tv_goodsdesc.setText(goodsData.getProdName() + " " + goodsData.getDescription());

        holder.tv_goodsdesc.setText(
                Html.fromHtml(
                        "<b>" + (null == goodsData.getProdName() ? "" : goodsData.getProdName()) + "</b> " +
                                "<font color=#FF0000 >" + (null == goodsData.getDescription() ? "" : goodsData.getDescription()) + "</font> "));

        holder.tv_goodsprice.setText("￥" + goodsData.getMinSalePrice() + "");
        holder.tv_collectnum.setText(goodsData.getPopularity() + "");
        return rowView;
    }

    public void setGdInfos(List<GoodsInfo> gdInfos) {
        this.gdInfos = gdInfos;
        this.notifyDataSetChanged();
    }

    final class AgentGoodsHolder {
        TextView tv_collectnum;
        TextView tv_goodsdesc;
        TextView tv_goodsprice;
        ImageView iv_icon;
        ProgressBar iv_pro_bar;
    }
}
