package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.MyUserInfo;
import com.gsta.v2.util.ImageUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 
 * @author longxianwen
 * @createTime Apr 2, 2013 3:26:17 PM
 * @version: 1.0
 * @desc:
 */
public class ShopFansAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private OnClickListener mClickListener;
    private List<MyUserInfo> mUserList;
    
	// 商品图片使用
	@SuppressWarnings("unused")
	private DisplayImageOptions optionsByNonePic;
	// 头像类使用
	private DisplayImageOptions optionsByNoPhoto;

    /**
     * @return the mUserList
     */
    public List<MyUserInfo> getmUserList() {
        return mUserList;
    }

    /**
     * @param mUserList
     *            the mUserList to set
     */
    public void setmUserList(List<MyUserInfo> mUserList) {
        this.mUserList = mUserList;
        this.notifyDataSetChanged();
    }

    public ShopFansAdapter(Context context, List<MyUserInfo> users,
            OnClickListener mClickListener) {
        layoutInflater = LayoutInflater.from(context);
        this.mClickListener = mClickListener;
        this.mUserList = users;
        
        optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
	     optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
    }

    public int getCount() {
        return mUserList.size();
    }

    public Object getItem(int position) {
        return mUserList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final MyUserInfo fansData = mUserList.get(position);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.contact_item, null);

        }

        ImageView iv_usericon = (ImageView) convertView.findViewById(R.id.iv_usericon);
        TextView tv_userName = (TextView) convertView.findViewById(R.id.tv_username);
        TextView tv_usersign = (TextView) convertView.findViewById(R.id.tv_usersign);
        ImageView iv_into = (ImageView) convertView.findViewById(R.id.iv_into);

        // iv_usericon.setImageResource(R.drawable.icon);
        if (fansData.getPhoto() != null && fansData.getPhoto().length() > 1) {
//            ImageUtil.getNetPic(context, null, iv_usericon,
//                    fansData.getPhoto(), R.drawable.nav_head);
            ImageUtil.getNetPicByUniversalImageLoad(null, iv_usericon, fansData.getPhoto(), optionsByNoPhoto);
        } else {
            iv_usericon.setImageResource(R.drawable.nav_head);
        }

        tv_userName.setText(fansData.getNickName());
        tv_usersign.setText(fansData.getMobile());

        iv_into.setOnClickListener(mClickListener);
        iv_into.setTag(position);
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

}
