package com.gsta.v2.activity.adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.Contacts;

/**
 * 
 * @author zhengjy
 * 
 */
public class ContactSelectAdapter extends BaseAdapter {
    private Context context;
    private List<Contacts> allcontacts;
    private List<Boolean> mchecked; // 是否选择了
    private boolean ifCheck = true;
    private ViewHolder holder;

    public ContactSelectAdapter(Context context, List<Contacts> allcontacts) {
        super();
        this.context = context;
        this.allcontacts = allcontacts;
        mchecked = new ArrayList<Boolean>();
        for (int i = 0; i < allcontacts.size(); i++) {
            mchecked.add(false);
        }
    }

    public void selectByIndex(int index, boolean flag) {
        mchecked.set(index, flag);
    }

    public int getSelectCount() {
        int count = 0;
        Iterator<Boolean> iter = mchecked.iterator();
        while (iter.hasNext()) {
            if (iter.next()) {
                count++;
            }
        }
        return count;
    }

    public List<Contacts> getSelectMember() {
        List<Contacts> contacts = new ArrayList<Contacts>();
        for (int i = 0; i < allcontacts.size(); i++) {
            if (mchecked.get(i)) {
                contacts.add(allcontacts.get(i));
            }
        }
        return contacts;
    }

    @Override
    public int getCount() {
        return allcontacts.size();
    }

    @Override
    public Object getItem(int position) {
        return allcontacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contacts item = (Contacts) getItem(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.contact_select_item, null);
            holder.imgIdentity = (ImageView) convertView
                    .findViewById(R.id.iv_identity);
            holder.selected = (CheckBox) convertView
                    .findViewById(R.id.mtrace_checkbox);
            // holder.txtHint = (TextView)
            // convertView.findViewById(R.id.tv_first_char_hint);
            holder.txtIdentity = (TextView) convertView
                    .findViewById(R.id.tv_identity);
            holder.txtName = (TextView) convertView
                    .findViewById(R.id.tv_username);
            holder.txtPhone = (TextView) convertView
                    .findViewById(R.id.tv_usersign);
            holder.userIcon = (ImageView) convertView
                    .findViewById(R.id.iv_usericon);
            convertView.setTag(holder);

            /*
             * if (!item.getFirstLetter().equals(Contacts.NO_LETTER)) { holder.txtHint.setVisibility(View.VISIBLE);
             * holder.txtHint.setText(item.getFirstLetter()); } else { // 此段代码不可缺：实例化一个CurrentView后，会被多次赋值并且只有最后一次赋值的position是正确
             * holder.txtHint.setVisibility(View.GONE); }
             */
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (ifCheck) {
            holder.selected.setVisibility(View.VISIBLE);
            holder.selected.setChecked(mchecked.get(position));
        } else {
            holder.selected.setVisibility(View.GONE);
        }
        holder.txtName.setText(item.getUserName());
        holder.txtPhone.setText(item.getPhoneNum());
        return convertView;
    }

    public class ViewHolder {
        public CheckBox selected; // 是否选择一组
        public TextView txtName; // 好友名称
        public TextView txtPhone; // 通讯录电话
        public TextView txtHint;//
        public TextView txtIdentity;//
        public ImageView userIcon; // 用户头像
        public ImageView imgIdentity; //
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
