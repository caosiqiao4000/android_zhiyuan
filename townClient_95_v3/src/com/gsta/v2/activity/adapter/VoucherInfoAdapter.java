package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.response.CouponInfo;

/**
 * 
 * @author wubo
 * @createtime 2012-9-5
 */
public class VoucherInfoAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<CouponInfo> items;
    private ViewHolder viewHolder;

    public VoucherInfoAdapter(Context context,
            List<CouponInfo> users) {
        layoutInflater = LayoutInflater.from(context);
        this.items = users;
    }

    /**
     * @param Items
     *            the Items to set
     */
    public void setItems(List<CouponInfo> Items) {
        this.items = Items;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CouponInfo item = items.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.ada_voucher_info_item, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.tv_1 = (TextView) convertView.findViewById(R.id.tv_1);
            viewHolder.tv_2 = (TextView) convertView.findViewById(R.id.tv_2);
            viewHolder.tv_3 = (TextView) convertView.findViewById(R.id.tv_3);
            viewHolder.tv_4 = (TextView) convertView.findViewById(R.id.tv_4);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.name.setText(String.valueOf(item.getId()));
        viewHolder.tv_1.setText(item.getExplanation());
        viewHolder.tv_2.setText(String.valueOf(item.getMoney()));
        viewHolder.tv_3.setText(item.getEnd_date());
        viewHolder.tv_4.setText(item.getRequirement());
        return convertView;
    }

    /**
     * 每个应用显示的内容，包括图标和名称
     * 
     * @author Yao.GUET
     * 
     */
    final class ViewHolder {
        TextView name;
        TextView tv_1;
        TextView tv_2;
        TextView tv_3;
        TextView tv_4;
    }

}
