package com.gsta.v2.activity.adapter;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.AgentWebViewActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.myshop.ShopHomePageActivity;
import com.gsta.v2.activity.mytown.MyUserIndentListActivity;
import com.gsta.v2.activity.mytown.MyUserIndentQueryActivity;
import com.gsta.v2.entity_v2.OrderInfo;
import com.gsta.v2.entity_v2.OrderWareInfo;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * @author caosq 2013-4-10 用户
 */
public class UserIndentListQueryAdapter extends BaseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(IndentAgentListQueryAdapter.class);
    private LayoutInflater layoutInflater;
    private List<OrderInfo> items;
    private Context context;
    private int query_code;// 查询类型
    private ViewIndentHolder holder;
    public static final String LOGISTICS_INFO = "logisticsInfo";
    private MyDialog activateVoucherDialog;
    // 商品图片使用
    private DisplayImageOptions optionsByNonePic;
    // 头像类使用
    private DisplayImageOptions optionsByNoPhoto;

    /**
     * @return the mUserList
     */
    public List<OrderInfo> getItems() {
        return items;
    }

    public UserIndentListQueryAdapter(Context context, List<OrderInfo> users, int query_code) {
        this.context = context;
        this.query_code = query_code;
        layoutInflater = LayoutInflater.from(context);
        this.items = users;
        optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
        optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public void notifyDataSetChanged() {
        cancelDialog();
        super.notifyDataSetChanged();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final OrderInfo item = items.get(position);
        if (convertView == null) {
            holder = new ViewIndentHolder();
            convertView = layoutInflater
                    .inflate(R.layout.indent_list_query_item, null);
            holder.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
            holder.imageView2 = (ImageView) convertView.findViewById(R.id.imageView2);
            holder.iv_pro_bar1 = (ProgressBar) convertView.findViewById(R.id.iv_pro_bar1);
            holder.iv_pro_bar2 = (ProgressBar) convertView.findViewById(R.id.iv_pro_bar2);
            holder.tv_title_name = (TextView) convertView.findViewById(R.id.tv_title_name);
            holder.tv_clickshop = (TextView) convertView.findViewById(R.id.tv_clickshop);
            holder.tv_name_two = (TextView) convertView.findViewById(R.id.tv_name_two);
            holder.tv_money = (TextView) convertView.findViewById(R.id.tv_money);
            holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            holder.btn_left = (Button) convertView.findViewById(R.id.btn_left);
            holder.btn_right = (Button) convertView.findViewById(R.id.btn_right);
            holder.ll_title = (LinearLayout) convertView.findViewById(R.id.ll_title);
            holder.ll_detail = (LinearLayout) convertView.findViewById(R.id.ll_detail);
            convertView.setTag(holder);
        } else {
            holder = (ViewIndentHolder) convertView.getTag();
        }
        if (null != item.getOrderWareInfos()) {
            final OrderWareInfo info = item.getOrderWareInfos().get(0);
            // 商品图片
            // ImageUtil.getNetPic(context, holder.iv_pro_bar2, holder.imageView2, Util.converPicPath(info.getMediaPath(), FinalUtil.PIC_MIDDLE),
            // R.drawable.none_pic);
            ImageUtil.getNetPicByUniversalImageLoad( holder.iv_pro_bar2, holder.imageView2, Util.converPicPath(info.getMediaPath(),
                    FinalUtil.PIC_MIDDLE), optionsByNonePic);

            holder.imageView2.setTag(position);
            holder.tv_name_two.setText(Html.fromHtml(
                    "<b>" + (null == info.getWareName() ? "" : info.getWareName()) + "</b> " +
                            "<font color=#FF0000 >" + (null == info.getDescription() ? "" : info.getDescription()) + "</font> "));
            holder.imageView2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int a = Integer.valueOf(v.getTag().toString());
                    final OrderInfo info = (OrderInfo) getItem(a);
                    OrderWareInfo ware = info.getOrderWareInfos().get(0);
                    // 跳转到商品详情
                    if (null != ware.getSpecificationId() || null != ware.getSpeciesId()) {
                        AgentApplication application = (AgentApplication) context.getApplicationContext();
                        List<Parameter> parameters = new ArrayList<Parameter>();
                        if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
                            parameters.add(new Parameter("account", application.getAccount()));
                            parameters.add(new Parameter("loginUid", application.getUid()));
                        } else {
                            parameters.add(new Parameter("account", ""));
                            parameters.add(new Parameter("loginUid", ""));
                        }
                        parameters.add(new Parameter(FinalUtil.AGENT_UID, ""));
                        parameters.add(new Parameter("specificationId", ware.getSpecificationId()));
                        parameters.add(new Parameter("speciesId", ware.getSpeciesId()));
                        Util.toWebViewActivity(context, AgentWebViewActivity.class, AgentConfig.getShopAdd(), parameters);

                        // Util.toWebViewActivity(context, AgentWebViewActivity.class, AgentConfig.getShopAdd(), ware.getSpecificationId(),
                        // ware.getSpeciesId());
                    } else {
                        logger.warn("跳转到商品详情 的参数为空 position = " + a);
                    }
                }
            });
        } else {
            logger.error(" 商品详细信息 OrderWareInfo is null position = " + position);
        }
        // 店主头像
        // ImageUtil.getNetPic(context, holder.iv_pro_bar1, holder.imageView1, item.getPhoto(), R.drawable.nav_head);

        ImageUtil.getNetPicByUniversalImageLoad(holder.iv_pro_bar1, holder.imageView1, Util.converPicPath(item.getPhoto(),
                FinalUtil.PIC_MIDDLE), optionsByNoPhoto);

        holder.tv_title_name.setText(null == item.getShopName() ? "商城总店" : item.getShopName());
        if (null == item.getShopName()) {
            holder.tv_clickshop.setVisibility(View.GONE);
        }
        holder.tv_money.setText("￥" + item.getOrderPrice());
        holder.ll_title.setTag(position);
        holder.ll_detail.setTag(position);
        holder.tv_time.setText(item.getOrderDate());
        if (null == item.getAgentUid() || null == item.getShopName()) {
            holder.ll_title.setEnabled(false);
        }
        holder.ll_title.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final int a = Integer.valueOf(v.getTag().toString());
                final OrderInfo info = (OrderInfo) getItem(a);
                @SuppressWarnings("unused")
                OrderWareInfo ware = info.getOrderWareInfos().get(0);
                if (null != item.getAgentUid() && null != item.getShopName()) {
                    Intent intent = new Intent();
                    // 跳转到相应店铺
                    intent.setClass(context, ShopHomePageActivity.class);
                    intent.putExtra(ShopHomePageActivity.IFMYSELF_SHOP_FLAG, item.getAgentUid());
                    intent.putExtra(ShopHomePageActivity.MYSELF_SHOPNAME_FLAG, item.getShopName());
                    context.startActivity(intent);
                } else {
                    // intent.setClass(context, MainActivity.class);
                    // intent.putExtra(FinalUtil.TABINDEX, 1);
                    logger.warn("跳转到相应店铺 getAgentUid 或者名称 is null ");
                }
            }
        });

        holder.ll_detail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 查看商品详细
                final int a = Integer.valueOf(v.getTag().toString());
                final OrderInfo info = (OrderInfo) getItem(a);
                List<Parameter> parameters = new ArrayList<Parameter>();
                parameters.add(new Parameter("orderId", info.getOrderId()));
                parameters.add(new Parameter("account", ((AgentApplication) context.getApplicationContext()).getAccount()));
                // parameters.add(new Parameter("systime", String.valueOf(new Date().getTime())));
                parameters.add(new Parameter("loginUid", ((AgentApplication) context.getApplicationContext()).getUid()));
                Util.toWebViewActivity(context, AgentWebViewActivity.class, AgentConfig.getOrderDetail(), parameters);
            }
        });

        /**
         * <p>
         * 1.待确认收货,2.待付款,3.待发货,4.已成功,5.已取消, opType =1时必填
         */
        if (query_code == MyUserIndentQueryActivity.QUERY_UNCONFIRMED) {// 4
            setBtnText("查看物流", "确认收货", position);
        } else if (query_code == MyUserIndentQueryActivity.QUERY_UNPAYMENT) {// 1
            // 支付方式1-在线支付，2-货到付款
            if (null != item.getPayMethod() && item.getPayMethod().equals("2")) {
                holder.btn_left.setTag(position);
                holder.btn_left.setText("取消订单");
                holder.btn_right.setVisibility(View.GONE);
                holder.btn_left.setOnClickListener(leftClickListener);
            } else {
                setBtnText("取消订单", "立即支付", position);
            }
        } else if (query_code == MyUserIndentQueryActivity.QUERY_UNDELIVERY || query_code == MyUserIndentQueryActivity.QUERY_UNCHECK) {// 2
            holder.btn_left.setTag(position);
            holder.btn_left.setText("申请退款");
            holder.btn_right.setVisibility(View.GONE);
            holder.btn_left.setOnClickListener(leftClickListener);
        } else if (query_code == MyUserIndentQueryActivity.QUERY_SUCCESS || query_code == MyUserIndentQueryActivity.QUERY_CANCEL) {
            convertView.findViewById(R.id.tv_line).setVisibility(View.GONE);
            convertView.findViewById(R.id.ll_btn).setVisibility(View.GONE);
        }
        return convertView;
    }

    private void setBtnText(String string, String string2, int position) {
        holder.btn_left.setText(string);
        holder.btn_right.setText(string2);
        holder.btn_left.setTag(position);
        holder.btn_left.setOnClickListener(leftClickListener);
        if (holder.btn_right.getVisibility() != 0) {
            holder.btn_right.setVisibility(0);
        }
        holder.btn_right.setTag(position);
        holder.btn_right.setOnClickListener(leftClickListener);
    }

    OnClickListener leftClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final MyUserIndentListActivity activity = (MyUserIndentListActivity) context;
            /**
             * <p>
             * //4.待确认收货,1.待付款,2.待发货,5.已成功,6.已取消, opType =1时必填
             */
            if (query_code == MyUserIndentQueryActivity.QUERY_UNCONFIRMED) {
                // setBtnText("查看物流", "确认收货");
                if (v.getId() == holder.btn_left.getId()) {
                    // Intent intent = new Intent();
                    // intent.setClass(context, OrderLogisticsInfoActivity.class);//
                    // ArrayList<OrderLogisticsInfo> infos =((ArrayList<OrderLogisticsInfo>) items.get( //
                    // Integer.valueOf(String.valueOf(holder.btn_left.getTag()))).getOrderLogisticsInfos();
                    // intent.putExtra(LOGISTICS_INFO, infos);
                    // context.startActivity(intent);
                } else if (v.getId() == holder.btn_right.getId()) {
                    // 先提示 再上传确认收货信息
                    final int a = Integer.valueOf(v.getTag().toString());
                    final OrderInfo info = (OrderInfo) getItem(a);
                    activateVoucherDialog = MyDialog.showDialog(context, "确认收货申请", "请确保收到货后再确认收货，以免钱财两空!", R.string.submit,
                            R.string.cancel, false,
                            new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (null != info.getOrderId()) {// 确认收货信息
                                        ServerSupportManager serverMana = new ServerSupportManager(activity, activity);
                                        List<Parameter> paras = new ArrayList<Parameter>();
                                        paras.add(new Parameter("uid", ((AgentApplication) context.getApplicationContext()).getUid()));
                                        paras.add(new Parameter("orderId", info.getOrderId()));
                                        serverMana.supportRequest(AgentConfig.myConfirmGoods(), paras, true,
                                                "提交中,请稍等 ...", MyUserIndentListActivity.btn_confirmGoods);
                                        activity.sendAdapterViewPosition(a);
                                    } else {
                                        logger.warn("确认收货申请  getOrderId is null ");
                                    }
                                    cancelDialog();
                                }
                            });
                }
            } else if (query_code == MyUserIndentQueryActivity.QUERY_UNPAYMENT) {
                final int a = Integer.valueOf(v.getTag().toString());
                final OrderInfo info = (OrderInfo) getItem(a);
                if (v.getId() == holder.btn_left.getId()) {// 取消订单
                    activateVoucherDialog = MyDialog.showDialog(context, "取消订单", "确定要取消订单吗?", R.string.submit,
                            R.string.cancel, false,
                            new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (null != info.getOrderId()) {// 取消订单
                                        ServerSupportManager serverMana = new ServerSupportManager(activity, activity);
                                        List<Parameter> paras = new ArrayList<Parameter>();
                                        paras.add(new Parameter("uid", ((AgentApplication) context.getApplicationContext()).getUid()));
                                        paras.add(new Parameter("orderId", info.getOrderId()));
                                        serverMana.supportRequest(AgentConfig.myCancelOrder(), paras, true,
                                                "提交中,请稍等 ...", MyUserIndentListActivity.btn_cancleIndent);
                                        activity.sendAdapterViewPosition(a);
                                    } else {
                                        logger.warn("取消订单申请  getOrderId is null ");
                                    }
                                    cancelDialog();
                                }
                            });
                } else if (v.getId() == holder.btn_right.getId()) {// 立即支付
                    @SuppressWarnings("unused")
                    final OrderWareInfo wareInfo = info.getOrderWareInfos().get(0);
                    List<Parameter> parameters = new ArrayList<Parameter>();
                    parameters.add(new Parameter("transactionId", info.getTransactionId()));
                    parameters.add(new Parameter("account", ((AgentApplication) context.getApplicationContext()).getAccount()));
                    // parameters.add(new Parameter("systime", String.valueOf(new Date().getTime())));
                    parameters.add(new Parameter("loginUid", ((AgentApplication) context.getApplicationContext()).getUid()));
                    Util.toWebViewActivity(activity, AgentWebViewActivity.class, AgentConfig.getOrderPayment(), parameters);
                }
            } else if (query_code == MyUserIndentQueryActivity.QUERY_UNDELIVERY || query_code == MyUserIndentQueryActivity.QUERY_UNCHECK) {
                // holder.btn_left.setText("发货提醒");
                if (v.getId() == holder.btn_left.getId()) { // 申请退款
                    final int position = Integer.valueOf(v.getTag().toString());
                    View editV = layoutInflater.inflate(R.layout.simple_edit_dialog, null);
                    ((TextView) editV.findViewById(R.id.edit_name)).setText("-----  退款原因  -----");
                    final EditText et = (EditText) editV.findViewById(R.id.dialog_edit1);
                    et.setSingleLine(false);
                    et.setLines(2);
                    et.setMaxLines(15);
                    et.setHint("请输入");
                    cancelDialog();
                    activateVoucherDialog = MyDialog.showDialog(context, "提交退款申请",
                            editV, R.string.submit, R.string.cancel, false, new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String a = et.getText().toString();
                                    if (TextUtils.isEmpty(a) || a.trim().length() <= 0) {
                                        a = " [系统自动提交]提交财务审核退款流程！";
                                    }
                                    UserIndentListQueryAdapter.this.showSubmitDialog(a, position);
                                }
                            });
                }
            }
        }
    };

    /**
     * 每个应用显示的内容，包括图标和名称
     * 
     * @author Yao.GUET
     * 
     */
    final class ViewIndentHolder {
        ImageView imageView1, imageView2;
        ProgressBar iv_pro_bar1, iv_pro_bar2;
        TextView tv_title_name, tv_name_two;// 这里需要HTML
        TextView tv_money, tv_time, tv_clickshop;
        Button btn_left, btn_right;
        LinearLayout ll_title, ll_detail;
    }

    private void cancelDialog() {
        if (null != activateVoucherDialog) {
            MyDialog.dismiss(activateVoucherDialog);
            activateVoucherDialog = null;
        }
    }

    // 退款申请使用
    private void showSubmitDialog(final String a, final int position) {
        final OrderInfo info = (OrderInfo) getItem(position);
        final MyUserIndentListActivity activity = (MyUserIndentListActivity) context;
        cancelDialog();
        activateVoucherDialog = MyDialog.showDialog(context, "提交退款申请", "退款后,该订单将取消,你可以在商品分类中重新购买,确认要退款吗?", R.string.submit, R.string.cancel, false,
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 给后台发送退款 HTTP
                        ServerSupportManager serverMana = new ServerSupportManager(activity, activity);
                        List<Parameter> paras = new ArrayList<Parameter>();
                        paras.add(new Parameter("uid", ((AgentApplication) context.getApplicationContext()).getUid()));
                        paras.add(new Parameter("orderId", info.getOrderId()));
                        paras.add(new Parameter("reason", a));// 退款原因
                        serverMana.supportRequest(AgentConfig.myUserDrawBack(), paras, true,
                                "提交中,请稍等 ...", MyUserIndentListActivity.btn_orderDrawBack);
                        activity.sendAdapterViewPosition(position);
                        cancelDialog();
                    }
                });
    }
}
