package com.gsta.v2.activity.adapter;

import java.util.List;

import com.gsta.v2.entity_v2.OrderLogisticsInfo;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class LogisticsInfoAdapter extends BaseAdapter {
    
    private List<OrderLogisticsInfo> infos;
    
    public LogisticsInfoAdapter(Context context,List<OrderLogisticsInfo> infos){
        this.infos = infos;
    }
    
    public void setInfos(List<OrderLogisticsInfo> infos) {
        this.infos = infos;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return infos.size();
    }

    @Override
    public Object getItem(int position) {
        return infos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        
        return null;
    }

}
