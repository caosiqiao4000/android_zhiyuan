package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity_v2.OrderWareInfo;

/**
 * 
 * @author wubo
 * @createtime 2012-9-5
 */
public class OrderInfoAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<OrderWareInfo> items;

    /**
     * @return the mUserList
     */
    public List<OrderWareInfo> getItems() {
        return items;
    }

    public void addContact(List<OrderWareInfo> OrderWareInfo) {
        if (this.items != null) {
            this.items.addAll(OrderWareInfo);
        } else
            this.items = OrderWareInfo;

        this.notifyDataSetChanged();
    }

    /**
     * @param Items
     *            the Items to set
     */
    public void setItems(List<OrderWareInfo> Items) {
        this.items = Items;
        this.notifyDataSetChanged();
    }

    public OrderInfoAdapter(Context context, List<OrderWareInfo> users) {
        layoutInflater = LayoutInflater.from(context);
        this.items = users;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.orderqueryinfo_item,
                    null);
        }
        final OrderWareInfo item = items.get(position);
//        TextView categoryMark = (TextView) convertView
//                .findViewById(R.id.categoryMark);
        TextView wareName = (TextView) convertView.findViewById(R.id.wareName);
//        TextView wareDesc = (TextView) convertView.findViewById(R.id.wareDesc);
//        TextView price = (TextView) convertView.findViewById(R.id.price);
//        TextView amount = (TextView) convertView.findViewById(R.id.amount);

        /**
         * 0,合约机;1,光宽带;2,手机卡;3,其他;
         */
        // switch (item.getCategoryMark()) {
        // case 0:
        // categoryMark.setText("商品类型:\t合约机");
        // break;
        // case 1:
        // categoryMark.setText("商品类型:\t光宽带");
        // break;
        // case 2:
        // categoryMark.setText("商品类型:\t手机卡");
        // break;
        // case 3:
        // categoryMark.setText("商品类型:\t其他");
        // break;
        // }

        wareName.setText("商品名称:\t"
                + (item.getWareName() == null ? "无" : item.getWareName()));
        // wareDesc.setText("商品规格:\t"
        // + (item.getWareDesc() == null ? "无" : item.getWareDesc()));
        // price.setText("商品价格:\t" + item.getPrice() + "元");
        // amount.setText("购买数量:\t" + item.getAmount());
        return convertView;
    }

}
