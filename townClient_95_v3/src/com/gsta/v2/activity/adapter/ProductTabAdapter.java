package com.gsta.v2.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;

public class ProductTabAdapter extends BaseAdapter {

    private Context context;
    private String[] tab;
    private int select = 1;
    int width;

    public ProductTabAdapter(Context context, String[] tab, int width) {
        this.context = context;
        this.tab = tab;
        this.width = width;
    }

    @Override
    public int getCount() {
        return tab.length;
    }

    public int getDefaultSelection() {
        return 1;
    }

    @Override
    public Object getItem(int arg0) {
        return tab[arg0];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public int getSelect() {
        return select;
    }

    public void setSelect(int select) {
        this.select = select;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if (null == v) {
            v = LayoutInflater.from(context)
                    .inflate(R.layout.producttab_view, null);
        }
        TextView tv_tap = (TextView) v.findViewById(R.id.tv_tap);
        tv_tap.setWidth(width / 3);
        tv_tap.setText(tab[position]);
        if (position == select) {
            tv_tap.setBackgroundResource(R.drawable.check_true);
        } else {
            tv_tap.setBackgroundResource(R.drawable.check_false);
        }
        return v;
    }
}
