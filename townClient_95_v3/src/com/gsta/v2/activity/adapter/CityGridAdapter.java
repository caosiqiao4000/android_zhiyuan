package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.City;

/**
 * @author wubo
 * @createtime 2012-9-1
 */
public class CityGridAdapter extends BaseAdapter {

	Context context;
	List<City> citys;

	public CityGridAdapter(Context context, List<City> citys) {
		this.context = context;
		this.citys = citys;
	}

	public void setCitys(List<City> citys) {
		this.citys = citys;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return citys.size();
	}

	@Override
	public Object getItem(int position) {
		return citys.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.select_city_gv_item, null);
		}
		TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
		tv_name.setText(citys.get(position).getName());
		return convertView;
	}

}
