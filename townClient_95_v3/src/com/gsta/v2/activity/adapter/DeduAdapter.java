package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.ProductDetailInfo;

/**
 * 
 * @author wubo
 * @createtime 2012-9-5
 */
public class DeduAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private List<ProductDetailInfo> mUserList;

	/**
	 * @return the mUserList
	 */
	public List<ProductDetailInfo> getmUserList() {
		return mUserList;
	}

	public void addContact(List<ProductDetailInfo> ProductDetailInfo) {
		if (this.mUserList != null) {
			this.mUserList.addAll(ProductDetailInfo);
		} else
			this.mUserList = ProductDetailInfo;

		this.notifyDataSetChanged();
	}

	/**
	 * @param mUserList
	 *            the mUserList to set
	 */
	public void setmUserList(List<ProductDetailInfo> mUserList) {
		this.mUserList = mUserList;
		this.notifyDataSetChanged();
	}

	public DeduAdapter(Context context, List<ProductDetailInfo> users) {
		layoutInflater = LayoutInflater.from(context);
		this.mUserList = users;
	}

	public int getCount() {
		return mUserList.size();
	}

	public Object getItem(int position) {
		return mUserList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ProductDetailInfo item = mUserList.get(position);
		if (convertView == null) {
			convertView = layoutInflater
					.inflate(R.layout.goods_dedu_item, null);
		}
		TextView tv_0 = (TextView) convertView.findViewById(R.id.tv_0);
		TextView tv_11 = (TextView) convertView.findViewById(R.id.tv_11);
		TextView tv_12 = (TextView) convertView.findViewById(R.id.tv_12);

		tv_0.setText(item.getProductName());

		tv_11.setText(item.getBrokerage());
		tv_12.setText(item.getDeduct());

		return convertView;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

}
