package com.gsta.v2.activity.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class GoodsListAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private Context context;
	private List<GoodsInfo> mList;
	private AgentGoodsHolder holder;
	private DisplayImageOptions options;

	public GoodsListAdapter(Context context, List<GoodsInfo> mList) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.mList = mList;
		options = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic,
				Bitmap.Config.ARGB_8888);
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public GoodsInfo getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		GoodsInfo item = getItem(position);
		if (null == convertView) {
			holder = new AgentGoodsHolder();
			convertView = layoutInflater.inflate(R.layout.list_item, null);
			holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
			holder.tvDetail = (TextView) convertView
					.findViewById(R.id.tv_detail);
			holder.tvPrice = (TextView) convertView
					.findViewById(R.id.tv_bottom_left);
			holder.pro_bar = (ProgressBar) convertView
					.findViewById(R.id.iv_pro_bar);
			holder.iv_icon = (ImageView) convertView.findViewById(R.id.iv_icon);
			convertView.setTag(holder);
		} else {
			holder = (AgentGoodsHolder) convertView.getTag();
		}
		ImageUtil.getNetPicByUniversalImageLoad(holder.pro_bar, holder.iv_icon,
				Util.converPicPath(item.getMediaPath(), FinalUtil.PIC_MIDDLE),
				options);

		holder.tvTitle.setText(item.getProdName());
		holder.tvPrice.setText("售价:￥" + item.getMinSalePrice());
		holder.tvDetail.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
		holder.tvDetail.setText("原价:￥" + item.getMinRetailPrice());
		return convertView;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public List<GoodsInfo> getmList() {
		return mList;
	}

	final class AgentGoodsHolder {
		TextView tvDetail;
		TextView tvPrice;
		TextView tvTitle;
		ImageView iv_icon;
		ProgressBar pro_bar;
	}
}