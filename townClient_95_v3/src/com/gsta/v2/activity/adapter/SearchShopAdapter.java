package com.gsta.v2.activity.adapter;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.ShopInfo;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ShopInfoDesComparator;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 
 * @author longxianwen
 * @createTime 2013-3-26 上午11:57:58
 * @version: 1.0
 * @desc:查询店铺/周top交易店铺
 */
public class SearchShopAdapter extends BaseAdapter {

    Context context;
    List<ShopInfo> rsInfos;
    private double localLat;
    private double localLng;
	// 商品图片使用
	private DisplayImageOptions optionsByNonePic;
	// 头像类使用
	private DisplayImageOptions optionsByNoPhoto;

    public SearchShopAdapter(Context context, List<ShopInfo> rsInfos,
            double localLat, double localLng) {
        this.context = context;
        this.rsInfos = rsInfos;
        this.localLat = localLat;
        this.localLng = localLng;
        
        optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
	     optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
    }

    @Override
    public int getCount() {
        if (rsInfos != null) {
            return rsInfos.size();
        }
        return 0;
    }

    @Override
    public ShopInfo getItem(int position) {
        return this.rsInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {

        @SuppressWarnings("unused")
        final ShopInfo item = getItem(position);
        if (rowView == null) {
            rowView = LayoutInflater.from(context).inflate(
                    R.layout.splist_item, null);
        }
        ImageView iv_icon = (ImageView) rowView.findViewById(R.id.iv_icon); // 店铺icon
        TextView tv_shopname = (TextView) rowView
                .findViewById(R.id.tv_shopname); // 店铺名称
        ImageView iv_level = (ImageView) rowView
                .findViewById(R.id.iv_level); // 店铺等级
        
        TextView tv_distance = (TextView) rowView
                .findViewById(R.id.tv_distance); // 店铺距离
        TextView tv_toast = (TextView) rowView.findViewById(R.id.texview1);
        TextView tv_notice1 = (TextView) rowView
                .findViewById(R.id.tv_notice1); // 店铺公告
        ProgressBar iv_pro_bar = (ProgressBar) rowView
                .findViewById(R.id.iv_pro_bar);

        ShopInfo shopData = rsInfos.get(position);

        // photo
//        iv_icon.setImageResource(R.drawable.icon);
        if (shopData.getPhoto() != null && shopData.getPhoto().length() > 1) {
        	
//            ImageUtil.getNetPic(context, iv_pro_bar, iv_icon,
//                    shopData.getPhoto(),R.drawable.nav_head);
            ImageUtil.getNetPicByUniversalImageLoad(iv_pro_bar, iv_icon, shopData.getPhoto(), optionsByNoPhoto);
        }else {
            iv_icon.setImageResource(R.drawable.nav_head);
        }

        tv_shopname.setText(shopData.getName());

        // levelcode
        iv_level.setImageResource(Util.getLevelCover(shopData
                .getLevelCode()));
        tv_distance.setVisibility(View.GONE);
        // location,计算距离
        if (localLat == 0 || localLng == 0) {
            tv_distance.setVisibility(View.GONE);
        } else {

            // 一公里以下：正常显示
            // 一公里以上：换算,1公里=1千米
            if (shopData.getDistance() == null) {
                // tv_distance.setText("店铺无定位数据");
                tv_distance.setText("");
            } else {
                if (shopData.getDistance() >= 1000) {
                    tv_distance.setText("距离" + shopData.getDistance()
                            / 1000 + "公里以内");
                } else {
                    tv_distance.setText("距离" + shopData.getDistance()
                            + "米以内");
                }
            }
        }

        // introduce
        if (shopData.getIntroduce() == null
                || shopData.getIntroduce().equals("")) {
            tv_toast.setVisibility(View.GONE);
            tv_notice1.setText("");
        } else {
            tv_notice1.setText(shopData.getIntroduce());
        }

        return rowView;
    }

    public List<ShopInfo> getRsInfos() {
        return rsInfos;
    }

    public void setRsInfos(List<ShopInfo> rsInfos) {
        this.rsInfos = rsInfos;
        this.notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        if (localLat != 0 && localLng != 0) {
            sort();
        }
        super.notifyDataSetChanged();
    }

    public void notifyData(double localLat, double localLng) {
        this.localLat = localLat;
        this.localLng = localLng;
        notifyDataSetChanged();
    }

    /**
     * 排序
     * 
     * @author longxianwen
     * @Title: sort
     * @param 设定文件
     * @return void 返回类型
     * @throws
     */
    public void sort() {
        for (int i = 0; i < rsInfos.size(); i++) {
            ShopInfo si = rsInfos.get(i);

            if (si.getLocation() != null
                    && !(si.getLocation().getLatitude() == 0)
                    && !(si.getLocation().getLongitude() == 0)) {
                long distance = Util.DistanceOfTwoPoints(localLat, localLng, si
                        .getLocation().getLatitude(), si.getLocation()
                        .getLongitude());
                si.setDistance(distance);
            }
        }
        ShopInfoDesComparator sid = new ShopInfoDesComparator();
        Collections.sort(rsInfos, sid);
    }
}
