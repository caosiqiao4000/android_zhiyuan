package com.gsta.v2.activity.adapter;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.entity.ShopInfo;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ShopInfoDesComparator;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 
 * @author longxianwen
 * @createTime 2013-3-26 上午11:57:58
 * @version: 1.0
 * @desc:查询周边店铺数据源
 */
public class RimShopAdapter extends BaseAdapter {

    Context context;
    List<ShopInfo> rsInfos;
    private double localLat;
    private double localLng;
    
	// 商品图片使用
	private DisplayImageOptions optionsByNonePic;
	// 头像类使用
	private DisplayImageOptions optionsByNoPhoto;

    private Map<Integer, View> viewMap = new HashMap<Integer, View>();

    public RimShopAdapter(Context context, List<ShopInfo> rsInfos,
            double localLat, double localLng) {
        this.context = context;
        this.rsInfos = rsInfos;
        this.localLat = localLat;
        this.localLng = localLng;
        
        optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
	     optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
    }

    @Override
    public int getCount() {
        if (rsInfos != null) {
            return rsInfos.size();
        }
        return 0;
    }

    @Override
    public ShopInfo getItem(int position) {
        return this.rsInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {

        final ShopInfo item = getItem(position);
        rowView = this.viewMap.get(position);

        if (rowView == null) {
            rowView = LayoutInflater.from(context).inflate(
                    R.layout.rslist_item, null);

            ImageView iv_icon = (ImageView) rowView.findViewById(R.id.iv_icon); // 店铺icon
            TextView tv_shopname = (TextView) rowView
                    .findViewById(R.id.tv_shopname); // 店铺名称
            ImageView iv_level = (ImageView) rowView
                    .findViewById(R.id.iv_level); // 店铺等级
            TextView tv_distance = (TextView) rowView
                    .findViewById(R.id.tv_distance); // 店铺距离
            TextView tv_notice1 = (TextView) rowView
                    .findViewById(R.id.tv_notice1); // 店铺公告
            ProgressBar iv_pro_bar = (ProgressBar) rowView
                    .findViewById(R.id.iv_pro_bar);

            ShopInfo shopData = rsInfos.get(position);

            if (shopData.getPhoto() != null && shopData.getPhoto().length() > 1) {
                ((AgentApplication) context.getApplicationContext()).setPhotoPathByUid(shopData.getAgentUid(), shopData.getPhoto());
                
//                ImageUtil.getNetPic(context, iv_pro_bar, iv_icon,
//                        shopData.getPhoto(), R.drawable.nav_head);
                ImageUtil.getNetPicByUniversalImageLoad(iv_pro_bar, iv_icon, shopData.getPhoto(), optionsByNoPhoto);
            } else {
                iv_icon.setImageResource(R.drawable.nav_head);
            }

            tv_shopname.setText(shopData.getName());

            // 用户等级
            iv_level.setImageResource(Util.getLevelCover(shopData.getLevelCode()));

            if (localLat == 0 || localLng == 0) {
                tv_distance.setVisibility(View.GONE);
            } else {
                if (shopData.getDistance() == null || shopData.getDistance() == Long.MAX_VALUE) {
                    tv_distance.setText("");
                } else {
                    // 一公里以下：正常显示
                    // 一公里以上：换算,1公里=1千米
                    if (shopData.getDistance() >= 1000) {
                        tv_distance.setText("距离" + shopData.getDistance() / 1000 + "公里以内");
                    } else {
                        tv_distance.setText("距离" + shopData.getDistance() + "米以内");
                    }
                }
            }

            if (shopData.getIntroduce() == null || shopData.getIntroduce().equals("")) {
                tv_notice1
                        .setText("无公告,敬请期待...");
            } else {
                tv_notice1.setText(shopData.getIntroduce());
            }

            viewMap.put(position, rowView);
        }

        return rowView;
    }

    public List<ShopInfo> getRsInfos() {
        return rsInfos;
    }

    public void setRsInfos(List<ShopInfo> rsInfos) {
        this.rsInfos = rsInfos;
        this.notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        // TODO Auto-generated method stub
        // this.viewMap = new HashMap<Integer, View>();
        viewMap.clear();
        if (localLat != 0 && localLng != 0) {
            sort();
        }
        super.notifyDataSetChanged();
    }

    public void notifyData(double localLat, double localLng) {
        this.localLat = localLat;
        this.localLng = localLng;
        notifyDataSetChanged();
    }

    /**
     * 排序
     * 
     * @author longxianwen
     * @Title: sort
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param 设定文件
     * @return void 返回类型
     * @throws
     */
    public void sort() {
        for (int i = 0; i < rsInfos.size(); i++) {
            ShopInfo si = rsInfos.get(i);

            if (si.getLocation() != null && !(si.getLocation().getLatitude() == 0) && !(si.getLocation().getLongitude() == 0)) {
                long distance = Util.DistanceOfTwoPoints(localLat, localLng, si
                        .getLocation().getLatitude(), si.getLocation()
                        .getLongitude());
                si.setDistance(distance);
            } else {
                // 服务器没有传递经纬度地址,设置Long.MAX_VALUE，再具体判断
                si.setDistance(Long.MAX_VALUE);
            }

        }
        ShopInfoDesComparator sid = new ShopInfoDesComparator();
        Collections.sort(rsInfos, sid);
    }
}
