package com.gsta.v2.activity;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.contact.ContactActivity;
import com.gsta.v2.activity.goods.HotSalesActivity;
import com.gsta.v2.activity.myshop.MyShopActivity;
import com.gsta.v2.activity.mytown.MyTownActivity;
import com.gsta.v2.activity.service.SendService;
import com.gsta.v2.activity.waiter.MyWaiterActivity;
import com.gsta.v2.entity.BaseAgentInfo;
import com.gsta.v2.entity.BaseUserinfo;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.Util;

/**
 * 主界面 五大功能选项卡
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
public class MainActivity extends TabActivity {

    private static final Logger logger = LoggerFactory.getLogger(MainActivity.class);
    private static final String TAG = Util.getClassName();

    /** Called when the activity is first created. */
    // public static final String TAB_1 = "会话消息";
    public static final String TAB_1 = "联系人";
    @SuppressWarnings("javadoc")
    public static final String TAB_2 = "商品分类";
    @SuppressWarnings("javadoc")
    public static final String TAB_3 = "我的数码城";
    @SuppressWarnings("javadoc")
    public static final String TAB_4 = "我的店铺";
    @SuppressWarnings("javadoc")
    public static final String TAB_5 = "客户服务";

    private TabHost mTabHost;
    // RadioButton不能直接使用 radioButtons[xx]使用
    private RadioButton radio_contact; //
    private RadioButton radio_sale;
    private RadioButton radio_mytown;
    private RadioButton radio_myshop;
    private RadioButton radio_waiter;
    private RadioButton[] radioButtons = { radio_sale, radio_myshop, radio_waiter, radio_contact, radio_mytown };

    private LinearLayout radioGroup;// 底部功能栏
    private LinearLayout ll_myshop; // 我的店铺
    private LinearLayout ll_msg_contact;// 联系人

    private AgentApplication application;

    /**
     * 选择了哪个位置
     */
    public static final String KEY_RADIO_INDEX = "radio_index";
    private int tabindex = 0; // 选择的是哪一个按钮
    private final String KEY_USER_INFO = "userInfo";
    private final String KEY_AGENT_INFO = "agentInfo";
    private final String KEY_ACCOUNT = "account";
    private final String KEY_INVITE_ACCOUNT = "inviteAccount";

    private MainReceiver receiver;// 消息广播
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case 1:
                addTabSpec();
                break;
            case 2:
                int index = msg.arg1;
                logger.info("Restore 恢复 KEY_RADIO_INDEX选择了哪个位置 " + index);
                radioButtons[index].setChecked(true);
                switch (index) {
                case 3:
                    mTabHost.setCurrentTabByTag(TAB_1);
                    break;
                case 0:
                    mTabHost.setCurrentTabByTag(TAB_2);
                    break;
                case 4:
                    mTabHost.setCurrentTabByTag(TAB_3);
                    break;
                case 1:
                    mTabHost.setCurrentTabByTag(TAB_4);
                    break;
                case 2:
                    mTabHost.setCurrentTabByTag(TAB_5);
                    break;
                }
                logger.info("mTabHost 选择了哪个位置 " + mTabHost.getCurrentTabTag());
                break;
            default:
                break;
            }
        };
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);

        application = (AgentApplication) getApplicationContext();
        application.getActivities().add(this);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);// dm用于获取手机屏幕大小
        application.setDensity(dm.density);
        initView();
        application.setWidthPixels(dm.widthPixels);
        application.setHeightPixels(dm.heightPixels);
        // 是否有参数传入用于控制子页面跳回到tab也选中某个tab列
        tabindex = getIntent().getIntExtra(FinalUtil.TABINDEX, 0);
        if (null != savedInstanceState) {
            restoreMethod(savedInstanceState);
        } else {
            Message message = Message.obtain();
            message.what = 2;
            message.arg1 = tabindex;
            handler.sendMessage(message);
        }
        initData();
        initListener();
        myShowShow();
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        restoreMethod(state);
    }

    /**
     * 程序被KILL时,恢复
     * 
     * @author caosq 2013-5-7 下午6:42:37
     * @param state
     */
    private void restoreMethod(Bundle state) {
        int b = state.getInt(FinalUtil.LONGIN_STATE_FLAG);
        if (b == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            application.setLoginState(b);
        }

        int a = state.getInt(KEY_RADIO_INDEX);
        if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            BaseUserinfo userinfo = (BaseUserinfo) state.getSerializable(KEY_USER_INFO);
            if (null != userinfo) {
                logger.info(" savedInstanceState 恢复用户数据  userinfo");
                application.setUserinfo(userinfo);
            }
            BaseAgentInfo agentInfo = (BaseAgentInfo) state.getSerializable(KEY_AGENT_INFO);
            if (null != agentInfo) {
                logger.info(" savedInstanceState 恢复用户数据  agentInfo");
                application.setAgentInfo(agentInfo);
            }
            String acc = (String) state.getSerializable(KEY_ACCOUNT);
            if (null != acc) {
                logger.info(" savedInstanceState 恢复用户数据  account");
                application.setAccount(acc);
            }
            String inviteAccount = (String) state.getSerializable(KEY_INVITE_ACCOUNT);
            if (null != inviteAccount) {
                logger.info(" savedInstanceState 恢复用户数据  inviteAccount");
                application.setInviteAccount(inviteAccount);
            }
        } else {
            logger.info(" 恢复KEY = " + a);
        }
        Message message = Message.obtain();
        message.what = 2;
        message.arg1 = a;
        handler.sendMessage(message);

    }

    /**
     * 游客看不到我的店铺,用户(登录的游客)可以看到我的店铺,但是只有注册功能
     * 
     * @author caosq 2013-5-2 下午5:10:22
     */
    private void myShowShow() {
        if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            if (ll_msg_contact.getVisibility() != 0) {
                ll_msg_contact.setVisibility(0);
            }
            if (application.getAgentStatu() == FinalUtil.AgentStatu_normal) {
                ll_myshop.setVisibility(0);
                radioGroup.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
            } else {
                ll_myshop.setVisibility(View.GONE);
                radioGroup.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            }
            // radioButtons[0].setEnabled(true);
            // radioButtons[3].setEnabled(true);
        } else {
            if (ll_msg_contact.getVisibility() == 0) {
                ll_msg_contact.setVisibility(View.GONE);
            }
            if (ll_myshop.getVisibility() == 0) {
                ll_myshop.setVisibility(View.GONE);
            }
            radioGroup.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            // radioButtons[3].setEnabled(false);
            // radioButtons[0].setEnabled(false);
        }
    }

    /**
     * initActivity
     */
    private void initView() {
        radioButtons[0] = (RadioButton) findViewById(R.id.radio_sale);
        radioButtons[1] = (RadioButton) findViewById(R.id.radio_myshop);
        radioButtons[2] = (RadioButton) findViewById(R.id.radio_waiter);
        radioButtons[3] = (RadioButton) findViewById(R.id.radio_contact);
        radioButtons[4] = (RadioButton) findViewById(R.id.radio_mytown);

        ll_msg_contact = (LinearLayout) findViewById(R.id.ll_msg_contact);
        ll_myshop = (LinearLayout) findViewById(R.id.ll_myshop);
        mTabHost = this.getTabHost();

        radioGroup = (LinearLayout) findViewById(R.id.main_radio);

    }

    private void initData() {
        handler.sendEmptyMessage(1);
    }

    private void initListener() {
        for (int i = 0; i < radioButtons.length; i++) {
            radioButtons[i].setOnClickListener(radio_click);
            radioButtons[i].setOnCheckedChangeListener(radio_checkChange);
        }
    }

    // 去掉其他未选中的radioButton 状态
    android.widget.CompoundButton.OnCheckedChangeListener radio_checkChange = new android.widget.CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                for (int i = 0; i < radioButtons.length; i++) {
                    if (buttonView.getId() != radioButtons[i].getId() && radioButtons[i].isChecked()) {
                        radioButtons[i].setChecked(false);
                    }
                }
            }
        }
    };

    OnClickListener radio_click = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.radio_contact:
                // 跳到相应的选项卡
                mTabHost.setCurrentTabByTag(TAB_1);
                tabindex = 3;
                break;
            case R.id.radio_sale:
                mTabHost.setCurrentTabByTag(TAB_2);
                tabindex = 0;
                break;
            case R.id.radio_mytown:
                mTabHost.setCurrentTabByTag(TAB_3);
                tabindex = 4;
                break;
            case R.id.radio_myshop:
                mTabHost.setCurrentTabByTag(TAB_4);
                tabindex = 1;
                break;
            case R.id.radio_waiter:
                mTabHost.setCurrentTabByTag(TAB_5);
                tabindex = 2;
                break;
            default:
                break;
            }
        }
    };

    /**
     * 添加 五大TabSpec功能选项卡
     * 
     * @author caosq 2013-5-2 下午4:46:08
     */
    private void addTabSpec() {
        TabSpec ts2 = mTabHost.newTabSpec(TAB_2).setIndicator(TAB_2);
        ts2.setContent(new Intent(MainActivity.this, HotSalesActivity.class).setFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        mTabHost.addTab(ts2);

        // 是否代理商 0:普通 1:代理商 2:正在审核 3:账号异常
        if (application.getAgentStatu() != FinalUtil.AgentStatu_normal) {
            // intent.setClass(MainActivity.this, PrePareRegisterActivity.class);
        } else {
            Intent intent = new Intent();
            TabSpec ts4 = mTabHost.newTabSpec(TAB_4).setIndicator(TAB_4);
            intent.setClass(MainActivity.this, MyShopActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ts4.setContent(intent);
            mTabHost.addTab(ts4);
        }

        TabSpec ts5 = mTabHost.newTabSpec(TAB_5).setIndicator(TAB_5);
        ts5.setContent(new Intent(MainActivity.this, MyWaiterActivity.class).setFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        mTabHost.addTab(ts5);

        // 新建一个功能选项卡
        TabSpec ts1 = mTabHost.newTabSpec(TAB_1).setIndicator(TAB_1); //
        ts1.setContent(new Intent(MainActivity.this, ContactActivity.class).setFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        // 添加选项卡到选项卡集合
        mTabHost.addTab(ts1);

        // 我的数码城
        TabSpec ts3 = mTabHost.newTabSpec(TAB_3).setIndicator(TAB_3);
        Intent intent3 = new Intent();
        if (application.getLoginState() != FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            intent3.setClass(MainActivity.this, LoginActivity.class);
        } else {
            intent3.setClass(MainActivity.this, MyTownActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        ts3.setContent(intent3);
        mTabHost.addTab(ts3);
    }

    /**
     * 生成新的TAB菜单
     * 
     * @author caosq 2013-5-6 下午3:47:08
     * @param tag
     *            新生成后,选中哪一个TAB
     */
    private void createNewMainTab(final String tag) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                changeTabHostTabSpec();
                mTabHost.setCurrentTabByTag(tag);
            }
        });
    }

    /**
     * 重新更改TABHOST中的TABSPEC
     * 
     * @author caosq 2013-5-30 上午9:38:55
     */
    private void changeTabHostTabSpec() {
        mTabHost.setCurrentTab(0);
        mTabHost.clearAllTabs();
        addTabSpec();
        myShowShow();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (null == receiver) {
            receiver = new MainReceiver();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(FinalUtil.RECEIVER_USER_LOGING_IN);
        filter.addAction(FinalUtil.RECEIVER_USER_LOGING_OUT);
        filter.addAction(FinalUtil.RECEIVER_USER_PROGRAM_OUT);
        filter.addAction(FinalUtil.RECEIVER_AGENT_LOGING_IN);
        // 用户在修改密码界面里退出登录
        registerReceiver(receiver, filter);
    }

    // mLocalActivityManager 接收用户状态变化
    private class MainReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String ac = intent.getAction();
            if (ac.equals(FinalUtil.RECEIVER_USER_LOGING_IN)) { // 用户登录
                createNewMainTab(TAB_3);
            } else if (ac.equals(FinalUtil.RECEIVER_USER_LOGING_OUT)) { // 用户退出登录
            	
            	  Intent intent2 = new Intent(MainActivity.this, SendService.class);
            	  intent2.putExtra(FinalUtil.SENDSERVICE_FLAG, FinalUtil.exitOpenfire);
                 startService(intent2);
                 
                createNewMainTab(TAB_3);  //清除数据
            } else if (ac.equals(FinalUtil.RECEIVER_USER_PROGRAM_OUT)) { // 退出程序
                leaveTown();
            } else if (ac.equals(FinalUtil.RECEIVER_AGENT_LOGING_IN)) {// 代理商注册成功
                radioButtons[1].setChecked(true);
                createNewMainTab(TAB_4);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        logger.info(TAG + " onSaveInstanceState 保存数据  " + Util.getSysNowTime());
        if (null != application.getUserinfo()) {
            outState.putSerializable(KEY_USER_INFO, application.getUserinfo());
        }
        if (null != application.getAgentInfo()) {
            outState.putSerializable(KEY_AGENT_INFO, application.getAgentInfo());
        }
        if (null != application.getAccount()) {
            outState.putSerializable(KEY_ACCOUNT, application.getAccount());
        }
        if (null != application.getInviteAccount()) {
            outState.putSerializable(KEY_INVITE_ACCOUNT, application.getInviteAccount());
        }
        outState.putInt(KEY_RADIO_INDEX, tabindex);
        outState.putInt(FinalUtil.LONGIN_STATE_FLAG, application.getLoginState());
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.e(TAG, String.valueOf(keyCode));
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Intent intent = new Intent(this, SendService.class);
            intent.putExtra(FinalUtil.SENDSERVICE_FLAG, FinalUtil.sendMin);
            startService(intent);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * 正常离开程序
     */
    private void leaveTown() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setIcon(R.drawable.warning);
        builder.setTitle("提示");
        builder.setMessage("确定要退出程序?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                application.setLoginOutIsNormal(true);
                leaveProgram();
            }
        });
        builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * 退出程序
     * 
     * @author caosq 2013-3-29 下午5:55:21
     */
    private void leaveProgram() {
        application.setLoginState(FinalUtil.LONGIN_LONGINSTATE_OUT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        List<Activity> activities = application.getActivities();
        // 关闭其他activity
        if (activities != null) {
            for (int i = 0; i < activities.size(); i++) {
                if (true == activities.get(i).isFinishing()) {
                    logger.warn(TAG + " " + activities.get(i) + " 已经关闭");
                    continue;
                }
                if (activities.get(i) != null
                        && false == activities.get(i).isFinishing()) {
                    Log.w(TAG, activities.get(i).getClass().getSimpleName() + " 现在关闭");
                    activities.get(i).finish();
                }
            }
        }
        Intent sendService = new Intent(MainActivity.this,
                SendService.class);
        boolean stopSend = stopService(sendService);
        if (!stopSend) {
            logger.warn(TAG + " 服务第一次关闭 末成功...");
        }
        try {
            logger.close();
            String packName = getPackageName();
            ActivityManager activityMgr = (ActivityManager) MainActivity.this
                    .getSystemService(ACTIVITY_SERVICE);
            activityMgr.restartPackage(packName);
            activityMgr.killBackgroundProcesses(packName);
            android.os.Process.killProcess(android.os.Process.myPid());
            logger.info(TAG + " 程序退出..." + Util.getSysNowTime());
            finish();
            // 系统退出
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                logger.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        System.exit(0);
    }

}