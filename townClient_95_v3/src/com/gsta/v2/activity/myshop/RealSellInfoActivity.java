package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.ComissionDirectAdapter;
import com.gsta.v2.activity.adapter.ComissionInDirectAdapter;
import com.gsta.v2.entity.DirectDeductDetail;
import com.gsta.v2.entity.IndirectDeductDetail;
import com.gsta.v2.response.DeductDetailResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 业绩查询(实时业绩)显示信息
 * 
 * @author caosq
 * 
 */
public class RealSellInfoActivity extends BaseActivity implements
        IUICallBackInterface {

    public static final Logger logger = LoggerFactory.getLogger(RealSellInfoActivity.class);
    public static final String TAG = Util.getClassName();

    private RadioGroup rGroup;
    private RadioButton btn_direct_commission; // 直接佣金
    private RadioButton btn_indirect_commission; // 间接佣金

    private ViewFlipper myViewFlip;
    // 直接收益信息
    private PullToRefreshListView pl_lv1;
    private List<DirectDeductDetail> directDeduct;
    private ListView lv1;
    private View noscore_one; // 0没有记录时显示
    private ComissionDirectAdapter adapter;
    // 间接收益信息
    private PullToRefreshListView pl_lv2;
    private List<IndirectDeductDetail> indirectDeduct;
    private ListView lv2;
    private View noscore_two; // 1没有记录时显示
    private ComissionInDirectAdapter adapter_indirect;

    // 加载更多
    private View loadMore1;
    private View loadMore2;
    private Button btn_search, btn_back;
    private TextView titleTextView;
    // private Button btn_direct_commission; // 直接佣金
    // private Button btn_indirect_commission;

    private QueryCondition queryCondition_one = new QueryCondition();
    private QueryCondition queryCondition_two = new QueryCondition();
    // 查询类型 0直接 1间接
    private int queryIndex = 0;
    // 控制按钮点击时的 查询
    private boolean needQuery[] = { true, false };

    public static final String QUERYTYPE_KEY = "queryType_key"; // 上次的查询类型
    public static final String QUERY_PRODUCTNAME = "query_productname"; // 上次的查询的商品名称
    public static final String QUERY_BEGINTIME = "query_beginTime"; // 上次的查询的开始时间
    public static final String QUERY_ENDTIME = "query_endTime"; // 上次的查询的结束时间

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                View nodataV = (View) msg.obj;
                nodataV.setVisibility(View.GONE);
            }
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.acti_realsell);
        mCurrentViewFlipIndex = 0;
        initView();
        initData();
        initListener();
    }

    private void initView() {
        btn_search = (Button) findViewById(R.id.mytitile_btn_right);
        btn_search.setText(R.string.MyShopActivity_fast_query);
        btn_search.setVisibility(0);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);

        titleTextView = (TextView) findViewById(R.id.mytitle_textView);
        titleTextView.setText("实时业绩");

        myViewFlip = (ViewFlipper) findViewById(R.id.myViewFlipper1);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        // 加载更多
        loadMore1 = inflater.inflate(R.layout.loadmore, null);
        loadMore1.setVisibility(0);
        Button btn_loadMore = (Button) loadMore1.findViewById(R.id.loadMoreButton);
        btn_loadMore.setVisibility(0);
        btn_loadMore.setOnClickListener(commissionListener);
        loadMore2 = inflater.inflate(R.layout.loadmore, null);
        loadMore2.setVisibility(0);
        Button btn_loadMore2 = (Button) loadMore2.findViewById(R.id.loadMoreButton);
        btn_loadMore2.setVisibility(0);
        btn_loadMore2.setOnClickListener(commissionListener);
        // 没有数据进提示
        noscore_one = (LinearLayout) findViewById(R.id.vf_nomore_one);
        noscore_one.setVisibility(View.GONE);
        pl_lv1 = (PullToRefreshListView) findViewById(R.id.pl_lv_1);
        lv1 = pl_lv1.getRefreshableView();

        noscore_two = (LinearLayout) findViewById(R.id.vf_nomore_two);
        noscore_two.setVisibility(View.GONE);
        pl_lv2 = (PullToRefreshListView) findViewById(R.id.pl_lv_2);
        lv2 = pl_lv2.getRefreshableView();
        // lv1.addFooterView(loadMore);
        // lv2.addFooterView(loadMore);

        rGroup = (RadioGroup) findViewById(R.id.rg1);
        btn_direct_commission = (RadioButton) findViewById(R.id.myshop_radio_direct_commission);
        btn_indirect_commission = (RadioButton) findViewById(R.id.myshop_radio_indirect_commission);
    }

    private void initData() {
        rGroup.check(btn_direct_commission.getId());

        queryCondition_one.setPage(1L);
        queryCondition_one.setNeedTime(false);
        queryCondition_one.setQueryType(0);
        queryCondition_two.setPage(1L);
        queryCondition_two.setNeedTime(false);
        queryCondition_two.setQueryType(1);

        directDeduct = new ArrayList<DirectDeductDetail>();
        adapter = new ComissionDirectAdapter(this, directDeduct);
        lv1.setAdapter(adapter);
        indirectDeduct = new ArrayList<IndirectDeductDetail>();
        adapter_indirect = new ComissionInDirectAdapter(this, indirectDeduct);
        lv2.setAdapter(adapter_indirect);
        getRealSell(queryCondition_one.isNeedTime(), queryCondition_one.getQueryType());
    }

    private void initListener() {
        rGroup.setOnCheckedChangeListener(rGroup_listener);

        btn_search.setOnClickListener(commissionListener);
        btn_back.setOnClickListener(commissionListener);

        pl_lv1.setRefreshing(true);
        pl_lv1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                directDeduct.clear();
                getRealSell(queryCondition_one.isNeedTime(), queryCondition_one.getQueryType());
            }
        });
        pl_lv2.setRefreshing(true);
        pl_lv2.setOnRefreshListener(new OnRefreshListener() { // 下拉刷新
            @Override
            public void onRefresh() {
                if (indirectDeduct.size() > 0) {
                    indirectDeduct.clear();
                }
                getRealSell(queryCondition_two.isNeedTime(), queryCondition_two.getQueryType());
            }
        });
    }

    OnCheckedChangeListener rGroup_listener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == btn_direct_commission.getId()) {// 直接佣金
                queryIndex = 0;
                if (myViewFlip.getDisplayedChild() != queryIndex) {
                    switchLayoutStateTo(myViewFlip, queryIndex);
                }
                if (needQuery[0]) {
                    return;
                } else {
                    needQuery[0] = true;
                    toQueryComission(false, queryIndex);
                }
                return;
            } else if (checkedId == btn_indirect_commission.getId()) { // 间接佣金
                queryIndex = 1;
                if (myViewFlip.getDisplayedChild() != queryIndex) {
                    switchLayoutStateTo(myViewFlip, queryIndex);
                }
                if (needQuery[1]) {
                    return;
                } else {
                    needQuery[1] = true;
                    toQueryComission(false, queryIndex);
                }
            }
        }
    };

    // 按钮点击事件
    OnClickListener commissionListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.mytitile_btn_left:
                RealSellInfoActivity.this.finish();
                break;
            case R.id.mytitile_btn_right: // 搜索
                Intent intent = new Intent();
                intent.setClass(RealSellInfoActivity.this, RealSellSearchActivity.class);
                intent.putExtra(QUERYTYPE_KEY, queryIndex);
                if (queryIndex == queryCondition_two.getQueryType()) {
                    if (null != queryCondition_two.getProductName()) {
                        intent.putExtra(QUERY_PRODUCTNAME, queryCondition_two.getProductName());
                    }
                    if (queryCondition_two.isNeedTime()) {
                        intent.putExtra(QUERY_BEGINTIME, queryCondition_two.getBeginTime());
                        intent.putExtra(QUERY_ENDTIME, queryCondition_two.getEndTime());
                    }
                } else if (queryIndex == queryCondition_one.getQueryType()) {
                    if (null != queryCondition_one.getProductName()) {
                        intent.putExtra(QUERY_PRODUCTNAME, queryCondition_one.getProductName());
                    }
                    intent.putExtra(QUERY_PRODUCTNAME, queryCondition_one.getProductName());
                    if (queryCondition_one.isNeedTime()) {
                        intent.putExtra(QUERY_BEGINTIME, queryCondition_one.getBeginTime());
                        intent.putExtra(QUERY_ENDTIME, queryCondition_one.getEndTime());
                    }
                }
                startActivityForResult(intent, 0);
                break;
            case R.id.loadMoreButton: // 加载更多
                if (queryIndex == queryCondition_two.getQueryType()) {
                    queryCondition_two.setPage(queryCondition_two.getPage() + 1);
                    queryCondition_two.setRefresh(false);
                    getRealSell(queryCondition_two.isNeedTime(), queryIndex);
                } else if (queryIndex == queryCondition_one.getQueryType()) {
                    queryCondition_one.setPage(queryCondition_one.getPage() + 1);
                    queryCondition_one.setRefresh(false);
                    getRealSell(queryCondition_one.isNeedTime(), queryIndex);
                }
                break;
            }
        }
    };

    /**
     * @param i
     *            // 查询哪个
     * 
     */
    private void toQueryComission(boolean isNeedTime, int i) {
        getRealSell(isNeedTime, i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0) {
            return;
        }
        if (resultCode == 1) {
            queryIndex = data.getExtras().getInt(FinalUtil.REALSELL_TYPE);
            final String aString = data.getExtras().getString(
                    FinalUtil.REALSELL_GOODSNAME).trim();
            if (queryCondition_one.getQueryType() == queryIndex) {
                if (aString != null && aString.length() > 0) {
                    queryCondition_one.setProductName(aString);
                } else {
                    queryCondition_one.setProductName(null);
                }
                queryCondition_one.setBeginTime(data.getExtras()
                        .getString(FinalUtil.REALSELL_BEGINTIME));
                queryCondition_one.setEndTime(data.getExtras().getString(FinalUtil.REALSELL_ENDTIME));

                queryCondition_one.setNeedTime(true);
                queryCondition_one.setPage((long) 1);
                queryCondition_one.setTotal(1l);
                directDeduct.clear();
                adapter.setItems(directDeduct);

                toQueryComission(queryCondition_one.isNeedTime(), queryIndex);
                rGroup.check(btn_direct_commission.getId());

            } else {
                if (aString != null && aString.length() > 0) {
                    queryCondition_two.setProductName(aString);
                } else {
                    queryCondition_two.setProductName(null);
                }
                queryCondition_two.setBeginTime(data.getExtras()
                        .getString(FinalUtil.REALSELL_BEGINTIME));
                queryCondition_two.setEndTime(data.getExtras().getString(FinalUtil.REALSELL_ENDTIME));

                queryCondition_two.setNeedTime(true);
                queryCondition_two.setPage((long) 1);
                queryCondition_two.setTotal(1l);
                indirectDeduct.clear();
                adapter_indirect.setItems(indirectDeduct);
                rGroup.check(btn_indirect_commission.getId());
                toQueryComission(queryCondition_two.isNeedTime(), queryIndex);
            }
            if (myViewFlip.getDisplayedChild() != queryIndex) {
                switchLayoutStateTo(myViewFlip, queryIndex);
            }
        }
    }

    /**
     * 查询业绩
     * 
     * @author wubo
     * @param b
     *            //是否按时间查询
     * @createtime 2012-9-6
     */
    public void getRealSell(boolean b, int type) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        // paras.add(new Parameter("uid", application.getUid()));
        paras.add(new Parameter("agentUid", application.getUid()));
        if (type == queryCondition_one.getQueryType()) {
            if (null != queryCondition_one.getProductName()) {
                paras.add(new Parameter("productName", queryCondition_one.getProductName()));
            }
            // 第一次进来不使用时间查询
            if (b) {
                paras.add(new Parameter("startTime", queryCondition_one.getBeginTime()));
                paras.add(new Parameter("endTime", queryCondition_one.getEndTime()));
            }
            paras.add(new Parameter("limit", queryCondition_one.getPage() + ",10"));
        } else {
            if (null != queryCondition_two.getProductName()) {
                paras.add(new Parameter("productName", queryCondition_two.getProductName()));
            }
            // 第一次进来不使用时间查询
            if (b) {
                paras.add(new Parameter("startTime", queryCondition_two.getBeginTime()));
                paras.add(new Parameter("endTime", queryCondition_two.getEndTime()));
            }
            paras.add(new Parameter("limit", queryCondition_two.getPage() + ",10"));
        }
        paras.add(new Parameter("type", String.valueOf(queryIndex)));
        serverMana.supportRequest(AgentConfig.RealSellInfo(), paras, false,
                "查询中,请稍等 ...", 999);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        pl_lv1.onRefreshComplete();
        pl_lv2.onRefreshComplete();
        if (!HttpResponseStatus(supportResponse))
            return;
        switch (caseKey) {
        case 999:
            DeductDetailResult result = JSONUtil.fromJson(supportResponse
                    .toString(), DeductDetailResult.class);
            if (result != null) {
                if (result.getCount() >= 0) {
                    switch (queryIndex) {
                    case 0:// 直接佣金
                        if (result.getDirectDeduct() != null && result.getDirectDeduct().size() > 0) {
                            if (queryCondition_one.isRefresh()) {
                                directDeduct.clear();
                            }
                            directDeduct.addAll(result.getDirectDeduct());
                        }
                        queryCondition_one.setPage(result.getCount());
                        queryCondition_one.setTotal(result.gettCount());
                        adapter.notifyDataSetChanged();
                        resultChange(result.gettCount(), directDeduct.size(), lv1, loadMore1, noscore_one);
                        break;
                    case 1:
                        if (result.getIndirectDeduct() != null && result.getIndirectDeduct().size() > 0) {
                            if (queryCondition_two.isRefresh()) {
                                indirectDeduct.clear();
                            }
                            indirectDeduct.addAll(result.getIndirectDeduct());
                        }
                        queryCondition_two.setPage(result.getCount());
                        queryCondition_two.setTotal(result.gettCount());
                        adapter_indirect.notifyDataSetChanged();
                        resultChange(result.gettCount(), indirectDeduct.size(), lv2, loadMore2, noscore_two);
                        break;
                    }
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }

    private void resultChange(Long total, int long1, ListView lView, View loadMore2, View noDataView) {
        if (long1 < total) {
            if (lView.getFooterViewsCount() <= 0) {
                lView.addFooterView(loadMore2);
            }
        } else {
            lView.removeFooterView(loadMore2);
            if (long1 <= 0) {
                showNoDataView(noDataView);
            }
        }
    }

    private void showNoDataView(View noDataLayout) {
        Message agem = Message.obtain();
        if (null != noDataLayout) {
            noDataLayout.setVisibility(0);
            agem.obj = noDataLayout;
        }
        agem.what = 1;
        handler.sendMessageDelayed(agem, 2000);
    }

    // 查询条件类
    class QueryCondition {
        private String productName;
        private boolean needTime; // 是否需要时间
        private boolean refresh; // 是否刷新 ture刷新 false加载下一页
        private String beginTime;
        private String endTime;
        private Long page = (long) 0; // 当前页码
        private Long total = 1l; // 总页数

        private int queryType; // 默认0 直接 1间接,,与排列顺序一样

        public boolean isRefresh() {
            return refresh;
        }

        public void setRefresh(boolean refresh) {
            this.refresh = refresh;
        }

        public boolean isNeedTime() {
            return needTime;
        }

        public void setNeedTime(boolean needTime) {
            this.needTime = needTime;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(String beginTime) {
            this.beginTime = beginTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public Long getPage() {
            return page;
        }

        public void setPage(Long page) {
            this.page = page;
        }

        public Long getTotal() {
            return total;
        }

        public void setTotal(Long total) {
            this.total = total;
        }

        public int getQueryType() {
            return queryType;
        }

        public void setQueryType(int queryType) {
            this.queryType = queryType;
        }
    }
}
