package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.MonthScoreAdapter;
import com.gsta.v2.entity.StatInfo;
import com.gsta.v2.response.MonthlyReportResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 月份业绩查询 收益月报表
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
public class MonthScoreActivity extends BaseActivity implements
		IUICallBackInterface {

	private Spinner s1;// 月份选择
	private ArrayAdapter<String> arrayAdapter;
	private List<String> months = new ArrayList<String>();
	private ListView lv1;
	private MonthScoreAdapter adapter;
	private List<StatInfo> statInfos = new ArrayList<StatInfo>();
	private Long page = 1l;
	private Long total = 1l;
	private View loadMore; // 查看更多,分页查询
	// Button btn_loadMore;
	private String month;
	private String searchUid;
	private LinearLayout noData;

	private TextView tv_title;
	private Button btn_back;

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				View nodataV = (View) msg.obj;
				nodataV.setVisibility(View.GONE);
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.monthscore);
		initView();
		initData();
		initListener();
	}

	private void initView() {
		tv_title = (TextView) findViewById(R.id.mytitle_textView);
		btn_back = (Button) findViewById(R.id.mytitile_btn_left);
		btn_back.setVisibility(0);

		s1 = (Spinner) findViewById(R.id.s1);
		lv1 = (ListView) findViewById(R.id.lv_1);
		noData = (LinearLayout) findViewById(R.id.nodata);
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		loadMore = inflater.inflate(R.layout.loadmore, null);
		Button btn_loadMore = (Button) loadMore
				.findViewById(R.id.loadMoreButton);
		btn_loadMore.setVisibility(View.GONE);
		btn_loadMore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 查询月份业绩
				getMonthScore(month);
			}
		});
		lv1.addFooterView(loadMore);
	}

	private void initData() {
		tv_title.setText("收益月报表");
		searchUid = getIntent().getExtras().getString(FinalUtil.OPERUID);
		months.add("所有月份业绩");
		months.addAll(Util.getMonthTime());
		arrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, months);
		arrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(arrayAdapter);
		adapter = new MonthScoreAdapter(this, statInfos);
		lv1.setAdapter(adapter);
		s1.setSelection(0);
	}

	private void initListener() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MonthScoreActivity.this.finish();
			}
		});
		s1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				statInfos.clear();
				page = 1l;
				total = 1l;
				if (position == 0) {
					month = "";
				} else {
					month = months.get(position);
				}
				getMonthScore(month);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
	}

	/**
	 * 查询业绩
	 * 
	 * @author wubo
	 * @createtime 2012-9-6
	 */
	public void getMonthScore(String month) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", application.getUid()));
		paras.add(new Parameter("agentUid", searchUid));
		paras.add(new Parameter("billcycle", month));
		paras.add(new Parameter("limit", page + ",10"));
		serverMana.supportRequest(AgentConfig.GetMonthReport(), paras, true,
				"查询中,请稍等 ...", 999);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		if (!HttpResponseStatus(supportResponse))
			return;
		switch (caseKey) {
		case 999:
			MonthlyReportResult result = JSONUtil.fromJson(
					supportResponse.toString(), MonthlyReportResult.class);
			if (result != null) {
				if (result.getStatInfo() != null) {
					statInfos.addAll(result.getStatInfo());
				}
				adapter.setItems(statInfos);
				page += result.getCount();
				total = result.gettCount();
				resultChange(total, statInfos.size(), lv1, loadMore, noData);
			} else {
				showToast(R.string.to_server_fail);
				return;
			}
			break;
		}
	}

	private void resultChange(Long total, int long1, ListView lView,
			View loadMore2, View noDataView) {
		if (long1 < total) {
			if (lView.getFooterViewsCount() <= 0) {
				lView.addFooterView(loadMore2);
			}
		} else {
			lView.removeFooterView(loadMore2);
			if (long1 <= 0) {
				showNoDataView(noDataView);
			}
		}
	}

	private void showNoDataView(View noDataLayout) {
		Message agem = Message.obtain();
		if (null != noDataLayout) {
			noDataLayout.setVisibility(0);
			agem.obj = noDataLayout;
		}
		agem.what = 1;
		handler.sendMessageDelayed(agem, 2000);
	}

}
