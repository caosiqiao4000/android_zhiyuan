package com.gsta.v2.activity.myshop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.CitySpinnerAdapter;
import com.gsta.v2.db.ICityDao;
import com.gsta.v2.db.impl.CityDaoImpl;
import com.gsta.v2.entity.AgentShopSet;
import com.gsta.v2.entity.AgentShopSetResult;
import com.gsta.v2.entity.City;
import com.gsta.v2.ui.CustomDialog;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.CameraAndShowDiagleActivity;
import com.gsta.v2.util.EditTextKeyListener;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.TextCountLimitWatcher;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * EditUserInfoActivity代理商信息设置 店铺设置
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
public class EditAgentShopInfoActivity extends CameraAndShowDiagleActivity implements
        IUICallBackInterface {

    private static final String TAG = Util.getClassName();// log tag

    private Button ib_myshop;
    private Button btn_submit;
    private Button btn_cancel;
    private TextView phone, mobile, email;
    private TextView post, address, city;
    private ImageView iv_shop;// 店铺图标
    private TextView tv_domain, tv_agentShopName, tv_introduce, tv_agentShopLevel;
    private ImageView iv_agentShopLevel;
    private final int upload_ShopIcon = 0xB1;

    private String domainString;// 如果域名没修改就不上传
    // 更改图片 时 因为后台没有改变图片地址 使用它改变手机缓存中的图片
    private String shopIconChange = null;
    // 店铺图标 与 用户头像是同一张 此处不修改,作保留
    // private LinearLayout ly_image_set;
    private LinearLayout ly_shop_name;
    private LinearLayout ly_realm_name;
    private LinearLayout ly_introduce;
    private LinearLayout ly_phone;
    private LinearLayout ly_mobile;
    private LinearLayout ly_email;
    private LinearLayout ly_post;
    private LinearLayout ly_address;
    private LinearLayout ly_city;

    private List<City> first = new ArrayList<City>();;// 一级
    private List<City> second = new ArrayList<City>();// 二级
    // private List<City> thrid = new ArrayList<City>();// 三级

    private Spinner s_first;// 省选择
    private CitySpinnerAdapter sa1;
    private int cho1 = 0;
    private Spinner s_second;// 市选择
    private CitySpinnerAdapter sa2;
    private int cho2 = 0;
    // private Spinner s_thrid;// 县区选择
    // CitySpinnerAdapter sa3;
    // int cho3 = 0;

    private ICityDao cityDao;

    private final int query_code = 0xc1; // 查询使用
    private final int modify_code = 0xc2; // 查询使用
    
    // 头像类使用
    private DisplayImageOptions optionsByNoPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.edit_userinfo);

        initView();

        initData();

        initListener();
    }

    /**
     * 初始化控件
     * 
     * @author wubo
     * @time 2013-3-14
     * 
     */
    private void initView() {
        btn_submit = (Button) findViewById(R.id.submit);
        btn_cancel = (Button) findViewById(R.id.cancel);

        tv_agentShopName = (TextView) findViewById(R.id.tv_agentShopName);
        tv_domain = (TextView) findViewById(R.id.tv_domain);
        phone = (TextView) findViewById(R.id.phone);
        mobile = (TextView) findViewById(R.id.mobile);
        email = (TextView) findViewById(R.id.email);
        post = (TextView) findViewById(R.id.post);
        address = (TextView) findViewById(R.id.address);
        city = (TextView) findViewById(R.id.city);
        tv_introduce = (TextView) findViewById(R.id.tv_introduce);
        tv_agentShopLevel = (TextView) findViewById(R.id.tv_agentShopLevel);
        iv_agentShopLevel = (ImageView) findViewById(R.id.iv_agentShopLevel);

        iv_shop = (ImageView) findViewById(R.id.iv_shop);

        // ly_image_set = (LinearLayout) findViewById(R.id.ly_image_set);
        ly_realm_name = (LinearLayout) findViewById(R.id.ly_realm_name);
        ly_shop_name = (LinearLayout) findViewById(R.id.ly_shop_name);
        ly_introduce = (LinearLayout) findViewById(R.id.ly_introduce);
        ly_phone = (LinearLayout) findViewById(R.id.ly_phone);
        ly_mobile = (LinearLayout) findViewById(R.id.ly_mobile);
        ly_email = (LinearLayout) findViewById(R.id.ly_email);
        ly_post = (LinearLayout) findViewById(R.id.ly_post);
        ly_address = (LinearLayout) findViewById(R.id.ly_address);
        ly_city = (LinearLayout) findViewById(R.id.ly_city);

        ib_myshop = (Button) findViewById(R.id.mytitile_btn_left);
        TextView tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText("店铺设置");
        ib_myshop.setVisibility(0);
    }

    /**
     * 初始化监听器
     */
    private void initListener() {
        btn_submit.setOnClickListener(mClickListener);
        btn_cancel.setOnClickListener(mClickListener);

        ly_introduce.setOnClickListener(mClickListener);
        ly_phone.setOnClickListener(mClickListener);
        ly_mobile.setOnClickListener(mClickListener);
        ly_email.setOnClickListener(mClickListener);
        ly_shop_name.setOnClickListener(mClickListener);
        ly_realm_name.setOnClickListener(mClickListener);
        ly_post.setOnClickListener(mClickListener);
        ly_address.setOnClickListener(mClickListener);
        ly_city.setOnClickListener(mClickListener);
        // ly_image_set.setOnClickListener(mClickListener);

        ib_myshop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditAgentShopInfoActivity.this.finish();
            }
        });
    }

    /**
     * 获取数据数组
     */
    private void initData() {
    	 optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
        cityDao = new CityDaoImpl(EditAgentShopInfoActivity.this);
        // 通过代码查询中文名
        first = cityDao.getChildCitybyId("2");
        sa1 = new CitySpinnerAdapter(EditAgentShopInfoActivity.this, first);
        sa2 = new CitySpinnerAdapter(EditAgentShopInfoActivity.this, second);
        queryAgentShopInfo();
    }

    /**
     * View单击监听器
     */
    private OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btn_submit) {// 确定
                // 验证
                uploadShopSet();
            } else if (v == btn_cancel) {
                EditAgentShopInfoActivity.this.finish(); // finish返回
            }
            // else if (v == ly_image_set) { // 图片
            // mShowDialog(DIALOG_ID_IMAGE);
            // }
            else if (v == ly_introduce) { // 公告
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, dealString(String
                        .valueOf(tv_introduce.getText())));
                mShowDialog(DIALOG_ID_INTRODUCE, bundle);
            } else if (v == ly_phone) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, dealString(String.valueOf(phone
                        .getText())));
                mShowDialog(DIALOG_ID_PHONE, bundle);
            } else if (v == ly_mobile) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, dealString(String.valueOf(mobile
                        .getText())));
                mShowDialog(DIALOG_ID_MOBILE, bundle);
            } else if (v == ly_email) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, dealString(String.valueOf(email
                        .getText())));
                mShowDialog(DIALOG_ID_EMAIL, bundle);
            } else if (v == ly_realm_name) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, dealString(String
                        .valueOf(tv_domain.getText())));
                mShowDialog(DIALOG_ID_DOMAIN, bundle);
            } else if (v == ly_post) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, dealString(String.valueOf(post
                        .getText())));
                mShowDialog(DIALOG_ID_POST, bundle);
            } else if (v == ly_address) {
                Bundle bundle = new Bundle();
                final String aString = (String) address.getText();
                bundle.putString(KEY_STRING, dealString(aString));
                mShowDialog(DIALOG_ID_ADDRESS, bundle);
            } else if (v == ly_city) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, dealString(String.valueOf(city
                        .getText())));
                mShowDialog(DIALOG_ID_CITY, bundle);
            }  else if(v == ly_shop_name) {
            	Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, dealString(String
                        .valueOf(tv_agentShopName.getText())));
                mShowDialog(DIALOG_ID_SHOPNAME, bundle);
            }
        }

    };

    /**
     * 选择省的事件
     */
    OnItemSelectedListener itemSelectedListener1 = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View v, int position,
                long id) {
            if (cho1 != position) {
                cho1 = position;
                second = cityDao.getChildCitybyId(first.get(position).getId());
                sa2.setCitys(second);
                s_second.setSelection(0);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    /**
     * 选择市的事件
     */
    OnItemSelectedListener itemSelectedListener2 = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View v, int position,
                long id) {
            if (cho2 != position) {
                cho2 = position;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    /**
     * dialog单选以及确定、取消按钮监听
     */
    private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            // TODO Auto-generated method stub
            if (dialog != null) {
                // 取消浮层
                dialog.cancel();
            }
            if (currentDialogId == DIALOG_ID_IMAGE) {// 上传图片
                doSelection(which);// 拍照还是从图库选择图片
            } else {
                setTextView(which);// 赋值
            }
        }
    };

    /**
     * 创建新的Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        // Builder builder = new AlertDialog.Builder(EditUserInfoActivity.this);
        CustomDialog.Builder customBuilder = new
                CustomDialog.Builder(setCreateContext());
        boolean a = true;
        String titleString = null;
        LayoutInflater flater = LayoutInflater.from(this);
        if (id == DIALOG_ID_CITY) {
            // builder = new AlertDialog.Builder(EditUserInfoActivity.this);
            View inflater = LayoutInflater.from(EditAgentShopInfoActivity.this)
                    .inflate(R.layout.city_selection, null);
            s_first = (Spinner) inflater.findViewById(R.id.s1);
            s_first.setAdapter(sa1);
            s_first.setOnItemSelectedListener(itemSelectedListener1);
            s_second = (Spinner) inflater.findViewById(R.id.s2);
            s_second.setAdapter(sa2);
            s_second.setOnItemSelectedListener(itemSelectedListener2);
            // s_thrid = (Spinner) inflater.findViewById(R.id.s3);
            // s_thrid.setVisibility(View.GONE);
            s_first.setSelection(cho1);
            s_second.setSelection(cho2);
            customBuilder.setTitle("选择代理城市");
            customBuilder.setContentView(inflater);
            customBuilder.setPositiveButton("确定",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String city_str = "";
                            if (first.size() >= cho1) {
                                city_str += first.get(cho1).getName();
                            }
                            if (second.size() >= cho2) {
                                city_str += second.get(cho2).getName();
                            }
                            city.setText(city_str);
                            dialog.cancel();
                        }
                    });
            customBuilder.setNegativeButton("取消", null);
        } else {
            dialogContentView = flater.inflate(
                    R.layout.dialog_editview, null);
            et_dialog = (EditText) dialogContentView.findViewById(R.id.et_dialog);
            switch (id) {

            case DIALOG_ID_INTRODUCE:
                titleString = "店铺公告";
                et_dialog.setText(tv_introduce.getText());
                et_dialog.setLines(3);
                // et_dialog.setKeyListener(new EditTextKeyListener(
                // EditTextKeyListener.EMAIL_TYPE));
                et_dialog.addTextChangedListener(new TextCountLimitWatcher(50,
                        et_dialog));
                break;
            case DIALOG_ID_EMAIL:
                titleString = "Email";
                et_dialog.setText(email.getText());
                et_dialog.setLines(3);
                et_dialog.setKeyListener(new EditTextKeyListener(
                        EditTextKeyListener.EMAIL_TYPE));
                et_dialog.addTextChangedListener(new TextCountLimitWatcher(50,
                        et_dialog));
                break;
            case DIALOG_ID_POST:
                titleString = "邮政编码";
                et_dialog.setText(post.getText());
                et_dialog.setSingleLine(true);
                et_dialog.setKeyListener(new EditTextKeyListener(
                        EditTextKeyListener.PHONE_TYPE));
                et_dialog.addTextChangedListener(new TextCountLimitWatcher(6,
                        et_dialog));
                break;
            case DIALOG_ID_DOMAIN:
                titleString = "域名设置";
                et_dialog.setText(tv_domain.getText());
                et_dialog.setSingleLine(true);
                // et_dialog.setKeyListener(new EditTextKeyListener(
                // EditTextKeyListener.PHONE_TYPE)); // ??????????????
                et_dialog.addTextChangedListener(new TextCountLimitWatcher(30,
                        et_dialog));
                break;
            // case DIALOG_ID_CITY:
            //
            // break;
            case DIALOG_ID_SHOPNAME:
            	titleString = "店铺名设置";
            	et_dialog.setText(tv_agentShopName.getText());
            	et_dialog.setSingleLine(true);
            	et_dialog.addTextChangedListener(new TextCountLimitWatcher(30,
            			et_dialog));
            	break;
            default:
                a = false;
                break;
            }
            et_dialog.setHint(R.string.please_input);
            Util.setEditCursorToTextEnd(et_dialog);
            // View view = flater.inflate(R.layout.dialog_title, null);
            // ((TextView) view.findViewById(R.id.dialog_title)).setText(null == titleString ? "无标题" : titleString);
            // builder.setCustomTitle(view);
            // builder.setView(dialogContentView);
            customBuilder.setTitle(null == titleString ? "无标题" : titleString);
            customBuilder.setContentView(dialogContentView);
            customBuilder.setPositiveButton(R.string.submit, mDialogClickListener);
            customBuilder.setNegativeButton(R.string.cancel, null);
        }
        if (a) {
            return customBuilder.create();
        } else
            return super.onCreateDialog(id);
    }

    /**
     * Dialog显示后被点击返回值设置
     * 
     * @param index
     */
    private void setTextView(int index) {
        switch (currentDialogId) {
        case DIALOG_ID_INTRODUCE:
            tv_introduce.setText(et_dialog.getText());
            break;
        case DIALOG_ID_CITY:
            city.setText(et_dialog.getText());
            break;
        case DIALOG_ID_PHONE:
            phone.setText(et_dialog.getText());
            break;
        case DIALOG_ID_MOBILE:
            mobile.setText(et_dialog.getText());
            break;
        case DIALOG_ID_EMAIL:
            email.setText(et_dialog.getText());
            break;
        case DIALOG_ID_SHOPNAME:
        	tv_agentShopName.setText(et_dialog.getText());
        	break;
        case DIALOG_ID_DOMAIN:
            tv_domain.setText(et_dialog.getText());
            break;
        case DIALOG_ID_POST:
            post.setText(et_dialog.getText());
            break;
        case DIALOG_ID_ADDRESS:
            address.setText(et_dialog.getText());
            break;
        }
    }

    /**
     * 设值
     * 
     * @author wubo
     * @param shopSet
     * @createtime 2012-9-14
     */
    public void setValue(AgentShopSet shopSet) {
        if (null != shopSet.getPhoto()) {
            Log.i(TAG, "店铺图标地址为: " + shopSet.getPhoto());
            // Util.getNetIcon(EditUserInfoActivity.this, iv_shop, shopIconChange = shopSet
            // .getShopIcon());
//            ImageUtil.getNetPic(EditAgentShopInfoActivity.this, findViewById(R.id.iv_pro_bar), iv_shop, shopIconChange = shopSet
//                    .getPhoto(), R.drawable.nav_head);
            
            ImageUtil.getNetPicByUniversalImageLoad( findViewById(R.id.iv_pro_bar), iv_shop, shopSet .getPhoto(), optionsByNoPhoto);
            
        }

        iv_agentShopLevel.setImageResource(Util.getLevelCover(shopSet.getLevelCode()));
        tv_agentShopLevel.setText(Html.fromHtml(
                "<font color=#FF0000 >" + Util.getLevelCoverTextDescript(shopSet.getLevelCode()) + "</font> "));
        phone.setText(shopSet.getPhone());
        mobile.setText(shopSet.getMobile());
        email.setText(shopSet.getEmail());
        tv_domain.setText(domainString = shopSet.getDomain());
        post.setText(shopSet.getZipCode());
        address.setText(shopSet.getContactAddress());
        tv_agentShopName.setText(shopSet.getName());
        tv_introduce.setText(shopSet.getIntroduce());

        // 查询到省份和城市的编码
        if (shopSet.getProvinceId() != null && shopSet.getCityId() != null) {
            for (int i = 0; i < first.size(); i++) {
                if (first.get(i).getId().equals(shopSet.getProvinceId())) {
                    Log.i(TAG, "接收时 省份ID为 : " + shopSet.getProvinceId());
                    cho1 = i;
                    break;
                }
            }
            second = cityDao.getChildCitybyId(first.get(cho1).getId());
            sa2.setCitys(second);
            for (int i = 0; i < second.size(); i++) {
                if (second.get(i).getId().equals(shopSet.getCityId())) {
                    Log.i(TAG, "接收时 城市ID为 : " + shopSet.getCityId());
                    cho2 = i;
                    break;
                }
            }
            String city_str = "";
            if (first.size() >= cho1) {
                city_str += first.get(cho1).getName();
            }
            if (second.size() >= cho2) {
                city_str += second.get(cho2).getName();
            }
            city.setText(city_str);
        }
    }

    // 根据代理商的UID查询代理商店铺信息
    private void queryAgentShopInfo() {
        try {
            // 与后台交互
            ServerSupportManager serverMana = new ServerSupportManager(this,
                    this);
            List<Parameter> paras = new ArrayList<Parameter>();
            paras.add(new Parameter("agentUid", String.valueOf(application
                    .getUid())));
            serverMana.supportRequest(AgentConfig.agentShopInfoQuery(), paras,
                    true, "查询中,请稍等 ...", query_code);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse))
            return;
        // BaseResult result = JSONUtil.fromJson(supportResponse.toString(),
        // BaseResult.class);
        switch (caseKey) {
        case upload_ShopIcon: // 店铺图标
            ImageUtil.saveUserIcon(photo, shopIconChange);
//            Map<String, Drawable> draws = AsyncImageLoader.getAsyInstance().getImageCache();
//            draws.remove(AgentConfig.getPicServer() + shopIconChange);
//            draws.put(AgentConfig.getPicServer() + shopIconChange, new BitmapDrawable(photo));
            iv_shop.setImageBitmap(photo);
            break;
        case query_code: // 查询返回
            AgentShopSetResult agentInfo = JSONUtil.fromJson(supportResponse
                    .toString(), AgentShopSetResult.class);
            if (agentInfo != null) {
                AgentShopSet shopSet = agentInfo.getAgentShopSet();
                if (null != shopSet) {
                    setValue(shopSet);
                }
            }
            break;
        case modify_code: // 提交结果
            showToast("修改成功");
            EditAgentShopInfoActivity.this.finish();
            break;
        }
    }

    /**
     * 切割图片之后返回的 Bundle数据处理，取得bitmap类型图片
     * 
     * @param extras
     *            void
     */
    @Override
    public void createPhoto(Bundle extras) {
        if (extras != null) {
            photo = extras.getParcelable("data");
            try {
                if (photo != null) {
                    imageview = iv_shop;
                    File file = getCompress(75, 75, 100);
                    updateShopIcon(file);
                } else {
                    showToast(R.string.res_get_photo_fail);
                }
            } catch (Exception e) {
                showToast(R.string.res_get_photo_fail);
            }
        }
    }

    // 上传修改的店铺信息
    private void uploadShopSet() {
        // 与后台交互
        ServerSupportManager serverMana = new ServerSupportManager(this,
                this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("name", String.valueOf(tv_agentShopName.getText())));
        String aString = String.valueOf(tv_domain.getText());
        if (null != aString && !aString.trim().equals(null != domainString ? domainString : "")) {
            paras.add(new Parameter("domain", String.valueOf(aString)));
        }
        paras.add(new Parameter("phone", String.valueOf(phone.getText())));
        paras.add(new Parameter("uid", String.valueOf(application
                .getUid())));
        paras.add(new Parameter("mobile", String.valueOf(mobile
                .getText())));
        paras.add(new Parameter("email", String.valueOf(email.getText())));
        paras.add(new Parameter("introduce", String.valueOf(tv_introduce
                .getText())));// 本店公告
        paras.add(new Parameter("zipCode", String.valueOf(post.getText())));// 邮政编码
        Log.i(TAG, "上传的省份ID " + cho1 + "  城市ID 是 " + cho2);
        if (null != first && first.size() > cho1) {
            paras.add(new Parameter("provinceId", String.valueOf(first
                    .get(cho1).getId())));
        }
        if (null != second && second.size() > cho2) {
            paras.add(new Parameter("cityId", String.valueOf(second.get(cho2)
                    .getId())));
        }
        paras.add(new Parameter("contactAddress", String.valueOf(address
                .getText())));
        serverMana.supportRequest(AgentConfig.agentShopSetModify(), paras,
                true, "提交中,请稍等 ...", modify_code);
    }

    //
    private void updateShopIcon(File file1) {
        try {
            // 与后台交互
            ServerSupportManager serverMana = new ServerSupportManager(this,
                    this);
            List<Parameter> paras = new ArrayList<Parameter>();
            paras.add(new Parameter("agentUid", String.valueOf(application
                    .getUid())));
            if (file1 != null) {
                ArrayList<MyFilePart> files = new ArrayList<MyFilePart>();
                MyFilePart uploader1 = new MyFilePart(file1.getName(), file1
                        .getName(), file1, new MimetypesFileTypeMap()
                        .getContentType(file1), SyncHttpClient.CONTENT_CHARSET);
                files.add(uploader1);
                serverMana.supportRequest(AgentConfig.uploadShopIcon(), paras,
                        files, true, "提交中,请稍等 ....", upload_ShopIcon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected android.content.DialogInterface.OnClickListener setDialogClickListener() {
        return mDialogClickListener;
    }

    @Override
    protected Context setCreateContext() {
        return EditAgentShopInfoActivity.this;
    }
}
