package com.gsta.v2.activity.myshop;

import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.mytown.MyUserIndentQueryActivity;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.Util;

/**
 * @author caosq 订单查询条件选择页面
 * @createtime 2012-4-11 RealSellInfoActivity
 */
public class OrderSearchActivity extends BaseActivity {

    private Spinner spinn_1;
    private EditText et_account, et_phone;
    private DatePicker begindate, enddate;
    private Calendar nowCal, beginCal, endCal;
    private Button sub, cancel;
    private String account = "";
    private String phone = "";
    private String beginTime = "";
    private String endTime = "";
    private int order_status = 1;
    private String querytype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.search_order);
        nowCal = Calendar.getInstance();
        beginCal = Calendar.getInstance();
        endCal = Calendar.getInstance();

        initView();
        initData();
        initListener();

    }

    private void initView() {
        begindate = (DatePicker) findViewById(R.id.begindate);
        enddate = (DatePicker) findViewById(R.id.enddate);

        sub = (Button) findViewById(R.id.submit);
        cancel = (Button) findViewById(R.id.cancel);
    }

    private void initData() {
        querytype = getIntent().getStringExtra(FinalUtil.QUERY_TYPE);
        if (null != querytype && querytype.equals("1")) {
        } else if (null != querytype && querytype.equals("2")) {// 用户只有时间设置
            findViewById(R.id.spinn_1).setVisibility(View.GONE);
            findViewById(R.id.view_edit1).setVisibility(View.GONE);
            findViewById(R.id.view_edit2).setVisibility(View.GONE);
        } else {
            spinn_1 = (Spinner) findViewById(R.id.spinn_1);

            et_account = (EditText) findViewById(R.id.view_edit1);

            et_phone = (EditText) findViewById(R.id.view_edit2);
            account = getIntent().getExtras()
                    .getString(FinalUtil.ORDER_ACCOUNT);
            phone = getIntent().getExtras()
                    .getString(FinalUtil.ORDER_PHONE);
            if (null != account) {
                et_account.setText(account);
            }
            if (null != phone) {
                et_phone.setText(phone);
            }
            Util.setEditCursorToTextEnd(et_account);
            Util.setEditCursorToTextEnd(et_phone);
            order_status = getIntent().getIntExtra(FinalUtil.ORDER_STATUS, 0);
            switch (order_status) {
            case MyUserIndentQueryActivity.QUERY_UNCONFIRMED:
                spinn_1.setSelection(3);
                break;
            case MyUserIndentQueryActivity.QUERY_SUCCESS:
                spinn_1.setSelection(4);
                break;
            case MyUserIndentQueryActivity.QUERY_CANCEL:
                spinn_1.setSelection(5);
                break;
            default:
                spinn_1.setSelection(order_status);
                break;
            }
        }
        final String beginTimeStr = getIntent().getStringExtra(FinalUtil.ORDER_BEGINTIME);
        final String endTimeStr = getIntent().getStringExtra(FinalUtil.ORDER_ENDTIME);
        if (null != beginTimeStr) {
            beginCal.setTime(Util.strToDate(beginTimeStr));
        } else {
            beginCal.set(nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH) - 1,
                    nowCal.get(Calendar.DAY_OF_MONTH), 00, 00, 00);
        }
        if (null != endTimeStr) {
            endCal.setTime(Util.strToDate(endTimeStr));
        } else {
            endCal.set(nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH),
                    nowCal.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
        }

        beginTime = Util.getFormatDate(beginCal.getTimeInMillis());
        endTime = Util.getFormatDate(endCal.getTimeInMillis());

        begindate
                .init(beginCal.get(Calendar.YEAR),
                        beginCal.get(Calendar.MONTH), beginCal
                                .get(Calendar.DAY_OF_MONTH), null);
        enddate.init(endCal.get(Calendar.YEAR), endCal.get(Calendar.MONTH),
                endCal.get(Calendar.DAY_OF_MONTH), null);
    }

    private void initListener() {
        sub.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                if (null != querytype && !querytype.equals("2")) {
                    account = et_account.getText().toString();
                    phone = et_phone.getText().toString();
                    intent.putExtra(FinalUtil.ORDER_ACCOUNT, account);
                    intent.putExtra(FinalUtil.ORDER_PHONE, phone);
                    intent.putExtra(FinalUtil.ORDER_STATUS, order_status);
                }
                begindate.clearFocus();
                enddate.clearFocus();
                beginCal.set(begindate.getYear(), begindate.getMonth(),
                        begindate.getDayOfMonth());
                endCal.set(enddate.getYear(), enddate.getMonth(), enddate
                        .getDayOfMonth());
                // 处理开始时间大于结束时间
                if (beginCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                    showToast(R.string.to_check_data);
                    return;
                } else {
                    beginTime = Util.getFormatDate(beginCal.getTimeInMillis());
                    endTime = Util.getFormatDate(endCal.getTimeInMillis());
                }
                intent.putExtra(FinalUtil.ORDER_BEGINTIME, beginTime);
                intent.putExtra(FinalUtil.ORDER_ENDTIME, endTime);
                setResult(1, intent);
                OrderSearchActivity.this.finish();
            }
        });
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(0);
                OrderSearchActivity.this.finish();
            }
        });

        if (null != spinn_1) {
            spinn_1.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                        int position, long id) {
                    switch (position) {
                    case 3:
                        order_status = MyUserIndentQueryActivity.QUERY_UNCONFIRMED;
                        break;
                    case 4:
                        order_status = MyUserIndentQueryActivity.QUERY_SUCCESS;
                        break;
                    case 5:
                        order_status = MyUserIndentQueryActivity.QUERY_CANCEL;
                        break;
                    default:
                        order_status = position;
                        break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }
}
