package com.gsta.v2.activity.myshop;

import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.Util;

/**
 * @author wubo
 * @createtime 2012-10-8 业绩搜索条件选择
 */
public class RealSellSearchActivity extends BaseActivity {

    public static final String TAG = Util.getClassName();
    private EditText et_productName;
    private DatePicker begindate, enddate;
    private Calendar nowCal, beginCal, endCal;
    private Button sub, cancel;
    private String productName = "";
    private String beginTime = "";
    private String endTime = "";

    private RadioGroup search_radiogroup;
    private int checkIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.search_realsell);
        nowCal = Calendar.getInstance();
        beginCal = Calendar.getInstance();
        endCal = Calendar.getInstance();

        checkIndex = getIntent().getIntExtra(
                RealSellInfoActivity.QUERYTYPE_KEY, 0);
        final String aString = getIntent().getStringExtra(
                RealSellInfoActivity.QUERY_PRODUCTNAME);

        initView();
        initData();
        initListener();
        if (null != aString) {
            et_productName.setText(aString);
        }
        if (checkIndex == 0) {
            ((RadioButton) search_radiogroup.findViewById(R.id.rb_direct))
                    .setChecked(true);
        } else if (checkIndex == 1) {
            ((RadioButton) search_radiogroup.findViewById(R.id.rb_indirect))
                    .setChecked(true);
        }
    }

    private void initView() {
        et_productName = (EditText) findViewById(R.id.view_edit1);
        et_productName.setHint("商品名称(可不填)");

        begindate = (DatePicker) findViewById(R.id.begindate);

        enddate = (DatePicker) findViewById(R.id.enddate);

        sub = (Button) findViewById(R.id.submit);
        cancel = (Button) findViewById(R.id.cancel);

        search_radiogroup = (RadioGroup) findViewById(R.id.search_radiogroup);
        search_radiogroup.check(checkIndex);
        search_radiogroup
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (checkedId == R.id.rb_direct) {
                            checkIndex = 0;
                        } else if (checkedId == R.id.rb_indirect) {
                            checkIndex = 1;
                        }
                        Log.i(TAG, "查询佣金 " + checkIndex + " 0直接 1 间接");
                    }
                });
    }

    private void initData() {
        final String beginTimeStr = getIntent().getStringExtra(
                RealSellInfoActivity.QUERY_BEGINTIME);
        final String endTimeStr = getIntent().getStringExtra(
                RealSellInfoActivity.QUERY_ENDTIME);
        if (null != beginTimeStr) {
            beginCal.setTime(Util.strToDate(beginTimeStr));
        } else {
            beginCal.set(nowCal.get(Calendar.YEAR),
                    nowCal.get(Calendar.MONTH) - 1, nowCal
                            .get(Calendar.DAY_OF_MONTH), 00, 00, 00);
        }
        if (null != endTimeStr) {
            endCal.setTime(Util.strToDate(endTimeStr));
        } else {
            endCal.set(nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH),
                    nowCal.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
        }

        beginTime = Util.getFormatDate(beginCal.getTimeInMillis());
        endTime = Util.getFormatDate(endCal.getTimeInMillis());

        begindate
                .init(beginCal.get(Calendar.YEAR),
                        beginCal.get(Calendar.MONTH), beginCal
                                .get(Calendar.DAY_OF_MONTH), null);
        enddate.init(endCal.get(Calendar.YEAR), endCal.get(Calendar.MONTH),
                endCal.get(Calendar.DAY_OF_MONTH), null);
    }

    private void initListener() {
        sub.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                begindate.clearFocus();
                enddate.clearFocus();
                productName = et_productName.getText().toString();
                beginCal.set(begindate.getYear(), begindate.getMonth(),
                        begindate.getDayOfMonth(), 00, 00, 00);
                endCal.set(enddate.getYear(), enddate.getMonth(), enddate
                        .getDayOfMonth(), 23, 59, 59);
                long beginTiem = beginCal.getTimeInMillis();
                long endTiem = endCal.getTimeInMillis();
                if (beginTiem >= endTiem) {
                    showToast("开始时间要小于结束时间");
                    return;
                }
                beginTime = Util.getFormatDate(beginTiem);
                endTime = Util.getFormatDate(endTiem);
                Intent intent = new Intent();
                intent.putExtra(FinalUtil.REALSELL_GOODSNAME, productName);
                intent.putExtra(FinalUtil.REALSELL_BEGINTIME, beginTime);
                intent.putExtra(FinalUtil.REALSELL_ENDTIME, endTime);
                intent.putExtra(FinalUtil.REALSELL_TYPE, checkIndex);
                setResult(1, intent);
                RealSellSearchActivity.this.finish();
            }
        });
        cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setResult(0);
                RealSellSearchActivity.this.finish();
            }
        });
    }
}
