package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.MyGoodsListManageUpAdapter;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.response.GoodsListResult;
import com.gsta.v2.ui.DiaglogSearchGoods;
import com.gsta.v2.ui.DiaglogSearchGoods.DialogSearchConfig;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 我的店铺 添加(上架)商品时使用类
 * 
 * @author caosq 2013-6-1
 */
public class MyGoodsManageActivity extends BaseActivity implements IUICallBackInterface {

    private TextView tv_title;
    private Button btn_left, btn_right;
    private PullToRefreshListView pl_r;
    private ListView listV;
    private MyGoodsListManageUpAdapter adapter;
    private ArrayList<GoodsInfo> goodsList;
    private View loadView;

    private int total = 0;
    private int count = 1;
    // 查询
    private static final int query_goods_code = 0x1f;
    // 刷新
    private static final int refre_goods_code = 0x2f;

    private int btnPosition;// 在哪个位置点击了上架按钮
    /**
     * 查询条件
     */
    private int type = 0;// 商品类别 0,合约机,智能机;1,光宽带;2,手机卡;3,手机加宽带; 默认为 0 热销是999
    private String provinceId = "33";// 省份 默认为空
    @SuppressWarnings("unused")
    private String cityId = "20574";// 地市 默认为空
    private String minPrice = "0";// 最小价格 默认为0
    private String maxPrice = "0";// 最大价格 默认为0
    private String keywords = "";// 商品关键词 默认为空
    private String orderBy = "00";// 排序字段，默认为00

    private DiaglogSearchGoods diagloSeach; // 显示查询对话框
    private DialogSearchConfig config; // 要使用的参数

    @SuppressWarnings("unused")
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (4 == msg.what) {
                ((View) msg.obj).setVisibility(View.GONE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.a_mygoods_mange);

        initView();
        initDate();
        initListener();
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        btn_left = (Button) findViewById(R.id.mytitile_btn_left);
        btn_left.setVisibility(0);
        btn_right = (Button) findViewById(R.id.mytitile_btn_right);
        btn_right.setVisibility(0);
        pl_r = (PullToRefreshListView) findViewById(R.id.pl_goods_mange);
        listV = pl_r.getRefreshableView();

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        loadView = inflater.inflate(R.layout.loadmore, null);
        Button btn_load1 = (Button) loadView.findViewById(R.id.loadMoreButton);
        btn_load1.setVisibility(0);
        btn_load1.setOnClickListener(btn_clickList);
    }

    private void initDate() {
        type = getIntent().getIntExtra(FinalUtil.PRODUCT_TYPE, 0);
        if (type == FinalUtil.PRODUCT_FLAG_CALLPHONE) {
            tv_title.setText(FinalUtil.PRODUCT_NAME_CALLPHONE);
        } else if (type == FinalUtil.PRODUCT_FLAG_BROADBAND) {
            tv_title.setText(FinalUtil.PRODUCT_NAME_BROADBAND);
        } else if (type == FinalUtil.PRODUCT_FLAG_PHONECARD) {
            tv_title.setText(FinalUtil.PRODUCT_NAME_PHONECARD);
        } else if (type == FinalUtil.PRODUCT_FLAG_PHONE_BROADBAND) {
            tv_title.setText(FinalUtil.PRODUCT_NAME_PHONE_BROADBAND);
        }

        btn_right.setText("搜  索");

        // 搜索配置
        diagloSeach = new DiaglogSearchGoods(MyGoodsManageActivity.this);
        config = diagloSeach.new DialogSearchConfig();

        goodsList = new ArrayList<GoodsInfo>();
        adapter = new MyGoodsListManageUpAdapter(MyGoodsManageActivity.this, goodsList);
        listV.setAdapter(adapter);
        pl_r.setRefreshing(true);
        productShopQuery(query_goods_code);
    }

    private void initListener() {
        btn_left.setOnClickListener(btn_clickList);
        btn_right.setOnClickListener(btn_clickList);
        pl_r.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                count = 1;
                total = 0;
                productShopQuery(refre_goods_code);
            }
        });
    }

    OnClickListener btn_clickList = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == btn_left.getId()) {
                MyGoodsManageActivity.this.finish();
            } else if (v.getId() == btn_right.getId()) { // 搜索
                String titleStr = tv_title.getText().toString();
                diagloSeach.showSeachDialog(titleStr, config, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                            int which) {
                        final DialogSearchConfig oldConfig = diagloSeach.getConfig();

                        provinceId = diagloSeach.getProvinceId();// 省份 默认为空
                        cityId = diagloSeach.getCityId();// 地市 默认为空
                        minPrice = oldConfig.getMinPrice();// 最小价格
                        // 默认为0
                        if (minPrice.trim().length() < 1) {
                            minPrice = "0";
                        }
                        config.setMinPrice(minPrice);
                        maxPrice = oldConfig.getMaxPrice();// 最大价格
                        // 默认为0
                        if (maxPrice.trim().length() < 1) {
                            maxPrice = "0";
                        }
                        config.setMaxPrice(maxPrice);
                        if (Integer.valueOf(minPrice) > Integer.valueOf(maxPrice)) {
                            showToast("左边的价格必须小于右边的价格,相等为查询全部价格");
                        }
                        keywords = oldConfig.getKeywords();// 商品关键词
                        config.setKeywords(keywords);

                        // 默认为空
                        orderBy = oldConfig.getOrderBy();
                        config.setOrderBy(orderBy);
                        config.setCho4(oldConfig.getCho4());
                        // 默认为0
                        count = 1;
                        if (goodsList.size() > 0) {
                            goodsList.clear();
                        }
                        productShopQuery(refre_goods_code);
                        dialog.dismiss();
                    }
                });
            } else if (v.getId() == R.id.loadMoreButton) {// 加载更多
                productShopQuery(query_goods_code);
            }
        }
    };

    /**
     * 平台商品上架查询
     * 
     * @author wubo
     * @time 2013-1-23
     * @param casekey
     * 
     */
    private void productShopQuery(int casekey) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("type", "" + type));// 商品类别 0,合约机;1,光宽带;2,手机卡;3,其他;
        // 默认为 0
        paras.add(new Parameter("agentUid", application.getUid()));
        paras.add(new Parameter("provinceId", provinceId));// 省份 默认为空
        paras.add(new Parameter("cityId", provinceId));// 地市 默认为空
        paras.add(new Parameter("minPrice", minPrice));// 最小价格 默认为0
        paras.add(new Parameter("maxPrice", maxPrice));// 最大价格 默认为0
        paras.add(new Parameter("keywords", keywords));// 商品关键词 默认为空
        paras.add(new Parameter("pageIndex", String.valueOf(count)));// 当前页数
        // 默认为1
        paras.add(new Parameter("pageSize", String.valueOf(10)));// 分页大小，默认为9
        // 00：上架新品降序 01：上架新品升序
        // 10：销售量降序 11：销售量升序
        // 20：价格降序 21：价格升序
        paras.add(new Parameter("orderBy", orderBy));// 排序字段，默认为00
        serverMana.supportRequest(AgentConfig.getGoodsList(), paras, false,
                "加载中,请稍等 ...", casekey);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_r.isRefreshing()) {
            pl_r.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }
        if (caseKey == MyGoodsActivity.upGoods_code) { // 上架成功
            changeItemByBtn(goodsList, adapter);
            showToast("商品上架成功");
            return;
        }
        GoodsListResult goodsResult = JSONUtil.fromJson(supportResponse
                .toString(), GoodsListResult.class);
        if (caseKey == query_goods_code || caseKey == refre_goods_code) {// 平台商品
            total = goodsResult.getTotal();
            if (caseKey == refre_goods_code) {
                count = 1;
                flushAdapterData(goodsList, adapter);
            }
            count += 1;
            List<GoodsInfo> goodsInfos = goodsResult.getGoodsList();
            if (null != goodsInfos && goodsInfos.size() > 0) {
                goodsList.addAll(goodsInfos);
                adapter.notifyDataSetChanged();
                showLoadView(total, goodsList.size(), loadView, goodsList);
            }
        }
    }

    /**
     * @author caosq 2013-5-21 下午3:00:11
     */
    private void showLoadView(int total, int count, View loadView, List<GoodsInfo> list) {
        if (total > count) {
            if (listV.getFooterViewsCount() == 0) {
                listV.addFooterView(loadView);
            }
        } else {
            listV.removeFooterView(loadView);
            if (list.size() <= 0) {
                showToast("没有数据");
                // showNoDataView(noDataView);
            }
        }
    }

    /**
     * 当为刷新时,初始化数据
     * 
     * @author caosq 2013-5-31 下午5:13:56
     */
    private void flushAdapterData(List<GoodsInfo> list, BaseAdapter adapter) {
        if (list.size() > 0) {
            list.clear();
        }
        adapter.notifyDataSetInvalidated();
    }

    /**
     * 从数据集中更改相应的状态
     * 
     * @author caosq 2013-4-18 下午4:53:27
     */
    private void changeItemByBtn(ArrayList<GoodsInfo> list, BaseAdapter adapter) {
        if (list.size() > btnPosition) {
            @SuppressWarnings("unused")
            GoodsInfo info = list.remove(btnPosition);
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * 当点击item中的按钮时,是在哪个位置
     * 
     * @author caosq 2013-4-17 下午4:22:54
     * @param a
     */
    public void sendAdapterViewPosition(int position) {
        this.btnPosition = position;
    }
}
