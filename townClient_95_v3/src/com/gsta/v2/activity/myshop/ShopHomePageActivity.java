package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.gsta.v2.activity.PopWindowBaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.GoodsListAdapter;
import com.gsta.v2.activity.goods.OtherGoodsDetailActivity;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.entity_v2.ShopLocationResult;
import com.gsta.v2.response.MyGoodsListResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * @author caosq 我的店铺首页 热销页面商品是推荐商品
 * @createtime 2012-9-24
 */
public class ShopHomePageActivity extends PopWindowBaseActivity implements
        IUICallBackInterface {
    /** Called when the activity is first created. */

    private Button ib_myshop; // 返回
    // 若为本人店铺，则收藏“星”更改为店铺商品管理操作 游客没有收藏功能
    // 添加店铺收藏功能接口--配合“我的联系人”：关注商家。 关注商家接口，查询关注商家接口。
    private Button ib_menu;

    // 上面的按钮放置控件
    private HorizontalScrollView hs1;
    private RadioGroup rg1;
    private RadioButton r1; // index 1
    private RadioButton r2;
    private RadioButton r3;
    private RadioButton r4;
    private RadioButton r5; // 店铺首页 index 0

    boolean isClick = false;

    ViewFlipper flipper;
    // 一
    private GoodsListAdapter goodsAdapter1;
    private PullToRefreshListView pl_1; // 智能机
    private ListView lv_1;
    private List<GoodsInfo> goods1;
    private View loadmoreView1;
    private Button btn_load1; // 加载更多
    private int total1 = 0; // 第一个选项卡有多少页
    private int count1 = 1; // 第一个选项卡加载哪一页
    private View noDataView1;
    // 二
    private GoodsListAdapter goodsAdapter2; // 手机卡
    private PullToRefreshListView pl_2;
    private ListView lv_2;
    private List<GoodsInfo> goods2;
    View v2;
    Button btn_load2;
    int total2 = 0;
    int count2 = 1;
    private View noDataView2;

    // 三
    private GoodsListAdapter goodsAdapter3; // 宽带
    private PullToRefreshListView pl_3;
    private ListView lv_3;
    private List<GoodsInfo> goods3;
    View v3;
    Button btn_load3;
    int total3 = 0;
    int count3 = 1;
    private View noDataView3;

    // 四
    private GoodsListAdapter goodsAdapter4; // 手机加宽带
    private PullToRefreshListView pl_4;
    private ListView lv_4;
    private List<GoodsInfo> goods4;
    View v4;
    Button btn_load4;
    int total4 = 0;
    int count4 = 1;
    private View noDataView4;

    private boolean[] needLoad = { false, true, true, true, true, true };// 下标0 不使用

    // 五
    private LinearLayout container_homepage;

    /**
     * 查询条件
     */
    String type = "0";// 商品类别 0,合约机;1,光宽带;2,手机卡;3,礼包; 默认为 0 热销是999
    String provinceId = "33";// 省份 默认为空
    String cityId = "20574";// 地市 默认为空
    String minPrice = "0";// 最小价格 默认为0
    String maxPrice = "0";// 最大价格 默认为0"
    String keywords = "";// 商品关键词 默认为空
    int pageIndex = 1;// 当前页数 默认为1
    int pageSize = 10;// 分页大小，默认为9
    String orderBy = "00";// 排序字段，默认为00

    // 00：上架新品降序 01：上架新品升序
    // 10：销售量降序 11：销售量升序
    // 20：价格降序 21：价格升序

    private TextView tv_title;
    public static final String IFMYSELF_SHOP_FLAG = "myshop_flag";
    public static final String MYSELF_SHOPNAME_FLAG = "myshop_name_flag";
    // 传递过来的 其他店铺或者自己的店铺
    private String agentUid;
    private String userShopName;

    private static final String noData_prompt = "没有获取到商品,请下拉刷新...";
    private static final long DELAYED_HIDE_TIME = 2000;

    private final int query1_code = 1;
    private final int query2_code = 2;
    private final int query3_code = 3;
    private final int query4_code = 4;

    private final int flush1_code = 11; // 这是刷新使用的CODE
    private final int flush2_code = 12;
    private final int flush3_code = 13;
    private final int flush4_code = 14;
    private final int agentShopAdd_code = 15;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            View view = (View) msg.obj;
            view.setVisibility(View.GONE);
        };
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.shop_home_page);
        if (null == getIntent().getStringExtra(IFMYSELF_SHOP_FLAG)) { // 是否自己的商品首页
            agentUid = application.getUid();
            userShopName = (null == application.getAgentInfo().getShopName() ? "" : application.getAgentInfo().getShopName());
        } else {
            agentUid = getIntent().getStringExtra(IFMYSELF_SHOP_FLAG);
            userShopName = getIntent().getStringExtra(MYSELF_SHOPNAME_FLAG);
        }
        initView();
        initDate();
        initListener();
    }

    private void initView() {

        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText(userShopName);

        hs1 = (HorizontalScrollView) findViewById(R.id.tab_view);
        rg1 = (RadioGroup) findViewById(R.id.rg1);
        r1 = (RadioButton) findViewById(R.id.r1);
        r2 = (RadioButton) findViewById(R.id.r2);
        r3 = (RadioButton) findViewById(R.id.r3);
        r4 = (RadioButton) findViewById(R.id.r4);
        r5 = (RadioButton) findViewById(R.id.r5);

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        flipper = (ViewFlipper) findViewById(R.id.myViewFlipper1);

        pl_1 = (PullToRefreshListView) findViewById(R.id.lv_1);
        noDataView1 = findViewById(R.id.nodata_1);
        ((TextView) noDataView1.findViewById(R.id.search)).setText(noData_prompt);
        lv_1 = pl_1.getRefreshableView();
        loadmoreView1 = inflater.inflate(R.layout.loadmore, null);
        btn_load1 = (Button) loadmoreView1.findViewById(R.id.loadMoreButton);
        btn_load1.setVisibility(View.GONE);
        lv_1.addFooterView(loadmoreView1);

        pl_2 = (PullToRefreshListView) findViewById(R.id.lv_2);
        lv_2 = pl_2.getRefreshableView();
        noDataView2 = findViewById(R.id.nodata_2);
        ((TextView) noDataView2.findViewById(R.id.search)).setText(noData_prompt);
        v2 = inflater.inflate(R.layout.loadmore, null);
        btn_load2 = (Button) v2.findViewById(R.id.loadMoreButton);
        btn_load2.setVisibility(View.GONE);
        lv_2.addFooterView(v2);

        pl_3 = (PullToRefreshListView) findViewById(R.id.lv_3);
        lv_3 = pl_3.getRefreshableView();
        noDataView3 = findViewById(R.id.nodata_3);
        ((TextView) noDataView3.findViewById(R.id.search)).setText(noData_prompt);
        v3 = inflater.inflate(R.layout.loadmore, null);
        btn_load3 = (Button) v3.findViewById(R.id.loadMoreButton);
        btn_load3.setVisibility(View.GONE);
        lv_3.addFooterView(v3);

        pl_4 = (PullToRefreshListView) findViewById(R.id.lv_4);
        lv_4 = pl_4.getRefreshableView();
        noDataView4 = findViewById(R.id.nodata_4);
        ((TextView) noDataView4.findViewById(R.id.search)).setText(noData_prompt);
        v4 = inflater.inflate(R.layout.loadmore, null);
        btn_load4 = (Button) v4.findViewById(R.id.loadMoreButton);
        btn_load4.setVisibility(View.GONE);
        lv_4.addFooterView(v4);

        container_homepage = (LinearLayout) findViewById(R.id.ly_container_homepage);

        ib_menu = (Button) findViewById(R.id.mytitile_btn_right);
        if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            ib_menu.setVisibility(0);
        } else {
            ib_menu.setVisibility(View.GONE);
        }
        ib_myshop = (Button) findViewById(R.id.mytitile_btn_left);
        ib_myshop.setVisibility(0);
    }

    private void initDate() {
        if (application.getUid().equals(agentUid)) {
            ib_menu.setText("管 理");
        } else {
            ib_menu.setBackgroundDrawable(getResources().getDrawable(R.drawable.myshop_addshop));
        }
        goods1 = new ArrayList<GoodsInfo>();
        goodsAdapter1 = new GoodsListAdapter(this, goods1);

        goods2 = new ArrayList<GoodsInfo>();
        goodsAdapter2 = new GoodsListAdapter(this, goods2);

        goods3 = new ArrayList<GoodsInfo>();
        goodsAdapter3 = new GoodsListAdapter(this, goods3);

        goods4 = new ArrayList<GoodsInfo>();
        goodsAdapter4 = new GoodsListAdapter(this, goods4);
        rg1.check(r5.getId());
        fristInToIntent();
        lv_1.setAdapter(goodsAdapter1);
        lv_2.setAdapter(goodsAdapter2);
        lv_3.setAdapter(goodsAdapter3);
        lv_4.setAdapter(goodsAdapter4);

    }

    private void initListener() {
        ib_myshop.setOnClickListener(btn_title_click);
        ib_menu.setOnClickListener(btn_title_click);

        rg1.setOnCheckedChangeListener(checkedChangeListener);
        lv_1.setOnItemClickListener(itemClickListener);

        lv_2.setOnItemClickListener(itemClickListener);

        lv_3.setOnItemClickListener(itemClickListener);

        lv_4.setOnItemClickListener(itemClickListener);

        btn_load1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = count1;// 当前页数 默认为1
                productQuery(query1_code);
            }
        });
        btn_load2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                pageIndex = count2;// 当前页数 默认为1
                productQuery(query2_code);
            }
        });
        btn_load3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                pageIndex = count3;// 当前页数 默认为1
                productQuery(query3_code);
            }
        });

        btn_load4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                pageIndex = count4;// 当前页数 默认为1
                productQuery(query4_code);
            }
        });

        pl_1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = "0";
                count1 = 1;
                pageIndex = 1;
                productQuery(flush1_code);
            }
        });

        pl_2.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = "2";
                count2 = 1;
                pageIndex = 1;
                productQuery(flush2_code);
            }
        });

        pl_3.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = "1";
                count3 = 1;
                pageIndex = 1;
                productQuery(flush3_code);
            }
        });

        pl_4.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = "3";
                count4 = 1;
                pageIndex = 1;
                productQuery(flush4_code);
            }
        });
    }

    OnClickListener btn_title_click = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.equals(ib_menu)) { // 是自己的店铺就是跳转到管理商品页面
                if (application.getUid().equals(agentUid)) {
                    Intent intent = new Intent();
                    intent.setClass(ShopHomePageActivity.this, MyGoodsActivity.class);
                    startActivity(intent);
                } else {// interface/shopmanager/AgentShopCollect 这是收藏该店铺功能
                    ServerSupportManager serverMana = new ServerSupportManager(ShopHomePageActivity.this, ShopHomePageActivity.this);
                    List<Parameter> paras = new ArrayList<Parameter>();
                    paras.add(new Parameter("agentUid", agentUid)); // 关系用户UID，被收藏的店铺标识UID
                    // 默认为 0
                    paras.add(new Parameter("userId", application.getUid()));// 用户标识
                    paras.add(new Parameter("opType", String.valueOf(0)));// 0:收藏，1：取消收藏
                    serverMana.supportRequest(AgentConfig.agentShopAdd(), paras, true,
                            "加载中,请稍等 ...", agentShopAdd_code);
                }
            } else if (v.equals(ib_myshop)) {
                ShopHomePageActivity.this.finish();
            }
        }
    };

    /**
     * 商品查询
     * 
     * @author wubo
     * @time 2013-1-23
     * @param casekey
     */
    private void productQuery(int casekey) {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        // 商品类别 0:店铺首页, 1:商品管理页
        paras.add(new Parameter("flag", "" + 0));
        paras.add(new Parameter("type", type)); // 商品类别 0,合约机;1,光宽带;2,手机卡;3,其他;
        // 默认为 0
        paras.add(new Parameter("agentUid", agentUid));
        paras.add(new Parameter("provinceId", provinceId));// 省份 默认为空
        paras.add(new Parameter("cityId", provinceId));// 地市 默认为空
        paras.add(new Parameter("minPrice", minPrice));// 最小价格 默认为0
        paras.add(new Parameter("maxPrice", maxPrice));// 最大价格 默认为0
        paras.add(new Parameter("keywords", keywords));// 商品关键词 默认为空
        paras.add(new Parameter("pageIndex", String.valueOf(pageIndex)));// 当前页数
        // 默认为1
        paras.add(new Parameter("pageSize", String.valueOf(pageSize)));// 分页大小，默认为9
        paras.add(new Parameter("orderBy", orderBy));// 排序字段，默认为00
        // 00：上架新品降序 01：上架新品升序
        // 10：销售量降序 11：销售量升序
        // 20：价格降序 21：价格升序
        serverMana.supportRequest(AgentConfig.getAgentGoodsList(), paras, true,
                "加载中,请稍等 ...", casekey);
    }

    // 下拉刷新VIEW的 点击事件
    OnItemClickListener itemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            GoodsInfo obj = (GoodsInfo) parent.getAdapter().getItem(position);
            // onItemClientEven(obj.getSpecificationId(), obj.getSpeciesId());
            Intent intent = new Intent(ShopHomePageActivity.this,
                    OtherGoodsDetailActivity.class);
            intent.putExtra(FinalUtil.GOODSINFO, obj);
            // intent.putExtra(FinalUtil.AGENT_UID, agentUid);
            // intent.putExtra(FinalUtil.AGENT_NAME, tv_title.getText());
            intent.putExtra(FinalUtil.IMBEAN, new IMBean(agentUid, tv_title.getText().toString(), ""));
            startActivity(intent);
        }
    };

    private final String MYSHOPHOMEPAGE_TAG_ONE = "homepage_one";

    // 上面的按钮放置控件 按钮组事件
    OnCheckedChangeListener checkedChangeListener = new OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == r1.getId()) { // 本店铺推荐商品
                if (flipper.getDisplayedChild() != 1) {
                    hs1.scrollTo(0, 0);
                    switchLayoutStateTo(flipper, 1);
                    type = "0";
                    if (needLoad[1]) {
                        needLoad[1] = false;
                        count1 = 1;
                        pageIndex = 1;
                        productQuery(flush1_code);
                    }
                }
                return;
            }
            if (checkedId == r2.getId()) { // 手机卡
                if (flipper.getDisplayedChild() != 2) {
                    hs1.scrollTo(100, 0);
                    switchLayoutStateTo(flipper, 2);
                    type = "2";
                    if (needLoad[2]) {
                        needLoad[2] = false;
                        count2 = 1;
                        pageIndex = 1;
                        productQuery(flush2_code);
                    }
                }
                return;
            }
            if (checkedId == r3.getId()) {// 宽带
                if (flipper.getDisplayedChild() != 3) {
                    hs1.scrollTo(220, 0);
                    switchLayoutStateTo(flipper, 3);
                    type = "1";
                    if (needLoad[3]) {
                        needLoad[3] = false;
                        count3 = 1;
                        pageIndex = 1;
                        productQuery(flush3_code);
                    }
                }
                return;
            }
            if (checkedId == r4.getId()) { // 手机+宽带
                if (flipper.getDisplayedChild() != 4) {
                    hs1.scrollTo(400, 0);
                    switchLayoutStateTo(flipper, 4);
                    type = "3";
                    if (needLoad[4]) {
                        needLoad[4] = false;
                        count4 = 1;
                        pageIndex = 1;
                        productQuery(flush4_code);
                    }
                }
                return;
            }

            if (checkedId == r5.getId()) { // 店铺首页
                if (flipper.getDisplayedChild() != 0) {
                    fristInToIntent();
                }
                return;
            }
        }
    };

    /**
	 * 
	 */
    private void fristInToIntent() {
        hs1.scrollTo(0, 0);
        switchLayoutStateTo(flipper, 0);
        if (needLoad[5]) {
            needLoad[5] = false;
            Intent intent = new Intent();
            intent.setClass(ShopHomePageActivity.this, MyShopHomePageOneActivity.class);
            intent.putExtra(IFMYSELF_SHOP_FLAG, agentUid);
            intent.putExtra(MYSELF_SHOPNAME_FLAG, userShopName);
            // intent.putExtra(IFNEED_TITLE, false);
            startActivity(MYSHOPHOMEPAGE_TAG_ONE, intent, container_homepage);
        }
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_1.isRefreshing()) {
            pl_1.onRefreshComplete();
        } else if (pl_2.isRefreshing()) {
            pl_2.onRefreshComplete();
        } else if (pl_3.isRefreshing()) {
            pl_3.onRefreshComplete();
        } else if (pl_4.isRefreshing()) {
            pl_4.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            if (caseKey == query1_code || caseKey == flush1_code) {
                showNoDataView(noDataView1);
            } else if (caseKey == query2_code || caseKey == flush2_code) {
                showNoDataView(noDataView2);
            } else if (caseKey == query3_code || caseKey == flush3_code) {
                showNoDataView(noDataView3);
            } else if (caseKey == query4_code || caseKey == flush4_code) {
                showNoDataView(noDataView4);
            }
            return;
        }

        if (caseKey == agentShopAdd_code) { // 收藏店铺结果
            ShopLocationResult goodsResult = JSONUtil.fromJson(supportResponse
                    .toString(), ShopLocationResult.class);
            if (goodsResult != null) {// 0:失败,1:成功， 999其他异常。
                showToast(goodsResult.getErrorMessage());
            } else {
                showToast(R.string.to_server_fail);
            }
            return;
        }

        MyGoodsListResult goodsResult = JSONUtil.fromJson(supportResponse
                .toString(), MyGoodsListResult.class);
        if (goodsResult != null) {
            if (caseKey == query1_code || caseKey == flush1_code) {
                if (caseKey == flush1_code) {
                    goods1.clear();
                    count1 = 1;
                    pageIndex = 1;
                }
                if (null != goodsResult.getAgentGoodsList() && goodsResult.getAgentGoodsList().size() > 0) {
                    goods1.addAll(goodsResult.getAgentGoodsList());
                }
                count1 += 1;
                goodsAdapter1.notifyDataSetChanged();
                if (goods1.size() <= 0) {
                    showNoDataView(noDataView1);
                }
                total1 = goodsResult.getPageCount();
                if (total1 >= count1) {
                    btn_load1.setVisibility(View.VISIBLE);
                } else {
                    btn_load1.setVisibility(View.GONE);
                }
            } else if (caseKey == query2_code || caseKey == flush2_code) {
                if (caseKey == flush2_code) {
                    count1 = 1;
                    pageIndex = 1;
                    goods2.clear();
                }
                if (null != goodsResult.getAgentGoodsList() && goodsResult.getAgentGoodsList().size() > 0) {
                    goods2.addAll(goodsResult.getAgentGoodsList());
                }
                goodsAdapter2.notifyDataSetChanged();
                count2 += 1;
                if (goods2.size() <= 0) {
                    showNoDataView(noDataView2);
                }
                total2 = goodsResult.getPageCount();
                if (total2 >= count2) {
                    btn_load2.setVisibility(View.VISIBLE);
                } else {
                    btn_load2.setVisibility(View.GONE);
                }
            } else if (caseKey == query3_code || caseKey == flush3_code) {
                if (caseKey == flush3_code) {
                    count3 = 1;
                    pageIndex = 1;
                    goods3.clear();
                }
                if (null != goodsResult.getAgentGoodsList() && goodsResult.getAgentGoodsList().size() > 0) {
                    goods3.addAll(goodsResult.getAgentGoodsList());
                }
                goodsAdapter3.notifyDataSetChanged();
                count3 += 1;
                if (goods3.size() <= 0) {
                    // showNoDataView(noDataView3);
                }
                total3 = goodsResult.getPageCount();
                if (total3 >= count3) {
                    btn_load3.setVisibility(View.VISIBLE);
                    handler.sendEmptyMessageDelayed(3, DELAYED_HIDE_TIME);
                } else {
                    btn_load3.setVisibility(View.GONE);
                }
            } else if (caseKey == query4_code || caseKey == flush4_code) {
                if (caseKey == flush4_code) {
                    count4 = 1;
                    pageIndex = 1;
                    goods4.clear();
                }
                if (null != goodsResult.getAgentGoodsList() && goodsResult.getAgentGoodsList().size() > 0) {
                    goods4.addAll(goodsResult.getAgentGoodsList());
                }
                // goodsAdapter4.setmList(goods4);
                goodsAdapter4.notifyDataSetChanged();
                count4 += 1;
                if (goods4.size() <= 0) {
                    showNoDataView(noDataView4);
                }
                total4 = goodsResult.getPageCount();
                if (total4 >= count4) {
                    btn_load4.setVisibility(View.VISIBLE);
                } else {
                    btn_load4.setVisibility(View.GONE);
                }
            }
        } else {
            showToast(R.string.to_server_fail);
            return;
        }
    }

    private void showNoDataView(View v) {
        v.setVisibility(0);
        final Message msg = Message.obtain(handler);
        msg.obj = v;
        handler.sendMessageDelayed(msg, DELAYED_HIDE_TIME);
    }

    @Override
    protected List<Map<String, Object>> createData() {
        List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
        Map<String, Object> map;
        map = new HashMap<String, Object>();
        map.put(KEY, "搜索商品");
        items.add(map);
        map = new HashMap<String, Object>();
        map.put(KEY, "地区更改");
        items.add(map);
        return items;
    }

}