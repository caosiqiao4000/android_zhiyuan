package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.MainActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.GiftAdapter;
import com.gsta.v2.entity.AgentMemoinfo;
import com.gsta.v2.entity.DonationInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.RemarkResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * @author caosq
 * @createtime 2012-9-24 赠品管理
 */
public class GiftManageActivity extends BaseActivity implements
        IUICallBackInterface {

    private PullToRefreshListView pl_remark;
    private ListView remarkList;// 显示列表
    private GiftAdapter rAdapter;// 数据源
    private List<DonationInfo> ams = new ArrayList<DonationInfo>();// 备忘录
    private Builder builder;
    private final int load_code = 11; // http 加载
    private final int del_code = 12;
    private String delId = ""; // 删除哪个赠品
    public static final int NOTE_TYPE_EDIT = 1; // 编辑笔记
    public static final int NOTE_TYPE_ADD = 0; // 添加笔记

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.gift_manage);
        // getAgentApplication();
        initView();
        initData();
        initListener();

        Button ib_myshop;
        ib_myshop = (Button) findViewById(R.id.mytitile_btn_left);
        ib_myshop.setVisibility(0);
        ib_myshop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GiftManageActivity.this,
                        MainActivity.class);
                intent.putExtra(FinalUtil.TABINDEX, 3);
                GiftManageActivity.this.startActivity(intent);
            }
        });
        TextView tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText("赠品管理");
        Button imageBut = (Button) findViewById(R.id.mytitile_btn_right);
        imageBut.setVisibility(0);
        imageBut.setBackgroundResource(R.drawable.add);
        imageBut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                showToast("添加一个赠品,未开发");
                // Intent intent = new Intent();
                // intent.putExtra(FinalUtil.ADDNOTE_TYPE, NOTE_TYPE_ADD);
                // intent.setClass(GiftManageActivity.this, AddNoteActivity.class);
                // startActivity(intent);
            }
        });
    }

    private void initView() {
        // TODO Auto-generated method stub
        pl_remark = (PullToRefreshListView) findViewById(R.id.lv_im);
        remarkList = pl_remark.getRefreshableView();
    }

    private void initData() {
        // rAdapter = new RemarkAdapter(GiftManageActivity.this, ams,
        // clickListener);
        remarkList.setAdapter(rAdapter);
    }

    private void initListener() {
        // TODO Auto-generated method stub
        remarkList.setOnItemClickListener(itemClickListener1);
        pl_remark.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                load();
            }
        });
    }

    protected void onResume() {
        super.onResume();
        load();
    };

    /**
     * 列表点击事件 编辑笔记
     */
    OnItemClickListener itemClickListener1 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                long id) {
            // TODO Auto-generated method stub
            AgentMemoinfo im = (AgentMemoinfo) parent.getAdapter().getItem(
                    position);
            Intent intent = new Intent();
            intent.putExtra(FinalUtil.ADDNOTE_TYPE, NOTE_TYPE_EDIT);
            intent.putExtra(FinalUtil.AGENTMEMOINFO, im);
            intent.setClass(GiftManageActivity.this, AddNoteActivity.class);
            startActivity(intent);
        }
    };

    OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            if (arg0 instanceof ImageView) {
                delId = (String) arg0.getTag();
                builder = new AlertDialog.Builder(GiftManageActivity.this);
                builder.setTitle("删除此备忘录?");
                builder.setPositiveButton("删除",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                del();
                            }
                        });
                builder.setNegativeButton("取消", null);
                builder.create().show();
            }
        };
    };

    public void del() {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", String.valueOf(application.getUid())));
        paras.add(new Parameter("type", "2"));
        paras.add(new Parameter("id", delId));
        serverMana.supportRequest(AgentConfig.MemoManger(), paras, false,
                "提交中,请稍等 ....", del_code);
    }

    public void load() {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", String.valueOf(application.getUid())));
        serverMana.supportRequest(AgentConfig.memoQuery(), paras, false,
                "提交中,请稍等 ....", load_code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {

        if (!HttpResponseStatus(supportResponse)) {
            pl_remark.onRefreshComplete();
            return;
        }

        switch (caseKey) {
        case load_code:
            pl_remark.onRefreshComplete();
            ams.clear();
            RemarkResult r1 = JSONUtil.fromJson(supportResponse.toString(),
                    RemarkResult.class);
            if (r1 != null) {
                if (r1.getErrorCode() == BaseResult.SUCCESS) {
                    // if (r1.getMemoInfo() != null && r1.getMemoInfo().size() > 0) {
                    // ams = r1.getMemoInfo();
                    // } else {
                    // showToast("备忘录为空!");
                    // }
                } else if (r1.getErrorCode() == BaseResult.FAIL) {
                    showToast(r1.errorMessage);
                } else {
                    showToast(R.string.to_server_fail);
                    return;
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            // rAdapter.setMessages(ams);
            break;
        case del_code:
            BaseResult r2 = JSONUtil.fromJson(supportResponse.toString(),
                    BaseResult.class);
            if (r2 != null) {
                if (r2.getErrorCode() == BaseResult.SUCCESS) {
                    for (int i = 0; i < ams.size(); i++) {
                        // if (ams.get(i).getId().equals(delId)) {
                        // ams.remove(i);
                        // break;
                        // }
                    }
                    // rAdapter.setMessages(ams);
                    showToast(r2.errorMessage);
                } else if (r2.getErrorCode() == BaseResult.FAIL) {
                    showToast(r2.errorMessage);
                } else {
                    showToast(R.string.to_server_fail);
                    return;
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }

}
