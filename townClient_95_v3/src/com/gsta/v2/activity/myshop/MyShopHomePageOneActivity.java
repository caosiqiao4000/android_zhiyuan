package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.GoodsListAdapter;
import com.gsta.v2.activity.goods.OtherGoodsDetailActivity;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.entity_v2.AgentInfo;
import com.gsta.v2.entity_v2.AgentInfoQueryResult;
import com.gsta.v2.response.MyGoodsListResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 我的店铺首页中的热销页面
 * 
 */
public class MyShopHomePageOneActivity extends MyShopHomePageBaseActivity {
    private static final Logger logger = LoggerFactory.getLogger();

    private GoodsListAdapter goodsAdapter5; // 店铺首页 后加的
    private PullToRefreshListView pl_0;
    private ListView lv_0;
    private List<GoodsInfo> goods0;
    boolean load5 = true;
    private View v5;
    private Button btn_load5;
    private View noDataLayout; // 无数据的提示 只显示两秒

    private MyShopQuery query;// 查询条件
    private final int query5_code = 5;
    private final int flush5_code = 15;
    private final int agentInfo_code = 0xc1; // 查看的代理商的相关信息

    private LinearLayout ly_photo;
    private ImageView iv_headphoto, iv_grade;
    @SuppressWarnings("unused")
    private ImageView iv_callme;
    private TextView tv_realm_name, tv_notice_info, tv_shop_phone;
    private ProgressBar iv_pro_bar;

    private String agentUid;
    private String agentShopName;
    private String agentPhotoPath;

    // 头像类使用
    private DisplayImageOptions optionsByNoPhoto;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 1) {
                noDataLayout.setVisibility(View.GONE);
            } else if (msg.what == 2) {
                // hideAnim = AnimationUtils.loadAnimation(MyShopHomePageOneActivity.this, R.anim.bar_hide_from_top);
                // hideAnim = new CustomTranslateAnim(
                // Animation.ZORDER_TOP, 0.0f, Animation.ZORDER_TOP,
                // 0.0f, Animation.ZORDER_TOP, 0.0f,
                // Animation.ZORDER_TOP, -1.0f);
            }
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myshop_homepage_one);
        if (null == getIntent().getStringExtra(ShopHomePageActivity.IFMYSELF_SHOP_FLAG)) { // 是否自己的商品首页
            agentUid = application.getUid();
            agentShopName = (null == application.getAgentInfo().getShopName() ? "" : application.getAgentInfo().getShopName());
        } else {
            agentUid = getIntent().getStringExtra(ShopHomePageActivity.IFMYSELF_SHOP_FLAG);
            agentShopName = getIntent().getStringExtra(ShopHomePageActivity.MYSELF_SHOPNAME_FLAG);
        }
        initView();
        initData();
        initListener();
    }

    private void initView() {
        noDataLayout = findViewById(R.id.nodata);
        TextView search = (TextView) noDataLayout.findViewById(R.id.search);
        search.setText("没有获取到商品,请下拉刷新...");
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        pl_0 = (PullToRefreshListView) findViewById(R.id.lv_1);
        lv_0 = pl_0.getRefreshableView();
        v5 = inflater.inflate(R.layout.loadmore, null);
        btn_load5 = (Button) v5.findViewById(R.id.loadMoreButton);
        btn_load5.setVisibility(View.GONE);
        lv_0.addFooterView(v5);

        iv_callme = (ImageView) findViewById(R.id.iv_callme);
        iv_grade = (ImageView) findViewById(R.id.iv_grade);
        iv_headphoto = (ImageView) findViewById(R.id.iv_headphoto);
        iv_pro_bar = (ProgressBar) findViewById(R.id.iv_pro_bar);
        ly_photo = (LinearLayout) findViewById(R.id.ly_photo);

        tv_notice_info = (TextView) findViewById(R.id.tv_notice_info);
        tv_realm_name = (TextView) findViewById(R.id.tv_realm_name);
        tv_shop_phone = (TextView) findViewById(R.id.tv_shop_phone);
    }

    private void initData() {

        optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
        query = new MyShopQuery();
        query.setAgentUid(getIntent().getStringExtra(ShopHomePageActivity.IFMYSELF_SHOP_FLAG));
        query.setQueryType("999");
        query.setProvinceId("33");
        query.setCityId("20574");
        query.setMinPrice("0");
        query.setMaxPrice("0");
        query.setPageIndex(1);
        query.setPageSize(10);
        queryAgentInfo();
        productQuery(query5_code, query, true);

        goods0 = new ArrayList<GoodsInfo>();
        goodsAdapter5 = new GoodsListAdapter(this, goods0);
        lv_0.setAdapter(goodsAdapter5);
    }

    private void initListener() {
        btn_load5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                productQuery(query5_code, query, true);
            }
        });

        lv_0.setOnItemClickListener(itemClickListener);
        lv_0.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                case OnScrollListener.SCROLL_STATE_IDLE: // 当不迁移转变时
                    // 断定迁移转变到底部
                    if (view.getLastVisiblePosition() > 6) {
                        // 在这里添加操纵
                        if (ly_photo.getVisibility() == 0) {
                            ly_photo.setVisibility(View.GONE);
                        }
                    } else {
                        if (ly_photo.getVisibility() == View.GONE) {
                            ly_photo.setVisibility(0);
                        }
                    }
                    break;
                default:
                    break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });

        pl_0.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                query.setPageIndex(0);
                query.setPageSize(10);
                productQuery(flush5_code, query, false);
            }
        });

    }

    // 下拉刷新VIEW的 点击事件
    OnItemClickListener itemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            GoodsInfo obj = (GoodsInfo) parent.getAdapter().getItem(position);
            // onItemClientEven(obj.getSpecificationId(), obj.getSpeciesId());
            Intent intent = new Intent(MyShopHomePageOneActivity.this,
                    OtherGoodsDetailActivity.class);
            intent.putExtra(FinalUtil.GOODSINFO, obj);
            // intent.putExtra(FinalUtil.AGENT_UID, agentUid);
            // intent.putExtra(FinalUtil.AGENT_NAME, agentShopName);
            intent.putExtra(FinalUtil.IMBEAN, new IMBean(agentUid, agentShopName, agentPhotoPath));
            startActivity(intent);
        }
    };

    // 店招信息
    private void queryAgentInfo() {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter(FinalUtil.AGENT_UID, agentUid));
        // 00：上架新品降序 01：上架新品升序
        // 10：销售量降序 11：销售量升序
        // 20：价格降序 21：价格升序
        serverMana.supportRequest(AgentConfig.shopUserInfoQuery(), paras, false,
                "加载中,请稍等 ...", agentInfo_code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_0.isRefreshing()) {
            pl_0.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            if (caseKey == query5_code || caseKey == flush5_code) {
                showNoDataView();
                return;
            }
        }

        try {
            if (caseKey == agentInfo_code) { // 店招信息
                AgentInfoQueryResult goodsResult = JSONUtil.fromJson(supportResponse
                        .toString(), AgentInfoQueryResult.class);
                if (goodsResult != null) {
                    // private ImageView iv_headphoto, iv_grade, iv_callme;
                    // private TextView tv_realm_name, tv_gift_info, tv_shop_phone;
                    AgentInfo info = goodsResult.getDetail();
                    if (null != info) {
                        iv_pro_bar.setVisibility(0);
                        agentPhotoPath = (null != info.getIconUrl() ? info.getIconUrl() : "");
                        //
                        // ImageUtil.getNetPic(MyShopHomePageOneActivity.this, iv_pro_bar, iv_headphoto,
                        // agentPhotoPath, R.drawable.nav_head);
                        ImageUtil.getNetPicByUniversalImageLoad(iv_pro_bar, iv_headphoto, agentPhotoPath, optionsByNoPhoto);
                        application.setPhotoPathByUid(agentUid, agentPhotoPath);
                        iv_grade.setImageResource(Util.getLevelCover(info.getLevelCode()));
                        tv_realm_name.setText(info.getShopUrl());
                        tv_notice_info.setText(null == info.getShopDesc() ? "" : info.getShopDesc());
                        tv_shop_phone.setText(null == info.getPhone() ? "" : info.getPhone());
                    }
                } else {
                    showToast(R.string.to_server_fail);
                }
                return;
            }

            MyGoodsListResult goodsResult = JSONUtil.fromJson(supportResponse
                    .toString(), MyGoodsListResult.class);
            if (goodsResult != null) {
                if (caseKey == query5_code) {
                    query.setPageIndex(query.getPageIndex() + query.getPageSize());
                } else if (caseKey == flush5_code) {
                    goods0.clear();
                    query.setPageIndex(1);
                }
                if (null != goodsResult.getAgentGoodsList() && goodsResult.getAgentGoodsList().size() > 0) {
                    goods0.addAll(goodsResult.getAgentGoodsList());
                } else {
                    showToast("本店没有相关商品");
                }
                goodsAdapter5.notifyDataSetChanged();
                if (query.getPageIndex() >= goodsResult.getPageCount()) {
                    btn_load5.setVisibility(View.GONE);
                } else {
                    btn_load5.setVisibility(View.VISIBLE);
                }
                // if (goods0.size() <= 0) {
                // showNoDataView();
                // }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
        }
    }

    private void showNoDataView() {
        noDataLayout.setVisibility(0);
        handler.sendEmptyMessageDelayed(1, 2000);
    }

}
