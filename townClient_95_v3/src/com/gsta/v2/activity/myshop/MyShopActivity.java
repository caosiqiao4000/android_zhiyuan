package com.gsta.v2.activity.myshop;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.activity.WeiboManagerActivity;
import com.gsta.v2.util.Util;

/**
 * 我的店铺
 * 
 * 我的店铺-首页。
 * 
 * 2.我的数码城-订单管理。包括：待确认收货交易、待付款交易、待发货交易、已成功交易、已经取消交易。
 * 
 * 3.我的数码城商品收藏接口，商品收藏查询接口。
 * 
 * @author caosq
 * 
 */
public class MyShopActivity extends TopBackActivity {

    private LinearLayout ly_info; // 店铺设置
    private LinearLayout ly_homepage; // 首页
    private LinearLayout ly_score; // 订单业绩
    private LinearLayout ly_note; // 营销笔记
    private LinearLayout ly_share; // 营销推广
    private LinearLayout ly_goods; // 商品管理
    private LinearLayout ly_contact; // 我要提现

    @SuppressWarnings("unused")
    private String versionName;
    private TextView tv_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myshop);
        initView();
        initData();
        initListener();
    }

    private void initView() {

        ly_homepage = (LinearLayout) findViewById(R.id.ly_homepage);
        ly_goods = (LinearLayout) findViewById(R.id.ly_goods);
        ly_score = (LinearLayout) findViewById(R.id.ly_score);
        ly_note = (LinearLayout) findViewById(R.id.ly_note);
        ly_share = (LinearLayout) findViewById(R.id.ly_share);
        ly_contact = (LinearLayout) findViewById(R.id.ly_contact);
        ly_info = (LinearLayout) findViewById(R.id.ly_info);

        tv_name = (TextView) findViewById(R.id.mytitle_textView);
    }

    private void initData() {
        versionName = Util.getAppVersionName(MyShopActivity.this);
        @SuppressWarnings("unused")
        String level = null;
        if (application.getUserinfo() == null) {
            return;
        } else {
            level = application.getUserinfo().getVip();
        }
        // iv_level.setImageDrawable(getResources().getDrawable(
        // Util.getLevelCover(level)));
        tv_name.setText("我的店铺");
        // 拉取未读新订单信息和新品上架信息 tv_noread_order tv_goods_info

    }

    private void initListener() {

        ly_homepage.setOnClickListener(listener);
        ly_goods.setOnClickListener(listener);
        ly_score.setOnClickListener(listener);
        ly_note.setOnClickListener(listener);
        ly_share.setOnClickListener(listener);
        ly_contact.setOnClickListener(listener);
        ly_info.setOnClickListener(listener);
    }

    OnClickListener listener = new OnClickListener() {

        @Override
        public void onClick(View vi) {
            if (vi == ly_info) { // 店铺设置
                Intent intent = new Intent(MyShopActivity.this,
                        EditAgentShopInfoActivity.class);
                startActivity(intent);
            } else if (vi == ly_goods) { // 本店商品管理
                Intent intent = new Intent(MyShopActivity.this,
                        MyGoodsActivity.class);
                startActivity(intent);
            } else if (vi == ly_score) { // 业绩查询
                Intent intent = new Intent(MyShopActivity.this,
                        ScoreActivity.class);
                startActivity(intent);
            } else if (vi == ly_note) { // 营销笔记
                Intent intent = new Intent(MyShopActivity.this,
                        RemarkListActivity.class);
                startActivity(intent);
            } else if (vi == ly_share) { // 营销推广
                Intent intent = new Intent(MyShopActivity.this,
                        WeiboManagerActivity.class);
                startActivity(intent);
            } else if (vi == ly_homepage) { // 店铺首页
                Intent intent = new Intent(MyShopActivity.this,
                        ShopHomePageActivity.class);
                startActivity(intent);
            }
            // else if (vi == ly_giftmanager) {// 赠品管理
            // Intent intent = new Intent(MyShopActivity.this,
            // GiftManageActivity.class);
            // startActivity(intent);
            // }
            else if (vi == ly_contact) { // 我要提现
                Intent intent = new Intent(MyShopActivity.this,
                        MyWithdrawActivity.class);
                startActivity(intent);
            }
        }
    };
}
