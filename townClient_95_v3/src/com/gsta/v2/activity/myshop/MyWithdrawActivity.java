package com.gsta.v2.activity.myshop;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.ui.CustomDialog;
import com.gsta.v2.util.Util;

/**
 * 
 * @author longxianwen
 * @createTime Apr 3, 2013 10:33:50 AM
 * @version: 1.0
 * @desc:我要提现
 */
public class MyWithdrawActivity extends TopBackActivity {

    private String TAG = Util.getClassName();
    private Context context = MyWithdrawActivity.this;
    private TextView mytitle_textView;
    private Button mytitile_btn_right, btn_back;
    // 上面的按钮放置控件
    private RadioGroup rg1;
    private RadioButton r1;
    private RadioButton r2;
    private RadioButton r3;

    // 页面
    private ViewFlipper flipper;

    // 父view
    private LinearLayout ll_myacount;
    private LinearLayout ll_inandexdetail;
    private LinearLayout ll_withdrawdetail;

    // 搜索
    private Calendar c, endCalendar;
    private DatePickerDialog begindate, enddate;
    private String type = "";
    private String status = "";
    private WithdrawSearchListener withdrawSearchListener;

    // index 0 为 InAndExDetailActivity的接口和配置 1 为 WithdrawDetailActivity的接口和配置
    private WithdrawSearchListener[] searchListeners = new WithdrawSearchListener[2];
    private SearchConfig[] configs = new SearchConfig[2];
    // 默认数据
    // private String tempAccount = "";
    private int type_pos = 0;
    private int status_pos = 0;

    private final String MYACOUNT_TAG = "myacount";
    private final String INANDEXDETAIL_TAG = "inandexdetail";
    private final String WITHDRAWDETAIL_TAG = "withdrawdetail";

    private boolean needTo[] = { true, true, true, true };// index 0 不使用

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.mywithdraw);
        // 加返回
        initView();
        initData();
        initListener();
    }

    private void initData() {
        c = Calendar.getInstance();
        endCalendar = Calendar.getInstance();
        if (flipper.getDisplayedChild() == 0) {
            toMyAccountPage();
        }
        configs[0] = new SearchConfig();
        configs[1] = new SearchConfig();
        // 显示前一个月
        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONDAY) - 1, c.get(Calendar.DAY_OF_MONTH));
        begindate = new DatePickerDialog(context, dataPickDialogList, c.get(Calendar.YEAR), c.get(Calendar.MONDAY), c.get(Calendar.DAY_OF_MONTH));
        // 当前时间
        enddate = new DatePickerDialog(context, dataPickDialogList, c.get(Calendar.YEAR), c.get(Calendar.MONDAY) + 1, c.get(Calendar.DAY_OF_MONTH));
    }

    private void initListener() {
        mytitile_btn_right.setOnClickListener(myClickListener);
        btn_back.setOnClickListener(myClickListener);
        rg1.setOnCheckedChangeListener(checkedChangeListener);
        rg1.check(r1.getId());
    }

    private void initView() {
        rg1 = (RadioGroup) findViewById(R.id.rg1);
        r1 = (RadioButton) findViewById(R.id.r1);
        r2 = (RadioButton) findViewById(R.id.r2);
        r3 = (RadioButton) findViewById(R.id.r3);

        flipper = (ViewFlipper) findViewById(R.id.myViewFlipper1);

        ll_myacount = (LinearLayout) findViewById(R.id.ll_myacount);
        ll_inandexdetail = (LinearLayout) findViewById(R.id.ll_inandexdetail);
        ll_withdrawdetail = (LinearLayout) findViewById(R.id.ll_withdrawdetail);

        mytitile_btn_right = (Button) findViewById(R.id.mytitile_btn_right);
        mytitile_btn_right.setText("搜 索");
        mytitile_btn_right.setVisibility(View.GONE);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);

        mytitle_textView = (TextView) findViewById(R.id.mytitle_textView);
        mytitle_textView.setText("我要提现");
    }

    // 弹出搜索对话框
    private OnClickListener myClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (v.getId() == mytitile_btn_right.getId()) {
                if (flipper.getDisplayedChild() == 1) {
                    showSearchDialog(flipper.getDisplayedChild());
                } else if (flipper.getDisplayedChild() == 2) {
                    showSearchDialog(flipper.getDisplayedChild());
                } else {
                    mytitile_btn_right.setVisibility(View.GONE);
                }
            } else if (v.getId() == btn_back.getId()) {
                MyWithdrawActivity.this.finish();
            }
        }
    };

    // ViewFlipper 选择切换
    OnCheckedChangeListener checkedChangeListener = new OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == r1.getId()) {
                if (flipper.getDisplayedChild() != 0) {
                    mytitile_btn_right.setVisibility(View.GONE);
                    switchLayoutStateTo(flipper, 0);
                    if (needTo[1]) {
                        needTo[1] = false;
                        toMyAccountPage();
                    }
                }
                return;
            }
            if (checkedId == r2.getId()) {
                if (flipper.getDisplayedChild() != 1) {
                    mytitile_btn_right.setVisibility(View.VISIBLE);
                    switchLayoutStateTo(flipper, 1);
                    if (needTo[2]) {
                        needTo[2] = false;
                        toInAndExPage();
                    }
                }
                return;
            }
            if (checkedId == r3.getId()) {
                if (flipper.getDisplayedChild() != 2) {
                    mytitile_btn_right.setVisibility(View.VISIBLE);
                    switchLayoutStateTo(flipper, 2);
                    if (needTo[3]) {
                        needTo[3] = false;
                        toWithDrawPage();
                    }
                }
                return;
            }
        }

    };

    /**
     * 
     * @author longxianwen
     * @Title: showSearchDialog
     * @Description: 搜索对话框
     * @param @param displayedChild 设定文件
     * @return void 返回类型
     * @throws
     */
    protected void showSearchDialog(int displayedChild) {

        CustomDialog.Builder customBuilder = new
                CustomDialog.Builder(MyWithdrawActivity.this);
        View inflater = LayoutInflater.from(MyWithdrawActivity.this)
                .inflate(R.layout.withdraw_search, null);

        final EditText et_account = (EditText) inflater.findViewById(R.id.et_account);
        Spinner sp_type = (Spinner) inflater.findViewById(R.id.sp_type);
        Spinner sp_status = (Spinner) inflater.findViewById(R.id.sp_status);

        sp_type.setOnItemSelectedListener(new MyOnItemSelectedListener(sp_type));
        sp_status.setOnItemSelectedListener(new MyOnItemSelectedListener(sp_status));

        final EditText start_time = (EditText) inflater.findViewById(R.id.start_time);
        final EditText end_time = (EditText) inflater.findViewById(R.id.end_time);

        //
        LinearLayout ll_account = (LinearLayout) inflater.findViewById(R.id.ll_account);
        LinearLayout ll_type = (LinearLayout) inflater.findViewById(R.id.ll_type);
        LinearLayout ll_status = (LinearLayout) inflater.findViewById(R.id.ll_status);

        if (displayedChild == 1) {
            customBuilder.setTitle("搜索收支明细");
            ll_account.setVisibility(View.GONE);
            ll_status.setVisibility(View.GONE);
            withdrawSearchListener = searchListeners[0];
            if (null != configs[0].startimeStr) {
                start_time.setText(Util.getFormatDateForDay(Long.valueOf(configs[0].startimeStr)));
                end_time.setText(Util.getFormatDateForDay(Long.valueOf(configs[0].endtimeStr)));
                sp_type.setSelection(configs[0].type);
                c.setTimeInMillis(Long.valueOf(configs[0].startimeStr));
                endCalendar.setTimeInMillis(Long.valueOf(configs[0].endtimeStr));
                begindate.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONDAY), c.get(Calendar.DAY_OF_MONTH));
                enddate.updateDate(endCalendar.get(Calendar.YEAR), endCalendar.get(Calendar.MONDAY), endCalendar.get(Calendar.DAY_OF_MONTH));
            } else {
                start_time.setText(Util.getFormatDateForDay(c.getTimeInMillis()));
                end_time.setText(Util.getFormatDateForDay(endCalendar.getTimeInMillis()));
            }
        } else if (displayedChild == 2) {
            customBuilder.setTitle("搜索提现明细");
            ll_type.setVisibility(View.GONE);
            withdrawSearchListener = searchListeners[1];
            if (null != configs[1].startimeStr) {
                start_time.setText(Util.getFormatDateForDay(Long.valueOf(configs[1].startimeStr)));
                end_time.setText(Util.getFormatDateForDay(Long.valueOf(configs[1].endtimeStr)));
                et_account.setText(configs[1].accountStr);
                sp_status.setSelection(configs[1].status_pos);
                c.setTimeInMillis(Long.valueOf(configs[1].startimeStr));
                endCalendar.setTimeInMillis(Long.valueOf(configs[1].endtimeStr));
                begindate.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONDAY), c.get(Calendar.DAY_OF_MONTH));
                enddate.updateDate(endCalendar.get(Calendar.YEAR), endCalendar.get(Calendar.MONDAY), endCalendar.get(Calendar.DAY_OF_MONTH));
            } else {
                start_time.setText(Util.getFormatDateForDay(c.getTimeInMillis()));
                end_time.setText(Util.getFormatDateForDay(endCalendar.getTimeInMillis()));
            }
        }

        start_time.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setDataDialog(start_time, 1);
                return true;
            }
        });
        end_time.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setDataDialog(end_time, 2);
                return true;
            }
        });

        customBuilder.setContentView(inflater);
        customBuilder.setPositiveButton("搜索", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String accountStr = et_account.getText().toString();
                // tempAccount = accountStr;
                String startimeStr = start_time.getText().toString();
                String endtimeStr = end_time.getText().toString();

                String temptype = getTypeInt(type);
                String tempStatus = getStatusInt(status);

                if (withdrawSearchListener != null) {
                    Log.d(TAG, withdrawSearchListener.toString());
                    long a = Util.strToDate(startimeStr.trim() + " 00:00:00").getTime();
                    long b = Util.strToDate(endtimeStr.trim() + " 23:59:59").getTime();
                    if (a < b) {

                        if (withdrawSearchListener instanceof InAndExDetailActivity) {
                            configs[0].startimeStr = a;
                            configs[0].endtimeStr = b;
                            configs[0].type = type_pos;
                        } else if (withdrawSearchListener instanceof WithdrawDetailActivity) {
                            configs[1].accountStr = accountStr;
                            configs[1].startimeStr = a;
                            configs[1].endtimeStr = b;
                            configs[1].status_pos = status_pos;
                        }
                        withdrawSearchListener.onSearch(accountStr, startimeStr, endtimeStr, temptype, tempStatus);
                        dialog.dismiss();
                    } else {
                        showToast("开始日期不能大于结束日期..");
                    }
                }
            }
        });
        customBuilder.setNegativeButton("取消", null);
        customBuilder.create().show();
    }

    /**
     * 子VIEW注册监听
     * 
     * @author caosq 2013-5-24 上午10:51:56
     * @param withdrawSearchListener
     */
    public void setWithdrawSearchListener(
            WithdrawSearchListener withdrawSearchListener) {
        if (withdrawSearchListener instanceof InAndExDetailActivity) {
            searchListeners[0] = withdrawSearchListener;
        } else if (withdrawSearchListener instanceof WithdrawDetailActivity) {
            searchListeners[1] = withdrawSearchListener;
        }
    }

    public void removeWithdrawSearchListener(
            WithdrawSearchListener withdrawSearchListener) {
        if (withdrawSearchListener instanceof InAndExDetailActivity) {
            searchListeners[0] = null;
        } else if (withdrawSearchListener instanceof WithdrawDetailActivity) {
            searchListeners[1] = null;
        }
    }

    // 实现与子VIEW的通信
    public interface WithdrawSearchListener {
        public void onSearch(String accountStr, String startimeStr, String endtimeStr, String type, String status);
    }

    // 保存子VIEW 搜索的配置
    class SearchConfig {
        String accountStr;
        Long startimeStr;// 保存LONG时间
        Long endtimeStr;// 保存LONG时间
        int type;
        int status_pos;
    }

    /**
     * 
     * @author longxianwen
     * @createTime Apr 11, 2013 8:18:59 PM
     * @version: 1.0
     * @desc:得到Spinner数据
     */
    class MyOnItemSelectedListener implements OnItemSelectedListener {

        private Spinner spinner;

        public MyOnItemSelectedListener(Spinner spinner) {
            super();
            this.spinner = spinner;
        }

        public MyOnItemSelectedListener() {
            super();
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position,
                long id) {
            if (spinner.getId() == R.id.sp_type) {
                String[] withdrawType = getResources().getStringArray(R.array.withdraw_type);
                type = withdrawType[position];
                type_pos = position;
            }
            else if (spinner.getId() == R.id.sp_status) {
                String[] withdrawStatus = getResources().getStringArray(R.array.withdraw_status);
                status = withdrawStatus[position];
                status_pos = position;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }

    private EditText tempEd;

    /**
     * 
     * @author longxianwen
     * @Title: setDataDialog
     * @Description: 设置搜索开始时间和结束时间
     * @param @param et
     * @return void 返回类型
     */
    private void setDataDialog(final EditText et, final int i) {
        if (i == 1) {
            begindate.show();
        } else {
            enddate.show();
        }
        tempEd = et;
    }

    DatePickerDialog.OnDateSetListener dataPickDialogList = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                int dayOfMonth) {
            c.set(year, monthOfYear, dayOfMonth);
            long tempLongTime = c.getTimeInMillis();
            String tempTime = Util.getFormatDateForDay(tempLongTime);
            tempEd.setText(tempTime);
        }
    };

    /**
     * 
     * @author longxianwen
     * @Title: toMyAccountPage
     * @Description: 跳转到我的账户
     * @return void 返回类型
     */
    private void toMyAccountPage() {
        if (needTo[1]) {
            needTo[1] = false;
        }
        Intent intent = new Intent();
        intent.setClass(context, MyAccountActivity.class);
        intent.putExtra(IFNEED_TITLE, false);
        startActivity(MYACOUNT_TAG, intent, ll_myacount);
    }

    /**
     * 
     * @author longxianwen
     * @Title: toInAndExPage
     * @Description: 跳转到收支明细
     * @return void 返回类型
     */
    private void toInAndExPage() {
        Intent intent = new Intent();
        intent.setClass(context, InAndExDetailActivity.class);
        startActivity(INANDEXDETAIL_TAG, intent, ll_inandexdetail);
    }

    /**
     * 
     * @author longxianwen
     * @Title: toWithDrawPage
     * @Description: 跳转到提现明细
     * @return void 返回类型
     */
    private void toWithDrawPage() {
        Intent intent = new Intent();
        intent.setClass(context, WithdrawDetailActivity.class);
        startActivity(WITHDRAWDETAIL_TAG, intent, ll_withdrawdetail);
    }

    public static final String RECHARGE_FLAG = "充值";
    public static final String RECHARGE_NUM_FLAG = "0";
    public static final String DEPOSIT_FLAG = "提现";
    public static final String DEPOSIT_NUM_FLAG = "1";
    public static final String SEPARATE_FLAG = "分成";
    public static final String SEPARATE_NUM_FLAG = "3";
    public static final String ALL_FLAG = "all";

    /**
     * 
     * @author longxianwen
     * @Title: getTypeString
     * @Description:交易类型转换
     * @param @param type
     * @param @return 设定文件
     * @return String 返回类型
     * @throws
     */
    private String getTypeInt(String type) {
        if (type.equals(RECHARGE_FLAG)) {
            return RECHARGE_NUM_FLAG;
        } else if (type.equals(DEPOSIT_FLAG)) {
            return DEPOSIT_NUM_FLAG;
        } else if (type.equals(SEPARATE_FLAG)) {
            return SEPARATE_NUM_FLAG;
        } else {
            return ALL_FLAG;
        }
    }

    /**
     * 
     * @author longxianwen
     * @Title: getTransactionStatusString
     * @Description: 交易状态
     * @param @param status
     * @param @return 设定文件
     * @return String 返回类型
     * @throws
     */
    private String getStatusInt(String status) {
        if (status.endsWith("待处理")) {
            return "0";
        } else if (status.endsWith("已转账")) {
            return "1";
        } else if (status.endsWith("申请驳回")) {
            return "2";
        } else {
            return "all";
        }
    }

}
