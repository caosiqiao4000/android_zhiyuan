package com.gsta.v2.activity.myshop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.db.INoteDao;
import com.gsta.v2.db.IPicUpLoadAfterSavePath;
import com.gsta.v2.db.impl.NoteDaoImpl;
import com.gsta.v2.db.impl.PicUpLoadSavePathDao;
import com.gsta.v2.entity.AgentMemoinfo;
import com.gsta.v2.entity.NoteInfo;
import com.gsta.v2.entity.PicUploadSavePathInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.MemoManageResult;
import com.gsta.v2.ui.CustomDialog;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.CameraAndShowDiagleActivity;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.TextCountLimitWatcher;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 营销备忘录
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
public class AddNoteActivity extends CameraAndShowDiagleActivity implements
        IUICallBackInterface {

    private Button btn_submit;// 提交按钮
    private Button btn_cancel;// 取消按钮
    private Button btn_back;// 取消按钮
    private Button delBtn;// 删除记录

    private TextView goodsId; // 记录条数
    TextView goodsName;// 商品姓名
    TextView name;// 用户姓名
    TextView num;// 身份证号
    TextView mobile;// 手机号码
    TextView address;// 地址
    TextView remark;// 备注
    TextView tv_title;// 标题

    /**
     * LinearLayout 主要是做点击事件
     */
    private LinearLayout ly_goodsName;
    private LinearLayout ly_name;
    private LinearLayout ly_num;
    private LinearLayout ly_mobile;
    private LinearLayout ly_address;
    private LinearLayout ly_remark;
    private LinearLayout ly_voice_upload; // 使用录音功能
    private LinearLayout ly_im_upload; // 使用拍照功能

    // 图片布局
    private ImageView im_upload1; // 点击放大图片
    private ImageView im_upload2;
    private ImageView im_upload3;
    private ImageView[] im_upload = new ImageView[3];// 图片布局集合
    private Bitmap[] imageBit = new Bitmap[3];// 图片缓存
    private String[] imagepathCache = new String[3];// 图片地址缓存
    boolean image_change[] = new boolean[3];// 做更新操作时,图片是否被修改
    // 语音布局
    private ImageView voice_upload1; // 语音点击播放
    private ImageView voice_upload2;
    private ImageView voice_upload3;
    private ImageView[] voice_upload = new ImageView[3];// 声音布局集合

    private final int DIALOG_ID_VOICE = 0x111f;

    final int comm_code = 11;// 提交
    final int del_code = 12;

    int type;// 营销笔记的类型 0:增加,1:修改,2: 删除
    AgentMemoinfo aminfo = null;

    boolean voice_ishave[] = new boolean[3];// 是否已经有声音文件

    String[] voicePaths = new String[3];// 录音文件地址
    String[] voicePathCache;// 录音文件地址缓存;

    private MediaRecorder recorder;// 录音工具
    private MediaPlayer mPlayer;// 录音文件播放工具
    String voicedir = null;// 录音文件缓存地址

    // 点击图片放大
    ImageView iv_showphoto;

    ProgressDialog voicePd;// 录音进度条
    int timer;// 录音时间(s)
    INoteDao noteDao;// 录音地址关联管理器
    NoteInfo noteInfo;// 营销笔记与录音地址关联类

    String noteId;// 营销笔记编号
    private IPicUpLoadAfterSavePath picDao;

    // 商品图片使用
    private DisplayImageOptions optionsByNonePic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_note);

        initView();
        initData();
        initListener();
    }

    private void initView() {
        btn_submit = (Button) findViewById(R.id.submit);
        btn_cancel = (Button) findViewById(R.id.cancel);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);

        goodsId = (TextView) findViewById(R.id.goodsId);
        goodsName = (TextView) findViewById(R.id.goodsName);
        name = (TextView) findViewById(R.id.name);
        num = (TextView) findViewById(R.id.num);
        mobile = (TextView) findViewById(R.id.mobile);
        address = (TextView) findViewById(R.id.address);
        remark = (TextView) findViewById(R.id.remark);
        tv_title = (TextView) findViewById(R.id.mytitle_textView);

        ly_goodsName = (LinearLayout) findViewById(R.id.ly_goodsName);
        ly_name = (LinearLayout) findViewById(R.id.ly_name);
        ly_num = (LinearLayout) findViewById(R.id.ly_num);
        ly_mobile = (LinearLayout) findViewById(R.id.ly_mobile);
        ly_address = (LinearLayout) findViewById(R.id.ly_address);
        ly_remark = (LinearLayout) findViewById(R.id.ly_remark);

        ly_voice_upload = (LinearLayout) findViewById(R.id.voice_upload_flag);
        ly_im_upload = (LinearLayout) findViewById(R.id.im_upload_flag);
        im_upload1 = (ImageView) findViewById(R.id.im_upload1);
        im_upload2 = (ImageView) findViewById(R.id.im_upload2);
        im_upload3 = (ImageView) findViewById(R.id.im_upload3);
        voice_upload1 = (ImageView) findViewById(R.id.voice_upload1);
        voice_upload2 = (ImageView) findViewById(R.id.voice_upload2);
        voice_upload3 = (ImageView) findViewById(R.id.voice_upload3);
    }

    /**
     * 初始化监听器
     */
    private void initListener() {

        /**
         * 确定和返回
         */
        btn_submit.setOnClickListener(mClickListener);
        btn_cancel.setOnClickListener(mClickListener);
        btn_back.setOnClickListener(mClickListener);
        if (null != delBtn) {
            delBtn.setOnClickListener(mClickListener);
        }

        // 图片
        for (int i = 0; i < im_upload.length; i++) {
            im_upload[i].setOnClickListener(imClickListener);
            im_upload[i].setOnLongClickListener(imLongClick);
        }

        // 录音
        for (int i = 0; i < voice_upload.length; i++) {
            voice_upload[i].setOnClickListener(voiceClickListener);
            voice_upload[i].setOnLongClickListener(voiceLongClick);
        }

        /**
         * 编辑区域
         */
        ly_goodsName.setOnClickListener(mClickListener);
        ly_name.setOnClickListener(mClickListener);
        ly_num.setOnClickListener(mClickListener);
        ly_mobile.setOnClickListener(mClickListener);
        ly_address.setOnClickListener(mClickListener);
        ly_remark.setOnClickListener(mClickListener);

        ly_im_upload.setOnClickListener(mClickListener);
        ly_voice_upload.setOnClickListener(mClickListener);
    }

    /**
     * 获取数据数组
     */
    private void initData() {

        optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);

        picDao = new PicUpLoadSavePathDao(AddNoteActivity.this);
        // 实例化管理器
        noteDao = new NoteDaoImpl(this);
        pullView();
        // 营销笔记的类型 0:增加,1:修改,2: 删除
        type = getIntent().getIntExtra(FinalUtil.ADDNOTE_TYPE, 0);
        tv_title.setText("营销笔记详细");
        if (type == RemarkListActivity.NOTE_TYPE_EDIT) { // 编辑
            delBtn = (Button) findViewById(R.id.mytitile_btn_right);
            delBtn.setVisibility(0);
            delBtn.setText("删除");
            // 填充值
            aminfo = (AgentMemoinfo) getIntent().getSerializableExtra(
                    FinalUtil.AGENTMEMOINFO);
            int pos = getIntent().getIntExtra(RemarkListActivity.ITEM_POSITION,
                    1);
            if (pos < 9) {
                goodsId.setText("记录 " + "0" + pos + ":");
            } else {
                goodsId.setText("记录 " + pos + ":");
            }
            goodsName.setText(aminfo.getProductName());
            name.setText(aminfo.getName());
            num.setText(aminfo.getIdCard());
            mobile.setText(aminfo.getMobile());
            address.setText(aminfo.getAddress());
            remark.setText(aminfo.getRemark());
            noteId = aminfo.getId();
            // 转换图片地址 目前不支持中文名称
            if (aminfo.getFilePath() != null
                    && aminfo.getFilePath().length() > 3) {
                PicUploadSavePathInfo info = picDao.getPicPathById(aminfo
                        .getId());
                if (null != info.getPicNavitePath()
                        && !TextUtils.isEmpty(info.getPicNavitePath().trim())) {
                    // 如果本地有保存图片地址,,,则先从本地加载
                    final String[] tempNativeS = info.getPicNavitePath().split(
                            ";");
                    final String netPicPath = aminfo.getFilePath();
                    final String[] tempS = netPicPath.split(";");
                    if (null != tempS) {
                        for (int i = 0; i < tempS.length; i++) {
                            if (null != tempS[i]) {
                                imagepathCache[i] = tempS[i];
                            } else {
                                continue;
                            }
                            // 本地有图片 并且在后台的保存路径中包含本地的同名文件
                            if (i < tempNativeS.length
                                    && null != tempNativeS[i] && imagepathCache[i].contains(givePathStringBySubString(tempNativeS[i]))) {
                                // 放大图片时使用
                                im_upload[i].setTag(tempNativeS[i]);
                                // 先从手机目录里找
                                final Bitmap temp = ImageUtil.loadImageFromSd(tempNativeS[i], application.getWidthPixels(),
                                        application.getHeightPixels(),
                                        false);
                                if (null == temp) {
                                    ImageUtil.getNetPicByUniversalImageLoad(findViewById(givePdId(i)), im_upload[i], imagepathCache[i],
                                            optionsByNonePic);
                                } else {
                                    im_upload[i].setImageBitmap(temp);
                                }
                            } else {
                                // 从网络中获取图片
                                ImageUtil.getNetPicByUniversalImageLoad(findViewById(givePdId(i)), im_upload[i], imagepathCache[i], optionsByNonePic);
                            }
                            // 获取图片
                        }
                    }
                } else {
                    final String[] tempS = aminfo.getFilePath().split(";");
                    if (null != tempS) {
                        for (int i = 0; i < tempS.length; i++) {
                            if (null != tempS[i]) {
                                imagepathCache[i] = tempS[i];
                            } else {
                                continue;
                            }
                            // 获取图片
                            /*
                             * ImageUtil.getNetPic(AddNoteActivity.this, findViewById(givePdId(i)), im_upload[i], imagepathCache[i],
                             * R.drawable.none_pic);
                             */

                            ImageUtil.getNetPicByUniversalImageLoad(findViewById(givePdId(i)), im_upload[i], imagepathCache[i], optionsByNonePic);
                        }
                    }
                }
            }

            // 从本地数据库得到录音信息
            noteInfo = noteDao.getNote(application.getUid(), noteId);
            // 转换录音地址
            if (noteInfo != null && noteInfo.getFilePath().length() > 3) {
                voicePathCache = noteInfo.getFilePath().split(";");
                // 批量设置录音信息
                for (int i = 0; i < voicePathCache.length; i++) {
                    // 修改录音状态为有文件
                    voice_ishave[i] = true;
                    // 设置封面
                    voice_upload[i].setImageResource(R.drawable.anzhishichang);
                    // 填充到文件列表
                    voicePaths[i] = voicePathCache[i];
                }
            }
        } else if (type == RemarkListActivity.NOTE_TYPE_ADD) {
            // 获取上一activity传入的商品名称
            final String name = getIntent().getStringExtra(
                    FinalUtil.ADDNOTE_GOODNAME);
            goodsName.setText(null == name ? "" : name);
        }
    }

    /**
     * @author caosq 2013-6-18 下午4:48:15
     * @param i
     * @return
     */
    private int givePdId(int i) {
        int _id = 0;
        if (i == 0) {
            _id = R.id.iv_pro_bar_0;
        } else if (i == 1) {
            _id = R.id.iv_pro_bar_1;
        } else {
            _id = R.id.iv_pro_bar_2;
        }
        return _id;
    }

    /**
     * 把布局文件放入集合
     * 
     * @author boge
     * @time 2013-3-25 void
     * 
     */
    public void pullView() {
        im_upload[0] = im_upload1;
        im_upload[1] = im_upload2;
        im_upload[2] = im_upload3;
        voice_upload[0] = voice_upload1;
        voice_upload[1] = voice_upload2;
        voice_upload[2] = voice_upload3;
    }

    /**
     * View单击监听器
     */
    private OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btn_submit) {// 确定
                update();
            } else if (v == btn_cancel || v == btn_back) {
                setResult(Activity.RESULT_CANCELED);
                AddNoteActivity.this.finish(); // finish返回
            } else if (v == delBtn) {// 删除记录
                Builder builder = new AlertDialog.Builder(AddNoteActivity.this);
                builder.setTitle("删除此备忘录?");
                builder.setPositiveButton("删除",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                del();
                            }
                        });
                builder.setNegativeButton("取消", null);
                builder.create().show();
            } else if (v == ly_goodsName) { // 商品名称
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, goodsName.getText().toString());
                mShowDialog(DIALOG_ID_INTRODUCE, bundle);
            } else if (v == ly_name) { // 用户名称
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, name.getText().toString());
                mShowDialog(DIALOG_ID_name, bundle);
            } else if (v == ly_num) { //  身份证
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, num.getText().toString());
                mShowDialog(DIALOG_ID_num, bundle);
            } else if (v == ly_mobile) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, mobile.getText().toString());
                mShowDialog(DIALOG_ID_mobile, bundle);
            } else if (v == ly_address) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, address.getText().toString());
                mShowDialog(DIALOG_ID_address, bundle);
            } else if (v == ly_remark) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, remark.getText().toString());
                mShowDialog(DIALOG_ID_remark, bundle);
            } else if (v == ly_im_upload) {// 上传图片 要判断左边是否已经有三张图片了
                if (null != imagepathCache[0] && null != imagepathCache[1]
                        && null != imagepathCache[2]) {
                    showToast("已选择的图片可以长按删除,最多只能上传三张图片!");
                    return;
                }
                mShowDialog(DIALOG_ID_IMAGE);
            } else if (v == ly_voice_upload) {// 上传语音 要判断左边是否已经有三段语音了
                if (voice_ishave[0] && voice_ishave[1] && voice_ishave[2]) {
                    showToast("已选择的语音可以长按删除,最多只能上传三段语音!");
                    return;
                }
                ly_voice_upload.setEnabled(false);
                mShowDialog(DIALOG_ID_VOICE);
                handler.post(r);
            }
        }
    };

    /**
     * 图片点击放大
     */
    OnClickListener imClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            for (int i = 0; i < im_upload.length; i++) {
                if (v == im_upload[i]) {
                    if (imagepathCache[i] != null) {
                        int _id = givePdId(i);
                        // 依据加载图标判断是否加载完成
                        if (findViewById(_id).getVisibility() == 0) {
                            showToast("未下载完成,请等待...");
                            return;
                        }
                        showPhoto(i, v);
                        return;
                    }
                }
            }
        }
    };

    /**
     * 图片 长按删除
     */
    private OnLongClickListener imLongClick = new OnLongClickListener() {
        @Override
        public boolean onLongClick(final View v) {
            for (int i = 0; i < im_upload.length; i++) {
                if (v == im_upload[i]) {
                    if (null != imagepathCache[i]) {
                        final int j = i;
                        Builder builder = new AlertDialog.Builder(
                                AddNoteActivity.this);
                        builder.setTitle("提示");
                        builder.setMessage("是否移除该文件?");
                        builder.setNegativeButton(R.string.cancel, null);
                        builder.setPositiveButton(R.string.submit,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        // 更换图标
                                        im_upload[j]
                                                .setImageResource(R.drawable.app_panel_add_icon_normal);
                                        // 清空缓存
                                        imageBit[j] = null;
                                        // 图片状态为已修改
                                        image_change[j] = true;
                                        imagepathCache[j] = null;
                                        return;

                                    }
                                });
                        builder.create().show();
                    }
                }
            }
            return false;
        }
    };

    /**
     * 录音点击
     */
    OnClickListener voiceClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            for (int i = 0; i < voice_upload.length; i++) {
                if (v == voice_upload[i]) {
                    if (voice_ishave[i] && voicePaths[i] != null) {
                        playVoice(voicePaths[i]);
                        return;
                    }
                }
            }
        }
    };

    /**
     * 录音
     */
    private OnLongClickListener voiceLongClick = new OnLongClickListener() {

        @Override
        public boolean onLongClick(final View v) {
            for (int j = 0; j < voice_upload.length; j++) {
                if (v == voice_upload[j]) {
                    if (voice_ishave[j]) {
                        final int i = j;
                        Builder builder = new AlertDialog.Builder(
                                AddNoteActivity.this);
                        builder.setTitle("提示");
                        builder.setMessage("是否移除该文件?");
                        builder.setNegativeButton(R.string.cancel, null);
                        builder.setPositiveButton(R.string.submit,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        voice_upload[i]
                                                .setImageResource(R.drawable.app_panel_add_icon_normal);
                                        voice_ishave[i] = false;
                                        voicePaths[i] = null;
                                        return;
                                    }

                                });

                        builder.create().show();
                    }
                }
            }
            return false;
        }
    };

    /**
     * dialog单选以及确定、取消按钮监听
     */
    private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (dialog != null) {
                // 取消浮层
                dialog.cancel();
            }
            if (currentDialogId == DIALOG_ID_IMAGE) {// 上传图片
                doSelection(which);// 拍照还是从图库选择图片
            } else {
                setTextView(which);// 赋值
            }
        }
    };

    /**
     * 创建新的Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        // 自定义builder用于弹出框
        // Builder builder = new AlertDialog.Builder(AddNoteActivity.this);
        switch (id) {
        case DIALOG_ID_INTRODUCE:// 笔记标题
            CustomDialog.Builder customBuilder = new CustomDialog.Builder(
                    setCreateContext());
            LayoutInflater flater = LayoutInflater.from(this);
            dialogContentView = flater.inflate(R.layout.dialog_editview, null);
            et_dialog = (EditText) dialogContentView
                    .findViewById(R.id.et_dialog);
            et_dialog.setText(goodsName.getText());
            et_dialog.selectAll();
            et_dialog.setLines(2);
            // et_dialog.setKeyListener(new EditTextKeyListener(
            // EditTextKeyListener.EMAIL_TYPE));
            et_dialog.addTextChangedListener(new TextCountLimitWatcher(40,
                    et_dialog));
            et_dialog.setHint(R.string.please_input);
            Util.setEditCursorToTextEnd(et_dialog);
            customBuilder.setTitle("笔记标题");
            customBuilder.setContentView(dialogContentView);
            customBuilder.setPositiveButton(R.string.submit,
                    mDialogClickListener);
            customBuilder.setNegativeButton(R.string.cancel, null);
            return customBuilder.create();
        case DIALOG_ID_VOICE:// 语音
            voicedir = Util.getVoiceDir()
                    + String.valueOf(new Date().getTime()) + ".amr";
            record(voicedir);
            voicePd = new ProgressDialog(this);
            // 设置进度条风格
            voicePd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            // 设置ProgressDialog 标题
            voicePd.setTitle("录音");
            // 设置ProgressDialog 提示信息
            voicePd.setMessage("已开始录音,点击确定保存,点击取消不保存,最长录音180秒!");
            // 设置ProgressDialog 标题图标
            voicePd.setIcon(android.R.drawable.ic_dialog_alert);
            // 设置ProgressDialog的最大进度
            voicePd.setMax(180);
            // 设置ProgressDialog 的一个Button
            voicePd.setButton("确定", new ProgressDialog.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    recordEnd();
                    saveVoice();
                    if (voicePd.isShowing()) {
                        voicePd.dismiss();
                    }
                }
            });
            voicePd.setButton3("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    recordEnd();
                    if (recorder != null) {
                        recorder.stop();// 停止录音
                        recorder.release();// 释放资源
                    }
                    if (voicePd.isShowing()) {
                        voicePd.dismiss();
                    }
                }
            });
            // 设置ProgressDialog 是否可以按退回按键取消
            voicePd.setCancelable(false);
            // 显示
            voicePd.show();
            break;
        default:
            break;
        }
        return super.onCreateDialog(id);
    }

    /**
     * 从Dialog给textview赋值
     * 
     * @param index
     */
    private void setTextView(int index) {
        switch (currentDialogId) {
        case DIALOG_ID_INTRODUCE: // 商品名称
            goodsName.setText(et_dialog.getText());
            break;
        case DIALOG_ID_name:
            name.setText(et_dialog.getText());
            break;
        case DIALOG_ID_num:
            num.setText(et_dialog.getText());
            break;
        case DIALOG_ID_mobile:
            mobile.setText(et_dialog.getText());
            break;
        case DIALOG_ID_address:
            address.setText(et_dialog.getText());
            break;
        case DIALOG_ID_remark:
            remark.setText(et_dialog.getText());
            break;
        }
    }

    /**
     * 验证非空
     * 
     * @author wubo
     * @time 2013-3-14
     * @param check
     * @param toastText
     * @return
     * 
     */
    public boolean checkEmpty(String check, final String toastText) {
        if (check == null || check.equals("")) {
            showToast(toastText);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 提交数据到后台
     * 
     * @author wubo
     * @time 2013-3-14
     * 
     */
    public void update() {
        if (goodsName.getText().toString().trim().equals("")) {
            showToast("商品名称不能为空!");
            return;
        }
        if (name.getText().toString().equals("")) {
            showToast("用户姓名不能为空!");
            return;
        }

        if (mobile.getText().toString().equals("")) {
            showToast("手机号码不能为空!");
            return;
        }
        if (!Util.isCellphone(mobile.getText().toString())) {
            showToast("手机号码格式不正确!");
            return;
        }

        if (num.getText().toString().length() != 15
                && num.getText().toString().length() != 18) {
            showToast("证件号码长度为15位或者18位!");
            return;
        }
        try {
            // 与后台交互
            ServerSupportManager serverMana = new ServerSupportManager(this,
                    this);
            List<Parameter> paras = new ArrayList<Parameter>();
            paras.add(new Parameter("uid", String.valueOf(application.getUid())));
            paras.add(new Parameter("type", String.valueOf(type)));
            if (aminfo != null) {
                paras.add(new Parameter("id", aminfo.getId()));
            }
            paras.add(new Parameter("name", name.getText().toString()));
            paras.add(new Parameter("idCard", num.getText().toString()));
            paras.add(new Parameter("address", address.getText().toString()));
            paras.add(new Parameter("productName", goodsName.getText()
                    .toString()));
            paras.add(new Parameter("mobile", mobile.getText().toString()));
            paras.add(new Parameter("remark", remark.getText().toString()));
            StringBuffer picPath = new StringBuffer();
            for (int i = 0; i < image_change.length; i++) {
                if (imagepathCache.length > i) {
                    if (imagepathCache[i] != null) {
                        // if (imagepathCache[i].contains("/SRMC/agent")) {
                        // picPath.append(imagepathCache[i] + ";");
                        // } else {
                        String a = givePathStringBySubString(imagepathCache[i]);
                        picPath.append(a + ";");
                        // }
                    }
                }
            }

            if (picPath.length() > 0) {
                paras.add(new Parameter("picPath", picPath.toString()
                        .substring(0, picPath.length() - 1)));
            } else {
                // paras.add(new Parameter("picPath", "abcd"));
            }
            Log.e("Debug", "picPaht:" + picPath.toString());
            ArrayList<MyFilePart> uploads = new ArrayList<MyFilePart>();
            for (int i = 0; i < imagepathCache.length; i++) {
                File file = null;
                tempPhotoPath = imagepathCache[i];
                // 用户上传原来的三个地址 比较新上传的地址 把不同地址的图片删除
                if (null != tempPhotoPath
                        && !tempPhotoPath.contains("/SRMC/agent")) {// 不包涵后台目录地址就是更改的图片
                    // 如果图片 过大,就要压缩,否则下载时会内存泄露
                    file = new File(tempPhotoPath);
                    // if (image_ishave[i] && image_change[i]) {// 有图片,且没有被修改过
                    // file = getCompress(75, 75, 100, imageBit[i]);// 把图片缓存变为图片
                    // }
                    if (file.exists()) {
                        // 限制图片大小
                        // double picSize = file.length();
                        // if ((picSize / 1000) > FinalUtil.PIC_UPLOAD_BIG_SIZE) {
                        // Bitmap bm = ImageUtil
                        // .loadImageFromSd(tempPhotoPath);
                        // double z = (picSize / 1000)
                        // / FinalUtil.PIC_UPLOAD_BIG_SIZE;
                        // bm = ImageUtil.zoomImage(bm,
                        // bm.getWidth() / Math.sqrt(z),
                        // bm.getHeight() / Math.sqrt(z));
                        // ImageUtil.saveUserIcon(bm, tempPhotoPath);
                        // }
                        if (file != null) {// 如果文件存在,准备上传
                            String a = tempPhotoPath.substring(
                                    tempPhotoPath.lastIndexOf("/") + 1,
                                    tempPhotoPath.length());
                            MyFilePart uploader1 = new MyFilePart(i + "", a,
                                    file,
                                    new MimetypesFileTypeMap()
                                            .getContentType(file),
                                    SyncHttpClient.CONTENT_CHARSET);
                            uploads.add(uploader1);
                        }
                    }
                }
            }
            Log.e("Debug", "uploads size:" + uploads.size());
            serverMana.supportRequest(AgentConfig.MemoManger(), paras, uploads,
                    true, "提交中,请稍等 ....", comm_code);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * 截取最后一个"/"的路径名称
     * 
     * @author caosq 2013-6-24 上午11:11:47
     * @param i
     * @return
     */
    private String givePathStringBySubString(String temp) {
        return temp.substring(
                temp.lastIndexOf("/") + 1,
                temp.length());
    }

    /**
     * 删除笔记
     * 
     * @author boge
     * @time 2013-3-26 void
     * 
     */
    public void del() {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", String.valueOf(application.getUid())));
        paras.add(new Parameter("type", "2"));
        paras.add(new Parameter("id", noteId));
        serverMana.supportRequest(AgentConfig.MemoManger(), paras, false,
                "提交中,请稍等 ....", del_code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse))
            return;
        switch (caseKey) {
        case comm_code: // 提交结果 如果成功,则在本地保存一份 提交图片的手机路径
            MemoManageResult baseResult = JSONUtil.fromJson(
                    supportResponse.toString(), MemoManageResult.class);
            if (baseResult != null) {
                showToast(baseResult.errorMessage);
                String voicePath = "";
                for (int i = 0; i < voice_ishave.length; i++) {
                    if (voice_ishave[i]) {
                        voicePath += voicePaths[i] + ";";
                    }
                }
                if (voicePath.length() > 0) {
                    voicePath = voicePath.substring(0, voicePath.length() - 1);
                }
                if (type == RemarkListActivity.NOTE_TYPE_ADD) {
                    noteId = baseResult.getMemoId();
                }
                NoteInfo note = new NoteInfo(application.getUid(), noteId,
                        voicePath);
                PicUploadSavePathInfo info = new PicUploadSavePathInfo();
                info.setPicNavitePath(giveNativePath());
                info.setPicChangeTime(String.valueOf(System.currentTimeMillis()));
                info.setServer_Db_Id(note.getNoteId());
                picDao.saveOrUpdataPicPathByID(info);
                noteDao.unionNote(note);
                setResult(Activity.RESULT_OK);
                finish();
            } else {
                showToast(R.string.to_server_fail);
                return;
            }

        case del_code:
            BaseResult r2 = JSONUtil.fromJson(supportResponse.toString(),
                    BaseResult.class);
            if (r2 != null) {
                finish();
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }

    /**
     * 合成手机本地图片地址
     * 
     * @author caosq 2013-6-18 下午7:30:12
     * @return
     */
    private String giveNativePath() {
        StringBuffer buffer = new StringBuffer();
        PicUploadSavePathInfo info = picDao.getPicPathById(noteId);
        if (image_change[0] || image_change[1] || image_change[2]) {// 有图片更改了
            // 先存储原来的本地地址
            String[] tmepS = new String[3];
            if (null != info.getPicNavitePath()) {
                String[] aStrings = info.getPicNavitePath().split(";");
                for (int i = 0; i < aStrings.length; i++) {
                    tmepS[i] = aStrings[i];
                }
            }
            // 把更改的位置替换
            for (int i = 0; i < image_change.length; i++) {
                if (image_change[i]) {
                    tmepS[i] = imagepathCache[i];
                }
                if (null != tmepS[i]) {
                    buffer.append(tmepS[i] + ";");
                }
            }
        } else {
            buffer.append(null == info.getPicNavitePath() ? "" : info
                    .getPicNavitePath());
        }
        return buffer.toString();
    }

    /**
     * 录音
     * 
     * @author boge
     * @time 2013-3-25 void
     * @param filePath
     * 
     */
    public void record(String filePath) {
        recorder = new MediaRecorder();// new出MediaRecorder对象
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        // 设置MediaRecorder的音频源为麦克风
        recorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
        // 设置MediaRecorder录制的音频格式
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        // 设置MediaRecorder录制音频的编码为amr.
        recorder.setOutputFile(filePath);
        // 设置录制好的音频文件保存路径
        try {
            recorder.prepare();// 准备录制
            recorder.start();// 开始录制
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 播放语音
     * 
     * @author wubo
     * @time 2013-3-22
     * @param filePath
     * 
     */
    public void playVoice(String filePath) {
        mPlayer = new MediaPlayer();
        mPlayer.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                if (voicePd.isShowing()) {
                    voicePd.dismiss();
                }
            }
        });
        voicePd = new ProgressDialog(this);
        // 设置ProgressDialog 标题
        voicePd.setTitle("录音");
        // 设置ProgressDialog 提示信息
        voicePd.setMessage("正在播放录音...");
        // 设置ProgressDialog 标题图标
        voicePd.setIcon(android.R.drawable.ic_dialog_alert);
        // 设置ProgressDialog的最大进度
        // 设置ProgressDialog 的一个Button
        voicePd.setButton("取消", new ProgressDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mPlayer.isPlaying()) {
                    mPlayer.stop();
                }
                voicePd.dismiss();
            }
        });
        // 设置ProgressDialog 是否可以按退回按键取消
        voicePd.setCancelable(false);
        // 显示
        voicePd.show();
        try {
            // 设置要播放的文件
            mPlayer.setDataSource(filePath);
            mPlayer.prepare();
            // 播放
            mPlayer.start();

        } catch (IOException e) {
        }
    }

    /**
     * 放大图片
     * 
     * @author boge
     * @param v
     * @time 2013-3-25 void
     * 
     */
    public void showPhoto(int index, View v) {
        Dialog builder = new Dialog(this, R.style.Dialog_Fullscreen);
        builder.setContentView(R.layout.dialog_imageview);
        iv_showphoto = (ImageView) builder.findViewById(R.id.iv_showphoto);
        if (image_change[index]) {
            iv_showphoto.setImageBitmap(imageBit[index]);
        } else {
            String nativePath = (String) v.getTag();
            // 先从手机目录里找
            final Bitmap temp = ImageUtil.loadImageFromSd(nativePath, application.getWidthPixels(), application.getHeightPixels(),
                    false);
            if (null == temp) {
                ImageUtil.getNetPicByUniversalImageLoad(null, iv_showphoto, imagepathCache[index],
                        optionsByNonePic);
            } else {
                iv_showphoto.setImageBitmap(temp);
            }
        }
        builder.show();
    }

    /**
     * 切割图片之后返回的 Bundle数据处理，取得bitmap类型图片
     * 
     * @param extras
     *            void
     */
    @Override
    public void createPhoto(Bundle extras) {
        if (extras != null) {
            photo = extras.getParcelable("data");
            try {
                if (photo != null) {
                    for (int i = 0; i < im_upload.length; i++) {
                        if (null == imagepathCache[i]) {
                            if (imageBit[i] != null) {
                                imageBit[i].recycle();
                            }
                            imageBit[i] = photo;
                            imagepathCache[i] = getPhotoPath();
                            im_upload[i].setImageBitmap(imageBit[i]);
                            image_change[i] = true;
                            return;
                        }
                    }
                } else {
                    showToast(R.string.res_get_photo_fail);
                }
            } catch (Exception e) {
                e.printStackTrace();
                showToast(R.string.res_get_photo_fail);
            }
        }
    }

    @Override
    protected android.content.DialogInterface.OnClickListener setDialogClickListener() {
        return mDialogClickListener;
    }

    // 该方法的内部类将在handler.sendMessage(msg)后执行
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            timer += 1;
            voicePd.setProgress(timer);
            if (timer >= 180) {
                recordEnd();
                if (voicePd != null) {
                    voicePd.dismiss();
                }
                saveVoice();
            }
        }
    };

    Runnable r = new Runnable() {
        @Override
        public void run() {
            // 得到一个消息对象，Message类是有Android系统
            Message msg = handler.obtainMessage();
            // 将消息加入到另外一个消息队列中去
            handler.sendMessage(msg);
            // 1000毫秒后加入线程到消息队列中
            handler.postDelayed(r, 1000);
        }
    };

    /**
     * 录音结束
     * 
     * @author wubo
     * @time 2013-3-22
     * 
     */
    public void recordEnd() {
        handler.removeCallbacks(r);
        timer = 0;
        ly_voice_upload.setEnabled(true);
    }

    /**
     * 保存录音
     * 
     * @author wubo
     * @time 2013-3-22
     * 
     */
    public void saveVoice() {
        if (recorder != null) {
            recorder.stop();// 停止录音
            recorder.release();// 释放资源
        }

        for (int i = 0; i < voice_upload.length; i++) {
            if (!voice_ishave[i]) {
                voicePaths[i] = voicedir;
                voice_ishave[i] = true;
                voice_upload[i].setImageResource(R.drawable.anzhishichang);
                return;
            }

        }
    }

    @Override
    protected Context setCreateContext() {
        return AddNoteActivity.this;
    }
}
