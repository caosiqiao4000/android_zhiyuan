package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.WithDrawAdapter;
import com.gsta.v2.activity.myshop.MyWithdrawActivity.WithdrawSearchListener;
import com.gsta.v2.entity.WithDrawInfo;
import com.gsta.v2.response.WithDrawResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 
 * @author longxianwen
 * @createTime Apr 9, 2013 6:19:18 PM
 * @version: 1.0
 * @desc:提现明细
 */
public class WithdrawDetailActivity extends BaseActivity implements
        IUICallBackInterface, WithdrawSearchListener {

    private String TAG = Util.getClassName();
    private Context context = WithdrawDetailActivity.this;
    private PullToRefreshListView pl_withdrawdetail;
    private ListView withdrawDetailListView;
    private WithDrawAdapter withDrawAdapter;
    private List<WithDrawInfo> wdInfos = new ArrayList<WithDrawInfo>();

    // private Button btn_loadmore;
    private View loadmoreView;
    private ProgressBar loadmroe_pb;
    private int pageSize = 0; // 分页
    private int totalSize = 20;

    private MyWithdrawActivity myWithdraw;
    // 搜索条件
    private String accountStr = ""; // 账号
    private String startimeStr = ""; // 开始时间
    private String endtimeStr = ""; // 结束时间
    private String status = ""; // 状态

    // private boolean flag = true;

    final int load_querycode = 11; // 数据查询标识

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.withdrawd);
        initView();
        initData();
        initListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        myWithdraw = (MyWithdrawActivity) this.getParent();
        myWithdraw.setWithdrawSearchListener(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (null != myWithdraw && keyCode == KeyEvent.KEYCODE_BACK) {
            myWithdraw.finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initListener() {
        pl_withdrawdetail.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageSize = 0;
                if (wdInfos.size() > 0) {
                    wdInfos.clear();
                    withDrawAdapter.notifyDataSetInvalidated();
                }
                load();
            }
        });
    }

    private void initView() {
        pl_withdrawdetail = (PullToRefreshListView) findViewById(R.id.pl_withdrawdetail);
        withdrawDetailListView = pl_withdrawdetail.getRefreshableView();

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        loadmoreView = inflater.inflate(R.layout.loadmore, null);
        Button btn_loadmore = (Button) loadmoreView.findViewById(R.id.loadMoreButton);
        btn_loadmore.setVisibility(View.GONE);
        btn_loadmore.setOnClickListener(myClickListener);

        loadmroe_pb = (ProgressBar) loadmoreView.findViewById(R.id.loadmroe_pb);
    }

    private void initData() {
        withDrawAdapter = new WithDrawAdapter(context, wdInfos);
        withdrawDetailListView.setAdapter(withDrawAdapter);
        load();
    }

    /**
     * 按钮单击事件
     */
    OnClickListener myClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.loadMoreButton:
                loadmroe_pb.setVisibility(View.VISIBLE);
                load();
                break;
            }
        }
    };

    // 提现明细-请求服务器
    private void load() {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);

        List<Parameter> paras = new ArrayList<Parameter>();
        Log.d(TAG, this.accountStr + "," + this.startimeStr + "," + this.endtimeStr + "," + this.status);

        if (this.status.equals(MyWithdrawActivity.ALL_FLAG)) {
            pageSize = 0;
            if (wdInfos.size() > 0) {
                wdInfos.clear();
                withDrawAdapter.notifyDataSetInvalidated();
            }
            // flag = true;
        } else {
            // 提现状态：0-待处理,1-已转账,2-申请驳回
            paras.add(new Parameter("status", status));// 提现状态
        }

        paras.add(new Parameter("agentUid", application.getUid()));
        // if (flag) {
        // paras.add(new Parameter("limit", pageSize + "," + totalSize));
        // } else {
        // 搜索
        pageSize = 0;
        if (wdInfos.size() > 0) {
            wdInfos.clear();
            withDrawAdapter.notifyDataSetInvalidated();
        }
        paras.add(new Parameter("account", accountStr));// 关联账号
        paras.add(new Parameter("startTime", startimeStr)); // 2013-04-16
        paras.add(new Parameter("endTime", endtimeStr));
        paras.add(new Parameter("limit", pageSize + "," + totalSize));
        // flag = true;
        // }

        serverMana.supportRequest(AgentConfig.getWithdrawUrl(), paras, true,
                "提交中,请稍等 ....", load_querycode);

    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {

        pl_withdrawdetail.onRefreshComplete();
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }

        switch (caseKey) {
        case load_querycode:
            WithDrawResult wdr = JSONUtil.fromJson(supportResponse.toString(),
                    WithDrawResult.class);

            if (wdr != null) {
                if (null != wdr.getList() && wdr.getList().size() > 0) {
                    wdInfos.addAll(wdr.getList());
                    if (wdr.gettCount() <= wdInfos.size()) {
                        withdrawDetailListView.removeFooterView(loadmoreView);
                    } else {
                        pageSize = (int) (wdr.gettCount() - wdr.getList().size());
                        loadmroe_pb.setVisibility(View.GONE);
                        if (withdrawDetailListView.getFooterViewsCount() == 0) {
                            withdrawDetailListView.addFooterView(loadmoreView);
                        }
                    }
                    withDrawAdapter.setWdInfos(wdInfos);
                } else {
                    // btn_loadmore.setVisibility(View.GONE);
                    // loadmroe_pb.setVisibility(View.VISIBLE);
                    // rr_loadmore.setVisibility(View.GONE);
                    // showToast("没有为您搜索到更多的记录!");
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }

    @Override
    public void onSearch(String accountStr, String startimeStr,
            String endtimeStr, String type, String status) {
        this.accountStr = accountStr;
        this.startimeStr = startimeStr;
        this.endtimeStr = endtimeStr;
        this.status = status;
        if (null != wdInfos && wdInfos.size() > 0) {
            wdInfos.clear();
            withDrawAdapter.notifyDataSetInvalidated();
        }
        pageSize = 0;
        load();
    }
}
