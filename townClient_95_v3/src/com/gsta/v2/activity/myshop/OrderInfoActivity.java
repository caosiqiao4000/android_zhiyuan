package com.gsta.v2.activity.myshop;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.OrderInfoAdapter;
import com.gsta.v2.entity_v2.OrderWareInfo;
import com.gsta.v2.util.FinalUtil;

/**
 * 我的店铺  订单信息
 * 
 * @author boge
 * @time 2013-3-27
 * 
 */
public class OrderInfoActivity extends BaseActivity {

	@SuppressWarnings("unused")
    private Button btn_search;
	private ListView lv1;
	private OrderInfoAdapter adapter;
	private ArrayList<OrderWareInfo> orders;// 订单信息

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.orderinfo);
		initView();
		initData();
		initListener();
	}

	private void initView() {
		lv1 = (ListView) findViewById(R.id.lv_1);
	}

	@SuppressWarnings("unchecked")
    private void initData() {
		// 订单信息通过传递得到
		orders = (ArrayList<OrderWareInfo>) getIntent().getSerializableExtra(
				FinalUtil.ORDERINFO);
		if (orders == null) {
			orders = new ArrayList<OrderWareInfo>();
		}
		adapter = new OrderInfoAdapter(this, orders);
		lv1.setAdapter(adapter);
	}

	private void initListener() {

	}
}
