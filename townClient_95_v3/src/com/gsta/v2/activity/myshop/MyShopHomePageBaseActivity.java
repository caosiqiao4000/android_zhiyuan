package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 我的店铺首页 子项共用
 * 
 * @author caosq
 * 
 */
public abstract class MyShopHomePageBaseActivity extends BaseActivity implements IUICallBackInterface {

    /**
     * 商品查询
     * 
     * @author wubo
     * @time 2013-1-23
     * @param casekey
     *            我的店铺热销接口
     * @param needOther
     *            是否需要除 UID以外的参数
     */
    protected void productQuery(int casekey, MyShopQuery query,boolean needPd) {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        // paras.add(new Parameter("type", query.getQueryType())); // 商品类别 0,合约机;1,光宽带;2,手机卡;3,其他;
        // 默认为 0
        paras.add(new Parameter("agentUid", query.getAgentUid()));
        paras.add(new Parameter("limit", query.getPageIndex() + ",10"));// 当前从哪一条开始查询
        // // 默认为1
        serverMana.supportRequest(AgentConfig.PromotionQuery(), paras, needPd,
                "加载中,请稍等 ...", casekey);
    }

    protected class MyShopQuery {
        private String agentUid;
        private String queryType; // 商品类别 0,合约机;1,光宽带;2,手机卡;3,礼包; 默认为 0 热销是999
        private String provinceId;
        private String cityId;
        private String minPrice;
        private String maxPrice;
        private String keywords; // 关键字
        private int pageSize; // 分页大小，默认为9
        private int pageIndex = 1;// 当前页数 默认为1 当前从哪一条开始查询
        private String orderBy;

        public String getAgentUid() {
            return agentUid;
        }

        public void setAgentUid(String agentUid) {
            this.agentUid = agentUid;
        }

        public String getQueryType() {
            return queryType;
        }

        public void setQueryType(String queryType) {
            this.queryType = queryType;
        }

        public String getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(String provinceId) {
            this.provinceId = provinceId;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getMinPrice() {
            return minPrice;
        }

        public void setMinPrice(String minPrice) {
            this.minPrice = minPrice;
        }

        public String getMaxPrice() {
            return maxPrice;
        }

        public void setMaxPrice(String maxPrice) {
            this.maxPrice = maxPrice;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getPageIndex() {
            return pageIndex;
        }

        public void setPageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
        }

        public String getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(String orderBy) {
            this.orderBy = orderBy;
        }
    }
}
