package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.IndentAgentListQueryAdapter;
import com.gsta.v2.activity.mytown.MyUserIndentQueryActivity;
import com.gsta.v2.entity_v2.Limit;
import com.gsta.v2.entity_v2.OrderInfo;
import com.gsta.v2.entity_v2.OrderResult;
import com.gsta.v2.entity_v2.OrderWareInfo;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 订单查询
 * 
 * @author boge
 * @time 2013-3-27
 * 
 */
public class OrderQueryActivity extends BaseActivity implements
        IUICallBackInterface {
    private Button btn_search, btn_back;// 搜索
    private TextView titleView;

    private PullToRefreshListView pl_0;
    private ListView listView; //
    //
    private View loadMoreView;

    private IndentAgentListQueryAdapter adapter;
    private List<OrderInfo> orders = new ArrayList<OrderInfo>();
    private QueryCondition indexQuery;

    private View noData;// 没有订单
    private Calendar nowCal;
    private Calendar beginCal;
    private Calendar endCal;
    private boolean isFrist;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            View view = (View) msg.obj;
            view.setVisibility(View.GONE);
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.orderquery);
        nowCal = Calendar.getInstance();
        beginCal = Calendar.getInstance();
        endCal = Calendar.getInstance();
        isFrist = true;
        initView();
        initData();
        initListener();
    }

    private void initView() {
        btn_search = (Button) findViewById(R.id.mytitile_btn_right);
        btn_search.setVisibility(0);
        btn_search.setText(R.string.MyShopActivity_query);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);
        titleView = (TextView) findViewById(R.id.mytitle_textView);

        pl_0 = (PullToRefreshListView) findViewById(R.id.pr_1);
        listView = (ListView) pl_0.getRefreshableView();
        noData = (LinearLayout) findViewById(R.id.no_data);
        ((TextView) noData.findViewById(R.id.search)).setText("还没有订单哦,赶紧去下单吧...");
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        loadMoreView = inflater.inflate(R.layout.loadmore, null);
        Button btn_loadmore = (Button) loadMoreView.findViewById(R.id.loadMoreButton);
        btn_loadmore.setVisibility(0);
        // 加载更多
        btn_loadmore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getAgentOrder(true);
            }
        });
        listView.setDividerHeight(0);
    }

    private void initData() {
        indexQuery = new QueryCondition();
        indexQuery.setLimit(new Limit(1, 10));
        indexQuery.setRefresh(true);
        indexQuery.setUid(application.getUid());
        indexQuery.setNeedTime(false);
        indexQuery.setAgentUid(application.getUid());
        indexQuery.setOrderStatus(MyUserIndentQueryActivity.QUERY_ALL);
        orders = new ArrayList<OrderInfo>();
        indexQuery.setQueryType(getIntent().getExtras().getString(FinalUtil.QUERY_TYPE));
        final String accout = getIntent().getExtras().getString(FinalUtil.ORDER_ACCOUNT);
        if (null != accout && accout.trim().length() > 0) {
            isFrist = false;
            indexQuery.setAccount(accout);
            titleView.setText(accout + "的订单");
        } else {
            titleView.setText(R.string.order_ALL);
        }
        // 初始化时间
        beginCal.set(nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH) - 1,
                nowCal.get(Calendar.DAY_OF_MONTH), 00, 00, 00);

        endCal.set(nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH),
                nowCal.get(Calendar.DAY_OF_MONTH), 23, 59, 59);

        indexQuery.setStartTime(Util.getFormatDate(beginCal.getTimeInMillis()));
        indexQuery.setEndTime(Util.getFormatDate(endCal.getTimeInMillis()));

        adapter = new IndentAgentListQueryAdapter(this, orders);
        listView.setAdapter(adapter);
        getAgentOrder(true);
    }

    private void initListener() {
        // 搜索条件页面
        btn_search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderQueryActivity.this,
                        OrderSearchActivity.class);
                intent.putExtra(FinalUtil.QUERY_TYPE, indexQuery.getQueryType());
                if (null != indexQuery.getStartTime()) {
                    intent.putExtra(FinalUtil.ORDER_BEGINTIME, indexQuery.getStartTime());
                }
                if (null != indexQuery.getEndTime()) {
                    intent.putExtra(FinalUtil.ORDER_ENDTIME, indexQuery.getEndTime());
                }
                if (null != indexQuery.getPhone()) {
                    intent.putExtra(FinalUtil.ORDER_PHONE, indexQuery.getPhone());
                }
                if (null != indexQuery.getAccount()) {
                    intent.putExtra(FinalUtil.ORDER_ACCOUNT, indexQuery.getAccount());
                }
                intent.putExtra(FinalUtil.ORDER_STATUS, indexQuery.getOrderStatus());
                startActivityForResult(intent, 0);
            }
        });

        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderQueryActivity.this.finish();
            }
        });

        pl_0.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                indexQuery.getLimit().setOffset(1);
                indexQuery.setRefresh(true);
                getAgentOrder(false);
            }
        });

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                Intent intent = new Intent(OrderQueryActivity.this,
                        OrderInfoActivity.class);
                List<OrderWareInfo> infos = orders.get(position).getOrderWareInfos();
                if (infos == null) {
                    showToast("该订单已作废!");
                } else {
                    intent.putExtra(FinalUtil.ORDERINFO, new ArrayList<OrderWareInfo>(infos));
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (resultCode == 0) {
            return;
        }
        if (resultCode == 1) {
            indexQuery.setAccount(data.getExtras().getString(FinalUtil.ORDER_ACCOUNT));
            indexQuery.setPhone(data.getExtras().getString(FinalUtil.ORDER_PHONE));
            indexQuery.setStartTime(data.getExtras().getString(FinalUtil.ORDER_BEGINTIME));
            indexQuery.setEndTime(data.getExtras().getString(FinalUtil.ORDER_ENDTIME));
            indexQuery.setOrderStatus(data.getExtras().getInt(FinalUtil.ORDER_STATUS));
            indexQuery.setRefresh(true);
            indexQuery.setNeedTime(true);
            // rr_loadmore.setVisibility(View.GONE);
            orders.clear();
            adapter.setItems(orders);
            indexQuery.getLimit().setOffset(1);
            switch (indexQuery.getOrderStatus()) {// 4.待确认收货,1.待付款,2.待发货,5.已成功,6.已取消, opType =1时必填
            case MyUserIndentQueryActivity.QUERY_UNCONFIRMED:
                titleView.setText(R.string.order_UNCONFIRMED);
                break;
            case MyUserIndentQueryActivity.QUERY_UNPAYMENT:
                titleView.setText(R.string.order_UNPAYMENT);
                break;
            case MyUserIndentQueryActivity.QUERY_UNDELIVERY:
                titleView.setText(R.string.order_UNDELIVERY);
                break;
            case MyUserIndentQueryActivity.QUERY_SUCCESS:
                titleView.setText(R.string.order_SUCCESS);
                break;
            case MyUserIndentQueryActivity.QUERY_CANCEL:
                titleView.setText(R.string.order_CANCEL);
                break;
            default:
                titleView.setText(R.string.order_ALL);
                break;
            }
            getAgentOrder(false);
        }
    }

    /**
     * 查询业绩
     * 
     * @author wubo
     * @createtime 2012-9-6
     */
    public void getAgentOrder(boolean needPd) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("agentUid", indexQuery.getAgentUid()));
        if (!isFrist) {
            if (null != indexQuery.getAccount() && indexQuery.getAccount().trim().length() > 0) {
                paras.add(new Parameter("account", indexQuery.getAccount()));
            }
            if (null != indexQuery.getPhone() && indexQuery.getPhone().trim().length() > 0) {
                paras.add(new Parameter("phone", indexQuery.getPhone()));
            }
            if (indexQuery.getOrderStatus() != null) {
                paras.add(new Parameter("orderStatus", String.valueOf(indexQuery.getOrderStatus())));
            }
            if (indexQuery.isNeedTime()) {
                paras.add(new Parameter("startTime", indexQuery.getStartTime()));
                paras.add(new Parameter("endTime", indexQuery.getEndTime()));
            }
        } else {
            isFrist = false;
        }
        paras.add(new Parameter("limit", indexQuery.getLimit().toString()));
        serverMana.supportRequest(AgentConfig.myQueryAgentOrder(), paras, needPd,
                "查询中,请稍等 ...", query_code);
    }

    private final int query_code = 999;

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse)) {
            if (pl_0.isRefreshing()) {
                pl_0.onRefreshComplete();
            }
            return;
        }
        if (pl_0.isRefreshing()) {
            pl_0.onRefreshComplete();
        }
        if (indexQuery.isRefresh()) {
            orders.clear();
            indexQuery.setRefresh(false);
        }
        switch (caseKey) {

        case query_code:
            OrderResult orderResult = JSONUtil.fromJson(supportResponse.toString(),
                    OrderResult.class);
            if (orderResult != null) {
                if (orderResult.getOrderInfos() != null && orderResult.getOrderInfos().size() > 0) {
                    orders.addAll(orderResult.getOrderInfos());
                    indexQuery.getLimit().setOffset(indexQuery.getLimit().getOffset() + indexQuery.getLimit().getLength());
                }
                adapter.setItems(orders);
                if (indexQuery.getLimit().getOffset() >= (null != orderResult.gettCount() ? orderResult.gettCount() : 0)) {
                    if (listView.getFooterViewsCount() > 0) {
                        listView.removeFooterView(loadMoreView);
                    }
                } else {
                    if (listView.getFooterViewsCount() == 0) {
                        listView.addFooterView(loadMoreView);
                    }
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            if (orders.size() <= 0) {
                showNoDataView(noData);
            }
            break;
        }
    }

    private void showNoDataView(View v) {
        v.setVisibility(0);
        final Message msg = Message.obtain(handler);
        msg.obj = v;
        handler.sendMessageDelayed(msg, 2000);
    }

    // 查询条件类
    final class QueryCondition {
        private String uid;
        private String agentUid;
        private boolean refresh; // 是否刷新 ture刷新 false加载下一页
        private Limit limit;
        private boolean needTime; // 是否需要时间
        private String productName;
        private String startTime;
        private String endTime;
        private String account;// 邮箱或者手机号码
        private String phone;// 查询的联系电S话
        // 4.待确认收货,1.待付款,2.待发货,5.已成功,6.已取消, opType =1时必填
        private Integer orderStatus;// 订单状态
        private String queryType;// 用户姿态状态

        public String getQueryType() {
            return queryType;
        }

        public void setQueryType(String queryType) {
            this.queryType = queryType;
        }

        public String getAgentUid() {
            return agentUid;
        }

        public void setAgentUid(String agentUid) {
            this.agentUid = agentUid;
        }

        public boolean isNeedTime() {
            return needTime;
        }

        public void setNeedTime(boolean needTime) {
            this.needTime = needTime;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public boolean isRefresh() {
            return refresh;
        }

        public void setRefresh(boolean refresh) {
            this.refresh = refresh;
        }

        public Limit getLimit() {
            return limit;
        }

        public void setLimit(Limit limit) {
            this.limit = limit;
        }
    }
}
