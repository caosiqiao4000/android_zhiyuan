package com.gsta.v2.activity.myshop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.R;
import com.gsta.v2.db.ICityDao;
import com.gsta.v2.db.impl.CityDaoImpl;
import com.gsta.v2.entity.City;
import com.gsta.v2.response.AccountBaseInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.WithDrawAccountInfo;
import com.gsta.v2.response.WithDrawResult;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.CameraAndShowDiagleActivity;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 
 * @author longxianwen
 * @createTime Apr 9, 2013 6:19:39 PM
 * @version: 1.0
 * @desc:我的账户
 */
public class MyAccountActivity extends CameraAndShowDiagleActivity implements
        IUICallBackInterface {

    private static final Logger logger = LoggerFactory.getLogger(MyAccountActivity.class);

    private TextView tv_name; // 账户名
    private TextView tv_identity_card; // 省份证号码
    private TextView tv_blance; // 账户余额
    private TextView tv_card_status; // 账户状态
    private Button btn_cash, btn_upload_pic; // 提现
    // tv_acctype 账户类型
    private TextView tv_acctype, tv_bank, tv_number, tv_myphone, tv_bandadd, tv_subbank;
    private EditText et_money;
    private LayoutInflater layoutInflater;
    private MyDialog activateVoucherDialog;

    private boolean[] imaChange = { false, false }; // 指示是否有更改
    private int indexImag = 0; // 选择哪张图片更改了
    private final int IMG_FORNT = 0x00AA;
    private final int IMG_VERSO = 0x00BB;
    private final int uploadPic_code = 0x00cc; // 上传图片
    private final int cashMoney_code = 0x00cd; // 提现
    private String[] imagPaht = { "", "" };

    private boolean isCash = false;// 是否可以提现
    private ICityDao cityDao;
    private MyWithdrawActivity myWithdraw; // 上级类

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);
        layoutInflater = LayoutInflater.from(MyAccountActivity.this);
        initView();
        initData();
    }

    private void initData() {
        queryMyAccount();
        cityDao = new CityDaoImpl(MyAccountActivity.this);
    }

    private void initView() {
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_identity_card = (TextView) findViewById(R.id.tv_identity_card);
        tv_blance = (TextView) findViewById(R.id.tv_blance);
        tv_card_status = (TextView) findViewById(R.id.tv_card_status);
        btn_cash = (Button) findViewById(R.id.btn_cash);
        btn_upload_pic = (Button) findViewById(R.id.btn_upload_pic);

        // tv_acctype,tv_bank,tv_number,tv_myphone,tv_bandadd,tv_subbank;
        tv_acctype = (TextView) findViewById(R.id.tv_acctype);
        tv_bank = (TextView) findViewById(R.id.tv_bank);
        tv_number = (TextView) findViewById(R.id.tv_number);
        tv_myphone = (TextView) findViewById(R.id.tv_myphone);
        tv_bandadd = (TextView) findViewById(R.id.tv_bandadd);
        tv_subbank = (TextView) findViewById(R.id.tv_subbank);
        et_money = (EditText) findViewById(R.id.et_money);

        btn_cash.setOnClickListener(myclicklistener);
        btn_upload_pic.setOnClickListener(myclicklistener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        myWithdraw = (MyWithdrawActivity) this.getParent();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (null != myWithdraw) {
            myWithdraw.finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private OnClickListener myclicklistener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.btn_cash) { // 提现
                if (isCash) {
                    final String a = et_money.getText().toString();
                    if (TextUtils.isEmpty(a) || a.trim() == null || Integer.valueOf(a) < 100) {
                        showToast("一次提现金额不得少于100.00元");
                        return;
                    }
                    // 提现
                    if (application.getAgentStatu() == FinalUtil.AgentStatu_normal) {
                        ServerSupportManager serverMana = new ServerSupportManager(MyAccountActivity.this,
                                MyAccountActivity.this);
                        List<Parameter> paras = new ArrayList<Parameter>();
                        paras.add(new Parameter("uid", application.getUid()));
                        paras.add(new Parameter("takeMoney", a));
                        // paymentAccountId
                        paras.add(new Parameter("paymentAccountId", tv_number.getText().toString()));
                        serverMana.supportRequest(AgentConfig.withDrawCash(), paras, true,
                                "提交中,请稍等 ....", cashMoney_code);
                    }
                } else {
                    isCash = !isCash;
                    showToast("提现需要证件验证身份,证件验证不通过时,无法提现,请先提交证件信息");
                }
            } else if (v.getId() == btn_upload_pic.getId()) {// 上传身份证图片
                View editV = layoutInflater.inflate(R.layout.dialog_pic_load, null);

                final ImageView iv_front = (ImageView) editV.findViewById(R.id.iv_front);
                final ImageView iv_verso = (ImageView) editV.findViewById(R.id.iv_verso);
                iv_front.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        indexImag = IMG_FORNT;
                        imageview = iv_front;
                        mShowDialog(DIALOG_ID_IMAGE_NO);
                    }
                });
                iv_verso.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        indexImag = IMG_VERSO;
                        imageview = iv_verso;
                        mShowDialog(DIALOG_ID_IMAGE_NO);
                    }
                });

                activateVoucherDialog = MyDialog.showDialog(MyAccountActivity.this, "上传身份证",
                        editV, R.string.submit, R.string.cancel, false, new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // 检查是否是两张图片 是否检测图片大小
                                if (imaChange[0] && imaChange[1]) {
                                    ServerSupportManager serverMana = new ServerSupportManager(MyAccountActivity.this,
                                            MyAccountActivity.this);
                                    ArrayList<MyFilePart> uploads = new ArrayList<MyFilePart>();
                                    List<Parameter> paras = new ArrayList<Parameter>();
                                    paras.add(new Parameter("uid", application.getUid()));
                                    // 证件附件
                                    paras.add(new Parameter("identityPic", "1"));
                                    uploadPic(iv_front, uploads, "0", imagPaht[0]);
                                    uploadPic(iv_verso, uploads, "1", imagPaht[1]);
                                    logger.error(" uploads pic size:" + uploads.size());
                                    serverMana.supportRequest(AgentConfig.uploadIdentityCard(), paras, uploads,
                                            true, "提交中,请稍等 ....", uploadPic_code);
                                } else {
                                    showToast("请选择身份证的正反面上传,同时上传两张...");
                                }
                            }
                        });
            }
        }
    };

    /**
     * 
     * @author caosq 2013-4-26 下午6:13:12
     * @param iv_front
     * @param uploads
     */
    private void uploadPic(final ImageView imageView, ArrayList<MyFilePart> uploads, String name, String imagPath) {
        File fileFront = new File(imagPath);
        // 压缩
        // tempPhotoPath = imagPath;
        // File fileFront = getCompress(75, 75, 100, ((BitmapDrawable) imageView.getDrawable()).getBitmap());
        if (fileFront != null) {// 如果文件存在,准备上传
            MyFilePart uploader1;
            try {
                uploader1 = new MyFilePart(name, fileFront
                        .getName(), fileFront, new MimetypesFileTypeMap()
                        .getContentType(fileFront),
                        SyncHttpClient.CONTENT_CHARSET);
                uploads.add(uploader1);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * bitmap转换成文件
     * 
     * @author wubo
     * @time 2013-3-22
     * @param width
     * @param height
     * @param quality
     * @return
     * 
     */
    protected File getCompress(int width, int height, int quality, Bitmap photo) {
        if (photo == null) {
            return null;
        }
        int bitmapWidth = photo.getWidth();
        int bitmapHeight = photo.getHeight();
        Matrix matrix = new Matrix();
        // 缩放图片的尺寸
        float scaleWidth = (float) width / bitmapWidth;
        float scaleHeight = (float) height / bitmapHeight;
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizeBitmap = Bitmap.createBitmap(photo, 0, 0, bitmapWidth,
                bitmapHeight, matrix, false);

        File tempFile = null;
        FileOutputStream out;
        try {
            tempFile = new File(tempPhotoPath);
            out = new FileOutputStream(tempFile);
            if (resizeBitmap.compress(Bitmap.CompressFormat.JPEG, quality, out)) {
                out.flush();
                out.close();
            }
            if (!resizeBitmap.isRecycled()) {
                resizeBitmap.recycle();
            }
        } catch (IOException e) {
            return null;
        }
        return tempFile;
    }

    /**
     * 切割图片之后返回的 Bundle数据处理，取得bitmap类型图片
     * 
     * @param extras
     *            void
     */
    @Override
    public void createPhoto(Bundle extras) {
        if (extras != null) {
            photo = extras.getParcelable("data");
            if (photo != null) {
                setImage(photo);
            } else {
                showToast(R.string.res_get_photo_fail);
            }
        }
    }

    @Override
    public void setImage(android.graphics.Bitmap bitmap) {
        super.setImage(bitmap);
        if (indexImag == IMG_VERSO) {
            imaChange[1] = true;
            imagPaht[1] = getPhotoPath();
            // imagPaht[1] = tempPhotoPath;
        } else if (indexImag == IMG_FORNT) {
            imaChange[0] = true;
            imagPaht[0] = getPhotoPath();
            // imagPaht[0] = tempPhotoPath;
        }
    };

    /**
     * dialog单选以及确定、取消按钮监听
     */
    private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            // TODO Auto-generated method stub
            if (dialog != null) {
                // 取消浮层
                dialog.cancel();
            }
            if (currentDialogId == DIALOG_ID_IMAGE) {// 上传图片
                doSelectPhoto(which);// 拍照还是从图库选择图片
            } else {
                // ...
            }
        }
    };

    private int query_account = 0xf1;

    // 我的账户查询接口
    private void queryMyAccount() {
        ServerSupportManager serverMana = new ServerSupportManager(this.getParent(), this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));
        serverMana.supportRequest(AgentConfig.queryMyAccount(), paras, true,
                "提交中,请稍等 ....", query_account);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse)) {
            if (null != activateVoucherDialog) {
                MyDialog.dismiss(activateVoucherDialog);
            }
            return;
        }
        if (caseKey == query_account) {
            WithDrawResult result = JSONUtil.fromJson(supportResponse.toString(), WithDrawResult.class);
            if (null != result.getAccountBaseInfo()) {
                AccountBaseInfo info = result.getAccountBaseInfo();
                tv_name.setText(null == info.getUserName() ? "" : info.getUserName()); // 账户名
                tv_identity_card.setText(null == info.getIdentityNo() ? "" : info.getIdentityNo());
                tv_blance.setText(String.valueOf(null == info.getRemain() ? "0" : info.getRemain()));
                if (null != info.getIdentityStatus()) {
                    // 0: 身份证未上传 1: 身份证已上传
                    // btn_cash, btn_upload_pic, btn_setaccount
                    if (info.getIdentityStatus().equals("0")) {
                        tv_card_status.setText("身份证未上传 ");
                        isCash = false;
                        if (btn_upload_pic.getVisibility() != 0) {
                            btn_upload_pic.setVisibility(0);
                        }
                    } else {
                        if (btn_upload_pic.getVisibility() != View.GONE) {
                            btn_upload_pic.setVisibility(View.GONE);
                        }
                        isCash = true;
                        tv_card_status.setText("身份证已上传 ");
                    }
                } else {
                    tv_card_status.setText("身份证状态未知 ");
                }
            } else {
                logger.warn("getAccountBaseInfo  is  null");
            }

            if (null != result.getWithDrawAccountInfo()) {
                WithDrawAccountInfo info = result.getWithDrawAccountInfo();
                tv_acctype.setText(null == info.getPaymentType() ? "" : info.getPaymentType());
                tv_bank.setText(null == info.getOpenBank() ? "" : info.getOpenBank());
                tv_number.setText(null == info.getPaymentAccountId() ? "" : info.getPaymentAccountId());
                tv_myphone.setText(null == info.getMobile() ? "" : info.getMobile());
                String a = "";
                if (null != info.getProvince()) {
                    City city = cityDao.queryCityById(info.getProvince());
                    a = (null != city.getName() ? city.getName() : "");
                }
                if (null != info.getCity()) {
                    City city = cityDao.queryCityById(info.getCity());
                    a += (null != city.getName() ? city.getName() : "");
                }
                tv_bandadd.setText(a);
                tv_subbank.setText(null == info.getBranchBank() ? "" : info.getBranchBank());
            } else {
                logger.warn("getWithDrawAccountInfo  is  null");
            }
        } else if (caseKey == uploadPic_code) {
            @SuppressWarnings("unused")
            BaseResult result = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
            if (null != activateVoucherDialog) {
                MyDialog.dismiss(activateVoucherDialog);
            }
            showToast("上传证件成功");
        } else if (caseKey == cashMoney_code) {
            @SuppressWarnings("unused")
            BaseResult result = JSONUtil.fromJson(supportResponse.toString(), BaseResult.class);
            showToast("申请提现成功");
        }
    }

    @Override
    protected android.content.DialogInterface.OnClickListener setDialogClickListener() {
        return mDialogClickListener;
    }

    @Override
    protected Context setCreateContext() {
        return MyAccountActivity.this;
    }
}
