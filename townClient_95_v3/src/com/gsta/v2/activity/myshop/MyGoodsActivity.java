package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.HotSaleGoodsListAdapter;
import com.gsta.v2.activity.adapter.MyGoodsListManageSeeAndDownAdapter;
import com.gsta.v2.entity.AgentGoodsInfo;
import com.gsta.v2.entity.WareSalesObject;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.MyGoodsListResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 我的店铺商品管理
 * 
 * 开始加载个人全部商品 点击搜索平台全部上架商品
 * 
 * @author caosq
 * 
 */
public class MyGoodsActivity extends BaseActivity implements
        IUICallBackInterface {
    /** Called when the activity is first created. */
    // ib_myshop 返回
    private Button ib_myshop, ib_menu;
    private TextView tv_titleView;

    private final int query1_code = 1;
    private final int query2_code = 2;
    private final int query3_code = 3;
    private final int query4_code = 4;
    private final int query5_code = 5;

    private final int flush1_code = 11;
    private final int flush2_code = 12;
    private final int flush3_code = 13;
    private final int flush4_code = 14;
    private final int flush5_code = 15;
    public static int downGoods_code = 16;// 下架
    public static int upGoods_code = 17;// 上架

    private HorizontalScrollView hs1;
    private RadioGroup rg1;
    private RadioButton r1;
    private RadioButton r2;
    private RadioButton r3;
    private RadioButton r4;
    // private RadioButton r5;

    private ViewFlipper flipper;
    // 一
    private MyGoodsListManageSeeAndDownAdapter goodsAdapter1;
    private PullToRefreshListView pl_1;
    private ListView lv_1;
    private ArrayList<AgentGoodsInfo> goods1;
    View v1;
    Button btn_load1;
    int total1 = 0;
    int count1 = 1;
    private View noDataView1;
    // 二
    private MyGoodsListManageSeeAndDownAdapter goodsAdapter2;
    private PullToRefreshListView pl_2;
    private ListView lv_2;
    private ArrayList<AgentGoodsInfo> goods2;
    View v2;
    Button btn_load2;
    int total2 = 0;
    int count2 = 1;
    private View noDataView2;

    // 三
    private MyGoodsListManageSeeAndDownAdapter goodsAdapter3;
    private PullToRefreshListView pl_3;
    private ListView lv_3;
    private ArrayList<AgentGoodsInfo> goods3;
    View v3;
    Button btn_load3;
    int total3 = 0;
    int count3 = 1;
    private View noDataView3;

    // 四
    private MyGoodsListManageSeeAndDownAdapter goodsAdapter4;
    private PullToRefreshListView pl_4;
    private ListView lv_4;
    private ArrayList<AgentGoodsInfo> goods4;
    View v4;
    Button btn_load4;
    int total4 = 0;
    int count4 = 1;
    private View noDataView4;

    // 五 促销
    private HotSaleGoodsListAdapter goodsAdapter5;
    private PullToRefreshListView pl_5;
    private ListView lv_5;
    private List<WareSalesObject> goods5;
    View v5;
    Button btn_load5;
    int total5 = 0;
    int count5 = 1;
    private View noDataView5;

    /**
     * 查询条件
     */
    private int type = 0;// 商品类别 0,合约机,智能机;1,光宽带;2,手机卡;3,手机加宽带; 默认为 0 热销是999
    private String provinceId = "33";// 省份 默认为空
    @SuppressWarnings("unused")
    private String cityId = "20574";// 地市 默认为空
    private String minPrice = "0";// 最小价格 默认为0
    private String maxPrice = "0";// 最大价格 默认为0
    private String keywords = "";// 商品关键词 默认为空
    private int pageIndex = 1;// 当前页数 默认为1
    private int pageSize = 10;// 分页大小，默认为9
    private String orderBy = "00";// 排序字段，默认为00

    private int btnPosition;// 在哪个位置点击了上架按钮
    private boolean[] needLoad = { false, true, true, true, true, true };// 下标0 动画结束为true

    public int getType() {
        return type;
    }

    // 00：上架新品降序 01：上架新品升序
    // 10：销售量降序 11：销售量升序
    // 20：价格降序 21：价格升序
    private static final String showText = "没有添加商品,或者刷新失败,请重新操作";

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (4 == msg.what) {
                ((View) msg.obj).setVisibility(View.GONE);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.mygoods);
        initView();
        initDate();
        initListener();
    }

    private void initView() {

        hs1 = (HorizontalScrollView) findViewById(R.id.tab_view);
        rg1 = (RadioGroup) findViewById(R.id.rg1);
        r1 = (RadioButton) findViewById(R.id.r1);
        r2 = (RadioButton) findViewById(R.id.r2);
        r3 = (RadioButton) findViewById(R.id.r3);
        r4 = (RadioButton) findViewById(R.id.r4);
        // r5 = (RadioButton) findViewById(R.id.r5);

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        flipper = (ViewFlipper) findViewById(R.id.myViewFlipper1);

        pl_1 = (PullToRefreshListView) findViewById(R.id.lv_1);
        lv_1 = pl_1.getRefreshableView();
        v1 = inflater.inflate(R.layout.loadmore, null);
        btn_load1 = (Button) v1.findViewById(R.id.loadMoreButton);
        btn_load1.setVisibility(View.GONE);
        noDataView1 = findViewById(R.id.nodata_1);
        ((TextView) noDataView1.findViewById(R.id.search)).setText(showText);

        pl_2 = (PullToRefreshListView) findViewById(R.id.lv_2);
        lv_2 = pl_2.getRefreshableView();
        v2 = inflater.inflate(R.layout.loadmore, null);
        btn_load2 = (Button) v2.findViewById(R.id.loadMoreButton);
        btn_load2.setVisibility(View.GONE);
        noDataView2 = findViewById(R.id.nodata_2);
        ((TextView) noDataView2.findViewById(R.id.search)).setText(showText);

        pl_3 = (PullToRefreshListView) findViewById(R.id.lv_3);
        lv_3 = pl_3.getRefreshableView();
        v3 = inflater.inflate(R.layout.loadmore, null);
        btn_load3 = (Button) v3.findViewById(R.id.loadMoreButton);
        btn_load3.setVisibility(View.GONE);
        noDataView3 = findViewById(R.id.nodata_3);
        ((TextView) noDataView3.findViewById(R.id.search)).setText(showText);

        pl_4 = (PullToRefreshListView) findViewById(R.id.lv_4);
        lv_4 = pl_4.getRefreshableView();
        v4 = inflater.inflate(R.layout.loadmore, null);
        btn_load4 = (Button) v4.findViewById(R.id.loadMoreButton);
        btn_load4.setVisibility(View.GONE);
        noDataView4 = findViewById(R.id.nodata_4);
        ((TextView) noDataView4.findViewById(R.id.search)).setText(showText);

        pl_5 = (PullToRefreshListView) findViewById(R.id.lv_5);
        lv_5 = pl_5.getRefreshableView();
        v5 = inflater.inflate(R.layout.loadmore, null);
        btn_load5 = (Button) v5.findViewById(R.id.loadMoreButton);
        btn_load5.setVisibility(View.GONE);
        noDataView5 = findViewById(R.id.nodata_5);
        ((TextView) noDataView5.findViewById(R.id.search)).setText(showText);

        ib_menu = (Button) findViewById(R.id.mytitile_btn_right);
        ib_myshop = (Button) findViewById(R.id.mytitile_btn_left);
        tv_titleView = (TextView) findViewById(R.id.mytitle_textView);
        tv_titleView.setText("商品管理");
        ib_myshop.setVisibility(0);
        ib_menu.setVisibility(0);
        ib_menu.setText("添  加");
    }

    private void initDate() {
        goods1 = new ArrayList<AgentGoodsInfo>();
        goodsAdapter1 = new MyGoodsListManageSeeAndDownAdapter(this, goods1);

        goods2 = new ArrayList<AgentGoodsInfo>();
        goodsAdapter2 = new MyGoodsListManageSeeAndDownAdapter(this, goods2);

        goods3 = new ArrayList<AgentGoodsInfo>();
        goodsAdapter3 = new MyGoodsListManageSeeAndDownAdapter(this, goods3);

        goods4 = new ArrayList<AgentGoodsInfo>();
        goodsAdapter4 = new MyGoodsListManageSeeAndDownAdapter(this, goods4);

        goods5 = new ArrayList<WareSalesObject>();
        goodsAdapter5 = new HotSaleGoodsListAdapter(this, goods5, 999);

        productQuery(flush1_code, true);

        lv_1.setAdapter(goodsAdapter1);
        lv_2.setAdapter(goodsAdapter2);
        lv_3.setAdapter(goodsAdapter3);
        lv_4.setAdapter(goodsAdapter4);
        lv_5.setAdapter(goodsAdapter5);

        lv_1.addFooterView(v1);
        lv_2.addFooterView(v2);
        lv_3.addFooterView(v3);
        lv_4.addFooterView(v4);
        lv_5.addFooterView(v5);
    }

    private void initListener() {

        rg1.setOnCheckedChangeListener(checkedChangeListener);
        // 搜索平台商品
        ib_menu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MyGoodsActivity.this, MyGoodsManageActivity.class);
                if (r1.isChecked()) {// 智能机
                    intent.putExtra(FinalUtil.PRODUCT_TYPE, FinalUtil.PRODUCT_FLAG_CALLPHONE);
                } else if (r2.isChecked()) {// 手机卡
                    intent.putExtra(FinalUtil.PRODUCT_TYPE, FinalUtil.PRODUCT_FLAG_PHONECARD);
                } else if (r3.isChecked()) {// 光宽带
                    intent.putExtra(FinalUtil.PRODUCT_TYPE, FinalUtil.PRODUCT_FLAG_BROADBAND);
                } else if (r4.isChecked()) {// 手机+宽带
                    intent.putExtra(FinalUtil.PRODUCT_TYPE, FinalUtil.PRODUCT_FLAG_PHONE_BROADBAND);
                } else {
                    showToast(getResources().getString(R.string.no_develop));
                    return;
                }
                startActivity(intent);
            }
        });

        ib_myshop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_load1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = count1;// 当前页数 默认为1
                productQuery(query1_code, true);
            }
        });
        btn_load2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = count2;// 当前页数 默认为1
                productQuery(query2_code, true);
            }
        });
        btn_load3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                pageIndex = count3;// 当前页数 默认为1
                productQuery(query3_code, true);
            }
        });

        btn_load4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = count4;// 当前页数 默认为1
                productQuery(query4_code, true);
            }
        });

        btn_load5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = count5;// 当前页数 默认为1
                productQuery(query5_code, true);
            }
        });

        pl_1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = FinalUtil.PRODUCT_FLAG_CALLPHONE;
                count1 = 1;
                pageIndex = 1;
                productQuery(flush1_code, false);
            }
        });

        pl_2.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = FinalUtil.PRODUCT_FLAG_PHONECARD;
                count2 = 1;
                pageIndex = 1;
                productQuery(flush2_code, false);
            }
        });

        pl_3.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = FinalUtil.PRODUCT_FLAG_BROADBAND;
                count3 = 1;
                pageIndex = 1;
                productQuery(flush3_code, false);
            }
        });

        pl_4.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = FinalUtil.PRODUCT_FLAG_PHONE_BROADBAND;
                count4 = 1;
                pageIndex = 1;
                productQuery(flush4_code, false);
            }
        });

        pl_5.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                count5 = 1;
                pageIndex = 1;
                productQuery(flush5_code, false);
            }
        });
    }

    // radioGroup中的点击事件
    OnCheckedChangeListener checkedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == r1.getId()) {
                if (flipper.getDisplayedChild() != 0) {
                    hs1.scrollTo(0, 0);
                    switchLayoutStateTo(flipper, 0);
                    type = FinalUtil.PRODUCT_FLAG_CALLPHONE;
                    if (needLoad[1]) {
                        needLoad[1] = false;
                        count1 = 1;
                        pageIndex = 1;
                        productQuery(flush1_code, true);
                    }
                }
                return;
            }
            if (checkedId == r2.getId()) {
                if (flipper.getDisplayedChild() != 1) {
                    hs1.scrollTo(0, 0);
                    switchLayoutStateTo(flipper, 1);
                    type = FinalUtil.PRODUCT_FLAG_PHONECARD;
                    if (needLoad[2]) {
                        needLoad[2] = false;
                        count2 = 1;
                        pageIndex = 1;
                        productQuery(flush2_code, true);
                    }
                }
                return;
            }
            if (checkedId == r3.getId()) {
                if (flipper.getDisplayedChild() != 2) {
                    hs1.scrollTo(220, 0);
                    switchLayoutStateTo(flipper, 2);
                    type = FinalUtil.PRODUCT_FLAG_BROADBAND;
                    if (needLoad[3]) {
                        needLoad[3] = false;
                        count3 = 1;
                        pageIndex = 1;
                        productQuery(flush3_code, true);
                    }
                }
                return;
            }
            if (checkedId == r4.getId()) {
                if (flipper.getDisplayedChild() != 3) {
                    hs1.scrollTo(400, 0);
                    switchLayoutStateTo(flipper, 3);
                    type = FinalUtil.PRODUCT_FLAG_PHONE_BROADBAND;
                    if (needLoad[4]) {
                        needLoad[4] = false;
                        count4 = 1;
                        pageIndex = 1;
                        productQuery(flush4_code, true);
                    }
                }
                return;
            }

            // if (checkedId == r5.getId()) {
            // if (flipper.getDisplayedChild() != 4) {
            // hs1.scrollTo(600, 0);
            // switchLayoutStateTo(flipper, 4);
            // if (needLoad[5]) {
            // needLoad[5] = false;
            // count5 = 1;
            // pageIndex = 1;
            // productQuery(flush5_code, true);
            // }
            // }
            // return;
            // }
        }
    };

    /**
     * 个人代理商品查询
     * 
     * @author wubo
     * @time 2013-1-23
     * @param casekey
     * 
     */
    private void productQuery(int casekey, boolean needPd) {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        // 商品类别 0:店铺首页, 1:商品管理页
        paras.add(new Parameter("flag", "" + 1));
        paras.add(new Parameter("type", "" + type));// 商品类别 0,合约机;1,光宽带;2,手机卡;3,其他;
        // 默认为 0
        paras.add(new Parameter("agentUid", application.getUid()));
        paras.add(new Parameter("provinceId", provinceId));// 省份 默认为空
        paras.add(new Parameter("cityId", provinceId));// 地市 默认为空
        paras.add(new Parameter("minPrice", minPrice));// 最小价格 默认为0
        paras.add(new Parameter("maxPrice", maxPrice));// 最大价格 默认为0
        paras.add(new Parameter("keywords", keywords));// 商品关键词 默认为空
        paras.add(new Parameter("pageIndex", String.valueOf(pageIndex)));// 当前页数
        // 默认为1
        paras.add(new Parameter("pageSize", String.valueOf(pageSize)));// 分页大小，默认为9
        paras.add(new Parameter("orderBy", orderBy));// 排序字段，默认为00
        // 00：上架新品降序 01：上架新品升序
        // 10：销售量降序 11：销售量升序
        // 20：价格降序 21：价格升序
        serverMana.supportRequest(AgentConfig.getAgentGoodsList(), paras,
                needPd, "加载中,请稍等 ...", casekey);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_1.isRefreshing()) {
            pl_1.onRefreshComplete();
        } else if (pl_2.isRefreshing()) {
            pl_2.onRefreshComplete();
        } else if (pl_3.isRefreshing()) {
            pl_3.onRefreshComplete();
        } else if (pl_4.isRefreshing()) {
            pl_4.onRefreshComplete();
        } else if (pl_5.isRefreshing()) {
            pl_5.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }
        if (caseKey == downGoods_code || caseKey == upGoods_code) {// 下架商品
            BaseResult result = JSONUtil.fromJson(supportResponse
                    .toString(), BaseResult.class);
            if (type == FinalUtil.PRODUCT_FLAG_CALLPHONE) {
                changeItemByBtn(goods1, goodsAdapter1);
            } else if (type == FinalUtil.PRODUCT_FLAG_BROADBAND) {
                changeItemByBtn(goods3, goodsAdapter3);
            } else if (type == FinalUtil.PRODUCT_FLAG_PHONECARD) {
                changeItemByBtn(goods2, goodsAdapter2);
            } else if (type == FinalUtil.PRODUCT_FLAG_PHONE_BROADBAND) {
                changeItemByBtn(goods4, goodsAdapter4);
            }
            showToast(result.getErrorMessage());
            return;
        }

        // 个人代理的商品
        MyGoodsListResult goodsResult = JSONUtil.fromJson(supportResponse
                .toString(), MyGoodsListResult.class);
        if (goodsResult != null) {
            if (caseKey == query1_code || caseKey == flush1_code) {
                if (caseKey == flush1_code) {
                    pageIndex = 1;
                    flushAdapterData(total1, goods1, goodsAdapter1);
                }
                count1 += 1;
                total1 = goodsResult.getPageCount();
                if (null != goodsResult.getAgentGoodsList() && goodsResult.getAgentGoodsList().size() > 0) {
                    goods1.addAll(goodsResult.getAgentGoodsList());
                    goodsAdapter1.notifyDataSetChanged();
                    showLoadView(total1, count1, btn_load1, goods1, noDataView1);
                }
            } else if (caseKey == query2_code || caseKey == flush2_code) {
                if (caseKey == flush2_code) {
                    count2 = 1;
                    cleanList(goods2);
                    goodsAdapter2.notifyDataSetInvalidated();
                }
                count2 += 1;
                total2 = goodsResult.getPageCount();
                if (null != goodsResult.getAgentGoodsList() && goodsResult.getAgentGoodsList().size() > 0) {
                    goods2.addAll(goodsResult.getAgentGoodsList());
                    goodsAdapter2.notifyDataSetChanged();
                    showLoadView(total2, count2, btn_load2, goods2, noDataView2);
                }
            } else if (caseKey == query3_code || caseKey == flush3_code) {
                if (caseKey == flush3_code) {
                    count3 = 1;
                    cleanList(goods3);
                    goodsAdapter3.notifyDataSetInvalidated();
                }
                count3 += 1;
                total3 = goodsResult.getPageCount();
                if (null != goodsResult.getAgentGoodsList() && goodsResult.getAgentGoodsList().size() > 0) {
                    goods3.addAll(goodsResult.getAgentGoodsList());
                    goodsAdapter3.notifyDataSetChanged();
                    showLoadView(total3, count3, btn_load3, goods3, noDataView3);
                }
            } else if (caseKey == query4_code || caseKey == flush4_code) {
                if (caseKey == flush4_code) {
                    count4 = 1;
                    cleanList(goods4);
                    goodsAdapter4.notifyDataSetInvalidated();
                }
                count4 += 1;
                total4 = goodsResult.getPageCount();
                if (null != goodsResult.getAgentGoodsList() && goodsResult.getAgentGoodsList().size() > 0) {
                    goods4.addAll(goodsResult.getAgentGoodsList());
                    goodsAdapter4.notifyDataSetChanged();
                    showLoadView(total4, count4, btn_load4, goods4, noDataView4);
                }
            }
        } else {
            showToast(R.string.to_server_fail);
            return;
        }
    }

    /**
     * 当为刷新时,初始化数据
     * 
     * @author caosq 2013-5-31 下午5:13:56
     */
    private void flushAdapterData(int count, List<AgentGoodsInfo> list, BaseAdapter adapter) {
        count = 1;
        cleanList(list);
        adapter.notifyDataSetInvalidated();
    }

    /**
     * @author caosq 2013-5-30 下午6:34:50
     * @param goods
     */
    private void cleanList(List<AgentGoodsInfo> goods) {
        if (goods.size() > 0) {
            goods.clear();
        }
    }

    /**
     * @author caosq 2013-5-21 下午3:00:11
     */
    private void showLoadView(int total, int count, Button btn, List<AgentGoodsInfo> list, View noDataView) {
        if (total >= count) {
            btn.setVisibility(View.VISIBLE);
        } else {
            btn.setVisibility(View.GONE);
            if (list.size() <= 0) {
                showNoDataView(noDataView);
            }
        }
    }

    private void showNoDataView(View noDataLayout) {
        Message agem = Message.obtain();
        if (null != noDataLayout) {
            noDataLayout.setVisibility(0);
            agem.obj = noDataLayout;
        }
        agem.what = 4;
        handler.sendMessageDelayed(agem, 2000);
    }

    /**
     * 从数据集中更改相应的状态
     * 
     * @author caosq 2013-4-18 下午4:53:27
     */
    private void changeItemByBtn(ArrayList<AgentGoodsInfo> list, BaseAdapter adapter) {
        if (list.size() > btnPosition) {
            AgentGoodsInfo info = list.get(btnPosition);
            if (info.getFlag() == MyGoodsListManageSeeAndDownAdapter.GOODS_DOWN_FLAG) {
                info.setFlag(MyGoodsListManageSeeAndDownAdapter.GOODS_UP_FLAG);
            } else if (info.getFlag() == MyGoodsListManageSeeAndDownAdapter.GOODS_UP_FLAG) {
                info.setFlag(MyGoodsListManageSeeAndDownAdapter.GOODS_DOWN_FLAG);
            } else if (info.getFlag() == MyGoodsListManageSeeAndDownAdapter.GOODS_NORMAL_FLAG) {
                info.setFlag(MyGoodsListManageSeeAndDownAdapter.GOODS_UP_FLAG);
            }
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * 当点击item中的按钮时,是在哪个位置
     * 
     * @author caosq 2013-4-17 下午4:22:54
     * @param a
     */
    public void sendAdapterViewPosition(int position) {
        this.btnPosition = position;
    }
}