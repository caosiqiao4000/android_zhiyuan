package com.gsta.v2.activity.myshop;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.util.FinalUtil;

/**
 * 业绩查询
 * 
 * @author wubo
 * @createtime 2012-7-31
 */
public class ScoreActivity extends BaseActivity {

    private LinearLayout realScore;
    private LinearLayout realOrders;
    private LinearLayout scoreStat;

    private Button ib_myshop;
    private TextView titleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.score);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        realScore = (LinearLayout) findViewById(R.id.stat_realScore);
        realOrders = (LinearLayout) findViewById(R.id.stat_realOrders);
        scoreStat = (LinearLayout) findViewById(R.id.stat_scoreStat);

        ib_myshop = (Button) findViewById(R.id.mytitile_btn_left);
        titleTextView = (TextView) findViewById(R.id.mytitle_textView);

        titleTextView.setText("业绩查询");
        ib_myshop.setVisibility(0);
    }

    private void initData() {

    }

    private void initListener() {
        // TODO Auto-generated method stub
        realScore.setOnClickListener(listener);
        realOrders.setOnClickListener(listener);
        scoreStat.setOnClickListener(listener);

        ib_myshop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ScoreActivity.this.finish();
            }
        });
    }

    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (v == realScore) {// 业绩查询
                Intent intent = new Intent(ScoreActivity.this,
                        RealSellInfoActivity.class);
                startActivity(intent);
            } else if (v == realOrders) { // 订单查询
                Intent intent = new Intent(ScoreActivity.this,
                        OrderQueryActivity.class);
                intent.putExtra(FinalUtil.QUERY_TYPE, "0");
                intent.putExtra(FinalUtil.ORDER_ACCOUNT, "");
                startActivity(intent);
            } else if (v == scoreStat) { // 收益月报表
                Intent intent = new Intent(ScoreActivity.this,
                        MonthScoreActivity.class);
                intent.putExtra(FinalUtil.OPERUID, application.getUid());
                startActivity(intent);
            }
        }
    };
}
