package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.RemarkAdapter;
import com.gsta.v2.db.IPicUpLoadAfterSavePath;
import com.gsta.v2.db.impl.PicUpLoadSavePathDao;
import com.gsta.v2.entity.AgentMemoinfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.RemarkResult;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * @author wubo
 * @createtime 2012-9-24 营销笔记
 */
public class RemarkListActivity extends BaseActivity implements
        IUICallBackInterface {
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger();
    private PullToRefreshListView pl_remark;
    private ListView remarkList;// 显示列表
    private RemarkAdapter rAdapter;// 数据源
    private List<AgentMemoinfo> ams = new ArrayList<AgentMemoinfo>();// 备忘录
    private final int load_code = 11;
    private final int del_code = 12;
    private String delId = "";

    private View noDataLayout; // 无数据的提示 只显示两秒
    public static final int NOTE_TYPE_EDIT = 1; // 编辑笔记
    public static final int NOTE_TYPE_ADD = 0; // 添加笔记
    public static final String ITEM_POSITION = "itemPosition"; // 在列表中的记录位置

    private MyDialog delAgentShopDialog;
    // 删除时删除数据库中的相关记录
    private IPicUpLoadAfterSavePath picDao;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 1) {
                noDataLayout.setVisibility(View.GONE);
            } else if (msg.what == 2) {
                final AgentMemoinfo info = (AgentMemoinfo) msg.obj;
                ListView listView = (ListView) LayoutInflater.from(RemarkListActivity.this).inflate(R.layout.dialog_list_view, null);
                String[] objects = { "查看详情", "删除笔记" };
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(RemarkListActivity.this, R.layout.dialog_text_view, objects);
                listView.setAdapter(adapter);
                // 设置监听器
                listView.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        switch (position) {
                        case 0:
                            toNoteActivity(info, NOTE_TYPE_EDIT, position + 1);
                            break;
                        case 1:
                            del(delId = info.getId());
                            break;
                        }
                        MyDialog.dismiss(delAgentShopDialog);
                    }
                });
                delAgentShopDialog = MyDialog.showDialog(RemarkListActivity.this, null == info.getProductName() ? "无标题" : info.getProductName(),
                        listView);
            }
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.remarklist);
        initView();
        initData();
        initListener();

        Button ib_myshop = (Button) findViewById(R.id.mytitile_btn_left);
        ib_myshop.setVisibility(0);
        ib_myshop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText("营销笔记列表");
        Button imageBut = (Button) findViewById(R.id.mytitile_btn_right);
        imageBut.setText("添  加");
        imageBut.setVisibility(0);
        imageBut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toNoteActivity(null, NOTE_TYPE_ADD, 0);
            }
        });
    }

    private void initView() {
        noDataLayout = findViewById(R.id.nodata);
        TextView search = (TextView) noDataLayout.findViewById(R.id.search);
        search.setText("还没有营销笔记哦,赶快点击右上角按钮添加一个吧...");
        pl_remark = (PullToRefreshListView) findViewById(R.id.lv_im);
        remarkList = pl_remark.getRefreshableView();
    }

    private void initData() {
        rAdapter = new RemarkAdapter(RemarkListActivity.this, ams);
        remarkList.setAdapter(rAdapter);
        picDao = new PicUpLoadSavePathDao(RemarkListActivity.this);
        load(true);
    }

    private void initListener() {
        remarkList.setOnItemClickListener(itemClickListener1);
        remarkList.setOnItemLongClickListener(longClickListener);
        pl_remark.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                load(false);
            }
        });
    }

    protected void onResume() {
        super.onResume();
    };

    /**
     * 列表点击事件 编辑笔记
     */
    OnItemClickListener itemClickListener1 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                long id) {
            AgentMemoinfo im = (AgentMemoinfo) parent.getItemAtPosition(
                    position);
            toNoteActivity(im, NOTE_TYPE_EDIT, position);
        }
    };

    OnItemLongClickListener longClickListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            AgentMemoinfo info = (AgentMemoinfo) parent.getItemAtPosition(position);
            // 弹出操作对话框
            Message message = handler.obtainMessage();
            message.what = 2;
            message.arg1 = position;
            message.obj = info;
            handler.sendMessage(message);
            return false;
        };
    };

    /**
     * @author caosq 2013-4-7 下午6:04:21
     * @param im
     * @param flag
     *            // 编辑或者查看
     */
    private void toNoteActivity(AgentMemoinfo im, int flag, int itemPosition) {
        Intent intent = new Intent();
        intent.putExtra(FinalUtil.ADDNOTE_TYPE, flag);
        intent.putExtra(ITEM_POSITION, itemPosition);
        if (null != im) {
            intent.putExtra(FinalUtil.AGENTMEMOINFO, im);
        }
        intent.setClass(RemarkListActivity.this, AddNoteActivity.class);
        startActivityForResult(intent, 0xaa);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0xaa) {
            if (resultCode == Activity.RESULT_OK) {
                pl_remark.setRefreshing(true);
                load(false);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void del(String remarkId) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", String.valueOf(application.getUid())));
        paras.add(new Parameter("type", "2"));
        paras.add(new Parameter("id", remarkId));
        paras.add(new Parameter("picPath", "a"));
        serverMana.supportRequest(AgentConfig.MemoManger(), paras, true,
                "提交中,请稍等 ....", del_code);
    }

    public void load(boolean needPd) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", String.valueOf(application.getUid())));
        serverMana.supportRequest(AgentConfig.memoQuery(), paras, needPd,
                "查询中,请稍等 ....", load_code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_remark.isRefreshing()) {
            pl_remark.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }
        switch (caseKey) {
        case load_code:
            ams.clear();
            RemarkResult r1 = JSONUtil.fromJson(supportResponse.toString(),
                    RemarkResult.class);
            if (r1 != null) {
                if (r1.getMemoInfo() != null && r1.getMemoInfo().size() > 0) {
                    ams = r1.getMemoInfo();
                }
                rAdapter.setMessages(ams);
                if (ams.size() <= 0) {
                    showNoDataView();
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        case del_code:
            BaseResult result = JSONUtil.fromJson(supportResponse.toString(),
                    BaseResult.class);
            for (int i = 0; i < ams.size(); i++) {
                if (ams.get(i).getId().equals(delId)) {
                    picDao.deleteByServerId(delId);
                    ams.remove(i);
                    break;
                }
            }
            rAdapter.setMessages(ams);
            showToast(result.errorMessage);
            break;
        }
    }

    private void showNoDataView() {
        noDataLayout.setVisibility(0);
        handler.sendEmptyMessageDelayed(1, 2000);
    }
}
