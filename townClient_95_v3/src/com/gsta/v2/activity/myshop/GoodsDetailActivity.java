package com.gsta.v2.activity.myshop;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.telephony.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.LevelRuleActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.DeduAdapter;
import com.gsta.v2.activity.adapter.ProductTabAdapter;
import com.gsta.v2.activity.adapter.ViewPageAdapter;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.entity.ProductDetailInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.ProductDetailResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.HorizontalListView;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 我的商品管理中的商品细节,包括分成
 * 
 * @author caosq 2013-5-20
 */
public class GoodsDetailActivity extends BaseActivity implements
        IUICallBackInterface {

    private TextView tv_title;
    private Button btn_back;
    private HorizontalListView scrcoolTab;
    private ProductTabAdapter tabAdapter;
    private ViewFlipper flipper;
    // 左上角商品图片
    private ImageView iv_portrait;
    private TextView tv_name;
    private TextView tv_price;
    private TextView tv_detail;
    // 收藏商品
    private TextView btn_fav;

    private GoodsInfo goodsInfo;
    // 图片放大 用的
    private ArrayList<String> uris = new ArrayList<String>();

    // 一 商品图片
    private ViewPager pageV_one;
    private ViewPageAdapter viewPageAdapter;

    // 二 商品分成
    private ListView lv_2;
    private DeduAdapter deduAdapter;
    private List<ProductDetailInfo> ProductDetailInfos = new ArrayList<ProductDetailInfo>();
    // 分成说明
    private TextView tv_des;

    private final int add_code = 11;
    private final int show_code = 12;
    
    // 商品图片使用
    private DisplayImageOptions optionsByNonePic;
    // 头像类使用
    @SuppressWarnings("unused")
	private DisplayImageOptions optionsByNoPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.goods_detail);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        btn_fav = (TextView) findViewById(R.id.btn_fav);

        iv_portrait = (ImageView) findViewById(R.id.iv_portrait);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_detail = (TextView) findViewById(R.id.tv_detail);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);
        scrcoolTab = (HorizontalListView) findViewById(R.id.tab_view);
        flipper = (ViewFlipper) findViewById(R.id.myViewFlipper1);

        // 一
        pageV_one = (ViewPager) findViewById(R.id.pager_one);

        // 二
        lv_2 = (ListView) findViewById(R.id.lv_2);
        tv_des = (TextView) findViewById(R.id.tv_des);
        tv_des.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

    }

    private void initData() {
    	
        optionsByNonePic = ImageUtil.getDefaultDispalyImageOptions(R.drawable.none_pic, Bitmap.Config.ARGB_8888);
        optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);
        
        tv_title.setText("商品明细");
        tabAdapter = new ProductTabAdapter(GoodsDetailActivity.this,
                new String[] { "商品分成", "商品图片", }, application.getWidthPixels());
        scrcoolTab.setAdapter(tabAdapter);
        scrcoolTab.setOnItemClickListener(onItemClickListener2);
        tabAdapter.setSelect(0);

        goodsInfo = (GoodsInfo) getIntent().getSerializableExtra(
                FinalUtil.GOODSINFO);
/*        ImageUtil.getNetPic(GoodsDetailActivity.this, iv_portrait, Util.converPicPath(goodsInfo
                .getMediaPath(), FinalUtil.PIC_MIDDLE),R.drawable.none_pic);*/
        ImageUtil.getNetPicByUniversalImageLoad( null, iv_portrait, Util.converPicPath(goodsInfo
                .getMediaPath(),FinalUtil.PIC_MIDDLE), optionsByNonePic);
        tv_name.setText(goodsInfo.getProdName());
        tv_price.setText("价格:" + goodsInfo.getMinSalePrice() + "-"
                + goodsInfo.getMaxSalePrice());
        tv_detail.setText(goodsInfo.getDescription());

        uris.add(goodsInfo.getMediaPath());
        viewPageAdapter = new ViewPageAdapter(GoodsDetailActivity.this, uris, imageClick);
        pageV_one.setAdapter(viewPageAdapter);

        deduAdapter = new DeduAdapter(this, ProductDetailInfos);
        lv_2.setAdapter(deduAdapter);
        getProductInfo();
    }

    private void initListener() {
        btn_back.setOnClickListener(listener);
        btn_fav.setOnClickListener(listener);
        // g1.setOnItemClickListener(imageClick);
        iv_portrait.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 放大 相册
                magnifyPic();
            }
        });

        tv_des.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GoodsDetailActivity.this,
                        LevelRuleActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * 放大图片
     * 
     * @author caosq 2013-6-17 上午11:32:00
     */
    private void magnifyPic() {
        ImageUtil.magnifyPic(GoodsDetailActivity.this,tv_title.getText(),0,uris);
    }

    OnClickListener imageClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // 放大 相册
            magnifyPic();
        }
    };

    AdapterView.OnItemClickListener onItemClickListener2 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            if (position == tabAdapter.getSelect()) {
                return;
            }
            tabAdapter.setSelect(position);
            switchLayoutStateTo(flipper, position);
        }
    };

    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == btn_fav.getId()) {
                favGoods();
            } else if (v.getId() == btn_back.getId()) {
                GoodsDetailActivity.this.finish();
            }
        }
    };

    /**
     * 发送私信
     * 
     * @param contacts
     *            void
     */
    @SuppressWarnings("unused")
    private void sendMsg(String phoneNum) {
        if (phoneNum != null && phoneNum.length() > 0) {
            String sms = "";
            SmsManager smsMana = SmsManager.getDefault();
            smsMana.sendTextMessage(phoneNum, null, sms, null, null);
        }
    }

    /**
     * 收藏商品
     * 
     * @author wubo
     * @createtime 2012-9-6
     */
    public void favGoods() {
        // 发送下架 请求
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        // 0:上架，1:下架
        paras.add(new Parameter("opType", "0"));
        paras.add(new Parameter("uid", application.getUid()));
        paras.add(new Parameter("productId", goodsInfo.getSpeciesId()));
        serverMana.supportRequest(AgentConfig.AddToMyMall(), paras, true,
                "提交中,请稍等 ...", add_code);
    }

    /**
     * 商品信息
     * 
     * @author wubo
     * @createtime 2012-10-13
     */
    public void getProductInfo() {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));
        // paras.add(new Parameter("agentUid", application.getUid()));
        paras.add(new Parameter("speciesId", goodsInfo.getSpeciesId()));
        serverMana.supportRequest(AgentConfig.ShowProductDetail(), paras, true,
                "查询中,请稍等 ...", show_code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse))
            return;

        switch (caseKey) {
        case add_code:
            BaseResult userResult = JSONUtil.fromJson(supportResponse
                    .toString(), BaseResult.class);
            if (userResult != null) {
                showToast(userResult.errorMessage);
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        case show_code:
            ProductDetailResult pd = JSONUtil.fromJson(supportResponse
                    .toString(), ProductDetailResult.class);
            if (pd != null) {
                List<String> lisStrings = pd.getMediaPath();
                if (null != lisStrings && lisStrings.size() > 0) {
                    uris.clear();
                    uris.addAll(lisStrings);
                    viewPageAdapter.notifyDataSetChanged();
                }
                ProductDetailInfos = pd.getProductDetailInfo();
                if (ProductDetailInfos == null) {
                    ProductDetailInfos = new ArrayList<ProductDetailInfo>();
                }
                deduAdapter.setmUserList(ProductDetailInfos);
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }

}
