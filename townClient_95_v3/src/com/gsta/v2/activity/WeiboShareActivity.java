package com.gsta.v2.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.ShortUrlResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.TextCountLimitWatcher;
import com.gsta.v2.util.WeiboUtil;
import com.tencent.weibo.api.TAPI;
import com.tencent.weibo.constants.OAuthConstants;
import com.tencent.weibo.oauthv2.OAuthV2;
import com.weibo.net.AccessToken;
import com.weibo.net.AsyncWeiboRunner;
import com.weibo.net.AsyncWeiboRunner.RequestListener;
import com.weibo.net.Utility;
import com.weibo.net.Weibo;
import com.weibo.net.WeiboException;
import com.weibo.net.WeiboParameters;

/**
 * 微博分享
 * 
 * @author wubo
 * @createtime 2012-8-29
 */
public class WeiboShareActivity extends BaseActivity implements
		IUICallBackInterface, RequestListener {

	TextView title;// 新浪还是腾讯
	EditText et_msg;// 分享的内容
	TextView tv_uri;// 分享地址
	Button btn_send;// 发送
	Button btn_cancel;// 取消分享
	private String mAccessToken = "";// 应用身份标识Token
	private String mTokenSecret = "";// 应用身份标识Secret
	String send_msg = "";// 内容文字
	String uri = "";// 链接地址
	GoodsInfo goodsInfo;// 商品信息
	int weiboFlag = 0;// 0sina 1tx
	OAuthV2 oAuth;// tx认证

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.weiboshare);
		// getAgentApplication();
		initView();
		initData();
		initListener();

	}

	private void initView() {
		// TODO Auto-generated method stub
		et_msg = (EditText) findViewById(R.id.et_msg);
		tv_uri = (TextView) findViewById(R.id.tv_uri);
		btn_send = (Button) findViewById(R.id.btn_send);
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		title = (TextView) findViewById(R.id.title);
		et_msg.addTextChangedListener(new TextCountLimitWatcher(100, et_msg));
	}

	private void initData() {
		// TODO Auto-generated method stub
		Intent in = this.getIntent();
		goodsInfo = (GoodsInfo) in.getSerializableExtra(FinalUtil.GOODSINFO);
		weiboFlag = in.getIntExtra(WeiboUtil.WEIBOFLAG, 0);
		et_msg.setText("“我是" + application.getUserName()
				+ "，我在东方数码城开店啦，亲们有买手机、宽带的来找我吧，给你们最优价”。");
		if (weiboFlag == 0) {
			title.setText("新浪微博");
		} else if (weiboFlag == 1) {
			title.setText("腾讯微博");
		}
		getShortUri();
	}

	private void initListener() {
		// TODO Auto-generated method stub
		btn_send.setOnClickListener(listener);
		btn_cancel.setOnClickListener(listener);
	}

	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btn_send) {
				if (uri.equals("")) {
					showToast("分享失败!");
					WeiboShareActivity.this.finish();
				} else {
					try {
						send_msg = et_msg.getText().toString() + "  "
								+ tv_uri.getText().toString();
						if (send_msg.length() <= 140) {
							if (weiboFlag == 0) {
								mAccessToken = getIntent().getStringExtra(
										WeiboUtil.EXTRA_ACCESS_TOKEN);
								mTokenSecret = getIntent().getStringExtra(
										WeiboUtil.EXTRA_TOKEN_SECRET);
								AccessToken accessToken = new AccessToken(
										mAccessToken, mTokenSecret);
								Weibo weibo = Weibo.getInstance();
								weibo.setAccessToken(accessToken);
								if (!TextUtils.isEmpty((String) (weibo
										.getAccessToken().getToken()))) {
									update(weibo, send_msg);
								} else {
									showToast("请先登录授权!");
								}
							} else if (weiboFlag == 1) {
								oAuth = (OAuthV2) getIntent()
										.getSerializableExtra(
												WeiboUtil.TX_OAUTH);
								shareWeibo(send_msg);
							}
						} else {
							showToast("最多140个字符!");
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else if (v == btn_cancel) {
				finish();
			}
		}
	};

	/**
	 * 腾讯微博
	 * 
	 * @author wubo
	 * @createtime 2012-9-21
	 * @param content
	 */
	public void shareWeibo(String content) {
		TAPI tapi = new TAPI(OAuthConstants.OAUTH_VERSION_2_A);
		String response = "";
		try {
			response = tapi.add(oAuth, "json", content, "");
			if (response != null) {
				JSONObject jsonObject = new JSONObject(response);
				int errcode = Integer.parseInt(jsonObject.get("errcode")
						.toString());
				if (errcode == 0) {
					showToast("分享成功!");
				} else {
					showToast("分享失败!");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			showToast("分享失败!");
			e.printStackTrace();
		}
		tapi.shutdownConnection();
		WeiboShareActivity.this.finish();
	}

	public void getShortUri() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", application.getUid()));
		paras.add(new Parameter("agentUid", application.getUid()));
		paras.add(new Parameter("speciesId", goodsInfo.getSpeciesId()));
		paras.add(new Parameter("prodNum", goodsInfo.getSku()));
		paras.add(new Parameter("unitId", goodsInfo.getSpecificationId()));
		serverMana.supportRequest(AgentConfig.ShortUrlGenerator(), paras, true,
				"查询中,请稍等 ...", 999);
	}

	/**
	 * 新浪微博
	 * 
	 * @author wubo
	 * @createtime 2012-9-21
	 * @param weibo
	 * @param status
	 * @return
	 * @throws WeiboException
	 */
	public String update(Weibo weibo, String status) throws WeiboException {
		WeiboParameters bundle = new WeiboParameters();
		bundle.add("source", WeiboUtil.SINA_CONSUMER_KEY);
		bundle.add("status", status);
		String rlt = "";
		String url = Weibo.SERVER + "statuses/update.json";
		AsyncWeiboRunner weiboRunner = new AsyncWeiboRunner(weibo);
		weiboRunner.request(this, url, bundle, Utility.HTTPMETHOD_POST, this);

		return rlt;
	}

	@Override
	public void onComplete(String response) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(WeiboShareActivity.this, "分享成功!",
						Toast.LENGTH_LONG).show();
			}
		});

		this.finish();
	}

	@Override
	public void onIOException(IOException e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onError(final WeiboException e) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Log.d("Debug",
						"WeiboException:" + e.getStatusCode() + ","
								+ e.getMessage());
				int errcode = e.getStatusCode();
				if (errcode == 20017 || errcode == 20019) {
					showToast("已经分享过该消息了!");
				} else if (errcode == 20020 || errcode == 20021) {
					showToast("包含非法或者广告信息!");
				} else if (errcode == 20032) {
					showToast("发布成功,但服务器可能会有延迟!");
				} else {
					Toast.makeText(WeiboShareActivity.this, "分享失败!",
							Toast.LENGTH_LONG).show();
				}
			}
		});

		this.finish();
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;
		switch (caseKey) {
		case 999:
			ShortUrlResult userResult = JSONUtil.fromJson(
					supportResponse.toString(), ShortUrlResult.class);
			if (userResult != null) {
				if (userResult.getErrorCode() == BaseResult.SUCCESS) {
					uri = userResult.getShortUrl();
					tv_uri.setText("地址:" + uri);
				} else if (userResult.getErrorCode() == BaseResult.FAIL) {
					showToast(userResult.errorMessage);
				} else {
					showToast(R.string.to_server_fail);
					return;
				}
			} else {
				showToast(R.string.to_server_fail);
				return;
			}
			break;
		}
	}
}
