package com.gsta.v2.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.gsta.v2.activity.adapter.CityGridAdapter;
import com.gsta.v2.db.ICityDao;
import com.gsta.v2.db.impl.CityDaoImpl;
import com.gsta.v2.entity.City;

/**
 * 选择省份 城市 返回给调用的 Activity MsgChatActivity EditUserInfoActivity
 * 
 * @author caosq 2013-5-3
 */
public class SelectCityAcitivy extends BaseActivity {

    private GridView gv_1;
    private TextView tv_title, tv_des;
    private Button btn_back, btn_sub, btn_cancel;

    private List<City> provinceData = new ArrayList<City>();;// 一级
    private List<City> cityData = new ArrayList<City>();// 二级

    // private Spinner s_first;// 省选择
    private CityGridAdapter adapt;
    private String cho1 = "0"; // 用户之前选择了哪个省
    private String cho2 = "0"; // 用户选择了哪个城市

    public static final String PROVINCE_FLAG = "provinceFlag";// 用户原来选择的省份 也是返回的标记
    public static final String CITY_FLAG = "cityFlag";

    private final int province_code = 0x88;
    private final int city_code = 0x89;
    private int selectIndex = province_code; // 用户选择省份还是城市

    // private Spinner s_second;// 市选择
    // private CityGridAdapter sa2;

    private ICityDao cityDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.a_select_city);
        cho1 = getIntent().getStringExtra(PROVINCE_FLAG);

        initView();
        initData();
        initListener();
    }

    private void initView() {
        gv_1 = (GridView) findViewById(R.id.gv_1);
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_des = (TextView) findViewById(R.id.tv_des);

        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_cancel = (Button) findViewById(R.id.cancel);
        btn_sub = (Button) findViewById(R.id.submit);
    }

    private void initData() {
        tv_title.setText("城 市 选 择");
        cityDao = new CityDaoImpl(SelectCityAcitivy.this);
        // 通过代码查询中文名
        provinceData = cityDao.getChildCitybyId("2");
        adapt = new CityGridAdapter(SelectCityAcitivy.this, provinceData);

        gv_1.setAdapter(adapt);
    }

    private void initListener() {
        btn_back.setOnClickListener(btnClick);
        btn_cancel.setOnClickListener(btnClick);
        btn_sub.setOnClickListener(btnClick);
        gv_1.setSelection(Integer.valueOf(null == cho1 ? "0" : cho1));
        gv_1.setOnItemClickListener(gv_itemClick);
    }

    OnClickListener btnClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == btn_back.getId()) {
                SelectCityAcitivy.this.finish();
            } else if (v.getId() == btn_sub.getId()) {
                if (selectIndex == city_code) { // 返回上一级
                    if (null == cho2) {
                        showToast("请选择城市");
                    } else {
                        // 返回
                        Intent intent = new Intent();
                        intent.putExtra(PROVINCE_FLAG, cho1);
                        intent.putExtra(CITY_FLAG, cho2);
                        setResult(Activity.RESULT_OK, intent);
                    }
                } else {
                    showToast("请选择省份");
                }
            } else if (v.getId() == btn_cancel.getId()) {
                if (selectIndex == city_code) { // 返回上一级
                    adapt.setCitys(provinceData);
                    gv_1.setSelection(Integer.valueOf(null == cho1 ? "0" : cho1));
                    selectIndex = province_code;
                } else {
                    SelectCityAcitivy.this.finish();
                }
            }
        }
    };

    OnItemClickListener gv_itemClick = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            City data = (City) parent.getItemAtPosition(position);
            if (selectIndex == province_code) {
                cho1 = data.getId();
                cityData = cityDao.getChildCitybyId(data.getId());
                adapt.setCitys(cityData);
                btn_cancel.setText("返回上一级");
                selectIndex = city_code;
                cho2 = null;
                tv_des.setText(data.getName() + " >> ");
            } else if (selectIndex == city_code) {
                cho2 = data.getId();
                tv_des.setText(tv_des.getText() + data.getName());
            }
        }
    };
}
