package com.gsta.v2.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.gsta.v2.util.AgentConfig;

/**
 * 佣金结算规则展示说明
 * 
 * @author wubo
 * @date 2012-11-29
 * 
 */
public class LevelRuleActivity extends BaseActivity {

    private TextView tv_title;
    private Button btn_back;
    WebView webView;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.levelrule);
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelRuleActivity.this.finish();
            }
        });

        tv_title.setText("佣金和分成结算规则");
        webView = (WebView) findViewById(R.id.webview);
        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(AgentConfig.Deductinfo());
        // 设置Web视图
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(AgentConfig.Deductinfo());
                return true;
            }
        });

        // 设置加载进度
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // setTitle("页面加载中，请稍候..." + progress + "%");
                setProgress(progress * 100);
                // if (progress == 100) {
                // setTitle("佣金和分成结算规则");
                // }
            }
        });
    };
}
