package com.gsta.v2.activity.mytown;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.entity_v2.OrderResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 我的数码城->订单管理
 * 
 * @author caosq
 * 
 */
public class MyUserIndentQueryActivity extends TopBackActivity implements
		IUICallBackInterface {

	private PullToRefreshListView pr_user_indent;
	private ListView LView; //
	private UserIndentAdapter adapter;
	private List<Integer> params;
	private int queryNum_code = 1;
	public static final String QUERY_TYPE = "query_type";// 0
	/**
	 * 
	 * 4.待确认收货,1.待付款,2.待发货,5.已成功,6.已取消, opType =1时必填
	 * <p>
	 * 订单状态(1 已提交;2 已支付(网上支付);3 已审核;4 已配送;5 已签收;6 失败(取消); 7已拒收;8:已校验)
	 * <p>
	 * 查看物流：status=4 确认收货：status=4 取消订单：status=1,2,3 立即支付：status=1
	 * <p>
	 * 取消订单：status=1 申请退款：status=2,3
	 */
	public static final int QUERY_ALL = 0x000;
	public static final int QUERY_UNPAYMENT = 0x001;
	public static final int QUERY_UNDELIVERY = 0x002;
	public static final int QUERY_UNCHECK = 0x003;
	public static final int QUERY_UNCONFIRMED = 0x004;
	public static final int QUERY_SUCCESS = 0x005;
	public static final int QUERY_CANCEL = 0x006;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_indent_manage);
		initView();
		initData();
		initListener();
	}

	private void initView() {
		pr_user_indent = (PullToRefreshListView) findViewById(R.id.pr_user_indent);
		LView = (ListView) pr_user_indent.getRefreshableView();
	}

	private void initData() {
		params = new ArrayList<Integer>();
		adapter = new UserIndentAdapter(this, params);
		LView.setAdapter(adapter);
		// 加载后台数据
		queryIndentNumInfo(true);
	}

	private void initListener() {
		pr_user_indent.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				queryIndentNumInfo(false);
			}
		});

		LView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
			    // 查看各种订单
				Intent intent = new Intent();
				intent.setClass(MyUserIndentQueryActivity.this,
						MyUserIndentListActivity.class);
				switch (position - 1) {
				case 0:
					intent.putExtra(QUERY_TYPE, QUERY_UNCONFIRMED);
					break;
				case 1:
					intent.putExtra(QUERY_TYPE, QUERY_UNPAYMENT);
					break;
				case 2:
					intent.putExtra(QUERY_TYPE, QUERY_UNDELIVERY);
					break;
				case 3:
					intent.putExtra(QUERY_TYPE, QUERY_SUCCESS);
					break;
				case 4:
					intent.putExtra(QUERY_TYPE, QUERY_CANCEL);
					break;
				default:
					break;
				}
				startActivity(intent);
			}
		});
	}

	/**
	 * interface/order/QueryOrder
	 * 
	 * http json 1.根据登录UID查询本人待确认收货,待付款,待发货,已成功,已取消交易信息 2.查询订单状态类别数量。
	 * 
	 * @author caosq 2013-4-10 上午11:14:12
	 * @param code
	 */
	private void queryIndentNumInfo(boolean needPr) {
		ServerSupportManager serverMana = new ServerSupportManager(
				this.getParent(), this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userId", application.getUid()));
		// 操作类型：0-查询数量,1-查询订单信息
		paras.add(new Parameter("opType", "0"));
		// // 操作用户类型: 0-普通用户, 1-代理商
		// paras.add(new Parameter("userType", "0"));
		serverMana.supportRequest(AgentConfig.myQueryUserOrder(), paras,
				needPr, "加载中,请稍等 ...", queryNum_code);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		if (pr_user_indent.isRefreshing()) {
			pr_user_indent.onRefreshComplete();
		}
		if (!HttpResponseStatus(supportResponse)) {
			return;
		}
		OrderResult orderResult = JSONUtil.fromJson(supportResponse.toString(),
				OrderResult.class);
		if (null != orderResult) {
			// tv_indent_1, tv_indent_2, tv_indent_3, tv_indent_4, tv_indent_5;
			if (params.size() > 0) {
				params.clear();
			}
			int a = orderResult.getUnconfirmed();
			params.add(a);
			a = orderResult.getUnpayment();
			params.add(a);
			a = orderResult.getUndelivery();
			params.add(a);
			a = orderResult.getSuccessOrder();
			params.add(a);
			a = orderResult.getCancelOrder();
			params.add(a);
			adapter.setParams(params);
		}
	}

	private class UserIndentAdapter extends BaseAdapter {
		private ViewIndentHolder holder;
		private List<Integer> params;
		private LayoutInflater layoutInflater;
		private String[] names = { "待确认收货交易", "待付款交易", "待发货交易", "已成功交易",
				"已取消交易" };

		public UserIndentAdapter(Context context, List<Integer> params) {
			this.params = params;
			layoutInflater = LayoutInflater.from(context);
		}

		public void setParams(List<Integer> params) {
			this.params = params;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return params.size();
		}

		@Override
		public Object getItem(int position) {
			return params.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final Integer para = (Integer) getItem(position);
			if (convertView == null) {
				holder = new ViewIndentHolder();
				convertView = layoutInflater.inflate(
						R.layout.a_user_indent_query_item, null);
				holder.tv_name = (TextView) convertView
						.findViewById(R.id.tv_name);
				holder.tv_num = (TextView) convertView
						.findViewById(R.id.tv_num);
				convertView.setTag(holder);
			} else {
				holder = (ViewIndentHolder) convertView.getTag();
			}
			holder.tv_name.setText(names[position]);
			setTextView(para, holder.tv_name, holder.tv_num);
			return convertView;
		}

		private void setTextView(Integer para, TextView name, TextView value) {
			if (para > 0) {
				name.setTextColor(Color.BLACK);
				value.setTextColor(Color.RED);
			} else {
				name.setTextColor(Color.GRAY);
				value.setTextColor(Color.GRAY);
			}
			value.setText("( " + para + " )");
		}

		final class ViewIndentHolder {
			TextView tv_name, tv_num;// 这里需要HTML
		}
	}
}