package com.gsta.v2.activity.mytown;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.UserIndentListQueryAdapter;
import com.gsta.v2.activity.myshop.OrderQueryActivity;
import com.gsta.v2.activity.myshop.OrderSearchActivity;
import com.gsta.v2.entity_v2.Limit;
import com.gsta.v2.entity_v2.OrderInfo;
import com.gsta.v2.entity_v2.OrderResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 我的数码城->订单管理->订单列表 与
 * 
 * @see OrderQueryActivity 类功能相似了
 * @author caosq
 */
public class MyUserIndentListActivity extends BaseActivity implements IUICallBackInterface {

    private PullToRefreshListView pl_0;
    private ListView listView; //
    private View loadMoreView;
    private TextView tv_title;
    private Button btn_left, btn_right;

    private UserIndentListQueryAdapter adapter;
    private List<OrderInfo> items;
    // 点击item的位置
    private int btnPosition = 0;

    private QueryCondition indexQuery;
    private int code; // 相关业务分类代码
    public static final int btn_cancleIndent = 0x00A; // 取消订单
    public static final int btn_orderDrawBack = 0x00C; // 退款申请
    public static final int btn_confirmGoods = 0x00B; // 确认收货
    private View noDataLayout; // 无数据的提示 只显示两秒

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 1) {
                noDataLayout.setVisibility(View.GONE);
            }
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.a_indent_list);
        code = getIntent().getIntExtra(MyUserIndentQueryActivity.QUERY_TYPE, 1);

        initView();
        initData();
        initListener();
    }

    private void initView() {
        pl_0 = (PullToRefreshListView) findViewById(R.id.pr_1);
        listView = (ListView) pl_0.getRefreshableView();
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        btn_left = (Button) findViewById(R.id.mytitile_btn_left);
        btn_right = (Button) findViewById(R.id.mytitile_btn_right);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        loadMoreView = inflater.inflate(R.layout.loadmore, null);
        Button btn_loadmore = (Button) loadMoreView.findViewById(R.id.loadMoreButton);
        btn_loadmore.setVisibility(0);
        btn_loadmore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                indexQuery.setRefresh(false);
                queryIndentListInfo(code, true);
            }
        });

        noDataLayout = findViewById(R.id.nodata);
        ((TextView) noDataLayout.findViewById(R.id.search)).setText("还没有相关交易记录...");
        listView.setDividerHeight(0);
    }

    private void initData() {
        indexQuery = new QueryCondition();
        indexQuery.setLimit(new Limit(1, 10));
        indexQuery.setRefresh(true);
        indexQuery.setUid(application.getUid());
        indexQuery.setNeedTime(false);
        switch (code) {// 4.待确认收货,1.待付款,2.待发货,5.已成功,6.已取消, opType =1时必填
        case MyUserIndentQueryActivity.QUERY_UNCONFIRMED:
            tv_title.setText(R.string.order_UNCONFIRMED);
            break;
        case MyUserIndentQueryActivity.QUERY_UNPAYMENT:
            tv_title.setText(R.string.order_UNPAYMENT);
            break;
        case MyUserIndentQueryActivity.QUERY_UNDELIVERY:
            tv_title.setText(R.string.order_UNDELIVERY);
            break;
        case MyUserIndentQueryActivity.QUERY_SUCCESS:
            tv_title.setText(R.string.order_SUCCESS);
            break;
        case MyUserIndentQueryActivity.QUERY_CANCEL:
            tv_title.setText(R.string.order_CANCEL);
            break;
        default:
            tv_title.setText(R.string.order_ALL);
            break;
        }
        btn_left.setVisibility(0);
        btn_right.setVisibility(0);
        btn_right.setText("搜  索");
        btn_right.setBackgroundResource(R.drawable.btn_menu);
        items = new ArrayList<OrderInfo>();
        adapter = new UserIndentListQueryAdapter(this, items, code);
        listView.setAdapter(adapter);
        // 查询后台
        queryIndentListInfo(code, true);
    }

    private void initListener() {
        btn_left.setOnClickListener(btnClick);
        btn_right.setOnClickListener(btnClick);
        pl_0.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                indexQuery.getLimit().setOffset(1);
                indexQuery.setRefresh(true);
                queryIndentListInfo(code, false);
            }
        });
//        listView.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (view.getId() == R.id.btn_left) {
//
//                } else if (view.getId() == R.id.btn_right) {
//
//                }
//                OrderInfo info = (OrderInfo) parent.getItemAtPosition(position);
//            }
//        });
    }

    OnClickListener btnClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btn_left) {
                MyUserIndentListActivity.this.finish();
            } else if (v == btn_right) { //搜索
                Intent intent = new Intent(MyUserIndentListActivity.this,
                        OrderSearchActivity.class);
                intent.putExtra(FinalUtil.QUERY_TYPE, "2");
                if (null != indexQuery.getStartTime()) {
                    intent.putExtra(FinalUtil.ORDER_BEGINTIME, indexQuery.getStartTime());
                }
                if (null != indexQuery.getEndTime()) {
                    intent.putExtra(FinalUtil.ORDER_ENDTIME, indexQuery.getEndTime());
                }
                startActivityForResult(intent, 0);
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0) {
            return;
        }
        if (resultCode == 1) {
            indexQuery.setNeedTime(true);
            indexQuery.setStartTime(data.getExtras().getString(FinalUtil.ORDER_BEGINTIME));
            indexQuery.setEndTime(data.getExtras().getString(FinalUtil.ORDER_ENDTIME));
            items.clear();
            indexQuery.getLimit().setOffset(1);
            queryIndentListInfo(code, true);
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * interface/order/QueryOrder
     * 
     * http json 1.根据登录UID查询本人待确认收货,待付款,待发货,已成功,已取消交易信息 2.查询订单状态类别数量。
     * 
     * @author caosq 2013-4-10 上午11:14:12
     * @param code
     * @param needPd
     */
    private void queryIndentListInfo(int code, boolean needPd) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("userId", indexQuery.getUid()));
        // 操作类型：0-查询数量,1-查询订单信息
        paras.add(new Parameter("opType", "1"));
        // 1.待确认收货,2.待付款,3.待发货,4.已成功,5.已取消, opType =1时必填
        paras.add(new Parameter("orderType", String.valueOf(code)));
        if (indexQuery.isNeedTime()) {
            paras.add(new Parameter("startTime", indexQuery.getStartTime()));
            paras.add(new Parameter("endTime", indexQuery.getEndTime()));
        }
        paras.add(new Parameter("limit", indexQuery.getLimit().toString()));
        serverMana.supportRequest(AgentConfig.myQueryUserOrder(), paras, needPd,
                "加载中,请稍等 ...", code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse)) {
            pl_0.onRefreshComplete();
            return;
        }
        if (caseKey == btn_cancleIndent) {
            showToast("取消订单成功 ...");
            removeItemByBtn();
            return;
        } else if (caseKey == btn_confirmGoods) {
            showToast("确认收货成功 ...");
            removeItemByBtn();
            return;
        } else if (caseKey == btn_orderDrawBack) {
            showToast("退款成功 ...");
            removeItemByBtn();
            return;
        }
        pl_0.onRefreshComplete();
        if (indexQuery.isRefresh()) {
            indexQuery.setRefresh(false);
            items.clear();
        }
        OrderResult orderResult = JSONUtil.fromJson(supportResponse.toString(), OrderResult.class);
        if (null != orderResult) {
            if (orderResult.getOrderInfos() != null && orderResult.getOrderInfos().size() > 0) {
                items.addAll(orderResult.getOrderInfos());
                // 使用页数 gettCount是条数
                indexQuery.getLimit().setOffset(indexQuery.getLimit().getOffset() + 1);
                // indexQuery.getLimit().setOffset(indexQuery.getLimit().getOffset() + orderResult.getOrderInfos().size());
            }
            // if (indexQuery.getLimit().getOffset() < (orderResult.gettCount() == null ? 0 : orderResult.gettCount())) {
            if ((indexQuery.getLimit().getOffset() * indexQuery.getLimit().getLength()) < (orderResult.gettCount() == null ? 0 : orderResult
                    .gettCount())) {
                if (listView.getFooterViewsCount() == 0) {
                    listView.addFooterView(loadMoreView);
                }
            } else {
                if (listView.getFooterViewsCount() > 0) {
                    listView.removeFooterView(loadMoreView);
                }
            }
            adapter.notifyDataSetChanged();
        } else {
            showToast(R.string.to_server_fail);
        }
        if (items.size() <= 0) {
            if (noDataLayout.getVisibility() != 0) {
                showNoDataView();
            }
        }
    }

    /**
     * 从数据集中移除操作数据
     * 
     * @author caosq 2013-4-18 下午4:53:27
     */
    private void removeItemByBtn() {
        if (items.size() > btnPosition) {
            items.remove(btnPosition);
        }
        adapter.notifyDataSetChanged();
    }

    private void showNoDataView() {
        noDataLayout.setVisibility(0);
        handler.sendEmptyMessageDelayed(1, 2000);
    }

    // 查询条件类
    final class QueryCondition {
        private String uid;
        private boolean refresh; // 是否刷新 ture刷新 false加载下一页
        private Limit limit; // 这里使用页数
        private boolean needTime; // 是否需要时间
        private String startTime;
        private String endTime;

        public boolean isNeedTime() {
            return needTime;
        }

        public void setNeedTime(boolean needTime) {
            this.needTime = needTime;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public boolean isRefresh() {
            return refresh;
        }

        public void setRefresh(boolean refresh) {
            this.refresh = refresh;
        }

        public Limit getLimit() {
            return limit;
        }

        public void setLimit(Limit limit) {
            this.limit = limit;
        }
    }

    /**
     * 当点击item中的按钮时,是在哪个位置
     * 
     * @author caosq 2013-4-17 下午4:22:54
     * @param a
     */
    public void sendAdapterViewPosition(int position) {
        this.btnPosition = position;
    }

}
