package com.gsta.v2.activity.mytown;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.VoucherInfoAdapter;
import com.gsta.v2.entity_v2.Limit;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.CouponInfo;
import com.gsta.v2.response.CouponManagerResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 代金券详情
 * 
 * @author caosq 2013-4-2
 */
public class MyVoucherInfoActivity extends BaseActivity implements IUICallBackInterface {

    private static final Logger logger = LoggerFactory.getLogger();

    private TextView tv_title;

    private View nodataView;
    private PullToRefreshListView pListView;
    private ListView lView;

    private Button btn_loadmore, btn_left;
    // private RelativeLayout rr_loadmore;
    private View loadmoreView;
    @SuppressWarnings("unused")
    private ProgressBar loadmroe_pb;

    private VoucherInfoAdapter adapter;
    private List<CouponInfo> items;

    private int code; // 相关业务代码
    private QueryCondition indexQuery;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 1) {
                nodataView.setVisibility(View.GONE);
            }
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.a_voucher_info);
        // getAgentApplication();
        initView();
        initData();
        initListener();
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        btn_left = (Button) findViewById(R.id.mytitile_btn_left);
        btn_left.setVisibility(0);
        nodataView = findViewById(R.id.nodata);
        ((TextView) nodataView.findViewById(R.id.search)).setText("没有相关记录哦...");

        pListView = (PullToRefreshListView) findViewById(R.id.lv_1);
        lView = pListView.getRefreshableView();
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        loadmoreView = inflater.inflate(R.layout.loadmore, null);
        btn_loadmore = (Button) loadmoreView.findViewById(R.id.loadMoreButton);
        btn_loadmore.setVisibility(0);
        btn_loadmore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                indexQuery.setRefresh(false);
                queryCouponInfo(code);
            }
        });
        lView.setDividerHeight(0);
    }

    private void initData() {
        code = getIntent().getIntExtra(MyVoucherActivity.VOUCHER_FLAG, 2);
        indexQuery = new QueryCondition();
        indexQuery.setLimit(new Limit(0, 10));
        indexQuery.setRefresh(true);
        items = new ArrayList<CouponInfo>();
        switch (code) {
        case 2:
            tv_title.setText("未使用代金券");
            break;
        case 1:
            tv_title.setText("已使用代金券");
            break;
        case 3:
            tv_title.setText("已过期代金券");
            break;
        }
        adapter = new VoucherInfoAdapter(this, items);
        lView.setAdapter(adapter);
        queryCouponInfo(code);
    }

    private void initListener() {
        btn_left.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MyVoucherInfoActivity.this.finish();
            }
        });
        pListView.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                indexQuery.getLimit().setOffset(0);
                indexQuery.setRefresh(true);
                queryCouponInfo(code);
            }
        });
    }

    // 代金券查询接口 /interface/coupon /QueryCoupon
    private void queryCouponInfo(int a) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("userId", application.getUid()));
        paras.add(new Parameter("couponType", String.valueOf(a)));// 0~1 1. 已使用,2.未使用,3.已失效。opType =1时必填
        paras.add(new Parameter("opType", "1"));// 操作类型：0-查询数量,1-查询代金券信息
        paras.add(new Parameter("limit", indexQuery.getLimit().toString()));// 查询分页区间，格式【0,20】
        serverMana.supportRequest(AgentConfig.queryCoupon(), paras, true,
                "加载中,请稍等 ...", a);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse)) {
            pListView.onRefreshComplete();
            logger.warn(caseKey + "请求失败...");
            return;
        }
        pListView.onRefreshComplete();
        BaseResult result = JSONUtil.fromJson(supportResponse.toString(),
                BaseResult.class);
        if (result == null || result.getErrorCode() == BaseResult.PARAMETER_ERROR || result.getErrorCode() == BaseResult.SYSTEM_ERROR) {
            logger.warn(result);
            showToast(result.errorMessage);
            return;
        }

        CouponManagerResult goodsResult = JSONUtil.fromJson(supportResponse
                .toString(), CouponManagerResult.class);
        if (goodsResult != null) {
            if (indexQuery.isRefresh()) {
                indexQuery.setRefresh(false);
                items.clear();
            }
            if (goodsResult.getCoupons() != null && goodsResult.getCoupons().size() > 0) {
                items.addAll(goodsResult.getCoupons());
            }
            indexQuery.getLimit().setOffset(indexQuery.getLimit().getOffset() + indexQuery.getLimit().getLength());
            if (indexQuery.getLimit().getOffset() < goodsResult.getTcount()) {
                if (lView.getFooterViewsCount()==0) {
                    lView.addFooterView(loadmoreView);
                }
                // rr_loadmore.setVisibility(View.VISIBLE);
            } else {
                if (lView.getFooterViewsCount() > 0) {
                    lView.removeFooterView(loadmoreView);
                }
                // rr_loadmore.setVisibility(View.GONE);
            }
            adapter.setItems(items);
            if (items.size() <= 0) {
                if (nodataView.getVisibility() != 0) {
                    showNoDataView();
                }
            }
        } else {
            if (indexQuery.isRefresh()) { // 当为刷新时,.就算没从后台得到数据也要清空数据
                items.clear();
                adapter.setItems(items);
            }
            if (items.size() <= 0) {
                if (nodataView.getVisibility() != 0) {
                    showNoDataView();
                }
            }
            showToast(R.string.to_server_fail);
            return;
        }
    }

    /**
     * 
     */
    private void showNoDataView() {
        nodataView.setVisibility(0);
        handler.sendEmptyMessageDelayed(1, 2000);
    }

    // 查询条件类
    class QueryCondition {
        private boolean refresh; // 是否刷新 ture刷新 false加载下一页
        private Limit limit;

        public boolean isRefresh() {
            return refresh;
        }

        public void setRefresh(boolean refresh) {
            this.refresh = refresh;
        }

        public Limit getLimit() {
            return limit;
        }

        public void setLimit(Limit limit) {
            this.limit = limit;
        }
    }
}
