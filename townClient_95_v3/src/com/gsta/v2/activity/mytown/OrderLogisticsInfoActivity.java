package com.gsta.v2.activity.mytown;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.LogisticsInfoAdapter;
import com.gsta.v2.activity.adapter.UserIndentListQueryAdapter;
import com.gsta.v2.entity_v2.OrderLogisticsInfo;
import com.gsta.v2.util.IUICallBackInterface;

/**
 * 订单物流详情
 * 
 * @author caosq 2013-4-11
 */
public class OrderLogisticsInfoActivity extends BaseActivity implements IUICallBackInterface {

    private TextView tv_title_name;
    @SuppressWarnings("unused")
    private Button btn_search;
    @SuppressWarnings("unused")
    private ListView lv1;
    @SuppressWarnings("unused")
    private View noDataView;
    @SuppressWarnings("unused")
    private LogisticsInfoAdapter adapter;
    private List<OrderLogisticsInfo> orderLogisticsInfos;// 物流信息

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_logistics_info);
        //getAgentApplication();
        initView();
        initData();
        initListener();
    };

    private void initView() {
        lv1 = (ListView) findViewById(R.id.lv_logistics);
        noDataView = findViewById(R.id.no_data);
        tv_title_name = (TextView) findViewById(R.id.mytitle_textView);
    }

    private void initData() {
        tv_title_name.setText("订单物流详情");
        
        orderLogisticsInfos = new ArrayList<OrderLogisticsInfo>();
        ArrayList<OrderLogisticsInfo> infos = extracted();
        if (null != infos) {
            orderLogisticsInfos.addAll(infos);
        }
        // 加载物流信息
        
    }

    @SuppressWarnings("unchecked")
    private ArrayList<OrderLogisticsInfo> extracted() {
        Intent intent = getIntent();
        return (ArrayList<OrderLogisticsInfo>) intent.getSerializableExtra(UserIndentListQueryAdapter.LOGISTICS_INFO);
    }

    private void initListener() {

    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        // TODO Auto-generated method stub

    }

}
