package com.gsta.v2.activity.mytown;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;

/**
 * 代金券问答
 * 
 * @author caosq 2013-4-3
 */
public class VoucherAnswerActivity extends BaseActivity {

    public static final String ANSWER_KEY = "key";
    private TextView tv_infos_1, tv_infos_2, tv_title;
    private Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.a_voucher_answer);

        tv_infos_1 = (TextView) findViewById(R.id.tv_infos_1);
        tv_infos_2 = (TextView) findViewById(R.id.tv_infos_2);
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                VoucherAnswerActivity.this.finish();
            }
        });

        final int a = getIntent().getIntExtra(ANSWER_KEY, 1);
        if (a == 1) { // 1.代金券发放规则
            tv_infos_1.setVisibility(0);
            tv_infos_2.setVisibility(View.GONE);
            tv_title.setText("代金券发放规则");
        } else if (a == 2) {// 代金券使用规则
            tv_title.setText("代金券使用规则");
            tv_infos_1.setVisibility(View.GONE);
            tv_infos_2.setVisibility(0);
        } else if (a == 3) {
            tv_title.setText("注册协议");
            TextView tView = (TextView) findViewById(R.id.tv_infos_3);
            tView.setMovementMethod(ScrollingMovementMethod.getInstance());
            findViewById(R.id.fl_voucher_des).setVisibility(View.GONE);
            findViewById(R.id.ll_reg_des).setVisibility(0);
        }
    }
}
