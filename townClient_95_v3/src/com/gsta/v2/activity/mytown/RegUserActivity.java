package com.gsta.v2.activity.mytown;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.response.RegisterResult;
import com.gsta.v2.response.ValidateCodeResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 注册新用户 (普通用户)
 * 
 * @author wubo
 * @createtime 2012-7-23
 */
public class RegUserActivity extends BaseActivity implements
		IUICallBackInterface {

	private LinearLayout ly_check;

	private EditText et_account_phone, et_account_email, et_check, et_pwd1,
			et_pwd2;
	// 注册协议
	private CheckBox reg_rem_password_chkbox;
	private TextView reg_treaty;

	private Button btn_check, btn_back, btn_reg, btn_title_back;
	private final int reg_Code = 11;
	private final int getCheck_Code = 12;
	// 防止输入了两个不同的手机号
	private String oldAccount = "";
	private String account = "";
	private TextView tv_title;

	private String check; // 用户输入验证码
	private String pwd1; // 密码
	private String pwd2; // 确认密码

	private long nowTime = 0;
	private long clickTime = 0;
	private String code = ""; // 系统验证码
	// 注册类型
	private CheckBox cb_phone, cb_email;
	private boolean flag = false;
	private String type = "0"; // 0 手机注册 1 邮箱注册

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		setContentView(R.layout.reg2);
		initView();
		initData();
		initListener();
	}

	private void initView() {
		reg_rem_password_chkbox = (CheckBox) findViewById(R.id.reg_rem_password_chkbox);
		reg_treaty = (TextView) findViewById(R.id.reg_treaty);
		reg_treaty.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

		ly_check = (LinearLayout) findViewById(R.id.ly_check);

		et_account_phone = (EditText) findViewById(R.id.account_phone);
		et_account_email = (EditText) findViewById(R.id.account_email);
		et_check = (EditText) findViewById(R.id.et_check);
		et_pwd1 = (EditText) findViewById(R.id.et_pwd1);
		et_pwd2 = (EditText) findViewById(R.id.et_pwd2);

		btn_check = (Button) findViewById(R.id.check_btn);
		btn_back = (Button) findViewById(R.id.back_btn);
		btn_reg = (Button) findViewById(R.id.reg_btn);

		cb_phone = (CheckBox) findViewById(R.id.cb_phone);
		cb_phone.setChecked(true);
		cb_email = (CheckBox) findViewById(R.id.cb_email);
		cb_email.setChecked(false);

		tv_title = (TextView) findViewById(R.id.mytitle_textView);
		btn_title_back = (Button) findViewById(R.id.mytitile_btn_left);
		btn_title_back.setVisibility(0);
	}

	private void initData() {
		tv_title.setText("新用户注册");
	}

	private void initListener() {
		btn_check.setOnClickListener(btnOnClickListener);
		btn_back.setOnClickListener(btnOnClickListener);
		btn_reg.setOnClickListener(btnOnClickListener);
		// btn_reg.setEnabled(false);
		btn_title_back.setOnClickListener(btnOnClickListener);

		reg_treaty.setOnClickListener(btnOnClickListener);

		cb_phone.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					cb_email.setChecked(false);
					ly_check.setVisibility(View.VISIBLE);
					et_account_phone.setVisibility(View.VISIBLE);
					et_account_email.setVisibility(View.GONE);
				} else {
					cb_email.setChecked(true);
					ly_check.setVisibility(View.GONE);
					et_account_phone.setVisibility(View.GONE);
					et_account_email.setVisibility(0);
				}
			}
		});
		cb_email.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					cb_phone.setChecked(false);
					ly_check.setVisibility(View.GONE);
					et_account_phone.setVisibility(View.GONE);
					et_account_email.setVisibility(0);
				} else {
					cb_phone.setChecked(true);
					ly_check.setVisibility(View.VISIBLE);
					et_account_phone.setVisibility(0);
					et_account_email.setVisibility(View.GONE);
				}
			}
		});
		// 同意协议
		reg_rem_password_chkbox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						flag = isChecked;
					}
				});
	}

	OnClickListener btnOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.reg_btn: // 注册按钮
				// if (type.equals("0")) {

				if (flag) {
					if (cb_phone.isChecked()) {
						type = "0";
						if (code.equals("")) {
							showToast("请先获取短信验证码!");
							return;
						}
						account = et_account_phone.getText().toString();
						if (!account.equals(oldAccount)) {
							showToast("更换了手机号,请重新取得验证码!");
							return;
						}
						nowTime = new Date().getTime();
						if (nowTime - clickTime > 1000 * 60 * 10) {
							showToast("验证码已过期,请重新点击获取!");
							return;
						}
						check = et_check.getText().toString();
						if (!check.equals(code)) {
							showToast("验证码输入错误,请重新输入!");
							return;
						}
					} else { // 邮箱注册
						type = "1";
						account = et_account_email.getText().toString();
						if (TextUtils.isEmpty(account.trim())
								|| !Util.isEmail(account)) {
							showToast("请输入正确的邮箱地址");
							return;
						}
					}
					if (!reg_rem_password_chkbox.isChecked()) {
						showToast("请先阅读注册协议,并同意该注册协议");
						return;
					}
					reg();
				} else {
					showToast("请先阅读注册协议,并同意该注册协议");
				}
				break;
			case R.id.back_btn:
				finish();
				break;
			case R.id.mytitile_btn_left:
				finish();
				break;
			case R.id.check_btn: // 获取验证码
				account = et_account_phone.getText().toString();
				if (account.length() == 0) {
					showToast("请输入账号!");
					return;
				}
				if (account.length() > 50) {
					showToast("输入的账号不合法!");
					return;
				}
				if (!Util.isCellphone(account)) {
					showToast("请输入正确的手机号!");
					return;
				}
				nowTime = new Date().getTime();
				if (nowTime - clickTime > 1000 * 60 || code.equals("")) {
					clickTime = nowTime;
					oldAccount = account;
					getCheck();
				} else {
					showToast("验证码已发送,请"
							+ (60 - ((nowTime - clickTime) / 1000)) + "秒后再尝试!");
				}
				break;
			case R.id.reg_treaty:// 注册协议
				Intent intent = new Intent();
				// intent.setClass(MyVoucherActivity.this,
				// AnswerActivity.class);
				intent.setClass(RegUserActivity.this,
						VoucherAnswerActivity.class);
				intent.putExtra(VoucherAnswerActivity.ANSWER_KEY, 3);
				startActivity(intent);
				break;
			}
		}
	};

	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return super.onKeyDown(keyCode, event);
	};

	/**
	 * 注册
	 * 
	 * @author wubo
	 * @createtime 2012-8-31
	 */
	private void reg() {

		pwd1 = et_pwd1.getText().toString();
		pwd2 = et_pwd2.getText().toString();

		// if (account.length() == 0) {
		// showToast("请输入账号!");
		// return;
		// }
		// if (account.length() > 50) {
		// showToast("输入的账号不合法!");
		// return;
		// }
		if (type.equals("0")) {
			if (!Util.isCellphone(account)) {
				showToast("请输入正确的手机号!");
				return;
			}
		}
		// else {
		// if (!Util.isEmail(account)) {
		// showToast("请输入正确的邮箱号!");
		// return;
		// }
		// }

		if (pwd1.length() < 6 || pwd1.length() > 16) {
			showToast("密码应在6~16个字符之间!");
			return;
		}

		if (!pwd1.equals(pwd2)) {
			showToast("两次输入密码不一致!");
			return;
		}

		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("account", account));// 登录账号
		paras.add(new Parameter("type", type));
		paras.add(new Parameter("password", pwd1));
		serverMana.supportRequest(AgentConfig.UserRegister(), paras, true,
				"提交中,请稍等 ...", reg_Code);
	}

	/**
	 * 获取验证码
	 * 
	 * @author wubo
	 * @time 2012-12-27
	 * 
	 */
	private void getCheck() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("phone", account));// 登录账号
		paras.add(new Parameter("type", String.valueOf(type)));// 登录密码
		serverMana.supportRequest(AgentConfig.GetValidateCode(), paras, true,
				"发送中,请稍等 ...", getCheck_Code);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		if (!HttpResponseStatus(supportResponse))
			return;

		switch (caseKey) {
		case reg_Code:
			RegisterResult br = JSONUtil.fromJson(supportResponse.toString(),
					RegisterResult.class);
			if (br == null || br.getErrorCode() == 999) {
				showToast(R.string.to_server_fail);
				return;
			}
			if (br.getErrorCode() == 0) {
				showToast(br.getErrorMessage());
				return;
			}
			showToast("恭喜...注册成功,你的帐号是 " + account);
			setResult(RESULT_OK,
					new Intent().putExtra(AgentConfig.LOGIN_IN_NAME, account));
			finish();
			break;
		case getCheck_Code:
			ValidateCodeResult vcr = JSONUtil.fromJson(
					supportResponse.toString(), ValidateCodeResult.class);
			if (vcr == null) {
				showToast(R.string.to_server_fail);
				return;
			}
			if (vcr.getCode() == null || vcr.getCode().equals("")) {
				showToast(vcr.getErrorMessage());
				return;
			} else {
				showToast("验证码已发送到手机:" + account + ",请注意查收,10分钟之内有效!");
			}
			code = vcr.getCode();
			break;
		}
	}
}