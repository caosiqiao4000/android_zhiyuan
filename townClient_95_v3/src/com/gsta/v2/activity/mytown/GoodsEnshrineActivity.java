package com.gsta.v2.activity.mytown;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.activity.adapter.GoodsEnshrineAdapter;
import com.gsta.v2.activity.goods.OtherGoodsDetailActivity;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.response.ProductCollectResult;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 
 * @author longxianwen
 * @createTime Apr 1, 2013 09:15:03 AM
 * @version: 1.0
 * @desc:商品收藏
 */
public class GoodsEnshrineActivity extends TopBackActivity implements
        IUICallBackInterface {

    private PullToRefreshListView pl_goods;
    private Context context = GoodsEnshrineActivity.this;
    private ListView goodsListView; // 商品列表
    private GoodsEnshrineAdapter goodsAdapter; // 商品数据源
    // 加载更多
    private View loadmoreView;
    private ProgressBar loadmroe_pb;
    private List<GoodsInfo> gdInfos = new ArrayList<GoodsInfo>();
    // 长按 对话框
    private MyDialog delAgentShopDialog;

    final int load_querycode = 11; // 查询标识
    final int cancel_code = 12;
    private int pageCount = 0; // 分页

    // 长按商品收藏接收handler，弹出对话框
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case 1:
                final GoodsInfo info = (GoodsInfo) msg.obj;
                ListView listView = (ListView) LayoutInflater.from(GoodsEnshrineActivity.this.getParent()).inflate(R.layout.dialog_list_view, null);
                String[] objects = { "查看详情", "取消收藏" };
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(GoodsEnshrineActivity.this.getParent(), R.layout.dialog_text_view, objects);
                listView.setAdapter(adapter);
                // 设置监听器
                listView.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        switch (position) {
                        case 0:
                            goToShop(info);
                            break;
                        case 1:
                            agentShopDel(info);
                            break;
                        }
                        MyDialog.dismiss(delAgentShopDialog);
                    }
                });
                delAgentShopDialog = MyDialog.showDialog(GoodsEnshrineActivity.this.getParent(), info.getProdName(), listView);
                break;

            default:
                break;
            }
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.goods_ensh);
        init();

        initView();
        initData();
        initListener();
    }

    // 初始化数据
    private void init() {
        load();
    }

    private void initData() {
        goodsAdapter = new GoodsEnshrineAdapter(context, gdInfos);
        goodsListView.setAdapter(goodsAdapter);
    }

    private void initView() {
        pl_goods = (PullToRefreshListView) findViewById(R.id.pl_goods);
        goodsListView = pl_goods.getRefreshableView();

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        loadmoreView = inflater.inflate(R.layout.loadmore, null);
        Button btn_loadmore = (Button) loadmoreView.findViewById(R.id.loadMoreButton);
        btn_loadmore.setVisibility(0);
        btn_loadmore.setOnClickListener(myClickListener);

        loadmroe_pb = (ProgressBar) loadmoreView.findViewById(R.id.loadmroe_pb);
    }

    private void initListener() {
        // 长按
        goodsListView.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                    int position, long id) {
                GoodsInfo info = (GoodsInfo) parent.getItemAtPosition(position);
                // 弹出操作对话框
                Message message = handler.obtainMessage();
                message.what = 1;
                message.obj = info;
                handler.sendMessage(message);
                return false;
            }

        });
        // 点击
        goodsListView.setOnItemClickListener(itemClickListener1);
        // 下拉刷新
        pl_goods.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageCount = 0;
                gdInfos.clear();
                load();
            }
        });
    }

    /**
     * 按钮单击事件
     */
    OnClickListener myClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.loadMoreButton:
                loadmroe_pb.setVisibility(View.VISIBLE);
                load();
                break;
            }
        }
    };

    OnItemClickListener itemClickListener1 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            GoodsInfo obj = (GoodsInfo) parent.getAdapter().getItem(position);
            goToShop(obj);
        }
    };

    /**
     * 
     * @author longxianwen
     * @Title: goToShop
     * @Description: 跳转到商品详情
     * @return void 返回类型
     */
    private void goToShop(GoodsInfo info) {
        Intent intent = new Intent(GoodsEnshrineActivity.this,
                OtherGoodsDetailActivity.class);
        intent.putExtra(FinalUtil.GOODSINFO, info);
        // intent.putExtra(FinalUtil.AGENT_UID, agentUid);
        // intent.putExtra(FinalUtil.AGENT_NAME, tv_title.getText());
        intent.putExtra(OtherGoodsDetailActivity.GOODS_ISHAVE, OtherGoodsDetailActivity.GOODS_HAVE);
        startActivity(intent);
    }

    /**
     * 商品收藏管理接口
     * 
     * @author longxianwen
     * @Title: agentShopDel
     * @Description: 取消收藏
     * @return void 返回类型
     */
    private void agentShopDel(GoodsInfo info) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("userId", application.getUid()));// 登录密码
        paras.add(new Parameter("specificationId", info.getSpecificationId()));
        paras.add(new Parameter("speciesId", info.getSpeciesId()));
        paras.add(new Parameter("opType", "1"));
        serverMana
                .supportRequest(
                        AgentConfig.cancelGoodsCollect(),
                        paras, false, "提交中,请稍等 ....", cancel_code);
    }

    // 从服务器获得数据
    private void load() {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("userId", application.getUid()));
        paras.add(new Parameter("limit", pageCount + ",10"));// 分页
        serverMana
                .supportRequest(
                        AgentConfig.getGoodsCollect(),
                        paras, false, "提交中,请稍等 ....", load_querycode);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_goods.isRefreshing()) {
            pl_goods.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }

        ProductCollectResult pcr = JSONUtil.fromJson(
                supportResponse.toString(), ProductCollectResult.class);
        switch (caseKey) {
        case load_querycode:
            if (pcr != null) {
                if (pcr.getConcernProducts() != null && pcr.getConcernProducts().size() > 0) {
                    gdInfos.addAll(pcr.getConcernProducts());
                    pageCount += pcr.getConcernProducts().size();
                    goodsAdapter.setGdInfos(gdInfos);
                    if (pcr.getTcount() > gdInfos.size() && goodsListView.getFooterViewsCount() == 0) {
                        loadmroe_pb.setVisibility(View.GONE);
                        goodsListView.addFooterView(loadmoreView);
                    }
                } else {
                    loadmroe_pb.setVisibility(View.VISIBLE);
                    goodsListView.removeFooterView(loadmoreView);
                    showToast("没有为您搜索到更多的商品收藏信息!");
                }
            } else {
                showToast(R.string.to_server_fail);
            }
            break;
        case cancel_code:
            if (pcr != null) {
                load();
                Toast.makeText(context, "删除成功!", Toast.LENGTH_SHORT).show();
            } else {
                showToast(R.string.to_server_fail);
            }
            break;
        }
    }
}
