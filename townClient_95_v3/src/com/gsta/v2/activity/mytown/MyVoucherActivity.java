package com.gsta.v2.activity.mytown;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.response.CouponManagerResult;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 数码城->代金券
 * 
 * @author caosq 2013-4-9
 */
public class MyVoucherActivity extends TopBackActivity implements IUICallBackInterface {

    private static final Logger logger = LoggerFactory.getLogger();
    private LinearLayout lv_frist, lv_two, lv_three, lv_four;
    private TextView tv_info1, tv_info2, tv_voucher_2, tv_voucher_3, tv_voucher_4;
    private int[] tvNameId = { 0, 0, R.id.tv_v_n2, R.id.tv_v_n3, R.id.tv_v_n4 };
    private TextView[] tvId = { tv_info1, tv_info2, tv_voucher_2, tv_voucher_3, tv_voucher_4 };
    private final int queryNum_code = 0x11; // 查数量
    private final int queryInfo_code = 0x12;// 查信息
    private final int activationCoupon_code = 0x13;// 激活

    private MyDialog activateVoucherDialog; // 激活代金券
    public static final String VOUCHER_FLAG = "voucher_flag";

    @SuppressWarnings("unused")
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {

        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_voucher_web);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        lv_frist = (LinearLayout) findViewById(R.id.lv_frist);
        lv_two = (LinearLayout) findViewById(R.id.lv_two);
        lv_three = (LinearLayout) findViewById(R.id.lv_three);
        lv_four = (LinearLayout) findViewById(R.id.lv_four);

        tvId[2] = (TextView) findViewById(R.id.tv_voucher_2);
        tvId[3] = (TextView) findViewById(R.id.tv_voucher_3);
        tvId[4] = (TextView) findViewById(R.id.tv_voucher_4);
        tv_info1 = (TextView) findViewById(R.id.tv_info1);
        tv_info2 = (TextView) findViewById(R.id.tv_info2);

    }

    private void initData() {
        // 查询
        queryCoupon();
    }

    private void initListener() {
        lv_frist.setOnClickListener(listener);
        lv_two.setOnClickListener(listener);
        lv_three.setOnClickListener(listener);
        lv_four.setOnClickListener(listener);

        tv_info1.setOnClickListener(listener);
        tv_info2.setOnClickListener(listener);
    }

    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View vi) {
            // TODO Auto-generated method stub
            if (vi == lv_frist) { // 激活实体券
                View v = LayoutInflater.from(MyVoucherActivity.this.getParent()).inflate(R.layout.simple_edit_dialog, null);
                final EditText et = (EditText) v.findViewById(R.id.dialog_edit1);
                et.setHint("请输入");
                activateVoucherDialog = MyDialog.showDialog(MyVoucherActivity.this.getParent(), "实体券激活",
                        v, "激活", R.string.cancel, false, new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final String a = et.getText().toString();
                                if (!TextUtils.isEmpty(a) && a.trim().length() > 0) {
                                    callActivationCoupon(a);
                                } else {
                                    showToast("代金券密码不能为空");
                                }
                            }
                        });
            } else if (vi == tv_info1 || vi == tv_info2) { // 使用说明
                Intent intent = new Intent();
                // intent.setClass(MyVoucherActivity.this, AnswerActivity.class);
                intent.setClass(MyVoucherActivity.this, VoucherAnswerActivity.class);
                if (vi == tv_info1) {
                    intent.putExtra(VoucherAnswerActivity.ANSWER_KEY, 1);
                } else {
                    intent.putExtra(VoucherAnswerActivity.ANSWER_KEY, 2);
                }
                startActivity(intent);
            } else {
                Intent intent = new Intent();
                intent.setClass(MyVoucherActivity.this.getParent(), MyVoucherInfoActivity.class);
                switch (vi.getId()) {
                case R.id.lv_two:// 未使用代金券
                    intent.putExtra(VOUCHER_FLAG, 2);
                    break;
                case R.id.lv_three: // 已使用代金券
                    intent.putExtra(VOUCHER_FLAG, 1);
                    break;
                case R.id.lv_four:// 已过期代金券
                    intent.putExtra(VOUCHER_FLAG, 3);
                    break;
                }
                startActivity(intent);
            }
        }
    };

    /**
     * 4.2 代金券激活接口
     * 
     * <pre>
     * /interface/coupon /activationCoupon
     * </pre>
     * 
     * 输入代金券密码进行代金券激活
     * 
     * @author caosq 2013-4-2 下午3:08:42
     */
    private void callActivationCoupon(String avtivationPass) {
        ServerSupportManager serverMana = new ServerSupportManager(this.getParent(), this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("userId", application.getUid()));
        paras.add(new Parameter("couponPassword", avtivationPass));// 代金券密码
        serverMana.supportRequest(AgentConfig.activationCoupon(), paras, true,
                "加载中,请稍等 ...", activationCoupon_code);
    }

    // 代金券查询接口 /interface/coupon /QueryCoupon
    private void queryCoupon() {
        ServerSupportManager serverMana = new ServerSupportManager(this.getParent(), this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("userId", application.getUid()));
        // paras.add(new Parameter("couponType",));// 0~1 1. 已使用,2.未使用,3.已失效。opType =1时必填
        paras.add(new Parameter("opType", "0"));// 操作类型：0-查询数量,1-查询代金券信息
        serverMana.supportRequest(AgentConfig.queryCoupon(), paras, true,
                "加载中,请稍等 ...", queryNum_code);
    }

    @SuppressWarnings("unused")
    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse)) {
            logger.warn(caseKey + "请求失败...");
            return;
        }
        if (caseKey == queryNum_code || caseKey == activationCoupon_code) {// 数量查询和激活
            CouponManagerResult goodsResult = JSONUtil.fromJson(supportResponse
                    .toString(), CouponManagerResult.class);
            if (goodsResult != null) {
                // tv_voucher_2, tv_voucher_3, tv_voucher_4;
                if (caseKey == queryNum_code) {
                    int a = goodsResult.getUnused();
                    setTextView(a, 2);
                    a = goodsResult.getUsed();
                    setTextView(a, 3);
                    a = goodsResult.getExpired();
                    setTextView(a, 4);
                } else {
                    // 激活代金券成功后、返回激活成功信息
                    String couponId = goodsResult.getCoupon_Id(); // 代金券主键ID
                    String coupon_Id = goodsResult.getCoupon_Id(); // 代金券编号
                    String end_date = goodsResult.getEnd_date(); // 有效期
                    Double couponMoney = goodsResult.getCouponMoney();// 代金券金额
                    MyDialog.dismiss(activateVoucherDialog);
                    showToast("激活成功...");
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
        } else if (caseKey == queryInfo_code) {

        }
    }

    // 改变显示的效果
    private void setTextView(int a, int tv) {
        if (a > 0) {
            ((TextView) findViewById(tvNameId[tv])).setTextColor(Color.BLACK);
            tvId[tv].setTextColor(Color.RED);
        } else {
            ((TextView) findViewById(tvNameId[tv])).setTextColor(Color.GRAY);
            tvId[tv].setTextColor(Color.GRAY);
        }
        tvId[tv].setText("(" + a + ")");
    }
}
