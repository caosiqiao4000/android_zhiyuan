package com.gsta.v2.activity.mytown;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.RegisterAgentActivity;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.OnTabAactivityResultListener;
import com.gsta.v2.util.Util;

/**
 * 我的数码城
 */
public class MyTownActivity extends TopBackActivity implements IUICallBackInterface {
    private static final Logger logger = LoggerFactory.getLogger(MyUserIndentListActivity.class);
    private final String TAG = Util.getClassName();
    private ViewFlipper flipper;
    // head
    private HorizontalScrollView hs1;
    private RadioGroup rg1;
    private RadioButton r1; // 订单
    private RadioButton r2;
    private RadioButton r3; //
    private RadioButton r4;

    // 一 账号管理
    private LinearLayout lv_account_manage;
    // 二 订单
    private LinearLayout container;
    // 三
    private LinearLayout ly_goods_enshrine;
    // 四 代金券
    private LinearLayout ly_voucher;

    private final String MYINDENTQUERY_TAG = "my_indent_query";// 订单
    private final String MYVOUCHERQUERY_TAG = "my_voucher_query"; // 代金券
    private final String MYGOODSQUERY_TAG = "my_goods_query"; // 收藏
    private final String MYACCOUNTMANAGE_TAG = "my_account_manage";// 账号

    private TextView tv_title;
    // 登出
    private Button btn_right;

    private boolean[] needLoad = { false, true, true, true, true };// 下标0 不使用
    private String SELECT_INDEX = "select_index"; // flipper 选中哪一个

    private Handler handler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case 1:
                // 设置用户注销状态
                application.setLoginState(FinalUtil.LONGIN_LONGINSTATE_Destory);
                application.setAgentInfo(null);
                application.setUserinfo(null);
                application.setInviteAccount(null);
                new com.gsta.v2.db.impl.SharedPreferencesUtil(
                        MyTownActivity.this).saveString(AgentConfig.LOGIN_IN_PASS, "");
                Log.d(TAG, "用户退出登录状态");
                // 通知mainAcitivy
                sendBroadcast(new Intent(FinalUtil.RECEIVER_USER_LOGING_OUT));
                break;
            default:
                break;
            }
        };
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.mytown);
        initView();
        initData();
        initListener();
        if (null != savedInstanceState) {
            switchLayoutStateTo(flipper, savedInstanceState.getInt(SELECT_INDEX));
        }
    }

    private void initView() {
        hs1 = (HorizontalScrollView) findViewById(R.id.tab_view);
        rg1 = (RadioGroup) findViewById(R.id.rg1);
        r1 = (RadioButton) findViewById(R.id.r1);
        r2 = (RadioButton) findViewById(R.id.r2);
        r3 = (RadioButton) findViewById(R.id.r3);
        r4 = (RadioButton) findViewById(R.id.r4);
        flipper = (ViewFlipper) findViewById(R.id.myViewFlipper1);

        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText("我 的 数 码 城");

        btn_right = (Button) findViewById(R.id.mytitile_btn_right);
        btn_right.setVisibility(0);

        container = (LinearLayout) findViewById(R.id.lv_indent);
        lv_account_manage = (LinearLayout) findViewById(R.id.lv_account_manage);
        ly_goods_enshrine = (LinearLayout) findViewById(R.id.ly_goods_enshrine);
        ly_voucher = (LinearLayout) findViewById(R.id.ly_voucher);

    }

    private void initData() {
        btn_right.setText("退出登录");
        toMyAccountManageActivityOnLinearLayout();
    }

    private void initListener() {
        rg1.setOnCheckedChangeListener(checkedChangeListener);
        btn_right.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Builder builder = new AlertDialog.Builder(MyTownActivity.this);
                builder.setTitle("退出登录状态")
                        .setMessage("\t退出登录状态后,将以游客状态使用本程序\n\t游客无法使用\"我的数码城\"相关功能,要管理\"我的数码城\",请重新登录")
                        .setPositiveButton("是", logoutClickListener).setNegativeButton("否", null);
                builder.create();
                builder.show();
            }
        });
    }

    /**
     * 登出 dialog单选以及确定、取消按钮监听
     */
    private DialogInterface.OnClickListener logoutClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            // 设置为登出状态
            handler.sendEmptyMessage(1);
            dialog.dismiss();
        }
    };

    // 上面横条按钮事件
    OnCheckedChangeListener checkedChangeListener = new OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == r1.getId()) { // 帐号管理
                if (flipper.getDisplayedChild() != 0) {
                    hs1.scrollTo(0, 0);
                    switchLayoutStateTo(flipper, 0);
                    if (needLoad[1]) {
                        needLoad[1] = false;
                        toMyAccountManageActivityOnLinearLayout();
                    }
                }
                return;
            }
            if (checkedId == r2.getId()) { // 订单管理
                if (flipper.getDisplayedChild() != 1) {
                    hs1.scrollTo(0, 0);
                    switchLayoutStateTo(flipper, 1);
                    if (needLoad[2]) {
                        needLoad[2] = false;
                        toMyIndentQueryActivityOnLinearLayout();
                    }
                }
                return;
            }
            if (checkedId == r3.getId()) { // 商品收藏
                if (flipper.getDisplayedChild() != 2) {
                    hs1.scrollTo(100, 0);
                    switchLayoutStateTo(flipper, 2);
                    if (needLoad[3]) {
                        needLoad[3] = false;
                        toMyIntentGoodsActivityOnLinearLayout();
                    }
                }
                return;
            }
            if (checkedId == r4.getId()) { // 代金券
                if (flipper.getDisplayedChild() != 3) {
                    hs1.scrollTo(200, 0);
                    switchLayoutStateTo(flipper, 3);
                    if (needLoad[4]) {
                        needLoad[4] = false;
                        toMyVoucherActivityOnLinearLayout();
                    }
                }
                return;
            }
        }
    };

    /**
     * 账号管理
     */
    private void toMyAccountManageActivityOnLinearLayout() {
        Intent intent = new Intent();
        intent.setClass(MyTownActivity.this, MyAccounManageActivity.class);
        intent.putExtra(IFNEED_TITLE, false);
        startActivity(MYACCOUNTMANAGE_TAG, intent, lv_account_manage);
    }

    /**
     * 到我的订单管理
     */
    private void toMyIndentQueryActivityOnLinearLayout() {
        Intent intent = new Intent();
        intent.setClass(MyTownActivity.this, MyUserIndentQueryActivity.class);
        intent.putExtra(IFNEED_TITLE, false);
        startActivity(MYINDENTQUERY_TAG, intent, container);
    }

    /**
     * @author longxianwen
     * @Title: toMyIntentGoodsActivityOnLinearLayout
     * @Description: 跳转到商品收藏
     * @return void 返回类型
     */
    private void toMyIntentGoodsActivityOnLinearLayout() {
        Intent intent = new Intent();
        intent.setClass(MyTownActivity.this, GoodsEnshrineActivity.class);
        startActivity(MYGOODSQUERY_TAG, intent, ly_goods_enshrine);
    }

    /**
     * 跳转到代金券 ly_voucher
     * 
     * @author caosq 2013-4-2 上午9:53:40
     */
    private void toMyVoucherActivityOnLinearLayout() {
        Intent intent = new Intent();
        intent.setClass(MyTownActivity.this, MyVoucherActivity.class);
        startActivity(MYVOUCHERQUERY_TAG, intent, ly_voucher);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse)) {
            logger.warn(supportResponse);
            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    OnItemClickListener listClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            switch (position) {
            case 0:
                Intent intent = new Intent();
                intent.setClass(MyTownActivity.this, RegisterAgentActivity.class);
                startActivity(intent);
                break;
            case 1:

                break;
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 取得当前活动的Activity
        Activity liveActivity = getLocalActivityManager().getCurrentActivity();
        // 回调方法
        OnTabAactivityResultListener onTabAactivityResultListener = (OnTabAactivityResultListener) liveActivity;
        onTabAactivityResultListener.onTabActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SELECT_INDEX, flipper.getDisplayedChild());
        super.onSaveInstanceState(outState);
    }
}