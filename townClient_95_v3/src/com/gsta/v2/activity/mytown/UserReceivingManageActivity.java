package com.gsta.v2.activity.mytown;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.util.IUICallBackInterface;

/**
 * 用户收货地址管理页
 * 
 * @author caosq 2013-5-3
 */
public class UserReceivingManageActivity extends BaseActivity implements IUICallBackInterface {

    private Button btn_sub, btn_back;
    private TextView tv_title;
    private ListView lv_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.a_user_rece_adress_manage);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText("收 货 地 址");

        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);
        btn_sub = (Button) findViewById(R.id.submit);

        LayoutInflater flater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lv_address = (ListView) findViewById(R.id.lv_address);
        View nullView = flater.inflate(R.layout.nomore, null);
        ((TextView) nullView.findViewById(R.id.search)).setText("还没有收货地址");
        lv_address.setEmptyView(nullView);
    }

    private void initData() {
        // TODO Auto-generated method stub

    }

    private void initListener() {
        btn_back.setOnClickListener(btnClick);
        btn_sub.setOnClickListener(btnClick);
        // 省份选择
        // et_2.setOnClickListener(btnClick);
    }

    private final int a_result_code = 0xB;

    OnClickListener btnClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == btn_back.getId()) {
                UserReceivingManageActivity.this.finish();
            } else if (v.getId() == btn_sub.getId()) {
                // 提交新增收货地址
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == a_result_code) {

        }
    };

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {

    }

}
