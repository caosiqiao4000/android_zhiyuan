package com.gsta.v2.activity.mytown;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.RegisterAgentActivity;
import com.gsta.v2.activity.UpdatePassActivity;
import com.gsta.v2.db.impl.SharedPreferencesUtil;
import com.gsta.v2.entity.BaseUserinfo;
import com.gsta.v2.response.LoginResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.CameraAndShowDiagleActivity;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.OnTabAactivityResultListener;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 我的账号管理
 * 
 * @author caosq
 */
public class MyAccounManageActivity extends CameraAndShowDiagleActivity implements
        IUICallBackInterface, OnTabAactivityResultListener {

    private LinearLayout ll_btn;
    private LinearLayout ly_nickName;
    private LinearLayout ly_name;
    // private LinearLayout ly_mobile;
    private LinearLayout ly_take_address; // 收货地址
    private LinearLayout ly_contact_address;
    private TextView tv_account_name, tv_nickName, tv_name, tv_mobile, tv_take_address, tv_contact_address;
    private TextView tv_upgrade_agent; // 升级代理商
    private ImageView iv_head;
    private ImageView iv_edit_pass;
    private Button btn_subb, btn_cancel;
    //
    private BaseUserinfo saveInfo;// 备用
    private static final String SAVE_USER_INFO = "saveUserInfo";

    private final int queryUser_code = 0xaa;
    private final int upData_code = 0xaa1;
    // 上传用户头像
    private final int upload_userIcon_code = 0xaa2;
    // 查询用户头像
    private boolean changeText = false; // 是否更改了相关信息

    // 更改图片 时 因为后台没有改变图片地址 使用它改变手机缓存中的图片
    private String shopIconChange = null;

    private SharedPreferencesUtil mUtil;

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (null != savedInstanceState) {
            restorInfo(savedInstanceState.getSerializable(SAVE_USER_INFO));
        }
    }

    private void restorInfo(Serializable serializable) {
        saveInfo = (BaseUserinfo) serializable;
        changeTextView(saveInfo);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_account_manage);
        initView();
        initData();
        initListener();
        if (null != savedInstanceState) {
            restorInfo(savedInstanceState.getSerializable(SAVE_USER_INFO));
        }
    }

    private void initView() {

        tv_upgrade_agent = (TextView) findViewById(R.id.tv_upgrade_agent);
        if (application.getAgentStatu() == FinalUtil.AgentStatu_normal) {
            tv_upgrade_agent.setVisibility(View.GONE);
        }

        ly_nickName = (LinearLayout) findViewById(R.id.ly_nickName);
        ly_name = (LinearLayout) findViewById(R.id.ly_name);
        // ly_mobile = (LinearLayout) findViewById(R.id.ly_mobile);
        ly_take_address = (LinearLayout) findViewById(R.id.ly_take_address);
        ly_contact_address = (LinearLayout) findViewById(R.id.ly_contact_address);
        ll_btn = (LinearLayout) findViewById(R.id.ll_btn);
        ll_btn.setVisibility(View.GONE);
        tv_account_name = (TextView) findViewById(R.id.tv_account_name);
        tv_nickName = (TextView) findViewById(R.id.tv_nickName);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_mobile = (TextView) findViewById(R.id.tv_mobile);
        tv_take_address = (TextView) findViewById(R.id.tv_take_address);
        tv_contact_address = (TextView) findViewById(R.id.tv_contact_address);

        iv_head = (ImageView) findViewById(R.id.iv_head);
        iv_edit_pass = (ImageView) findViewById(R.id.iv_edit_pass);
        btn_subb = (Button) findViewById(R.id.btn_subb);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
    }

    private void initData() {
        mUtil = new com.gsta.v2.db.impl.SharedPreferencesUtil(
                MyAccounManageActivity.this);
        // 加载后台数据
        queryUserAccount(true);
    }

    private void initListener() {
        ly_nickName.setOnClickListener(lv_click);
        ly_name.setOnClickListener(lv_click);
        // ly_mobile.setOnClickListener(lv_click);
        ly_take_address.setOnClickListener(lv_click);
        ly_contact_address.setOnClickListener(lv_click);

        iv_head.setOnClickListener(iv_click);
        iv_edit_pass.setOnClickListener(iv_click);
        btn_cancel.setOnClickListener(iv_click);
        btn_subb.setOnClickListener(iv_click);
        tv_upgrade_agent.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_upgrade_agent.setOnClickListener(iv_click);
    }

    // 修改各项的值
    OnClickListener lv_click = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.ly_nickName:
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STRING, tv_nickName.getText().toString());
                mShowDialog(DIALOG_ID_nickName, bundle);
                break;
            case R.id.ly_name:
                Bundle bundle1 = new Bundle();
                bundle1.putString(KEY_STRING, tv_name.getText().toString());
                mShowDialog(DIALOG_ID_name, bundle1);
                break;
            case R.id.ly_mobile:
                Bundle bundle2 = new Bundle();
                bundle2.putString(KEY_STRING, tv_mobile.getText().toString());
                mShowDialog(DIALOG_ID_mobile, bundle2);
                break;
            case R.id.ly_take_address: // 收货地址
                // Bundle bundle3 = new Bundle();
                // bundle3.putString(KEY_STRING, tv_take_address.getText().toString());
                // mShowDialog(DIALOG_ID_address, bundle3);
                // 跳转到地址管理页
                // Intent intent = new Intent();
                // intent.setClass(MyAccounManageActivity.this, UserReceivingManageActivity.class);
                // startActivity(intent);
                break;
            case R.id.ly_contact_address: // 联系地址
                Bundle bundle4 = new Bundle();
                bundle4.putString(KEY_STRING, tv_contact_address.getText().toString());
                mShowDialog(DIALOG_ID_contantAddress, bundle4);
                break;
            default:
                break;
            }

        }
    };

    /**
     * dialog单选以及确定、取消按钮监听
     */
    private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (dialog != null) {
                // 取消浮层
                dialog.cancel();
            }
            setTextView(which);// 赋值
        }
    };

    // 图片 点击
    OnClickListener iv_click = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.iv_head: // 头像
                mShowDialog(DIALOG_ID_IMAGE);
                break;
            case R.id.iv_edit_pass: // 修改密码
                Intent intent = new Intent(MyAccounManageActivity.this,
                        UpdatePassActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_subb: // 提交
                // 保存用户修改的信息
                updataAccout(false);
                break;
            case R.id.btn_cancel: // 还原
                changeTextView(saveInfo);
                break;
            }
            // 升级代理商
            if (v.getId() == tv_upgrade_agent.getId()) {
                Intent intent = new Intent();
                intent.setClass(MyAccounManageActivity.this.getParent(), RegisterAgentActivity.class);
                startActivity(intent);
            }
        }
    };

    /**
     * 从Dialog给textview赋值
     * 
     * @param index
     */
    private void setTextView(int index) {
        CharSequence cs = et_dialog.getText();
        switch (currentDialogId) {
        case DIALOG_ID_nickName:
            if (!tv_nickName.getText().equals(cs)) {
                tv_nickName.setText(cs);
                changeText = true;
            }
            break;
        case DIALOG_ID_name:
            if (!tv_name.getText().equals(cs)) {
                tv_name.setText(cs);
                changeText = true;
            }
            break;
        case DIALOG_ID_mobile:
            if (!tv_mobile.getText().equals(cs)) {
                if (!Util.isCellphone(cs.toString())) {
                    showToast("手机号码格式不正确!");
                    return;
                }
                tv_mobile.setText(cs);
                changeText = true;
            }
            break;
        case DIALOG_ID_address:
            if (!tv_take_address.getText().equals(cs)) {
                tv_take_address.setText(cs);
                changeText = true;
            }
            break;
        case DIALOG_ID_contantAddress:
            if (!tv_contact_address.getText().equals(cs)) {
                tv_contact_address.setText(cs);
                changeText = true;
            }
            break;
        }
        if (changeText) {
            ll_btn.setVisibility(0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVE_USER_INFO, saveInfo);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.i("TopBackActivity", "" + keyCode);
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goToNotificationIntent();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_MENU) {
            sendBroadcast(new Intent(FinalUtil.RECEIVER_USER_PROGRAM_OUT));
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * 上传修改信息 帐号管理接口 agent/interface/usermanager/AccountManager
     * <p>
     * 包括用户基本信息设置、修改。如：昵称, 姓名, 手机号码, 收货地址, 联系地址等。
     * 
     * @author caosq 2013-5-3 下午3:21:46
     * @param b
     */
    private void updataAccout(boolean needPr) {
        ServerSupportManager serverMana = new ServerSupportManager(this.getParent(), this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));
        paras.add(new Parameter("nickName", tv_nickName.getText().toString()));
        paras.add(new Parameter("firstName", tv_name.getText().toString()));
        // paras.add(new Parameter("mobile", tv_mobile.getText().toString()));
        // paras.add(new Parameter("deliveryAddress", tv_take_address.getText().toString()));
        paras.add(new Parameter("contactAddress", tv_contact_address.getText().toString()));
        serverMana.supportRequest(AgentConfig.myUserAccount(), paras, needPr,
                "加载中,请稍等 ...", upData_code);
    }

    /**
     * 帐号查询接口 agent/interface/usermanager/QuerySubscriber
     * <p>
     * 查询用户基本信息：头像、账户名称、信用级别、姓名、收货地址等
     * 
     * @author caosq 2013-5-3 下午2:55:55
     * @param needPr
     */
    private void queryUserAccount(boolean needPr) {
        ServerSupportManager serverMana = new ServerSupportManager(this.getParent(), this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));
        serverMana.supportRequest(AgentConfig.myUserQuerySubscriber(), paras, needPr,
                "加载中,请稍等 ...", queryUser_code);
    }

    // 上传用户头像图片
    private void updateShopIcon(File file1) {
        try {
            // 与后台交互
            ServerSupportManager serverMana = new ServerSupportManager(this.getParent(),
                    this);
            List<Parameter> paras = new ArrayList<Parameter>();
            paras.add(new Parameter("uid", String.valueOf(application
                    .getUid())));
            if (file1 != null) {
                ArrayList<MyFilePart> files = new ArrayList<MyFilePart>();
                MyFilePart uploader1 = new MyFilePart(file1.getName(), file1
                        .getName(), file1, new MimetypesFileTypeMap()
                        .getContentType(file1), SyncHttpClient.CONTENT_CHARSET);
                files.add(uploader1);
                serverMana.supportRequest(AgentConfig.UpHead(), paras,
                        files, true, "提交中,请稍等 ....", upload_userIcon_code);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }
        changeText = false;
        if (caseKey == upData_code) {
            ll_btn.setVisibility(View.GONE);
            showToast("帐号管理信息修改成功");
            queryUserAccount(false);
        } else if (caseKey == upload_userIcon_code) {
            ImageUtil.saveUserIcon(photo, shopIconChange);
            // AsyncImageLoader.getAsyInstance().getImageCache()
            // .put(AgentConfig.getPicServer() + shopIconChange, new BitmapDrawable(photo));
            iv_head.setImageBitmap(photo);
            showToast("用户头像修改成功");
            // queryUserAccount(false);
        } else if (caseKey == queryUser_code) {
            LoginResult res = JSONUtil.fromJson(supportResponse.toString(),
                    LoginResult.class);
            if (null != res) {
                BaseUserinfo info = res.getUserInfo();
                tv_account_name.setText(mUtil.getString(AgentConfig.LOGIN_IN_NAME, info.getUserName()));
                if (null != info) {
                    // 信用级别
                    @SuppressWarnings("unused")
                    Integer a = info.getGrade();
                    changeTextView(info);
                    saveInfo = info;
                }
            }
        }
    }

    /**
     * @author caosq 2013-5-14 下午7:13:38
     * @param info
     */
    private void changeTextView(BaseUserinfo info) {
        if (null != info.getPhoto()) {
            // ImageUtil.getNetPicThroughoutByNet(MyAccounManageActivity.this, iv_head, shopIconChange = info.getPhoto());

            ImageUtil.getNetPicByUniversalImageLoad(null, iv_head, shopIconChange = info.getPhoto(),
                    ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888));
        }

        tv_name.setText(info.getFirstName() == null ? "" : info.getFirstName());
        tv_nickName.setText(info.getNickName() == null ? "" : info.getNickName());
        tv_mobile.setText(info.getMobile() == null ? "" : info.getMobile());
        tv_take_address.setText(info.getDeliveryAddress() == null ? "" : info.getDeliveryAddress());
        tv_contact_address.setText(info.getAddress() == null ? "" : info.getAddress());
        if (ll_btn.getVisibility() == 0) {
            ll_btn.setVisibility(View.GONE);
        }
    }

    @Override
    protected android.content.DialogInterface.OnClickListener setDialogClickListener() {
        return mDialogClickListener;
    }

    @Override
    protected Context setCreateContext() {
        return MyAccounManageActivity.this.getParent();
    }

    // 因为是activityGroup动态启动的ACTIVITY 无法直接获取 系统图片 要通过父Activity来获取
    // 原因 http://blog.csdn.net/yusewuhen/article/details/8851226
    @Override
    public void openCamera() {
        createInit();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Util.getSdkardDir(), currPhotoName);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        MyAccounManageActivity.this.getParent().startActivityForResult(intent, OPEN_CAMERA);
    }

    @Override
    public void openPicture() {
        createInit();
        Intent intent = new Intent(Intent.ACTION_PICK, null);
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                IMAGE_UNSPECIFIED);
        MyAccounManageActivity.this.getParent().startActivityForResult(intent, LOCAL_PICTURE);
    }

    /**
     * 切割图片之后返回的 Bundle数据处理，取得bitmap类型图片
     * 
     * @param extras
     *            void
     */
    @Override
    public void createPhoto(Bundle extras) {
        if (extras != null) {
            photo = extras.getParcelable("data");
            try {
                if (photo != null) {
                    imageview = iv_head;
                    File file = getCompress(75, 75, 100);
                    updateShopIcon(file);
                } else {
                    showToast(R.string.res_get_photo_fail);
                }
            } catch (Exception e) {
                showToast(R.string.res_get_photo_fail);
            }
        }
    }

    @Override
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, IMAGE_UNSPECIFIED);
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 64);
        intent.putExtra("outputY", 64);
        intent.putExtra("return-data", true);
        MyAccounManageActivity.this.getParent().startActivityForResult(intent, ZOOM_RESULT);
    }

    @Override
    public void onTabActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0)
            return;
        if (data != null) {
            photoUri = data.getData();
            if (photoUri != null) {
                String path = getPhotoPath(photoUri);
                if (path != null) {
                    setPhotoPath(path);
                }
            }
        }

        if (requestCode == LOCAL_PICTURE) { // 读取相册图片切割
            startPhotoZoom(data.getData());
        }
        if (requestCode == OPEN_CAMERA) { // 读取拍照图片切割
            File file = new File(tempPhotoPath); // 设置文件保存路径
            setPhotoPath(file.toString());
            startPhotoZoom(Uri.fromFile(file));
        }
        if (requestCode == ZOOM_RESULT) { // 切割结果
            createPhoto(data.getExtras());
        }
        if (requestCode == CAMERA) { // 拍照图片
            // File file = new File(UtilHelper.getSdkardPangkerDir() +
            // "/temp.jpg"); // 设置文件保存路径
            getPhoto(tempPhotoPath);
        }
        if (requestCode == PICTURE) { // 读取相册图片
            getPhoto(getPhotoPath());
        }
        // super.onActivityResult(requestCode, resultCode, data);
    }

    // // 测试 WEBVIEW 1
    // public void towebview1(View view) {
    // // 参数
    // String url = "http://121.33.203.178:9998/agent/index.jsp?p=" + Math.random();
    // List<Parameter> parameters = new ArrayList<Parameter>();
    // parameters.add(new Parameter("orderId", ""));
    // // parameters.add(new Parameter("systime", String.valueOf(new Date().getTime())));
    // Util.toWebViewActivity(this, AgentWebViewActivity.class, url, parameters);
    // }
    //
    // // 测试 WEBVIEW 2
    // public void towebview2(View view) {
    // // 参数
    // String url = "http://121.33.203.178:9998/agent/show.jsp?p=" + Math.random();
    // List<Parameter> parameters = new ArrayList<Parameter>();
    // parameters.add(new Parameter("orderId", ""));
    // // parameters.add(new Parameter("systime", String.valueOf(new Date().getTime())));
    // ServerSupportManager serverMana = new ServerSupportManager(this.getParent(), this);
    // List<Parameter> paras = new ArrayList<Parameter>();
    // paras.add(new Parameter("uid", "100001"));
    // paras.add(new Parameter("nickName", "feijinbo2013"));
    // paras.add(new Parameter("firstName", "费金波"));
    // serverMana.supportRequest(url, paras, true,
    // "加载中,请稍等 ...", upData_code);
    // }

}
