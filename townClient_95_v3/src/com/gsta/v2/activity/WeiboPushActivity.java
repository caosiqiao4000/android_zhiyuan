package com.gsta.v2.activity;

import java.io.IOException;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.TextCountLimitWatcher;
import com.gsta.v2.util.WeiboUtil;
import com.tencent.weibo.api.TAPI;
import com.tencent.weibo.constants.OAuthConstants;
import com.tencent.weibo.oauthv2.OAuthV2;
import com.weibo.net.AccessToken;
import com.weibo.net.AsyncWeiboRunner;
import com.weibo.net.AsyncWeiboRunner.RequestListener;
import com.weibo.net.Utility;
import com.weibo.net.Weibo;
import com.weibo.net.WeiboException;
import com.weibo.net.WeiboParameters;

/**
 * 微博推广店铺
 *@author wubo
 *@createtime 2012-8-29
 */
public class WeiboPushActivity extends BaseActivity implements RequestListener {

	TextView title;
	EditText et_msg;
	TextView tv_uri;
	Button btn_send;
	Button btn_cancel;
	private String mAccessToken = "";
	private String mTokenSecret = "";
	String send_msg = "";
	String uri = "";
	int weiboFlag = 0;
	OAuthV2 oAuth;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.weiboshare);
		//getAgentApplication();
		initView();
		initData();
		initListener();

	}

	private void initView() {
		// TODO Auto-generated method stub
		et_msg = (EditText) findViewById(R.id.et_msg);
		tv_uri = (TextView) findViewById(R.id.tv_uri);
		btn_send = (Button) findViewById(R.id.btn_send);
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		title = (TextView) findViewById(R.id.title);
		et_msg.addTextChangedListener(new TextCountLimitWatcher(100, et_msg));
	}

	private void initData() {
		// TODO Auto-generated method stub
		Intent in = this.getIntent();
		send_msg = getIntent().getStringExtra(FinalUtil.SENDMMS_MSG);
		uri = getIntent().getStringExtra(FinalUtil.SENDMMS_ADD);
		weiboFlag = in.getIntExtra(WeiboUtil.WEIBOFLAG, 0);
		et_msg.setText(send_msg);
		tv_uri.setText(AgentConfig.Service_Head+uri);
		if (weiboFlag == 0) {
			title.setText("新浪微博");
		} else if (weiboFlag == 1) {
			title.setText("腾讯微博");
		}
	}

	private void initListener() {
		// TODO Auto-generated method stub
		btn_send.setOnClickListener(listener);
		btn_cancel.setOnClickListener(listener);
	}

	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == btn_send) {
				if (uri.equals("")) {
					showToast("分享失败!");
					WeiboPushActivity.this.finish();
				} else {
					try {
						send_msg = et_msg.getText().toString() + "  "
								+ tv_uri.getText().toString();
						if (send_msg.length() <= 140) {
							if (weiboFlag == 0) {
								mAccessToken = getIntent().getStringExtra(
										WeiboUtil.EXTRA_ACCESS_TOKEN);
								mTokenSecret = getIntent().getStringExtra(
										WeiboUtil.EXTRA_TOKEN_SECRET);
								AccessToken accessToken = new AccessToken(
										mAccessToken, mTokenSecret);
								Weibo weibo = Weibo.getInstance();
								weibo.setAccessToken(accessToken);
								if (!TextUtils.isEmpty((String) (weibo
										.getAccessToken().getToken()))) {
									update(weibo, send_msg);
								} else {
									showToast("请先登录授权!");
								}
							} else if (weiboFlag == 1) {
								oAuth = (OAuthV2) getIntent()
										.getSerializableExtra(
												WeiboUtil.TX_OAUTH);
								shareWeibo(send_msg);
							}
						} else {
							showToast("最多140个字符!");
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else if (v == btn_cancel) {
				finish();
			}
		}
	};

	/**
	 * 腾讯微博
	 * 
	 * @author wubo
	 * @createtime 2012-9-21
	 * @param content
	 */
	public void shareWeibo(String content) {
		TAPI tapi = new TAPI(OAuthConstants.OAUTH_VERSION_2_A);
		String response = "";
		try {
			response = tapi.add(oAuth, "json", content, "");
			if (response != null) {
				JSONObject jsonObject = new JSONObject(response);
				int errcode = Integer.parseInt(jsonObject.get("errcode")
						.toString());
				if (errcode == 0) {
					showToast("分享成功!");
				} else {
					showToast("分享失败!");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			showToast("分享失败!");
			e.printStackTrace();
		}
		tapi.shutdownConnection();
		WeiboPushActivity.this.finish();
	}

	/**
	 * 新浪微博
	 * 
	 * @author wubo
	 * @createtime 2012-9-21
	 * @param weibo
	 * @param status
	 * @return
	 * @throws WeiboException
	 */
	public String update(Weibo weibo, String status) throws WeiboException {
		WeiboParameters bundle = new WeiboParameters();
		bundle.add("source", WeiboUtil.SINA_CONSUMER_KEY);
		bundle.add("status", status);
		String rlt = "";
		String url = Weibo.SERVER + "statuses/update.json";
		AsyncWeiboRunner weiboRunner = new AsyncWeiboRunner(weibo);
		weiboRunner.request(this, url, bundle, Utility.HTTPMETHOD_POST, this);

		return rlt;
	}

	@Override
	public void onComplete(String response) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(WeiboPushActivity.this, "分享成功!",
						Toast.LENGTH_LONG).show();
			}
		});

		this.finish();
	}

	@Override
	public void onIOException(IOException e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onError(final WeiboException e) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Log.d("Debug", "WeiboException:" + e.getStatusCode() + ","
						+ e.getMessage());
				int errcode = e.getStatusCode();
				if (errcode == 20017 || errcode == 20019) {
					showToast("已经分享过该消息了!");
				} else if (errcode == 20020 || errcode == 20021) {
					showToast("包含非法或者广告信息!");
				} else if (errcode == 20032) {
					showToast("发布成功,但服务器可能会有延迟!");
				} else {
					Toast.makeText(WeiboPushActivity.this, "分享失败!",
							Toast.LENGTH_LONG).show();
				}
			}
		});

		this.finish();
	}
}
