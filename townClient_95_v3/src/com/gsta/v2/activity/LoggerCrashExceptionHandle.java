package com.gsta.v2.activity;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.util.Log;

import com.google.code.microlog4android.Level;
import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.Util;

/**
 * 主线程异常捕捉   可用于上传服务器错误信息
 * @author caosq
 * 2013-6-15
 */
public class LoggerCrashExceptionHandle implements UncaughtExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(LoggerCrashExceptionHandle.class);
    private final static String TAG = "TownCrash";

    private AgentApplication application;
    private Context mContext;

    /** 使用Properties来保存设备的信息和错误堆栈信息 */
    List<Parameter> paras = new ArrayList<Parameter>();
    private static final String VERSION_NAME = "versionName";
    private static final String VERSION_CODE = "versionCode";
    private static final String STACK_TRACE = "stackTrace";
    private static final String CRASH_EXCEPTION = "exception";

    private Thread.UncaughtExceptionHandler defaultExceptionHandler;
    // 单例声明CustomException;
    private static LoggerCrashExceptionHandle crashException;

    private LoggerCrashExceptionHandle() {
    }

    @SuppressWarnings("javadoc")
    public static LoggerCrashExceptionHandle getInstance() {
        if (crashException == null) {
            crashException = new LoggerCrashExceptionHandle();
        }
        logger.setLevel(Level.DEBUG);
        return crashException;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        logger.error(TAG, ex);
//        application.setLoginOutIsNormal(false);
        if (!handleException(ex) && defaultExceptionHandler != null) {
            // 如果用户没有处理则让系统默认的异常处理器来处理
//            Log.e("LoginOutIsNormal", "uncaughtException set  " + application.isLoginOutIsNormal());
            logger.error("  程序异常 .s.." + Util.getSysNowTime());

            defaultExceptionHandler.uncaughtException(thread, ex);
        } else {
            // Sleep一会后结束程序
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Log.e(TAG, "Error : ", e);
            }
        }
    }

    // 自定义错误处理
    private boolean handleException(Throwable ex) {
        if (ex == null) {
            Log.w(TAG, "handleException --- ex==null");
            return true;
        }
        final String msg = ex.getLocalizedMessage();
        if (msg == null) {
            return false;
        }
        // 收集设备信息
        collectCrashDeviceInfo(mContext);
        // 保存错误报告文件
        saveThrowableInfo(ex);
        // 发送错误报告到服务器
        sendCrashReportsToServer(ex);
        return true;
    }

    private void sendCrashReportsToServer(Throwable ex) {
        paras.add(new Parameter("ranger", "5"));
        paras.add(new Parameter("phoneModel", Build.MODEL));
        paras.add(new Parameter("versionSDK", Build.VERSION.SDK));
        paras.add(new Parameter("fersionFrimware", Build.VERSION.RELEASE));
        paras.add(new Parameter("pixels", application.getWidthPixels() + "*" + application.getHeightPixels()));
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < paras.size(); i++) {
            Parameter parameter = paras.get(i);
            buf.append(parameter.mName + ":" + parameter.mValue + "\n");
        }
        logger.error(buf.toString());
    }

    private void saveThrowableInfo(Throwable ex) {
        Writer info = new StringWriter();
        PrintWriter printWriter = new PrintWriter(info);
        ex.printStackTrace(printWriter);
        Throwable cause = ex.getCause();
        while (null != cause) {
            cause.printStackTrace(printWriter);
            cause = cause.getCause();
        }
        String result = info.toString();
        printWriter.close();
        paras.add(new Parameter(CRASH_EXCEPTION, ex.getLocalizedMessage()));
        paras.add(new Parameter(STACK_TRACE, result));
        logger.error(result);
    }

    private void collectCrashDeviceInfo(Context mCon) {
        PackageManager pm = mCon.getPackageManager();
        PackageInfo pi;
        try {
            pi = pm.getPackageInfo(mCon.getPackageName(), PackageManager.GET_ACTIVITIES);
            if (null != pi) {
                paras.add(new Parameter("userid", ((AgentApplication) mContext).getUid()));
                paras.add(new Parameter(VERSION_CODE, "" + pi.versionCode));
                paras.add(new Parameter(VERSION_NAME, pi.versionName == null ? "not set" : pi.versionName));
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * @author caosq
     * 2013-6-15 上午11:39:30
     * @param context
     */
    public void initApplication(Context context) {
        mContext = context;
        application = (AgentApplication) context;
        // // 以下用来捕获程序崩溃异常
        // Intent intent = new Intent();
        // // 参数1：包名，参数2：程序入口的activity
        // intent.setClassName("com.wachoo.pangker.activity", "com.wachoo.pangker.activity.StartActivity");
        defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

}
