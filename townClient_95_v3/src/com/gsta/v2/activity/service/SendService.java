package com.gsta.v2.activity.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import mobile.http.ConnectivityHelper;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.MainActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.db.ICityDao;
import com.gsta.v2.db.IContactsDao;
import com.gsta.v2.db.impl.CityDaoImpl;
import com.gsta.v2.db.impl.ContactsDaoImpl;
import com.gsta.v2.entity.City;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.OpenFireManager;
import com.gsta.v2.util.Util;

/**
 * 服务 主要用户OPENFIRE的使用
 * 
 * @author wubo
 * @createtime 2012-8-29
 */
public class SendService extends Service {

    private static final Logger logger = LoggerFactory.getLogger(SendService.class);
    private static final String TAG = Util.getClassName();
    private AgentApplication application;
    private Bundle bundle;
    private OpenFireManager fireManager;
    private String user;
    private String pass;
    private Handler handler;

    // >>>登陆到服务器线程
    private ServiceListnenerThread mListnenerThread;
    private GuardListenerThread mGuardListenerThread;

    @Override
    public void onCreate() {
        super.onCreate();
        application = (AgentApplication) getApplicationContext();
        fireManager = OpenFireManager.getInstance(SendService.this);
        handler = new SendServiceHandler();
        logger.debug("SendService is onCreate..." + Util.getSysNowTime());
    }

    private void startThread() {
        // >>>>>登陆线程启动
        mListnenerThread = new ServiceListnenerThread();
        mListnenerThread.start();
        mGuardListenerThread = new GuardListenerThread();
        mGuardListenerThread.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flags, startId);
        }
        bundle = intent.getExtras();
        if (null == bundle) {
            return START_NOT_STICKY;
        }
        switch (bundle.getInt(FinalUtil.SENDSERVICE_FLAG)) {
        case FinalUtil.sendDefault_value:
            break;
        case FinalUtil.sendMsg:// 发送短信
            Toast.makeText(SendService.this, "开始发送信息...", Toast.LENGTH_LONG)
                    .show();
            @SuppressWarnings("unchecked")
            List<String> users = (ArrayList<String>) intent
                    .getSerializableExtra(FinalUtil.SMSUER);
            String smsContent = intent.getStringExtra(FinalUtil.SMSCONTENT);
            for (int i = 0; i < users.size(); i++) {
                SmsManager smsManager = SmsManager.getDefault();
                if (smsContent.length() > 70) {
                    List<String> contents = smsManager
                            .divideMessage(smsContent);
                    for (String sms : contents) {
                        smsManager.sendTextMessage(users.get(i), null, sms,
                                null, null);
                    }
                } else {
                    smsManager.sendTextMessage(users.get(i), null, smsContent,
                            null, null);
                }

            }
            Toast.makeText(SendService.this, "信息全部发送成功!", Toast.LENGTH_LONG)
                    .show();
            break;
        case FinalUtil.saveCitys:// 保存城市代码到数据库
            new Thread() {
                public void run() {
                    ICityDao cityDao = new CityDaoImpl(SendService.this);
                    List<City> citys = new ArrayList<City>();
                    try {
                        // 从资源目录里面读取文件流
                        InputStream inputStream = SendService.this
                                .getResources().openRawResource(R.raw.city);
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(inputStream));
                        String str = "";
                        // 把城市信息保存到list
                        while ((str = br.readLine()) != null
                                && str.length() > 0) {
                            String[] obj = str.split(",");
                            citys.add(new City(obj[0], obj[1], obj[2]));
                        }
                        // 把城市信息保存到数据库
                        cityDao.saveAllCity(citys);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                };
            }.start();
            break;
        case FinalUtil.saveContacts:// 保存手机联系人信息到数据库
            new Thread() {
                public void run() {
                    IContactsDao contactDao = new ContactsDaoImpl(
                            SendService.this);
                    contactDao
                            .getPhoneLocalContaces();
                    // application.setLocalPhone(contactDao
                    // .getPhoneLocalContaces());
                }
            }.start();
            break;
        case FinalUtil.imLogin:// openfire登录
            new Thread() {
                public void run() {
                    // 获取本地时间和系统时间的时间差异,以后台系统时间为主
                    Util.setDifference_time();
                };
            }.start();
            user = bundle.getString(FinalUtil.Login_User);
            pass = bundle.getString(FinalUtil.Login_Pass);
            new Thread() {
                public void run() {
                    // 登录openfire
                    startThread();
                    fireManager.loginIM(user, pass);
                };
            }.start();
            break;
        case FinalUtil.sendIM:// 发送openfire消息
            new Thread() {
                public void run() {
                    IMBean imBean = (IMBean) bundle
                            .getSerializable(FinalUtil.IMBEAN);
                    // 发送
                    fireManager.sendPacket(imBean);
                };
            }.start();
            break;
        case FinalUtil.sendMin:// 程序最小化到通知栏
            handler.sendEmptyMessage(FinalUtil.sendMin);
            break;
        case FinalUtil.sendUpdate:
            // String fileName = intent.getStringExtra(FinalUtil.Version_Name);
            // // 创建文件
            // Util.createFile(fileName);// 创建文件
            //
            // createNotification();// 首次创建
            //
            // createThread();// 线程下载
        case FinalUtil.exitOpenfire:
            // mListnenerThread
            // mGuardListenerThread
            if (mGuardListenerThread != null) {
                mGuardListenerThread.stopRun();
            }
            if (mListnenerThread != null) {
                mListnenerThread.stopRun();
            }
            fireManager.loginOut();

            break;

        }

        return START_NOT_STICKY;
    }

    int logintime = 0;// 登录次数

    // >>>>> 登录到IM服务器
    private boolean loginToIM() {
        // Log.i("ServiceListnenerThread", "loginToIM!");
        try {
            boolean isLogged = false;
            logintime++;
            // 判断是第几次登录，如果是第一次不需要重新初始化 XMPPConnection
            if (logintime == 0) {
                fireManager = OpenFireManager.getInstance(SendService.this);
                Log.i("ServiceListnenerThread", "logintoim--logintime=0");
            } else if (logintime < 30) {
                serviceSleeptime = 20000;
            }else {
                serviceSleeptime = 10000;
                logintime = -1;
            }
            fireManager.loginOut();
            // >>>>>>>>>判断是否连接到服务器
            if (!fireManager.isConnected()) {
                Log.i("ServiceListnenerThread", "logintoim--未连接");
                if (fireManager.connectOpenfire()) {
                    Log.i("ServiceListnenerThread", "logintoim--连接success");
                    if (fireManager.loginIM(user, pass)) {
                        fireManager.addIMListener();
                        isLogged = true;
                    } else {
                        fireManager.loginOut();
                    }
                }
            }
            // >>>>>>>>>>>>连接到服务器，直接登陆即可
            else {
                if (fireManager.loginIM(user, pass)) {
                    isLogged = true;
                } else {
                    fireManager.loginOut();
                }
            }
            return isLogged;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // >>>>登陆线程守护类
    private class GuardListenerThread extends Thread {
        // >>>>>>是否要继续进行
        private boolean isRun = true;

        @Override
        public void run() {
            while (isRun) {
                try {
                    Thread.sleep(30000);
                    if (isRun) {
                        checkServiceListnenerThread();
                    }
                } catch (Exception e) {
                    Log.e("ServiceListnenerThread", e.getMessage());
                }
            }
        }

        public void stopRun() {
            isRun = false;
        }
    }

    // 用户异常退出时使用的多次登录间隔时间
    private int serviceSleeptime = 10000;

    // >>>>>>>>>>>>>>>>> openfire登录守护线程
    public class ServiceListnenerThread extends Thread {

        private boolean isRun = true;

        @Override
        public void run() {
            super.run();
            while (isRun) {
                try {
                    // >>>>>检查checkGuardListenerThread
                    checkGuardListenerThread();

                    // 无网络情况client network
                    if (!ConnectivityHelper.ConnectivityIsAvailable(SendService.this)) {
                        // >>>>>>>>>>>> add by wangxin
                        // >>>>>>>>通知用户没有网络无法登陆
                    } else {// >>>>>>>>>>有网络的case
                        // openfire connection status : logined
                        if (fireManager.isLoggedIn()) {
                            Thread.sleep(5000);
                            continue;
                        }
                        // login to openfire
                        if (loginToIM()) {
                            // >>>>>通知登录成功

                        } else {

                            // >>>>>通知登录失败
                            Log.i("ServiceListnenerThread", ">>>>>>>>>>login---fail!");
                        }
                    }
                    Thread.sleep(serviceSleeptime);
                } catch (InterruptedException e) {
                    logger.error(TAG, e);
                    Log.i("ServiceListnenerThread", "error:" + e.getMessage());
                } catch (Exception e) {
                    logger.error(TAG, e);
                }
            }
        }

        public void stopRun() {
            isRun = false;
        }
    }

    // >>>>>>>>>检查守护线程运行情况
    private void checkGuardListenerThread() {
        // >>>>>>>检查守护线程是否启动
        if (mGuardListenerThread == null) {
            mGuardListenerThread = new GuardListenerThread();
            mGuardListenerThread.start();
        } else if (!mGuardListenerThread.isAlive()) {
            // mGuardListenerThread.stop();
            mGuardListenerThread = new GuardListenerThread();
            mGuardListenerThread.start();
        }
    }

    // >>>>>>>>>>检查登陆线程运行情况
    private void checkServiceListnenerThread() {
        // >>>检查登陆线程是否启动
        if (mListnenerThread == null) {
            mListnenerThread = new ServiceListnenerThread();
            mListnenerThread.start();
        } else if (!mListnenerThread.isAlive()) {
            mListnenerThread = new ServiceListnenerThread();
            mListnenerThread.start();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * 根据字符串显示提示
     * 
     * @param toastText
     */
    public void showToast(final String toastText) {
        Toast.makeText(SendService.this, toastText, Toast.LENGTH_SHORT).show();
    }

    /**
     * 根据Id显示提示
     * 
     * @param toastText
     */
    public void showToast(final int toastText) {
        Toast.makeText(SendService.this, toastText, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        logger.warn(TAG + " onDestroy  服务关闭..." + Util.getSysNowTime());
        // openfire登出
        if (null != fireManager) {
            fireManager.loginOut();
        }
        if (null != mGuardListenerThread) {
            mGuardListenerThread.stopRun();
        }
        if (null != mListnenerThread) {
            mListnenerThread.stopRun();
        }
        fireManager = null;
        super.onDestroy();
    }

    /**
     * 程序最小化挂通知栏
     * 
     * @author wubo
     * @createtime 2012-10-16
     */
    private void min() {
        if (application.isNotice_flag()) {
            logger.info(TAG + " min程序已经挂载通知栏 " + Util.getSysNowTime());
            return;
        }
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        int icon = R.drawable.icon; // 通知图标
        CharSequence tickerText = "电信营销客户端正在运行"; // 状态栏显示的通知文本提示
        long when = System.currentTimeMillis(); // 通知产生的时间，会在通知信息里显示
        Notification notification = new Notification(icon, tickerText, when);
        notification.flags = Notification.FLAG_AUTO_CANCEL
                | Notification.FLAG_NO_CLEAR;
        // notification.flags = Notification.FLAG_ONGOING_EVENT;
        // 实例化Intent
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        // 获得PendingIntent
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        logger.info(TAG + " PendingIntent.getActivity  min挂通知栏" + Util.getSysNowTime());

        // 设置事件信息，显示在拉开的里面
        notification.setLatestEventInfo(this, "东方数码城", "点击进入电信营销客户端", pi);
        // 发出通知
        manager.notify(0, notification);
        application.setNotice_flag(true);
        Intent home = new Intent(Intent.ACTION_MAIN);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        home.addCategory(Intent.CATEGORY_HOME);
        startActivity(home);
    }

    class SendServiceHandler extends Handler {
        @Override
        public void handleMessage(android.os.Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            if (msg.what == FinalUtil.sendMin) {
                min();
            }
        }
    }

}
