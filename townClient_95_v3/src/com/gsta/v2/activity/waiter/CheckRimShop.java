package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.RimShopAdapter;
import com.gsta.v2.activity.location.LocateManager;
import com.gsta.v2.activity.location.LocateManager.LocateListener;
import com.gsta.v2.activity.location.LocateManager.LocateType;
import com.gsta.v2.activity.location.Location;
import com.gsta.v2.activity.location.SharedPreferencesUtil;
import com.gsta.v2.activity.myshop.ShopHomePageActivity;
import com.gsta.v2.entity.ShopInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.RimShopResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 
 * @author longxianwen
 * @createTime 2013-3-26 上午11:57:10
 * @version: 1.0
 * @desc: 附近店铺
 */
public class CheckRimShop extends BaseActivity implements IUICallBackInterface {

    private String TAG = Util.getClassName();
    private Context context = CheckRimShop.this;
    private static SharedPreferencesUtil sPreferencesUtil ;
    private Button mytitile_btn_left;
    private Button btn_right;

    private View loadmoreView;
    private ProgressBar loadmroe_pb;

    private TextView mytitle_textView;
    private PullToRefreshListView pl_checktimshop;
    private ListView rimshopListView; // 周边店铺列表
    private RimShopAdapter rsadpter; // 数据源
    private List<ShopInfo> rsInfos = new ArrayList<ShopInfo>();// 周边店铺信息
    private PopupWindow window;
    private ListView list; // 弹窗里的ListView
    private double geoLat = 0; // 纬度
    private double geoLng = 0; // 经度
    private LocateManager loc;

    private int pageCount = 0; // 分页
    private final String CENTER_ALL = "全部";
    private final String CENTER_ONEHANDRED = "100";
    private final String CENTER_TWOHANDRED = "200";
    private final String CENTER_FIVEHANDRED = "500";
    private final String CENTER_ONEKM = "1000";
    private final String CENTER_LAST = "米之内";

    String KEY = "key";
    ArrayList<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

    final int load_querycode = 11; // 周边数据查询标识
    final int up_code = 12; // 上传标识
    final int cender_querycode = 13; // 中心距离查询

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.checkrimshop);

        sPreferencesUtil = new SharedPreferencesUtil(this.context);
        init();
        initView();
        initData();
        initListener();

    }

    /**
     * 
     * @author longxianwen
     * @Title: init
     * @Description: 得到当前位置的经纬度
     * @return void 返回类型
     */
    private void init() {
    	
    	//会出现数据同步。两次数据没有清除到
/*    	this.geoLat = Double.parseDouble(sPreferencesUtil.getString("lat", "0"));
    	this.geoLng = Double.parseDouble(sPreferencesUtil.getString("lng", "0"));
    	
    	if(geoLat != 0 && geoLng != 0) {
    		pageCount = 0;
            rsInfos.clear();
    		load();
    	}*/

        loc = LocateManager.getInstance(context);
        // loc.locate(this);
        loc.locate(new LocateListener() {

            @Override
            public void onLocationChanged(Location location, int locateResult,
                    LocateType locateType) {
                showToast("定位成功: " + location.getLatitude() + "," + location.getLongitude());
                // 得到定位信息
                if (location != null) {
                	//保存到SharePreferences
//                	sPreferencesUtil.saveString("lat", location.getLatitude()+"");
//                	sPreferencesUtil.saveString("lng", location.getLongitude()+"");
                    CheckRimShop.this.geoLat = location.getLatitude();
                    CheckRimShop.this.geoLng = location.getLongitude();
                    pageCount = 0;
                    rsInfos.clear();
                    load();
                    uploadLocation(geoLat, geoLng);
                    rsadpter.notifyData(geoLat, geoLng);
                }
            }
        });

    }

    /**
     * @author longxianwen
     * @Title: initView
     * @Description: 初始化组件，监听事件
     * @return void 返回类型
     */
    private void initView() {

        pl_checktimshop = (PullToRefreshListView) findViewById(R.id.pl_crs);
        rimshopListView = pl_checktimshop.getRefreshableView();

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        loadmoreView = inflater.inflate(R.layout.loadmore, null);
        Button btn_loadmore = (Button) loadmoreView.findViewById(R.id.loadMoreButton);
        btn_loadmore.setVisibility(View.GONE);
        btn_loadmore.setOnClickListener(myClickListener);

        loadmroe_pb = (ProgressBar) loadmoreView.findViewById(R.id.loadmroe_pb);

        mytitile_btn_left = (Button) findViewById(R.id.mytitile_btn_left);
        mytitile_btn_left.setVisibility(View.VISIBLE);
        mytitile_btn_left.setOnClickListener(myClickListener);

        btn_right = (Button) findViewById(R.id.mytitile_btn_right);
        // imageBut.setVisibility(View.VISIBLE);
        btn_right.setBackgroundResource(R.drawable.btn_main_more);
        btn_right.setOnClickListener(myClickListener);

        mytitle_textView = (TextView) findViewById(R.id.mytitle_textView);
        mytitle_textView.setText(R.string.msg_rimshop);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        loc.disableMyLocation();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (loc != null) {
            loc.disableMyLocation();
        }
        loc = null;
        super.onDestroy();
    }

    /**
     * 
     * @author longxianwen
     * @Title: initData
     * @Description: 初始化数据
     * @return void 返回类型
     */
    private void initData() {
        rsadpter = new RimShopAdapter(context, rsInfos, 0, 0);
        rimshopListView.setAdapter(rsadpter);
    }

    /**
     * 按钮单击事件
     */
    OnClickListener myClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.mytitile_btn_left:
                CheckRimShop.this.finish();
                break;
            case R.id.mytitile_btn_right:
                popWindow(v, listClickListener);
                break;
            case R.id.loadMoreButton:
                loadmroe_pb.setVisibility(View.VISIBLE);
                load();
                break;
            }
        }
    };

    /**
     * 按条件搜索事件
     */
    OnItemClickListener listClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            switch (position) {
            case 0:
                pageCount = 0;
                rsInfos.clear();
                load();
                break;
            case 1:
                pageCount = 0;
                rsInfos.clear();
                center_query_load(geoLat, geoLng, CENTER_ONEHANDRED);
                break;
            case 2:
                pageCount = 0;
                rsInfos.clear();
                center_query_load(geoLat, geoLng, CENTER_TWOHANDRED);
                break;
            case 3:
                pageCount = 0;
                rsInfos.clear();
                center_query_load(geoLat, geoLng, CENTER_FIVEHANDRED);
                break;
            case 4:
                pageCount = 0;
                rsInfos.clear();
                center_query_load(geoLat, geoLng, CENTER_ONEKM);
                break;
            }

            if (window != null) {
                window.dismiss();
            }
        }

    };

    /**
     * 
     * @author longxianwen
     * @Title: initListener
     * @Description: 列表单击监听事件，下拉刷新事件
     * @return void 返回类型
     */
    private void initListener() {
        pl_checktimshop.setRefreshing(true);// 设置页头可见
        rimshopListView.setOnItemClickListener(itemClickListener1);
        pl_checktimshop.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageCount = 0;
                rsInfos.clear();
                load();
            }
        });
    }

    /**
     * 列表点击事件
     */
    OnItemClickListener itemClickListener1 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                long id) {
            // TODO Auto-generated method stub
            // Toast.makeText(context, "点击了", Toast.LENGTH_SHORT).show();

            ShopInfo shopInfo = (ShopInfo) parent.getAdapter()
                    .getItem(position);

            Intent intent = new Intent();
            intent.setClass(context, ShopHomePageActivity.class);
            intent.putExtra(ShopHomePageActivity.IFMYSELF_SHOP_FLAG,
                    shopInfo.getUserKo());
            intent.putExtra(ShopHomePageActivity.MYSELF_SHOPNAME_FLAG,
                    shopInfo.getName());
            startActivity(intent);
        }
    };

    /**
     * 
     * @author longxianwen
     * @Title: load
     * @Description: 周边店铺查询
     * @return void 返回类型
     */
    protected void load() {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);

        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", String.valueOf(application.getUid())));

        paras.add(new Parameter("longitude", geoLng + ""));
        paras.add(new Parameter("latitude", geoLat + ""));
        paras.add(new Parameter("limit", pageCount + ",20"));

        serverMana.supportRequest(AgentConfig.checkRimShop(), paras, false,
                "提交中,请稍等 ....", load_querycode);
    }

    /**
     * 
     * @author longxianwen
     * @Title: uploadLocation
     * @Description: 上传自己当前位置到后台
     * @param @param geoLat
     * @param @param geoLng 设定文件
     * @return void 返回类型
     */
    private void uploadLocation(Double geoLat, Double geoLng) {
        if (application.getLoginState() != FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            return;
        }
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();

        paras.add(new Parameter("userId", application.getUid()));
        paras.add(new Parameter("longitude", geoLng + ""));
        paras.add(new Parameter("latitude", geoLat + ""));

        serverMana.supportRequest(AgentConfig.upLocation(), paras, false,
                "提交中,请稍等 ....", up_code);
    }

    /**
     * @author longxianwen
     * @Title: center_query_load
     * @Description: 中心距离查询
     * @param @param geoLat
     * @param @param geoLng
     * @param @param center_distance 范围
     * @return void 返回类型
     * @throws
     */
    private void center_query_load(Double geoLat, Double geoLng,
            String center_distance) {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();

        paras.add(new Parameter("longitude", geoLng + ""));
        paras.add(new Parameter("latitude", geoLat + ""));
        paras.add(new Parameter("distance", center_distance));
        paras.add(new Parameter("limit", pageCount + ",20"));

        serverMana.supportRequest(AgentConfig.checkCenterDistance(), paras,
                false, "提交中,请稍等 ....", cender_querycode);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        pl_checktimshop.onRefreshComplete();
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }

        switch (caseKey) {
        case load_querycode:
            RimShopResult rsr = JSONUtil.fromJson(supportResponse.toString(),
                    RimShopResult.class);
            if (rsr != null) {
                if (rsr.getNears() != null && rsr.getNears().size() > 0) {

                    rsInfos.addAll(rsr.getNears());
                    pageCount += rsr.getNears().size();
                    loadmroe_pb.setVisibility(View.GONE);
                    if (rimshopListView.getFooterViewsCount() == 0) {
                        rimshopListView.addFooterView(loadmoreView);
                    }
                    // rsadpter.setRsInfos(rsInfos);
                    rsadpter.notifyDataSetChanged();
                } else {
                    loadmroe_pb.setVisibility(View.VISIBLE);
                    if (rimshopListView.getFooterViewsCount() > 0) {
                        rimshopListView.removeFooterView(loadmoreView);
                    }
                    showToast("没有为您搜索到周边店铺!");
                }

            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;

        case cender_querycode:

            RimShopResult rsrs = JSONUtil.fromJson(supportResponse.toString(),
                    RimShopResult.class);

            if (rsrs != null) {
                if (rsrs.getNears() != null && rsrs.getNears().size() > 0) {

                    rsInfos.addAll(rsrs.getNears());
                    // rsInfos = rsr.getNears();
                    pageCount += rsrs.getNears().size();
                    // btn_loadmore.setVisibility(View.VISIBLE);
                    loadmroe_pb.setVisibility(View.GONE);
                    // rr_loadmore.setVisibility(View.VISIBLE);
                    rsadpter.notifyDataSetChanged();
                } else {
                    // btn_loadmore.setVisibility(View.GONE);
                    loadmroe_pb.setVisibility(View.VISIBLE);
                    // rr_loadmore.setVisibility(View.GONE);
                    showToast("没有为您搜索到周边店铺!");
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }

            break;

        case up_code:

            RimShopResult rr = JSONUtil.fromJson(supportResponse.toString(),
                    RimShopResult.class);

            if (rr != null) {
                if (rr.getErrorCode() == BaseResult.SUCCESS) {
                    Log.d(TAG, "位置上传成功");
                } else if (rr.getErrorCode() == BaseResult.FAIL) {
                    showToast(rr.errorMessage);
                } else {
                    showToast(R.string.to_server_fail);
                    return;
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }

    }

    /**
     * 弹窗条目数据
     * 
     * @return
     */
    public List<Map<String, Object>> createData() {

        Map<String, Object> map;

        map = new HashMap<String, Object>();
        map.put(KEY, CENTER_ALL);
        items.add(map);
        map = new HashMap<String, Object>();
        map.put(KEY, CENTER_ONEHANDRED + CENTER_LAST);
        items.add(map);
        map = new HashMap<String, Object>();
        map.put(KEY, CENTER_TWOHANDRED + CENTER_LAST);
        items.add(map);
        map = new HashMap<String, Object>();
        map.put(KEY, CENTER_FIVEHANDRED + CENTER_LAST);
        items.add(map);
        map = new HashMap<String, Object>();
        map.put(KEY, CENTER_ONEKM + CENTER_LAST);
        items.add(map);

        return items;
    }

    /**
     * 生成弹窗
     * 
     * @param parent
     *            点击哪个按钮出现
     * @param listClickListener
     *            监听弹窗里的事件
     */
    protected void popWindow(View parent, OnItemClickListener listClickListener) {
        if (window == null) {
            LayoutInflater lay = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = lay.inflate(R.layout.pop, null);
            list = (ListView) v.findViewById(R.id.pop_list);

            SimpleAdapter adapter = new SimpleAdapter(this, createData(),
                    R.layout.pop_list_item, new String[] { KEY },
                    new int[] { R.id.title });

            list.setAdapter(adapter);
            list.setItemsCanFocus(false);
            list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            list.setOnItemClickListener(listClickListener);
            // window = new PopupWindow(v, 260, 300);
            int x = (int) getResources().getDimension(R.dimen.shop_pop_x);
            int y = (int) getResources().getDimension(R.dimen.shop_pop_x);
            if (application.getAgentInfo() != null
                    && application.getAgentInfo().getAgentIdentity() != null
                    && application.getAgentInfo().getAgentIdentity() == 11) {
                y = (int) getResources().getDimension(R.dimen.shop_pop_y);
            }
            window = new PopupWindow(v, x, y);
        }
        window.setBackgroundDrawable(getResources().getDrawable(
                R.drawable.dialog_1));
        window.setFocusable(true);
        window.setOutsideTouchable(false);
        window.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                // TODO Auto-generated method stub
            }
        });
        window.update();
        window.showAtLocation(parent, Gravity.CENTER_HORIZONTAL | Gravity.TOP
                | Gravity.RIGHT, 0, 90);
    }

    // /**
    // * 得到定位信息并上传
    // */
    // @Override
    // public void onLocationChanged(Location location, int locateResult,
    // LocateType locateType) {
    // CheckRimShop.this.geoLat = location.getLatitude();
    // CheckRimShop.this.geoLng = location.getLongitude();
    // uploadLocation(geoLat, geoLng);
    // rsadpter.notifyData(geoLat, geoLng);
    // }

}
