package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.contact.ContactActivity;
import com.gsta.v2.activity.contact.ContactSelectActivity;
import com.gsta.v2.activity.service.SendService;
import com.gsta.v2.db.IContactsDao;
import com.gsta.v2.db.impl.ContactsDaoImpl;
import com.gsta.v2.entity.Contacts;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 发送短信分享
 * 
 * @author wubo
 * @createtime 2012-8-29
 */
public class MsgSendActivity extends BaseActivity implements
		IUICallBackInterface {

	EditText et_user;// 短信接收方
	ImageButton ib_add;// 添加短信接收方
	EditText et_msg;// 发送内容
	TextView tv_uri;// 链接地址
	Button btn_send;// 发送按钮
	Button btn_cancel;// 取消按钮
	List<String> users;// 短信接收方集合
	String phoneNum;// 传入的电话号码
	String msg;// 短信内容
	String add;// 网站地址
	String user;// 文本框填入接收方集合的值
	String type;// // 0邀请注册代理商1推荐商品2邀请注册普通用户

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.msgsend);
		//getAgentApplication();
		initView();
		initData();
		initListener();

	}

	private void initView() {
		// TODO Auto-generated method stub
		et_user = (EditText) findViewById(R.id.et_user);
		ib_add = (ImageButton) findViewById(R.id.ib_add);
		et_msg = (EditText) findViewById(R.id.et_msg);
		tv_uri = (TextView) findViewById(R.id.tv_uri);
		btn_send = (Button) findViewById(R.id.btn_send);
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
	}

	private void initData() {
		// TODO Auto-generated method stub
		phoneNum = getIntent().getStringExtra(FinalUtil.SENDMMS_NUM);
		msg = getIntent().getStringExtra(FinalUtil.SENDMMS_MSG);
		add = getIntent().getStringExtra(FinalUtil.SENDMMS_ADD);
		type = getIntent().getStringExtra(FinalUtil.SENDMMS_TYPE);
		users = new ArrayList<String>();
		if (phoneNum != null && !phoneNum.equals("")) {
			users.add(phoneNum);
			et_user.setText(phoneNum);
		}
		et_msg.setText(msg);
		tv_uri.setText(add);
	}

	private void initListener() {
		// TODO Auto-generated method stub
		ib_add.setOnClickListener(listener);
		btn_send.setOnClickListener(listener);
		btn_cancel.setOnClickListener(listener);
	}

	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == ib_add) {
				IContactsDao contactDao = new ContactsDaoImpl(
						MsgSendActivity.this);
				List<Contacts> selectList = contactDao
						.getAllContacts(application.getUid());
				if (selectList == null || selectList.size() == 0) {
					showToast("通讯录里面还没人哟~");
				} else {
					user = et_user.getText().toString();
					if (user.length() > 0) {
						users = new ArrayList<String>(Arrays.asList(user
								.split(",")));
					}
					Intent intent = new Intent();
					intent.setClass(MsgSendActivity.this,
							ContactSelectActivity.class);
					intent.putExtra(ContactActivity.CONTACT_OPTYPE, 1);
					MsgSendActivity.this.startActivityForResult(intent, 0);
				}
			} else if (v == btn_send) {
				user = et_user.getText().toString();
				if (user.length() > 0) {
					if (type.equals("0") || type.equals("2")) {
						createInvite(user);
					}
					users = new ArrayList<String>(
							Arrays.asList(user.split(",")));
					if (users.size() < 20) {
						Intent sendService = new Intent(MsgSendActivity.this,
								SendService.class);
						sendService.putExtra(FinalUtil.SENDSERVICE_FLAG,
								FinalUtil.sendMsg);
						sendService.putStringArrayListExtra(FinalUtil.SMSUER,
								(ArrayList<String>) users);
						sendService.putExtra(FinalUtil.SMSCONTENT, et_msg
								.getText().toString()
								+ "  "
								+ tv_uri.getText().toString());
						startService(sendService);
					} else {
						Toast.makeText(MsgSendActivity.this, "联系人已达到20上限!",
								Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(MsgSendActivity.this, "请输入收件人电话号码!",
							Toast.LENGTH_SHORT).show();
				}

			} else if (v == btn_cancel) {
				finish();
			}
		}
	};

	public void createInvite(String account) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("inviteUid", application.getUid()));
		paras.add(new Parameter("account", account));
		if (type.equals("0")) {
			paras.add(new Parameter("inviteType", "1"));
		} else {
			paras.add(new Parameter("inviteType", "0"));
		}
		serverMana.supportRequest(AgentConfig.Invite(), paras, false,
				"邀请中,请稍等 ...", 999);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		// TODO Auto-generated method stub
		if (requestCode == 0 && resultCode == RESULT_OK) {
			@SuppressWarnings("unchecked")
            List<Contacts> contacts = (List<Contacts>) intent
					.getSerializableExtra(Contacts.LOCALCONTACTS_KEY);
			if (contacts != null && contacts.size() > 0) {
				for (int i = 0; i < users.size(); i++) {
					for (int j = contacts.size() - 1; j >= 0; j--) {
						if (users.get(i).equals(contacts.get(j).getPhoneNum())) {
							contacts.remove(j);
						}
					}
				}
				for (int i = 0; i < contacts.size(); i++) {
					if (users.size() < 20) {
						users.add(contacts.get(i).getPhoneNum());
						user += ("," + contacts.get(i).getPhoneNum());
					} else {
						Toast.makeText(MsgSendActivity.this, "联系人已达到20上限!",
								Toast.LENGTH_SHORT).show();
						break;
					}
				}
				if (user.indexOf(",") == 0) {
					user = user.substring(1);
				}
			}
			et_user.setText(user);
		}
		super.onActivityResult(requestCode, resultCode, intent);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		switch (caseKey) {
		case 999:
			BaseResult userResult = JSONUtil.fromJson(
					supportResponse.toString(), BaseResult.class);
			if (userResult != null) {
				if (userResult.getErrorCode() == BaseResult.SUCCESS) {
					showToast("邀请成功!");
				} else if (userResult.getErrorCode() == BaseResult.FAIL) {
					showToast(userResult.errorMessage);
				} else {
					showToast(R.string.to_server_fail);
					return;
				}
			} else {
				showToast(R.string.to_server_fail);
				return;
			}
			break;
		}
	}
}
