package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.util.IUICallBackInterface;

/**
 * 服务咨询
 */
public class MyWaiterActivity extends TopBackActivity implements
        IUICallBackInterface {

    private ViewFlipper flipper;
    private TextView tv_mytitle;

    // head
    private HorizontalScrollView hs1;
    private RadioGroup rg1;
    private RadioButton r1;// 会话消息
    private RadioButton r2;
    private RadioButton r3;
    private RadioButton r4;

    // 一 会话
    private LinearLayout ll_vf_msg;

    // 二常见问题
    public ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();

    // 三 店铺查询
    private TextView shop_query_1, shop_query_2, shop_query_4;
    // private LinearLayout ly_query_shop;

    // 四 热门 此功能已经做好 在原工程的 HotappsActivity 类中
    @SuppressWarnings("unused")
    private LinearLayout ll_hotsalos_app;

    private boolean[] needLoad = { false, true, true, true, true };// 下标0 不使用

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mywaiter);
        initView();
        initData();
        initListener();

    }

    private void initView() {
        hs1 = (HorizontalScrollView) findViewById(R.id.tab_view);
        rg1 = (RadioGroup) findViewById(R.id.rg1);
        r1 = (RadioButton) findViewById(R.id.r1);
        r2 = (RadioButton) findViewById(R.id.r2);
        r3 = (RadioButton) findViewById(R.id.r3);
        r4 = (RadioButton) findViewById(R.id.r4);
        flipper = (ViewFlipper) findViewById(R.id.myViewFlipper1);

        // 一
        ll_vf_msg = (LinearLayout) findViewById(R.id.ll_vf_msg);
        //
        shop_query_1 = (TextView) findViewById(R.id.shop_query_1);
        shop_query_2 = (TextView) findViewById(R.id.shop_query_2);
        shop_query_4 = (TextView) findViewById(R.id.shop_query_4);
        // ly_query_shop = (LinearLayout) findViewById(R.id.ly_query_shop);

        ll_hotsalos_app = (LinearLayout) findViewById(R.id.ll_hotsalos_app);

        tv_mytitle = (TextView) findViewById(R.id.mytitle_textView);
        tv_mytitle.setText("服务咨询");
    }

    private void initData() {
        toMsgList();
        init();
    }

    private void initListener() {
        rg1.check(r1.getId());
        rg1.setOnCheckedChangeListener(checkedChangeListener);

        shop_query_1.setOnClickListener(shop_query_tv_click);
        shop_query_2.setOnClickListener(shop_query_tv_click);
        shop_query_4.setOnClickListener(shop_query_tv_click);
    }

    // 店铺查询
    OnClickListener shop_query_tv_click = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.shop_query_1:// 查找店铺
                Intent intent1 = new Intent(MyWaiterActivity.this,
                        SearchShopActivity.class);
                startActivity(intent1);
                break;
            case R.id.shop_query_2:// 查看周边店铺
                Intent intent2 = new Intent(MyWaiterActivity.this,
                        CheckRimShop.class);
                startActivity(intent2);
                break;
            case R.id.shop_query_4: // 周TOP交易店铺
                Intent intent3 = new Intent(MyWaiterActivity.this,
                        QueryTopNShopActivity.class);
                startActivity(intent3);
                break;
            }
        }
    };

    private final String MSGLISTACTIVITY_TAG = "MsgListActivity";
    // 上面菜单按钮组
    OnCheckedChangeListener checkedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == r1.getId()) { // 会话消息
                if (flipper.getDisplayedChild() != 0) {
                    hs1.scrollTo(0, 0);
                    switchLayoutStateTo(flipper, 0);
                    if (needLoad[1]) {
                        needLoad[1] = false;
                        toMsgList();
                    }
                }
                return;
            }
            if (checkedId == r2.getId()) { // 常见问题
                if (flipper.getDisplayedChild() != 1) {
                    hs1.scrollTo(0, 0);
                    switchLayoutStateTo(flipper, 1);
                }
                return;
            }
            if (checkedId == r3.getId()) { // 店铺查询
                if (flipper.getDisplayedChild() != 2) {
                    hs1.scrollTo(350, 0);
                    switchLayoutStateTo(flipper, 2);
                }
                return;
            }
            if (checkedId == r4.getId()) { // 会话消息
                if (flipper.getDisplayedChild() != 3) {
                    hs1.scrollTo(400, 0);
                    switchLayoutStateTo(flipper, 3);
                }
                return;
            }
        }
    };

    /**
     * 中转到聊天界面
     * 
     * @author caosq 2013-4-23 下午4:58:49
     */
    private void toMsgList() {
        Intent intent = new Intent();
        intent.setClass(MyWaiterActivity.this,
                MsgListActivity.class);
        intent.putExtra(IFNEED_TITLE, false);
        startActivity(MSGLISTACTIVITY_TAG, intent, ll_vf_msg);
    }

    /**
     * 获取我的客服
     * 
     * @author wubo
     * @createtime 2012-9-5
     */
    // public void getWaiter() {
    // ServerSupportManager serverMana = new ServerSupportManager(this, this);
    // List<Parameter> paras = new ArrayList<Parameter>();
    // serverMana.supportRequest(AgentConfig.GetTelServiceList(), paras,
    // false, "加载中,请稍等 ...", 999);
    // }

    // public void setValue(int index) {
    // contactUId = String.valueOf(waiters.get(index).getUid());
    // contactPhone = waiters.get(index).getOnlineNo();
    // contactName = waiters.get(index).getServiceNo();
    // }

    // 常见问题
    private void init() {
        // 绑定Layout里面的ListView
        ListView list = (ListView) findViewById(R.id.listViewQuestions);
        // 生成动态数组，加入数据

        for (int i = 2; i <= 26; i++) {
            /*
             * if(i==0){ HashMap<String, Object> map = new HashMap<String, Object>(); map.put("ItemImage", R.drawable.right);//图像资源的ID
             * map.put("ItemTitle", this.getString(R.string.Q0)); map.put("ItemId", ""+i); listItem.add(map); }else if(i==1){ HashMap<String, Object>
             * map = new HashMap<String, Object>(); map.put("ItemImage", R.drawable.right);//图像资源的ID map.put("ItemTitle",
             * this.getString(R.string.Q1)); map.put("ItemId", ""+i); listItem.add(map); } else
             */if (i == 2) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q2));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 3) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q3));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 4) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q4));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 5) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q5));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 6) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q6));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 7) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q7));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 8) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q8));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 9) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q9));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 10) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q10));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 11) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q11));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 12) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q12));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 13) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q13));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 14) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q14));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 15) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q15));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 16) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q16));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 17) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q17));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 18) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q18));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 19) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q19));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 20) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q20));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 21) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q21));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 22) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q22));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 23) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q23));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 24) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q24));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 25) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q25));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else if (i == 26) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("ItemImage", R.drawable.right);// 图像资源的ID
                map.put("ItemTitle", this.getString(R.string.Q26));
                map.put("ItemId", "" + i);
                listItem.add(map);
            } else {

            }
        }

        // 生成适配器的Item和动态数组对应的元素
        SimpleAdapter listItemAdapter = new SimpleAdapter(this, listItem,// 数据源
                R.layout.question_item,// ListItem的XML实现
                // 动态数组与ImageItem对应的子项
                new String[] { "ItemImage", "ItemTitle" },
                // ImageItem的XML文件里面的一个ImageView,两个TextView ID
                new int[] { R.id.imageViewQuestion, R.id.textViewQustTitle });

        // 添加并且显示
        list.setAdapter(listItemAdapter);

        // 添加点击
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3) {
                String QuestionID = listItem.get(arg2).get("ItemId").toString();
                Intent intent = new Intent(MyWaiterActivity.this,
                        AnswerActivity.class);
                intent.putExtra("QID", QuestionID);
                startActivity(intent);
            }
        });
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }
    }
}
