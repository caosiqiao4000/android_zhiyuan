package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.contact.ContactActivity;
import com.gsta.v2.activity.contact.ContactSelectActivity;
import com.gsta.v2.activity.service.SendService;
import com.gsta.v2.db.IContactsDao;
import com.gsta.v2.db.impl.ContactsDaoImpl;
import com.gsta.v2.entity.Contacts;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.ShortUrlResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 短信营销
 * 
 * @author wubo
 * @createtime 2012-8-29
 */
public class MsgSellActivity extends BaseActivity implements
        IUICallBackInterface {

    EditText et_user;// 短信接收方
    ImageButton ib_add;// 添加短信接收方
    EditText et_msg;// 发送内容
    TextView tv_uri;// 链接地址
    Button btn_send;// 发送按钮
    Button btn_cancel;// 取消按钮
    List<String> users;// 短信接收方集合
    String user;// 文本框填入接收方集合的值
    GoodsInfo goodsInfo;// 商品信息
    String uri;// 链接地址

    private TextView tv_title;
    private Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.msgsell);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText("短信营销");
        et_user = (EditText) findViewById(R.id.et_user);
        ib_add = (ImageButton) findViewById(R.id.ib_add);
        et_msg = (EditText) findViewById(R.id.et_msg);
        tv_uri = (TextView) findViewById(R.id.tv_uri);
        btn_send = (Button) findViewById(R.id.btn_send);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);
    }

    private void initData() {
        // 商品信息
        goodsInfo = (GoodsInfo) getIntent().getSerializableExtra(
                FinalUtil.GOODSINFO);
        users = new ArrayList<String>();
        // 内容输入
        et_msg.setText("“我是" + application.getUserName()
                + "，我在东方数码城开店啦，亲们有买手机、宽带的来找我吧，给你们最优价”。");
        // 获取商品短地址
        getShortUri();
    }

    private void initListener() {
        btn_back.setOnClickListener(listener);
        ib_add.setOnClickListener(listener);
        btn_send.setOnClickListener(listener);
        btn_cancel.setOnClickListener(listener);
    }

    OnClickListener listener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (v == ib_add) {// 添加联系人
                // 联系人数据管理器
                IContactsDao contactDao = new ContactsDaoImpl(
                        MsgSellActivity.this);
                // 得到所有联系人
                List<Contacts> selectList = contactDao
                        .getAllContacts(application.getUid());
                if (selectList == null || selectList.size() == 0) {
                    showToast("通讯录里面还没人哟~");
                } else {
                    user = et_user.getText().toString();
                    if (user.length() > 0) {
                        users = new ArrayList<String>(Arrays.asList(user
                                .split(",")));
                    }
                    // 调用联系人选择器,选择联系人
                    Intent intent = new Intent();
                    intent.setClass(MsgSellActivity.this,
                            ContactSelectActivity.class);
                    intent.putExtra(ContactActivity.CONTACT_OPTYPE, 1);
                    MsgSellActivity.this.startActivityForResult(intent, 0);
                }
            } else if (v == btn_send) {// 发送短信
                if (uri.equals("")) {// uri为空无意义则不需要发送
                    showToast("发送失败!");
                    MsgSellActivity.this.finish();
                } else {
                    user = et_user.getText().toString();
                    if (user.length() > 0) {
                        users = new ArrayList<String>(Arrays.asList(user
                                .split(",")));
                        if (users.size() < 20) {
                            // 最多每次发送二十人,通过service发送
                            Intent sendService = new Intent(
                                    MsgSellActivity.this, SendService.class);
                            sendService.putExtra(FinalUtil.SENDSERVICE_FLAG,
                                    FinalUtil.sendMsg);
                            sendService
                                    .putStringArrayListExtra(FinalUtil.SMSUER,
                                            (ArrayList<String>) users);
                            sendService.putExtra(FinalUtil.SMSCONTENT, et_msg
                                    .getText().toString()
                                    + "  " + tv_uri.getText().toString());
                            startService(sendService);
                        } else {
                            Toast.makeText(MsgSellActivity.this, "联系人已达到20上限!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(MsgSellActivity.this, "请输入收件人电话号码!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (v == btn_cancel || v == btn_back) {// 取消发送关闭页面
                finish();
            }
        }
    };

    /**
     * 获取商品短地址
     * 
     * @author wubo
     * @time 2013-3-20
     * 
     */
    public void getShortUri() {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));
        paras.add(new Parameter("agentUid", application.getUid()));
        paras.add(new Parameter("speciesId", goodsInfo.getSpeciesId()));
        paras.add(new Parameter("prodNum", goodsInfo.getSku()));
        paras.add(new Parameter("unitId", goodsInfo.getSpecificationId()));
        serverMana.supportRequest(AgentConfig.ShortUrlGenerator(), paras, true,
                "查询中,请稍等 ...", 999);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
            Intent intent) {
        // TODO Auto-generated method stub
        if (requestCode == 0 && resultCode == RESULT_OK) {
            // 联系人选择器返回选中的联系人
            List<Contacts> contacts = (List<Contacts>) intent
                    .getSerializableExtra(Contacts.LOCALCONTACTS_KEY);
            if (contacts != null && contacts.size() > 0) {
                // 根据选中的联系人,修改短信接收人信息
                for (int i = 0; i < users.size(); i++) {
                    for (int j = contacts.size() - 1; j >= 0; j--) {
                        if (users.get(i).equals(contacts.get(j).getPhoneNum())) {
                            contacts.remove(j);
                        }
                    }
                }
                for (int i = 0; i < contacts.size(); i++) {
                    if (users.size() < 20) {
                        users.add(contacts.get(i).getPhoneNum());
                        user += ("," + contacts.get(i).getPhoneNum());
                    } else {
                        Toast.makeText(MsgSellActivity.this, "联系人已达到20上限!",
                                Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                if (user.indexOf(",") == 0) {
                    user = user.substring(1);
                }
            }
            // 设置短信接收方
            et_user.setText(user);
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse))
            return;

        switch (caseKey) {
        case 999:
            ShortUrlResult userResult = JSONUtil.fromJson(supportResponse
                    .toString(), ShortUrlResult.class);
            if (userResult != null) {
                if (userResult.getErrorCode() == BaseResult.SUCCESS) {
                    uri = userResult.getShortUrl();
                    tv_uri.setText("地址:" + uri);
                } else if (userResult.getErrorCode() == BaseResult.FAIL) {
                    showToast(userResult.errorMessage);
                } else {
                    showToast(R.string.to_server_fail);
                    return;
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }
}
