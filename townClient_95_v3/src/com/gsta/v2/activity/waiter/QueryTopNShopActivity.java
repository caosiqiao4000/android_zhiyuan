package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.SearchShopAdapter;
import com.gsta.v2.activity.location.LocateManager;
import com.gsta.v2.activity.location.LocateManager.LocateListener;
import com.gsta.v2.activity.location.LocateManager.LocateType;
import com.gsta.v2.activity.location.Location;
import com.gsta.v2.activity.myshop.ShopHomePageActivity;
import com.gsta.v2.entity.ShopInfo;
import com.gsta.v2.response.ShopManagerResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 
 * @author longxianwen
 * @createTime Apr 18, 2013 10:42:02 AM
 * @version: 1.0
 * @desc: 周TOP交易店铺查询
 */
public class QueryTopNShopActivity extends BaseActivity implements
        IUICallBackInterface {

    private String TAG = Util.getClassName();
    private Context context = QueryTopNShopActivity.this;
    private Button mytitile_btn_left;
    private TextView mytitle_textView;

    private View loadmoreView;
    private ProgressBar loadmroe_pb;

    private int pageCount = 0; // 分页
    private PullToRefreshListView pl_topshop;
    private ListView shopListView;
    private SearchShopAdapter adapter; // 数据
    private List<ShopInfo> shopInfos = new ArrayList<ShopInfo>();// 周TOP交易店铺信息
    final int load_querycode = 11; // 周边数据查询标识

    private double geoLat = 0; // 纬度
    private double geoLng = 0; // 经度
    private LocateManager loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.querytopshop);

        init();
        initView();
        initData();
        initListener();

    }

    private void init() {
        load();

        loc = LocateManager.getInstance(context);
        loc.locate(new LocateListener() {

            @Override
            public void onLocationChanged(Location location, int locateResult,
                    LocateType locateType) {
                showToast("定位成功: " + location.getLatitude() + ","
                        + location.getLongitude());
                Log.d(TAG,
                        "定位成功: " + location.getLatitude() + ","
                                + location.getLongitude());
                // 得到定位信息
                if (location != null) {
                    // loc.enableMyLocation();
                    // //?java.util.ConcurrentModificationException
                    QueryTopNShopActivity.this.geoLat = location.getLatitude();
                    QueryTopNShopActivity.this.geoLng = location.getLongitude();
                    adapter.notifyData(geoLat, geoLng);
                }
            }
        });
    }

    private void initView() {

        pl_topshop = (PullToRefreshListView) findViewById(R.id.pl_topshop);
        shopListView = pl_topshop.getRefreshableView();

        mytitile_btn_left = (Button) findViewById(R.id.mytitile_btn_left);
        mytitile_btn_left.setVisibility(View.VISIBLE);
        mytitile_btn_left.setOnClickListener(myClickListener);

        mytitle_textView = (TextView) findViewById(R.id.mytitle_textView);
        mytitle_textView.setText(R.string.msg_topshop);

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        loadmoreView = inflater.inflate(R.layout.loadmore, null);
        Button btn_loadmore = (Button) loadmoreView.findViewById(R.id.loadMoreButton);
        btn_loadmore.setOnClickListener(myClickListener);

        loadmroe_pb = (ProgressBar) loadmoreView.findViewById(R.id.loadmroe_pb);
    }

    private void initData() {
        adapter = new SearchShopAdapter(context, shopInfos, 0, 0);
        shopListView.setAdapter(adapter);
    }

    private void initListener() {
        pl_topshop.setRefreshing(true);// 设置页头可见
        shopListView.setOnItemClickListener(itemClickListener1);
        pl_topshop.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageCount = 0;
                if (!shopInfos.isEmpty()) {
                    shopInfos.clear();
                }
                adapter.notifyDataSetInvalidated();
                load();
            }
        });
    }

    /**
     * 列表点击事件
     */
    OnItemClickListener itemClickListener1 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                long id) {
            ShopInfo shopInfo = (ShopInfo) parent.getAdapter()
                    .getItem(position);

            Intent intent = new Intent();
            intent.setClass(context, ShopHomePageActivity.class);
            intent.putExtra(ShopHomePageActivity.IFMYSELF_SHOP_FLAG,
                    shopInfo.getUserKo());
            intent.putExtra(ShopHomePageActivity.MYSELF_SHOPNAME_FLAG,
                    shopInfo.getName());
            startActivity(intent);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        loc.disableMyLocation();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (loc != null) {
            loc.disableMyLocation();
        }
        loc = null;
        super.onDestroy();
    }

    /**
     * 按钮单击事件
     */
    OnClickListener myClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.mytitile_btn_left:
                QueryTopNShopActivity.this.finish();
                break;
            case R.id.loadMoreButton:
                loadmroe_pb.setVisibility(View.VISIBLE);
                load();
                break;
            }
        }
    };

    protected void load() {

        ServerSupportManager serverMana = new ServerSupportManager(this, this);

        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("limit", pageCount + ",20"));

        serverMana.supportRequest(AgentConfig.searchTopShop(), paras, false,
                "提交中,请稍等 ....", load_querycode);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_topshop.isRefreshing()) {
            pl_topshop.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }

        switch (caseKey) {
        case load_querycode:
            ShopManagerResult rmr = JSONUtil.fromJson(
                    supportResponse.toString(), ShopManagerResult.class);
            if (rmr != null) {
                if (rmr.getList() != null && rmr.getList().size() > 0) {
                    shopInfos.addAll(rmr.getList());
                    pageCount += rmr.getList().size();
                    if (shopInfos.size() <= rmr.gettCount()) {
                        loadmroe_pb.setVisibility(View.GONE);
                        if (shopListView.getFooterViewsCount() == 0) {
                            shopListView.addFooterView(loadmoreView);
                        }
                    } else {
                        if (shopListView.getFooterViewsCount() > 0) {
                            shopListView.removeFooterView(loadmoreView);
                        }
                    }
                    adapter.setRsInfos(shopInfos);
                } else {
                    if (shopListView.getFooterViewsCount() > 0) {
                        shopListView.removeFooterView(loadmoreView);
                    }
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }

}
