package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;

/**
 * 常见问题   问答列表
 * 
 * @author boge
 * @time 2013-3-27
 * 
 */
public class QuestionActivity extends BaseActivity {
	public ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.question);
		//getAgentApplication();
		init();

	}

	private void init() {
		// 绑定Layout里面的ListView
		ListView list = (ListView) findViewById(R.id.listViewQuestions);
		// 生成动态数组，加入数据

		for (int i = 2; i <= 26; i++) {
			/*
			 * if(i==0){ HashMap<String, Object> map = new HashMap<String,
			 * Object>(); map.put("ItemImage", R.drawable.right);//图像资源的ID
			 * map.put("ItemTitle", this.getString(R.string.Q0));
			 * map.put("ItemId", ""+i); listItem.add(map); }else if(i==1){
			 * HashMap<String, Object> map = new HashMap<String, Object>();
			 * map.put("ItemImage", R.drawable.right);//图像资源的ID
			 * map.put("ItemTitle", this.getString(R.string.Q1));
			 * map.put("ItemId", ""+i); listItem.add(map); } else
			 */if (i == 2) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q2));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 3) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q3));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 4) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q4));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 5) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q5));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 6) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q6));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 7) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q7));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 8) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q8));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 9) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q9));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 10) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q10));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 11) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q11));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 12) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q12));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 13) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q13));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 14) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q14));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 15) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q15));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 16) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q16));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 17) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q17));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 18) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q18));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 19) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q19));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 20) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q20));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 21) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q21));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 22) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q22));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 23) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q23));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 24) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q24));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 25) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q25));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else if (i == 26) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ItemImage", R.drawable.right);// 图像资源的ID
				map.put("ItemTitle", this.getString(R.string.Q26));
				map.put("ItemId", "" + i);
				listItem.add(map);
			} else {

			}

		}

		// 生成适配器的Item和动态数组对应的元素
		SimpleAdapter listItemAdapter = new SimpleAdapter(this, listItem,// 数据源
				R.layout.question_item,// ListItem的XML实现
				// 动态数组与ImageItem对应的子项
				new String[] { "ItemImage", "ItemTitle" },
				// ImageItem的XML文件里面的一个ImageView,两个TextView ID
				new int[] { R.id.imageViewQuestion, R.id.textViewQustTitle });

		// 添加并且显示
		list.setAdapter(listItemAdapter);

		// 添加点击
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String QuestionID = listItem.get(arg2).get("ItemId").toString();

				Intent intent = new Intent(QuestionActivity.this,
						AnswerActivity.class);
				intent.putExtra("QID", QuestionID);

				startActivity(intent);

			}

		});
	}
}
