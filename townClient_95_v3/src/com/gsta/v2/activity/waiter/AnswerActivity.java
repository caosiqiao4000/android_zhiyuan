package com.gsta.v2.activity.waiter;

import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;

/**
 * 问答
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
public class AnswerActivity extends BaseActivity {
	private TextView qtextview, atextview;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.answer);
		//getAgentApplication();
		int QID = Integer.parseInt(getIntent().getStringExtra("QID"));

		qtextview = (TextView) this.findViewById(R.id.textViewQ);
		atextview = (TextView) this.findViewById(R.id.textViewA);
		showanswer(QID);
		// qtextview.setText("问：问题1");
		// atextview.setText("答：回答1回答1回答1回答1回答1回答1回答1回答1回答1回答1回答1回答1回答1回答1回答1回答1回答1回答1");
	}

	/**
	 * 处理问答
	 * 
	 * @author wubo
	 * @time 2013-3-14
	 * @param Qid
	 * 
	 */
	void showanswer(int Qid) {
		switch (Qid) {
		case 2:
			qtextview.setText(this.getString(R.string.Q2));
			atextview.setText(this.getString(R.string.A2));
			break;
		case 3:
			qtextview.setText(this.getString(R.string.Q3));
			atextview.setText(this.getString(R.string.A3));
			break;
		case 4:
			qtextview.setText(this.getString(R.string.Q4));
			atextview.setText(this.getString(R.string.A4));
			break;
		case 5:
			qtextview.setText(this.getString(R.string.Q5));
			atextview.setText(this.getString(R.string.A5));
			break;
		case 6:
			qtextview.setText(this.getString(R.string.Q6));
			atextview.setText(this.getString(R.string.A6));
			break;
		case 7:
			qtextview.setText(this.getString(R.string.Q7));
			atextview.setText(this.getString(R.string.A7));
			break;
		case 8:
			qtextview.setText(this.getString(R.string.Q8));
			atextview.setText(this.getString(R.string.A8));
			break;
		case 9:
			qtextview.setText(this.getString(R.string.Q9));
			atextview.setText(this.getString(R.string.A9));
			break;
		case 10:
			qtextview.setText(this.getString(R.string.Q10));
			atextview.setText(this.getString(R.string.A10));
			break;

		case 11:
			qtextview.setText(this.getString(R.string.Q11));
			atextview.setText(this.getString(R.string.A11));
			break;
		case 12:
			qtextview.setText(this.getString(R.string.Q12));
			atextview.setText(this.getString(R.string.A12));
			break;
		case 13:
			qtextview.setText(this.getString(R.string.Q13));
			atextview.setText(this.getString(R.string.A13));
			break;
		case 14:
			qtextview.setText(this.getString(R.string.Q14));
			atextview.setText(this.getString(R.string.A14));
			break;
		case 15:
			qtextview.setText(this.getString(R.string.Q15));
			atextview.setText(this.getString(R.string.A15));
			break;

		case 16:
			qtextview.setText(this.getString(R.string.Q16));
			atextview.setText(this.getString(R.string.A16));
			break;
		case 17:
			qtextview.setText(this.getString(R.string.Q17));
			atextview.setText(this.getString(R.string.A17));
			break;

		case 18:
			qtextview.setText(this.getString(R.string.Q18));
			atextview.setText(this.getString(R.string.A18));
			break;
		case 19:
			qtextview.setText(this.getString(R.string.Q19));
			atextview.setText(this.getString(R.string.A19));
			break;
		case 20:
			qtextview.setText(this.getString(R.string.Q20));
			atextview.setText(this.getString(R.string.A20));
			break;
		case 21:
			qtextview.setText(this.getString(R.string.Q21));
			atextview.setText(this.getString(R.string.A21));
			break;

		case 22:
			qtextview.setText(this.getString(R.string.Q22));
			atextview.setText(this.getString(R.string.A22));
			break;
		case 23:
			qtextview.setText(this.getString(R.string.Q23));
			atextview.setText(this.getString(R.string.A23));
			break;
		case 24:
			qtextview.setText(this.getString(R.string.Q24));
			atextview.setText(this.getString(R.string.A24));
			break;

		case 25:
			qtextview.setText(this.getString(R.string.Q25));
			atextview.setText(this.getString(R.string.A25));
			break;
		case 26:
			qtextview.setText(this.getString(R.string.Q26));
			atextview.setText(this.getString(R.string.A26));
			break;

		default:
			break;
		}
	}
}
