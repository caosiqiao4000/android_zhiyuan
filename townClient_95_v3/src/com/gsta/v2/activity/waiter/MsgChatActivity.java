package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.MsgTalkAdapter;
import com.gsta.v2.activity.service.SendService;
import com.gsta.v2.db.impl.IMHistoryDaoImpl;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.PopupExpressAdapter;
import com.gsta.v2.util.SmileyParser;
import com.gsta.v2.util.TextCountLimitWatcher;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * IM聊天会话界面
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
public class MsgChatActivity extends BaseActivity {

    private TextView userName;// 昵称
    private ImageView msgico;// 头像
    private EditText sendMsg;// 发送私信内容
    private Button sendMsgButton;// 发送私信
    private IMHistoryDaoImpl IMBeanDao;// 数据管理器

    private MsgTalkAdapter msgTalkAdapter;// 数据适配器
    private IMBean topeer;// 传入的聊天对象的信息
    private List<IMBean> msgs;// 消息集合
    private ListView messLView;// 聊天信息list
    private ImageView history;// 切换到历史记录
    private PopupWindow popupWindow;// 表情选择框
    private ImageButton btnExpression;// 弹出表情框
    private GridView mExpresssion; // 表情显示
    private LinearLayout mExpressPopupShow;

    // 发送信息组装类
    private IMBean sendImBean;

    private Intent sendService;// 发送消息service
    private MsgChatReceiver chatReceiver;// 消息广播
    private SmileyParser parser;// 图片和文字互转工具类

    // 自己的 uid
    private String myUserID;

    private Builder builder;

    // 头像类使用
    private DisplayImageOptions optionsByNoPhoto;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            sendBroadcast(new Intent(FinalUtil.RECEIVER_IM_REFRUSH_ACTION));
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 防止输入法在启动Activity时自动弹出
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // 去除标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.msgtalk);
        // 注册广播
        registerReciver();
        IMBeanDao = new IMHistoryDaoImpl(this);
        // getAgentApplication();
        myUserID = application.getUid();
        Bundle bundle = getIntent().getExtras();
        // 传过来的聊天对象
        topeer = (IMBean) bundle.getSerializable(FinalUtil.IMBEAN);
        sendService = new Intent(MsgChatActivity.this, SendService.class);
        initView();
        initData();
        initListenter();
    }

    /**
     * init view
     * 
     */
    private void initView() {
        history = (ImageView) findViewById(R.id.history);
        sendMsg = (EditText) findViewById(R.id.message);
        msgico = (ImageView) findViewById(R.id.msgico);
        sendMsgButton = (Button) findViewById(R.id.sendmsg);
        userName = (TextView) findViewById(R.id.nickname);
        btnExpression = (ImageButton) findViewById(R.id.last);
        messLView = (ListView) findViewById(R.id.message_single_listview);
        mExpressPopupShow = (LinearLayout) findViewById(R.id.express_popup_parent);
        sendMsg.addTextChangedListener(new TextCountLimitWatcher(100, sendMsg));
        // 初始化表情框
        initPopupWindow();
    }

    public void initData() {

        optionsByNoPhoto = ImageUtil.getDefaultDispalyImageOptions(R.drawable.nav_head, Bitmap.Config.ARGB_8888);

        parser = SmileyParser.getInstance(this);// 表情转化辅助工具
        userName.setText(topeer.getHisName());
        msgs = IMBeanDao.getUserMsgs(myUserID, topeer.getHisUserId());
        if (null == msgs) {
            msgs = new ArrayList<IMBean>();
        }
        msgTalkAdapter = new MsgTalkAdapter(MsgChatActivity.this, msgs);
        messLView.setAdapter(msgTalkAdapter);
        // 更新聊天记录为已读
        updateReadStatus();
        // 获取聊天对象头像
        String photouri = application.getPhotoPathByUid(topeer.getHisUserId());
        if (null == photouri || TextUtils.isEmpty(photouri)) {
            if (topeer.getHisPhotoPath().trim().length() == 0) {
                msgico.setImageResource(R.drawable.nav_head);
            } else {
                ImageUtil.getNetPicByUniversalImageLoad(null, msgico, topeer.getHisPhotoPath(), optionsByNoPhoto);
            }
        } else {
            // 从服务器获取
            // ImageUtil.getNetPic(MsgChatActivity.this, msgico, photouri,R.drawable.nav_head);
            ImageUtil.getNetPicByUniversalImageLoad(null, msgico, photouri, optionsByNoPhoto);
        }
    }

    private void initListenter() {
        sendMsgButton.setOnClickListener(listener);
        btnExpression.setOnClickListener(listener);
        history.setOnClickListener(listener);

        messLView
                .setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent,
                            View view, int position, long id) {
                        final int index = position;
                        builder = new AlertDialog.Builder(MsgChatActivity.this);
                        builder.setTitle("选择操作");
                        builder.setItems(
                                new String[] { "复制消息", "删除消息", "清空记录" },
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        // TODO Auto-generated method stub
                                        switch (which) {
                                        // "0复制消息", "1删除消息", "2清空记录"
                                        case 0:
                                            Util.copyText(MsgChatActivity.this,
                                                    msgs.get(index)
                                                            .getContent());
                                            break;
                                        case 1:
                                            IMBeanDao.deleteUserMsgById(msgs
                                                    .get(index).getId());
                                            msgs = IMBeanDao.getUserMsgs(
                                                    myUserID, topeer
                                                            .getHisUserId());
                                            msgTalkAdapter.setMsgs(msgs);
                                            sendBroadcast(new Intent(
                                                    FinalUtil.RECEIVER_IM_REFRUSH_ACTION));
                                            break;
                                        case 2: // 删除消息
                                            IMBeanDao.deleteUserMsgByUid(
                                                    myUserID, topeer
                                                            .getHisUserId());
                                            msgs = IMBeanDao.getUserMsgs(
                                                    myUserID, topeer
                                                            .getHisUserId());
                                            msgTalkAdapter.setMsgs(msgs);
                                            sendBroadcast(new Intent(
                                                    FinalUtil.RECEIVER_IM_REFRUSH_ACTION));
                                            break;
                                        }
                                    }
                                }).create().show();
                        return false;
                    }
                });
        // 重新发送
        messLView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                // TODO Auto-generated method stub
                final IMBean msg = msgs.get(position);
                if (msg.getSended() != null
                        && msg.getSended().equals(FinalUtil.IM_UNSEND)) {
                    builder = new AlertDialog.Builder(MsgChatActivity.this);
                    builder.setTitle("重发");
                    builder.setMessage("是否重新发送?");
                    builder.setPositiveButton("重发",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                        int which) {
                                    // TODO Auto-generated method stub
                                    IMBeanDao.deleteUserMsgById(msg.getId());
                                    msg.setTime(Util.getSysNowTime());
                                    sendService.putExtra(FinalUtil.IMBEAN, msg);
                                    sendService.putExtra(
                                            FinalUtil.SENDSERVICE_FLAG,
                                            FinalUtil.sendIM);
                                    startService(sendService);
                                }
                            });
                    builder.setNegativeButton("取消", null).create().show();
                }
            }
        });
    }

    // 发送文字信息
    private void sendMessage(String sendmessage) {
        if (null == sendImBean) {
            sendImBean = new IMBean();
            // 发送方,自己发送
            sendImBean.setDirection(FinalUtil.IM_FROM_ME);
            // 我的uid
            sendImBean.setMyUserId(myUserID);
            // 是否已经阅读
            sendImBean.setFlag(FinalUtil.IM_READED);
        }
        sendImBean.setContent(sendmessage);
        // 接收方的uid
        sendImBean.setHisUserId(topeer.getHisUserId());
        // 发送消息的时间
        sendImBean.setTime(Util.getSysNowTime());
        // 自己的昵称
        sendImBean.setUserName(null == application.getUserinfo().getNickName() ? application.getUid() : application.getUserinfo().getNickName());
        // // 对方 的昵称
        sendImBean.setHisName(null == topeer.getHisName() ? topeer.getHisUserId() : topeer.getHisName());
        // 自己的头像
        sendImBean.setHisPhotoPath(null == application.getUserinfo().getPhoto() ? "" : application.getUserinfo().getPhoto());
        sendService.putExtra(FinalUtil.IMBEAN, sendImBean);
        sendService.putExtra(FinalUtil.SENDSERVICE_FLAG, FinalUtil.sendIM);
        startService(sendService);
    }

    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == sendMsgButton) {// 发送消息
                String sendmessage = sendMsg.getText().toString();
                if (sendmessage.length() > 0) {
                    sendMessage(sendmessage);
                }
                sendMsg.setText("");
            } else if (v == history) {// 聊天记录
                Intent intent = new Intent(MsgChatActivity.this,
                        MsgHistoryActivity.class);
                intent.putExtra(FinalUtil.SENDIM_HISUID, topeer.getHisUserId());
                intent
                        .putExtra(FinalUtil.SENDIM_USERNAME, topeer
                                .getUserName());
                startActivity(intent);
            } else if (v == btnExpression) {// 弹出表情选择框
                if (!popupWindow.isShowing()) {
                    int y = mExpressPopupShow.getHeight();
                    popupWindow.showAtLocation(btnExpression, Gravity.BOTTOM,
                            0, y);
                } else {
                    popupWindow.dismiss();
                }
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(MsgChatActivity.this
                                .getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    };

    /**
     * 注册广播 私信接受广播 接收到别人的消息时在这里更新
     * 
     * @author wubo
     * @time 2013-3-20
     * 
     */
    public void registerReciver() {
        chatReceiver = new MsgChatReceiver();
        IntentFilter filter = new IntentFilter(FinalUtil.RECEIVER_IM_ACTION);
        registerReceiver(chatReceiver, filter);
    }

    /**
     * 解除广播
     * 
     * @author wubo
     * @time 2013-3-20
     * 
     */
    public void unRegisterReceiver() {
        unregisterReceiver(chatReceiver);
    }

    /**
     * 接收到新消息后 处理消息状态广播
     * 
     * @author wubo
     * @time 2013-3-20
     * 
     */
    public class MsgChatReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateReadStatus();
            msgs = IMBeanDao.getUserMsgs(myUserID, topeer.getHisUserId());
            msgTalkAdapter.setMsgs(msgs);
        }
    }

    /**
     * 更改消息是否已读(状态) 更改消息成已经状态
     * 
     * @author wubo
     * @time 2013-3-20
     * 
     */
    public void updateReadStatus() {
        IMBeanDao.updateReadStatus(myUserID, topeer.getHisUserId());
        handler.sendEmptyMessageDelayed(1, 800);

    }

    /**
     * 私聊表情选择 void
     */
    private void initPopupWindow() {
        View popView = LayoutInflater.from(this).inflate(
                R.layout.grid_express_popup, null);
        popupWindow = new PopupWindow(popView,
                // LinearLayout.LayoutParams.FILL_PARENT, application.getHeightPixels() / 5,
                LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,
                false);
        mExpresssion = (GridView) popView.findViewById(R.id.gv_expression);
        PopupExpressAdapter adapter = new PopupExpressAdapter(this);
        mExpresssion.setAdapter(adapter);
        popupWindow.setBackgroundDrawable(getResources().getDrawable(
                R.drawable.popup_bg_white));
        popupWindow.setAnimationStyle(R.style.popupWindowBottomCenter);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.update();
        mExpresssion.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                    int position, long arg3) {
                String expressStr = getResources().getStringArray(
                        R.array.express_item_texts)[position];
                String oldStr = sendMsg.getText().toString();
                sendMsg.setText(parser.addSmileySpans(oldStr + expressStr));
                Util.setEditCursorToTextEnd(sendMsg);
                popupWindow.dismiss();
            }
        });

    }

    @Override
    protected void onDestroy() {
        unRegisterReceiver();
        super.onDestroy();
    }
}