package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.activity.adapter.IMHistoryAdapter;
import com.gsta.v2.db.IMHistoryDao;
import com.gsta.v2.db.impl.IMHistoryDaoImpl;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.response.GetPhotoPathResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 私信聊天对象列表
 * 
 * @author wubo
 * @createtime 2012-9-24
 */
public class MsgListActivity extends TopBackActivity implements
        IUICallBackInterface {

    private ListView imList;// 私信显示列表
    private IMHistoryAdapter imHistoryAdapter;// 数据源
    private IMHistoryDao historyDao;// 数据库访问
    private List<IMBean> ims = new ArrayList<IMBean>();// 私信
    private ContactReceiver contactReceiver;
    private Builder builder;
    // private View noDataView;
    // 客服电话
    private LinearLayout ly_waiter_phone;

    // 客服助手
    // private RelativeLayout ly_town_assistant;
    // 客服助手 时间和信息
    // private TextView tv_assistant_time, tv_assistant_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.msglist);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        // ly_town_assistant = (RelativeLayout) findViewById(R.id.ly_town_assistant);
        ly_waiter_phone = (LinearLayout) findViewById(R.id.ly_waiter_phone);
        // tv_assistant_message = (TextView) findViewById(R.id.tv_assistant_message);
        // tv_assistant_time = (TextView) findViewById(R.id.tv_assistant_time);

        imList = (ListView) findViewById(R.id.lv_im);
        // noDataView = findViewById(R.id.no_data);
        // ((TextView) noDataView.findViewById(R.id.search)).setText("没找到与别人的聊天记录哦");
        // imList.setEmptyView(noDataView);
    }

    private void initData() {
        // 注册广播
        registerReciver();
        historyDao = new IMHistoryDaoImpl(MsgListActivity.this);
        // 取得聊天记录
        ims = historyDao.getUserMessages(application.getUid());
        imHistoryAdapter = new IMHistoryAdapter(MsgListActivity.this, ims);
        imList.setAdapter(imHistoryAdapter);
        // 头像和姓名 先从数据库表中查询历史记录 如果没有再去后台查询 取得后台用户信息后保存到用户信息表中
        // getIcon();
    }

    private void initListener() {
        ly_waiter_phone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel://" + "4009910001"));
                startActivity(intent);
            }
        });

        imList.setOnItemClickListener(itemClickListener1);
        imList.setOnItemLongClickListener(itemLongClickListener);
    }

    /**
     * 得到头像
     * 
     * @author wubo
     * @time 2013-3-1
     * 
     */
    private void getIcon() {
        if (ims == null || ims.size() == 0) {
            return;
        }
        StringBuffer ruid = new StringBuffer();
        for (int i = 0; i < ims.size(); i++) {
            ruid.append(ims.get(i).getHisUserId() + ",");
        }
        final String ruidP = ruid.toString().substring(0, ruid.length() - 1);
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", String.valueOf(application.getUid())));
        paras.add(new Parameter("ruid", ruidP));
        serverMana.supportRequest(AgentConfig.GetPhotoPath(), paras);
    }

    /**
     * 列表点击事件 点击进入私信聊天界面
     */
    OnItemClickListener itemClickListener1 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                long id) {
            IMBean im = ims.get(position);
            if (im.getHisUserId() == null) {
                showToast("不能与该用户私聊");
            } else {
                Intent intent = new Intent();
                intent.putExtra(FinalUtil.IMBEAN, im);
                intent.setClass(MsgListActivity.this, MsgChatActivity.class);
                startActivity(intent);
            }
        }
    };

    OnItemLongClickListener itemLongClickListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view,
                int position, long id) {
            final int index = position;
            builder = new AlertDialog.Builder(MsgListActivity.this.getParent());
            builder.setTitle(ims.get(index).getUserName());
            builder.setItems(new String[] { "删除记录" },
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            historyDao.deleteUserMsgByUid(application.getUid(),
                                    ims.get(index).getHisUserId());
                            ims = historyDao.getUserMessages(application
                                    .getUid());
                            imHistoryAdapter.setMessages(ims);
                        }

                    }).create().show();
            return false;
        }
    };

    /**
     * 注册广播
     * 
     * @author wubo
     * @time 2013-3-1
     * 
     */
    public void registerReciver() {
        // 监听im信息的广播 私信界面更新广播
        contactReceiver = new ContactReceiver();
        IntentFilter filter = new IntentFilter(
                FinalUtil.RECEIVER_IM_REFRUSH_ACTION);
        registerReceiver(contactReceiver, filter);
    }

    /**
     * 解除注册
     * 
     * @author wubo
     * @time 2013-3-1
     * 
     */
    public void unRegisterReceiver() {
        unregisterReceiver(contactReceiver);
    }

    /**
     * 重新获取私信记录
     * 
     * @author boge
     * @time 2013-3-27
     * 
     */
    public class ContactReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ims = historyDao.getUserMessages(application.getUid());
            imHistoryAdapter.setMessages(ims);
            // if (ims.size() <= 0) {
            //
            // }
            getIcon();
        }
    }

    @Override
    protected void onDestroy() {
        unRegisterReceiver();
        super.onDestroy();
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse))
            return;

        GetPhotoPathResult result = JSONUtil.fromJson(supportResponse
                .toString(), GetPhotoPathResult.class);
        if (result != null) {
            if (null != result.getPhotoPath() && result.getPhotoPath().size() > 0) {
                // application.setPhotoPathMap(result.getPhotoPath());
                application.getPhotoPathMap().putAll(result.getPhotoPath());
                imHistoryAdapter.notifyDataSetChanged();
            }
        }
    }
}
