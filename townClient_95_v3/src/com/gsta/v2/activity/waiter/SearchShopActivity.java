package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.SearchShopAdapter;
import com.gsta.v2.activity.location.LocateManager;
import com.gsta.v2.activity.location.LocateManager.LocateListener;
import com.gsta.v2.activity.location.LocateManager.LocateType;
import com.gsta.v2.activity.location.Location;
import com.gsta.v2.activity.myshop.ShopHomePageActivity;
import com.gsta.v2.entity.ShopInfo;
import com.gsta.v2.response.ShopManagerResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.CustomDialog;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 
 * @author longxianwen
 * @createTime Apr 18, 2013 10:42:40 AM
 * @version: 1.0
 * @desc:店铺搜索
 */
public class SearchShopActivity extends BaseActivity implements
        IUICallBackInterface {

    private String TAG = Util.getClassName();
    private Context context = SearchShopActivity.this;
    private Button mytitile_btn_left, btn_right;
    private EditText domain_name, shop_name; // 店铺域名，店铺名称
    private String tempDomainName = "", tempShopName = "";
    private TextView mytitle_textView;
    @SuppressWarnings("unused")
    private int pageCount = 0; // 分页
    //
    private PullToRefreshListView pl_searchshop;
    private ListView shopListView;
    private SearchShopAdapter adapter; // 数据
    private List<ShopInfo> shopInfos = new ArrayList<ShopInfo>();// 查询店铺信息
    private InputMethodManager imm;

    final int load_querycode = 11; // 周边数据查询标识

    private double geoLat = 0; // 纬度
    private double geoLng = 0; // 经度
    private LocateManager loc;

    private LayoutInflater flater;
    private View loadMoreView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.searchshop);

        init();
        initView();
        initData();
        initListener();
    }

    private void init() {
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        loc = LocateManager.getInstance(context);
    }

    private void initView() {

        pl_searchshop = (PullToRefreshListView) findViewById(R.id.pl_searchshop);
        shopListView = pl_searchshop.getRefreshableView();

        mytitile_btn_left = (Button) findViewById(R.id.mytitile_btn_left);
        mytitile_btn_left.setVisibility(View.VISIBLE);
        btn_right = (Button) findViewById(R.id.mytitile_btn_right);
        btn_right.setVisibility(0);
        btn_right.setText("搜  索");

        mytitle_textView = (TextView) findViewById(R.id.mytitle_textView);
        mytitle_textView.setText(R.string.msg_searchshop);

        flater = LayoutInflater.from(this);

        // 加载更多
        loadMoreView = flater.inflate(R.layout.loadmore, null);
        Button button = (Button) loadMoreView.findViewById(R.id.loadMoreButton);
        button.setOnClickListener(myClickListener);
    }

    private void initData() {
        adapter = new SearchShopAdapter(context, shopInfos, 0, 0);
        shopListView.setAdapter(adapter);
        loc.locate(new LocateListener() {

            @Override
            public void onLocationChanged(Location location, int locateResult,
                    LocateType locateType) {
                showToast("定位成功: " + location.getLatitude() + ","
                        + location.getLongitude());
                Log.d(TAG,
                        "定位成功: " + location.getLatitude() + ","
                                + location.getLongitude());
                // 得到定位信息
                if (location != null) {
                    SearchShopActivity.this.geoLat = location.getLatitude();
                    SearchShopActivity.this.geoLng = location.getLongitude();
                    adapter.notifyData(geoLat, geoLng);
                }
            }
        });
    }

    private void initListener() {

        mytitile_btn_left.setOnClickListener(myClickListener);
        btn_right.setOnClickListener(myClickListener);
        shopListView.setOnItemClickListener(itemClickListener1);
        pl_searchshop.setOnRefreshListener(new OnRefreshListener() { // 刷新
                    @Override
                    public void onRefresh() {
                        // tempDomainName = "", tempShopName = "";
                        if ("".equals(tempDomainName) && "".equals(tempShopName)) {
                            btn_right.performClick();
                            pl_searchshop.onRefreshComplete();
                            return;
                        }
                        pageCount = 0;
                        shopInfos.clear();
                        load(tempShopName, tempDomainName);
                    }
                });
    }

    /**
     * dialog以及确定、取消按钮监听
     */
    private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (dialog != null) {
                // 取消浮层
                dialog.cancel();
            }
            pl_searchshop.setRefreshing(true);
            tempDomainName = domain_name.getText().toString();
            tempShopName = shop_name.getText().toString();
            imm.hideSoftInputFromWindow(SearchShopActivity.this.getCurrentFocus().getWindowToken(), 0); // 关闭软件盘
            pageCount = 0;
            shopInfos.clear();
            load(tempShopName, tempDomainName);
        }
    };

    /**
     * 列表点击事件
     */
    OnItemClickListener itemClickListener1 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                long id) {
            ShopInfo shopInfo = (ShopInfo) parent.getAdapter()
                    .getItem(position);
            Intent intent = new Intent();
            intent.setClass(context, ShopHomePageActivity.class);
            intent.putExtra(ShopHomePageActivity.IFMYSELF_SHOP_FLAG,
                    shopInfo.getUserKo());
            intent.putExtra(ShopHomePageActivity.MYSELF_SHOPNAME_FLAG,
                    shopInfo.getName());
            startActivity(intent);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        loc.disableMyLocation();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (loc != null) {
            loc.disableMyLocation();
        }
        loc = null;
        super.onDestroy();
    }

    /**
     * 按钮单击事件
     */
    OnClickListener myClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.mytitile_btn_left:
                SearchShopActivity.this.finish();
                break;
            case R.id.mytitile_btn_right:
                showDialog(0xaa);
                break;
            case R.id.loadMoreButton:
                load(tempShopName, tempDomainName);
                break;
            }
        }
    };

    @Override
    protected android.app.Dialog onCreateDialog(int id) {
        if (id == 0xaa) { // 查找店铺条件
            CustomDialog.Builder dialog = new CustomDialog.Builder(SearchShopActivity.this);
            View view = flater.inflate(R.layout.search_shop_dialog_layout, null);
            domain_name = (EditText) view.findViewById(R.id.domain_name);
            shop_name = (EditText) view.findViewById(R.id.shop_name);
            if (tempDomainName.trim().length() > 0) {
                domain_name.setText(tempDomainName);
                Util.setEditCursorToTextEnd(domain_name);
            }
            if (tempShopName.trim().length() > 0) {
                shop_name.setText(tempShopName);
                Util.setEditCursorToTextEnd(shop_name);
            }
            dialog.setTitle("查找店铺条件");
            dialog.setContentView(view);
            dialog.setPositiveButton(R.string.submit, mDialogClickListener);
            dialog.setNegativeButton(R.string.cancel, null);
            return dialog.create();
        } else {
            return super.onCreateDialog(id);
        }
    };

    protected void load(String shopName, String shopDomain) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("shopName", shopName));
        paras.add(new Parameter("domain", shopDomain));

        serverMana.supportRequest(AgentConfig.searchShop(), paras, false,
                "提交中,请稍等 ....", load_querycode);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_searchshop.isRefreshing()) {
            pl_searchshop.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }

        switch (caseKey) {
        case load_querycode:
            ShopManagerResult rmr = JSONUtil.fromJson(
                    supportResponse.toString(), ShopManagerResult.class);
            if (rmr != null) {
                if (rmr.getList() != null && rmr.getList().size() > 0) {
                    shopInfos.addAll(rmr.getList());
                    pageCount += 1;
                    adapter.setRsInfos(shopInfos);
                    // if (rmr.gettCount() > shopInfos.size()) {
                    // if (shopListView.getFooterViewsCount() == 0) {
                    // shopListView.addFooterView(loadMoreView);
                    // }
                    // } else {
                    // shopListView.removeFooterView(loadMoreView);
                    // }
                } else {
                    showToast("没有为您搜索到店铺信息!");
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
        }
    }
}
