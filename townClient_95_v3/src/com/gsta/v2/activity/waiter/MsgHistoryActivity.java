package com.gsta.v2.activity.waiter;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.MsgHistoryAdapter;
import com.gsta.v2.db.IMHistoryDao;
import com.gsta.v2.db.impl.IMHistoryDaoImpl;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.util.FinalUtil;

/**
 * 私信聊天历史记录
 * 
 * @author wubo
 * @time 2013-3-18
 * 
 */
public class MsgHistoryActivity extends BaseActivity {

    private ListView history;// 消息list
    private Button last;// 上一页
    private Button next;// 下一页
    private TextView count;// 当前页数和总共页数
    private MsgHistoryAdapter adapter;// 适配器
    private List<IMBean> list;// 所有的消息列
    String uid;// 本人uid
    String username;// 本人名称
    String himuid;// 聊天对象uid
    String himusername;// 聊天对象名称
    private List<IMBean> list_item;
    int start = 0;// 每次加载的第一条数据编号
    int length = 8;// 每次加载的数据条数
    int total = 0;// 一共有多少条数据
    int totalPage = 0;// 数据一共分多少页
    int page = 1;// 当前页面编号
    IMHistoryDao IMBeanDao;// 消息数据管理器

    private TextView tv_title;
    private Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.msghistory);
        // 获取聊天对象uid
        himuid = getIntent().getExtras().getString(FinalUtil.SENDIM_HISUID);
        // 对方名称
        himusername = getIntent().getExtras().getString(
                FinalUtil.SENDIM_USERNAME);
        initView();
        initData();
        initListenner();
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText(getResources().getString(R.string.history_show));
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MsgHistoryActivity.this.finish();
            }
        });

        history = (ListView) findViewById(R.id.history_list);
        last = (Button) findViewById(R.id.last);
        next = (Button) findViewById(R.id.next);
        count = (TextView) findViewById(R.id.count);
    }

    private void initData() {
        uid = application.getUid();
        username = application.getUserName();
        // 私信类
        IMBeanDao = new IMHistoryDaoImpl(MsgHistoryActivity.this);
        // 获取历史聊天记录
        list = IMBeanDao.getUserMsgsHistory(uid, himuid);
        total = list.size();
        if (total % length == 0) {
            totalPage = total / length;
        } else {
            totalPage = (total / length) + 1;
        }
        if (totalPage == 0) {
            totalPage = 1;
        }
        count.setText(page + "/" + totalPage);
        list_item = new ArrayList<IMBean>();
        // 分页查询历史记录
        msgLimit();
        adapter = new MsgHistoryAdapter(MsgHistoryActivity.this, list_item,
                username, himusername);
        history.setAdapter(adapter);
    }

    private void initListenner() {
        last.setOnClickListener(listener);
        next.setOnClickListener(listener);
    }

    OnClickListener listener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (v == next) {// 下一页
                if ((page + 1) <= totalPage) {
                    start += length;
                    page += 1;
                    msgLimit();
                    adapter.notifyDataSetChanged();
                    count.setText(page + "/" + totalPage);
                } else {
                    Toast.makeText(MsgHistoryActivity.this, R.string.toonext,
                            Toast.LENGTH_SHORT).show();
                }
            } else if (v == last) {
                if (page - 1 > 0) {// 上一页
                    start -= length;
                    page -= 1;
                    msgLimit();
                    adapter.notifyDataSetChanged();
                    count.setText(page + "/" + totalPage);
                } else {
                    Toast.makeText(MsgHistoryActivity.this, R.string.toolast,
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    /**
     * 分页
     * 
     * @author wubo
     * @createtime Apr 13, 2012
     */
    private void msgLimit() {
        list_item.clear();
        if ((start + length) >= total) {
            for (int i = start; i < total; i++) {
                list_item.add(list.get(i));
            }
        } else {
            for (int i = start; i < start + length; i++) {
                list_item.add(list.get(i));
            }
        }
    }
}
