package com.gsta.v2.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.waiter.MsgSendActivity;
import com.gsta.v2.db.impl.SharedPreferencesUtil;
import com.gsta.v2.entity.TxUserInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.ExtensionMsgResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.WeiboUtil;
import com.tencent.weibo.api.UserAPI;
import com.tencent.weibo.constants.OAuthConstants;
import com.tencent.weibo.oauthv2.OAuthV2;
import com.tencent.weibo.webview.OAuthV2AuthorizeWebView;
import com.weibo.net.AccessToken;
import com.weibo.net.DialogError;
import com.weibo.net.Oauth2AccessTokenHeader;
import com.weibo.net.Utility;
import com.weibo.net.Weibo;
import com.weibo.net.WeiboDialogListener;
import com.weibo.net.WeiboException;
import com.weibo.net.WeiboParameters;

/**
 * 微博管理
 * 
 * @author wubo
 * @createtime 2012-9-10
 */
public class WeiboManagerActivity extends BaseActivity implements
        IUICallBackInterface {
    private static final Logger logger = LoggerFactory.getLogger();

    LinearLayout ly_sina;
    LinearLayout ly_tx;
    TextView tv_sina;
    TextView tv_tx;

    ImageButton btn_msg;
    ImageButton btn_weibo;
    ImageButton btn_txweibo;

    Builder builder;
    final int unsinaGrant = 0; // 没绑定
    final int sinaGranted = 1;
    boolean isBindSina = false;
    final int untxGrant = 2;
    final int txGranted = 3;
    boolean isBindTx = false;

    int currentId = -1;
    SharedPreferencesUtil preferencesUtil;

    String sina_token = "";
    String sina_expires_in = "";
    String sina_uid = "";
    String sina_nick = "";

    OAuthV2 oAuth;
    String tx_token = "";
    String tx_expires_in = "";
    String tx_openid = "";
    String tx_openkey = "";
    String tx_nick = "";

    private Dialog popupDialog;

    private final int msg_code = 10;
    private final int sina_code = 11;
    private final int tx_code = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.weibomanager);
        // getAgentApplication();
        preferencesUtil = new SharedPreferencesUtil(WeiboManagerActivity.this);
        initView();
        initData();
        initListener();

        TextView mytitle_textView;
        mytitle_textView = (TextView) findViewById(R.id.mytitle_textView);
        mytitle_textView.setText("营销推广");

        Button ib_myshop;
        ib_myshop = (Button) findViewById(R.id.mytitile_btn_left);
        ib_myshop.setVisibility(View.VISIBLE);
        ib_myshop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                WeiboManagerActivity.this.finish();
            }
        });
    }

    private void initView() {
        // TODO Auto-generated method stub
        ly_sina = (LinearLayout) findViewById(R.id.ly_sina);
        ly_tx = (LinearLayout) findViewById(R.id.ly_tx);
        tv_sina = (TextView) findViewById(R.id.tv_sina);
        tv_tx = (TextView) findViewById(R.id.tv_tx);

        btn_msg = (ImageButton) findViewById(R.id.btn_msg);
        btn_weibo = (ImageButton) findViewById(R.id.btn_weibo);
        btn_txweibo = (ImageButton) findViewById(R.id.btn_txweibo);
    }

    private void initData() {
        getPopu();
        /**
         * 新浪微博
         */
        sina_token = preferencesUtil.getString(WeiboUtil.SINA_TOKEN, "");
        sina_expires_in = preferencesUtil.getString(WeiboUtil.SINA_EXPIRES_IN,
                "");
        sina_uid = preferencesUtil.getString(WeiboUtil.SINA_UID, "");
        sina_nick = preferencesUtil.getString(WeiboUtil.SINA_NICK, "");
        if (sina_token.equals("") || sina_expires_in.equals("")
                || sina_uid.equals("")) {
            isBindSina = false;
        } else {
            isBindSina = true;
            // getSinaNickName();
            if (sina_nick != null && sina_nick.length() > 0) {
                tv_sina.setText(sina_nick);
            } else {
                String str = "<font color=\"#FF0000\">未取得用户昵称!</font>";
                tv_sina.setText(Html.fromHtml(str));
            }
        }

        /**
         * 腾讯微博
         */
        oAuth = new OAuthV2(WeiboUtil.TX_CALL_URI);
        oAuth.setClientId(WeiboUtil.TX_CLIENTID);
        oAuth.setClientSecret(WeiboUtil.TX_CLIENTSECRET);
        tx_token = preferencesUtil.getString(WeiboUtil.TX_TOKEN, "");
        tx_openid = preferencesUtil.getString(WeiboUtil.TX_OPENID, "");
        tx_openkey = preferencesUtil.getString(WeiboUtil.TX_OPENKEY, "");
        tx_expires_in = preferencesUtil.getString(WeiboUtil.TX_EXPIRES_IN, "");
        tx_nick = preferencesUtil.getString(WeiboUtil.TX_NICK, "");
        if (tx_token.equals("") || tx_openid.equals("")
                || tx_openkey.equals("") || tx_expires_in.equals("")) {
            isBindTx = false;
        } else {
            oAuth.setAccessToken(tx_token);
            oAuth.setOpenid(tx_openid);
            oAuth.setOpenkey(tx_openkey);
            oAuth.setExpiresIn(tx_expires_in);
            isBindTx = true;
            if (tx_nick != null && tx_nick.length() > 0) {
                tv_tx.setText(tx_nick);
            } else {
                String str = "<font color=\"#FF0000\">未取得用户昵称!</font>";
                tv_tx.setText(Html.fromHtml(str));
            }
        }
        popupDialog.dismiss();
    }

    private void initListener() {
        // TODO Auto-generated method stub
        ly_sina.setOnClickListener(clickListener);
        ly_tx.setOnClickListener(clickListener);

        btn_msg.setOnClickListener(listener);
        btn_weibo.setOnClickListener(listener);
        btn_txweibo.setOnClickListener(listener);
    }

    OnClickListener listener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (v == btn_msg) {
                getRecom(msg_code);
            } else if (v == btn_weibo) {
                if (isBindSina) {
                    getRecom(sina_code);
                } else {
                    showDialogById(unsinaGrant);
                }
            } else if (v == btn_txweibo) {
                if (isBindTx) {
                    getRecom(tx_code);
                } else {
                    showDialogById(untxGrant);
                }
            }
        }
    };

    public void getPopu() {
        popupDialog = new Dialog(WeiboManagerActivity.this,
                android.R.style.Theme_Translucent_NoTitleBar);
        popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popupDialog.setContentView(R.layout.loading_layout);
        popupDialog.setCancelable(false);
        TextView txtMsg = (TextView) popupDialog
                .findViewById(R.id.left_textView);
        txtMsg.setText("加载中,请稍等...");
        popupDialog.show();
    }

    OnClickListener clickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (v == ly_sina) {
                if (isBindSina) {
                    showDialogById(sinaGranted);
                } else {
                    showDialogById(unsinaGrant);
                }
            } else if (v == ly_tx) {
                if (isBindTx) {
                    showDialogById(txGranted);
                } else {
                    showDialogById(untxGrant);
                }
            }
        }
    };

    public void showDialogById(int id) {
        if (currentId != -1) {
            removeDialog(currentId);
        }
        currentId = id;
        showDialog(id);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        builder = new AlertDialog.Builder(WeiboManagerActivity.this);
        switch (id) {
        case unsinaGrant:
            builder.setTitle("新浪微博授权操作");
            builder.setMessage("是否授权并绑定新浪微博?");
            builder.setPositiveButton("是",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            // TODO Auto-generated method stub
                            Weibo weibo = Weibo.getInstance();
                            weibo.setupConsumerConfig(
                                    WeiboUtil.SINA_CONSUMER_KEY,
                                    WeiboUtil.SINA_CONSUMER_SECRET);
                            // Oauth2.0 隐式授权认证方式
                            weibo.setRedirectUrl(WeiboUtil.SINA_CALL_URI);// 应用回调页
                            weibo.authorize(WeiboManagerActivity.this,
                                    new AuthDialogListener());
                        }
                    });
            builder.setNegativeButton("否", null);
            break;
        case sinaGranted:
            builder.setTitle("新浪微博授权操作");
            builder.setMessage("是否解除绑定的新浪微博?");
            builder.setPositiveButton("是",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            // TODO Auto-generated method stub
                            preferencesUtil
                                    .saveString(WeiboUtil.SINA_TOKEN, "");
                            preferencesUtil.saveString(
                                    WeiboUtil.SINA_EXPIRES_IN, "");
                            preferencesUtil.saveString(WeiboUtil.SINA_UID, "");
                            tv_sina.setText("未绑定");
                            Utility.clearCookies(WeiboManagerActivity.this);
                            isBindSina = false;
                            showToast("解除绑定成功!");
                            tx_token = "";
                            tx_expires_in = "";
                            tx_openid = "";
                            tx_openkey = "";
                        }
                    });
            builder.setNegativeButton("否", null);
            break;
        case untxGrant:
            builder.setTitle("腾讯微博授权操作");
            builder.setMessage("是否授权并绑定腾讯微博?");
            builder.setPositiveButton("是",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            // TODO Auto-generated method stub
                            Intent intent = new Intent(
                                    WeiboManagerActivity.this,
                                    OAuthV2AuthorizeWebView.class);
                            intent.putExtra("oauth", oAuth);
                            startActivityForResult(intent, 1);
                        }
                    });
            builder.setNegativeButton("否", null);
            break;
        case txGranted:
            builder.setTitle("腾讯微博授权操作");
            builder.setMessage("是否解除绑定的腾讯微博?");
            builder.setPositiveButton("是",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            // TODO Auto-generated method stub
                            preferencesUtil.saveString(WeiboUtil.TX_TOKEN, "");
                            preferencesUtil.saveString(WeiboUtil.TX_OPENID, "");
                            preferencesUtil
                                    .saveString(WeiboUtil.TX_OPENKEY, "");
                            preferencesUtil.saveString(WeiboUtil.TX_EXPIRES_IN,
                                    "");
                            tv_tx.setText("未绑定");
                            isBindTx = false;
                            showToast("解除绑定成功!");
                            oAuth = null;
                        }
                    });
            builder.setNegativeButton("否", null);
            break;
        }
        return builder.create();
    }

    public void getTxNickName() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                UserAPI userAPI = new UserAPI(OAuthConstants.OAUTH_VERSION_2_A);
                try {
                    String response = userAPI.info(oAuth, "json");// 获取用户信息
                    TxUserInfo info = JSONUtil.fromJson(response,
                            TxUserInfo.class);
                    tv_tx.setText(info.getData().getNick());
                    preferencesUtil.saveString(WeiboUtil.TX_NICK, info
                            .getData().getNick());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                userAPI.shutdownConnection();
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == OAuthV2AuthorizeWebView.RESULT_CODE) {
                oAuth = (OAuthV2) data.getExtras().getSerializable("oauth");
                preferencesUtil.saveString(WeiboUtil.TX_TOKEN, oAuth
                        .getAccessToken());
                preferencesUtil.saveString(WeiboUtil.TX_OPENID, oAuth
                        .getOpenid());
                preferencesUtil.saveString(WeiboUtil.TX_OPENKEY, oAuth
                        .getOpenkey());
                preferencesUtil.saveString(WeiboUtil.TX_EXPIRES_IN, oAuth
                        .getExpiresIn());
                getTxNickName();
                isBindTx = true;
                showToast("绑定成功!");
            }
        }
    }

    class AuthDialogListener implements WeiboDialogListener {

        @Override
        public void onComplete(Bundle values) {
            sina_token = values.getString("access_token");
            sina_expires_in = values.getString("expires_in");
            sina_uid = values.getString("uid");
            preferencesUtil.saveString(WeiboUtil.SINA_TOKEN, sina_token);
            preferencesUtil.saveString(WeiboUtil.SINA_EXPIRES_IN,
                    sina_expires_in);
            preferencesUtil.saveString(WeiboUtil.SINA_UID, sina_uid);
            isBindSina = true;
            getSinaNickName();
            showToast("绑定成功!");
        }

        @Override
        public void onError(DialogError e) {
            logger.error(e);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onWeiboException(WeiboException e) {
            logger.error(e);
        }

    }

    /**
     * 获取新浪用户信息
     * 
     * @author wubo
     * @createtime 2012-9-21
     * @return
     * @throws WeiboException
     */
    public void getSinaNickName() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    AccessToken accessToken = new AccessToken(sina_token,
                            WeiboUtil.SINA_CONSUMER_SECRET);
                    Weibo weibo = Weibo.getInstance();
                    Utility.setAuthorization(new Oauth2AccessTokenHeader());
                    weibo.setAccessToken(accessToken);
                    WeiboParameters bundle = new WeiboParameters();
                    bundle.add("source", WeiboUtil.SINA_CONSUMER_KEY);
                    bundle.add("access_token", sina_token);
                    bundle.add("uid", sina_uid);
                    String url = "https://api.weibo.com/2/users/show.json";
                    String msg = Utility.openUrl(WeiboManagerActivity.this,
                            url, "GET", bundle, null, accessToken);
                    JSONObject jsonObject = new JSONObject(msg);
                    if (jsonObject != null) {
                        sina_nick = jsonObject.getString("name").toString();
                        tv_sina.setText(sina_nick);
                        preferencesUtil.saveString(WeiboUtil.SINA_NICK,
                                sina_nick);
                    }
                } catch (WeiboException e) {
                    // TODO: handle exception
                    String str = "<font color=\"#FF0000\">未取得用户昵称!</font>";
                    tv_sina.setText(Html.fromHtml(str));
                    if (e.getStatusCode() == 403) {
                        Toast.makeText(WeiboManagerActivity.this,
                                "该应用未得到新浪审核,要获取用户信息,请先通知开发者把此账号设置为测试账号",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    String str = "<font color=\"#FF0000\">未取得用户昵称!</font>";
                    tv_sina.setText(Html.fromHtml(str));
                    return;
                }
            }
        });
    }

    /**
     * 获取推荐信息
     * 
     * @author wubo
     * @createtime 2012-9-5
     */
    public void getRecom(int code) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));// 登录账号
        serverMana.supportRequest(AgentConfig.ExtensionMsg(), paras, true,
                "加载中,请稍等 ...", code);
    }

    @Override
    public void uiCallBack(Object supportResponse, final int caseKey) {
        // TODO Auto-generated method stub
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }

        ExtensionMsgResult emf = JSONUtil.fromJson(supportResponse.toString(),
                ExtensionMsgResult.class);
        if (emf != null) {
            if (emf.getErrorCode() == BaseResult.SUCCESS
                    && emf.getContent() != null && emf.getContent().size() > 0) {
                List<String> itemlist = emf.getContent();
                final String[] items = new String[itemlist.size()];
                for (int i = 0; i < itemlist.size(); i++) {
                    items[i] = itemlist.get(i);
                }
                final String url = emf.getShopUrl();
                new AlertDialog.Builder(WeiboManagerActivity.this).setTitle(
                        "选择推荐信息").setItems(items,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                switch (caseKey) {
                                case msg_code:
                                    Intent intent = new Intent(
                                            WeiboManagerActivity.this,
                                            MsgSendActivity.class);
                                    intent.putExtra(FinalUtil.SENDMMS_NUM, "");
                                    intent.putExtra(FinalUtil.SENDMMS_MSG,
                                            items[which]);
                                    intent.putExtra(FinalUtil.SENDMMS_ADD, url);
                                    intent
                                            .putExtra(FinalUtil.SENDMMS_TYPE,
                                                    "1");
                                    startActivity(intent);
                                    break;
                                case sina_code:

                                    AccessToken accessToken = new AccessToken(
                                            sina_token,
                                            WeiboUtil.SINA_CONSUMER_SECRET);
                                    accessToken.setExpiresIn(sina_expires_in);
                                    Utility
                                            .setAuthorization(new Oauth2AccessTokenHeader());
                                    Weibo.getInstance().setAccessToken(
                                            accessToken);
                                    Intent i = new Intent(
                                            WeiboManagerActivity.this,
                                            WeiboPushActivity.class);
                                    i.putExtra(WeiboUtil.EXTRA_ACCESS_TOKEN,
                                            accessToken.getToken());
                                    i.putExtra(WeiboUtil.EXTRA_TOKEN_SECRET,
                                            accessToken.getSecret());
                                    i.putExtra(FinalUtil.SENDMMS_MSG,
                                            items[which]);
                                    i.putExtra(FinalUtil.SENDMMS_ADD, url);
                                    i.putExtra(WeiboUtil.WEIBOFLAG, 0);
                                    WeiboManagerActivity.this.startActivity(i);
                                    break;
                                case tx_code:
                                    Intent itx = new Intent(
                                            WeiboManagerActivity.this,
                                            WeiboPushActivity.class);
                                    itx.putExtra(WeiboUtil.TX_OAUTH, oAuth);
                                    itx.putExtra(FinalUtil.SENDMMS_MSG,
                                            items[which]);
                                    itx.putExtra(FinalUtil.SENDMMS_ADD, url);
                                    itx.putExtra(WeiboUtil.WEIBOFLAG, 1);
                                    WeiboManagerActivity.this
                                            .startActivity(itx);
                                    break;
                                }
                            }
                        }).setNegativeButton("取消", null).create().show();
            } else if (emf.getErrorCode() == BaseResult.FAIL) {
                showToast(emf.errorMessage);
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
        } else {
            showToast(R.string.to_server_fail);
            return;
        }
    }
}
