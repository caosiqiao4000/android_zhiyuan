package com.gsta.v2.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.gsta.v2.activity.adapter.CitySpinnerAdapter;
import com.gsta.v2.db.ICityDao;
import com.gsta.v2.db.impl.CityDaoImpl;
import com.gsta.v2.entity.BaseUserinfo;
import com.gsta.v2.entity.City;
import com.gsta.v2.response.LoginResult;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.CameraAndShowDiagleActivity;
import com.gsta.v2.util.EditTextKeyListener;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.TextCountLimitWatcher;
import com.gsta.v2.util.Util;

/**
 * 注册代理商
 * 
 * @author boge
 * @time 2013-3-27
 * 
 */
public class RegisterAgentActivity extends CameraAndShowDiagleActivity implements
        IUICallBackInterface {
    Button btn_sub;
    Button btn_back;
    EditText agentName;
    // 店铺名称
    private EditText et_shopName;
    EditText mobile;
    TextView city;
    // 证件类型
    LinearLayout fl_identityType;
    TextView identityType;
    EditText identityNum_bycard; // 身份证号码
    EditText identityNum_byother; // 其他证号码
    String identity_item[];
    Builder builder;
    // 证件类型下标
    int identity_cho = 0;
    // 营销工号
    EditText agentNO;
    EditText address;
    Button upload1;
    // Button upload2;
    // 推荐人帐号
    LinearLayout fl_followUid;
    EditText followUid;
    // 营销工号
    private RadioGroup radioGroup1;

    // 图片地址
    // String pic1_path = "";
    // String pic2_path = "";
    // int open1_request = 1;
    // int open2_request = 2;

    final int register_code = 1;

    // 城市
    LinearLayout fl_city;
    ICityDao cityDao;
    List<City> first;// 一级
    List<City> second = new ArrayList<City>();// 二级

    Spinner s_first;
    CitySpinnerAdapter sa1;
    int cho1 = 0;
    Spinner s_second;
    CitySpinnerAdapter sa2;
    int cho2 = 0;
    // 邀请人账号
    String followAccount;
    private TextView tv_title;
    private Button btn_t_back;
    // 身份证处理
    private LayoutInflater layoutInflater;
    private boolean[] imaChange = { false, false }; // 指示是否有更改
    private int indexImag = 0; // 选择哪张图片更改了
    private final int IMG_FORNT = 0x00AA;
    private final int IMG_VERSO = 0x00BB;
    private String[] imagPaht = { "", "" };

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            // 通知mainActivity 用户注册成功
            sendBroadcast(new Intent(FinalUtil.RECEIVER_AGENT_LOGING_IN));
            finish();
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.register);
        layoutInflater = LayoutInflater.from(RegisterAgentActivity.this);
        cityDao = new CityDaoImpl(RegisterAgentActivity.this);
        initView();
        initDate();
        initListener();
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText("代理商注册");

        btn_t_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_t_back.setVisibility(0);
        btn_sub = (Button) findViewById(R.id.btn_sub);
        btn_back = (Button) findViewById(R.id.back_btn);
        agentName = (EditText) findViewById(R.id.agentName);
        agentName.addTextChangedListener(new TextCountLimitWatcher(20,
                agentName));
        et_shopName = (EditText) findViewById(R.id.et_shopName);
        et_shopName.addTextChangedListener(new TextCountLimitWatcher(28,
                agentName));
        mobile = (EditText) findViewById(R.id.mobile);
        mobile.setKeyListener(new EditTextKeyListener(
                EditTextKeyListener.PHONE_TYPE));
        mobile.addTextChangedListener(new TextCountLimitWatcher(11, mobile));
        identityType = (TextView) findViewById(R.id.identityType);
        city = (TextView) findViewById(R.id.city);
        identityNum_bycard = (EditText) findViewById(R.id.identityNum_bycard);
        identityNum_byother = (EditText) findViewById(R.id.identityNum_byother);
        followUid = (EditText) findViewById(R.id.followUid);
        followUid.addTextChangedListener(new TextCountLimitWatcher(20,
                followUid));
        agentNO = (EditText) findViewById(R.id.agentNO);
        agentNO.addTextChangedListener(new TextCountLimitWatcher(20, agentNO));
        fl_identityType = (LinearLayout) findViewById(R.id.fl_identityType);
        fl_city = (LinearLayout) findViewById(R.id.fl_city);
        address = (EditText) findViewById(R.id.address);
        address.addTextChangedListener(new TextCountLimitWatcher(140, address));
        //
        upload1 = (Button) findViewById(R.id.upload1);
        // upload2 = (Button) findViewById(R.id.upload2);
        fl_followUid = (LinearLayout) findViewById(R.id.fl_followUid);

        radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
    }

    private void initDate() {
        identity_item = getResources().getStringArray(R.array.identitytype);
        first = new ArrayList<City>();
        second = new ArrayList<City>();
        first = cityDao.getChildCitybyId("2");
        second = cityDao.getChildCitybyId(first.get(0).getId());
        sa1 = new CitySpinnerAdapter(RegisterAgentActivity.this, first);
        sa2 = new CitySpinnerAdapter(RegisterAgentActivity.this, second);

        followAccount = application.getInviteAccount();

        if (followAccount != null && followAccount.length() > 0) {
            followUid.setText(followAccount);
            followUid.setEnabled(false);
        }
        identityType.setText(identity_item[identity_cho]);
    }

    private void initListener() {

        fl_identityType.setOnClickListener(listener);
        fl_city.setOnClickListener(listener);
        upload1.setOnClickListener(listener);
        btn_sub.setOnClickListener(listener);
        btn_back.setOnClickListener(listener);
        btn_t_back.setOnClickListener(listener);
        radioGroup1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio1) { // 有营销工号
                    agentNO.setVisibility(0);
                } else {
                    agentNO.setVisibility(View.GONE);
                }
            }
        });
    }

    private MyDialog activateVoucherDialog;
    private final int uploadPic_code = 0x00cc; // 上传图片

    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == upload1) { // 正面证件照
                View editV = layoutInflater.inflate(R.layout.dialog_pic_load, null);

                final ImageView iv_front = (ImageView) editV.findViewById(R.id.iv_front);
                final ImageView iv_verso = (ImageView) editV.findViewById(R.id.iv_verso);
                iv_front.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        indexImag = IMG_FORNT;
                        imageview = iv_front;
                        mShowDialog(DIALOG_ID_IMAGE_NO);
                    }
                });
                iv_verso.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        indexImag = IMG_VERSO;
                        imageview = iv_verso;
                        mShowDialog(DIALOG_ID_IMAGE_NO);
                    }
                });

                activateVoucherDialog = MyDialog.showDialog(RegisterAgentActivity.this, "上传身份证",
                        editV, R.string.submit, R.string.cancel, false, new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // 检查是否是两张图片 是否检测图片大小
                                if (imaChange[0] && imaChange[1]) {
                                    ServerSupportManager serverMana = new ServerSupportManager(RegisterAgentActivity.this,
                                            RegisterAgentActivity.this);
                                    ArrayList<MyFilePart> uploads = new ArrayList<MyFilePart>();
                                    List<Parameter> paras = new ArrayList<Parameter>();
                                    paras.add(new Parameter("uid", application.getUid()));
                                    // 证件附件
                                    paras.add(new Parameter("identityPic", "1"));
                                    uploadPic(iv_front, uploads, "0", imagPaht[0]);
                                    uploadPic(iv_verso, uploads, "1", imagPaht[1]);
                                    serverMana.supportRequest(AgentConfig.uploadIdentityCard(), paras, uploads,
                                            true, "提交中,请稍等 ....", uploadPic_code);
                                } else {
                                    showToast("请选择身份证的正反面上传,同时上传两张...");
                                }
                            }
                        });
                activateVoucherDialog.getDialog().setCanceledOnTouchOutside(false);
            } else if (v == fl_identityType) {
                builder = new AlertDialog.Builder(RegisterAgentActivity.this);
                builder.setTitle("选择证件类型").setItems(identity_item,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                identity_cho = which;
                                changeEditType();

                            }
                        });
                builder.create().show();
            } else if (v == fl_city) {
                builder = new AlertDialog.Builder(RegisterAgentActivity.this);
                View inflater = LayoutInflater.from(RegisterAgentActivity.this)
                        .inflate(R.layout.city_selection, null);
                s_first = (Spinner) inflater.findViewById(R.id.s1);
                s_first.setAdapter(sa1);
                s_first.setOnItemSelectedListener(itemSelectedListener1);
                s_second = (Spinner) inflater.findViewById(R.id.s2);
                s_second.setAdapter(sa2);
                s_second.setOnItemSelectedListener(itemSelectedListener2);
                s_first.setSelection(cho1);
                s_second.setSelection(cho2);
                builder.setTitle("选择代理城市").setView(inflater);
                builder.setPositiveButton("确定",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                String city_str = "";
                                if (first.size() >= cho1) {
                                    city_str += first.get(cho1).getName();
                                }
                                if (second.size() >= cho2) {
                                    city_str += second.get(cho2).getName();
                                }
                                city.setText(city_str);
                            }
                        });
                builder.setNegativeButton("取消", null);
                builder.create().show();
            } else if (v == btn_sub) { // 提交
                register();
            } else if (v == btn_back || v.getId() == btn_t_back.getId()) {
                finish();
            }
        }
    };

    /**
     * 组装上传图片
     * 
     * @author caosq 2013-4-26 下午6:13:12
     * @param iv_front
     * @param uploads
     */
    private void uploadPic(final ImageView imageView, ArrayList<MyFilePart> uploads, String name, String imagPath) {
        File fileFront = new File(imagPath);
        if (fileFront != null) {// 如果文件存在,准备上传
            MyFilePart uploader1;
            try {
                uploader1 = new MyFilePart(name, fileFront
                        .getName(), fileFront, new MimetypesFileTypeMap()
                        .getContentType(fileFront),
                        SyncHttpClient.CONTENT_CHARSET);
                uploads.add(uploader1);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    // 只有身份证验证 其他 不验证
    private void changeEditType() {
        if (identity_cho == 0) { // 身份证类型
            identityNum_bycard.setVisibility(0);
            identityNum_byother.setVisibility(View.GONE);
        } else {
            identityNum_bycard.setVisibility(View.GONE);
            identityNum_byother.setVisibility(0);
        }
        identityType.setText(identity_item[identity_cho]);
    }

    OnItemSelectedListener itemSelectedListener1 = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View v, int position,
                long id) {
            if (cho1 != position) {
                cho1 = position;
                second = cityDao.getChildCitybyId(first.get(position).getId());
                sa2.setCitys(second);
                s_second.setSelection(0);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    OnItemSelectedListener itemSelectedListener2 = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View v, int position,
                long id) {
            if (cho2 != position) {
                cho2 = position;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    /**
     * dialog单选以及确定、取消按钮监听
     */
    private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (dialog != null) {
                // 取消浮层
                dialog.cancel();
            }
            if (currentDialogId == DIALOG_ID_IMAGE) {// 上传图片
                doSelectPhoto(which);// 拍照还是从图库选择图片
            } else {
                // ...
            }
        }
    };

    /**
     * 切割图片之后返回的 Bundle数据处理，取得bitmap类型图片
     * 
     * @param extras
     *            void
     */
    @Override
    public void createPhoto(Bundle extras) {
        if (extras != null) {
            photo = extras.getParcelable("data");
            if (photo != null) {
                setImage(photo);
            } else {
                showToast(R.string.res_get_photo_fail);
            }
        }
    }

    @Override
    public void setImage(android.graphics.Bitmap bitmap) {
        super.setImage(bitmap);
        if (indexImag == IMG_VERSO) {
            imaChange[1] = true;
            imagPaht[1] = getPhotoPath();
        } else if (indexImag == IMG_FORNT) {
            imaChange[0] = true;
            imagPaht[0] = getPhotoPath();
        }
    };

    public void register() {
        if (!Util.isUserName(agentName.getText().toString())) {
            showToast("代理商名称的长度只能在2-10个字母、汉字、数字之间!");
            return;
        }
        if (!Util.isUserName(et_shopName.getText().toString())) {
            showToast("店铺名称的长度只能在4-14个字符之间!");
            return;
        }
        if (mobile.getText().toString().equals("")) {
            showToast("手机号码不能为空!");
            return;
        }
        if (!Util.isCellphone(mobile.getText().toString())) {
            showToast("手机号码格式不正确!");
            return;
        }
        if (identityType.getText().toString().equals("")) {
            showToast("证件类型不能为空!");
            return;
        }
        if (identity_cho == 0) {
            if (identityNum_bycard.getText().toString().length() != 15
                    && identityNum_bycard.getText().toString().length() != 18) {
                showToast("证件号码长度为15位或者18位!");
                return;
            }
        } else {
            if (identityNum_bycard.getText().toString().length() == 0) {
                showToast("证件号码不能为空!");
                return;
            }
        }
        if (city.getText().toString().equals("")) {
            showToast("代理城市不能为空!");
            return;
        }

        /*
         * String agentNo = agentNO.getText().toString(); followAccount = followUid.getText().toString(); if (agentNo.trim().length() == 0) { if
         * (followAccount.trim().length() == 0) { showToast("推荐人账号不能为空!"); return; } }
         */

        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));
        paras.add(new Parameter("userName", agentName.getText().toString()));
        paras.add(new Parameter("shopName", et_shopName.getText().toString()));
        paras.add(new Parameter("provinceId", first.get(cho1).getId()));
        paras.add(new Parameter("cityId", second.get(cho2).getId()));
        paras.add(new Parameter("identityType", String.valueOf(identity_cho)));
        if (identity_cho == 0) {
            paras.add(new Parameter("identityNo", identityNum_bycard.getText()
                    .toString()));
        } else {
            paras.add(new Parameter("identityNo", identityNum_byother.getText()
                    .toString()));
        }
        paras.add(new Parameter("mobile", mobile.getText().toString()));
        paras.add(new Parameter("address", address.getText().toString()));
        paras.add(new Parameter("account", followUid.getText().toString()));
        paras.add(new Parameter("agentNO", agentNO.getText().toString()));
        serverMana.supportRequest(AgentConfig.AgentInfoAdd(), paras, true,
                "提交中,请稍等 ....", register_code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse))
            return;

        if (caseKey == uploadPic_code) {
            if (null != activateVoucherDialog) {
                MyDialog.dismiss(activateVoucherDialog);
            }
            showToast("上传证件成功");
            return;
        }

        switch (caseKey) {
        case register_code:
            LoginResult loginResult = JSONUtil.fromJson(supportResponse
                    .toString(), LoginResult.class);
            BaseUserinfo info = loginResult.getUserInfo();
            if (info == null) {
                finish();
            } else {
                // 保存用户信息
                application.setInviteAccount(loginResult.getInviteAccount());
                application.setUserinfo(info);
            }
            application.setAgentInfo(loginResult.getAgentInfo());
            showToast("代理商资料信息已提交,先去商品管理吧!");
            application.setAgentStatu(FinalUtil.AgentStatu_normal);
            handler.sendEmptyMessage(1);
            break;
        }

    }

    @Override
    protected android.content.DialogInterface.OnClickListener setDialogClickListener() {
        return mDialogClickListener;
    }

    @Override
    protected Context setCreateContext() {
        return RegisterAgentActivity.this;
    }
}
