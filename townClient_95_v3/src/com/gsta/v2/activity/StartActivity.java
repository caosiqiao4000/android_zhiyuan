package com.gsta.v2.activity;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mobile.http.HttpReqCode;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Window;
import android.widget.TextView;

import com.google.code.microlog4android.Level;
import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.google.code.microlog4android.appender.FileAppender;
import com.google.code.microlog4android.appender.LogCatAppender;
import com.google.code.microlog4android.config.PropertyConfigurator;
import com.google.code.microlog4android.format.Formatter;
import com.google.code.microlog4android.format.PatternFormatter;
import com.gsta.v2.activity.service.SendService;
import com.gsta.v2.db.impl.SharedPreferencesUtil;
import com.gsta.v2.entity.BaseAgentInfo;
import com.gsta.v2.entity.BaseUserinfo;
import com.gsta.v2.response.LoginResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 这个类是启动界面
 * 
 * @author caosq 2013-5-3
 */
public class StartActivity extends BaseActivity implements IUICallBackInterface {

    private static final Logger logger = LoggerFactory.getLogger(LoginActivity.class);
    private TextView tv_des;
    private Intent sendService;
    private SharedPreferencesUtil mUtil;
    private String username;
    private String pass;
    private int type = 0; // 登录类型 0:手机登录 1:邮箱登陆 2 游客 不登录
    private final int start_Code = 0x001;
    private final int query_version_code = 0x002;
    private final String TAG = Util.getClassName();

    private final String KEY_USER_INFO = "userInfo";
    private final String KEY_AGENT_INFO = "agentInfo";
    private final String KEY_ACCOUNT = "account";
    private final String KEY_INVITE_ACCOUNT = "inviteAccount";

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 1) {
                gotoMainAcivity();
            } else if (msg.what == 3) {
                stopService(new Intent(StartActivity.this, SendService.class));
                System.exit(0);
            }
        };
    };

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.a_start);
        loggerConfig();

        if (application.isLoginOutIsNormal()) {
            handler.sendEmptyMessage(3);
            return;
        }
        initView();
        initData();

        if (savedInstanceState != null) {
            restoreData(savedInstanceState);
        }
    }

    /**
     * @author caosq 2013-5-8 下午2:57:16
     * @param savedInstanceState
     */
    private void restoreData(android.os.Bundle savedInstanceState) {
        Serializable ser = savedInstanceState.getSerializable(FinalUtil.LONGIN_STATE_FLAG);
        int a = 0;
        if (null != ser) {
            a = (Integer) ser;
        }
        application.setLoginState(a);
        if (a == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            BaseUserinfo userinfo = (BaseUserinfo) savedInstanceState.getSerializable(KEY_USER_INFO);
            if (null != userinfo) {
                logger.info(" savedInstanceState 恢复用户数据  userinfo");
                application.setUserinfo(userinfo);
            }
            BaseAgentInfo agentInfo = (BaseAgentInfo) savedInstanceState.getSerializable(KEY_AGENT_INFO);
            if (null != agentInfo) {
                logger.info(" savedInstanceState 恢复用户数据  agentInfo");
                application.setAgentInfo(agentInfo);
            }
            String acc = (String) savedInstanceState.getSerializable(KEY_ACCOUNT);
            if (null != acc) {
                logger.info(" savedInstanceState 恢复用户数据  account");
                application.setAccount(acc);
            }
            String inviteAccount = (String) savedInstanceState.getSerializable(KEY_INVITE_ACCOUNT);
            if (null != inviteAccount) {
                logger.info(" savedInstanceState 恢复用户数据  inviteAccount");
                application.setInviteAccount(inviteAccount);
            }
            prepareLogin();
            return;
        } else {
            logger.info(" 恢复KEY = " + a);
        }
    }

    private void initView() {
        tv_des = (TextView) findViewById(R.id.tv_start_des);
        mUtil = new com.gsta.v2.db.impl.SharedPreferencesUtil(
                StartActivity.this);
        sendService = new Intent(StartActivity.this, SendService.class);
        startService(sendService);
    }

    private void initData() {
        queryVersion();

        saveCityInfo();
    }

    /**
     * 
     * 检测版本后
     * 
     * @author caosq 2013-5-7 下午3:02:49
     */
    private void prepareLogin() {
        getUserNameAndPass();
        logger.warn("startActivity is prepareLogin");
        // 如果以前登录过就在后台登录
        if (TextUtils.isEmpty(username.trim()) == false && TextUtils.isEmpty(pass.trim()) == false) {
            if (Util.isCellphone(username)) {
                type = 0;
                login();
            } else if (Util.isEmail(username)) {
                type = 1;
                login();
            }
        } else {
            type = 2;
            handler.sendEmptyMessageDelayed(1, 3000);
        }
    }

    private void getUserNameAndPass() {
        // 取得SharedPreferences存储的用户名
        username = mUtil.getString(AgentConfig.LOGIN_IN_NAME, "");
        // 取得SharedPreferences存储的密码
        pass = mUtil.getString(AgentConfig.LOGIN_IN_PASS, "");
    }

    private void loggerConfig() {
        logger.setClientID("TownClient_95");
        logger.setLevel(Level.DEBUG);
        PropertyConfigurator.getConfigurator(this).configure();
        PatternFormatter pFormatter = new PatternFormatter();
        pFormatter.setPattern("%d {ISO8601} [%P] %m %T");
        LogCatAppender logCatAppender = new LogCatAppender();
        logCatAppender.setFormatter(pFormatter);
        @SuppressWarnings("unused")
        Formatter formatter = logCatAppender.getFormatter();
        try {
            if (!logCatAppender.isLogOpen()) {
                logCatAppender.open();
            }
            logger.addAppender(logCatAppender);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        final FileAppender fa = new FileAppender();
        fa.setFileName("agentLogCat.txt");
        fa.setAppend(true);
        try {
            if (!fa.isLogOpen()) {
                fa.open();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.addAppender(fa);
        logger.debug(" StartActivity is onCreate " + Util.getSysNowTime());
        logger.debug(logCatAppender.getLogSize());
    }

    private void saveCityInfo() {
        // 城市代码信息是否已经写入到数据库
        String cityload = mUtil.getString(SharedPreferencesUtil.CITY_LOAD,
                "fail");
        // 若城市信息没写到数据库,则调用service读取城市代码文件写入数据库
        if (!cityload.equals("succ")) {
            sendService.putExtra(FinalUtil.SENDSERVICE_FLAG,
                    FinalUtil.saveCitys);
            startService(sendService);
        }
        // 读取手机通讯录保存到数据库
        sendService
                .putExtra(FinalUtil.SENDSERVICE_FLAG, FinalUtil.saveContacts);
        startService(sendService);
    }

    private void gotoMainAcivity() {
        // 跳转到MainActivity
        Intent intent = new Intent(StartActivity.this,
                MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * @author caosq 2013-4-19 下午4:12:07
     */
    private void startOpenfire() {
        sendService.putExtra(FinalUtil.SENDSERVICE_FLAG,
                FinalUtil.imLogin);
        sendService.putExtra(FinalUtil.Login_User, application.getUid());
        sendService.putExtra(FinalUtil.Login_Pass, pass);
        // 登录openfire
        startService(sendService);
    }

    // 查询版本
    private void queryVersion() {
        tv_des.setText("检查版本信息  ...");
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        serverMana.supportRequest(AgentConfig.queryVersion(), paras, false,
                "登录中,请稍等   ...", query_version_code);
        // 在有 WIFI 但WIFI无法正常联网的情况下 HTTP超时无法返回
        // handler.sendEmptyMessageDelayed(what, delayMillis);
    }

    /**
     * 登录
     * 
     * @author wubo
     * @createtime 2012-8-31
     */
    private void login() {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("account", username));// 登录账号
        paras.add(new Parameter("password", pass));// 登录密码
        paras.add(new Parameter("type", String.valueOf(type)));
        serverMana.supportRequest(AgentConfig.getLogin(), paras, false,
                "登录中,请稍等 ...", start_Code);
        tv_des.setText("登录中,请稍等 ...");
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse)) {
            if (caseKey == query_version_code && !(supportResponse instanceof HttpReqCode)) {
                prepareLogin();
            } else {
                handler.sendEmptyMessageAtTime(1, 3000);
            }
            return;
        }
        LoginResult loginResult = JSONUtil.fromJson(supportResponse
                .toString(), LoginResult.class);
        if (caseKey == query_version_code) {
            String versionName = Util.getAppVersionName(StartActivity.this);
            // 版本更新
            if (null != loginResult && loginResult.getVersionCode() != null
                    && !loginResult.getVersionCode().equals(
                            versionName)) {
                Builder buil = new AlertDialog.Builder(
                        StartActivity.this);
                buil.setCancelable(false)
                        .setTitle("有新版本V" + loginResult.getVersionCode() + ":")
                        .setMessage("更新内容为:\n" + loginResult.getVersionDesc())
                        .setPositiveButton("更新", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int which) {
                                Intent intent = new Intent();
                                intent.setAction("android.intent.action.VIEW");
                                Uri content_url = Uri.parse(AgentConfig.downLoadPath());
                                intent.setData(content_url);
                                startActivity(intent);
                                handler.sendEmptyMessageDelayed(3, 1000);
                            }
                        });
                loginResult.setUpdateType(0);
                if (loginResult.getUpdateType() == 0) {// 该版本是否强制升级
                    // 0:不强制升级 1:强制升级
                    buil.setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(
                                        DialogInterface arg0,
                                        int arg1) {
                                    prepareLogin();
                                }
                            });
                } else {
                    buil.setNegativeButton("退出",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    handler.sendEmptyMessage(3);
                                }
                            });
                }
                buil.create().show();
            } else {
                prepareLogin();
            }
        }
        if (caseKey == start_Code) {
            if (loginResult != null) {
                BaseUserinfo info = loginResult.getUserInfo();
                if (info == null) {
                    finish();
                } else {
                    application.setLoginState(FinalUtil.LONGIN_LONGINSTATE_SUCCEED);
                    // 保存用户信息
                    application.setInviteAccount(loginResult
                            .getInviteAccount());
                    application.setUserinfo(info);
                    application.setAccount(username);
                    BaseAgentInfo agentInfo = loginResult.getAgentInfo();
                    application.setAgentInfo(agentInfo);
                    startOpenfire();
                    handler.sendEmptyMessage(1);
                }
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        restoreData(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        logger.info(TAG + " onSaveInstanceState 保存数据  " + Util.getSysNowTime());
        super.onSaveInstanceState(outState);
    }
}
