package com.gsta.v2.activity;

import android.annotation.SuppressLint;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.CookieSyncManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.util.WebViewDialog;

/**
 * 使用自定义浏览器 购买流程和其他物流流程
 * 
 * @author fitch
 * @desc webview购买流程
 */
public class AgentWebViewActivity extends BaseActivity {

	private static final Logger logger = LoggerFactory
			.getLogger(AgentWebViewActivity.class);

	private WebView webView;
	private ImageButton close;// 关闭
	private ImageButton refresh;// 刷新
	private ImageButton goForward;// 前进
	private ImageButton goBack;// 后退
	private WebSettings webSettings;
	private ProgressBar pb;

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1:
				webView.setVisibility(View.GONE);
				webView.destroy();
				break;
			default:
				break;
			}
		};
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.stat_webview);
		Bundle bundle = getIntent().getExtras();
		String url = bundle.getString("url"); // 传入的url
		String data = bundle.getString("data");
		if (null == url) {
			showToast("传入的 URL 不能为空");
			finish();
		}

		initView();
		loadWebView(url, data);
	}

	private void initView() {
		pb = (ProgressBar) findViewById(R.id.pb);
		close = (ImageButton) this.findViewById(R.id.closeView);// 关闭
		refresh = (ImageButton) this.findViewById(R.id.refresh);// 刷新
		goForward = (ImageButton) this.findViewById(R.id.goForward);// 向前
		goBack = (ImageButton) this.findViewById(R.id.goBack);// 回退
		webView = (WebView) findViewById(R.id.webview);
	}

	/**
	 * 加载页面
	 * 
	 * @param url
	 */
	@SuppressLint("SetJavaScriptEnabled")
	public void loadWebView(String url, String data) {
		// 绑定事件
		close.setOnClickListener(onClick);
		refresh.setOnClickListener(onClick);
		goForward.setOnClickListener(onClick);
		goBack.setOnClickListener(onClick);

		// webView.setWebChromeClient(new WebViewDialog(this, pb));
		webView.setWebChromeClient(new WebViewDialog(this, pb, goForward,
				goBack));
		webView.addJavascriptInterface(new JavaScriptInterface(), "agentClient");
		CookieSyncManager.createInstance(this);
		CookieSyncManager.getInstance().startSync();
		webView.clearCache(true);
		webView.clearHistory();

		if (url == null || url.equals("")) {
			return;
		}
		if (data != null && data.length() > 0) {
			url = url + "?" + data;
		} else {
			showToast("参数有错误..." + data);
		}
		final String realUrl = url;
		webSettings = webView.getSettings();
		webSettings.setSupportZoom(true);
		webSettings.setBuiltInZoomControls(true);// 放大控件
		webSettings.setSavePassword(false);
		webSettings.setSaveFormData(true);
		webSettings.setJavaScriptEnabled(true);
		logger.info("AgentWebViewActivity url= " + url);
		handler.post(new Runnable() {
			@Override
			public void run() {
				webView.loadUrl(realUrl);// 加载页面
				webView.setWebViewClient(new WebViewClient() {// 设置Web视图
					@Override
					public boolean shouldOverrideUrlLoading(WebView view,
							String url) {
						Log.i(">>>Url:", url);
						view.loadUrl(url);
						return true;
					}

					@Override
					public void onReceivedError(WebView view, int errorCode,
							String description, String failingUrl) {
						super.onReceivedError(view, errorCode, description,
								failingUrl);
					}

					@Override
					public void onReceivedSslError(WebView view,
							SslErrorHandler handler, SslError error) {
						handler.proceed();
					}

				});

			}
		});
	}

	final class JavaScriptInterface {
		// 该方法被浏览器端调用
		public void clickOnAndroid() {
			handler.post(new Runnable() {
				public void run() {
					// 调用js中的wave()方法
					webView.loadUrl("javascript:wave();");
				}
			});
			// 提示关闭页面完成后,结束当前窗口
			finish();
		}

		public void closeWebview() {
			// 提示关闭页面完成后,结束当前窗口
			finish();
		}
	}

	@Override
	// 设置回退,覆盖Activity类的onKeyDown方法
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (webView.canGoBack()) {
				webView.goBack();
			} else {
				this.finish();
			}
		}
		return false;
	}

	/**
	 * 点击事件
	 */
	public OnClickListener onClick = new OnClickListener() {
		public void onClick(View v) {
			if (v == goBack) {
				if (webView.canGoBack()) {
					webView.goBack();
				}
			} else if (v == goForward) {
				if (webView.canGoForward()) {
					webView.goForward();
				}
			} else if (v == refresh) {
				webView.reload();
			} else if (v == close) {
				finish();
				return;
			}
		};
	};

	@Override
	protected void onDestroy() {
		webView.setVisibility(View.GONE);
		webView.destroy();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

}
