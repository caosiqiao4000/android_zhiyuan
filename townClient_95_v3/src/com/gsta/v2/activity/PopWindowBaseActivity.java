package com.gsta.v2.activity;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.SimpleAdapter;

/**
 * 
 * 有下拉弹窗的ACTIVITY 实现
 */
public abstract class PopWindowBaseActivity extends BaseActivity {

    protected PopupWindow window;
    protected ListView list;
    protected final String KEY = "key";

    /**
     * 生成弹窗
     * 
     * @param parent
     *            点击哪个按钮出现
     * @param listClickListener
     *            监听弹窗里的事件
     */
    protected void popWindow(View parent, OnItemClickListener listClickListener) {
        if (window == null) {
            window = initPopWindowBindClickListener(listClickListener);
        }
        setPopWindow();
        window.showAtLocation(parent, Gravity.CENTER_HORIZONTAL | Gravity.TOP
                | Gravity.RIGHT, 0, 90);
    }

    /**
     * 将弹窗显示在选择View 的相对位置
     * 
     * @author caosq 2013-4-22 下午4:15:23
     * @param parent
     * @param listClickListener
     */
    // protected void popWindowShowAsDropDown(View parent, OnItemClickListener listClickListener) {
    // if (window == null) {
    // window = initPopWindowBindClickListener(listClickListener);
    // }
    // setPopWindow();
    // int[] location = new int[2];
    // parent.getLocationOnScreen(location);
    // Rect anchorRect = new Rect(location[0], location[1],
    // location[0] + parent.getWidth(), location[1] + parent.getHeight());
    //
    // int x = location[0];
    // int y = location[1];
    // window.showAsDropDown(parent, -(2 * (parent.getWidth())), 0);
    // }

    /**
     * @author caosq 2013-4-22 下午4:14:06
     */
    private void setPopWindow() {
        window.setBackgroundDrawable(getResources().getDrawable(
                R.drawable.dialog_1));
        window.setFocusable(true);
        window.setOutsideTouchable(false);
        window.setAnimationStyle(R.style.popupWindowAnim);
        window.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                // TODO Auto-generated method stub
            }
        });
        window.update();
    }

    /**
     * 初始化 弹窗和 绑定事件
     * 
     * @author caosq 2013-4-22 下午3:52:31
     * @param listClickListener
     */
    private PopupWindow initPopWindowBindClickListener(OnItemClickListener listClickListener) {
        LayoutInflater lay = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = lay.inflate(R.layout.pop, null);
        list = (ListView) v.findViewById(R.id.pop_list);

        SimpleAdapter adapter = new SimpleAdapter(this, createData(),
                R.layout.pop_list_item, new String[] { KEY },
                new int[] { R.id.title });

        list.setAdapter(adapter);
        list.setItemsCanFocus(false);
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        list.setOnItemClickListener(listClickListener);
        // window = new PopupWindow(v, 260, 300);
        int x = (int) getResources().getDimension(R.dimen.pop_x);
        int y = (int) getResources().getDimension(R.dimen.pop_x);
        if (application.getAgentInfo() != null
                && application.getAgentInfo().getAgentIdentity() != null
                && application.getAgentInfo().getAgentIdentity() == 11) {
            y = (int) getResources().getDimension(R.dimen.pop_y);
        }
        return new PopupWindow(v, x, y);
    }

    /**
     * 弹窗里要显示的数据
     * 
     * @return
     */
    protected abstract List<Map<String, Object>> createData();
}
