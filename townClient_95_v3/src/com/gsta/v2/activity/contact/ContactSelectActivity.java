package com.gsta.v2.activity.contact;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.ContactSelectAdapter;
import com.gsta.v2.db.IContactsDao;
import com.gsta.v2.db.impl.ContactsDaoImpl;
import com.gsta.v2.entity.Contacts;

/**
 * 本地通讯录联系人选择
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
public class ContactSelectActivity extends BaseActivity {

    private ListView mListView;
    private ContactSelectAdapter mContactAdapter;

    private Button btnSubmit, btnCancel, btnBack;
    private List<Contacts> selectList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.contact_select);

        mListView = (ListView) findViewById(R.id.lv_contact_select);
        btnSubmit = (Button) findViewById(R.id.submit);
        btnCancel = (Button) findViewById(R.id.cancel);
        btnBack = (Button) findViewById(R.id.mytitile_btn_left);
        btnBack.setVisibility(0);

        TextView tvtitle = (TextView) findViewById(R.id.mytitle_textView);
        int opType = getIntent().getIntExtra(ContactActivity.CONTACT_OPTYPE, 0);

        // 从数据库读取联系人
        IContactsDao contactDao = new ContactsDaoImpl(this);
        if (opType == 0) {// 从手机联系人里面获取
            selectList = contactDao.getPhoneLocalContaces();
            if (null != selectList || selectList.size() > 0) {
                List<Contacts> allContacts = contactDao.getAllContacts(application.getUid());
                // 去掉重复的联系人
                if (null != allContacts && allContacts.size() > 0) {
                    for (int i = 0; i < allContacts.size(); i++) {
                        for (int j = 0; j < selectList.size(); j++) {
                            if (allContacts.get(i).getPhoneNum().equals(selectList.get(j).getPhoneNum())) {
                                selectList.remove(j);
                            }
                        }
                    }
                    // 全部加入通讯录
                    if (selectList == null || selectList.size() == 0) {
                        showToast("通讯录里面的联系人已经全部添加~");
                        finish();
                    }
                }
            }
        } else {// 从本地通讯录获取
            // selectList = application.getLocalPhone();
            selectList = contactDao.getAllContacts(application.getUid());
        }
        tvtitle.setText("本地通讯录");
        // 加入通讯录或者联系人里面没用户
        if (selectList == null || selectList.size() == 0) {
            showToast("程序通讯录里面还没人哟,请在联系人的通讯录功能项里面添加~");
            finish();
        }
        mContactAdapter = new ContactSelectAdapter(this, selectList);

        mListView.setAdapter(mContactAdapter);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int arg2,
                    long arg3) {
                ContactSelectAdapter.ViewHolder vHollder = (ContactSelectAdapter.ViewHolder) view
                        .getTag();
                // 更改联系人选择状况
                vHollder.selected.toggle();
                mContactAdapter.selectByIndex(arg2, vHollder.selected
                        .isChecked());
                changeCount();
            }
        });

        /**
         * 提交
         */
        btnSubmit.setOnClickListener(new OnClickListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                List<Contacts> selectMember = mContactAdapter.getSelectMember();
                Intent intent = new Intent();
                intent.putParcelableArrayListExtra(Contacts.LOCALCONTACTS_KEY,
                        (ArrayList<? extends Parcelable>) selectMember);
                ContactSelectActivity.this.setResult(RESULT_OK, intent);
                ContactSelectActivity.this.finish();
            }
        });

        btnCancel.setOnClickListener(btnClick);
        btnBack.setOnClickListener(btnClick);

    }

    OnClickListener btnClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            ContactSelectActivity.this.finish();
        }
    };

    /**
     * 改变选择联系人的数量
     * 
     * @author wubo
     * @time 2013-3-14
     * 
     */
    private void changeCount() {
        // TODO Auto-generated method stub
        @SuppressWarnings("unused")
        String countString = "(" + (mContactAdapter.getSelectCount()) + ")";
    }

    public List<Contacts> getSelectList() {
        return selectList;
    }

    public void setSelectList(List<Contacts> selectList) {
        this.selectList = selectList;
    }

}
