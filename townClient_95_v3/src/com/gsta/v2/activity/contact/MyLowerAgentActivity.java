package com.gsta.v2.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.activity.adapter.AgentAdapter;
import com.gsta.v2.entity.ActionItem;
import com.gsta.v2.entity.AgentInfo;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.response.ContactResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.ui.QuickActionBar;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 我介绍的代理商
 * 
 * @author caosq 2013-5-22
 */
public class MyLowerAgentActivity extends TopBackActivity implements
        IUICallBackInterface {

    private PullToRefreshListView pl_agent;
    private ListView lv_agent;
    private List<AgentInfo> agents = new ArrayList<AgentInfo>();
    AgentAdapter agentAdapter;

    @SuppressWarnings("unused")
    private String contactId = "";
    private String contactUId = "";
    private String contactPhone = "";
    @SuppressWarnings("unused")
    private String contactNote = "";
    private String contactName;
    @SuppressWarnings("unused")
    private int index;

    boolean agent_load = true;
    private final int buyermsg_code = 8;
    private final int agentf_code = 14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loweragent);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        pl_agent = (PullToRefreshListView) findViewById(R.id.lv_agent);
        lv_agent = pl_agent.getRefreshableView();
    }

    private void initData() {
        // agentDao = new AgentDaoImpl(ContactsActivity.this);
        // agents = agentDao.getAgents(application.getUid());
        agentAdapter = new AgentAdapter(this, agents, agentClickListener);
        lv_agent.setAdapter(agentAdapter);
        getAgent(agentf_code);
    }

    private final OnClickListener agentClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof ImageView) {
                int position = ((Integer) view.getTag()).intValue();
                ActionItem actMessage = new ActionItem(getResources()
                        .getDrawable(R.drawable.sms), "短信", this);
                ActionItem actCall = new ActionItem(getResources().getDrawable(
                        R.drawable.call), "呼叫", this);
                ActionItem actIM = new ActionItem(getResources().getDrawable(
                        R.drawable.im), "私聊", this);
                QuickActionBar qaBar = new QuickActionBar(view, position);
                qaBar.setEnableActionsLayoutAnim(true);
                qaBar.addActionItem(actCall);
                qaBar.addActionItem(actMessage);
                qaBar.addActionItem(actIM);
                qaBar.show();
            } else if (view instanceof LinearLayout) {
                // ActionItem组件
                LinearLayout actionsLayout = (LinearLayout) view;
                QuickActionBar bar = (QuickActionBar) actionsLayout.getTag();
                bar.dismissQuickActionBar();
                int currentUserPosition = bar.getListItemIndex();
                TextView txtView = (TextView) actionsLayout
                        .findViewById(R.id.qa_actionItem_name);
                String actionName = txtView.getText().toString();
                setValue(currentUserPosition);
                if (actionName.equals("短信")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Uri smsToUri = Uri.parse("smsto:" + contactPhone);
                    Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
                    startActivity(mIntent);
                } else if (actionName.equals("呼叫")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel://" + contactPhone));
                    startActivity(intent);
                } else if (actionName.equals("私聊")) {
                    // toIM(new IMBean(contactUId, contactName));
                    String a = agents.get(currentUserPosition).getPhoto();
                    if (null == a) {
                        a = "";
                    }
                    if (!Util.toIM(MyLowerAgentActivity.this, new IMBean(contactUId, contactName, a))) {
                        showToast("不能与店主聊天...");
                    }
                }
            }
        }

    };

    public void setValue(int index) {
        this.index = index;
        contactId = "";
        contactUId = String.valueOf(agents.get(index).getUid());
        contactPhone = agents.get(index).getMobile();
        contactNote = "";
        contactName = agents.get(index).getName();
    }

    private void initListener() {
        lv_agent.setOnItemClickListener(itemClickListener4);
        pl_agent.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAgent(agentf_code);
            }
        });
    }

    // listView点击 查询下家代理商 的 相关信息
    OnItemClickListener itemClickListener4 = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                long id) {
            AgentInfo item = ((AgentInfo) parent.getAdapter().getItem(position));
            Intent intent = new Intent(MyLowerAgentActivity.this,
                    SeachAgentInfoActivity.class);
            intent.putExtra(FinalUtil.BASEAGENTINFO, item);
            startActivityForResult(intent, buyermsg_code);
        }
    };

    /**
     * 获取我的下家
     * 
     * @author wubo
     * @createtime 2012-9-5
     */
    public void getAgent(int code) {
        pl_agent.setRefreshing(true);
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        // paras.add(new Parameter("uid", "10013"));// 登录账号
        paras.add(new Parameter("uid", application.getUid()));// 登录账号
        serverMana.supportRequest(AgentConfig.getAgents(), paras, false,
                "加载中,请稍等 ...", code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_agent.isRefreshing()) {
            pl_agent.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }

        switch (caseKey) {
        case agentf_code:
            agents.clear();
            ContactResult userResult = JSONUtil.fromJson(supportResponse.toString(),
                    ContactResult.class);
            if (userResult != null) {
                if (userResult.getAgents() != null) {
                    agents = userResult.getAgents();
                }
                agentAdapter.setmUserList(agents);
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }
}
