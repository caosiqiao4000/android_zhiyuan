package com.gsta.v2.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.activity.adapter.ShopFansAdapter;
import com.gsta.v2.activity.waiter.MsgSendActivity;
import com.gsta.v2.entity.ActionItem;
import com.gsta.v2.entity.MyUserInfo;
import com.gsta.v2.response.ContactResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.ui.QuickActionBar;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 
 * @author longxianwen
 * @createTime Apr 2, 2013 9:30:24 AM
 * @version: 1.0
 * @desc:本店粉丝
 */
public class MyShopFans extends TopBackActivity implements IUICallBackInterface {

    private Context context = MyShopFans.this;
    private PullToRefreshListView pl_myshopfans;
    private ListView msfListView;
    private List<MyUserInfo> items = new ArrayList<MyUserInfo>();
    private ShopFansAdapter adapter;
    // private Button btn_loadmore;
    // private View btn_loadmore;
    private View loadMoreView;
    // private RelativeLayout rr_loadmore;
    private ProgressBar loadmroe_pb;

    final int load_querycode = 11; // 数据查询标识
    private int pageCount = 0;

    @SuppressWarnings("unused")
    private String contactId = ""; // 后台数据 ID
    private String contactPhone = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myshopfans);

        init();
        initView();
        initData();
        initListener();
    }

    private void init() {
        load();
    }

    private void initListener() {
        msfListView.setOnItemClickListener(listener1);
        pl_myshopfans.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageCount = 0;
                items.clear();
                load();
            }
        });
    }

    OnItemClickListener listener1 = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            // 每一项单击
        }
    };

    private void initData() {
        // TODO Auto-generated method stub
        adapter = new ShopFansAdapter(context, items, onClickListener);
        msfListView.setAdapter(adapter);
    }

    private OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            if (view instanceof ImageView) {
                int position = ((Integer) view.getTag()).intValue();
                ActionItem actMessage = new ActionItem(getResources()
                        .getDrawable(R.drawable.sms), "短信", this);
                // ActionItem actCall = new ActionItem(getResources().getDrawable(
                // R.drawable.call), "呼叫", this);
                // ActionItem actDel = new ActionItem(getResources().getDrawable(
                // R.drawable.remove), "移除", this);
                ActionItem invite = new ActionItem(getResources().getDrawable(
                        R.drawable.addgoods), "邀请", this);
                QuickActionBar qaBar = new QuickActionBar(view, position);
                qaBar.setEnableActionsLayoutAnim(true);
                // qaBar.addActionItem(actCall);
                qaBar.addActionItem(actMessage);
                // qaBar.addActionItem(actDel);
                qaBar.addActionItem(invite);
                qaBar.show();
            } else if (view instanceof LinearLayout) {
                // ActionItem组件
                LinearLayout actionsLayout = (LinearLayout) view;
                QuickActionBar bar = (QuickActionBar) actionsLayout.getTag();
                bar.dismissQuickActionBar();
                int currentUserPosition = bar.getListItemIndex();
                TextView txtView = (TextView) actionsLayout
                        .findViewById(R.id.qa_actionItem_name);
                String actionName = txtView.getText().toString();
                setValue(currentUserPosition);
                if (actionName.equals("短信")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Uri smsToUri = Uri.parse("smsto:" + contactPhone);
                    Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
                    startActivity(mIntent);
                } else if (actionName.equals("呼叫")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel://" + contactPhone));
                    startActivity(intent);
                } else if (actionName.equals("移除")) {
                    // 依据ID删除粉丝

                } else if (actionName.equals("邀请")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Intent intent = new Intent(MyShopFans.this,
                            MsgSendActivity.class);
                    intent.putExtra(FinalUtil.SENDMMS_NUM, contactPhone);
                    intent.putExtra(FinalUtil.SENDMMS_MSG,
                            "我在东方数码城做代理商，挣了不少钱。有好处当然不会忘了你，快来注册吧。");
                    intent.putExtra(FinalUtil.SENDMMS_ADD, "下载地址:"
                            + AgentConfig.downLoadPath());
                    intent.putExtra(FinalUtil.SENDMMS_TYPE, "2");
                    startActivity(intent);
                }
            }
        }
    };

    private void setValue(int currentUserPosition) {
        MyUserInfo info = items.get(currentUserPosition);
        if (null != info) {
            contactId = String.valueOf(info.getId());
            contactPhone = info.getMobile();
        }
    }

    private void initView() {
        pl_myshopfans = (PullToRefreshListView) findViewById(R.id.pl_myshopfans);
        msfListView = pl_myshopfans.getRefreshableView();

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        loadMoreView = inflater.inflate(R.layout.loadmore, null);

        Button rr_loadmore = (Button) loadMoreView.findViewById(R.id.loadMoreButton);
        rr_loadmore.setVisibility(View.GONE);
        rr_loadmore.setOnClickListener(myClickListener);

        loadmroe_pb = (ProgressBar) loadMoreView.findViewById(R.id.loadmroe_pb);
    }

    OnClickListener myClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            // case R.id.loadMoreButton:
            // loadmroe_pb.setVisibility(View.VISIBLE);
            // load();
            // break;
            case R.id.loadMoreButton:
                loadmroe_pb.setVisibility(View.VISIBLE);
                load();
                break;
            }
        }
    };

    private void load() {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();

        paras.add(new Parameter("uid", application.getUid()));
        paras.add(new Parameter("limit", pageCount + ",20"));// 登录密码

        serverMana.supportRequest(AgentConfig.getMyShopFansUrl(), paras, false,
                "提交中,请稍等 ....", load_querycode);
    }

    boolean isload = true;

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_myshopfans.isRefreshing()) {
            pl_myshopfans.onRefreshComplete();
        }
        switch (caseKey) {
        case load_querycode:
            ContactResult cr = JSONUtil.fromJson(supportResponse.toString(),
                    ContactResult.class);
            if (cr != null) {
                if (cr.getUsers() != null && cr.getUsers().size() > 0) {
                    items.addAll(cr.getUsers());
                    // items = cr.getUsers();
                    pageCount += cr.getUsers().size();
                    adapter.setmUserList(items);

                    if (cr.gettCount() > items.size()) {
                        loadmroe_pb.setVisibility(View.GONE);
                        loadMoreView.setVisibility(View.VISIBLE);
                        if (msfListView.getFooterViewsCount() == 0) {
                            msfListView.addFooterView(loadMoreView);
                        }
                    } else {
                        msfListView.removeFooterView(loadMoreView);
                    }
                } else {
                    showToast("没有为您搜索到更多的粉丝!");
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }
}
