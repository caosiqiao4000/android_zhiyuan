package com.gsta.v2.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.activity.adapter.RimShopAdapter;
import com.gsta.v2.activity.location.CommonLocation;
import com.gsta.v2.activity.location.CommonLocation.CommLocationInterface;
import com.gsta.v2.activity.myshop.ShopHomePageActivity;
import com.gsta.v2.entity.ShopInfo;
import com.gsta.v2.entity_v2.ShopLocationResult;
import com.gsta.v2.response.RimShopResult;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 我的关注商家
 * 
 * @author caosq 2013-3-29
 */
public class MyAttentionShopActivity extends TopBackActivity implements
		IUICallBackInterface {
	private static final String TAG = Util.getClassName();
	private PullToRefreshListView pl_merchant;
	private ListView lv_merchant;
	private View loadMoreBuyer;
	private Button btn_loadMoreBuyer;
	private View noDataView;
	private RimShopAdapter adapter;
	private List<ShopInfo> shopInfos;
	private MyDialog delAgentShopDialog;
	private CommonLocation location; // 定位
	private MyAttentionShopQuery query;

	private final int attentionShop_code = 10; // 关注商家
	private final int up_code = 12; // 上传标识
	private final int agentShopDel_code = 13;// 取消店铺的收藏

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1:
				final ShopInfo info = (ShopInfo) msg.obj;
				ListView listView = (ListView) LayoutInflater.from(
						MyAttentionShopActivity.this.getParent()).inflate(
						R.layout.dialog_list_view, null);
				String[] objects = { "查看详情", "取消收藏" };
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
						MyAttentionShopActivity.this.getParent(),
						R.layout.dialog_text_view, objects);
				listView.setAdapter(adapter);
				// 设置监听器
				listView.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						switch (position) {
						case 0:
							goToShop(info);
							break;
						case 1:
							agentShopDel(info);
							break;
						}
						MyDialog.dismiss(delAgentShopDialog);
					}
				});
				delAgentShopDialog = MyDialog.showDialog(
						MyAttentionShopActivity.this.getParent(),
						info.getName(), listView);
				break;

			default:
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_attention_shop);
		init();
		initView();
		initData();
		initListener();
	}

	private void init() {
		location = CommonLocation.getCommlocation(MyAttentionShopActivity.this
				.getParent());
		if (null != location) {
			location.enableMyLocation();
			location.setCommLocationInterface(locaInter);
		}
	}

	private void initView() {
		pl_merchant = (PullToRefreshListView) findViewById(R.id.pl_merchant);
		lv_merchant = pl_merchant.getRefreshableView();
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		loadMoreBuyer = inflater.inflate(R.layout.loadmore, null);
		btn_loadMoreBuyer = (Button) loadMoreBuyer
				.findViewById(R.id.loadMoreButton);
		btn_loadMoreBuyer.setVisibility(View.GONE);
		lv_merchant.addFooterView(loadMoreBuyer);
		noDataView = findViewById(R.id.nodata);
	}

	private void initData() {
		shopInfos = new ArrayList<ShopInfo>();
		adapter = new RimShopAdapter(MyAttentionShopActivity.this, shopInfos,
				0, 0);
		lv_merchant.setAdapter(adapter);
		query = new MyAttentionShopQuery();
		query.setUid(application.getUid());
		query.setRefresh(false);
		getAttentionShop(attentionShop_code);
	}

	private void initListener() {

		lv_merchant.setOnItemClickListener(itemClickListener);
		lv_merchant.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				ShopInfo info = (ShopInfo) parent.getItemAtPosition(position);
				// 弹出操作对话框
				Message message = handler.obtainMessage();
				message.what = 1;
				message.obj = info;
				handler.sendMessage(message);
				return false;
			}
		});
		pl_merchant.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				// query.setIndex(1);
				// query.setTotalNum(0);
				query.setRefresh(true);
				shopInfos.clear();
				getAttentionShop(attentionShop_code);
			}
		});
	}

	OnItemClickListener itemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			ShopInfo shopInfo = (ShopInfo) parent.getAdapter()
					.getItem(position);
			goToShop(shopInfo);
		}

	};

	/**
	 * @author caosq 2013-4-1 下午4:13:51
	 * @param shopInfo
	 *            去代理商的店铺
	 */
	private void goToShop(ShopInfo shopInfo) {
		Intent intent = new Intent();
		intent.setClass(MyAttentionShopActivity.this,
				ShopHomePageActivity.class);
		intent.putExtra(ShopHomePageActivity.IFMYSELF_SHOP_FLAG,
				shopInfo.getAgentUid());
		intent.putExtra(ShopHomePageActivity.MYSELF_SHOPNAME_FLAG,
				shopInfo.getName());
		startActivity(intent);
	}

	// 取消店铺的收藏
	private void agentShopDel(ShopInfo info) {
		ServerSupportManager serverMana = new ServerSupportManager(
				MyAttentionShopActivity.this.getParent(),
				MyAttentionShopActivity.this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("agentUid", info.getAgentUid())); // 关系用户UID，被收藏的店铺标识UID
		// 默认为 0
		paras.add(new Parameter("userId", application.getUid()));// 用户标识
		paras.add(new Parameter("opType", String.valueOf(1)));// 0:收藏，1：取消收藏
		serverMana.supportRequest(AgentConfig.agentShopAdd(), paras, true,
				"加载中,请稍等 ...", agentShopDel_code);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onStart() {
		if (location != null) {
			location.setCommLocationInterface(locaInter);
		}
		super.onStart();
	}

	@Override
	protected void onPause() {
		if (null != location) {
			location.disableMyLocation(locaInter);
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if (location != null) {
			location.disableMyLocation(locaInter);
		}
		location = null;
		super.onDestroy();
	}

	CommLocationInterface locaInter = new CommLocationInterface() {
		@Override
		public void getLoation(double geoLat, double geoLng) {
			uploadLocation(geoLat, geoLng);
			adapter.notifyData(geoLat, geoLng);
		}
	};

	/**
	 * 
	 * @author longxianwen
	 * @Title: uploadLocation
	 * @Description: 上传自己当前位置到后台
	 * @param @param geoLat
	 * @param @param geoLng 设定文件
	 * @return void 返回类型
	 */
	private void uploadLocation(Double geoLat, Double geoLng) {
		if (application.getLoginState() != FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
			return;
		}
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();

		paras.add(new Parameter("userId", application.getUid()));
		paras.add(new Parameter("longitude", geoLng + ""));
		paras.add(new Parameter("latitude", geoLat + ""));

		serverMana.supportRequest(AgentConfig.upLocation(), paras, false,
				"提交中,请稍等 ....", up_code);
	}

	/**
	 * interface/ shopmanager /QueryCollectShop 查询关注商家信息
	 * 
	 * @author caosq 2013-3-29 下午2:41:12
	 * @param caseCode
	 */
	private void getAttentionShop(int caseCode) {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("userId", application.getUid()));
		serverMana.supportRequest(AgentConfig.queryAttentionShop(), paras,
				false, "加载中,请稍等 ...", caseCode);

	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		pl_merchant.onRefreshComplete();
		if (!HttpResponseStatus(supportResponse)) {
			return;
		}

		if (caseKey == attentionShop_code) { // 取得关注商家
			ShopLocationResult userResult = JSONUtil.fromJson(
					supportResponse.toString(), ShopLocationResult.class);
			if (userResult != null) {
				if (query.isRefresh()) {
					query.setRefresh(false);
					shopInfos.clear();
					adapter.notifyDataSetInvalidated();
				}
				if (null != userResult.getConcernShops()
						&& userResult.getConcernShops().size() > 0) {
					shopInfos.addAll(userResult.getConcernShops());
				}
				// adapter.setRsInfos(shopInfos);
				adapter.notifyDataSetChanged();
				if (shopInfos.size() <= 0) {
					showNoDataView();
				}
			} else {
				showToast("无数据");
			}
		} else if (caseKey == agentShopDel_code) {
			ShopLocationResult goodsResult = JSONUtil.fromJson(
					supportResponse.toString(), ShopLocationResult.class);
			if (goodsResult != null) {// 0:失败,1:成功， 999其他异常。
				query.setRefresh(true);
				getAttentionShop(attentionShop_code);
				showToast(goodsResult.getErrorMessage());
			} else {
				showToast(R.string.to_server_fail);
			}
			return;
		} else if (caseKey == up_code) {
			RimShopResult rr = JSONUtil.fromJson(supportResponse.toString(),
					RimShopResult.class);
			if (rr != null) {
				Log.d(TAG, "上传成功");
			} else {
				showToast(R.string.to_server_fail);
				return;
			}
		}
	}

	private void showNoDataView() {
		noDataView.setVisibility(0);
		handler.sendEmptyMessageDelayed(1, 2000);
	}

	class MyAttentionShopQuery {
		private String uid;
		private boolean refresh;

		public boolean isRefresh() {
			return refresh;
		}

		public void setRefresh(boolean refresh) {
			this.refresh = refresh;
		}

		public String getUid() {
			return uid;
		}

		public void setUid(String uid) {
			this.uid = uid;
		}
	}
}
