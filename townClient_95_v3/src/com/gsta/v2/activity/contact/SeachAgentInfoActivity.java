package com.gsta.v2.activity.contact;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.myshop.MonthScoreActivity;
import com.gsta.v2.db.ICityDao;
import com.gsta.v2.db.impl.CityDaoImpl;
import com.gsta.v2.entity.AgentInfo;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.Util;

/**
 * 查询下家代理商 的 相关信息
 * 
 * @author boge
 * @time 2013-3-26
 * 
 */
public class SeachAgentInfoActivity extends BaseActivity {

    private TextView tv_title;

    private TextView agentName;
    private TextView mobile;
    private TextView email;
    private TextView qq;
    private TextView msn;

    private TextView iv_level;
    private TextView phone;
    private TextView post;
    private TextView address;
    private TextView users;
    private TextView goods;
    // 代理地区
    private TextView tv_adress;

    private Button search, btn_back;

    private AgentInfo agentInfo;
    private ICityDao cityDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.seachagentinfo);
        cityDao = new CityDaoImpl(SeachAgentInfoActivity.this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        agentName = (TextView) findViewById(R.id.agentName);
        iv_level = (TextView) findViewById(R.id.iv_level);
        tv_adress = (TextView) findViewById(R.id.tv_adress);
        phone = (TextView) findViewById(R.id.phone);
        mobile = (TextView) findViewById(R.id.mobile);
        email = (TextView) findViewById(R.id.email);
        qq = (TextView) findViewById(R.id.qq);
        msn = (TextView) findViewById(R.id.msn);
        post = (TextView) findViewById(R.id.post);
        address = (TextView) findViewById(R.id.address);
        users = (TextView) findViewById(R.id.users);
        goods = (TextView) findViewById(R.id.goods);
        search = (Button) findViewById(R.id.search);

        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);
    }

    /**
     * 获取数据数组
     */
    private void initData() {
        agentInfo = (AgentInfo) getIntent().getSerializableExtra(
                FinalUtil.BASEAGENTINFO);
        tv_title.setText(agentInfo.getName());
        agentName.setText(agentInfo.getUserName());
        phone.setText(agentInfo.getPhone());
        mobile.setText(agentInfo.getMobile());
        email.setText(agentInfo.getEmail());
        qq.setText(agentInfo.getQq());
        msn.setText(agentInfo.getMsn());
        post.setText(agentInfo.getZipCode());
        address.setText(agentInfo.getAddress());
        if (null != agentInfo.getCityId()) {
            com.gsta.v2.entity.City cityInfoCity = cityDao.queryCityById(agentInfo.getCityId());
            tv_adress.setText(null == cityInfoCity.getName() ? "" : cityInfoCity.getName());
        }
        if (agentInfo.getUsers() == null) {
            users.setText("0");
        } else {
            users.setText(agentInfo.getUsers().toString());
        }
        if (agentInfo.getGoods() == null) {
            goods.setText("0");
        } else {
            goods.setText(agentInfo.getGoods().toString());
        }
        // iv_level.setImageDrawable(getResources().getDrawable(
        // Util.getLevelCover(agentInfo.getGrade())));
        if (null != agentInfo.getGrade()) {
            iv_level.setText(Util.getLevelCoverTextDescript(agentInfo.getGrade()));
        }
    }

    private void initListener() {
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SeachAgentInfoActivity.this.finish();
            }
        });

        search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SeachAgentInfoActivity.this,
                        MonthScoreActivity.class);
                intent.putExtra(FinalUtil.OPERUID, agentInfo.getUid());
                startActivity(intent);
            }
        });
    }
}
