package com.gsta.v2.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.BuyerAdapter;
import com.gsta.v2.activity.adapter.ContactsAdapter;
import com.gsta.v2.activity.myshop.OrderQueryActivity;
import com.gsta.v2.activity.waiter.MsgSendActivity;
import com.gsta.v2.db.IContactsDao;
import com.gsta.v2.db.impl.ContactsDaoImpl;
import com.gsta.v2.entity.ActionItem;
import com.gsta.v2.entity.BuyerInfo;
import com.gsta.v2.entity.Contacts;
import com.gsta.v2.response.ContactResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.ui.QuickActionBar;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;

/**
 * 联系人 联系人权限： 1.代理商展示：通讯录，我的买家，本店用户，本店粉丝，关注商家，我介绍的代理商。
 * <p>
 * 2.普通用户展示：通讯录，我的卖家，关注商家
 */
public class ContactActivity extends TopBackAndShowDialogActivity implements IUICallBackInterface {

    private ViewFlipper flipper;
    // head
    private HorizontalScrollView hs1;
    private RadioGroup rg1;
    private RadioButton r1;
    private RadioButton r2;
    private RadioButton r3;
    private RadioButton r4;
    private RadioButton r5;
    private RadioButton r6;
    private RadioButton r7;
    private final String MYSHOPUSERS_TAG = "myShopUser_a";
    private final String MYSHOPFANS_TAG = "myShopFans_a";
    private final String MYATTENTIONSHOP_TAG = "myAttentShop_a";
    private final String MYLOWERAGENT_TAG = "myloweragent_a";
    private final String MYVENDOR_TAG = "myVendor_a";

    private TextView tv_title;

    // 1 联系人列表
    private IContactsDao contactDao;
    private EditText etSearch; // 搜索框
    // 搜索按钮
    private ImageView btnClear;
    private ListView lv_contact; // 联系人列表
    private List<Contacts> contacts = new ArrayList<Contacts>(); // 添加的通讯录成员
    private ContactsAdapter contactsAdapter;
    // 通讯录 搜索框的添加按钮
    private ImageView btnAdd;
    // 0 是从手机通讯录添加 1 是从程序通讯录选择
    public static final String CONTACT_OPTYPE = "opType";

    private String contactId = "";
    // private String contactUId = "";
    private String contactPhone = "";
    private String contactNote = "";
    // private String contactName;
    private int index;
    private final int addContact_code = 7;

    // 3 我的买家
    private PullToRefreshListView pl_buyer;
    private ListView lv_buyer;
    private List<BuyerInfo> buyers = new ArrayList<BuyerInfo>();
    private BuyerAdapter buyerAdapter;
    private long buyerIndex = 1;
    private long buyerLength = 1;
    private View loadMoreBuyer;
    // private Button btn_loadMoreBuyer;
    private String query_acount = "";

    // 我的卖家
    private LinearLayout ly_vendor;

    // 4 我的用户 本店用户
    private LinearLayout ly_users;
    // 5 粉丝
    private LinearLayout ly_fans;

    // 6 关注商家
    private LinearLayout ly_merchant;

    // 我介绍的代理商
    private LinearLayout ly_agent;

    private final int buyer_code = 2;
    private final int updateBuyerNote_code = 5;

    private final int buyerf_code = 12; // 买家

    private boolean[] radioClickBoolean = { true, true, true, true, true, true, true, true }; // 第0个不使用
    // 代理商状态
    private int type = 0;
    // 选择了不同的radio
    private int radioIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_contacter);
        type = application.getAgentStatu();

        initView();
        initData();
        initListener();
    }

    private void initView() {
        hs1 = (HorizontalScrollView) findViewById(R.id.tab_view);
        rg1 = (RadioGroup) findViewById(R.id.rg1);
        r1 = (RadioButton) findViewById(R.id.r1);
        r2 = (RadioButton) findViewById(R.id.r2);
        r3 = (RadioButton) findViewById(R.id.r3);
        r4 = (RadioButton) findViewById(R.id.r4);
        r5 = (RadioButton) findViewById(R.id.r5);
        r6 = (RadioButton) findViewById(R.id.r6);
        r7 = (RadioButton) findViewById(R.id.r7);

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        flipper = (ViewFlipper) findViewById(R.id.myViewFlipper1);

        etSearch = (EditText) findViewById(R.id.et_search);
        etSearch.addTextChangedListener(editContactTextChangeListener);
        btnAdd = (ImageView) findViewById(R.id.btn_add);
        btnClear = (ImageView) findViewById(R.id.btn_clear);
        // 清除通讯录搜索条件
        btnClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
                contactsAdapter.setmUserList(contacts);
                btnClear.setImageResource(R.drawable.search_off);
            }
        });
        lv_contact = (ListView) findViewById(R.id.lv_contact);

        pl_buyer = (PullToRefreshListView) findViewById(R.id.lv_buyer);
        lv_buyer = pl_buyer.getRefreshableView();
        loadMoreBuyer = inflater.inflate(R.layout.loadmore, null);
        Button btn_loadMoreBuyer = (Button) loadMoreBuyer
                .findViewById(R.id.loadMoreButton);
        btn_loadMoreBuyer.setVisibility(0);
        btn_loadMoreBuyer.setOnClickListener(buyerClickListener);

        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText("联  系  人");
        ly_users = (LinearLayout) findViewById(R.id.ly_users);
        ly_merchant = (LinearLayout) findViewById(R.id.ly_merchant);
        ly_agent = (LinearLayout) findViewById(R.id.ly_agent);
        ly_fans = (LinearLayout) findViewById(R.id.ly_fans);
        ly_vendor = (LinearLayout) findViewById(R.id.ly_vendor);
    }

    private void initData() {
        contactsAdapter = new ContactsAdapter(this, contacts,
                contactClickListener);
        contactDao = new ContactsDaoImpl(this);
        contacts = contactDao.getAllContacts(application.getUid());
        if (contacts != null && contacts.size() > 0) {
            contactsAdapter.setmUserList(contacts);
        }
        lv_contact.setAdapter(contactsAdapter);

        buyerAdapter = new BuyerAdapter(this, buyers, buyerClickListener);
        lv_buyer.setAdapter(buyerAdapter);

        changeContactRadio();
    }

    private void initListener() {
        rg1.setOnCheckedChangeListener(checkedChangeListener);
        btnAdd.setOnClickListener(clickListener);
        pl_buyer.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                buyerIndex = 1;
                getBuyer(buyerf_code);
            }
        });
    }

    // 我的买家
    private final OnClickListener buyerClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof ImageView) {
                int position = ((Integer) view.getTag()).intValue();
                QuickActionBar qaBar = new QuickActionBar(view, position);
                // 当是社区经理时
                if (application.getAgentInfo() != null
                        && application.getAgentInfo().getAgentIdentity() != null
                        && application.getAgentInfo().getAgentIdentity() == FinalUtil.AgentStatu_Identity) {
                    ActionItem actMessage = new ActionItem(getResources()
                            .getDrawable(R.drawable.sms), "短信", this);
                    ActionItem actCall = new ActionItem(getResources()
                            .getDrawable(R.drawable.call), "呼叫", this);
                    qaBar.addActionItem(actCall);
                    qaBar.addActionItem(actMessage);
                }
                // 普通代理商
                ActionItem actOrder = new ActionItem(getResources()
                        .getDrawable(R.drawable.search), "查询订单", this);
                ActionItem actNote = new ActionItem(getResources()
                        .getDrawable(R.drawable.note), "备注", this);
                qaBar.setEnableActionsLayoutAnim(true);
                qaBar.addActionItem(actOrder);
                qaBar.addActionItem(actNote);
                qaBar.show();
            } else if (view instanceof LinearLayout) {
                // ActionItem组件
                LinearLayout actionsLayout = (LinearLayout) view;
                QuickActionBar bar = (QuickActionBar) actionsLayout
                        .getTag();
                bar.dismissQuickActionBar();
                int currentUserPosition = bar.getListItemIndex();
                TextView txtView = (TextView) actionsLayout
                        .findViewById(R.id.qa_actionItem_name);
                String actionName = txtView.getText().toString();
                setValue(currentUserPosition);
                if (actionName.equals("短信")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Uri smsToUri = Uri.parse("smsto:" + contactPhone);
                    Intent mIntent = new Intent(Intent.ACTION_SENDTO,
                            smsToUri);
                    startActivity(mIntent);
                } else if (actionName.equals("呼叫")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel://" + contactPhone));
                    startActivity(intent);
                } else if (actionName.equals("备注")) {
                    Bundle bundle = new Bundle();
                    bundle.putString(KEY_STRING, contactNote);
                    showDialog(remark_code, bundle);
                } else if (actionName.equals("查询订单")) {
                    Intent intent = new Intent(ContactActivity.this,
                            OrderQueryActivity.class);
                    intent.putExtra(FinalUtil.QUERY_TYPE, "1");
                    intent.putExtra(FinalUtil.ORDER_ACCOUNT, query_acount);
                    startActivity(intent);
                }
            } else if (R.id.loadMoreButton == view.getId()) { // 买家 加载更多
                getBuyer(buyer_code);
            }
        }
    };

    /**
     * 联系人权限： 1.代理商展示：通讯录，我的买家，本店用户，本店粉丝，关注商家，我介绍的代理商。
     * <p>
     * 2.普通用户展示：通讯录，我的卖家，关注商家
     */
    private void changeContactRadio() {
        if (type == FinalUtil.AgentStatu_normal) {
            r3.setVisibility(View.VISIBLE);
            r4.setVisibility(View.VISIBLE);
            r5.setVisibility(View.VISIBLE);
            r7.setVisibility(View.VISIBLE);
        } else {
            r3.setVisibility(View.GONE);
            r4.setVisibility(View.GONE);
            r5.setVisibility(View.GONE);
            r7.setVisibility(View.GONE);
        }
    }

    @Override
    protected Dialog onCreateDialog(final int id) {
        return super.onCreateDialog(id);
    }

    /**
     * 加号按钮的点击事件
     */
    OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnAdd) {
                btnClear.performClick();
                // if (application.getLocalPhone() == null
                // || application.getLocalPhone().size() == 0) {
                // showToast("本地通讯录还没人哟~");
                // } else
                // {
                Intent intent = new Intent();
                intent.setClass(ContactActivity.this,
                        ContactSelectActivity.class);
                intent.putExtra(ContactActivity.CONTACT_OPTYPE, 0);
                ContactActivity.this.startActivityForResult(intent,
                        addContact_code);
                // }
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        radioIndex = rg1.getCheckedRadioButtonId();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (rg1.getCheckedRadioButtonId() != radioIndex) {
            if (null != checkedChangeListener) {
                checkedChangeListener.onCheckedChanged(rg1, radioIndex);
            }
        }
    }

    // 上面横条按钮事件
    OnCheckedChangeListener checkedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == r1.getId()) { // 通讯录
                if (flipper.getDisplayedChild() != 0) {
                    switchLayoutStateTo(flipper, 0);
                    hs1.scrollTo(0, 0);
                }
            } else if (checkedId == r2.getId()) { // 我的卖家
                if (flipper.getDisplayedChild() != 1) {
                    hs1.scrollTo(0, 0);
                    switchLayoutStateTo(flipper, 1);
                    if (radioClickBoolean[2]) {
                        radioClickBoolean[2] = false;
                        Intent intent = new Intent();
                        intent.setClass(ContactActivity.this, MyVendorsActivity.class);
                        startActivity(MYVENDOR_TAG, intent, ly_vendor);
                    }
                }
            } else if (checkedId == r3.getId()) { // 我的买家
                if (flipper.getDisplayedChild() != 2) {
                    hs1.scrollTo(50, 0);
                    switchLayoutStateTo(flipper, 2);
                    if (radioClickBoolean[3]) {
                        radioClickBoolean[3] = false;
                        getBuyer(buyerf_code);
                    }
                }
            } else if (checkedId == r4.getId()) { // 本店用户
                if (flipper.getDisplayedChild() != 3) {
                    hs1.scrollTo(130, 0);
                    switchLayoutStateTo(flipper, 3);
                    if (radioClickBoolean[4]) {
                        radioClickBoolean[4] = false;
                        Intent intent = new Intent();
                        intent.setClass(ContactActivity.this, MyShopUsers.class);
                        intent.putExtra(IFNEED_TITLE, false);
                        startActivity(MYSHOPUSERS_TAG, intent, ly_users);
                    }
                }
            } else if (checkedId == r5.getId()) { // 本店粉丝
                if (flipper.getDisplayedChild() != 4) {
                    hs1.scrollTo(240, 0);
                    switchLayoutStateTo(flipper, 4);
                    if (radioClickBoolean[5]) {
                        radioClickBoolean[5] = false;
                        Intent intent = new Intent();
                        intent.setClass(ContactActivity.this, MyShopFans.class);
                        intent.putExtra(IFNEED_TITLE, false);
                        startActivity(MYSHOPFANS_TAG, intent, ly_fans);
                    }
                }
            } else if (checkedId == r6.getId()) { // 关注商家
                if (flipper.getDisplayedChild() != 5) {
                    hs1.scrollTo(450, 0);
                    switchLayoutStateTo(flipper, 5);
                    if (radioClickBoolean[6]) {
                        radioClickBoolean[6] = false;
                        Intent intent = new Intent();
                        intent.setClass(ContactActivity.this, MyAttentionShopActivity.class);
                        startActivity(MYATTENTIONSHOP_TAG, intent, ly_merchant);
                    }
                }
            } else if (checkedId == r7.getId()) { // 我介绍的代理商
                if (flipper.getDisplayedChild() != 6) {
                    hs1.scrollTo(550, 0);
                    switchLayoutStateTo(flipper, 6);
                    if (radioClickBoolean[7]) {
                        radioClickBoolean[7] = false;
                        Intent intent = new Intent();
                        intent.setClass(ContactActivity.this, MyLowerAgentActivity.class);
                        startActivity(MYLOWERAGENT_TAG, intent, ly_agent);
                    }
                }
            }
        }
    };

    /**
     * 搜索框字符串改变监听器
     * 
     * 通讯录
     */
    private final TextWatcher editContactTextChangeListener = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            btnClear.setImageResource(R.drawable.back);
            searchContactList(s.toString().trim());
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
    };

    // 依据输入 查询本地已经添加的联系人
    private void searchContactList(String s) {
        List<Contacts> searchList = new ArrayList<Contacts>();
        if (s == null || s.length() < 1) {
            searchList = contacts;
        }
        if (s != null && s.length() > 0) {
            for (Contacts contactUser : contacts) {
                if (contactUser.getPhoneNum().contains(s)
                        || (contactUser.getUserName() != null && contactUser.getUserName().contains(s))) {
                    searchList.add(contactUser);
                }
            }
        }
        contactsAdapter.setmUserList(searchList);
    }

    // 通讯录右侧功能
    private final OnClickListener contactClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof ImageView) {
                int position = ((Integer) view.getTag()).intValue();
                ActionItem actMessage = new ActionItem(getResources()
                        .getDrawable(R.drawable.sms), "短信", this);
                ActionItem actCall = new ActionItem(getResources().getDrawable(
                        R.drawable.call), "呼叫", this);
                ActionItem actDel = new ActionItem(getResources().getDrawable(
                        R.drawable.remove), "移除", this);
                ActionItem invite = new ActionItem(getResources().getDrawable(
                        R.drawable.addgoods), "邀请", this);
                QuickActionBar qaBar = new QuickActionBar(view, position);
                qaBar.setEnableActionsLayoutAnim(true);
                qaBar.addActionItem(actCall);
                qaBar.addActionItem(actMessage);
                qaBar.addActionItem(actDel);
                qaBar.addActionItem(invite);
                qaBar.show();
            } else if (view instanceof LinearLayout) {
                // ActionItem组件
                LinearLayout actionsLayout = (LinearLayout) view;
                QuickActionBar bar = (QuickActionBar) actionsLayout.getTag();
                bar.dismissQuickActionBar();
                int currentUserPosition = bar.getListItemIndex();
                TextView txtView = (TextView) actionsLayout
                        .findViewById(R.id.qa_actionItem_name);
                String actionName = txtView.getText().toString();
                setValue(currentUserPosition);
                if (actionName.equals("短信")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Uri smsToUri = Uri.parse("smsto:" + contactPhone);
                    Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
                    startActivity(mIntent);
                } else if (actionName.equals("呼叫")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel://" + contactPhone));
                    startActivity(intent);
                } else if (actionName.equals("移除")) {
                    contactDao.deleteContact(application.getUid(), contactId);
                    contacts = contactDao.getAllContacts(application.getUid());
                    contactsAdapter.setmUserList(contacts);
                } else if (actionName.equals("邀请")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Intent intent = new Intent(ContactActivity.this,
                            MsgSendActivity.class);
                    intent.putExtra(FinalUtil.SENDMMS_NUM, contactPhone);
                    intent.putExtra(FinalUtil.SENDMMS_MSG,
                            "我在东方数码城做代理商，挣了不少钱。有好处当然不会忘了你，快来来注册吧。");
                    intent.putExtra(FinalUtil.SENDMMS_ADD, "下载地址:"
                            + AgentConfig.downLoadPath());
                    intent.putExtra(FinalUtil.SENDMMS_TYPE, "2");
                    startActivity(intent);
                }
            }
        }
    };

    // 设置对应需要的值
    public void setValue(int index) {
        // this.index = index;
        if (r1.isChecked()) {
            contactId = String.valueOf(contacts.get(index).getId());
            // contactUId = "";
            contactPhone = contacts.get(index).getPhoneNum();
            // contactName = "";
            return;
        } else if (r3.isChecked()) {
            BuyerInfo info = buyers.get(index);
            contactNote = info.getRemark();
            contactId = info.getId();
            this.index = index;
            contactPhone = info.getPhone();
            query_acount = info.getAccount();
        }
    }

    /**
     * 获取我的买家 当登录者是社区经理时,查看所有,其他则隐藏手机号
     * 
     * @author wubo
     * @createtime 2012-9-5
     */
    public void getBuyer(int buyerf_code) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));// 登录账号
        paras.add(new Parameter("limit", buyerIndex + ",10"));
        serverMana.supportRequest(AgentConfig.getBuyers(), paras, false,
                "加载中,请稍等 ...", buyerf_code);
    }

    /**
     * 修改买家备注
     * 
     * @author wubo
     * @createtime 2012-9-6
     */
    public void updateBuyerNote() {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));
        paras.add(new Parameter("id", contactId));
        paras.add(new Parameter("type", "0"));
        paras.add(new Parameter("remark", contactNote));
        serverMana.supportRequest(AgentConfig.modifyRemark(), paras, true,
                "提交中,请稍等 ...", updateBuyerNote_code);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
            Intent intent) {
        if (requestCode == addContact_code && resultCode == RESULT_OK) {
            List<Contacts> cons = (List<Contacts>) intent
                    .getSerializableExtra(Contacts.LOCALCONTACTS_KEY);
            if (cons != null && cons.size() > 0) {
                contactDao.saveContacts(application.getUid(), cons);
                contacts = contactDao.getAllContacts(application.getUid());
                contactsAdapter.setmUserList(contacts);
            }
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_buyer.isRefreshing()) {
            pl_buyer.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }
        if (caseKey == buyer_code || caseKey == buyerf_code) { // 买家
            ContactResult userResult = JSONUtil.fromJson(supportResponse.toString(),
                    ContactResult.class);
            if (null != userResult) {
                if (caseKey == buyerf_code) {
                    buyers.clear();
                }
                if (userResult.getBuyers() != null) {
                    buyers.addAll(userResult.getBuyers());
                }
                buyerAdapter.notifyDataSetChanged();
                buyerIndex += 1;
                buyerLength = userResult.gettCount();
                if (buyers.size() >= buyerLength) {
                    if (lv_buyer.getFooterViewsCount() > 0) {
                        lv_buyer.removeFooterView(loadMoreBuyer);
                    }
                } else {
                    if (lv_buyer.getFooterViewsCount() == 0) {
                        lv_buyer.addFooterView(loadMoreBuyer);
                    }
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
        } else if (caseKey == updateBuyerNote_code) { // 备注
            buyers.get(index).setRemark(contactNote);
            buyerAdapter.notifyDataSetChanged();
            showToast("修改备注成功");
        } else {
            showToast(R.string.to_server_fail);
        }
    }

    @Override
    protected android.content.DialogInterface.OnClickListener setDialogClickListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contactNote = et_dialog.getText().toString();
                updateBuyerNote();
                if (dialog != null) {
                    // 取消浮层
                    dialog.cancel();
                }
            }
        };
    }

    @Override
    protected Context setCreateContext() {
        return ContactActivity.this;
    }
}
