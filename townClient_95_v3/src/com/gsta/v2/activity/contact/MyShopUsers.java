package com.gsta.v2.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.adapter.UserAdapter;
import com.gsta.v2.activity.waiter.MsgChatActivity;
import com.gsta.v2.entity.ActionItem;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.entity.MyUserInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.ContactResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.ui.QuickActionBar;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 本店用户 相关功能
 * 
 * @author caosq
 */
public class MyShopUsers extends TopBackAndShowDialogActivity implements IUICallBackInterface {

    private PullToRefreshListView pl_user;
    private ListView lv_user;
    private List<MyUserInfo> users = new ArrayList<MyUserInfo>();
    private UserAdapter userAdapter;
    private long userLength = 1;
    private View loadMoreUser;
    private View noDataLayout; // 无数据的提示 只显示两秒

    private String contactId = ""; // 用户的ID
    private String contactUId = "";
    private String contactPhone = ""; // 用户电话
    private String contactNote = "";
    private String contactName;
    // 选择了哪一个
    private int index;
    //
    private int userIndex = 1; // limit使用的开始页码
    private boolean user_load = true; // 是否加载 true 为加载 false 为刷新
    private final int user_code = 3; // 加载标识
    private final int updateUserNote_code = 6; // 修改用户备注

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 1) {
                noDataLayout.setVisibility(View.GONE);
            }
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_pull_to_refresh);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        pl_user = (PullToRefreshListView) findViewById(R.id.lv_users);
        lv_user = pl_user.getRefreshableView();

        loadMoreUser = inflater.inflate(R.layout.loadmore, null);
        loadMoreUser.setVisibility(0);

        noDataLayout = findViewById(R.id.nodata);
        ((TextView) noDataLayout.findViewById(R.id.search)).setText("你还没有用户哦,赶紧去发展用户吧...");
    }

    private void initData() {
        userAdapter = new UserAdapter(this, users, userNoIMClickListener,
                userIMClickListener);
        lv_user.setAdapter(userAdapter);

        pl_user.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                userIndex = 1;
                user_load = false;
                getUser(user_code);
            }
        });

        if (user_load) {
            getUser(user_code);
        }
    }

    private void initListener() {
        loadMoreUser.setOnClickListener(clickListener);
    }

    /**
     * 点击事件
     */
    OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == loadMoreUser) {
                getUser(user_code);
            }
        }
    };

    // listView中的item 非智能机使用
    private final OnClickListener userNoIMClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof ImageView) {
                int position = ((Integer) view.getTag()).intValue();
                ActionItem actMessage = new ActionItem(getResources()
                        .getDrawable(R.drawable.sms), "短信", this);
                ActionItem actCall = new ActionItem(getResources().getDrawable(
                        R.drawable.call), "呼叫", this);
                ActionItem actNote = new ActionItem(getResources().getDrawable(
                        R.drawable.note), "备注", this);
                QuickActionBar qaBar = new QuickActionBar(view, position);
                qaBar.setEnableActionsLayoutAnim(true);
                qaBar.addActionItem(actCall);
                qaBar.addActionItem(actMessage);
                qaBar.addActionItem(actNote);
                qaBar.show();
            } else if (view instanceof LinearLayout) {
                // ActionItem组件
                LinearLayout actionsLayout = (LinearLayout) view;
                QuickActionBar bar = (QuickActionBar) actionsLayout.getTag();
                bar.dismissQuickActionBar();
                int currentUserPosition = bar.getListItemIndex();
                TextView txtView = (TextView) actionsLayout
                        .findViewById(R.id.qa_actionItem_name);
                String actionName = txtView.getText().toString();
                setValue(currentUserPosition);
                if (actionName.equals("短信")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Uri smsToUri = Uri.parse("smsto:" + contactPhone);
                    Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
                    startActivity(mIntent);
                } else if (actionName.equals("呼叫")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel://" + contactPhone));
                    startActivity(intent);
                } else if (actionName.equals("备注")) {
                    Bundle bundle = new Bundle();
                    bundle.putString(KEY_STRING, contactNote);
                    showDialog(remark_code, bundle);
                }
            }
        }
    };

    @Override
    protected Dialog onCreateDialog(final int id) {
        return super.onCreateDialog(id);
    }

    // listView中的item 智能机使用
    private final OnClickListener userIMClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof ImageView) {
                int position = ((Integer) view.getTag()).intValue();
                ActionItem actMessage = new ActionItem(getResources()
                        .getDrawable(R.drawable.sms), "短信", this);
                ActionItem actCall = new ActionItem(getResources().getDrawable(
                        R.drawable.call), "呼叫", this);
                ActionItem actIM = new ActionItem(getResources().getDrawable(
                        R.drawable.im), "私聊", this);
                ActionItem actNote = new ActionItem(getResources().getDrawable(
                        R.drawable.note), "备注", this);
                QuickActionBar qaBar = new QuickActionBar(view, position);
                qaBar.setEnableActionsLayoutAnim(true);
                qaBar.addActionItem(actCall);
                qaBar.addActionItem(actMessage);
                qaBar.addActionItem(actIM);
                qaBar.addActionItem(actNote);
                qaBar.show();
            } else if (view instanceof LinearLayout) {

                // ActionItem组件
                LinearLayout actionsLayout = (LinearLayout) view;
                QuickActionBar bar = (QuickActionBar) actionsLayout.getTag();
                bar.dismissQuickActionBar();
                int currentUserPosition = bar.getListItemIndex();
                setValue(currentUserPosition);
                TextView txtView = (TextView) actionsLayout
                        .findViewById(R.id.qa_actionItem_name);
                String actionName = txtView.getText().toString();
                if (actionName.equals("短信")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    if (!Util.isCellphone(contactPhone)) {
                        showToast("手机号码格式不正确!");
                        return;
                    }
                    Uri smsToUri = Uri.parse("smsto:" + contactPhone);
                    Intent mIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
                    startActivity(mIntent);
                } else if (actionName.equals("呼叫")) {
                    if (contactPhone == null || contactPhone.equals("")) {
                        showToast("该用户手机号码有误,不能与之通信");
                        return;
                    }
                    if (!Util.isCellphone(contactPhone)) {
                        showToast("手机号码格式不正确!");
                        return;
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel://" + contactPhone));
                    startActivity(intent);
                } else if (actionName.equals("私聊")) {
                    String a = users.get(currentUserPosition).getPhoto();
                    if (null == a) {
                        a = "";
                    }
                    toIM(new IMBean(contactUId, contactName,a));
                } else if (actionName.equals("备注")) {
                    showDialog(remark_code);
                }
            }
        }
    };

    /**
     * 发送私信
     * 
     * @author wubo
     * @createtime 2012-9-10
     * @param im
     */
    public void toIM(IMBean im) {
        if (im.getHisUserId() == null || im.getUserName() == null) {
            showToast("不能与该用户私聊");
        } else {
            Intent intent = new Intent();
            intent.putExtra(FinalUtil.IMBEAN, im);
            intent.setClass(MyShopUsers.this, MsgChatActivity.class);
            startActivity(intent);
        }
    }

    public void setValue(int index) {
        contactId = String.valueOf(users.get(index).getId());
        contactUId = users.get(index).getUid();
        contactPhone = users.get(index).getMobile();
        contactNote = "";
        if (users.get(index).getRemark() != null) {
            contactNote = users.get(index).getRemark();
        }
        contactName = users.get(index).getAccount();
        if (users.get(index).getRemark() != null
                && users.get(index).getRemark().length() > 0) {
            if (users.get(index).getUserName() == null) {
                contactName = (users.get(index).getRemark());
            } else {
                contactName = users.get(index).getRemark() + "("
                        + users.get(index).getUserName() + ")";
            }
        } else {
            if (users.get(index).getUserName() == null) {
                contactName = "";
            } else {
                contactName = users.get(index).getUserName();
            }
        }
    }

    /**
     * 获取我的用户
     * 
     * @author wubo
     * @createtime 2012-9-5
     */
    public void getUser(int code) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));// 登录账号
        paras.add(new Parameter("limit", userIndex + ",10"));
        serverMana.supportRequest(AgentConfig.getUsers(), paras, false,
                "加载中,请稍等 ...", code);
    }

    /**
     * 修改用户备注
     * 
     * @author wubo
     * @createtime 2012-9-6
     */
    public void updateUserNote() {
        ServerSupportManager serverMana = new ServerSupportManager(this.getParent(), this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));
        paras.add(new Parameter("id", contactId));
        paras.add(new Parameter("type", "1"));
        paras.add(new Parameter("remark", contactNote));
        serverMana.supportRequest(AgentConfig.modifyRemark(), paras, true,
                "提交中,请稍等 ...", updateUserNote_code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (pl_user.isRefreshing()) {
            pl_user.onRefreshComplete();
        }
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }

        switch (caseKey) {
        case user_code: // 以user_load区别是否刷新
            if (!user_load) {
                users.clear();
            }
            ContactResult userResult = JSONUtil.fromJson(supportResponse.toString(),
                    ContactResult.class);
            if (userResult != null) {
                if (userResult.getUsers() != null) {
                    users.addAll(userResult.getUsers());
                }
                userAdapter.setmUserList(users);
                userIndex += userResult.getCount();
                userLength = userResult.gettCount();
                if (userIndex > userLength) {
                    // loadMoreUser.setVisibility(View.GONE);
                    if (lv_user.getFooterViewsCount() > 0) {
                        lv_user.removeFooterView(loadMoreUser);
                    }
                } else {
                    // loadMoreUser.setVisibility(View.VISIBLE);
                    if (lv_user.getFooterViewsCount() == 0) {
                        lv_user.addFooterView(loadMoreUser);
                    }
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        case updateUserNote_code: // 更新用户备注
            BaseResult baseResult = JSONUtil.fromJson(supportResponse.toString(),
                    BaseResult.class);
            if (baseResult != null) {
                users.get(index).setRemark(contactNote);
                userAdapter.notifyDataSetChanged();
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;

        }
        if (users.size() <= 0) {
            if (noDataLayout.getVisibility() != 0) {
                // showNoDataView();
            }
        }
    }

    @SuppressWarnings("unused")
    private void showNoDataView() {
        noDataLayout.setVisibility(0);
        handler.sendEmptyMessageDelayed(1, 2000);
    }

    @Override
    protected android.content.DialogInterface.OnClickListener setDialogClickListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contactNote = et_dialog.getText().toString();
                updateUserNote();
                if (dialog != null) {
                    // 取消浮层
                    dialog.cancel();
                }
            }
        };
    }

    @Override
    protected Context setCreateContext() {
        return MyShopUsers.this.getParent();
    }
}
