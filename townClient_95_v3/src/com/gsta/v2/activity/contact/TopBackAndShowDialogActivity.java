package com.gsta.v2.activity.contact;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.ui.CustomDialog;
import com.gsta.v2.util.Util;

/**
 * 此abstract类实现主界面返回 及 备注等的DIALOG显示
 * 
 * @author caosq 2013-5-30
 */
public abstract class TopBackAndShowDialogActivity extends TopBackActivity {

    private Logger logger = LoggerFactory.getLogger();
    private int currentDialogId = -1;
    protected static final String KEY_STRING = "key";
    // 备注
    protected static final int remark_code = 0xac1;
    //
    private CustomDialog.Builder builder;
    private View dialogView;
    protected EditText et_dialog;

    @Override
    protected Dialog onCreateDialog(int id) {
        return super.onCreateDialog(id);
    }

    @Override
    protected Dialog onCreateDialog(int id, Bundle args) {
        if (id == remark_code) {
            builder = new CustomDialog.Builder(setCreateContext());
            String a = "";
            if (null != args) {
                a = args.getString(KEY_STRING);
            }
            builder.setTitle("修改备注");
            dialogView = LayoutInflater.from(setCreateContext()).inflate(
                    R.layout.dialog_editview, null);
            et_dialog = (EditText) dialogView.findViewById(R.id.et_dialog);
            et_dialog.setText(null == a ? "" : a);
            Util.setEditCursorToTextEnd(et_dialog);
            builder.setContentView(dialogView);
            builder.setPositiveButton(R.string.submit,setDialogClickListener());
            builder.setNegativeButton(R.string.cancel, null);
            return builder.create();
        }
        return super.onCreateDialog(id, args);
    }

    /**
     * 在父类代码中加入监听 这里只对不是图片的使用有效
     */
    protected abstract Dialog.OnClickListener setDialogClickListener();

    /**
     * 设置产生窗口的ACTIVITY
     */
    protected abstract Context setCreateContext();

    /**
     * 先清除之前的dialog再显示新的dialog
     * 
     * @param id
     */
    protected void mShowDialog(int id) {
        if (currentDialogId != -1) {
            // 移除dialog
            removeDialog(currentDialogId);
        }
        currentDialogId = id;
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        // 判断Android系统SDK的版本，我们一般用currentVersion < android.os.Build.VERSION_CODES.FROYO
        // 的方式进行判断是2.2以下版本
        if (currentVersion < android.os.Build.VERSION_CODES.FROYO) {
            logger.info("手机Android系统SDK 版本小于2.2 == " + currentVersion);
            onCreateDialog(id);
        } else {
            showDialog(id);// 弹出浮层
        }
    }

    /**
     * 先清除之前的dialog再显示新的dialog
     * 
     * @param id
     */
    protected void mShowDialog(int id, Bundle bundle) {
        if (currentDialogId != -1) {
            // 移除dialog
            removeDialog(currentDialogId);
        }
        currentDialogId = id;
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        // 判断Android系统SDK的版本，我们一般用currentVersion < android.os.Build.VERSION_CODES.FROYO
        // 的方式进行判断是2.2以下版本
        if (currentVersion < android.os.Build.VERSION_CODES.FROYO) {
            logger.info("手机Android系统SDK 版本小于2.2 == " + currentVersion);
            onCreateDialog(id, bundle);
        } else {
            showDialog(id, bundle);// 弹出浮层
        }
    }
}
