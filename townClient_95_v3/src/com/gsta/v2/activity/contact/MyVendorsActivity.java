package com.gsta.v2.activity.contact;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.activity.adapter.MyVendorsAdapter;
import com.gsta.v2.activity.location.CommonLocation;
import com.gsta.v2.activity.location.CommonLocation.CommLocationInterface;
import com.gsta.v2.activity.myshop.ShopHomePageActivity;
import com.gsta.v2.entity.SellerInfo;
import com.gsta.v2.response.ContactResult;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 
 * @ClassName: MyShopFans
 * @Description: 我的卖家
 * @author longxianwen
 * @date May 24, 2013 10:26:22 AM
 */
public class MyVendorsActivity extends TopBackActivity implements
        IUICallBackInterface {

    private static final String TAG = Util.getClassName();
    final int load_querycode = 11; // 数据查询标识
    private final int up_code = 12; // 上传标识
    // 页码
    private int pageCount = 0;
    private PullToRefreshListView pl_vendor;
    private ListView lv_vendor;
    private List<SellerInfo> vendorsInfos;
    private MyVendorsAdapter adapter;

    private CommonLocation location; // 定位

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_attention_shop);

        init();
        initView();
        initData();
        initListener();
    }

    private void init() {
        location = CommonLocation.getCommlocation(MyVendorsActivity.this.getParent());
        if (null != location) {
            location.enableMyLocation();
            location.setCommLocationInterface(locaInter);
        }
    }

    CommLocationInterface locaInter = new CommLocationInterface() {
        @Override
        public void getLoation(double geoLat, double geoLng) {
            uploadLocation(geoLat, geoLng);
            adapter.notifyData(geoLat, geoLng);
        }
    };

    private void initView() {
        pl_vendor = (PullToRefreshListView) findViewById(R.id.pl_merchant);
        lv_vendor = pl_vendor.getRefreshableView();

    }

    private void initData() {
        vendorsInfos = new ArrayList<SellerInfo>();
        adapter = new MyVendorsAdapter(MyVendorsActivity.this, vendorsInfos, 0,
                0);
        lv_vendor.setAdapter(adapter);

        getVendorsData();
    }

    private void initListener() {
        lv_vendor.setOnItemClickListener(itemClickListener);
        pl_vendor.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageCount = 0;
                if (vendorsInfos.size() > 0) {
                    vendorsInfos.clear();
                    adapter.notifyDataSetInvalidated();
                }
                getVendorsData();
            }
        });
    }

    OnItemClickListener itemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            SellerInfo sellerInfo = (SellerInfo) parent.getAdapter()
                    .getItem(position);
            goToShop(sellerInfo);
        }

        private void goToShop(SellerInfo sellerInfo) {
            Intent intent = new Intent();
            intent.setClass(MyVendorsActivity.this.getParent(), ShopHomePageActivity.class);
            intent.putExtra(ShopHomePageActivity.IFMYSELF_SHOP_FLAG,
                    sellerInfo.getAgentUid());
            intent.putExtra(ShopHomePageActivity.MYSELF_SHOPNAME_FLAG,
                    sellerInfo.getShopName());
            startActivity(intent);
        }

    };

    /**
     * 从服务器获取我的卖家信息
     */
    private void getVendorsData() {
        pl_vendor.setRefreshing(true);
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("uid", application.getUid()));
        paras.add(new Parameter("limit", pageCount + ",20"));
        serverMana.supportRequest(AgentConfig.getVendors(), paras, false,
                "加载中,请稍等 ...", load_querycode);
    }

    @Override
    protected void onStart() {
        if (null != location) {
            location.setCommLocationInterface(locaInter);
        }
        super.onStart();
    }

    @Override
    protected void onPause() {
        if (null != location) {
            location.disableMyLocation(locaInter);
        }
        super.onPause();
    }

    /**
     * 
     * @author longxianwen
     * @Title: uploadLocation
     * @Description: 上传自己当前位置到后台
     * @param @param geoLat
     * @param @param geoLng 设定文件
     * @return void 返回类型
     */
    private void uploadLocation(Double geoLat, Double geoLng) {
        if (application.getLoginState() != FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            return;
        }
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("userId", application.getUid()));
        paras.add(new Parameter("longitude", geoLng + ""));
        paras.add(new Parameter("latitude", geoLat + ""));
        serverMana.supportRequest(AgentConfig.upLocation(), paras, false,
                "提交中,请稍等 ....", up_code);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        pl_vendor.onRefreshComplete();
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }

        switch (caseKey) {
        case load_querycode: // 我的卖家
            ContactResult cr = JSONUtil.fromJson(supportResponse.toString(),
                    ContactResult.class);
            if (cr != null) {
                if (cr.getSellers() != null && cr.getSellers().size() > 0) {
                    vendorsInfos.addAll(cr.getSellers());
                    pageCount += cr.getSellers().size();
                    adapter.setRsInfos(vendorsInfos);
                } else {
                    showToast("没有为您搜索到更多的卖家!");
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        case up_code: // 上传定位
            ContactResult cr2 = JSONUtil.fromJson(supportResponse.toString(),
                    ContactResult.class);
            if (cr2 != null) {
                Log.d(TAG, "上传成功");
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
        }
    }
}
