package com.gsta.v2.activity.location;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.widget.Toast;

import com.amap.mapapi.location.LocationManagerProxy;

/**
 * 
 * @author longxianwen
 * @createTime Mar 29, 2013 6:26:20 PM
 * @version: 1.0
 * @desc:定位类
 */
public class CommonLocation implements LocationListener {


    private long updateTime = 600000; // 更新时间 60000
    private long minDistance = 10; // 更新距离
    private static CommonLocation commLocation = null;
    private List<CommLocationInterface> locas; // 所有需要定位通知的类注册
    private LocationManagerProxy locationManager = null;
    private double geoLat = 0; // 纬度
    private double geoLng = 0; // 经度
    private Context context;

    public LocationManagerProxy getLocationManager() {
        return locationManager;
    }

    private CommonLocation(Context context) {
        super();
        this.context = context;
        locas = new ArrayList<CommonLocation.CommLocationInterface>();
        locationManager = LocationManagerProxy.getInstance(context);
    }

    public static CommonLocation getCommlocation(Context context) {
        if (commLocation == null) {
            commLocation = new CommonLocation(context);
        }
        return commLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        // 得到定位结果
        if (location != null) {
            geoLat = location.getLatitude();
            geoLng = location.getLongitude();
            String str = ("定位成功:(" + geoLat + "," + geoLng + ")");
            Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
            System.out.println(str);
            if (null != locas && locas.size() > 0) {
                for (int i = 0; i < locas.size(); i++) {
                    locas.get(i).getLoation(geoLat, geoLng);
                }
            } else {
                locationManager.removeUpdates(this);
            }
        }
    }

    public void setCommLocationInterface(CommLocationInterface commLocationInterface) {
        if (!locas.contains(commLocationInterface)) {
            locas.add(commLocationInterface);
        }
    }

    public interface CommLocationInterface {

        public void getLoation(double geoLat, double geoLng);

    };

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * 
     * @author longxianwen
     * @Title: enableMyLocation
     * @Description: 开始定位
     * @return boolean 返回类型
     */
    public boolean enableMyLocation() {
        boolean result = true;
        Criteria cri = new Criteria();
        cri.setAccuracy(Criteria.ACCURACY_COARSE);
        cri.setAltitudeRequired(false);
        cri.setBearingRequired(false);
        cri.setCostAllowed(false);
        String bestProvider = locationManager.getBestProvider(cri, true);
        
//        Location location = locationManager.getLastKnownLocation(bestProvider);
//        if(location != null) {
//        	onLocationChanged(location);
//        }
        locationManager.requestLocationUpdates(bestProvider, updateTime, minDistance, this);
        return result;
    }

    /**
     * @author longxianwen
     * @Title: disableMyLocation
     * @Description: 移除定位更新
     * @return void 返回类型
     */
    public void disableMyLocation(CommLocationInterface commLocationInterface) {
//        if (null != locas && locas.contains(commLocationInterface)) {
//            if (!locas.remove(commLocationInterface)) {
//                logger.warn(commLocationInterface + "定位监听移除失败...");
//                Log.w(TAG, commLocationInterface + " 移除成功...");
//            } else {
//                Log.i(TAG, commLocationInterface + " 移除成功...");
//            }
//        }
//        if (null != locas && locas.size() == 0) {
//            locationManager.removeUpdates(this);
//        }
        locationManager.removeUpdates(this);
    }

}
