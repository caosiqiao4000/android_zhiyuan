package com.gsta.v2.activity.location;

import mobile.json.JSONUtil;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.amap.mapapi.location.LocationManagerProxy;
import com.amap.mapapi.location.LocationProviderProxy;
import com.gsta.v2.util.Util;

/**
 * 定位：定位不与账号挂钩，只和手机有关系,这样可以加快定位速度
 * 
 * @author Administrator
 * 
 */
public class LocateManager implements LocationListener {
	
	static int loc_sumtime = 1000 * 5;// 总的定位时间
	static int mintime = 1 * 1000 * 60 * 10;
	// location change distant
	static float mindistant = 10.0f;

	public static final int LOCATE_USTART = -1;
	public static final int LOCATE_START = 0;
	public static final int LOCATE_SUCCESS = 1;
	public static final int LOCATE_FAILED = 2;
	public static final int LOCATE_LAST = 3;

	private static SharedPreferencesUtil sPreferencesUtil;
	public static final String LOCATE_JSON = "pangkerlocation";
	public static final double LOCATE_LON = 113.404157;
	public static final double LOCATE_LAT = 23.059323;

	private LocationManagerProxy locationManagerProxy = null;
	private LocateListener locateListener;
	private Context context;
	private Criteria cri;
	private static LocateManager locateManager;
	private LocateType locateType = LocateType.cannot;
	boolean _if = true;

	public static synchronized LocateManager getInstance(Context context) {
		if (locateManager == null)
			locateManager = new LocateManager(context);
		return locateManager;
	}

	private LocateManager(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		sPreferencesUtil = new SharedPreferencesUtil(this.context);
		cri = new Criteria();
		cri.setAccuracy(Criteria.ACCURACY_COARSE);
		cri.setAltitudeRequired(false);
		cri.setBearingRequired(false);
		cri.setCostAllowed(false);
	}

	public LocationManagerProxy initLocationManagerProxy() {
		if (locationManagerProxy == null)
			locationManagerProxy = LocationManagerProxy.getInstance(this.context);
		return locationManagerProxy;
	}

	public void locate(LocateListener locateListener) {
		this.locateListener = locateListener;
		enableMyLocation();
	}

	public void locate(LocateListener locateListener, boolean flg) {
		this.locateListener = locateListener;
	}
	
	public void destroy() {
		if (locationManagerProxy == null){
			locationManagerProxy.destory();
		}
		_if = false;
	}

	// 开启
	public boolean enableMyLocation() {
		boolean result = true;
		initLocationManagerProxy();
		locateType = getLocateType();
		if (this.locateType == LocateType.cannot) {
			String strLocation = sPreferencesUtil.getString(LOCATE_JSON, "");
			com.gsta.v2.activity.location.Location loc = JSONUtil.fromJson(
					strLocation, com.gsta.v2.activity.location.Location.class);
			if (loc == null) {
				loc = new com.gsta.v2.activity.location.Location(LOCATE_LON, LOCATE_LAT);
			}
			this.locateListener.onLocationChanged(loc, LOCATE_FAILED, LocateType.cannot);
		} else {
			locationManagerProxy.requestLocationUpdates(locateType.toString(), mintime, mindistant, this);
		}
		return result;
	}

	// 关闭
	public void disableMyLocation() {
		if (locationManagerProxy != null) {
			locationManagerProxy.removeUpdates(this);
			locationManagerProxy.destory();
		}
		locationManagerProxy = null;
	}

	public interface LocateListener {
		public void onLocationChanged(com.gsta.v2.activity.location.Location location, int locateResult, LocateType locateType);
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		com.gsta.v2.activity.location.Location loc;
		if (locateListener == null || !_if)
			return;
		if (location != null) {
			loc = new com.gsta.v2.activity.location.Location(location.getLongitude(), location.getLatitude());
			sPreferencesUtil.saveString(LOCATE_JSON, getJsonString(loc));
			if(context != null){
				locateListener.onLocationChanged(loc, LOCATE_SUCCESS, locateType);
			}
		} else {
			String strLocation = sPreferencesUtil.getString(LOCATE_JSON, "");
			loc = JSONUtil.fromJson(strLocation,
					com.gsta.v2.activity.location.Location.class);
			if (loc == null) {
				loc = new com.gsta.v2.activity.location.Location(LOCATE_LON, LOCATE_LAT);
			}
			locateListener.onLocationChanged(loc, LOCATE_FAILED, this.locateType);
		}
	}

	private String getJsonString(com.gsta.v2.activity.location.Location location) {
		return "{\'timestamp\':" + Util.getNowTime() + ",\'longitude\':"
				+ location.getLongitude() + ",\'latitude\':"
				+ location.getLatitude() + ",\'source\':\'" + "mapabc"
				+ "\',\'accurate\':" + location.getAccurate() + "}";
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}

	public void stopLocate(LocateListener listener) {
		// TODO Auto-generated method stub
	}

	// get locate type 判断是否具有定位能力
	public LocateType getLocateType() {
		initLocationManagerProxy();
		String bestProvider = locationManagerProxy.getBestProvider(cri, true);
		if (LocationManagerProxy.GPS_PROVIDER.equals(bestProvider)) {
			return LocateType.gps;
		}
		if (LocationManagerProxy.NETWORK_PROVIDER.equals(bestProvider)) {
			return LocateType.network;
		}
		if (LocationProviderProxy.MapABCNetwork.equals(bestProvider)) {
			return LocateType.lbs;
		}

		this.locateType = LocateType.cannot;
		return LocateType.cannot;
	}

	public static enum LocateType {
		gps, network, lbs, cell, cannot
	}
}
