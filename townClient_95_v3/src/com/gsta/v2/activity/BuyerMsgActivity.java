package com.gsta.v2.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.gsta.v2.activity.adapter.BuyerMsgAdapter;
import com.gsta.v2.db.IBuyerMsgDao;
import com.gsta.v2.db.impl.BuyerMsgDaoImpl;
import com.gsta.v2.entity.MessageInfo;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.MessageResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.PopupExpressAdapter;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.SmileyParser;
import com.gsta.v2.util.TextCountLimitWatcher;
import com.gsta.v2.util.Util;

/**
 * 买家留言   此类功能未开发
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
public class BuyerMsgActivity extends BaseActivity implements
		IUICallBackInterface {

	TextView title;
	ImageButton back;// 返回
	ImageButton edit;// 编辑
	ListView lv_msg;// 消息列表
	String sentUid;
	String sentName;
	final int query_code = 0;
	final int send_code = 1;
	private SmileyParser parser;

	private EditText sendMsg;// 发送留言内容
	private Button sendMsgButton;// 发送留言
	private PopupWindow popupWindow;
	private ImageButton btnExpression;
	private GridView mExpresssion;
	private LinearLayout mExpressPopupShow;

	List<MessageInfo> infos = new ArrayList<MessageInfo>();
	BuyerMsgAdapter buyerMsgAdapter;
	IBuyerMsgDao buyerMsgDao;

	View loadMoreMsg;
	Button btn_loadMoreMsg;
	String msg = "";

	Builder builder;

	private DisplayMetrics dm = new DisplayMetrics();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindowManager().getDefaultDisplay().getMetrics(dm);// dm用于获取手机屏幕大小
		// 防止输入法在启动Activity时自动弹出
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.buyermsg);
		initView();
		initData();
		initListener();
	}

	private void initView() {
		// TODO Auto-generated method stub
		back = (ImageButton) findViewById(R.id.back);
		edit = (ImageButton) findViewById(R.id.edit);
		lv_msg = (ListView) findViewById(R.id.msg_list);
		title = (TextView) findViewById(R.id.title);

		sendMsgButton = (Button) findViewById(R.id.sendmsg);
		btnExpression = (ImageButton) findViewById(R.id.last);
		sendMsg = (EditText) findViewById(R.id.message);
		mExpressPopupShow = (LinearLayout) findViewById(R.id.express_popup_parent);
		// 隐藏图片选择器
		mExpressPopupShow.setVisibility(View.GONE);
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		loadMoreMsg = inflater.inflate(R.layout.loadmore, null);
		btn_loadMoreMsg = (Button) loadMoreMsg
				.findViewById(R.id.loadMoreButton);
		btn_loadMoreMsg.setText("加载留言历史记录");
		// 添加加载更多的按钮
		lv_msg.addFooterView(loadMoreMsg);
	}

	private void initData() {
		parser = SmileyParser.getInstance(this);// 表情转化辅助工具
		buyerMsgDao = new BuyerMsgDaoImpl(BuyerMsgActivity.this);
		sentUid = getIntent().getExtras().getString(FinalUtil.SENTUID);
		sentName = getIntent().getExtras().getString(FinalUtil.SENTNAME);
		title.setText(sentName + "的留言");
		buyerMsgAdapter = new BuyerMsgAdapter(BuyerMsgActivity.this, infos);
		lv_msg.setAdapter(buyerMsgAdapter);
		// 输入框监听事件
		sendMsg.addTextChangedListener(new TextCountLimitWatcher(140, sendMsg));
		initPopupWindow();
		loadData();
	}

	private void initListener() {
		// TODO Auto-generated method stub
		back.setOnClickListener(clickListener);
		edit.setOnClickListener(clickListener);
		btn_loadMoreMsg.setOnClickListener(clickListener);
		btnExpression.setOnClickListener(clickListener);
		sendMsgButton.setOnClickListener(clickListener);
		lv_msg.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final int index = position;
				builder = new AlertDialog.Builder(BuyerMsgActivity.this);
				builder.setTitle("选择操作");
				builder.setItems(new String[] { "复制消息", "删除消息", "清空记录" },
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// 0复制消息 1删除消息 2清空记录
								switch (which) {
								case 0:
									Util.copyText(BuyerMsgActivity.this, infos
											.get(index).getContent());
									break;
								case 1:
									buyerMsgDao.deleteById(Long.parseLong(infos
											.get(index).getId()));
									infos = buyerMsgDao.getBuyerMsgs(
											application.getUid(), sentUid);
									if (infos != null && infos.size() > 0) {
										buyerMsgAdapter.setData(infos);
									} else {
										infos = new ArrayList<MessageInfo>();
										buyerMsgAdapter.setData(infos);
										showToast("没有历史留言信息!");
									}
									break;
								case 2:
									buyerMsgDao.deleteAll(application.getUid(),
											sentUid);
									infos = buyerMsgDao.getBuyerMsgs(
											application.getUid(), sentUid);
									if (infos != null && infos.size() > 0) {
										buyerMsgAdapter.setData(infos);
									} else {
										infos = new ArrayList<MessageInfo>();
										buyerMsgAdapter.setData(infos);
										showToast("没有历史留言信息!");
									}
									break;
								}
							}
						}).create().show();
			}
		});
	}

	/**
	 * 点击事件
	 */
	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v == back) {// 返回
				result();
			} else if (v == btn_loadMoreMsg) {// 加载历史消息
				infos = buyerMsgDao.getBuyerMsgs(application.getUid(), sentUid);
				if (infos != null && infos.size() > 0) {
					buyerMsgAdapter.setData(infos);
				} else {
					infos = new ArrayList<MessageInfo>();
					buyerMsgAdapter.setData(infos);
					showToast("没有历史留言信息!");
				}
				btn_loadMoreMsg.setVisibility(View.GONE);
			} else if (v == edit) {// 编辑留言
				mExpressPopupShow.setVisibility(View.VISIBLE);
			} else if (v == btnExpression) {// 图片选择
				if (!popupWindow.isShowing()) {
					int y = mExpressPopupShow.getHeight();
					popupWindow.showAtLocation(btnExpression, Gravity.BOTTOM,
							0, y);
				} else {
					popupWindow.dismiss();
				}
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
						.hideSoftInputFromWindow(BuyerMsgActivity.this
								.getCurrentFocus().getWindowToken(),
								InputMethodManager.HIDE_NOT_ALWAYS);
			} else if (v == sendMsgButton) {// 发送留言
				mExpressPopupShow.setVisibility(View.GONE);
				msg = sendMsg.getText().toString();
				if (msg.trim().length() > 0) {
					sendMsg.setText("");
					sendMsg();
				}
			}
		}
	};

	/**
	 * 返回调用的activity
	 * 
	 * @author wubo
	 * @time 2013-3-14
	 * 
	 */
	public void result() {
		BuyerMsgActivity.this.setResult(RESULT_OK);
		BuyerMsgActivity.this.finish();
	}

	/**
	 * 私聊表情选择 void
	 */
	private void initPopupWindow() {
		// TODO Auto-generated method stub
		View popView = LayoutInflater.from(this).inflate(
				R.layout.grid_express_popup, null);
		popupWindow = new PopupWindow(popView,
				LinearLayout.LayoutParams.FILL_PARENT, dm.heightPixels / 3,
				false);
		mExpresssion = (GridView) popView.findViewById(R.id.gv_expression);
		PopupExpressAdapter adapter = new PopupExpressAdapter(this);
		mExpresssion.setAdapter(adapter);
		popupWindow.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.popup_bg_white));
		popupWindow.setAnimationStyle(R.style.popupWindowBottomCenter);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.update();
		mExpresssion.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				String expressStr = getResources().getStringArray(
						R.array.express_item_texts)[position];
				String oldStr = sendMsg.getText().toString();
				sendMsg.setText(parser.addSmileySpans(oldStr + expressStr));
				popupWindow.dismiss();
			}
		});

	}

	/**
	 * 发表留言
	 * 
	 * @author wubo
	 * @time 2013-3-14
	 * 
	 */
	public void sendMsg() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("sentUid", application.getUid()));// 登录账号
		paras.add(new Parameter("receiveUid", sentUid));
		paras.add(new Parameter("content", msg));
		serverMana.supportRequest(AgentConfig.sendMessage(), paras, true,
				"发表中,请稍等 ...", send_code);
	}

	/**
	 * 加载未读留言
	 * 
	 * @author wubo
	 * @time 2013-3-14
	 * 
	 */
	public void loadData() {
		ServerSupportManager serverMana = new ServerSupportManager(this, this);
		List<Parameter> paras = new ArrayList<Parameter>();
		paras.add(new Parameter("uid", application.getUid()));// 登录账号
		paras.add(new Parameter("sentUid", sentUid));
		serverMana.supportRequest(AgentConfig.queryMessage(), paras, true,
				"加载中,请稍等 ...", query_code);
	}

	@Override
	public void uiCallBack(Object supportResponse, int caseKey) {
		// TODO Auto-generated method stub
		if (!HttpResponseStatus(supportResponse))
			return;

		switch (caseKey) {
		case query_code:
			MessageResult baseResult = JSONUtil.fromJson(supportResponse
					.toString(), MessageResult.class);
			if (baseResult != null) {
				if (baseResult.getErrorCode() == BaseResult.SUCCESS) {
					if (baseResult.getMessage() != null) {
						infos = baseResult.getMessage();
						buyerMsgDao.saveBuyerMsgs(application.getUid(), infos);
					} else {
						infos = new ArrayList<MessageInfo>();
					}
					buyerMsgAdapter.setData(infos);

				} else if (baseResult.getErrorCode() == BaseResult.FAIL) {
					showToast(baseResult.errorMessage);
				} else {
					showToast(R.string.to_server_fail);
					return;
				}
			} else {
				showToast(R.string.to_server_fail);
				return;
			}
			break;
		case send_code:
			BaseResult result = JSONUtil.fromJson(supportResponse.toString(),
					BaseResult.class);
			if (result != null) {
				if (result.getErrorCode() == BaseResult.SUCCESS) {
					showToast(result.errorMessage);
					MessageInfo info = new MessageInfo();
					info.setContent(msg);
					info.setSentTime(Util.getSysNowTime());
					info.setSentUid(sentUid);
					info.setDir("0");
					buyerMsgDao.saveBuyerMsg(application.getUid(), info);
					List<MessageInfo> lists = new ArrayList<MessageInfo>();
					lists.add(info);
					lists.addAll(infos);
					infos = lists;
					buyerMsgAdapter.setData(infos);
				} else if (result.getErrorCode() == BaseResult.FAIL) {
					showToast(result.errorMessage);
				} else {
					showToast(R.string.to_server_fail);
				}
			} else {
				showToast(R.string.to_server_fail);
				return;
			}
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		/**
		 * 点击返回按钮
		 * 
		 * @author wubo
		 * @time 2013-3-14
		 * @param keyCode
		 * @param event
		 * @return
		 * 
		 */
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			result();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}
}
