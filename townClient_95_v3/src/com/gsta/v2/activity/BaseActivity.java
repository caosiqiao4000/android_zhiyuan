package com.gsta.v2.activity;

import java.util.HashMap;
import java.util.Map;

import mobile.http.HttpReqCode;
import mobile.json.JSONUtil;
import android.app.ActivityGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.service.SendService;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.util.AgentApplication;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.Util;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * 使用的程序的最高父类,包含一些常用的方法和常量
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
public abstract class BaseActivity extends ActivityGroup {
	private static final Logger logger = LoggerFactory
			.getLogger(BaseActivity.class);
	protected AgentApplication application;
	private NotificationManager manager;
	private final int timeScroll = 300;
	private static final String TAG = Util.getClassName();
	public static final String IFNEED_TITLE = "need_title";// 是否需要标题
	protected ImageLoader imageLoader = ImageLoader.getInstance();//图片管理器
	/**
	 * 当需要与其他动画一直使用时 监听
	 */
	protected Animation inFromRight, inFromLeft, outToRight, outToLeft;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (null == application) {
			application = (AgentApplication) getApplicationContext();
		}
		if (!application.getActivities().contains(this)) {
			application.getActivities().add(this);
		}
		if (getIntent().getFlags() == 335544320) {
			getIntent().setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		}
		inFromLeft = inFromLeftAnimation();
		inFromRight = inFromRightAnimation();
		outToLeft = outToLeftAnimation();
		outToRight = outToRightAnimation();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_HOME
				|| keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
			goToNotificationIntent();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	/**
	 * 将程序放置到通知栏
	 */
	protected void goToNotificationIntent() {
		Intent intent = new Intent(this, SendService.class);
		intent.putExtra(FinalUtil.SENDSERVICE_FLAG, FinalUtil.sendMin);
		startService(intent);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (null == manager) {
			manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		}
		if (application.isNotice_flag()) {
			manager.cancel(0);
			application.setNotice_flag(false);
			logger.info(TAG + " onResume 取消挂载通知栏 " + Util.getSysNowTime());
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (null != application && application.getActivities().contains(this)) {
			application.getActivities().remove(this);
		}
	}

	/**
	 * 子View管理
	 */
	private Map<String, View> childViews = new HashMap<String, View>();

	// private String currentTag;

	/**
	 * 
	 * 加载子Activity 在一个VIEW里加载ACTIVITY 这个方法可以多个ACTIVITY替换
	 * 
	 * @param tag
	 * @param intent
	 */
	protected void startActivity(String tag, Intent intent, ViewGroup container) {
		View originView = childViews.get(tag);
		final Window window = getLocalActivityManager().startActivity(tag,
				intent);
		final View decorView = window.getDecorView();
		if (decorView != originView && originView != null) {
			if (originView.getParent() != null)
				((ViewGroup) originView.getParent()).removeView(originView);
		}
		childViews.put(tag, decorView);
		if (decorView != null) {
			decorView.setVisibility(View.VISIBLE);
			decorView.setFocusableInTouchMode(true);
			((ViewGroup) decorView)
					.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
			if (decorView.getParent() == null) {
				container.addView(decorView, new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.FILL_PARENT));
			}
			decorView.requestFocus();
		}
	}

	/**
	 * 此方法可以指定跳转到某个页面
	 */
	protected int mCurrentViewFlipIndex = 0; // 当前ViewFlipper显示LAYOUT的位置

	protected void switchLayoutStateTo(ViewFlipper myViewFlip, int switchTo) {

		while (mCurrentViewFlipIndex != switchTo) {
			if (mCurrentViewFlipIndex > switchTo) {
				mCurrentViewFlipIndex--;
				myViewFlip.setInAnimation(inFromLeft);
				myViewFlip.setOutAnimation(outToRight);
				myViewFlip.showPrevious();
			} else {
				mCurrentViewFlipIndex++;
				myViewFlip.setInAnimation(inFromRight);
				myViewFlip.setOutAnimation(outToLeft);
				myViewFlip.showNext();
			}
		}
	}

	/**
	 * 定义从右侧进入的动画效果
	 * 
	 * @return
	 */
	protected Animation inFromRightAnimation() {
		Animation inFromRight = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, +1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromRight.setDuration(timeScroll);
		inFromRight.setInterpolator(new AccelerateInterpolator());
		return inFromRight;
	}

	/**
	 * 定义从左侧退出的动画效果
	 * 
	 * @return
	 */
	protected Animation outToLeftAnimation() {
		Animation outtoLeft = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		outtoLeft.setDuration(timeScroll);
		outtoLeft.setInterpolator(new AccelerateInterpolator());
		return outtoLeft;
	}

	/**
	 * 定义从左侧进入的动画效果
	 * 
	 * @return
	 */
	protected Animation inFromLeftAnimation() {
		Animation inFromLeft = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromLeft.setDuration(timeScroll);
		inFromLeft.setInterpolator(new AccelerateInterpolator());
		return inFromLeft;
	}

	/**
	 * 定义从右侧退出时的动画效果
	 * 
	 * @return
	 */
	protected Animation outToRightAnimation() {
		Animation outtoRight = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, +1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		outtoRight.setDuration(timeScroll);
		outtoRight.setInterpolator(new AccelerateInterpolator());
		return outtoRight;
	}

	/**
	 * 根据字符串显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final String toastText) {
		Toast.makeText(BaseActivity.this, toastText, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 根据Id显示提示
	 * 
	 * @param toastText
	 */
	public void showToast(final int toastText) {
		Toast.makeText(BaseActivity.this, toastText, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 检查网络
	 * 
	 * @author wubo
	 * @time 2013-3-14
	 * @param supportResponse
	 * @return
	 * 
	 */
	public boolean HttpResponseStatus(Object supportResponse) {
		final String reString = supportResponse.toString();
		if (supportResponse == null || reString.trim().length() == 0) {
			showToast(R.string.to_server_fail);
			return false;
		} else {
			if (supportResponse instanceof HttpReqCode) {
				HttpReqCode httpReqCode = (HttpReqCode) supportResponse;
				if (httpReqCode.equals(HttpReqCode.no_network)) {
					showToast("请检查您的网络！");
				} else if (httpReqCode.equals(HttpReqCode.error)) {
					showToast(R.string.to_server_fail);
				}
				logger.warn(reString);
				return false;
			} else {
				// 防止http XML
				if (reString.contains("</head><body>")) {
					return false;
				}
				// 其他错误情况
				BaseResult result = JSONUtil.fromJson(reString,
						BaseResult.class);
				if (null == result) {
					logger.error("result  is  null");
					showToast(R.string.to_server_error);
					return false;
				}
				if (result.getErrorCode() == BaseResult.UNDEAL) {
					logger.warn(reString);
					return true;
				}
				if (result.getErrorCode() <= BaseResult.FAIL
						|| result.getErrorCode() > BaseResult.SUCCESS) {
					logger.warn(reString);
					if (result.getErrorCode() == BaseResult.FAIL) {
						showToast(null == result.getErrorMessage() ? "提交失败,系统未处理"
								: result.getErrorMessage());
					} else {
						showToast(R.string.to_server_error);
					}
					return false;
				}
			}
		}
		Log.i(TAG, "supportResponse= " + reString);
		return true;
	}

	/**
	 * 取得资源文件的message
	 * 
	 * @param id
	 * @return
	 */
	public String getResourcesMessage(int id) {
		return getResources().getString(id);
	}

}
