package com.gsta.v2.activity.goods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.http.HttpReqCode;
import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.AgentWebViewActivity;
import com.gsta.v2.activity.MainActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.TopBackActivity;
import com.gsta.v2.activity.adapter.GoodsListAdapter;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.response.GoodsListResult;
import com.gsta.v2.ui.DiaglogSearchGoods;
import com.gsta.v2.ui.DiaglogSearchGoods.DialogSearchConfig;
import com.gsta.v2.ui.PullToRefreshBase.OnRefreshListener;
import com.gsta.v2.ui.PullToRefreshListView;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;
import com.gsta.v2.util.WebViewDialog;

/**
 * 热销商品
 * 
 * @author wubo
 * @time 2013-3-14
 * 
 */
@SuppressLint("UseSparseArrays")
public class HotSalesActivity extends TopBackActivity implements
        IUICallBackInterface {
    private static final Logger logger = LoggerFactory.getLogger(HotSalesActivity.class);
    // 菜单弹窗按钮
    private Button ib_menu;
    
    final int query2_code = 2;
    final int query3_code = 3;
    final int query4_code = 4;
    final int query5_code = 5;

    final int flush2_code = 12;
    final int flush3_code = 13;
    final int flush4_code = 14;
    final int flush5_code = 15;
    // 上面的按钮放置控件
    private HorizontalScrollView hs1;
    private RadioGroup rg1;
    private RadioButton r1;
    private RadioButton r2;
    private RadioButton r3;
    private RadioButton r4;
    private RadioButton r5;

    private ViewFlipper flipper;
    // 一
    private GoodsListAdapter goodsAdapter1;
    private PullToRefreshListView pl_1; // 智能机
    private ListView lv_1;
    private ArrayList<GoodsInfo> goods1;
    private View loadMoveV1;// 加载更多
    int total1 = 0; // 第一个选项卡有多少页
    int count1 = 1; // 第一个选项卡加载哪一页
    private ConfigutionUtil conf_1;

    // 二
    private GoodsListAdapter goodsAdapter2; // 手机卡
    private PullToRefreshListView pl_2;
    private ListView lv_2;
    private ArrayList<GoodsInfo> goods2;
    private View loadMoreV2;
    private int total2 = 0;
    private int count2 = 1;

    // 三
    private GoodsListAdapter goodsAdapter3; // 宽带
    private PullToRefreshListView pl_3;
    private ListView lv_3;
    private ArrayList<GoodsInfo> goods3;
    private View loadMoreV3;
    private int total3 = 0;
    private int count3 = 1;

    // 四
    private GoodsListAdapter goodsAdapter4; // 手机加宽带
    private PullToRefreshListView pl_4;
    private ListView lv_4;
    private ArrayList<GoodsInfo> goods4;
    private View loadMoreV4;
    private int total4 = 0;
    private int count4 = 1;

    // 五
    private WebView webview; // 充值
    private WebSettings settings;
    private ProgressBar pb;
    private String loadurl;

    /**
     * 查询条件
     */
    private String type = "0";// 商品类别 0,合约机;1,光宽带;2,手机卡;3,礼包; 默认为 0 热销是999
    private String provinceId = "33";// 省份 默认为空
    @SuppressWarnings("unused")
    private String cityId = "20574";// 地市 默认为空
    private String minPrice = "0";// 最小价格 默认为0
    private String maxPrice = "0";// 最大价格 默认为0
    private String keywords = "";// 商品关键词 默认为空
    private int pageIndex = 1;// 当前页数 默认为1
    private int pageSize = 10;// 分页大小，默认为9
    // 00：上架新品降序 01：上架新品升序
    // 10：销售量降序 11：销售量升序
    // 20：价格降序 21：价格升序
    private String orderBy = "00";// 排序字段，默认为00

    private DiaglogSearchGoods diagloSeach; // 显示查询对话框
    private DialogSearchConfig config; // 要使用的参数
    private Drawable search_drawable; // 右边 放大按钮
    private Drawable refreDrawable;// 右边 刷新按钮

    private Map<Integer, DialogSearchConfig> configMap; // 查询参数保存类

    private TextView tv_title;
    private boolean[] needLoad = { false, true, true, true, true, true };// 下标0 动画结束为true
    private View noData11, noData22, noData33, noData44;

    private Handler handler = new Handler() {
        @SuppressLint("SetJavaScriptEnabled")
        public void handleMessage(android.os.Message msg) {
            if (4 == msg.what) {// 隐藏无数据提示
                View vi = (View) msg.obj;
                if (null != vi) {
                    vi.setVisibility(View.GONE);
                }
            }
            switch (msg.what) {
            case 1: // 加载充值网页页面
                CookieSyncManager.createInstance(HotSalesActivity.this);
                CookieManager.getInstance().removeSessionCookie();
                CookieSyncManager.getInstance().startSync();
                webview.clearCache(true);
                webview.clearHistory();
                webview.setWebChromeClient(new WebViewDialog(HotSalesActivity.this, pb));
                List<Parameter> paras = new ArrayList<Parameter>();
                // String time = String.valueOf(new Date().getTime());

                String a = application.getUid();
                if (a.equals("null")) {
                    a = "";
                }
                paras.add(new Parameter("account", (null != application.getAccount() ? application.getAccount() : "")));
                paras.add(new Parameter("loginUid", a));
                paras.add(new Parameter("agentUid", a));
                // paras.add(new Parameter("systime", time));
                String data = Util.getURLDataByParams(paras);
                loadurl = AgentConfig.toClientRecharge() + "?" + data;
                logger.info(" 充值 loadurl = " + loadurl);
                webview.loadUrl(loadurl);
                webview.clearHistory();
                webview.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        logger.info(" 充值 loadurl = " + url);
                        view.loadUrl(url);
                        return true;
                    }

                    @Override
                    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                        handler.proceed(); // 允许所有证书
                    }
                });
                break;
            case 2: // webView初始化
                pb = (ProgressBar) findViewById(R.id.pb);
                webview = (WebView) findViewById(R.id.webview);
                //
                settings = webview.getSettings();
                // settings.setBuiltInZoomControls(true);
                settings.setBuiltInZoomControls(false);
                settings.setJavaScriptEnabled(true);
                break;
            default:
                break;
            }
        };
    };

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (null != savedInstanceState) {
            restoreState(savedInstanceState);
        }
    };

    // KILL后恢复数据
    private void restoreState(Bundle savedInstanceState) {
        int a = savedInstanceState.getInt(MainActivity.KEY_RADIO_INDEX);
        findViewById(a).performClick();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.hotsales);
        initView();
        initDate();
        initListener();
        if (null != savedInstanceState) {
            restoreState(savedInstanceState);
        }
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_title.setText("商品分类目录");

        hs1 = (HorizontalScrollView) findViewById(R.id.tab_view);
        rg1 = (RadioGroup) findViewById(R.id.rg1);
        r1 = (RadioButton) findViewById(R.id.r1);
        r2 = (RadioButton) findViewById(R.id.r2);
        r3 = (RadioButton) findViewById(R.id.r3);
        r4 = (RadioButton) findViewById(R.id.r4);
        r5 = (RadioButton) findViewById(R.id.r5);

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        flipper = (ViewFlipper) findViewById(R.id.myViewFlipper1);

        pl_1 = (PullToRefreshListView) findViewById(R.id.lv_1);
        lv_1 = pl_1.getRefreshableView();
        loadMoveV1 = inflater.inflate(R.layout.loadmore, null);
        Button btn_load1 = (Button) loadMoveV1.findViewById(R.id.loadMoreButton);
        btn_load1.setVisibility(0);
        btn_load1.setOnClickListener(btn_clickList);

        pl_2 = (PullToRefreshListView) findViewById(R.id.lv_2);
        lv_2 = pl_2.getRefreshableView();
        loadMoreV2 = inflater.inflate(R.layout.loadmore, null);
        Button btn_load2 = (Button) loadMoreV2.findViewById(R.id.loadMoreButton);
        btn_load2.setVisibility(0);
        btn_load2.setOnClickListener(btn_clickList);

        pl_3 = (PullToRefreshListView) findViewById(R.id.lv_3);
        lv_3 = pl_3.getRefreshableView();
        loadMoreV3 = inflater.inflate(R.layout.loadmore, null);
        Button btn_load3 = (Button) loadMoreV3.findViewById(R.id.loadMoreButton);
        btn_load3.setVisibility(0);
        btn_load3.setOnClickListener(btn_clickList);

        pl_4 = (PullToRefreshListView) findViewById(R.id.lv_4);
        lv_4 = pl_4.getRefreshableView();
        loadMoreV4 = inflater.inflate(R.layout.loadmore, null);
        Button btn_load4 = (Button) loadMoreV4.findViewById(R.id.loadMoreButton);
        btn_load4.setVisibility(0);
        btn_load4.setOnClickListener(btn_clickList);

        noData11 = findViewById(R.id.nodata_11);
        ((TextView) noData11.findViewById(R.id.search)).setText("没查询到相关商品,请下拉刷新试试");
        noData22 = findViewById(R.id.nodata_22);
        ((TextView) noData22.findViewById(R.id.search)).setText("没查询到相关商品,请下拉刷新试试");
        noData33 = findViewById(R.id.nodata_33);
        ((TextView) noData33.findViewById(R.id.search)).setText("没查询到相关商品,请下拉刷新试试");
        noData44 = findViewById(R.id.nodata_44);
        ((TextView) noData44.findViewById(R.id.search)).setText("没查询到相关商品,请下拉刷新试试");

        search_drawable = getResources().getDrawable(R.drawable.search_imagev_bg);
        refreDrawable = getResources().getDrawable(R.drawable.hot_sales_refre);

        ib_menu = (Button) findViewById(R.id.mytitile_btn_right);
        ib_menu.setVisibility(0);
        ib_menu.setBackgroundDrawable(search_drawable);

        handler.sendEmptyMessage(2);
        handler.sendEmptyMessage(1);
    }

    private void initDate() {
        goods1 = new ArrayList<GoodsInfo>();
        goodsAdapter1 = new GoodsListAdapter(this, goods1);

        goods2 = new ArrayList<GoodsInfo>();
        goodsAdapter2 = new GoodsListAdapter(this, goods2);

        goods3 = new ArrayList<GoodsInfo>();
        goodsAdapter3 = new GoodsListAdapter(this, goods3);

        goods4 = new ArrayList<GoodsInfo>();
        goodsAdapter4 = new GoodsListAdapter(this, goods4);

        lv_1.setAdapter(goodsAdapter1);
        lv_2.setAdapter(goodsAdapter2);
        lv_3.setAdapter(goodsAdapter3);
        lv_4.setAdapter(goodsAdapter4);

        conf_1 = new ConfigutionUtil();
        conf_1.setQueryCode(1);
        conf_1.setRefresh(true);
        // 搜索条件配置类
        diagloSeach = new DiaglogSearchGoods(HotSalesActivity.this);
        configMap = new HashMap<Integer, DiaglogSearchGoods.DialogSearchConfig>();
        DialogSearchConfig config1 = diagloSeach.new DialogSearchConfig();
        config1.setMaxPrice("8000");
        DialogSearchConfig config2 = diagloSeach.new DialogSearchConfig();
        config2.setMaxPrice("1000");
        DialogSearchConfig config3 = diagloSeach.new DialogSearchConfig();
        config3.setMaxPrice("5000");
        DialogSearchConfig config4 = diagloSeach.new DialogSearchConfig();
        config4.setMaxPrice("8000");
        configMap.put(r1.getId(), config1);
        configMap.put(r2.getId(), config2);
        configMap.put(r3.getId(), config3);
        configMap.put(r4.getId(), config4);
        config = configMap.get(r1.getId());

        // 查询产品
        productQuery(conf_1.getQueryCode(), pl_1, false);
        if (needLoad[1]) {
            needLoad[1] = false;
        }
        rg1.check(r1.getId());
        // handler.sendEmptyMessage(2);
    }

    private void initListener() {
        inFromRight.setAnimationListener(animListener);
        inFromLeft.setAnimationListener(animListener);
        rg1.setOnCheckedChangeListener(checkedChangeListener);
        ib_menu.setOnClickListener(new OnClickListener() { // 搜索
            @Override
            public void onClick(View v) {
                if (r5.isChecked()) {
                    webview.loadUrl(loadurl);
                    return;
                }
                String titleNmae = null;
                if (r1.isChecked()) {
                    titleNmae = "智能机";
                    config = configMap.get(r1.getId());
                } else if (r2.isChecked()) {
                    titleNmae = "手机卡";
                    config = configMap.get(r2.getId());
                } else if (r3.isChecked()) {
                    titleNmae = getResources().getString(R.string.shop_goods_broadband);
                    config = configMap.get(r3.getId());
                } else if (r4.isChecked()) {
                    titleNmae = "手机+宽带";
                    config = configMap.get(r4.getId());
                }
                diagloSeach.showSeachDialog(titleNmae, config, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                            int which) {
                        final DialogSearchConfig oldConfig = diagloSeach.getConfig();

                        provinceId = diagloSeach.getProvinceId();// 省份 默认为空
                        cityId = diagloSeach.getCityId();// 地市 默认为空
                        minPrice = oldConfig.getMinPrice();// 最小价格
                        // 默认为0
                        if (minPrice.trim().length() < 1) {
                            minPrice = "0";
                            // et_min.setText("0");
                        }
                        config.setMinPrice(minPrice);
                        maxPrice = oldConfig.getMaxPrice();// 最大价格
                        // 默认为0
                        if (maxPrice.trim().length() < 1) {
                            maxPrice = "0";
                            // et_max.setText("0");
                        }
                        config.setMaxPrice(maxPrice);
                        if (Integer.valueOf(minPrice) > Integer.valueOf(maxPrice)) {
                            showToast("左边的价格必须小于右边的价格,相等为查询全部价格");
                        }
                        keywords = oldConfig.getKeywords();// 商品关键词
                        config.setKeywords(keywords);

                        // 默认为空
                        orderBy = oldConfig.getOrderBy();
                        config.setOrderBy(orderBy);
                        config.setCho4(oldConfig.getCho4());
                        // TODO Auto-generated method stub
                        // 默认为0
                        if (r1.isChecked()) {
                            count1 = 1;
                            pageIndex = count1;
                            conf_1.setRefresh(true);
                            productQuery(conf_1.getQueryCode(), null, false);
                        } else if (r2.isChecked()) {
                            count2 = 1;
                            pageIndex = count2;
                            productQuery(flush2_code, null, false);
                        } else if (r3.isChecked()) {
                            count3 = 1;
                            pageIndex = count3;
                            productQuery(flush3_code, null, false);
                        } else if (r4.isChecked()) {
                            count4 = 1;
                            pageIndex = count4;
                            productQuery(flush4_code, null, false);
                        }
                        dialog.dismiss();
                    }

                });
            }
        });

        lv_1.setOnItemClickListener(itemClickListener);
        lv_2.setOnItemClickListener(itemClickListener);
        lv_3.setOnItemClickListener(itemClickListener);
        lv_4.setOnItemClickListener(itemClickListener);

        // 下拉刷新监听
        pl_1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = "0";
                count1 = 1;
                pageIndex = 1;
                conf_1.setRefresh(true);
                productQuery(conf_1.getQueryCode(), pl_1, false);
            }
        });

        pl_2.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = "2";
                count2 = 1;
                pageIndex = count2;
                productQuery(flush2_code, pl_2, false);
            }
        });

        pl_3.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = "1";
                count3 = 1;
                pageIndex = count3;
                productQuery(flush3_code, pl_3, false);
            }
        });

        pl_4.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                type = "3";
                count4 = 1;
                pageIndex = count4;
                productQuery(flush4_code, pl_4, false);
            }
        });
    }

    OnClickListener btn_clickList = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.loadMoreButton) {
                if (r1.isChecked()) {
                    pageIndex = count1;// 当前页数 默认为1
                    // productQuery(query1_code, false);
                    conf_1.setRefresh(false);
                    productQuery(conf_1.getQueryCode(), null, true);
                } else if (r2.isChecked()) {
                    pageIndex = count2;// 当前页数 默认为1
                    productQuery(query2_code, null, true);
                } else if (r3.isChecked()) {
                    pageIndex = count3;// 当前页数 默认为1
                    productQuery(query3_code, null, true);
                } else if (r4.isChecked()) {
                    pageIndex = count4;// 当前页数 默认为1
                    productQuery(query4_code, null, true);
                }
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
    };

    @Override
    protected void onResume() {
        super.onResume();
    };

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    };

    // 在滑动动画结束后
    AnimationListener animListener = new AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            needLoad[0] = false;
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if (r1.isChecked()) {
                config = configMap.get(r1.getId());
                if (needLoad[1]) {
                    needLoad[1] = false;
                    count1 = 1;
                    pageIndex = count1;
                    conf_1.setRefresh(true);
                    productQuery(conf_1.getQueryCode(), pl_1, false);
                }
            } else if (r2.isChecked()) {
                config = configMap.get(r2.getId());
                if (needLoad[2]) {
                    needLoad[2] = false;
                    count2 = 1;
                    pageIndex = count2;
                    productQuery(flush2_code, pl_2, false);
                }
            } else if (r3.isChecked()) {
                config = configMap.get(r3.getId());
                if (needLoad[3]) {
                    needLoad[3] = false;
                    count3 = 1;
                    pageIndex = count3;
                    productQuery(flush3_code, pl_3, false);
                }
            } else if (r4.isChecked()) {
                config = configMap.get(r4.getId());
                if (needLoad[4]) {
                    needLoad[4] = false;
                    count4 = 1;
                    pageIndex = count4;
                    productQuery(flush4_code, pl_4, false);
                }
            }
        }
    };

    // 下拉刷新listVIEW的 点击事件
    OnItemClickListener itemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            GoodsInfo obj = (GoodsInfo) parent.getAdapter().getItem(position);
            Intent intent = new Intent(HotSalesActivity.this,
                    OtherGoodsDetailActivity.class);
            intent.putExtra(FinalUtil.GOODSINFO, obj);
            startActivity(intent);
        }
    };

    // ViewFlipper 选择切换
    OnCheckedChangeListener checkedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == r1.getId()) {
                if (flipper.getDisplayedChild() != 0) {
                    ib_menu.setBackgroundDrawable(search_drawable);
                    hs1.scrollTo(0, 0);
                    type = "0";
                    switchLayoutStateTo(flipper, 0);
                }
                return;
            }
            if (checkedId == r2.getId()) {
                if (flipper.getDisplayedChild() != 1) {
                    ib_menu.setBackgroundDrawable(search_drawable);
                    hs1.scrollTo(0, 0);
                    switchLayoutStateTo(flipper, 1);
                }
                type = "2";
                return;
            }
            if (checkedId == r3.getId()) {
                if (flipper.getDisplayedChild() != 2) {
                    ib_menu.setBackgroundDrawable(search_drawable);
                    hs1.scrollTo(350, 0);
                    switchLayoutStateTo(flipper, 2);
                }
                type = "1";
                return;
            }
            if (checkedId == r4.getId()) {
                if (flipper.getDisplayedChild() != 3) {
                    hs1.scrollTo(450, 0);
                    ib_menu.setBackgroundDrawable(search_drawable);
                    switchLayoutStateTo(flipper, 3);
                }
                type = "3";
                return;
            }

            if (checkedId == r5.getId()) { // 充值
                if (flipper.getDisplayedChild() != 4) {
                    ib_menu.setBackgroundDrawable(refreDrawable);
                    hs1.scrollTo(600, 0);
                    switchLayoutStateTo(flipper, 4);
                }
                return;
            }
        }
    };

    /**
     * @param specificationId
     *            类型ID
     * @param speciesId
     *            商品id 跳转去商品详细页面
     */
    public void onItemClientEven(String specificationId, String speciesId) {
        List<Parameter> parameters = new ArrayList<Parameter>();
        if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            parameters.add(new Parameter("account", application.getAccount()));
            parameters.add(new Parameter("loginUid", application.getUid()));
        } else {
            parameters.add(new Parameter("account", ""));
            parameters.add(new Parameter("loginUid", ""));
        }
        parameters.add(new Parameter(FinalUtil.AGENT_UID, ""));
        parameters.add(new Parameter("specificationId", specificationId));
        parameters.add(new Parameter("speciesId", speciesId));
        Util.toWebViewActivity(this, AgentWebViewActivity.class, AgentConfig.getShopAdd(), parameters);

        // Util.toWebViewActivity(this, AgentWebViewActivity.class, AgentConfig.getShopAdd(), specificationId, speciesId);
    }

    /**
     * 商品查询
     * 
     * @author wubo
     * @time 2013-1-23
     * @param casekey
     * 
     */
    private void productQuery(int casekey, PullToRefreshListView refreV, boolean needPd) {
        if (null != refreV) {
            refreV.setRefreshing(true);
            needPd = false;
        }
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("type", type));// 商品类别 0,合约机;1,光宽带;2,手机卡;3,其他;
        // 默认为 0
        // paras.add(new Parameter("agentUId", application.getUid()));
        paras.add(new Parameter("provinceId", provinceId));// 省份 默认为空
        paras.add(new Parameter("cityId", provinceId));// 地市 默认为空
        paras.add(new Parameter("minPrice", config.getMinPrice()));// 最小价格 默认为0
        paras.add(new Parameter("maxPrice", config.getMaxPrice()));// 最大价格 默认为0
        paras.add(new Parameter("keywords", config.getKeywords()));// 商品关键词 默认为空
        paras.add(new Parameter("orderBy", config.getOrderBy()));// 排序字段，默认为00
        paras.add(new Parameter("pageIndex", String.valueOf(pageIndex)));// 当前页数
        // 默认为1
        paras.add(new Parameter("pageSize", String.valueOf(pageSize)));// 分页大小，默认为9
        // 00：上架新品降序 01：上架新品升序
        // 10：销售量降序 11：销售量升序
        // 20：价格降序 21：价格升序
        serverMana.supportRequest(AgentConfig.getGoodsList(), paras, needPd,
                "加载中,请稍等 ...", casekey);
    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        logger.info("最热接收的信息是: " + supportResponse.toString());
        if (pl_1.isRefreshing()) {
            pl_1.onRefreshComplete();
        } else if (pl_2.isRefreshing()) {
            pl_2.onRefreshComplete();
        } else if (pl_3.isRefreshing()) {
            pl_3.onRefreshComplete();
        } else if (pl_4.isRefreshing()) {
            pl_4.onRefreshComplete();
        }

        if (!HttpResponseStatus(supportResponse)) {
            boolean isShow = true;
            if (supportResponse instanceof HttpReqCode) {
                isShow = false;
            }
            if (caseKey == conf_1.getQueryCode()) {
                if (goods1.size() <= 0) {
                    showNoDataView(isShow ? noData11 : null);
                }
            } else if (caseKey == query2_code || caseKey == flush2_code) {
                if (goods2.size() <= 0) {
                    showNoDataView(isShow ? noData22 : null);
                }
            } else if (caseKey == query3_code || caseKey == flush3_code) {
                if (goods3.size() <= 0) {
                    showNoDataView(isShow ? noData33 : null);
                }
            } else if (caseKey == query4_code || caseKey == flush4_code) {
                if (goods4.size() <= 0) {
                    showNoDataView(isShow ? noData44 : null);
                }
            }
            return;
        }
        GoodsListResult goodsResult = JSONUtil.fromJson(supportResponse
                .toString(), GoodsListResult.class);
        if (goodsResult != null && null != goodsResult.getGoodsList()) {
            if (caseKey == conf_1.getQueryCode()) {
                if (conf_1.isRefresh()) {
                    goods1.clear();
                    goodsAdapter1.notifyDataSetInvalidated();
                }
                goods1.addAll(goodsResult.getGoodsList());
                goodsAdapter1.notifyDataSetChanged();
                count1 += 1;
                total1 = goodsResult.getTotal();
                resultChange(total1, goods1.size(), loadMoveV1, goods1, noData11, lv_1);
                return;
            } else if (caseKey == flush2_code || caseKey == query2_code) {
                if (caseKey == flush2_code) {
                    goods2.clear();
                    goodsAdapter2.notifyDataSetInvalidated();
                }
                goods2.addAll(goodsResult.getGoodsList());
                goodsAdapter2.notifyDataSetChanged();
                count2 += 1;
                total2 = goodsResult.getTotal();
                resultChange(total2, goods2.size(), loadMoreV2, goods2, noData22, lv_2);
                return;
            } else if (caseKey == flush3_code || caseKey == query3_code) {
                if (caseKey == flush3_code) {
                    goods3.clear();
                    goodsAdapter3.notifyDataSetInvalidated();
                }
                goods3.addAll(goodsResult.getGoodsList());
                goodsAdapter3.notifyDataSetChanged();
                count3 += 1;
                total3 = goodsResult.getTotal();
                resultChange(total3, goods3.size(), loadMoreV3, goods3, noData33, lv_3);
                return;
            } else if (caseKey == flush4_code || caseKey == query4_code) {
                if (caseKey == flush4_code) {
                    goods4.clear();
                    goodsAdapter4.notifyDataSetInvalidated();
                }
                goods4.addAll(goodsResult.getGoodsList());
                goodsAdapter4.notifyDataSetChanged();
                count4 += 1;
                total4 = goodsResult.getTotal();
                resultChange(total4, goods4.size(), loadMoreV4, goods4, noData44, lv_4);
                return;
            }
        } else {
            showToast(R.string.to_server_fail);
            return;
        }
    }

    /**
     * 依据服务器结果数据更改相应控件状态
     * 
     * @author caosq 2013-5-14 上午11:08:22
     * @param lv
     */
    private void resultChange(int total, int count, View btnButton, List<GoodsInfo> goods, View noDataView, ListView lv) {
        if (total > count) {
            if (lv.getFooterViewsCount() == 0) {
                lv.addFooterView(btnButton);
            }
        } else {
            lv.removeFooterView(btnButton);
            if (goods.size() <= 0) {
                showNoDataView(noDataView);
            }
        }
    }

    private void showNoDataView(View noDataLayout) {
        Message agem = Message.obtain();
        if (null != noDataLayout) {
            noDataLayout.setVisibility(0);
            agem.obj = noDataLayout;
        }
        agem.what = 4;
        handler.sendMessageDelayed(agem, 2000);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(MainActivity.KEY_RADIO_INDEX, rg1.getCheckedRadioButtonId());
    };

    // 配置界面刷新类
    class ConfigutionUtil {
        private boolean refresh;
        private int queryCode;

        public boolean isRefresh() {
            return refresh;
        }

        public void setRefresh(boolean refresh) {
            this.refresh = refresh;
        }

        public int getQueryCode() {
            return queryCode;
        }

        public void setQueryCode(int queryCode) {
            this.queryCode = queryCode;
        }
    }
}