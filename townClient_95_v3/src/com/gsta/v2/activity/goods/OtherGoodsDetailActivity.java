package com.gsta.v2.activity.goods;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.telephony.SmsManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gsta.v2.activity.AgentWebViewActivity;
import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;
import com.gsta.v2.activity.WeiboShareActivity;
import com.gsta.v2.activity.adapter.ViewPageAdapter;
import com.gsta.v2.activity.waiter.MsgSellActivity;
import com.gsta.v2.db.impl.SharedPreferencesUtil;
import com.gsta.v2.entity.GoodsInfo;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.entity.ShopProduct;
import com.gsta.v2.response.BaseResult;
import com.gsta.v2.response.ShopProductResult;
import com.gsta.v2.ui.MyDialog;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ImageUtil;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;
import com.gsta.v2.util.WeiboUtil;
import com.tencent.weibo.oauthv2.OAuthV2;
import com.tencent.weibo.webview.OAuthV2AuthorizeWebView;
import com.weibo.net.AccessToken;
import com.weibo.net.DialogError;
import com.weibo.net.Oauth2AccessTokenHeader;
import com.weibo.net.Utility;
import com.weibo.net.Weibo;
import com.weibo.net.WeiboDialogListener;
import com.weibo.net.WeiboException;

/**
 * 商品详细信息
 * 
 * @author boge
 * @time 2013-3-27
 * 
 */
public class OtherGoodsDetailActivity extends BaseActivity implements
        IUICallBackInterface {
    private Button btn_back;
    private TextView tv_title;
    private TextView tv_detail, tv_stocks, tv_price;
    private TextView tv_oldprice, tv_shopname, tv_shopprovide;
    private Button btn_share;// 微博/短信分享
    private Button btn_fav;// 商品收藏
    // 查看详情
    private Button btn_todetail;
    // 查看详情
    private TextView btn_shopping;

    private MyDialog myDialog;
    // 咨询店主
    private TextView tv_msg_query_shopman;
    private String sina_token = "";
    private String sina_expires_in = "";
    private String sina_uid = "";

    private OAuthV2 oAuth;
    private String tx_token = "";
    private String tx_expires_in = "";
    private String tx_openid = "";
    private String tx_openkey = "";

    private SharedPreferencesUtil preferencesUtil;

    private GoodsInfo goodsInfo;

    // 一 图片显示
    private ViewPager pager_one;
    private ViewPageAdapter adapter_small;

    /**
     * 传递图片放大的标识KEY
     */
    public static final String PIC_MAGNIFY_FLAG = "pic_magnify";
    /**
     * 要放大图片的名称
     */
    public static final String PIC_MAGNIFY_NAME_FLAG = "pic_name_magnify";

    private List<String> uris = new ArrayList<String>();

    final int add_code = 11;
    final int show_code = 12;

    /**
     * 是否显示收藏按钮
     */
    public static final String GOODS_ISHAVE = "goods_ishave";
    /**
     * 已经收藏不显示
     */
    public static final int GOODS_HAVE = 0xff1;
    /**
     * 未收藏 显示
     */
    public static final int GOODS_NO_HAVE = 0xff2;
    // 跳转过来的店铺的 IMBean
    private IMBean imBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.othergoods_detail);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        btn_share = (Button) findViewById(R.id.btn_weibo);
        btn_fav = (Button) findViewById(R.id.btn_msg);
        btn_shopping = (TextView) findViewById(R.id.btn_shoppingcar);
        tv_msg_query_shopman = (TextView) findViewById(R.id.tv_msg_query_shopman);

        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_todetail = (Button) findViewById(R.id.btn_todetail);
        btn_back.setVisibility(0);

        // iv_portrait = (ImageView) findViewById(R.id.iv_portrait);
        // tv_name = (TextView) findViewById(R.id.tv_name);
        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_detail = (TextView) findViewById(R.id.tv_detail);
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        tv_stocks = (TextView) findViewById(R.id.tv_stocks);
        tv_oldprice = (TextView) findViewById(R.id.tv_oldprice);
        tv_shopname = (TextView) findViewById(R.id.tv_shopname);
        tv_shopprovide = (TextView) findViewById(R.id.tv_shopprovide);
        // 一
        pager_one = (ViewPager) findViewById(R.id.pager_one);
    }

    private final String stock = "库 存: ";
    private final String price = "售 价: ￥";
    private final String costPrice = "原 价: ￥";

    private void initData() {
        preferencesUtil = new SharedPreferencesUtil(OtherGoodsDetailActivity.this);
        Intent intent = getIntent();
        // 隐藏收 藏
        if (GOODS_NO_HAVE != intent.getIntExtra(GOODS_ISHAVE, GOODS_NO_HAVE)) {
            findViewById(R.id.ll_msg).setVisibility(View.GONE);
        }
        imBean = (IMBean) intent.getSerializableExtra(FinalUtil.IMBEAN);
        if (null == imBean) {
            imBean = new IMBean();
        }
        // 隐藏咨询店主
        if (application.getLoginState() != FinalUtil.LONGIN_LONGINSTATE_SUCCEED ||
                null == imBean.getHisUserId() || imBean.getHisUserId().trim().length() == 0 || imBean.getHisUserId().equals(application.getUid())) {
            findViewById(R.id.ll_msg_query_shopman).setVisibility(View.GONE);
        }
        // 隐藏分享
        if (application.getAgentStatu() != FinalUtil.AgentStatu_normal) {
            findViewById(R.id.ll_weibo).setVisibility(View.GONE);
        }
        goodsInfo = (GoodsInfo) intent.getSerializableExtra(
                FinalUtil.GOODSINFO);
        if (null != goodsInfo) {
            tv_title.setText(goodsInfo.getProdName());
            uris.add(goodsInfo.getMediaPath());
            adapter_small = new ViewPageAdapter(OtherGoodsDetailActivity.this, uris, imageClick);
            pager_one.setAdapter(adapter_small);
            getProductInfo();
        } else {
            showToast("没有商品信息");
            finish();
        }
    }

    private void initListener() {
        btn_back.setOnClickListener(listener);
        btn_share.setOnClickListener(listener);
        btn_fav.setOnClickListener(listener);
        btn_shopping.setOnClickListener(listener);
        tv_msg_query_shopman.setOnClickListener(listener);
        btn_todetail.setOnClickListener(listener);

    }

    // 放大 查看
    android.view.View.OnClickListener imageClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // 放大 相册
            ImageUtil.magnifyPic(OtherGoodsDetailActivity.this, tv_title.getText(), 0, uris);
        }
    };

    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btn_share) { // 微博分享
                if (application.getLoginState() != FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
                    showToast(" 对不起,你目前未登录,请登录后进行分享...");
                    return;
                }
                ListView listView = (ListView) LayoutInflater.from(OtherGoodsDetailActivity.this)
                        .inflate(R.layout.dialog_list_view, null);
                String[] objects = { "短信分享", "新浪微博分享", "腾讯微博分享" };
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(OtherGoodsDetailActivity.this, R.layout.dialog_text_view, objects);
                listView.setAdapter(adapter);
                // 设置监听器
                listView.setOnItemClickListener(listClickListener);
                myDialog = MyDialog.showDialog(OtherGoodsDetailActivity.this, "分    享", listView);
            } else if (v.getId() == btn_fav.getId()) { // 收藏商品
                if (application.getLoginState() != FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
                    showToast(" 对不起,你目前未登录,请登录后收藏该商品...");
                    return;
                }
                agentShopDel(goodsInfo);
            } else if (v == btn_back) {
                finish();
            } else if (v == btn_shopping || v == btn_todetail) {// 点击购买
                // 调用浏览器
                List<Parameter> parameters = new ArrayList<Parameter>();
                if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
                    parameters.add(new Parameter("account", application.getAccount()));
                    parameters.add(new Parameter("loginUid", application.getUid()));
                } else {
                    parameters.add(new Parameter("account", ""));
                    parameters.add(new Parameter("loginUid", ""));
                }
                parameters.add(new Parameter(FinalUtil.AGENT_UID, null == imBean.getHisUserId() ? "" : imBean.getHisUserId()));
                parameters.add(new Parameter("specificationId", goodsInfo.getSpecificationId()));
                parameters.add(new Parameter("speciesId", goodsInfo.getSpeciesId()));
                Util.toWebViewActivity(OtherGoodsDetailActivity.this, AgentWebViewActivity.class, AgentConfig.getShopAdd(), parameters);
            } else if (v.getId() == tv_msg_query_shopman.getId()) { // 咨询店主
                if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
                    //
                    if (null == imBean.getHisUserId() || imBean.getHisUserId() == application.getUid()) {
                        showToast("你处在商城总店中,请与客服联系...");
                    } else {
                        if (null == imBean.getHisName()) {
                            showToast("代理商名称未知...");
                        } else {
                            if (!Util.toIM(OtherGoodsDetailActivity.this, imBean)) {
                                showToast("不能与店主聊天...");
                            }
                        }
                    }
                } else {
                    showToast(" 对不起,你目前未登录,请登录后再咨询店主...");
                }
            }
        }
    };

    /**
     * 发送短信
     * 
     * @param contacts
     *            void
     */
    @SuppressWarnings("unused")
    private void sendMsg(String phoneNum) {
        if (phoneNum != null && phoneNum.length() > 0) {
            String sms = "";
            SmsManager smsMana = SmsManager.getDefault();
            smsMana.sendTextMessage(phoneNum, null, sms, null, null);
        }
    }

    // 微博
    class AuthDialogListener implements WeiboDialogListener {
        @Override
        public void onComplete(Bundle values) {
            sina_token = values.getString("access_token");
            sina_expires_in = values.getString("expires_in");
            sina_uid = values.getString("uid");
            share2weibo(sina_token, sina_expires_in, sina_uid);
        }

        @Override
        public void onError(DialogError e) {
        }

        @Override
        public void onCancel() {
        }

        @Override
        public void onWeiboException(WeiboException e) {
        }
    }

    /**
     * 新浪分享
     * 
     * @author wubo
     * @createtime 2012-9-21
     * @param token
     * @param expires_in
     */
    private void share2weibo(String token, String expires_in, String sina_uid) {
        AccessToken accessToken = new AccessToken(token,
                WeiboUtil.SINA_CONSUMER_SECRET);
        accessToken.setExpiresIn(expires_in);
        // 隐式登录需要填写
        Utility.setAuthorization(new Oauth2AccessTokenHeader());
        Weibo.getInstance().setAccessToken(accessToken);
        if (TextUtils.isEmpty(accessToken.getToken())) {
            showToast("授权失败!");
        } else {
            preferencesUtil.saveString(WeiboUtil.SINA_TOKEN, token);
            preferencesUtil.saveString(WeiboUtil.SINA_EXPIRES_IN, expires_in);
            preferencesUtil.saveString(WeiboUtil.SINA_UID, sina_uid);
            Intent i = new Intent(OtherGoodsDetailActivity.this,
                    WeiboShareActivity.class);
            // sina token
            i.putExtra(WeiboUtil.EXTRA_ACCESS_TOKEN, accessToken.getToken());
            // sina secret
            i.putExtra(WeiboUtil.EXTRA_TOKEN_SECRET, accessToken.getSecret());
            // 商品信息
            i.putExtra(FinalUtil.GOODSINFO, goodsInfo);
            // 标识 新浪还是腾讯 0sina 1tx
            i.putExtra(WeiboUtil.WEIBOFLAG, 0);
            OtherGoodsDetailActivity.this.startActivity(i);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == OAuthV2AuthorizeWebView.RESULT_CODE) {
                oAuth = (OAuthV2) data.getExtras().getSerializable("oauth");
                preferencesUtil.saveString(WeiboUtil.TX_TOKEN,
                        oAuth.getAccessToken());
                preferencesUtil.saveString(WeiboUtil.TX_OPENID,
                        oAuth.getOpenid());
                preferencesUtil.saveString(WeiboUtil.TX_OPENKEY,
                        oAuth.getOpenkey());
                preferencesUtil.saveString(WeiboUtil.TX_EXPIRES_IN,
                        oAuth.getExpiresIn());

                // 调用API
                share2txweibo();
            }
        }

    }

    /**
     * 腾讯微博分享
     * 
     * @author boge
     * @time 2013-3-28 void
     * 
     */
    public void share2txweibo() {
        Intent i = new Intent(OtherGoodsDetailActivity.this,
                WeiboShareActivity.class);
        i.putExtra(WeiboUtil.TX_OAUTH, oAuth);
        i.putExtra(FinalUtil.GOODSINFO, goodsInfo);
        i.putExtra(WeiboUtil.WEIBOFLAG, 1);
        OtherGoodsDetailActivity.this.startActivity(i);
    }

    // 弹窗点击事件
    OnItemClickListener listClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            switch (position) {
            case 0:// 短信分享
                Intent intent = new Intent(OtherGoodsDetailActivity.this,
                        MsgSellActivity.class);
                intent.putExtra("opType", 1);
                intent.putExtra(FinalUtil.GOODSINFO, goodsInfo);
                OtherGoodsDetailActivity.this.startActivity(intent);
                break;
            case 1:// 新浪微博分享
                sina_token = preferencesUtil
                        .getString(WeiboUtil.SINA_TOKEN, "");
                sina_expires_in = preferencesUtil.getString(
                        WeiboUtil.SINA_EXPIRES_IN, "");
                sina_uid = preferencesUtil.getString(WeiboUtil.SINA_UID, "");
                if (sina_token.equals("") || sina_expires_in.equals("")
                        || sina_uid.equals("")) { // 登录
                    Weibo weibo = Weibo.getInstance();
                    Utility.clearCookies(OtherGoodsDetailActivity.this);
                    weibo.setupConsumerConfig(WeiboUtil.SINA_CONSUMER_KEY,
                            WeiboUtil.SINA_CONSUMER_SECRET);
                    // Oauth2.0 隐式授权认证方式
                    weibo.setRedirectUrl(WeiboUtil.SINA_CALL_URI);// 应用回调页
                    weibo.authorize(OtherGoodsDetailActivity.this,
                            new AuthDialogListener());
                } else {
                    share2weibo(sina_token, sina_expires_in, sina_uid);
                }
                break;
            case 2:// 腾讯微博分享
                oAuth = new OAuthV2(WeiboUtil.TX_CALL_URI);
                oAuth.setClientId(WeiboUtil.TX_CLIENTID);
                oAuth.setClientSecret(WeiboUtil.TX_CLIENTSECRET);
                tx_token = preferencesUtil.getString(WeiboUtil.TX_TOKEN, "");
                tx_openid = preferencesUtil.getString(WeiboUtil.TX_OPENID, "");
                tx_openkey = preferencesUtil
                        .getString(WeiboUtil.TX_OPENKEY, "");
                tx_expires_in = preferencesUtil.getString(
                        WeiboUtil.TX_EXPIRES_IN, "");
                if (tx_token.equals("") || tx_openid.equals("")
                        || tx_openkey.equals("") || tx_expires_in.equals("")) {
                    Intent intent2 = new Intent(OtherGoodsDetailActivity.this,
                            OAuthV2AuthorizeWebView.class);
                    intent2.putExtra("oauth", oAuth);
                    startActivityForResult(intent2, 1);
                } else {
                    oAuth.setAccessToken(tx_token);
                    oAuth.setOpenid(tx_openid);
                    oAuth.setOpenkey(tx_openkey);
                    oAuth.setExpiresIn(tx_expires_in);
                    share2txweibo();
                }
                break;
            }
            // if (window != null) {
            // window.dismiss();
            // }
            MyDialog.dismiss(myDialog);
        }
    };

    /**
     * 商品信息
     * 
     * @author wubo
     * @createtime 2012-10-13
     */
    public void getProductInfo() {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) { // 在商城总店购买 就不使用代理商UID 后面可改正为域名
            paras.add(new Parameter("uid", application.getUid()));// 访问者uid
        }

        if (null != imBean.getHisUserId() && imBean.getHisUserId().trim().length() > 0) {
            paras.add(new Parameter("agentUid", imBean.getHisUserId()));// 店铺域名或者shop+uid
            // paras.add(new Parameter("eshopUids", "shop" + agentUid));// 店铺域名或者shop+uid
        }

        if (null != goodsInfo.getSpecificationId()) {
            paras.add(new Parameter("specificationId", goodsInfo
                    .getSpecificationId()));
        } else {
            return;
        }

        if (null != goodsInfo.getSpeciesId()) {
            paras.add(new Parameter("speciesId", goodsInfo.getSpeciesId()));
        } else {
            return;
        }
        serverMana.supportRequest(AgentConfig.shopProduct(), paras, true,
                "查询中,请稍等 ...", show_code);
    }

    /**
     * 
     * @author longxianwen
     * @Title: agentShopDel
     * @Description: 添加收藏
     * @return void 返回类型
     */
    private void agentShopDel(GoodsInfo info) {
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("userId", application.getUid()));// 登录密码
        paras.add(new Parameter("specificationId", info.getSpecificationId()));
        paras.add(new Parameter("speciesId", info.getSpeciesId()));
        // 0:收藏，1：取消收藏
        paras.add(new Parameter("opType", "0"));
        serverMana.supportRequest(
                AgentConfig.cancelGoodsCollect(),
                paras, false, "提交中,请稍等 ....", add_code);

    }

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        if (!HttpResponseStatus(supportResponse))
            return;

        switch (caseKey) {
        case show_code:
            ShopProductResult pd = JSONUtil.fromJson(
                    supportResponse.toString(), ShopProductResult.class);
            if (pd != null) {
                ShopProduct sp = pd.getShopProduct();
                final List<String> uriDown = sp.getMediaPath();
                if (null != uriDown && uriDown.size() > 0) {
                    if (uris.size() > 0) {
                        uris.clear();
                    }
                    uris.addAll(uriDown);
                }
                adapter_small.notifyDataSetChanged();
                if (sp.getIsCollect() == 1) {
                    // btn_fav.setVisibility(View.GONE);
                    findViewById(R.id.ll_msg).setVisibility(View.GONE);
                }
                tv_detail.setText(Html.fromHtml(
                        // "<b textSize=20>" + (null == sp.getProdName() ? "未命名" : sp.getProdName()) + "</b> " +
                        "<font color=#FF0000 textSize=15>" + (null == sp.getDescription() ? "" : sp.getDescription()) + "</font> "));
                if (tv_stocks.getVisibility() != 0) {
                    tv_stocks.setVisibility(0);
                }
                tv_stocks.setText(stock + sp.getAmount());
                tv_price.setText(price + sp.getSalePrice());
                tv_oldprice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                tv_oldprice.setText(costPrice + goodsInfo.getMinRetailPrice());
                // tv_oldprice.setText(costPrice + sp.getRetailPrice());
                tv_shopname.setText(Html.fromHtml(
                        "<b>" + "您现在位于:" + "</b> " +
                                "<font color=#0080FF >" + (null == sp.getShopName() ? "商城总店" : sp.getShopName()) + "</font> "));
                tv_shopprovide.setText(Html.fromHtml(
                        "<b>" + "商品提供方:" + "</b> " +
                                "<font color=#0080FF >" + (null == sp.getMerchantName() ? "商城总店" : sp.getMerchantName()) + "</font> "));
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        case add_code:// 添加收藏
            BaseResult result = JSONUtil.fromJson(
                    supportResponse.toString(), ShopProductResult.class);
            btn_fav.setVisibility(View.GONE);
            showToast(result.getErrorMessage());
            // btn_fav.setVisibility(View.GONE);
            break;
        }
    }
}
