package com.gsta.v2.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.http.Parameter;
import mobile.json.JSONUtil;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.gsta.v2.activity.mytown.RegUserActivity;
import com.gsta.v2.activity.service.SendService;
import com.gsta.v2.db.impl.SharedPreferencesUtil;
import com.gsta.v2.entity.BaseUserinfo;
import com.gsta.v2.response.LoginResult;
import com.gsta.v2.util.AgentConfig;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.IUICallBackInterface;
import com.gsta.v2.util.ServerSupportManager;
import com.gsta.v2.util.Util;

/**
 * 登录     客户端
 * 
 * @author wubo
 * @createtime 2012-7-23
 */
public class LoginActivity extends TopBackActivity implements IUICallBackInterface {

    private EditText accountText, passWordText;
    private Button btn_login, btn_reg;
    private TextView tv_title;

    private CheckBox rem_passWord_chkbox, rem_no_chkbox;
    private final String CHECK_BOXONE = "checkOne";
    private final String CHECK_BOXTWO = "checkTwo";

    private SharedPreferencesUtil mUtil;
    final int Login_Code = 1;

    // 
    private String oldUserName;
    private String pass;
    private int type = 0;
    // 启动或者调用启动的服务
    private Intent sendService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.login);
        // 初始化控件
        initView();
        // 初始化配置数据
        initData();
        // 初始化监听
        initListener();
    }

    private void initView() {
        accountText = (EditText) findViewById(R.id.login_account_edit);
        passWordText = (EditText) findViewById(R.id.login_password_edit);
        btn_login = (Button) findViewById(R.id.login_btn);
        btn_reg = (Button) findViewById(R.id.reg_btn);
        rem_passWord_chkbox = (CheckBox) findViewById(R.id.login_rem_password_chkbox);
        rem_passWord_chkbox.setChecked(true);
        rem_no_chkbox = (CheckBox) findViewById(R.id.login_rem_no_chkbox);
        rem_no_chkbox.setChecked(false);

        tv_title = (TextView) findViewById(R.id.mytitle_textView);
    }

    private void initData() {
        tv_title.setText("用 户 登 录");
        // SharedPreferences存储一些参数
        mUtil = new com.gsta.v2.db.impl.SharedPreferencesUtil(
                LoginActivity.this);
        // 取得SharedPreferences存储的用户名
        oldUserName = mUtil.getString(AgentConfig.LOGIN_IN_NAME, "");
        // 取得SharedPreferences存储的密码
        pass = mUtil.getString(AgentConfig.LOGIN_IN_PASS, "");
        rem_passWord_chkbox.setChecked(mUtil.getBoolean(CHECK_BOXONE, true));
        rem_no_chkbox.setChecked(mUtil.getBoolean(CHECK_BOXTWO, false));
        pass = mUtil.getString(AgentConfig.LOGIN_IN_PASS, "");
        accountText.setText(oldUserName);
        passWordText.setText(pass);

        Util.setEditCursorToTextEnd(accountText);
        Util.setEditCursorToTextEnd(passWordText);
        sendService = new Intent(LoginActivity.this, SendService.class);
    }

    private void initListener() {
        btn_login.setOnClickListener(btnOnClickListener);
        btn_reg.setOnClickListener(btnOnClickListener);
        rem_passWord_chkbox
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                            boolean isChecked) {
                        if (isChecked) {
                            rem_no_chkbox.setChecked(false);
                        }
                    }
                });
        rem_no_chkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked) {
                if (isChecked) {
                    rem_passWord_chkbox.setChecked(false);
                }
            }
        });
    }

    OnClickListener btnOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.login_btn: // 要检查 chcekbox
                final String newUserName = accountText.getText().toString();
                pass = passWordText.getText().toString();
                if (TextUtils.isEmpty(newUserName) || newUserName.trim().equals("")) {
                    showToast("请输入用户名!");
                } else if (TextUtils.isEmpty(pass) || pass.trim().equals("")) {
                    showToast("请输入密码!");
                } else {
                    if (Util.isCellphone(newUserName)) {
                        type = 0;
                        login();
                    } else if (Util.isEmail(newUserName)) {
                        type = 1;
                        login();
                    } else {
                        showToast("用户名为邮箱或手机号!");
                    }
                }
                break;
            case R.id.reg_btn: // 注册
                Intent intent = new Intent(LoginActivity.this,
                        RegUserActivity.class);
                startActivityForResult(intent, 0xaa);
                break;
            }
        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0xaa && resultCode == RESULT_OK) {
            accountText.setText(data.getStringExtra(AgentConfig.LOGIN_IN_NAME));
            passWordText.setText("");
        }
    };

    /**
     * 登录
     * 
     * @author wubo
     * @createtime 2012-8-31
     */
    private void login() {
        btn_login.setEnabled(false);
        ServerSupportManager serverMana = new ServerSupportManager(this, this);
        List<Parameter> paras = new ArrayList<Parameter>();
        paras.add(new Parameter("account", accountText.getText().toString()));// 登录账号
        paras.add(new Parameter("password", pass));// 登录密码
        paras.add(new Parameter("type", String.valueOf(type)));// 登录密码
        serverMana.supportRequest(AgentConfig.getLogin(), paras, false,
                "登录中,请稍等 ...", Login_Code);
    }

    // 是否更换的登录帐号 true 更改
    private boolean isChangeUser = false;

    @Override
    public void uiCallBack(Object supportResponse, int caseKey) {
        btn_login.setEnabled(true);
        if (!HttpResponseStatus(supportResponse)) {
            return;
        }
        switch (caseKey) {
        case Login_Code:
            LoginResult loginResult = JSONUtil.fromJson(supportResponse
                    .toString(), LoginResult.class);
            if (loginResult != null) {
                // String versionName = Util.getAppVersionName(LoginActivity.this);
                BaseUserinfo info = loginResult.getUserInfo();
                if (info == null) {
                    finish();
                } else {
                    // 保存用户信息
                    application.setInviteAccount(loginResult.getInviteAccount());
                    application.setUserinfo(info);
                    final String aString = accountText.getText().toString();
                    application.setAccount(aString);
                    if (application.getLoginState() == FinalUtil.LONGIN_LONGINSTATE_Destory && !aString.equals(oldUserName)) {
                        isChangeUser = true;
                    }
                    application.setLoginState(FinalUtil.LONGIN_LONGINSTATE_SUCCEED);
                    if (rem_passWord_chkbox.isChecked()) {// 如果记住密码被选中则记住用户名和密码
                        mUtil.saveString(AgentConfig.LOGIN_IN_NAME,
                                aString);
                        mUtil.saveString(AgentConfig.LOGIN_IN_PASS,
                                passWordText.getText().toString());
                    } else if (rem_no_chkbox.isChecked()) {// 选中则记住用户名不记住密码
                        mUtil.saveString(AgentConfig.LOGIN_IN_NAME,
                                aString);
                        mUtil.saveString(AgentConfig.LOGIN_IN_PASS, "");
                    } else {// 如果不记住账号被选中则不记住用户名和密码
                        mUtil.saveString(AgentConfig.LOGIN_IN_NAME, "");
                        mUtil.saveString(AgentConfig.LOGIN_IN_PASS, "");
                    }
                    mUtil.saveBoolean(CHECK_BOXONE, rem_passWord_chkbox.isChecked());
                    mUtil.saveBoolean(CHECK_BOXTWO, rem_no_chkbox.isChecked());
                    application.setAgentInfo(loginResult.getAgentInfo());

                    startOpenfire();
                    gotoMainAcivity();
                }
            } else {
                showToast(R.string.to_server_fail);
                return;
            }
            break;
        }
    }

    /**
     * @author caosq 2013-4-19 下午4:12:07
     */
    private void startOpenfire() {
        sendService.putExtra(FinalUtil.SENDSERVICE_FLAG,
                FinalUtil.imLogin);
        sendService.putExtra(FinalUtil.Login_User, application.getUid());
        sendService.putExtra(FinalUtil.Login_Pass, pass);
        // 登录openfire
        startService(sendService);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    // 如果是切换帐号 则要清除所有原登录帐号的信息
    private void gotoMainAcivity() {
        Intent intent = new Intent(FinalUtil.RECEIVER_USER_LOGING_IN);
        intent.putExtra(FinalUtil.Login_User, isChangeUser);
        sendBroadcast(intent);
        // finish();
    }
}