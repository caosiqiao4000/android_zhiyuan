package com.gsta.v2.activity;

import java.util.List;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.gsta.v2.activity.adapter.ViewPageMagnifyAdapter;
import com.gsta.v2.activity.goods.OtherGoodsDetailActivity;

/**
 * 依据传入的图片地址,显示图片
 * 
 * @author caosq 2013-6-17
 */
public class ShowPicMagnifyActivity extends BaseActivity {
    private TextView tv_title;
    private ViewPager pViewPager;
    private ViewPageMagnifyAdapter adapter;
    private Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.a_pic_magnify);

        initView();
        initData();
        initListener();
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.mytitle_textView);
        btn_back = (Button) findViewById(R.id.mytitile_btn_left);
        btn_back.setVisibility(0);
        pViewPager = (ViewPager) findViewById(R.id.pager_one);
    }

    private void initData() {
        tv_title.setText(null == getIntent().getStringExtra(OtherGoodsDetailActivity.PIC_MAGNIFY_NAME_FLAG) ? "商品详情" : getIntent().getStringExtra(
                OtherGoodsDetailActivity.PIC_MAGNIFY_NAME_FLAG));
        List<String> uris = getIntent().getStringArrayListExtra(OtherGoodsDetailActivity.PIC_MAGNIFY_FLAG);
        if (null != uris && uris.size() > 0) {
            adapter = new ViewPageMagnifyAdapter(ShowPicMagnifyActivity.this, uris, imageClick);
            pViewPager.setAdapter(adapter);
            if (uris.size() > 1) {
                pViewPager.setBackgroundResource(R.drawable.photo_bg);
            } else {
                pViewPager.setBackgroundResource(0);
            }
        } else {
            showToast("没有图片要显示...");
            finish();
        }
    }

    private void initListener() {
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // 图片缩放
    OnClickListener imageClick = new OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

}
