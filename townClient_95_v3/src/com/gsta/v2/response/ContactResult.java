package com.gsta.v2.response;

import java.util.List;

import com.gsta.v2.entity.AgentInfo;
import com.gsta.v2.entity.BuyerInfo;
import com.gsta.v2.entity.MyUserInfo;
import com.gsta.v2.entity.SellerInfo;

/**
 * 联系人管理通用结果类
 * @author: chenjd
 * @createTime: 2012-9-5 上午9:32:06 
 * @version: 1.0
 * @desc:
 */
 
public class ContactResult extends BaseResult{
	private Long count;// 当前返回集合数
	private Long tCount;// 满足条件总记录数
	
	private List<BuyerInfo> buyers;// 我的买家信息
	private List<AgentInfo> agents;// 下级代理商信息
	private List<MyUserInfo> users;// 我的用户信息
	private List<SellerInfo>  sellers;        //我的卖家信息
	
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public Long gettCount() {
		return tCount;
	}
	public void settCount(Long tCount) {
		this.tCount = tCount;
	}
	public List<BuyerInfo> getBuyers() {
		return buyers;
	}
	public void setBuyers(List<BuyerInfo> buyers) {
		this.buyers = buyers;
	}
	public List<AgentInfo> getAgents() {
		return agents;
	}
	public void setAgents(List<AgentInfo> agents) {
		this.agents = agents;
	}
	public List<MyUserInfo> getUsers() {
		return users;
	}
	public void setUsers(List<MyUserInfo> users) {
		this.users = users;
	}
	public List<SellerInfo> getSellers() {
		return sellers;
	}
	public void setSellers(List<SellerInfo> sellers) {
		this.sellers = sellers;
	}
	
	
}
