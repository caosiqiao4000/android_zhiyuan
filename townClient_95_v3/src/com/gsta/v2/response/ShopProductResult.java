package com.gsta.v2.response;

import com.gsta.v2.entity.ShopProduct;


/**
 * 查询店铺单品返回结果类
 *
 */
public class ShopProductResult extends BaseResult{
	
	private ShopProduct shopProduct;

	public ShopProduct getShopProduct() {
		return shopProduct;
	}

	public void setShopProduct(ShopProduct shopProduct) {
		this.shopProduct = shopProduct;
	}

}
