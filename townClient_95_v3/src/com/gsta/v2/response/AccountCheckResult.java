package com.gsta.v2.response;

import java.io.Serializable;

/**
 * com.gsta.response.AccountCheckResult
 * 
 * @author wubo
 * @date 2012-12-27
 * 
 */
public class AccountCheckResult extends BaseResult implements Serializable {
	
	String inviteAccount;

	public String getInviteAccount() {
		return inviteAccount;
	}

	public void setInviteAccount(String inviteAccount) {
		this.inviteAccount = inviteAccount;
	}
	
	

}
