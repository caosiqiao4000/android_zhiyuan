package com.gsta.v2.response;

import java.io.Serializable;
import java.util.List;

/**
 * com.gsta.response.ExtensionMsgResult
 * 
 * @author wubo
 * @date 2013-1-9
 * 
 */
public class ExtensionMsgResult extends BaseResult implements Serializable {

	private String shopUrl;
	private List<String> content;

	public List<String> getContent() {
		return content;
	}

	public void setContent(List<String> content) {
		this.content = content;
	}

	public String getShopUrl() {
		return shopUrl;
	}

	public void setShopUrl(String shopUrl) {
		this.shopUrl = shopUrl;
	}

}
