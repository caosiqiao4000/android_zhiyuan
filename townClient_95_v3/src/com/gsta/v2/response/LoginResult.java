package com.gsta.v2.response;

import com.gsta.v2.entity.BaseAgentInfo;
import com.gsta.v2.entity.BaseUserinfo;

public class LoginResult extends BaseResult {

	private BaseUserinfo userInfo;
	private BaseAgentInfo agentInfo;
	private String versionCode;// 最新版本号
	private String versionDesc;// 版本说明
	private String updateTime; // 版本更新时间
	private Integer updateType; // 0:不强制升级 1:强制升级
	private String inviteAccount;// 推荐人账号(非代理商用户登录返回)

	public BaseUserinfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(BaseUserinfo userInfo) {
		this.userInfo = userInfo;
	}

	public BaseAgentInfo getAgentInfo() {
		return agentInfo;
	}

	public void setAgentInfo(BaseAgentInfo agentInfo) {
		this.agentInfo = agentInfo;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionDesc() {
		return versionDesc;
	}

	public void setVersionDesc(String versionDesc) {
		this.versionDesc = versionDesc;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getUpdateType() {
		return updateType;
	}

	public void setUpdateType(Integer updateType) {
		this.updateType = updateType;
	}

	public String getInviteAccount() {
		return inviteAccount;
	}

	public void setInviteAccount(String inviteAccount) {
		this.inviteAccount = inviteAccount;
	}

}
