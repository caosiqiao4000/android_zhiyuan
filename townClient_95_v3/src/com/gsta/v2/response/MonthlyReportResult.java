package com.gsta.v2.response;

import java.io.Serializable;
import java.util.List;

import com.gsta.v2.entity.StatInfo;

/**
 *@author wubo
 *@createtime 2012-9-26
 */
public class MonthlyReportResult extends BaseResult implements Serializable {

	private Long count;
	private Long tCount;
	private List<StatInfo> statInfo;

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long gettCount() {
		return tCount;
	}

	public void settCount(Long tCount) {
		this.tCount = tCount;
	}

	public List<StatInfo> getStatInfo() {
		return statInfo;
	}

	public void setStatInfo(List<StatInfo> statInfo) {
		this.statInfo = statInfo;
	}

}
