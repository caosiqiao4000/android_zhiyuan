package com.gsta.v2.response;

import java.io.Serializable;

/**
 * com.gsta.response.MemoManageResult
 * 
 * @author wubo
 * @date 2012-12-27
 * 
 */
public class MemoManageResult extends BaseResult implements Serializable {

	String memoId;

	public String getMemoId() {
		return memoId;
	}

	public void setMemoId(String memoId) {
		this.memoId = memoId;
	}

}
