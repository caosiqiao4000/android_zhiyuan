package com.gsta.v2.response;

import com.gsta.v2.entity.BaseAgentInfo;


public class QueryAgentResult extends BaseResult{

	private BaseAgentInfo agentInfo;

	public BaseAgentInfo getAgentInfo() {
		return agentInfo;
	}

	public void setAgentInfo(BaseAgentInfo agentInfo) {
		this.agentInfo = agentInfo;
	}

}
