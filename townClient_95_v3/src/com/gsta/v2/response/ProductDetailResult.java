package com.gsta.v2.response;

import java.util.ArrayList;
import java.util.List;

import com.gsta.v2.entity.DonationInfo;
import com.gsta.v2.entity.ProductDetailInfo;

/**
 * 商品详细信息展示
 * 
 * @author: chenjd
 * @createTime: 2012-10-12 下午6:15:41
 * @version: 1.0
 * @desc:
 */
public class ProductDetailResult extends BaseResult {

	private List<String> mediaPath = new ArrayList<String>();// 商品图片地址
	private List<DonationInfo> donationInfo = new ArrayList<DonationInfo>();// 赠品信息
	private List<ProductDetailInfo> productDetailInfo = new ArrayList<ProductDetailInfo>();// 商品分成信息

	public List<String> getMediaPath() {
		return mediaPath;
	}

	public void setMediaPath(List<String> mediaPath) {
		this.mediaPath = mediaPath;
	}

	public List<DonationInfo> getDonationInfo() {
		return donationInfo;
	}

	public void setDonationInfo(List<DonationInfo> donationInfo) {
		this.donationInfo = donationInfo;
	}

	public List<ProductDetailInfo> getProductDetailInfo() {
		return productDetailInfo;
	}

	public void setProductDetailInfo(List<ProductDetailInfo> productDetailInfo) {
		this.productDetailInfo = productDetailInfo;
	}

}
