package com.gsta.v2.response;

import java.io.Serializable;
import java.util.List;

import com.gsta.v2.entity.GoodsInfo;

/**
 * @author wubo
 * @createtime 2012-9-18
 */
public class GoodsListResult extends BaseResult implements Serializable {

	private int responseCode;// 0：处理异常，1：处理成功
	private int total; // 总条数
	private int pageCount; // 总页数
	private List<GoodsInfo> goodsList;// 商品列表

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public List<GoodsInfo> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<GoodsInfo> goodsList) {
		this.goodsList = goodsList;
	}

}
