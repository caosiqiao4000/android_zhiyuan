package com.gsta.v2.response;

/**
 * @author zengxj
 * @createTime 2013-4-23
 * @version: 1.0
 * @desc: 账户基本信息
 */
public class AccountBaseInfo {
    
    private String userName;// 真实姓名
    private String identityNo;// 身份证号码
    private Double remain;// 账户余额
    private String identityStatus;// 账户身份证状态(0: 身份证未上传, 1: 身份证已上传)
    
    
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getIdentityNo() {
        return identityNo;
    }
    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }
    public Double getRemain() {
        return remain;
    }
    public void setRemain(Double remain) {
        this.remain = remain;
    }
    public String getIdentityStatus() {
        return identityStatus;
    }
    public void setIdentityStatus(String identityStatus) {
        this.identityStatus = identityStatus;
    }
    

}
