package com.gsta.v2.response;

/**
 * com.gsta.response.VersionResult
 * 
 * @author wubo
 * @date 2012-11-23
 * 
 */
public class VersionResult extends BaseResult {

	private String versionCode;
	private String versionDesc;
	private String updateTime;

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionDesc() {
		return versionDesc;
	}

	public void setVersionDesc(String versionDesc) {
		this.versionDesc = versionDesc;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

}
