package com.gsta.v2.response;

import java.util.List;

import com.gsta.v2.entity.AgentMemoinfo;

/**
 * com.gsta.response.RemarkResult
 * 
 * @author wubo
 * @date 2012-11-23
 * 
 */
public class RemarkResult extends BaseResult {

	/**
     * 2013-4-7
     */
    private static final long serialVersionUID = 1L;
    private List<AgentMemoinfo> memoInfo;

	public List<AgentMemoinfo> getMemoInfo() {
		return memoInfo;
	}

	public void setMemoInfo(List<AgentMemoinfo> memoInfo) {
		this.memoInfo = memoInfo;
	}

}
