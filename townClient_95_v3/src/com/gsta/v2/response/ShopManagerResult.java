package com.gsta.v2.response;

import java.util.List;

import com.gsta.v2.entity.ShopInfo;

/**
 * 
 * @author longxianwen
 * @createTime Apr 18, 2013 5:45:44 PM
 * @version: 1.0
 * @desc:店铺查询/TOP店铺查询返回结果类
 */
public class ShopManagerResult extends BaseResult {
	
	private Long tCount;  //满足条件记录总数
	private List<ShopInfo> list; //店铺商家列表
	public Long gettCount() {
		return tCount;
	}
	public void settCount(Long tCount) {
		this.tCount = tCount;
	}
	public List<ShopInfo> getList() {
		return list;
	}
	public void setList(List<ShopInfo> list) {
		this.list = list;
	}
}
