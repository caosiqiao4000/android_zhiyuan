package com.gsta.v2.response;

import java.io.Serializable;

/**
 * com.gsta.response.RegisterResult
 * 
 * @author wubo
 * @date 2012-12-28
 * 
 */
public class RegisterResult extends BaseResult implements Serializable {

	String uid;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

}
