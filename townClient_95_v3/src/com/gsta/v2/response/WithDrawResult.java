package com.gsta.v2.response;

import java.util.ArrayList;
import java.util.List;

import com.gsta.v2.entity.WithDrawInfo;

/**
 * @author fitch
 * @createtime 2013-4-7 下午2:02:16
 * @version 1.0
 * @desc 提现相关实体类
 */
public class WithDrawResult extends BaseResult {

	public Long tCount;
	public List<WithDrawInfo> list = new ArrayList<WithDrawInfo>();
	public AccountBaseInfo accountBaseInfo;
	public WithDrawAccountInfo withDrawAccountInfo;

	public Long gettCount() {
		return tCount;
	}

	public void settCount(Long tCount) {
		this.tCount = tCount;
	}

	public List<WithDrawInfo> getList() {
		return list;
	}

	public void setList(List<WithDrawInfo> list) {
		this.list = list;
	}

	public AccountBaseInfo getAccountBaseInfo() {
		return accountBaseInfo;
	}

	public void setAccountBaseInfo(AccountBaseInfo accountBaseInfo) {
		this.accountBaseInfo = accountBaseInfo;
	}

	public WithDrawAccountInfo getWithDrawAccountInfo() {
		return withDrawAccountInfo;
	}

	public void setWithDrawAccountInfo(WithDrawAccountInfo withDrawAccountInfo) {
		this.withDrawAccountInfo = withDrawAccountInfo;
	}

}
