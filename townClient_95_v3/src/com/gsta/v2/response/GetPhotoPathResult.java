package com.gsta.v2.response;

import java.io.Serializable;
import java.util.Map;

/**
 * com.gsta.response.GetPhotoPathResult
 * 
 * @author wubo
 * @date 2012-12-29
 * 
 */
public class GetPhotoPathResult extends BaseResult implements Serializable {

	Map<String, String> photoPath;

	public Map<String, String> getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(Map<String, String> photoPath) {
		this.photoPath = photoPath;
	}

}
