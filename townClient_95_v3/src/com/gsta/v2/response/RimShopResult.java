package com.gsta.v2.response;

import java.util.List;

import com.gsta.v2.entity.ShopInfo;

/**
 * 
 * @author longxianwen
 * @createTime Mar 26, 2013 5:19:10 PM
 * @version: 1.0
 * @desc:
 */
public class RimShopResult extends BaseResult {

	private List<ShopInfo> nears;

	public List<ShopInfo> getNears() {
		return nears;
	}

	public void setNears(List<ShopInfo> nears) {
		this.nears = nears;
	}
}