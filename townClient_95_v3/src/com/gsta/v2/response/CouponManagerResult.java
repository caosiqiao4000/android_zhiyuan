package com.gsta.v2.response;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fitch
 * @createtime 2013-3-31 下午8:37:32
 * @version 1.0
 * @desc
 */
public class CouponManagerResult extends BaseResult {

    private Integer used;// 已使用代金券数量
    private Integer unused;// 未使用代金券数量
    private Integer expired;// 过期代金券数量
    private List<CouponInfo> coupons = new ArrayList<CouponInfo>();// 代金券信息,可根据需要进行扩展添加
    private Long tcount;// 满足条件的记录总数

    // 激活代金券成功后、返回激活成功信息
    private String couponId; // 代金券主键ID
    private String coupon_Id; // 代金券编号
    private String end_date; // 有效期
    private Double couponMoney;// 代金券金额

    public Integer getUsed() {
        return used;
    }

    public void setUsed(Integer used) {
        this.used = used;
    }

    public Integer getUnused() {
        return unused;
    }

    public void setUnused(Integer unused) {
        this.unused = unused;
    }

    public Integer getExpired() {
        return expired;
    }

    public void setExpired(Integer expired) {
        this.expired = expired;
    }

    public List<CouponInfo> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<CouponInfo> coupons) {
        this.coupons = coupons;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCoupon_Id() {
        return coupon_Id;
    }

    public void setCoupon_Id(String coupon_Id) {
        this.coupon_Id = coupon_Id;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Double getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(Double couponMoney) {
        this.couponMoney = couponMoney;
    }

    public Long getTcount() {
        return tcount;
    }

    public void setTcount(Long tcount) {
        this.tcount = tcount;
    }

    /*
     * public class CouponInfo { private CouponInfo data = new CouponInfo();
     * 
     * public String getId() { return data.getId(); }
     * 
     * public void setId(String id) { this.data.setId(id); }
     * 
     * public String getExplanation() { return data.getExplanation(); }
     * 
     * public void setExplanation(String explanation) { this.data.setExplanation(explanation); }
     * 
     * public Double getMoney() { return data.getMoney(); }
     * 
     * public void setMoney(Double money) { this.data.setMoney(money); }
     * 
     * public String getEnd_date() { return data.getEnd_date(); }
     * 
     * public void setEnd_date(String end_date) { this.data.setEnd_date(end_date); }
     * 
     * public String getRequirement() { return data.getRequirement(); }
     * 
     * public void setRequirement(String requirement) { this.data.setRequirement(requirement); }
     * 
     * }
     */

}
