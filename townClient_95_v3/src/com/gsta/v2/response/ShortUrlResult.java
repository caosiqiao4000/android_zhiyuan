package com.gsta.v2.response;

/**
 *@author wubo
 *@createtime 2012-10-11
 */
public class ShortUrlResult extends BaseResult {

	private String shortUrl;

	public String getShortUrl() {
		return shortUrl;
	}

	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}

}
