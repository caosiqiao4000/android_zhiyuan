package com.gsta.v2.response;

import java.io.Serializable;

/**
 * com.gsta.response.ValidateCodeResult
 * 
 * @author wubo
 * @date 2012-12-27
 * 
 */
public class ValidateCodeResult extends BaseResult implements Serializable {

	String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
