package com.gsta.v2.response;

import java.util.List;

import com.gsta.v2.entity.WareSalesObject;

/**
 * com.gsta.v2.response.HotWareResult
 * 
 * @author wubo
 * @date 2013-1-23
 * 
 */
public class HotWareResult extends BaseResult {

	List<WareSalesObject> hotWare;

	public List<WareSalesObject> getHotWare() {
		return hotWare;
	}

	public void setHotWare(List<WareSalesObject> hotWare) {
		this.hotWare = hotWare;
	}

}
