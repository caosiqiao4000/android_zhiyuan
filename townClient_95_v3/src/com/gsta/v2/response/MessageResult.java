package com.gsta.v2.response;

import java.util.List;

import com.gsta.v2.entity.MessageInfo;

public class MessageResult extends BaseResult {

	private List<MessageInfo> message;

	public List<MessageInfo> getMessage() {
		return message;
	}

	public void setMessage(List<MessageInfo> message) {
		this.message = message;
	}

}
