package com.gsta.v2.response;

/**
 * @author zengxj
 * @createTime 2013-4-23
 * @version: 1.0
 * @desc: 提现账户信息
 */
public class WithDrawAccountInfo {
    
    private String accountId;// 账户ID
    private String paymentType;// 账户类型
    private String openBank;// 开户银行
    private String paymentAccountId;// 银行卡号
    private String province;// 开户行所在省份
    private String city;// 开户行所在城市
    private String branchBank;// 开户支行
    private String mobile;// 手机号码
    
    
    public String getAccountId() {
        return accountId;
    }
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    public String getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    public String getOpenBank() {
        return openBank;
    }
    public void setOpenBank(String openBank) {
        this.openBank = openBank;
    }
    public String getPaymentAccountId() {
        return paymentAccountId;
    }
    public void setPaymentAccountId(String paymentAccountId) {
        this.paymentAccountId = paymentAccountId;
    }
    public String getProvince() {
        return province;
    }
    public void setProvince(String province) {
        this.province = province;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getBranchBank() {
        return branchBank;
    }
    public void setBranchBank(String branchBank) {
        this.branchBank = branchBank;
    }
    public String getMobile() {
        return mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    

}
