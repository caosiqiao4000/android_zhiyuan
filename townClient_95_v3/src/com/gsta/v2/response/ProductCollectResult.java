package com.gsta.v2.response;

import java.util.List;

import com.gsta.v2.entity.GoodsInfo;

/**
 * 商品收藏Result
 * 
 * @author zengxj
 * @createTime 2013-3-29
 * @version: 1.0
 * @desc:
 */
public class ProductCollectResult extends BaseResult {

    private static final long serialVersionUID = 32609732281710823L;

    private List<GoodsInfo> concernProducts;// 商品收藏列表
    private Integer tcount;// 总数量

    public Integer getTcount() {
        return tcount;
    }

    public void setTcount(Integer tcount) {
        this.tcount = tcount;
    }

    public List<GoodsInfo> getConcernProducts() {
        return concernProducts;
    }

    public void setConcernProducts(List<GoodsInfo> concernProducts) {
        this.concernProducts = concernProducts;
    }

}
