package com.gsta.v2.response;

import java.util.List;

import com.gsta.v2.entity.DirectDeductDetail;
import com.gsta.v2.entity.IndirectDeductDetail;

public class DeductDetailResult extends BaseResult {
    /**
	 * 
	 */
    private static final long serialVersionUID = 5968124139658756718L;
    private Long count;// 当前返回的页数
    private Long tCount;// 满足条件的总页数

    private List<DirectDeductDetail> directDeduct;// 直接收益信息
    private List<IndirectDeductDetail> indirectDeduct;// 间接收益信息

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long gettCount() {
        return tCount;
    }

    public void settCount(Long tCount) {
        this.tCount = tCount;
    }

    public List<DirectDeductDetail> getDirectDeduct() {
        return directDeduct;
    }

    public void setDirectDeduct(List<DirectDeductDetail> directDeduct) {
        this.directDeduct = directDeduct;
    }

    public List<IndirectDeductDetail> getIndirectDeduct() {
        return indirectDeduct;
    }

    public void setIndirectDeduct(List<IndirectDeductDetail> indirectDeduct) {
        this.indirectDeduct = indirectDeduct;
    }

}
