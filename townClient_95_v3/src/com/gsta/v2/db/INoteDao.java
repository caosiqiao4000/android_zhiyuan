/**
 *
 * @author wubo
 * @date 2013-3-25
 * 
 */
package com.gsta.v2.db;

import com.gsta.v2.entity.NoteInfo;

/**
 * com.gsta.v2.db.INoteDao
 * 
 * @author wubo
 * @date 2013-3-25
 * 
 */
public interface INoteDao {

	/**
	 * 拿到录音文件地址
	 * 
	 * @author wubo
	 * @time 2013-3-25
	 * @param uid
	 * @param noteId
	 * @return
	 * 
	 */
	public NoteInfo getNote(String uid, String noteId);

	/**
	 * 关联录音文件地址
	 * 
	 * @author wubo
	 * @time 2013-3-25
	 * @param Notes
	 * @return
	 * 
	 */
	public boolean unionNote(NoteInfo note);

}
