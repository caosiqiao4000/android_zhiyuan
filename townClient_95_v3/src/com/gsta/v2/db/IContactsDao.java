package com.gsta.v2.db;

import java.util.List;

import com.gsta.v2.entity.Contacts;

public interface IContactsDao {

	/**
	 * 获取本地通讯录
	 * 
	 * @author wubo
	 * @createtime 2012-9-7
	 * @return
	 */
	public List<Contacts> getPhoneLocalContaces();

	public String getAllPhoneNumbers();

	/**
	 * 保存用户
	 * 
	 * @author wubo
	 * @createtime 2012-9-7
	 * @param uid
	 * @param contacts
	 */
	public void saveContacts(String uid, List<Contacts> contacts);

	/**
	 * 是否已经存在
	 * 
	 * @author wubo
	 * @createtime 2012-9-7
	 * @param phoneNum
	 * @return
	 */
	public boolean isExist(String phoneNum);

	/**
	 * 获取通讯录成员
	 * 
	 * @author wubo
	 * @createtime 2012-9-7
	 * @param uid
	 * @return
	 */
	public List<Contacts> getAllContacts(String uid);

	/**
	 * 删除通讯录成员
	 * 
	 * @author wubo
	 * @createtime 2012-9-7
	 * @param id
	 */
	public void deleteContact(String uid, String id);

}
