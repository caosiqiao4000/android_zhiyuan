package com.gsta.v2.db;

import com.gsta.v2.entity.PicUploadSavePathInfo;

/**
 * 当本地图片上传后保存与本地图片之间的联系
 * 
 * @author caosq 2013-6-17
 */
public interface IPicUpLoadAfterSavePath {

    /**
     * 保存上传图片后的数据库主键与手机图片路径间的关系
     * 
     * @author caosq 2013-6-17 下午8:54:13
     * @param id
     * @param sardPath
     *            可能是多个图片的地址 中间用";"隔开   更新手机图片保存地址
     */
    public void saveOrUpdataPicPathByID(PicUploadSavePathInfo sardPath);

    /**
     * 依据服务器的ID 查询
     * 
     * @author caosq 2013-6-17 下午8:59:09
     * @param id
     *            服务器的ID
     * @return
     */
    public PicUploadSavePathInfo getPicPathById(String id);

    /**
     * 依据服务器的删除的 图片ID 删除相关记录
     * 
     * @author caosq 2013-6-18 下午3:36:49
     * @param id
     */
    public void deleteByServerId(String id);

}
