package com.gsta.v2.db;

import java.util.List;

import com.gsta.v2.entity.IMBean;

/**
 * 私信管理
 * 
 * @author wubo
 * @createtime 2011-12-6 上午10:29:19
 */
public interface IMHistoryDao {

	/**
	 * 获取私信
	 * 
	 * @author wubo
	 * @createtime 2011-12-6 上午10:32:16
	 * @param myuid
	 *            登录用户uid
	 * @param himuid
	 *            聊天对象uid
	 * @return
	 */
	public List<IMBean> getUserMsgs(String myuid, String himuid);

	/**
	 * 添加私信
	 * 
	 * @author wubo
	 * @createtime 2011-12-6 上午10:34:50
	 * @param msg
	 */
	public long saveUserMsg(IMBean msg);

	/**
	 * 根据私信ID删除私信
	 * 
	 * @author wubo
	 * @createtime 2011-12-6 上午10:36:38
	 * @param id
	 * @return
	 */
	public boolean deleteUserMsgById(int id);

	/**
	 * 删除与某用户的私信
	 * 
	 * @author wubo
	 * @createtime 2011-12-6 上午10:38:22
	 * @param myuid
	 *            登录用户uid
	 * @param himuid
	 *            聊天对象uid
	 * @return
	 */
	public boolean deleteUserMsgByUid(String myuid, String himuid);

	/**
	 * 查找所有聊天对象
	 * 
	 * @author wubo
	 * @createtime 2011-12-7 下午04:37:12
	 * @param uid
	 * @return
	 */
	public List<IMBean> getUserMessages(String uid);

	/**
	 * 是否阅读状态
	 * 
	 * @author wubo
	 * @createtime 2012-9-10
	 * @param myuid
	 * @param himuid
	 */
	public void updateReadStatus(String myuid, String himuid);

	/**
	 * 聊天历史记录
	 * 
	 * @author wubo
	 * @createtime 2012-9-10
	 * @param myuid
	 * @param himuid
	 * @return
	 */
	public List<IMBean> getUserMsgsHistory(String myuid, String himuid);
}
