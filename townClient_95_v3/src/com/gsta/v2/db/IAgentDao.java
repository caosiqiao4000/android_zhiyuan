package com.gsta.v2.db;

import java.util.List;

import com.gsta.v2.entity.AgentInfo;

/**
 *@author wubo
 *@createtime 2012-9-6
 */
public interface IAgentDao {

	/**
	 * 保存下家
	 * 
	 * @author wubo
	 * @createtime 2012-9-6
	 * @param agentInfos
	 */
	public void saveAgents(String uid, List<AgentInfo> agentInfos);

	/**
	 * 获取下家
	 * 
	 * @author wubo
	 * @createtime 2012-9-6
	 * @return
	 */
	public List<AgentInfo> getAgents(String uid);
}
