package com.gsta.v2.db;

import java.util.List;

import com.gsta.v2.entity.City;

/**
 * @author wubo
 * @createtime 2012-9-1
 */
public interface ICityDao {

    /**
     * 根据城市名称查询下级城市
     * 
     * @author wubo
     * @createtime 2012-9-1
     * @param name
     * @return
     */
    public List<City> getChildCitybyName(String name);

    /**
     * 根据parentId查询下级城市
     * 
     * @author wubo
     * @createtime 2012-9-1
     * @param Id
     * @return
     */
    public List<City> getChildCitybyId(String parentId);

    /**
     * 保存城市信息
     * 
     * @author wubo
     * @createtime 2012-9-1
     * @param citys
     */
    public void saveAllCity(List<City> citys);

    /**
     * 根据ID查询城市名称
     * 
     * @author caosq 2013-4-28 下午2:37:26
     * @param citys
     */
    public City queryCityById(String id);
}
