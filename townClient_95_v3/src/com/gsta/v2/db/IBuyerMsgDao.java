package com.gsta.v2.db;

import java.util.List;

import com.gsta.v2.entity.MessageInfo;

/**
 *@author wubo
 *@createtime 2012-9-10
 */
public interface IBuyerMsgDao {

	/**
	 * 保存留言
	 * 
	 * @author wubo
	 * @createtime 2012-9-10
	 * @param infos
	 */
	public void saveBuyerMsgs(String uid, List<MessageInfo> infos);

	/**
	 * 查询留言
	 * 
	 * @author wubo
	 * @createtime 2012-9-10
	 * @param uid
	 * @param himuid
	 */
	public List<MessageInfo> getBuyerMsgs(String uid, String himuid);

	/**
	 * 保存一条留言
	 * 
	 * @author wubo
	 * @createtime 2012-9-11
	 * @param uid
	 * @param msg
	 */
	public void saveBuyerMsg(String uid, MessageInfo msg);

	/**
	 * 根据ID删除信息
	 * 
	 * @author wubo
	 * @createtime 2012-9-12
	 * @param id
	 */
	public void deleteById(Long id);

	/**
	 * 删除此人所有留言
	 * 
	 * @author wubo
	 * @createtime 2012-9-12
	 * @param uid
	 * @param himuid
	 */
	public void deleteAll(String myuid, String himuid);
}
