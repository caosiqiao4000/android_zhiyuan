package com.gsta.v2.db.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.gsta.v2.db.IPicUpLoadAfterSavePath;
import com.gsta.v2.entity.PicUploadSavePathInfo;

public class PicUpLoadSavePathDao implements IPicUpLoadAfterSavePath {
    private DBAdapter mdb;
    private Context context;

    public PicUpLoadSavePathDao(Context context) {
        this.context = context;
    }

    @Override
    public void saveOrUpdataPicPathByID(PicUploadSavePathInfo sardPath) {
        if (sardPath.getPicNavitePath() == null) {
            return;
        }
        Cursor mCursor = null;
        try {
            mdb = new DBAdapter(context);
            mdb.open();

            String sql = "select " + mdb.PATH_DB_ID + " from " + mdb.TABLE_PIC_PATH + " where " + mdb.SERVER_DB_ID + " = ?";
            mCursor = mdb.rawQuery(sql, new String[] { sardPath.getServer_Db_Id() });
            if (mCursor.getCount() > 0) { // 更新
                // 这里更改所有 ,应该要加限制条件
                sql = "update " + mdb.TABLE_PIC_PATH + " set " + mdb.PIC_NATIVE_PATH
                        + " = ? ," + mdb.PIC_CHANGE_TIME + " = ? " + "where " + mdb.SERVER_DB_ID + " =? ";
                mdb.deleteOrUpdate(sql, new String[] { sardPath.getPicNavitePath(), sardPath.getPicChangeTime(),
                        sardPath.getServer_Db_Id() });
            } else {
                ContentValues cv = new ContentValues();
                cv.put(mdb.SERVER_DB_ID, sardPath.getServer_Db_Id());
                cv.put(mdb.PIC_NATIVE_PATH, sardPath.getPicNavitePath());
                cv.put(mdb.PIC_CHANGE_TIME, sardPath.getPicChangeTime());

                mdb.insertData(mdb.TABLE_PIC_PATH, cv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
        }
    }

    @Override
    public PicUploadSavePathInfo getPicPathById(String id) {
        PicUploadSavePathInfo info = new PicUploadSavePathInfo();
        Cursor mCursor = null;
        try {
            mdb = new DBAdapter(context);
            mdb.open();
            final String selectSQL = "select * from " + mdb.TABLE_PIC_PATH + " where " + mdb.SERVER_DB_ID + "= ? ";
            mCursor = mdb.rawQuery(selectSQL, new String[] { id });
            if (null != mCursor && mCursor.getCount() > 0) {
                mCursor.moveToFirst();
                info.setPicNavitePath(mCursor.getString(mCursor.getColumnIndex(mdb.PIC_NATIVE_PATH)));
                info.setPicChangeTime(mCursor.getString(mCursor
                        .getColumnIndex(mdb.PIC_CHANGE_TIME)));
                info.setServer_Db_Id(mCursor.getString(mCursor.getColumnIndex(mdb.SERVER_DB_ID)));
               
            }
          
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
            
        }
        return info;
    }

    @Override
    public void deleteByServerId(String id) {
        try {
            mdb = new DBAdapter(this.context);
            mdb.open();
            String sql = "delete from " + mdb.TABLE_PIC_PATH + " where "
                    + mdb.SERVER_DB_ID + " =?";
            mdb.deleteOrUpdate(sql, new Object[] { id });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
        }
    }
}
