/**
 *
 * com.gsta.v2.db.impl.NoteDaoImpl
 * @author boge
 * @date 2013-3-25
 * 
 */

package com.gsta.v2.db.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.gsta.v2.db.INoteDao;
import com.gsta.v2.entity.NoteInfo;

/**
 * 
 * 营销笔记
 * @author boge
 * @time 2013-3-25
 * 
 */
public class NoteDaoImpl implements INoteDao {

	private DBAdapter mdb;
	private Context context;

	public NoteDaoImpl(Context context) {
		this.context = context;
	}

	/**
	 * 
	 * @author boge
	 * @time 2013-3-25
	 * @param uid
	 * @param noteId
	 * @return
	 * 
	 */
	@Override
	public NoteInfo getNote(String uid, String noteId) {
		Cursor mCursor = null;
		try {
			mdb = new DBAdapter(this.context);
			mdb.open();
			String sql = "SELECT * FROM " + mdb.TABLE_NOTE + " WHERE "
					+ mdb.MYUID + " = ? and " + mdb.NOTE_ID + " = ? ";
			mCursor = mdb.select(sql, new String[] { uid, noteId });
			if (mCursor == null || mCursor.getCount() < 1) {
				return null;
			}
			if (mCursor.moveToFirst()) {
				NoteInfo msg = new NoteInfo();
				msg
						.setUid(mCursor.getString(mCursor
								.getColumnIndex(mdb.MYUID)));
				msg.setNoteId(mCursor.getString(mCursor
						.getColumnIndex(mdb.NOTE_ID)));
				msg.setFilePath(mCursor.getString(mCursor
						.getColumnIndex(mdb.NOTE_FILEPATH)));
				return msg;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mdb != null) {
				if (mdb.mSQLiteDatabase != null) {
					mdb.mSQLiteDatabase.close();
				}
				mdb.close();
			}
		}
	}

	/**
	 * 
	 * @author boge
	 * @time 2013-3-25
	 * @param voicePaths
	 * @return
	 * 
	 */
	@Override
	public boolean unionNote(NoteInfo note) {
		try {
			mdb = new DBAdapter(this.context);
			mdb.open();
			Cursor mCursor = null;
			String sql = "SELECT * FROM " + mdb.TABLE_NOTE + " WHERE "
					+ mdb.MYUID + " = ? and " + mdb.NOTE_ID + " = ? ";
			mCursor = mdb.select(sql, new String[] { note.getUid(),
					note.getNoteId() });
			if (mCursor != null && mCursor.moveToFirst()) {
				String sql2 = "DELETE FROM " + mdb.TABLE_NOTE + " WHERE "
						+ mdb.MYUID + " = ? and " + mdb.NOTE_ID + " = ? ";
				mdb.deleteOrUpdate(sql2, new String[] { note.getUid(),
						note.getNoteId() });
			}
			ContentValues cv = new ContentValues();
			cv.put(mdb.MYUID, note.getUid());
			cv.put(mdb.NOTE_ID, note.getNoteId());
			cv.put(mdb.NOTE_FILEPATH, note.getFilePath());
			mdb.insertData(mdb.TABLE_NOTE, cv);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (mdb != null) {
				if (mdb.mSQLiteDatabase != null) {
					mdb.mSQLiteDatabase.close();
				}
				mdb.close();
			}
		}
	}

}
