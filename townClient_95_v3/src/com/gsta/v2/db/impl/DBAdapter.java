package com.gsta.v2.db.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter {

    // 数据库名称
    private final String DB_NAME = "gsta.db";
    // 数据库版本
    private final int DB_VERSION = 2;

    private Context mContext = null;
    public SQLiteDatabase mSQLiteDatabase = null;
    public DatabaseHelper mDatabaseHelper = null;
    public final String MYUID = "my_uid";

    /**
     * 联系人表
     */
    public final String TABLE_CONTACTS = "ContactsTable";
    public final String CONTACTS_SID = "_id";
    public final String CONTACTS_ID = "contact_id";
    public final String CONTACTS_USERNAME = "username";
    public final String CONTACTS_PHONENUM = "phonenum";
    public final String CONTACTS_SORTKEY = "sortkey";
    public final String CONTACTS_FIRSTLETTER = "firstLetter";

    /**
     * 地区表
     */
    public final String TABLE_CITY = "CityTable"; // 表名
    public final String CITY_CID = "_id";
    public final String CITY_ID = "id";
    public final String CITY_PARENTID = "parentid";
    public final String CITY_NAME = "name";

    /**
     * 代理商表
     */
    public final String TABLE_AGENT = "AgentTable";
    public final String AGETN_ID = "_id";
    public final String AGENT_UID = "agent_uid";// 代理商唯一标识
    public final String AGENT_NAME = "agent_name";// 代理商名称
    public final String AGENT_CITYID = "agent_cityId";// 地市
    public final String AGENT_GRADE = "agent_grade";// 等级
    public final String AGENT_USERS = "agent_users";// 发展用户数
    public final String AGENT_GOODS = "agent_goods";// 销售商品数
    public final String AGENT_USERNAME = "agent_userName";// 代理商姓名
    public final String AGENT_MOBILE = "agent_mobile";// 手机
    public final String AGENT_PHONE = "agent_phone";// 固话
    public final String AGENT_EMAIL = "agent_email";// 邮箱
    public final String AGENT_QQ = "agent_qq";// qq
    public final String AGENT_ZIPCODE = "agent_zipCode";// 邮编
    public final String AGENT_ADDRESS = "agent_address";// 联系地址

    /**
     * IM记录表
     */
    public final String TABLE_IM = "IMTable";
    public final String IM_ID = "_id";
    public final String IM_HISUSERID = "im_hisuserid";// 用户id
    public final String IM_USERNAME = "im_username";// 自己使用的用户名
    public final String IM_HISNAME = "im_hisName";// 对方的用户名
    public final String IM_HISPHOTO = "im_hisPhoto";// 对方的用户名
    public final String IM_CONTENT = "im_content"; // 信息的内容
    public final String IM_TIME = "im_time"; // 发信息的时间
    public final String IM_DIRECTION = "im_direction"; // 谁发的信息(0表示登陆者,1表示聊天对象)
    public final String IM_USERTYPE = "im_usertype";// 用户类型
    public final String IM_SENDED = "im_sended";// 是否发送成功(0失败,1成功);
    public final String IM_FLAG = "im_flag";// 是否阅读(0未读,1已读)

    /**
     * 买家留言表
     */
    public final String TABLE_BUYERMSG = "buyermsgTable";
    public final String BUYERMSG_ID = "buyermsg_id";// id
    public final String BUYERMSG_HIMUID = "buyermsg_himuid";// 买家的uid
    public final String BUYERMSG_TIME = "buyermsg_time";// 留言时间
    public final String BUYERMSG_CONTENT = "buyermsg_content";// 留言内容
    public final String BUYERMSG_DIRECTION = "buyermsg_direction";// 谁发的信息(0表示登陆者,1表示我的买家)

    /**
     * 营销备忘录录音文件地址
     */
    public final String TABLE_NOTE = "noteTable";
    public final String NOTE_ID = "note_id";// 营销备忘录编号
    public final String NOTE_FILEPATH = "note_filepath";// 文件地址

    /**
     * 程序中使用过的上传的图片 在手机中的位置关系保存下来
     * <p>
     * 在加载图片时,优先使用手机里的图片,不从网络加载
     */
    public final String TABLE_PIC_PATH = "picPathTable"; // 文件地址
    public final String PATH_DB_ID = "path_db_id"; // 主键ID
    public final String SERVER_DB_ID = "server_db_id"; // 服务器对应的主键ID
    public final String PIC_NATIVE_PATH = "picNativePath"; // 更改的图片在手机中的地址
    public final String PIC_CHANGE_TIME = "buyermsg_time"; // 更改的时间

    /**
     * 创建联系人表
     */
    private String SQL_CREATE_CONTACT_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_CONTACTS + " (" + CONTACTS_SID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + CONTACTS_ID + " INTEGER,"
            + MYUID + " VARCHAR(15)," + CONTACTS_USERNAME + " VARCHAR(15),"
            + CONTACTS_PHONENUM + " VARCHAR(60)," + CONTACTS_SORTKEY
            + " VARCHAR(15)," + CONTACTS_FIRSTLETTER + " VARCHAR(15))";

    /**
     * 创建城市表
     */
    private String SQL_CREATE_CITY_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_CITY + " (" + CITY_CID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + CITY_ID + " VARCHAR(20),"
            + CITY_PARENTID + " VARCHAR(20)," + CITY_NAME + " VARCHAR(50))";

    /**
     * 创建下家代理表
     */
    private String SQL_CREATE_AGENT_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_AGENT + " (" + AGETN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + MYUID + " VARCHAR(20),"
            + AGENT_UID + " VARCHAR(20)," + AGENT_NAME + " VARCHAR(20),"
            + AGENT_CITYID + " VARCHAR(20)," + AGENT_GRADE + " VARCHAR(20),"
            + AGENT_USERS + " VARCHAR(20)," + AGENT_GOODS + " VARCHAR(20),"
            + AGENT_USERNAME + " VARCHAR(20)," + AGENT_MOBILE + " VARCHAR(20),"
            + AGENT_PHONE + " VARCHAR(20)," + AGENT_EMAIL + " VARCHAR(20),"
            + AGENT_ZIPCODE + " VARCHAR(20)," + AGENT_QQ + " VARCHAR(20),"
            + AGENT_ADDRESS + " VARCHAR(500))";

    /**
     * 创建IM表
     * 
     * @author wubo
     * @createtime 2012-9-8
     * @param context
     */
    private String SQL_CREATE_IM_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_IM + " (" + IM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + MYUID + " VARCHAR(20)," + IM_HISUSERID + " VARCHAR(20),"
            + IM_USERNAME + " VARCHAR(20)," + IM_HISNAME + " VARCHAR(20)," + IM_HISPHOTO + " VARCHAR(40)," + IM_CONTENT + " VARCHAR(20),"
            + IM_TIME + " VARCHAR(20)," + IM_DIRECTION + " VARCHAR(20),"
            + IM_FLAG + " VARCHAR(20)," + IM_SENDED + " VARCHAR(20),"
            + IM_USERTYPE + " VARCHAR(20))";

    /**
     * 创建买家留言表
     */
    private String SQL_CREATE_BUYERMSG_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_BUYERMSG + " (" + BUYERMSG_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + MYUID + " VARCHAR(20),"
            + BUYERMSG_HIMUID + " VARCHAR(20)," + BUYERMSG_TIME
            + " VARCHAR(20)," + BUYERMSG_DIRECTION + " VARCHAR(20),"
            + BUYERMSG_CONTENT + " VARCHAR(20))";

    /**
     * 创建手机本地图片与上传图片的关系
     */
    private String SQL_CREATE_PicNativePath_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_PIC_PATH + " (" + PATH_DB_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + SERVER_DB_ID + " VARCHAR(40),"
            + PIC_NATIVE_PATH + " VARCHAR(300)," + BUYERMSG_TIME
            + " VARCHAR(20))";

    private String SQL_CREATE_FLOW_TABLE = "create table if not exists flowTB (_id integer primary key autoincrement,mobile text not null,imsi text not null,exeid integer not null,exename text not null,exeimage text not null,starttime datetime not null,endtime datetime not null,mode text not null,type text not null,count real not null,status integer not null,isup integer not null);";

    /**
     * 创建营销备忘录录音文件地址表
     * 
     * @param context
     */
    private String SQL_CREATE_NOTE_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NOTE + " (" + MYUID + " VARCHAR(20), " + NOTE_ID
            + " VARCHAR(20)," + NOTE_FILEPATH + " VARCHAR(20));";

    public DBAdapter(Context context) {
        mContext = context;
    }

    private class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);

        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_CONTACT_TABLE);
            db.execSQL(SQL_CREATE_CITY_TABLE);
            db.execSQL(SQL_CREATE_AGENT_TABLE);
            db.execSQL(SQL_CREATE_IM_TABLE);
            db.execSQL(SQL_CREATE_BUYERMSG_TABLE);
            db.execSQL(SQL_CREATE_FLOW_TABLE);
            db.execSQL(SQL_CREATE_NOTE_TABLE);
            db.execSQL(SQL_CREATE_PicNativePath_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            // alert table 原表A to 临时表temp_A
            // create 表A
            // insert into A select from temp_A
        }
    }

    /**
     * 数据库升级
     * 
     * @author wubo
     * @createtime 2012-9-12
     */
    public void updateTable() {

    }

    // 打开数据库
    public synchronized void open() {
        try {
            mDatabaseHelper = new DatabaseHelper(mContext);
            if (mSQLiteDatabase == null) {
                mSQLiteDatabase = mDatabaseHelper.getWritableDatabase();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    // 关闭数据库
    public void close() {
        if (mDatabaseHelper != null) {
            mDatabaseHelper.close();
        }
    }

    public Cursor rawQuery(String sql) {
        Cursor mCursor = null;
        try {
            mCursor = mSQLiteDatabase.rawQuery(sql, null);
        } catch (Exception e) {
        }
        return mCursor;
    }

    public Cursor rawQuery(String sql, String[] value) {
        Cursor mCursor = null;
        try {
            mCursor = mSQLiteDatabase.rawQuery(sql, value);
        } catch (Exception e) {
        }
        return mCursor;
    }

    // 插入一条数据
    public long insertData(String table, ContentValues initialValues) {
        return mSQLiteDatabase.insert(table, "_id", initialValues);
    }

    // 通过Cursor查询所有指定字段的数据
    public Cursor getAllDatas(String table, String[] keyString) {
        return mSQLiteDatabase.query(table, keyString, null, null, null, null,
                null);
    }

    // 通过Cursor查询所有指定字段的数据
    public Cursor getDatasbyLimit(String table, String[] keyString,
            String limitString) {
        return mSQLiteDatabase.query(table, keyString, null, null, null, null,
                null, limitString);
    }

    // 按条件，通过Cursor查询指定字段的数据
    public Cursor getAllDatas(String table, String[] keyString, String key,
            String value) {
        Cursor mCursor = null;
        try {
            mCursor = mSQLiteDatabase.query(true, table, keyString, key + "='"
                    + value + "'", null, null, null, null, null);
            if (mCursor != null) {
                mCursor.moveToFirst();
            }
        } catch (Exception e) {
        }
        return mCursor;
    }

    // 通过Cursor查询所有数据
    public Cursor getAllDatas(String table) {
        String selectSQL = "select * from " + table;
        Cursor mCursor = null;
        try {
            mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
        } catch (Exception e) {
        }
        return mCursor;
    }

    // 通过Cursor查询所有数据
    public Cursor getAllDatas1(String table, String order) {
        String selectSQL = "select * from " + table + " Order By " + order;
        Cursor mCursor = null;
        try {
            mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
        } catch (Exception e) {
        }
        return mCursor;
    }

    // 按条件，通过Cursor查询数据
    public Cursor getAllDatas(String table, String wherekey, String wherevalue) {
        String selectSQL = "select * from " + table + " where " + wherekey
                + " = '" + wherevalue + "'";
        Cursor mCursor = null;
        try {
            mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
        } catch (Exception e) {
        }
        return mCursor;
    }

    // 按条件，通过Cursor查询数据
    public Cursor getAllDatas(String table, String wherekey, String wherevalue,
            String orderby) {
        String selectSQL = "Select * From " + table + " Where " + wherekey
                + " = '" + wherevalue + "' Order By " + orderby;
        Cursor mCursor = null;
        try {
            mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
        } catch (Exception e) {
        }
        return mCursor;
    }

    // 按条件，通过Cursor查询数据
    public Cursor getAllDatasexcept(String table, String wherekey,
            String wherevalue, String order) {
        String selectSQL = "select * from " + table + " where " + wherekey
                + " != '" + wherevalue + "' Order By " + order;
        Cursor mCursor = null;
        try {
            mCursor = mSQLiteDatabase.rawQuery(selectSQL, null);
        } catch (Exception e) {
        }
        return mCursor;
    }

    /**
     * 根据城市父ID查询城市
     * 
     * @author wubo
     * @createtime 2012-9-6
     * @param parentId
     * @return
     */
    public Cursor getChildCitybyId(String parentId) {
        String sql = "select * from " + TABLE_CITY + " where " + CITY_PARENTID
                + " = ? ";
        Cursor cursor = null;
        try {
            cursor = mSQLiteDatabase.rawQuery(sql, new String[] { parentId });
        } catch (Exception e) {
            return cursor;
        }
        return cursor;
    }

    /**
     * 获取我的下家
     * 
     * @author wubo
     * @createtime 2012-9-6
     * @param uid
     * @return
     */
    public Cursor getAgents(String uid) {

        String sql = "select * from " + TABLE_AGENT + " where " + MYUID + "=?";
        Cursor cursor = null;
        try {
            cursor = mSQLiteDatabase.rawQuery(sql, new String[] { uid });
        } catch (Exception e) {
            return cursor;
        }
        return cursor;
    }

    /**
     * 删,改
     * 
     * @author wubo
     * @createtime 2012-9-8
     * @param sql
     * @param values
     */
    public void deleteOrUpdate(String sql, Object[] values) {
        mSQLiteDatabase.execSQL(sql, values);
    }

    /**
     * 查询
     * 
     * @author wubo
     * @createtime 2012-9-8
     * @param sql
     * @param values
     * @return
     */
    public Cursor select(String sql, String[] values) {
        Cursor mCursor = null;
        try {
            mCursor = mSQLiteDatabase.rawQuery(sql, values);
        } catch (Exception e) {
            return null;
        }
        return mCursor;
    }

}
