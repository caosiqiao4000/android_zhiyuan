package com.gsta.v2.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.gsta.v2.db.IBuyerMsgDao;
import com.gsta.v2.entity.MessageInfo;

/**
 * 卖家留言
 * 
 * @author wubo
 *@createtime 2012-9-10
 */
public class BuyerMsgDaoImpl implements IBuyerMsgDao {

	private DBAdapter mdb;
	private Context context;

	public BuyerMsgDaoImpl(Context context) {
		this.context = context;
	}

	@Override
	public List<MessageInfo> getBuyerMsgs(String uid, String himuid) {
		// TODO Auto-generated method stub
		List<MessageInfo> msgs = new ArrayList<MessageInfo>();
		Cursor mCursor = null;
		try {
			mdb = new DBAdapter(this.context);
			mdb.open();
			String sql = "SELECT * FROM " + mdb.TABLE_BUYERMSG + " WHERE "
					+ mdb.MYUID + " = ? and " + mdb.BUYERMSG_HIMUID
					+ " =? ORDER BY " + mdb.BUYERMSG_TIME + " desc ";
			mCursor = mdb.select(sql, new String[] { uid, himuid });
			if (mCursor == null || mCursor.getCount() < 1) {
				return msgs;
			}
			for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor
					.moveToNext()) {
				MessageInfo msg = new MessageInfo();
				msg.setId(mCursor.getString(mCursor
						.getColumnIndex(mdb.BUYERMSG_ID)));
				msg.setSentUid(mCursor.getString(mCursor
						.getColumnIndex(mdb.BUYERMSG_HIMUID)));// 登录用户的uid
				msg.setSentTime(mCursor.getString(mCursor
						.getColumnIndex(mdb.BUYERMSG_TIME)));// 用户名
				msg.setContent(mCursor.getString(mCursor
						.getColumnIndex(mdb.BUYERMSG_CONTENT))); // 信息的内容
				msg.setDir(mCursor.getString(mCursor
						.getColumnIndex(mdb.BUYERMSG_DIRECTION)));
				msgs.add(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mdb != null) {
				if (mdb.mSQLiteDatabase != null) {
					mdb.mSQLiteDatabase.close();
				}
				mdb.close();
			}
		}
		return msgs;
	}

	@Override
	public void saveBuyerMsgs(String uid, List<MessageInfo> infos) {
		// TODO Auto-generated method stub
		try {
			mdb = new DBAdapter(this.context);
			mdb.open();
			mdb.mSQLiteDatabase.beginTransaction();
			MessageInfo msg;
			for (int i = 0; i < infos.size(); i++) {
				msg = infos.get(i);
				ContentValues cv = new ContentValues();
				cv.put(mdb.MYUID, uid);
				cv.put(mdb.BUYERMSG_HIMUID, msg.getSentUid());
				cv.put(mdb.BUYERMSG_CONTENT, msg.getContent());
				cv.put(mdb.BUYERMSG_TIME, msg.getSentTime());
				cv.put(mdb.BUYERMSG_DIRECTION, "1");
				mdb.insertData(mdb.TABLE_BUYERMSG, cv);
			}
			mdb.mSQLiteDatabase.setTransactionSuccessful();
			mdb.mSQLiteDatabase.endTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {
			if (mdb != null) {
				if (mdb.mSQLiteDatabase != null) {
					mdb.mSQLiteDatabase.close();
				}
				mdb.close();
			}
		}
	}

	@Override
	public void saveBuyerMsg(String uid, MessageInfo msg) {
		// TODO Auto-generated method stub
		try {
			mdb = new DBAdapter(this.context);
			mdb.open();
			ContentValues cv = new ContentValues();
			cv.put(mdb.MYUID, uid);
			cv.put(mdb.BUYERMSG_HIMUID, msg.getSentUid());
			cv.put(mdb.BUYERMSG_CONTENT, msg.getContent());
			cv.put(mdb.BUYERMSG_TIME, msg.getSentTime());
			cv.put(mdb.BUYERMSG_DIRECTION, "0");
			mdb.insertData(mdb.TABLE_BUYERMSG, cv);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {
			if (mdb != null) {
				if (mdb.mSQLiteDatabase != null) {
					mdb.mSQLiteDatabase.close();
				}
				mdb.close();
			}
		}
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		try {
			mdb = new DBAdapter(this.context);
			mdb.open();
			String sql = "delete from " + mdb.TABLE_BUYERMSG + " where "
					+ mdb.BUYERMSG_ID + " =?";
			mdb.deleteOrUpdate(sql, new Object[] { id });
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mdb != null) {
				if (mdb.mSQLiteDatabase != null) {
					mdb.mSQLiteDatabase.close();
				}
				mdb.close();
			}
		}
	}

	@Override
	public void deleteAll(String myuid, String himuid) {
		// TODO Auto-generated method stub
		try {
			mdb = new DBAdapter(this.context);
			mdb.open();
			String sql = "delete from " + mdb.TABLE_BUYERMSG + " where "
					+ mdb.MYUID + " =? and " + mdb.BUYERMSG_HIMUID + " =?";
			mdb.deleteOrUpdate(sql, new Object[] { myuid, himuid });
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mdb != null) {
				if (mdb.mSQLiteDatabase != null) {
					mdb.mSQLiteDatabase.close();
				}
				mdb.close();
			}
		}
	}

}
