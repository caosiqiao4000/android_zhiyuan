package com.gsta.v2.db.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.gsta.v2.db.IAgentDao;
import com.gsta.v2.entity.AgentInfo;

/**
 * 代理商信息data
 * 
 * @author wubo
 *@createtime 2012-9-6
 */
public class AgentDaoImpl implements IAgentDao {

	public DBAdapter db;
	private Context context;

	public AgentDaoImpl(Context context) {
		this.context = context;
	}

	/**
	 * 根据UID得到用户的代理商信息
	 * 
	 * @author wubo
	 * @time 2013-3-19
	 * @param uid
	 * @return
	 * 
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gsta.v2.db.IAgentDao#getAgents(java.lang.String)
	 */
	@Override
	public List<AgentInfo> getAgents(String uid) {
		List<AgentInfo> agents = new ArrayList<AgentInfo>();
		Cursor cursor = null;
		try {
			db = new DBAdapter(context);
			db.open();
			cursor = db.getAgents(uid);
			if (cursor == null)
				return agents;
			if (cursor.getCount() == 0)
				return agents;

			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor
					.moveToNext()) {
				AgentInfo agentInfo = new AgentInfo();
				agentInfo.setUid(cursor.getString(cursor
						.getColumnIndex(db.AGENT_UID)));
				agentInfo.setName(cursor.getString(cursor
						.getColumnIndex(db.AGENT_NAME)));
				agentInfo.setCityId(cursor.getString(cursor
						.getColumnIndex(db.AGENT_CITYID)));
				agentInfo.setGrade(cursor.getString(cursor
						.getColumnIndex(db.AGENT_GRADE)));
				agentInfo.setUsers(cursor.getInt(cursor
						.getColumnIndex(db.AGENT_USERS)));
				agentInfo.setGoods(cursor.getInt(cursor
						.getColumnIndex(db.AGENT_GOODS)));
				agentInfo.setUserName(cursor.getString(cursor
						.getColumnIndex(db.AGENT_USERNAME)));
				agentInfo.setMobile(cursor.getString(cursor
						.getColumnIndex(db.AGENT_MOBILE)));
				agentInfo.setPhone(cursor.getString(cursor
						.getColumnIndex(db.AGENT_PHONE)));
				agentInfo.setEmail(cursor.getString(cursor
						.getColumnIndex(db.AGENT_EMAIL)));
				agentInfo.setQq(cursor.getString(cursor
						.getColumnIndex(db.AGENT_QQ)));
				agentInfo.setZipCode(cursor.getString(cursor
						.getColumnIndex(db.AGENT_ZIPCODE)));
				agentInfo.setAddress(cursor.getString(cursor
						.getColumnIndex(db.AGENT_ADDRESS)));
				agents.add(agentInfo);
			}
			return agents;

		} catch (Exception e) {
			return agents;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				if (db.mSQLiteDatabase != null) {
					db.mSQLiteDatabase.close();
				}
				db.close();
			}
		}
	}

	@Override
	public void saveAgents(String uid, List<AgentInfo> agentInfos) {
		// TODO Auto-generated method stub
		try {
			db = new DBAdapter(context);
			db.open();
			db.mSQLiteDatabase.beginTransaction();
			for (int i = 0; i < agentInfos.size(); i++) {
				AgentInfo agentInfo = agentInfos.get(i);
				Log.e("MeetRoom", "ing:" + new Date().getTime());
				ContentValues cv = new ContentValues();
				cv.put(db.MYUID, uid);
				cv.put(db.AGENT_UID, agentInfo.getUid());
				cv.put(db.AGENT_NAME, agentInfo.getName());
				cv.put(db.AGENT_CITYID, agentInfo.getCityId());
				cv.put(db.AGENT_GRADE, agentInfo.getGrade());
				cv.put(db.AGENT_USERS, agentInfo.getUsers());
				cv.put(db.AGENT_GOODS, agentInfo.getGoods());
				cv.put(db.AGENT_USERNAME, agentInfo.getUserName());
				cv.put(db.AGENT_MOBILE, agentInfo.getMobile());
				cv.put(db.AGENT_PHONE, agentInfo.getPhone());
				cv.put(db.AGENT_EMAIL, agentInfo.getEmail());
				cv.put(db.AGENT_QQ, agentInfo.getQq());
				cv.put(db.AGENT_ZIPCODE, agentInfo.getZipCode());
				cv.put(db.AGENT_ADDRESS, agentInfo.getAddress());
				db.insertData(db.TABLE_AGENT, cv);
			}
			db.mSQLiteDatabase.setTransactionSuccessful();
			db.mSQLiteDatabase.endTransaction();
			Log.e("my", "end:" + new Date().getTime());
			new SharedPreferencesUtil(context).saveString(
					SharedPreferencesUtil.CITY_LOAD, "succ");
		} catch (Exception e) {
			return;
		} finally {
			if (db != null) {
				if (db.mSQLiteDatabase != null) {
					db.mSQLiteDatabase.close();
				}
				db.close();
			}
		}
	}
}
