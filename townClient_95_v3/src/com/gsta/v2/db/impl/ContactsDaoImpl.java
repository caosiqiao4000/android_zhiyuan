package com.gsta.v2.db.impl;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.TextUtils;

import com.gsta.v2.db.IContactsDao;
import com.gsta.v2.entity.Contacts;
import com.gsta.v2.util.Util;

/**
 * 本地通讯录
 * 
 * @author boge
 * @time 2013-3-27
 * 
 */
public class ContactsDaoImpl implements IContactsDao {
	private DBAdapter mDataBaseAdapter;
	private Context mContext;
	private ContentResolver resolver;
	static final Uri phoneuri = Uri
			.parse("content://com.android.contacts/data/phones");
	private static final String[] PHONES_PROJECTION = new String[] {
			Phone.CONTACT_ID, Phone.DISPLAY_NAME, Phone.NUMBER, "sort_key" };

	public ContactsDaoImpl(Context context) {
		this.mContext = context;
		this.resolver = this.mContext.getContentResolver();
	}

	@Override
	public List<Contacts> getPhoneLocalContaces() {

		Cursor phoneCursor = null;
		List<Contacts> contacts = new ArrayList<Contacts>();
		try {

			phoneCursor = resolver.query(phoneuri, PHONES_PROJECTION, null,
					null, "sort_key COLLATE LOCALIZED asc");

			if (phoneCursor != null) {
				while (phoneCursor.moveToNext()) {
					Contacts contact = new Contacts();
					// 得到手机号码
					String phoneNumber = phoneCursor.getString(phoneCursor
							.getColumnIndex(Phone.NUMBER));

					// 当手机号码为空的或者为空字段 跳过当前循环
					if (TextUtils.isEmpty(phoneNumber))
						continue;

					// 得到联系人名称
					String contactName = phoneCursor.getString(phoneCursor
							.getColumnIndex(Phone.DISPLAY_NAME));
					// 得到联系人ID
					Long contactid = phoneCursor.getLong(phoneCursor
							.getColumnIndex(Phone.CONTACT_ID));
					// 排序key
					String sortkey = phoneCursor.getString(phoneCursor
							.getColumnIndex("sort_key"));
					if (phoneNumber.contains("+86")) {
						phoneNumber = phoneNumber.substring(3, phoneNumber
								.length());
					}
					contact.setUserName(contactName);
					contact.setPhoneNum(phoneNumber);
					contact.setContactId(contactid);
					contact.setSortkey(sortkey);

					// 当前汉语拼音首字母
					String currentStr = Util.getAlpha(sortkey);
					contact.setFirstLetter(currentStr);

					contacts.add(contact);
				}
				phoneCursor.close();
			}
		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			if (phoneCursor != null)
				phoneCursor.close();
		}
		return contacts;
	}

	@Override
	public String getAllPhoneNumbers() {
		// TODO Auto-generated method stub
		StringBuffer allPhoneNumbers = new StringBuffer("");
		Cursor phones = null;
		try {
			phones = resolver.query(phoneuri, PHONES_PROJECTION, null, null,
					"sort_key COLLATE LOCALIZED asc");
			if (phones != null) {
				phones.moveToFirst();
				while (phones.moveToNext()) {
					// 得到联系人名称
					String phoneNumber = phones.getString(phones
							.getColumnIndex(Phone.NUMBER));
					if (phoneNumber.contains("+86")) {
						phoneNumber = phoneNumber.substring(3, phoneNumber
								.length());
					}
					allPhoneNumbers.append(phoneNumber);
					allPhoneNumbers.append(",");
				}
			}
		} catch (Exception e) {

		} finally {
			if (phones != null)
				phones.close();
		}

		if (allPhoneNumbers.length() > 0)
			return allPhoneNumbers.toString().substring(0,
					allPhoneNumbers.length() - 1);
		else
			return "";
	}

	/**
	 * 
	 */
	@Override
	public void saveContacts(String uid, List<Contacts> contacts) {
		// TODO Auto-generated method stub
		try {
			mDataBaseAdapter = new DBAdapter(this.mContext);
			mDataBaseAdapter.open();
			for (Contacts contact : contacts) {
				if (!isExist(uid, contact.getPhoneNum())) {
					ContentValues cValues = new ContentValues();
					cValues.put(mDataBaseAdapter.CONTACTS_ID, contact
							.getContactId());
					cValues.put(mDataBaseAdapter.MYUID, uid);
					cValues.put(mDataBaseAdapter.CONTACTS_USERNAME, contact
							.getUserName());
					cValues.put(mDataBaseAdapter.CONTACTS_SORTKEY, contact
							.getSortkey());
					cValues.put(mDataBaseAdapter.CONTACTS_FIRSTLETTER, contact
							.getFirstLetter());
					cValues.put(mDataBaseAdapter.CONTACTS_PHONENUM, contact
							.getPhoneNum());
					boolean isInsertSuccess = mDataBaseAdapter.insertData(
							mDataBaseAdapter.TABLE_CONTACTS, cValues) > 0;
					System.out.println("save saveContacts()---------"
							+ isInsertSuccess);
				}
			}
		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			if (mDataBaseAdapter != null) {
				if (mDataBaseAdapter.mSQLiteDatabase != null) {
					mDataBaseAdapter.mSQLiteDatabase.close();
				}
				mDataBaseAdapter.close();
			}
		}
	}

	/**
	 * 判断该当前用户是否存在
	 */
	@Override
	public boolean isExist(String phoneNum) {
		String selectSQL = "select * from " + mDataBaseAdapter.TABLE_CONTACTS
				+ " where " + mDataBaseAdapter.CONTACTS_PHONENUM + "='"
				+ phoneNum + "'";
		Cursor mCursor = null;
		try {
			mCursor = mDataBaseAdapter.rawQuery(selectSQL);

			return mCursor != null && mCursor.getCount() > 0;

		} catch (Exception e) {
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return false;
	}

	/**
	 * 判断该当前用户是否存在
	 */
	public boolean isExist(String uid, String phoneNum) {
		String selectSQL = "select * from " + mDataBaseAdapter.TABLE_CONTACTS
				+ " where " + mDataBaseAdapter.CONTACTS_PHONENUM + "=? and "
				+ mDataBaseAdapter.MYUID + "=?";
		Cursor mCursor = null;
		try {
			mCursor = mDataBaseAdapter.rawQuery(selectSQL, new String[] {
					phoneNum, uid });

			return mCursor != null && mCursor.getCount() > 0;

		} catch (Exception e) {
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
		}
		return false;
	}

	/**
	 * 获取所有用户
	 */
	@Override
	public List<Contacts> getAllContacts(String uid) {

		Cursor mCursor = null;
		List<Contacts> contactList = new ArrayList<Contacts>();
		try {
			mDataBaseAdapter = new DBAdapter(this.mContext);
			mDataBaseAdapter.open();
			String selectSQL = "select * from "
					+ mDataBaseAdapter.TABLE_CONTACTS + " where "
					+ mDataBaseAdapter.MYUID + " = ? " + " ORDER BY "
					+ mDataBaseAdapter.CONTACTS_FIRSTLETTER + " ASC";
			mCursor = mDataBaseAdapter
					.rawQuery(selectSQL, new String[] { uid });
			if (mCursor != null && mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				for (; !mCursor.isAfterLast(); mCursor.moveToNext()) {
					Contacts contact = new Contacts();
					contact.setId(mCursor.getInt(mCursor
							.getColumnIndex(mDataBaseAdapter.CONTACTS_SID)));
					contact.setContactId(mCursor.getLong(mCursor
							.getColumnIndex(mDataBaseAdapter.CONTACTS_ID)));
					contact
							.setUserName(mCursor
									.getString(mCursor
											.getColumnIndex(mDataBaseAdapter.CONTACTS_USERNAME)));
					contact
							.setPhoneNum(mCursor
									.getString(mCursor
											.getColumnIndex(mDataBaseAdapter.CONTACTS_PHONENUM)));
					String sortkey = mCursor.getString(mCursor
							.getColumnIndex(mDataBaseAdapter.CONTACTS_SORTKEY));
					contact.setSortkey(sortkey);
					// 当前汉语拼音首字母
					String currentStr = Util.getAlpha(sortkey);
					// 上一个汉语拼音首字母，如果不存在为" "
					String previewStr = (contactList.size() - 1) >= 0 ? Util
							.getAlpha(contactList.get(contactList.size() - 1)
									.getSortkey()) : " ";
					if (!previewStr.equals(currentStr)) {
						contact.setFirstLetter(Util.getAlpha(sortkey));
					} else {
						contact.setFirstLetter(Contacts.NO_LETTER);
					}

					contactList.add(contact);
				}
			}
			return contactList;
		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			if (mCursor != null) {
				mCursor.close();
			}
			if (mDataBaseAdapter != null) {
				mDataBaseAdapter.close();
			}
		}
		return contactList;
	}

	@Override
	public void deleteContact(String uid, String id) {
		// TODO Auto-generated method stub
		try {
			mDataBaseAdapter = new DBAdapter(this.mContext);
			mDataBaseAdapter.open();
			String sql = "delete from " + mDataBaseAdapter.TABLE_CONTACTS
					+ " where " + mDataBaseAdapter.MYUID + " =? and "
					+ mDataBaseAdapter.CONTACTS_SID + " =?";
			mDataBaseAdapter.deleteOrUpdate(sql, new String[] { uid, id });
		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			if (mDataBaseAdapter != null) {
				if (mDataBaseAdapter.mSQLiteDatabase != null) {
					mDataBaseAdapter.mSQLiteDatabase.close();
				}
				mDataBaseAdapter.close();
			}
		}
	}

}
