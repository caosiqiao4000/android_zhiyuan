package com.gsta.v2.db.impl;

import org.json.JSONException;
import org.json.JSONObject;

import com.gsta.v2.util.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * 公共存储空间
 * 
 * @author boge
 * @time 2013-3-27
 * 
 */
public class SharedPreferencesUtil {
    private final String PREFERENCE_NAME = "telemarletingclient";
    private String TAG = Util.getClassName();// log tag
    public static final String CITY_LOAD = "cityload";

    @SuppressWarnings("unused")
    private Context context;
    private SharedPreferences mSharedPreferences;

    /**
     * 这里构造器会多次NEW ,mSharedPreferences被多次生成?
     * 
     * @param context
     */
    public SharedPreferencesUtil(Context context) {
        this.context = context;
        if (null == mSharedPreferences) {
            mSharedPreferences = context.getSharedPreferences(PREFERENCE_NAME,
                    Context.MODE_PRIVATE);
        }
    }

    // 保存字符串
    public void saveString(String key, String value) {
        mSharedPreferences.edit().putString(key, value).commit();
    }

    // 获取字符串
    public String getString(String key, String... defValue) {
        if (defValue.length > 0)
            return mSharedPreferences.getString(key, defValue[0]);
        else
            return mSharedPreferences.getString(key, null);

    }

    // 保存整数值
    public void saveInt(String key, Integer value) {
        mSharedPreferences.edit().putInt(key, value).commit();
    }

    // 保存整数值
    public Integer getInt(String key, Integer defValue) {
        return mSharedPreferences.getInt(key, defValue);
    }

    // 保存布尔值
    public void saveBoolean(String key, Boolean value) {
        mSharedPreferences.edit().putBoolean(key, value).commit();
    }

    // 获取布尔值
    public Boolean getBoolean(String key, Boolean... defValue) {
        if (defValue.length > 0)
            return mSharedPreferences.getBoolean(key, defValue[0]);
        else
            return mSharedPreferences.getBoolean(key, false);

    }

    // 保存布尔值
    public void saveFloat(String key, Float value) {
        mSharedPreferences.edit().putFloat(key, value).commit();
    }

    // 获取布尔值
    public Float getFloat(String key, Float... defValue) {
        if (defValue.length > 0)
            return mSharedPreferences.getFloat(key, defValue[0]);
        else
            return mSharedPreferences.getFloat(key, 0.0f);

    }

    // 返回用户的信息。
    public JSONObject getJSON(String count) {
        JSONObject object;
        try {
            object = new JSONObject(mSharedPreferences.getString(count, ""));
            return object;
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public void saveJSONObject(String count, String json) {
        mSharedPreferences.edit().putString(count, json).commit();
    }

    public void removeJSONObject(String count) {
        try {
            mSharedPreferences.edit().remove(count);
        } catch (Exception e) {
            // TODO: handle exception
        }

    }
}
