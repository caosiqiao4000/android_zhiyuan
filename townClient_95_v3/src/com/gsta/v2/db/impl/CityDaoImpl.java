package com.gsta.v2.db.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.gsta.v2.db.ICityDao;
import com.gsta.v2.entity.City;
import com.gsta.v2.util.Util;

/**
 * 城市信息
 * 
 * @author wubo
 * @createtime 2012-9-1
 */
public class CityDaoImpl implements ICityDao {

    private final String TAG = Util.getClassName();
    public DBAdapter db;
    private Context context;

    public CityDaoImpl(Context context) {
        this.context = context;
    }

    @Override
    public List<City> getChildCitybyId(String parentId) {
        // TODO Auto-generated method stub
        List<City> citys = new ArrayList<City>();
        Cursor cursor = null;
        try {
            db = new DBAdapter(context);
            db.open();
            cursor = db.getChildCitybyId(parentId);
            if (cursor == null)
                return citys;
            if (cursor.getCount() == 0)
                return citys;

            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor
                    .moveToNext()) {
                City city = new City();
                city.setId(cursor.getString(cursor.getColumnIndex(db.CITY_ID)));
                city.setName(cursor.getString(cursor
                        .getColumnIndex(db.CITY_NAME)));
                city.setParent(cursor.getString(cursor
                        .getColumnIndex(db.CITY_PARENTID)));
                citys.add(city);
            }
        } catch (Exception e) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                if (db.mSQLiteDatabase != null) {
                    db.mSQLiteDatabase.close();
                }
                db.close();
            }
        }
        return citys;
    }

    @Override
    public List<City> getChildCitybyName(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void saveAllCity(List<City> citys) {
        try {
            db = new DBAdapter(context);
            db.open();
            db.mSQLiteDatabase.beginTransaction();
            for (int i = 0; i < citys.size(); i++) {
                City city = citys.get(i);
                ContentValues cv = new ContentValues();
                cv.put(db.CITY_ID, city.getId());
                cv.put(db.CITY_PARENTID, city.getParent());
                cv.put(db.CITY_NAME, city.getName());
                db.insertData(db.TABLE_CITY, cv);
            }
            db.mSQLiteDatabase.setTransactionSuccessful();
            Log.e(TAG, "saveCity end: " + new Date().getTime());
            new SharedPreferencesUtil(context).saveString(
                    SharedPreferencesUtil.CITY_LOAD, "succ");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        } finally {
            db.mSQLiteDatabase.endTransaction();
            if (db != null) {
                if (db.mSQLiteDatabase != null) {
                    db.mSQLiteDatabase.close();
                }
                db.close();
            }
        }
    }

    @SuppressWarnings("finally")
    @Override
    public City queryCityById(String id) {
        City city = new City();
        Cursor mCursor = null;
        try {
            db = new DBAdapter(context);
            db.open();
            final String selectSQL = "select * from " + db.TABLE_CITY + " where " + db.CITY_ID + "= ? ";
            mCursor = db.rawQuery(selectSQL, new String[] { id });
            if (null != mCursor && mCursor.getCount() > 0) {
                mCursor.moveToFirst();
                city.setId(mCursor.getString(mCursor.getColumnIndex(db.CITY_ID)));
                city.setName(mCursor.getString(mCursor
                        .getColumnIndex(db.CITY_NAME)));
                city.setParent(mCursor.getString(mCursor
                        .getColumnIndex(db.CITY_PARENTID)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
            if (db != null) {
                if (db.mSQLiteDatabase != null) {
                    db.mSQLiteDatabase.close();
                }
                db.close();
            }
            return city;
        }
    }
}
