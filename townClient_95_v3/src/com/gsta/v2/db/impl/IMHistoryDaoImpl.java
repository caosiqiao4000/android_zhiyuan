package com.gsta.v2.db.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.gsta.v2.db.IMHistoryDao;
import com.gsta.v2.entity.IMBean;
import com.gsta.v2.util.FinalUtil;
import com.gsta.v2.util.Util;

/**
 * 私信会话
 * 
 * @author boge
 * @time 2013-3-27
 * 
 */
public class IMHistoryDaoImpl implements IMHistoryDao {

    private DBAdapter mdb;
    private Context context;

    public IMHistoryDaoImpl(Context context) {
        this.context = context;
    }

    @Override
    public boolean deleteUserMsgById(int id) {
        try {
            mdb = new DBAdapter(this.context);
            mdb.open();
            String sql = "delete from " + mdb.TABLE_IM + " where " + mdb.IM_ID
                    + " =?";
            mdb.deleteOrUpdate(sql, new Object[] { id });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
        }
    }

    @Override
    public boolean deleteUserMsgByUid(String myuid, String himuid) {
        try {
            mdb = new DBAdapter(this.context);
            mdb.open();
            String sql = "delete from " + mdb.TABLE_IM + " where " + mdb.MYUID
                    + " =? and " + mdb.IM_HISUSERID + " =?";
            mdb.deleteOrUpdate(sql, new Object[] { myuid, himuid });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
        }
    }

    @Override
    public List<IMBean> getUserMsgs(String myuid, String himuid) {
        List<IMBean> msgs = new ArrayList<IMBean>();
        Cursor mCursor = null;
        try {
            mdb = new DBAdapter(this.context);
            mdb.open();
            // 末加上时间或者条件限制
            String sql = "SELECT * FROM " + mdb.TABLE_IM + " WHERE "
                    + mdb.MYUID + " = ? and " + mdb.IM_HISUSERID
                    + " =? ORDER BY " + mdb.IM_TIME + " asc ";
            mCursor = mdb.select(sql,
                    new String[] { Util.trimUserIDDomain(myuid),
                            Util.trimUserIDDomain(himuid) });
            if (mCursor == null || mCursor.getCount() < 1) {
                return msgs;
            }
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor
                    .moveToNext()) {
                IMBean msg = new IMBean();
                msg.setId(mCursor.getInt(mCursor.getColumnIndex(mdb.IM_ID)));
                msg.setMyUserId(mCursor.getString(mCursor
                        .getColumnIndex(mdb.MYUID)));// 登录用户的uid
                msg.setUserName(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_USERNAME)));// 用户名
                msg.setHisName(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_HISNAME)));// 用户名
                msg.setHisUserId(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_HISUSERID)));// 他的uid
                msg.setHisPhotoPath(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_HISPHOTO)));// 用户名
                msg.setContent(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_CONTENT))); // 信息的内容
                msg.setTime(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_TIME))); // 发信息的时间
                msg.setDirection(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_DIRECTION))); // 谁发的信息(0表示登陆者,1表示聊天对象)
                msg.setUserType(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_USERTYPE)));// 用户类型
                msg.setSended(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_SENDED)));
                msg.setFlag(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_FLAG)));// 是否阅读
                msgs.add(msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
        }
        return msgs;
    }

    @Override
    public List<IMBean> getUserMsgsHistory(String myuid, String himuid) {
        List<IMBean> msgs = new ArrayList<IMBean>();
        Cursor mCursor = null;
        try {
            mdb = new DBAdapter(this.context);
            mdb.open();
            String sql = "SELECT * FROM " + mdb.TABLE_IM + " WHERE "
                    + mdb.MYUID + " = ? and " + mdb.IM_HISUSERID
                    + " =? ORDER BY " + mdb.IM_TIME + " desc ";
            mCursor = mdb.select(sql,
                    new String[] { Util.trimUserIDDomain(myuid),
                            Util.trimUserIDDomain(himuid) });
            if (mCursor == null || mCursor.getCount() < 1) {
                return msgs;
            }
            for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor
                    .moveToNext()) {
                IMBean msg = new IMBean();
                msg.setId(mCursor.getInt(mCursor.getColumnIndex(mdb.IM_ID)));
                msg.setMyUserId(mCursor.getString(mCursor
                        .getColumnIndex(mdb.MYUID)));// 登录用户的uid
                msg.setUserName(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_USERNAME)));// 用户名
                msg.setHisName(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_HISNAME)));// 用户名
                msg.setHisUserId(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_HISUSERID)));// 他的uid
                msg.setHisPhotoPath(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_HISPHOTO)));// 用户名
                msg.setContent(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_CONTENT))); // 信息的内容
                msg.setTime(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_TIME))); // 发信息的时间
                msg.setDirection(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_DIRECTION))); // 谁发的信息(0表示登陆者,1表示聊天对象)
                msg.setUserType(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_USERTYPE)));// 用户类型
                msg.setFlag(mCursor.getString(mCursor
                        .getColumnIndex(mdb.IM_FLAG)));// 是否阅读
                msgs.add(msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
        }
        return msgs;
    }

    @Override
    public long saveUserMsg(IMBean msg) {
        try {
            mdb = new DBAdapter(this.context);
            mdb.open();
            ContentValues cv = new ContentValues();
            cv.put(mdb.MYUID, Util.trimUserIDDomain(msg.getMyUserId()));
            cv.put(mdb.IM_HISUSERID, Util.trimUserIDDomain(msg.getHisUserId()));
            cv.put(mdb.IM_USERNAME, msg.getUserName()); // ?
            cv.put(mdb.IM_HISNAME, msg.getHisName()); // ?
            cv.put(mdb.IM_HISPHOTO, msg.getHisPhotoPath()); // ?
            cv.put(mdb.IM_CONTENT, msg.getContent());
            cv.put(mdb.IM_TIME, msg.getTime());
            cv.put(mdb.IM_DIRECTION, msg.getDirection());
            cv.put(mdb.IM_USERTYPE, msg.getUserType());
            cv.put(mdb.IM_FLAG, msg.getFlag());
            cv.put(mdb.IM_SENDED, msg.getSended());
            return mdb.insertData(mdb.TABLE_IM, cv);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
        }
    }

    @Override
    public List<IMBean> getUserMessages(String uid) {
        // TODO Auto-generated method stub
        List<IMBean> msgs = new ArrayList<IMBean>();
        Map<String, String> map = new HashMap<String, String>();
        Cursor mCursor1 = null;
        Cursor mCursor2 = null;
        try {
            mdb = new DBAdapter(this.context);
            mdb.open();
            String sql1 = "SELECT * FROM " + mdb.TABLE_IM + " WHERE "
                    + mdb.MYUID + " = ?  GROUP BY " + mdb.IM_HISUSERID
                    + " ORDER BY " + mdb.IM_TIME + " desc";
            // select im_hisuserid, count(*) from IMTable where im_flag ='0' 这语法只能查出一条
            // select deptno,count(*) from emp group by deptno; // 此语法返回 符号每种状态的有多少条
            String sql2 = "select " + mdb.IM_HISUSERID + ", count(*) from "
                    + mdb.TABLE_IM + " where " + mdb.IM_FLAG + " ='0'";
            mCursor1 = mdb.select(sql1, new String[] { Util
                    .trimUserIDDomain(uid) });
            if (mCursor1 == null || mCursor1.getCount() < 1) {
                return msgs;
            }
            for (mCursor1.moveToFirst(); !mCursor1.isAfterLast(); mCursor1
                    .moveToNext()) {
                IMBean msg = new IMBean();
                msg.setMyUserId(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.MYUID)));// uid
                msg.setHisUserId(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.IM_HISUSERID)));// 他的uid
                msg.setUserName(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.IM_USERNAME)));// 用户名
                msg.setHisName(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.IM_HISNAME)));// 用户名
                msg.setHisPhotoPath(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.IM_HISPHOTO)));// 用户名
                msg.setContent(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.IM_CONTENT))); // 信息的内容
                msg.setTime(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.IM_TIME))); // 发信息的时间
                msg.setDirection(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.IM_DIRECTION))); // 谁发的信息(0表示登陆者,1表示聊天对象)
                msg.setSended(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.IM_SENDED)));
                msg.setUserType(mCursor1.getString(mCursor1
                        .getColumnIndex(mdb.IM_USERTYPE)));// 用户类型
                msg.setFlag("0");
                msgs.add(msg);
            }
            mCursor2 = mdb.select(sql2, null);
            if (mCursor2 == null || mCursor2.getCount() < 1) {
                return msgs;
            }
            for (mCursor2.moveToFirst(); !mCursor2.isAfterLast(); mCursor2
                    .moveToNext()) {
                map.put(mCursor2.getString(0), mCursor2.getString(1));
            }
            for (int i = 0; i < msgs.size(); i++) {
                final String hisUid = msgs.get(i).getHisUserId();
                if (map.containsKey(hisUid)) { // 设置聊天信息 是否已读
                    msgs.get(i).setFlag(map.get(hisUid));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mCursor1 != null) {
                mCursor1.close();
            }
            if (mCursor2 != null) {
                mCursor2.close();
            }
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
        }
        return msgs;
    }

    @Override
    public void updateReadStatus(String myuid, String himuid) {
        // TODO Auto-generated method stub
        try {
            mdb = new DBAdapter(this.context);
            mdb.open();
            // 这里更改所有 ,应该要加限制条件
            String sql = "update " + mdb.TABLE_IM + " set " + mdb.IM_FLAG
                    + " = ?" + " where " + mdb.MYUID + " =? and "
                    + mdb.IM_HISUSERID + "=? ";
            mdb.deleteOrUpdate(sql, new String[] { FinalUtil.IM_READED, myuid,
                    himuid });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mdb != null) {
                if (mdb.mSQLiteDatabase != null) {
                    mdb.mSQLiteDatabase.close();
                }
                mdb.close();
            }
        }
    }
}
