package com.gsta.v2.db;

import com.gsta.v2.entity.BaseUserinfo;

/**
 * 查询相关用户信息
 * 
 * @author wubo
 * @createtime 2012-9-6
 */
public interface IUserInfoDao {

    public BaseUserinfo getUserInfo();

    public void saveOrUpdateUserInfo(BaseUserinfo userinfo);
}
