package com.gsta.v2.entity_v2;


/**
 * @author: feijinbo
 * @createTime: 2011-12-10 上午11:31:08
 * @version: 1.0
 * @desc :用于手机分页查询
 */
public class Limit implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private int offset;// 起始记录行
    private int length;// 获取记录行条数

    public Limit() {
    }

    public Limit(int offset, int length) {
        this.offset = offset;
        this.length = length;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return getOffset()+","+getLength();
    }
}
