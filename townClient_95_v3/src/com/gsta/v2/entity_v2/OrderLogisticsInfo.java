package com.gsta.v2.entity_v2;

/**
 * @author zengxj
 * @createTime 2013-4-9
 * @version: 1.0
 * @desc:
 */
public class OrderLogisticsInfo {

    private String orderId;// 订单编号
    private String processTime;// 处理时间
    private String processInformation;// 处理信息

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProcessTime() {
        return processTime;
    }

    public void setProcessTime(String processTime) {
        this.processTime = processTime;
    }

    public String getProcessInformation() {
        return processInformation;
    }

    public void setProcessInformation(String processInformation) {
        this.processInformation = processInformation;
    }

}
