package com.gsta.v2.entity_v2;

import com.gsta.v2.response.BaseResult;

/**
 * @author fitch
 * @createtime 2013-3-26 下午7:28:41
 * @version 1.0
 * @desc
 */
public class AgentInfoQueryResult extends BaseResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6901133549408104998L;
	private AgentInfo detail =new AgentInfo();

	public AgentInfo getDetail() {
		return detail;
	}

	public void setDetail(AgentInfo detail) {
		this.detail = detail;
	}
	
}
