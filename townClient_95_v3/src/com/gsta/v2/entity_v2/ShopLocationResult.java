package com.gsta.v2.entity_v2;

import java.util.List;

import com.gsta.v2.entity.ShopInfo;
import com.gsta.v2.response.BaseResult;

/**
 * 代理商店铺信息结果集（包含代理商店铺经纬度等）
 * @author zengxj
 * @createTime 2013-3-28
 * @version: 1.0
 * @desc:
 */
public class ShopLocationResult extends BaseResult {

	private static final long serialVersionUID = 5037213602456512429L;
	
	private List<ShopInfo> concernShops;// 关注商家

	public List<ShopInfo> getConcernShops() {
		return concernShops;
	}

	public void setConcernShops(List<ShopInfo> concernShops) {
		this.concernShops = concernShops;
	}
}
