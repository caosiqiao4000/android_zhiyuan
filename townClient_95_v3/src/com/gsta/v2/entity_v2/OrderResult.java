package com.gsta.v2.entity_v2;

import java.util.ArrayList;
import java.util.List;

import com.gsta.v2.response.BaseResult;


/**
 * 订单信息查询返回结果类
 * @author zengxj
 * @createTime 2013-4-9
 * @version: 1.0
 * @desc:
 */
public class OrderResult extends BaseResult{

	private static final long serialVersionUID = 7339486321252292091L;

	private Integer unconfirmed;// 待确认收货交易数量
	private Integer unpayment;// 待付款交易数量
	private Integer undelivery;// 待发货交易数量
	private Integer successOrder;// 已成功交易数量
	private Integer cancelOrder;// 已取消交易数量
	
	private Long tCount;// 满足条件总记录条数
	private List<OrderInfo> orderInfos = new ArrayList<OrderInfo>();// 订单信息集合
	
	
	public Integer getUnconfirmed() {
		return unconfirmed;
	}
	public void setUnconfirmed(Integer unconfirmed) {
		this.unconfirmed = unconfirmed;
	}
	public Integer getUnpayment() {
		return unpayment;
	}
	public void setUnpayment(Integer unpayment) {
		this.unpayment = unpayment;
	}
	public Integer getUndelivery() {
		return undelivery;
	}
	public void setUndelivery(Integer undelivery) {
		this.undelivery = undelivery;
	}
	public Integer getSuccessOrder() {
		return successOrder;
	}
	public void setSuccessOrder(Integer successOrder) {
		this.successOrder = successOrder;
	}
	public Integer getCancelOrder() {
		return cancelOrder;
	}
	public void setCancelOrder(Integer cancelOrder) {
		this.cancelOrder = cancelOrder;
	}
	public Long gettCount() {
		return tCount;     
	}
	public void settCount(Long tCount) {
		this.tCount = tCount;
	}
	public List<OrderInfo> getOrderInfos() {
		return orderInfos;    
	}
	public void setOrderInfos(List<OrderInfo> orderInfos) {
		this.orderInfos = orderInfos;
	}


}