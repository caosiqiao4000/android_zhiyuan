package com.gsta.v2.entity_v2;

import java.util.List;

/**
 * 订单信息
 * 
 * @author zengxj
 * @createTime 2013-4-9
 * @version: 1.0
 * @desc:
 */
public class OrderInfo {

    private String orderId;// 订单编号
    private String transactionId;// 交易ID
    private String uid;// 用户ID
    private String account;// 买家帐号
    private String orderDate;// 下单时间
    private Double orderPrice;// 订单金额
    private Integer orderStatus;// 订单状态
    private String agentUid;// 代理商UID
    private String shopName;// 下单所在店铺的名称
    private String photo;// 下单所在店铺的图标
    private String payMethod;// 支付方式1-在线支付，2-货到付款
    private List<OrderWareInfo> orderWareInfos;// 商品信息
    private List<OrderLogisticsInfo> orderLogisticsInfos;// 物流跟踪信息

    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getAgentUid() {
        return agentUid;
    }

    public void setAgentUid(String agentUid) {
        this.agentUid = agentUid;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<OrderWareInfo> getOrderWareInfos() {
        return orderWareInfos;
    }

    public void setOrderWareInfos(List<OrderWareInfo> orderWareInfos) {
        this.orderWareInfos = orderWareInfos;
    }

    public List<OrderLogisticsInfo> getOrderLogisticsInfos() {
        return orderLogisticsInfos;
    }

    public void setOrderLogisticsInfos(List<OrderLogisticsInfo> orderLogisticsInfos) {
        this.orderLogisticsInfos = orderLogisticsInfos;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

}
