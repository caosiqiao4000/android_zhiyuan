package com.gsta.v2.entity_v2;

import java.io.Serializable;

/**
 * 订单商品信息
 * 
 * @author zengxj
 * @createTime 2013-4-12
 * @version: 1.0
 * @desc:
 */
public class OrderWareInfo implements Serializable {

    private String specificationId;// 类型ID
    private String speciesId;// 商品ID
    private String unitId;// 货号
    private String wareName;// 商品名称
    private String description;// 商品描述
    private String mediaPath;// 商品图片(媒体路径)

    public String getSpecificationId() {
        return specificationId;
    }

    public void setSpecificationId(String specificationId) {
        this.specificationId = specificationId;
    }

    public String getSpeciesId() {
        return speciesId;
    }

    public void setSpeciesId(String speciesId) {
        this.speciesId = speciesId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getWareName() {
        if (null == wareName) {
            wareName = "";
        }
        return wareName;
    }

    public void setWareName(String wareName) {
        this.wareName = wareName;
    }

    public String getDescription() {
        if (null == description) {
            description = "";
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

}
