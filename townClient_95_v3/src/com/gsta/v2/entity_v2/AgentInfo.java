package com.gsta.v2.entity_v2;

/**
 * @author fitch
 * @createtime 2013-3-26 下午7:24:09
 * @version 1.0
 * @desc
 */
public class AgentInfo implements java.io.Serializable {

	private String uid;// 用户ID
	private String agentName;// 代理商名称
	private Integer agentType;// 代理商类型
	private String levelCode;// 0~1 代理商级别
	private Integer identity;// 0~1 代理商身份类别
	private String username;// 0~1 代理商姓名
	private String phone;// 0~1 联系电话
	private String mobile;// 0~1 手机号码
	private String provinceId;// 0~1 代理省份ID
	private String cityId;// 0~1 代理城市ID
	private String areaId;// 0~1 地区编码
	private String address;// 0~1 联系地址
	private String zipcode;// 0~1 邮编
	private String email;// 0~1 邮箱
	private Integer followUsers;// 0~1 发展用户数
	private Integer directUser;// 0~1 直接用户数
	private Integer indirectUser;// 0~1 间接用户数
	private Integer saleCount;// 0~1 销售商品数
	private Double grossSales;// 0~1 销售额
	private String iconUrl;// 0~1 头像URL（用户表获取）
	private String shopName;// 0~1 店铺名称
	private String shopUrl;// 0~1 店铺域名=domain+”.95town.cn”
	private String shopDesc;// 0~1 店铺公告

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Integer getAgentType() {
		return agentType;
	}

	public void setAgentType(Integer agentType) {
		this.agentType = agentType;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public Integer getIdentity() {
		return identity;
	}

	public void setIdentity(Integer identity) {
		this.identity = identity;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getFollowUsers() {
		return followUsers;
	}

	public void setFollowUsers(Integer followUsers) {
		this.followUsers = followUsers;
	}

	public Integer getDirectUser() {
		return directUser;
	}

	public void setDirectUser(Integer directUser) {
		this.directUser = directUser;
	}

	public Integer getIndirectUser() {
		return indirectUser;
	}

	public void setIndirectUser(Integer indirectUser) {
		this.indirectUser = indirectUser;
	}

	public Integer getSaleCount() {
		return saleCount;
	}

	public void setSaleCount(Integer saleCount) {
		this.saleCount = saleCount;
	}

	public Double getGrossSales() {
		return grossSales;
	}

	public void setGrossSales(Double grossSales) {
		this.grossSales = grossSales;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopUrl() {
		return shopUrl;
	}

	public void setShopUrl(String shopUrl) {
		this.shopUrl = shopUrl;
	}

	public String getShopDesc() {
		return shopDesc;
	}

	public void setShopDesc(String shopDesc) {
		this.shopDesc = shopDesc;
	}

}
