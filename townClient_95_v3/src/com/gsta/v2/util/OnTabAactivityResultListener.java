package com.gsta.v2.util;

import android.content.Intent;

/**
 * 动态VIEW 调用系统图片时使用的回调类
 * @author caosq
 * 2013-5-24
 */
public interface OnTabAactivityResultListener {
    /**
     * tabActivity ActivityGroup 调用系统图片时使用的回调类
     * 
     * @author caosq 2013-5-24 下午4:59:54
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onTabActivityResult(int requestCode, int resultCode, Intent data);
}
