package com.gsta.v2.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;



public class ReadFromJson {

	
	public static String parseJsonsToString(String strResult,String indexName) {
		try {  
		    JSONTokener tokener = new JSONTokener(strResult);  
		   // JSONObject j = (JSONObject)tokener.nextValue();  
		    JSONObject j = (JSONObject)tokener.nextValue();  
		    //int status = j.getInt("status");  
		   // JSONArray jsonObjs = j.getJSONArray("result");  
		    
		    return j.getString(indexName);
		              
		        } catch (JSONException e) {  
		           // System.out.println("Jsons parse error !");  
		          // e.printStackTrace();  
		        	return "error";
		        } catch (Exception e1) {  
		           //e1.printStackTrace();  
		        	return "error";
		      }  
		       
		    
		}  
	public static int parseJsonsToInt(String strResult,String indexName) {
		try {  
		    JSONTokener tokener = new JSONTokener(strResult);  
		   // JSONObject j = (JSONObject)tokener.nextValue();  
		    JSONObject j = (JSONObject)tokener.nextValue();  
		    //int status = j.getInt("status");  
		   // JSONArray jsonObjs = j.getJSONArray("result");  
		    
		    return j.getInt(indexName);
		              
		        } catch (JSONException e) {  
		           // System.out.println("Jsons parse error !");  
		          // e.printStackTrace();  
		        	return -10000;
		        } catch (Exception e1) {  
		           //e1.printStackTrace();  
		        	return -10000;
		      }  
		       
		    
		}  
	public static JSONObject parseJsons(String strResult,String indexName) {
		try {  
		    JSONTokener tokener = new JSONTokener(strResult);  
		   // JSONObject j = (JSONObject)tokener.nextValue();  
		    JSONObject j = (JSONObject)tokener.nextValue();  
		    //int status = j.getInt("status");  
		   // JSONArray jsonObjs = j.getJSONArray("result");  
		    
		    return j.getJSONObject(indexName);
		              
		        } catch (JSONException e) {  
		           // System.out.println("Jsons parse error !");  
		          // e.printStackTrace();  
		        	return null;
		        } catch (Exception e1) {  
		           //e1.printStackTrace();  
		        	return null;
		      }  
		       
		    
		} 
	public static JSONArray parseJsonsArray(String strResult,String indexName) {
		try {  
		    JSONTokener tokener = new JSONTokener(strResult);  
		   // JSONObject j = (JSONObject)tokener.nextValue();  
		    JSONObject j = (JSONObject)tokener.nextValue();  
		    //int status = j.getInt("status");  
		   // JSONArray jsonObjs = j.getJSONArray("result");  
		    
		    return j.getJSONArray(indexName);
		              
		        } catch (JSONException e) {  
		           // System.out.println("Jsons parse error !");  
		          // e.printStackTrace();  
		        	return null;
		        } catch (Exception e1) {  
		           //e1.printStackTrace();  
		        	return null;
		      }  
		       
		    
		} 

}
