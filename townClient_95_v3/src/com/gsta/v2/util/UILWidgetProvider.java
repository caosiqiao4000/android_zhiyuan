package com.gsta.v2.util;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;

/**
 * @author fitch
 * @create 2013-6-23 下午6:33:11
 * @version 1.0
 */
public class UILWidgetProvider extends AppWidgetProvider {
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		AgentApplication.initImageLoader(context);
	}
}
