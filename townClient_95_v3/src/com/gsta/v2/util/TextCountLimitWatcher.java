package com.gsta.v2.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * EditText 字数限制
 * 
 * @author boge
 * @time 2013-3-26
 * 
 */
public class TextCountLimitWatcher implements TextWatcher {

    private String TAG = TextCountLimitWatcher.class.getName();// log tag
    private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
            .getLogger(TextCountLimitWatcher.class);
    private int maxLenght = 8;// 限制字数
    private CharSequence temp;
    private int selectionStart;
    private int selectionEnd;
    public EditText mEditText;

    public TextCountLimitWatcher() {
    }

    public TextCountLimitWatcher(int maxLenght, EditText mEditText) {
        this.mEditText = mEditText;
        this.maxLenght = maxLenght;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // if (s != null && s.length() > maxLenght) {
        // s = s.subSequence(0,
        // maxLenght);
        // }
        temp = s;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
            int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            selectionStart = mEditText.getSelectionStart();
            selectionEnd = mEditText.getSelectionEnd();
            if (temp == null) {
                temp = "";
            }
            if (temp.length() > maxLenght) {
                selectionStart = maxLenght + 1;
                if (selectionStart - 1 >= 0
                        && selectionStart - 1 <= selectionEnd) {
                    s.delete(selectionStart - 1, selectionEnd);
                    mEditText.setText(s);
                    mEditText.setSelection(s.length());// 设置光标在最后
                }

            }

        } catch (StackOverflowError e) {
            logger.error(TAG, e);
        } catch (Exception e) {
            logger.error(TAG, e);
        }

    }

}
