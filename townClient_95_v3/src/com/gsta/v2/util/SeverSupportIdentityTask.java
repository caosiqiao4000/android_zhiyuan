package com.gsta.v2.util;

import java.util.List;

import mobile.http.ConnectivityHelper;
import mobile.http.HttpReqCode;
import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Window;
import android.widget.TextView;

import com.gsta.v2.activity.R;

/**
 * 
 * 异步刷新ui类
 * 
 * @author wangxin
 * 
 */
public class SeverSupportIdentityTask extends
		AsyncTask<Object, Integer, Object> {
	private String TAG = Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private Context context;
	private IUICallBackInterface callBackInterface;
	// request url
	private String url;
	// Parameter
	private List<Parameter> paras;
	// pdShow isShow ?
	private boolean pdShow;
	// ProgressDialog show message
	private String pdMsg;
	// ProgressDialog
	private MyFilePart uploader1;

	private MyFilePart uploader2;
	private int caseKey;
	protected SyncHttpClient http = new SyncHttpClient();
	// 文件总大小

	private Dialog popupDialog;

	// 字符集

	@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub
		if (!ConnectivityHelper.ConnectivityIsAvailable(context)) {
			return HttpReqCode.no_network;
		}

		try {
			return http.httpPostWithFile(url, paras, uploader1, uploader2);
		} catch (Exception e) {
			logger.error(TAG, e);
			return HttpReqCode.error;
		}

	}

	/***
	 * 
	 * SeverSupportTask
	 * 
	 * @param context
	 * @param pdShow
	 * @param paras
	 * @param url
	 * @param callBackInterface
	 */
	public SeverSupportIdentityTask(Context context,
			IUICallBackInterface callBackInterface, String url,
			List<Parameter> paras2, MyFilePart uploader1, MyFilePart uploader2,
			boolean pdShow, String pdMsg, int caseKey) {
		this.context = context;
		this.callBackInterface = callBackInterface;
		this.url = url;
		this.paras = paras2;
		this.pdShow = pdShow;
		this.pdMsg = pdMsg;
		this.caseKey = caseKey;
		this.uploader1 = uploader1;
		this.uploader2 = uploader2;
	}

	@Override
	protected void onPostExecute(Object result) {
		if (pdShow) {
			popupDialog.dismiss();
		}
		if (callBackInterface != null) {
			callBackInterface.uiCallBack(result, this.caseKey);
		}
	}

	@Override
	protected void onPreExecute() {
		if (pdShow) {
			popupDialog = new Dialog(context,
					android.R.style.Theme_Translucent_NoTitleBar);
			popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			popupDialog.setContentView(R.layout.loading_layout);
			TextView txtMsg = (TextView) popupDialog
					.findViewById(R.id.left_textView);
			txtMsg.setText(pdMsg);

			popupDialog.show();
		}
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(progress);
	}
}
