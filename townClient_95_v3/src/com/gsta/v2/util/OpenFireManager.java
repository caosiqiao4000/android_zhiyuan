package com.gsta.v2.util;

import java.util.Date;

import mobile.http.ConnectivityHelper;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.util.Log;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.db.IMHistoryDao;
import com.gsta.v2.db.impl.IMHistoryDaoImpl;
import com.gsta.v2.entity.IMBean;

/**
 * 私信登录发送注销管理类
 * 
 * 121.33.203.178 后台管理帐号 10106 pass1234 spark 10256 pass1234\10256
 * <p>
 * http://122.227.214.18:9090/index.jsp 10106 pass1234
 * 
 * @author wubo 10256
 * @createtime 2012-9-8
 */
public class OpenFireManager {

    private Logger logger = LoggerFactory.getLogger(OpenFireManager.class);
    // 正式
    private final String Openfire_Ip = "115.239.134.127";
    // private final String Openfire_Ip = "122.227.214.18";
    // 测试
    // private final String Openfire_Ip = "121.33.203.178";
    private final int Openfire_Port = 5222;
    private final String Openfire_ServerName = "95town.cn";

    private ConnectionConfiguration connConfig = null;
    private XMPPConnection xmppConn = null;

    private static OpenFireManager fireManager;
    private static Context context;
    private static AgentApplication application;
    private static IMHistoryDao dao;
    @SuppressWarnings("unused")
    private Roster roster;
    // 服务传递过来
    private String user;
    private String pass;

    /**
     * 构造 初始化配置信息
     * 
     * @author boge
     * @date 2013-3-27
     * 
     */
    private OpenFireManager() {
        generateXMPPConfig();
    }

    /**
     * @author caosq 2013-6-25 下午2:24:29
     */
    private void generateXMPPConfig() {
        if (connConfig == null) {
            connConfig = new ConnectionConfiguration(Openfire_Ip,
                    Openfire_Port, Openfire_ServerName);
            connConfig.setTruststorePath("/system/etc/security/cacerts.bks");
            connConfig.setSecurityMode(SecurityMode.disabled);
            connConfig.setSASLAuthenticationEnabled(false);
        }
        if (xmppConn == null) {
            xmppConn = new XMPPConnection(connConfig);
        }
    }

    public static OpenFireManager getInstance(Context cont) {
        context = cont;
        application = (AgentApplication) context.getApplicationContext();
        if (null == dao) {
            dao = new IMHistoryDaoImpl(context);
        }
        if (fireManager == null) {
            fireManager = new OpenFireManager();
        }
        return fireManager;
    }

    /**
     * openfire 服务名
     * 
     * @author wubo
     * @createtime 2012-9-7
     * @return
     */
    public String getOpenfireServername() {
        return Openfire_ServerName;
    }

    public ConnectionConfiguration getConnConfig() {
        return connConfig;
    }

    public XMPPConnection getXmppConn() {
        return xmppConn;
    }

    public boolean isLoggedIn() {
        // if (isConnected() && xmppConn.getUser() != null) {
        if (isConnected() && xmppConn.isAuthenticated()) {
            return true;
        }
        return false;
    }

    /**
     * 判断xmpp服务器是否连通 Returns true if currently connected to the XMPP server
     * 
     * @return
     */
    public boolean isConnected() {
        if (xmppConn != null && xmppConn.isConnected()) {
            return true;
        }
        return false;
    }

    public boolean connectOpenfire() {
        // 根据之前的配置，连接服务器
        try {
            generateXMPPConfig();
            xmppConn.connect();
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    public String getOpenfire_ServerName() {
        return Openfire_ServerName;
    }

    /**
     * 转换uid+域名
     * 
     * @author wubo
     * @createtime 2012-9-8
     * @param uid
     * @return
     */
    public String getServerDomain(String uid) {
        return uid + "@" + Openfire_ServerName;
    }

    // 接收其他用户发来的消息
    PacketListener packetListener = new PacketListener() {
        @Override
        public void processPacket(Packet packet) {
            Message packetMsg = (Message) packet;
            // 发送人标识
            String hisuid = Util.trimUserIDDomain(packetMsg.getFrom());
            if (hisuid.equals(application.getUid())) {
                return;
            }
            if (packetMsg.getBody() == null || packetMsg.getBody().length() < 1) {
                return;
            }
            IMBean msg = new IMBean();
            msg.setContent(packetMsg.getBody());
            Log.e("myOpenfire", "1.openfire收到消息 " + msg.getContent());
            msg.setDirection(FinalUtil.IM_TO_ME);
            msg.setMyUserId(application.getUid());
            msg.setHisUserId(hisuid);
            if (packetMsg.getProperty(FinalUtil.SENDIM_TIME) != null
                    && packetMsg.getProperty(FinalUtil.SENDIM_TIME).toString()
                            .length() > 0) {
                msg.setTime(packetMsg.getProperty(FinalUtil.SENDIM_TIME)
                        .toString());
            } else {
                msg.setTime(Util.getFormatDate(new Date().getTime()));
            }
            if (packetMsg.getProperty(FinalUtil.SENDIM_USERNAME) != null
                    && packetMsg.getProperty(FinalUtil.SENDIM_USERNAME)
                            .toString().length() > 0) {
                msg.setHisName(packetMsg
                        .getProperty(FinalUtil.SENDIM_USERNAME).toString());
            } else {
                // 目前填充的昵称,如果是对方没有传昵称,则填入uid 待改
                msg.setHisName(hisuid);
            }
            if (packetMsg.getProperty(FinalUtil.SENDIM_HISPHOTO) != null
                    && packetMsg.getProperty(FinalUtil.SENDIM_HISPHOTO)
                            .toString().length() > 0) {
                msg.setHisPhotoPath(packetMsg
                        .getProperty(FinalUtil.SENDIM_HISPHOTO).toString());
            } else {
                // 目前填充的昵称,如果是对方没有传昵称,则填入uid 待改
                msg.setHisPhotoPath("");
            }

            msg.setFlag(FinalUtil.IM_UNREAD);
            msg.setSended(FinalUtil.IM_SENDED);
            if (null == dao) {
                dao = new IMHistoryDaoImpl(context);
            }
            dao.saveUserMsg(msg);
            // 振动
            Vibrator vib = (Vibrator) context
                    .getSystemService(Service.VIBRATOR_SERVICE);
            // 设置振动参数
            vib.vibrate(new long[] { 100, 100, 100, 200 }, -1);
            // 这里要判断最顶层的ACTIVITY是否是MasChatActivity
            context.sendBroadcast(new Intent(FinalUtil.RECEIVER_IM_ACTION));
            context.sendBroadcast(new Intent(
                    FinalUtil.RECEIVER_IM_REFRUSH_ACTION));
        }
    };

    /**
     * 发送消息
     * 
     * @author wubo
     * @createtime 2012-9-10
     * @param packetMsg
     */
    public void sendPacket(IMBean imBean) {
        boolean sended = false;
        if (ConnectivityHelper.ConnectivityIsAvailable(context)) {
            Log.i("myOpenfire", "12.openfire准备发送消息");
            Message packetMsg = new Message(getServerDomain(imBean
                    .getHisUserId()));
            packetMsg.setType(Type.chat);
            packetMsg.setFrom(application.getUid());
            packetMsg.setBody(imBean.getContent());
            packetMsg.setProperty(FinalUtil.SENDIM_TIME, imBean.getTime());
            // packetMsg.setProperty(FinalUtil.SENDIM_HISNAME, imBean
            // .getHisName());
            // 自己头像
            packetMsg.setProperty(FinalUtil.SENDIM_HISPHOTO, imBean
                    .getHisPhotoPath());
            // 自己的名称
            packetMsg.setProperty(FinalUtil.SENDIM_USERNAME, imBean
                    .getUserName());
            if (isLoggedIn()) {
                xmppConn.sendPacket(packetMsg);
                sended = true;
                Log.i("myOpenfire", "13.openfire发送消息1 " + imBean
                        .getHisUserId());
            } else {
                if (loginIM(user, pass)) {
                    xmppConn.sendPacket(packetMsg);
                    sended = true;
                    Log.i("myOpenfire", "14.openfire发送消息2 " + imBean
                            .getHisUserId());
                }
            }
        }
        if (sended) {
            imBean.setSended(FinalUtil.IM_SENDED);
        } else {
            imBean.setSended(FinalUtil.IM_UNSEND);
        }
        long a = dao.saveUserMsg(imBean);
        // 通知聊天界面
        context.sendBroadcast(new Intent(FinalUtil.RECEIVER_IM_ACTION));
        // 通知聊天记录列表界面
        context.sendBroadcast(new Intent(FinalUtil.RECEIVER_IM_REFRUSH_ACTION));
    }

    // 客户端与服务器之间的TCP状态处理事件
    private ConnectionListener xmppConListener = new ConnectionListener() {
        public void connectionClosed() {
            // 客户端与服务器之间的TCP正常关闭
            xmppConn.removeConnectionListener(xmppConListener);
            Log.e("myOpenfire", "2.openfire正常关闭");
        }

        public void connectionClosedOnError(Exception e) {
            // 客户端与服务器之间的TCP非正常关闭
            xmppConn.removeConnectionListener(xmppConListener);
            xmppConn.disconnect();
            Log.e("myOpenfire", "3.openfire非正常关闭");
        }

        public void reconnectingIn(int seconds) {
            // seconds秒后，客户端将重连服务器
            Log.e("myOpenfire", "4.openfire重连");
        }

        public void reconnectionFailed(Exception e) {
            // 客户端将重连服务器失败
            Log.e("myOpenfire", "5.openfire重连失败");
        }

        public void reconnectionSuccessful() {
            // 客户端将重连服务器成功
            Log.e("myOpenfire", "6.openfire重连成功");
        }
    };

    public boolean loginIM(final String user, final String pass) {
        this.user = user;
        this.pass = pass;
        if (!fireManager.isLoggedIn()) {
            try {
                if (!isConnected()) {
                    xmppConn.connect();
                    Log.i("myOpenfire", "7.openfire链接成功");
                }
                xmppConn.login(user, pass, fireManager.getOpenfireServername());
                Log.e("myOpenfire", "9.openfire登录成功");

                addIMListener();

                Presence presence = new Presence(Presence.Type.available);
                // 发送状态对象
                xmppConn.sendPacket(presence);
                // 获取好友列表
                // if (null != xmppConn.getRoster()) {
                // Collection<RosterEntry> rosters = xmppConn.getRoster().getEntries();
                // System.out.println("我的好友列表：=======================");
                // for (RosterEntry rosterEntry : rosters) {
                // @SuppressWarnings("unused")
                // ItemStatus itemStatus = rosterEntry.getStatus();
                // System.out.print("name: " + rosterEntry.getName() + ",jid: " + rosterEntry.getUser());
                // System.out.println("");
                // }
                // }
                Log.i("myOpenfire", "10.openfire上线成功");
            } catch (XMPPException e) {
                logger.error(e);
                Log.e("myOpenfire", "8.openfire连接失败");
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("myOpenfire", "11.openfire登录失败" + e.getMessage());
                return false;
            }
        }
        return true;
    }

    /**
     * @author caosq 2013-6-28 下午2:32:37
     */
    public void addIMListener() {
        xmppConn.addPacketListener(packetListener, null);
        xmppConn.addConnectionListener(xmppConListener);// 连接
    }

    /**
     * 登出
     * 
     * @author wubo
     * @createtime 2012-9-10
     */
    public void loginOut() {
        try {
            if (isConnected()) {
                if (isLoggedIn()) {
                    Presence presence = new Presence(Presence.Type.unavailable);
                    xmppConn.sendPacket(presence);
                    Log.i("myOpenfire", "15.openfire下线成功");
                    // 断开客户端与服务器的连接
                    Log.i("myOpenfire", "16.openfire断开连接");
                }
                if (isConnected()) {
                    xmppConn.disconnect();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            xmppConn = null;
        }
    }
}
