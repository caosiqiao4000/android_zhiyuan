package com.gsta.v2.util;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.ShowPicMagnifyActivity;
import com.gsta.v2.activity.goods.OtherGoodsDetailActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

/**
 * 图片帮助类
 * 
 * @author boge
 * @time 2013-3-26
 * 
 */
public class ImageUtil {
	private static String TAG = Util.getClassName();// log tag

	private static final Logger logger = LoggerFactory.getLogger();

	/**
	 * 以2.2的计算，minSideLength的值以92或者320，maxNumOfPixels是512*384或者128 * 128
	 * 
	 * @param options
	 * @param minSideLength
	 * @param maxNumOfPixels
	 * @return
	 */
	public static int computeSampleSize(BitmapFactory.Options options,
			int minSideLength, int maxNumOfPixels) {
		int initialSize = computeInitialSampleSize(options, minSideLength,
				maxNumOfPixels);
		int roundedSize;
		if (initialSize <= 8) {
			roundedSize = 1;
			while (roundedSize < initialSize) {
				roundedSize <<= 1;
			}
		} else {
			roundedSize = (initialSize + 7) / 8 * 8;
		}
		return roundedSize;
	}

	private static int computeInitialSampleSize(BitmapFactory.Options options,
			int minSideLength, int maxNumOfPixels) {
		double w = options.outWidth;
		double h = options.outHeight;

		int lowerBound = (maxNumOfPixels == -1) ? 1 : (int) Math.ceil(Math
				.sqrt(w * h / maxNumOfPixels));
		int upperBound = (minSideLength == -1) ? 128 : (int) Math.min(
				Math.floor(w / minSideLength), Math.floor(h / minSideLength));
		if (upperBound < lowerBound) {
			return lowerBound;
		}
		if ((maxNumOfPixels == -1) && (minSideLength == -1)) {
			return 1;
		} else if (minSideLength == -1) {
			return lowerBound;
		} else {
			return upperBound;
		}
	}

	/**
	 * 加载SD卡图片文件
	 * 
	 * @param imageUrl
	 * @return
	 */
	public static Bitmap loadImageFromSd(String imageUrl) {
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(imageUrl, opts);
		opts.inSampleSize = ImageUtil.computeSampleSize(opts, -1, 128 * 128);
		opts.inJustDecodeBounds = false;
		opts.inPreferredConfig = Bitmap.Config.ALPHA_8;// 8位位图
		opts.inInputShareable = true;
		opts.inPurgeable = true;
		try {
			Bitmap bmp = BitmapFactory.decodeFile(imageUrl, opts);
			if (bmp == null) {
				return null;
			}
			return bmp;
		} catch (OutOfMemoryError e) {
			logger.error(TAG, e);
			return null;
		}
	}

	/**
	 * 取得Bitmap 图片的大小 KB
	 * 
	 * @author caosq 2013-6-13 下午3:17:04
	 * @param bitmap
	 * @return
	 */
	public static double getBitMapSize(Bitmap bitmap) {
		try {
			// 将bitmap放至数组中，这里主要是为了获得bitmap的大小（比实际读取的原文件要大）
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte[] b = baos.toByteArray();
			// 将字节换成KB
			return b.length / 1024;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	// 保存到SD卡
	public static void saveUserIcon(Bitmap bitmap, String fileName) {
		if (bitmap == null) {
			return;
		}

		try {
			// 压缩bitmap大小
			// 图片允许最大空间 单位：KB
			double maxSize = 400.00;
			// 将bitmap放至数组中，这里主要是为了获得bitmap的大小（比实际读取的原文件要大）
			double mid = getBitMapSize(bitmap);
			// 判断bitmap占用空间是否大于允许最大空间 如果大于则压缩 小于则不压缩
			if (mid > maxSize) {
				// 获取bitmap大小 是允许最大大小的多少倍
				double i = mid / maxSize;
				// 开始压缩 此处用到平方根 将宽带和高度压缩掉对应的平方根倍
				// （1.保持刻度和高度和原bitmap比率一致，压缩后也达到了最大大小占用空间的大小）
				bitmap = zoomImage(bitmap, bitmap.getWidth() / Math.sqrt(i),
						bitmap.getHeight() / Math.sqrt(i));
			}
//			saveBitmap(bitmap, FinalUtil.iconDir, fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author longxianwen
	 * @Title: zoomImage
	 * @Description: 图片的缩放到指定大小
	 * @param bgimage
	 *            源图片资源
	 * @param newWidth
	 *            缩放后宽度
	 * @param newHeight
	 *            缩放后高度
	 * @return Bitmap 压缩后的bitmap
	 * @throws
	 */
	public static Bitmap zoomImage(Bitmap bgimage, double newWidth,
			double newHeight) {
		// 获取这个图片的宽和高
		float width = bgimage.getWidth();
		float height = bgimage.getHeight();
		// 创建操作图片用的matrix对象
		Matrix matrix = new Matrix();
		// 计算宽高缩放率
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		// 缩放图片动作
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
				(int) height, matrix, true);
		if (!bgimage.isRecycled()) {
			bgimage.recycle();// 记得释放资源，否则会内存溢出
		}
		return bitmap;
	}

//	public static void saveUserIcon(File file, String userid) {
//		if (file == null || !file.exists()) {
//			return;
//		}
//		String fileName = userid + ".jpg";
//		File dir = new File(FinalUtil.iconDir);
//		try {
//			if (!dir.exists()) {
//				dir.mkdir();
//			}
//		} catch (Exception ioe) {
//
//		}
//		File newFile = new File(FinalUtil.iconDir + fileName);
//		FileInputStream input = null;
//		FileOutputStream output = null;
//		try {
//			input = new FileInputStream(file);
//			output = new FileOutputStream(newFile);
//			byte[] buffer = new byte[4096];
//			int n = 0;
//			while (-1 != (n = input.read(buffer))) {
//				output.write(buffer, 0, n);
//			}
//		} catch (Exception ioe) {
//			ioe.printStackTrace();
//		} finally {
//			if (output != null) {
//				try {
//					output.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//			if (input != null) {
//				try {
//					input.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}

	/**
	 * 写入SD卡目录
	 * 
	 * @author caosq 2013-5-17 下午2:51:17
	 * @param bitmap
	 * @param savePath
	 * @param fileName
	 */
//	private static void saveBitmap(Bitmap bitmap, String savePath,
//			String fileName) {
//		fileName = fileName.replace("/", "_");
//		if (fileName.charAt(0) == '_') {
//			fileName = fileName.substring(1);
//		}
//		try {
//			File dir = new File(savePath);
//			boolean flag = false;
//			if (!dir.exists()) {
//				flag = dir.mkdir();
//			}
//			if (!flag) {
//				dir.mkdirs();
//			}
//			File f = new File(savePath + fileName);
//			if (f.exists()) {
//				if (!Util.forceDelete(f)) {
//					Log.i("util", "delete failed!");
//					return;
//				}
//			}
//
//			f.createNewFile();
//			FileOutputStream fOut = null;
//
//			fOut = new FileOutputStream(f);
//
//			bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
//
//			fOut.flush();
//
//			fOut.close();
//			Log.i("util", "save icon sucess userid==" + fileName);
//		} catch (Exception e) {
//			Log.e("util", e.getMessage());
//		}
//	}

	/**
	 * 根据 文件名 取得手机中的图片
	 * 
	 * @author caosq 2013-6-6 上午11:20:27
	 * @param fileName
	 * @return
	 */
//	public static Bitmap getIconFromLocal(String fileName) {
//		fileName = fileName.replace("/", "_");
//		if (fileName.charAt(0) == '_') {
//			fileName = fileName.substring(1);
//		}
//		Bitmap bm = null;
//		String filePath = FinalUtil.iconDir + fileName;
//		File file = new File(filePath);
//		if (file.exists()) {
//			bm = BitmapFactory.decodeFile(filePath);
//		}
//		return bm;
//	}

	/**
	 * 转换 Drawable to Bitmap
	 * 
	 * @author caosq 2013-6-6 上午11:19:59
	 * @param drawable
	 * @return
	 */
	public static Bitmap drawableToBitmap(Drawable drawable) {
		try {
			Bitmap bitmap = Bitmap
					.createBitmap(
							drawable.getIntrinsicWidth(),
							drawable.getIntrinsicHeight(),
							drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
									: Bitmap.Config.RGB_565);
			Canvas canvas = new Canvas(bitmap);
			// canvas.setBitmap(bitmap);
			drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
					drawable.getIntrinsicHeight());
			drawable.draw(canvas);

			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 转换 Bitmap to Drawable
	 * 
	 * @author caosq 2013-6-15 下午5:28:41
	 * @param bitmap
	 * @return
	 */
	public static Drawable bitmapToDrawable(Bitmap bitmap) {
		return new BitmapDrawable(bitmap);
	}

	/**
	 * 从本地SD卡程序目录取图片
	 * 
	 * @author caosq 2013-5-17 下午2:37:57
	 * @param fileName
	 * @return
	 */
//	public static Drawable getDrawableFromLocal(String fileName) {
//		System.gc();
//		try {
//			fileName = fileName.replace("/", "_");
//			if (fileName.charAt(0) == '_') {
//				fileName = fileName.substring(1);
//			}
//			Drawable bm = null;
//			String filePath = FinalUtil.iconDir + fileName;
//			File file = new File(filePath);
//			if (file.exists()) {
//				bm = Drawable.createFromPath(filePath);
//			}
//			return bm;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}

	/**
	 * 使用图片 UniversalImageLoad SDK工程 进行图片加载
	 * 
	 * @author caosq 2013-6-20 下午2:19:08
	 * @param context
	 * @param pro_bor
	 * @param imageview
	 * @param path
	 * @param options
	 */
	public static void getNetPicByUniversalImageLoad(final View pro_bor,
			final ImageView imageview, final String path,
			DisplayImageOptions options) {
		if (path == null || path.length() < 1) {
			if (null != pro_bor) {
				pro_bor.setVisibility(View.GONE);
			}
			return;
		}
		ImageLoader imageLoader = ImageLoader.getInstance();
		final String url = AgentConfig.getPicServer() + path;
		imageLoader.displayImage(url, imageview, options,
				new SimpleImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						if (null != pro_bor) {
							pro_bor.setVisibility(View.VISIBLE);
						}
					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						@SuppressWarnings("unused")
						String message = null;
						switch (failReason.getType()) {
						case IO_ERROR:
							message = "Input/Output error";
							break;
						case DECODING_ERROR:
							message = "Image can't be decoded";
							break;
						case NETWORK_DENIED:
							message = "Downloads are denied";
							break;
						case OUT_OF_MEMORY:
							message = "Out Of Memory error";
							break;
						case UNKNOWN:
							message = "Unknown error";
							break;
						}
						if (null != pro_bor) {
							pro_bor.setVisibility(View.GONE);
						}
					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						if (null != pro_bor) {
							pro_bor.setVisibility(View.GONE);
						}
					}
				});
	}

	/**
	 * 获取大文件缩略图
	 * 
	 * @param filePath
	 * @return Bitmap
	 */
	public static Bitmap getImageThumbnail(String filePath) {
		try {
			byte[] imageByte = Util.getByteFromPath(filePath);
			// 以下是把图片转化为缩略图再加载
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length,
					options);
			options.inJustDecodeBounds = false;
			int be = (int) (options.outHeight / (float) 200);
			if (be <= 0) {
				be = 1;
			}
			options.inSampleSize = be;
			return BitmapFactory.decodeByteArray(imageByte, 0,
					imageByte.length, options); // 返回缩略图
		} catch (OutOfMemoryError e) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 放大查看图片的ACTIVITY
	 * 
	 * @author caosq 2013-6-17 下午7:06:17
	 * @param context
	 * @param text
	 *            标题
	 * @param i
	 *            优先显示哪一张
	 * @param picArray
	 *            图片地址
	 */
	public static void magnifyPic(Context context, CharSequence title, int i,
			List<String> picArray) {
		Intent intent = new Intent();
		intent.setClass(context, ShowPicMagnifyActivity.class);
		if (picArray == null) {
			return;
		}
		intent.putStringArrayListExtra(
				OtherGoodsDetailActivity.PIC_MAGNIFY_FLAG,
				new ArrayList<String>(picArray));
		intent.putExtra(OtherGoodsDetailActivity.PIC_MAGNIFY_NAME_FLAG, title);
		context.startActivity(intent);
	}

	/**
	 * 创建默认的图片操作选项
	 * 
	 * @author caosq 2013-6-20 下午4:53:32
	 * @param defaultPic
	 *            默认显示的图片
	 * @param argb8888
	 *            要下载和使用的图片质量
	 * @return
	 */
	public static DisplayImageOptions getDefaultDispalyImageOptions(
			int defaultPic, Config argb8888) {
		return new DisplayImageOptions.Builder().showStubImage(defaultPic)
				.showImageForEmptyUri(defaultPic).showImageOnFail(defaultPic)
				.bitmapConfig(argb8888).cacheInMemory().cacheOnDisc()
				// 图片圆角
				.displayer(new RoundedBitmapDisplayer(5)).build();
	}

	/**
	 * 加载SD卡图片文件
	 * 
	 * @param imageUrl
	 * @return
	 */
	public static Bitmap loadImageFromSd(String imageUrl, int screenWidth,
			int screenHeight, boolean ifHD) {
		if (ifHD) { // 使用原图
			try {
				Bitmap bmp = BitmapFactory.decodeFile(imageUrl);
				return bmp;
			} catch (OutOfMemoryError err) {
				return null;
			}
		}
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(imageUrl, opts);
		opts.inSampleSize = ImageUtil.computeSampleSize(opts, -1, screenWidth
				* screenHeight);
		opts.inJustDecodeBounds = false;
		try {
			Bitmap bmp = BitmapFactory.decodeFile(imageUrl, opts);
			if (bmp == null) {
				return null;
			} else {
				return bmp;
			}
		} catch (OutOfMemoryError err) {
			return null;
		}
	}
}
