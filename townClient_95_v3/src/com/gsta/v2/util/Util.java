package com.gsta.v2.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mobile.encrypt.EncryptUtils;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.ClipboardManager;
import android.text.Selection;
import android.text.Spannable;
import android.util.Log;
import android.widget.EditText;

import com.gsta.v2.activity.R;
import com.gsta.v2.activity.waiter.MsgChatActivity;
import com.gsta.v2.entity.IMBean;

/**
 * 常用工具包
 * 
 * @author boge
 * @time 2013-3-26
 * 
 */
@SuppressLint("SimpleDateFormat")
public class Util {

    static long difference_time = 0;
    final static double EarthRadiu = 6378.137f;

    /**
     * 获取自身类名
     * 
     * @author wubo
     * @createtime 2012-7-9
     */
    public static String getClassName() {
        return new Throwable().getStackTrace()[1].getClassName();
    }

    /**
     * 通讯录
     * 
     * @param str
     * @return String
     */
    public static String getAlpha(String str) {
        if (str == null || str.trim().length() == 0) {
            return "#";
        }
        char c = str.trim().substring(0, 1).charAt(0);
        // 正则表达式，判断首字母是否是英文字母
        Pattern pattern = Pattern.compile("^[A-Za-z]+$");
        if (pattern.matcher(c + "").matches()) {
            return (c + "").toUpperCase(Locale.getDefault());
        } else {
            return "#";
        }
    }

    /**
     * 取得当前时间 格式yyyyMMddHHMMss
     * 
     * @return
     */
    public static String getNowTime() {
        Date now = new Date();
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss",
                Locale.getDefault());
        String formatTime = format.format(now);
        return formatTime;
    }

    /**
     * 取得当前时间 格式yyyy-MM-dd HH:MM:ss
     * <p>
     * 经过与服务器时间偏差处理
     * 
     * @return
     */
    public static String getSysNowTime() {
        Date now = new Date();
        now = new Date(now.getTime() + difference_time);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());
        String formatTime = format.format(now);
        return formatTime;
    }

    /**
     * 系统时间与服务器时间差异
     * 
     * @author wubo
     * @createtime 2012-9-12
     * @param differenceTime
     */
    public static void setDifference_time() {
        difference_time = Util.getServerSysTime() - new Date().getTime();
        Log.e("myOpenfire", "系统时间与服务器时间差异 = " + difference_time);
    }

    /**
     * 获取毫秒级时间
     * 
     * @author wubo
     * @createtime 2012-9-10
     * @return
     */
    public static String getLongDate() {
        return String.valueOf(new Date().getTime());
    }

    /**
     * 毫秒级时间格式化
     * 
     * @author wubo
     * @createtime 2012-9-10
     * @param longDate
     * @return
     */
    public static String getFormatDate(long longDate) {
        Date date = new Date(longDate);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());
        String formatTime = format.format(date);
        return formatTime;
    }

    /**
     * 毫秒级时间格式化
     * 
     * @author wubo
     * @createtime 2012-9-10
     * @param longDate
     * @return
     * @throws ParseException
     */
    // public static String getFormatDate(String timeStr) {
    // DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale
    // .getDefault());
    // try {
    // Date date = format.parse(timeStr);
    // Calendar c = Calendar.getInstance();
    // c.setTime(date);
    // String formatTime = Util.getFormatDate(c.getTimeInMillis());
    // return formatTime;
    // } catch (ParseException e) {
    // e.printStackTrace();
    // return null;
    // }
    // }

    /**
     * 
     * @author longxianwen
     * @Title: getCompareDate
     * @Description: 比较两个格式化日期(yyyy-MM--dd)的先后
     * @param @param startTimeStr
     * @param @param endTimeStr
     * @param @throws ParseException 设定文件
     * @return boolean 返回类型
     * @throws
     */
    // public static boolean compareDate(String startTimeStr, String endTimeStr)
    // throws ParseException {
    //
    // DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale
    // .getDefault());
    // Date startDate = format.parse(startTimeStr);
    // Date endDate = format.parse(endTimeStr);
    //
    // Calendar c1 = Calendar.getInstance();
    // c1.setTime(startDate);
    // Calendar c2 = Calendar.getInstance();
    // c2.setTime(endDate);
    //
    // if (c1.getTimeInMillis() < c2.getTimeInMillis()) {
    // return true;
    // } else if (c1.getTimeInMillis() == c2.getTimeInMillis()) {
    // return true;
    // } else {
    // return false;
    // }
    // }

    /**
     * 
     * @author longxianwen
     * @Title: getFormatDate
     * @Description: 时间格式化，精确到天
     * @param @param longDate
     * @return String 返回类型
     */
    public static String getFormatDateForDay(long longDate) {
        Date date = new Date(longDate);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd",
                Locale.getDefault());
        String formatTime = format.format(date);
        return formatTime;
    }

    /**
     * 时间转换器
     * 
     * @author zsy yyyy-MM-dd HH:mm:ss
     */
    public static Date strToDate(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());
        try {
            ParsePosition pos = new ParsePosition(0);
            Date date = df.parse(str, pos);
            return date;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * 获取12个月份
     * 
     * @author wubo
     * @createtime 2012-9-26
     * @return
     */
    public static List<String> getMonthTime() {
        List<String> months = new ArrayList<String>();
        SimpleDateFormat sd = new SimpleDateFormat("yyyyMM",
                Locale.getDefault());
        Calendar cal = Calendar.getInstance();
        for (int i = 1; i < 13; i++) {
            cal.add(Calendar.MONTH, -1);
            String rs = sd.format(cal.getTime());
            months.add(rs);
        }
        return months;
    }

    public static String getMonth(Date date) {
        SimpleDateFormat sd = new SimpleDateFormat("yyyyMM",
                Locale.getDefault());
        return sd.format(date);
    }

    /**
     * 转换
     * 
     * @author caosq 2013-6-8 下午4:49:43
     * @return
     */
    public static String getNowPullTime() {
        Date now = new Date();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm",
                Locale.getDefault());
        String formatTime = format.format(now);
        return formatTime;
    }

    public static String getDateByFormat(Date date, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.format(date);

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 根据时间取得时间点
     * 
     * @param date
     *            格式如：1338427757079
     * @return
     * 
     *         Add by zhengjy
     */
    public static String getDateDiff(Date date) {
        Calendar today = Calendar.getInstance();
        int nowMonth = today.get(Calendar.MONTH);
        int nowDay = today.get(Calendar.DAY_OF_MONTH);
        int oldDay = date.getDate();
        String result = "";
        try {
            if (nowMonth == date.getMonth()) {
                if (nowDay == oldDay) {
                    result = "今天  " + getDateByFormat(date, "HH:mm");
                } else if (nowDay - oldDay == 1) {
                    result = "昨天  " + getDateByFormat(date, "HH:mm");
                } else if (nowDay - oldDay == 2) {
                    result = "前天  " + getDateByFormat(date, "HH:mm");
                } else
                    result = getDateByFormat(date, "MM月dd日  HH:mm");
            } else
                result = getDateByFormat(date, "MM月dd日  HH:mm");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getSdkardDir() {
        if (Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            File file = new File(Environment.getExternalStorageDirectory()
                    .toString() + "/TeleMarket");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file.toString();
        }
        return null;
    }

    /**
     * 指定图片（path）转换
     * 
     * @param filePath
     * @return
     * 
     * @author wangxin 2012-2-21 上午09:25:02
     */
    public static Bitmap BytestoBitmap(String filePath) {
        try {
            if (filePath == null)
                return null;
            else if (filePath.length() != 0) {
                return BitmapFactory.decodeFile(filePath);
            } else {
                return null;
            }
        } catch (OutOfMemoryError err) {
            return null;
        }
    }

    public static byte[] getByteFromPath(String filename) {
        InputStream is = null;
        try {
            is = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
        }
        return Util.readInputStream(is);
    }

    /**
     * 
     * readInputStream
     * 
     * @param is
     * @return
     */
    public static byte[] readInputStream(InputStream is) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = -1;
        try {
            while ((length = is.read(buffer)) != -1) {
                baos.write(buffer, 0, length);
            }
            baos.flush();
        } catch (IOException e) {
        }
        byte[] data = baos.toByteArray();
        try {
            is.close();
            baos.close();
        } catch (IOException e) {
        }
        return data;
    }

    /**
     * 获取服务器时间
     * 
     * @author wubo
     * @createtime 2012-9-27
     * @return
     */
    public static Long getServerSysTime() {
        try {
            String url = AgentConfig.getSystime();
            SyncHttpClient client = new SyncHttpClient();
            return Long.valueOf(client.httpPost(url, ""));
        } catch (Exception e) {
            // TODO: handle exception
            return new Date().getTime();
        }
    }

    public static boolean forceDelete(File f) {
        boolean result = false;
        int tryCount = 0;
        while (!result && tryCount++ < 5) {
            System.gc();
            result = f.delete();
        }
        return result;
    }

    public static boolean isExitFileSD(String path) {
        File file = new File(path);
        return file.exists();
    }

    public static boolean deleteFileSD(String path) {
        File file = new File(path);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    /**
     * 隐藏手机号
     * 
     * @author wubo
     * @createtime 2012-9-7
     * @param phoneNum
     * @return
     */
    public static String hidePhoneNum(String phoneNum) {
        if (phoneNum == null) {
            return "";
        }
        if (phoneNum.length() == 11) {
            char[] cs = phoneNum.toCharArray();
            cs[3] = '*';
            cs[4] = '*';
            cs[5] = '*';
            cs[6] = '*';
            return new String(cs);
        }
        return phoneNum;
    }

    /**
     * 隐藏姓名
     * 
     * @author wubo
     * @createtime 2012-9-7
     * @param phoneNum
     * @return
     */
    public static String hideName(String name) {
        if (name == null) {
            return "";
        }
        if (name.length() > 1) {
            char first = name.charAt(0);
            return new String(first + "**");
        }
        return name;
    }

    /**
     * 除去域名
     * 
     * @author wubo
     * @createtime 2012-9-8
     * @param userid
     * @return
     */
    public static String trimUserIDDomain(String userid) {
        if (userid != null && userid.length() > 0) {
            int index = userid.indexOf("@");
            if (index != -1) {
                return userid.substring(0, userid.indexOf("@"));
            }
        }
        return userid;
    }

    public static String converString(String username) {
        int i = username.indexOf("@");
        if (i != -1) {
            username = username.replace("@", "\40");
        }
        return username;
    }

    /**
     * 将后台正常图片地址转换成自己需要的大小图片地址
     * 
     * @author caosq 2013-6-6 下午7:59:43
     * @param url
     *            后台正常大小的图片地址
     * @param suffix
     *            要获取的大小 如 FinalUtil.PIC_SMALL
     * @return
     */
    public static String converPicPath(String url, String suffix) {
        if (url == null || url.trim().length() < 1) {
            return null;
        }
        int a = url.lastIndexOf(".");
        String aString = url.substring(a, a + 1);
        return url.replace(aString, suffix + aString);
    }

    public static void copyText(Context context, String text) {
        ClipboardManager cm = (ClipboardManager) context
                .getSystemService(Context.CLIPBOARD_SERVICE);
        cm.setText(text);
    }

    /**
     * 判断代理商等级
     * 
     * @author boge
     * @time 2013-3-27 int
     * @param level
     * @return
     * 
     */
    public static int getLevelCover(String level) {
        if (level == null) {
            return R.drawable.level1;
        }
        if (level.equals(FinalUtil.LEVEL_1)) {
            return R.drawable.level1;
        }
        if (level.equals(FinalUtil.LEVEL_2)) {
            return R.drawable.level2;
        }
        if (level.equals(FinalUtil.LEVEL_3)) {
            return R.drawable.level3;
        }
        if (level.equals(FinalUtil.LEVEL_4)) {
            return R.drawable.level4;
        }
        return R.drawable.level1;
    }

    /**
     * 判断代理商等级 返回文字
     * 
     * @author boge
     * @time 2013-3-27 int
     * @param level
     * @return
     * 
     */
    public static String getLevelCoverTextDescript(String level) {
        if (level == null) {
            return "";
        }
        if (level.equals(FinalUtil.LEVEL_1)) {
            return "普通代理商";
        }
        if (level.equals(FinalUtil.LEVEL_2)) {
            return "银牌代理商";
        }
        if (level.equals(FinalUtil.LEVEL_3)) {
            return "金牌代理商";
        }
        if (level.equals(FinalUtil.LEVEL_4)) {
            return "钻石代理商";
        }
        return "";
    }

    /** 判断邮箱是否存在 */
    public static boolean isEmpty(String x) {
        if (x == null) {
            return true;
        } else if (x.trim().equals("")) {
            return true;
        }
        return false;
    }

    /**
     * 验证邮箱
     * 
     * @author wubo
     * @createtime 2012-9-13
     * @param strEmail
     * @return
     */
    public static boolean isEmail(String strEmail) {
        // ^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$
        String strPattern = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
        Pattern p = Pattern.compile(strPattern);
        Matcher m = p.matcher(strEmail);
        if (m.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 验证手机号
     * 
     * @author wubo
     * @createtime 2012-9-13
     * @param str
     * @return
     */
    public static boolean isCellphone(String str) {
        Pattern pattern = Pattern.compile("1[0-9]{10}");
        Matcher matcher = pattern.matcher(str);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 验证代理商姓名
     * 
     * @author wubo
     * @createtime 2012-9-13
     * @param str
     * @return
     */
    public static boolean isUserName(String str) {
        if (str == null || str.length() < 2 || str.length() > 10) {
            return false;
        }
        Pattern pattern = Pattern.compile("^[\\w\\u4e00-\\u9fa5]*$");
        Matcher matcher = pattern.matcher(str);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取版本编号
     * 
     * @author wubo
     * @time 2012-11-23
     * @param context
     * @return
     * 
     */
    public static String getAppVersionName(Context context) {
        String versionName = "";
        try {
            // ---get the package info---
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = pi.versionName;
            if (versionName == null || versionName.length() <= 0) {
                return "";
            }
        } catch (Exception e) {
            Log.e("VersionInfo", "Exception", e);
        }
        return versionName;
    }

    public static boolean createFile(String fileName) {
        try {
            File file = new File(getSdkardDir(), fileName);
            if (file.exists()) {
                file.delete();
            }
            if (file.createNewFile()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 录音文件存放目录
     * 
     * @author wubo
     * @time 2013-3-19
     * 
     */
    public static String getVoiceDir() {
        File file = new File(FinalUtil.voiceDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        return FinalUtil.voiceDir;
    }

    /**
     * @author longxianwen
     * @Title: DistanceOfTwoPoints
     * @Description: 根据两点间经纬度坐标（double值），计算两点间距离，单位为米
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return int 返回类型
     * @throws
     */
    public static long DistanceOfTwoPoints(double lat1, double lng1,
            double lat2, double lng2) {

        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);

        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
                + Math.cos(radLat1) * Math.cos(radLat2)
                * Math.pow(Math.sin(b / 2), 2)));
        s = s * EarthRadiu;
        return Integer.parseInt(Math.round(s * 1000) + "");
    }

    /**
     * @author longxianwen
     * @Title: rad
     * @Description: 求某个经纬度的值的角度值
     * @param d
     * @return double 返回类型
     * @throws
     */
    private static double rad(double d) {
        return d * Math.PI / 180;
    }

    /**
     * 将editView的光标设置到文字的最后
     */
    public static void setEditCursorToTextEnd(EditText et_dialog) {
        CharSequence text = et_dialog.getText();
        if (text instanceof Spannable) {
            Spannable spanText = (Spannable) text;
            Selection.setSelection(spanText, text.length());
        }
    }

    /**
     * 使用WEBView 加载网页 推荐
     * 
     * @author caosq 2013-4-22 下午3:12:01
     * @param toActivity
     *            // 要使用的WEB Activity
     * @param toUrl
     *            // 要使用的接口地址
     * @param specificationId
     *            类型ID
     * @param speciesId
     *            商品id 跳转去商品详细页
     */
    public static void toWebViewActivity(Context context, Class<?> toActivity,
            String toUrl, List<Parameter> paras) {
        Intent intent = new Intent(context, toActivity);
        intent.putExtra("url", toUrl);
        intent.putExtra("data", getURLDataByParams(paras));
        context.startActivity(intent);
    }

    /**
     * 组装参数
     * 
     * @author caosq 2013-6-21 上午9:47:27
     * @param paras
     * @return
     */
    public static String getURLDataByParams(List<Parameter> paras) {
        if (null == paras || paras.size() == 0) {
            throw new RuntimeException(
                    "getDataByParams方法  List<Parameter> is null or 0");
        }
        paras.add(new Parameter("systime", String.valueOf(new Date().getTime())));
        String token = "";
        try {
            token = EncryptUtils.getAuthenticatorByJaveMD5(paras);
        } catch (Exception e) {
            e.printStackTrace();
        }
        StringBuffer dataString = new StringBuffer();
        // 添加参数
        for (Iterator<Parameter> iter = paras.iterator(); iter.hasNext();) {
            Parameter para = (Parameter) iter.next();
            dataString.append(para.mName + "=" + URLEncoder.encode(para.mValue)
                    + "&");
        }
        // 将 token 更改成 t
        dataString.append("t=" + token);
        return dataString.toString();
    }

    /**
     * 跳转 到 openfire 聊天界面,不是列表界面
     * 
     * @author caosq 2013-4-22 下午3:21:00
     * @param context
     * @param im
     * @return is false 不能与该用户私聊
     */
    public static boolean toIM(Context context, IMBean im) {
        if (im.getHisUserId() == null) {
            return false;
        } else {
            Intent intent = new Intent();
            intent.putExtra(FinalUtil.IMBEAN, im);
            intent.setClass(context, MsgChatActivity.class);
            context.startActivity(intent);
            return true;
        }
    }
}
