package com.gsta.v2.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.gsta.v2.activity.R;

/**
 * 聊天表情选择视图adapter
 * 
 * @author zhengjy
 * 
 */
public class PopupExpressAdapter extends BaseAdapter {

	private Context context;
	public static final int[] gridArray = { R.drawable.p001, R.drawable.p002,
			R.drawable.p003, R.drawable.p004, R.drawable.p005, R.drawable.p006,
			R.drawable.p007, R.drawable.p008, R.drawable.p009, R.drawable.p010,
			R.drawable.p011, R.drawable.p012, R.drawable.p013, R.drawable.p014,
			R.drawable.p015, R.drawable.p016, R.drawable.p017, R.drawable.p018,
			R.drawable.p019, R.drawable.p020 };

	public PopupExpressAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return gridArray.length;
	}

	@Override
	public Integer getItem(int position) {
		// TODO Auto-generated method stub
		return gridArray[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = LayoutInflater.from(context).inflate(
				R.layout.popup_express_item, null);
		ImageView ivExpress = (ImageView) convertView
				.findViewById(R.id.iv_item);
		ivExpress.setImageResource(getItem(position));
		return convertView;
	}

}
