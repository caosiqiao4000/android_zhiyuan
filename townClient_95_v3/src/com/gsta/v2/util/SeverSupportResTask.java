package com.gsta.v2.util;

import java.util.List;

import mobile.http.ConnectivityHelper;
import mobile.http.HttpReqCode;
import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import android.content.Context;
import android.os.AsyncTask;

/**
 * 
 * 异步刷新ui类
 * 
 * @author wangxin
 * 
 */
public class SeverSupportResTask extends AsyncTask<Object, Integer, Object> {
	private String TAG = Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory
			.getLogger();
	private Context context;
	private IUICallBackInterface callBackInterface;
	// request url
	private String url;
	// Parameter
	private List<Parameter> paras;
	// pdShow isShow ?
	// ProgressDialog show message
	// ProgressDialog
	private MyFilePart uploader;

	private String fileName;
	private int caseKey;
	protected SyncHttpClient http = new SyncHttpClient();

	@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub
		if (!ConnectivityHelper.ConnectivityIsAvailable(context)) {
			return HttpReqCode.no_network;
		}

		try {
			paras.add(new Parameter("appId", ((AgentApplication) context
					.getApplicationContext()).getAppid()));
			return http.httpPostWithFile(url, paras, fileName, uploader);
		} catch (Exception e) {
			logger.error(TAG, e);
			return HttpReqCode.error;
		}

	}

	/***
	 * 
	 * SeverSupportTask
	 * 
	 * @param context
	 * @param pdShow
	 * @param paras
	 * @param url
	 * @param callBackInterface
	 */
	public SeverSupportResTask(Context context,
			IUICallBackInterface callBackInterface, String url,
			List<Parameter> paras2, MyFilePart uploader, String fileName,
			boolean pdShow, String pdMsg, int caseKey) {
		this.context = context;
		this.callBackInterface = callBackInterface;
		this.url = url;
		this.paras = paras2;
		this.caseKey = caseKey;
		this.uploader = uploader;
		this.fileName = fileName;
	}

	@Override
	protected void onPostExecute(Object result) {
		if (callBackInterface != null) {
			callBackInterface.uiCallBack(result, this.caseKey);
		}
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(progress);
	}
}
