package com.gsta.v2.util;

import java.io.File;

import android.os.Environment;

/**
 * 所有的常量定义
 * 
 * @author wubo
 * @createtime 2012-9-10
 */
public class FinalUtil {

    /**
     * 登录状态
     */
    public final static int LONGIN_LONGINSTATE_OUT = 0xA1; // 末登录
    public final static int LONGIN_LONGINSTATE_SUCCEED = 0xA2;// 登录成功
    public final static int LONGIN_LONGINSTATE_NOSUCCEED = 0xA3;// 登录失败
    /**
     * 登录成功后,再登出的状态 参考WINDOW设计
     */
    public final static int LONGIN_LONGINSTATE_Destory = 0xA4;// 登录注销
    public final static String LONGIN_STATE_FLAG = "loginState";// 登录标记

    /**
     * 用户从游客状态登录成功的广播
     */
    public final static String RECEIVER_USER_LOGING_IN = "user_login_seccess";
    /**
     * 用户注册成代理商成功后的广播
     */
    public final static String RECEIVER_AGENT_LOGING_IN = "agent_login_seccess";
    /**
     * 用户退出到游客状态的广播
     */
    public final static String RECEIVER_USER_LOGING_OUT = "user_login_out";
    /**
     * 用户退出程序的广播
     */
    public final static String RECEIVER_USER_PROGRAM_OUT = "user_program_out";

    /**
     * 代理商身份状态
     */
    public final static int AgentStatu_Audit = 0;// 审核态
    public final static int AgentStatu_normal = 1;// 正常态
    public final static int AgentStatu_Pause = 2;// 暂停态
    public final static int AgentStatu_Destory = 3;// 注销态
    public final static int AgentStatu_Identity = 11;// 社区经理

    /**
     * 后台图片共有四种 后台统一传递给前台正常大小URL 由前台自己添加转换
     * 
     * 后台传递 例如: /SRMC/Ware/Basic/S5FD0CF1/20130606/basic_112008.jpg
     * <p>
     * 前台需要最小图片 则 /SRMC/Ware/Basic/S5FD0CF1/20130606/basic_112008_small.jpg
     */
    public final static String PIC_SMALL = "_small";
    public final static String PIC_MIDDLE = "_middle";
    public final static String PIC_BIG = "_big";

    /**
     * 前台上传的图片最大不能超过800KB 身份证图片除外
     */
    public final static double PIC_UPLOAD_BIG_SIZE = 800.00;

    /**
     * 登录openfire
     */
    public final static String Login_User = "user";// 登录的uid
    public final static String Login_Pass = "pass";// 登录的密码

    /**
     * 更新版本
     */
    public final static String Version_Name = "version_name";// 版本名称

    /**
     * 用户等级
     */
    public final static String LEVEL_1 = "1001";// 普通
    public final static String LEVEL_2 = "1002";// 银牌
    public final static String LEVEL_3 = "1003";// 金牌
    public final static String LEVEL_4 = "1004";// 钻石

    /**
     * 私信
     */
    public final static String SENDIM_CONTENT = "sendim_content";// 聊天内容
    public final static String SENDIM_USERNAME = "sendim_username";// 自己的用户名
    public final static String SENDIM_HISUID = "sendim_hisuid";// 对方的uid
    public final static String SENDIM_HISNAME = "sendim_hisName";// 对方的用户名
    public final static String SENDIM_HISPHOTO = "sendim_hisPhoto";// 对方的头像
    public final static String SENDIM_TIME = "sendim_time";// 消息时间
    /**
     * 我发送的 谁发的信息(0表示登陆者,1表示聊天对象
     */
    public final static String SENDIM_DIRECTION_MY = "0";
    /**
     * 别人发过来的 谁发的信息(0表示登陆者,1表示聊天对象
     */
    public final static String SENDIM_DIRECTION_OTHER = "1";

    /**
     * sendservice
     */
    public final static String SENDSERVICE_FLAG = "flag";// start 标识
    public final static int sendDefault_value = 0;// default
    public final static int sendMsg = 1;// 发送短信
    public final static int saveCitys = 2;// 保存城市信息
    public final static int saveContacts = 3;// 保存本地联系人信息
    public final static int imLogin = 4;// 私信登录
    public final static int sendIM = 5;// 发送私信
    public final static int sendMin = 6;// min
    public final static int sendUpdate = 7;// 更新程序
    public final static int exitOpenfire = 8;// 退出程序

    /**
     * 私信接受广播
     */
    public final static String RECEIVER_IM_ACTION = "im.receiver.action";
    /**
     * 私信界面更新广播
     */
    public final static String RECEIVER_IM_REFRUSH_ACTION = "im.receiver.refuresh.action";

    /**
     * Intent常用
     */
    public final static String IMBEAN = "IMBEAN";
    public final static String SENTUID = "SENTUID";
    public final static String SENTNAME = "SENTNAME";
    public final static String BASEAGENTINFO = "BASEAGENTINFO";

    public final static String GOODSINFO = "GOODSINFO";

    /**
     * 代理商UID
     */
    public final static String AGENT_UID = "agentUid";

    public final static String SMSUER = "SMSUER";
    public final static String SMSCONTENT = "SMSCONTENT";
    public final static String OPERUID = "operuid";
    public final static String ORDERINFO = "orderinfo";

    public final static String ORDER_ACCOUNT = "order_account";
    public final static String ORDER_PHONE = "order_phone";
    public final static String ORDER_BEGINTIME = "order_begintime";
    public final static String ORDER_ENDTIME = "order_endtime";
    public final static String ORDER_STATUS = "order_status";

    public final static String REALSELL_GOODSNAME = "realsell_goodsname";
    public final static String REALSELL_BEGINTIME = "realsell_begintime";
    public final static String REALSELL_ENDTIME = "realsell_endtime";// 查询的结束时间
    public final static String REALSELL_TYPE = "realsell_type"; // 0 直接 1 间接

    /**
     * 商品分类标记
     */
    public final static String PRODUCT_TYPE = "product_type";
    /**
     * 智能机
     */
    public final static String PRODUCT_TYPE_CALLPHONE = "product_callphone";
    public final static String PRODUCT_NAME_CALLPHONE = "智能机";
    public final static int PRODUCT_FLAG_CALLPHONE = 0x000;
    /**
     * 手机卡
     */
    public final static String PRODUCT_TYPE_PHONECARD = "product_phoneCard";
    public final static String PRODUCT_NAME_PHONECARD = "手机卡";
    public final static int PRODUCT_FLAG_PHONECARD = 0x002;
    /**
     * 光宽带
     */
    public final static String PRODUCT_TYPE_BROADBAND = "product_broadBand";
    public final static String PRODUCT_NAME_BROADBAND = "高速宽带";
    public final static int PRODUCT_FLAG_BROADBAND = 0x001;
    /**
     * 手机+宽带
     */
    public final static String PRODUCT_TYPE_PHONE_BROADBAND = "product_phone_broadBand";
    public final static String PRODUCT_NAME_PHONE_BROADBAND = "手机+宽带";
    public final static int PRODUCT_FLAG_PHONE_BROADBAND = 0x003;

    /**
     * 0自己,1买家 2 用户
     */
    public final static String QUERY_TYPE = "query_type";
    // public final static String QUERY_ACCOUNT = "query_account";

    public final static String ADDNOTE_TYPE = "addnote_type";// 0:增加,1:修改,2: 删除
    public final static String ADDNOTE_GOODNAME = "addnote_goodname";
    public final static String AGENTMEMOINFO = "agentmemoinfo";

    public final static String SENDMMS_NUM = "sendmms_num";
    public final static String SENDMMS_MSG = "sendmms_msg";
    public final static String SENDMMS_ADD = "sendmms_add";
    public final static String SENDMMS_TYPE = "sendmms_type";// 0邀请注册代理商1推荐商品2邀请注册普通用户

    /**
     * imbean不同标识
     */
    public static final String IM_FROM_ME = "0";// 我发送的消息
    public static final String IM_TO_ME = "1";// 对方发送的消息
    public static final String IM_UNREAD = "0";// 私信未阅读
    public static final String IM_READED = "1";// 私信已读
    public static final String IM_UNSEND = "0";// 发送失败
    public static final String IM_SENDED = "1";// 发送成功

    /**
     * SharedPreferences 常量
     */
    public static final String SPF_FIRST = "spf_first";// 是否是第一次开启

    public static final String TABINDEX = "tabindex";// 1会话消息 2最热促销 3我的数码城 4我的店铺
    // 5客户服务

    public final static File sdDir = Environment.getExternalStorageDirectory();// 获取根目录
    public final static String iconDir = sdDir + "/AgentClient/Pics/";// 图片目录
    public final static String voiceDir = sdDir + "/AgentClient/Voices/";// 录音目录
}
