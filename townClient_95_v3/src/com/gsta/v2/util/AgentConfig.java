package com.gsta.v2.util;

/**
 * 
 * @author fitch
 * @desc 与后台相关接口配置信息
 */
public class AgentConfig {

    public static final String TELEMARKET_PACKAGE_NAME = "com.gsta.activity";
    public static final String Service_Head = "http://";
    public static final String LOGIN_IN_NAME = "userName";
    public static final String LOGIN_IN_PASS = "userPassWord";
    // 测试系统
   // public static final String Server_Eshop = "http://121.33.203.178:9999/eshop/";
    //public static final String Server_Location = "http://121.33.203.178:9998/mall_index/";
  //  public static final String Server_Agent = "http://121.33.203.178:9998/agent/";
  //  public static final String Server_Stat = "http://121.33.203.178:9999/agent_stat/";

    // 正式运营系统
     public static final String Server_Eshop = "http://www.95town.cn/eshop/";
     public static final String Server_Location = "http://115.239.134.127/mall_index/";
     public static final String Server_Agent = "http://www.95town.cn/agent/";
     public static final String Server_Stat = "http://www.95town.cn/agent_stat/";
   
    public static final String getPhotoUrl() {
        return getServer() + "/SRMC/agent/images/avatar/";
    }

    public static final String getServer() {
        //return "http://121.33.203.178:9998";
         return "http://www.95town.cn";
    }

    /**
     * 获取头像
     * 
     * @return
     */
    public static String getPicServer() {
        return "http://www.95town.cn";
        // return "http://121.33.203.178:9999";
    }

    /**
     * 位置搜索相关
     */

    // 查询附近店铺
    public static String checkRimShop() {
        return Server_Location + "queryNear";
    }

    // 查找店铺
    public static String searchShop() {
        return Server_Agent + "interface/shopmanager/SearchShop";
    }

    // TOP店铺查询
    public static String searchTopShop() {
        return Server_Agent + "interface/shopmanager/QueryTopNShop";
    }

    // 上传位置
    public static String upLocation() {
        return Server_Location + "upLocation";
    }

    // 中心距离查询
    public static String checkCenterDistance() {
        return Server_Location + "queryWithin";
    }

    /**
     * 用户管理相关
     */

    // 登录
    public static final String getLogin() {
        return Server_Agent + "interface/usermanager/Login";
    }

    /**
     * 客户端软件版本信息查询接口
     * <p>
     * agent/interface/usermanager/QueryVersion
     * 
     * @author caosq 2013-5-7 下午2:23:13
     * @return
     */
    public static final String queryVersion() {
        return Server_Agent + "interface/usermanager/QueryVersion";
    }

    // 用户注册
    public static final String UserRegister() {
        return Server_Agent + "interface/usermanager/UserRegister";
    }

    // 获取短信验证码
    public static final String GetValidateCode() {
        return Server_Agent + "interface/usermanager/GetValidateCode";
    }

    // 邀请注册
    public static final String Invite() {
        return Server_Agent + "interface/usermanager/Invite";
    }

    // 账号检测
    public static final String AccountCheck() {
        return Server_Agent + "interface/usermanager/AccountCheck";
    }

    // 注册代理商信息
    public static final String AgentInfoAdd() {
        return Server_Agent + "interface/usermanager/AgentInfoAdd";
    }

    // 更新代理商信息
    public static final String AgentInfoModify() {
        return Server_Agent + "interface/usermanager/AgentInfoModify";
    }

    // 查询代理商信息
    public static final String AgentInfoQuery() {
        return Server_Agent + "interface/usermanager/AgentInfoQuery";
    }

    /**
     * 提现明细查询接口
     * 
     * @author caosq 2013-4-25 上午11:07:54
     * @return
     */
    public static String getWithdrawUrl() {
        return Server_Agent + "interface/withdraw/QueryWithdrawDetail";
    }

    /**
     * 收支明细查询接口
     * 
     * @author caosq 2013-4-25 上午11:07:49
     * @return
     */
    public static String getInAndComeUrl() {
        return Server_Agent + "interface/withdraw/QueryWithdraw";
    }

    /**
     * 我的账户查询接口 interface/withdraw/QueryMyAccount
     * <p>
     * 根据uid查询该代理商的基本信息和提现账户信息, 该uid必须是代理商身份, 而且已经上传了身份证
     * 
     * @author caosq 2013-4-25 上午11:08:34
     * @return
     */
    public static String queryMyAccount() {
        return Server_Agent + "interface/withdraw/QueryMyAccount";
    }

    /**
     * 身份证上传接口 agent/interface/withdraw/UploadIdentityCard
     * <p>
     * 用户上传身份证图片, 包含身份证正面和反面
     * 
     * @author caosq 2013-4-26 下午6:32:27
     * @return
     */
    public static String uploadIdentityCard() {
        return Server_Agent + "interface/withdraw/UploadIdentityCard";
    }

    /**
     * 申请提现接口 eshop/interface/withdraw/WithDrawCash
     * <p>
     * 用户申请提现, 用户必须是代理商, 且需要先上传身份证, 提现金额不能小于100元, 申请后发起提现流程, 给后台审核
     * 
     * @author caosq 2013-4-27 下午8:20:43
     * @return
     */
    public static String withDrawCash() {
        return Server_Eshop + "interface/withdraw/WithDrawCash";
    }

    // 上传用户头像
    public static final String UpHead() {
        return Server_Agent + "interface/usermanager/UpHead";
    }

    // 获取头像 批量查询用户头像地址
    public static final String GetPhotoPath() {
        return Server_Agent + "interface/usermanager/GetPhotoPath";
    }

    // 密码修改
    public static final String PasswordReset() {
        return Server_Agent + "interface/usermanager/PasswordReset";
    }

    // 根据代理商UID查询代理商信息，包括代理商基本信息、店铺信息
    public static String shopUserInfoQuery() {
        return Server_Agent + "interface/usermanager/ShopInfoQuery";
    }

    /**
     * 商品管理相关
     */

    // 收藏商品到店铺 同时上架了
    public static final String AddToMyMall() {
        return Server_Agent + "interface/product/AddToMyMall";
    }

    public static final String ExtensionMsg() {
        return Server_Agent + "interface/product/ExtensionMsg";
    }

    // 商品收藏查询
    public static String getGoodsCollect() {
        return Server_Agent + "interface/product/QueryProductCollect";
    }

    /**
     * 商品收藏管理接口（包括收藏、取消收藏） interface/product/ProductCollect
     * <p>
     * 用户点击收藏图标的时候，把该商品加入到用户的商品收藏列表，或者取消收藏某件商品。
     * <p>
     * 注意：收藏商品时需要校验，不可以重复收藏同一商品。 取消收藏
     * 
     * @author caosq 2013-5-14 上午11:27:30
     * @return
     */
    public static String cancelGoodsCollect() {
        return Server_Agent + "interface/product/ProductCollect";
    }

    // 商品信息
    public static final String ShowProductDetail() {
        return Server_Agent + "interface/product/ShowProductDetail";
    }

    // 分成描述静态页面
    public static final String Deductinfo() {
        return Server_Agent + "common/deductinfo.html";
    }

    /**
     * 联系人相关
     */

    // 获取我的用户
    public static final String getUsers() {
        return Server_Agent + "interface/contact/getUsers";
    }

    // 获取我的下家
    public static final String getAgents() {
        return Server_Agent + "interface/contact/getAgents";
    }

    // 获取我的卖家
    public static final String getVendors() {
        return Server_Agent + "interface/contact/GetSellers";
    }

    // 获取我的买家
    public static final String getBuyers() {
        return Server_Agent + "interface/contact/getBuyers";
    }

    // 修改买家和用户备注
    public static final String modifyRemark() {
        return Server_Agent + "interface/contact/modifyRemark";
    }

    // 查询买家留言
    public static final String queryMessage() {
        return Server_Agent + "interface/contact/queryMessage";
    }

    // 向买家发送留言
    public static final String sendMessage() {
        return Server_Agent + "interface/contact/sendMessage";
    }

    // 本地粉丝
    public static String getMyShopFansUrl() {
        return Server_Agent + "interface/contact/AgentShopFansQuery";
    }

    /**
     * 店铺管理
     */

    // 关注商家查询接口 根据用户的ID查询该用户收藏的代理商店铺
    public static final String queryAttentionShop() {
        return Server_Agent + "interface/shopmanager/QueryCollectShop";
    }

    // 收藏店铺到 联系人关注商家 代理商店铺收藏管理接口（包括收藏、取消收藏）
    public static final String agentShopAdd() {
        return Server_Agent + "interface/shopmanager/AgentShopCollect";
    }

    // 店铺设置信息查询接口
    public static String agentShopInfoQuery() {
        return Server_Agent + "interface/shopmanager/AgentShopInfoQuery";
    }

    // 店铺促销查询接口
    public static String PromotionQuery() {
        return Server_Agent + "interface/shopmanager/PromotionQuery";
    }

    // 店铺设置信息修改接口
    public static String agentShopSetModify() {
        return Server_Agent + "interface/shopmanager/AgentShopSetModify";
    }

    // 店铺图标上传接口
    public static String uploadShopIcon() {
        return Server_Agent + "interface/shopmanager/uploadShopIcon";
    }

    /**
     * 代金券管理
     */

    // 1.根据登录UID查询本人未使用、已使用、过期代金券信息;2.查询代金券状态类别数量。
    public static String queryCoupon() {
        return Server_Agent + "interface/coupon/QueryCoupon";
    }

    // 代金券激活接口
    public static String activationCoupon() {
        return Server_Agent + "interface/coupon/activationCoupon";
    }

    /**
     * 客服管理
     * 
     * @return
     */

    // 获取我的电信客服人员列表
    public static final String GetTelServiceList() {
        return Server_Agent + "interface/customerservice/GetTelServiceList";
    }

    // 产品查询接口

    /**
     * 商品列表 此处是商场的列表
     * <p>
     * 2013/06/13 增加 @RequestParam(value="agentUid",defaultValue="") String agentUid,
     * <p>
     * 商城总店不传 在管理商品里面要传店铺的 agentUid
     */
    public static final String getGoodsList() {
        // return Server_Eshop + "remote/getGoodsList.json";
        return Server_Eshop + "remote/getMobileGoodsList.json";
    }

    // 代理商的商品列表 此处是个人的列表
    // http://121.33.203.178:9998/agent/interface/product/getAgentGoodsList
    public static final String getAgentGoodsList() {
        // return Server_Eshop + "remote/getAgentGoodsList.json";
        return Server_Agent + "interface/product/getAgentGoodsList";
    }

    /**
     * 店铺产品管理接口 MyMallManager.json
     * <p>
     * 代理人查询当前我的店铺产品信息，用于管理本店铺产品上下架。
     * 
     * @author caosq 2013-5-21 上午11:08:46
     * @return
     */
    // public static final String myMallManager() {
    // return Server_Agent + "remote/MyMallManager.json";
    // // return Server_Eshop + "remote/MyMallManager.json";
    // }

    // 商品信息 商品简略页面 可咨询店主和点击购买(新)
    public static final String shopProduct() {
        return Server_Eshop + "interface/product/shopProduct";
    }

    /**
     * 中转到此网页 必须都加上下列参数
     * <p>
     * agentUid=10014&account=haoy%40gsta.com&loginUid=10013&systime= 1370574249429&specificationId=S5FD0CF1&speciesId=W60F8389&t=51221
     * ae00147ea9ac6a03febdb14f15a 商品信息(网页)
     * 
     * @author caosq 2013-6-7 上午11:19:36
     * @return
     */
    public static final String getShopAdd() {
        return Server_Eshop + "client/product/goProduct";
    }

    /**
     * 立即支付
     * <p>
     * http://121.33.203.178:9999/eshop/client/orderContract/orderPayment
     * 
     * @author caosq 2013-4-17 下午8:01:28
     * @return
     */
    public static final String getOrderPayment() {
        return Server_Eshop + "interface/order/directPayment";
    }

    /**
     * eshop client/member/myOrder/detail
     * 
     * @author caosq 2013-4-19 下午5:50:50
     * @return
     */
    public static final String getOrderDetail() {
        // return Server_Eshop + "client/member/myOrder/detail";
        return Server_Eshop + "client/myOrder/detail";
    }

    /**
     * 备忘录
     */

    // 备忘录管理
    public static final String MemoManger() {
        return Server_Agent + "interface/client/memoManger";
    }

    // 备忘录查询
    public static final String memoQuery() {
        return Server_Agent + "interface/client/memoQuery";
    }

    /**
     * 充值
     */
    public static final String toClientRecharge() {
        return Server_Eshop + "portal/client/toRecharge";
    }

    /**
     * interface/order/QueryOrder
     * 
     * http json 1.根据登录UID查询本人待确认收货,待付款,待发货,已成功,已取消交易信息 2.查询订单状态类别数量。
     * 
     * @author caosq 2013-4-10 上午11:21:10
     * @return
     */
    public static final String myQueryUserOrder() {
        return Server_Agent + "interface/order/QueryUserOrder";
    }

    /**
     * 帐号管理接口 agent/interface/usermanager/AccountManager
     * <p>
     * 包括用户基本信息设置、修改。如：昵称, 姓名, 手机号码, 收货地址, 联系地址等。
     * 
     * @author caosq 2013-5-3 下午2:57:47
     * @return
     */
    public static final String myUserAccount() {
        return Server_Agent + "interface/usermanager/AccountManager";
    }

    /**
     * 帐号查询接口 agent/interface/usermanager/QuerySubscriber
     * <p>
     * 查询用户基本信息：头像、账户名称、信用级别、姓名、收货地址等
     * 
     * @author caosq 2013-5-3 下午6:12:33
     * @return
     */
    public static final String myUserQuerySubscriber() {
        return Server_Agent + "interface/usermanager/QuerySubscriber";
    }

    /**
     * 取消订单接口 eshop/interface/order/CancelOrder
     * 
     * http json 取消订单, 同时商品库存回滚, 释放号码, 返回积分等
     * 
     * @author caosq 2013-4-17 下午4:12:29
     * @return
     */
    public static final String myCancelOrder() {
        return Server_Eshop + "interface/order/CancelOrder";
    }

    /**
     * 申请退款接口 eshop/interface/order/DrawBack
     * 
     * http json 用户申请退款, 该订单必须是已支付状态, 申请退款之后由后台进行审核
     * 
     * @author caosq 2013-4-18 下午4:45:58
     * @return
     */
    public static final String myUserDrawBack() {
        return Server_Eshop + "interface/order/DrawBack";
    }

    /**
     * 确认收货接口 eshop/interface/order/ConfirmGoods
     * 
     * http json 确认收货, 同时发放代金券
     * 
     * @author caosq 2013-4-18 上午10:34:40
     * @return
     */
    public static final String myConfirmGoods() {
        return Server_Eshop + "interface/order/ConfirmGoods";
    }

    /**
     * 代理商店铺订单查询接口
     * 
     * http json 1.根据登录UID查询本人待确认收货,待付款,待发货,已成功,已取消交易信息 2.查询订单状态类别数量。
     * 
     * @author caosq 2013-4-10 上午11:21:10
     * @return
     */
    public static final String myQueryAgentOrder() {
        return Server_Agent + "interface/order/QueryAgentOrder";
    }

    /**
     * 系统管理
     */

    // 获取系统时间
    public static final String getSystime() {
        return Server_Agent + "interface/getSystime";
    }

    /**
     * 网页重新生成session
     */
    public static final String reLogin() {
        return Server_Eshop + "client/member/login";
    }

    /**
     * 业绩管理
     */

    // 查询月业绩
    public static final String GetMonthReport() {
        return Server_Agent + "interface/stat/GetMonthReport";
    }

    // 查询账单
    public static final String OrderQuery() {
        return Server_Agent + "interface/order/OrderQuery";
    }

    /**
     * 6.4 实时业绩查询接口(新) 可以根据商品名称查询业绩信息，支持模糊查询 实时业绩
     * 
     * @author caosq 2013-5-25 下午4:29:53
     * @return
     */
    public static final String RealSellInfo() {
        // return Server_Agent + "interface/stat/RealSellInfo";
        return Server_Agent + "interface/stat/RealTimeInfo";
    }

    /**
     * 短地址
     */
    public static final String ShortUrlGenerator() {
        return Server_Agent + "interface/product/ShortUrlGenerator.json";
    }

    // 注册短地址
    public static final String ShortUrlRegister() {
        return Server_Agent + "interface/product/ShortUrlRegister.json";
    }

    public static final String getRegisterUrl(String uid) {
        return "http://www.95town.cn/agent/user/agentSysUsersBase/openRegister?uid="
                + uid;
    }

    public static final String downLoadPath() {
        return "http://t.95town.cn/Zbe6Zv";
    }

}
