package com.gsta.v2.util;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;

import com.gsta.v2.activity.R;

/**
 * 文字图片互转
 * 
 * @author boge
 * @time 2013-3-26
 * 
 */
public class SmileyParser {
	// Singleton stuff

	private static SmileyParser sInstance;

	public static SmileyParser getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new SmileyParser(context);
		}
		return sInstance;
	}

	private final Context mContext;
	private final String[] mSmileyTexts;
	private final Pattern mPattern;
	private final HashMap<String, Integer> mSmileyToRes;
	public final String s_warning = "warning";
	private int r_warning;

	private SmileyParser(Context context) {
		mContext = context;
		mSmileyTexts = mContext.getResources().getStringArray(
				DEFAULT_SMILEY_TEXTS);
		r_warning = R.drawable.msgwarningout;
		mSmileyToRes = buildSmileyToRes();
		mPattern = buildPattern();

	}

	static class Smileys {
		private static final int[] sIconIds = { R.drawable.p001,
				R.drawable.p002, R.drawable.p003, R.drawable.p004,
				R.drawable.p005, R.drawable.p006, R.drawable.p007,
				R.drawable.p008, R.drawable.p009, R.drawable.p010,
				R.drawable.p011, R.drawable.p012, R.drawable.p013,
				R.drawable.p014, R.drawable.p015, R.drawable.p016,
				R.drawable.p017, R.drawable.p018, R.drawable.p019,
				R.drawable.p020 };
	}

	public static final int DEFAULT_SMILEY_TEXTS = R.array.express_item_texts;
	public static final int DEFAULT_SMILEY_NAMES = R.array.express_item_names;

	private HashMap<String, Integer> buildSmileyToRes() {
		if (Smileys.sIconIds.length != mSmileyTexts.length) {
			throw new IllegalStateException("Smiley resource ID/text mismatch");
		}

		HashMap<String, Integer> smileyToRes = new HashMap<String, Integer>(
				mSmileyTexts.length);
		for (int i = 0; i < mSmileyTexts.length; i++) {
			smileyToRes.put(mSmileyTexts[i], Smileys.sIconIds[i]);
		}
		// 附加
		smileyToRes.put(s_warning, r_warning);
		return smileyToRes;
	}

	// 构建正则表达式
	private Pattern buildPattern() {
		StringBuilder patternString = new StringBuilder(mSmileyTexts.length * 3);
		patternString.append('(');
		for (String s : mSmileyTexts) {
			patternString.append(Pattern.quote(s));
			patternString.append('|');
		}
		// 附加
		patternString.append(Pattern.quote(s_warning));
		patternString.append('|');
		patternString.replace(patternString.length() - 1, patternString
				.length(), ")");

		return Pattern.compile(patternString.toString());
	}

	// 根据文本替换成图片
	public CharSequence addSmileySpans(CharSequence text) {
		if (text != null && text.length() > 0) {
			SpannableStringBuilder builder = new SpannableStringBuilder(text);
			Matcher matcher = mPattern.matcher(text);
			while (matcher.find()) {
				int resId = mSmileyToRes.get(matcher.group());
				builder.setSpan(new ImageSpan(mContext, resId),
						matcher.start(), matcher.end(),
						Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
			return builder;
		}
		return text;
	}
}
