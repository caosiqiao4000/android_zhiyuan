package com.gsta.v2.util;

import java.util.ArrayList;
import java.util.List;

import mobile.http.MyFilePart;
import mobile.http.Parameter;
import android.content.Context;

/**
 * 
 * 后台调用服务器类
 * 
 * 
 * @author wangxin
 * 
 */
public class ServerSupportManager {
	private Context context;
	/** server interface */
	private IUICallBackInterface callBackInterface;

	/**
	 * 
	 * ServerSupportManager
	 * 
	 * @param context
	 * @param callBackInterface
	 */
	public ServerSupportManager(Context context,
			IUICallBackInterface callBackInterface) {
		this.context = context;
		this.callBackInterface = callBackInterface;
	}

	/**
	 * 
	 * 调用后台接口
	 * 
	 * @param url
	 *            接口地址
	 * @param paras
	 *            请求参数
	 * @param pdShow
	 *            是否需要等待框提示 false,不提示;true提示
	 * @param msg
	 *            显示的消息
	 */
	public void supportRequest(String url, List<Parameter> paras,
			boolean pdShow, String pdMsg) {
		SeverSupportTask task = new SeverSupportTask(context,
				callBackInterface, url, paras, pdShow, pdMsg, 999);
		task.execute();
	}

	/**
	 * 
	 * 调用后台接口
	 * 
	 * @param url
	 *            接口地址
	 * @param paras
	 *            请求参数
	 * @param pdShow
	 *            是否需要等待框提示 false,不提示;true提示
	 * @param msg
	 *            显示的消息
	 * @param caseKey
	 *            case处理，不同业务区分标识
	 */
	public void supportRequest(String url, List<Parameter> paras,
			boolean pdShow, String pdMsg, int caseKey) {
		SeverSupportTask task = new SeverSupportTask(context,
				callBackInterface, url, paras, pdShow, pdMsg, caseKey);
		task.execute();
	}

	/**
	 * supportRequest
	 * 
	 * @param url
	 *            调用接口地址
	 * @param paras
	 *            调用参数
	 */
	public void supportRequest(String url, List<Parameter> paras) {

		SeverSupportTask task = new SeverSupportTask(context,
				callBackInterface, url, paras, false, "", 999);
		task.execute();
	}

	/**
	 * 
	 * supportRequest
	 * 
	 * @param url
	 *            调用接口地址
	 * @param paras
	 *            调用参数
	 * @param caseKey
	 *            case处理，不同业务区分标识
	 */
	public void supportRequest(String url, List<Parameter> paras, int caseKey) {
		SeverSupportTask task = new SeverSupportTask(context,
				callBackInterface, url, paras, false, "", caseKey);

		task.execute();

	}

	/**
	 * 文件上传
	 * 
	 * @param url
	 * @param paras
	 * @param file
	 * @param fileName
	 * @param pdShow
	 * @param mess
	 * @param caseKey
	 *            void
	 */
	public void supportRequest(String url, List<Parameter> paras,
			MyFilePart uploader, String fileName, boolean pdShow, String mess,
			int caseKey) {
		SeverSupportResTask task = new SeverSupportResTask(context,
				callBackInterface, url, paras, uploader, fileName, pdShow,
				mess, caseKey);
		task.execute();

	}

	/**
	 * 文件上传
	 * 
	 * @param url
	 * @param paras
	 * @param file
	 * @param fileName
	 * @param pdShow
	 * @param mess
	 * @param caseKey
	 *            void
	 */
	public void supportRequest(String url, List<Parameter> paras,
			MyFilePart uploader, byte[] byteCover, int caseKey) {
		SeverSupportTResByteTask task = new SeverSupportTResByteTask(context,
				callBackInterface, url, paras, uploader, byteCover, caseKey);
		task.execute();

	}

	public void supportRequest(String url, List<Parameter> paras2,
			MyFilePart uploader1, MyFilePart uploader2, boolean pdShow,
			String pdMsg, int caseKey) {
		SeverSupportIdentityTask task = new SeverSupportIdentityTask(context,
				callBackInterface, url, paras2, uploader1, uploader2, pdShow,
				pdMsg, caseKey);
		task.execute();

	}

	/**
	 * 上传文件不限个数
	 * 
	 * @author wubo
	 * @time 2013-3-22
	 * @param url
	 * @param paras2
	 * @param uploader1
	 * @param uploader2
	 * @param pdShow
	 * @param pdMsg
	 * @param caseKey
	 * 
	 */
	public void supportRequest(String url, List<Parameter> paras2,
			ArrayList<MyFilePart> uploaders, boolean pdShow, String pdMsg, int caseKey) {
		SeverSupportMoreFileTask task = new SeverSupportMoreFileTask(context,
				callBackInterface, url, paras2, uploaders, pdShow, pdMsg,
				caseKey);
		task.execute();

	}
}
