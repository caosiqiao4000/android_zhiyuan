package com.gsta.v2.util;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.R;
import com.gsta.v2.ui.CustomDialog;
import com.gsta.v2.ui.MyDialog;

/**
 * 当需要上传图片和验证输入字段时使用   可以共用
 * 
 * @author caosq
 * 
 */
public abstract class CameraAndShowDiagleActivity extends CameraBaseActivity {
    private Logger logger = LoggerFactory.getLogger();
    private static final String TAG = Util.getClassName();// log tag
    protected static final int DIALOG_ID_name = 0x101; // 名称
    protected static final int DIALOG_ID_nickName = 0x102; // 昵称
    protected static final int DIALOG_ID_contantAddress = 0x103; // 联系地址
    protected static final int DIALOG_ID_num = 0x104; // 编号
    protected static final int DIALOG_ID_phone = 0x105; // 电话
    protected static final int DIALOG_ID_mobile = 0x106; // 手机
    protected static final int DIALOG_ID_address = 0x107; // 收货地址
    protected static final int DIALOG_ID_remark = 0x108; // 评论
    protected static final int DIALOG_ID_IMAGE = 0x110; // 图片标记 裁剪
    protected static final int DIALOG_ID_IMAGE_NO = 0x111; // 图片标记 不裁剪

    protected static final int DIALOG_ID_PHONE = DIALOG_ID_phone;
    protected static final int DIALOG_ID_MOBILE = DIALOG_ID_mobile;
    protected static final int DIALOG_ID_INTRODUCE = 0x121;// 店铺公告
    protected static final int DIALOG_ID_EMAIL = 0x125; // 邮箱
    protected static final int DIALOG_ID_QQ = 0x126; // QQ
    protected static final int DIALOG_ID_MSN = 0x127; // MSN
    protected static final int DIALOG_ID_POST = 0x128; // 邮编
    protected static final int DIALOG_ID_ADDRESS = DIALOG_ID_address;
    protected static final int DIALOG_ID_CITY = 0x130; // 城市
    protected static final int DIALOG_ID_DOMAIN = 0x131;// 域名
    protected static final int DIALOG_ID_SHOPNAME = 0x132;// 店铺名称

    private MyDialog imageSelectDialog = null;
    protected int currentDialogId = -1;
    protected View dialogContentView;// 弹出层
    protected EditText et_dialog;// 弹出层的输入框
    protected static final String KEY_STRING = "key";

    @Override
    protected Dialog onCreateDialog(int id, Bundle args) {
        CustomDialog.Builder customBuilder = new
                CustomDialog.Builder(setCreateContext());
        String a = "";
        if (null != args) {
            a = args.getString(KEY_STRING);
        }
        String titleString = null;
        boolean ifShow = true;
        LayoutInflater flater = LayoutInflater.from(setCreateContext());
        // 自定义弹出view
        dialogContentView = flater.inflate(R.layout.dialog_editview, null);
        et_dialog = (EditText) dialogContentView.findViewById(R.id.et_dialog);
        et_dialog.setText(null == a ? "" : a);
        switch (id) {
        case DIALOG_ID_phone:
            titleString = "固话";
            et_dialog.setSingleLine(true);
            et_dialog.addTextChangedListener(new TextCountLimitWatcher(20,
                    et_dialog));
            et_dialog.setKeyListener(new EditTextKeyListener(
                    EditTextKeyListener.PHONE_TYPE));
            break;
        case DIALOG_ID_mobile:
            titleString = "手机号";
            et_dialog.addTextChangedListener(new TextCountLimitWatcher(11,
                    et_dialog));
            et_dialog.setSingleLine(true);
            et_dialog.setKeyListener(new EditTextKeyListener(
                    EditTextKeyListener.PHONE_TYPE));
            break;
        case DIALOG_ID_name:
            titleString = "用户姓名";
            et_dialog.addTextChangedListener(new TextCountLimitWatcher(20,
                    et_dialog));
            et_dialog.setLines(3);
            break;
        case DIALOG_ID_nickName:
            titleString = "用户昵称";
            et_dialog.addTextChangedListener(new TextCountLimitWatcher(20,
                    et_dialog));
            et_dialog.setLines(3);
            break;
        case DIALOG_ID_num:
            titleString = "身份证号";
            et_dialog.addTextChangedListener(new TextCountLimitWatcher(18,
                    et_dialog));
            et_dialog.setLines(2);
            et_dialog.setKeyListener(new EditTextKeyListener(
                    EditTextKeyListener.CARD_TYPE));
            break;
        case DIALOG_ID_remark:
            titleString = "备注";
            et_dialog.setLines(5);
            et_dialog.addTextChangedListener(new TextCountLimitWatcher(140,
                    et_dialog));
            break;
        case DIALOG_ID_address:
            titleString = "地址";
            et_dialog.setLines(3);
            et_dialog.addTextChangedListener(new TextCountLimitWatcher(100,
                    et_dialog));
            break;
        case DIALOG_ID_contantAddress:
            titleString = "联系地址";
            et_dialog.setLines(3);
            et_dialog.setMinHeight(120);
            et_dialog.addTextChangedListener(new TextCountLimitWatcher(100,
                    et_dialog));
            break;
        default:
            ifShow = false;
            break;
        }
        et_dialog.setHint(R.string.please_input);
        Util.setEditCursorToTextEnd(et_dialog);
        customBuilder.setTitle(null == titleString ? "无标题" : titleString);
        customBuilder.setContentView(dialogContentView);
        customBuilder.setPositiveButton(R.string.submit, setDialogClickListener());
        customBuilder.setNegativeButton(R.string.cancel, null);
        if (ifShow) {
            return customBuilder.create();
        } else {
            return onCreateDialog(id);
        }
    }

    @Override
    protected Dialog onCreateDialog(final int id) {
        // 自定义builder用于弹出框
        Log.w(TAG, "调用一个参数的onCreateDialog");
        boolean ifShow = true;
        final int b = id;
        if (id == DIALOG_ID_IMAGE_NO || id == DIALOG_ID_IMAGE) {
            ListView listView = (ListView) LayoutInflater.from(setCreateContext()).inflate(R.layout.dialog_list_view, null);
            String[] objects = { getResourcesMessage(R.string.open_camera), getResourcesMessage(R.string.local_photo) };
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(setCreateContext(), R.layout.dialog_text_view, objects);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (b == DIALOG_ID_IMAGE_NO) {
                        doSelectPhoto(position);// 拍照还是从图库选择图片
                    } else {
                        doSelection(position);// 拍照还是从图库选择图片
                    }
                    MyDialog.dismiss(imageSelectDialog);
                }
            });
            imageSelectDialog = MyDialog.showDialog(setCreateContext(), getResourcesMessage(R.string.chooice), listView);
        } else {
            ifShow = false;
        }
        if (ifShow) {
            return null;
        } else {
            return super.onCreateDialog(id);
        }
    }

    /**
     * 在父类代码中加入监听 这里只对不是图片的使用有效
     */
    protected abstract Dialog.OnClickListener setDialogClickListener();

    /**
     * 设置产生窗口的ACTIVITY
     */
    protected abstract Context setCreateContext();

    /**
     * 先清除之前的dialog再显示新的dialog
     * 
     * @param id
     */
    protected void mShowDialog(int id) {
        if (currentDialogId != -1) {
            // 移除dialog
            removeDialog(currentDialogId);
        }
        currentDialogId = id;
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        // 判断Android系统SDK的版本，我们一般用currentVersion < android.os.Build.VERSION_CODES.FROYO
        // 的方式进行判断是2.2以下版本
        if (currentVersion < android.os.Build.VERSION_CODES.FROYO) {
            logger.info("手机Android系统SDK 版本小于2.2 == " + currentVersion);
            onCreateDialog(id);
        } else {
            showDialog(id);// 弹出浮层
        }
    }

    protected String dealString(String string) {
        if (TextUtils.isEmpty(string) || string.trim().length() == 0) {
            return "";
        }
        return string;
    }

    /**
     * 先清除之前的dialog再显示新的dialog
     * 
     * @param id
     */
    protected void mShowDialog(int id, Bundle bundle) {
        if (currentDialogId != -1) {
            // 移除dialog
            removeDialog(currentDialogId);
        }
        currentDialogId = id;
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        // 判断Android系统SDK的版本，我们一般用currentVersion < android.os.Build.VERSION_CODES.FROYO
        // 的方式进行判断是2.2以下版本
        if (currentVersion < android.os.Build.VERSION_CODES.FROYO) {
            logger.info("手机Android系统SDK 版本小于2.2 == " + currentVersion);
            onCreateDialog(id, bundle);
        } else {
            showDialog(id, bundle);// 弹出浮层
        }
    }
}
