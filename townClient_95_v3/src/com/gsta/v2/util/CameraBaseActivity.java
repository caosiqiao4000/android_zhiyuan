package com.gsta.v2.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.BaseActivity;
import com.gsta.v2.activity.R;

/**
 * 调用 系统相机 拍照和取得相片
 * @author caosq
 * 2013-6-15
 */
public abstract class CameraBaseActivity extends BaseActivity {

	private String TAG = "CameraActivity";// log tag

	private static final Logger logger = LoggerFactory.getLogger();

	public final String IMAGE_UNSPECIFIED = "image/*";
	public String currPhotoName;
	public String tempPhotoPath;
	public Bitmap photo;
	protected Bitmap bitmap;
	public Uri photoUri;
	public String filePath;  //不裁剪时的原图地址

	private final int COMPRESS = 300;

	public final int OPEN_CAMERA = 1; // 打开相机并切割
	public final int LOCAL_PICTURE = 2; // 打开本地相册并切割
	public final int ZOOM_RESULT = 3; // 切割图片返回结果

	public final int PICTURE = 4; // 打开本地相册
	public final int CAMERA = 5; // 打开相机

	public String tempFile = null;
	public ImageView imageview = null;
	public TextView txtTime;

	private void init() {
		currPhotoName = Util.getNowTime() + ".jpg";
		tempPhotoPath = Util.getSdkardDir() + "/" + currPhotoName;
	}
	
	public void createInit(){
	    init();
	}

	/**
	 * 打开相机
	 * 
	 * @param activity
	 */
	public void onlyCamera() {
		init();
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = new File(Util.getSdkardDir(), currPhotoName);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
		startActivityForResult(intent, CAMERA);
	}

	/**
	 * 打开本地相册
	 * 
	 * @param activity
	 */
	public void onlyPicture() {
		init();
		Intent intent = new Intent(Intent.ACTION_PICK, null);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				IMAGE_UNSPECIFIED);
		startActivityForResult(intent, PICTURE);
	}

	/**
	 * 打开相机并切割
	 */
	public void openCamera() {
		init();
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = new File(Util.getSdkardDir(), currPhotoName);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
		startActivityForResult(intent, OPEN_CAMERA);
	}

	/**
	 * 打开本地相册并切割
	 */
	public void openPicture() {
		init();
		Intent intent = new Intent(Intent.ACTION_PICK, null);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				IMAGE_UNSPECIFIED);
		startActivityForResult(intent, LOCAL_PICTURE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 0)
			return;
		if (data != null) {
			photoUri = data.getData();
			if (photoUri != null) {
				String path = getPhotoPath(photoUri);
				if (path != null) {
					setPhotoPath(path);
				}
			}
		}

		if (requestCode == LOCAL_PICTURE) { // 读取相册图片切割
			startPhotoZoom(data.getData());
		}
		if (requestCode == OPEN_CAMERA) { // 读取拍照图片切割
			File file = new File(tempPhotoPath); // 设置文件保存路径
			setPhotoPath(file.toString());
			startPhotoZoom(Uri.fromFile(file));
		}
		if (requestCode == ZOOM_RESULT) { // 切割结果
			createPhoto(data.getExtras());
		}
		if (requestCode == CAMERA) { // 拍照图片 
			// File file = new File(UtilHelper.getSdkardPangkerDir() +
			// "/temp.jpg"); // 设置文件保存路径
			getPhoto(tempPhotoPath);
		}
		if (requestCode == PICTURE) { // 读取相册图片
			getPhoto(getPhotoPath());
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 切割图片之后返回的 Bundle数据处理，取得bitmap类型图片
	 * 
	 * @param extras
	 *            void
	 */
	public void createPhoto(Bundle extras) {
		if (extras != null) {
			if (photo != null) {
				photo.recycle();
			}
			photo = extras.getParcelable("data");

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			try {
				photo.compress(Bitmap.CompressFormat.JPEG, 75, stream);// (0 -
				if (photo != null) {
					setImage(photo);
				} else
					showToast(R.string.res_get_photo_fail);
				stream.close();
			} catch (IOException e) {
				logger.error(TAG, e);
			} catch (Exception e) {
				logger.error(TAG, e);
			}

			/*
			 * Intent mIntent = new Intent(ACTION_NAME); sendBroadcast(mIntent);
			 * // 发送广播
			 */}
	}

	/**
	 * 返回bitmap图片 图片上传模块
	 * 
	 * @param uri
	 *            void
	 */
	public void getPhoto(String filePath) {
		if (filePath != null) {
			setPhotoPath(filePath);
			try {
				bitmap = Util.BytestoBitmap(filePath);
				if (bitmap == null) {
					bitmap = ImageUtil.getImageThumbnail(filePath);
				}
				if (bitmap != null) {
					bitmap = getBitmapScale(bitmap, COMPRESS, COMPRESS);
					setImage(bitmap);
				} else
					showToast(R.string.res_get_photo_fail);
			} catch (OutOfMemoryError e) {
				logger.error(TAG, e);
				showToast(R.string.res_get_photo_fail);
			} catch (Exception e) {
				logger.error(TAG, e);
				showToast(R.string.res_get_photo_fail);
			}
		}
	}

	public void setImage(Bitmap bitmap) {
		imageview.setImageBitmap(bitmap);
		imageview.setVisibility(View.VISIBLE);
		if (txtTime != null) {
			txtTime.setVisibility(View.VISIBLE);
			txtTime.setText("时间：" + Util.getSysNowTime());
		}
	}

	public Bitmap getBitmapScale(Bitmap bitmap, int newWidth, int newHeight) {
		int bmpwidth = bitmap.getWidth();
		int bmpheight = bitmap.getHeight();
		// int newWidth = 480;
		// int newHeight = 450;
		float scaleWidth = ((float) newWidth) / bmpwidth;
		float scaleHeight = ((float) newHeight) / bmpheight;
		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap newbitmap = Bitmap.createBitmap(bitmap, 0, 0, bmpwidth,
				bmpheight, matrix, true);
		return newbitmap;
	}

	/**
	 * 裁剪图片
	 * 
	 * @param uri
	 *            void
	 */
	public void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, IMAGE_UNSPECIFIED);
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 64);
		intent.putExtra("outputY", 64);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, ZOOM_RESULT);
	}

	/**
	 * 获取图片在sd卡中的路径
	 * 
	 * @param uri
	 * @return String
	 */
	public String getPhotoPath(Uri uri) {
		String[] proj = { MediaStore.Images.Media.DATA,
				MediaStore.Images.Media._ID };
		Cursor cursor = managedQuery(uri, proj, null, null, null);
		if (cursor == null) {
			return null;
		}
		int colum_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		String path = cursor.getString(colum_index);
		return path;
	}

	/**
	 * 上传头像图片方式（经过切割）
	 * 
	 * @param which
	 */
	public void doSelection(int which) {
		if (null == Util.getSdkardDir()) {
			showToast("未安装SD卡,无法使用");
			return;
		}
		if (which == 0) {
			openCamera();
		}
		if (which == 1) {
			openPicture();
		}

	}

	/**
	 * 上传图片方式（不经过切割）
	 * 
	 * @param which
	 */
	public void doSelectPhoto(int which) {
		if (which == 0) {
			onlyCamera();
		}
		if (which == 1) {
			onlyPicture();
		}
	}

	public Bitmap getPhoto() {
		return photo;
	}

	public void setPhoto(Bitmap photo) {
		this.photo = photo;
	}

	public Uri getPhotoUri() {
		return photoUri;
	}

	public void setPhotoUri(Uri photoUri) {
		this.photoUri = photoUri;
	}

	public String getPhotoPath() {
		return filePath;
	}

	public void setPhotoPath(String photoPath) {
		this.filePath = photoPath;
	}

	protected byte[] getBompress(int width, int height, int quality) {
		byte[] bytes = null;
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		int bitmapWidth = photo.getWidth();
		int bitmapHeight = photo.getHeight();
		Matrix matrix = new Matrix();
		// 缩放图片的尺寸
		float scaleWidth = (float) width / bitmapWidth;
		float scaleHeight = (float) height / bitmapHeight;
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap resizeBitmap = Bitmap.createBitmap(photo, 0, 0, bitmapWidth,
				bitmapHeight, matrix, false);
		try {
			resizeBitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream);
			bytes = stream.toByteArray();
			if (!resizeBitmap.isRecycled()) {
				resizeBitmap.recycle();
			}
		} catch (Exception e) {
			logger.error(TAG, e);
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(TAG, e);
			}
		}
		return bytes;
	}

	protected File getCompress(int width, int height, int quality) {
		if (photo == null) {
			return null;
		}
		int bitmapWidth = photo.getWidth();
		int bitmapHeight = photo.getHeight();
		Matrix matrix = new Matrix();
		// 缩放图片的尺寸
		float scaleWidth = (float) width / bitmapWidth;
		float scaleHeight = (float) height / bitmapHeight;
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap resizeBitmap = Bitmap.createBitmap(photo, 0, 0, bitmapWidth,
				bitmapHeight, matrix, false);

		File tempFile = null;
		FileOutputStream out;
		try {
			tempFile = new File(tempPhotoPath);
			out = new FileOutputStream(tempFile);
			if (resizeBitmap.compress(Bitmap.CompressFormat.JPEG, quality, out)) {
				out.flush();
				out.close();
			}
			if (!resizeBitmap.isRecycled()) {
				resizeBitmap.recycle();
			}
		} catch (IOException e) {
			// TODO Auto-IOException catch block
			logger.error(TAG, e);
			return null;
		}
		return tempFile;
	}

	public File getCompress() throws IOException {
		if (Util.isEmpty(filePath)) {
			return null;
		}
		File file2 = new File(tempPhotoPath);
		if (!file2.exists()) {
			file2.createNewFile();
		}
		FileOutputStream out = new FileOutputStream(file2);
		if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)) {
			out.flush();
			out.close();
		}
		return file2;
	}

}
