package com.gsta.v2.util;

import android.text.method.NumberKeyListener;

/**
 *  EditText输入框字符控制
 * 
 * @author wubo
 *@createtime 2012-9-14
 */
public class EditTextKeyListener extends NumberKeyListener {

	private int type = 0;
	public final static int PHONE_TYPE = 1;
	public final static int EMAIL_TYPE = 2;
	public final static int NUMBER_TYPE = 3;
	public final static int CARD_TYPE = 4;
	public final static char[] phone_char = new char[] { '0', '1', '2', '3',
			'4', '5', '6', '7', '8', '9', '-' };
	public final static char[] email_char = new char[] { '0', '1', '2', '3',
			'4', '5', '6', '7', '8', '9', '@', '.', '_', 'a', 'b', 'c', 'd',
			'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
			'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
			'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
			'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
	public final static char[] number_char = new char[] { '0', '1', '2', '3',
			'4', '5', '6', '7', '8', '9' };
	public final static char[] card_char = new char[] { '0', '1', '2', '3',
			'4', '5', '6', '7', '8', '9', 'X', 'x' };

	public EditTextKeyListener() {
		this.type = EMAIL_TYPE;
	}

	public EditTextKeyListener(int type) {
		this.type = type;
	}

	@Override
	protected char[] getAcceptedChars() {
		// TODO Auto-generated method stub
		switch (type) {
		case PHONE_TYPE:
			return phone_char;
		case EMAIL_TYPE:
			return email_char;
		case NUMBER_TYPE:
			return number_char;
		case CARD_TYPE:
			return card_char;
		default:
			return email_char;
		}
	}

	@Override
	public int getInputType() {
		// TODO Auto-generated method stub

		switch (type) {
		case PHONE_TYPE:
			return android.text.InputType.TYPE_CLASS_PHONE;
		case EMAIL_TYPE:
			return android.text.InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
		case NUMBER_TYPE:
			return android.text.InputType.TYPE_CLASS_NUMBER;
		case CARD_TYPE:
			return android.text.InputType.TYPE_CLASS_NUMBER;
		default:
			return android.text.InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
		}
	}
}
