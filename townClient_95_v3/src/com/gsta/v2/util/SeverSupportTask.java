package com.gsta.v2.util;

import java.util.List;

import mobile.http.ConnectivityHelper;
import mobile.http.HttpReqCode;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Window;
import android.widget.TextView;

import com.gsta.v2.activity.R;

/**
 * 
 * 异步刷新ui类
 * 
 * @author wangxin
 * 
 */
public class SeverSupportTask extends AsyncTask<Object, Object, Object> {

    private Context context;
    private IUICallBackInterface callBackInterface;
    // request url
    private String url;
    // Parameter
    private List<Parameter> paras;
    // pdShow isShow ?
    private boolean pdShow;
    // ProgressDialog show message
    private String pdMsg;
    // ProgressDialog
    private Dialog popupDialog;

    private int caseKey;
    protected SyncHttpClient http = new SyncHttpClient();

    @Override
    protected Object doInBackground(Object... params) {
        // TODO Auto-generated method stub

        if (!ConnectivityHelper.ConnectivityIsAvailable(context)) {
            return HttpReqCode.no_network;
        }
        try {
            paras.add(new Parameter("appId", ((AgentApplication) context.getApplicationContext()).getAppid()));
            return http.httpPost(url, paras);
        } catch (Exception e) {
            e.printStackTrace();
            return HttpReqCode.error;
        }
    }

    /***
     * 
     * SeverSupportTask
     * 
     * @param context
     * @param pdShow
     * @param paras
     * @param url
     * @param callBackInterface
     */
    public SeverSupportTask(Context context,
            IUICallBackInterface callBackInterface, String url,
            List<Parameter> paras, boolean pdShow, String pdMsg, int caseKey) {
        this.context = context;
        this.callBackInterface = callBackInterface;
        this.url = url;
        this.paras = paras;
        this.pdShow = pdShow;
        this.pdMsg = pdMsg;
        this.caseKey = caseKey;
    }

    @Override
    protected void onPostExecute(Object result) {
        if (pdShow) {
            popupDialog.dismiss();
        }
        if (callBackInterface != null) {
            callBackInterface.uiCallBack(result, this.caseKey);
        }
    }

    @Override
    protected void onPreExecute() {
        if (pdShow) {
            popupDialog = new Dialog(context,
                    android.R.style.Theme_Translucent_NoTitleBar);
            popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            popupDialog.setContentView(R.layout.loading_layout);
            TextView txtMsg = (TextView) popupDialog
                    .findViewById(R.id.left_textView);
            txtMsg.setText(pdMsg);
            popupDialog.show();
        }
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
    }
}
