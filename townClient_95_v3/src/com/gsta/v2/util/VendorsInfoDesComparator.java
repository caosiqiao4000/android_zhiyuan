package com.gsta.v2.util;

import java.util.Comparator;

import com.gsta.v2.entity.SellerInfo;

public class VendorsInfoDesComparator implements Comparator<SellerInfo> {

	@Override
	public int compare(SellerInfo obj1, SellerInfo obj2) {
		// TODO Auto-generated method stub
		if(obj1.getDistance() != null && obj2.getDistance() != null) {
			if (obj1.getDistance() > obj2.getDistance()) {
				return 1;
			}
			if (obj1.getDistance() < obj2.getDistance()) {
				return -1;
			}
		}
		return 0;
	}

}
