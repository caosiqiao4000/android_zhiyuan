package com.gsta.v2.util;

/**
 * 微博辅助
 * @author wubo
 * @createtime 2012-9-4
 */
public class WeiboUtil {

    // 腾讯应用网址
    public static final String TX_CALL_URI = "http://www.95town.cn";
    public static final String TX_CLIENTID = "801375863";
    public static final String TX_CLIENTSECRET = "24c8d2a8bdcc11185c7a6e92be4152cc";
    public static final String TX_TOKEN = "tx_token";
    public static final String TX_EXPIRES_IN = "tx_expires_in";
    public static final String TX_OPENID = "tx_openid";
    public static final String TX_OPENKEY = "tx_openkey";
    public static final String TX_OAUTH = "tx_oauth";
    public static final String TX_NICK = "tx_nick";

    // 新浪95town官方的。App Key：4222181665 .App Secret：bd9e1a0a37a3a9539c2d8951b04a6eb2.
    public static final String SINA_CONSUMER_KEY = "4222181665";// 1254929778
    public static final String SINA_CONSUMER_SECRET = "bd9e1a0a37a3a9539c2d8951b04a6eb2";// eabdce795373ada6704f40288ce2de85
    public static final String SINA_CALL_URI = "http://www.95town.cn";
    public static final String SINA_TOKEN = "sina_token";
    public static final String SINA_EXPIRES_IN = "sina_expires_in";
    public static final String EXTRA_WEIBO_CONTENT = "com.weibo.android.content";
    public static final String EXTRA_PIC_URI = "com.weibo.android.pic.uri";
    public static final String EXTRA_ACCESS_TOKEN = "com.weibo.android.accesstoken";
    public static final String EXTRA_TOKEN_SECRET = "com.weibo.android.token.secret";
    public static final String SINA_UID = "sina_uid";
    public static final String SINA_NICK = "sina_nick";
    public final static String WEIBOFLAG = "WEIBOFLAG";// 0sina 1tx

}
