package com.gsta.v2.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;
import com.gsta.v2.activity.LoggerCrashExceptionHandle;
import com.gsta.v2.entity.BaseAgentInfo;
import com.gsta.v2.entity.BaseUserinfo;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * 程序 的application环境,用来保存一些共用的数据
 * 
 */
public class AgentApplication extends Application {

    private static final Logger logger = LoggerFactory.getLogger(AgentApplication.class);
    private BaseUserinfo userinfo;// 用户信息
    private BaseAgentInfo agentInfo;// 代理商信息
    private List<Activity> activities = new ArrayList<Activity>();
    private String account;
    private String inviteAccount;// 邀请人账号
    private int loginState = FinalUtil.LONGIN_LONGINSTATE_OUT; // 0xA1 末登录 0xA2 登录成功 0xA3 登录失败

    // 图片显示的分辨率
    public float density = 1.0f;
    private int widthPixels;
    private int heightPixels;

    // 图片预览分辨率
    private int picPreviewResolution;
    // 图片浏览分辨率
    private int picBrowseResolution;

    private boolean notice_flag = false;
    // 聊天对象头像 key UID value 路径
    private Map<String, String> photoPathMap;
    private boolean loginOutIsNormal = false;// 是否正常退出 true 正常退出 false 异常退出

    @Override
    public void onCreate() {
        super.onCreate();
        LoggerCrashExceptionHandle customException = LoggerCrashExceptionHandle.getInstance();
        customException.initApplication(getApplicationContext());
        logger.info(" app ------->> onCreate");
        photoPathMap = new HashMap<String, String>();
        // 图片处理框架
        initImageLoader(getApplicationContext());
    }

    public static void initImageLoader(Context context) {
        int maxMemory = 10 * 1024 * 1024;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
            maxMemory = 5 * 1024 * 1024;
        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                // 内存缓存大小
                .memoryCacheSize(10 * 1024 * 1024)
                // 生成多种大小图片
                .denyCacheImageMultipleSizesInMemory()
                // 本地图片
                .discCacheSize(maxMemory)
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .enableLogging() // Not necessary in common
                .build();

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
        if (this.density == 1.0) {
            // >>>设置预览分辨率
            setPicPreviewResolution(AsyncImageLoader.PIC_TYPE_MDPI);
            // >>>设置浏览分辨率
            setPicBrowseResolution(AsyncImageLoader.PIC_TYPE_HD_MAX);
        } else if (this.density == 1.5) {
            // >>>设置预览分辨率
            setPicPreviewResolution(AsyncImageLoader.PIC_TYPE_MIPI);
            // >>>设置浏览分辨率
            setPicBrowseResolution(AsyncImageLoader.PIC_TYPE_HD_MIN);

        } else if (this.density == 2.0) {
            setPicPreviewResolution(AsyncImageLoader.PIC_TYPE_HIPI);
            // >>>设置浏览分辨率
            setPicBrowseResolution(AsyncImageLoader.PIC_TYPE_HD_MAX);
        } else {
            setPicPreviewResolution(AsyncImageLoader.PIC_TYPE_HDPI);
            // >>>设置浏览分辨率
            setPicBrowseResolution(AsyncImageLoader.PIC_TYPE_HD_MIN);
        }
    }

    public boolean isLoginOutIsNormal() {
        return loginOutIsNormal;
    }

    /**
     * 是否正常退出 true 正常退出 false 异常退出
     * 
     * @author caosq 2013-5-3 上午11:56:25
     * @param loginOutError
     */
    public void setLoginOutIsNormal(boolean loginOutIsNormal) {
        this.loginOutIsNormal = loginOutIsNormal;
    }

    public int getPicPreviewResolution() {
        return picPreviewResolution;
    }

    public void setPicPreviewResolution(int picPreviewResolution) {
        this.picPreviewResolution = picPreviewResolution;
    }

    public int getPicBrowseResolution() {
        return picBrowseResolution;
    }

    public void setPicBrowseResolution(int picBrowseResolution) {
        this.picBrowseResolution = picBrowseResolution;
    }

    public int getWidthPixels() {
        return widthPixels;
    }

    public void setWidthPixels(int widthPixels) {
        this.widthPixels = widthPixels;
    }

    public int getHeightPixels() {
        return heightPixels;
    }

    public void setHeightPixels(int heightPixels) {
        this.heightPixels = heightPixels;
    }

    public int getLoginState() {
        return loginState;
    }

    public void setLoginState(int loginState) {
        this.loginState = loginState;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @author caosq 2013-4-24 下午3:19:10
     * @param keyUid
     *            用户UDI
     * @return 指定UID 头像路径
     */
    public String getPhotoPathByUid(String keyUid) {
        return photoPathMap.get(keyUid);
    }

    public Map<String, String> getPhotoPathMap() {
        return photoPathMap;
    }

    /**
     * 保存聊天对象的 UID和 头像路径
     * 
     * @author caosq 2013-4-24 下午3:17:54
     * @param keyUid
     *            用户UID
     * @param photoPath
     */
    public void setPhotoPathByUid(String keyUid, String photoPath) {
        if (!photoPathMap.containsKey(keyUid)) {
            if (null != photoPath && !TextUtils.isEmpty(photoPath.trim())) {
                this.photoPathMap.put(keyUid, photoPath);
            }
        }
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    /**
     * 个人 uid 与 自己作为代理商 UID是一样的
     * 
     * @author wubo
     * @createtime 2012-9-11
     * @return
     */
    public String getUid() {
        return String.valueOf(getUserinfo().getUid());
    }

    /**
     * 10000 营销客户端系统 10001 用户客户端系统 10002 代理商门户 10003 商城门户 …
     * 
     * @return
     */
    public String getAppid() {
        return "10000";
    }

    /**
     * 代理商身份
     * 
     * @author wubo
     * @createtime 2012-9-11
     * @return 0审核态,1正常态
     */
    public int getAgentStatu() {
        if (getLoginState() == FinalUtil.LONGIN_LONGINSTATE_SUCCEED) {
            return getUserinfo().getIsAgent();
        } else {
            return FinalUtil.AgentStatu_Destory;
        }
    }

    public void setAgentStatu(Integer isAgent) {
        getUserinfo().setIsAgent(isAgent);
    }

    public String getUserName() {
        if (null != getUserinfo().getIsAgent() && getUserinfo().getIsAgent() == 1) {
            if (agentInfo != null) {
                return agentInfo.getAgentName();
            }
        }
        return getUserinfo().getUserName() == null ? "" : getUserinfo()
                .getUserName();
    }

    public BaseUserinfo getUserinfo() {
        if (userinfo == null) {
            // ((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
            // .cancel(0);
            // Intent intent = new Intent(AgentApplication.this,
            // LoginActivity.class);
            // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // startActivity(intent);
            // System.exit(0);
            return new BaseUserinfo();
        } else {
            return userinfo;
        }
    }

    public void setUserinfo(BaseUserinfo userinfo) {
        this.userinfo = userinfo;
    }

    public BaseAgentInfo getAgentInfo() {
        if (null == agentInfo) {
            return new BaseAgentInfo();
        }
        return agentInfo;
    }

    public void setAgentInfo(BaseAgentInfo agentInfo) {
        this.agentInfo = agentInfo;
    }

    public boolean isNotice_flag() {
        return notice_flag;
    }

    public void setNotice_flag(boolean noticeFlag) {
        notice_flag = noticeFlag;
    }

    public String getInviteAccount() {
        return inviteAccount;
    }

    public void setInviteAccount(String inviteAccount) {
        this.inviteAccount = inviteAccount;
    }

    /**
     * 异常退出时使用
     * 
     * @author caosq 2013-4-15 下午5:33:39
     */
    // public void errorleaveTown() {
    // logger.error("AgentApplication errorleaveTown() 异常退出时使用");
    // // 未加上定位相关的退出
    // leaveProgram();
    // }
}
