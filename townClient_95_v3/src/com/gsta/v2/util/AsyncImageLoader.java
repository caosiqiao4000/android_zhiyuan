package com.gsta.v2.util;

import android.graphics.drawable.Drawable;

/**
 * 网络异步加载图片以及本地异步加载图片（sd卡地址）
 * 
 * @author Administrator
 * 
 */
public class AsyncImageLoader {
    // dm.density,这个值是因硬件分辨率而异的，如果是屏幕硬件宽为320像素，那么这个值就是1,如果是480,这个值就是480/320,就是1.5
    public static final int PIC_TYPE_MAX = 10000;// 最大 （原图）
    public static final int PIC_TYPE_HD_MAX = 800; // 超大
    public static final int PIC_TYPE_HD_MIN = 640; // 超大
    public static final int PIC_TYPE_HDPI = 480; // 中大
    public static final int PIC_TYPE_HIPI = 360; // 大
    public static final int PIC_TYPE_MIPI = 300; // 中
    public static final int PIC_TYPE_MDPI = 240; // 小
    public static final int PIC_TYPE_LDPI = 120; // 微小
    public static final int PIC_TYPE_MIN = 60; // 最小

    // 手机缓存图片最大容量 这里是数量
    public static final int HARD_CACHE_CAPACITY = 70;

    // 先定义A缓 固定缓存
    // private final HashMap<String, Drawable> mHardBitmapCache = new LinkedHashMap<String, Drawable>(HARD_CACHE_CAPACITY / 2, 0.75f, true) {
    // /**
    // * 2013-6-19
    // */
    // private static final long serialVersionUID = 1L;
    //
    // @Override
    // protected boolean removeEldestEntry(LinkedHashMap.Entry<String, Drawable> eldest) {
    // if (size() > HARD_CACHE_CAPACITY) {
    // // 当map的size大于30时，把最近不常用的key放到mSoftBitmapCache中，从而保证mHardBitmapCache的效率
    // mSoftBitmapCache.put(eldest.getKey(), new SoftReference<Drawable>(eldest.getValue()));
    // return true;
    // } else
    // return false;
    // }
    // };

    // 缓存B 临时缓存
    // private final static ConcurrentHashMap<String, SoftReference<Drawable>> mSoftBitmapCache = new ConcurrentHashMap<String,
    // SoftReference<Drawable>>(
    // HARD_CACHE_CAPACITY / 2);

    private static AsyncImageLoader asyncImageLoader;

    // class LoadTask extends AsyncTask<String, Void, Drawable> {
    //
    // private String httpurl;
    // private ImageCallback callback;
    // private String filePath;
    // private int maxLoadPix; // 下载的最大像素
    //
    // public LoadTask(ImageCallback callback, int maxLoadPix) {
    // super();
    // this.maxLoadPix = maxLoadPix;
    // this.callback = callback;
    // }
    //
    // @Override
    // protected Drawable doInBackground(String... params) {
    // this.httpurl = params[0];
    // this.filePath = params[1];
    // return loadImageFromUrl(httpurl, maxLoadPix);
    // }
    //
    // @Override
    // protected void onPostExecute(Drawable result) {
    // if (result != null) {// 网络加载成功,保存到手机缓存
    // if (filePath != null) {
    // Bitmap writeBit = ImageUtil.drawableToBitmap(result);
    // if (writeBit != null) {
    // mHardBitmapCache.put(httpurl, result);
    // ImageUtil.saveUserIcon(writeBit, filePath);
    // writeBit.recycle();
    // }
    // }
    // }
    // callback.imageLoaded(result, httpurl);
    // }
    // }

    // public void loadDrawable(String filePath, String imageUrl,
    // ImageCallback callback, Context context) {
    //
    // if (mHardBitmapCache.containsKey(imageUrl)
    // && mHardBitmapCache.get(imageUrl) != null
    // && mHardBitmapCache.get(imageUrl) != null) {// 手机图片缓存
    // callback.imageLoaded(getBitmapFromCache(imageUrl), imageUrl);
    // } else {
    // if (filePath != null) {// 从手机SD卡程序目录取图片
    // Drawable oldpic = ImageUtil.getDrawableFromLocal(filePath);
    // if (oldpic != null) {
    // mHardBitmapCache.put(imageUrl, oldpic);
    // callback.imageLoaded(mHardBitmapCache.get(imageUrl),
    // imageUrl);
    // } else {
    // AgentApplication application = (AgentApplication) context.getApplicationContext();
    // new LoadTask(callback, application.getHeightPixels() * application.getWidthPixels()).execute(imageUrl, filePath);
    // }
    // } else {
    // AgentApplication application = (AgentApplication) context.getApplicationContext();
    // new LoadTask(callback, application.getHeightPixels() * application.getWidthPixels()).execute(imageUrl, filePath);
    // }
    // }
    // }

    /**
     * 从缓存中获取图片
     */
    // private Drawable getBitmapFromCache(String url) {
    // // 先从mHardBitmapCache缓存中获取
    // synchronized (mHardBitmapCache) {
    // final Drawable bitmap = mHardBitmapCache.get(url);
    // if (bitmap != null) {
    // // 如果找到的话，把元素移到linkedhashmap的最前面，从而保证在LRU算法中是最后被删除
    // mHardBitmapCache.remove(url);
    // mHardBitmapCache.put(url, bitmap);
    // return bitmap;
    // }
    // }
    // // 如果mHardBitmapCache中找不到，到mSoftBitmapCache中找
    // SoftReference<Drawable> bitmapReference = mSoftBitmapCache.get(url);
    // if (bitmapReference != null) {
    // final Drawable bitmap = bitmapReference.get();
    // if (bitmap != null) {
    // return bitmap;
    // } else {
    // mSoftBitmapCache.remove(url);
    // }
    // }
    // return null;
    // }

    /**
     * 始终从网络中下载图片
     * 
     * @author caosq 2013-6-19 下午3:18:40
     * @param filePath
     * @param imageUrl
     * @param callback
     * @param context
     */
//    public void loadDrawableByNet(String filePath, String imageUrl,
//            ImageCallback callback, Context context) {
//        if (filePath != null) {// 从手机SD卡目录取图片
//            AgentApplication application = (AgentApplication) context.getApplicationContext();
////            new LoadTask(callback, application.getHeightPixels() * application.getWidthPixels()).execute(imageUrl, filePath);
//        }
//    }

    // 执行网络加载
    // protected Drawable loadImageFromUrl(String imageUrl, int maxLoadPix) {
    // InputStream is = null;
    // Options options = new Options();
    // try {
    // is = new URL(imageUrl).openStream();
    // options.inJustDecodeBounds = true;
    // try {
    // BitmapFactory.decodeStream(is, null, options);
    // } catch (Exception e) {
    // e.printStackTrace();
    // is.close();
    // return null;
    // }
    // // 图片模糊
    // // options.inSampleSize = ImageUtil.computeSampleSize(options, -1, 128 * 128);
    // options.inSampleSize = ImageUtil.computeSampleSize(options, -1, maxLoadPix);
    // logger.info("loadImageFromUrl options.inSampleSize === " + options.inSampleSize);
    // options.inJustDecodeBounds = false;
    // is = new URL(imageUrl).openStream();
    // Bitmap temp = BitmapFactory.decodeStream(is, null, options);
    // // 这个方法会导致 OOM
    // // return Drawable.createFromStream(is, "src");
    // return ImageUtil.bitmapToDrawable(temp);
    // } catch (OutOfMemoryError error) {
    // error.printStackTrace();
    // return null;
    // } catch (Exception e) {
    // return null;
    // } finally {
    // try {
    // if (is != null) {
    // is.close();
    // }
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // }
    // }

    public interface ImageCallback {
        public void imageLoaded(Drawable imageDrawable, String imageUrl);
    }

    // public Map<String, Drawable> getImageCache() {
    // return mHardBitmapCache;
    // }

    public static AsyncImageLoader getAsyInstance() {
        if (asyncImageLoader == null) {
            asyncImageLoader = new AsyncImageLoader();
        }
        return asyncImageLoader;
    }

    // private void loadImage(String imageUrl, int imgWidth, boolean flag,
    // OnImageLoadListener listener) {
    // if (mHardBitmapCache.containsKey(imageUrl)
    // && mHardBitmapCache.get(imageUrl) != null) {
    // listener.onImageLoad(mHardBitmapCache.get(imageUrl));
    // } else {
    // new LoadSdTask(listener, imgWidth, flag).execute(imageUrl);
    // }
    // }

    // class LoadSdTask extends AsyncTask<String, Void, Bitmap> {
    //
    // private OnImageLoadListener listener;
    // private boolean flag;
    // private int width;
    // private String httpurl;
    //
    // public LoadSdTask(OnImageLoadListener listener, int width, boolean flag) {
    // super();
    // this.listener = listener;
    // this.flag = flag;
    // this.width = width;
    // }
    //
    // @Override
    // protected Bitmap doInBackground(String... params) {
    // this.httpurl = params[0];
    // return loadImageFromSd(httpurl, width, flag);
    // }
    //
    // @Override
    // protected void onPostExecute(Bitmap result) {
    // if (result != null) {
    // Drawable drawable = new BitmapDrawable(result);
    // mHardBitmapCache.put(httpurl, drawable);
    // listener.onImageLoad(drawable);
    // } else {
    // listener.onImageLoad(null);
    // }
    // }
    // }

    // public void loadSDImage(final String imageUrl, final int imgWidth,
    // final OnImageLoadListener listener) {
    // loadImage(imageUrl, imgWidth, false, listener);
    // }

    // public interface OnImageLoadListener {
    // // public void onImageLoad(Bitmap drawable);
    // public void onImageLoad(Drawable drawable);
    // }
}
