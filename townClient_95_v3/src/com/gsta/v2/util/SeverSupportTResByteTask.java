package com.gsta.v2.util;

import java.util.List;

import mobile.http.ConnectivityHelper;
import mobile.http.HttpReqCode;
import mobile.http.MyFilePart;
import mobile.http.Parameter;
import mobile.http.SyncHttpClient;
import android.content.Context;
import android.os.AsyncTask;

public class SeverSupportTResByteTask extends AsyncTask<Object, Integer, Object>{
	private String TAG = Util.getClassName();// log tag

	private static final com.google.code.microlog4android.Logger logger = com.google.code.microlog4android.LoggerFactory.getLogger();
	private IUICallBackInterface callBackInterface;
	private String url;
	private List<Parameter> paras;
	private byte[] bytes;
	private int caseKey;
	private MyFilePart uploader;
	private Context context;
	protected SyncHttpClient http = new SyncHttpClient();
	
	/***
	 * 
	 * SeverSupportTask public String httpPostWithFile(String url,
	 * List<Parameter> params,String fileParamName, File file, byte[] cover)
	 * throws Exception
	 * 
	 * @param context
	 * @param pdShow
	 * @param paras
	 * @param url
	 * @param callBackInterface
	 */
	public SeverSupportTResByteTask(Context context, IUICallBackInterface callBackInterface, String url,
			List<Parameter> paras, MyFilePart uploader, byte[] cover, int caseKey) {
		this.context = context;
		this.callBackInterface = callBackInterface;
		this.url = url;
		this.paras = paras;
		this.uploader = uploader;
		this.bytes = cover;
		this.caseKey = caseKey;

	}

	@Override
	protected Object doInBackground(Object... params) {
		
		if (!ConnectivityHelper.ConnectivityIsAvailable(context)) {
			return HttpReqCode.no_network;
		}
		try {
			String resString = http.httpPostWithFile(url, paras, uploader, bytes);
			if(resString == null){
				return HttpReqCode.error;
			}
			return resString;
		}  catch (Exception e) {
			logger.error(TAG, e);
			return HttpReqCode.error;
		}
	}
	
	@Override
	protected void onPostExecute(Object result) {
		if (callBackInterface != null) {
			callBackInterface.uiCallBack(result, this.caseKey);
		}
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(progress);
	}
	
}
