package com.gsta.v2.util;

import java.util.Comparator;

import com.gsta.v2.entity.ShopInfo;

public class ShopInfoDesComparator implements Comparator<ShopInfo> {

	@Override
	public int compare(ShopInfo obj1, ShopInfo obj2) {
		// TODO Auto-generated method stub
		if(obj1.getDistance() != null && obj2.getDistance() != null) {
			if (obj1.getDistance() > obj2.getDistance()) {
				return 1;
			}
			if (obj1.getDistance() < obj2.getDistance()) {
				return -1;
			}
		}
		return 0;
	}

}
