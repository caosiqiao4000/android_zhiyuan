package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 我的用户信息
 * @author: chenjd
 * @createTime: 2012-9-8 下午3:24:38 
 * @version: 1.0
 * @desc:
 */
public class MyUserInfo implements Serializable{
	
	private String id;// 主键id
	private String uid;// 合约机用户必填
	private String account;// 产品账号，如手机卡，宽带账号
	private String type;// 业务类型，光宽带、合约机
	private String productName;// 商品名称
	private String userName;// 用户姓名
	private String mobile;// 联系电话
	private String orderTime;// 订购日期
	private String remark;// 备注名称
	private String memo;// 备注信息
	private String photo;// 头像地址
	private String nickName;// 用户昵称
	
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public MyUserInfo() {
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}

}
