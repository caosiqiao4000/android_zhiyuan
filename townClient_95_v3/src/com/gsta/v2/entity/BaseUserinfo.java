package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * @author: chenjd
 * @createTime: 2012-8-30 下午5:13:49
 * @version: 1.0
 * @desc: 用户基本信息
 */
public class BaseUserinfo implements Serializable {

    private String uid; // 用户ID，唯一标识，手机号码
    private String userName; // 用户名
    private String nickName; // 用户昵称
    private String phone; // 联系电话
    private String identification; // 证件号码
    private String sex; // 性别
    private Integer age; // 年龄
    private String vip; // 会员等级
    private String agentCity; // 当前代理的城市
    private String address; // 联系地址
    private String email; // 邮箱
    private String zipCode; // 邮政编码
    private Integer isAgent; // 是否代理商 0:普通 1:代理商
    private String photo;// 用户头像地址
    private String mobile;// 手机号码
    private Integer grade;// 信用级别
    private String firstName;// 姓名
    private String deliveryAddress;// 收货地址

    public BaseUserinfo() {
    }

    public BaseUserinfo(String uid, String userName, String nickName,
            String phone, String identification, String sex, Integer age,
            String vip, String agentCity, String address, String email,
            String zipCode, Integer isAgent, String photo) {
        this.uid = uid;
        this.userName = userName;
        this.nickName = nickName;
        this.phone = phone;
        this.identification = identification;
        this.sex = sex;
        this.age = age;
        this.vip = vip;
        this.agentCity = agentCity;
        this.address = address;
        this.email = email;
        this.zipCode = zipCode;
        this.isAgent = isAgent;
        this.photo = photo;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getAgentCity() {
        return agentCity;
    }

    public void setAgentCity(String agentCity) {
        this.agentCity = agentCity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Integer getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Integer isAgent) {
        this.isAgent = isAgent;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }
}
