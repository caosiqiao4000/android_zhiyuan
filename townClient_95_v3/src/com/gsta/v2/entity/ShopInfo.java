package com.gsta.v2.entity;

/**
 * @author fitch
 * @createtime 2013-3-23 下午9:56:48
 * @version 1.0
 * @desc
 */
public class ShopInfo implements java.io.Serializable {

    private static final long serialVersionUID = 3907602826524693453L;
    
    private String userKo;// 用户标识
    private String agentUid;// 关系用户UID，被收藏的店铺标识UID
    private String photo;// 头像
    private String levelCode;// 等级编码
    private String name;// 店铺名称
    private String introduce;// 店铺介绍
    private Location location;//位置信息
    private Long upTime;// 位置上报时间 
    private Long distance;//距离

    
    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public String getUserKo() {
        return userKo;
    }
    
    public void setUserKo(String userKo) {
        this.userKo = userKo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getLevelCode() {
        return levelCode;
    }

    public void setLevelCode(String levelCode) {
        this.levelCode = levelCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Long getUpTime() {
        return upTime;
    }

    public void setUpTime(Long upTime) {
        this.upTime = upTime;
    }

    public String getAgentUid() {
        return agentUid;
    }

    public void setAgentUid(String agentUid) {
        this.agentUid = agentUid;
    }
    
}
