package com.gsta.v2.entity;

/**
 * com.gsta.v2.entity.WareSalesObject
 * @author wubo
 * @date 2013-1-23
 * 
 */

import java.math.BigDecimal;

/**
 * 商品销售排行
 * 
 * @author zzq
 */
public class WareSalesObject implements java.io.Serializable {

	private static final long serialVersionUID = -4613165514371160031L;

	/**
	 * 类型ID
	 */
	private String specificationId;
	/**
	 * 商品信息ID
	 */
	private String speciesId;
	/**
	 * 销售量
	 */
	private int salesVolume;

	/**
	 * 最大价格
	 */
	private double maxPrice;
	/**
	 * 最小价格
	 */
	private double minPrice;

	/**
	 * 馆类型
	 */
	private String type;
	/**
	 * 商品名称
	 */
	private String prodName;
	/**
	 * 媒体路径
	 */
	private String mediaPath;

	/**
	 * 最小原价
	 */
	private double minRetailPrice;

	/**
	 * 最高原价
	 */
	private double maxRetailPrice;

	/**
	 * 描述
	 */
	private String speciDesc;

	/**
	 * 供应商名称
	 */
	private String merchantName;

	private double discountRate; // 折扣比率

	/**
	 * 商品标识 0-合约机 1-光宽带 2-手机卡 3-大礼包 4-其他
	 */
	public int wareFlag;

	public double getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}

	public String getSpecificationId() {
		return specificationId;
	}

	public void setSpecificationId(String specificationId) {
		this.specificationId = specificationId;
	}

	public String getSpeciesId() {
		return speciesId;
	}

	public void setSpeciesId(String speciesId) {
		this.speciesId = speciesId;
	}

	public int getSalesVolume() {
		return salesVolume;
	}

	public void setSalesVolume(BigDecimal salesVolume) {
		this.salesVolume = salesVolume.intValue();
	}

	public double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(double maxPrice) {
		this.maxPrice = maxPrice;
	}

	public double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getMediaPath() {
		return mediaPath;
	}

	public void setMediaPath(String mediaPath) {
		this.mediaPath = mediaPath;
	}

	public double getMinRetailPrice() {
		return minRetailPrice;
	}

	public void setMinRetailPrice(double minRetailPrice) {
		this.minRetailPrice = minRetailPrice;
	}

	public double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public String getSpeciDesc() {
		return speciDesc;
	}

	public void setSpeciDesc(String speciDesc) {
		this.speciDesc = speciDesc;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public int getWareFlag() {
		return wareFlag;
	}

	public void setWareFlag(int wareFlag) {
		this.wareFlag = wareFlag;
	}

}
