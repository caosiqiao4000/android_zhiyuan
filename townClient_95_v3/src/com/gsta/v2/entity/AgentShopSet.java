package com.gsta.v2.entity;

/**
 * 代理商店铺设置
 * 
 * @author zengxj
 * @createTime: 2013-3-18 下午3:24:41
 * @version: 1.0
 * @desc:
 */
public class AgentShopSet {

    private String photo;// 店铺图标

    private String name;// 店铺名称

    private String domain;// 域名

    private String phone;// 固定电话

    private String mobile;// 手机号码

    private String email;// 邮箱

    private String introduce;// 本店公告

    private String zipCode;// 邮政编码

    private String provinceId;// 代理省份ID

    private String cityId;// 代理城市ID

    private String contactAddress;// 联系地址
    
    private String levelCode; //店铺等级

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getLevelCode() {
        return levelCode;
    }

    public void setLevelCode(String levelCode) {
        this.levelCode = levelCode;
    }
}
