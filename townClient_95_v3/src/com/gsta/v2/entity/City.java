package com.gsta.v2.entity;

import java.io.Serializable;

/**
 *@author wubo
 *@createtime 2012-9-1
 */
public class City implements Serializable{

	private int cid;
	private String id;//城市代码
	private String parent;//父级代码
	private String name;//城市名称
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public City(String id, String parent, String name) {
		super();
		this.id = id;
		this.parent = parent;
		this.name = name;
	}
	public City() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
