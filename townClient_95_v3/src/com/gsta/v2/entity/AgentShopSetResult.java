package com.gsta.v2.entity;

import com.gsta.v2.response.BaseResult;

/**
 * 代理商店铺设置结果类
 * @author zengxj
 * @createTime: 2013-3-18 下午3:24:41
 * @version: 1.0
 * @desc: 
 */
public class AgentShopSetResult extends BaseResult {
	
	private static final long serialVersionUID = 3112388293610896817L;
	
	private AgentShopSet agentShopSet;

	public AgentShopSet getAgentShopSet() {
		return agentShopSet;
	}

	public void setAgentShopSet(AgentShopSet agentShopSet) {
		this.agentShopSet = agentShopSet;
	}

}
