package com.gsta.v2.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *@author wubo
 *@createtime 2012-9-18
 */
public class GoodsInfo implements Serializable {
	private String specificationId;// 类型ID
	private String speciesId;// 商品id
	private String sku;// 货号
	private String description;// 商品描述
	private String mediaPath;// 图片地址
	private double minSalePrice;// 最小销售价
	private double maxSalePrice;// 最大销售价
	private double minRetailPrice;// 最小市场价
	private double maxRetailPrice;// 最大市场价
	private double salePrice;// 销售价
	private double retailPrice;// 市场价
	private String prodName;// 商品名称
	private String prodBrand;// 商品品牌
	
	private String agentGoodId;     
	private Integer popularity;     //收藏人气
	private String userId;          //收藏者ID
	private String packageId;		//套餐ID
	private Timestamp dealTime;		//收藏时间
	

	public String getAgentGoodId() {
		return agentGoodId;
	}

	public void setAgentGoodId(String agentGoodId) {
		this.agentGoodId = agentGoodId;
	}

	public Integer getPopularity() {
		return popularity;
	}

	public void setPopularity(Integer popularity) {
		this.popularity = popularity;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public Timestamp getDealTime() {
		return dealTime;
	}

	public void setDealTime(Timestamp dealTime) {
		this.dealTime = dealTime;
	}

	public String getSpecificationId() {
		return specificationId;
	}

	public void setSpecificationId(String specificationId) {
		this.specificationId = specificationId;
	}

	public String getSpeciesId() {
		return speciesId;
	}

	public void setSpeciesId(String speciesId) {
		this.speciesId = speciesId;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMediaPath() {
		return mediaPath;
	}

	public void setMediaPath(String mediaPath) {
		this.mediaPath = mediaPath;
	}

	public double getMinSalePrice() {
		return minSalePrice;
	}

	public void setMinSalePrice(double minSalePrice) {
		this.minSalePrice = minSalePrice;
	}

	public double getMaxSalePrice() {
		return maxSalePrice;
	}

	public void setMaxSalePrice(double maxSalePrice) {
		this.maxSalePrice = maxSalePrice;
	}

	public double getMinRetailPrice() {
		return minRetailPrice;
	}

	public void setMinRetailPrice(double minRetailPrice) {
		this.minRetailPrice = minRetailPrice;
	}

	public double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdBrand() {
		return prodBrand;
	}

	public void setProdBrand(String prodBrand) {
		this.prodBrand = prodBrand;
	}

	public double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(double salePrice) {
		this.salePrice = salePrice;
	}

	public double getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(double retailPrice) {
		this.retailPrice = retailPrice;
	}

}
