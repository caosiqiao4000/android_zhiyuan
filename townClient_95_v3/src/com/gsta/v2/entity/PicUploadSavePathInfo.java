package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 上传图片后需要保存的本地路径信息
 * 
 * @author caosq 2013-6-18
 */
@SuppressWarnings("serial")
public class PicUploadSavePathInfo implements Serializable {

    private String server_Db_Id;// 服务器对应的主键ID
    private String picNavitePath; // 更改的图片在手机中的地址
    private String picChangeTime; // 更改的时间

    public String getServer_Db_Id() {
        return server_Db_Id;
    }

    public void setServer_Db_Id(String server_Db_Id) {
        this.server_Db_Id = server_Db_Id;
    }

    public String getPicNavitePath() {
        return picNavitePath;
    }

    public void setPicNavitePath(String picNavitePath) {
        this.picNavitePath = picNavitePath;
    }

    public String getPicChangeTime() {
        return picChangeTime;
    }

    public void setPicChangeTime(String picChangeTime) {
        this.picChangeTime = picChangeTime;
    }

}
