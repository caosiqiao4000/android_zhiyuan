package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 买家留言信息
 * 
 * @author: chenjd
 * @createTime: 2012-9-8 下午3:26:47
 * @version: 1.0
 * @desc:
 */
public class MessageInfo implements Serializable {

	private String id;// 留言主键id
	private String sentUid;// 发表留言用户uid
	private String nickName;// 用户昵称
	private Integer ifHeadIcon;// 用户是否上传头像
	private String content;// 留言信息
	private String sentTime;// 留言发表时间
	private String dir;// 谁发的信息(0表示登陆者,1表示我的买家)

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public MessageInfo() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSentUid() {
		return sentUid;
	}

	public void setSentUid(String sentUid) {
		this.sentUid = sentUid;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getIfHeadIcon() {
		return ifHeadIcon;
	}

	public void setIfHeadIcon(Integer ifHeadIcon) {
		this.ifHeadIcon = ifHeadIcon;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSentTime() {
		return sentTime;
	}

	public void setSentTime(String sentTime) {
		this.sentTime = sentTime;
	}
}
