package com.gsta.v2.entity;

import java.io.Serializable;


/**
 * 商品展示信息
 * @author: chenjd
 * @createTime: 2012-11-27 下午4:48:10 
 * @version: 1.0
 * @desc:
 */
public class ProductDetailInfo implements Serializable{

	private String productName;// 商品名称
	private String brokerage;// 佣金（单位元）
	private String deduct;// 分成
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getBrokerage() {
		return brokerage;
	}
	public void setBrokerage(String brokerage) {
		this.brokerage = brokerage;
	}
	public String getDeduct() {
		return deduct;
	}
	public void setDeduct(String deduct) {
		this.deduct = deduct;
	}
	
}
