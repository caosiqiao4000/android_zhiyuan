package com.gsta.v2.entity;

import java.io.Serializable;

public class DirectDeductDetail implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4763354980506226534L;
	private String orderTime;// 订单时间
	private String orderId;// 订单号
	private String productName;// 商品名称
	private String mobile;// 号码
	private Integer orderCount;// 销售数量
	private Double amount;// 销售金额（元）
	private Double brokerageDirect;// 直接佣金（元）
	private Double deducts;// 预期直接分成（元）
	private String ifStat;// 结算标识
	private Integer deductType; //分成方式
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public  Integer getOrderCount() {
		return orderCount;
	}
	public void setOrderCount( Integer orderCount) {
		this.orderCount = orderCount;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getBrokerageDirect() {
		return brokerageDirect;
	}
	public void setBrokerageDirect(Double brokerageDirect) {
		this.brokerageDirect = brokerageDirect;
	}
	public Double getDeducts() {
		return deducts;
	}
	public void setDeducts(Double deducts) {
		this.deducts = deducts;
	}
	public String getIfStat() {
		return ifStat;
	}
	public void setIfStat(String ifStat) {
		this.ifStat = ifStat;
	}
	
	public Integer getDeductType() {
		return deductType;
	}
	public void setDeductType(Integer deductType) {
		this.deductType = deductType;
	}

}
