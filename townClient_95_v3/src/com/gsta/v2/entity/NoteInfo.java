package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 营销笔记与备忘录关联 com.gsta.v2.entity.NoteInfo
 * 
 * @author wubo
 * @date 2013-3-22
 * 
 */
public class NoteInfo implements Serializable {

	private String uid;// 用户uid
	private String noteId;// 营销笔记编号
	private String filePath;// 文件以地址以";"隔开

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getNoteId() {
		return noteId;
	}

	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public NoteInfo(String uid, String noteId, String filePath) {
		super();
		this.uid = uid;
		this.noteId = noteId;
		this.filePath = filePath;
	}

	public NoteInfo() {
		super();
	}

}
