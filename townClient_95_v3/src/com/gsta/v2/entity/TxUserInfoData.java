package com.gsta.v2.entity;

import java.io.Serializable;

/**
 *@author wubo
 *@createtime 2012-9-21
 */
public class TxUserInfoData implements Serializable {

	private String nick;

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;

	}
}
