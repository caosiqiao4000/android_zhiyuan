package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * @author wubo
 * @createtime 2012-9-18
 */
public class AgentGoodsInfo extends GoodsInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private int flag; // 状态标志 1:上架 2 下架
	private int ifShow; // 是否首页列表显示 1:显示 2：不显示

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public int getIfShow() {
		return ifShow;
	}

	public void setIfShow(int ifShow) {
		this.ifShow = ifShow;
	}

}
