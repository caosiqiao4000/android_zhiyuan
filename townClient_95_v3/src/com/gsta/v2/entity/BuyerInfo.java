package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 我的买家信息
 * @author: chenjd
 * @createTime: 2012-9-8 下午3:23:54 
 * @version: 1.0
 * @desc:
 */
public class BuyerInfo implements Serializable{
	private String id;// 主键id
	private String uid;// 买家UID
	private String account;// 买家账号
	private String nickName;// 买家昵称
	private String phone;// 联系电话
	private String remark;// 备注名称
	private String memo;// 备注信息
	private Integer orders;// 购买订单数
	private Integer msgCount;// 未读留言数
	private String photo;// 头像地址
	
	public BuyerInfo() {
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Integer getOrders() {
		return orders;
	}
	public void setOrders(Integer orders) {
		this.orders = orders;
	}
	public Integer getMsgCount() {
		return msgCount;
	}
	public void setMsgCount(Integer msgCount) {
		this.msgCount = msgCount;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
}
