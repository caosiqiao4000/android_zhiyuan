package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 
 * @author caosq
 *
 */
public class Contacts implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NO_LETTER = "NO_LETTER";
	public static final String LOCALCONTACTS_KEY = "Contacts";
	private int id;
	private Long contactId;
	private String phoneNum;
	private String userName;
	private String sortkey;
	private String firstLetter;
	
	public Contacts() {
		super();
	}
	
	public Contacts(Long contactId, String phoneNum, String userName,String sortkey,String firstLetter) {
		super();
		this.contactId = contactId;
		this.phoneNum = phoneNum;
		this.userName = userName;
		this.sortkey = sortkey;
		this.firstLetter = firstLetter;
	}

	public Long getContactId() {
		return contactId;
	}
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSortkey() {
		return sortkey;
	}

	public void setSortkey(String sortkey) {
		this.sortkey = sortkey;
	}

	public String getFirstLetter() {
		return firstLetter;
	}

	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	
	
}
