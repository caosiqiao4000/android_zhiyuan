package com.gsta.v2.entity;

import java.io.Serializable;

public class User implements Serializable{
	
	private String mobile;      //用户手机号码
	private String imsi;        //用户手机SIM卡IMSI号
	private String mac;         //用户手机MAC号
	private double longitude;   //用户所在地点经度
	private double latitude;    //用户所在地点纬度
	private int type;           //用户类型（1：原手机+原号码  2：非原手机+原号码  3：非原号码）
	
	public String getMobile() {
		return mobile;
	}
	public String getIMSI() {
		return imsi;
	}
	public String getMAC() {
		return mac;
	}
	public double getLongitude()
	{
		return longitude;
	}
	public double getLatitude()
	{
		return latitude;
	}
	public int getType()
	{
		return type;
	}
	public void setMobie(String Mobile)
	{
		mobile=Mobile;
	}
	public void setIMSI(String IMSI)
	{
		imsi=IMSI;
	}
	public void setMAC(String MAC)
	{
		mac=MAC;
	}
	public void setLongitude(double Longitude)
	{
		longitude=Longitude;
	}
	public void setLatitude(double Latitude)
	{
		latitude=Latitude;
	}
	public void setType(int Type)
	{
		type=Type;
	}
	
}
