package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 
 * @author longxianwen
 * @createtime 2013-3-26 下午4:52:48
 * @version 1.0
 * @desc
 */
public class Location implements Serializable {

	private double longitude;// 经度
	private double latitude;// 纬度
	
	public Location(){
		
	}

	public Location(double longitude, double latitude) {
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

}