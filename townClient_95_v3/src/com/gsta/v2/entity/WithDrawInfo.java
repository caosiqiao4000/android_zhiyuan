package com.gsta.v2.entity;

/**
 * 
 * @author longxianwen
 * @createTime Apr 10, 2013 2:56:14 PM
 * @version: 1.0
 * @desc:收支明细/提现明细
 */
public class WithDrawInfo {
	
	private String type;                  //交易类别
	private String time;                  //处理时间
	private Double incomeSum;             //收入金额
	private Double paySum;                //支付金额
	private Double balance;               //余额
	private String transactionType;       //提取类别
	private String paymentAccount;        //提取账号
	private String transactionDate;       //提取时间
	private String transactionStatus;      //状态
	private Double transactionAmount;      //提取金额
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Double getIncomeSum() {
		return incomeSum;
	}
	public void setIncomeSum(Double incomeSum) {
		this.incomeSum = incomeSum;
	}
	public Double getPaySum() {
		return paySum;
	}
	public void setPaySum(Double paySum) {
		this.paySum = paySum;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getPaymentAccount() {
		return paymentAccount;
	}
	public void setPaymentAccount(String paymentAccount) {
		this.paymentAccount = paymentAccount;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public Double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
}
