package com.gsta.v2.entity;

import java.io.Serializable;

/**
 *@author wubo
 *@createtime 2012-9-21
 */
public class TxUserInfo implements Serializable {

	private TxUserInfoData data;

	public TxUserInfoData getData() {
		return data;
	}

	public void setData(TxUserInfoData data) {
		this.data = data;
	}

}
