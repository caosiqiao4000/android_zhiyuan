package com.gsta.v2.entity;

import java.util.Date;

/**
 * 推荐产品信息类 
 * @author Administrator
 *
 */
public class RecommendInfo {
	public RecommendInfo(){};
	
	private String title;  // 标题
	private Date time;
	private String productNum;  // 编号
	private String describe;   // 描述
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	
}
