package com.gsta.v2.entity;

import java.io.Serializable;

/**
 *@author wubo
 *@createtime 2012-9-26
 */
public class StatInfo implements Serializable {

	private String uid;// 0~1 代理人用户ID
	private String billcycle;// 0~1 账期
	private Double trafficDirect;// 1 直接话务和流量分成
	private Double brokerageDirect;// 1 直接佣金
	private Double trafficIndirect;// 1 间接话务和流量分成
	private Double brokerageIndirect;// 1 间接佣金
	private Double totalFee;// 1 总额

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getBillcycle() {
		return billcycle;
	}

	public void setBillcycle(String billcycle) {
		this.billcycle = billcycle;
	}

	public Double getTrafficDirect() {
		return trafficDirect;
	}

	public void setTrafficDirect(Double trafficDirect) {
		this.trafficDirect = trafficDirect;
	}

	public Double getBrokerageDirect() {
		return brokerageDirect;
	}

	public void setBrokerageDirect(Double brokerageDirect) {
		this.brokerageDirect = brokerageDirect;
	}

	public Double getTrafficIndirect() {
		return trafficIndirect;
	}

	public void setTrafficIndirect(Double trafficIndirect) {
		this.trafficIndirect = trafficIndirect;
	}

	public Double getBrokerageIndirect() {
		return brokerageIndirect;
	}

	public void setBrokerageIndirect(Double brokerageIndirect) {
		this.brokerageIndirect = brokerageIndirect;
	}

	public Double getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Double totalFee) {
		this.totalFee = totalFee;
	}



	public StatInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

}
