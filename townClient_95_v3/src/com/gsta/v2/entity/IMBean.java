package com.gsta.v2.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class IMBean implements Serializable {

    private int id; // 私信编号
    private String myUserId;// 登录用户的uid
    private String hisUserId;// 聊天用户id
    private String userName;// 聊天自己的用户名
    private String hisName;// 聊天对方的用户名(可以防止改变名称)
    private String hisPhotoPath;// 聊天对方的地址
    private String content; // 信息的内容
    private String time; // 发信息的时间
    private String direction; // 谁发的信息(0表示登陆者,1表示聊天对象)
    private String userType;// 用户类型
    private String sended;// 是否发送成功(0失败,1成功);
    private String flag;// 是否阅读(0未读,1已读)

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the myUserId
     */
    public String getMyUserId() {
        return myUserId;
    }

    /**
     * @param myUserId
     *            the myUserId to set
     */
    public void setMyUserId(String myUserId) {
        this.myUserId = myUserId;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     *            the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time
     *            the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the direction
     */
    public String getDirection() {
        return direction;
    }

    /**
     * @param direction
     *            the direction to set
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getHisUserId() {
        return hisUserId;
    }

    public void setHisUserId(String hisUserId) {
        this.hisUserId = hisUserId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public IMBean() {
    }

    public IMBean(String hisUserId, String hisName,String hisPhotoPath) {
        super();
        this.hisUserId = hisUserId;
        this.hisName = hisName;
        this.hisPhotoPath = hisPhotoPath;
    }

    public String getSended() {
        return sended;
    }

    public void setSended(String sended) {
        this.sended = sended;
    }

    public String getHisName() {
        return hisName;
    }

    public void setHisName(String hisName) {
        this.hisName = hisName;
    }

    public String getHisPhotoPath() {
        return hisPhotoPath;
    }

    public void setHisPhotoPath(String hisPhotoPath) {
        this.hisPhotoPath = hisPhotoPath;
    }

}
