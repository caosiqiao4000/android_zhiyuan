package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 间接佣金类
 * @author caosq
 *
 */
public class IndirectDeductDetail implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 527966410127412736L;
	private String orderTime;// 订单时间
	private String orderId;// 订单号
	private String agentNames;// 下级代理
	private String productName;// 商品名称
	private Integer orderCount;// 销售数量
	private Double amount;// 销售金额（元）
	private Double brokerageIndirect;// 间接佣金（元）
	private Double deducts;// 预期间接分成（元）
	private String ifStat;// 结算标识
	private Integer deductType; //分成方式
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getAgentNames() {
		return agentNames;
	}
	public void setAgentNames(String agentNames) {
		this.agentNames = agentNames;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getOrderCount() {
		return orderCount;
	}
	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getBrokerageIndirect() {
		return brokerageIndirect;
	}
	public void setBrokerageIndirect(Double brokerageIndirect) {
		this.brokerageIndirect = brokerageIndirect;
	}
	public Double getDeducts() {
		return deducts;
	}
	public void setDeducts(Double deducts) {
		this.deducts = deducts;
	}
	public String getIfStat() {
		return ifStat;
	}
	public void setIfStat(String ifStat) {
		this.ifStat = ifStat;
	}
	public Integer getDeductType() {
		return deductType;
	}
	public void setDeductType(Integer deductType) {
		this.deductType = deductType;
	}
}
