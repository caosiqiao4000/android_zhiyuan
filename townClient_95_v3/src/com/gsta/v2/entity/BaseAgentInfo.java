package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * @author: chenjd
 * @createTime: 2012-8-30 下午5:13:49
 * @version: 1.0
 * @desc: 代理商基本信息
 */
public class BaseAgentInfo implements Serializable {

    /**
     * 2013-6-1
     */
    private static final long serialVersionUID = 1L;
    private String uid;// 调用者UID
    private Integer agentIdentity;// 代理商身份类别
    private Integer identityType;// 证件类型
    private String identityNo;// 证件号码
    private String agentName;// 代理商名称
    private String userName;// 代理商真实名称
    private String provinceId;// 所属省份
    private String agentNo;// 社区经理工号
    private String cityId;// 所属地市
    private String areaId;// 所属地区
    private String phone;// 联系电话
    private String mobile;// 手机号码
    private String address;// 通讯或者邮寄地址
    private String zipCode;// 邮编号码
    private String followId;// 上家代理uid
    private String followAccount;// 上家代理帐号
    private String email;// 邮箱
    private String qq;// QQ账号
    private String msn;// MSN账号
    private String domain;// 代理商店铺二级域名
    private String shopName;// 店铺名称

    public BaseAgentInfo() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getAgentIdentity() {
        return agentIdentity;
    }

    public void setAgentIdentity(Integer agentIdentity) {
        this.agentIdentity = agentIdentity;
    }

    public Integer getIdentityType() {
        return identityType;
    }

    public void setIdentityType(Integer identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getFollowId() {
        return followId;
    }

    public void setFollowId(String followId) {
        this.followId = followId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }

    public String getFollowAccount() {
        return followAccount;
    }

    public void setFollowAccount(String followAccount) {
        this.followAccount = followAccount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

}
