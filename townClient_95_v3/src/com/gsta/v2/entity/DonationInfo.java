package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 
 * @author: chenjd
 * @createTime: 2012-10-12 下午3:58:32 
 * @version: 1.0
 * @desc:赠品展示信息
 */
public class DonationInfo implements Serializable{
	
	private String itemName;// 赠品名称
	private String itenDesc;// 赠品描述
	private String itemUrl;// 赠品图片URL
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItenDesc() {
		return itenDesc;
	}
	public void setItenDesc(String itenDesc) {
		this.itenDesc = itenDesc;
	}
	public String getItemUrl() {
		return itemUrl;
	}
	public void setItemUrl(String itemUrl) {
		this.itemUrl = itemUrl;
	}
}
