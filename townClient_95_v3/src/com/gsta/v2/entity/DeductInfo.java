package com.gsta.v2.entity;

import java.io.Serializable;

/**
 * 商品分成信息
 * 
 * @author: chenjd
 * @createTime: 2012-10-14 下午1:37:47
 * @version: 1.0
 * @desc:
 */
public class DeductInfo implements Serializable{
	private String levelName;// 等级名称
	private String levelCode;// 等级编码
	private String brokerageDirect;// 直接佣金
	private String brokerageIndirect;// 间接佣金
	private String trafficDirectRatio;// 直接分成
	private String trafficIndirectRatio;// 间接分成
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public String getLevelCode() {
		return levelCode;
	}
	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}
	public String getBrokerageDirect() {
		return brokerageDirect;
	}
	public void setBrokerageDirect(String brokerageDirect) {
		this.brokerageDirect = brokerageDirect;
	}
	public String getBrokerageIndirect() {
		return brokerageIndirect;
	}
	public void setBrokerageIndirect(String brokerageIndirect) {
		this.brokerageIndirect = brokerageIndirect;
	}
	public String getTrafficDirectRatio() {
		return trafficDirectRatio;
	}
	public void setTrafficDirectRatio(String trafficDirectRatio) {
		this.trafficDirectRatio = trafficDirectRatio;
	}
	public String getTrafficIndirectRatio() {
		return trafficIndirectRatio;
	}
	public void setTrafficIndirectRatio(String trafficIndirectRatio) {
		this.trafficIndirectRatio = trafficIndirectRatio;
	}

}
