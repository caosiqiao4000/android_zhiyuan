package com.gsta.v2.entity;

/**
 * 我的卖家信息
 * @author zengxj
 * @createTime 2013-5-23
 * @version: 1.0
 * @desc:
 */
public class SellerInfo {
	
	private String agentUid; // 卖家UID,唯一标识
	private String photo;    // 卖家头像地址
	private String shopName; // 卖家店铺名称
	 private String levelCode;// 等级编码
	private String memo;     // 卖家店铺公告
	private Double longitude;// 卖家店铺位置信息-经度
	private Double latitude; // 卖家店铺位置信息-纬度
    private Long distance;//距离
	
	public SellerInfo(){
		
	}
	
	public String getAgentUid() {
		return agentUid;
	}
	public void setAgentUid(String agentUid) {
		this.agentUid = agentUid;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}
	
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Long getDistance() {
		return distance;
	}

	public void setDistance(Long distance) {
		this.distance = distance;
	}
}