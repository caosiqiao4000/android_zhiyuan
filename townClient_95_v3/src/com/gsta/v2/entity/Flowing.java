package com.gsta.v2.entity;

import java.io.Serializable;

public class Flowing implements Serializable{
	private int id;
	private String mobile;
	private String imsi;
	private int exeid;
	private String exename;
	private String exeimage;
	private String starttime;
	private String endtime;
	private String mode;
	private String type;
	private double count;
	private int status;
	private int isup;
	
	public int getID()
	{
		return id;
	}
	public String getMobile() {
		return mobile;
	}
	public String getIMSI() {
		return imsi;
	}
	public int getExeID() {
		return exeid;
	}
	public String getExename() {
		return exename;
	}
	public String getExeimage() {
		return exeimage;
	}
	public String getStarttime() {
		return starttime;
	}
	public String getEndtime()
	{
		return endtime;
	}
	public String getMode()
	{
		return mode;
	}
	public String getType()
	{
		return type;
	}
	public double getCount()
	{
		return count;
	}
	public int getStatus()
	{
		return status;
	}
	public int getIsup()
	{
		return isup;
	}
	public void setID(int ID)
	{
		id=ID;
	}
	public void setMobie(String Mobile)
	{
		mobile=Mobile;
	}
	public void setIMSI(String IMSI)
	{
		imsi=IMSI;
	}
	public void setExeID(int ExeID)
	{
		exeid=ExeID;
	}
	public void setExename(String Exename)
	{
		exename=Exename;
	}
	public void setExeimage(String Exeimage)
	{
		exeimage=Exeimage;
	}
	public void setStarttime(String Starttime)
	{
		starttime=Starttime;
	}
	public void setEndtime(String Endtime)
	{
		endtime=Endtime;
	}
	public void setMode(String Mode)
	{
		mode=Mode;
	}
	public void setType(String Type)
	{
		type=Type;
	}
	public void setCount(double Count)
	{
		count=Count;
	}
	public void setStatus(int Status)
	{
		status=Status;
	}
	public void setIsup(int Isup)
	{
		isup=Isup;
	}
}
